<?php
include_once 'conexion.php';

/**
 * Logiciel : exemple d'utilisation de HTML2PDF
 * 
 * Convertisseur HTML => PDF
 * Distribu� sous la licence LGPL. 
 *
 * @author		Laurent MINGUET <webmaster@html2pdf.fr>
 * 
 * isset($_GET['vuehtml']) n'est pas obligatoire
 * il permet juste d'afficher le r�sultat au format HTML
 * si le param�tre 'vuehtml' est pass� en param�tre _GET
 */
 	// r�cup�ration du contenu HTML

 	ob_start();

 	include(dirname(__FILE__).'/fuente/Imp_Reporte_Asignado.php');
//  include(dirname(__FILE__).'/fuente/Imp_frm_reteica_ccg.php');
	
	$content = ob_get_clean();
	
	// conversion HTML => PDF
	require_once(dirname(__FILE__).'/../libpdf2/html2pdf.class.php');
//		require_once(dirname(__FILE__).'/../../../libpdf2/html2pdf.class.php');
	try
	{

set_time_limit(0);
        //$html2pdf = new HTML2PDF('l','A4','es', false, 'ISO-8859-15', array(8, 8, 8, 8)); 
		$html2pdf = new HTML2PDF('P','A4', 'fr', false, 'ISO-8859-15');
    	$html2pdf->setDefaultFont('Arial');
		$html2pdf->writeHTML($content, isset($_GET['vuehtml']));
		$html2pdf->Output('exemple01.pdf');
	}
	catch(HTML2PDF_exception $e) { echo $e; }


?>