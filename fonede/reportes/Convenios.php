<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
	
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Listado Seg&uacute;n su Convenio</title>

<link type="text/css" href="../../css/css.1.8/jquery-ui-1.8.17.custom.css" rel="stylesheet" />
<script language="javascript" src="../../js/1.6/jquery-1.6.2.min.js"></script>
<script language="javascript" src="../../js/1.6/jquery-ui-1.8.16.custom.min.js"></script>

<link href="../../css/Estilos.css" rel="stylesheet" type="text/css" />
<link type="text/css" href="../../css/marco.css" rel="stylesheet"/>

<script languaje="javascript" src="js/reportes.js"></script>

<script>
function generareportepostulado(id) {

		var page = '../pdf/pdf_reporte_Convenios.php?ok=ok&variable_postulado='+id;
		//window.location.href=page;
		window.open(page,'_blank');			
        //location.href='pdf/pdf_reportes_Reteica.php?id_formulario='+id+'&estado=2';
}
</script>
  <script>
function verpostulados(tipoconvenio,seccional_postulados,vinculacion_postulados,estados,check_direccion_postulado
,check_telefono_postulado,check_seccional_postulado,check_genero_postulado,periododesde,periodohasta){

var page = 'Convenios.php?tipoconvenio='+tipoconvenio
		 +'&seccional_postulados='+seccional_postulados
		 +'&vinculacion_postulados='+vinculacion_postulados
		 +'&estados='+estados		
         +'&check_direccion_postulado='+check_direccion_postulado
         +'&check_telefono_postulado='+check_telefono_postulado
		 +'&check_seccional_postulado='+check_seccional_postulado		
		 +'&check_genero_postulado='+check_genero_postulado
		 +'&periododesde='+periododesde
		 +'&periodohasta='+periodohasta
         +'&ok=ok';
         window.location.href=page;   	
	
}//fin function verportulados(){ 
  </script>      

<script>
    $(function() {
        $( "#periododesde,#periodohasta" ).datepicker({
		/*
		changeMonth: true,
	    changeYear: true,
    	maxDate: '+0D', 
	    constrainInput: false,
		*/
		});
    });
</script> 

<script type="text/javascript">
    function marcar(source)
    {
        checkboxes=document.getElementsByTagName('input'); //obtenemos todos los controles del tipo Input
        for(i=0;i<checkboxes.length;i++) //recoremos todos los controles
        {
            if(checkboxes[i].type == "checkbox") //solo si es un checkbox entramos
            {
                checkboxes[i].checked=source.checked; //si es un checkbox le damos el valor del checkbox que lo llam� (Marcar/Desmarcar Todos)
            }
        }
    }
</script>

</head>

<body>
<p>
  <?php

ini_set('display_errors','Off'); 
include_once '../../rsc/pdo/IFXDbManejador.php';
include_once '../../rsc/pdo/IFXerror.php'; 


$db = IFXDbManejador::conectarDB(); 
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();	
}

$sql="SELECT * FROM aportes500";
$rs_seccional=$db->querySimple($sql);

$sql="SELECT * FROM aportes221";
$rs_vinculacion=$db->querySimple($sql);

$sql_tipoconvenio="SELECT * FROM aportes220 ORDER BY descripcion ASC";
$rs_tipoconvenio=$db->querySimple($sql_tipoconvenio);

function cambiarFormatoFecha2($fecha){
    list($anio,$mes,$dia)=explode("-",$fecha);
     return $dia."-".$mes."-".$anio;
}
		
?>
  <?php

if ($_GET['ok'] == 'ok') {
	
			  		  			 			  	

$check_direccion_postulado=$_GET['check_direccion_postulado'];
$check_telefono_postulado=$_GET['check_telefono_postulado'];
$check_seccional_postulado=$_GET['check_seccional_postulado'];
$check_genero_postulado=$_GET['check_genero_postulado'];

$tipoconvenio=$_GET['tipoconvenio'];
$vinculacion_postulados=$_GET['vinculacion_postulados'];
$seccional_postulados=$_GET['seccional_postulados'];

$arreglo= explode(";", $_GET['estados']);
$estados=$arreglo[0];
$procesos=$arreglo[1];

$periododesde=$_GET['periododesde'];
$periodohasta=$_GET['periodohasta'];

	$sql = "select aportes221.idpersona,aportes221.estado,aportes221.idagencia,aportes221.vinculacion,aportes221.especial,aportes221.idradicacion
            ,aportes221.fecharadicacion,aportes221.fechagrabacion,aportes221.fechapostulacion,aportes221.fechaasignacion,aportes221.fechagiro
	        ,aportes221.apto,aportes221.ideps,aportes221.identidadeducativa
	        ,aportes015.identificacion,aportes015.pnombre, aportes015.snombre, aportes015.papellido, aportes015.sapellido
			,aportes015.direccion, aportes015.telefono,aportes015.sexo,aportes500.agencia
			,aportes220.idconvenio,aportes220.codigo,aportes220.valoralimentacion,aportes220.valorsalud,aportes220.valoreducacion
			from aportes221,aportes015,aportes500,aportes220
			where 1=1
			and aportes221.idpersona=aportes015.idpersona
			and aportes221.idagencia=aportes500.codigo 
			and aportes221.idconvenio=aportes220.idconvenio
			and	aportes221.idconvenio='".$tipoconvenio."'
			and aportes221.vinculacion='".$vinculacion_postulados."'";
			
	 
	if ($seccional_postulados !='0') {
	$sql = $sql . " and aportes221.idagencia=" . $seccional_postulados;
	}	
	if ($estados!='') {
	$sql = $sql . " and aportes221.estado= '".$estados."'";
	}	
	if($procesos !='')
	{
	$sql= $sql . " and aportes221.proceso='".$procesos."'";
	}
	if ($estados=='R' && $procesos=='') {
	$sql = $sql . " and aportes221.fecharadicacion BETWEEN '".$periododesde."' and '".$periodohasta."'";
	} 
	if (($periododesde != '' && $periodohasta != '') && ($estados=='P' || $estados=='S' || $estados=='X' || $estados=='T' || ($estados=='R' && $procesos=='Gr') || $estados=='')) {
	$sql = $sql . " and aportes221.fechagrabacion BETWEEN '".$periododesde."' and '".$periodohasta."'";
	} 
	if ($periododesde != '' && $periodohasta != '' && $estados=='R' && $procesos=='Po') {
	$sql = $sql . " and aportes221.fechapostulacion BETWEEN '".$periododesde."' and '".$periodohasta."'";
	}  
	if ($periododesde != '' && $periodohasta != '' && $estados=='A' && $procesos=='As') {
	$sql = $sql . " and aportes221.fechaasignacion BETWEEN '".$periododesde."' and '".$periodohasta."'";
	}
	if ($periododesde != '' && $periodohasta != '' && $estados=='G' && $procesos=='Gi') {
	$sql = $sql . " and aportes221.fechagiro BETWEEN '".$periododesde."' and '".$periodohasta."'";
	}
	
	$sql = $sql . " order by especial DESC, aportes221.idradicacion ASC"; 

  	$rs=$db->querySimple($sql); 
	$rs1=$db->querySimple($sql);
			
	//echo'<br>sql= '.$sql; 			
	$encontrodatos=0;
	while($registro1=$rs1->fetch()){	
	++$encotrodatos;			 
	 }

  if($estados=='R' && $procesos=='')
	{
		$tituloestado='RADICADAS';
	}
	else 
    if($estados=='R' && $procesos=='Gr')
	{
		$tituloestado='GRABADAS';
	}
	else
    if($estados=='R' && $procesos=='Po')
	{
		$tituloestado='POSTULADAS';
	}
	else
	if($estados=='A' && $procesos=='As')
	{
		$tituloestado='ASIGNADAS';
	}
	else
	if($estados=='G' && $procesos=='Gi')
	{
		$tituloestado='GIRADAS';
	}
	else
	if($estados=='P')
	{
		$tituloestado='PENDIENTES';
	}
	else
	if($estados=='S')
	{
		$tituloestado='SUSPENDIDAS';
	}
	else
	if($estados=='X')
	{
		$tituloestado='RECHAZADAS';
	}
	else
	if($estados=='T')
	{
		$tituloestado='TERMINADAS';
	}

$sqlap220="SELECT * FROM aportes220 WHERE idconvenio='".$tipoconvenio."'";
$rsap220=$db->querySimple($sqlap220);
while($rowap220=$rsap220->fetch()){	
	$descripcion=$rowap220['descripcion'];
	$vigencia=$rowap220['vigencia'];
	$valoralimentacion=$rowap220['valoralimentacion'];
	$valorsalud=$rowap220['valorsalud'];
	$valoreducacion=$rowap220['valoreducacion'];	
	 }
	 
if($encotrodatos!=0)
{	 
	
	if($vinculacion_postulados==1)
	{ 	
		$tituloconosin="SIN VINCULACI&Oacute;N";
	}
	else
	if($vinculacion_postulados==2)
	{
		$tituloconosin="CON VINCULACI&Oacute;N";
	}
?>
</p>
<table height="172" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="29" class="arriba_iz">&nbsp;</td>
    <td class="arriba_ce"><span class="letrablanca">::&nbsp;<strong>LISTADO DE PERSONAS <?php echo $tituloestado; ?> CON CONVENIO DE <?php echo strtoupper($descripcion)." ".$vigencia." ".$tituloconosin;?></strong>&nbsp;::</span></td>
    <td class="arriba_de" >&nbsp;</td>
  </tr>
  <tr>
    <td class="cuerpo_iz">&nbsp;</td>
    <td class="cuerpo_ce"><a href="Convenios.php" title="Volver"><img src="../imagenes/left_green.png" width="20" height="20" /></a><a href="../Excel/Excel_ReporteConvenios.php?ok=ok&amp;check_direccion_postulado=<?php echo $_GET['check_direccion_postulado'];?>&amp;
     check_telefono_postulado=<?php echo $_GET['check_telefono_postulado'];?>&amp;
     check_seccional_postulado=<?php echo $_GET['check_seccional_postulado'];?>&amp;     
     check_genero_postulado=<?php echo $_GET['check_genero_postulado'];?>&amp;
     tipoconvenio=<?php echo $_GET['tipoconvenio'];?>&amp;  
     vinculacion_postulados=<?php echo $_GET['vinculacion_postulados'];?>&amp;  
     seccional_postulados=<?php echo $_GET['seccional_postulados'];?>&amp;    
     estados=<?php echo $_GET['estados'];?>&amp;     
     periododesde=<?php echo $_GET['periododesde'];?>&amp; 
     periodohasta=<?php echo $_GET['periodohasta'];?>&amp;  
     " target="_blank" title="Excel"><img src="../imagenes/excel.png" width="20" height="20" /></a>
      <?php $variable_postulado = $_GET['check_direccion_postulado'].'*'.$_GET['check_telefono_postulado'].'*'.$_GET['check_seccional_postulado'].'*'.$_GET['check_genero_postulado'].'*'.$_GET['tipoconvenio'].'*'.$_GET['vinculacion_postulados'].'*'.$_GET['seccional_postulados'].'*'.$_GET['estados'].'*'.$_GET['periododesde'].'*'.$_GET['periodohasta'];?>
    <img src="../imagenes/pdf.png" width="20" id="<?php echo $variable_postulado; ?>" title="Generar Pdf" onclick="generareportepostulado(this.id);"/></td>
    <td class="cuerpo_de">&nbsp;</td>
  </tr>
  <tr>
    <td class="cuerpo_iz">&nbsp;</td>
    <!-- TABLAS DE FORMULARIO -->
    <td class="cuerpo_ce"><center>
      <table border="0" cellspacing="0" class="tablero">
        <tr>
          <td><center>
            <table width="100%" class="tablero">
              <tr>
                <th align="center">conse</th>
                <th align="center">N&uacute;mero radicado</th>
                <th align="center">Identificaci&oacute;n</th>
                <th align="center">Nombre y Apellido</th>
               <?php 
				if($estados=='R' && $procesos=='')
				{
				?>
                <th align="center">Fecha Radicaci&oacute;n</th> 
                 <?php 
				}
				?>
                
                <?php 
				if($estados=='P' || $estados=='S' || $estados=='X' || $estados=='T' || ($estados=='R' && $procesos=='Gr') || $estados=='')
				{
				?>
                <th align="center">Fecha Grabaci&oacute;n</th> 
                 <?php 
				}
				?>
                
                 <?php 
				if($estados=='R' && $procesos=='Po')
				{
				?>
                <th align="center">Fecha Postulaci&oacute;n</th> 
                 <?php 
				}
				?>
                
                 <?php 
				if($estados=='A' && $procesos=='As')
				{
				?>
                <th align="center">Fecha Asignaci&oacute;n</th> 
                 <?php 
				}
				?>
                
                 <?php 
				if($estados=='G' && $procesos=='Gi')
				{
				?>
                <th align="center">Fecha Giro</th> 
                 <?php 
				}
				?>   
                
                   
                <?php
				if($valoralimentacion!=0)
				{
					$variableA='A';
				?>
                <th align="center">Valor Alimentaci&oacute;n</th> 
                <?php
				}// fin valoralimentacion
				?>
                
                <?php
				if($valorsalud!=0)
				{
					$variableS='S';
				?>
                <th align="center">Valor Salud</th>
                <?php
				}// fin valorsalud
				?>
                
                <?php
				if($valoreducacion!=0)
				{
					$variableE='E';
				?>
                <th align="center">Valor Educaci&oacute;n</th>    
                <?php
				}// valoreducacion
				?>
                
                
                <?php
				if(($variableS=='S' || $variableA!='A') && $valorsalud!=0)
				{					
				?>              
                <th align="center">Eps</th>
                  <?php
				}// eps
				?>
                 <?php
				if(($variableE=='E' || $variableA!='A') && $valoreducacion!=0)
				{					
				?>                 
                <th align="center"> Entidad Educativa</th>
                 <?php
				}// entidad educativa
				?>
                                           
                <?php
			if($check_direccion_postulado=="true")
			  {
				  ?>
                <th align="center">Direcci&oacute;n</th>
                <?php
			  }
			if($check_telefono_postulado=="true")
			  {
				  ?>
                <th align="center">Tel&eacute;fono</th>
                <?php
			  }
			 if($check_seccional_postulado=="true")
			  {
				  ?>
                <th align="center">Seccional</th>            
                <?php
			  }  
			 if($check_genero_postulado=="true")
			  {
				  ?>
                <th align="center">Genero</th>
                <?php
			  }  			  			  			  			  			  
			?>		             
               <th align="center">Estado</th>		           
                </tr>
              <?php 
 $cont_postulados=0;
 $Talimento=0;
 $Tsalud=0;
 $Teducacion=0;
  
 while($registro=$rs->fetch()){
 ++$cont_postulados;
 
 $Talimento=$Talimento + $registro['valoralimentacion'];
 $Teducacion=$Teducacion + $registro['valoreducacion'];
 $Tsalud=$Tsalud + $registro['valorsalud'];
 
 
   ?>
              <tr>
                <td align="center"><?php echo $cont_postulados; ?> &nbsp;</td>
                <td align="center"><?php echo $registro['idradicacion']; ?>&nbsp;</td>
                <td align="center"><?php echo $registro['identificacion']; ?>&nbsp;</td>
                <td align="center"><?php echo utf8_encode($registro['pnombre']." ".$registro['snombre']." ".$registro['papellido']." ".$registro['sapellido']); ?>&nbsp;</td>
                 <?php 
				if($estados=='R' && $procesos=='')
				{
				?>
                <td align="center"><?php echo $registro['fecharadicacion']; ?>&nbsp;</td>
                 <?php 
				}
				?>
                 
				<?php 
				if($estados=='P' || $estados=='S' || $estados=='X' || $estados=='T' || ($estados=='R' && $procesos=='Gr') || $estados=='')
				{
				?>
                <td align="center"><?php echo $registro['fechagrabacion']; ?>&nbsp;</td>
                 <?php 
				}
				?>
                
                 <?php 
				if($estados=='R' && $procesos=='Po')
				{
				?>
                <td align="center"><?php echo $registro['fechapostulacion']; ?>&nbsp;</td>
                 <?php 
				}
				?>
                
                 <?php 
				if($estados=='A' && $procesos=='As')
				{
				?>
                <td align="center"><?php echo $registro['fechaasignacion']; ?>&nbsp;</td> 
                 <?php 
				}
				?>
                
                 <?php 
				if($estados=='G' && $procesos=='Gi')
				{
				?>
                <td align="center"><?php echo $registro['fechagiro']; ?>&nbsp;</td> 
                 <?php 
				}
				?>         
                
                <?php
				if($valoralimentacion!=0)
				{
				?>
                <td align="center"><?php echo number_format($registro['valoralimentacion'], 0, '', '.'); ?>&nbsp;</td>
                <?php
				}// fin valoralimentacion
				?>
                
                <?php
				if($valorsalud!=0)
				{
				?>
                <td align="center"><?php echo number_format($registro['valorsalud'], 0, '', '.'); ?>&nbsp;</td>
                <?php
				}// fin valorsalud
				?>
                
                <?php
				if($valoreducacion!=0)
				{
				?>
                <td align="center"><?php echo number_format($registro['valoreducacion'], 0, '', '.'); ?>&nbsp;</td>   
                <?php
				}// valoreducacion
				?>
                
                
                 <?php
				if(($variableS=='S' || $variableA!='A') && $valorsalud!=0)
				{					
				?>              
                <td align="center"><?php
				  $sqlAp206="SELECT * FROM aportes206 where idadministradora='".$registro['ideps']."'";
				  $rsAp206=$db->querySimple($sqlAp206);
				  
				  while($rowAp206=$rsAp206->fetch()){	
				   $razonsocial=$rowAp206['razonsocial'];
				   
						}//fin while					
					 echo $razonsocial;?>&nbsp;
                 </td>
                  <?php
				}// eps
				?>
                 <?php
				if(($variableE=='E' || $variableA!='A') && $valoreducacion!=0)
				{					
				?>                 
                <td align="center"><?php			
				  $sqlap091="SELECT * FROM aportes091 where iddetalledef='".$registro['identidadeducativa']."'";
				  $rsap091=$db->querySimple($sqlap091);
				  
				  while($rowap091=$rsap091->fetch()){	
				   $detalledefinicion1=$rowap091['detalledefinicion'];
				   
						}//fin while					
					 echo $detalledefinicion1;?>&nbsp;
                </td>
                 <?php
				}// entidad educativa
				?>              
                
                               
                <?php
                if($check_direccion_postulado=="true")
                  {
                      ?>
                <td align="center"><?php echo utf8_encode($registro['direccion']); ?>&nbsp;</td>
                <?php
                  }
                if($check_telefono_postulado=="true")
                  {
                      ?>
                <td align="center"><?php echo utf8_encode($registro['telefono']); ?>&nbsp;</td>
                <?php
                  }
                if($check_seccional_postulado=="true")
                  {
                      ?>
                <td align="center"><?php echo utf8_encode($registro['agencia']); ?>&nbsp;</td>
                <?php
                  }	    			  			  			  			  			     			  			  			  			  			  
                if($check_genero_postulado=="true")
                  {
                      ?>
                <td align="center"><?php echo $registro['sexo']; ?>&nbsp;</td>
                <?php
                  }	    			  			  			  			  			  
                     ?>               	
                <td align="center"><?php echo $registro['estado']; ?>&nbsp;</td>			
                </tr>
              <?php
        }//fin while
    ?>
              <tr>
                <td colspan="4" align="center" bgcolor="#F7D358" style="border:0"><strong>TOTAL</strong></td>
                 <?php 
				if($estados=='R' && $procesos=='')
				{
				?>
                 <td align="center" bgcolor="#F7D358" style="border:0">&nbsp;</td> 
                 <?php 
				}
				?>
                 
				<?php 
				if($estados=='P' || $estados=='S' || $estados=='X' || $estados=='T' || ($estados=='R' && $procesos=='Gr') || $estados=='')
				{
				?>
                 <td align="center" bgcolor="#F7D358" style="border:0">&nbsp;</td> 
                 <?php 
				}
				?>
                
                 <?php 
				if($estados=='R' && $procesos=='Po')
				{
				?>
                 <td align="center" bgcolor="#F7D358" style="border:0">&nbsp;</td> 
                 <?php 
				}
				?>
                
                 <?php 
				if($estados=='A' && $procesos=='As')
				{
				?>
                 <td align="center" bgcolor="#F7D358" style="border:0">&nbsp;</td>  
                 <?php 
				}
				?>
                
                 <?php 
				if($estados=='G' && $procesos=='Gi')
				{
				?>
                 <td align="center" bgcolor="#F7D358" style="border:0">&nbsp;</td> 
                 <?php 
				}
				?>                
                
                <?php 
				if($valoralimentacion!=0)
				{
				?>
                <td align="center" bgcolor="#F7D358" style="border:0"><strong><?php echo number_format($Talimento, 0, '', '.'); ?>&nbsp;</strong></td>
                <?php
				}// fin valoralimentacion
				?>
                
                <?php
				if($valorsalud!=0)
				{
				?>
                <td align="center" bgcolor="#F7D358" style="border:0"><strong><?php echo number_format($Tsalud, 0, '', '.'); ?>&nbsp;</strong></td>
                <?php
				}// fin valorsalud
				?>
                
                <?php
				if($valoreducacion!=0)
				{
				?>
                <td align="center" bgcolor="#F7D358" style="border:0"><strong><?php echo number_format($Teducacion, 0, '', '.'); ?>&nbsp;</strong></td>   
                <?php
				}// valoreducacion
				?>                
                 
                 <?php
				if(($variableS=='S' || $variableA!='A') && $valorsalud!=0)
				{					
				?>              
                <td align="center" bgcolor="#F7D358" style="border:0">&nbsp;</td>
                  <?php
				}// eps
				?>
                 <?php
				if(($variableE=='E' || $variableA!='A') && $valoreducacion!=0)
				{					
				?>                 
                <td align="center" bgcolor="#F7D358" style="border:0">&nbsp;</td>
                 <?php
				}// entidad educativa
				?>              
                
                               
                <?php
                if($check_direccion_postulado=="true")
                  {
                      ?>
               <td align="center" bgcolor="#F7D358" style="border:0">&nbsp;</td>
                <?php
                  }
                if($check_telefono_postulado=="true")
                  {
                      ?>
                <td align="center" bgcolor="#F7D358" style="border:0">&nbsp;</td>
                <?php
                  }
                if($check_seccional_postulado=="true")
                  {
                      ?>
                <td align="center" bgcolor="#F7D358" style="border:0">&nbsp;</td>
                <?php
                  }	    			  			  			  			  			     			  			  			  			  			  
                if($check_genero_postulado=="true")
                  {
                      ?>
                <td align="center" bgcolor="#F7D358" style="border:0">&nbsp;</td>
                <?php
                  }	    			  			  			  			  			  
                     ?>               	
                <td align="center" bgcolor="#F7D358" style="border:0">&nbsp;</td>
                </tr>            
              </table>
            </center></td>
        </tr>
      </table>
    </center></td>
    <td class="cuerpo_de"></td>
    <!-- FONDO DERECHA -->
  </tr>
  <tr>
    <td height="41" class="abajo_iz">&nbsp;</td>
    <td class="abajo_ce">&nbsp;</td>
    <td class="abajo_de">&nbsp;</td>
  </tr>
</table>
<?php
  }// fin encontrodatos
		else
		{
			if($vinculacion_postulados==1)
			{
				echo "<script language='JavaScript'>alert('No se Encontraron Datos de Personas '+'".ucwords(strtolower($tituloestado))."'+' con Convenio de ' +'".ucwords(strtolower($descripcion))." ".$vigencia."'+ ' Sin Vinculacion'); location.href='Convenios.php';</script>";
			}
			else
			if($vinculacion_postulados==2)
			{
				echo "<script language='JavaScript'>alert('No se Encontraron Datos de Personas '+'".ucwords(strtolower($tituloestado))."'+' con Convenio de ' +'".ucwords(strtolower($descripcion))." ".$vigencia."'+ ' Con Vinculacion'); location.href='Convenios.php';</script>";
			}			
		}
    }//fin if $_GET['ok'] == 'ok'
	 else {
?>     		
 <form name="form_postulado" method="post" action="Postulacion.php">
  <table width="481" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td height="29" class="arriba_iz">&nbsp;</td>
      <td class="arriba_ce"><span class="letrablanca">::&nbsp;<strong>LISTADO SEGUN SU CONVENIO</strong>&nbsp;::</span></td>
      <td class="arriba_de" >&nbsp;</td>
    </tr>
    <tr>
      <td class="cuerpo_iz">&nbsp;</td>
      <td class="cuerpo_ce"><div id="error" style="color:#FF0000"></div></td>
      <td class="cuerpo_de">&nbsp;</td>
    </tr>
    <tr>
      <td class="cuerpo_iz">&nbsp;</td>
      <!-- TABLAS DE FORMULARIO -->
      <td class="cuerpo_ce"><center>
        <table width="367" border="0" cellspacing="0" class="tablero">
         <tr bgcolor="#EBEBEB">
            <td colspan="4" style="text-align:center" >&nbsp;</td>
          </tr>
          <tr>
            <td><strong>Convenio:</strong></td>
            <td colspan="3"><select name="tipoconvenio" id="tipoconvenio" class="box1" style="width:200px">
              <option value="">::Seleccione::</option>
              <?php	
	          while($row_tipoconvenio=$rs_tipoconvenio->fetch()){   
		         echo "<option value=".$row_tipoconvenio['idconvenio'].">".$row_tipoconvenio['descripcion']." ".$row_tipoconvenio['vigencia']."</option>";					  
				}// fin while 	  
               ?>
            </select>
              <img src="../../imagenes/menu/obligado.png" align="middle"/></td>
          </tr>
          <tr>
            <td><strong>Tipo de Vinculaci&oacute;n:</strong></td>
            <td colspan="3"><select name="vinculacion_postulados" size="1" id="vinculacion_postulados" class="box1">
              <option value="0" selected="selected">::Seleccione::</option>
              <option value="1">Sin Vinculacion</option>
              <option value="2">Con Vinculacion</option>
            </select>
              <img src="../../imagenes/menu/obligado.png" align="middle"/></td>
          </tr>
          <tr>
            <td><strong>Estados:</strong></td>
            <td colspan="3"><select name="estados" size="1" id="estados" class="box1">
              <option value="" selected="selected">::Todos::</option>
              <option value="R;">Radicados</option>
              <option value="R;Gr">Grabados</option>
              <option value="R;Po">Postulados</option>
              <option value="A;As">Asignados</option>
              <option value="G;Gi">Girados</option>
              <option value="P;">Pendientes</option>
              <option value="S;">Suspendidos</option>
              <option value="X;">Rechazados</option>
              <option value="T;">Terminados</option>
            </select></td>
          </tr>
          <tr>
            <td><strong>Seccional:</strong></td>
            <td colspan="3"><select name="seccional_postulados" size="1" id="seccional_postulados" class="box1">
              <option value="0">::Todos::</option>
              <?php
	while($row=$rs_seccional->fetch()){
	
	  echo "<option value=".$row['codigo'] ." >".$row['agencia']."</option>";
	}
	?>
              </select></td>
          </tr>
          <tr>
            <td><strong>Periodo Desde:</strong></td>
            <td colspan="3"><input name="periododesde" type="text" id="periododesde" onkeydown='solonumeros(this);' onkeyup='solonumeros(this);' value=""/>
              <img src="../../imagenes/menu/obligado.png" align="middle"/></td>
          </tr>
          <tr>
            <td><strong>Periodo Hasta:</strong></td>
            <td colspan="3"><input name="periodohasta" type="text" id="periodohasta" onkeydown='solonumeros(this);' onkeyup='solonumeros(this);' value=""/>
              <img src="../../imagenes/menu/obligado.png" align="middle"/></td>
          </tr>
          <tr>
            <td colspan="4">&nbsp;</td>
          </tr>
          <tr bgcolor="#EBEBEB">
            <td colspan="4" style="text-align:center" ><strong>Campos Adicionales</strong></td>
          </tr>
          <tr>
            <td colspan="4"><input type="checkbox" onclick="marcar(this);" />
              <strong>Marcar/Desmarcar Todos&nbsp;</strong></td>
            </tr>
          <tr>
            <td><input type="checkbox" name="check_genero_postulado" id="check_genero_postulado" />
              <label>Genero</label></td>
            <td colspan="3"><input type="checkbox" name="check_telefono_postulado" id="check_telefono_postulado" />
              <label>Tel&eacute;fono</label></td>
          </tr>
          <tr>
            <td><input name="check_seccional_postulado" type="checkbox" id="check_seccional_postulado" />
              <label>Seccional</label></td>
            <td colspan="3"><input type="checkbox" name="check_direccion_postulado" id="check_direccion_postulado" />
              <label>Direcci&oacute;n</label></td>
          </tr>
          <tr>
            <td colspan="4">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="4"><center><input name="button" id="button"  type="button" onclick="listarconvenio();" value="Generar" /></center></td>
          </tr>
        </table>
      </center></td>
      <td class="cuerpo_de"></td>
      <!-- FONDO DERECHA -->
    </tr>
    <tr>
      <td height="41" class="abajo_iz">&nbsp;</td>
      <td class="abajo_ce">&nbsp;</td>
      <td class="abajo_de">&nbsp;</td>
    </tr>
  </table>
</form>
<?php
}//fin else if $_GET['ok'] == 'ok'
?>
</body>
</html>