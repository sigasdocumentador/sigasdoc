<?php

	function calcular_dias($fecha_ini,$fecha_fin) {
		date_default_timezone_set("America/Bogota");
		$dia_ini = substr($fecha_ini, 8, 2);  
		$mes_ini =  substr($fecha_ini, 5, 2);
		$year_ini =substr($fecha_ini, 0, 4);

		$dia_fin = substr($fecha_fin, 8, 2);  
		$mes_fin = substr($fecha_fin, 5, 2);
		$year_fin= substr($fecha_fin, 0, 4);

		$timestamp1 = mktime(0,0,0,$mes_ini,$dia_ini,$year_ini);
		$timestamp2 = mktime(4,12,0, $mes_fin,$dia_fin,$year_fin);
				
		//resto a una fecha la otra
		$segundos_diferencia = $timestamp1 - $timestamp2;
		//echo $segundos_diferencia;
			
		//convierto segundos en d�as
		$dias_diferencia = $segundos_diferencia / (60 * 60 * 24);
			
		//obtengo el valor absoulto de los d�as (quito el posible signo negativo)
		$dias_diferencia = abs($dias_diferencia);
				
		//quito los decimales a los d�as de diferencia
		$dias_diferencia = floor($dias_diferencia);
				
		return $dias_diferencia;
				
	}
	
function calcular_fecha($fecha_ini, $fecha_fin){
	date_default_timezone_set("America/Bogota");
	$hoy = date("Y-m-d");
	$dia_a = date("d");
	$mes_a = date("m");
	$year_a = date("Y");
	$year_a= $year_a-3;
	$antes =	date($year_a."-".$mes_a."-".$dia_a);
	
//	echo $hoy."<br>";
//	echo $antes."<br>";

	if (($fecha_ini <= $antes) and ($fecha_fin >= $antes)){
		$fecha_ini = $antes;
		}
	if (($fecha_ini > $hoy)){
	}
	else {
		if (($fecha_fin >= $hoy) || ($fecha_fin == "") || ($fecha_fin == "000-00-00")){
			$fecha_fin = $hoy;
		}
	}
			
	
	if (($fecha_ini >= $antes) and ($fecha_fin <= $hoy)){
	//	 echo "cumple <br>";	
		$dias_periodo = calcular_dias($fecha_ini,$fecha_fin);
	//	echo "<br>dias del periodo ".$dias_periodo."<br>";
	}
	else{
	//	echo "NO cumple  <br>";	
			$dias_periodo=0;
	}
					
	
	return $dias_periodo;		
  
}
?>