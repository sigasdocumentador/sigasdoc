// JavaScript Document
var error=0;

function setCookie(c_name,value,exdays)
{
	var exdate=new Date();
	exdate.setDate(exdate.getDate() + exdays);
	var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
	document.cookie=c_name + "=" + c_value;
}// fin setCookie

// funcion para obtener cookie en javascript
	function getCookie(c_name)
	{
		var i,x,y,ARRcookies=document.cookie.split(";");
		for (i=0;i<ARRcookies.length;i++)
		{
  			x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
  			y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
  			x=x.replace(/^\s+|\s+$/g,"");
  			if (x==c_name)
    			{
    				return unescape(y);
    			}// fin if
		}// fin for
  	}// fin getCookie
//funcion para obtener cookie en javascript

 function borrarcookie(){
	 
	 $("table.tablero :input.ui-state-error,select.ui-state-error").removeClass("ui-state-error");//Limpiar la clase de error
	 	   
	  setCookie('numeroradicacion','',-1);
   	  setCookie('fecharadicacion','',-1);
	  setCookie('identificacion','',-1);
	 	  	  	    
	  setCookie('idpersona','',-1);
	  
	  setCookie('idpersonac','',-1);	
	  setCookie('idpersonab','',-1);  
	  
	  setCookie('pnombre','',-1);
	  setCookie('snombre','',-1);
	  setCookie('papellido','',-1);
	  setCookie('sapellido','',-1);
	  setCookie('dias','',-1);	
	  setCookie('vinculacion','',-1);
	  setCookie('vinculacion1','',-1);
	  setCookie('ciudad','',-1);
	  
	  setCookie('alertaasocajaspostulado','',-1);
	  setCookie('alertainactivopostulado','',-1);
	  setCookie('alertafosygapostulado','',-1);
	  	
	  setCookie('alertaasocajasbeneficiario','',-1);
	  setCookie('alertainactivobeneficiario','',-1);
	  setCookie('alertafosygabeneficiario','',-1);
	  
	  
	  setCookie('nombreasociacionartista','',-1);	
	  setCookie('telefonoasociacionartista','',-1);	
	  setCookie('nombreasociaciondeportista','',-1);	
	  setCookie('telefonoasociaciondeportista','',-1);	
	  setCookie('nombreasociacionescritor','',-1);	
	  setCookie('telefonoasociacionescritor','',-1);
	  	
	  setCookie('contar','',-1);
	  setCookie('tiempoguardado','',-1);
	  setCookie('verificaB','',-1);  
	  setCookie('conyuguesi','',-1);
	  setCookie('convivenconyugue','',-1);	  
	
	  location.reload(); 
    }// finborrarcookie

 function borrarcookie1(){	
 
 $("table.tablero :input.ui-state-error,select.ui-state-error").removeClass("ui-state-error");//Limpiar la clase de error
    
	  setCookie('numeroradicacion','',-1);
   	  setCookie('fecharadicacion','',-1);
	  setCookie('identificacion','',-1);
	 	  	  	    
	  setCookie('idpersona','',-1);	
	    
      setCookie('idpersonac','',-1);
	  setCookie('idpersonab','',-1);
	  
	  setCookie('pnombre','',-1);
	  setCookie('snombre','',-1);
	  setCookie('papellido','',-1);
	  setCookie('sapellido','',-1);
	  setCookie('dias','',-1);	
	  setCookie('vinculacion','',-1);
	  setCookie('vinculacion1','',-1);
	  setCookie('ciudad','',-1);  
	  
	  setCookie('alertaasocajaspostulado','',-1);
	  setCookie('alertainactivopostulado','',-1);
	  setCookie('alertafosygapostulado','',-1);
	  
	  setCookie('alertaasocajasbeneficiario','',-1);
	  setCookie('alertainactivobeneficiario','',-1);
	  setCookie('alertafosygabeneficiario','',-1);
	  
	  setCookie('nombreasociacionartista','',-1);	
	  setCookie('telefonoasociacionartista','',-1);	
	  setCookie('nombreasociaciondeportista','',-1);	
	  setCookie('telefonoasociaciondeportista','',-1);	
	  setCookie('nombreasociacionescritor','',-1);	
	  setCookie('telefonoasociacionescritor','',-1);
	  
	  setCookie('contar','',-1);	
	  setCookie('tiempoguardado','',-1);	  
      setCookie('verificaB','',-1);
	  setCookie('conyuguesi','',-1);
	  setCookie('convivenconyugue','',-1);
   
    }// borrarcookie1

function vehiculootrosdatos()
{
	if(document.form3.vehiculo.value == 'S')
	{
		document.form3.marca_otrosdatos.disabled = false; 
		document.form3.modelo_otrosdatos.disabled = false; 
		document.form3.placa_otrosdatos.disabled = false;
		
		document.form3.marca_otrosdatos.focus();
	}// fin if
	else
	{
		if(document.form3.vehiculo.value == 'N')
		{
			document.form3.marca_otrosdatos.disabled = true; 
			document.form3.modelo_otrosdatos.disabled = true; 
			document.form3.placa_otrosdatos.disabled = true;
			
			document.form3.marca_otrosdatos.value = ""; 
			document.form3.modelo_otrosdatos.value = ""; 
			document.form3.placa_otrosdatos.value = "";
			
		}// fin if
	}// fin else
	
}// fin vehiculo

function cuentabancariaotrosdatos()
{
   if(document.form3.cuentabancaria.value =='S')
	{
		document.getElementById("tipocuenta_otrosdatos").disabled = false; 
		document.getElementById("tipocuenta_otrosdatos").focus();
	
		document.getElementById("banco_otrosdatos").disabled = false; 
		document.getElementById("numerocuenta").disabled = false; 

	}// fin if
	else
	{
		if(document.form3.cuentabancaria.value =='N')
		{
			
		document.getElementById("tipocuenta_otrosdatos").disabled = true; 
		document.getElementById("tipocuenta_otrosdatos").selectedIndex = 0;
		
		document.getElementById("banco_otrosdatos").disabled = true; 
		document.getElementById("banco_otrosdatos").selectedIndex = 0;
		
		document.getElementById("numerocuenta").disabled = true; 
		document.getElementById("numerocuenta").value = ""; 		
			
		}// fin if
	}// fin else
	
}// fin cuentabancaria 

function fondopensionesotrosdatos()
{
   if(document.form3.pensiones.value =='S')
	{
		document.getElementById("fondopensiones").disabled = false; 
		document.getElementById("fondopensiones").focus();

	}// fin if
	else
	{
		if(document.form3.pensiones.value =='N')
		{
			
		document.getElementById("fondopensiones").disabled = true; 
		document.getElementById("fondopensiones").selectedIndex = 0;		
			
		}// fin if
	}// fin else
	
}// fin fondopensionesotrosdatos 

function fondocesantiasotrosdatos()
{
   if(document.form3.cesantias.value =='S')
	{
		document.getElementById("fondocesantias").disabled = false; 
		document.getElementById("fondocesantias").focus();

	}// fin if
	else
	{
		if(document.form3.cesantias.value =='N')
		{
			
		document.getElementById("fondocesantias").disabled = true; 
		document.getElementById("fondocesantias").selectedIndex = 0;		
			
		}// fin if
	}// fin else
	
}// fin fondocesantiasotrosdatos

function asociacionotrosdatos()
{
   if(document.form3.asociacion.value =='S')
	{
		document.getElementById("as").style.display="block";
		 	 
		// inicio artista 
				
		if(getCookie("nombreasociacionartista")!=undefined)	
		{
		var nombreasociacionartista= getCookie("nombreasociacionartista");		
		var nuevonombreartista=nombreasociacionartista;
		nuevonombreartista=nuevonombreartista.replace('+', " ");	
			do {
			nuevonombreartista=nuevonombreartista.replace('+', " ");
		
			} while(nuevonombreartista.indexOf('+') >= 0);		
				
		$("#nombreasociacionartista").val (nuevonombreartista);
		var telefonoasociacionartista= getCookie("telefonoasociacionartista");
		$("#telefonoasociacionartista").val (telefonoasociacionartista);		
		document.getElementById('artista').checked=true;
		}// fin if
		
		// fin artista
		
		// inicio deportista
		if(getCookie("nombreasociaciondeportista")!=undefined)	
		{		
		var nombreasociaciondeportista= getCookie("nombreasociaciondeportista");	
		var nuevonombredeportista=nombreasociaciondeportista;
		nuevonombredeportista=nuevonombredeportista.replace('+', " ");	
			do {
			nuevonombredeportista=nuevonombredeportista.replace('+', " ");
		
			} while(nuevonombredeportista.indexOf('+') >= 0);		
				
		$("#nombreasociaciondeportista").val (nuevonombredeportista);
		var telefonoasociaciondeportista= getCookie("telefonoasociaciondeportista");
		$("#telefonoasociaciondeportista").val (telefonoasociaciondeportista);	
		document.getElementById('deportista').checked=true;
		}// fin if
		// fin deportista
		
		// inicio escritor
		if(getCookie("nombreasociacionescritor")!=undefined)	
		{		
		var nombreasociacionescritor= getCookie("nombreasociacionescritor");	
		var nuevonombreescritor=nombreasociacionescritor;
		nuevonombreescritor=nuevonombreescritor.replace('+', " ");	
			do {
			nuevonombreescritor=nuevonombreescritor.replace('+', " ");
		
			} while(nuevonombreescritor.indexOf('+') >= 0);		
				
		$("#nombreasociacionescritor").val (nuevonombreescritor);
		var telefonoasociacionescritor= getCookie("telefonoasociacionescritor");
		$("#telefonoasociacionescritor").val (telefonoasociacionescritor);	
		document.getElementById('escritor').checked=true;
		}// fin if
		// fin escritor
		 
		 		 		 
	}// fin if
	else
	{
	 if(document.form3.asociacion.value =='N')
		{
			
		document.getElementById("as").style.display="none";
		document.getElementById('artista').checked=false;
		document.getElementById('nombreasociacionartista').value='';
		document.getElementById('telefonoasociacionartista').value='';
		 
		document.getElementById('deportista').checked=false;
		document.getElementById('nombreasociaciondeportista').value='';
		document.getElementById('telefonoasociaciondeportista').value='';
		 
		document.getElementById('escritor').checked=false;	
		document.getElementById('nombreasociacionescritor').value='';
		document.getElementById('telefonoasociacionescritor').value='';	
			
		}// fin if
	}// fin else
	
}// fin asociacionotrosdatos

function chekcasociacionotrosdatosartista()
{
   if(document.getElementById('artista').checked != true)
	{
		document.getElementById('nombreasociacionartista').value='';
		document.getElementById('telefonoasociacionartista').value='';		
		 		 		 
	}// fin if	
	else
	{
	
	document.getElementById('nombreasociacionartista').focus();
	
	if(getCookie("nombreasociacionartista")!=undefined)
	{	
	var nombreasociacionartista= getCookie("nombreasociacionartista");	
	var nuevonombreartista=nombreasociacionartista;
	nuevonombreartista=nuevonombreartista.replace('+', " ");	
		do {
		nuevonombreartista=nuevonombreartista.replace('+', " ");
	
		} while(nuevonombreartista.indexOf('+') >= 0);		
			
	$("#nombreasociacionartista").val (nuevonombreartista);
	var telefonoasociacionartista= getCookie("telefonoasociacionartista");
	$("#telefonoasociacionartista").val (telefonoasociacionartista);	
	}// fin else
	}// fin else
	
}// fin chekcasociacionotrosdatosartista

function chekcasociacionotrosdatosdeportista()
{
   	if(document.getElementById('deportista').checked != true)
	{
		document.getElementById('nombreasociaciondeportista').value='';
		document.getElementById('telefonoasociaciondeportista').value='';		
	}// fin if	
	else
	{
		
	document.getElementById('nombreasociaciondeportista').focus();
	
    if(getCookie("nombreasociaciondeportista")!=undefined)
	{	
	var nombreasociaciondeportista= getCookie("nombreasociaciondeportista");	
	var nuevonombredeportista=nombreasociaciondeportista;
	nuevonombredeportista=nuevonombredeportista.replace('+', " ");	
		do {
		nuevonombredeportista=nuevonombredeportista.replace('+', " ");
	
		} while(nuevonombredeportista.indexOf('+') >= 0);		
			
	$("#nombreasociaciondeportista").val (nuevonombredeportista);
	var telefonoasociaciondeportista= getCookie("telefonoasociaciondeportista");
	$("#telefonoasociaciondeportista").val (telefonoasociaciondeportista);	
	}// fin else
	}// fin else
		
}// fin chekcasociacionotrosdatosdeportista

function chekcasociacionotrosdatosescritor()
{
	if(document.getElementById('escritor').checked != true)
	{
	document.getElementById('nombreasociacionescritor').value='';
	document.getElementById('telefonoasociacionescritor').value='';		
	}// fin if
	else
	{
	
	document.getElementById('nombreasociacionescritor').focus();
	
	if(getCookie("nombreasociacionescritor")!=undefined)
	{	
	var nombreasociacionescritor= getCookie("nombreasociacionescritor");	
	var nuevonombreescritor=nombreasociacionescritor;
	nuevonombreescritor=nuevonombreescritor.replace('+', " ");	
		do {
		nuevonombreescritor=nuevonombreescritor.replace('+', " ");
	
		} while(nuevonombreescritor.indexOf('+') >= 0);		
			
	$("#nombreasociacionescritor").val (nuevonombreescritor);
	var telefonoasociacionescritor= getCookie("telefonoasociacionescritor");
	$("#telefonoasociacionescritor").val (telefonoasociacionescritor);
	}// fin if
	}// fin else
	
}// fin chekcasociacionotrosdatosescritor

function afiliaciontrosdatos()
{
  if(document.getElementById("afiliacion").value =='S')
	{
		document.getElementById("cont").style.display="block";		
	}
	else
    {
		document.getElementById("cont").style.display="none";				
	}
	
	
}// fin afiliaciontrosdatos 


function voltiarFecha(f){
	var dia=f.slice(-2);
	var mes=f.slice(5,7);
	var ano=f.slice(0,4);
	fecha=mes+"/"+dia+"/"+ano;
	return fecha;
	}// fin voltiarFecha
	
function voltiarFecha1(f){	
	var ano=f.slice(-4);	
	var mes=f.slice(0,2);	
	var dia=f.slice(3,5);		
	fecha=ano+"/"+mes+"/"+dia;
	return fecha;
	}// fin voltiarFecha1
	
	 
  function limpiarconyuguecompleto(){
	  
	  $("table.tablero :input.ui-state-error,select.ui-state-error").removeClass("ui-state-error");//Limpiar la clase de error
		 		
			   setCookie('idpersonac','',-1);
			   
			   document.getElementById("tiporelacion_conyugue").value = ""; 	
			   document.getElementById("conviven_conyugue").value = ""; 	
			   document.getElementById("tipoidentificacion_conyugue").value = ""; 	
			   document.getElementById("numeroidentificacion_conyugue").value = ""; 			   
			   document.getElementById("papellido_conyugue").value = ""; 	
			   document.getElementById("sapellido_conyugue").value = ""; 
			   document.getElementById("pnombre_conyugue").value = ""; 
			   document.getElementById("snombre_conyugue").value = ""; 
			   document.getElementById("genero_conyugue").value = ""; 
			   document.getElementById("direccion_conyugue").value = ""; 
			   document.getElementById("telefono_conyugue").value = ""; 
			   document.getElementById("celular_conyugue").value = ""; 
			   document.getElementById("email_conyugue").value = ""; 
			   document.getElementById("tipovivienda_conyugue").value = ""; 			   
			   document.getElementById("dptresidencia_conyugue").value = ""; 
			   document.getElementById("ciudadresidencia_conyugue").value = "";
			   document.getElementById("zona_conyugue").value = ""; 
			   document.getElementById("barrio_conyugue").value = "";
			   document.getElementById("estadocivil_conyugue").value = ""; 
			   document.getElementById("fechanace_conyugue").value = ""; 
			   document.getElementById("ciudadnace_conyugue").value = ""; 
			   document.getElementById("dptnacimiento_conyugue").value = ""; 
			   document.getElementById("escolaridad_conyugue").value = ""; 
			   
			   setCookie('alertaasocajasbeneficiario','',-1);
	           setCookie('alertainactivobeneficiario','',-1);
	           setCookie('alertafosygabeneficiario','',-1);
			   
			   document.getElementById('guardarc').style.display='block';
			   document.getElementById('actualizarc').style.display='none'; 
			  
    }// limpiarconyuguecompleto

  function limpiarconyugue(){
	  
	  		   setCookie('idpersonac','',-1);
			   
			   document.getElementById("papellido_conyugue").value = ""; 	
			   document.getElementById("sapellido_conyugue").value = ""; 
			   document.getElementById("pnombre_conyugue").value = ""; 
			   document.getElementById("snombre_conyugue").value = ""; 
			   document.getElementById("genero_conyugue").value = ""; 
			   document.getElementById("direccion_conyugue").value = ""; 
			   document.getElementById("telefono_conyugue").value = ""; 
			   document.getElementById("celular_conyugue").value = ""; 
			   document.getElementById("email_conyugue").value = ""; 
			   document.getElementById("tipovivienda_conyugue").value = ""; 			   
			   document.getElementById("dptresidencia_conyugue").value = ""; 
			   document.getElementById("ciudadresidencia_conyugue").value = "";
			   document.getElementById("zona_conyugue").value = ""; 
			   document.getElementById("barrio_conyugue").value = "";
			   document.getElementById("estadocivil_conyugue").value = ""; 
			   document.getElementById("fechanace_conyugue").value = ""; 
			   document.getElementById("ciudadnace_conyugue").value = ""; 
			   document.getElementById("dptnacimiento_conyugue").value = ""; 
			   document.getElementById("escolaridad_conyugue").value = ""; 
			  
			   setCookie('alertaasocajasbeneficiario','',-1);
	           setCookie('alertainactivobeneficiario','',-1);
	           setCookie('alertafosygabeneficiario','',-1);
    }// limpiarconyugue
	
 function limpiarbeneficiariocompleto(){
	 
	 $("table.tablero :input.ui-state-error,select.ui-state-error").removeClass("ui-state-error");//Limpiar la clase de error
	 
	           setCookie('idpersonab','',-1);
			   	
			   document.getElementById("parentesco_beneficiario").value = ""; 
			   document.getElementById("madreopadre").value = ""; 
			   document.getElementById("tipoidentificacion_beneficiario").value = ""; 
			   document.getElementById("numeroidentificacion_beneficiario").value = "";			   	 		
			   document.getElementById("papellido_beneficiario").value = ""; 	
			   document.getElementById("sapellido_beneficiario").value = ""; 
			   document.getElementById("pnombre_beneficiario").value = ""; 
			   document.getElementById("snombre_beneficiario").value = ""; 
			   document.getElementById("genero_beneficiario").value = "";			   
			   document.getElementById("fechanace_beneficiario").value = ""; 			   
			   document.getElementById('check_desplazado_beneficiario').checked=false;
			   document.getElementById('check_discapacitado_beneficiario').checked=false;
			   document.getElementById('check_reinsertado_beneficiario').checked=false;			   
			   document.getElementById("tipoestudio_beneficiario").value = ""; 	
			   
			   setCookie('alertaasocajasbeneficiario','',-1);
	           setCookie('alertainactivobeneficiario','',-1);
	           setCookie('alertafosygabeneficiario','',-1);		
			   
			   document.getElementById('guardarb').style.display='block';  	 
			   document.getElementById('actualizarb').style.display='none';  
			  
    }// limpiarbeneficiariocompleto
 function limpiarbeneficiario(){
	 
	           setCookie('idpersonab','',-1);
			   		 		
			   document.getElementById("papellido_beneficiario").value = ""; 	
			   document.getElementById("sapellido_beneficiario").value = ""; 
			   document.getElementById("pnombre_beneficiario").value = ""; 
			   document.getElementById("snombre_beneficiario").value = ""; 
			   document.getElementById("genero_beneficiario").value = "";			   
			   document.getElementById("fechanace_beneficiario").value = ""; 			   
			   document.getElementById('check_desplazado_beneficiario').checked=false;
			   document.getElementById('check_discapacitado_beneficiario').checked=false;
			   document.getElementById('check_reinsertado_beneficiario').checked=false;			   
			   document.getElementById("tipoestudio_beneficiario").value = ""; 	
			   
			   setCookie('alertaasocajasbeneficiario','',-1);
	           setCookie('alertainactivobeneficiario','',-1);
	           setCookie('alertafosygabeneficiario','',-1);		  
			   			  
    }// limpiarbeneficiario
	
//------------------------------------- Buscar fosyga,asocajas y activo------------------------------------------------------------------------------//

function buscarfosyga(identificacion){
		    $.getJSON('buscarfosyga.php',{v0:identificacion},function(datos){	
			if(datos>0)
				{
					alert("Beneficiario Encontrado en Fosyga");
					
					 if(document.getElementById("conviven_conyugue").value=='S')
					 {
					   limpiarconyuguecompleto();
					 }
					limpiarbeneficiariocompleto();
				}
		})		
	}// fin buscarfosyga

function buscarasocajas(identificacion){
		    $.getJSON('buscarasocajas.php',{v0:identificacion},function(datos){		           		
			if(datos>0)
				{
					alert("Beneficiario Encontrado en Asocajas");
					
					 if(document.getElementById("conviven_conyugue").value=='S')
					 {
					   limpiarconyuguecompleto();
					 }
					limpiarbeneficiariocompleto();
				}
		})		
	}// fin buscarasocajas
	
function buscaractivo(idpersona){
		    $.getJSON('buscaractivo.php',{v0:idpersona},function(datos){		           		
			if(datos>0)
				{
					alert("Beneficiario Activo");
					
					 if(document.getElementById("conviven_conyugue").value=='S')
					 {
					   limpiarconyuguecompleto();
					 }
					limpiarbeneficiariocompleto();
				}
		})		
	}// fin buscaractivo
	
function buscarconyuguesi(){
		    $.getJSON('buscarconyuguesi.php',function(datos){
		    var conyuguesi=	datos[0];
			setCookie("conyuguesi",conyuguesi, 1);          		
		})		
	}// fin buscarconyuguesi

//------------------------------------- Datos Postulado-----------------------------------------------------------------------------------------------//
	
function actualizarpostulado(){	
	
	$("table.tablero :input.ui-state-error,select.ui-state-error").removeClass("ui-state-error");//Limpiar la clase de error
	
	var numeroradicado=$("#numeroradicacion").val();
	if($("#numeroradicacion").val()=='')	{
		$("#numeroradicacion").addClass("ui-state-error");
		error++;
		}// fin if
	
	var consecutivo=$("#consecutivo").val();
	
	var fechagrabacion=$("#fechagrabacion").val();
	//var fechagrabacion=voltiarFecha1(fechgrabacion);
	
	var fecharadicacion=$("#fecharadicacion").val();	
	//var fecharadicacion=voltiarFecha1(fecradicacion);	
	
	var tipodocumento=$("#tipodocumento_postulado").val();
	if($("#tipodocumento_postulado").val()=='0')	{
		$("#tipodocumento_postulado").addClass("ui-state-error");
		error++;
		}// fin if
		
	var numeroiden=$("#numeroidentificacion_postulado").val();
	if($("#numeroidentificacion_postulado").val()=='')	{
		$("#numeroidentificacion_postulado").addClass("ui-state-error");
		error++;
		}// fin if
		
	var papellido=$.trim($("#papellido_postulado").val());
	if($("#papellido_postulado").val()=='')	{
		$("#papellido_postulado").addClass("ui-state-error");
		error++;
		}// fin if
		
	var sapellido=$.trim($("#sapellido_postulado").val());
	var pnombre=$.trim($("#pnombre_postulado").val());
	if($("#pnombre_postulado").val()=='')	{
		$("#pnombre_postulado").addClass("ui-state-error");
		error++;
		}// fin if
		
	var snombre=$.trim($("#snombre_postulado").val());
	var genero=$("#genero_postulado").val();
	if($("#genero_postulado").val()=='0')	{
		$("#genero_postulado").addClass("ui-state-error");
		error++;
		}// fin if
	
	var direccion=$("#direccion_postulado").val();
	var telefono=$("#telefono_postulado").val();
	var ciudad=$("#ciudad_postulado").val();
	if($("#ciudad_postulado").val()=='0')	{
		$("#ciudad_postulado").addClass("ui-state-error");
		error++;
		}// fin if
		
	var dpt=$("#departamento_postulado").val();
	if($("#departamento_postulado").val()=='0')	{
		$("#departamento_postulado").addClass("ui-state-error");
		error++;
		}// fin if
		
	var estadocivil=$("#estadocivil_postulado").val();
	var fecha=$("#fechanacimineto_postulado").val();
	//var fecha=voltiarFecha1(fechamodificada);
	var escolaridad=$("#escolaridad_postulado").val();
	var idp=$("#txtIdPersona").val();
	
	
	
	var desplazado = $('[name="desplazado_postulado"]:checked').val();
	if(desplazado == "on"){
		 desplazado="S";
		}// fin if
		else
		{
		 desplazado="N";
		}// fin else
				
	//alert("desplazado="+desplazado);
	
	var discapacitado = $('[name="discapacitado_postulado"]:checked').val();
	if(discapacitado == "on"){
		 discapacitado="I";
		}// fin if
		else
		{
		 discapacitado="N";
		}// fin else
		
	var reinsertado = $('[name="reinsertado_postulado"]:checked').val();
	if(reinsertado == "on"){
		 reinsertado="S";
		}// fin if
		else
		{
		 reinsertado="N";
		}// fin else
		
	var documentos = $('[name="documentacion"]:checked').val();	
	
	if(documentos==undefined)
	{
		document.getElementById('ms').style.display='block';
		
	}
	else
	{
		document.getElementById('ms').style.display='none';
	}
	
	if(documentos == "on"){
		 documentos=1;
		}// fin if
		else
		{
		 documentos=0;
		}// fin else
	
	var afiliacion = $("#afiliacion").val();
	var verifica = getCookie("verificaB");
	if(afiliacion=='N' && verifica!='N')
		{
		cancelar();
		}
	if($("#afiliacion").val()=='0')	{
		$("#afiliacion").addClass("ui-state-error");
		error++;
		}// fin if	
	var horagrabacion=$.trim($("#horagrabacion").val());
	
	var dias=$("#dias").val();
	var conosinvinculacion=$("#conosinvinculacion").val();
	if(conosinvinculacion=="Sin Vinculacion")
	 {
		 conosinvinculacion=1;
	 }// fin if conosinvinculacion=="Sin Vinculacion"
	 else
	if(conosinvinculacion=="Con Vinculacion")
	 {
	    conosinvinculacion=2;
	}// fin if conosinvinculacion=="Con Vinculacion"
	//if(nom.length==0) return false;
	/*
	if (error>0){
		alert("Falta Completar");
		return false
	}
	*/
 if($('[name="documentacion"]:checked').val()!=undefined && tipodocumento!=0 && numeroiden!="" && papellido!="" && pnombre!="" && genero!=0
      && ciudad!=0 && dpt!=0 && afiliacion!=0)
	{
		
	$.getJSON('actualizarpostulado.php',{v0:tipodocumento,v1:numeroiden,v2:papellido,v3:sapellido
		,v4:pnombre,v5:snombre,v6:genero,v7:direccion,v8:telefono,v9:ciudad,v10:dpt,v11:estadocivil,v12:fecha,v13:escolaridad,v14:idp,v15:discapacitado
		,v16:desplazado,v17:reinsertado,vf0:consecutivo,vf1:fechagrabacion,vf2:fecharadicacion,vf3:numeroradicado,vf4:documentos,vf5:afiliacion,vf6:horagrabacion
		,vf7:conosinvinculacion,vf8:dias},function(datos){
		if(datos==0)
		{
			alert("No se pudo actualizar los datos");
			}
		
		alert("Registro actualizado");		
		//setTimeout('document.location.reload()',500);
		busquedaobservacionesgenerales();
		
		var alertaasocajaspostulado= getCookie("alertaasocajaspostulado");
		var alertainactivopostulado= getCookie("alertainactivopostulado");
		var alertafosygapostulado= getCookie("alertafosygapostulado");
		
		
		if(alertaasocajaspostulado=='asocajaspostulado')
			{
				alert("Postulado Encontrado en Asocajas");
			}		
		if(alertafosygapostulado=='fosygapostulado')
			{
					alert("Postulado Encontrado en Fosyga");
			}
		if(alertainactivopostulado=='inactivo')
			{
					alert("Postulado Activo");
			}
		
		if(alertaasocajaspostulado=='asocajaspostulado' || alertafosygapostulado=='fosygapostulado')
		{
		  borrarcookie();
		}
	
		
		})
	}// fin if
	else
	{
		alert("Falta Completar Datos");
	}// fin else
	
	
  }	// fin actualizarpostulado
	
function buscarradicado(){
		
		var numradi = $("#numeroradicacion").val();
		$.getJSON('buscarradicado.php',{v0:numradi},function(datos){		
			$.each(datos,function(i,fila){
				
				setCookie('ciudad','',-1);	
				
			    setCookie('alertaasocajaspostulado','',-1);
	            setCookie('alertainactivopostulado','',-1);
	            setCookie('alertafosygapostulado','',-1);
				
			    setCookie('alertaasocajasbeneficiario','',-1);
	            setCookie('alertainactivobeneficiario','',-1);
	            setCookie('alertafosygabeneficiario','',-1);
				
				$("#numeroradicacion").val (fila.idradicacion);
				setCookie("numeroradicacion", fila.idradicacion, 1);
				$("#txtnumeroradicacion").val (fila.idradicacion);	
				//alert("Numeroradicacion:"+fila.idradicacion);	
				
				$("#fecharadicacion").val (fila.fecharadicacion);	
				setCookie("fecharadicacion", fila.fecharadicacion, 1);			
				
				$("#tipodocumento_postulado").val(fila.idtipodocumento);				
				
				$("#numeroidentificacion_postulado").val(fila.identificacion);
				setCookie("identificacion", fila.identificacion, 1);
												
				$("#pnombre_postulado").val(fila.pnombre);
				setCookie("pnombre", fila.pnombre, 1);
				
				$("#snombre_postulado").val(fila.snombre);
				setCookie("snombre", fila.snombre, 1);
								
				$("#papellido_postulado").val(fila.papellido);
				setCookie("papellido", fila.papellido, 1);
				
				$("#sapellido_postulado").val(fila.sapellido);
				setCookie("sapellido", fila.sapellido, 1);
				
				$("#fechanacimineto_postulado").val (fila.fechanacimiento);								
				$("#genero_postulado").val(fila.sexo);	
				
				$("#departamento_postulado").val(fila.iddepresidencia);	
				setCookie("iddepresidencia", fila.iddepresidencia, 1);
				
				ciudad=fila.idciuresidencia;
				dependencia_municipio(fila.iddepresidencia,ciudad);	
				
				setCookie("ciudad", fila.idciuresidencia, 1);
												
				$("#ciudad_postulado").val(fila.idciuresidencia);							
							
							
			    $("#direccion_postulado").val(fila.direccion);				
				$("#telefono_postulado").val(fila.telefono);				
				$("#escolaridad_postulado").val(fila.idescolaridad);				
				$("#estadocivil_postulado").val(fila.idestadocivil);	
				
				$("#txtIdPersona").val(fila.idpersona);
				setCookie("idpersona", fila.idpersona, 1);
				//var persona=$("#txtIdPersona").val(fila.idpersona);
				//alert("Id persona:"+fila.idpersona);										
					
				if(fila.capacidadtrabajo == "I"){
					
					document.getElementById('discapacitado_postulado').checked=true;
					//alert("trae 1="+fila.documentos);
					}// fin if
					else
					{
					document.getElementById('discapacitado_postulado').checked=false;
					//alert("trae 0="+fila.documentos);
					}// fin else	
					
					
				if(fila.desplazado == "S"){
					
					document.getElementById('desplazado_postulado').checked=true;
					//alert("trae 1="+fila.documentos);
					}// fin if
					else
					{
					document.getElementById('desplazado_postulado').checked=false;
					//alert("trae 0="+fila.documentos);
					}// fin else	
					
				if(fila.reinsertado == "S"){
					
					document.getElementById('reinsertado_postulado').checked=true;
					//alert("trae 1="+fila.documentos);
					}// fin if
					else
					{
					document.getElementById('reinsertado_postulado').checked=false;
					//alert("trae 0="+fila.documentos);
					}// fin else				
						
				return;
			});
		})		
	}// fin buscarradicado
function busquedaradicadoporidentificacion(tipodocumentopostulado,numeroidentificacionpostulado){
	   
		$.getJSON('busquedaradicadoporidentificacion.php',{v0:tipodocumentopostulado, v1:numeroidentificacionpostulado},function(datos){
			$.each(datos,function(i,fila){
				
				$("#numeroradicacion").val (fila.idradicacion);
				setCookie("numeroradicacion", fila.idradicacion, 1);
				$("#txtnumeroradicacion").val (fila.idradicacion);	
				//alert("Numeroradicacion:"+fila.idradicacion);	
				
				$("#fecharadicacion").val (fila.fecharadicacion);	
				setCookie("fecharadicacion", fila.fecharadicacion, 1);			
				
				$("#tipodocumento_postulado").val(fila.idtipodocumento);				
				
				$("#numeroidentificacion_postulado").val(fila.identificacion);
				setCookie("identificacion", fila.identificacion, 1);
												
				$("#pnombre_postulado").val(fila.pnombre);
				setCookie("pnombre", fila.pnombre, 1);
				
				$("#snombre_postulado").val(fila.snombre);
				setCookie("snombre", fila.snombre, 1);
								
				$("#papellido_postulado").val(fila.papellido);
				setCookie("papellido", fila.papellido, 1);
				
				$("#sapellido_postulado").val(fila.sapellido);
				setCookie("sapellido", fila.sapellido, 1);
				
				$("#fechanacimineto_postulado").val (fila.fechanacimiento);								
				$("#genero_postulado").val(fila.sexo);	
				
				$("#departamento_postulado").val(fila.iddepresidencia);	
				setCookie("iddepresidencia", fila.iddepresidencia, 1);
				
				ciudad=fila.idciuresidencia;
				dependencia_municipio(fila.iddepresidencia,ciudad);	
				
				setCookie("ciudad", fila.idciuresidencia, 1);
												
				$("#ciudad_postulado").val(fila.idciuresidencia);							
							
							
			    $("#direccion_postulado").val(fila.direccion);				
				$("#telefono_postulado").val(fila.telefono);				
				$("#escolaridad_postulado").val(fila.idescolaridad);				
				$("#estadocivil_postulado").val(fila.idestadocivil);	
				
				$("#txtIdPersona").val(fila.idpersona);
				setCookie("idpersona", fila.idpersona, 1);
				//var persona=$("#txtIdPersona").val(fila.idpersona);
				//alert("Id persona:"+fila.idpersona);										
					
				if(fila.capacidadtrabajo == "I"){
					
					document.getElementById('discapacitado_postulado').checked=true;
					//alert("trae 1="+fila.documentos);
					}// fin if
					else
					{
					document.getElementById('discapacitado_postulado').checked=false;
					//alert("trae 0="+fila.documentos);
					}// fin else	
					
					
				if(fila.desplazado == "S"){
					
					document.getElementById('desplazado_postulado').checked=true;
					//alert("trae 1="+fila.documentos);
					}// fin if
					else
					{
					document.getElementById('desplazado_postulado').checked=false;
					//alert("trae 0="+fila.documentos);
					}// fin else	
					
				if(fila.reinsertado == "S"){
					
					document.getElementById('reinsertado_postulado').checked=true;
					//alert("trae 1="+fila.documentos);
					}// fin if
					else
					{
					document.getElementById('reinsertado_postulado').checked=false;
					//alert("trae 0="+fila.documentos);
					}// fin else				
						
				return;
			});
			if(datos==0)
			{
				 limpiarCampos();
				 borrarcookie1();
				 document.getElementById("numeroradicacion").selectedIndex = '';	
				 document.form1.fechagrabacion.value = document.form1.fechaactual.value;					 			 
			}
		})		
	}// fin busquedaradicadoporidentificacion

function buscardatosfonede(){
	
		var fechagrabacion;
		var numradi = $("#numeroradicacion").val();		
	    $.getJSON('buscardatosfonede.php',{v0:numradi},function(datos){
		
		if(datos==0)
		{
			buscartiempoyvinculacion();
			document.getElementById('documentacion').checked=false;
			document.getElementById('consecutivofonede').value="";			
		}
			$.each(datos,function(i,fila){	
				
			$("#fecharadicacion").val(fila.fecharadicacion);
			setCookie("fecharadicacion",fila.fecharadicacion, 1);
						
			fechagrabacion=$("#fechagrabacion").val(fila.fechagrabacion);
			//alert(fechagrabacion);
	        //var fechagrabacion=voltiarFecha1(fechgrabacion);
							
			if(fila.documentos == 1){			
			document.getElementById('documentacion').checked=true;
			//alert("trae 1="+fila.documentos);
				}// fin if
				else
				{
				document.getElementById('documentacion').checked=false;
				//alert("trae 0="+fila.documentos);
				}// fin else	
			$("#afiliacion").val(fila.afiliacionesotrascajas);			
			$("#consecutivofonede").val(fila.consecutivo);
			
			$("#dias").val(fila.tiempo);
			
			var vinculacion=fila.vinculacion;
				
				if(vinculacion==1)
					{
						vinculacion="Sin Vinculacion";
					}// fin if vinculacion==1
				else
				if(vinculacion==2)
				    {
					  vinculacion="Con Vinculacion";	
					}//fin vinculacion==2
			
			$("#conosinvinculacion").val(vinculacion);
    			return;
			});
		})	
		if(fechagrabacion!=0)
		{	
	      document.form1.fechagrabacion.value = document.form1.fechaactual.value;		
		}
			
	}// fin buscardatosfonede 
	



function busquedacompleta(){
		
		$("table.tablero :input.ui-state-error,select.ui-state-error").removeClass("ui-state-error");//Limpiar la clase de error
		
	  	var tipodocumentopostulado = $("#tipodocumento_postulado").val();
	    if($("#tipodocumento_postulado").val()=='0')	{
			$("#tipodocumento_postulado").addClass("ui-state-error");
			error++;
			}// fin if
		var numeroidentificacionpostulado = $("#numeroidentificacion_postulado").val();		
				
		$.getJSON('busquedacompleta.php',{v0:tipodocumentopostulado, v1:numeroidentificacionpostulado},function(datos){
			
			$.each(datos,function(i,fila){
				
				borrarcookie1();
				
				$("#numeroradicacion").val (fila.idradicacion);								
				setCookie("numeroradicacion", fila.idradicacion, 1);
				$("#txtnumeroradicacion").val (fila.idradicacion);
				//alert("Numeroradicacion:"+fila.idradicacion);	
				
				$("#fecharadicacion").val (fila.fecharadicacion);
				setCookie("fecharadicacion",fila.fecharadicacion, 1);
				
				$("#tipodocumento_postulado").val(fila.idtipodocumento);				
				
				//$("#numeroidentificacion_postulado").val(fila.identificacion);
				setCookie("identificacion", fila.identificacion, 1);
												
				$("#pnombre_postulado").val(fila.pnombre);
				setCookie("pnombre", fila.pnombre, 1);
				
				$("#snombre_postulado").val(fila.snombre);
				setCookie("snombre", fila.snombre, 1);
								
				$("#papellido_postulado").val(fila.papellido);
				setCookie("papellido", fila.papellido, 1);
				
				$("#sapellido_postulado").val(fila.sapellido);
				setCookie("sapellido", fila.sapellido, 1);
				
				$("#fechanacimineto_postulado").val (fila.fechanacimiento);								
				$("#genero_postulado").val(fila.sexo);	
				
				$("#departamento_postulado").val(fila.iddepresidencia);	
				setCookie("iddepresidencia", fila.iddepresidencia, 1);
				
				ciudad=fila.idciuresidencia;
				dependencia_municipio(fila.iddepresidencia,ciudad);	
				
				setCookie("ciudad", fila.idciuresidencia, 1);
												
				$("#ciudad_postulado").val(fila.idciuresidencia);							
							
							
			    $("#direccion_postulado").val(fila.direccion);				
				$("#telefono_postulado").val(fila.telefono);				
				$("#escolaridad_postulado").val(fila.idescolaridad);				
				$("#estadocivil_postulado").val(fila.idestadocivil);	
				
				$("#txtIdPersona").val(fila.idpersona);
				setCookie("idpersona", fila.idpersona, 1);
				//var persona=$("#txtIdPersona").val(fila.idpersona);
				//alert("Id persona:"+fila.idpersona);										
					
				if(fila.capacidadtrabajo == "I"){
					
					document.getElementById('discapacitado_postulado').checked=true;
					//alert("trae 1="+fila.documentos);
					}// fin if
					else
					{
					document.getElementById('discapacitado_postulado').checked=false;
					//alert("trae 0="+fila.documentos);
					}// fin else	
					
					
				if(fila.desplazado == "S"){
					
					document.getElementById('desplazado_postulado').checked=true;
					//alert("trae 1="+fila.documentos);
					}// fin if
					else
					{
					document.getElementById('desplazado_postulado').checked=false;
					//alert("trae 0="+fila.documentos);
					}// fin else	
					
				if(fila.reinsertado == "S"){
					
				document.getElementById('reinsertado_postulado').checked=true;
				//alert("trae 1="+fila.documentos);
				}// fin if
				else
				{
				document.getElementById('reinsertado_postulado').checked=false;
				//alert("trae 0="+fila.documentos);
				}// fin else						       	
				
				var fechagrabacion=$("#fechagrabacion").val(fila.fechagrabacion);
				//var fechagrabacion=voltiarFecha1(fechgrabacion);
							
				if(fila.documentos == 1){			
				document.getElementById('documentacion').checked=true;
				//alert("trae 1="+fila.documentos);
					}// fin if
					else
					{
					document.getElementById('documentacion').checked=false;
					//alert("trae 0="+fila.documentos);
					}// fin else	
				$("#afiliacion").val(fila.afiliacionesotrascajas);	
				$("#consecutivofonede").val(fila.consecutivo);
				
				$("#dias").val(fila.tiempo);
				//setCookie("dias", fila.tiempo, 1);
				
				var vinculacion=fila.vinculacion;				
				
				if(vinculacion==1)
					{
						vinculacion="Sin Vinculacion";
					}// fin if vinculacion==1
				else
				if(vinculacion==2)
				    {
					  vinculacion="Con Vinculacion";	
					}//fin vinculacion==2
					
				$("#conosinvinculacion").val(vinculacion);	
							
				//setCookie("vinculacion", fila.vinculacion, 1);
								
				//document.getElementById("numeroidentificacion_postulado").focus();
				
				return;
			});
			
			if(datos==0)
			{
				//limpiarCampos();
				//borrarcookie1(); 
				busquedaradicadoporidentificacion(tipodocumentopostulado,numeroidentificacionpostulado);
				document.getElementById("numeroradicacion").focus();
				 
			}
		})		
	}// fin busquedacompleta
	
function buscartiempoyvinculacion(){
	
	   if($("#numeroradicacion").val()!='')
		{
			var numradi = $("#numeroradicacion").val();	
		}
		else
		{		
			var numradi = $("#txtnumeroradicacion").val();	
		}
	    //alert("numradi="+numradi);	 
		$.getJSON('buscartiempoyvinculacion.php',{v:numradi},function(datos){		
		
			arreglo = datos.split('*');
	        var dias=arreglo[0];
			var vinculacion=arreglo[1];
			
			setCookie("vinculacion", vinculacion, 1);
			setCookie("vinculacionconserva", vinculacion, 1);
			
			if(vinculacion==1)
				{
					vinculacion="Sin Vinculacion";
				}// fin if vinculacion==1
			else
			if(vinculacion==2)
				{
				   vinculacion="Con Vinculacion";	
				}//fin vinculacion==2
					
			$.each(datos,function(i,fila){
					
				setCookie("dias", dias, 1);
				setCookie("diasconserva", dias, 1);			
			    $("#dias").val(dias);	
				$("#conosinvinculacion").val(vinculacion); 
				
				return;
			});
		})	
	  
	}// fin buscartiempoyvinculacion
	  
function buscartiempoyvinculacionCookie(){
	
	   if($("#numeroradicacion").val()!='')
		{
			var numradi = $("#numeroradicacion").val();	
		}
		else
		{		
			var numradi = $("#txtnumeroradicacion").val();	
		}
	   
	    //alert("numradi="+numradi);	 
		$.getJSON('buscartiempoyvinculacion.php',{v:numradi},function(datos){		
		
			arreglo = datos.split('*');
	        var dias=arreglo[0];
			var vinculacion=arreglo[1];
			
			setCookie("vinculacion", vinculacion, 1);			
			setCookie("vinculacionconserva", vinculacion, 1);
			
			if(vinculacion==1)
				{
					vinculacion="Sin Vinculacion";
				}// fin if vinculacion==1
			else
			if(vinculacion==2)
				{
				   vinculacion="Con Vinculacion";	
				}//fin vinculacion==2
					
			$.each(datos,function(i,fila){
					
				setCookie("dias", dias, 1);
				setCookie("diasconserva", dias, 1);				
				return;
			});
		})	
	  
	}// fin buscartiempoyvinculacionCookie  
	



//---------------------------------------------------------------------------------------------------------------------------------------------------//
//-------------------------------------------------- Convenio y OTROS REQUISITOS--------------------------------------------------------------------//

function buscarconvenio(){
		
		var numconvenio = $("#Tipo_convenio").val();
		$.getJSON('buscarconvenio.php',{v0:numconvenio},function(datos){			
			$.each(datos,function(i,fila){
				$("#ValorAlimento").val(fila.valoralimentacion);
				$("#ValorSalud").val(fila.valorsalud);
				$("#ValorEducacion").val(fila.valoreducacion);
				$("#Vigencia_convenio").val(fila.vigencia);
				$("#codigo_convenio").val(fila.codigo);			
				$("#txtIdconvenio").val(fila.idconvenio);
				
				if($("#ValorAlimento").val()!=0)
				{
				 document.getElementById("Instituciones_convenio").disabled = true;
				 document.getElementById("Eps_convenio").disabled = true;
				 document.getElementById("Instituciones_convenio").selectedIndex = 0; 
				 document.getElementById("Eps_convenio").selectedIndex = 0;					
				}
			  	if($("#ValorSalud").val()!=0)
				{
				 document.getElementById("Instituciones_convenio").disabled = true;
				 document.getElementById("Eps_convenio").disabled = false; 
				 document.getElementById("Instituciones_convenio").selectedIndex = 0; 					 
				}
				
				if($("#ValorEducacion").val()!=0)
				{
				 document.getElementById("Instituciones_convenio").disabled = false; 
				 document.getElementById("Eps_convenio").disabled = true;	
				 document.getElementById("Eps_convenio").selectedIndex = 0;	
				}	
				
				if($("#ValorAlimento").val()!=0 && $("#ValorSalud").val()!=0)
				{
				 document.getElementById("Instituciones_convenio").disabled = true;
				 document.getElementById("Eps_convenio").disabled = false; 	
				 document.getElementById("Instituciones_convenio").selectedIndex = 0; 				
				}
				
				if($("#ValorAlimento").val()!=0 && $("#ValorEducacion").val()!=0)
				{
				 document.getElementById("Instituciones_convenio").disabled = false;
				 document.getElementById("Eps_convenio").disabled = true; 	
				 document.getElementById("Eps_convenio").selectedIndex = 0;					
				}
			
				if($("#ValorAlimento").val()!=0 && $("#ValorSalud").val()!=0 && $("#ValorEducacion").val()!=0)
				{
				 document.getElementById("Instituciones_convenio").disabled = false; 
				 document.getElementById("Eps_convenio").disabled = false; 	
				}										
				if($("#ValorSalud").val()!=0 && $("#ValorEducacion").val()!=0)
				{
				 document.getElementById("Instituciones_convenio").disabled = false;
				 document.getElementById("Eps_convenio").disabled = false; 					 
				}
				
			   
				
				return;
			});
		})
		
	}// fin buscarconvenio  

function busquedaobservacionesgenerales(){			
		    $.getJSON('busquedaobservacionesgenerales.php',function(datos){		           		
		    if(datos!=1)	
				{	
				   $( "#dialog" ).dialog( "open" );		
				}
		})	
	}// fin busquedaobservacionesgenerales

function guardarobservacionesgenerales(){
	    $("table.tablero :input.ui-state-error,select.ui-state-error").removeClass("ui-state-error");//Limpiar la clase de error
		
	    var observacionconvenio=$("#observacionconvenio").val();
	    if($("#observacionconvenio").val()=='')	{
		$("#observacionconvenio").addClass("ui-state-error");
		error++;
		}// fin if	
			if(observacionconvenio!="")
			{
		    	$.getJSON('guardarobservacionesgenerales.php',{v0:observacionconvenio},function(datos){})
				$( "#dialog" ).dialog( "close" );					
			}// fin if
	}// fin guardarobservacionesgenerales
	
	
function actualizaotrosdatos(){	
	
	$("table.tablero :input.ui-state-error,select.ui-state-error").removeClass("ui-state-error");//Limpiar la clase de error
	
	var vinculacion=getCookie("vinculacion1");
	if(vinculacion==2)
	{
		var contar=0;
		var valorcurso = new Array() ;
		var contadorcurso= $("#consecurso").val(); 
				
		var controlcurso=0;	
				
		for (x=1; x <= contadorcurso; x = x+1)
		  {
			  
		var idcurso = $("#idcurso"+x).val();	
		var curso = $('[name="curso'+x+'"]:checked').val();		
				   
		//alert("identificacion="+identificacion);
		//alert("seleccionar="+seleccionar);
		
		if(curso!=undefined && curso!="")
		  {
			 controlcurso=controlcurso+1;
			 valorcurso[controlcurso-1]=idcurso;
			  var contar=x;			 
			 //alert(valorcurso[x-1]);
		   }//fin if identificacion		  
		
        }// fin for   	
	}
	else
	if(vinculacion==1)
	{
		contar=3;
	}
	
	if(contar<3 || contar==0)
		{
			document.getElementById('mscursos').style.display='block';
		}	
		else
		{
			document.getElementById('mscursos').style.display='none';
		}
	
	
	
	var idconvenio=$("#Tipo_convenio").val();
   	
	var ideps=$("#Eps_convenio").val();
	var tipovivienda=$("#tipo_vivienda_otrosdatos").val();
	if($("#tipo_vivienda_otrosdatos").val()=='')	{
		$("#tipo_vivienda_otrosdatos").addClass("ui-state-error");
		error++;
		}// fin if	
	var idepspostulado=$("#Eps_postulado").val();	
	if($("#Eps_postulado").val()=='')	{
		$("#Eps_postulado").addClass("ui-state-error");
		error++;
		}// fin if	
	var identidadeducativa=$("#Instituciones_convenio").val();	
	var idarp=$("#arp_otrosdatos").val();
	if($("#arp_otrosdatos").val()=='')	{
		$("#arp_otrosdatos").addClass("ui-state-error");
		error++;
		}// fin if	
	
	if(document.getElementById("Eps_convenio").disabled == false && document.getElementById("Eps_convenio").value=='')
    {
		idconvenio='';				
		if($("#Eps_convenio").val()=='')	{
			$("#Eps_convenio").addClass("ui-state-error");
			error++;
			}// fin i
		
	}
	if(document.getElementById("Instituciones_convenio").disabled == false && document.getElementById("Instituciones_convenio").value=='')
    {
		idconvenio='';		
		if($("#Instituciones_convenio").val()=='')	{
			$("#Instituciones_convenio").addClass("ui-state-error");
			error++;
			}// fin i
	}
	
	if($("#Tipo_convenio").val()=='')	{
		$("#Tipo_convenio").addClass("ui-state-error");
		error++;
		}// fin if	
		
	var pensiones=$("#pensiones").val();
	
	var idfonpensiones=$("#fondopensiones").val();
	
	if(document.getElementById("fondopensiones").disabled == false && document.getElementById("fondopensiones").value=='')
    {
		pensiones='';	
		if($("#fondopensiones").val()=='')	{
			$("#fondopensiones").addClass("ui-state-error");
			error++;
			}// fin i
		
	}
	
	if($("#pensiones").val()=='')	{
		$("#pensiones").addClass("ui-state-error");
		error++;
		}// fin if	
	
	
	var cesantias=$("#cesantias").val();
		
	var idfoncesantias=$("#fondocesantias").val();
	
    if(document.getElementById("fondocesantias").disabled == false && document.getElementById("fondocesantias").value=='')
    {
		cesantias='';		
		if($("#fondocesantias").val()=='')	{
			$("#fondocesantias").addClass("ui-state-error");
			error++;
			}// fin if	
		
	}
		
	if($("#cesantias").val()=='')	{
		$("#cesantias").addClass("ui-state-error");
		error++;
		}// fin if	
	
	
	var idcuentabancaria=$("#cuentabancaria").val();

	var idtipocuentabancaria=$("#tipocuenta_otrosdatos").val();
	var idbanco=$("#banco_otrosdatos").val();
	var numerocuenta=$("#numerocuenta").val();
	
	if(document.getElementById("tipocuenta_otrosdatos").disabled == false && document.getElementById("tipocuenta_otrosdatos").value=='')
    {
		idcuentabancaria='';		
		if($("#tipocuenta_otrosdatos").val()=='')	{
			$("#tipocuenta_otrosdatos").addClass("ui-state-error");
			error++;
			}// fin i
		
	}
	if(document.getElementById("banco_otrosdatos").disabled == false && document.getElementById("banco_otrosdatos").value=='')
    {
		idcuentabancaria='';
		if($("#banco_otrosdatos").val()=='')	{
			$("#banco_otrosdatos").addClass("ui-state-error");
			error++;
			}// fin i
		
	}
	if(document.getElementById("numerocuenta").disabled == false && document.getElementById("numerocuenta").value=='')
    {
		idcuentabancaria='';		
		if($("#numerocuenta").val()=='')	{
			$("#numerocuenta").addClass("ui-state-error");
			error++;
			}// fin i
		
	}
	
	if($("#cuentabancaria").val()=='')	{
		$("#cuentabancaria").addClass("ui-state-error");
		error++;
		}// fin if	
	
	
	var vehiculo = $("#vehiculo").val();	
	if($("#vehiculo").val()=='')	{
		$("#vehiculo").addClass("ui-state-error");
		error++;
		}// fin if	
	var marca=$("#marca_otrosdatos").val();
	var modelo=$("#modelo_otrosdatos").val();
	var placa=$("#placa_otrosdatos").val();	
	
	if(document.getElementById("marca_otrosdatos").disabled == false && document.getElementById("marca_otrosdatos").value=='')
    {
		vehiculo='';		
		if($("#marca_otrosdatos").val()=='')	{
			$("#marca_otrosdatos").addClass("ui-state-error");
			error++;
			}// fin i
		
	}	
	if(document.getElementById("modelo_otrosdatos").disabled == false && document.getElementById("modelo_otrosdatos").value=='')
    {
		vehiculo='';		
		if($("#modelo_otrosdatos").val()=='')	{
			$("#modelo_otrosdatos").addClass("ui-state-error");
			error++;
			}// fin i
		
	}
	if(document.getElementById("placa_otrosdatos").disabled == false && document.getElementById("placa_otrosdatos").value=='')
    {
		vehiculo='';		
		if($("#placa_otrosdatos").val()=='')	{
			$("#placa_otrosdatos").addClass("ui-state-error");
			error++;
			}// fin i
		
	}	
	
	
	var especial=$("#asociacion").val();

	
	var artista = $('[name="artista"]:checked').val();	
	if(artista == "on"){
		 artista='S';
		}// fin if
		else
		{
		 artista='N';
		}// fin else
	var nombreasociacionartista=$("#nombreasociacionartista").val();
	var telefonoasociacionartista=$("#telefonoasociacionartista").val();
	
	var deportista = $('[name="deportista"]:checked').val();
	if(deportista == "on"){
		 deportista='S';
		}// fin if
		else
		{
		 deportista='N';
		}// fin else	
	var nombreasociaciondeportista=$("#nombreasociaciondeportista").val();
	var telefonoasociaciondeportista=$("#telefonoasociaciondeportista").val();
	
	var escritor = $('[name="escritor"]:checked').val();
	if(escritor == "on"){
		 escritor='S';
		}// fin if
		else
		{
		 escritor='N';
		}// fin else		
	var nombreasociacionescritor=$("#nombreasociacionescritor").val();
	var telefonoasociacionescritor=$("#telefonoasociacionescritor").val();
	
	if(document.getElementById('asociacion').value=='S' && document.getElementById('artista').checked==false && document.getElementById('deportista').checked==false && document.getElementById('escritor').checked==false )
    {
		especial='';	
		document.getElementById("asociacion").selectedIndex = "";	
	
		
	}	
	
	if($("#asociacion").val()=='')	{
		$("#asociacion").addClass("ui-state-error");
		error++;
		}// fin if	
	
	
	var ultimosalario=$("#salario_otrosdatos").val();	
	if($("#salario_otrosdatos").val()=='')	{
		$("#salario_otrosdatos").addClass("ui-state-error");
		error++;
		}// fin if		

if(idconvenio!='' && tipovivienda!='' && idepspostulado!='' && idarp!='' && pensiones!='' && cesantias!='' && idcuentabancaria!='' && vehiculo!='' && especial!='' && ultimosalario!=''
&& contar>=3)
{
   busquedaobservacionesgenerales();
   
   $.getJSON('actualizaotrosdatosconvinculacion.php',{v2:tipovivienda,v3:idepspostulado,v4:idarp,v5:pensiones,v6:idfonpensiones
              ,v7:cesantias,v8:idfoncesantias,v9:idcuentabancaria,v10:idtipocuentabancaria,v11:idbanco,v12:numerocuenta,v13:vehiculo
			  ,v14:marca,v15:modelo,v16:placa},function(datos){		   
		 })
	
	guardarsinvinculacion(idconvenio,ideps,especial,artista,nombreasociacionartista,telefonoasociacionartista,deportista,nombreasociaciondeportista,
						telefonoasociaciondeportista,escritor,nombreasociacionescritor,telefonoasociacionescritor,identidadeducativa,ultimosalario);
	if(vinculacion==2)
	{
	   $.getJSON('GuardarCurso.php',{v0:valorcurso[controlcurso-1],v1:contadorcurso,valorcurso:valorcurso},function(datos1){})   
      }// fin if
    }
	 
 else
 {
	 alert("Falta Completar Datos");
 }
}// fin actualizaotrosdatos	


function guardarsinvinculacion(idconvenio,ideps,especial,artista,nombreasociacionartista,telefonoasociacionartista,deportista,nombreasociaciondeportista,
						telefonoasociaciondeportista,escritor,nombreasociacionescritor,telefonoasociacionescritor,identidadeducativa,ultimosalario){
		
     $.getJSON('actualizaotrosdatossinvinculacion.php',{v0:idconvenio,v1:ideps,v17:especial,v18:artista,v19:nombreasociacionartista
	              ,v20:telefonoasociacionartista,v21:deportista,v22:nombreasociaciondeportista,v23:telefonoasociaciondeportista
				  ,v24:escritor,v25:nombreasociacionescritor,v26:telefonoasociacionescritor
				  ,v27:identidadeducativa,v28:ultimosalario,ultimosalario:ultimosalario},function(datos){
		if(datos==0)
			{
				alert("No se pudo actualizar los datos");
			}// fin if
		else{
			alert("Registro actualizado");			
			//setTimeout('document.location.reload()',500);	
			//borrarcookie1();
		}// fin else
		})
							
	}// fin guardarsinvinculacion




 function guardarrelacioncurso(){
		
		var valorcurso = new Array() ;
		var contadorcurso= $("#consecurso").val(); 
				
		var controlcurso=0;	
				
		for (x=1; x <= contadorcurso; x = x+1)
		  {
			  
		var idcurso = $("#idcurso"+x).val();	
		var curso = $('[name="curso'+x+'"]:checked').val();		
				   
		//alert("identificacion="+identificacion);
		//alert("seleccionar="+seleccionar);
		
		if(curso!=undefined && curso!="")
		  {
			 controlcurso=controlcurso+1;
			 valorcurso[controlcurso-1]=idcurso;
			 //alert(valorcurso[x-1]);
		   }//fin if identificacion		  
		
      }// fin for	  	  
    	$.getJSON('GuardarCurso.php',{v0:valorcurso[controlcurso-1],v1:contadorcurso,valorcurso:valorcurso},function(datos){			
		})	
	}// fin guardarrelacioncurso


	

//---------------------------------------------------------------------------------------------------------------------------------------------------//
//------------------------------------- Grupo Familiar-----------------------------------------------------------------------------------------------//
   
   
   
    function guardarrelaciongrupofamiliar(){
		var valor = new Array() ;
		var valornull = new Array() ;
				
		var contador= $("#conse").val(); 
				
		var control=0;	
		var controlnull=0;			
		
		for (x=1; x <= contador; x = x+1)
		  {
			  
		var persona = $("#per"+x).val();	
		
		arreglo = persona.split('*');
	    id=arreglo[0];
		//alert("id="+id);
	    idparentesco=arreglo[1];	
		//alert("idparentesco="+idparentesco);
		
		var seleccionar = $('[name="seleccionar'+x+'"]:checked').val();		
        var radio_sino = $('[name="radio_sino'+x+'"]:checked').val();
	    var nombrecursosena=$("#nombrecursosena"+x).val();		
		
				   
		//alert("identificacion="+identificacion);
		//alert("seleccionar="+seleccionar);
		//alert("seleccionar="+seleccionar);	 
		
		if(seleccionar==undefined && idparentesco==34 )
		  {
			controlnull=controlnull+1;
			valornull[controlnull-1]=persona;	
		  }
		   
	    
		if(seleccionar!=undefined && seleccionar!="" )
		  {
			 control=control+1;
			 valor[control-1]=persona;
			 	 			 
			 //alert(valor[x-1]);
		   }//fin if identificacion		 

      }// fin for	  
	 
    	$.getJSON('GuardarGrupofamiliar.php',{contador:contador,valor:valor,valornull:valornull},function(datos){
			
		})
		
		
		//alert("valor[control-1]="+valor[control-1]);
		if(valor[control-1]!=undefined && valor[control-1]!="")
			{
				alert("Relacion del Postulado Guardada correctamente");	
				$("#principal").load('PrincipalGrupoFamiliar.php');	
				document.getElementById('tiporelacion').selectedIndex = 0;
				busquedaobservacionesgenerales();
				
				
		var alertaasocajasbeneficiario= getCookie("alertaasocajasbeneficiario");
		var alertafosygabeneficiario= getCookie("alertafosygabeneficiario");
		var alertainactivobeneficiario= getCookie("alertainactivobeneficiario");
		
		if(alertaasocajasbeneficiario=='asocajasbeneficiario')
			{
				alert("Beneficiario Encontrado en Asocajas");
			}
		 		
		 if(alertafosygabeneficiario=='fosygabeneficiario')
				{
					alert("Beneficiario Encontrado en Fosyga");
				}
		
		 if(alertainactivobeneficiario=='inactivobeneficiario')
				{
					alert("Beneficiario Activo");
				}		
				
				//setTimeout('document.location.reload()',500);
			}// fin if	
			else
			{		
				alert("Seleccione los Beneficiarios o Conyugues del Postulado");	
			}// fin else	
					
		}// fin guardarrelaciongrupofamiliar
		
		
function validarbeneficiario(){

	$("table.tablero :input.ui-state-error,select.ui-state-error").removeClass("ui-state-error");//Limpiar la clase de error
	
	var parentescob=$("#parentesco_beneficiario").val();   
	var tipoidentificacionb=$("#tipoidentificacion_beneficiario").val();
	var fechanaceb=$("#fechanace_beneficiario").val();	
	var tipoestudiob=$("#tipoestudio_beneficiario").val();
    var madreopadre = $("#madreopadre").val();	
	    
	// calcular edad
	var birthday = new Date(fechanaceb);
	var today = new Date();
	var years = today.getFullYear() - birthday.getFullYear();
	
	// Reset birthday to the current year.
	birthday.setFullYear(today.getFullYear());
	
	// If the user's birthday has not occurred yet this year, subtract 1.
	if (today < birthday)
	{
		years--;
	}
	//document.write("You are " + years + " years old.");
	//alert(years);
	// Output: You are years years old.
	// fin calcular edad
	  
 	if(parentescob==35 || parentescob==38)
	{
if(document.getElementById("tipoidentificacion_beneficiario").value==1 || document.getElementById("tipoidentificacion_beneficiario").value==3 || document.getElementById("tipoidentificacion_beneficiario").value==4 || document.getElementById("tipoidentificacion_beneficiario").value==5 || document.getElementById("tipoidentificacion_beneficiario").value==8)
		{
			document.getElementById("tipoidentificacion_beneficiario").selectedIndex = "";		     
			parentescob="";
			tipoidentificacionb="";
					
			//limpiarbeneficiariocompleto();
					
		}
						
	if(years<5 && tipoestudiob!=203)
			   {
				   document.getElementById("tipoestudio_beneficiario").selectedIndex = 0;					  
				   tipoestudiob='';
			   }
    if(years>=18)
		{
			document.getElementById("fechanace_beneficiario").value = "";
			fechanaceb="";  			
			alert("Solo se pueden registrar hijos menores de 18");
		}
					
	}
	
	if(parentescob==36 || parentescob==37)
	{
		if(madreopadre!=0)
		{
			document.getElementById("madreopadre").selectedIndex = "";
			madreopadre="";
		}
	}
		
	if($("#parentesco_beneficiario").val()=='')	{
		$("#parentesco_beneficiario").addClass("ui-state-error");
		error++;
		}// fin if	
	
	if($("#tipoidentificacion_beneficiario").val()=='')	{
		$("#tipoidentificacion_beneficiario").addClass("ui-state-error");
		error++;
		}// fin if
		
	var numeroidentificacionb=$("#numeroidentificacion_beneficiario").val();
	if($("#numeroidentificacion_beneficiario").val()=='')	{
		$("#numeroidentificacion_beneficiario").addClass("ui-state-error");
		error++;
		}// fin if
		
	var pnombreb=$("#pnombre_beneficiario").val();
	if($("#pnombre_beneficiario").val()=='')	{
		$("#pnombre_beneficiario").addClass("ui-state-error");
		error++;
		}// fin if
	var snombreb=$("#snombre_beneficiario").val();
	var papellidob=$("#papellido_beneficiario").val();
	if($("#papellido_beneficiario").val()=='')	{
		$("#papellido_beneficiario").addClass("ui-state-error");
		error++;
		}// fin if
		
	var sapellidob=$("#sapellido_beneficiario").val();
	var generob=$("#genero_beneficiario").val();
	if($("#genero_beneficiario").val()=='')	{
		$("#genero_beneficiario").addClass("ui-state-error");
		error++;
		}// fin if
			
	//var fechanaceb=voltiarFecha1(fechamob);	
	if($("#fechanace_beneficiario").val()=='')	{
		$("#fechanace_beneficiario").addClass("ui-state-error");
		error++;
		}// fin if
	
	var desplazadob = $('[name="check_desplazado_beneficiario"]:checked').val();
	if(desplazadob == "on"){
		 desplazadob="S";
		}// fin if
		else
		{
		 desplazadob="N";
		}// fin else
				
	//alert("desplazado="+desplazado);
	
	var discapacitadob = $('[name="check_discapacitado_beneficiario"]:checked').val();
	if(discapacitadob == "on"){
		 discapacitadob="I";
		}// fin if
		else
		{
		 discapacitadob="N";
		}// fin else
		
	var reinsertadob = $('[name="check_reinsertado_beneficiario"]:checked').val();
	if(reinsertadob == "on"){
		 reinsertadob="S";
		}// fin if
		else
		{
		 reinsertadob="N";
		}// fun else
		
    var tipoestudio_beneficiario = $("#tipoestudio_beneficiario").val();	
	if($("#tipoestudio_beneficiario").val()=='')	{
		$("#tipoestudio_beneficiario").addClass("ui-state-error");
		error++;
		}// fin if

	if($("#madreopadre").val()=='')	{
		$("#madreopadre").addClass("ui-state-error");
		error++;
		}// fin if	
	

	if(parentescob!="" && tipoidentificacionb!="" && numeroidentificacionb!=0 && papellidob!="" && pnombreb!="" && generob!="" && fechanaceb!="" && tipoestudio_beneficiario!="" && madreopadre!="")
	 {
		guardarvalidarbeneficiario(tipoidentificacionb,numeroidentificacionb,papellidob,sapellidob,pnombreb,snombreb,
			 generob,fechanaceb,tipoestudiob,discapacitadob,desplazadob,reinsertadob,parentescob,madreopadre);
		
	 }// fin if
 }// fin validarbeneficiario	
	
function guardarvalidarbeneficiario(tipoidentificacionb,numeroidentificacionb,papellidob,sapellidob,pnombreb,snombreb,
         generob,fechanaceb,tipoestudiob,discapacitadob,desplazadob,reinsertadob,parentescob,madreopadre){
		
		$.getJSON('GuardarBeneficiario.php',{v0:tipoidentificacionb,v1:numeroidentificacionb,v2:papellidob,v3:sapellidob
		       ,v4:pnombreb,v5:snombreb,v6:generob,v7:fechanaceb,v8:tipoestudiob,v9:discapacitadob
			   ,v10:desplazadob,v11:reinsertadob,vr:parentescob,tpr:madreopadre},function(datos){
			
			if(datos==0)
			{
				alert("El Registro ya Existe");
			
		    }// fin if	
			else
			{		
				alert("Registro guardado");
				
				var alertaasocajasbeneficiario= getCookie("alertaasocajasbeneficiario");
				var alertafosygabeneficiario= getCookie("alertafosygabeneficiario");
				var alertainactivobeneficiario= getCookie("alertainactivobeneficiario");
				
				if(alertaasocajasbeneficiario=='asocajasbeneficiario')
					{
						alert("Beneficiario Encontrado en Asocajas");
					}
				 		
				 if(alertafosygabeneficiario=='fosygabeneficiario')
						{
							alert("Beneficiario Encontrado en Fosyga");
						}
				
				 if(alertainactivobeneficiario=='inactivobeneficiario')
						{
							alert("Beneficiario Activo");
						}
				//location.reload();
				document.getElementById("principal").style.display="block";	
				$("#principal").load('PrincipalGrupoFamiliar.php');
				document.getElementById('tiporelacion').selectedIndex = 0;				 
				document.getElementById("conyugue").style.display="none";
				document.getElementById("beneficiario").style.display="none";								
				//setTimeout('document.location.reload()',500);
			}// fin else
			//alert("Registro guardado numero id: " + datos);
		})
						
		}// fin guardarvalidarbeneficiario
		


function validaractualizarbeneficiario(){
		
	$("table.tablero :input.ui-state-error,select.ui-state-error").removeClass("ui-state-error");//Limpiar la clase de error
	
	var parentescob=$("#parentesco_beneficiario").val();   
	var tipoidentificacionb=$("#tipoidentificacion_beneficiario").val();
	var fechanaceb=$("#fechanace_beneficiario").val();	
	var tipoestudiob=$("#tipoestudio_beneficiario").val();
	var madreopadre = $("#madreopadre").val();	
	    
	// calcular edad
	var birthday = new Date(fechanaceb);
	var today = new Date();
	var years = today.getFullYear() - birthday.getFullYear();
	
	// Reset birthday to the current year.
	birthday.setFullYear(today.getFullYear());
	
	// If the user's birthday has not occurred yet this year, subtract 1.
	if (today < birthday)
	{
		years--;
	}
	//document.write("You are " + years + " years old.");
	//alert(years);
	// Output: You are years years old.
	// fin calcular edad
	  
   	if(parentescob==35 || parentescob==38)
	{
if(document.getElementById("tipoidentificacion_beneficiario").value==1 || document.getElementById("tipoidentificacion_beneficiario").value==3 || document.getElementById("tipoidentificacion_beneficiario").value==4 || document.getElementById("tipoidentificacion_beneficiario").value==5 || document.getElementById("tipoidentificacion_beneficiario").value==8)
		{
			document.getElementById("tipoidentificacion_beneficiario").selectedIndex = "";		     
			parentescob="";
			tipoidentificacionb="";
					
			//limpiarbeneficiariocompleto();
					
		}
						
	if(years<5 && tipoestudiob!=203)
			   {
				   document.getElementById("tipoestudio_beneficiario").selectedIndex = 0;					  
				   tipoestudiob='';
			   }
    if(years>=18)
		{
			document.getElementById("fechanace_beneficiario").value = "";
			fechanaceb="";  			
			alert("Solo se pueden registrar hijos menores de 18");
		}
					
	}
	
	if(parentescob==36 || parentescob==37)
	{
		if(madreopadre!=0)
		{
			document.getElementById("madreopadre").selectedIndex = "";
			madreopadre="";
		}
	}
		
	
	if($("#parentesco_beneficiario").val()=='')	{
		$("#parentesco_beneficiario").addClass("ui-state-error");
		error++;
		}// fin if	
	
	if($("#tipoidentificacion_beneficiario").val()=='')	{
		$("#tipoidentificacion_beneficiario").addClass("ui-state-error");
		error++;
		}// fin if
		
	var numeroidentificacionb=$("#numeroidentificacion_beneficiario").val();
	if($("#numeroidentificacion_beneficiario").val()=='')	{
		$("#numeroidentificacion_beneficiario").addClass("ui-state-error");
		error++;
		}// fin if
		
	var pnombreb=$("#pnombre_beneficiario").val();
	if($("#pnombre_beneficiario").val()=='')	{
		$("#pnombre_beneficiario").addClass("ui-state-error");
		error++;
		}// fin if
	var snombreb=$("#snombre_beneficiario").val();
	var papellidob=$("#papellido_beneficiario").val();
	if($("#papellido_beneficiario").val()=='')	{
		$("#papellido_beneficiario").addClass("ui-state-error");
		error++;
		}// fin if
		
	var sapellidob=$("#sapellido_beneficiario").val();
	var generob=$("#genero_beneficiario").val();
	if($("#genero_beneficiario").val()=='')	{
		$("#genero_beneficiario").addClass("ui-state-error");
		error++;
		}// fin if
			
	//var fechanaceb=voltiarFecha1(fechamob);	
	if($("#fechanace_beneficiario").val()=='')	{
		$("#fechanace_beneficiario").addClass("ui-state-error");
		error++;
		}// fin if
	
	var desplazadob = $('[name="check_desplazado_beneficiario"]:checked').val();
	if(desplazadob == "on"){
		 desplazadob="S";
		}// fin if
		else
		{
		 desplazadob="N";
		}// fin else
				
	//alert("desplazado="+desplazado);
	
	var discapacitadob = $('[name="check_discapacitado_beneficiario"]:checked').val();
	if(discapacitadob == "on"){
		 discapacitadob="I";
		}// fin if
		else
		{
		 discapacitadob="N";
		}// fin else
		
	var reinsertadob = $('[name="check_reinsertado_beneficiario"]:checked').val();
	if(reinsertadob == "on"){
		 reinsertadob="S";
		}// fin if
		else
		{
		 reinsertadob="N";
		}// fun else
	
    var tipoestudio_beneficiario = $("#tipoestudio_beneficiario").val();	
	if($("#tipoestudio_beneficiario").val()=='')	{
		$("#tipoestudio_beneficiario").addClass("ui-state-error");
		error++;
		}// fin if	
			
	if($("#madreopadre").val()=='')	{
		$("#madreopadre").addClass("ui-state-error");
		error++;
		}// fin if	

	
	
	if(parentescob!=0 && tipoidentificacionb!=0 && numeroidentificacionb!=0 && papellidob!="" && pnombreb!="" && generob!="" && fechanaceb!="" && tipoestudio_beneficiario!=0 && madreopadre!="")
		 {
		   
		   actualizarbeneficiario(tipoidentificacionb,numeroidentificacionb,papellidob,sapellidob,pnombreb,snombreb,
				 generob,fechanaceb,tipoestudiob,discapacitadob,desplazadob,reinsertadob,parentescob,madreopadre);
				
		 }// fin if
	 
		}// fin validaractualizarbeneficiario
		

function actualizarbeneficiario(tipoidentificacionb,numeroidentificacionb,papellidob,sapellidob,pnombreb,snombreb,
			 generob,fechanaceb,tipoestudiob,discapacitadob,desplazadob,reinsertadob,parentescob,madreopadre){

		$.getJSON('actualizarbeneficiario.php',{v0:tipoidentificacionb,v1:numeroidentificacionb,v2:papellidob,v3:sapellidob
		       ,v4:pnombreb,v5:snombreb,v6:generob,v7:fechanaceb,v8:tipoestudiob,v9:discapacitadob
			   ,v10:desplazadob,v11:reinsertadob,vr:parentescob,tpr:madreopadre},function(datos){
			
			if(datos==0)
			{
				alert("No se pudo actualizar los datos");
			
		    }// fin if	
			else
			{		
				alert("Registro actualizado");
				busquedaobservacionesgenerales();
				
				var alertaasocajasbeneficiario= getCookie("alertaasocajasbeneficiario");
				var alertafosygabeneficiario= getCookie("alertafosygabeneficiario");
				var alertainactivobeneficiario= getCookie("alertainactivobeneficiario");
				
				if(alertaasocajasbeneficiario=='asocajasbeneficiario')
					{
						alert("Beneficiario Encontrado en Asocajas");
					}
				 		
				 if(alertafosygabeneficiario=='fosygabeneficiario')
						{
							alert("Beneficiario Encontrado en Fosyga");
						}
				
				 if(alertainactivobeneficiario=='inactivobeneficiario')
						{
							alert("Beneficiario Activo");
						}
						
				//location.reload();
				document.getElementById("principal").style.display="block";
				$("#principal").load('PrincipalGrupoFamiliar.php');	
				document.getElementById('tiporelacion').selectedIndex = 0;
				document.getElementById("conyugue").style.display="none";
				document.getElementById("beneficiario").style.display="none";	
				//setTimeout('document.location.reload()',500);
			}// fin else
			//alert("Registro guardado numero id: " + datos);
		})		
  }// fin actualizarbeneficiario

function buscardatosrelacionbeneficiario(idbeneficiario){
		    $.getJSON('buscardatosrelacionbeneficiario.php',{v0:idbeneficiario},function(datos){					           		
			$.each(datos,function(i,fila){	
			   $("#parentesco_beneficiario").val(fila.idparentesco);
			   $("#madreopadre").val(fila.idconyuge);
			   
				return;
			});
			if(datos==0)
			{
				document.getElementById('guardarb').style.display='block';
				document.getElementById('actualizarb').style.display='none'; 
			}
			else
			{
				document.getElementById('guardarb').style.display='none';
				document.getElementById('actualizarb').style.display='block';
			}			
		})	
	}// fin buscardatosrelacionbeneficiario
		
		
function buscardatosbeneficiario(){
		
	    $("table.tablero :input.ui-state-error,select.ui-state-error").removeClass("ui-state-error");//Limpiar la clase de error
		
	  	var tipoidentificacionb = $("#tipoidentificacion_beneficiario").val();
	    if($("#tipoidentificacion_beneficiario").val()=='')	{
			$("#tipoidentificacion_beneficiario").addClass("ui-state-error");
			error++;
			}// fin if	

		var numeroidentificacionb = $("#numeroidentificacion_beneficiario").val();		
		
		$.getJSON('buscardatosbeneficiario.php',{v0:tipoidentificacionb,v1:numeroidentificacionb},function(datos){
					
			$.each(datos,function(i,fila){	
			    
				setCookie("idpersonab", fila.idpersona, 1);
												
				$("#papellido_beneficiario").val(fila.papellido);
				$("#sapellido_beneficiario").val(fila.sapellido );
				$("#pnombre_beneficiario").val(fila.pnombre );
				$("#snombre_beneficiario").val(fila.snombre);
				$("#genero_beneficiario").val(fila.sexo);
				$("#fechanace_beneficiario").val(fila.fechanacimiento);
					
				if(fila.capacidadtrabajo == "I"){
					
					document.getElementById('check_discapacitado_beneficiario').checked=true;
					//alert("trae 1="+fila.documentos);
					}// fin if
					else
					{
					document.getElementById('check_discapacitado_beneficiario').checked=false;
					//alert("trae 0="+fila.documentos);
					}// fin else	
					
					
				if(fila.desplazado == "S"){
					
					document.getElementById('check_desplazado_beneficiario').checked=true;
					//alert("trae 1="+fila.documentos);
					}// fin if
					else
					{
					document.getElementById('check_desplazado_beneficiario').checked=false;
					//alert("trae 0="+fila.documentos);
					}// fin else	
					
				if(fila.reinsertado == "S"){
					
					document.getElementById('check_reinsertado_beneficiario').checked=true;
					//alert("trae 1="+fila.documentos);
					}// fin if
					else
					{
					document.getElementById('check_reinsertado_beneficiario').checked=false;
					//alert("trae 0="+fila.documentos);
					}// fin else
					
			    $("#tipoestudio_beneficiario").val(fila.idescolaridad);			
				
					buscarfosyga(numeroidentificacionb);
					buscarasocajas(numeroidentificacionb);
					buscaractivo(fila.idpersona);					
					buscardatosrelacionbeneficiario(fila.idpersona);
					
			
			      					
				return;
			});
			if(datos==0)
			{
				 document.getElementById('guardarb').style.display='block';
				 document.getElementById('actualizarb').style.display='none'; 
				 limpiarbeneficiario();
			}
					
		})
	
				
	}// fin buscardatosbeneficiario		
 
		

function validarconyugue(){

	$("table.tablero :input.ui-state-error,select.ui-state-error").removeClass("ui-state-error");//Limpiar la clase de error
	
	var parentescoc=$("#tiporelacion_conyugue").val(); 
	if($("#tiporelacion_conyugue").val()=='')	{
		$("#tiporelacion_conyugue").addClass("ui-state-error");
		error++;
		}// fin if	
	var tipoidentificacionc=$("#tipoidentificacion_conyugue").val();
	if($("#tipoidentificacion_conyugue").val()=='')	{
		$("#tipoidentificacion_conyugue").addClass("ui-state-error");
		error++;
		}// fin if		
		
	var conviven=$("#conviven_conyugue").val();	
	var conyuguesitrae = getCookie("conyuguesi");
		
	   if(conyuguesitrae==1 && document.getElementById("conviven_conyugue").value!='N' )
		{
			conviven='';	
			document.getElementById("conviven_conyugue").selectedIndex = "";	
		}	
	
	if($("#conviven_conyugue").val()=='')	{
		$("#conviven_conyugue").addClass("ui-state-error");
		error++;
		}// fin if	
	
	var numeroidentificacionc=$("#numeroidentificacion_conyugue").val();
	if($("#numeroidentificacion_conyugue").val()=='')	{
		$("#numeroidentificacion_conyugue").addClass("ui-state-error");
		error++;
		}// fin if	
		
	var pnombrec=$("#papellido_conyugue").val();
	if($("#papellido_conyugue").val()=='')	{
		$("#papellido_conyugue").addClass("ui-state-error");
		error++;
		}// fin if	
	var snombrec=$("#sapellido_conyugue").val();
	var papellidoc=$("#pnombre_conyugue").val();
	if($("#pnombre_conyugue").val()=='')	{
		$("#pnombre_conyugue").addClass("ui-state-error");
		error++;
		}// fin if	
	var sapellidoc=$("#snombre_conyugue").val();	
	var generoc=$("#genero_conyugue").val();
	if($("#genero_conyugue").val()=='')	{
		$("#genero_conyugue").addClass("ui-state-error");
		error++;
		}// fin if	
		
	var direccionc=$("#direccion_conyugue").val();
	var barrioc=$("#barrio_conyugue").val();
	var telefonoc=$("#telefono_conyugue").val();
	var celulcarc=$("#celular_conyugue").val();
	var emailc=$("#email_conyugue").val();
	var tipoviviendac=$("#tipovivienda_conyugue").val();
	var ciudadresidenciac=$("#ciudadresidencia_conyugue").val();
	if($("#ciudadresidencia_conyugue").val()=='')	{
		$("#ciudadresidencia_conyugue").addClass("ui-state-error");
		error++;
		}// fin if	
		
	var dptresidenciac=$("#dptresidencia_conyugue").val();
	if($("#dptresidencia_conyugue").val()=='')	{
		$("#dptresidencia_conyugue").addClass("ui-state-error");
		error++;
		}// fin if	
		
	var zonac=$("#zona_conyugue").val();
	var estadocivilc=$("#estadocivil_conyugue").val();
		
	var fechanacec=$("#fechanace_conyugue").val();	
	
	// calcular edad
	var birthday = new Date(fechanacec);
	var today = new Date();
	var years = today.getFullYear() - birthday.getFullYear();

	// Reset birthday to the current year.
	birthday.setFullYear(today.getFullYear());

	// If the user's birthday has not occurred yet this year, subtract 1.
	if (today < birthday)
	{
		years--;
	}
	//document.write("You are " + years + " years old.");
	//alert(years);
	// Output: You are years years old.
	// fin calcular edad


	if(years<16)
	{
		document.getElementById("fechanace_conyugue").value = "";
		fechanacec="";  			
		
	}

	
	
	
	//var fechanacec=voltiarFecha1(fechamoc);	
	if($("#fechanace_conyugue").val()=='')	{
		$("#fechanace_conyugue").addClass("ui-state-error");
		error++;
		}// fin if	
	
	var ciudadnacec=$("#ciudadnace_conyugue").val();
	if($("#ciudadnace_conyugue").val()=='')	{
		$("#ciudadnace_conyugue").addClass("ui-state-error");
		error++;
		}// fin if	
	var dptnacimientoc=$("#dptnacimiento_conyugue").val();	
	if($("#dptnacimiento_conyugue").val()=='')	{
		$("#dptnacimiento_conyugue").addClass("ui-state-error");
		error++;
		}// fin if	
				
	var escolaridadc=$("#escolaridad_conyugue").val();

   if(parentescoc!=0 && conviven!="" && tipoidentificacionc!=0 && numeroidentificacionc!=0 && papellidoc!="" && pnombrec!="" 
         && generoc!="" && dptresidenciac!="" && ciudadresidenciac!="" && fechanacec!="" && dptnacimientoc!="" && ciudadnacec!="")
	 {
	guardarvalidarconyugue(tipoidentificacionc,numeroidentificacionc,pnombrec,snombrec,papellidoc,sapellidoc,generoc,direccionc,barrioc,telefonoc,
           celulcarc,emailc,tipoviviendac,ciudadresidenciac,dptresidenciac,zonac,estadocivilc,fechanacec,ciudadnacec,dptnacimientoc,escolaridadc,parentescoc,conviven);
		   
	 }// fin if
	 
	}// fin validarconyugue
	
	
function guardarvalidarconyugue(tipoidentificacionc,numeroidentificacionc,pnombrec,snombrec,papellidoc,sapellidoc,generoc,direccionc,barrioc,telefonoc,
           celulcarc,emailc,tipoviviendac,ciudadresidenciac,dptresidenciac,zonac,estadocivilc,fechanacec,ciudadnacec,dptnacimientoc,escolaridadc,parentescoc,conviven){
		
		$.getJSON('GuardarConyugue.php',{v0:tipoidentificacionc,v1:numeroidentificacionc,v2:pnombrec,v3:snombrec,v4:papellidoc,v5:sapellidoc,v6:generoc
		           ,v7:direccionc,v8:barrioc,v9:telefonoc,v10:celulcarc,v11:emailc,v12:tipoviviendac,v13:ciudadresidenciac,v14:dptresidenciac,v15:zonac
				   ,v16:estadocivilc,v17:fechanacec,v18:ciudadnacec,v19:dptnacimientoc,v20:escolaridadc,vrc:parentescoc,vrc1:conviven},function(datos){
			
			if(datos==0)
			{
				alert("El Registro ya Existe");
			
		    }// fin if
			else
				{
					alert("Registro guardado");
					
					var alertaasocajasbeneficiario= getCookie("alertaasocajasbeneficiario");
		            var alertafosygabeneficiario= getCookie("alertafosygabeneficiario");
		            var alertainactivobeneficiario= getCookie("alertainactivobeneficiario");
					
					if(alertaasocajasbeneficiario=='asocajasbeneficiario')
						{
							alert("Beneficiario Encontrado en Asocajas");
						}
					 		
					 if(alertafosygabeneficiario=='fosygabeneficiario')
							{
								alert("Beneficiario Encontrado en Fosyga");
							}
					 
					 if(alertainactivobeneficiario=='inactivobeneficiario')
							{
								alert("Beneficiario Activo");
							}		

					//location.reload();
				    document.getElementById("principal").style.display="block";	
					$("#principal").load('PrincipalGrupoFamiliar.php');
					document.getElementById('tiporelacion').selectedIndex = 0;
				    document.getElementById("conyugue").style.display="none";
				    document.getElementById("beneficiario").style.display="none";	
					$("#principal").load();
											
					//setTimeout('document.location.reload()',500);
				}// fin else
			//alert("Registro guardado numero id: " + datos);
		})
							
		}// fin guardarvalidarconyugue
		
		
function validaractualizarconyugue(){
		
	$("table.tablero :input.ui-state-error,select.ui-state-error").removeClass("ui-state-error");//Limpiar la clase de error
	
	var parentescoc=$("#tiporelacion_conyugue").val(); 
	if($("#tiporelacion_conyugue").val()=='')	{
		$("#tiporelacion_conyugue").addClass("ui-state-error");
		error++;
		}// fin if	
	var tipoidentificacionc=$("#tipoidentificacion_conyugue").val();
	if($("#tipoidentificacion_conyugue").val()=='')	{
		$("#tipoidentificacion_conyugue").addClass("ui-state-error");
		error++;
		}// fin if		
		
	var conviven=$("#conviven_conyugue").val();	
    var conyuguesitrae = getCookie("conyuguesi");
	var convivenconyuguetrae = getCookie("convivenconyugue");
	
	if(conyuguesitrae==1 && document.getElementById("conviven_conyugue").value!='N' && convivenconyuguetrae=='N')
		{
			conviven='';	
			document.getElementById("conviven_conyugue").selectedIndex = "";	
		}	
	if(convivenconyuguetrae=='S')
	{
		conviven='S';
		document.getElementById("conviven_conyugue").selectedIndex = "S";
	}
	
	if($("#conviven_conyugue").val()=='')	{
		$("#conviven_conyugue").addClass("ui-state-error");
		error++;
		}// fin if	
	
	var numeroidentificacionc=$("#numeroidentificacion_conyugue").val();
	if($("#numeroidentificacion_conyugue").val()=='')	{
		$("#numeroidentificacion_conyugue").addClass("ui-state-error");
		error++;
		}// fin if	
		
	var pnombrec=$("#papellido_conyugue").val();
	if($("#papellido_conyugue").val()=='')	{
		$("#papellido_conyugue").addClass("ui-state-error");
		error++;
		}// fin if	
	var snombrec=$("#sapellido_conyugue").val();
	var papellidoc=$("#pnombre_conyugue").val();
	if($("#pnombre_conyugue").val()=='')	{
		$("#pnombre_conyugue").addClass("ui-state-error");
		error++;
		}// fin if	
	var sapellidoc=$("#snombre_conyugue").val();	
	var generoc=$("#genero_conyugue").val();
	if($("#genero_conyugue").val()=='')	{
		$("#genero_conyugue").addClass("ui-state-error");
		error++;
		}// fin if	
		
	var direccionc=$("#direccion_conyugue").val();
	var barrioc=$("#barrio_conyugue").val();
	var telefonoc=$("#telefono_conyugue").val();
	var celulcarc=$("#celular_conyugue").val();
	var emailc=$("#email_conyugue").val();
	var tipoviviendac=$("#tipovivienda_conyugue").val();
	var ciudadresidenciac=$("#ciudadresidencia_conyugue").val();
	if($("#ciudadresidencia_conyugue").val()=='')	{
		$("#ciudadresidencia_conyugue").addClass("ui-state-error");
		error++;
		}// fin if	
		
	var dptresidenciac=$("#dptresidencia_conyugue").val();
	if($("#dptresidencia_conyugue").val()=='')	{
		$("#dptresidencia_conyugue").addClass("ui-state-error");
		error++;
		}// fin if	
		
	var zonac=$("#zona_conyugue").val();
	var estadocivilc=$("#estadocivil_conyugue").val();
		
	var fechanacec=$("#fechanace_conyugue").val();	
	
	// calcular edad
	var birthday = new Date(fechanacec);
	var today = new Date();
	var years = today.getFullYear() - birthday.getFullYear();

	// Reset birthday to the current year.
	birthday.setFullYear(today.getFullYear());

	// If the user's birthday has not occurred yet this year, subtract 1.
	if (today < birthday)
	{
		years--;
	}
	//document.write("You are " + years + " years old.");
	//alert(years);
	// Output: You are years years old.
	// fin calcular edad


	if(years<16)
	{
		document.getElementById("fechanace_conyugue").value = "";
		fechanacec="";  			
		
	}	
	
	//var fechanacec=voltiarFecha1(fechamoc);	
	if($("#fechanace_conyugue").val()=='')	{
		$("#fechanace_conyugue").addClass("ui-state-error");
		error++;
		}// fin if	
	
	var ciudadnacec=$("#ciudadnace_conyugue").val();
	if($("#ciudadnace_conyugue").val()=='')	{
		$("#ciudadnace_conyugue").addClass("ui-state-error");
		error++;
		}// fin if	
	var dptnacimientoc=$("#dptnacimiento_conyugue").val();	
	if($("#dptnacimiento_conyugue").val()=='')	{
		$("#dptnacimiento_conyugue").addClass("ui-state-error");
		error++;
		}// fin if	
				
	var escolaridadc=$("#escolaridad_conyugue").val();

   if(parentescoc!=0 && conviven!="" && tipoidentificacionc!=0 && numeroidentificacionc!=0 && papellidoc!="" && pnombrec!="" 
         && generoc!="" && dptresidenciac!="" && ciudadresidenciac!="" && fechanacec!="" && dptnacimientoc!="" && ciudadnacec!="")
	 {
		 	
     actualizarconyugue(tipoidentificacionc,numeroidentificacionc,pnombrec,snombrec,papellidoc,sapellidoc,generoc,direccionc,barrioc,telefonoc,
           celulcarc,emailc,tipoviviendac,ciudadresidenciac,dptresidenciac,zonac,estadocivilc,fechanacec,ciudadnacec
		   ,dptnacimientoc,escolaridadc,parentescoc,conviven);
			
	 }// fin if
	 
		}// fin validaractualizarconyugue
		
 function actualizarconyugue(tipoidentificacionc,numeroidentificacionc,pnombrec,snombrec,papellidoc,sapellidoc,generoc,direccionc,barrioc,telefonoc
           ,celulcarc,emailc,tipoviviendac,ciudadresidenciac,dptresidenciac,zonac,estadocivilc,fechanacec,ciudadnacec
		   ,dptnacimientoc,escolaridadc,parentescoc,conviven){
		
 $.getJSON('actualizarconyugue.php',{v0:tipoidentificacionc,v1:numeroidentificacionc,v2:pnombrec,v3:snombrec,v4:papellidoc,v5:sapellidoc,v6:generoc
		           ,v7:direccionc,v8:barrioc,v9:telefonoc,v10:celulcarc,v11:emailc,v12:tipoviviendac,v13:ciudadresidenciac,v14:dptresidenciac,v15:zonac
				   ,v16:estadocivilc,v17:fechanacec,v18:ciudadnacec,v19:dptnacimientoc,v20:escolaridadc,vrc:parentescoc,vrc1:conviven},function(datos){
			
			if(datos==0)
			{
				alert("No se pudo actualizar los datos");
			
		    }// fin if	
			else
			{		
				alert("Registro actualizado");
				busquedaobservacionesgenerales();
				
				var alertaasocajasbeneficiario= getCookie("alertaasocajasbeneficiario");
				var alertafosygabeneficiario= getCookie("alertafosygabeneficiario");
				var alertainactivobeneficiario= getCookie("alertainactivobeneficiario");
				
				if(alertaasocajasbeneficiario=='asocajasbeneficiario')
					{
						alert("Beneficiario Encontrado en Asocajas");
					}
				 		
				 if(alertafosygabeneficiario=='fosygabeneficiario')
						{
							alert("Beneficiario Encontrado en Fosyga");
						}
				
				 if(alertainactivobeneficiario=='inactivobeneficiario')
						{
							alert("Beneficiario Activo");
						}		
						
				//location.reload();
			    document.getElementById("principal").style.display="block";
				$("#principal").load('PrincipalGrupoFamiliar.php');
				document.getElementById('tiporelacion').selectedIndex = 0;	
				document.getElementById("conyugue").style.display="none";
				document.getElementById("beneficiario").style.display="none";
				//setTimeout('document.location.reload()',500);
			}// fin else
			//alert("Registro guardado numero id: " + datos);
		})
 }//  fin actualizarconyugue
 
function buscardatosrelacionconyugue(idconyugue){
	
            setCookie('convivenconyugue','',-1);			
		    $.getJSON('buscardatosrelacionconyugue.php',{v0:idconyugue},function(datos){				
			$.each(datos,function(i,fila){	
			   $("#tiporelacion_conyugue").val(fila.idparentesco);
			   $("#conviven_conyugue").val(fila.conviven);
			   
			   setCookie("convivenconyugue", fila.conviven, 1);
			   
				return;
			});
			
			if(datos==0)
			{
				document.getElementById('guardarc').style.display='block';
				document.getElementById('actualizarc').style.display='none'; 
			}
			else
			{
				document.getElementById('guardarc').style.display='none';
				document.getElementById('actualizarc').style.display='block';
			}
		
		})	
	}// fin buscardatosrelacionconyugue
		
		
  
function buscardatosconyugue(){
		
		
		$("table.tablero :input.ui-state-error,select.ui-state-error").removeClass("ui-state-error");//Limpiar la clase de error
		
	  	var tipoidentificacionc = $("#tipoidentificacion_conyugue").val();
	    if($("#tipoidentificacion_conyugue").val()=='')	{
			$("#tipoidentificacion_conyugue").addClass("ui-state-error");
			error++;
			}// fin if	

		var numeroidentificacionc = $("#numeroidentificacion_conyugue").val();		
		
		$.getJSON('buscardatosconyugue.php',{v0:tipoidentificacionc,v1:numeroidentificacionc},function(datos){
					
			$.each(datos,function(i,fila){	
			
	          	setCookie("idpersonac", fila.idpersona, 1);
											
				$("#papellido_conyugue").val(fila.papellido);
				$("#sapellido_conyugue").val(fila.sapellido );
				$("#pnombre_conyugue").val(fila.pnombre );
				$("#snombre_conyugue").val(fila.snombre);
				$("#genero_conyugue").val(fila.sexo);
				$("#direccion_conyugue").val(fila.direccion);				
				$("#telefono_conyugue").val(fila.telefono);
				$("#celular_conyugue").val(fila.celular);
				$("#email_conyugue").val(fila.email);
				$("#tipovivienda_conyugue").val(fila.idtipovivienda );
				
				$("#dptresidencia_conyugue").val(fila.iddepresidencia);	
				idciudad=fila.idciuresidencia;			
				dependencia_municipio(fila.iddepresidencia,idciudad);		 
					
     			idzona=fila.idzona;
				dependencia_municipio_zona(fila.idciuresidencia,idzona)
				
				idbarrio=fila.idbarrio;
				dependencia_zona_barrio(fila.idzona,idbarrio)				
		 
				$("#estadocivil_conyugue").val(fila.idestadocivil);
				$("#fechanace_conyugue").val(fila.fechanacimiento);
							 			  
				$("#dptnacimiento_conyugue").val(fila.iddepnace);
				idciunace=fila.idciunace;			
				dependencia_municipionace(fila.iddepnace,idciunace);	
								  
				$("#escolaridad_conyugue").val(fila.idescolaridad);	
				
				buscarfosyga(numeroidentificacionc);
				buscarasocajas(numeroidentificacionc);
				buscaractivo(fila.idpersona);					
				buscardatosrelacionconyugue(fila.idpersona);	
							
				return;
			});
					
			if(datos==0)
			{
				 document.getElementById('guardarc').style.display='block';
				 document.getElementById('actualizarc').style.display='none'; 
				 limpiarconyugue();
			}
					
		})
	
				
	}// fin buscardatosconyugue		
		
//---------------------------------------------------------------------------------------------------------------------------------------------------//
