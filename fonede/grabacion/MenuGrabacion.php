<?php 
date_default_timezone_set('America/Bogota');
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$url=$_SERVER['PHP_SELF'];
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'auditoria.php';
auditar($url);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Registrar Grabaci&oacute;n</title>
<link type="text/css" href="../../css/css.1.8/jquery-ui-1.8.17.custom.css" rel="stylesheet" />
<link type="text/css" href="../../css/Estilos.css" rel="stylesheet"/>
<link type="text/css" rel="stylesheet" href="../../css/marco.css" />

<script language="javascript" src="../../newjs/1.6/jquery-1.6.2.min.js"></script>
<script language="javascript" src="../../newjs/1.6/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="../../js/comunes.js"></script>
<script languaje="javascript" src="js/grabacion.js"></script>

    
<style>
        body { font-size: 12px; }		
        label, input { display:block; }
        input.text { margin-bottom:12px; width:95%; padding: .4em; }
        fieldset { padding:0; border:0; margin-top:25px; }
        h1 { font-size: 1.2em; margin: .6em 0; }
        div#users-contain { width: 350px; margin: 20px 0; }
        div#users-contain table { margin: 1em 0; border-collapse: collapse; width: 100%; }
        div#users-contain table td, div#users-contain table th { border: 1px solid #eee; padding: .6em 10px; text-align: left; }
        .ui-dialog .ui-state-error { padding: .3em; }
        .validateTips { border: 1px solid transparent; padding: 0.3em; }
</style>


		<!--[if lt IE 9]>
			<style>
				.content{
					height: auto;
					margin: 0;
				}
				.content div {
					position: relative;
				}
			</style>
		<![endif]-->
 
  
  <script>
  $(document).ready(function() {
    $("#tabs").tabs();	
  });
  </script> 
  
  <script>
    	
// funcion para obtener cookie en javascript
	function getCookie(c_name)
	{
		var i,x,y,ARRcookies=document.cookie.split(";");
		for (i=0;i<ARRcookies.length;i++)
		{
  			x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
  			y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
  			x=x.replace(/^\s+|\s+$/g,"");
  			if (x==c_name)
    			{
    				return unescape(y);
    			}// fin if
		}// fin for
  	}// fin getCookie

// ejemplo de como tomar la identificacion en javascript
	function mostrar_id() {
		ident=getCookie("identificacion");
		//alert("Identif:"+ident);
	}// fin mostrar_id

	function mostrar_carga() {
		ident=getCookie("identificacion");
		//alert("Cargado Grupo Familiar Identif:"+ident);
	}// fin mostrar_carga
	   
   </script>

</head>

<body>
<div class="demo" id="admin">
  
<div id="tabs">
    <ul>
        <li><a href="DatosPostulado.php"><span>Datos del Postulado</span></a></li>
        <li><a href="GrupoFamiliar.php"><span>Grupo Familiar</span></a></li>
        <li><a href="OtrosDatos.php"><span>Otros Requisitos</span></a></li>        
    </ul>
   
</div><!--fin div id="tabs"-->
</div><!-- End demo -->

<div style="display: none;" class="demo-description" >
<p>Click tabs to swap between content that is broken into logical sections.</p>
</div><!-- End demo-description -->
</body>
</html>
