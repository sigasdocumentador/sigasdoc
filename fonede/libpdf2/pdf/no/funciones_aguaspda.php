<?php

require_once('../../soap/lib/nusoap.php');
$urlservices = $_COOKIE['url_services'];
$revision = $_COOKIE['revisar'];

function mostrar_dato($parid, $clase, $anchocol, $visible, $idformulario, $nvomod)
{
		global $urlservices;
		global $nvomod;
		global $revision;
		$datovisible=$visible;
		$soapclient = new soapclient($urlservices.'ver_diccionario_datos.php');
		$parametros = array('parid'=>$parid);
		$datosdic = $soapclient->call('ver_diccionario',$parametros);
              $chequeo="";
		 foreach ($datosdic as $datodic) {
			$idpresent=$datodic["idpresent"];
			$tipodato=$datodic["tipodato"];
			$idtabla=$datodic["idtabla"];
			$longdato=$datodic["longdato"];
			$idcombocond=trim($datodic["idcombocond"]);
			$valorcombocond=$datodic["valorcombocond"];
			$codigocondic=$datodic["codigocondic"];
			$codigocondic=trim($codigocondic);
			$codigocondic=str_replace("%", "'", $codigocondic);
		 } 
		$nombrecampo=$parid."_0_0_datof";
		echo "<td class='".$clase."' width='".$anchocol."%' valign='bottom'>";
//		echo "id ".$parid;
//		echo " combocond ".$idcombocond;


		$valordato="";
		$valorotro="";
		if ($nvomod=="M")
		{
			$soapclient = new soapclient($urlservices.'ver_dato_formulario.php');
			$parametros = array('paridform'=>$idformulario, 'parnombdato'=>$nombrecampo);
			$valores = $soapclient->call('leerdatoform',$parametros);

			if (count($valores) > 0) {
				foreach ($valores as $unvalor) {
					$valordato=$unvalor["valordato"];
					$valordato=utf8_encode($valordato);
					$valorotro=$unvalor["valorotro"];
					$valorotro=utf8_encode($valorotro);
					$indcorregir=$unvalor["indcorregir"];

		 		} 
		 	} 
 		}
	$valordato=utf8_decode($valordato);

		$htmldisabled="";



		if ($revision != "S" && $indcorregir == "1")
		{
		        echo "<div style='font-family:courier new; color:white; background-color:red; font-size:12px'>";
		        echo "Corregir";
	        	  echo "</div>";

		}

		if (strlen($valordato) > 0)
		{
			$datovisible="S";
		}


		if ($idpresent=="1")
		{
			veropcionestabla($idtabla, $nombrecampo, "S", $valordato, $valorotro, $codigocondic, $nvomod,$idcombocond,$valorcombocond);
		}
		if ($idpresent=="4")
		{
			veropcionestablachk($idformulario, $idtabla, $nombrecampo, "S", $valordato, $valorotro, $codigocondic, $nvomod,$idcombocond,$valorcombocond);
		}
		if ($idpresent=="2")

		{
			$htmlvisible="";
			if ($datovisible=="N")
			{
//				$htmlvisible='style="display:none"';
				$htmlvisible=' style="visibility:hidden" ';
			}

 	      		$htmlinput=$valordato;
			echo $htmlinput;                     
                     

			if (strlen($idcombocond) > 0)
			{
		   		echo "<script>";
				$combocondic=$idcombocond."_0_0_datof";
		   		$funcion_combo_cond="mostrar_segun_combo('".$combocondic."', '".$valorcombocond."', '".$nombrecampo."');";
		   		echo $funcion_combo_cond;
		   		echo "</script>";
			}
	
		}

		if ($idpresent=="3")

		{
			$htmlvisible="";
			if ($datovisible=="N")
			{
//				$htmlvisible='style="display:none"';
				$htmlvisible=' style="visibility:hidden" ';
			}

 	      		$htmlinput=$valordato;
			echo $htmlinput;                     
			if (strlen($idcombocond) > 0)
			{
		   		echo "<script>";
				$combocondic=$idcombocond."_0_0_datof";
		   		$funcion_combo_cond="mostrar_segun_combo('".$combocondic."', '".$valorcombocond."', '".$nombrecampo."');";
		   		echo $funcion_combo_cond;
		   		echo "</script>";
			}
                     
		}
		if ($idpresent=="5")

		{
			$htmlvisible="";
			if ($datovisible=="N")
			{
//				$htmlvisible='style="display:none"';
				$htmlvisible=' style="visibility:hidden" ';
			}

                     if($valordato==1)
                     {

 	      		$htmlinput='<input name="'.$nombrecampo.'" type="checkbox" id="'.$nombrecampo.'" onchange="ActualizaDatoCheck(\''.$nombrecampo.'\', \''.$tipodato.'\')"  value="1" '.$htmlvisible.$htmldisabled.' width="100%" checked="checked" disabled="disabled">';
                      }

                     else
                     {
 	      		$htmlinput='<input name="'.$nombrecampo.'" type="checkbox" id="'.$nombrecampo.'" onchange="ActualizaDatoCheck(\''.$nombrecampo.'\', \''.$tipodato.'\')"  value="1" '.$htmlvisible.$htmldisabled.' width="100%" disabled="disabled"  >';
                     }
			echo $htmlinput;                     
			if (strlen($idcombocond) > 0)
			{
		   		echo "<script>";
				$combocondic=$idcombocond."_0_0_datof";
		   		$funcion_combo_cond="mostrar_segun_combo('".$combocondic."', '".$valorcombocond."', '".$nombrecampo."');";

		   		echo $funcion_combo_cond;
		   		echo "</script>";
			}
                     
		}




		echo "</td>";
}

function mostrar_dato_acueducto($parid, $clase, $anchocol, $visible, $idformulario, $nvomod)
{

		global $urlservices;
		global $nvomod;
		global $revision;
		$datovisible=$visible;
		$soapclient = new soapclient($urlservices.'ver_diccionario_datos.php');
		$parametros = array('parid'=>$parid);
		$datosdic = $soapclient->call('ver_diccionario',$parametros);
              $chequeo="";
		 foreach ($datosdic as $datodic) {
			$idpresent=$datodic["idpresent"];
			$tipodato=$datodic["tipodato"];
			$idtabla=$datodic["idtabla"];
			$longdato=$datodic["longdato"];
			$idcombocond=trim($datodic["idcombocond"]);
			$valorcombocond=$datodic["valorcombocond"];
			$codigocondic=$datodic["codigocondic"];
			$codigocondic=trim($codigocondic);
			$codigocondic=str_replace("%", "'", $codigocondic);
		 } 
		$nombrecampo=$parid."_0_0_datof";
		echo "<td class='".$clase."' width='".$anchocol."%' valign='bottom'>";
//		echo "id ".$parid;
//		echo " combocond ".$idcombocond;


		$valordato="";
		$valorotro="";
		if ($nvomod=="M")
		{
			$soapclient = new soapclient($urlservices.'ver_dato_acueducto.php');
			$parametros = array('paridform'=>$idformulario, 'parnombdato'=>$nombrecampo);
			$valores = $soapclient->call('leerdatoform',$parametros);

			if (count($valores) > 0) {
				foreach ($valores as $unvalor) {
					$valordato=$unvalor["valordato"];
					$valordato=utf8_encode($valordato);
					$valorotro=$unvalor["valorotro"];
					$valorotro=utf8_encode($valorotro);
					$indcorregir=$unvalor["indcorregir"];

		 		} 
		 	} 
 		}

		$htmldisabled="";



		if ($revision != "S" && $indcorregir == "1")
		{
		        echo "<div style='font-family:courier new; color:white; background-color:red; font-size:12px'>";
		        echo "Corregir";
	        	  echo "</div>";

		}

		if (strlen($valordato) > 0)
		{
			$datovisible="S";
		}


		if ($idpresent=="1")
		{
			veropcionestabla($idtabla, $nombrecampo, "S", $valordato, $valorotro, $codigocondic, $nvomod,$idcombocond,$valorcombocond);
		}
		if ($idpresent=="4")
		{
			veropcionestablachkAcued($idformulario, $idtabla, $nombrecampo, "S", $valordato, $valorotro, $codigocondic, $nvomod,$idcombocond,$valorcombocond);
		}
		if ($idpresent=="2")

		{
			$htmlvisible="";
			if ($datovisible=="N")
			{
//				$htmlvisible='style="display:none"';
//				$htmlvisible=' disabled="" ';
				$htmlvisible=' style="visibility:hidden" ';
			}

 	      		$htmlinput=$valordato;
			echo $htmlinput;                     
                     

			if (strlen($idcombocond) > 0)
			{
		   		echo "<script>";
				$combocondic=$idcombocond."_0_0_datof";
		   		$funcion_combo_cond="mostrar_segun_combo('".$combocondic."', '".$valorcombocond."', '".$nombrecampo."');";
		   		echo $funcion_combo_cond;
		   		echo "</script>";
			}
	
		}

		if ($idpresent=="3")

		{
			$htmlvisible="";
			if ($datovisible=="N")
			{
//				$htmlvisible='style="display:none"';
//				$htmlvisible=' disabled="" ';
				$htmlvisible=' style="visibility:hidden" ';
			}

 	      		$htmlinput=$valordato;
			echo $htmlinput;                     
			if (strlen($idcombocond) > 0)
			{
		   		echo "<script>";
				$combocondic=$idcombocond."_0_0_datof";
		   		$funcion_combo_cond="mostrar_segun_combo('".$combocondic."', '".$valorcombocond."', '".$nombrecampo."');";
		   		echo $funcion_combo_cond;
		   		echo "</script>";
			}
                     
		}
		if ($idpresent=="5")

		{
			$htmlvisible="";
			if ($datovisible=="N")
			{
//				$htmlvisible='style="display:none"';
				$htmlvisible=' style="visibility:hidden" ';
			}

                     if($valordato==1)
                     {

 	      		$htmlinput='<input name="'.$nombrecampo.'" type="checkbox" id="'.$nombrecampo.'" onchange="ActualizaDatoCheck(\''.$nombrecampo.'\', \''.$tipodato.'\')"  value="1" '.$htmlvisible.$htmldisabled.' width="100%" checked="checked" disabled="disabled"  >';
                      }

                     else
                     {
 	      		$htmlinput='<input name="'.$nombrecampo.'" type="checkbox" id="'.$nombrecampo.'" onchange="ActualizaDatoCheck(\''.$nombrecampo.'\', \''.$tipodato.'\')"  value="1" '.$htmlvisible.$htmldisabled.' width="100%"  disabled="disabled" >';
                     }
			echo $htmlinput;                     
			if (strlen($idcombocond) > 0)
			{
		   		echo "<script>";
				$combocondic=$idcombocond."_0_0_datof";
		   		$funcion_combo_cond="mostrar_segun_combo('".$combocondic."', '".$valorcombocond."', '".$nombrecampo."');";

		   		echo $funcion_combo_cond;
		   		echo "</script>";
			}
                     
		}




		echo "</td>";
}


function veropcionestabla($idtabla, $nombresel, $indgrabar, $valordato, $valorotro, $codigocondic,$nvomod,$idcombocond,$valorcombocond)
{
global $urlservices;
global $revision;

$htmldisabled="";

if ($revision=="S")
{
}

$soapclient = new soapclient($urlservices.'ver_opciones_tabla.php');
$parametros = array('paridtabla'=>$idtabla);
$registros = $soapclient->call('consultaropcion',$parametros);

//echo $soapclient->request; // shows XML of SOAP request
//echo $soapclient->response; //shows XML of SOAP response 

	$selparte1= '<select size="1" name="'.$nombresel.'" id="'.$nombresel.'" ';
	$selparte2= "";
	$ind=1;
	$indotro=-1;

	$nombre_otro="";

	$estilovisible='visibility:hidden';
	foreach ($registros as $info) {
		$idopcion =$info["idopcion"];
		$descripcion2=$info["descripcion2"];
		$adicional=$info["ind_adicional"];
	       $descripcion2=utf8_decode($descripcion2);

		if ($adicional=="si")
		{
			$pos = strpos($nombresel, "0_0_");
			$nombre_otro=substr($nombresel,0,$pos+3)."_otro";
//	       	$nombre_otro=$info["nombre_otro"];
			
		       $indotro=$ind;
		}
		$txtsel=' ';
		if ($idopcion==$valordato)
		{
			$txtsel=' selected="selected"';
                     $selparte22=$descripcion2;
			if ($adicional=="si")
			{
				$estilovisible='visibility:block';
			}
		}

		$selparte2 = $selparte2.'<option value="'.$idopcion.'"'.$txtsel.'>'.$descripcion2.'</option>';
		$ind=$ind+1;
	}
	$codigo_onchange="mostrar_ocultar_otro('".$indgrabar."',".$indotro.",this.name, this.selectedIndex,'".$nombre_otro."'); ".$codigocondic;
        if($selparte22=="")
	$selparte22="No seleccionado";

	$selparte2=$selparte2.'</select>';

       if($nombre_otro != "")
   {
      $selparte3=" ".$valorotro;
   }

       echo $selparte22.$selparte3;
	if (strlen($idcombocond) > 0)
			{
		   		echo "<script>";
				$combocondic=$idcombocond."_0_0_datof";
		   		$funcion_combo_cond="mostrar_segun_combo('".$combocondic."', '".$valorcombocond."', '".$nombresel."');";
		   		echo $funcion_combo_cond;
		   		echo "</script>";
			}



}
// ====================================================================
function veropcionestablachk($idformulario, $idtabla, $nombrechk, $indgrabar, $valordato, $valorotro, $codigocondic,$nvomod,$idcombocond,$valorcombocond)
{
global $urlservices;
global $revision;
//$revision="S";

$htmldisabled="";

if ($revision=="S")
{
}

$soapclient = new soapclient($urlservices.'ver_opciones_tabla.php');
$parametros = array('paridtabla'=>$idtabla);
$registros = $soapclient->call('consultaropcion',$parametros);

//echo $soapclient->request; // shows XML of SOAP request
//echo $soapclient->response; //shows XML of SOAP response 

	$inputchktotal= "";
	$selparte2= "";
	$ind=1;


	foreach ($registros as $info) {
		$estilovisible='visibility:hidden';
		$htmlchecked="";
		$idopcion =$info["idopcion"];
		$descripcion2=$info["descripcion2"];
	        $descripcion2=utf8_decode($descripcion2);
		$adicional=$info["ind_adicional"];
		$idinput=$nombrechk.$idopcion;
		$valorotro="";
		$nombre_otro="";
		if ($adicional=="si")
		{
			$pos = strpos($nombrechk, "0_0_");
			$nombre_otro=substr($nombrechk,0,$pos+3)."_otro";
		}


			$soapclient2 = new soapclient($urlservices.'ver_datochk_formulario.php');
			$parametros = array('paridform'=>$idformulario, 'parnombdato'=>$nombrechk, 'parvalordato'=>$idopcion);
			$valores = $soapclient2->call('leerdatoform',$parametros);

			if (count($valores) > 0) {
				$htmlchecked=" checked='checked' ";
				$estilovisible='visibility:visible';

				foreach ($valores as $unvalor) {
					$valorotro=$unvalor["valorotro"];
					$valorotro=utf8_encode($valorotro);
		 		} 
		 	} 


		$inputotro="";
		$indotro=0;
       		if($nombre_otro != "")
		{
			$indotro=1;
      			$inputotro=$valorotro.'<br>';
   		}
		$inputchk= '<input type="checkbox"  id="'.$idinput.'" disabled="disabled" onchange="ActualizaDatoChk(\''.$nombrechk.'\', \''.$idinput.'\', \''.$indotro.'\', \''.$nombre_otro.'\')"   name="'.$nombrechk.'" value="'.$idopcion.'" '.$htmlchecked.'/>'.$descripcion2.'<br>';
		$inputchktotal= $inputchktotal.$inputchk.$inputotro;
		
	}
       	echo $inputchktotal;

	if (strlen($idcombocond) > 0)
			{
		   		echo "<script>";
				$combocondic=$idcombocond."_0_0_datof";
		   		$funcion_combo_cond="mostrar_segun_combo('".$combocondic."', '".$valorcombocond."', '".$idinput."');";
		   		echo $funcion_combo_cond;
		   		$funcion_combo_cond="mostrar_segun_combo('".$combocondic."', '".$valorcombocond."', '".$nombrechk."');";
		   		echo $funcion_combo_cond;
		   		echo "</script>";
			}



}



// ====================================================================
function veropcionestablachkAcued($idformulario, $idtabla, $nombrechk, $indgrabar, $valordato, $valorotro, $codigocondic,$nvomod,$idcombocond,$valorcombocond)
{
global $urlservices;
global $revision;
//$revision="S";

$htmldisabled="";

if ($revision=="S")
{
}

$soapclient = new soapclient($urlservices.'ver_opciones_tabla.php');
$parametros = array('paridtabla'=>$idtabla);
$registros = $soapclient->call('consultaropcion',$parametros);

//echo $soapclient->request; // shows XML of SOAP request
//echo $soapclient->response; //shows XML of SOAP response 

	$inputchktotal= "";
	$selparte2= "";
	$ind=1;


	foreach ($registros as $info) {
		$estilovisible='visibility:hidden';
		$htmlchecked="";
		$idopcion =$info["idopcion"];
		$descripcion2=$info["descripcion2"];
	        $descripcion2=utf8_decode($descripcion2);
		$adicional=$info["ind_adicional"];
		$idinput=$nombrechk.$idopcion;
		$valorotro="";
		$nombre_otro="";
		if ($adicional=="si")
		{
			$pos = strpos($nombrechk, "0_0_");
			$nombre_otro=substr($nombrechk,0,$pos+3)."_otro";
		}


			$soapclient2 = new soapclient($urlservices.'ver_datochk_acueducto.php');
			$parametros = array('paridform'=>$idformulario, 'parnombdato'=>$nombrechk, 'parvalordato'=>$idopcion);
			$valores = $soapclient2->call('leerdatoform',$parametros);

			if (count($valores) > 0) {
				$htmlchecked=" checked='checked' ";
				$estilovisible='visibility:visible';

				foreach ($valores as $unvalor) {
					$valorotro=$unvalor["valorotro"];
					$valorotro=utf8_encode($valorotro);
		 		} 
		 	} 


		$inputotro="";
		$indotro=0;
       		if($nombre_otro != "")
		{
			$indotro=1;
      			$inputotro=$valorotro;
   		}
		$inputchk= '<input type="checkbox"  id="'.$idinput.'" onchange="ActualizaDatoChk(\''.$nombrechk.'\', \''.$idinput.'\', \''.$indotro.'\', \''.$nombre_otro.'\')"   name="'.$nombrechk.'" value="'.$idopcion.'" '.$htmlchecked.' disabled="disabled"/>'.$descripcion2.'<br>';
		$inputchktotal= $inputchktotal.$inputchk.$inputotro;
		
	}
       	echo $inputchktotal;

	if (strlen($idcombocond) > 0)
			{
		   		echo "<script>";
				$combocondic=$idcombocond."_0_0_datof";
		   		$funcion_combo_cond="mostrar_segun_combo('".$combocondic."', '".$valorcombocond."', '".$idinput."');";
		   		echo $funcion_combo_cond;
		   		$funcion_combo_cond="mostrar_segun_combo('".$combocondic."', '".$valorcombocond."', '".$nombrechk."');";
		   		echo $funcion_combo_cond;
		   		echo "</script>";
			}



}



//===========================================

function verelementos($cod_bloque, $idformulario, $nvomod)
{
global $urlservices;
$soapclient = new soapclient($urlservices.'ver_elementos_form.php');
//$soapclient = new soapclient($urlservices.'ver_element.php');
$parcod_bloque = $cod_bloque;
$parindtitulo="S";
$parnivel="3";
$parametros = array('parcod_bloque'=>$parcod_bloque, 'parindtitulo'=>$parindtitulo, 'parinivel'=>$parnivel);
$titulos = $soapclient->call('ver_elementos',$parametros);
//echo $soapclient->request; // shows XML of SOAP request
//echo $soapclient->response; //shows XML of SOAP response 
 foreach ($titulos as $titulo) {
	$numpresent=$titulo["numpresent"];
	$descripcion=$titulo["descripcion"];
	//$descripcion=utf8_encode($descripcion);
	$clase=$titulo["clase"];
  }

  $nombloque="bloque".$cod_bloque;
  $nomboton="btn".$cod_bloque;

  echo "<h2>".$descripcion."</h2><br>";


  echo "<div id='".$nombloque."' style='display:block' class='cuadro' >";

$soapclient2 = new soapclient($urlservices.'ver_elementos_form.php');
$parcod_bloque = $cod_bloque;
$parindtitulo="N";
$parnivel="3";
$parametros = array('parcod_bloque'=>$parcod_bloque, 'parindtitulo'=>$parindtitulo, 'parinivel'=>$parnivel);
$renglones = $soapclient2->call('ver_elementos',$parametros);
//echo $soapclient->request; // shows XML of SOAP request
//echo $soapclient->response; //shows XML of SOAP response 

 foreach ($renglones as $renglon) {
	$numpresent=$renglon["numpresent"];
	if ($numpresent==" ")
	{
		$numpresent="&nbsp";
	}

	$descripcion=$renglon["descripcion"];
	//$descripcion=utf8_encode($descripcion);
	$obligatorio=$renglon["obligatorio"];

if ($obligatorio  == 'S')

{
	$numpresent=	$numpresent."(*)";
} 

	$anchodesc=$renglon["anchodesc"]."%";
	$clase=$renglon["clase"];
	$ndatosasoc=$renglon["ndatosasoc"];
	$ntitasoc=$renglon["ntitasoc"];
	$iddicc1=$renglon["iddicc1"];
	$iddicc2=$renglon["iddicc2"];
	$iddicc3=$renglon["iddicc3"];
	$iddicc4=$renglon["iddicc4"];

	$ancho1=$renglon["ancho1"];
	$ancho2=$renglon["ancho2"];
	$ancho3=$renglon["ancho3"];
	$ancho4=$renglon["ancho4"];

	$titasoc1=$renglon["titasoc1"];
	//$titasoc1=utf8_encode($titasoc1);

	$titasoc2=$renglon["titasoc2"];
	//$titasoc2=utf8_encode($titasoc2);

	$titasoc3=$renglon["titasoc3"];
	//$titasoc3=utf8_encode($titasoc3);

	$titasoc4=$renglon["titasoc4"];
	//$titasoc4=utf8_encode($titasoc4);

	$visible1=$renglon["visible1"];
	$visible2=$renglon["visible2"];
	$visible3=$renglon["visible3"];
	$visible4=$renglon["visible4"];




  echo "<table width='850' cellspacing='1' cellpadding='0' align='center'>";
  echo "<tr>";
  if ($anchodesc > 0)
  {
  echo "<td  class='".$clase."' width='".$anchodesc."'>".$descripcion."</td>";
  if ($ntitasoc > 0)
  {
	for ( $j = 1; $j <= $ntitasoc; $j += 1) {
		$tituloasoc="${titasoc.$j}";
		$anchocol="${ancho.$j}";
		echo "<td class='".$clase."' width='".$anchocol."%'>".$tituloasoc." </td>";
	}

  }
  if ($ndatosasoc > 0)
  {
	for ( $j = 1; $j <= $ndatosasoc; $j += 1) {
		$parid="${iddicc.$j}";
		$visible="${visible.$j}";
		$anchocol="${ancho.$j}";
		mostrar_dato($parid, $clase, $anchocol, $visible, $idformulario, $nvomod);
	}

  }
  }
// anchodesc=0
  else
  {
	  if ($ntitasoc == $ndatosasoc)
	  {
		$ncol=$ntitasoc + $ndatosasoc;
		for ( $j = 1; $j <= $ntitasoc; $j += 1) {
			$tituloasoc="${titasoc.$j}";
			$anchocol="${ancho.$j}";
			echo "<td class='".$clase."'  width='".$anchocol."%'>".$tituloasoc." </td>";

		$parid="${iddicc.$j}";
		$visible="${visible.$j}";
		$anchocol="${ancho.$j}";
		mostrar_dato($parid, $clase, $anchocol, $visible, $idformulario, $nvomod);

		}

	  }
  }

  echo "</tr>&nbsp;<tr></tr>";
  echo "</table>";
  }
  echo "</div>";



}


function verseccion($cod_seccion, $idformulario, $nvomod)
{
require_once('../../soap/lib/nusoap.php');
global $urlservices;
//$soapclient = new soapclient($urlservices.'ver_secciones.php');
$parametros = array('parseccion'=>$cod_seccion);
//$registros = $soapclient->call('consultarseccion',$parametros);

$soapclient3 = new soapclient($urlservices.'ver_cod_bloque.php');
$registros3 = $soapclient3->call('ver_descripcion',$parametros);



//echo $soapclient->request; // shows XML of SOAP request
//echo $soapclient->response; //shows XML of SOAP response 
	foreach ($registros3 as $info) {
	$cod_bloque=$info["codigo_bloque"];
	
  verelementos($cod_bloque, $idformulario, $nvomod);
		  }
		  echo "</table>";
		  
}
function ver_veredas($idmunicipio, $indsel)
{
global $urlservices;
$soapclient = new soapclient($urlservices.'ver_veredas.php');
$parametros = array('paridmuni'=>$idmunicipio,'parvereda'=> $idvereda);
$registros = $soapclient->call('consultarveredas',$parametros);

//echo $soapclient->request; // shows XML of SOAP request
//echo $soapclient->response; //shows XML of SOAP response 
//       echo '<td>';
	echo '<select size="1" name="cmb_veredas" id="cmb_veredas" onchange="activar_boton_nuevo(this.selectedIndex)">';
	echo'<option value="0">Seleccione vereda</option>';
	$ind=1;
	foreach ($registros as $info) {
	$idvereda =$info["idvereda"];
	$nombre=$info["nombre"];
	if ($ind == $indsel)
	{
		echo'<option value="'.$idvereda.'" selected="selected">'.$nombre.'</option>';
	}
	else
	{
		echo'<option value="'.$idvereda.'">'.$nombre.'</option>';
	}
	$ind=$ind+1;
	}
	
//       echo '</td>';
}


function ver_veredas_agregar($codusuario,$indsel)
{
global $urlservices;
$soapclient = new soapclient($urlservices.'ver_veredash.php');
$parametros = array('parusuario'=>$codusuario);
$registros = $soapclient->call('consultarveredas',$parametros);

//echo $soapclient->request; // shows XML of SOAP request
//echo $soapclient->response; //shows XML of SOAP response 
//       echo '<td>';
	echo '<select size="1" name="cmb_veredas" id="cmb_veredas" onchange="activar_boton_nuevo(this.selectedIndex)">';
	echo'<option value="0">Seleccione vereda</option>';
	$ind=1;
	foreach ($registros as $info) {
	$idvereda=$info["idvereda"];
	$vereda=$info["nombre"];
	$vereda=utf8_decode($vereda);

	if ($ind == $indsel)
	{
		echo'<option value="'.$idvereda.'" selected="selected">'.$vereda.'</option>';
	}
	else
	{
		echo'<option value="'.$idvereda.'">'.$vereda.'</option>';
	}
	$ind=$ind+1;
	}
	
//       echo '</td>';
}

function ver_tipo_usuario($idtipo)
{
global $urlservices;
$soapclient = new soapclient($urlservices.'ver_tipo_usuarios.php');
$parametros = array('paridtipousuario'=>$paridtipousuario,'paridtipo'=> $paridtipo);
$registros = $soapclient->call('consultartipo',$parametros);

//echo $soapclient->request; // shows XML of SOAP request
//echo $soapclient->response; //shows XML of SOAP response 
       

       echo '<tr>';
       echo '<td>';
       echo 'Vereda:';

       echo '</td>';

       echo '<td class="bodyentrada_class">';
	echo '<select size="1" name="cmb_veredas" id="cmb_veredas" onchange="activar_boton_nuevo(this.selectedIndex)" >';
	foreach ($registros as $info) {
	$idtipo =$info["idtipo"];
	$descripcion=$info["descripcion"];
	$descripcion=utf8_encode($descripcion);
	echo'<option value="'.$idtipo.'">'.$descripcion.'</option>';
	}
	
       echo '</td>';
       echo '</tr>';

}

function verelementos2($cod_bloque, $idformulario, $nvomod)
{
global $urlservices;
$soapclient = new soapclient($urlservices.'ver_elementos_form.php');
$parcod_bloque = $cod_bloque;
$parindtitulo="S";
$parnivel="3";
$parametros = array('parcod_bloque'=>$parcod_bloque, 'parindtitulo'=>$parindtitulo, 'parinivel'=>$parnivel);
$titulos = $soapclient->call('ver_elementos',$parametros);
//echo $soapclient->request; // shows XML of SOAP request
//echo $soapclient->response; //shows XML of SOAP response 
 foreach ($titulos as $titulo) {
	$idelem=$titulo["id_elem"];
	$numpresent=$titulo["numpresent"];
	$descripcion=$titulo["descripcion"];
	$descripcion=utf8_encode($descripcion);

	$clase=$titulo["clase"];
  }

  $nombloque="bloque".$cod_bloque;
  $funcion_mostrar="mostrar_bloque('".$nombloque."')";
  $funcion_ocultar="ocultar_bloque('".$nombloque."')";


  echo "<table width='950' cellspacing='1' cellpadding='0' align='center'>";
  echo "</tr>";
  echo "<td width='5%' class='".$clase."' ></td>";
    $hreftitulo="mod_elemento.php?idtabla=".$idelem;

  echo "<td width='95%' class='".$clase."'><a href='".$hreftitulo."'>".$descripcion."</a></td>";

  echo "<td>";
  echo '<input type="button" onclick="'.$funcion_mostrar.'" value="+" class="botones"/>';
  echo "</td>";
  echo "<td>";
  echo '<input type="button" onclick="'.$funcion_ocultar.'" value="-" class="botones"/>';
  echo "</td>";


  echo "</tr>";
  echo "</table>";

  echo "<div id='".$nombloque."' style='display:none'>";

$soapclient2 = new soapclient($urlservices.'ver_elementos_form.php');
$parcod_bloque = $cod_bloque;
$parindtitulo="N";
$parnivel="3";
$parametros = array('parcod_bloque'=>$parcod_bloque, 'parindtitulo'=>$parindtitulo, 'parinivel'=>$parnivel);
$renglones = $soapclient2->call('ver_elementos',$parametros);
//echo $soapclient->request; // shows XML of SOAP request
//echo $soapclient->response; //shows XML of SOAP response 

 foreach ($renglones as $renglon) {
	$numpresent=$renglon["numpresent"];
	if ($numpresent==" ")
	{
		$numpresent="&nbsp";
	}
	$idelemento=$renglon["id_elem"];
	$descripcion=$renglon["descripcion"];
	$descripcion=utf8_encode($descripcion);
	$anchodesc=$renglon["anchodesc"]."%";
	$clase=$renglon["clase"];
	$ndatosasoc=$renglon["ndatosasoc"];
	$ntitasoc=$renglon["ntitasoc"];
	$iddicc1=$renglon["iddicc1"];
	$iddicc2=$renglon["iddicc2"];
	$iddicc3=$renglon["iddicc3"];
	$iddicc4=$renglon["iddicc4"];
	$titasoc1=$renglon["titasoc1"];
	$titasoc2=$renglon["titasoc2"];
	$titasoc3=$renglon["titasoc3"];
	$titasoc4=$renglon["titasoc4"];


    $hreftabla="mod_elemento.php?idtabla=".$idelemento;

  echo "<table width='950' cellspacing='1' cellpadding='0' align='center'>";
  echo "<tr>";

  echo "<td width='5%' class='".$clase."' ></td>";
   
  echo "<td  class='".$clase."' width='".$anchodesc."'><a href='".$hreftabla."'>".$descripcion."</a></td>";

  if ($ntitasoc > 0)
  {
	for ( $j = 1; $j <= $ntitasoc; $j += 1) {
		$tituloasoc="${titasoc.$j}";
		echo "<td class='".$clase."' >".$tituloasoc." </td>";
	}

  }
  if ($ndatosasoc > 0)
  {
	for ( $j = 1; $j <= $ndatosasoc; $j += 1) {
		$parid="${iddicc.$j}";
		$soapclient3 = new soapclient($urlservices.'ver_diccionario_datos.php');
		$parametros = array('parid'=>$parid);
		$datosdic = $soapclient3->call('ver_diccionario',$parametros);

		 foreach ($datosdic as $datodic) {
			$idpresent=$datodic["idpresent"];
			$tipodato=$datodic["tipodato"];
			$idtabla=$datodic["idtabla"];
			$longdato=$datodic["longdato"];
		 } 
		$nombrecampo=$parid."_0_0_datof";
		echo "<td class='".$clase."' >";
		if ($idpresent=="1")
		{
			veropcionestabla($idtabla, $nombrecampo);
		}
		if ($idpresent=="2")
		{

 	      		$htmlinput='<input name="'.$nombrecampo.'" type="text" id="'.$nombrecampo.'"/>';
			echo $htmlinput;                     
                     
		}

		if ($idpresent=="3")
		{

 	      		$htmlinput='<textarea name="'.$nombrecampo.'"  id="'.$nombrecampo.'" cols="40" rows="3"></textarea>';
			echo $htmlinput;                     
                     
		}
		echo "</td>";
	}

  }
   // echo '<td width="20px"><a href="'.$hreftabla.'">mod</a></td>';

  echo "</tr>";
  echo "</table>";
  }
  echo "</div>";



}

function verseccion2($cod_seccion, $nvomod)
{
require_once('../soap/lib/nusoap.php');
global $urlservices;
$soapclient = new soapclient($urlservices.'ver_secciones.php');
$parametros = array('parseccion'=>$cod_seccion);
$registros = $soapclient->call('consultarseccion',$parametros);

$soapclient3 = new soapclient($urlservices.'ver_cod_bloque.php');
$registros3 = $soapclient3->call('ver_descripcion',$parametros);

//echo $soapclient->request; // shows XML of SOAP request
//echo $soapclient->response; //shows XML of SOAP response 
	foreach ($registros3 as $info) {
	$cod_bloque=$info["codigo_bloque"];
	
  verelementos2($cod_bloque, $idformulario, $nvomod);
		  }
		  echo "</table>";
		  
}



function verelementos_acueducto($cod_bloque, $idformulario, $nvomod)
{
global $urlservices;
$soapclient = new soapclient($urlservices.'ver_elementos_acue.php');
//$soapclient = new soapclient($urlservices.'ver_element.php');
$parcod_bloque = $cod_bloque;
$parindtitulo="S";
$parnivel="3";
$parametros = array('parcod_bloque'=>$parcod_bloque, 'parindtitulo'=>$parindtitulo, 'parinivel'=>$parnivel);
$titulos = $soapclient->call('ver_elementos',$parametros);
//echo $soapclient->request; // shows XML of SOAP request
//echo $soapclient->response; //shows XML of SOAP response 
 foreach ($titulos as $titulo) {
	$numpresent=$titulo["numpresent"];
	$descripcion=$titulo["descripcion"];
	//$descripcion=utf8_encode($descripcion);
	$clase=$titulo["clase"];
  }

  $nombloque="bloque".$cod_bloque;




  echo "<h2>".$descripcion."</h2><br>";




  echo "<div id='".$nombloque."' style='display:block' class='cuadro'>";

$soapclient2 = new soapclient($urlservices.'ver_elementos_acue.php');
$parcod_bloque = $cod_bloque;
$parindtitulo="N";
$parnivel="3";
$parametros = array('parcod_bloque'=>$parcod_bloque, 'parindtitulo'=>$parindtitulo, 'parinivel'=>$parnivel);
$renglones = $soapclient2->call('ver_elementos',$parametros);
//echo $soapclient->request; // shows XML of SOAP request
//echo $soapclient->response; //shows XML of SOAP response 
 $parnombre=descripcion; 
 foreach ($renglones as $renglon) {
	$numpresent=$renglon["numpresent"];
	if ($numpresent==" ")
	{
		$numpresent="&nbsp";
	}
	$descripcion=$renglon["descripcion"];
	//$descripcion=utf8_encode($descripcion);
	$obligatorio=$renglon["obligatorio"];

	$anchodesc=$renglon["anchodesc"]."%";
	$clase=$renglon["clase"];
	$ndatosasoc=$renglon["ndatosasoc"];
	$ntitasoc=$renglon["ntitasoc"];
	$iddicc1=$renglon["iddicc1"];
	$iddicc2=$renglon["iddicc2"];
	$iddicc3=$renglon["iddicc3"];
	$iddicc4=$renglon["iddicc4"];

	$ancho1=$renglon["ancho1"];
	$ancho2=$renglon["ancho2"];
	$ancho3=$renglon["ancho3"];
	$ancho4=$renglon["ancho4"];

	$titasoc1=$renglon["titasoc1"];
	//$titasoc1=utf8_encode($titasoc1);

	$titasoc2=$renglon["titasoc2"];
	//$titasoc2=utf8_encode($titasoc2);

	$titasoc3=$renglon["titasoc3"];
	//$titasoc3=utf8_encode($titasoc3);

	$titasoc4=$renglon["titasoc4"];
	//$titasoc4=utf8_encode($titasoc4);

	$visible1=$renglon["visible1"];
	$visible2=$renglon["visible2"];
	$visible3=$renglon["visible3"];
	$visible4=$renglon["visible4"];




  echo "<table width='850' cellspacing='1' cellpadding='0' align='center'>";
  echo "<tr>";
  if ($anchodesc > 0)
  {
  echo "<td  class='".$clase."' width='".$anchodesc."'>".$descripcion."</td>";
  if ($ntitasoc > 0)
  {
	for ( $j = 1; $j <= $ntitasoc; $j += 1) {
		$tituloasoc="${titasoc.$j}";
		$anchocol="${ancho.$j}";
		echo "<td class='".$clase."' width='".$anchocol."%'>".$tituloasoc." </td>";
	}

  }
  if ($ndatosasoc > 0)
  {
	for ( $j = 1; $j <= $ndatosasoc; $j += 1) {
		$parid="${iddicc.$j}";
		$visible="${visible.$j}";
		$anchocol="${ancho.$j}";
		mostrar_dato_acueducto($parid, $clase, $anchocol, $visible, $idformulario, $nvomod);
	}

  }
  }
// anchodesc=0
  else
  {
	  if ($ntitasoc == $ndatosasoc)
	  {
		$ncol=$ntitasoc + $ndatosasoc;
		for ( $j = 1; $j <= $ntitasoc; $j += 1) {
			$tituloasoc="${titasoc.$j}";
			$anchocol="${ancho.$j}";
			echo "<td class='".$clase."'  width='".$anchocol."%'>".$tituloasoc." </td>";

		$parid="${iddicc.$j}";
		$visible="${visible.$j}";
		$anchocol="${ancho.$j}";
		mostrar_dato_acueducto($parid, $clase, $anchocol, $visible, $idformulario, $nvomod);

		}

	  }
  }

  echo "</tr>&nbsp;<tr></tr>";
  echo "</table>";
  }
  echo "</div>";



}


function verseccion3($cod_seccion, $idformulario)
{
require_once('../soap/lib/nusoap.php');
global $urlservices;
//$soapclient = new soapclient($urlservices.'ver_secciones.php');
$parametros = array('parseccion'=>$cod_seccion);
//$registros = $soapclient->call('consultarseccion',$parametros);

$soapclient3 = new soapclient($urlservices.'ver_cod_bloque.php');
$registros3 = $soapclient3->call('ver_descripcion',$parametros);



//echo $soapclient->request; // shows XML of SOAP request
//echo $soapclient->response; //shows XML of SOAP response 
	foreach ($registros3 as $info) {
	$cod_bloque=$info["codigo_bloque"];
	
  verelementos_acueducto($cod_bloque, $idformulario, $nvomod);
		  }
		  echo "</table>";
		  
}

function verelementos4($cod_bloque, $idformulario, $nvomod)
{
global $urlservices;
$soapclient = new soapclient($urlservices.'ver_elementos_acue.php');
$parcod_bloque = $cod_bloque;
$parindtitulo="S";
$parnivel="3";
$parametros = array('parcod_bloque'=>$parcod_bloque, 'parindtitulo'=>$parindtitulo, 'parinivel'=>$parnivel);
$titulos = $soapclient->call('ver_elementos',$parametros);
//echo $soapclient->request; // shows XML of SOAP request
//echo $soapclient->response; //shows XML of SOAP response 
 foreach ($titulos as $titulo) {
	$idelem=$titulo["id_elem"];
	$numpresent=$titulo["numpresent"];
	$descripcion=$titulo["descripcion"];
	$descripcion=utf8_encode($descripcion);

	$clase=$titulo["clase"];
  }

  $nombloque="bloque".$cod_bloque;
  $funcion_mostrar="mostrar_bloque('".$nombloque."')";
  $funcion_ocultar="ocultar_bloque('".$nombloque."')";


  echo "<table width='950' cellspacing='1' cellpadding='0' align='center'>";
  echo "</tr>";
  echo "<td width='5%' class='".$clase."' ></td>";
    $hreftitulo="mod_elemento.php?idtabla=".$idelem;

  echo "<td width='95%' class='".$clase."'><a href='".$hreftitulo."'>".$descripcion."</a></td>";

  echo "<td>";
  echo '<input type="button" onclick="'.$funcion_mostrar.'" value="+" class="botones"/>';
  echo "</td>";
  echo "<td>";
  echo '<input type="button" onclick="'.$funcion_ocultar.'" value="-" class="botones"/>';
  echo "</td>";


  echo "</tr>";
  echo "</table>";

  echo "<div id='".$nombloque."' style='display:none'>";

$soapclient2 = new soapclient($urlservices.'ver_elementos_acue.php');
$parcod_bloque = $cod_bloque;
$parindtitulo="N";
$parnivel="3";
$parametros = array('parcod_bloque'=>$parcod_bloque, 'parindtitulo'=>$parindtitulo, 'parinivel'=>$parnivel);
$renglones = $soapclient2->call('ver_elementos',$parametros);
//echo $soapclient->request; // shows XML of SOAP request
//echo $soapclient->response; //shows XML of SOAP response 

 foreach ($renglones as $renglon) {
	$numpresent=$renglon["numpresent"];
	if ($numpresent==" ")
	{
		$numpresent="&nbsp";
	}
	$idelemento=$renglon["id_elem"];
	$descripcion=$renglon["descripcion"];
	$descripcion=utf8_encode($descripcion);
	$anchodesc=$renglon["anchodesc"]."%";
	$clase=$renglon["clase"];
	$ndatosasoc=$renglon["ndatosasoc"];
	$ntitasoc=$renglon["ntitasoc"];
	$iddicc1=$renglon["iddicc1"];
	$iddicc2=$renglon["iddicc2"];
	$iddicc3=$renglon["iddicc3"];
	$iddicc4=$renglon["iddicc4"];
	$titasoc1=$renglon["titasoc1"];
	$titasoc2=$renglon["titasoc2"];
	$titasoc3=$renglon["titasoc3"];
	$titasoc4=$renglon["titasoc4"];


    $hreftabla="mod_elemento.php?idtabla=".$idelemento;

  echo "<table width='950' cellspacing='1' cellpadding='0' align='center'>";
  echo "<tr>";

  echo "<td width='5%' class='".$clase."' ></td>";
   
  echo "<td  class='".$clase."' width='".$anchodesc."'><a href='".$hreftabla."'>".$descripcion."</a></td>";

  if ($ntitasoc > 0)
  {
	for ( $j = 1; $j <= $ntitasoc; $j += 1) {
		$tituloasoc="${titasoc.$j}";
		echo "<td class='".$clase."' >".$tituloasoc." </td>";
	}

  }
  if ($ndatosasoc > 0)
  {
	for ( $j = 1; $j <= $ndatosasoc; $j += 1) {
		$parid="${iddicc.$j}";
		$soapclient3 = new soapclient($urlservices.'ver_diccionario_datos.php');
		$parametros = array('parid'=>$parid);
		$datosdic = $soapclient3->call('ver_diccionario',$parametros);

		 foreach ($datosdic as $datodic) {
			$idpresent=$datodic["idpresent"];
			$tipodato=$datodic["tipodato"];
			$idtabla=$datodic["idtabla"];
			$longdato=$datodic["longdato"];
		 } 
		$nombrecampo=$parid."_0_0_datof";
		echo "<td class='".$clase."' >";
		if ($idpresent=="1")
		{
			veropcionestabla($idtabla, $nombrecampo);
		}
		if ($idpresent=="2")
		{

 	      		$htmlinput='<input name="'.$nombrecampo.'" type="text" id="'.$nombrecampo.'"/>';
			echo $htmlinput;                     
                     
		}

		if ($idpresent=="3")
		{

 	      		$htmlinput='<textarea name="'.$nombrecampo.'"  id="'.$nombrecampo.'" cols="40" rows="3"></textarea>';
			echo $htmlinput;                     
                     
		}
		echo "</td>";
	}

  }
   // echo '<td width="20px"><a href="'.$hreftabla.'">mod</a></td>';

  echo "</tr>";
  echo "</table>";
  }
  echo "</div>";



}

function verseccion4($cod_seccion)
{
require_once('soap/lib/nusoap.php');
global $urlservices;
$soapclient = new soapclient($urlservices.'ver_secciones.php');
$parametros = array('parseccion'=>$cod_seccion);
$registros = $soapclient->call('consultarseccion',$parametros);

$soapclient3 = new soapclient($urlservices.'ver_cod_bloque.php');
$registros3 = $soapclient3->call('ver_descripcion',$parametros);

//echo $soapclient->request; // shows XML of SOAP request
//echo $soapclient->response; //shows XML of SOAP response 
	foreach ($registros3 as $info) {
	$cod_bloque=$info["codigo_bloque"];
	
  verelementos4($cod_bloque, $idformulario, $nvomod);
		  }
		  echo "</table>";
		  
}



?>