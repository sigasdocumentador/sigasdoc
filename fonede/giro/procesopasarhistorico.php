<html>
<meta http-equiv="Pragma"content="no-cache">
<meta http-equiv="expires"content="0">
<head> 
<title>Proceso de Pasar Hist&oacute;rico</title>
<style>
.progress-label {
float: left;
margin-left: 35%;
margin-top: 5px;
font-weight: bold;
text-shadow: 1px 1px 0 #fff;
}
</style>

<link type="text/css" href="../../css/css.1.8/jquery-ui-1.8.17.custom.css" rel="stylesheet" />
<script language="javascript" src="../../js/1.6/jquery-1.6.2.min.js"></script>
<script language="javascript" src="../../js/1.6/jquery-ui-1.8.16.custom.min.js"></script>


<script>

function setCookie(c_name,value,exdays)
{
	var exdate=new Date();
	exdate.setDate(exdate.getDate() + exdays);
	var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
	document.cookie=c_name + "=" + c_value;
}// fin setCookie

// funcion para obtener cookie en javascript
	function getCookie(c_name)
	{
		var i,x,y,ARRcookies=document.cookie.split(";");
		for (i=0;i<ARRcookies.length;i++)
		{
  			x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
  			y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
  			x=x.replace(/^\s+|\s+$/g,"");
  			if (x==c_name)
    			{
    				return unescape(y);
    			}// fin if
		}// fin for
  	}// fin getCookie
//funcion para obtener cookie en javascript
</script>


 <script> 
	$(function() {
		
if(confirm('Realmente desea pasar a Historico?')){
    document.getElementById("progressbar").style.display="block";
	
	
	$("#pasarhistorico").load('PasarHistorico.php');	
		
    var tiempofinalhistorico= getCookie("tiempofinalhistorico");
  	
	var progressbar = $( "#progressbar" ),
	progressLabel = $( ".progress-label" );
	progressbar.progressbar({
	value: false,
	change: function() {
	progressLabel.text( progressbar.progressbar( "value" ) + "%" );
	},
	complete: function() {
	progressLabel.text("Finalizado");	
	}
	});
	function progress() {
	var val = progressbar.progressbar( "value" ) || 0;
	progressbar.progressbar( "value", val + 1 );
	if ( val < 99 ) {
	setTimeout( progress, tiempofinalhistorico );
	}
	if(val==99)
	{
		document.getElementById('resultado').style.display='block';
	}
	}
	setTimeout( progress, tiempofinalhistorico );
	
	
	$( "#dialog-message" ).dialog({
		modal: true,	
		width: 350,	
		buttons: {
	Ok: function() {
			if(document.getElementById('resultado').style.display=='block')	
			{
				$( this ).dialog( "close" );
				location.href='Giro.php';
            }
		}
	 },
    show: {
		effect: "blind",
		duration: 1000
	},
	 hide: {
		effect: "explode",
		duration: 1000
		},
	 open: function() {
          //Hide closing "X" for this dialog only.
          $(this).parent().children().children("a.ui-dialog-titlebar-close").remove();
    }	
	 });
	
	
	
}// fin confirm
else
{	
	location.href='Giro.php';
}
	
	});
</script>

</head>
<body>
<div id="pasarhistorico"></div>
<div id="dialog-message" title="Proceso de Pasar Hist&oacute;rico" style=" width:100%">

<div id="progressbar" style="display:none"><div class="progress-label">Loading...</div></div>

<div id="resultado" style="display:none">
<p>
<span class="ui-icon ui-icon-circle-check" style="float: left; margin: 0 7px 50px 0;"></span>
Proceso Finalizado...
</p>
</div>
</div>
</body>
</html>


