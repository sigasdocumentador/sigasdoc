<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
	
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Listar Asignaci&oacute;n</title>

<link type="text/css" href="../../css/css.1.8/jquery-ui-1.8.17.custom.css" rel="stylesheet" />
<script language="javascript" src="../../js/1.6/jquery-1.6.2.min.js"></script>
<script language="javascript" src="../../js/1.6/jquery-ui-1.8.16.custom.min.js"></script>

<link href="../../css/Estilos.css" rel="stylesheet" type="text/css" />
<link type="text/css" href="../../css/marco.css" rel="stylesheet"/>

<script languaje="javascript" src="js/Asignacion.js"></script>
<script type="text/javascript" src="../../js/comunes.js"></script>

<script>
function generareporteasignado(id) {

		var page = '../pdf/pdf_reporte_Asignado.php?ok=ok&variable_asignado='+id;
		//window.location.href=page;
		window.open(page,'_blank');			
        //location.href='pdf/pdf_reportes_Reteica.php?id_formulario='+id+'&estado=2';
}
</script>


<script>
function verasignados(total,vinculacion_asignado,idapropiacion,seccional,cantidad,cantidad2){

var page = 'ListarAsignacion.php?total='+total
		 +'&conosinvinculacion='+conosinvinculacion		
		 +'&idapropiacion='+idapropiacion
		 +'&seccional='+seccional
		 +'&cantidad='+cantidad	
		 +'&cantidad2='+cantidad2		 
		 +'&ok=ok';
         window.location.href=page;   	
	
}//fin function verasignados(){ 
</script>       
  
</head>

<body>
<p>
  <?php
ini_set('display_errors','Off');
date_default_timezone_set('America/Bogota');
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$url=$_SERVER['PHP_SELF'];
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'auditoria.php';
auditar($url);

include_once '../../rsc/pdo/IFXDbManejador.php';
include_once '../../rsc/pdo/IFXerror.php';

$db = IFXDbManejador::conectarDB(); 
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();	
}

session_start();
$usuario=$_SESSION['USUARIO'];

$sql="SELECT * FROM aportes500";
$rs_seccional=$db->querySimple($sql);

?>
          <?php
           $sql2="SELECT aportes222.idapropiacion,aportes222.descripcion,aportes222.totalsubsidiosinicial,aportes222.totalfinal,aportes222.vinculacion,aportes222.totalsubsidiosfinal
                          ,aportes223.descripcionperiodo,aportes222.vigencia
                  FROM aportes222,aportes223
                  where 1=1 
                  and aportes222.idperiodo=aportes223.idperiodo
				  and aportes222.totalsubsidiosinicial IS NOT NULL
				  and aportes222.totalsubsidiosfinal!=0 ";
        	$rs2=$db->querySimple($sql2);				
        ?>
              
          <?php
		         
       if ($_GET['ok'] == 'ok') {
		            
            $total=$_GET['total'];								
			$conosinvinculacion=$_GET['conosinvinculacion'];
			$idapropiacion=$_GET['idapropiacion'];
			$seccional=$_GET['seccional'];
			$cantidad=$_GET['cantidad'];
			$cantidad2=$_GET['cantidad2'];
			
			$totalactualizar=$total-$cantidad2;			
			
			if($cantidad2<=$total)
			{
					
			$sqlap221="select  top ".$cantidad." * from aportes221 where (estado='R' or estado='X' or estado='P')
				   and proceso='Po' 
				   and vinculacion='".$conosinvinculacion."'
            	   and idagencia='".$seccional."'";					
			$rsap221=$db->querySimple($sqlap221);
			//echo "sql= ".$sqlap221."<br>";
			$contar=0;					
			
			while($rowap221=$rsap221->fetch()){	
			      ++$contar;	
				  $proceso = "asignacion";			  
				  $idpersona=$rowap221['idpersona'];
				  				  			  
				  include_once '../grabacion/validar.php';	
				  
				  $sqlap221idap="SELECT COUNT(idapropiacion) FROM aportes221 where idapropiacion='".$idapropiacion."' ";
				  $rsap221idap=$db->querySimple($sqlap221idap);
				  $rowap221idap=$rsap221idap->fetchColumn();
				  				  
				  include_once '../grabacion/ActualizarEstados.php';				 
				  echo "<script>
				            var page = 'ListarAsignacion.php?total='+".$total."
									 +'&conosinvinculacion='+".$conosinvinculacion."
									 +'&idapropiacion='+".$idapropiacion."
            						 +'&seccional='+'".$seccional."'
									 +'&cantidad='+".($cantidad - 1)."
									 +'&cantidad2='+".$cantidad2."
									 +'&ok=ok';
									 window.location.href=page;   	
				  </script>";										 
			  }	// fin while		
				  
		   	$sqlAP221="select * from aportes221 where estado='R' 
				   and proceso='Po' 
				   and vinculacion='".$conosinvinculacion."'
            	   and idagencia='".$seccional."'";					
			$rsAP221=$db->querySimple($sqlAP221);
			while($rowAP221=$rsAP221->fetch()){	
			$idpersonaAc=$rowAP221['idpersona'];			 		
			
			$sql_update="update aportes221 set estado='P',usuario='$usuario',fechasistema= cast(GetDate() as date)
			where idpersona = '".$idpersonaAc."'";
			$rs_update=$db->queryActualiza($sql_update,'aportes221');					
			}			
								          
            $sql = "select aportes221.idradicacion,aportes221.estado,aportes221.fecharadicacion,aportes221.vinculacion,aportes015.identificacion,aportes015.pnombre,aportes015.snombre,aportes015.papellido
                           ,aportes015.sapellido,aportes015.sexo,aportes221.estado,aportes221.idagencia,aportes221.apto,aportes221.idpersona
                           ,aportes220.valoralimentacion,aportes220.valorsalud,aportes220.valoreducacion,aportes222.idapropiacion,aportes222.totaldisponible,aportes222.totalfinal,aportes222.salariominimo
						   ,aportes500.agencia 	 
                    from   aportes221,aportes015,aportes220,aportes222,aportes500  
                    where 1=1			
                    and aportes015.idpersona=aportes221.idpersona 
                    and aportes221.idconvenio=aportes220.idconvenio                    
					and aportes221.vinculacion=aportes222.vinculacion
					and aportes221.idagencia=aportes500.codigo 
					and aportes221.vinculacion='".$conosinvinculacion."'
					and aportes221.idagencia='".$seccional."'
                    and aportes221.estado='A'";	       
                       
            $sql = $sql . " order by especial DESC, idradicacion ASC";			
						
            $rs=$db->querySimple($sql);
			$rs1=$db->querySimple($sql);
			
			//echo'<br>sql= '.$sql; 			
			$encontrodatos=0;			
			while($registro1=$rs1->fetch()){	
			++$encotrodatos;	 
			}	     
			
if($encotrodatos!=0)
{
			 
					
			if($conosinvinculacion==1)
			{ 	
				$tituloconosin="SIN VINCULACI&Oacute;N";
			}
			else
			if($conosinvinculacion==2)
			{
				$tituloconosin="CON VINCULACI&Oacute;N";
			}
            
        ?>
</p>
<table height="172" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="29" class="arriba_iz">&nbsp;</td>
    <td class="arriba_ce"><span class="letrablanca">::&nbsp;<strong>LISTA DE ASIGNADOS A FONEDE <?php echo $tituloconosin;?></strong>&nbsp;::</span></td>
    <td class="arriba_de" >&nbsp;</td>
  </tr>
  <tr>
    <td class="cuerpo_iz">&nbsp;</td>
    <td class="cuerpo_ce"><a href="ListarAsignacion.php" title="Volver"><img src="../imagenes/left_green.png" width="20" height="20" /></a><a href="../Excel/Excel_ReporteAsignado.php?ok=ok&amp;total=<?php echo $_GET['total'];?>&amp;
     conosinvinculacion=<?php echo $_GET['conosinvinculacion'];?>&amp;       
     idapropiacion=<?php echo $_GET['idapropiacion'];?>&amp; 
     seccional=<?php echo $_GET['seccional'];?>&amp; 
     cantidad=<?php echo $_GET['cantidad'];?>&amp; 
     cantidad2=<?php echo $_GET['cantidad2'];?>&amp;      
     " target="_blank" title="Excel"><img src="../imagenes/excel.png" width="20" height="20" /></a>
      <?php $variable_asignado =$_GET['total'].'*'.$_GET['conosinvinculacion'].'*'.$_GET['idapropiacion'].'*'.$_GET['seccional'].'*'.$_GET['cantidad'].'*'.$_GET['cantidad2'];?>
    <img src="../imagenes/pdf.png" width="20" height="20" id="<?php echo $variable_asignado; ?>" title="Generar Pdf" onclick="generareporteasignado(this.id);"/></td>
    <td class="cuerpo_de">&nbsp;</td>
  </tr>
  <tr>
    <td class="cuerpo_iz">&nbsp;</td>
    <!-- TABLAS DE FORMULARIO -->
    <td class="cuerpo_ce"><center>
      <table border="0" cellspacing="0" class="tablero">
        <tr>
          <td><center>
            <table class="tablero">
              <tr>
                <th align="center">conse</th>
                <th align="center">Radicado</th>
                <th align="center">Identificaci&oacute;n</th>
                <th align="center">Nombre y Apellido</th>
                <th align="center">Seccional</th>
                <th align="center">Estado</th>
                </tr>
              <?php 
           
         $cont_asignado=0;		
         while($registro=$rs->fetch()){
         ++$cont_asignado;
         $persona=$registro['idpersona'];
         $apropia=$registro['idapropiacion'];		 		 		       
		 		      
			//actuliza en fonede el idapropiacion		  
            $sql_apropiacion="update aportes221 set idapropiacion=".$apropia.",usuario='$usuario',fechasistema= cast(GetDate() as date) where idpersona=".$persona."";
            $rs_apropiacion=$db->queryActualiza($sql_apropiacion,'aportes221');	
     
			//actuliza apropiacion en cursos		  
            $sqlap226="update aportes226 set idapropiacion=".$apropia.",usuario='$usuario',fechasistema= cast(GetDate() as date) where idpersona=".$persona."";
            $rsap226=$db->queryActualiza($sqlap226,'aportes226');	
	
			//inserta campos dependiendo el numero de cuotas
			
			$sqlap225="select * from aportes225 where idpersona=".$persona."";
			$rsap225=$db->querySimple($sqlap225,'aportes225');	
			
     		while($rowap225=$rsap225->fetch()){
				$idpersonagiro=$rowap225['idpersona'];			
			}
			
			if($idpersonagiro!=$persona)
			{
				for ($i=0; $i<=5; $i++){								
						$sql_numerocuotas="insert into aportes225 (idpersona,usuario,fechasistema) values('".$persona."','".$usuario."',cast(GetDate() as date))";
						$rs_numerocuotas=$db->queryInsert($sql_numerocuotas,'aportes225');	
							
						}// fin for
			 }
           	?>
              <tr>
                <td align="center"><?php echo $cont_asignado; ?> &nbsp;</td>
                <td align="center"><?php echo $registro['idradicacion']; ?>&nbsp;</td>
                <td align="center"><?php echo $registro['identificacion']; ?>&nbsp;</td>
                <td align="center"><?php echo utf8_encode($registro['pnombre']." ".$registro['snombre']." ".$registro['papellido']." ".$registro['sapellido']); ?>&nbsp;</td>
                <td align="center"><?php echo $registro['agencia']; ?>&nbsp;</td>
                <td align="center"><?php echo $registro['estado']; ?>&nbsp;</td>
                </tr>
              <?php
                }//fin while			
            ?>
              </table>
            </center></td>
        </tr>
      </table>
    </center></td>
    <td class="cuerpo_de"></td>
    <!-- FONDO DERECHA -->
  </tr>
  <tr>
    <td height="41" class="abajo_iz">&nbsp;</td>
    <td class="abajo_ce">&nbsp;</td>
    <td class="abajo_de">&nbsp;</td>
  </tr>
</table>
<div align="center"></div>    
        <?php
		
		if($totalactualizar>=0)
		{				
		$sqlap222="update aportes222 set totalsubsidiosfinal='".$totalactualizar."',usuario='$usuario',fechasistema= cast(GetDate() as date) where idapropiacion='".$idapropiacion."'";
        $rsap222=$db->queryActualiza($sqlap222,'aportes222');
		}
		
		}// fin encontrodatos
		else
		{
			if($conosinvinculacion==1)
			{
				echo "<script language='JavaScript'>alert('No se Encontraron Postulados Sin Vinculacion Para Asignar'); location.href='ListarAsignacion.php'</script>";
			}
			else
			if($conosinvinculacion==2)
			{
				echo "<script language='JavaScript'>alert('No se Encontraron Postulados Con Vinculacion Para Asignar'); location.href='ListarAsignacion.php'</script>";
			}			
		}
		}// fin if
		else
		{
			echo "<script language='JavaScript'>alert('La cantidad de subsidio ingresado es mayor a la de la apropiacion disponible'); location.href='ListarAsignacion.php'</script>";
		}		

      }//fin if $_POST['ok'] == 'ok'
	  
      else {
        ?>         		      
        <!--<div  id="divpostulados">-->
        <form name="form_asignacion" method="post" action="">
          <table border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td height="29" class="arriba_iz">&nbsp;</td>
              <td class="arriba_ce"><span class="letrablanca">::&nbsp;<strong>ASIGNACI&Oacute;N</strong>&nbsp;::</span></td>
              <td class="arriba_de" >&nbsp;</td>
            </tr>
            <tr>
              <td class="cuerpo_iz">&nbsp;</td>
              <td class="cuerpo_ce"><div id="error" style="color:#FF0000"></div></td>
              <td class="cuerpo_de">&nbsp;</td>
            </tr>
            <tr>
              <td class="cuerpo_iz">&nbsp;</td>
              <!-- TABLAS DE FORMULARIO -->
              <td class="cuerpo_ce"><center>
                <table width="367" border="0" cellspacing="0" class="tablero">
                  <tr bgcolor="#EBEBEB">
                    <td colspan="4" style="text-align:center" ></td>
                  </tr>
                  <tr bgcolor="#EBEBEB">
                    <td colspan="4" style="text-align:center" ><strong>ASIGNAR APROPIACI&Oacute;N DEL ULTIMO MES</strong></td>
                  </tr>
                  <tr>
                    <td><strong>Descripcion:</strong></td>
                    <td colspan="3"><select name="vinculacion_asignado" size="1" id="vinculacion_asignado" class="boxlargo" onchange="buscarPresupuestoFinal();">
                      <option value="" selected="selected">::Seleccione::</option>
                      <?php
                   while($row=$rs2->fetch()){					   
                         if ($row['vinculacion'] == 1) {
                          echo "<option value=".$row['totalsubsidiosfinal']."*".$row['vinculacion']."*".$row['idapropiacion'].">".$row['descripcion']."/".$row['descripcionperiodo']."/".$row['vigencia']."/Sin Vinculaci&oacute;n/".$row['totalsubsidiosfinal']."</option>";
                         }
                          else  {
                          echo "<option value=".$row['totalsubsidiosfinal']."*".$row['vinculacion']."*".$row['idapropiacion'].">".$row['descripcion']."/".$row['descripcionperiodo']."/".$row['vigencia']."/Con Vinculaci&oacute;n/".$row['totalsubsidiosfinal']."</option>";			  
                         }
                   }
                   ?>
                    </select></td>
                  </tr>
                  <tr>
                    <td><strong>Seccional:</strong></td>
                    <td colspan="3"><select name="seccional" size="1" id="seccional" class="box1">
                      <option value="">::Seleccione::</option>
                      <?php
						while($row=$rs_seccional->fetch()){
						
						  echo "<option value=".$row['codigo'] ." >".$row['agencia']."</option>";
						}
					  ?>
                    </select></td>
                  </tr>
                  <tr>
                    <td><strong>Cantidad Subsidios:</strong></td>
                    <td colspan="3"><input name="cantidad" type="text" id="cantidad"
               onkeydown='solonumeros(this);' onkeyup='solonumeros(this);'/></td>
                  </tr>
                  <tr>
                    <td><strong>Presupuesto Final:</strong></td>
                    <td colspan="3"><strong><label id="presupuestofinal"></label></strong></td>
                  </tr>
                  <tr>
                    <td colspan="4">&nbsp;<center>
                      <input name="button" id="button"  type="button" onclick="if(confirm('&iexcl; Recuerde que antes de cada Asignaci&oacute;n debe hacer los cruces ante Fosyga y Asocajas !')){listarasignados();}" value="Generar Asignados" />
                    </center></td>
                  </tr>
                </table>
              </center></td>
              <td class="cuerpo_de"></td>
              <!-- FONDO DERECHA -->
            </tr>
            <tr>
              <td height="41" class="abajo_iz">&nbsp;</td>
              <td class="abajo_ce">&nbsp;</td>
              <td class="abajo_de">&nbsp;</td>
            </tr>
          </table>
</form>
      <?php
      }//fin else if $_POST['ok'] == 'ok'
      ?>
</body>
</html>