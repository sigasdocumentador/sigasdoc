// JavaScript Document
function numberFormat(numero){ 
        // Variable que contendra el resultado final
        var resultado = "";
        
        // Si el numero empieza por el valor "-" (numero negativo)
        if(numero[0]=="-")
        {
            // Cogemos el numero eliminando los posibles puntos que tenga, y sin
            // el signo negativo
            nuevoNumero=numero.replace(/\./g,'').substring(1);
        }else{
            // Cogemos el numero eliminando los posibles puntos que tenga
            nuevoNumero=numero.replace(/\./g,'');
        }
        
        // Si tiene decimales, se los quitamos al numero
        if(numero.indexOf(",")>=0)
            nuevoNumero=nuevoNumero.substring(0,nuevoNumero.indexOf(","));

        // Ponemos un punto cada 3 caracteres
        for (var j, i = nuevoNumero.length - 1, j = 0; i >= 0; i--, j++) 
            resultado = nuevoNumero.charAt(i) + ((j > 0) && (j % 3 == 0)? ".": "") + resultado; 
        
        // Si tiene decimales, se lo a�adimos al numero una vez forateado con 
        // los separadores de miles
        if(numero.indexOf(",")>=0)
            resultado+=numero.substring(numero.indexOf(","));

        if(numero[0]=="-")
        {
            // Devolvemos el valor a�adiendo al inicio el signo negativo
            return "-"+resultado;
        }else{
            return resultado;
        }
    }
	
function buscardescripcionapropiacion(){
		
		if($("#descripcion_apropiaciones").val()=='')	{
			alert("Seleccione la descripcion");
			limpiarCampos();			
     		return
		}
		
		var descripapropiaciones = $("#descripcion_apropiaciones").val();
		$.getJSON('buscarApropiacionTotal.php',{v0:descripapropiaciones},function(datos){
			if (datos==0){
				alert("Datos no encontrados");
				limpiarCampos();
				return false;
			}
			$.each(datos,function(i,fila){				
				
				$("#totaldisponible").val(fila.totaldisponible);
				$("#salariominimo").val(fila.salariominimo);															
				$("#idapropiacion").val(fila.idapropiacion);				
				return;
			});
		})
		
	} // fin buscardescripcionapropiacion

function buscarPresupuestoFinal(){
		
		var descripcion = $("#vinculacion_asignado").val();		
		document.getElementById('presupuestofinal').innerHTML='';
		
		arreglo = descripcion.split('*');
	        var totalsubcidios=arreglo[0];
			var vinculacion=arreglo[1];
			var idaporpiacion=arreglo[2];
			
		$.getJSON('buscarPresupuestoFinal.php',{v0:idaporpiacion},function(datos){		
			$.each(datos,function(i,fila){				
				document.getElementById('presupuestofinal').innerHTML=numberFormat(fila.totalfinal);				
				return;
			});
		})
		
	} // fin buscarPresupuestoFinal
	
function actualizartotalusuarios(){
	
	$("table.tablero :input.ui-state-error,select.ui-state-error").removeClass("ui-state-error");//Limpiar la clase de error
	var descripcionapropiaciones=$("#descripcion_apropiaciones").val();
	if($("#descripcion_apropiaciones").val()=='0')	{
		$("#descripcion_apropiaciones").addClass("ui-state-error");
		error++;
		}// fin if 

	var totalusuarios=$("#totalusuarios").val();
	var idapropiacion=$("#idapropiacion").val();	
	var totalfinal=$("#totalfinal").val();
	
	if(descripcionapropiaciones!=0)
	{
		$.getJSON('actualizartotalusuarios.php',{v0:totalusuarios,v1:idapropiacion,v2:totalfinal},function(datos){
			
			if(datos==0)
			{
				alert("No se pudo guardar los datos");
			
			}// fin if
			if(datos>0)
			{			
				alert("Registro guardado con un total de : " +totalusuarios+ " Subsidios");				
			}// fin if
					
		})
	}// fin if
	else
		{
			alert("Falta Completar Datos");
		}// fin else
}// fin actualizartotalusuarios	

function listarasignados(){
	
	$("table.tablero :input.ui-state-error,select.ui-state-error").removeClass("ui-state-error");//Limpiar la clase de error
	
	if($("#vinculacion_asignado").val()=='')	{
		$("#vinculacion_asignado").addClass("ui-state-error");
		error++;
		}// fin if
	
	if($("#seccional").val()=='')	{
		$("#seccional").addClass("ui-state-error");
		error++;
		}// fin if
		
	if($("#cantidad").val()=='')	{
		$("#cantidad").addClass("ui-state-error");
		error++;
		}// fin if
		
	var apropiacion=document.getElementById("vinculacion_asignado").value;
	var seccionalpostulado=document.getElementById("seccional").value;	
	var cantidad=document.getElementById("cantidad").value;
	var cantidad2=document.getElementById("cantidad").value;
	
	arreglo = apropiacion.split('*');
	total=arreglo[0];	
	conosinvinculacion=arreglo[1];
	idapropiacion=arreglo[2];
	
	//alert (conosinvinculacion);
	//alert (total);
	//if(nom.length==0) return false;
	if(apropiacion!='' && seccionalpostulado!='' && cantidad!='')
	{		
  		verasignados (total,conosinvinculacion,idapropiacion,seccionalpostulado,cantidad,cantidad2);
	}
}// fin listarasignados