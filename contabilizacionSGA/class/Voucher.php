<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Voucher
 *
 * @author nuevastic
 */
include ("UserReservedData.php");
include ("VoucherLineKey.php");
class Voucher {
    public $payeeEntityTaxId;
	public $paymentTermsCode;
    public $glOffsetCode;
    public $amountTaxableDomestic;
    public $statusCodePay;
    public $taxRateAreaCode;
    public $taxExplanationCode;
    public $voucherLineKey;
    public $userReservedData;
/**
 * 
 * @return String
 */
    function getPayeeEntityTaxId() {
        return $this->payeeEntityTaxId;
    }
/**
 * 
 * @return String
 */
    function getPaymentTermsCode() {
        return $this->paymentTermsCode;
    }
/**
 * 
 * @return String
 */
    function getGlOffsetCode() {
        return $this->glOffsetCode;
    }
/**
 * 
 * @return double
 */
    function getAmountTaxableDomestic() {
        return $this->amountTaxableDomestic;
    }
/**
 * 
 * @return String
 */
    function getStatusCodePay() {
        return $this->statusCodePay;
    }
/**
 * 
 * @return String
 */
    function getTaxRateAreaCode() {
        return $this->taxRateAreaCode;
    }
/**
 * 
 * @return String
 */
    function getTaxExplanationCode() {
        return $this->taxExplanationCode;
    }
/**
 * 
 * @return VoucherLineKey
 */
    function getVoucherLineKey() {
        return $this->voucherLineKey;
    }
/**
 * 
 * @return UserReservedData;
 */
    function getUserReservedDdata() {
        return $this->userReservedData;
    }
/**
 * 
 * @param String $paymentTermsCode
 */
    function setPayeeEntityTaxId	($payeeEntityTaxId) {
        $this->payeeEntityTaxId	= $payeeEntityTaxId;
    }
/**
 * 
 * @param String $paymentTermsCode
 */
    function setPaymentTermsCode($paymentTermsCode) {
        $this->paymentTermsCode = $paymentTermsCode;
    }
/**
 * 
 * @param String $glOffsetCode
 */
    function setGlOffsetCode($glOffsetCode) {
        $this->glOffsetCode = $glOffsetCode;
    }
/**
 * 
 * @param double $amountTaxableDomestic
 */
    function setAmountTaxableDomestic($amountTaxableDomestic) {
        $this->amountTaxableDomestic = $amountTaxableDomestic;
    }
/**
 * 
 * @param String $statusCodePay
 */
    function setStatusCodePay($statusCodePay) {
        $this->statusCodePay = $statusCodePay;
    }
/**
 * 
 * @param String $taxRateAreaCode
 */
    function setTaxRateAreaCode($taxRateAreaCode) {
        $this->taxRateAreaCode = $taxRateAreaCode;
    }
/**
 * 
 * @param String $taxExplanationCode
 */
    function setTaxExplanationCode($taxExplanationCode) {
        $this->taxExplanationCode = $taxExplanationCode;
    }
/**
 * 
 * @param VoucherLineKey $voucherLineKey
 */
    function setVoucherLineKey($voucherLineKey) {
        $this->voucherLineKey = $voucherLineKey;
    }
/**
 * 
 * @param UserReservedData $userReservedDdata
 */
    function setUserReservedDdata($userReservedDdata) {
        $this->userReservedData = $userReservedDdata;
    }


}
