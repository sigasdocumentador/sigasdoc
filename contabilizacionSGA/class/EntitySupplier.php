<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EntitySupplier
 *
 * @author nuevastic
 */
class EntitySupplier {
   
   public  $entityTaxld;
   
   /**
    * 
    * @param String $entityTaxld
    */
   function __construct($entityTaxld="") {
       $this->entityTaxld = $entityTaxld;
   }

   
/**
 * 
 * @return String
 */
   function getEntityTaxld() {
       return $this->entityTaxld;
   }

  
/**
 * 
 * @param String $entityTaxld
 */
   function setEntityTaxld($entityTaxld) {
       $this->entityTaxld = $entityTaxld;
   }



}
