<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PurchaseOrderKey
 *
 * @author nuevastic
 */
class PurchaseOrderKey {
   public $documentNumber;
/**
 * 
 * @param int $documentNumber
 */
   function __construct($documentNumber="") {
       $this->documentNumber = $documentNumber;
   }
/**
 * 
 * @return int
 */
   function getDocumentNumber() {
       return $this->documentNumber;
   }
/**
 * 
 * @param int $documentNumber
 */
   function setDocumentNumber($documentNumber) {
       $this->documentNumber = $documentNumber;
   }



}
