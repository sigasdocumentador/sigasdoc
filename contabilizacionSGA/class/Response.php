<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include("Error.php");
/**
 * Description of Response
 *
 * @author nuevastic
 */
class Response {
    //private $result;
    //private $error;
	private $processedHeader;
	
	function __construct($result=null,$error=null,$processedHeader=null){
		$this->result=$result;
		$this->error=$error;
		$this->processedHeader=$processedHeader;
	}
	
    function getResult() {
        return $this->result;
    }

    function getError() {
        return $this->error;
    }

	function getProcessedHeader() {
        return $this->processedHeader;
    }
	
    function setResult($result) {
        $this->result = $result;
    }

    function setError($error) {
        $this->error = $error;
    }

	function setProcessedHeader($processedHeader) {
        $this->processedHeader = $processedHeader;
    }
}
