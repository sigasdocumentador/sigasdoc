<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of GlDistribution
 *
 * @author nuevastic
 * @
 */
class GlDistribution {
    public $documentLineNumber;
    public $amountDomestic;
    public $objectAccount;
    public $subsidiary;
    /**
     * 
     * @param int $documentLineNumber
     * @param double $amountDomestic
     * @param String $objectAccount
     * @param String $subsidiary
     */
    function __construct($documentLineNumber="", $amountDomestic="", $objectAccount="", $subsidiary="") {
        $this->documentLineNumber = $documentLineNumber;
        $this->amountDomestic = $amountDomestic;
        $this->objectAccount = $objectAccount;
        $this->subsidiary = $subsidiary;
    }
    /**
     * 
     * @return int
     */
    function getDocumentLineNumber() {
        return $this->documentLineNumber;
    }
    /**
     * 
     * @return double
     */
    function getAmountDomestic() {
        return $this->amountDomestic;
    }
    /**
     * 
     * @return String
     */
    function getObjectAccount() {
        return $this->objectAccount;
    }
    /**
     * 
     * @return String
     */
    function getSubsidiary() {
        return $this->subsidiary;
    }
    /**
     * 
     * @param int $documentLineNumber
     */
    function setDocumentLineNumber($documentLineNumber) {
        $this->documentLineNumber = $documentLineNumber;
    }
    /**
     * 
     * @param double $amountDomestic
     */
    function setAmountDomestic($amountDomestic) {
        $this->amountDomestic = $amountDomestic;
    }
    /**
     * 
     * @param String $objectAccount
     */
    function setObjectAccount($objectAccount) {
        $this->objectAccount = $objectAccount;
    }
    /**
     * 
     * @param String $subsidiary
     */
    function setSubsidiary($subsidiary) {
        $this->subsidiary = $subsidiary;
    }



}
