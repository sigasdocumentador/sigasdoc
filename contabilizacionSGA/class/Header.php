<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of request
 *@var VoucherDetail[] $voucherDetail;
 * @author nuevastic
 */
include ("VoucherDetail.php");
include ("EntitySupplier.php");
include ("PurchaseOrderKey.php");
include("EntityPayee.php");
class Header {
    private $bussinessUnit;
    private $company;
    private $paymentTermsCode;
    private $supplierInvoiceNumber;
    private $dateInvoice;
    private $dateAccounting;
    private $dateBatch;
    private $remark;
    private $documentTypeCode;
    private $entitySupplier;
    private $entityPayee;
    private $purchaseOrderKey;
    private $voucherDetail;
/**
 * 
 * @return String
 */ 
    function getBussinessUnit() {
        return $this->bussinessUnit;
    }
/**
 * 
 * @return String
 */
    function getCompany() {
        return $this->company;
    }
/**
 * 
 * @return String
 */
    function getPaymentTermsCode() {
        return $this->paymentTermsCode;
    }
/**
 * 
 * @return String
 */
    function getSupplierInvoiceNumber() {
        return $this->supplierInvoiceNumber;
    }
/**
 * 
 * @return String
 */
    function getDateInvoice() {
        return $this->dateInvoice;
    }
/**
 * 
 * @return String
 */
    function getDateAccounting() {
        return $this->dateAccounting;
    }
/**
 * 
 * @return String
 */
    function getDateBatch() {
        return $this->dateBatch;
    }
/**
 * 
 * @return String
 */
    function getRemark() {
        return $this->remark;
    }
/**
 * 
 * @return String
 */
    function getDocumentTypeCode() {
        return $this->documentTypeCode;
    }
/**
 * 
 * @return EntitySupplier
 */
    function getEntitySupplier() {
        return $this->entitySupplier;
    }
    function getEntityPayee() {
        return $this->entityPayee;
    }

    /**
 * 
 * @return PurchaseOrderKey
 */
    function getPurchaseOrderKey() {
        return $this->purchaseOrderKey;
    }
/**
 * 
 * @return VocheurDetail[]
 */
    function getVoucherDetail() {
        return $this->voucherDetail;
    }
/**
 * 
 * @param String $bussinessUnit
 */
    function setBussinessUnit($bussinessUnit) {
        $this->bussinessUnit = $bussinessUnit;
    }
/**
 * 
 * @param String $company
 */
    function setCompany($company) {
        $this->company = $company;
    }
/**
 * 
 * @param String $paymentTermsCode
 */
    function setPaymentTermsCode($paymentTermsCode) {
        $this->paymentTermsCode = $paymentTermsCode;
    }
/**
 * 
 * @param String $supplierInvoiceNumber
 */
    function setSupplierInvoiceNumber($supplierInvoiceNumber) {
        $this->supplierInvoiceNumber = $supplierInvoiceNumber;
    }
/**
 * 
 * @param String $dateInvoice
 */
    function setDateInvoice($dateInvoice) {
        $this->dateInvoice = $dateInvoice;
    }
/**
 * 
 * @param String $dateAccounting
 */
    function setDateAccounting($dateAccounting) {
        $this->dateAccounting = $dateAccounting;
    }
/**
 * 
 * @param String $dateBatch
 */
    function setDateBatch($dateBatch) {
        $this->dateBatch = $dateBatch;
    }
/**
 * 
 * @param String $remark
 */
    function setRemark($remark) {
        $this->remark = $remark;
    }
/**
 * 
 * @param String $documentTypeCode
 */
    function setDocumentTypeCode($documentTypeCode) {
        $this->documentTypeCode = $documentTypeCode;
    }
/**
 * 
 * @param EntitySupplier $entitySupplier
 */
    function setEntitySupplier($entitySupplier) {
        $this->entitySupplier = $entitySupplier;
    }
    function setEntityPayee($entityPayee) {
        $this->entityPayee = $entityPayee;
    }

    /**
 * 
 * @param PurchaseOrderKey $purchaseOrderKey
 */
    function setPurchaseOrderKey($purchaseOrderKey) {
        $this->purchaseOrderKey = $purchaseOrderKey;
    }
/**
 * 
 * @param VoucherDetail[] $voucherDetail
 */
    function setVoucherDetail($voucherDetail) {
        $this->voucherDetail = $voucherDetail;
    }



    
}
