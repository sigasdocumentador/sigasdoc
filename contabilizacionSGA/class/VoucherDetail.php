<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of VoucherDetail
 *
 * @author nuevastic
 */
include("GlDistribution.php");
include("Voucher.php");
class VoucherDetail {
    public $voucher;
    public $glDistribution;
    
    function __construct($voucher, $glDistribution) {
        $this->voucher = $voucher;
        $this->glDistribution = $glDistribution;
    }
/**
 * 
 * @return Voucher
 */
    function getVoucher() {
        return $this->voucher;
    }
/**
 * 
 * @return GlDistribution
 */
    function getGlDistribution() {
        return $this->glDistribution;
    }
/**
 * 
 * @param Voucher $voucher
 */
    function setVoucher($voucher) {
        $this->voucher = $voucher;
    }
/**
 * 
 * @param GlDistribution $glDistribution
 */
    function setGlDistribution($glDistribution) {
        $this->glDistribution = $glDistribution;
    }


}
