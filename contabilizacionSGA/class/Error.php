<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Error
 *
 * @author nuevastic
 */
class Error {
    private $errorCode;
    private $errorMessage;
    function __construct($errorCode="", $errorMessage="") {
        $this->errorCode = $errorCode;
        $this->errorMessage = $errorMessage;
    }

    function getErrorCode() {
        return $this->errorCode;
    }

    function getErrorMessage() {
        return $this->errorMessage;
    }
    function setErrorCode($errorCode) {
        $this->errorCode = $errorCode;
    }

    function setErrorMessage($errorMessage) {
        $this->errorMessage = $errorMessage;
    }



}
