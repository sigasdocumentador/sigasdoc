<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of userReservedData
 *
 * @author nuevastic
 */
class UserReservedData {
    public $userReservedDate;
    public $userReservedReference;
    
    function __construct($userReservedDate, $userReservedReference) {
        $this->userReservedDate = $userReservedDate;
        $this->userReservedReference = $userReservedReference;
    }


    /**
 * 
 * @return String
 */
    function getUserReservedDate() {
        return $this->userReservedDate;
    }
/**
 * 
 * @return String
 */
    function getUserReservedReference() {
        return $this->userReservedReference;
    }
/**
 * 
 * @param String $userReservedDate
 */
    function setUserReservedDate($userReservedDate) {
        $this->userReservedDate = $userReservedDate;
    }
/**
 * 
 * @param String $userReservedReference
 */
    function setUserReservedReference($userReservedReference) {
        $this->userReservedReference = $userReservedReference;
    }


}
