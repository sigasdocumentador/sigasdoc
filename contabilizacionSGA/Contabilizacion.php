<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of contabilizacion
 *
 * @author nuevastic
 */
include ("class/Header.php");
include ("class/Response.php");

class Contabilizacion {
    private  $wsdl="http://10.10.1.127:8111/ITC_OSB/proxy/ContabilizacionSGA?WSDL";
    private $soap_serv;
    private $header;
    private $response;
    function __construct($header=array()){
        $this->soap_serv=new SoapClient($this->wsdl);  
		$this->response=new Response(); 
        $this->setHeader($header);
        
    }
	public function setHeader($header){
		 $this->header=$header;
	}
    
    public function procesar(){
    
	$contabilizacionSGARequest=new stdClass();
    $contabilizacionSGARequest->header=$this->header;
	
    try{
   		$x=$this->soap_serv->contabilizar($contabilizacionSGARequest);
		//var_dump($x);
		/*if($x->result!="ERROR"){*/
			//$this->response->setResult($x->result);
		$error=new Error();
		/*}*/    
		/*else
		{
			$error=new Error();
			$error->setErrorCode($x->error->codigo);
			$error->setErrorMessage($x->error->mensaje);
		}*/
		//$this->response->setResult($x->result);
		$this->response->setError($error);
		$this->response->setProcessedHeader($x->processedHeader);
    }
    catch(Exception $ex){
    	$error=new Error("Service-error",$ex->getMessage());
		$this->response=new Response("ERROR",$error);
       
    }
     return $this->response;  
    }   
	public function getResponse(){
		return $this->response;
	}
    
    
   
}
