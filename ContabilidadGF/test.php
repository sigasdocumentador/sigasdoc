<?php
   $root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
   include_once  $root;
   include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'utilitario.class.php';
   
   include_once '../config.php';
   include 'Contabilidad.php';
   
   class BaseContabilidad{
   
	/*DOCUMENT_TYPE_CODE = J1
				amount_domestic=intval($row["valor_plani_tipo6"])
				$aux=new Fecha(substr($row["fecha_recau_tipo1"], 0, 4),substr($row["fecha_recau_tipo1"], 4, 2),substr($row["fecha_recau_tipo1"], 6, 2))
				revisar aportes013 con substr($row["fecha_recau_tipo1"], 0, 4);
				documentNumber = substr($row["fecha_recau_tipo1"], 0, 2)0000_CONSECUTIVO
				periodo=$row["perio_pago_tipo6"]
				nit=$row["ident_aport_tipo6_db"] -> convertir 
				$credit=explode(NUEVA_CUENTA_CONTABLE_CREDITO,'.');				 
				$debit=explode(NUEVA_CUENTA_CONTABLE_DEBITO,'.');
				nameExplanationAlpha=P.APO-YYYY
				
				entityTaxId	ediUserId	ediTransactionNumber	ediLineNumber	ediBatchNumber	documentCompany	documentTypeCode	documentNumber	dateAccounting	company	amountDomestic	nameExplanationAlpha	nameExplanationRemark	businessUnitHome	reference1	reference2	jobPoolGroupingCode	businessUnit	objectAccount	subsidiary
			[0]nit|cedula	SIGAS-TILAG						1				1	SIGAS-200416-001	00002			J1				15000022			20160420	00002		-56800		P.APO-periodo-nit|cedula						02090401			NEIVA										$credit[0]		$credit[1]			$credit[2]
			[1]				SIGAS-TILAG						1				2	SIGAS-200416-001	00002			J1				15000022			20160420	00002		 56800		P.APO-periodo-nit								02090401			NEIVA										$debit[0]		$debit[1]			$debit[2]
				
	*/
	
   function contabilidad_entry($amount,$date,$periodo,$nit,$year,$tranNumber,$consecutivo_batch)
   {
	   $nameExplanationAlpha="P.APO-".$periodo."-".$nit;
	   $nit_formatted=format_nit($nit);
	   $consecutivo=obtener_consecutivo($year);
	   $batch="SIGAS-".$date."-".$consecutivo_batch;
	   $documentNumber = $year."0000".$consecutivo;
	   Crear(true,$nit_formatted,$user,$tranNumber,"1",$batch,$dateAccounting,$amount,$nameExplanationAlpha);
	   Crear(false,$nit_formatted,$user,$tranNumber,"2",$batch,$dateAccounting,$amount,$nameExplanationAlpha);
   }
   
   function obtener_consecutivo($year)
   {
	   
   }
	
	
   function CrearContabilidad($isCredit,$nit,$user,$tranNumber,$tranLine,$batch,$dateAccounting,$amount,$nameExplanationAlpha){
		$journal=new journalEntry();
	    $journal->setEntityTaxID($nit);
		$journal->setEdiUserId($user);
		$journal->setEdiTransactionNumber($tranNumber);
		$journal->setEdiLineNumber($tranLine);
		$journal->setEdiBatchNumber($batch);
		$journal->setDocumentCompany("00002");
		$journal->setDocumentTypeCode(DOCUMENT_TYPE_CODE);
		$journal->setDocumentNumber("15000022");
		$journal->setDateAccounting($dateAccounting);
		$journal->setCompany("00002");
		
		$journal->setNameExplanationAlpha($nameExplanationAlpha);
		$journal->setNameExplanationRemark(" ");
		$journal->setBusinessUnitHome("02090401");
		$journal->setReference1("NEIVA");
		$journal->setReference2(" ");
		$journal->setJobPoolGroupingCode(" ");
		if($isCredit)
		{
			$credit=explode(".",NUEVA_CUENTA_CONTABLE_CREDITO);
			$journal->setAmountDomestic("-".$amount);
			$journal->setBusinessUnit($credit[0]);
			$journal->setObjectAccount($credit[1]);
			$journal->setSubsidiary($credit[2]);
		}
		else
		{
			$journal->setEntityTaxID(" ");
			$debit=explode(".",NUEVA_CUENTA_CONTABLE_DEBITO);
			$journal->setAmountDomestic($amount);
			$journal->setBusinessUnit($debit[0]);
			$journal->setObjectAccount($debit[1]);
			$journal->setSubsidiary($debit[2]);
		}
		$contabilidad=new Contabilidad($journal);
		$response=$contabilidad->procesar();
		return $response;
	}

	function add_space_to_nit($nit)
	{
		$aux=trim($nit);
		$count_nit=strlen($aux);
		if($count_nit<16){
		  $aux=str_pad($aux,16," ",STR_PAD_LEFT);
		}
		return $aux;
	}

	function clean_name($name)
	{
		$name=str_replace("&"," ",$name);
		return $name;
	}
	
	$respuesta =CrearContabilidad(true,"1.075.227.896-6","tilag","1","1","SIGAS-200416-001","20160420","58.00","P.APO-201603-891180008-2");
	debug($respuesta);
	
	function format_nit($nit) {
	  // nit debe ser 900316300-4
	  $nit = preg_replace("/[^0-9]/", "", $nit);
	  $length = strlen($nit);
	  debug($length);
	  switch($length) {
	  case 10:
	   return preg_replace("/([0-9]{3})([0-9]{3})([0-9]{3})([0-9]{1})/", "$1.$2.$3-$4", $nit);
	  break;
	  default:
		return $nit;
	  break;
	  }
	}

}
	
	
?>
