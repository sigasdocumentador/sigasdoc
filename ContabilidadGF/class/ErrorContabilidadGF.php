<?php

class ErrorContabilidadGF{
    private $errorCode;
    private $errorMessage;
    function __construct($errorCode="", $errorMessage="") {
        $this->errorCode = $errorCode;
        $this->errorMessage = $errorMessage;
    }

    function getErrorCode() {
        return $this->errorCode;
    }

    function getErrorMessage() {
        return $this->errorMessage;
    }
    function setErrorCode($errorCode) {
        $this->errorCode = $errorCode;
    }

    function setErrorMessage($errorMessage) {
        $this->errorMessage = $errorMessage;
    }



}
