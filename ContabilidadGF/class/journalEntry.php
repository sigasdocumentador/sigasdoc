<?php
class journalEntry
{
  private $entityTaxId;
  private $ediUserId;
  private $ediTransactionNumber;
  private $ediLineNumber;
  private $ediBatchNumber;
  private $documentCompany;
  private $documentTypeCode;
  private $documentNumber;
  private $dateAccounting;
  private $company;
  private $amountDomestic;
  private $nameExplanationAlpha;
  private $nameExplanationRemark;
  private $businessUnitHome;
  private $reference1;
  private $reference2;
  private $jobPoolGroupingCode;
  private $businessUnit;
  private $objectAccount;
  private $subsidiary;
  private $ledgerTypeCode;


  function getLedgerTypeCode() {
      return $this->ledgerTypeCode;
  }

  function setLedgerTypeCode($ledgerTypeCode) {
      $this->ledgerTypeCode = $ledgerTypeCode;
  }

      /**
     * Get the value of Entity Tax Id
     *
     * @return mixed
     */
    public function getEntityTaxId()
    {
        return $this->entityTaxId;
    }

    /**
     * Set the value of Entity Tax Id
     *
     * @param mixed entityTaxId
     *
     * @return self
     */
    public function setEntityTaxId($entityTaxId)
    {
        $this->entityTaxId = $entityTaxId;

        return $this;
    }

    /**
     * Get the value of Edi User Id
     *
     * @return mixed
     */
    public function getEdiUserId()
    {
        return $this->ediUserId;
    }

    /**
     * Set the value of Edi User Id
     *
     * @param mixed ediUserId
     *
     * @return self
     */
    public function setEdiUserId($ediUserId)
    {
        $this->ediUserId = $ediUserId;

        return $this;
    }

    /**
     * Get the value of Edi Transaction Number
     *
     * @return mixed
     */
    public function getEdiTransactionNumber()
    {
        return $this->ediTransactionNumber;
    }

    /**
     * Set the value of Edi Transaction Number
     *
     * @param mixed ediTransactionNumber
     *
     * @return self
     */
    public function setEdiTransactionNumber($ediTransactionNumber)
    {
        $this->ediTransactionNumber = $ediTransactionNumber;

        return $this;
    }

    /**
     * Get the value of Edi Line Number
     *
     * @return mixed
     */
    public function getEdiLineNumber()
    {
        return $this->ediLineNumber;
    }

    /**
     * Set the value of Edi Line Number
     *
     * @param mixed ediLineNumber
     *
     * @return self
     */
    public function setEdiLineNumber($ediLineNumber)
    {
        $this->ediLineNumber = $ediLineNumber;

        return $this;
    }

    /**
     * Get the value of Edi Batch Number
     *
     * @return mixed
     */
    public function getEdiBatchNumber()
    {
        return $this->ediBatchNumber;
    }

    /**
     * Set the value of Edi Batch Number
     *
     * @param mixed ediBatchNumber
     *
     * @return self
     */
    public function setEdiBatchNumber($ediBatchNumber)
    {
        $this->ediBatchNumber = $ediBatchNumber;

        return $this;
    }

    /**
     * Get the value of Document Company
     *
     * @return mixed
     */
    public function getDocumentCompany()
    {
        return $this->documentCompany;
    }

    /**
     * Set the value of Document Company
     *
     * @param mixed documentCompany
     *
     * @return self
     */
    public function setDocumentCompany($documentCompany)
    {
        $this->documentCompany = $documentCompany;

        return $this;
    }

    /**
     * Get the value of Document Type Code
     *
     * @return mixed
     */
    public function getDocumentTypeCode()
    {
        return $this->documentTypeCode;
    }

    /**
     * Set the value of Document Type Code
     *
     * @param mixed documentTypeCode
     *
     * @return self
     */
    public function setDocumentTypeCode($documentTypeCode)
    {
        $this->documentTypeCode = $documentTypeCode;

        return $this;
    }

    /**
     * Get the value of Document Number
     *
     * @return mixed
     */
    public function getDocumentNumber()
    {
        return $this->documentNumber;
    }

    /**
     * Set the value of Document Number
     *
     * @param mixed documentNumber
     *
     * @return self
     */
    public function setDocumentNumber($documentNumber)
    {
        $this->documentNumber = $documentNumber;

        return $this;
    }

    /**
     * Get the value of Date Accounting
     *
     * @return mixed
     */
    public function getDateAccounting()
    {
        return $this->dateAccounting;
    }

    /**
     * Set the value of Date Accounting
     *
     * @param mixed dateAccounting
     *
     * @return self
     */
    public function setDateAccounting($dateAccounting)
    {
        $this->dateAccounting = $dateAccounting;

        return $this;
    }

    /**
     * Get the value of Company
     *
     * @return mixed
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set the value of Company
     *
     * @param mixed company
     *
     * @return self
     */
    public function setCompany($company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get the value of Amount Domestic
     *
     * @return mixed
     */
    public function getAmountDomestic()
    {
        return $this->amountDomestic;
    }

    /**
     * Set the value of Amount Domestic
     *
     * @param mixed amountDomestic
     *
     * @return self
     */
    public function setAmountDomestic($amountDomestic)
    {
        $this->amountDomestic = $amountDomestic;

        return $this;
    }

    /**
     * Get the value of Name Explanation Alpha
     *
     * @return mixed
     */
    public function getNameExplanationAlpha()
    {
        return $this->nameExplanationAlpha;
    }

    /**
     * Set the value of Name Explanation Alpha
     *
     * @param mixed nameExplanationAlpha
     *
     * @return self
     */
    public function setNameExplanationAlpha($nameExplanationAlpha)
    {
        $this->nameExplanationAlpha = $nameExplanationAlpha;

        return $this;
    }

    /**
     * Get the value of Name Explanation Remark
     *
     * @return mixed
     */
    public function getNameExplanationRemark()
    {
        return $this->nameExplanationRemark;
    }

    /**
     * Set the value of Name Explanation Remark
     *
     * @param mixed nameExplanationRemark
     *
     * @return self
     */
    public function setNameExplanationRemark($nameExplanationRemark)
    {
        $this->nameExplanationRemark = $nameExplanationRemark;

        return $this;
    }

    /**
     * Get the value of Business Unit Home
     *
     * @return mixed
     */
    public function getBusinessUnitHome()
    {
        return $this->businessUnitHome;
    }

    /**
     * Set the value of Business Unit Home
     *
     * @param mixed businessUnitHome
     *
     * @return self
     */
    public function setBusinessUnitHome($businessUnitHome)
    {
        $this->businessUnitHome = $businessUnitHome;

        return $this;
    }

    /**
     * Get the value of Reference
     *
     * @return mixed
     */
    public function getReference1()
    {
        return $this->reference1;
    }

    /**
     * Set the value of Reference
     *
     * @param mixed reference1
     *
     * @return self
     */
    public function setReference1($reference1)
    {
        $this->reference1 = $reference1;

        return $this;
    }

    /**
     * Get the value of Reference
     *
     * @return mixed
     */
    public function getReference2()
    {
        return $this->reference2;
    }

    /**
     * Set the value of Reference
     *
     * @param mixed reference2
     *
     * @return self
     */
    public function setReference2($reference2)
    {
        $this->reference2 = $reference2;

        return $this;
    }

    /**
     * Get the value of Job Pool Grouping Code
     *
     * @return mixed
     */
    public function getJobPoolGroupingCode()
    {
        return $this->jobPoolGroupingCode;
    }

    /**
     * Set the value of Job Pool Grouping Code
     *
     * @param mixed jobPoolGroupingCode
     *
     * @return self
     */
    public function setJobPoolGroupingCode($jobPoolGroupingCode)
    {
        $this->jobPoolGroupingCode = $jobPoolGroupingCode;

        return $this;
    }

    /**
     * Get the value of Business Unit
     *
     * @return mixed
     */
    public function getBusinessUnit()
    {
        return $this->businessUnit;
    }

    /**
     * Set the value of Business Unit
     *
     * @param mixed businessUnit
     *
     * @return self
     */
    public function setBusinessUnit($businessUnit)
    {
        $this->businessUnit = $businessUnit;

        return $this;
    }

    /**
     * Get the value of Object Account
     *
     * @return mixed
     */
    public function getObjectAccount()
    {
        return $this->objectAccount;
    }

    /**
     * Set the value of Object Account
     *
     * @param mixed objectAccount
     *
     * @return self
     */
    public function setObjectAccount($objectAccount)
    {
        $this->objectAccount = $objectAccount;

        return $this;
    }

    /**
     * Get the value of Subsidiary
     *
     * @return mixed
     */
    public function getSubsidiary()
    {
        return $this->subsidiary;
    }

    /**
     * Set the value of Subsidiary
     *
     * @param mixed subsidiary
     *
     * @return self
     */
    public function setSubsidiary($subsidiary)
    {
        $this->subsidiary = $subsidiary;

        return $this;
    }

}
