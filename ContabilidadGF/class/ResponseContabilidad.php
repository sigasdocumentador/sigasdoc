<?php
include("ErrorContabilidadGF.php");
include("ProcessedJournalEntry.php");
class ResponseContabilidad{

	private $numberRecordsInserted;
	private $processedJournalEntry;
	private $error;


    /**
     * Get the value of Number Records Inserted
     *
     * @return mixed
     */
    public function getNumberRecordsInserted()
    {
        return $this->numberRecordsInserted;
    }

    /**
     * Set the value of Number Records Inserted
     *
     * @param mixed numberRecordsInserted
     *
     * @return self
     */
    public function setNumberRecordsInserted($numberRecordsInserted)
    {
        $this->numberRecordsInserted = $numberRecordsInserted;

        return $this;
    }

    /**
     * Get the value of Processed Journal Entry
     *
     * @return mixed
     */
    public function getProcessedJournalEntry()
    {
        return $this->processedJournalEntry;
    }

    /**
     * Set the value of Processed Journal Entry
     *
     * @param mixed processedJournalEntry
     *
     * @return self
     */
    public function setProcessedJournalEntry($processedJournalEntry)
    {
        $this->processedJournalEntry = $processedJournalEntry;

        return $this;
    }

    /**
     * Get the value of Error
     *
     * @return mixed
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * Set the value of Error
     *
     * @param mixed error
     *
     * @return self
     */
    public function setError($error)
    {
        $this->error = $error;

        return $this;
    }

}
