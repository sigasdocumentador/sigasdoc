<?php
include("ErrorContabilidadGF.php");
include("JournalEntry.php");
class ProcessedJournalEntry{

  private $result;
  private $journalEntry;
  private $error;


    /**
     * Get the value of Result
     *
     * @return mixed
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * Set the value of Result
     *
     * @param mixed result
     *
     * @return self
     */
    public function setResult($result)
    {
        $this->result = $result;

        return $this;
    }

    /**
     * Get the value of Journal Entry
     *
     * @return mixed
     */
    public function getJournalEntry()
    {
        return $this->journalEntry;
    }

    /**
     * Set the value of Journal Entry
     *
     * @param mixed journalEntry
     *
     * @return self
     */
    public function setJournalEntry($journalEntry)
    {
        $this->journalEntry = $journalEntry;

        return $this;
    }

    /**
     * Get the value of Error
     *
     * @return mixed
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * Set the value of Error
     *
     * @param mixed error
     *
     * @return self
     */
    public function setError($error)
    {
        $this->error = $error;

        return $this;
    }

}
