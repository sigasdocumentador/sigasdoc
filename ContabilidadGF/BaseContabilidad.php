<?php
   $root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
   include_once  $root;
   include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';
   include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'utilitario.class.php';
   include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
   include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';

   
   include 'Contabilidad.php';
   
   class BaseContabilidad{
   
   
   private $debug;
	/*DOCUMENT_TYPE_CODE = J1
				amount_domestic=intval($row["valor_plani_tipo6"])
				$aux=new Fecha(substr($row["fecha_recau_tipo1"], 0, 4),substr($row["fecha_recau_tipo1"], 4, 2),substr($row["fecha_recau_tipo1"], 6, 2))
				revisar aportes013 con substr($row["fecha_recau_tipo1"], 0, 4);
				documentNumber = substr($row["fecha_recau_tipo1"], 0, 2)0000_CONSECUTIVO
				periodo=$row["perio_pago_tipo6"]
				nit=$row["ident_aport_tipo6_db"] -> convertir 
				$credit=explode(NUEVA_CUENTA_CONTABLE_CREDITO,'.');				 
				$debit=explode(NUEVA_CUENTA_CONTABLE_DEBITO,'.');
				nameExplanationAlpha=P.APO-YYYY
				
				entityTaxId	ediUserId	ediTransactionNumber	ediLineNumber	ediBatchNumber	documentCompany	documentTypeCode	documentNumber	dateAccounting	company	amountDomestic	nameExplanationAlpha	nameExplanationRemark	businessUnitHome	reference1	reference2	jobPoolGroupingCode	businessUnit	objectAccount	subsidiary
			[0]nit|cedula	SIGAS-TILAG						1				1	SIGAS-200416-001	00002			J1				15000022			20160420	00002		-56800		P.APO-periodo-nit|cedula						02090401			NEIVA										$credit[0]		$credit[1]			$credit[2]
			[1]				SIGAS-TILAG						1				2	SIGAS-200416-001	00002			J1				15000022			20160420	00002		 56800		P.APO-periodo-nit								02090401			NEIVA										$debit[0]		$debit[1]			$debit[2]
				
	*/
	function __construct($debug=false){
		$this->debug=$debug;
	}
	
   function contabilidad_entry($amount,$date,$periodo,$nit,$nit_digito,$year,$tranNumber,$consecutivo_batch,$user,$numeroLinea)
   {
           date_default_timezone_set("America/Bogota");
           $fechaActual = date("Ymd");
           $nameExplanationAlpha = "PAGO APORTES ".$fechaActual;
	   $nameExplanationRemark = "P.APO-".$periodo."-".$nit;
	   $nit_formatted=Utilitario::format_nit($nit);
	   if($nit_digito!="")
	   {
		   $nit_formatted=$nit_formatted."-".$nit_digito;
	   }
	   $consecutivo=$this->obtener_consecutivo($year);
	   //$batch="SIGAS-".$date."-".$consecutivo_batch;
           $batch="SIGAS-$consecutivo_batch";
           $year = substr($fechaActual, 2, 3);
	   $documentNumber = $year.str_pad($consecutivo, 5, "0", STR_PAD_LEFT);
           date_default_timezone_set("America/Bogota");
           
	   $dateAccounting = date('Ymd');
	   if(strpos($nit_formatted,"-"))
		//$nit_formatted=$this->add_space_to_nit($nit_formatted);
                $nit_formatted= str_pad($nit_formatted, 16, " ", STR_PAD_LEFT);
           
	   //$response1=$this->CrearContabilidad(true,$nit_formatted,$user,$tranNumber,"1",$batch,$dateAccounting,$amount,$nameExplanationAlpha,$documentNumber);
	   //$response2=$this->CrearContabilidad(false,$nit_formatted,$user,$tranNumber,"2",$batch,$dateAccounting,$amount,$nameExplanationAlpha,$documentNumber);
           
           
           //------------------------------------------------Movimiento Cr�dito-----------------------------------------------
           $credit = explode((string)".",NUEVA_CUENTA_CONTABLE_CREDITO);
           $sentenciaSql = "
                                INSERT INTO 
                                    movimientocontable
                                (
                                    entityTaxId,
                                    ediuserid,
                                    editransactionnumber,
                                    edilinenumber,
                                    edibatchnumber,
                                    documentcompany,
                                    documenttypecode,
                                    documentnumber,
                                    dateaccounting,
                                    company,
                                    namexplanationalpha,
                                    nameexplanationremark,
                                    bussinessunithome,
                                    reference1,
                                    reference2,
                                    jobpoolgroupingcode,
                                    amountdomestic,
                                    businessunit,
                                    objectaccount,
                                    subsidiary,
                                    ledgertypecode,
                                    fecha,
                                    consecutivobatch
                                )   
                                VALUES
                                (
                                    --'".(string)$nit_formatted."'
                                    ''    
                                    ,'".(string)"SIG-".$user."'
                                    ,'".(string)$tranNumber."'
                                    ,'".(string)$numeroLinea."'
                                    ,'".(string)$batch."'
                                    ,'".(string)"00002"."'
                                    ,'".(string)DOCUMENT_TYPE_CODE."'
                                    ,'".(string)$documentNumber."'
                                    ,'".(string)$dateAccounting."'
                                    ,'".(string)"00002"."'
                                    ,'".(string)$nameExplanationAlpha."'
                                    ,'".(string)$nameExplanationRemark."'
                                    ,'".(string)str_pad("02090401", 12, " ", STR_PAD_LEFT)."'
                                    ,'".(string)"NEIVA"."'
                                    ,'".(string)" "."'
                                    ,'".(string)" "."'
                                    ,'".(string)$amount."'
                                    ,'".(string)str_pad($credit[0], 12, " ", STR_PAD_LEFT)."'
                                    ,'".(string)$credit[1]."'
                                    ,'".(string)$credit[2]."'
                                    ,''
                                    ,GETDATE()    
                                    ,'".$consecutivo_batch."'
                                )
                           ";
		$db = $this->connectar();
		$rs = $db->queryInsert($sentenciaSql,'movimientocontable');//El segundo parametro es la tabla donde se esta insertando
                
                
                //------------------------------------------------Movimiento D�bito-----------------------------------------------
                $numeroLinea++;
                $debit = explode(".",(string)NUEVA_CUENTA_CONTABLE_DEBITO);
                $sentenciaSql = "
                                 INSERT INTO 
                                     movimientocontable
                                 (
                                     entityTaxId,
                                     ediuserid,
                                     editransactionnumber,
                                     edilinenumber,
                                     edibatchnumber,
                                     documentcompany,
                                     documenttypecode,
                                     documentnumber,
                                     dateaccounting,
                                     company,
                                     namexplanationalpha,
                                     nameexplanationremark,
                                     bussinessunithome,
                                     reference1,
                                     reference2,
                                     jobpoolgroupingcode,
                                     amountdomestic,
                                     businessunit,
                                     objectaccount,
                                     subsidiary,
                                     ledgertypecode,
                                     fecha,
                                     consecutivobatch
                                 )   
                                 VALUES
                                 (
                                     '".(string)$nit_formatted."'
                                     ,'".(string)"SIG-".$user."'
                                     ,'".(string)$tranNumber."'
                                     ,'".(string)$numeroLinea."'
                                     ,'".(string)$batch."'
                                     ,'".(string)"00002"."'
                                     ,'".(string)DOCUMENT_TYPE_CODE."'
                                     ,'".(string)$documentNumber."'
                                     ,'".(string)$dateAccounting."'
                                     ,'".(string)"00002"."'
                                     ,'".(string)$nameExplanationAlpha."'
                                     ,'".(string)$nameExplanationRemark."'
                                     ,'".(string)str_pad("02090401", 12, " ", STR_PAD_LEFT)."'
                                     ,'".(string)"NEIVA"."'
                                     ,'".(string)" "."'
                                     ,'".(string)" "."'
                                     ,'-".(string)$amount."'
                                     ,'".(string)str_pad($debit[0], 12, " ", STR_PAD_LEFT)."'
                                     ,'".(string)$debit[1]."'
                                     ,'".(string)$debit[2]."'
                                     ,''    
                                     ,GETDATE()    
                                     ,'".$consecutivo_batch."'
                                 )
                            ";
                $db = $this->connectar();
                $rs = $db->queryInsert($sentenciaSql,'movimientocontable');//El segundo parametro es la tabla donde se esta insertando
                
                return $numeroLinea;
   }
   
   function CrearContabilidad($isCredit,$nit,$user,$tranNumber,$tranLine,$batch,$dateAccounting,$amount,$nameExplanationAlpha,$documentNumber){
		$journal=new journalEntry();
                $journal->setEntityTaxID((string)trim($nit));
		$journal->setEdiUserId((string)"SIG-".$user);
		$journal->setEdiTransactionNumber((string)$tranNumber);
		$journal->setEdiLineNumber((string)$tranLine);
		$journal->setEdiBatchNumber((string)$batch);
		$journal->setDocumentCompany((string)"00002");
		$journal->setDocumentTypeCode((string)DOCUMENT_TYPE_CODE);
		$journal->setDocumentNumber((string)$documentNumber);
		$journal->setDateAccounting((string)$dateAccounting);
		$journal->setCompany((string)"00002");
		
		$journal->setNameExplanationAlpha((string)$nameExplanationAlpha);
		$journal->setNameExplanationRemark((string)" ");
		$journal->setBusinessUnitHome((string)str_pad("02090401", 12, " ", STR_PAD_LEFT));
		$journal->setReference1((string)"NEIVA");
		$journal->setReference2((string)" ");
		$journal->setJobPoolGroupingCode((string)" ");
		if($isCredit)
		{
			$credit=explode((string)".",NUEVA_CUENTA_CONTABLE_CREDITO);
			$journal->setAmountDomestic((string)"-".$amount);
			$journal->setBusinessUnit((string)str_pad($credit[0], 12, " ", STR_PAD_LEFT));
			$journal->setObjectAccount((string)$credit[1]);
			$journal->setSubsidiary((string)$credit[2]);
		}
		else
		{
			$journal->setEntityTaxID(" ");
			$debit=explode(".",(string)NUEVA_CUENTA_CONTABLE_DEBITO);
			$journal->setAmountDomestic((string)$amount);
			$journal->setBusinessUnit((string)str_pad($debit[0], 12, " ", STR_PAD_LEFT));
			$journal->setObjectAccount((string)$debit[1]);
			$journal->setSubsidiary((string)$debit[2], 12, " ", STR_PAD_LEFT);
		}
                $journal->setLedgerTypeCode("");
		$contabilidad=new Contabilidad($journal);
		$response=$contabilidad->procesar();
		if($this->debug)
			debug($response);
		return $response;
	}

	function add_space_to_nit($nit)
	{
		if($this->debug)
			debug("agregando espacio");
		$aux=trim($nit);
		$count_nit=strlen($aux);
		if($this->debug)
		{
			debug("antes de espacio");
			debug(strlen($aux));
		}
		if($count_nit<16){
		  $aux=str_pad($aux,16," ",STR_PAD_LEFT);
		}
		if($this->debug)
		{
			debug("despues de espacio");
			debug(strlen($aux));
		}
		return $aux;
	}

	function clean_name($name)
	{
		if($this->debug)
		{
			debug("limpiando nombre");
			debug($name);
		}
		if(strpos($name,"&"))
		{
			$name=str_replace("&"," ",$name);
			if($this->debug)
			{
				debug("nuevo nombre");
				debug($name);
			}
		}
		return $name;
	}
	
	function connectar()
	{
		$db = IFXDbManejador::conectarDB();
		if($db->conexionID==null){
			$cadena = $db->error;
			echo msg_error($cadena);
			exit();
		}
		return $db;
	}
	
	function obtener_consecutivo($ano)
	{
		$sql="select numero from aportes013 where comprobante='CAT' and ano='%s';";
		$sql=sprintf($sql,"20".$ano);
		$db=$this->connectar();
		$query=$db->querySimple($sql);
		$result=$query->fetchAll();
		if($this->debug)
			debug($result);
		if(count($result)>0)
		{
			return strval($result[0]['numero']);
		}
		else
		{
			$result=$this->agregar_consecutivo_ano($ano);
			if($this->debug)
				debug($result);
		}
		return strval(1);
	    
	}
	
	function agregar_consecutivo_ano($ano){
		$sql="insert into aportes013 (comprobante,detalle,numero,ano) values ('CAT','CONSECUTIVO ANO TESORERIA',1,'%s')";
		$sql=sprintf($sql,"20".$ano);
		$db=$this->connectar();
		$rs=$db->queryInsert($sql,'aportes013');//El segundo parametro es la tabla donde se esta insertando

		if($rs !=0 ){
			$result=$rs;
		}
		else{
			$result=msg_error($db->error);
		}
		return $result;
	}
	
	function actualizar_consecutivo_ano($ano){
		$sql="update aportes013 set numero=numero+1 where comprobante='CAT' and ano = '%s'";
		$sql=sprintf($sql,"20".$ano);
		$db=$this->connectar();
		$rs=$db->queryActualiza($sql);
		if($rs !=0 ){
			$result=$rs;
		}
		else{
			$result=msg_error($db->error);
		}
		return $result;
	}
	
	function ObtenerConsecutivoBatch()
	{
		$sql="select numero from aportes013 where comprobante='CPB';";
		$db=$this->connectar();
		$query=$db->querySimple($sql);
		$result=$query->fetchAll();
		if($this->debug)
			debug($result);
		$numero="00001";
		if(count($result)>0)
		{
			$numero=strval($result[0]['numero']);
			$numero=$this->add_ceros($numero);
		}
		else
		{
			$result=$this->agregar_consecutivo_batch();
			if($this->debug)
				debug($result);
		}
		return $numero;
	}
	
	function agregar_consecutivo_batch(){
		$sql="insert into aportes013 (comprobante,detalle,numero,ano) values ('CPB','CONSIGNACION PLANILLA BATCH',1,'')";
		$db=$this->connectar();
		$rs=$db->queryInsert($sql,'aportes013');//El segundo parametro es la tabla donde se esta insertando

		if($rs !=0 ){
			$result=$rs;
		}
		else{
			$result=msg_error($db->error);
		}
		return $result;
	}
	
	function actualizar_consecutivo_batch(){
		$sql="update aportes013 set numero=numero+1 where comprobante='CPB'";
		$db=$this->connectar();
		$rs=$db->queryActualiza($sql);
		if($rs !=0 ){
			$result=$rs;
		}
		else{
			$result=msg_error($db->error);
		}
		return $result;
	}
        
    function actualizar_consecutivo_anio(){
		$sql="update aportes013 set numero=numero+1 where comprobante='CAT'";
		$db=$this->connectar();
		$rs=$db->queryActualiza($sql);
		if($rs !=0 ){
			$result=$rs;
		}
		else{
			$result=msg_error($db->error);
		}
		return $result;
	}
	
	function ObtenerDigitoNit($nit)
	{
		$sql="select digito from aportes048 where nit='%s';";
		$sql=sprintf($sql,$nit);
		$db=$this->connectar();
		$query=$db->querySimple($sql);
		$result=$query->fetchAll();
		if($this->debug)
			debug($result);
		$digito="";
		if(count($result)>0)
		{
			$digito=$result[0]['digito'];
		}
		return $digito;
	}
	
	function incrementar_anos($anos)
	{
		if($this->debug)
			debug($anos);
		foreach($anos as $ano)
		{
			if($this->debug)
				debug($ano);
			$result=$this->actualizar_consecutivo_ano($ano);
			if($this->debug)
				debug($result);
		}
		
	}
	
	function add_ceros($number)
	{
		$aux=trim($number);
		$count_number=strlen($aux);
		if($count_number<5){
		  $aux=str_pad($aux,5,"0",STR_PAD_LEFT);
		}
		return $aux;
	}
function enviarMovimientoJDE($numeroComprobante){
    /*Envi� de los movimientos contables a JD Edwards*/
    $sentenciaSql = "
                        SELECT 
                            * 
                        FROM 
                            movimientocontable 
                        WHERE 
                            consecutivobatch = '".$numeroComprobante."'
                    ";
    $db = $this->connectar();
    $query=$db->querySimple($sentenciaSql);
    $result=$query->fetchAll();
    
    $arrMovimientoContable = array();
    foreach($result as $fila){        
        $journal = new journalEntry();
        $journal->setEntityTaxID((string)$fila["entityTaxId"]);
        $journal->setEdiUserId((string)$fila["ediuserid"]);
        $journal->setEdiTransactionNumber($fila["editransactionnumber"]);
        $journal->setEdiLineNumber($fila["edilinenumber"]);
        $journal->setEdiBatchNumber($fila["edibatchnumber"]);
        $journal->setDocumentCompany($fila["documentcompany"]);
        $journal->setDocumentTypeCode($fila["documenttypecode"]);
        $journal->setDocumentNumber($fila["documentnumber"]);
        $journal->setDateAccounting($fila["dateaccounting"]);
        $journal->setCompany($fila["company"]);

        $journal->setNameExplanationAlpha($fila["namexplanationalpha"]);
        $journal->setNameExplanationRemark($fila["nameexplanationremark"]);
        $journal->setBusinessUnitHome($fila["bussinessunithome"]);
        $journal->setReference1($fila["reference1"]);
        $journal->setReference2($fila["reference2"]);
        $journal->setJobPoolGroupingCode($fila["jobpoolgroupingcode"]);
        $journal->setAmountDomestic((string)$fila["amountdomestic"]);
        $journal->setBusinessUnit((string)$fila["businessunit"]);
        $journal->setObjectAccount((string)$fila["objectaccount"]);
        $journal->setSubsidiary((string)$fila["subsidiary"]);
        $journal->setLedgerTypeCode($fila["ledgertypecode"]);
        
        array_push($arrMovimientoContable, $journal);
    }
    $contabilidad = new Contabilidad($arrMovimientoContable);
    $response = $contabilidad->procesar();
    if($this->debug)
            debug($response);
    return $response;
}
}
//$respuesta =$cont->CrearContabilidad(true,$nit,"tilag","1","1","SIGAS-200416-001","20160420","58.00","P.APO-201603-891180008-2");
	//debug($respuesta);
	
	
?>
