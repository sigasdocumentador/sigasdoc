<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include("class/JournalEntry.php");
include ("class/ResponseContabilidad.php");
/**
 * Description of Terceros
 *
 * @author nuevastic
 */
class Contabilidad {
     private  $wsdl="http://10.10.1.130:8011/ITC_OSB/proxy/ContabilizacionGestionFinanciera?WSDL";
     private $soap_serv;
     private $header;
     private $response;


	 /*****Constructor********/

	 function __construct($journal=array()){
        $this->soap_serv=new SoapClient($this->wsdl,array('trace' => 1));
		$response=new Response();
		$this->header=$journal;
	 }

	 public function setJournal($journal){
	 	$this->header=$journal;
	 }


     public function procesar(){
		 $contabilidadRequest=new stdClass();
		 $contabilidadRequest->journalEntry=$this->header;
		 //echo "<p>payload:";
		 //var_dump($contabilidadRequest);
		 //echo "</p>";
		 try{
		 	$res=$this->soap_serv->contabilizar($contabilidadRequest);
			$this->procesarResponse($res);
                        
                        $file = fopen("pruebas Contabilizacion WSDL.txt", "a+");
                        fwrite($file, "REQUEST:\n" . $this->soap_serv->__getLastRequest() . "\n" . PHP_EOL);
                        fwrite($file, "-----------------------------------------------------------------------------------" . PHP_EOL);
                        fclose($file);
                        
                        //echo "REQUEST:\n" . $this->soap_serv->__getLastRequest() . "\n" . PHP_EOL;
		 }
		 catch(Exception $e){
		 	/*var_dump($tercerosRequest);
			echo "<br>";*/
                        echo "REQUEST:\n" . $this->soap_serv->__getLastRequest() . "\n" . PHP_EOL;
			var_dump($e);
			$this->response=new Response();
			$error=new Error();
		 	$error->setErrorCode("Service Error");
		 	$error->setErrorMessage($e->getMessage());
		 }
		 return $this->response;
    }



	private function procesarResponse($res){
		//var_dump($res);
		$this->response=new ResponseContabilidad();
		if(isset($res->error)){
			$error=new ErrorContabilidadGF();
			$error->setErrorCode($res->error->codigo);
			$error->setErrorMessage($res->error->mensaje);
			$this->response->setError($error);
			}
		else{

			$this->response->setNumberRecordsInserted($res->numberRecordsInserted);
			$this->response->setProcessedJournalEntry($res->processedJournalEntry);

		}

	}


	public function getResponse()
	{
		return $this->response;
	}

}
