<?php
	date_default_timezone_set('America/Bogota');
	$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
	include_once  $root;
	
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
	$db = IFXDbManejador::conectarDB();
	if($db->conexionID==null){
		$cadena = $db->error;
		echo msg_error($cadena);
		exit();
	}
	
	$db->inicioTransaccion();
	
	$arrAportes = $_POST["aporte"];
	
	$banderaError = 0;
	foreach($arrAportes as $row){
		$sql = "UPDATE aportes011 SET estado_promotoria='{$row["estado_promotoria"]}' WHERE idaporte=".$row["idaporte"];
		
		if(($db->queryActualiza($sql))===false){
			$banderaError++;
			break;
		}
	}
	
	if($banderaError==0){
		$db->confirmarTransaccion();
		echo 1;
	}else{
		$db->cancelarTransaccion();
		echo 0;
	}
?>