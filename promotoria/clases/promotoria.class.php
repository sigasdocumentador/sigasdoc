<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';

include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'utilitario.class.php';

class Promotoria {
	/************************************
	 *  ATRIBUTOS ENTIDADES			    *
	***********************************/
	private static $con = null;
	
	function __construct(){
		try{
			self::$con = IFXDbManejador::conectarDB();
			if( self::$con->conexionID==null ){
				throw new Exception(self::$con->error);
			}
		}catch(Exception $e){
			echo $e->getMessage();
			exit();
		}
	}
	
	function inicioTransaccion(){
		self::$con->inicioTransaccion();
	}
	function cancelarTransaccion(){
		self::$con->cancelarTransaccion();
	}
	function confirmarTransaccion(){
		self::$con->confirmarTransaccion();
	}
	
	/************************************
	 *  INSERT 						    *
	 ***********************************/
	/**
	 * Guarda el asesor
	 * @param unknown_type $datos
	 * @return number: 0|1
	 */
	function insertar_asesor($datos){
			$sql="INSERT INTO aportes300(idpersona,idtipoasesor,fechainicio,fechafinal,estado,usuario,fechasistema) 
						VALUES(".$datos["idpersona"].",".$datos["idtipoasesor"].",'".$datos["fechainicio"]."','".$datos["fechafinal"]."','".$datos["estado"]."','".$datos["usuario"]."',cast(getdate() as date))";
			return self::$con->queryInsert($sql,"aportes300") === null ? 0 : 1;
	}

	function insertar_promotor($arr){
	   	$sql="insert into aportes301(idpersona,fechainicio,fechafinal,estado,usuario,fechasistema) values(".$arr[0].",'".$arr[1]."','".$arr[2]."','".$arr[3]."','".$arr[4]."',cast(getdate() as date))";
		return self::$con->queryInsert($sql,"aportes301") === null ? 0 : 1;  	
	}
	
	
	function guardar_visita($datos,$usuario){
		$fechaApelacion = empty($datos["fecha_apelacion"])?"null":"'{$datos["fecha_apelacion"]}'";
		$sql = "INSERT INTO dbo.aportes302
					(idvisita,fechavisita,idempresa,conclusion,novedad,idpromotor,usuario,idcausal,idabogadoexterno,idabogadointerno,idagencia,liquidacion,apelacion,fecha_apelacion)
			VALUES ('{$datos["idvisita"]}','{$datos["fechavisita"]}',{$datos["idempresa"]},:conclusion,:novedad,{$datos["idpromotor"]},'$usuario',{$datos["idcausal"]},{$datos["idabogadoexterno"]},{$datos["idabogadointerno"]},{$datos["idagencia"]},'{$datos["liquidacion"]}','{$datos["apelacion"]}',$fechaApelacion)";
		
		$statement = self::$con->conexionID->prepare($sql);
		$statement->bindValue(":conclusion", $datos["conclusion"],  PDO::PARAM_STR);
		$statement->bindValue(":novedad", $datos["novedad"],  PDO::PARAM_STR);
		$guardada = $statement->execute();
		return $guardada == false ? 0 : 1;
		
		/*$sql="insert into aportes302(idvisita,fechavisita,idempresa,conclusion,novedad,convenio,fechapago,idpromotor,idcausal,usuario,fechasistema)
							  values('".$arr[0]."','".$arr[1]."',".$arr[2].",:conclusion,:novedad,'".$arr[5]."','".$arr[6]."',".$arr[7].",".$arr[8].",'".$arr[9]."',cast(getdate() as date))";
		$statement = self::$con->conexionID->prepare($sql);
		$statement->bindValue(":conclusion", $arr[3],  PDO::PARAM_STR);
		$statement->bindValue(":novedad", $arr[4],  PDO::PARAM_STR);
		$guardada = $statement->execute();
		return $guardada == false ? 0 : 1;*/
	}
	
	/**
	 * Guarda el detalle de la liquidacion
	 * Retorna 0 o el id del registro insertado
	 * 
	 * FECHA_ACTUALIZACION: 2014-06-18
	 * 
	 * @param unknown_type $datos
	 * @param unknown_type $usuario
	 * @return multitype:int
	 */
	function guardar_detalle_liquidacion( $datos, $usuario ){
		$sql = "INSERT INTO dbo.aportes303 (idliquidacion, bruto, aportes, neto, bruto_inicial, aporte_inicial, neto_inicial, abono, cancelado, periodo, periodo_inicial, observacion, usuario)
					VALUES ( {$datos["idliquidacion"]}, {$datos["bruto"]}, {$datos["aportes"]}, {$datos["neto"]}, {$datos["bruto_inicial"]}, {$datos["aporte_inicial"]}, {$datos["neto_inicial"]}, {$datos["abono"]}, '{$datos["cancelado"]}', '{$datos["periodo"]}', '{$datos["periodo_inicial"]}', :observacion, '$usuario')";
		
		$statement = self::$con->conexionID->prepare($sql);
		$statement->bindValue(":observacion", $datos["observacion"],  PDO::PARAM_STR);
		$guardada = $statement->execute();
		
		$idDetalle = 0;
		if($guardada!=false){
			$idDetalle = self::$con->conexionID->lastInsertId("aportes303");
		}
		
		return $idDetalle;
	}
	
	/**
	 * Guardar el seguimiento de la visita
	 * @param unknown_type $datos
	 * @param unknown_type $usuario
	 * @return number
	 */
	function insertar_seguimiento( $datos, $usuario ){
		$sql="INSERT INTO aportes304(idvisita,observacion,usuario)
				VALUES ('{$datos["idvisita"]}',:observacion,'$usuario')";
		$statement = self::$con->conexionID->prepare($sql);
		$statement->bindValue(":observacion", $datos["observacion"],  PDO::PARAM_STR);
		$guardada = $statement->execute();
		return $guardada == false ? 0 : 1;
	}
	
	/**
	 * Guarda la liquidacion de la visita
	 * 
	 * FECHA_ACTUALIZACION: 2014-06-18
	 * 
	 * @param unknown_type $datos
	 * @param unknown_type $usuario
	 * @return number
	 */
	function guardar_liquidacion( $datos, $usuario ){
		$fechaEstado = empty($datos["fecha_estado"])?"null":"'{$datos["fecha_estado"]}'";
		
		$sql = "INSERT INTO aportes305(
						    idliquidacion, fechaliquidacion, idvisita, idtiponotificacion, fechanotificacion,
						    ibc, observaciones, documentacion, indicador, id_estado, fecha_estado, usuario, fechasistema
						)VALUES({$datos["idliquidacion"]}, '{$datos["fechaliquidacion"]}', '{$datos["idvisita"]}', {$datos["idtiponotificacion"]}, '{$datos["fechanotificacion"]}',
								 {$datos["ibc"]}, :observaciones,:documentacion, {$datos["indicador"]}, {$datos["id_estado"]}, $fechaEstado, '$usuario', cast(getdate() as date))";	
		$statement = self::$con->conexionID->prepare($sql);
		$statement->bindValue(":observaciones", $datos["observaciones"],  PDO::PARAM_STR);
		$statement->bindValue(":documentacion", $datos["documentacion"],  PDO::PARAM_STR);
		$guardada = $statement->execute();
		return $guardada == false ? 0 : 1;		
	}
	
	/**
	 * Guarda el convenio
	 *
	 * FECHA_ACTUALIZACION: 2014-06-24
	 *
	 * @param unknown_type $datos
	 * @param unknown_type $usuario
	 * @return number
	 */
	function guardar_convenio( $datos ){
		$sql = "INSERT INTO dbo.aportes306
					(consecutivo, indicador, valor,	usuario)
				VALUES ({$datos["consecutivo"]}, {$datos["indicador"]}, {$datos["valor"]}, '{$datos["usuario"]}')";
		$statement = self::$con->queryInsert($sql,"aportes306");
		return $statement === null ? 0: $statement;
	}
	
	/**
	 * Guarda la fecha del convenio
	 *
	 * FECHA_ACTUALIZACION: 2014-06-24
	 *
	 * @param unknown_type $datos
	 * @param unknown_type $usuario
	 * @return number
	 */
	function guardar_fecha_convenio( $datos ){
		$sql = "INSERT INTO dbo.aportes307
					(fechapago, interes, idconvenio, usuario)
				VALUES ('{$datos["fechapago"]}', {$datos["interes"]}, {$datos["idconvenio"]}, '{$datos["usuario"]}')";
		$statement = self::$con->queryInsert($sql,"aportes307");
		return $statement === null ? 0: $statement;
	}
	
	/**
	 * Guarda los periodos del convenio
	 *
	 * FECHA_ACTUALIZACION: 2014-06-24
	 *
	 * @param unknown_type $datos
	 * @param unknown_type $usuario
	 * @return number
	 */
	function guardar_periodo_convenio( $datos ){
		$sql = "INSERT INTO dbo.aportes308
					(iddetalleconvenio, idperiodoliquidacion, valor, usuario)
				VALUES ({$datos["iddetalleconvenio"]}, {$datos["idperiodoliquidacion"]}, {$datos["valor"]}, '{$datos["usuario"]}')";
		$statement = self::$con->queryInsert($sql,"aportes308");
		return $statement === null ? 0: $statement;
	}
	
	/**
	 * Guarda el reverso del abono al periodo liquidado
	 *
	 *
	 * @param unknown_type $datos
	 * @param unknown_type $usuario
	 * @return number
	 */
	function guardar_reverso_abono_periodo_liquidado( $datos, $usuario ){
		$fechaAbono = empty($datos["fechaabono"])?"null":"'{$datos["fechaabono"]}'";
	
		$sql = "UPDATE aportes310 
				SET abono={$datos["abono"]}
					, valor_reverso={$datos["valor_reverso"]}
					, fecha_reverso='{$datos["fecha_reverso"]}'
					, observacion=:observacion
					, usuario_reverso='{$datos["usuario_reverso"]}'
					, usuario_modificacion = '$usuario'
					, fecha_modificacion= GETDATE() 
				WHERE id_abono_periodo={$datos["id_abono_periodo"]}";
	
		$statement = self::$con->conexionID->prepare($sql);
		$statement->bindValue(":observacion", $datos["observacion"],  PDO::PARAM_STR);
		$guardada = $statement->execute();
		return $guardada == false ? 0 : 1;
	}
	
	/**
	 * Guarda los ajustes al periodo liquidado
	 *
	 *
	 * @param unknown_type $datos
	 * @param unknown_type $usuario
	 * @return number
	 */
	function guardar_ajuste_periodo_liquidado( $datos, $usuario ){
		$sql = "INSERT INTO aportes311(valor,periodo,fecha_ajuste,observacion,id_causal,id_periodo_liquidacion,usuario_creacion)
				VALUES ({$datos["valor"]}, '{$datos["periodo"]}', '{$datos["fecha_ajuste"]}', :observacion, {$datos["id_causal"]}, {$datos["id_periodo_liquidacion"]},'$usuario')";
		$statement = self::$con->conexionID->prepare($sql);
		$statement->bindValue(":observacion", $datos["observacion"],  PDO::PARAM_STR);
		$guardada = $statement->execute();
		return $guardada == false ? 0 : 1;		
	}
	
	/**
	 * Guarda cierre de promotoria
	 *
	 * @param unknown_type $datos
	 * @param unknown_type $usuario
	 * @return number
	 */
	function guardar_cierre( $datos, $usuario ){
		$sql = "INSERT INTO aportes315(periodo, fecha_cierre, id_estado, usuario_creacion)
					VALUES ('{$datos["periodo"]}', '{$datos["fecha_cierre"]}',{$datos["id_estado"]},'$usuario')";
		$statement = self::$con->conexionID->prepare($sql);
		$guardada = $statement->execute();
		return $guardada == false ? 0 : 1;
	}
	
	/**
	 * Guarda permiso cierre usuario
	 *
	 * @param unknown_type $datos
	 * @param unknown_type $usuario
	 * @return number
	 */
	function guardar_permiso_cierre( $datos, $usuario ){
		$sql = "INSERT INTO aportes316(usuario, fecha_inicial, fecha_final, estado, usuario_creacion)
				VALUES ('{$datos["usuario"]}', '{$datos["fecha_inicial"]}', '{$datos["fecha_final"]}', '{$datos["estado"]}', '$usuario')";
		$statement = self::$con->conexionID->prepare($sql);
		$guardada = $statement->execute();
		return $guardada == false ? 0 : 1;
	}
	
	/**
	 * Guarda las causales del ajuste
	 *
	 *
	 * @param unknown_type $datos
	 * @param unknown_type $usuario
	 * @return number
	 */
	function guardar_causal_ajuste( $datos, $usuario ){
		$sql = "INSERT INTO aportes320(codigo,nombre,descripcion,estado,usuario_creacion)
		VALUES ('{$datos["codigo"]}', '{$datos["nombre"]}', :descripcion, '{$datos["estado"]}','$usuario')";
		$statement = self::$con->conexionID->prepare($sql);
		$statement->bindValue(":descripcion", $datos["descripcion"],  PDO::PARAM_STR);
		$guardada = $statement->execute();
		return $guardada == false ? 0 : 1;
	}
	/**
	 * Guardar los periodos de la liquidacion
	 * @param unknown_type $datos
	 * @return int
	 */
	/*function guardar_periodo_liquidacion( $datos ){
		$sql = "INSERT INTO dbo.aportes309
					(iddetalleliquidacion,valor,periodo,usuario)
				VALUES ({$datos["iddetalleliquidacion"]}, {$datos["valor"]},'{$datos["periodo"]}', '{$datos["usuario"]}')";
		$statement = self::$con->queryInsert($sql,"aportes309");
		return $statement === null ? 0: $statement;
	}*/
	
	/***********************************
	 *  DELETE
	* *********************************/
	
	/**
	 * Elimina el detalle de la liquidacion
	 * 
	 * FECHA_ACTUALIZACION: 2014-06-18
	 * 
	 * @param unknown_type $idDetalleLiquidacion
	 * @return Ambigous <number, unknown>
	 */
	function eliminar_detalle_liquidacion( $idDetalleLiquidacion ){
		$sql = "DELETE FROM dbo.aportes303 WHERE iddetalle=$idDetalleLiquidacion";
		$statement = self::$con->queryActualiza($sql);
		return $statement === null ? 0: $statement;
	}
	
	/**
	 * Elimina el abono al periodo de la liquidacion
	 *
	 *
	 * @param unknown_type $idDetalleLiquidacion
	 * @return Ambigous <number, unknown>
	 */
	function eliminar_abono_periodo_liquidacion( $arrFiltro ){
		
		$attrEntidad = array(
				array("nombre"=>"id_abono_periodo","tipo"=>"NUMBER","entidad"=>"aportes310")
				,array("nombre"=>"id_periodo_liquidacion","tipo"=>"NUMBER","entidad"=>"aportes310")
				,array("nombre"=>"id_kardex","tipo"=>"DATE","entidad"=>"aportes310"));
		
		$filtroSql = Utilitario::obtenerFiltro($attrEntidad,$arrFiltro);
		
		$sql = "DELETE FROM dbo.aportes310 $filtroSql";
		$statement = self::$con->conexionID->prepare($sql);
		$guardada = $statement->execute();
		return $guardada == false ? 0 : 1;
	}
	
	/***********************************
	 *  UPDATE
	 * *********************************/
	function actualizar_asesor($arr){
		$sql="UPDATE aportes300 SET idtipoasesor=".$arr[1].", fechainicio='".$arr[2]."',fechafinal='".$arr[3]."',estado='".$arr[4]."' WHERE idasesor=".$arr[0];
		return self::$con->queryActualiza($sql);
	}
	
	function actualizar_promotor($arr){
		$sql="UPDATE aportes301 set fechainicio='".$arr[1]."',fechafinal='".$arr[2]."',estado='".$arr[3]."' where idpromotor=".$arr[0];
		return self::$con->queryActualiza($sql);
	}
	
	/**
	 * Actualiza los datos de la visita
	 *
	 * FECHA_ACTUALIZACION: 2014-06-20
	 *
	 * @param unknown_type $datos
	 * @param unknown_type $usuario
	 * @return Ambigous <number, unknown>
	 */
	function actualizar_visita($datos,$usuario){
		$fechaApelacion = empty($datos["fecha_apelacion"])?"null":"'{$datos["fecha_apelacion"]}'";
		
		$sql = "UPDATE dbo.aportes302
				SET 
					fechavisita = '{$datos["fechavisita"]}',
					idempresa = {$datos["idempresa"]},
					conclusion = :conclusion,
					novedad = :novedad,
					idpromotor = {$datos["idpromotor"]},
					usuario = '$usuario',
					idcausal = {$datos["idcausal"]},
					idabogadoexterno = {$datos["idabogadoexterno"]},
					idabogadointerno = {$datos["idabogadointerno"]},
					idagencia = {$datos["idagencia"]},
					liquidacion = '{$datos["liquidacion"]}',
					apelacion = '{$datos["apelacion"]}', 
					fecha_apelacion = $fechaApelacion
				WHERE idvisita = '{$datos["idvisita"]}'";
		
		$statement = self::$con->conexionID->prepare($sql);
		$statement->bindValue(":conclusion", $datos["conclusion"],  PDO::PARAM_STR);
		$statement->bindValue(":novedad", $datos["novedad"],  PDO::PARAM_STR);
		$guardada = $statement->execute();
		return $guardada == false ? 0 : 1;		
	}
	
	function actualizar_asesor_visita($datos){
		$sql="UPDATE aportes302 SET idabogadointerno={$datos["idabogadointerno"]}, idabogadoexterno={$datos["idabogadoexterno"]} WHERE idvisita='{$datos["idvisita"]}'";
		return self::$con->queryActualiza($sql);
	}
	
	function actualizar_numero_reitero_visita($datos){
		$sql="UPDATE aportes302 SET numeroreitero={$datos["numeroreitero"]} WHERE idvisita ='{$datos["idvisita"]}'";
		return self::$con->queryActualiza($sql);
	}
	
	function actualizar_consecutivo_visita($datos){
		$sql="UPDATE aportes302 SET consecutivo={$datos["consecutivo"]} WHERE idvisita='{$datos["idvisita"]}'";
		return self::$con->queryActualiza($sql);
	}
	
	function actualizar_estado_pys($idVisita,$motivo,$usuario){
		$sql = "UPDATE aportes302
				SET
					estado='P', motivo='$motivo', usuariopys='$usuario', fechaestado=cast(getdate() as date)
				WHERE idvisita='$idVisita'";
		return self::$con->queryActualiza($sql);
	}
	
	function actualizar_estado_visita($datos,$usuario){
		$sql = "UPDATE aportes302
				SET
					estado='{$datos["estado"]}'
					, motivo='{$datos["motivo"]}'
					, fechaestado=cast(getdate() as date)
					, usuariopys='{$datos["usuariopys"]}'
				WHERE idvisita='{$datos["idvisita"]}'";
		return self::$con->queryActualiza($sql);
	}
	
	/**
	 * Actualiza el detalle de la liquidacion
	 * 
	 * FECHA_ACTUALIZACION: 2014-06-18
	 * 
	 * @param unknown_type $datos
	 * @param unknown_type $usuario
	 * @return number
	 */
	function actualizar_detalle_liquidacion( $datos, $usuario ){
	
		$sql = "UPDATE dbo.aportes303
				SET periodo = '{$datos["periodo"]}',
					bruto = {$datos["bruto"]},
					aportes = {$datos["aportes"]},
					neto = {$datos["neto"]},
					observacion = :observacion,
					usuario = '$usuario'
				WHERE iddetalle = {$datos["iddetalle"]}";
	
		$statement = self::$con->conexionID->prepare($sql);
		$statement->bindValue(":observacion", $datos["observacion"],  PDO::PARAM_STR);
		$guardada = $statement->execute();
		return $guardada == false ? 0 : 1;
	}
	
	/**
	 * Actualiza el detalle de la liquidacion completa
	 *
	 *
	 * @param unknown_type $datos
	 * @param unknown_type $usuario
	 * @return number
	 */
	function actualizar_detalle_liquidacion_completa( $datos, $usuario ){
		$fechaAbono = empty($datos["fechaabono"])?"null":"'{$datos["fechaabono"]}'";
		
		$sql = "UPDATE aportes303
				SET bruto = {$datos["bruto"]}
					,aportes = {$datos["aportes"]}
					,neto = {$datos["neto"]}					
					,abono = {$datos["abono"]}
					,cancelado = '{$datos["cancelado"]}'
					,fechaabono = $fechaAbono
					,periodo = '{$datos["periodo"]}'
					,observacion = :observacion
					,bruto_inicial = {$datos["bruto_inicial"]}
					,aporte_inicial = {$datos["aporte_inicial"]}
					,neto_inicial = {$datos["neto_inicial"]}
					,periodo_inicial = '{$datos["periodo_inicial"]}'
					,usuario = '$usuario'
					,fechasistema = GETDATE()
				WHERE iddetalle = {$datos["iddetalle"]}";
	
		$statement = self::$con->conexionID->prepare($sql);
		$statement->bindValue(":observacion", $datos["observacion"],  PDO::PARAM_STR);
		$guardada = $statement->execute();
		return $guardada == false ? 0 : 1;
	}
	
	/**
	 * Actualiza el estado a la liquidacion
	 *
	 *
	 * @param unknown_type $datos
	 * @param unknown_type $usuario
	 * @return number
	 */
	function actualizar_estado_liquidacion( $datos, $usuario ){
		$fechaEstado = empty($datos["fecha_estado"])?"null":"'{$datos["fecha_estado"]}'";
		
		$sql = "UPDATE aportes305
				SET id_estado = {$datos["id_estado"]}
					, fecha_estado = $fechaEstado
					, observaciones = :observaciones
					, usuario = '$usuario'
					, fechasistema = GETDATE()
				WHERE idliquidacion = '{$datos["idliquidacion"]}'";
	
		$statement = self::$con->conexionID->prepare($sql);
		$statement->bindValue(":observaciones", $datos["observaciones"],  PDO::PARAM_STR);
		$guardada = $statement->execute();
		return $guardada == false ? 0 : 1;
	}
	
	/*function actulizar_abono_convenio($datos){
		$sql = "UPDATE dbo.aportes307
					SET abono = {$datos["abono"]},observacion = :observacion, fechaabono = cast(getdate() as date)
					WHERE iddetalleconvenio = {$datos["iddetalleconvenio"]}";
		$statement = self::$con->conexionID->prepare($sql);
		$statement->bindValue(":observacion", $datos["observacion"],  PDO::PARAM_STR);
		$guardada = $statement->execute();
		return $guardada == false ? 0 : 1;
	}*/
	
	function actuliza_estado_convenio($idConvenio, $estado, $motivoEstado){
		$sql = "UPDATE dbo.aportes306
				SET estado = '$estado', motivoestado = '$motivoEstado', fechaestado=CAST(getdate() AS DATE)
				WHERE idconvenio = $idConvenio";
		$statement = self::$con->conexionID->prepare($sql);
		$guardada = $statement->execute();
		return $guardada == false ? 0 : 1;
	}
	
	function actuliza_estado_abono_periodo_liquidacion($data,$arrFiltro,$usuario){
		
		$attrEntidad = array(
				array("nombre"=>"id_abono_periodo","tipo"=>"NUMBER","entidad"=>"aportes310")
				,array("nombre"=>"id_periodo_liquidacion","tipo"=>"NUMBER","entidad"=>"aportes310")
				,array("nombre"=>"id_kardex","tipo"=>"DATE","entidad"=>"aportes310"));
		
		$filtroSql = Utilitario::obtenerFiltro($attrEntidad,$arrFiltro);
		
		$sql = "UPDATE aportes310
				SET estado='{$data["estado"]}'
					, id_causal_estado={$data["id_causal_estado"]}
					, usuario_modificacion='$usuario'
					,fecha_modificacion=getdate() 
				$filtroSql";
		$statement = self::$con->conexionID->prepare($sql);
		//$statement->bindValue(":observacion", $data["observacion"],  PDO::PARAM_STR);
		$guardada = $statement->execute();
		return $guardada == false ? 0 : 1;
	}
	
	function actuliza_cierre($data,$usuario){
	
		$sql = "UPDATE aportes315
				SET periodo='{$data["periodo"]}'
					, fecha_cierre='{$data["fecha_cierre"]}'
					, id_estado={$data["id_estado"]}
					, usuario_modificacion='$usuario'
					, fecha_modificacion=getdate()
				WHERE id_cierre={$data["id_cierre"]}";
		$statement = self::$con->conexionID->prepare($sql);
		$guardada = $statement->execute();
		return $guardada == false ? 0 : 1;
	}
	
	function actuliza_permiso_cierre($data,$usuario){
	
		$sql = "UPDATE aportes316
				SET usuario='{$data["usuario"]}'
					, fecha_inicial='{$data["fecha_inicial"]}'
					, fecha_final='{$data["fecha_final"]}'
					, estado='{$data["estado"]}'
					, usuario_modificacion='$usuario'
					,fecha_modificacion=getdate()
				WHERE id_permiso_cierre={$data["id_permiso_cierre"]}";
		$statement = self::$con->conexionID->prepare($sql);
		$guardada = $statement->execute();
		return $guardada == false ? 0 : 1;
	}
	
	function actuliza_causal_ajuste($data,$usuario){
	
		$sql = "UPDATE aportes320
				SET codigo='{$data["codigo"]}'
					, nombre='{$data["nombre"]}'
					, descripcion=:descripcion
					, estado='{$data["estado"]}'
					, usuario_modificacion='$usuario'
					,fecha_modificacion=getdate()
				WHERE id_causal_ajuste={$data["id_causal_ajuste"]}";
		$statement = self::$con->conexionID->prepare($sql);
		$statement->bindValue(":descripcion", $data["descripcion"],  PDO::PARAM_STR);
		$guardada = $statement->execute();
		return $guardada == false ? 0 : 1;
	}
	
	/***********************************
	 *  SELECT
	 * *********************************/
	/**
	 * El parametro debe ser un array indice valor array("atributo"=>valor,...)
	 * @param unknown_type $arrAtributoValor
	 * @return multitype:NULL | array
	 */
	function buscar_asesor($arrAtributoValor){
		$attrEntidad = array(
				array("nombre"=>"idasesor","tipo"=>"NUMBER","entidad"=>"a300")
				,array("nombre"=>"idpersona","tipo"=>"NUMBER","entidad"=>"a300")
				,array("nombre"=>"fechainicio","tipo"=>"DATE","entidad"=>"a300")
				,array("nombre"=>"fechafinal","tipo"=>"DATE","entidad"=>"a300")
				,array("nombre"=>"estado","tipo"=>"TEXT","entidad"=>"a300")
				,array("nombre"=>"usuario","tipo"=>"TEXT","entidad"=>"a300")
				,array("nombre"=>"fechasistema","tipo"=>"DATE","entidad"=>"a300")
				,array("nombre"=>"idtipoasesor","tipo"=>"NUMBER","entidad"=>"a300"));
		
		$filtroSql = Utilitario::obtenerFiltro($attrEntidad,$arrAtributoValor);
		$sql = "SELECT a300.*
				,a15.pnombre+' '+ISNULL(a15.snombre,'')+' '+a15.papellido+' '+ISNULL(a15.sapellido,'') AS asesor 
			FROM aportes300 a300 
				INNER JOIN aportes015 a15 ON a15.idpersona=a300.idpersona
		$filtroSql";
		return $this->fetchConsulta($sql);
	}
	
	/**
	 * Buscar el promotor por usuario, este debe estar activo
	 *
	 * FECHA_ACTUALIZACION: 2014-06-18
	 *
	 * @param unknown_type $usuario
	 * @return <multitype:array, multitype:NULL >
	 */
	function buscar_promotor_visita($arrAtributoValor){	
		$attrEntidad = array(
				array("nombre"=>"idpromotor","tipo"=>"NUMBER","entidad"=>"a301")
				,array("nombre"=>"idpersona","tipo"=>"NUMBER","entidad"=>"a301"));
		
		$filtroSql = Utilitario::obtenerFiltro($attrEntidad,$arrAtributoValor);
		$sql="SELECT
				a301.idpromotor, a301.idpersona,a15.pnombre,isnull(a15.snombre,'') AS snombre, a15.papellido, isnull(a15.sapellido,'') AS sapellido,a301.fechainicio,a301.fechafinal,a301.estado
			FROM aportes301 a301
				INNER JOIN aportes015 a15 ON a15.idpersona=a301.idpersona
			$filtroSql";
		
		return $this->fetchConsulta($sql);
	}
	/*function buscar_promotor_visita($idPromotor = 0, $idPersona = 0){
  		$sql="SELECT 
    				a301.idpromotor, a301.idpersona,a15.pnombre,isnull(a15.snombre,'') AS snombre, a15.papellido, isnull(a15.sapellido,'') AS sapellido,a301.fechainicio,a301.fechafinal,a301.estado
				FROM aportes301 a301
				INNER JOIN aportes015 a15 ON a15.idpersona=a301.idpersona";
  		if( intval($idPromotor) > 0 )
  			$sql .= " where a301.idpromotor=".$idPromotor; 
  		else if( intval($idPersona) > 0 )
  			$sql .= " where a15.idpersona=".$idPersona;
  		return self::$con->querySimple($sql);
	}*/
	
	/**
	 * Buscar el promotor por usuario, este debe estar activo
	 * 
	 * FECHA_ACTUALIZACION: 2014-06-18
	 * 
	 * @param unknown_type $usuario
	 * @return <multitype:array, multitype:NULL >
	 */
	function buscar_usuario_promotor($usuario){
		$sql= " SELECT a15.idpersona,a301.idpromotor,a15.pnombre + ' ' + isnull(a15.snombre,'') + ' ' + a15.papellido + ' ' + isnull(a15.sapellido,'') AS promotor,a301.fechainicio,a301.fechafinal,a301.estado
    			FROM aportes519 a519
					INNER JOIN aportes015 a15 ON a15.identificacion=a519.identificacion
					INNER JOIN aportes301 a301 ON a301.idpersona=a15.idpersona
				WHERE a519.usuario='$usuario'
					AND a301.estado='A'
					AND a301.fechainicio<= convert(Date,getDate()) AND a301.fechafinal>= convert(Date,getDate())";
		return $this->fetchConsulta($sql);
	}
	
	/**
	 * El parametro debe ser un array indice valor array("atributo"=>valor,...)
	 * 
	 * @param unknown_type $arrAtributoValor
	 * @return multitype:NULL | array
	 */
	function buscar_visita($arrAtributoValor){
		$attrEntidad = array(
				array("nombre"=>"idvisita","tipo"=>"TEXT","entidad"=>"a302")
				,array("nombre"=>"estado","tipo"=>"TEXT","entidad"=>"a302")
				,array("nombre"=>"idempresa","tipo"=>"NUMBER","entidad"=>"a302")
				,array("nombre"=>"idpromotor","tipo"=>"NUMBER","entidad"=>"a302")
				,array("nombre"=>"liquidacion","tipo"=>"TEXT","entidad"=>"a302")
				,array("nombre"=>"fechavisita","tipo"=>"DATE","entidad"=>"a302")
				,array("nombre"=>"nit","tipo"=>"TEXT","entidad"=>"a48"));
		
		$filtroSql = Utilitario::obtenerFiltro($attrEntidad,$arrAtributoValor);
		$sql="SELECT a302.*
				,a48.nit,a48.razonsocial
			FROM aportes302 a302
				INNER JOIN aportes048 a48 ON a48.idempresa=a302.idempresa
			$filtroSql
			ORDER BY fechavisita DESC";
		return $this->fetchConsulta($sql);
	}
	
	/*function buscar_visita_empresa($idempresa){
		$sql="select * from aportes302 where idempresa=$idempresa order by fechavisita desc";
		return self::$con->querySimple($sql);
	}*/
	
	function buscar_consecutivo_visita(){
		$sql="select MAX(consecutivo) consecutivo from aportes302";
		return self::$con->querySimple($sql)->fetchObject()->consecutivo;
	}
	
	function buscar_consecutivo_convenio(){
		$sql="select MAX(consecutivo) consecutivo from aportes306";
		return self::$con->querySimple($sql)->fetchObject()->consecutivo;
	}
	
	/*function buscar_liquidacion($idliquidacion){
		$sql="SELECT DISTINCT a305.*--,a303.iddetalle--,a303.periodos, a303.sueldos, a303.integral, a303.supernumerario, a303.festivos, a303.nocturnos, a303.antiguedad, a303.jornales, a303.comisiones, a303.vacaciones, a303.primavacaciones, a303.bonificaciones, a303.primasextralegales, a303.obradestajo, a303.viaticospermanentes, a303.remuneracionsocios, a303.contratosagricolas, a303.primaservicios, a303.primamanejo, a303.gastosrepresentacion, a303.bonificacionservicios, a303.subsidioalimentacion, a303.primatecnica, a303.horascatedra, a303.licenciasremuneradas, a303.indemnizacionvacaciones, a303.otros1, a303.otros2, a303.bruto, a303.aportes, a303.aportescancelados, a303.capital, a303.tasainteres, a303.interesmora, a303.neto
				FROM aportes305 a305
    				--INNER JOIN aportes303 a303 ON a303.idliquidacion=a305.idliquidacion
				WHERE a305.idliquidacion=$idliquidacion";
		return self::$con->querySimple($sql);
	}*/
	
	/**
	 * METODO: Obtiene los datos de la liquidacion
	 * 		El parametro debe ser un array indice valor array("atributo"=>valor,...)
	 * 
	 * FECHA_ACTUALIZACION: 2014-06-18
	 * 
	 * @param unknown_type $arrAtributoValor
	 * @return Ambigous <multitype:array, multitype:NULL >
	 */
	function buscar_liquidacion($arrAtributoValor){
		
		$attrEntidad = array(
				array("nombre"=>"idliquidacion","tipo"=>"NUMBER","entidad"=>"a305")
				,array("nombre"=>"idvisita","tipo"=>"TEXT","entidad"=>"a302")
				,array("nombre"=>"fechavisita","tipo"=>"DATE","entidad"=>"a302")
				,array("nombre"=>"estado","tipo"=>"TEXT","entidad"=>"a302")
				,array("nombre"=>"idagencia","tipo"=>"NUMBER","entidad"=>"a302")
				,array("nombre"=>"idpromotor","tipo"=>"NUMBER","entidad"=>"a301")
				,array("nombre"=>"idempresa","tipo"=>"NUMBER","entidad"=>"a48")
				,array("nombre"=>"nit","tipo"=>"TEXT","entidad"=>"a48"));
		$filtroSql = Utilitario::obtenerFiltro($attrEntidad,$arrAtributoValor);
		
		$sql="SELECT DISTINCT
					a305.idliquidacion,a305.fechaliquidacion,a305.idtiponotificacion,a305.fechanotificacion,a305.ibc,a305.observaciones,a305.documentacion,a305.indicador,a305.usuario,a305.fechasistema,a305.periodoletra,a305.periodonumero, a305.id_estado AS id_estado_liquidacion, a305.fecha_estado AS fecha_estado_liquidacion
					, a302.idvisita, a302.idpromotor, a302.fechavisita, a302.estado AS estadovisita, a302.fechaestado, a302.idagencia, a302.liquidacion, a302.numeroreitero,a302.consecutivo
					, a15.idpersona AS idpersonapromotor,a15.identificacion AS identificacionpromotor,a15.pnombre + ' ' + isnull(a15.snombre,'') + ' ' + a15.papellido + ' ' + isnull(a15.sapellido,'') AS promotor
					, a48.idempresa, a48.nit, a48.razonsocial
					, a91.detalledefinicion AS estado_liquidacion
				FROM aportes305 a305
					INNER JOIN aportes302 a302 ON a302.idvisita=a305.idvisita
					INNER JOIN aportes301 a301 ON a301.idpromotor=a302.idpromotor
					INNER JOIN aportes015 a15 ON a15.idpersona=a301.idpersona
					INNER JOIN aportes048 a48 ON a48.idempresa=a302.idempresa
					LEFT JOIN aportes091 a91 ON a91.iddetalledef=a305.id_estado
				$filtroSql
			ORDER BY a48.razonsocial";

		return $this->fetchConsulta($sql);
	}
	
	
	/**
	 * METODO: Obtiene los datos de la liquidacion
	 * 		El parametro debe ser un array indice valor array("atributo"=>valor,...)
	 * 
	 * FECHA_ACTUALIZACION: 2014-06-18
	 * 
	 * @param unknown_type $arrAtributoValor
	 * @return Ambigous <multitype:array, multitype:NULL >
	 */
	function buscar_detalle_liquidacion($arrAtributoValor){
	
		$attrEntidad = array(
				array("nombre"=>"idliquidacion","tipo"=>"NUMBER","entidad"=>"a305")
				,array("nombre"=>"iddetalle","tipo"=>"NUMBER","entidad"=>"a303")
				,array("nombre"=>"periodo","tipo"=>"TEXT","entidad"=>"a303")
				,array("nombre"=>"cancelado","tipo"=>"TEXT","entidad"=>"a303")
				,array("nombre"=>"idvisita","tipo"=>"TEXT","entidad"=>"a302")
				,array("nombre"=>"idpromotor","tipo"=>"NUMBER","entidad"=>"a301")
				,array("nombre"=>"idempresa","tipo"=>"NUMBER","entidad"=>"a48"));
		$filtroSql = Utilitario::obtenerFiltro($attrEntidad,$arrAtributoValor);
	
		$sql="SELECT DISTINCT
					a305.idliquidacion,a305.fechaliquidacion,a305.idtiponotificacion,a305.fechanotificacion,a305.ibc,a305.observaciones,a305.documentacion,a305.indicador,a305.usuario,a305.fechasistema,a305.periodoletra,a305.periodonumero, a305.id_estado AS id_estado_liquidacion, a305.fecha_estado AS fecha_estado_liquidacion
					, a303.iddetalle,a303.periodo,a303.bruto,a303.aportes,a303.neto,a303.usuario,a303.fechasistema AS fechasistemadetalle,a303.abono,a303.cancelado,a303.fechaabono, isnull(a303.observacion,'') AS observaciondetalle, a303.bruto_inicial, a303.aporte_inicial, a303.neto_inicial, a303.periodo_inicial
					, a302.idvisita, a302.idpromotor, a302.fechavisita, a302.estado AS estadovisita, a302.fechaestado, a302.idagencia, a302.liquidacion
					, a15.idpersona AS idpersonapromotor,a15.identificacion AS identificacionpromotor,a15.pnombre + ' ' + isnull(a15.snombre,'') + ' ' + a15.papellido + ' ' + isnull(a15.sapellido,'') AS promotor
					, a48.idempresa, a48.nit, a48.razonsocial
					, a91.detalledefinicion AS estado_liquidacion
				FROM aportes305 a305
					INNER JOIN aportes303 a303 ON a303.idliquidacion=a305.idliquidacion
					INNER JOIN aportes302 a302 ON a302.idvisita=a305.idvisita
					INNER JOIN aportes301 a301 ON a301.idpromotor=a302.idpromotor
					INNER JOIN aportes015 a15 ON a15.idpersona=a301.idpersona
					INNER JOIN aportes048 a48 ON a48.idempresa=a302.idempresa
					LEFT JOIN aportes091 a91 ON a91.iddetalledef=a305.id_estado
				$filtroSql";
		return $this->fetchConsulta($sql);
	}
	
	/**
	 * El parametro debe ser un array indice valor array("atributo"=>valor,...)
	 * @param unknown_type $arrAtributoValor
	 * @return multitype:NULL | array
	 */
	/*function buscar_liquidacion_empresa($arrAtributoValor){
		$attrEntidad = array(
				array("nombre"=>"idempresa","tipo"=>"NUMBER","entidad"=>"a302"));
		$filtroSql = Utilitario::obtenerFiltro($attrEntidad,$arrAtributoValor);
		
		$sql="SELECT DISTINCT a302.idvisita, a302.idpromotor,a302.estado
					, a303.iddetalle
				    , a305.idliquidacion, a305.fechaliquidacion a305.indicador
				    , a15.pnombre + ' ' + isnull(a15.snombre,'') + ' ' + a15.papellido + ' ' + isnull(a15.sapellido,'') AS promotor 
				FROM aportes302 a302
					INNER JOIN aportes305 a305 ON a305.idvisita=a302.idvisita
					INNER JOIN aportes303 a303 ON a303.idliquidacion=a305.idliquidacion
					INNER JOIN aportes301 a301 ON a301.idpromotor=a302.idpromotor
					INNER JOIN aportes015 a15 ON a15.idpersona=a301.idpersona
				$filtroSql
				ORDER BY a305.idliquidacion DESC";
		return $this->fetchConsulta($sql);
	}*/
	
	/**
	 * El parametro debe ser un array indice valor array("atributo"=>valor,...)
	 * @param unknown_type $arrAtributoValor
	 * @return multitype:NULL | array
	 */
	/*function buscar_liquidacion_periodo($arrAtributoValor){
		$attrEntidad = array(
				array("nombre"=>"idperiodoliquidacion","tipo"=>"NUMBER","entidad"=>"a309")
				,array("nombre"=>"iddetalleliquidacion","tipo"=>"NUMBER","entidad"=>"a309")
				,array("nombre"=>"periodo","tipo"=>"TEXT","entidad"=>"a309")
				,array("nombre"=>"cancelado","tipo"=>"TEXT","entidad"=>"a309")
				,array("nombre"=>"idliquidacion","tipo"=>"NUMBER","entidad"=>"a303"));
	
		$filtroSql = Utilitario::obtenerFiltro($attrEntidad,$arrAtributoValor);
		$sql = "SELECT a305.idliquidacion,a303.iddetalle,a309.*
				FROM aportes305 a305
					INNER JOIN aportes303 a303 ON a303.idliquidacion=a305.idliquidacion
					INNER JOIN aportes309 a309 ON a309.iddetalleliquidacion=a303.iddetalle
				$filtroSql
				ORDER BY a309.periodo";
		return $this->fetchConsulta($sql);
	}*/
	
	/** 
	 * Metodo para buscar las liquidaciones de la empresa
	 * El parametro debe ser un array indice valor array("atributo"=>valor,...)
	 * @param unknown_type $arrAtributoValor
	 * @return multitype:NULL | array
	 */
	/*function empresa_liquidacion($arrAtributoValor){
		$attrEntidad = array(
				array("nombre"=>"idempresa","tipo"=>"NUMBER","entidad"=>"a302")
				,array("nombre"=>"estado","tipo"=>"TEXT","entidad"=>"a302")
				,array("nombre"=>"nit","tipo"=>"TEXT","entidad"=>"a48")
				,array("nombre"=>"idvisita","tipo"=>"TEXT","entidad"=>"a302")
				,array("nombre"=>"fechavisita","tipo"=>"DATE","entidad"=>"a302")
				,array("nombre"=>"convenio","tipo"=>"TEXT","entidad"=>"a302")
				,array("nombre"=>"numeroreitero","tipo"=>"NUMBER","entidad"=>"a302")
				,array("nombre"=>"cancelado","tipo"=>"TEXT","entidad"=>"a309")
				,array("nombre"=>"periodo","tipo"=>"TEXT","entidad"=>"a309"));
	
		$filtroSql = Utilitario::obtenerFiltro($attrEntidad,$arrAtributoValor);
		echo $sql = "SELECT a302.idvisita,a302.fechavisita,a302.numeroreitero,a302.consecutivo,a302.convenio
					,a48.nit,a48.razonsocial
					,a305.idliquidacion
					,a309.idperiodoliquidacion,a309.periodo,ISNULL(a309.abono,0) AS abono,a309.cancelado
				FROM aportes302 a302
					INNER JOIN aportes048 a48 ON a48.idempresa=a302.idempresa
					INNER JOIN aportes305 a305 ON a305.idvisita=a302.idvisita
					INNER JOIN aportes303 a303 ON a303.idliquidacion=a305.idliquidacion
					INNER JOIN aportes309 a309 ON a309.iddetalleliquidacion=a303.iddetalle
			$filtroSql
			ORDER BY a302.idvisita";
		return $this->fetchConsulta($sql);
	}*/
	
	function empresa_sin_convenio(){
	 	$sql = "SELECT 
				   	a302.idvisita,a302.fechavisita,a302.fechapago, isnull( sum( a303.neto ), 0 ) AS valor
				    , a48.idempresa, a48.nit, a48.razonsocial, datediff( dd,getdate(),a302.fechapago ) AS plazo
				FROM aportes302 a302
				    LEFT JOIN aportes305 a305 ON a305.idvisita=a302.idvisita
				    LEFT JOIN aportes303 a303 ON a303.idliquidacion=a305.idliquidacion
				    INNER JOIN aportes048 a48 ON a48.idempresa=a302.idempresa
				WHERE a302.convenio='N' AND a302.estado='A'
				GROUP BY a302.idvisita, a302.fechavisita, a302.fechapago, a48.idempresa, a48.nit, a48.razonsocial";
	 	return self::$con->querySimple($sql);
	}
	
	//fecha: 2013-09-26
	/*function empresa_con_convenio(){
		$sql = "SELECT DISTINCT
					a48.idempresa, a48.nit, a48.razonsocial
				FROM aportes302 a302
					INNER JOIN aportes305 a305 ON a305.idvisita=a302.idvisita
					INNER JOIN aportes303 a303 ON a303.idliquidacion=a305.idliquidacion
					INNER JOIN aportes048 a48 ON a48.idempresa=a302.idempresa
				WHERE a302.convenio='S' AND a302.estado='A'";
		return self::$con->querySimple($sql);
	}*/
	
	/*function ultimo_seguimiento($idvisita){
		$sql="SELECT TOP 1 convert(varchar,fechasistema,103) + ' ' + hora as ultimo FROM aportes304  WHERE idvisita='".$idvisita."' ORDER BY fechasistema desc";
	 	return self::$con->querySimple($sql);
	}*/
	
	/**
	 * Buscar el historial del seguimientos por visita
	 * 
	 * El parametro debe ser un array indice valor array("atributo"=>valor,...)
	 * @param unknown_type $arrAtributoValor
	 * @return multitype:NULL | array
	 */
	function buscar_historial_seguimiento( $arrAtributoValor ){
		$attrEntidad = array(
				array("nombre"=>"idvisita","tipo"=>"NUMBER","entidad"=>"a304"));
		$filtroSql = Utilitario::obtenerFiltro($attrEntidad,$arrAtributoValor);
		
	  	$sql="SELECT * FROM aportes304 a304 $filtroSql ORDER BY fechasistema";
	   	return $this->fetchConsulta($sql);
	}

	function empresas_activas_pys(){
		/*Nota: validar para que salgan las visitas con liq pagas*/
		$sql = "SELECT
					a302.idvisita, a302.estado, a91.detalledefinicion AS causal, a302.liquidacion
					,a48.idempresa, a48.nit, a48.razonsocial, a500.agencia
				FROM aportes302 a302
					INNER JOIN aportes048 a48 ON a48.idempresa=a302.idempresa
					INNER JOIN aportes091 a91 ON a91.iddetalledef=a302.idcausal
					LEFT JOIN aportes500 a500 ON a500.idagencia=a302.idagencia
				WHERE a302.estado='A'
					AND 0=(
						SELECT COUNT(*)
						FROM aportes305 a305
							INNER JOIN aportes303 a303 ON a303.idliquidacion=a305.idliquidacion
							LEFT JOIN aportes308 a308 ON a308.idperiodoliquidacion=a303.iddetalle
							LEFT JOIN aportes307 a307 ON a307.iddetalleconvenio=a308.iddetalleconvenio
							LEFT JOIN aportes306 a306 ON a306.idconvenio=a307.idconvenio
						WHERE a305.idvisita=a302.idvisita AND (a303.cancelado='N' OR a306.estado='A')
					)
				ORDER BY a48.razonsocial, a302.idvisita";
		
		/*$sql = "SELECT 
				    a302.idvisita, a302.estado, a91.detalledefinicion AS causal, a302.convenio
				    ,a48.idempresa, a48.nit, a48.razonsocial
				FROM aportes302 a302
				    INNER JOIN aportes048 a48 ON a48.idempresa=a302.idempresa
				    INNER JOIN aportes091 a91 ON a91.iddetalledef=a302.idcausal
				WHERE a302.estado = 'A' 
					-- Verifica que no existan periodos sin cancelar
					AND 0=(
						SELECT count(*) 
						FROM aportes305 a305
							INNER JOIN aportes303 a303 ON a303.idliquidacion=a305.idliquidacion
							INNER JOIN aportes309 a309 ON a309.iddetalleliquidacion=a303.iddetalle
						WHERE a305.idvisita=a302.idvisita AND a309.cancelado='N'
						)
					-- Verifica que no exista saldo
						AND 0>=(
							isnull((SELECT sum(CASE WHEN (a309.valor-a309.abono)<0 THEN 0 ELSE (a309.valor-a309.abono) END)
							FROM aportes305 a305
								INNER JOIN aportes303 a303 ON a303.idliquidacion=a305.idliquidacion
								INNER JOIN aportes309 a309 ON a309.iddetalleliquidacion=a303.iddetalle
							WHERE a305.idvisita=a302.idvisita),0)
						)
				ORDER BY a48.razonsocial, a302.idvisita";*/
		
		return $this->fetchConsulta($sql);
	}	
	
	/**
	 * El parametro debe ser un array indice valor array("atributo"=>valor,...)
	 * @param unknown_type $arrAtributoValor
	 * @return multitype:NULL | array
	 */
	//function empresas_convenio($arrAtributoValor){
	function buscar_convenio($arrAtributoValor){
		$attrEntidad = array(
				array("nombre"=>"estado","tipo"=>"TEXT","entidad"=>"a306")
				,array("nombre"=>"idempresa","tipo"=>"NUMBER","entidad"=>"a302"));
		
		$filtroSql = Utilitario::obtenerFiltro($attrEntidad,$arrAtributoValor);
		$sql = "SELECT DISTINCT a48.idempresa,a48.razonsocial,a48.nit,a306.idconvenio,a306.consecutivo
						, a306.estado
				FROM aportes306 a306
					INNER JOIN aportes307 a307 ON a307.idconvenio=a306.idconvenio
					INNER JOIN aportes308 a308 ON a308.iddetalleconvenio=a307.iddetalleconvenio
					INNER JOIN aportes303 a303 ON a303.iddetalle=a308.idperiodoliquidacion
					INNER JOIN aportes305 a305 ON a305.idliquidacion=a303.idliquidacion
					INNER JOIN aportes302 a302 ON a302.idvisita=a305.idvisita
					INNER JOIN aportes048 a48 ON a48.idempresa=a302.idempresa
				$filtroSql
				ORDER BY a306.consecutivo ASC";
		return $this->fetchConsulta($sql);
	}

	//function convenio_empresa($idEmpresa){
	function buscar_fecha_convenio($arrAtributoValor){
		$attrEntidad = array(
				array("nombre"=>"idconvenio","tipo"=>"NUMBER","entidad"=>"a306")
				,array("nombre"=>"estado","tipo"=>"TEXT","entidad"=>"a306")
				,array("nombre"=>"iddetalleconvenio","tipo"=>"NUMBER","entidad"=>"a307")
				,array("nombre"=>"idempresa","tipo"=>"NUMBER","entidad"=>"a302"));
		
		$filtroSql = Utilitario::obtenerFiltro($attrEntidad,$arrAtributoValor);
		
		$sql = "SELECT DISTINCT a306.consecutivo, a306.valor, a306.idconvenio
					,a307.iddetalleconvenio, a307.fechapago,a307.iddetalleconvenio,a307.interes,a307.abonointeres,a307.cancelado,a307.fechacancelado
				FROM aportes306 a306
					INNER JOIN aportes307 a307 ON a307.idconvenio=a306.idconvenio
					INNER JOIN aportes308 a308 ON a308.iddetalleconvenio=a307.iddetalleconvenio
					INNER JOIN aportes303 a303 ON a303.iddetalle=a308.idperiodoliquidacion
					INNER JOIN aportes305 a305 ON a305.idliquidacion=a303.idliquidacion
					INNER JOIN aportes302 a302 ON a302.idvisita=a305.idvisita
					INNER JOIN aportes048 a48 ON a48.idempresa=a302.idempresa
				$filtroSql";
		return $this->fetchConsulta($sql);
		/*$sql = "SELECT DISTINCT a306.consecutivo, a306.valor, a306.idconvenio
					, isnull(a307.abono,0) AS abono
					, a307.fechapago,a307.iddetalleconvenio,a307.interes
				FROM aportes306 a306
					INNER JOIN aportes307 a307 ON a307.idconvenio=a306.idconvenio
					INNER JOIN aportes308 a308 ON a308.iddetalleconvenio=a307.iddetalleconvenio
					INNER JOIN aportes309 a309 ON a309.idperiodoliquidacion=a308.idperiodoliquidacion
					INNER JOIN aportes303 a303 ON a303.iddetalle=a309.iddetalleliquidacion
					INNER JOIN aportes305 a305 ON a305.idliquidacion=a303.idliquidacion
					INNER JOIN aportes302 a302 ON a302.idvisita=a305.idvisita
					INNER JOIN aportes048 a48 ON a48.idempresa=a302.idempresa
				WHERE a307.estado='A' AND a306.estado='A' AND a48.idempresa=$idEmpresa";
		return self::$con->querySimple($sql);*/
	}
	
	//function periodos_fecha_convenio($idFechaConvenio){
	function buscar_periodo_convenio($arrAtributoValor){
		$attrEntidad = array(
				array("nombre"=>"iddetalleconvenio","tipo"=>"NUMBER","entidad"=>"a308")
				,array("nombre"=>"idconvenio","tipo"=>"NUMBER","entidad"=>"a306"));
		
		$filtroSql = Utilitario::obtenerFiltro($attrEntidad,$arrAtributoValor);
		
		$sql = "SELECT a308.*,a303.periodo 
				FROM aportes306 a306
					INNER JOIN aportes307 a307 ON a307.idconvenio=a306.idconvenio
					INNER JOIN aportes308 a308 ON a308.iddetalleconvenio=a307.iddetalleconvenio
					INNER JOIN aportes303 a303 ON a303.iddetalle=a308.idperiodoliquidacion
					INNER JOIN aportes305 a305 ON a305.idliquidacion=a303.idliquidacion
					INNER JOIN aportes302 a302 ON a302.idvisita=a305.idvisita
					INNER JOIN aportes048 a48 ON a48.idempresa=a302.idempresa
				$filtroSql
				ORDER BY a303.periodo";
		return $this->fetchConsulta($sql);	
	}
	
	/**
	 * Metodo para buscar los periodos por convenio convenios
	 */
	/*public function buscar_valor_periodo_convenio($idConvenio){
		$sql = "SELECT a308.idperiodoliquidacion,sum(isnull(a308.valor,0)) AS valor
				FROM aportes306 a306
					INNER JOIN aportes307 a307 ON a307.idconvenio=a306.idconvenio
					INNER JOIN aportes308 a308 ON a308.iddetalleconvenio=a307.iddetalleconvenio
				WHERE a306.idconvenio=$idConvenio
				GROUP BY a308.idperiodoliquidacion";
		return $this->fetchConsulta($sql);
	}*/
	
	
	/**
	 * METODO: Obtiene los datos del abono del periodo liquidado
	 * 		El parametro debe ser un array indice valor array("atributo"=>valor,...)
	 *
	 *
	 * @param unknown_type $arrAtributoValor
	 * @return Ambigous <multitype:array, multitype:NULL >
	 */
	function buscar_abono_periodo_liquidado($arrAtributoValor){
	
		$attrEntidad = array(
				array("nombre"=>"idliquidacion","tipo"=>"NUMBER","entidad"=>"a305")
				,array("nombre"=>"iddetalle","tipo"=>"NUMBER","entidad"=>"a303")
				,array("nombre"=>"periodo","tipo"=>"TEXT","entidad"=>"a303")
				,array("nombre"=>"cancelado","tipo"=>"TEXT","entidad"=>"a303")
				,array("nombre"=>"id_abono_periodo","tipo"=>"NUMBER","entidad"=>"a310")
				,array("nombre"=>"numero_recibo","tipo"=>"NUMBER","entidad"=>"a310")
				,array("nombre"=>"estado","tipo"=>"TEXT","entidad"=>"a310")
				,array("nombre"=>"idvisita","tipo"=>"TEXT","entidad"=>"a302")
				,array("nombre"=>"idpromotor","tipo"=>"NUMBER","entidad"=>"a301")
				,array("nombre"=>"idempresa","tipo"=>"NUMBER","entidad"=>"a48"));
		$filtroSql = Utilitario::obtenerFiltro($attrEntidad,$arrAtributoValor);
	
		$sql="SELECT DISTINCT
					a305.idliquidacion,a305.fechaliquidacion,a305.idtiponotificacion,a305.fechanotificacion,a305.ibc,a305.observaciones,a305.documentacion,a305.indicador,a305.usuario,a305.fechasistema,a305.periodoletra,a305.periodonumero, a305.id_estado AS id_estado_liquidacion, a305.fecha_estado AS fecha_estado_liquidacion
					, a303.iddetalle,a303.periodo,a303.bruto,a303.aportes,a303.neto,a303.usuario,a303.fechasistema AS fechasistemadetalle,a303.abono,a303.cancelado,a303.fechaabono, isnull(a303.observacion,'') AS observaciondetalle, a303.bruto_inicial, a303.aporte_inicial, a303.neto_inicial, a303.periodo_inicial
					, a310.id_abono_periodo, a310.abono AS abono_discriminado, a310.numero_recibo, a310.fecha_abono AS fecha_abono_discriminado, a310.id_periodo_liquidacion, a310.id_kardex, a310.estado AS estado_abono, a310.id_causal_estado, a310.valor_reverso, a310.fecha_reverso, a310.observacion AS observacion_abono, a310.usuario_reverso
					, a302.idvisita, a302.idpromotor, a302.fechavisita, a302.estado AS estadovisita, a302.fechaestado, a302.idagencia, a302.liquidacion
					, a15.idpersona AS idpersonapromotor,a15.identificacion AS identificacionpromotor,a15.pnombre + ' ' + isnull(a15.snombre,'') + ' ' + a15.papellido + ' ' + isnull(a15.sapellido,'') AS promotor
					, a48.idempresa, a48.nit, a48.razonsocial
					, a91.detalledefinicion AS estado_liquidacion
					, b91.detalledefinicion AS causal_estado_abono
			FROM aportes305 a305
				INNER JOIN aportes303 a303 ON a303.idliquidacion=a305.idliquidacion
				INNER JOIN aportes310 a310 ON a310.id_periodo_liquidacion=a303.iddetalle
				INNER JOIN aportes302 a302 ON a302.idvisita=a305.idvisita
				INNER JOIN aportes301 a301 ON a301.idpromotor=a302.idpromotor
				INNER JOIN aportes015 a15 ON a15.idpersona=a301.idpersona
				INNER JOIN aportes048 a48 ON a48.idempresa=a302.idempresa
				LEFT JOIN aportes091 a91 ON a91.iddetalledef=a305.id_estado
				LEFT JOIN aportes091 b91 ON b91.iddetalledef=a310.id_causal_estado
		$filtroSql";
		return $this->fetchConsulta($sql);
	}
	
	/**
	 * METODO: Obtiene los datos del cierre
	 * 		El parametro debe ser un array indice valor array("atributo"=>valor,...)
	 *
	 *
	 * @param unknown_type $arrAtributoValor
	 * @return Ambigous <multitype:array, multitype:NULL >
	 */
	function buscar_cierre($arrAtributoValor){
	
		$attrEntidad = array(
				array("nombre"=>"id_cierre","tipo"=>"NUMBER","entidad"=>"a315")
				, array("nombre"=>"periodo","tipo"=>"TEXT","entidad"=>"a315")
				, array("nombre"=>"fecha_cierre","tipo"=>"TEXT","entidad"=>"a315")				
			);
		$filtroSql = Utilitario::obtenerFiltro($attrEntidad,$arrAtributoValor);
	
		$sql="SELECT a315.id_cierre, a315.periodo, a315.fecha_cierre, a315.id_estado, a315.usuario_creacion, a315.usuario_modificacion, a315.fecha_creacion, a315.fecha_modificacion
					, a91.detalledefinicion AS estado
				FROM aportes315 a315
					LEFT JOIN aportes091 a91 ON a91.iddetalledef=a315.id_estado
			$filtroSql";
		return Utilitario::fetchConsulta($sql,self::$con);
	}
	
	/**
	 * METODO: Obtiene los datos del permiso cierre
	 * 		El parametro debe ser un array indice valor array("atributo"=>valor,...)
	 *
	 * @param unknown_type $arrAtributoValor
	 * @return Ambigous <multitype:array, multitype:NULL >
	 */
	function buscar_permiso_cierre($arrAtributoValor){
	
		$attrEntidad = array(
				array("nombre"=>"id_permiso_cierre","tipo"=>"NUMBER","entidad"=>"a316")
				, array("nombre"=>"usuario","tipo"=>"TEXT","entidad"=>"a316")
				, array("nombre"=>"estado","tipo"=>"TEXT","entidad"=>"a316"));
		$filtroSql = Utilitario::obtenerFiltro($attrEntidad,$arrAtributoValor);
	
		$sql="SELECT id_permiso_cierre, usuario, fecha_inicial, fecha_final, estado, usuario_creacion, usuario_modificacion, fecha_creacion, fecha_modificacion
			FROM aportes316 a316
			$filtroSql";
		return Utilitario::fetchConsulta($sql,self::$con);
	}
	
	/**
	 * METODO: Obtiene los datos de la causal para el ajuste de los periodos liquidados
	 * 		El parametro debe ser un array indice valor array("atributo"=>valor,...)
	 *
	 *
	 * @param unknown_type $arrAtributoValor
	 * @return Ambigous <multitype:array, multitype:NULL >
	 */
	function buscar_causal_ajuste($arrAtributoValor){
	
		$attrEntidad = array(
				array("nombre"=>"id_causal_ajuste","tipo"=>"NUMBER","entidad"=>"a320")
				,array("nombre"=>"codigo","tipo"=>"TEXT","entidad"=>"a320")
				,array("nombre"=>"nombre","tipo"=>"TEXT","entidad"=>"a320")
				,array("nombre"=>"estado","tipo"=>"TEXT","entidad"=>"a320"));
		$filtroSql = Utilitario::obtenerFiltro($attrEntidad,$arrAtributoValor);
	
		$sql="SELECT id_causal_ajuste, codigo, nombre, descripcion, estado
			FROM aportes320 a320
			$filtroSql";
		return Utilitario::fetchConsulta($sql,self::$con);
	}
	
	
	/**
	 * METODO: Verificar si el usuario tiene permisos para realizar modificaciones respecto al cierre
	 * 		El parametro debe ser un array indice valor array("atributo"=>valor,...)
	 *
	 *
	 * @param unknown_type $arrAtributoValor
	 * @return boolean
	 */
	function verificar_permiso_cierre($arrAtributoValor){
		$resultado = true;
		
		//Consultar si el periodo esta cerrado
		$sql = "SELECT id_cierre,periodo,fecha_cierre, dateadd(d,-1,dateadd(m,1,periodo+'01')) 
				FROM aportes315 
				WHERE dateadd(d,-1,dateadd(m,1,convert(DATE,(periodo+'01'))))>='{$arrAtributoValor["fecha"]}'
					AND id_estado=(
							SELECT TOP 1 a91.iddetalledef 
							FROM aportes090 a90
								INNER JOIN aportes091 a91 ON a91.iddefinicion=a90.iddefinicion
							WHERE a90.codigo='ESTADO_CIERRE_PROMOTORIA' AND a91.codigo='EST_CERRA'
						)";
		
		$dataEstadoCierre = Utilitario::fetchConsulta($sql,self::$con);
		if(count($dataEstadoCierre)==0){
			//Consultar si el usuario tiene permisos
			$sql = "SELECT id_permiso_cierre, usuario, fecha_inicial, fecha_final, estado
					FROM aportes316
					WHERE usuario='{$arrAtributoValor["usuario"]}'
						AND GETDATE() BETWEEN fecha_inicial AND fecha_final
						AND estado='A'";
			$dataPermisoUsuario = Utilitario::fetchConsulta($sql,self::$con);
			
			if(count($dataPermisoUsuario)==0){
				$sql="SELECT id_cierre,periodo,fecha_cierre, dateadd(d,-1,dateadd(m,1,periodo+'01')) 
						FROM aportes315
						WHERE dateadd(d,-1,dateadd(m,1,convert(DATE,(periodo+'01'))))>='{$arrAtributoValor["fecha"]}'";
				$dataCierre = Utilitario::fetchConsulta($sql,self::$con);
				
				if(count($dataCierre)>0){
					$resultado = false;
				}
			}
					
		}else{
			$resultado = false;
		}
				
		return $resultado;
	}
	
	/**
	 * Retorna los valores obtenidos en la consulta
	 * @return multitype:array
	 */
	private function fetchConsulta($querySql){
		$resultado = array();
		$rs = self::$con->querySimple($querySql);
		while($row = $rs->fetch())
			$resultado[] = array_map("trim",$row);
			//$resultado[] = array_map("utf8_encode",$row);
		return $resultado;
	}
	
	/**
	 * Ejecuta el procedimiento para preparar los datos del cierre
	 *
	 * @param unknown_type $periodo
	 * * @param unknown_type $usuario
	 * @return {int} ( 	0 => No se pudo realizar el procedimiento,
	 * 					1 => El procedimiento se ejecuto correctamente)
	 */
	function ejecutar_sp_generar_datos_cierre($periodo,$usuario){
		$resultado = 0;
		$sentencia = self::$con->conexionID->prepare ( "EXEC [promotoria].[sp_000_Generar_Datos_Cierre]
				@periodo = '$periodo'
				, @usuario = '$usuario'
				, @resultado = :resultado" );
		$sentencia->bindParam ( ":resultado", $resultado, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000 );
		$sentencia->execute ();
		if ($resultado > 0)
			return 1;
		else
			return 0;
	}
	
	/**
	 * Ejecuta el procedimiento almacenado que almacena los periodos
	 * en letras y numeros en la entidad de la liquidacion
	 * 
	 * @param unknown_type $idLiquidacion
	 * @return {int} ( 	0 => No se pudo realizar el procedimiento,
	 * 					1 => El procedimiento se ejecuto correctamente)
	 */
	function ejecutar_sp_observacion_liq($idLiquidacion){
		$resultado = 0;
		$sentencia = self::$con->conexionID->prepare ( "EXEC [kardex].[sp_020_Observacion_Liquidacion]
				@id_liquidacion = $idLiquidacion
				, @resultado = :resultado" );
		$sentencia->bindParam ( ":resultado", $resultado, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000 );
		$sentencia->execute ();
		if ($resultado > 0)
			return 1;
		else
			return 0;
	}
}
?>