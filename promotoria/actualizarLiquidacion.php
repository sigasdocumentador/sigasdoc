<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz. DIRECTORY_SEPARATOR .'clases' . DIRECTORY_SEPARATOR . 'p.definiciones.class.php';
include_once 'clases' . DIRECTORY_SEPARATOR . 'promotoria.class.php';

$objPromotoria=new Promotoria;
$objDatosDetalle = isset($_POST["objDatosDetalle"])?$_POST["objDatosDetalle"]:array();
$objDatosDetalElimi = isset($_POST["objDatosDetalElimi"])?$_POST["objDatosDetalElimi"]:array();
$objDatosLiquidacion = $_POST["objDatosLiquidacion"];

$objDefiniciones = new Definiciones();

//Obtener id_causal_estado del abono
$dataDefinicion = $objDefiniciones->buscar_definicion_detalle_definicion(
		array("codigo_definicion"=>"CAUSAL_ABONO_PERIODO_LIQ"
				,"codigo_detalle_definicion"=>"ACTUA_LIQ"));

$idCausalEstadoAbonoActualizacion = 0;
if(count($dataDefinicion)>0){
	$idCausalEstadoAbonoActualizacion = $dataDefinicion[0]["iddetalledef"];
}

$dataDefinicion = $objDefiniciones->buscar_definicion_detalle_definicion(
		array("codigo_definicion"=>"CAUSAL_ABONO_PERIODO_LIQ"
				,"codigo_detalle_definicion"=>"ANULA_LIQ"));

$idCausalEstadoAbonoAnulacion = 0;
if(count($dataDefinicion)>0){
	$idCausalEstadoAbonoAnulacion = $dataDefinicion[0]["iddetalledef"];
}

$usuario = $_SESSION["USUARIO"];

$banderaError = 0;
$objPromotoria->inicioTransaccion();

//Actualizar detalle liquidacion y abono periodo liquidacion
if($banderaError==0){
	foreach ($objDatosDetalle as $dataDetalle){
		$rsActualiza = 0;
		
		if($dataDetalle["iddetalle"]>0){			
			
			//Actualizar el detalle de la liquidacion
			$rsActualiza = $objPromotoria->actualizar_detalle_liquidacion_completa( $dataDetalle, $usuario );
			if($rsActualiza>0){
				//Inactivar los abonos
				$arrData = array("estado"=>'I',"id_causal_estado"=>$idCausalEstadoAbonoActualizacion);
				$arrFiltro = array("id_periodo_liquidacion"=>$dataDetalle["iddetalle"]);
				
				$rsActualiza = $objPromotoria->actuliza_estado_abono_periodo_liquidacion($arrData,$arrFiltro,$usuario); 
			}
				
		}else{ 
			//Guardar el detalle liquidacion
			$rsActualiza = $objPromotoria->guardar_detalle_liquidacion( $dataDetalle, $usuario );
		}
			
		if($rsActualiza==0){
			$banderaError++;
			break;
		}
	}
}


//Eliminar detalle liquidacion, abono periodo liquidacion
if($banderaError==0){	
	
	//Eliminar el detalle liquidacion
	foreach ($objDatosDetalElimi as $datoDetalElimi){
		$rsActualizaElimina = 0;	
		
		//Actualizar la observacion de los periodos a eliminar. Con el fin de almacenar la observacion en la auditoria
		$rsActualizaElimina = $objPromotoria->actualizar_detalle_liquidacion_completa( $datoDetalElimi, $usuario );
		if($rsActualizaElimina==0){
			$banderaError++;
			break;
		}
		
		$rsActualiza = $objPromotoria->eliminar_detalle_liquidacion( $datoDetalElimi["iddetalle"] );
		if($rsActualiza==0){
			$banderaError++;
			break;
		}
		
		//Eliminar el abono al periodo
		$arrFiltro = array("id_periodo_liquidacion"=>$datoDetalElimi["iddetalle"]);
		$rsEliminarAbono = $objPromotoria->eliminar_abono_periodo_liquidacion($arrFiltro);
		if($rsEliminarAbono==0){
			$banderaError++;
			break;
		}
	}
}

//Actualizar el estado de la liquidacion
if($banderaError==0){
	
	//Consultar para ver si es diferente
	$arrDatos = $objPromotoria->buscar_liquidacion(array("idliquidacion"=>$objDatosLiquidacion["idliquidacion"]));
	
	if(count($arrDatos)>0){
		
		if($arrDatos[0]["id_estado_liquidacion"]!=$objDatosLiquidacion["id_estado"]){
			
			if($objPromotoria->actualizar_estado_liquidacion($objDatosLiquidacion,$usuario)==0)
				$banderaError = 1;
		}
			
	}else{
		$banderaError = 1;
	}
}

//Actualizar el periodo liquidado si el estado de la liquidacion es anulada
if($banderaError==0 && $objDatosLiquidacion["codigo_estado"]=="ANULADA"){

	//Obtener los detalles de la liquidacion
	$arrDataDetalle = $objPromotoria->buscar_detalle_liquidacion(array("idliquidacion"=>$objDatosLiquidacion["idliquidacion"]));
	
	if(count($arrDataDetalle)==0){
		$banderaError = 1;
	}
	
	foreach($arrDataDetalle as $dataDetalle){
		//Los periodos que no estan cancelados no se actualizan
		if($dataDetalle["abono"]==0) continue;
		
		$arrDataActualiza = array(
				"iddetalle"=>$dataDetalle["iddetalle"]
				, "periodo"=>$dataDetalle["periodo"]
				, "periodo_inicial"=>$dataDetalle["periodo_inicial"]
				, "bruto"=>$dataDetalle["bruto"]
				, "aportes"=>$dataDetalle["aportes"]
				, "neto"=>$dataDetalle["neto"]
				, "abono"=>0
				, "cancelado"=>'N'
				, "fechaabono"=>''
				, "bruto_inicial"=>($dataDetalle["bruto_inicial"]>0?$dataDetalle["bruto_inicial"]:0)
				, "aporte_inicial"=>($dataDetalle["aporte_inicial"]>0?$dataDetalle["aporte_inicial"]:0)
				, "neto_inicial"=>($dataDetalle["neto_inicial"]>0?$dataDetalle["neto_inicial"]:0)
				, "observacion"=>$dataDetalle["observaciondetalle"]
		);
		
		//Actualizar el periodo
		if($objPromotoria->actualizar_detalle_liquidacion_completa($arrDataActualiza,$usuario)>0){
			
			//Inactivar los abonos
			$arrData = array("estado"=>'I',"id_causal_estado"=>$idCausalEstadoAbonoAnulacion);
			$arrFiltro = array("id_periodo_liquidacion"=>$dataDetalle["iddetalle"]);
		
			if($objPromotoria->actuliza_estado_abono_periodo_liquidacion($arrData,$arrFiltro,$usuario)==0){
				$banderaError = 1;
				break;
			}
		}else{
			$banderaError = 1;
			break;
		}
	}
}

if($banderaError==0){
	//Confirmar transaccion
	$objPromotoria->confirmarTransaccion();
	echo 1;
}else{
	//Cancelar transaccion
	$objPromotoria->cancelarTransaccion();
	echo 0;
}
?>