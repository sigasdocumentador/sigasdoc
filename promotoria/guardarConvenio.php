<?php
	$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
	include_once  $root;
	include_once 'clases' . DIRECTORY_SEPARATOR . 'promotoria.class.php';
	
	$objPromotoria = new Promotoria;
	
	$datos = Array();
	$datosDetalle = Array();
	$datosPeriodo = Array();
	
	$baderaConvenio = $_POST["baderaConvenio"];
	$datos["valor"] = $_POST["valorAcuerdo"];
	$datos["indicador"] = $_POST["indice"];
	$idConvenio = $_POST["idConvenio"];
	//$objPeriodo = json_decode($_POST["periodo"]);
	$objPeriodo = $_POST["periodo"];
	
	$datos["usuario"] = $_SESSION["USUARIO"];
	
	$objPromotoria->inicioTransaccion();
	$rsConvenio = 0;
	if( $baderaConvenio == "verdadero" ){
		
		$datos["consecutivo"] = $objPromotoria->buscar_consecutivo_convenio();
		$datos["consecutivo"] = $datos["consecutivo"] > 0 ? $datos["consecutivo"] + 1 : 1; 
		$datosDetalle["idconvenio"] = $objPromotoria->guardar_convenio( $datos );
	}

	$datosDetalle["idconvenio"] = isset($datosDetalle["idconvenio"])?$datosDetalle["idconvenio"]:$idConvenio;
	
	if($datosDetalle["idconvenio"]>0){
		$datosDetalle["fechapago"] = $_POST["fechaPago"];
		$datosDetalle["interes"] = $_POST["valorInteres"];
		$datosDetalle["usuario"] = $_SESSION["USUARIO"];
			
		$idDetalleConvenio = $objPromotoria->guardar_fecha_convenio( $datosDetalle );
		
		if( $idDetalleConvenio > 0 ){
			$datosPeriodo["iddetalleconvenio"] = $idDetalleConvenio;
			$rsPeriodo = 0;
			$banderaPeriodo = 0;
			foreach ($objPeriodo as $arrayPeriodo){				
				//$datosPeriodo["periodo"] = $arrayPeriodo->periodo;
				//$datosPeriodo["idliquidacion"] = $arrayPeriodo->idLiquidacion;
				$datosPeriodo["idperiodoliquidacion"] = $arrayPeriodo["idPeriodo"];
				$datosPeriodo["valor"] = $arrayPeriodo["valor"];
				$datosPeriodo["usuario"] = $_SESSION["USUARIO"];
				
				$rsPeriodo = $objPromotoria->guardar_periodo_convenio($datosPeriodo);
				if( $rsPeriodo == 0 ){
					$banderaPeriodo++;
					break;
				}
			}
			if($banderaPeriodo == 0){
				$objPromotoria->confirmarTransaccion();
				echo $datosDetalle["idconvenio"];
			}else{
				$objPromotoria->cancelarTransaccion();
				echo 0;
			}
		}else{
			$objPromotoria->cancelarTransaccion();
			echo 0;
		}
	}else if( $baderaConvenio == "verdadero" ){
		$objPromotoria->cancelarTransaccion();
		echo 0;
	}
?>