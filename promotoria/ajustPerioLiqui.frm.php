<?php	
	set_time_limit(0);
	ini_set("display_errors",'1');
	date_default_timezone_set("America/Bogota");
	$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
	include_once  $root;
	include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';
	
	$url=$_SERVER['PHP_SELF'];
	include_once $raiz.DIRECTORY_SEPARATOR.'phpComunes'.DIRECTORY_SEPARATOR.'auditoria.php';
	auditar($url);
	
	//Obtener la fecha de la version
	$fecver = date('Ymd h:i:s A',filectime('ajustPerioLiqui.frm.php'));
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<title>::Promotoria | Ajuste::</title>
		
		<link type="text/css" href="<?php echo URL_PORTAL; ?>newcss/Estilos.css" rel="stylesheet">
		<link type="text/css" href="<?php echo URL_PORTAL; ?>newcss/css.1.8/jquery-ui-1.8.17.custom.css" rel="stylesheet">
		<link type="text/css" href="<?php echo URL_PORTAL; ?>newcss/marco.css" rel="stylesheet">
		
		<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/1.6/jquery-1.6.2.min.js"></script>
		<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/1.6/jquery-ui-1.8.16.custom.min.js"></script>
		<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/comunes.js"></script>
		<script type="text/javascript" src="js/ajustPerioLiqui.js"></script>
		
	</head>
	<body>
		<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0">
			<tbody>
			
			  	<!-- ESTILOS SUPERIOR TABLA-->
			  	<tr>
				    <td width="13" height="29" class="arriba_iz">&nbsp;</td>
				    <td class="arriba_ce">
				    	<span class="letrablanca">::&nbsp;Ajuste Periodo liquidacion&nbsp;::</span>
				    	<div style="text-align:right;float:right;">
								<?php echo 'Versi&oacute;n: ' . $fecver; ?>
						</div>
				    </td>
				    <td width="13" class="arriba_de" align="right">&nbsp;</td>
			  	</tr>
		  	
		  	<!-- ESTILOS ICONOS TABLA-->
			  	<tr>
			    	<td class="cuerpo_iz">&nbsp;</td>
			    	<td class="cuerpo_ce">
						<img src="../imagenes/menu/nuevo.png" name="btnNuevo" id="btnNuevo" width="16" height="16" style="cursor:pointer" title="Nuevo"/>
						<img width="3" height="1" src="../imagenes/spacer.gif" />
						<img src="../imagenes/menu/grabar.png" name="btnGuardar" id="btnGuardar" width="16"  height=16 style="cursor:pointer" title="Guardar"/>     
			    	</td>
			    	<td class="cuerpo_de">&nbsp;</td>
			  	</tr>
			  	
		  	<!-- ESTILOS MEDIO TABLA-->
			  	<tr>
			    	<td class="cuerpo_iz">&nbsp;</td>
			    	<td class="cuerpo_ce"></td>
			    	<td class="cuerpo_de">&nbsp;</td>
			  	</tr>  
			  	
		  	<!-- CONTENIDO TABLA-->
			  	<tr>
			   		<td class="cuerpo_iz">&nbsp;</td>
			   		<td class="cuerpo_ce">
						<table cellspacing="0" width="90%" border="0" class="tablero" align="center">
			      			<tbody>
				    			<tr>
				      				<td ># Liquidaci&oacute;n</td>
				      				<td >
				      					<input type="text" name="txtNumeroLiquidacion" id="txtNumeroLiquidacion" class="boxfecha element-required"/>
				      					&nbsp;&nbsp;<input type="button" name="btnBuscar" id="btnBuscar" value="Buscar"/>
				      					
				      					<input type="hidden" name="hidIdVisita" id="hidIdVisita"/>
				      					<input type="hidden" name="hidIndicadorLiquidacion" id="hidIndicadorLiquidacion"/>
				      					<input type="hidden" name="hidFechaVisita" id="hidFechaVisita"/>
				      					<input type="hidden" name="hidUsuario" id="hidUsuario" value="<?php echo $_SESSION["USUARIO"];?>"/>
				      				</td>
				        		</tr>
				        		<tr>
				      				<td>Datos Visita</td>
				      				<td id="tdDatosVisita"></td>
				        		</tr>
				        		<tr>
				      				<td>Razon Social</td>
				      				<td id="tdRazonSocial"></td>
				        		</tr>
				        		<tr>
				      				<td>Causal Ajuste</td>
				      				<td >
				      					<select name="cmbCausalAjuste" id="cmbCausalAjuste" class="boxmediano element-required">
				      						<option value="">--SELECCIONES--</option>
				      					</select>
				      					<span class="clsObligatorio">&nbsp;&nbsp;&nbsp;</span>
				      				</td>
				        		</tr>
				        		<tr>
				      				<td >Fecha Ajuste</td>
				      				<td >
				      					<input type="text" name="txtFechaAjuste" id="txtFechaAjuste" class="boxfecha element-required"/>
				      					<span class="clsObligatorio">&nbsp;&nbsp;&nbsp;</span>
				      				</td>
				        		</tr>
				        		<tr>
				      				<td >Observaci&oacute;n</td>
				      				<td >
				      					<textarea name="txaObservacion" id="txaObservacion" class="element-required" cols="70" rows="3" class="element-required"></textarea>
				      					<span class="clsObligatorio">&nbsp;&nbsp;&nbsp;</span>
				      				</td>
				        		</tr>
				        		<tr>
				        			<td colspan="2">&nbsp;</td>
				        		</tr>
				      			<tr>
				      				<td colspan="2">
				      					<table align="center" id="tblPeriodo" class="tablero" width="70%">
							      			<thead>
								         		<tr>
								         			<th>PERIODO</th>
								         			<th>APORTE</th>
								         			<th>BASE LIQ</th>
								         			<th>TOTAL</th>
								         		</tr>
								         	</thead>
								         	<tbody>
							         		</tbody>  
							    		</table>
				      				</td>
				      			</tr>
				      			<tr>
				        			<td colspan="2">&nbsp;</td>
				        		</tr>
			      			</tbody>
			      		</table>
					</td>
		    		<td class="cuerpo_de">&nbsp;</td>
		  		</tr>		  
	  		<!-- ESTILOS PIE TABLA-->
		  		<tr>
		    		<td class="abajo_iz" >&nbsp;</td>
		    		<td class="abajo_ce" ></td>
		    		<td class="abajo_de" >&nbsp;</td>
		  		</tr>		  
			</tbody>
		</table>
	</body>
</html>