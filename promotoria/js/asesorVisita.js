$(function(){
	asesores();
	
	$("#txtNoVisita").blur(function(){
		nuevo();
		var idVisita = $(this).val().trim();
		if(!idVisita){return false;}
		
		//Buscar los datos de la visita
		var datosVisita = buscarVisita(idVisita);
		if(typeof datosVisita == "object"){
			datosVisita = datosVisita[0];
			
			//Asignar los datos al formulario
			$("#lblNit").text(datosVisita.nit);
			$("#lblRazonSocial").text(datosVisita.razonsocial);
			
			//Verificar si la visita ya tiene asesores asignados
			if(parseInt(datosVisita.idabogadointerno)>0){
				$("#cmbAsesorJuridicoI").attr("disabled",true).val(datosVisita.idabogadointerno);
			}
			if(parseInt(datosVisita.idabogadoexterno)>0){
				$("#cmbAsesorJuridicoE").attr("disabled",true).val(datosVisita.idabogadoexterno);
			}
		}else{
			//No existe la visita
			$("#tdMensaje").html("<b style='color:red;'>La visita no existe o esta a paz y salvo</b>");
		}
	});
});

function asesores(){
	var htmlAsesorJuridicoI = "",
		htmlAsesorJuridicoE = "",
		datosAsesor;

	//Buscar historial seguimiento
	datosAsesor = buscarAsesor();
	if(typeof datosAsesor == "object"){
		$.each(datosAsesor,function(i,row){
			//[4225]ASESOR JURIDICO INTERNO
			if(row.idtipoasesor==4225){
				htmlAsesorJuridicoI += "<option value='"+row.idasesor+"'>"+row.asesor+"</option>";
			
			//[4226]ASESOR JURIDICO EXTERNO
			}else if(row.idtipoasesor==4226){
				htmlAsesorJuridicoE += "<option value='"+row.idasesor+"'>"+row.asesor+"</option>";
			}					
		});
		
		//Adicionar los asesores al formulario
		$("#cmbAsesorJuridicoI").append(htmlAsesorJuridicoI);
		$("#cmbAsesorJuridicoE").append(htmlAsesorJuridicoE);
	}
}

/**
 * Obtener los datos de la visita
 * @param idVisita
 * @returns
 */
function buscarVisita(idVisita){
	var resultado = null;
	$.ajax({
		url:"buscarVisita.php",
		type:"POST",
		data:{objDatosFiltro:{idvisita:idVisita,estado:"A"}},
		dataType:"json",
		async:false,
		success:function(datos){
			resultado=datos;
		}
	});
	return resultado;
}

/**
 * Obtener los datos de la visita
 * @param idVisita
 * @returns
 */
function buscarAsesor(){
	var resultado = null;
	$.ajax({
		url:"buscarAsesor.php",
		type:"POST",
		data:{objDatosFiltro:{estado:"A"}},
		dataType:"json",
		async:false,
		success:function(datos){
			resultado=datos;
		}
	});
	return resultado;
}

function guardar(){
	var idVisita = $("#txtNoVisita").val().trim();
	var idAsesorJuridicoI = $("#cmbAsesorJuridicoI").val().trim();
	var idAsesorJuridicoE = $("#cmbAsesorJuridicoE").val().trim();
	
	//Remover class error de los campos
	$(".ui-state-error").removeClass("ui-state-error");
	
	if(!idVisita){$("#txtNoVisita").addClass("ui-state-error");return false;}
	
	//Validar si los asesores ya estan almacenados
	if($("#cmbAsesorJuridicoI").is(":disabled") && $("#cmbAsesorJuridicoE").is(":disabled")){
		alert("Los asesores ya estan almacenados.");
		return false;
	}
	//Validar que seleccione algun asesor
	if(($("#cmbAsesorJuridicoI").is(":disabled") || idAsesorJuridicoI==0) 
			&& ( $("#cmbAsesorJuridicoE").is(":disabled") || idAsesorJuridicoE==0)){
		if(idAsesorJuridicoI==0){$("#cmbAsesorJuridicoI").addClass("ui-state-error");return false;}
		if(idAsesorJuridicoE==0){$("#cmbAsesorJuridicoE").addClass("ui-state-error");return false;}	
	}
	
	$.ajax({
		url:"guardarAsesorVisita.php",
		type:"POST",
		data:{idVisita:idVisita,idAsesorJuridicoI:idAsesorJuridicoI,idAsesorJuridicoE:idAsesorJuridicoE},
		dataType:"json",
		async:false,
		success:function(datos){
			if(datos==0){
				alert("Error al guardar los asesores");
				return;
			}
			alert("Los asesores se guardaron correctamente");
			$("#txtNoVisita").val("");
			nuevo();
		}
	});
}

function nuevo(){
	$("#lblNit,#lblRazonSocial").text("");
	$("#tdMensaje").html("");
	$("#cmbAsesorJuridicoE,#cmbAsesorJuridicoI").attr("disabled",false).val(0);
}