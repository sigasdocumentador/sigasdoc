$(function(){
	$("#btnEjecutar").button();
	$("#btnEjecutar").click(ejecutar);
});

function ejecutar(){
	var idVisita = $("#txtNoVisita").val().trim();
	
	if(idVisita==0){
		alert("Debe digitar el numero de la visita!!");
		return false;
	}
	
	$.ajax({
		url:'ejecuCancePerioLiquiTemp.php',
		type:'POST',
		data:{idVisita:idVisita},
		//dataType:'json',
		async:false,
		success:function(data){
			if(data==1){
				alert("El proceso se ejecuto correctamente!!");
				$("#txtNoVisita").val("");
			}else{
				alert("ERROR. El proceso no se ejecuto!!");
			}
		}
	})
}