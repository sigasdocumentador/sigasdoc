 var urlGuardReverAbonoPerioLiqui = "guardReverAbonoPerioLiqui.php"
	 , urlBuscaAbonoPeriodLiqui = "buscaAbonoPeriodLiqui.php"
     , urlVerifPermiCierr = "verifPermiCierr.php";
 
 
$(function(){
	
	$("#btnNuevo").click(nuevo);
	$("#btnGuardar").click(guardar);
	$("#btnBuscar").button().click(ctrBuscar);
	$("#txtFechaReverso").change(ctrFechaReverso);
	
	datepickerC("FECHA","#txtFechaReverso");
});

function fetchAbonoPeriodLiqui(objFiltro){
	var resultado = null;
	
	$.ajax({
		url: urlBuscaAbonoPeriodLiqui,
		type:"POST",
		data: {objDatosFiltro:objFiltro},
		dataType:"json",
		async: false,
		success: function(data){
			resultado = data;
		}
	});
	
	return resultado;
}

function fetchPermisoCierre(objFiltro){
	var resultado = null;
	
	$.ajax({
		url: urlVerifPermiCierr,
		type:"POST",
		data: {objDatosFiltro:objFiltro},
		dataType:"json",
		async: false,
		success: function(data){
			resultado = data;
		}
	});
	
	return resultado;
}

/**
 * METODO: Adiciona evento blur al campo periodo
 * */
function addEventBlurRever(idInputReverso){
	
	//Obtener el valor del aporte para el periodo
	$("#"+idInputReverso).blur(function(){
		
		var idTrAbono = "#"+$(this).parent().parent().attr("id");
		var reversoFinal = $(this).val();
		var reversoInicial = $(idTrAbono + " #hidValorReversoInicial").val().trim();
		var valorAbono = $(idTrAbono + " #hidValorAbonoDiscriminado").val().trim();
		var abonoFinal = 0;
		
		reversoFinal = isNaN(parseInt(reversoFinal))?0:parseInt(reversoFinal);
		reversoInicial = isNaN(parseInt(reversoInicial))?0:parseInt(reversoInicial);
		valorAbono = isNaN(parseInt(valorAbono))?0:parseInt(valorAbono);
		
		//Verificar para que no vuelva ha realizar reverso al mismo abono
		if(reversoInicial>0 && reversoFinal != reversoInicial){
			alert("El abono solo puede tener un reverso, y este ya tiene un reveso. Por tanto no se puede realizar la accion!!");
			$(this).val(reversoInicial);
			return false;
		}
		
		//Verificar que el valor del reverso sea menor o igual al valor abonado
		if(reversoFinal>valorAbono){
			alert("El valor del reverso debe ser menor o igual al abono!!");
			$(this).val("");
			return false;
		}
		
		//Control observacion
		$(idTrAbono + " #txaObservacion").removeClass("element-required");
		if(reversoFinal>0 && reversoInicial==0){
			$(idTrAbono + " #txaObservacion").addClass("element-required");
		}
		
		//Control del abono final
		abonoFinal = valorAbono-reversoFinal;
		$(idTrAbono + " #tdAbonoFinal").text(formatCurrency(abonoFinal));
	});
}

function ctrBuscar(){
	var idLiquidacion = $("#txtNumeroLiquidacion").val().trim();
	
	if(!idLiquidacion) return false; 
	
	var objFiltro = {idliquidacion:idLiquidacion,estado:"A"};
	var dataPeriodo = fetchAbonoPeriodLiqui(objFiltro);
	
	var datosVisita = "";
	var razonSocial = "";
	var htmlPeriodo = "";
	var idVisita = "";
	var fechaVisita = "";
	
	$("#tblAbonoPeriodo tbody").html("");
	if(dataPeriodo && typeof dataPeriodo == "object" && dataPeriodo.length>0){
		
		datosVisita = dataPeriodo[0].fechavisita + " <b>#" + dataPeriodo[0].idvisita+"</b>";
		razonSocial = dataPeriodo[0].razonsocial;
		idVisita = dataPeriodo[0].idvisita;
		fechaVisita = dataPeriodo[0].fechavisita;
			
		$.each(dataPeriodo,function(i,row){
			var idTrPeriodo = "trIdAbono"+row.id_abono_periodo;
			var idInputReverso = "txtValorReversoFinal"+row.id_abono_periodo;
			
 			var valorReverso = isNaN(parseInt(row.valor_reverso))?0:parseInt(row.valor_reverso);
 			var abonoInicial = parseInt(row.abono_discriminado)+valorReverso;
 			
			htmlPeriodo = '<tr id="'+idTrPeriodo+'">'
	     				+'<td>'
	     				+	row.periodo
	     				+	'<input type="hidden" name="hidIdDetalle" id="hidIdDetalle" value="'+row.iddetalle+'"/>'
	     				+'</td>'
	     				+'<td>'+row.numero_recibo+'</td>'
	     				+'<td>'+row.fecha_abono_discriminado+'</td>'
	     				+'<td style="text-align:right;">'+formatCurrency(row.neto)+'</td>'
	     				+'<td style="text-align:right;">'+formatCurrency(abonoInicial)+'</td>'
			 			+'<td>'
			 			+	'<input type="text" name="'+idInputReverso+'" id="'+idInputReverso+'" class="boxfecha" value="'+valorReverso+'" style="text-align:right;"/>'
			 			+	'<input type="hidden" name="hidValorReversoInicial" id="hidValorReversoInicial" value="'+valorReverso+'"/>'
			 			+	'<input type="hidden" name="hidValorAbonoDiscriminado" id="hidValorAbonoDiscriminado" value="'+row.abono_discriminado+'"/>'
			 			+	'<input type="hidden" name="hidValorAbono" id="hidValorAbono" value="'+row.abono+'"/>'
			 			+'</td>'
	     				+'<td id="tdAbonoFinal" style="text-align:right;">'+formatCurrency(row.abono_discriminado)+'</td>'
			 			+'<td>'
			 			+	'<textarea name="txaObservacion" id="txaObservacion" cols="50" rows="1">'+row.observacion_abono+'</textarea>'
			 			+'</td></tr>';
			
			$("#tblAbonoPeriodo tbody").append(htmlPeriodo);
			//Adicionar evento para validar el periodo
			addEventBlurRever(idInputReverso);
		});
		
	}else{
		razonSocial = "<b>NO EXISTEN DATOS!!</b>";
	}
	
	$("#tdDatosVisita").html(datosVisita);
	$("#tdRazonSocial").html(razonSocial);
	$("#hidIdVisita").val(idVisita);
	$("#hidFechaVisita").val(fechaVisita);
	
}

function ctrFechaReverso(){
	var fechaReverso = $("#txtFechaReverso").val();
	
	if(!fechaReverso) return false;
	
	var fechaVisita = $("#hidFechaVisita").val();
	
	if(new Date(fechaReverso)< new Date(fechaVisita)){
		alert("La fecha del reverso ("+fechaReverso+") es inferior a la fecha de la visita ("+fechaVisita+")");
		$("#txtFechaReverso").val("");
	}
}

function guardar(){
		
	if(validateElementsRequired()>0) return false;
	
	if($("#tblAbonoPeriodo tbody tr").length==0){
		alert("Debes cargar los abonos de los periodos liquidados");
		return false;
	}
	
	var objData = {data_reverso:[],data_detalle_liquidacion:[],data_liquidacion:{}};
	var objDataDetelle = {};
	$("#tblAbonoPeriodo tbody tr").each(function(i,row){
		var idTr = "#"+$(this).attr("id");
		var idAbono = idTr.replace("#trIdAbono","");
		
		var abono = parseInt($(idTr + " #hidValorAbono").val().trim());
		var abonoDiscriminado = $(idTr + " #hidValorAbonoDiscriminado").val().trim();
		var reversoInicial = $(idTr + " input[name^='hidValorReversoInicial']").val().trim();
		var reversoFinal = $(idTr + " #txtValorReversoFinal"+idAbono).val().trim();
		
		if(reversoFinal>0 && reversoInicial==0){
			
			var idDetelleLiquidacion = $(idTr + " input[name^='hidIdDetalle']").val().trim();
			abonoDiscriminado = abonoDiscriminado-reversoFinal;
			
			//Datos del reverso
			objData.data_reverso[objData.data_reverso.length] = { 
					abono: abonoDiscriminado
					, valor_reverso: reversoFinal
					, fecha_reverso: $("#txtFechaReverso").val().trim()
					, observacion: $(idTr + " #txaObservacion").val().trim()
					, usuario_reverso: $("#hidUsuario").val().trim()
					, id_abono_periodo: idAbono
					, id_periodo_liquidacion: idDetelleLiquidacion
			};
			
			if(typeof objDataDetelle["id"+idDetelleLiquidacion] == 'undefined'){
				objDataDetelle["id"+idDetelleLiquidacion] = {
						abono:abono
						,id_detalle: idDetelleLiquidacion
				};
			}
			
			objDataDetelle["id"+idDetelleLiquidacion].abono =(objDataDetelle["id"+idDetelleLiquidacion].abono-reversoFinal);
		}
	});
	
	$.each(objDataDetelle,function(i,row){
		objData.data_detalle_liquidacion[objData.data_detalle_liquidacion.length] = row;
	});
	
	objData.data_liquidacion = {id_visita: $("#hidIdVisita").val()}
	
	//Verificar si se realizaron ajustes
	if(objData.data_reverso.length==0){
		alert("Debe realizar el reverso!!");
		return false;
	}
	
	var objFiltro = {
			usuario: $("#hidUsuario").val().trim() 
			, fecha: $("#txtFechaReverso").val().trim()
		};
	if(fetchPermisoCierre(objFiltro)==0){
		alert("La fecha del reverso no corresponde al cierre y el usuario no tiene permisos. \n Por este motivo no se guardo la informacion");
		return false;
	}
	
	var resultado = 0;
	//Hacer la peticion al servidor
	$.ajax({
		url: urlGuardReverAbonoPerioLiqui,
		type:"POST",
		data: {data:objData},
		dataType:"json",
		async: false,
		success: function(data){
			resultado = data;
		}
	});
	
	if(resultado==1){
		nuevo();		
		alert("Los datos se guardaron correctamente!!");
	}else{
		alert("Error al guardar los datos. Favor reportar al administrador T.I.");
	}
}

function nuevo(){
	limpiarCampos2("input:text, #hidFechaVisita, #tdRazonSocial, #tdDatosVisita, #tblAbonoPeriodo tbody");
}

