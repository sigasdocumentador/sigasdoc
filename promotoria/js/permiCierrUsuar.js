 var urlLogPermiCierrUsuar = "permiCierrUsuar.log.php"
	 , urlBuscarUsuario = "";
 
 
$(function(){
	urlBuscarUsuario = src()+"phpComunes/buscaUsuarPerso.php";
	
	$("#btnGuardar").button().click(guardar);
	$("#txtUsuario").blur(ctrBuscarUsuario);
	
	datepickerC("FECHA_BETWEEN_LIBRE","#txtFechaInicial,#txtFechaFinal");
});


function fetchUsuario(objFiltro){
	var resultado = null;
	
	$.ajax({
		url: urlBuscarUsuario,
		type:"POST",
		data: {objDatosFiltro:objFiltro},
		dataType:"json",
		async: false,
		success: function(data){
			resultado = data;
		}
	});
	
	return resultado;
}

function fetchPermiCierrUsuar(objFiltro){
	var resultado = null;
	
	$.ajax({
		url: urlLogPermiCierrUsuar,
		type:"POST",
		data: {accion:'S',datos:objFiltro},
		dataType:"json",
		async: false,
		success: function(data){
			resultado = data;
		}
	});
	
	return resultado;
}

function ctrBuscarUsuario(){
	var usuario = $("#txtUsuario").val();
	
	if(!usuario) return false;
	
	var objFiltro = {usuario:usuario};
	var dataUsuario = fetchUsuario(objFiltro);
	
	nuevo();
	$("#txtUsuario").val(usuario);
	
	var htmlUsuario = "";
	
	if(dataUsuario && typeof dataUsuario == "object" && dataUsuario.length>0){
		htmlUsuario = dataUsuario[0].identificacion+" - "+dataUsuario[0].nombres;
		
		// verificar si ya tiene el permiso
		var dataPermiso = fetchPermiCierrUsuar({usuario:usuario});
		
		if(dataPermiso.data && typeof dataPermiso.data == "object" && dataPermiso.data.length>0){
			if(confirm("El permiso para el usuario ya se encuentra grabado, desea actualizar los datos!!")){
				
				$("#hidIdPermisoCierre").val(dataPermiso.data[0].id_permiso_cierre);
				$("#txtFechaInicial").val(dataPermiso.data[0].fecha_inicial);
				$("#txtFechaFinal").val(dataPermiso.data[0].fecha_final);
				$("#cmbEstado").val(dataPermiso.data[0].estado);
			}else{
				nuevo();
				htmlUsuario = "";
			}
		}
	}else{
		htmlUsuario = "<b>No se encontraron datos</b>";
		nuevo();
	}
	
	$("#tdDatosUsuario").html(htmlUsuario);
}

function guardar(){
		
	if(validateElementsRequired()>0) return false;
	
	var accion = "I"; 
	var objData = {
			id_permiso_cierre: $("#hidIdPermisoCierre").val().trim()
			, usuario: $("#txtUsuario").val().trim()
			, fecha_inicial: $("#txtFechaInicial").val()
			, fecha_final: $("#txtFechaFinal").val()
			, estado: $("#cmbEstado").val()
		};
	
	if(objData.id_permiso_cierre!="")accion = "U";
	
	var resultado = 0;
	//Hacer la peticion al servidor
	$.ajax({
		url: urlLogPermiCierrUsuar,
		type:"POST",
		data: {accion:accion, datos:objData},
		dataType:"json",
		async: false,
		success: function(data){
			if(data.error==0)
				resultado = 1;
			else
				resultado = 0; 
		}
	});
	
	if(resultado==1){
		nuevo();		
		alert("Los datos se guardaron correctamente");
	}else{
		alert("Error al guardar los datos. Favor reportar al administrador T.I.");
	}
}

function nuevo(){
	limpiarCampos2("#txtUsuario, #hidIdPermisoCierre, #txtFechaInicial, #txtFechaFinal, #cmbEstado, #tdDatosUsuario");
}

