var URL=src(),
	periodosLiquidacion = new Array(),
	arrDefinEstadLiqui = [],
	uriDetalleDefinicion = "",
	urlVerifPermiCierr = "verifPermiCierr.php";

$(document).ready(function(){
	uriDetalleDefinicion = URL+"phpComunes/buscaDetalDefin.php";
	/********************************
	 *          EVENTOS             *
	 ********************************/
	
	$("#btnAdicionarTrPeriodo,#btnBuscarNit,#btnAgregarPeriodos").button();
	$("#iconActualizar").hide();
	
	//Activo los detapicker para los campos fecha
	datepickerC("FECHA","#txtFechaLiqui,#txtFechaNotificacion,#txtFechaEstado");
	datepickerC("PERIODO","#txtPeriodoIncial");
	
	//Adicionar la fila para el periodo
	//addTrPeriodo();
	
	//Preguntar siempre si el numero de visita tiene algun valor
	if( $("#txtNumerVisit").val() != '' ){
		$("#txtNumerVisit").focus().blur();
	}
	
	//Control del combo estado
	ctrEstadoLiquidacion();
	
	//Control de la fecha estado de la liquidacion
	$("#cmbEstado").change(ctrFechaEstado);
	
	//Evento para el element indicador
	$("#cmbIndice").change(function(){
		if(!verificarIndicador()) return false;
		
		//Calcular los valores para cada periodo
		$("#tblDetalleLiquidacion tbody tr").each(function(){
			calcularAporte($(this).attr("id"));
		});
	});	
	
	//Adicionar eventos a los datos del periodo liquidado
	addEventBlurPeriod("txtPeriodo1");
	addEventChangeElemePeriod("trPeriodo1");
	
	
	//Dialog Tabla Liquidaciones
	$("#tablaLiquidaciones").dialog({
		autoOpen: false,
		width: 1000,
		modal:true,
		open:function(){

			//Limpiar Tabla 
			$("#liquidaciones tbody tr").remove(); 
			$("#txtNitLiquidacion").val('').focus();
			$("#totalLiqTabla,#datosEmpresaLiq").html('');
		},
		buttons:{
			"IMPRIMIR":function(){
				var idEmpresa = $("#spnIdEmpresa").text().trim();
				if( idEmpresa != 0 ){
					var url="http://"+getCookie("URL_Reportes")+"/promotoria/reporte002.jsp?v0="+idEmpresa+"&v1="+$("#txtUsuario").val();
					window.open(url,"_BLANK");
				}
			}
		}	  
	});
	
	$("#btnBuscarNit").click(function(){
		var nitLiquidacion = $("#txtNitLiquidacion").val().trim();
		
		//Resetear Tabla del dialog de la visita
		$("#liquidaciones tbody tr").remove();
		$("#totalLiqTabla,#datosEmpresaLiq").html('');
		$("#tablaLiquidaciones .ui-state-error").removeClass("ui-state-error");	  
		
		if( nitLiquidacion == 0 ){
			$("#txtNitLiquidacion").addClass("ui-state-error");
			return false;
		}
		
		var datosEmpresa = buscarEmpresa( null, nitLiquidacion);
		if( datosEmpresa == 0 ){
			$("#liquidaciones").append("<tr><td colspan='10' class='Rojo' align='center'>No existe el nit en la base de datos de SIGAS!!.</td></tr>");
			return false;
		}
		
		//Adicionar los datos de la empresa al formulario
		$("#datosEmpresaLiq").html( "<span id='spnIdEmpresa'>" + datosEmpresa.idempresa + "</span>" + datosEmpresa.razonsocial );
	
		var datosLiquidacion = buscaDatosLiquidacion({idempresa:datosEmpresa.idempresa});
		if( typeof datosLiquidacion !== "object" || datosLiquidacion.length==0){
			$("#liquidaciones").append("<tr><td colspan='10' class='Rojo' align='center'>No hay historial de liquidaciones!!</td></tr>");
			return false;
		}
		
		var totalNeto = 0,
			bandera = 0;
		$.each( datosLiquidacion, function(i,row){
			
			//Verificamos que la visita este activa
			if( row.estadovisita == "A" ){
				var periodos = "",
					arrayPeriodo = null;
				
				//Obtenemos el detalle de la liquidacion
				arrayPeriodo = buscaDetalLiqui({idliquidacion:row.idliquidacion});
				
				var bruto = 0,
					aportes = 0,
					neto = 0;
				
				for( i = 0, j = 0, len = arrayPeriodo.length; i < len; i++, j++ ){
					periodos += arrayPeriodo[i].periodo + " - ";
					bruto = bruto + parseFloat(arrayPeriodo[i].bruto);
					aportes = aportes + parseFloat(arrayPeriodo[i].aportes);
					neto = neto + parseFloat(arrayPeriodo[i].neto);
					if( j == 5 ){
						periodos += "\n";
						j = 0;
					}
				}
				
				periodos = periodos.substring(0, periodos.length-3);
				$("#liquidaciones").append("<tr>" +
						"<td>" + row.idvisita + "</td>" +
						"<td><a href='#' onClick='buscarLiquidacion(" + row.idliquidacion + ", \"" + row.idvisita + "\" )' style='color:blue;'>" + row.idliquidacion + "</a></td>" +
						"<td>" + row.fechaliquidacion + "</td>" +
						"<td>" + row.estado_liquidacion + "</td>" +
						"<td>" + periodos + "</td>" +
						"<td>" + row.promotor + "</td>" +
						"<td>" + formatCurrency(bruto)   + "</td>" +
						"<td>" + parseFloat(row.indicador)   + "%</td>" +
						"<td>" + formatCurrency(aportes) + "</td>" +
						"<td>" + formatCurrency(neto)    + "</td>" +
				"</tr>");
				totalNeto += parseFloat(neto);
				bandera++;
			}
		});
		if( bandera == 0 )
			$("#liquidaciones").append("<tr><td colspan='10' class='Rojo' align='center'>No hay visitas activas</td></tr>");
		else
			$("#totalLiqTabla").html( "<b>" + formatCurrency( totalNeto ) + "</b>" );
	});
	
	//Dialog para mostrar los periodos a eliminar cuando se este actualizando la liquidacion
	$("#tablaPeriodoEliminar").dialog({
		autoOpen: false,
		width: 600,
		modal:true,
		buttons:{
			"ACEPTAR":function(){
				actualizarLiquidacion();
				$("#tablaPeriodoEliminar").dialog("close");
			}
		}
	});
	
	
	//$(".sortable tr:odd").addClass("vzebra-odd");
	
});//fin ready

//funcion para obtener cookie en javascript
function getCookie(c_name)
{
	var i,x,y,ARRcookies=document.cookie.split(";");
	for (i=0;i<ARRcookies.length;i++)
	{
			x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
			y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
			x=x.replace(/^\s+|\s+$/g,"");
			if (x==c_name)
			{
				return unescape(y);
			}// fin if
	}// fin for
	}// fin getCookie
//funcion para obtener cookie en javascript

/**********************************************************
 * METODOS PARA EL CONTROL DE LOS ELEMENTOS DEL FORMULARIO 
 * ********************************************************/

function ctrEstadoLiquidacion(){
	var objFiltro = {codigo_definicion:"ESTADO_LIQUIDACION"};
	var dataEstado = fetchDetalleDefinicion(objFiltro);
	var htmlCombo = '<option value="">--SELECCIONE--</option>';
	
	arrDefinEstadLiqui = [];
	
	if(dataEstado && typeof dataEstado == "object" && dataEstado.length>0){
		$.each(dataEstado,function(i,row){
			htmlCombo += '<option value="'+row.iddetalledef+'">'+row.detalledefinicion+'</option>';
			
			//Guardamos los datos en un array global para al momento de guardar obtener el codigo del estado
			arrDefinEstadLiqui["id"+row.iddetalledef] = row;
		});
	}
	
	$("#cmbEstado").html(htmlCombo);
}

function ctrFechaEstado(){
	var idEstadoLiquidacion = $("#cmbEstado").val();
	
	//Verificar si los datos se van ha actualizar
	if(idEstadoLiquidacion && ($("#iconActualizar").is(":visible") || arrDefinEstadLiqui["id"+idEstadoLiquidacion].codigo=="ANULADA")) {
		$("#txtFechaEstado").addClass("element-required");
		$("#clsFechaEstado").show();
	}else{
		$("#txtFechaEstado").removeClass("element-required");
		$("#clsFechaEstado").hide();
	}
}

function ctrTotalPeriodo(){
	var arrDataTotal = [];
	var arrAnio = [];
	$("#tblDetalleLiquidacion tbody tr").each(function(i,row){
		var idTr = "#"+$(this).attr("id");
		
		var periodo = $(idTr + " input[name^='txtPeriodo']").val().trim();
		var anio = periodo.substring(0,4);
		
		if(!arrDataTotal[anio] || typeof arrDataTotal[anio] == "undefined"){
			arrDataTotal[anio] = {
					anio: anio
					, total_aporte: 0
					, total_bruto: 0
					, total_neto: 0
			};
			arrAnio[arrAnio.length] = anio;
		}		
		
		arrDataTotal[anio].total_aporte += isNaN(parseInt($(idTr + " #txtAporte").val()))?0:parseInt($(idTr + " #txtAporte").val());
		arrDataTotal[anio].total_bruto += isNaN(parseInt($(idTr + " #txtBaseLiquidacion").val()))?0:parseInt($(idTr + " #txtBaseLiquidacion").val());
		arrDataTotal[anio].total_neto += isNaN(parseInt($(idTr + " #txtTotalPagar").val()))?0:parseInt($(idTr + " #txtTotalPagar").val());
		
	});
	
	var htmlTfoot = "";

	$.each(arrAnio,function(i, row){
		htmlTfoot += '<tr class="clsTotalDetalle"><th colspan="2">TOTAL '+arrDataTotal[row].anio+'</th>'
 				+'<th>'+formatCurrency(arrDataTotal[row].total_aporte)+'</th>'
 				+'<th>'+formatCurrency(arrDataTotal[row].total_bruto)+'</th>'
 				+'<th>'+formatCurrency(arrDataTotal[row].total_neto)+'</th>'
 				+'<th>&nbsp;</th></tr>';
	});
	
	htmlTfoot +=  '<tr><td colspan="9" style="text-align: center;">'
					+'<input type="button" name="btnAdicionarTrPeriodo" id="btnAdicionarTrPeriodo" value="Adicionar Periodo" onclick="addTrPeriodo();"/>'
					+'</td></tr>';

	$("#tblDetalleLiquidacion tfoot").html(htmlTfoot);
	$("#btnAdicionarTrPeriodo").button();
}

/**
 * METODO: Adiciona evento blur al campo periodo
 * */
function addEventBlurPeriod(idInputPeriodo){
	var selecTrPerio = "#trPeriodo"+idInputPeriodo.replace("txtPeriodo","");
	
	//Obtener el valor del aporte para el periodo
	$("#"+idInputPeriodo).blur(function(){
		
		var idTrPeriodo = $(this).parent().parent().attr("id");
		
		if($(this).val()==0)return false;
		
		var idEmpresa = $("#hdIdEmpresa").val();
		
		//Validar que los datos se cargaron correctamente
		if(idEmpresa==0){
			alert("Falta cargar los datos de la visita!!");
			$(this).val("");
			return false;
		}
		
		var idInputPeriodo = "#"+$(this).attr("id");
		//Verificar que el periodo no este duplicado
		if(!verifPerioLiqui(idInputPeriodo)){
			return false;
		}
		
		//Controlar el elemento para indicar la observacion del periodo
		contrObserPerio(idTrPeriodo);
	});
	
	//Controlar la clase .ui-state-error del elemento para la observacion
	$("#tblDetalleLiquidacion "+selecTrPerio+" #txaObservacion").blur(function(){
		var observacion = $(this).val().trim();
		if(observacion!=0){
			$(this).removeClass("ui-state-error");
		}else{
			$(this).addClass("ui-state-error");
		}
	});
}

/**
 * METODO: Adiciona el evento change a los elementos (base liquidacion, aporte, ...) del perido 
 * @param idTrPeriodo Id tr del periodo
 */
function addEventChangeElemePeriod(idTrPeriodo){
	var selecInputPerio = "#txtPeriodo"+idTrPeriodo.replace("trPeriodo","");
	
	//Evento para el element base liquidacion
	$("#"+idTrPeriodo+" #txtAporte").change(function(){
		if(!verificarIndicador()) return false;
		
		var idTrPeriodo = $(this).parent().parent().attr("id");
		calcularAporte(idTrPeriodo);
		
		//Calcular los totales
		ctrTotalPeriodo();
		
		//Controlar el elemento para indicar la observacion del periodo
		contrObserPerio(idTrPeriodo);
	});
	
}

/**
 * METODO: Adiciona el evento keypress a los elementos (base liquidacion, aporte, ...) del perido
 * @param idTrPeriodo Id tr del periodo
 */
function addEventPressElemePeriod(idTrPeriodo){
	
	//Controlar la tecla TAB
	$("#tblDetalleLiquidacion #"+idTrPeriodo+" input[name='txtAporte'], " +
	"#tblDetalleLiquidacion #"+idTrPeriodo+" textarea[name='txaObservacion']").keypress(function( evento ){
		if(evento.keyCode==9){
		   var thisInput = $(evento.originalEvent.originalTarget);
		   var nameInput = thisInput.attr("name");
		   
		   var idTr = "#"+thisInput.parent().parent().attr("id");
		   var idTrNext = "#"+thisInput.parent().parent().nextAll(":visible").first().attr("id");
		   
		   if(typeof idTrNext != "undefined"){
			   
			   if(nameInput=="txtAporte" && !$(idTr+ " textarea[name='txaObservacion']").is(":disabled")){
				   $(idTr+ " textarea[name='txaObservacion']").focus();
			   }else if(nameInput=="txaObservacion" || $(idTr+ " textarea[name='txaObservacion']").is(":disabled")){
				   $(idTrNext+ " input[name='txtAporte']").focus();
			   }
		   }
		   return false;
		}
	});
}

function addTrPeriodoMasivo(){
	var cantidadPeriodos = parseInt($("#txtCantidadPeriodos").val()),
		totalBaseLiqui = parseInt($("#txtTotalBaseLiqui").val()),
		periodoIncial = $("#txtPeriodoIncial").val().trim(),
		indice = parseFloat($("#cmbIndice").val()),
		bruto = 0,
		aportes = 0;
	
	$(".ui-state-error").removeClass("ui-state-error");
	if(cantidadPeriodos==0){$("#txtCantidadPeriodos").addClass("ui-state-error");return false;}
	if(totalBaseLiqui==0){$("#txtTotalBaseLiqui").addClass("ui-state-error");return false;}
	if(periodoIncial==0){$("#txtPeriodoIncial").addClass("ui-state-error");return false;}
	if(indice==0){$("#cmbIndice").addClass("ui-state-error");return false;}
	
	bruto = Math.round(totalBaseLiqui/cantidadPeriodos);
	aportes = Math.round( ( bruto*indice ) / 100 ); 
	
	//Preparar los datos para el periodo
	var objDatos = {
			iddetalle:"",
			periodo:"",
			bruto:bruto,
			aportes:aportes,
			neto:aportes			
		}
	
	var periodoDate = new Date(periodoIncial.substring(0,4)+'/'+periodoIncial.substring(6,4)+"/01");
	//Adicionar los tr para los periodos
	for(i=0;i<cantidadPeriodos;i++){	
		objDatos.periodo = periodoIncial;
		addTrPeriodo(objDatos);
		
		//Incrementar el periodo
		var mes = "";
		periodoDate.setMonth(periodoDate.getMonth()+1);
		periodoIncial = periodoDate.getFullYear();
		mes = parseInt(periodoDate.getMonth())+1;
		mes = parseInt(mes)<10?"0"+mes:mes;
		periodoIncial = periodoIncial+""+mes;
	}
	
	//Limpiar los campos 
	$("#txtCantidadPeriodos,#txtTotalBaseLiqui,#txtPeriodoIncial").val("");
	
}

/**
 * METODO: Adiciona nueva fila "tr" para ingresar otro periodo
 * 
 * @param objDatos Datos para cada element
 * */
function addTrPeriodo(objDatos){
	var numeroTr = 1;
	//Verificamos si existen filas "tr" 
	if($("#tblDetalleLiquidacion tbody tr").length>0){
		var arrIdTrAnterior = $("#tblDetalleLiquidacion tbody tr:last").attr("id");
		numeroTr = parseInt(arrIdTrAnterior.replace("trPeriodo",""))+1
	}
	
	//Creamos el id para la nueva fila "tr"
	var idTrNuevo = "trPeriodo"+numeroTr;
	//Creamos el id para los elementos
	var idInputPeriodo = "txtPeriodo"+numeroTr;
	
	var htmlTr = '<tr id="'+idTrNuevo+'">'
			+ '<td><img src="../imagenes/borrar.png" alt="Eliminar" style="cursor:pointer;" onclick="removeTrPeriodo(\''+idTrNuevo+'\');"/>' 
			+ '<input type="hidden" name="hidIdDetalle" id="hidIdDetalle"/></td>'
			+ '<td>'
				+ '<input type="text" name="'+idInputPeriodo+'" id="'+idInputPeriodo+'" size="8" class="element-required" readonly="readonly"/>'
				+ '<input type="hidden" name="hidPeriodoOriginal" id="hidPeriodoOriginal"/>'
				+ '<input type="hidden" name="hidBanderaEliminar" id="hidBanderaEliminar" value="0"/>'
				+ '<input type="hidden" name="hidAjustePeriodo" id="hidAjustePeriodo"/>'
				+ '<input type="hidden" name="hidAporteOriginal" id="hidAporteOriginal"/></td>'
			+ '<td><input type="text" name="txtAporte" id="txtAporte" size="20" class="element-required boxfecha"/></td>'
			+ '<td><input type="text" name="txtBaseLiquidacion" id="txtBaseLiquidacion" size="20" disabled="disabled" class="boxfecha element-required"/></td>'
			+ '<td><input type="text" name="txtTotalPagar" id="txtTotalPagar" size="20" disabled="disabled" class="boxfecha element-required"/></td>'
			+ '<td><textarea name="txaObservacion" id="txaObservacion" disabled="disabled" rows="1" cols="40" maxlength="100"/></td></tr>'
	
	$("#tblDetalleLiquidacion tbody").append(htmlTr);
	
	if(typeof objDatos == "object"){
		
		var periodoOriginal = 0;
		var aporteOriginal = 0;
		var ajustePeriodo = 'N'; 
		
		if(objDatos.iddetalle != ""){
			periodoOriginal = objDatos.periodo;
			aporteOriginal = objDatos.aportes;
		}
		
		if(parseInt(aporteOriginal)==0){
			ajustePeriodo = 'S'
		}
		
		//Adicionar los valores a cada element
		$("#tblDetalleLiquidacion #"+idTrNuevo+" #hidIdDetalle").val(objDatos.iddetalle);
		$("#tblDetalleLiquidacion #"+idTrNuevo+" #"+idInputPeriodo).val(objDatos.periodo);
		$("#tblDetalleLiquidacion #"+idTrNuevo+" #hidPeriodoOriginal").val(parseFloat(periodoOriginal));
		$("#tblDetalleLiquidacion #"+idTrNuevo+" #hidAporteOriginal").val(parseFloat(aporteOriginal));
		$("#tblDetalleLiquidacion #"+idTrNuevo+" #hidAjustePeriodo").val(ajustePeriodo);
		$("#tblDetalleLiquidacion #"+idTrNuevo+" #txtBaseLiquidacion").val(parseFloat(objDatos.bruto));
		$("#tblDetalleLiquidacion #"+idTrNuevo+" #txtAporte").val(parseFloat(objDatos.aportes));
		$("#tblDetalleLiquidacion #"+idTrNuevo+" #txtTotalPagar").val(parseFloat(objDatos.neto));
		$("#tblDetalleLiquidacion #"+idTrNuevo+" #txaObservacion").val(objDatos.observaciondetalle);
		
	}
	
	datepickerC("PERIODO","#tblDetalleLiquidacion tbody #"+idTrNuevo+" #"+idInputPeriodo);
	addEventBlurPeriod(idInputPeriodo);
	addEventChangeElemePeriod(idTrNuevo);
	addEventPressElemePeriod(idTrNuevo);
	
	//Trigger blur para activar el campo de la observacion
	$("#tblDetalleLiquidacion #"+idTrNuevo+" #txtAporte").trigger("change");
	
	//Remover los campos obligatorios, si existe observacion
	//if(typeof objDatos == "object" && objDatos.length>0 && objDatos.observaciondetalle.trim()!=0){
	if(typeof objDatos == "object" && typeof objDatos.observaciondetalle != "undefined" && objDatos.observaciondetalle.trim()!=0){
		$("#tblDetalleLiquidacion #"+idTrNuevo+" #txtAporte, " +
				"#tblDetalleLiquidacion #"+idTrNuevo+" #txtBaseLiquidacion, " +
				"#tblDetalleLiquidacion #"+idTrNuevo+" #txtTotalPagar")
			.removeClass("element-required");
	}
}

/**
 * METODO: Remover fila "tr" para eliminar el periodo
 * 
 * @param idTrPeriodo Id tr a remover
 * */
function removeTrPeriodo(idTrPeriodo){
	//Verificar para que exista mas de un periodo
	if($("#tblDetalleLiquidacion tbody tr:visible").length==1)return false;
	
	var thisObservacionPeriodo = $("#tblDetalleLiquidacion #"+idTrPeriodo+" #txaObservacion"); 
	
	//Obtener el valor de la bandera
	var banderaEliminar = $("#tblDetalleLiquidacion #"+idTrPeriodo+" #hidBanderaEliminar").val();
	
	//Verificar si el periodo ya esta almacenado en la base de datos
	if($("#tblDetalleLiquidacion #"+idTrPeriodo+" #hidIdDetalle").val().trim() != 0 && banderaEliminar == 0){
		//Pregunta para habilitar la observacion del periodo
		if(confirm("Primero debes digitar la observaci\xF3n!!")){
			enabledObserPerio(idTrPeriodo);
			$("#tblDetalleLiquidacion #"+idTrPeriodo+" #hidBanderaEliminar").val(1);
		}
		return false;
	}
	
	//Verificar si el periodo tiene observacion
	if($("#tblDetalleLiquidacion #"+idTrPeriodo+" #hidIdDetalle").val().trim() != 0 && thisObservacionPeriodo.val().trim()==0 ){
		thisObservacionPeriodo.addClass("ui-state-error");
		return false;
	}
	
	//Verificar si el usuario quiere eliminar el periodo
	if(!confirm("Deseas eliminar el periodo!!")) return false;
	
	$("#"+idTrPeriodo).hide();
	$("#tblDetalleLiquidacion #"+idTrPeriodo+" #hidBanderaEliminar").val(0);
}

/**
 * METODO: Control del campo para adicionar la observacion del periodo
 * 
 * @param idTrPeriodo Id tr donde esta el elemento para indicar la observacion
 * */
function contrObserPerio(idTrPeriodo){
	var conseTrPeriodo = idTrPeriodo.replace("trPeriodo","");
	var periodo = $("#tblDetalleLiquidacion #"+idTrPeriodo+" #txtPeriodo"+conseTrPeriodo).val().trim(),
		aporte = $("#tblDetalleLiquidacion #"+idTrPeriodo+" #txtAporte").val().trim(),
		
		periodoOriginal = $("#tblDetalleLiquidacion #"+idTrPeriodo+" #hidPeriodoOriginal").val().trim(),
		aporteOriginal = $("#tblDetalleLiquidacion #"+idTrPeriodo+" #hidAporteOriginal").val().trim();
	
	//Verificar pera habilitar el elemento de la observacion
	if(periodo!=periodoOriginal || aporte!=aporteOriginal){
		enabledObserPerio(idTrPeriodo);
	}else{
		disabledObserPerio(idTrPeriodo);
	}
}


/**
 * METODO: Habilita el campo para adicionar la observacion del periodo
 * 
 * @param idTrPeriodo Id tr donde esta el elemento para indicar la observacion
 * */
function enabledObserPerio(idTrPeriodo){
	
	var classObligatorio = "" 
	
	//La observacion no es obligatoria al momento de guardar
	if(!$("#bGuardar").is(":visible")){
		classObligatorio = "element-required";
	}
	
	$("#tblDetalleLiquidacion #"+idTrPeriodo+" #txaObservacion")
		.addClass(classObligatorio)
		.removeClass("ui-state-error")
		.attr("disabled",false);
	
	$("#tblDetalleLiquidacion #"+idTrPeriodo+" #txtAporte, " +
			"#tblDetalleLiquidacion #"+idTrPeriodo+" #txtBaseLiquidacion, " +
			"#tblDetalleLiquidacion #"+idTrPeriodo+" #txtTotalPagar")
		.removeClass("element-required");
}

/**
 * METODO: Deshabilitar el campo para adicionar la observacion del periodo
 * 
 * @param idTrPeriodo Id tr donde esta el elemento para indicar la observacion
 * */
function disabledObserPerio(idTrPeriodo){
	$("#tblDetalleLiquidacion #"+idTrPeriodo+" #txaObservacion")
		.removeClass("ui-state-error element-required")
		.attr("disabled",true);
	
	$("#tblDetalleLiquidacion #"+idTrPeriodo+" #txtAporte, " +
			"#tblDetalleLiquidacion #"+idTrPeriodo+" #txtBaseLiquidacion, " +
			"#tblDetalleLiquidacion #"+idTrPeriodo+" #txtTotalPagar")
		.addClass("element-required");
}

/**
 * METODO: Verifica si el interes esta seleccionado
 *
 * @returns {Boolean}
 */
function verificarIndicador(){
	//Validar que el indice este seleccionado
	if($("#cmbIndice").val()==0){
		$("#cmbIndice").addClass("ui-state-error");
		return false;
	}else{
		$("#cmbIndice").removeClass("ui-state-error");
	}
	return true;
}

/**
 * METODO: Calcula el aporte
 * @param idTrPeriodo Id tr de los datos del aplicativo
 */
function calcularAporte(idTrPeriodo){
	//aporte = (base liquidacion * indice)/100
	
	var indice = parseFloat($("#cmbIndice").val()),
	aporte = parseFloat($("#"+idTrPeriodo+" #txtAporte").val());
	
	indice = isNaN(indice)?0:indice;
	aporte = isNaN(aporte)?0:aporte; 
	
	var baseLiquidacion = ( aporte / indice ) * 100;
	
	baseLiquidacion = Math.round(baseLiquidacion);
	
	$("#"+idTrPeriodo+" #txtBaseLiquidacion").val( (baseLiquidacion) );
	$("#"+idTrPeriodo+" #txtTotalPagar").val( (aporte) );
	
	/*var indice = parseFloat($("#cmbIndice").val()),
		baseLiquidacion = parseFloat($("#"+idTrPeriodo+" #txtBaseLiquidacion").val());
	
	indice = isNaN(indice)?0:indice;
	baseLiquidacion = isNaN(baseLiquidacion)?0:baseLiquidacion; 
	
	var aporte = ( baseLiquidacion * indice ) / 100;
	
	aporte = Math.round(aporte);
	
	$("#"+idTrPeriodo+" #txtAporte").val( (aporte) );
	$("#"+idTrPeriodo+" #txtTotalPagar").val( (aporte) );
	*/
	//calcularCapital(idTrPeriodo);
}

function buscarVisita( idVisita ){
	limpiarCampos2( "#txtFechaLiqui,#txtNumerLiqui,#txtFechaLiqui,#txtFechaNotificacion" );
	periodosLiquidacion = [];
	if( idVisita == "" )return false;
	
	var datosVisita = buscarDatosVisita({idvisita:idVisita});
	if( datosVisita.length > 0 ){
		//Obtener el primer registro 
		datosVisita = datosVisita[0];
		
		//if( datosVisita.estado != 'P' && datosVisita.convenio != "V" ){
		if( datosVisita.estado == 'A' &&  datosVisita.liquidacion=='S' ){
			
			var datosAportante = buscarEmpresa( datosVisita.idempresa );
			$("#txtNumerVisit").val(idVisita).attr("disabled",true);
			$("#tdFechaVisita").text( datosVisita.fechavisita );
			$("#txtFechaLiqui,#txtFechaNotificacion").val( datosVisita.fechavisita );
			llenarDatosAportante( datosAportante );	
			
			//Obtener las liquidaciones que tiene la visita, puede no tener
			arrDatosDetalle = buscaDetalLiqui({idvisita:idVisita});
			$.each(arrDatosDetalle,function(i,row){
				periodosLiquidacion[periodosLiquidacion.length] = {
						periodo:row.periodo,
						idliquidacion:row.idliquidacion
					};
			});
			
		}else if(datosVisita.liquidacion=='N'){
			MENSAJE("La visita No. " + idVisita + ", no est\u00E1 para liquidaci\xF3n");
			nuevaLiquidacion();
			
		}else{
			MENSAJE("La visita No. " + idVisita + ", est\u00E1 al d\u00EDa.");
			nuevaLiquidacion();
			return false;
		}
	}else{
		MENSAJE("No existe la visita No. " + idVisita);
		return false;
	}
	return true;
}

/**
 * METODO: Adicionar los datos de la empresa a los elementos del formulario
 * 
 * @param Array datos Contiene los datos de la empresa 
 */
function llenarDatosAportante( datos ){
	var nombreRepLegal = datos.pnr + ( ( datos.snr == 0 ) ?  ""  : " " + datos.snr) +  " " + datos.par + " " + datos.sar;
	
	$("#hdIdEmpresa").val( datos.idempresa );
	$("#tdNit").text( datos.nit );
	$("#tdEstadoEmpresa").text( datos.estado );
	$("#tdRazonSocial").text( datos.razonsocial );
	$("#tdDireccion").text( datos.direccion );
	$("#tdTelefono").text( datos.telefono );
	$("#tdCiudad").text( datos.municipio );
	$("#tdRepresentante").text( nombreRepLegal );
	$("#tdIdentRepre").text( datos.identificacionrpl );
	$("#tdIndice").text( datos.indicador );
	$("#tdTipoAfiliacion").text( datos.tipoAfiliacion );
	$("#tdTipoEmpresa").text( datos.sector );
	
	var contenidoHtml = "";
	//Verificar que los datos de la empresa esten completos
	$(".clDatosAportante").each(function(i,row){
		var idTd = $(this).attr("id"); 
		if( $(this).text() == 0 && idTd!="tdRepresentante" && idTd !="tdIdentRepre")
			contenidoHtml += "- " + $(this).prev().text() + "<br/>";  
	});
	
	if( contenidoHtml != "" ){
		contenidoHtml = "<b>Faltan datos de la empresa <br/> Nit " + datos.nit + " <br/> " + datos.razonsocial + "</b><br/>" + contenidoHtml
		MENSAJE(contenidoHtml);
		nuevaLiquidacion();
	}
}

/**
 * METODO: Verifica si el numero de la liquidacion ya existe
 * 
 * @returns {Boolean}
 */
function verificarLiquidacion(){
	var idLiquidacion = $("#txtNumerLiqui").val().trim();
	if( idLiquidacion == 0 )return false;
	
	if( isNaN( parseInt(idLiquidacion) ) ){
		$("#txtNumerLiqui").addClass("ui-state-error");
	}else{
		$("#txtNumerLiqui").removeClass("ui-state-error");
	}
	
	//Obtiene los datos de la liquidacion en caso de existir
	var datosLiquidacion = buscaDatosLiquidacion( {idliquidacion:idLiquidacion} );
	if( typeof datosLiquidacion === "object" ){
		if(datosLiquidacion.length>0){
			var contenidoMessage = "Ya existe la liquidacion No. " +  idLiquidacion + " \n" + "Visita No. " + datosLiquidacion[0].idvisita;
			alert( contenidoMessage );
			$("#txtNumerLiqui").val("");
		}
	}
}


/**
 * METODO: Asigna los datos de la liquidacion al formulario
 * 
 * @param idLiquidacion
 * @param idVisita
 */
function buscarLiquidacion( idLiquidacion, idVisita ){
	var resultadoVisita = true,
		datosDetalLiqui = "";
	
	resultadoVisita = buscarVisita( idVisita );
	
	//Verificar si los datos de la visita se cargaron correctamente
	if( resultadoVisita && $("#txtNumerVisit").val() != 0 ){
		
		datosDetalLiqui = buscaDetalLiqui( {idliquidacion:idLiquidacion} );
	
		//Verificar los datos obtenidos de la liquidacion
		if( typeof datosDetalLiqui === "object" && datosDetalLiqui.length>0){
			
			var datosLiquidacion = datosDetalLiqui[0];
			
			//Adicionar los campos de la liquidacion al formulario
			$( "#txtNumerLiqui" ).val( idLiquidacion ).attr( "disabled",true );
			$( "#txtFechaLiqui" ).val( datosLiquidacion.fechaliquidacion );
			$( "#txtIngreBase" ).val( datosLiquidacion.ibc );
			$( "#cmbTipoNotificacion" ).val( datosLiquidacion.idtiponotificacion );
			$( "#txtFechaNotificacion" ).val( datosLiquidacion.fechanotificacion );
			$( "#txaObservacion" ).val( datosLiquidacion.observaciones );
			$( "#txaDocumentacion" ).val( datosLiquidacion.documentacion );
			$( "#cmbIndice" ).val( parseFloat( datosLiquidacion.indicador ) ).attr( "disabled",true );
			$( "#cmbEstado" ).val( datosLiquidacion.id_estado_liquidacion );
			$( "#txtFechaEstado" ).val( datosLiquidacion.fecha_estado_liquidacion );
			
			//Remover todos los tr de la tabla periodos 
			$("#tblDetalleLiquidacion tbody tr").remove();
			
			$.each(datosDetalLiqui,function(i, row){
				//Adicionar los campos del detalle liquidacion al formulario
				addTrPeriodo(row);
			});
			
			//CONTROL BOTONES
			$("#bGuardar").hide();
			$("#iconActualizar").show();
		}else{
			MENSAJE("<p>Error cargando los datos de la liquidaci\xF3n</p>");
			nuevaLiquidacion();
		}
		$("#tablaLiquidaciones").dialog("close");
	}
}

/**
 * METODO: Abre el dialogo para buscar las liquidaciones de la empresa
 */
function openDialogBuscaLiquid(){
	nuevaLiquidacion();
    $("#tablaLiquidaciones").dialog("open");
}

/**
 * 	METODO: Resetea el formulario
 */
function nuevaLiquidacion(){
	//nuevo=1;
	//banderaLiqui = false;
	periodosLiquidacion = [];
	var usuario = $("#txtUsuario").val();
	
	limpiarCampos2( ".clDatosAportante, input, select, textarea, #etiquetas, #tdFechaVisita, #cmbEstado, #txtFechaEstado" );
	$( "#txtNumerVisit,#txtNumerLiqui,#cmbIndice" ).removeAttr( "disabled" );
	$("#iconActualizar").hide();
	$("#bGuardar").show();
	
	$("#txtUsuario").val(usuario);
	//ordenBase( null );
	//banderaNumerPeriodo = 0;
	
	//Fecha estado liquidacion
	$("#txtFechaEstado").removeClass("element-required");
	$("#clsFechaEstado").hide();
	
	//Remover los tr de dialogo que muestra los periodos a eliminar
	$("#tblPeriodoEliminar tbody tr").remove(); 
		
	//Remover todos los tr de la tabla periodos 
	$("#tblDetalleLiquidacion tbody tr").remove();
	
	//Adicionar la fila para el periodo
	addTrPeriodo();
	
	$(".ui-state-error").removeClass("ui-state-error");
	
	//Adicionar la fila para el periodo
	//addTrPeriodo();
	
	$("#tblDetalleLiquidacion .clsTotalDetalle").remove();
}


/******************************
 * METODOS PARA LAS PETICIONES
 * ****************************/

/**
 * METODO: Busca los datos de la visita
 * 
 * @param String idVisita
 * @returns Array  
 * */
function buscarDatosVisita(objDatosFiltro){
	var datosRetorno = "";
	$.ajax({
		url:"buscarVisita.php",
		type:"POST",
		data:{objDatosFiltro:objDatosFiltro},
		dataType:"json",
		async:false,
		success:function(datos){
			//************************************
			datosRetorno = datos;
		}
	});
	return datosRetorno;
}

/**
 * METODO: Busca los datos de la empresa
 * 
 * @param int idEmpresa
 * @param String nit
 * @returns Array  
 * */
function buscarEmpresa(idEmpresa = null , nit = null){
	var datosRetorno = null;
	if( idEmpresa != null ){
		$.ajax({
			url:URL+"phpComunes/buscarEmpresaId.php",
			type:"POST",
			data:{v0:idEmpresa},
			dataType:"json",
			async:false,
			success:function(datos){
				if( datos == 0 )
					datosRetorno = 0;
				else
					nit = datos[0].nit;
			}
		});
	}
	if(datosRetorno == null ){
		$.ajax({
			url:URL+"phpComunes/buscarEmpresaCompleta.php",
			type:"POST",
			data:{v0:nit},
			dataType:"json",
			async:false,
			success:function(datos){
				if( datos == 0 )
					datosRetorno = 1;
				else
					datosRetorno = datos[0];
				//************************************
			}
		});
	}	
	return datosRetorno;
}

/**
 * METODO: Busca los datos de la liquidacion
 * 
 * @param int objDatosFiltro
 * @returns Array  
 * */
function buscaDatosLiquidacion( objDatosFiltro ){
	var datosRetorno = ""; 
	$.ajax({
		url:"buscarLiquidacion.php",
		type:"POST",
		data:{objDatosFiltro:objDatosFiltro},
		dataType:"json",
		async:false,
		success:function(datos){
			datosRetorno = datos;
		}
	});
	return datosRetorno;
}

/**
 * METODO: Busca los datos del detalle liquidacion
 * 
 * @param int objDatosFiltro
 * @returns Array  
 * */
function buscaDetalLiqui(objDatosFiltro){
	var datosRetorno = "";
	$.ajax({
		url:"buscarDetalLiqui.php",
		type:"POST",
		data:{objDatosFiltro:objDatosFiltro},
		dataType:"json",
		async:false,
		success:function(datos){
			//************************************
			datosRetorno = datos;
		}
	});
	return datosRetorno;
}

/**
 * METODO: Busca detalle definicion
 * 
 * @param int objDatosFiltro
 * @returns Array  
 * */
function fetchDetalleDefinicion(objDatosFiltro){
	var datosRetorno = "";
	$.ajax({
		url:uriDetalleDefinicion,
		type:"POST",
		data:{objDatosFiltro:objDatosFiltro},
		dataType:"json",
		async:false,
		success:function(datos){
			//************************************
			datosRetorno = datos;
		}
	});
	return datosRetorno;
}


function fetchPermisoCierre(objFiltro){
	var resultado = null;
	
	$.ajax({
		url: urlVerifPermiCierr,
		type:"POST",
		data: {objDatosFiltro:objFiltro},
		dataType:"json",
		async: false,
		success: function(data){
			resultado = data;
		}
	});
	
	return resultado;
}

/********************************
 * METODOS PARA LAS VALIDACIONES
 * ******************************/

function verifPerioLiqui( campo ){
	var periodo = $(campo).val();
	var idEmpresa = $("#hdIdEmpresa").val().trim();
	var banderaA = 0;
	var banderaB = 0;
	
	//Verificar con los periodos que estan en el formulario
	$("#tblDetalleLiquidacion tbody tr").each(function(i,row){
		var thisTr = $(this);
		var idTr = "#"+thisTr.attr("id");
	    var idInputPeriodo = "#txtPeriodo"+(idTr.replace("#trPeriodo",""));
	    
	    //Verifica si el periodo esta duplicado
		if( idInputPeriodo != campo && periodo==$(idInputPeriodo).val() ){
			
			//Verificar si el periodo esta aculto y fue por motivo de modificacion
			if(!thisTr.is(":visible") && $(idTr + " #hidIdDetalle").val() != 0){
				//El periodo que esta seleccionando fue eliminado por la actualizacion 
				banderaA++;
			}else if(thisTr.is(":visible")){
				banderaB++;
			}
		}
	});
	if(banderaA>0){
		contenidoHtml = "<b>El periodo</b><br/> " +  periodo + "<br/>" + " Fue eliminado, Por tanto no se puede almacenar!!"; 
		MENSAJE( contenidoHtml );
		//Limpiamos el campo del periodo
		$(campo).val("");
		return false;
		
	}else if (banderaB>0){
		contenidoHtml = "<b>El periodo</b><br/> " +  periodo + "<br/>" + " Ya existe en el formulario!!"; 
		MENSAJE( contenidoHtml );
		//Limpiamos el campo del periodo
		$(campo).val("");
		return false;
	}
	
	contenidoHtml = "";
	//Verficar con los periodos que estan en la base de datos
	$.each(periodosLiquidacion,function(i,row){
		var bandera = 0;
		if(periodo==row.periodo){
			
			//Verificar si el periodo esta visible
			$("#tblDetalleLiquidacion tbody tr input[name^='txtPeriodo']:visible").each(function(i,row){
				
				var idTr = $(this).parent().parent().attr("id");
				
				var campoPeriodo = "#txtPeriodo"+(idTr.replace("trPeriodo",""));
				
				if(campo != campoPeriodo && $(this).val().trim() == periodo)
					bandera++;
			});
			
			if(bandera>0)
				contenidoHtml = "<b>El periodo</b><br/> " +  row.periodo + "<br/>" + " Ya existe en la Base de datos de SIGAS con la liquidaci\xF3n No. <b>"+row.idliquidacion+"</b>";
		}
	});
	
	if(contenidoHtml!=""){
		MENSAJE( contenidoHtml );
		$(campo).val("");
		return false;
	}
	
	//Verificar si el periodo existe con otra visita diferente, si existe se debe dejar seguir con el proceso
	var htmlDatos = "";
	var objDataDetalLiqui = buscaDetalLiqui({periodo:periodo,idempresa:idEmpresa});
	if(objDataDetalLiqui && typeof objDataDetalLiqui == "object"){
		
		$.each(objDataDetalLiqui,function(i,row){
			if(htmlDatos==""){
				htmlDatos += "<b>ADVERTENCIA El periodo:</b> " +  row.periodo + "<br/> Ya existe en la Base de datos de SIGAS:<br/><br/>";
			}
			htmlDatos += "Liquidaci\xF3n No. <b>"+row.idliquidacion+"</b> Y visita No. <b>"+row.idvisita+"</b><br/>";
		});
	}
	
	if(htmlDatos!=""){
		MENSAJE( htmlDatos );
	}
	
	return true;
}

/*
function limpiarCampo( classCampo ){
	var usuario = $("#txtUsuario").val();
	var tipoCampo = "";
	$( classCampo ).each(function(i,row){
		
		if( $( this ).get(0).name == "base" ){
			$( this ).val(0);
			return;
		}
		tipoCampo = $( this ).get(0).type;
		
		if( typeof tipoCampo === "undefined" )
			tipoCampo = $( this ).get(0).localName;
		
		if( tipoCampo == "select-one" )
			$( this ).val(0);
		else if( tipoCampo == "text" || tipoCampo == "textarea" || tipoCampo == "hidden" )
			$( this ).val("");
		else if( tipoCampo == "div" || tipoCampo == "td" || tipoCampo == "span" )
			$( this ).text("");
		else if( tipoCampo == "checkbox" )
			$( this ).attr("checked",false);
	});	
	$("#txtUsuario").val( usuario );
}*/

/**
 * METODO: Valida los elementos requeridos del formulario
 * 
 * @returns int Numero de elementos vacios
 */
/*function validaCampoFormu(){
	//En este metodo no se validan los periodos a eliminar o actualizar
	
	var banderaError = 0;
	$( ".ui-state-error" ).removeClass( "ui-state-error" );
	$(".element-required").each(function(i,row){
		//Verifica que los campos esten visibles porque pueden ser periodos eliminados
		if($( this ).val() == 0 && $( this ).is(":visible")){
			$( this ).addClass( "ui-state-error" );
			banderaError++;
		}
	});
	return banderaError;
}*/

function validarDatosRequeridos(){

	$("#tblDetalleLiquidacion tbody tr input:text:visible").addClass("element-required");
	//Agregar o remover la clase requerida de los elementos del periodo
	$("#tblDetalleLiquidacion tbody tr").each(function(i,row){
	    var thisTr = $(this),
	    	idTr = "#"+thisTr.attr("id"),
	    	numeroConsecutivo = 0;
	    
	    numeroConsecutivo = idTr.replace("#trPeriodo","");
	    
	    var idInputPeriodo = "#txtPeriodo"+numeroConsecutivo;;
	    var ajustePeriodo = $(idTr + " #hidAjustePeriodo").val();
	    if(thisTr.is(":visible") && ajustePeriodo == "S"){
	    	$(idTr + " #txtAporte, "+idTr + " #txtBaseLiquidacion, "+idTr + " #txtTotalPagar").removeClass("element-required");
	    }
	});
	
	return validateElementsRequired();

}

/**
 * METODO: Obtener el detalle de la liquidacion
 * 
 * @returns Array Indice 0: datos a guardar o actualizar, Indice 1: datos a eliminar
 */
function obtenDatosDetal(){
	var numeroLiquidacion = $( "#txtNumerLiqui" ).val().trim(),
		objDatosDetalGuard = [],
		objDatosDetalElimi = [],
		objDatosLiquidacion = [];
	
	//Obtener los datos de los periodos
	$("#tblDetalleLiquidacion tbody tr").each(function(i,row){
	    var thisTr = $(this),
	    	idTr = "#"+thisTr.attr("id"),
	    	numeroConsecutivo = 0;
	    
	    numeroConsecutivo = idTr.replace("#trPeriodo","");
	    
	    var idInputPeriodo = "#txtPeriodo"+numeroConsecutivo;;
	    
	    if(thisTr.is(":visible")){
	    	
	    	//Obtener los datos del periodo para guardar o actualizar
	    	
	    	//Verificar si el periodo se actualizo o es nuevo
	    	var aporteOriginal = $(idTr + " #hidAporteOriginal").val().trim();
	    	var aporteNuevo = $(idTr + " #txtAporte").val().trim();
	    	var periodoOriginal = $(idTr + " #hidPeriodoOriginal").val().trim();
	    	var periodoNuevo = $(idTr + " " + idInputPeriodo).val().trim();
	    	var idDetalle = $(idTr + " #hidIdDetalle").val().trim();
	    	
	    	if( aporteOriginal!=aporteNuevo || periodoOriginal!=periodoNuevo){
	    		
	    		objDatosDetalGuard[objDatosDetalGuard.length] = {
		    			iddetalle: $(idTr + " #hidIdDetalle").val().trim(),
		    			idliquidacion:numeroLiquidacion,
		    			bruto: $(idTr + " #txtBaseLiquidacion").val().trim(),
		    			aportes: $(idTr + " #txtAporte").val().trim(),
		    			neto: $(idTr + " #txtTotalPagar").val().trim(),
		    			periodo: $(idTr + " " + idInputPeriodo).val().trim(),
		    			observacion: $(idTr + " #txaObservacion").val().trim(),
		    			bruto_inicial: $(idTr + " #txtBaseLiquidacion").val().trim(),
		    			aporte_inicial: $(idTr + " #txtAporte").val().trim(),
		    			neto_inicial: $(idTr + " #txtTotalPagar").val().trim(),
		    			periodo_inicial: $(idTr + " " + idInputPeriodo).val().trim(),
		    			abono: 0,
		    			cancelado: 'N',
		    			fechaabono: ''
		    		};
	    	}
	    }else{
	    	//Obtener los datos del periodo para eliminar
	        if(parseInt($(idTr + " #hidIdDetalle" ).val())>0){
	        	objDatosDetalElimi[objDatosDetalElimi.length] = {
		    			iddetalle: $(idTr + " #hidIdDetalle").val().trim(),
		    			idliquidacion:numeroLiquidacion,
		    			bruto: $(idTr + " #txtBaseLiquidacion").val().trim(),
		    			aportes: $(idTr + " #txtAporte").val().trim(),
		    			neto: $(idTr + " #txtTotalPagar").val().trim(),
		    			periodo: $(idTr + " " + idInputPeriodo).val().trim(),
		    			observacion: $(idTr + " #txaObservacion").val().trim(),
		    			bruto_inicial: $(idTr + " #txtBaseLiquidacion").val().trim(),
		    			aporte_inicial: $(idTr + " #txtAporte").val().trim(),
		    			neto_inicial: $(idTr + " #txtTotalPagar").val().trim(),
		    			periodo_inicial: $(idTr + " " + idInputPeriodo).val().trim(),
		    			abono: 0,
		    			cancelado: 'N',
		    			fechaabono: ''
		    		};
	        	
	        }
	    }
	});
	
	var idEstado = $("#cmbEstado").val();
	var codigoEstado = arrDefinEstadLiqui["id"+idEstado].codigo;
	
	objDatosLiquidacion = {
			idliquidacion:      $( "#txtNumerLiqui" ).val().trim(),
			fechaliquidacion:   $( "#txtFechaLiqui" ).val().trim(),
			idvisita:           $( "#txtNumerVisit" ).val().trim(),
			idtiponotificacion: $( "#cmbTipoNotificacion" ).val().trim(),
			fechanotificacion:  $( "#txtFechaNotificacion" ).val().trim(),
			ibc:                $( "#txtIngreBase" ).val().trim() | 0,  
			observaciones:      $( "#txaObservacion" ).val().trim(), 
			documentacion:      $( "#txaDocumentacion" ).val().trim(),
			indicador:          $( "#cmbIndice" ).val().trim(),
			id_estado:          idEstado,
			fecha_estado: 		$( "#txtFechaEstado" ).val().trim(),
			codigo_estado: 		codigoEstado
		};
	
	return [objDatosDetalGuard, objDatosDetalElimi, objDatosLiquidacion];
}

function guardarLiquidacion(){
	/*if( nuevo == 0 ){
		alert("Haga click primero en Nuevo");
		return false;
	}*/
	
	if( validarDatosRequeridos() > 0 ){
		MENSAJE("<b>La informacion esta incompleta</b>");
		return false;
	}

	var objFiltro = {
			usuario: $("#txtUsuario").val().trim() 
			, fecha: $("#tdFechaVisita").text().trim()
		};
	if(fetchPermisoCierre(objFiltro)==0){
		alert("La fecha de la visita no corresponde al cierre y el usuario no tiene permisos. \n Por este motivo no se guardo la informacion");
		return false;
	}
	
	var arrDatosLiquidacion = obtenDatosDetal();
	var datosLiquidacion = null,
		datosDetalle = arrDatosLiquidacion[0],
		datosLiquidacion = arrDatosLiquidacion[2];
	
	/*var idEstado = $("#cmbEstado").val();
	var codigoEstado = arrDefinEstadLiqui["id"+idEstado].codigo;
	
	datosLiquidacion = {
			idliquidacion:      $( "#txtNumerLiqui" ).val().trim(),
			fechaliquidacion:   $( "#txtFechaLiqui" ).val().trim(),
			idvisita:           $( "#txtNumerVisit" ).val().trim(),
			idtiponotificacion: $( "#cmbTipoNotificacion" ).val().trim(),
			fechanotificacion:  $( "#txtFechaNotificacion" ).val().trim(),
			ibc:                $( "#txtIngreBase" ).val().trim() | 0,  
			observaciones:      $( "#txaObservacion" ).val().trim(), 
			documentacion:      $( "#txaDocumentacion" ).val().trim(),
			indicador:          $( "#cmbIndice" ).val().trim(),
			id_estado:          idEstado,
			codigo_estado: 		codigoEstado
		};
		*/

	$.ajax({
		url: "guardarLiquidacion.php",
		type: "POST",
		data: {v0:datosLiquidacion,v1:datosDetalle},
		typeData: "json",
		async: false,
		success: function( datos ){
			if( datos == 0 ){
				MENSAJE("<p>No se pudo guardar la liquidaci\u00F3n</p>");
			}else if( datos == 1 ){
				MENSAJE("<p>La liquidaci\u00F3n se guardo correctamente!!</p>");
				nuevaLiquidacion();				
			}
		}
	});	
}

function mostrarPerioElimi(){
	var htmlPeriodoEliminar = "";
	//Obtener los periodos a eliminar
	$("#tblDetalleLiquidacion tbody tr :not(:visible)").each(function(){
	    var idTr = "#"+$(this).attr("id");
	    var idInputPeriodo = "#txtPeriodo"+idTr.replace("#trPeriodo","");
	    
	    if($("#" + idTr + " #hidIdDetalle").val()>0){
	    	htmlPeriodoEliminar += "<tr><td>"+$(idInputPeriodo).val()+"</td><td>"+$(idTr+" #txtTotalPagar").val()+"</td></tr>"; 
	    }
	});
	
	if(htmlPeriodoEliminar!=""){
		//Mostrar los periodos a eliminar
		$("#tblPeriodoEliminar tbody").html(htmlPeriodoEliminar); 
		$("#tablaPeriodoEliminar").dialog("open");
	}else{
		//No hay periodos para eliminar
		actualizarLiquidacion();
	}
}

function actualizarLiquidacion(){
	
	if( validarDatosRequeridos() > 0 ){
		MENSAJE("<b>La informaci\xF3n esta incompleta</b>");
		return false;
	}
	
	var arrDatosLiquidacion = obtenDatosDetal();
	var datosDetalle = arrDatosLiquidacion[0],
		datosDetalElimi = arrDatosLiquidacion[1],
		datosLiquidacion = arrDatosLiquidacion[2];
	
	var objFiltro = {
			usuario: $("#txtUsuario").val().trim() 
			, fecha: $("#tdFechaVisita").text().trim()
		};
	if(fetchPermisoCierre(objFiltro)==0){
		alert("La fecha de la visita no corresponde al cierre y el usuario no tiene permisos. \n Por este motivo no se guardo la informacion");
		return false;
	}
	
	$.ajax({
		url: "actualizarLiquidacion.php",
		type: "POST",
		data: {objDatosDetalle:datosDetalle,objDatosDetalElimi:datosDetalElimi, objDatosLiquidacion:datosLiquidacion},
		typeData: "json",
		async: false,
		success: function( datos ){
			if( datos == 0 ){
				MENSAJE("<p>No se pudo actualizar la liquidaci\u00F3n</p>");
			}else if( datos == 1 ){
				alert( "La liquidaci\u00F3n se atualizo correctamente" );
				nuevaLiquidacion();
			}
		}
	});
}