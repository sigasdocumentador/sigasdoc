<?php 
	$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
	include_once  $root;
	
	$url=$_SERVER['PHP_SELF'];
	include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'auditoria.php';
	auditar($url);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<title>::Promotoria | Empresa convenio de pago</title>
		<link href="../css/Estilos.css" rel="stylesheet" type="text/css" />
		<link href="../css/estilo_tablas.css" rel="stylesheet" type="text/css" />
		<link type="text/css" href="../css/formularios/base/ui.all.css" rel="stylesheet" />
		<link href="../css/marco.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="../js/jquery-1.4.2.js"></script>
		<script type="text/javascript" src="../js/formularios/ui/ui.core.js"></script>
		<script type="text/javascript" src="../js/formularios/ui/ui.dialog.js"></script>
		<script type="text/javascript" src="../js/effects.Jquery.js"></script>
		<script type="text/javascript" src="../js/comunes.js"></script>
		<script type="text/javascript" src="../js/jquery.tablesorter.js"></script>
		<script type="text/javascript" src="../js/jquery.tablesorter.pager.js"></script>
		<script type="text/javascript" src="js/empresaSinConv.js"></script>
		<style type="text/css">
			.claNumero{
				text-align:center;
			}
			
			.sortable {border:1px solid #eaeaea;margin:10px auto}
			.sortable th {background:#f3f3f3; text-align:left; color:#666; border:1px solid #eaeaea; border-right:none; padding:4px}
			.sortable th h3 {padding:0 18px; margin:0; font:11px Verdana, Geneva, sans-serif}
			.sortable td {padding:6px 6px 6px 10px;border:1px solid #eaeaea; font-size:10px; text-align:left}
			.sortable .head h3 {background:url(../imagenes/imagesSorter/sort.gif) 7px center no-repeat; cursor:pointer; padding:0 18x}
			.sortable .desc h3 {background:url(../imagenes/imagesSorter/desc.gif) 7px center no-repeat; cursor:pointer; padding:0 18px}
			.sortable .asc h3 {background:url(../imagenes/imagesSorter/asc.gif) 7px  center no-repeat; cursor:pointer; padding:0 18px}
			.sortable .head:hover, .sortable .desc:hover, .sortable .asc:hover {color:#000}
			.sortable .evenrow td {background:#ecf2f6}
			.sortable .oddrow td {background:#fff}
			.sortable td.evenselected {background:#F8F8F8}
			.sortable td.oddselected {background:#ffffff}
			
			#controls { margin:0 auto; height:20px; /*width:800px;*/ overflow:hidden;}
			#perpage {float:left; width:200px; margin-left:40px;}
			#perpage select {float:left; font-size:11px}
			#perpage label {float:left; margin:2px 0 0 5px; font-size:10px}
			#navigation {float:left;/* width:600px;*/ text-align:center}
			#navigation img {cursor:pointer}
			#text {float:left; width:200px; text-align:left; margin-first:2px; font-size:10px}
			.pager{ margin:10px;}
		</style>
	</head>
	<body>
		<table border="0" align="center" cellpadding="0" cellspacing="0" width="80%">
  			<tbody>
  				<!-- ESTILOS SUPERIOR TABLA-->
  				<tr>
    				<td width="13" height="29" class="arriba_iz">&nbsp;</td>
     				<td class="arriba_ce">
     					<span class="letrablanca">::&nbsp;Empresas Convenio de pago&nbsp;::</span>
     				</td>
     				<td width="13" class="arriba_de" align="right">&nbsp;</td>
  				</tr>
  				<!-- ESTILOS ICONOS TABLA-->
  				<tr>
    				<td class="cuerpo_iz">&nbsp;</td>
    				<td class="cuerpo_ce">
    					<img src="../imagenes/menu/informacion.png" name="iconInfo" width="16" id="iconInfo" style="border:none; cursor:pointer" title="Manual" onClick="mostrarAyuda();" />
    					<img src="../imagenes/menu/notas.png" name="iconSugerencia" width="16" height="16" id="iconSugerencia" style="cursor: pointer" title="Colaborac&oacute;on en l&oacute;nea" onClick="notas();" />
    				</td>
    				<td class="cuerpo_de">&nbsp;</td>
  				</tr>
  				<!-- ESTILOS MEDIO TABLA-->
  				<tr>
    				<td class="cuerpo_iz">&nbsp;</td>
    				<td class="cuerpo_ce">
	    				<!-- TABLA DE EMPRESAS SIN CONVENIO DE PAGO	 -->
    					<table cellpadding="0" cellspacing="0" width="80%"  border="0" class="sortable hover" id="tablaEmpresa">
      						<caption style="text-align:left; padding:5px; font-size:11px;"><span></span></caption>
      						<thead>
      							<tr>
        							<th class="head"><h3><strong>Nit</strong></h3></th>
        							<th class="head"><h3><strong>Empresa</strong></h3></th>
        							<th class="head"><h3><strong>Consecutivo</strong></h3></th>
      							</tr>
      						</thead>
  	    					<tbody>
     						</tbody>
  						</table>    
    					<!-- BOTONES DE ORDENACION -->
   						<div id="pager" class="pager">
   							<div id="perpage">
								<select class="pagesize">
									<option selected="selected"  value="5">5</option>
									<option value="20">20</option>
									<option value="30">30</option>
									<option  value="40">40</option>
								</select>
        						<label>Registros Por P&aacute;gina</label>
        					</div>
        					<div id="navigation">
								<img src="../imagenes/imagesSorter/first.gif" class="first"/>
								<img src="../imagenes/imagesSorter/previous.gif" class="prev"/>
        						<img src="../imagenes/imagesSorter/next.gif" class="next"/>
								<img src="../imagenes/imagesSorter/last.gif" class="last"/> 
        						<label>P&aacute;gina</label>
								<input type="text" class="pagedisplay boxfecha" readonly="readonly" />
       						</div>
						</div>
    				</td>
   					<td class="cuerpo_de">&nbsp;</td>
  				</tr>
  				<!-- CONTENIDO TABLA-->
  				<tr>
   					<td class="cuerpo_iz">&nbsp;</td>
   					<td class="cuerpo_ce">
   						<input type="hidden" name="hidIdEmpresa" id="hidIdEmpresa" />
   						<input type="hidden" name="hidIdVisita" id="hidIdVisita" />
   					</td> 
     				<td class="cuerpo_de">&nbsp;</td>
  				</tr>  
  				<!-- ESTILOS PIE TABLA-->
  				<tr>
    				<td class="abajo_iz" >&nbsp;</td>
    				<td class="abajo_ce" ></td>
    				<td class="abajo_de" >&nbsp;</td>
  				</tr>  
			</tbody>
		</table>
		<div id="divDatosConvenio" title="::Datos empresa::">
			<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
			<tbody>
		  	<!-- ESTILOS SUPERIOR TABLA-->
		  	<tr>
			    <td width="13" height="29" class="arriba_iz">&nbsp;</td>
			    <td class="arriba_ce"><span class="letrablanca">::&nbsp;DATOS CONVENIO&nbsp;::</span></td>
			    <td width="13" class="arriba_de" align="right">&nbsp;</td>
		  	</tr> 
		  	<!-- ESTILOS MEDIO TABLA-->
		  	<tr>
		    	<td class="cuerpo_iz">&nbsp;</td>
		    	<td class="cuerpo_ce"></td>
		    	<td class="cuerpo_de">&nbsp;</td>
		  	</tr>  
		  	<!-- CONTENIDO TABLA-->
		  	<tr>
		   		<td class="cuerpo_iz">&nbsp;</td>
		   		<td class="cuerpo_ce">
					<table cellspacing="0" width="90%" border="0" class="tablero" align="center" id="tblDatosConvenio">
		      			<thead>
			    			<tr>
			      				<th colspan="8">
			      					DATOS EMPRESA
			      				</th>
			      			</tr>
			    			<tr>
			      				<td colspan="8" style="text-align:center;" id="tdEmpresa"></td>
			        		</tr>
			    			<tr>
			      				<th colspan="8">
			      					DATOS DEL CONVENIO DE PAGO
			      				</th>
			      			</tr>
			    			<tr class="ui-state-default">
			    				<td >Consecutivo</td>
			      				<td >Fecha de pago</td>
			      				<td>Valor Interes</td>
			      				<td >Abono Interes</td>
			      				<td >Periodos</td>
			      				<td>Valor Periodos</td>
			      				<td >Abono Periodos</td>
			      				<td >Saldo</td>
			      			</tr>
		      			</thead>
		      			<tbody>
		      			</tbody>
		      			<tfoot>
		      				<tr>
		      					<td colspan="2">
		      						<h4>Total Acuerdo Distribuido</h4>
		      					</td>
		      					<td class='claNumero'><h4 id="h4TotalAcuerDistr"></h4></td>
		      					<td colspan="2">	
		      						<h4>Total Acuerdo</h4>
		      						<input type="hidden" name="hidIdConvenio" id="hidIdConvenio"/>
		      					</td>
		      					<td class='claNumero'><h4 id="h4TotalAcuerdo"></h4></td>
		      					<td>
		      						<h4>Total Saldo</h4>
		      					</td>
		      					<td colspan="2" class='claNumero'><h4 id="h4TotalSaldo"></h4></td>
		      				</tr>
		      				<tr>
		      					<td colspan="2">Motivo Estado</td>
		      					<td colspan="6">
		      						<textarea name="txaMotivoEstado" id="txaMotivoEstado" cols="80" rows="3" maxlength="250" class="boxlargo"></textarea>
									<span class="clsObligatorio">&nbsp;&nbsp;&nbsp;</span>
		      					</td>
		      				</tr>
		      			</tfoot>
		      		</table>
					</td>
		    		<td class="cuerpo_de">&nbsp;</td>
		  		</tr>		  
		  		<!-- ESTILOS PIE TABLA-->
		  		<tr>
		    		<td class="abajo_iz" >&nbsp;</td>
		    		<td class="abajo_ce" ></td>
		    		<td class="abajo_de" >&nbsp;</td>
		  		</tr>		  
			</tbody>
		</table>
		</div>
	</body>
</html>