<?php
date_default_timezone_set('America/Bogota');
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';

//Include de las clases que tiene los procesos
include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'p.aportes.class.php';
include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'p.afiliacion.class.php';

class Cron{
	/************************************
	 *  ATRIBUTOS ENTIDADES			    *
	***********************************/
	private static $con = null;
	private $usuario = null;
	private $arrDatosProcesos = null;
	private $objDatoProceso = null;
	private $estadoProceso = null;
	private $mensajeError = null;
	
	function __construct(){
		try{
			self::$con = IFXDbManejador::conectarDB();
			if( self::$con->conexionID==null ){
				throw new Exception(self::$con->error);
			}
		}catch(Exception $e){
			echo $e->getMessage();
			exit();
		}
	}
	
	/**
	 * Metodo: Ejecutar los procesos automaticos
	 * @param string $usuario
	 */
	public function ejecutar($usuario){				
		$this->usuario = $usuario;
		$this->obtener_procesos();
		
		/* PROCESOS */
		//$this->proceso_morosas();
		$this->proceso_inactivar_beneficiario();
	}
	
	/**
	 * Metodo: Obtener los procesos a ejecutar
	 */
	private function obtener_procesos(){
		$this->arrDatosProcesos = array();
		
		$querySql = "SELECT id_proceso, id_tipo_proceso, rango_dia, 
						hora_ejecuta, estado,
						usuario_ejecuta,convert(VARCHAR(12),fecha_ultima_proceso,111) AS fecha_ultima_proceso
					FROM aportes555";
		$rsDatosFecha = self::$con->querySimple($querySql);
		
		while(($rowDatoFecha = $rsDatosFecha->fetchObject())==true){
			$this->arrDatosProcesos[$rowDatoFecha->id_tipo_proceso][] = $rowDatoFecha;
		}
		
		if(!is_array($this->arrDatosProcesos) && count(($this->arrDatosProcesos))==0){
			$this->mensajeError = "Error no existen registros de procesos en la DB,obtener_procesos";
			$this->logError();
		}
	}
	
	/**
	 * Metodo: Valida si el proceso esta listo para ejecutarse 
	 */
	private function validar_estado_proceso(){
		$this->estadoProceso = false;
		
		//Verifica si el proceso no esta en ejecucion
		if($this->objDatoProceso->estado=="A"){		
			try{
				//Fecha del sistema
				$fechaActual = new DateTime(date('Y/m/d H:i'));
				
				$fechaEjecucion = date("Y/m/d H:i",
						strtotime("{$this->objDatoProceso->fecha_ultima_proceso} {$this->objDatoProceso->hora_ejecuta} + {$this->objDatoProceso->rango_dia} days"));
				$fechaEjecucion = new DateTime($fechaEjecucion);
				
				if($fechaEjecucion <= $fechaActual){
					$this->estadoProceso = true;
				}
				
				/*//Debug
				echo "<br/>fecha Actual<br/>";
				var_dump($fechaActual);
				
				echo "<br/>fecha Ejecuta<br/>";
				var_dump($fechaEjecucion);
				
				echo "<br/>Diferencia ";
				//var_dump($fechaActual  $fechaEjecucion);
				
				//-----*/			
			}catch(Exception $e){
				$this->mensajeError = "Error validando la fecha de estado del proceso,validar_estado_proceso";
				$this->logError();
			}
		}
	}
	
	/**
	 * Metodo: Actualizar la fecha del proceso que fue ejecutado
	 */
	private function actualizar_fecha_proceso(){
		$querySql = "UPDATE aportes555 SET estado='A', fecha_ultima_proceso=CAST(getdate() AS DATETIME), usuario_ejecuta='$this->usuario'
					WHERE id_proceso=".$this->objDatoProceso->id_proceso;
		
		if(self::$con->queryActualiza($querySql)==false){
			$this->mensajeError = "Error al actualizar la fecha ultima del proceso DB,actualizar_fecha_proceso";
			$this->logError();
		}
	}
	
	/**
	 * Metodo: Actualizar el estado del proceso que se esta ejecutando
	 */
	private function actualizar_estado_proceso(){
		$querySql = "UPDATE aportes555 SET estado='P', usuario_ejecuta='$this->usuario'
					WHERE id_proceso=".$this->objDatoProceso->id_proceso;
	
		if(self::$con->queryActualiza($querySql)==false){
			$this->mensajeError = "Error al actualizar el estado del proceso DB,actualizar_estado_proceso";
			$this->logError();
		}
	}
	
	/**
	 * Metodo: Almacena los log de errores que sucedan durante 
	 * 		   el proceso del cron
	 */
	private function logError(){
		//La estructura de atributo $this->mensajeError es: mensaje, metodo
		$dirLogError = $GLOBALS["ruta_cargados"]."cron".DIRECTORY_SEPARATOR."logs".DIRECTORY_SEPARATOR;
		
		//Verificar si el directorio existe
		if(!file_exists($dirLogError)){
			//Crear los directorios
			mkdir($dirLogError,0777,true);
		}
		
		$dirLogError .= "logs".date('Y-m-d').".txt";
		$fpLogError = fopen ( $dirLogError, "a" );
		
		if($fpLogError!=false){
			$this->mensajeError .= ",".date('Y/m/d H:i:s')."\r\n";
			//Escribe el error
			if((fwrite($fpLogError,$this->mensajeError))!=false){
				fclose($fpLogError);
			}
			$this->mensajeError = null;
		}
	}
	
	/*********************************************
	 *  METODOS PARA EJECUTAR LOS PROCEDIMIENTOS *
	**********************************************/
	/**
	 * Metodo: Para ejecutar el tipo proceso [4241][EJECUTAR PROCESO MOROSAS]
	 */
	private function proceso_morosas(){
		$this->objDatoProceso = null;
		
		//Objeto Aportes
		$objAportes = new Aportes();
		
		foreach ($this->arrDatosProcesos["4241"] as $rowDatoProceso){
			$this->objDatoProceso = $rowDatoProceso;
			$this->validar_estado_proceso();
			
			//Ejecutar el proceso
			if($this->estadoProceso==true){
				//Actualizar el estado(P) del Proceso
				$this->actualizar_estado_proceso();
				
				$rsProceso = $objAportes->ejecutar_sp_procesar_morosas($this->usuario);
				
				if($rsProceso==0){
					$this->mensajeError = "Error en el procedimiento almacenado de morosas,proceso_morosas";
					$this->logError();
				}
				
				//Actualizar fecha ultima del proceso
				$this->actualizar_fecha_proceso();
			}
		}
	}
	
	/**
	 * Metodo: Para ejecutar el tipo proceso [4240][INACTIVAR BENEFICIARIOS]
	 */
	private function proceso_inactivar_beneficiario(){
		$this->objDatoProceso = null;
	
		//Objeto Aportes
		$objAfiliacion = new Afiliacion();
	
		foreach ($this->arrDatosProcesos["4240"] as $rowDatoProceso){
			$this->objDatoProceso = $rowDatoProceso;
			$this->validar_estado_proceso();
				
			//Ejecutar el proceso
			if($this->estadoProceso==true){
				//Actualizar el estado(P) del Proceso
				$this->actualizar_estado_proceso();
				
				$rsProceso = $objAfiliacion->inactivar_beneficiarios($this->usuario);
	
				if($rsProceso==0){
					$this->mensajeError = "Error al inactivar los beneficiarios,proceso_inactivar_beneficiario";
					$this->logError();
				}
				//Actualizar fecha ultima del proceso
				$this->actualizar_fecha_proceso();
			}
		}
	}
}
?>