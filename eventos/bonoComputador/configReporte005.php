<?php 
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$url=$_SERVER['PHP_SELF'];
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'auditoria.php';
//include_once 'phpComunes/auditoria.php';
auditar($url);
include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';
//include_once("config.php");

$urlReportes=$url_rep;
global $arregloAgencias;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>::Reporte bonos por fecha::</title>
<link type="text/css" href="<?php echo URL_PORTAL.DIRECTORIO_ESTILOS_CSS; ?>formularios/base/ui.all.css" rel="stylesheet" />
<link type="text/css" href="<?php echo URL_PORTAL.DIRECTORIO_ESTILOS_CSS; ?>Estilos.css" rel="stylesheet"/>
<link type="text/css" href="<?php echo URL_PORTAL.DIRECTORIO_ESTILOS_CSS; ?>estilo_tablas.css" rel="stylesheet"/>
<link type="text/css" href="<?php echo URL_PORTAL.DIRECTORIO_ESTILOS_CSS; ?>formularios/demos.css" />
<link type="text/css" href="<?php echo URL_PORTAL.DIRECTORIO_ESTILOS_CSS; ?>/estiloReporte.css" rel="stylesheet"/>
<script type="text/javascript" src="<?php echo URL_PORTAL.DIRECTORIO_SCRIPTS_JS; ?>jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL.DIRECTORIO_SCRIPTS_JS; ?>formularios/ui/ui.core.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL.DIRECTORIO_SCRIPTS_JS; ?>formularios/ui/ui.tabs.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL.DIRECTORIO_SCRIPTS_JS; ?>formularios/ui/ui.dialog.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL.DIRECTORIO_SCRIPTS_JS; ?>formularios/ui/ui.datepicker.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL.DIRECTORIO_SCRIPTS_JS; ?>jquery.alphanumeric.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL.DIRECTORIO_SCRIPTS_JS; ?>comunes.js"></script>
<script type="text/javascript" src="../js/reportes.js"></script>
<script type="text/javascript">
shortcut.add("Shift+F",function() {
		                                var URL=src();
		var url=URL+"aportes/trabajadores/consultaTrabajador.php";
    	window.open(url,"_blank");
    },{
	'propagate' : true,
	'target' : document 
        });        
</script>
<script >
$(document).ready(function() {
	$( "#filtro_fechaInicial" ).datepicker({dateFormat: 'mm-dd-yy'});
	$( "#filtro_fechaFinal" ).datepicker({dateFormat: 'mm-dd-yy'});
});
</script>

</head>
<table width="100%" border="0">
  <tr>
    <td ><img src="../../../imagenes/logo_reporte.png" width="707" height="86" /></td>
  </tr>
  <tr>
    <td align="center" ><strong class="titulo">Reporte bonos por fecha</strong></td>
  </tr>
</table>

<body>
	<form name="frmReporteBonos" id="frmReporteBonos" action="<?php echo $urlReportes ."evento/bonosCP/reporte005.php"; ?>" method="post" class="clasea" >
		<table class="tablaR hover">
			<tr>
				<td colspan='2'>
					<fieldset>
						<legend>Rango de fechas</legend>
						<label for='filtro_fechaInicial'>Fecha incial:</label> <input type='text' name='filtro[fechaInicial]' id='filtro_fechaInicial' /><br/>
						<label for='filtro_fechaFinal'>Fecha final:</label> <input type='text' name='filtro[fechaFinal]' id='filtro_fechaFinal' /><br/>
					</fieldset>
				</td>
			</tr>
			<tr>
				<td align="center"  colspan="2"><input type="submit" value="Consultar" /></td>
			</tr>
		</table>
	</form>
</body>
</html>