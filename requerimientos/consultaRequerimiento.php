
<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
$usuario=$_SESSION['USUARIO'];
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';

$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

$sql = "SELECT aportes084.*, aportes091.detalledefinicion FROM aportes084 INNER JOIN aportes091 ON aportes084.idtiporequerimiento = aportes091.iddetalledef";

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Requerimiento</title>
<link type="text/css" href="../css/Estilos.css" rel="stylesheet"/>
<link type="text/css" href="../css/marco.css" rel="stylesheet"/>
<link type="text/css" rel="stylesheet" href="../css/css.1.8/jquery-ui-1.8.17.custom.css" />
<script language="javascript" src="../js/1.6/jquery-1.6.2.min.js"></script>
<script language="javascript" src="../js/1.6/jquery-ui-1.8.16.custom.min.js"></script>
<script language="javascript" src="../js/comunes"></script>
<script language="javascript" src="js/requerimientos.js"></script>

</head>

<body>
<center>
<table width="80%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td class="arriba_iz" >&nbsp;</td>
<td class="arriba_ce" ><span class="letrablanca">::Consulta Requerimientos&nbsp;::</span></td>
<td class="arriba_de" >&nbsp;</td>
</tr>
<tr>
<td class="cuerpo_iz">&nbsp;</td>
<td class="cuerpo_ce">
</td>
<td class="cuerpo_de">&nbsp;</td>
</tr>
<tr>
<td class="cuerpo_iz" >&nbsp;</td>
<td class="cuerpo_ce" >
<table width="100%" border="0" class="tablero" cellspacing="0">
  <tr>
    <td width="2%">Nu</td>
    <td width="7%">Fecha</td>
    <td width="10%">Tipo</td>
    <td width="21%">Requerimiento</td>
    <td width="5%">Apro</td>
    <td width="9%">Fecha Aprobaci&oacute;n</td>
    <td width="9%">Asignado A</td>
    <td width="9%">Fecha Asignaci&oacute;n </td>
    <td width="9%">Fecha Entrega </td>
    <td width="9%">Notas</td>
    <td width="10%">Usuario</td>
  </tr>
    <?php
    /*
     * idrequerimiento
    * idtiporequerimiento
    * requerimiento
    * aprobado
    * fechaaprobacion
    * asignadoa
    * fechaasignacion
    * fechaentrega
    * notas
    * fechasistema
    * usuario
    * */
		$consulta= $db->querySimple($sql);
		while($row = $consulta->fetch()){
			echo "<tr><td>".$row['idrequerimiento']."</td><td>".$row['fechasistema']."</td><td>".$row['detalledefinicion']."</td>
			<td>".$row['requerimiento']."</td><td>".$row['aprobado']."</td><td>".$row['fechaaprobacion']."</td><td>".$row['asignadoa']."</td>
			<td>".$row['fechaasignacion']."</td><td>".$row['fechaentrega']."</td><td>".$row['notas']."</td><td>".$row['usuario']."</td></tr>";
		}
	?>
</table></td>
<td class="cuerpo_de" >&nbsp;</td>
</tr>
<tr>
<td class="cuerpo_iz" >&nbsp;</td>
<td class="cuerpo_ce" >&nbsp;</td>
<td class="cuerpo_de" >&nbsp;</td>
</tr>
<tr>
<td class="abajo_iz" >&nbsp;</td>
<td class="abajo_ce" >&nbsp;</td>
<td class="abajo_de" >&nbsp;</td>
</tr>
</table>

</center>
</body>
</html>
