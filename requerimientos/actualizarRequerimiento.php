<?php

date_default_timezone_set('America/Bogota');
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
/*
 * idrequerimiento
* idtiporequerimiento
* requerimiento
* aprobado
* fechaaprobacion
* asignadoa
* fechaasignacion
* fechaentrega
* notas
* fechasistema
* usuario
* */
$campo0=(empty($_REQUEST['v0'])) ? NULL : $_REQUEST['v0'];	//idrequerimiento
$campo1=(empty($_REQUEST['v1'])) ? NULL : $_REQUEST['v1'];	//aprobado
$campo2=(empty($_REQUEST['v2'])) ? NULL : $_REQUEST['v2'];	//fechaaprobacion
$campo3=(empty($_REQUEST['v3'])) ? NULL : $_REQUEST['v3'];	//asignadoa
$campo4=(empty($_REQUEST['v4'])) ? NULL : $_REQUEST['v4'];	//fechaasignacion
$campo5=(empty($_REQUEST['v5'])) ? NULL : $_REQUEST['v5'];	//fechaentrega
$campo6=(empty($_REQUEST['v6'])) ? NULL : $_REQUEST['v6'];	//notas

$sql="UPDATE aportes084 SET
		aprobado = :campo1,
		fechaaprobacion = :campo2,
		asignadoa = :campo3,
		fechaasignacion = :campo4,
		fechaentrega = :campo5,
		notas = :campo6
		WHERE idrequerimiento = :campo0";
$statement = $db->conexionID->prepare($sql);
$guardada = false;

$statement->bindParam(":campo0", $campo0, PDO::PARAM_INT);
$statement->bindParam(":campo1", $campo1, PDO::PARAM_STR);
$statement->bindParam(":campo2", $campo2, PDO::PARAM_STR);
$statement->bindParam(":campo3", $campo3, PDO::PARAM_STR);
$statement->bindParam(":campo4", $campo4, PDO::PARAM_STR);
$statement->bindParam(":campo5", $campo5, PDO::PARAM_STR);
$statement->bindParam(":campo6", $campo6, PDO::PARAM_STR);
$guardada = $statement->execute();
if($guardada){
	// buscar id de la empresa creada
	$rs = '1';
	echo $rs;
}else{
	// errores
	echo 0;
}


?>