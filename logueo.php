<?php
/* autor:       orlando puentes
 * fecha:       05/31/2011
 * objetivo:    
 */
ini_set("display_errors",'1');
//$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
//include_once  $root;
include_once 'config.php';

include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'session.php';
iniciar();
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
include_once 'seguridad/phpComunes/funciones.php';
//include_once 'cron/cron.class.php';

$db = IFXDbManejador::conectarDB();
$hoy =date('U');
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

$fechaHoy=date("m/d/Y");
$sql="Select count(*) as cuenta from aportes550 where proceso_inactivos='$fechaHoy'";
$rs = $db->querySimple($sql);
$row=$rs->fetch();
if($row['cuenta']==0){
	$sql="UPDATE aportes519 SET estado='I' where fechaterminacion < '$hoy'";
	$rs=$db->querySimple($sql);
	$sql="Update aportes550 set proceso_inactivos='$fechaHoy'";
	$rs=$db->querySimple($sql);	
	}
$sql="select DATEADD(month,-1,getdate()) as fecha";
$rs=$db->querySimple($sql);
$row=$rs->fetch();
$fecha=$row['fecha'];
$anno = substr($fecha,0,4);
$mes = substr($fecha,5,2);
$periodo =$anno.$mes;
$sql="Select count(*) as cuenta from aportes550 where periodo='$periodo'";
$rs = $db->querySimple($sql);
$row=$rs->fetch();
if($row['cuenta']==0){
	$sql="update aportes550 set periodo=$periodo";
	$rs=$db->queryActualiza($sql);
}

$u=$_REQUEST['v0'];
$c=md5($_REQUEST['v1']);
$sql = "select * from aportes519 where usuario='$u' and clave='$c'";
$rs = $db->querySimple($sql);
$con=0;
while($row = $rs->fetch()){
	$con++;
	$idUsuario = $row['idusuario'];
	$nombre=$row['nombres'];
	$ida=$row['idagencia'];
	$estado=$row['estado'];
	$cambio=$row['cambio'];
	$idrol=$row['idrol'];
	$fechaexpira=$row['fechaexpira'];
	$temporal=$row['inactivaciontemporal'];
	$fcambio=$row['fechacambio'];
	//$adiciona=$row[''];
	//$modifica=$row[''];
}

if($con==0){
	echo 0;  //no exite
	ingreso(0,$db,$u);
	exit();
	}
if($estado!='A'){
	echo 1;		//usuario inactivo
	ingreso(1,$db,$u);
	exit();
}	
if($cambio=='N'){
	echo 2;	//no ha cambiado la clave
	ingreso(2,$db,$u);
	session_start();
	$_SESSION['LOGUIADO']='SI';
	$_SESSION['ID_USUARIO']=$idUsuario;
	$_SESSION['USUARIO']=$u;
	$_SESSION['USSER']=$nombre;
	$_SESSION['AGENCIA']="0".$ida;
	$_SESSION['IDROL']=$idrol;
	$_SESSION['ADICIONA']='S';  //$adiciona;
	$_SESSION['MODIFICA'] ='S'; //$modifica;
	$_SESSION['RAIZ']=$raiz;
	$_SESSION['CEDULA']=$u;
	$_SESSION['URLREPORTE'] = $ruta_reportes;
	exit();
}
if($temporal=='S'){
	echo 3;	//inactivacion temporal
	ingreso(3,$db,$u);
	exit();
}
//86400
$diasfaltan=($fcambio-$hoy)/86400;

/*
if($fechaexpira-$hoy<0){
	echo 5;	//clave expiro
	ingreso(5,$db,$u);
	exit();
//corriger ????????????'
}
*/

if($con==1){
	session_start();
	$_SESSION['LOGUIADO']='SI';
	$_SESSION['ID_USUARIO']=$idUsuario;
	$_SESSION['USUARIO']=$u;
	$_SESSION['USSER']=$nombre;
	$_SESSION['AGENCIA']="0".$ida;
	$_SESSION['IDROL']=$idrol;
	$_SESSION['ADICIONA']='S';  //$adiciona;
	$_SESSION['MODIFICA'] ='S'; //$modifica;
	$_SESSION['RAIZ']=$raiz;
	$_SESSION['CEDULA']=$u;
	$_SESSION['URLREPORTE'] = $ruta_reportes;
	
if($diasfaltan<=0){
	echo 4;	//cambio de clave
	ingreso(4,$db,$u);
	exit();
}

if($diasfaltan<6){
	echo 100+$diasfaltan; //clave por vencer
	}
else {
	echo 10;	//usuario validado
	ingreso(10,$db,$u);
	}
	}
	
$rs->closeCursor();

function ingreso($op,$db,$u){
	 if ($_SERVER) {  
       if ( $_SERVER["HTTP_X_FORWARDED_FOR"] ) {  
           $realip = $_SERVER["HTTP_X_FORWARDED_FOR"];  
       } elseif ( $_SERVER["HTTP_CLIENT_IP"] ) {  
           $realip = $_SERVER["HTTP_CLIENT_IP"];  
       } else {  
           $realip = $_SERVER["REMOTE_ADDR"];  
       }  
    } else {  
       if ( getenv( 'HTTP_X_FORWARDED_FOR' ) ) {  
          $realip = getenv( 'HTTP_X_FORWARDED_FOR' );  
       } elseif ( getenv( 'HTTP_CLIENT_IP' ) ) {  
          $realip = getenv( 'HTTP_CLIENT_IP' );  
       } else {  
          $realip = getenv( 'REMOTE_ADDR' );  
       }  
   }  
	switch(intval($op)){
		case 0 : $status="Usuario o clave no existe"; break;
		case 1 : $status="Usuario inactivo"; break;
		case 2 : $status="No ha cambiado la clave"; break;
		case 3 : $status="Inactivacion temporal"; break;
		case 4 : $status="Clave vencida"; break;
		case 5 : $status="Usuario expiro "; break;
		case 10 : $status="Logueo exitoso"; break;
		} 
		$inicio = date("m/d/Y g:i:s am");
		$sql="Delete from aportes518 where usuario='$u'";
		$rs = $db->queryActualiza($sql);
		$sql="insert into aportes518 (usuario,inicia,ip) values('$u','$inicio','$realip')";
		$rs = $db->queryActualiza($sql);
		//$rs->closeCursor();
	}
	
	include_once $raiz.DIRECTORY_SEPARATOR.'cron'.DIRECTORY_SEPARATOR.'cron.class.php';
	
	//METODOS---------------------------
	$objeto = new Cron();
	$rs= $objeto->obtener_procesos();
	//----------------------------------
?>