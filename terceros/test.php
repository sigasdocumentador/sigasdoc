<?php
   ini_set('display_errors','1');
   error_reporting(E_ALL);
   include_once '../config.php';
   include 'Terceros.php';

   function Consulta($nit){
		$arraytercero=new TerceroProcesar();
	    $arraytercero->setActionType("I");
	    $entity=new Entity();
	    $entity->setEntityTaxID($nit);
		$arraytercero->setEntity($entity);
		$tercero=new Terceros($arraytercero,true);
		$response=$tercero->procesar();
		return $response;
	}

  function Creacion($user,$campo2,$campo3,$campo7,$campo9,$campo10,$campo11,$campo13,$campo16){

  		$arraytercero=new TerceroProcesar();
  	    $arraytercero->setActionType("A");
  	    $entity=new Entity();
  	 	$entity->setEntityTypeCode("C");
        $nit=add_space_to_nit($campo2."-".$campo3);
  	    $entity->setEntityTaxID($nit);
  		$entity->setEntityName(clean_name($campo7));
  		$entity->setIndustryClassificationCode(" ");
  		$entity->setCreditMessageCode(" ");
  		$entity->setPersonCorporationCode("A");
  		$entity->setEntityParentID(" ");
  		$entity->setEntityParentTaxID(" ");
  		$entity->setEntityBeneficiaryID(" ");
  		$entity->setEntityBeneficiaryTaxID(" ");
  		$entity->setCategoryCode003("06");
  		$entity->setCategoryCode004("000");
  		$entity->setCategoryCode008("020");
  		$entity->setCategoryCode009("NA");
  		$entity->setCategoryCode010("NA");
  		$entity->setCategoryCode011("NAL");
  		$entity->setCategoryCode012(" ");
  		$entity->setCategoryCode013(" ");
  		$entity->setCategoryCode016("EMP");
  		$entity->setCategoryCode021("NA");
  		$entity->setEntityAddressLine1($campo9);
  		$entity->setEntityAddressLine2(" ");
  		$entity->setEntityAddressLine3(" ");
  		$entity->setEntityAddressLine4(" ");
  		$entity->setEntityCity($campo11);
  		$entity->setEntityStateCode($campo10);
  		$entity->setEntityEmail($campo16);
  		$entity->setTaxPayerTypeColombia("A");
		
		$today=date("Ymd");
  		$entity->setDateEffective($today);
  		$entity->setDateExpir("20501231");

        //Por Verificat
  		$entity->setCiiuCodeColombia("0010");

		$entityp=new EntityPhone();
  		$entityp->setAreaCode(" ");
		//$entityp->setPhoneTypeCode($tel_type);
		$entityp->setPhoneTypeCode("");
  		$entityp->setPhoneNumber($campo13);
  		$entity->setEntityPhone($entityp);
  		$arraytercero->setEntity($entity);
		
		
  		$supplier=new SupplierInfo();
  		$supplier->setGlOffsetCode("COGT");
  		$supplier->setPaymentInstrmentCode("C");
  		$supplier->setPaymentTermsCode("030");
		
  		$bankacc=new BankAccount();
  		/*$bankacc->setBankTransitNumber("Banco Pruebas");
  		$bankacc->setCustBankAcctNumber("9999999999001");
  		$bankacc->setDescription001("Cuenta bancaria de pruebas");
  		$bankacc->setCodigoSWIFT("019");
  		$bankacc->setCheckingorSavingsAccount(" ");*/
		
  		$bankacc2=new BankAccount();
  		/*$bankacc2->setBankTransitNumber("Banco Pruebas");
  		$bankacc2->setCustBankAcctNumber("9999999999002");
  		$bankacc2->setDescription001("Cuenta bancaria de pruebas");
  		$bankacc2->setCodigoSWIFT("019");
  		$bankacc2->setCheckingorSavingsAccount("0");*/
		
  		$supplier->setBankAccount(array($bankacc,$bankacc2));
		
		
  		//$arraytercero->setSupplierInfo($supplier);
  		$custom=new CustomerInfo();
  		$custom->setCompany("00002");
  		$custom->setPaymentTermsCode("030");
  		$custom->setCollectionManagerCode("GC02");
  		$custom->setPolicyNumber("7");
  		$custom->setPolicyCompany("00000");
  		$custom->setGlOffset("CLIE");
  		$custom->setTaxRateAreaCode("VENTAS");
  		$custom->setTaxExplanationCode("V");
  		$arraytercero->setCustomerInfo($custom);
  		$audit=new AuditInfo();
  		$audit->setUserID($user);
  		$audit->setProgramID("SIGAS");
  		$audit->setWorkStationID("SIGASWEB");
  		$arraytercero->setAuditInfo($audit);
  		$tercero=new Terceros($arraytercero);
  		$response=$tercero->procesar();
  		return $response;

  	}

  function Actualizacion($user,$campo2,$campo3,$campo7,$campo9,$campo10,$campo11,$campo13,$campo16){

  		$arraytercero=new TerceroProcesar();
  	    $arraytercero->setActionType("C");
  	    $entity=new Entity();
  	 	$entity->setEntityTypeCode("C");
        $nit=add_space_to_nit($campo2."-".$campo3);
		echo "nit: ".$nit."<br/>";
  	    $entity->setEntityTaxID($nit);
		echo "name: ".clean_name($campo7)."<br/>";
  		$entity->setEntityName(clean_name($campo7));
  		$entity->setIndustryClassificationCode(" ");
  		$entity->setCreditMessageCode(" ");
  		$entity->setPersonCorporationCode("A");
  		$entity->setEntityParentID(" ");
  		$entity->setEntityParentTaxID(" ");
  		$entity->setEntityBeneficiaryID(" ");
  		$entity->setEntityBeneficiaryTaxID(" ");
  		$entity->setCategoryCode003("06");
  		$entity->setCategoryCode004("000");
  		$entity->setCategoryCode008("020");
  		$entity->setCategoryCode009("NA");
  		$entity->setCategoryCode010("NA");
  		$entity->setCategoryCode011("NAL");
  		$entity->setCategoryCode012(" ");
  		$entity->setCategoryCode013(" ");
  		$entity->setCategoryCode016("EMP");
  		$entity->setCategoryCode021("NA");
  		$entity->setEntityAddressLine1($campo9);
  		$entity->setEntityAddressLine2(" ");
  		$entity->setEntityAddressLine3(" ");
  		$entity->setEntityAddressLine4(" ");
  		$entity->setEntityCity($campo11);
  		$entity->setEntityStateCode($campo10);
  		$entity->setEntityEmail($campo16);
  		$entity->setTaxPayerTypeColombia("A");
		
		$today=date("Ymd");
  		$entity->setDateEffective($today);
  		$entity->setDateExpir("20501231");

        //Por Verificat
  		$entity->setCiiuCodeColombia("0010");

		$entityp=new EntityPhone();
  		$entityp->setAreaCode(" ");
		//$entityp->setPhoneTypeCode($tel_type);
		$entityp->setPhoneTypeCode("");
  		$entityp->setPhoneNumber($campo13);
  		//$entity->setEntityPhone($entityp);//Adding phone to main entity
		
  		$arraytercero->setEntity($entity);
		
		
  		$supplier=new SupplierInfo();
  		$supplier->setGlOffsetCode("COGT");
  		$supplier->setPaymentInstrmentCode("C");
  		$supplier->setPaymentTermsCode("030");
		
		
		$bankacc=new BankAccount();
		/*$bankacc->setBankTransitNumber("Banco Pruebas");
		$bankacc->setCustBankAcctNumber("9999999999001");
		$bankacc->setDescription001("Cuenta bancaria de pruebas");
		$bankacc->setCodigoSWIFT("019");
		$bankacc->setCheckingorSavingsAccount("2");*/
		$bankacc2=new BankAccount();
		/*$bankacc2->setBankTransitNumber("Banco Pruebas");
		$bankacc2->setCustBankAcctNumber("9999999999002");
		$bankacc2->setDescription001("Cuenta bancaria de pruebas");
		$bankacc2->setCodigoSWIFT("019");
		$bankacc2->setCheckingorSavingsAccount("2");*/
		$supplier->setBankAccount(array($bankacc,$bankacc2));
		$arraytercero->setSupplierInfo($supplier);
		
		$custom=new CustomerInfo();
  		$custom->setCompany("00002");
  		$custom->setPaymentTermsCode("030");
  		$custom->setCollectionManagerCode("GC02");
  		$custom->setPolicyNumber("7");
  		$custom->setPolicyCompany("00000");
  		$custom->setGlOffset("CLIE");
  		$custom->setTaxRateAreaCode("VENTAS");
  		$custom->setTaxExplanationCode("V");
  		$arraytercero->setCustomerInfo($custom);
		
  		$audit=new AuditInfo();
  		$audit->setUserID($user);
  		$audit->setProgramID("SIGAS");
  		$audit->setWorkStationID("SIGASWEB");
  		$arraytercero->setAuditInfo($audit);
		
		$tercero=new Terceros($arraytercero);
		$response=$tercero->procesar();
	    return $response;
	}


	function add_space_to_nit($nit)
	{
		$aux=trim($nit);
		$count_nit=strlen($aux);
		if($count_nit<16){
		  $aux=str_pad($aux,16," ",STR_PAD_LEFT);
		}
		return $aux;
	}

	function clean_name($name)
	{
		$name=str_replace("&"," ",$name);
		return $name;
	}

	//exit(add_space_to_nit("1075227898-6"));
	//descomentar la linea siguiente para ver el ejemplo de consulta
	//$respuesta = Consulta();
	//var_dump($respuesta);
	//echo "<br/><br/>";
	//descomentar la linea siguiente para ver el ejemplo de creacion
	
	//$ciudad="NEIVA";
	//$respuesta =Creacion("tilag","1.075.227.896","6","SERGIO IVAN MEDINA GOMEZ","CALLE 55 1 D- 30","41",$ciudad,"8750403","xxx@xxx.com");
	//debug($respuesta);
	
	debug(Consulta("1.075.227.896-6"));
	/*echo "<br/><br/>";
	$ciudad="BOGOTA";
	$respuesta =Actualizacion("tilag","1.075.227.896","6","SERGIO CAMILO MEDINA GOMEZ","CALLE 55 1 D- 31","41",$ciudad,"8750403","xxx@xxx.com");
	echo "<br/><br/>";
	var_dump(get_object_vars($respuesta));
	echo "<br/><br/>";
	var_dump(get_class_methods($respuesta));
	echo "<br/><br/>";
	$aux=$respuesta->getError();
	var_dump($aux->getErrorCode());*/
	echo "<br/><br/>";
	/*if($respuesta->getError()!=NULL)
	{
		echo "force Create!!<br/> ".$respuesta->getError()->getErrorCode();
		
	}
	/*if(isset($respuesta->getError()))
	{
		echo "THERE WAS AN ERROR";
		echo "<br/><br/>";
	}*/
	//print_r($respuesta);
    echo "<br/><br/>";
	
	//$respuesta = Consulta();
	//var_dump($respuesta);
	//echo "<br/><br/>";
	//descomentar la linea siguiente para ver el ejemplo de actualizacion
	//$respuesta=Actualizacion();
	//var_dump($respuesta);
?>
