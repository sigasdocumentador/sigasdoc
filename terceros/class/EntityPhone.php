<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EntityPhone
 *
 * @author nuevastic
 */
class EntityPhone {
    private $areaCode;
    private $phoneTypeCode;
    private $phoneNumber;
    function __construct($areaCode=null, $phoneTypeCode=null, $phoneNumber=null) {
        $this->areaCode = $areaCode;
        $this->phoneTypeCode = $phoneTypeCode;
        $this->phoneNumber = $phoneNumber;
    }
	
	public function to_json() {
        return json_encode(get_object_vars($this));
    }

    function getAreaCode() {
        return $this->areaCode;
    }

    function getPhoneTypeCode() {
        return $this->phoneTypeCode;
    }

    function getPhoneNumber() {
        return $this->phoneNumber;
    }

    function setAreaCode($areaCode) {
        $this->areaCode = $areaCode;
    }

    function setPhoneTypeCode($phoneTypeCode) {
        $this->phoneTypeCode = $phoneTypeCode;
    }

    function setPhoneNumber($phoneNumber) {
        $this->phoneNumber = $phoneNumber;
    }


}
