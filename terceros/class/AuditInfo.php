<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AuditInfo
 *
 * @author nuevastic
 */
class AuditInfo {
    private $userID;
    private $programID;
    private $workStationID;
    public function __construct($userID=null, $programID=null, $workStationID=null) {
        $this->userID = $userID;
        $this->programID = $programID;
        $this->workStationID = $workStationID;
    }
	
	public function to_json() {
        return json_encode(get_object_vars($this));
    }
	
    public function getUserID() {
        return $this->userID;
    }

    public function getProgramID() {
        return $this->programID;
    }

    public function getWorkStationID() {
        return $this->workStationID;
    }

    public function setUserID($userID) {
        $this->userID = $userID;
    }

    public function setProgramID($programID) {
        $this->programID = $programID;
    }

    public function setWorkStationID($workStationID) {
        $this->workStationID = $workStationID;
    }


}
