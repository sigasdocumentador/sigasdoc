<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include("EntityPhone.php");
/**
 * Description of Entity
 *
 * @author nuevastic
 */
class Entity{
    private $entityTypeCode;
    private $entityTaxID;
    private $entityName;
    private $industryClassificationCode;
    private $creditMessageCode;
    private $personCorporationCode;
    private $entityParentID;
    private $entityParentTaxID;
    private $entityBeneficiaryID;
    private $entityBeneficiaryTaxID;
    private $categoryCode003;
    private $categoryCode004;
    private $categoryCode008;
    private $categoryCode009;
    private $categoryCode010;
    private $categoryCode011;
    private $categoryCode012;
    private $categoryCode013;
    private $categoryCode016;
    private $categoryCode021;
	private $entityAddressLine1;
    private $entityAddressLine2;
    private $entityAddressLine3;
    private $entityAddressLine4;
    private $entityCity;
    private $entityStateCode;
    private $entityEmail;
    private $taxPayerTypeColombia;
    private $dateEffective;
    private $dateExpir;
    private $ciiuCodeColombia;
    private $entityPhone;    
    public function __construct($entityTypeCode=null, $entityTaxID=null, $entityName=null, $industryClassificationCode=null, $creditMessageCode=null, $personCorporationCode=null, $entityParentID=null, $entityParentTaxID=null, $entityBeneficiaryID=null, $entityBeneficiaryTaxID=null, $categoryCode003=null, $categoryCode004=null, $categoryCode008=null, $categoryCode009=null, $categoryCode010=null, $categoryCode011=null, $categoryCode012=null, $categoryCode013=null, $categoryCode016=null, $categoryCode021=null, $entityAddressLine1=null,$entityAddressLine2=null, $entityAddressLine3=null, $entityAddressLine4=null, $entityCity=null, $entityStateCode=null, $entityEmail=null, $taxPayerTypeColombia=null, $dateEffective=null, $dateExpir=null, $ciiuCodeColombia=null, $entityPhone=null) {
        $this->entityTypeCode = $entityTypeCode;
        $this->entityTaxID = $entityTaxID;
        $this->entityName = $entityName;
        $this->industryClassificationCode = $industryClassificationCode;
        $this->creditMessageCode = $creditMessageCode;
        $this->personCorporationCode = $personCorporationCode;
        $this->entityParentID = $entityParentID;
        $this->entityParentTaxID = $entityParentTaxID;
        $this->entityBeneficiaryID = $entityBeneficiaryID;
        $this->entityBeneficiaryTaxID = $entityBeneficiaryTaxID;
        $this->categoryCode003 = $categoryCode003;
        $this->categoryCode004 = $categoryCode004;
        $this->categoryCode008 = $categoryCode008;
        $this->categoryCode009 = $categoryCode009;
        $this->categoryCode010 = $categoryCode010;
        $this->categoryCode011 = $categoryCode011;
        $this->categoryCode012 = $categoryCode012;
        $this->categoryCode013 = $categoryCode013;
        $this->categoryCode016 = $categoryCode016;
        $this->categoryCode021 = $categoryCode021;
		$this->entityAddressLine1 = $entityAddressLine1;
        $this->entityAddressLine2 = $entityAddressLine2;
        $this->entityAddressLine3 = $entityAddressLine3;
        $this->entityAddressLine4 = $entityAddressLine4;
        $this->entityCity = $entityCity;
        $this->entityStateCode = $entityStateCode;
        $this->entityEmail = $entityEmail;
        $this->taxPayerTypeColombia = $taxPayerTypeColombia;
        $this->dateEffective = $dateEffective;
        $this->dateExpir = $dateExpir;
        $this->ciiuCodeColombia = $ciiuCodeColombia;
        $this->entityPhone = $entityPhone;
    }
    public function getEntityTypeCode() {
        return $this->entityTypeCode;
    }

    public function getEntityTaxID() {
        return $this->entityTaxID;
    }

    public function getEntityName() {
        return $this->entityName;
    }

    public function getIndustryClassificationCode() {
        return $this->industryClassificationCode;
    }

    public function getCreditMessageCode() {
        return $this->creditMessageCode;
    }

    public function getPersonCorporationCode() {
        return $this->personCorporationCode;
    }

    public function getEntityParentID() {
        return $this->entityParentID;
    }

    public function getEntityParentTaxID() {
        return $this->entityParentTaxID;
    }

    public function getEntityBeneficiaryID() {
        return $this->entityBeneficiaryID;
    }

    public function getEntityBeneficiaryTaxID() {
        return $this->entityBeneficiaryTaxID;
    }

    public function getCategoryCode003() {
        return $this->categoryCode003;
    }

    public function getCategoryCode004() {
        return $this->categoryCode004;
    }

    public function getCategoryCode008() {
        return $this->categoryCode008;
    }

    public function getCategoryCode009() {
        return $this->categoryCode009;
    }

    public function getCategoryCode010() {
        return $this->categoryCode010;
    }

    public function getCategoryCode011() {
        return $this->categoryCode011;
    }

    public function getCategoryCode012() {
        return $this->categoryCode012;
    }

    public function getCategoryCode013() {
        return $this->categoryCode013;
    }

    public function getCategoryCode016() {
        return $this->categoryCode016;
    }

    public function getCategoryCode021() {
        return $this->categoryCode021;
    }

	public function getEntityAddressLine1() {
        return $this->entityAddressLine1;
    }
    public function getEntityAddressLine2() {
        return $this->entityAddressLine2;
    }

    public function getEntityAddressLine3() {
        return $this->entityAddressLine3;
    }

    public function getEntityAddressLine4() {
        return $this->entityAddressLine4;
    }

    public function getEntityCity() {
        return $this->entityCity;
    }

    public function getEntityStateCode() {
        return $this->entityStateCode;
    }

    public function getEntityEmail() {
        return $this->entityEmail;
    }

    public function getTaxPayerTypeColombia() {
        return $this->taxPayerTypeColombia;
    }

    public function getDateEffective() {
        return $this->dateEffective;
    }

    public function getDateExpir() {
        return $this->dateExpir;
    }

    public function getCiiuCodeColombia() {
        return $this->ciiuCodeColombia;
    }

    public function getEntityPhone() {
        return $this->entityPhone;
    }

    public function setEntityTypeCode($entityTypeCode) {
        $this->entityTypeCode = $entityTypeCode;
    }

    public function setEntityTaxID($entityTaxID) {
        $this->entityTaxID = $entityTaxID;
    }

    public function setEntityName($entityName) {
        $this->entityName = $entityName;
    }

    public function setIndustryClassificationCode($industryClassificationCode) {
        $this->industryClassificationCode = $industryClassificationCode;
    }

    public function setCreditMessageCode($creditMessageCode) {
        $this->creditMessageCode = $creditMessageCode;
    }

    public function setPersonCorporationCode($personCorporationCode) {
        $this->personCorporationCode = $personCorporationCode;
    }

    public function setEntityParentID($entityParentID) {
        $this->entityParentID = $entityParentID;
    }

    public function setEntityParentTaxID($entityParentTaxID) {
        $this->entityParentTaxID = $entityParentTaxID;
    }

    public function setEntityBeneficiaryID($entityBeneficiaryID) {
        $this->entityBeneficiaryID = $entityBeneficiaryID;
    }

    public function setEntityBeneficiaryTaxID($entityBeneficiaryTaxID) {
        $this->entityBeneficiaryTaxID = $entityBeneficiaryTaxID;
    }

    public function setCategoryCode003($categoryCode003) {
        $this->categoryCode003 = $categoryCode003;
    }

    public function setCategoryCode004($categoryCode004) {
        $this->categoryCode004 = $categoryCode004;
    }

    public function setCategoryCode008($categoryCode008) {
        $this->categoryCode008 = $categoryCode008;
    }

    public function setCategoryCode009($categoryCode009) {
        $this->categoryCode009 = $categoryCode009;
    }

    public function setCategoryCode010($categoryCode010) {
        $this->categoryCode010 = $categoryCode010;
    }

    public function setCategoryCode011($categoryCode011) {
        $this->categoryCode011 = $categoryCode011;
    }

    public function setCategoryCode012($categoryCode012) {
        $this->categoryCode012 = $categoryCode012;
    }

    public function setCategoryCode013($categoryCode013) {
        $this->categoryCode013 = $categoryCode013;
    }

    public function setCategoryCode016($categoryCode016) {
        $this->categoryCode016 = $categoryCode016;
    }

    public function setCategoryCode021($categoryCode021) {
        $this->categoryCode021 = $categoryCode021;
    }

	public function setEntityAddressLine1($entityAddressLine1) {
        $this->entityAddressLine1 = $entityAddressLine1;
    }
	
    public function setEntityAddressLine2($entityAddressLine2) {
        $this->entityAddressLine2 = $entityAddressLine2;
    }

    public function setEntityAddressLine3($entityAddressLine3) {
        $this->entityAddressLine3 = $entityAddressLine3;
    }

    public function setEntityAddressLine4($entityAddressLine4) {
        $this->entityAddressLine4 = $entityAddressLine4;
    }

    public function setEntityCity($entityCity) {
        $this->entityCity = $entityCity;
    }

    public function setEntityStateCode($entityStateCode) {
        $this->entityStateCode = $entityStateCode;
    }

    public function setEntityEmail($entityEmail) {
        $this->entityEmail = $entityEmail;
    }

    public function setTaxPayerTypeColombia($taxPayerTypeColombia) {
        $this->taxPayerTypeColombia = $taxPayerTypeColombia;
    }

    public function setDateEffective($dateEffective) {
        $this->dateEffective = $dateEffective;
    }

    public function setDateExpir($dateExpir) {
        $this->dateExpir = $dateExpir;
    }

    public function setCiiuCodeColombia($ciiuCodeColombia) {
        $this->ciiuCodeColombia = $ciiuCodeColombia;
    }

    public function setEntityPhone($entityPhone) {
        $this->entityPhone = $entityPhone;
    }
	
	public function to_json() {
        return json_encode(get_object_vars($this));
    }

}
