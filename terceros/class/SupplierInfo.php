<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include("BankAccount.php");
/**
 * Description of SupplierInfo
 *
 * @author nuevastic
 */
class SupplierInfo {
   private $glOffsetCode;
   private $paymentInstrmentCode;
   private $paymentTermsCode;
   private $bankAccount;
   public function __construct($glOffsetCode=null, $paymentInstrmentCode=null, $paymentTermsCode=null, $bankAccount=null) {
       $this->glOffsetCode = $glOffsetCode;
       $this->paymentInstrmentCode = $paymentInstrmentCode;
       $this->paymentTermsCode = $paymentTermsCode;
       $this->bankAccount = $bankAccount;
   }
   
   public function to_json() {
        return json_encode(get_object_vars($this));
    }

   public function getGlOffsetCode() {
       return $this->glOffsetCode;
   }

   public function getPaymentInstrmentCode() {
       return $this->paymentInstrmentCode;
   }

   public function getPaymentTermsCode() {
       return $this->paymentTermsCode;
   }

   public function getBankAccount() {
       return $this->bankAccount;
   }

   public function setGlOffsetCode($glOffsetCode) {
       $this->glOffsetCode = $glOffsetCode;
   }

   public function setPaymentInstrmentCode($paymentInstrmentCode) {
       $this->paymentInstrmentCode = $paymentInstrmentCode;
   }

   public function setPaymentTermsCode($paymentTermsCode) {
       $this->paymentTermsCode = $paymentTermsCode;
   }

   public function setBankAccount($bankAccount) {
       $this->bankAccount = $bankAccount;
   }


}
