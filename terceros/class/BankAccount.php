<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BankAccount
 *
 * @author nuevastic
 */
class BankAccount {
    private $bankTransitNumber;
    private $custBankAcctNumber;
    private $description001;
    private $codigoSWIFT;
    private $checkingorSavingsAccount;
    
    function __construct($bankTransitNumber=null, $custBankAcctNumber=null, $description001=null, $codigoSWIFT=null, $checkingorSavingsAccount=null) {
        $this->bankTransitNumber = $bankTransitNumber;
        $this->custBankAcctNumber = $custBankAcctNumber;
        $this->description001 = $description001;
        $this->codigoSWIFT = $codigoSWIFT;
        $this->checkingorSavingsAccount = $checkingorSavingsAccount;
    }
	
	public function to_json() {
        return json_encode(get_object_vars($this));
    }
	
    public function getBankTransitNumber() {
        return $this->bankTransitNumber;
    }

    public function getCustBankAcctNumber() {
        return $this->custBankAcctNumber;
    }

    public function getDescription001() {
        return $this->description001;
    }

    public function getCodigoSWIFT() {
        return $this->codigoSWIFT;
    }

    public function getCheckingorSavingsAccount() {
        return $this->checkingorSavingsAccount;
    }

    public function setBankTransitNumber($bankTransitNumber) {
        $this->bankTransitNumber = $bankTransitNumber;
    }

    public function setCustBankAcctNumber($custBankAcctNumber) {
        $this->custBankAcctNumber = $custBankAcctNumber;
    }

    public function setDescription001($description001) {
        $this->description001 = $description001;
    }

    public function setCodigoSWIFT($codigoSWIFT) {
        $this->codigoSWIFT = $codigoSWIFT;
    }

    public function setCheckingorSavingsAccount($checkingorSavingsAccount) {
        $this->checkingorSavingsAccount = $checkingorSavingsAccount;
    }


}
