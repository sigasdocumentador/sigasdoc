<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CustomerInfo
 *
 * @author nuevastic
 */
class CustomerInfo {
    private $company;
    private $paymentTermsCode;
    private $collectionManagerCode;
    private $policyNumber;
    private $policyCompany;
    private $glOffSet;
    private $taxRateAreaCode;
    private $taxExplanationCode;
    
    function __construct($company=null, $paymentTermsCode=null, $collectionManagerCode=null, $policyNumber=null, $policyCompany=null, $glOffSet=null, $taxRateAreaCode=null, $taxExplanationCode=null) {
        $this->company = $company;
        $this->paymentTermsCode = $paymentTermsCode;
        $this->collectionManagerCode = $collectionManagerCode;
        $this->policyNumber = $policyNumber;
        $this->policyCompany = $policyCompany;
        $this->glOffSet = $glOffSet;
        $this->taxRateAreaCode = $taxRateAreaCode;
        $this->taxExplanationCode = $taxExplanationCode;
    }
	
	public function to_json() {
        return json_encode(get_object_vars($this));
    }
	
    public function getCompany() {
        return $this->company;
    }

    public function getPaymentTermsCode() {
        return $this->paymentTermsCode;
    }

    public function getCollectionManagerCode() {
        return $this->collectionManagerCode;
    }

    public function getPolicyNumber() {
        return $this->policyNumber;
    }

    public function getPolicyCompany() {
        return $this->policyCompany;
    }

    public function getGlOffset() {
        return $this->glOffSet;
    }

    public function getTaxRateAreaCode() {
        return $this->taxRateAreaCode;
    }

    public function getTaxExplanationCode() {
        return $this->taxExplanationCode;
    }

    public function setCompany($company) {
        $this->company = $company;
    }

    public function setPaymentTermsCode($paymentTermsCode) {
        $this->paymentTermsCode = $paymentTermsCode;
    }

    public function setCollectionManagerCode($collectionManagerCode) {
        $this->collectionManagerCode = $collectionManagerCode;
    }

    public function setPolicyNumber($policyNumber) {
        $this->policyNumber = $policyNumber;
    }

    public function setPolicyCompany($policyCompany) {
        $this->policyCompany = $policyCompany;
    }

    public function setGlOffset($glOffSet) {
        $this->glOffSet = $glOffSet;
    }

    public function setTaxRateAreaCode($taxRateAreaCode) {
        $this->taxRateAreaCode = $taxRateAreaCode;
    }

    public function setTaxExplanationCode($taxExplanationCode) {
        $this->taxExplanationCode = $taxExplanationCode;
    }



}
