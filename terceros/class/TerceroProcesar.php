<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include("Entity.php");
include("SupplierInfo.php");
include("CustomerInfo.php");
include("AuditInfo.php");
/**
 * Description of TerceroProcesar
 *
 * @author nuevastic
 */
class TerceroProcesar {
    private $actionType;
    private $entity;
    private $supplierInfo;
    private $customerInfo;
    private $auditInfo;
    public function __construct($actionType=null, $entity=null, $supplierInfo=null, $customerInfo=null, $auditInfo=null) {
        $this->actionType = $actionType;
        $this->entity = $entity;
        $this->supplierInfo = $supplierInfo;
        $this->customerInfo = $customerInfo;
        $this->auditInfo = $auditInfo;
    }
    public function getActionType() {
        return $this->actionType;
    }

    public function getEntity() {
        return $this->entity;
    }

    public function getSupplierInfo() {
        return $this->supplierInfo;
    }

    public function getCustomerInfo() {
        return $this->customerInfo;
    }

    public function getAuditInfo() {
        return $this->auditInfo;
    }

    public function setActionType($actionType) {
        $this->actionType = $actionType;
    }

    public function setEntity($entity) {
        $this->entity = $entity;
    }

    public function setSupplierInfo($supplierInfo) {
        $this->supplierInfo = $supplierInfo;
    }

    public function setCustomerInfo($customerInfo) {
        $this->customerInfo = $customerInfo;
    }

    public function setAuditInfo($auditInfo) {
        $this->auditInfo = $auditInfo;
    }


    
}
