<?php
include("Error.php");
class Response{
	private $entityTaxID;
	private $entityAN8;
	private $entityName;
	private $entityAddressLine1;
	private $entityAddressLine2;
	private $entityAddressLine3;
	private $entityAddressLine4;
	private $entityCity;
	private $entityPhone;
	private $error;
	
    public function getEntityTaxID() {
        return $this->entityTaxID;
    }

    public function setEntityTaxID($entityTaxID) {
        $this->entityTaxID = $entityTaxID;
    }

    public function getEntityAN8() {
        return $this->entityAN8;
    }

    public function setEntityAN8($entityAN8) {
        $this->entityAN8 = $entityAN8;
    }

    public function getEntityName() {
        return $this->entityName;
    }

    public function setEntityName($entityName) {
        $this->entityName = $entityName;
    }

    public function getEntityAddressLine1() {
        return $this->entityAddressLine1;
    }

    public function setEntityAddressLine1($entityAddressLine1) {
        $this->entityAddressLine1 = $entityAddressLine1;
    }

    public function getEntityAddressLine2() {
        return $this->entityAddressLine2;
    }

    public function setEntityAddressLine2($entityAddressLine2) {
        $this->entityAddressLine2 = $entityAddressLine2;
    }

    public function getEntityAddressLine3() {
        return $this->entityAddressLine3;
    }

    public function setEntityAddressLine3($entityAddressLine3) {
        $this->entityAddressLine3 = $entityAddressLine3;
    }

    public function getEntityAddressLine4() {
        return $this->entityAddressLine4;
    }

    public function setEntityAddressLine4($entityAddressLine4) {
        $this->entityAddressLine4 = $entityAddressLine4;
    }

    public function getEntityCity() {
        return $this->entityCity;
    }

    public function setEntityCity($entityCity) {
        $this->entityCity = $entityCity;
    }

    public function getEntityPhone() {
        return $this->entityPhone;
    }

    public function setEntityPhone($entityPhone) {
        $this->entityPhone = $entityPhone;
    }

    public function getError() {
        return $this->error;
    }

    public function setError($error) 
	{
		$this->error = $error;
    }
	
	
}
