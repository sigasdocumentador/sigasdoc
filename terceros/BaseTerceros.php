<?php
   $root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
   include_once  $root;
   include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';
   
   include 'Terceros.php';
   
   include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
   include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';


class BaseTerceros{   
   
    private $debug;
	
	function __construct($debug=false){
		$this->debug=$debug;
    }
   
   function ConsultaTercero($nit){
		$arraytercero=new TerceroProcesar();
	    $arraytercero->setActionType("I");
	    $entity=new Entity();
	    if(strpos($nit,"-"))
			$nit=$this->add_space_to_nit($nit);
		$entity->setEntityTaxID($nit);
		$arraytercero->setEntity($entity);
		$tercero=new Terceros($arraytercero,$this->debug);
		$response=$tercero->procesar();
		return $response;
	}
	
	function ExisteTercero($nit)
	{
		$respuesta=$this->ConsultaTercero($nit);
		//debug($respuesta);
		$error=$respuesta->getError();
		//debug(isset($error));
		if(isset($error))
		{
                    //var_dump($error);
			return 0;//Cero es falso
		}
		return 1;//Uno es verdadero
	}

  function CreacionTercero($user,$nit,$razon_social,$direccion,$estado,$codigoCiudad,$ciudad,$telefono,$email,$tipo_documento){

		log_info('Creando tercero con: ', array($user,$nit,$razon_social,$direccion,$estado,$ciudad,$telefono,$email,$tipo_documento));
		if($this->debug)
		{
			debug("creando tercero con");
			debug(array($user,$nit,$razon_social,$direccion,$estado,$ciudad,$telefono,$email));
		}
  		$arraytercero=new TerceroProcesar();
  	    $arraytercero->setActionType("A");
  	    $entity=new Entity();
  	 	$entity->setEntityTypeCode("GF");
		if(strpos($nit,"-"))
			$nit=$this->add_space_to_nit($nit);
  	    $entity->setEntityTaxID($nit);
		
		$razon_social=$this->clean_name($razon_social);
		$razon_social=$this->separar($razon_social);
		$entity->setEntityName($razon_social[0]);//Solo Acepta 40 Caracteres
  		
		$entity->setIndustryClassificationCode(" ");
  		$entity->setCreditMessageCode(" ");
		
		$letter=$this->get_corporation_code_homologacion($tipo_documento);
		$entity->setPersonCorporationCode($letter);
                
                
                $categoryCode021 = $this->obtenerValorHomologacionJDE($codigoCiudad); //Código ciudad tercero
		
		
  		$entity->setEntityParentID(" ");
  		$entity->setEntityParentTaxID(" ");
  		$entity->setEntityBeneficiaryID(" ");
  		$entity->setEntityBeneficiaryTaxID(" ");
  		$entity->setCategoryCode003("06");
  		$entity->setCategoryCode004("000");
  		$entity->setCategoryCode008("020");
  		$entity->setCategoryCode009("NA");
  		$entity->setCategoryCode010("NA");
  		$entity->setCategoryCode011("NAL");
  		$entity->setCategoryCode012(" ");
  		$entity->setCategoryCode013(" ");
  		$entity->setCategoryCode016("EMP");
  		$entity->setCategoryCode021($categoryCode021);
		$direccion=$this->separar($direccion);
		$entity->setEntityAddressLine1($direccion[0]);//>40 usar address2
		if(isset($direccion[1]))
			$entity->setEntityAddressLine2($direccion[1]);
		else
			$entity->setEntityAddressLine2(" ");
		if(isset($direccion[2]))
			$entity->setEntityAddressLine3($direccion[2]);
		else
			$entity->setEntityAddressLine3(" ");
		if(isset($direccion[3]))
			$entity->setEntityAddressLine4($direccion[3]);
		else
			$entity->setEntityAddressLine4(" ");
  		$entity->setEntityCity($ciudad);
  		$entity->setEntityStateCode($estado);
                
                if($email == "" || $email == null || $email == "null" || !isset($email)){
                    $email = "notiene@gmail.com";
                }
                
  		$entity->setEntityEmail($email);
  		$entity->setTaxPayerTypeColombia("A");
		
		$today=date("Ymd");
  		$entity->setDateEffective($today);
  		$entity->setDateExpir("20501231");

                //Por Verificat
  		$entity->setCiiuCodeColombia("0010");

		
                
                if($telefono == "" || $telefono == null || $telefono == "null" || !isset($telefono)){
                   $telefono = "0000000"; 
                }
                
                $entityp=new EntityPhone();
  		$entityp->setAreaCode("57");
		//$entityp->setPhoneTypeCode($tel_type);
		$entityp->setPhoneTypeCode("CEL"); //Celular
  		$entityp->setPhoneNumber($telefono);
  		$entity->setEntityPhone($entityp);
  		$arraytercero->setEntity($entity);
		
		
  		$supplier=new SupplierInfo();
  		$supplier->setGlOffsetCode("COGT");
  		$supplier->setPaymentInstrmentCode("C");
  		$supplier->setPaymentTermsCode("030");
				
  		$arraytercero->setSupplierInfo($supplier);
  		$custom=new CustomerInfo();
  		$custom->setCompany("00002");
  		$custom->setPaymentTermsCode("030");
  		$custom->setCollectionManagerCode("GC02");
  		$custom->setPolicyNumber("7");
  		$custom->setPolicyCompany("00000");
  		$custom->setGlOffset("CLIE");
  		$custom->setTaxRateAreaCode("VENTAS");
  		$custom->setTaxExplanationCode("V");
  		$arraytercero->setCustomerInfo($custom);
  		$audit=new AuditInfo();
  		$audit->setUserID("SIG-".$user);
  		$audit->setProgramID("SIGAS");
  		$audit->setWorkStationID("SIGASWEB");
  		$arraytercero->setAuditInfo($audit);
                
  		$tercero=new Terceros($arraytercero,$this->debug);
		$response=$tercero->procesar();
		$response->payload=json_encode(
								array(
									'Entity'=>$entity->to_json(),
									'Supplier'=>$supplier->to_json(),
									'Customer'=>$custom->to_json(),
									'Audit'=>$audit->to_json()
									)
							);
		if($this->debug)
			debug($response);
		return $response;

  	}

  function ActualizacionTercero($user,$nit,$razon_social,$direccion,$estado,$ciudad,$telefono,$email,$tipo_documento){

		log_info('Actualizando tercero con: ', array($user,$nit,$razon_social,$direccion,$estado,$ciudad,$telefono,$email,$tipo_documento));
		$arraytercero=new TerceroProcesar();
  	    $arraytercero->setActionType("A");
  	    $entity=new Entity();
  	 	$entity->setEntityTypeCode("GF");
        $nit=$this->add_space_to_nit($nit);
		$entity->setEntityTaxID($nit);
		
		$razon_social=$this->clean_name($razon_social);
		$razon_social=$this->separar($razon_social);
		$entity->setEntityName($razon_social[0]);//Solo Acepta 40 Caracteres
		
  		$entity->setIndustryClassificationCode(" ");
  		$entity->setCreditMessageCode(" ");
		
  		$letter=$this->get_corporation_code_homologacion($tipo_documento);
		$entity->setPersonCorporationCode($letter);
			
  		$entity->setEntityParentID(" ");
  		$entity->setEntityParentTaxID(" ");
  		$entity->setEntityBeneficiaryID(" ");
  		$entity->setEntityBeneficiaryTaxID(" ");
  		$entity->setCategoryCode003("06");
  		$entity->setCategoryCode004("000");
  		$entity->setCategoryCode008("020");
  		$entity->setCategoryCode009("NA");
  		$entity->setCategoryCode010("NA");
  		$entity->setCategoryCode011("NAL");
  		$entity->setCategoryCode012(" ");
  		$entity->setCategoryCode013(" ");
  		$entity->setCategoryCode016("EMP");
  		$entity->setCategoryCode021("NA");
  		$direccion=$this->separar($direccion);
		$entity->setEntityAddressLine1($direccion[0]);//>40 usar address2
		if(isset($direccion[1]))
			$entity->setEntityAddressLine2($direccion[1]);
		else
			$entity->setEntityAddressLine2(" ");
		if(isset($direccion[2]))
			$entity->setEntityAddressLine3($direccion[2]);
		else
			$entity->setEntityAddressLine3(" ");
		if(isset($direccion[3]))
			$entity->setEntityAddressLine4($direccion[3]);
		else
			$entity->setEntityAddressLine4(" ");
  		$entity->setEntityCity($ciudad);
  		$entity->setEntityStateCode($estado);
  		$entity->setEntityEmail($email);
  		$entity->setTaxPayerTypeColombia("A");
		
		$today=date("Ymd");
  		$entity->setDateEffective($today);
  		$entity->setDateExpir("20501231");

        //Por Verificat
  		$entity->setCiiuCodeColombia("0010");

		$entityp=new EntityPhone();
  		$entityp->setAreaCode(" ");
		//$entityp->setPhoneTypeCode($tel_type);
		$entityp->setPhoneTypeCode("");
  		$entityp->setPhoneNumber($telefono);
  		//$entity->setEntityPhone($entityp);//Adding phone to main entity
		
  		$arraytercero->setEntity($entity);
		
		$supplier=new SupplierInfo();
  		$supplier->setGlOffsetCode("COGT");
  		$supplier->setPaymentInstrmentCode("C");
  		$supplier->setPaymentTermsCode("030");
		
		$custom=new CustomerInfo();
  		$custom->setCompany("00002");
  		$custom->setPaymentTermsCode("030");
  		$custom->setCollectionManagerCode("GC02");
  		$custom->setPolicyNumber("7");
  		$custom->setPolicyCompany("00000");
  		$custom->setGlOffset("CLIE");
  		$custom->setTaxRateAreaCode("VENTAS");
  		$custom->setTaxExplanationCode("V");
  		$arraytercero->setCustomerInfo($custom);
		
  		$audit=new AuditInfo();
  		$audit->setUserID($user);
  		$audit->setProgramID("SIGAS");
  		$audit->setWorkStationID("SIGASWEB");
  		$arraytercero->setAuditInfo($audit);
		
		$tercero=new Terceros($arraytercero);
		$response=$tercero->procesar();
		$response->payload=json_encode(
								array(
									'Entity'=>$entity->to_json(),
									'Supplier'=>$supplier->to_json(),
									'Customer'=>$custom->to_json(),
									'Audit'=>$audit->to_json()
									)
							);
	    return $response;
	}
	
	function ObtenerCiudad($departamento,$ciudad)
	{
		$db=$this->connectar();
		$sql="select distinct municipio as ciudad from aportes089 where codmunicipio='%s'";
		$sql=sprintf($sql,$departamento.$ciudad);
		$query=$db->querySimple($sql);
		$result=$query->fetchAll();
		if(count($result)>0)
		{
			return $result[0]['ciudad'];
		}
		return " ";
	}

	function ObtenerRazonSocial($identificacion,$idtipodocumento)
	{
		if($this->debug){
			debug("recibi");
			debug(array($identificacion,$idtipodocumento));
		}
		if($idtipodocumento=="1"||$idtipodocumento=="4")
		{
			$razon=$this->razon_social_cedula($identificacion,$idtipodocumento);
			if($razon=="") 
				$razon=$this->razon_social_nit($identificacion);
		}
		else
		{
			$razon=$this->razon_social_nit($identificacion);
		}
		return $razon;
	}
	
	function razon_social_cedula($identificacion,$idtipodocumento)
	{
		$sql="select pnombre,snombre,papellido,sapellido from aportes015 where identificacion = '%s' and idtipodocumento=%s;";
		$sql=sprintf($sql,$identificacion,$idtipodocumento);
		$db=$this->connectar();
		$query=$db->querySimple($sql);
		$result=$query->fetchAll();
		if($this->debug)
			debug($result);
		$razon="";
		if(count($result)>0)
		{
			$string="";
			if(isset($result[0]["pnombre"]))
			{
				$string=$this->remove_white_space($result[0]["pnombre"]);
			}
			if(isset($result[0]["snombre"]))
			{
				$string=$string." ".$this->remove_white_space($result[0]["snombre"]);
			}
			if(isset($result[0]["papellido"]))
			{
				$string=$string." ".$this->remove_white_space($result[0]["papellido"]);
			}
			if(isset($result[0]["sapellido"]))
			{
				$string=$string." ".$this->remove_white_space($result[0]["sapellido"]);
			}
			$razon=$this->cleanString($string);
		}
		return $razon;	
	}
	
	function razon_social_nit($identificacion)
	{
		$sql="select razonsocial from aportes048 where nit = '%s';";
		$sql=sprintf($sql,$identificacion);
		$db=$this->connectar();
		$query=$db->querySimple($sql);
		$result=$query->fetchAll();
		if($this->debug)
			debug($result);
		$razon="";
		if(count($result)>0)
		{
			$razon=$result[0]["razonsocial"];
			$razon=$this->cleanString($razon);
		}
		return $razon;
	}
	
	function get_corporation_code_homologacion($aux)
	{
		$sql="SELECT D.CodigoTabla2_detalles as Codigo FROM Homologacion as H "
				."Join Detalles as D on D.ID_detalles=H.Dato_Homologacion "
				."where H.Dato_Sigas=%s and H.Estado='A' and D.Grupos_ID=1";
		$sql=sprintf($sql,$aux);
		$db=$this->connectar();
		$query=$db->querySimple($sql);
		$result=$query->fetchAll();
		return $result[0]['Codigo'];
	}
        function obtenerValorHomologacionJDE($codigoJDE){
            $sql="
                    SELECT 
                        CodigoTabla2_detalles AS Codigo 
                    FROM 
                        Detalles 
                    WHERE 
                        Descripcion2_detalles LIKE '%".$codigoJDE."%'
                ";
            $db=$this->connectar();
            $query=$db->querySimple($sql);
            $result=$query->fetchAll();
            return $result[0]['Codigo'];
        }

	function add_space_to_nit($nit)
	{
		if($this->debug)
			debug("agregando espacio");
		$aux=trim($nit);
		$count_nit=strlen($aux);
		if($this->debug)
		{
			debug("antes de espacio");
			debug(strlen($aux));
		}
		if($count_nit<16){
		  $aux=str_pad($aux,16," ",STR_PAD_LEFT);
		}
		if($this->debug)
		{
			debug("despues de espacio");
			debug(strlen($aux));
		}
		return $aux;
	}

	function clean_name($name)
	{
		if($this->debug)
		{
			debug("limpiando nombre");
			debug($name);
		}
		if(strpos($name,"&"))
		{
			$name=str_replace("&"," ",$name);
			if($this->debug)
			{
				debug("nuevo nombre");
				debug($name);
			}
		}
		return $name;
	}
	
	function remove_white_space($string)
	{
		$string = str_replace(' ', '', $string);
		$string = preg_replace('/\s+/', '', $string);
		return $string;
	}
	
	function cleanString($text) {
		$utf8 = array(
			'/[áàâãªä]/u'   =>   'a',
			'/[ÁÀÂÃÄ]/u'    =>   'A',
			'/[ÍÌÎÏ]/u'     =>   'I',
			'/[íìîï]/u'     =>   'i',
			'/[éèêë]/u'     =>   'e',
			'/[ÉÈÊË]/u'     =>   'E',
			'/[óòôõºö]/u'   =>   'o',
			'/[ÓÒÔÕÖ]/u'    =>   'O',
			'/[úùûü]/u'     =>   'u',
			'/[ÚÙÛÜ]/u'     =>   'U',
			'/ç/'           =>   'c',
			'/Ç/'           =>   'C',
			'/ñ/'           =>   'n',
			'/Ñ/'           =>   'N',
			'/–/'           =>   '-', // UTF-8 hyphen to "normal" hyphen
			'/[’‘‹›‚]/u'    =>   ' ', // Literally a single quote
			'/[“”«»„]/u'    =>   ' ', // Double quote
			'/ /'           =>   ' ', // nonbreaking space (equiv. to 0x160)
		);
		$string=preg_replace(array_keys($utf8), array_values($utf8), $text);
		$string = preg_replace('/[^A-Za-z\s]/', '', $string);
		$string= strtoupper($string);
		return $string;
	}
	
	function connectar()
	{
		$db = IFXDbManejador::conectarDB();
		if($db->conexionID==null){
			$cadena = $db->error;
			echo msg_error($cadena);
			exit();
		}
		return $db;
	}
	
	function separar($address)
	{
		return str_split($address,40);
		
	}
        
        function obtenerInformacionEmpresa($nit){
            $retorno = array();
            $sql="
                    SELECT 
                        idtipodocumento
                        ,digito
                    FROM 
                        aportes048
                    WHERE 
                        nit = '$nit'
                ";
            $db = $this->connectar();
            $query = $db->querySimple($sql);
            $result = $query->fetchAll();
            $retorno["idTipoDocumento"] = $result[0]['idtipodocumento'];
            $retorno["digitoVerificacion"] = $result[0]['digito'];
            return $retorno;
        }

}
?>
