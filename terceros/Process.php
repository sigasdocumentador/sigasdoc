<?php

ini_set('display_errors','1');
error_reporting(E_ALL);
include 'BaseTerceros.php';
   
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'utilitario.class.php';

$usuario=$_SESSION['USUARIO'];
$action=$_REQUEST['action'];
$type=$_REQUEST['type'];

if($type=="Terceros")
{
	$terceros=new BaseTerceros();
	$result=array("error"=>false,"msg"=>"","AN8Tercero"=>"");
        
	/*Get Variables for Terceros */
	$campo1=$_REQUEST['campo1'];//tipo documento
	$campo2=$_REQUEST['campo2'];//nit
	$campo3=$_REQUEST['campo3'];//nit digit
	$campo7=$_REQUEST['campo7'];//raz�n social
	$campo9=$_REQUEST['campo9'];//Direccion
	$campo10=$_REQUEST['campo10'];//Departamento
        $campo11=$_REQUEST['campo11'];//Ciudad (Nombre)
	$campo12=$_REQUEST['campo12'];//ID Ciudad
	$campo13=$_REQUEST['campo13'];//Tel
	$campo16=$_REQUEST['campo16'];//Email
	$nit=Utilitario::format_nit($campo2);
	
	log_info('Process: ', array($usuario,$action,$type,$campo1,$campo2,$campo3,$campo7,$campo9,$campo10,$campo11,$campo13,$campo16));
		
        if($campo1 == 5){ // NIT
            if(isset($campo3)){
		$nit=$nit."-".$campo3;
            }
        }
	if($action=="New")
	{
		$respuesta=$terceros->CreacionTercero($usuario,$nit,$campo7,$campo9,$campo10,$campo11,$campo12,$campo13,$campo16,$campo1);
		$result["payload"]=$respuesta->payload;
		if($respuesta->getError()!=NULL&&$respuesta->getError()->getErrorCode()!="DB-ERR-0")
		{
			
			$result["error"]=true;
			$result["msg"]=$respuesta->getError()->getErrorMessage();
		}
		else
		{
			$result["msg"]="Creacion Exitosa";
                        $result["AN8Tercero"]=$respuesta->getEntityAN8();
		}
		
	}
        /*
	if($action=="Update")
	{
		$respuesta=$terceros->ActualizacionTercero($usuario,$nit,$campo7,$campo9,$campo10,$campo11,$campo13,$campo16,$campo1);
		$result["payload"]=$respuesta->payload;
		if($respuesta->getError()!=NULL&&$respuesta->getError()->getErrorCode()!="DB-ERR-0")
		{
			if($respuesta->getError()->getErrorCode()=="WS-ERR-0030")
			{
				$respuesta=$terceros->CreacionTercero($usuario,$nit,$campo7,$campo9,$campo10,$campo11,$campo13,$campo16,$campo1);
				if($respuesta->getError()!=NULL&&$respuesta->getError()->getErrorCode()!="DB-ERR-0")
				{
					echo "Error";
					$result["error"]=true;
					$result["msg"]=$respuesta->getError()->getErrorMessage();
				}else{
					echo "Create";
					$result["msg"]="Creacion Exitosa";
				}
				
			}
		}
		else{
			echo "Update";
			$result["msg"]="Actualizacion Exitosa";
		}
	}	
         * * 
         */
	if($action=="Show")
	{
		$respuesta=$terceros->ConsultaTercero($nit);
		if($respuesta->getError()!=NULL)
		{
			$result["error"]=false;
		}
		else
		{
			$result["msg"]=$respuesta;
		}
	}
         
         
}

echo json_encode($result);