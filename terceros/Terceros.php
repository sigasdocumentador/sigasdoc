<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include("class/TerceroProcesar.php");
include ("class/Response.php");
include ("class/XMLSerializer.php");

/**
 * Description of Terceros
 *
 * @author nuevastic
 */
class Terceros {
     private  $wsdl="http://10.10.1.130:8011/ITC_OSB/proxy/GestionTerceros?WSDL";
     private $soap_serv;
     private $header;
     private $response;
     private $debug;
	 
	 /*****Constructor********/
	 
	 function __construct($tercero=array(),$debug=false){
        $this->soap_serv=new SoapClient($this->wsdl,array('trace' => 1));        
		$response=new Response(); 
		$this->header=$tercero;
		$this->debug=$debug;
	 }
	 
	 public function setTercero($tercero){
	 	$this->header=$tercero;
	 }
	

     public function procesar(){
		 $tercerosRequest=new stdClass();
		 $tercerosRequest->terceroProcesar=$this->header;
                 //echo htmlentities(XMLSerializer::generateValidXmlFromObj($tercerosRequest));die();
		 try{    
			if($this->debug)
			{
				debug("Paylod a Tercero");
				debug($tercerosRequest);
			}
                        
		 	$res=$this->soap_serv->procesar($tercerosRequest); 
                        
                        
                        $file = fopen("pruebas Envio WSDL.txt", "a+");
                        fwrite($file, "REQUEST:\n" . $this->soap_serv->__getLastRequest() . "\n" . PHP_EOL);
                        fwrite($file, "-----------------------------------------------------------------------------------" . PHP_EOL);
                        fclose($file);
                        
                        //echo "REQUEST:\n" . $this->soap_serv->__getLastRequest() . "\n" . PHP_EOL;
                          
                         
                        //echo "REQUEST:\n" . $this->soap_serv->__getLastRequest() . "\n" . PHP_EOL;
                        
                        //echo "REQUEST:\n" . $this->soap_serv->__getLastRequest() . "\n";
                       
                        
			
                        if($this->debug)
			{
				debug("Respuesta de Tercero");
				var_dump($res);
				debug($res);
				debug($res->terceroProcesado);
				debug($res->terceroProcesado->entityTaxID);
			}
                        
			$this->procesarResponse($res);	
		 }
		 catch(Exception $e){
		 	$this->response=new Response();
			$error=new Error();
		 	$error->setErrorCode("Service Error");
		 	$error->setErrorMessage($e->getMessage());
			$this->response->setError($error);
		 }  
		 return $this->response;
    }
	
	
	
	private function procesarResponse($res){
		$this->response=new Response();
		if(isset($res->terceroProcesado->error)){
			$error=new Error();
			$error->setErrorCode($res->terceroProcesado->error->codigo);
			$error->setErrorMessage($res->terceroProcesado->error->mensaje);
			$this->response->setError($error);
			}
		else{
			
			$this->response->setEntityTaxID($res->terceroProcesado->entityTaxID);
			$this->response->setEntityAN8($res->terceroProcesado->entityAN8);
			$this->response->setEntityName($res->terceroProcesado->entityName);
			$this->response->setEntityAddressLine1($res->terceroProcesado->entityAddressLine1);
			$this->response->setEntityAddressLine2($res->terceroProcesado->entityAddressLine2);
			$this->response->setEntityAddressLine3($res->terceroProcesado->entityAddressLine3);
			$this->response->setEntityAddressLine4($res->terceroProcesado->entityAddressLine4);
			$this->response->setEntityCity($res->terceroProcesado->entityCity);
			$phone=array();
			$phone=(isset($res->terceroProcesado->entityPhone))?$res->terceroProcesado->entityPhone:array();
			$this->response->setEntityPhone($phone);
			
		}
			
	}
	
	
	public function getResponse()
	{
		return $this->response;
	}
	
}
