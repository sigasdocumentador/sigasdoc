<?php
ini_set("display_errors",'1');
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$idp=$_REQUEST['v0'];
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
include_once $raiz.DIRECTORY_SEPARATOR.'seguridad'.DIRECTORY_SEPARATOR.'phpComunes'.DIRECTORY_SEPARATOR.'funciones.php';
include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'ciudades.class.php';
$objCiudad=new Ciudades();

$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Consulta Persona</title>
<script type="text/javascript" src="../js/llenarCampos.js"></script>
<script type="text/javascript" >
	$(document).ready(function(){		
		$("#cboCiudad").jCombo("2", { 
			parent: "#cboDepto",
			selected_value: '41001'
		});
		$("#cboZona").jCombo("3", {
			parent: "#cboCiudad"
		});	
		$("#cboBarrio").jCombo("4", {
			parent: "#cboZona"
		});	
		
		$("#cboDepto2").jCombo("1", {
			parent: "#cboPais2",
			selected_value: '41' 
		});	
		$("#cboCiudad2").jCombo("2", { 
			parent: "#cboDepto2",
			selected_value: '41001'
		});	
		
		nombreCorto();
		llenarCampos(<?php echo $idp; ?>);

		$(function(){		
			setTimeout((function(){
				$("#tablaPersonaTab input,#tablaPersonaTab select").attr("disabled","disabled");				
			}),200);		
		});
	});
</script>
</head>
<body>
<center>
<table width="80%" border="0" cellspacing="0" cellpadding="0"  id="tablaPersonaTab">
	<tr>
		<td class="arriba_iz" >&nbsp;</td>
		<td class="arriba_ce" ><span class="letrablanca">::&nbsp;Datos Persona&nbsp;::</span></td>
		<td class="arriba_de" >&nbsp;</td>
	</tr>
	<tr>
		<td class="cuerpo_iz">&nbsp;</td>
		<td class="cuerpo_ce">&nbsp;</td>
		<td class="cuerpo_de">&nbsp;</td>
	<tr>
	<tr>
		<td class="cuerpo_iz">&nbsp;</td>
		<td class="cuerpo_ce">&nbsp;
			<table id="persona" class="tablero" width="100%" >
				<tr>
					<td width="25%">Tipo Documento</td>
					<td>
						<select name="txtTipoD" id="txtTipoD" class="box1">
							<option value="0" selected="selected">Seleccione...</option>
						<?php
							$rs=$db->Definiciones(1,1);
							while($result=$rs->fetch()){
								echo "<option value=".$result['iddetalledef'].">".$result['detalledefinicion']."</option>";
							}
						?>
						</select> <img src="../imagenes/menu/obligado.png" width="12" height="12" />
					</td>
					<td>N&uacute;mero</td>
					<td>
						<input name="txtNumero" type="text" class="box1" id="txtNumero" />
						 <img src="../imagenes/menu/obligado.png" width="12" height="12"  />
						 - <label id="lblIdPersona" name="lblIdPersona">&nbsp;</label>
					</td>
				</tr>
				<tr>
					<td>Primer Nombre</td>
					<td><input name="txtPNombre" type="text" class="box1" id="txtPNombre" /> <img src="../imagenes/menu/obligado.png" width="12" height="12" /></td>
					<td>Segundo Nombre</td>
					<td><input name="txtSNombre" type="text" class="box1" id="txtSNombre" /></td>
				</tr>
				<tr>
					<td>Primer Apellido</td>
					<td><input name="txtPApellido" type="text" class="box1" id="txtPApellido" /> <img src="../imagenes/menu/obligado.png" width="12" height="12" /></td>
					<td>Segundo Apellido</td>
					<td><input name="txtSApellido" type="text" class="box1" id="txtSApellido" /></td>
				</tr>
				<tr>
					<td>Sexo</td>
					<td>
						<select name="txtSexo" class="box1" id="txtSexo">
							<option value="0" selected="selected">Seleccione...</option>
							<option value="M">Masculino</option>
							<option value="F">Femenino</option>
						</select> <img src="../imagenes/menu/obligado.png" width="12" height="12" />
					</td>
					<td>Estado Civil</td>
					<td>
						<select name="txtEstado2" id="txtEstado2" class="box1">
							<option value="0" selected="selected">Seleccione...</option>
						<?php
							$estado = $db->Definiciones(10,1);
							while($r_defi=$estado->fetch()){
								if($r_defi['iddetalledef']!=54){
									echo "<option value=".$r_defi['iddetalledef'].">".$r_defi['detalledefinicion']."</option>";
								}
							}
						?>
						</select> <img src="../imagenes/menu/obligado.png" width="12" height="12" />
					</td>
				</tr>
				<tr>
					<td>Tel&eacute;fono</td>
					<td><input name="txtTelefono" type="text" class="box1" id="txtTelefono" /></td>
					<td>Celular</td>
					<td><input name="txtCelular" type="text" class="box1" id="txtCelular" /></td>
				</tr>
				<tr>
					<td>E-mail</td>
					<td><input name="txtEmail" type="text" class="box1" id="txtEmail" /></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td >Depto Reside</td>
					<td >
						<select name="cboDepto" id="cboDepto" class="box1">
							<option value="0" selected="selected">Seleccione...</option>
						<?php
							$dpto = $db->Departamento();
							while($r_defi=$dpto->fetch()){
								echo "<option value=".$r_defi['coddepartamento'].">".utf8_encode($r_defi['departmento'])."</option>";
							}
						?>
						</select> <img src="../imagenes/menu/obligado.png" width="12" height="12" />
					</td>
					<td >Ciudad Reside</td>
					<td >
						<select name="cboCiudad" id="cboCiudad" class="box1">
							<option value="0" selected="selected">Seleccione...</option>
						</select> <img src="../imagenes/menu/obligado.png" width="12" height="12" />
					</td>
				</tr>
				<tr>
					<td >Zona Reside</td>
					<td >
						<select name="cboZona" id="cboZona" class="box1">
							<option value="0" selected="selected">Seleccione...</option>
						</select> <img src="../imagenes/menu/obligado.png" width="12" height="12" />
					</td>
					<td >Barrio</td>
					<td >
						<select name="cboBarrio" id="cboBarrio" class="box1">
							<option value="0" selected="selected">Seleccione...</option>
						</select> <img src="../imagenes/menu/obligado.png" alt="" width="12" height="12" />
					</td>
				</tr>
				<tr>
					<td >Direcci&oacute;n</td>
					<td ><input name="txtDireccion" type="text" class="box1" id="txtDireccion" onfocus="direccion(this,document.getElementById('txtTipoV'));" /> <img src="../imagenes/menu/obligado.png" width="12" height="12" /></td>
					<td>Ubicaci&oacute;n Vivienda</td>
					<td>
						<select name="txtTipoV" id="txtTipoV" class="box1">
							<option value="0" selected="selected">Seleccione...</option>
							<option value="U">URBANA</option>
							<option value="R">RURAL</option>
						</select> <img src="../imagenes/menu/obligado.png" alt="" width="12" height="12" />
					</td>
				</tr>
				<tr>
					<td >Tipo Propiedad</td>
					<td>
						<select name="txtCasa" id="txtCasa" class="box1">
							<option value="0" selected="selected">Seleccione...</option>
						<?php
							$defin = $db->Definiciones(42,1);
							while($r_defi=$defin->fetch()){
								echo "<option value=".$r_defi['iddetalledef'].">".$r_defi['detalledefinicion']."</option>";
							}
						?>
						</select> <img src="../imagenes/menu/obligado.png" width="12" height="12" />
					</td>
					<td >&nbsp;</td>
					<td >&nbsp;</td>
				</tr>
				   <tr>
				    	<td >Fecha Nace</td>
				    	<td ><input name="textfield6" type="text" class="box1" id="txtFechaN" readonly="readonly"/> <img src="../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
				    	<td >Pais Nace</td>
				    	<td >
				    		<select name="cboPais2" id="cboPais2" class="box1">
				      			<option value="0" selected="selected">Seleccione</option>
							      <?php
									$consulta = $objCiudad->paises();
									while($r_defi=mssql_fetch_array($consulta)){
										echo "<option value=".$r_defi['idpais'].">".utf8_encode($r_defi['pais'])."</option>";
									}
						        ?>
				      		</select>
				      		<img src="../imagenes/menu/obligado.png" width="12" height="12" />
				    	</td>
				    </tr>				
					<tr>
						<td >Depto Nace</td>
						<td >
							<select name="cboDepto2" id="cboDepto2" class="box1">
								<option value="0" selected="selected">Seleccione...</option>							
							</select> <img src="../imagenes/menu/obligado.png" width="12" height="12" />
						</td>
						<td >Ciudad Nace</td>
						<td >
							<select name="cboCiudad2" id="cboCiudad2" class="box1">
								<option value="0" selected="selected">Seleccione...</option>
							</select> <img src="../imagenes/menu/obligado.png" alt="" width="12" height="12" />
						</td>
					</tr>
					<tr>
						<td >Capacidad Trabajo</td>
						<td >
							<select name="txtCapacidad" id="txtCapacidad" class="box1">
								<option value="0" selected="selected">Seleccione...</option>
								<option value="N">Normal</option>
								<option value="I">Discapacitado</option>
							</select> 
						</td>
						<td >&nbsp;</td>
						<td >&nbsp;</td>
					</tr>
				<tr>
					<td >Nombre Corto</td>
					<td colspan="3" ><input name="txtNombreC" type="text" class="boxlargo" id="txtNombreC" readonly="readonly" /></td>
				</tr>
			</table>
		</td>
		<td class="cuerpo_de">&nbsp;</td>
	<tr>
	<tr>
		<td class="cuerpo_iz">&nbsp;</td>
		<td class="cuerpo_ce" >&nbsp;</td>
		<td class="cuerpo_de" >&nbsp;</td>
	<tr>
		<td class="abajo_iz">&nbsp;</td>
		<td class="abajo_ce"></td>
		<td class="abajo_de">&nbsp;</td>
	</tr>
</table>

</center>
</body>
</html>