<?php
ini_set("display_errors",'1');
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
include_once $raiz.DIRECTORY_SEPARATOR.'seguridad'.DIRECTORY_SEPARATOR.'phpComunes'.DIRECTORY_SEPARATOR.'funciones.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
$idPersona = $_REQUEST["v0"]; 
$sql="SELECT detalledefinicion, razonsocial, fechaingreso, fecharetiro, salario, primaria, aportes016.estado, semanas, categoria FROM aportes016 
INNER JOIN aportes048 ON aportes016.idempresa=aportes048.idempresa AND aportes048.principal='S' INNER JOIN aportes091 ON aportes016.tipoformulario = aportes091.iddetalledef WHERE idpersona=$idPersona UNION SELECT detalledefinicion, razonsocial, fechaingreso, fecharetiro, salario, primaria, aportes017.estado, semanas, categoria FROM aportes017 INNER JOIN aportes048 ON aportes017.idempresa=aportes048.idempresa AND aportes048.principal='S' INNER JOIN aportes091 ON aportes017.tipoformulario=aportes091.iddetalledef WHERE idpersona=$idPersona";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Documento sin t�tulo</title>
<link type="text/css" href="../css/Estilos.css" rel="stylesheet"/>
<link type="text/css" href="../css/marco.css" rel="stylesheet"/>
</head>

<body>
<center>
<table width="95%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td class="arriba_iz" >&nbsp;</td>
<td class="arriba_ce" ><span class="letrablanca">::&nbsp;Afiliaciones&nbsp;::</span></td>
<td class="arriba_de" >&nbsp;</td>
</tr>
<tr>
<td class="cuerpo_iz">&nbsp;</td>
<td class="cuerpo_ce"><label style="font-size:14px;font-weight:bold;text-align:left">Afiliaciones como Trabajador</label>&nbsp;</td>
<td class="cuerpo_de">&nbsp;</td>
</tr>
<tr>
<td class="cuerpo_iz" >&nbsp;</td>
<td class="cuerpo_ce" >
<table width="100%" border="0" cellspacing="0" class="tablero">
       <tr>
          <th width="12%" >Tipo Afilia</th>
          <th width="40%" >Empresa</th>
          <th width="8%" >F Ingreso</th>
          <th width="11%" >F Retiro</th>
          <th width="10%" >Salario</th>
          <th width="4%" >P</th>
          <th width="5%" >Est</th>
          <th width="4%" >Sem</th>
          <th width="6%" >Cat</th>
       </tr>
<?php
$rs=$db->querySimple($sql);
while($row=$rs->fetch()){
?>
	 <tr>
          <td ><?php echo $row['detalledefinicion']; ?></td>
          <td ><?php echo $row['razonsocial']; ?></td>
          <td ><?php echo $row['fechaingreso']; ?></td>
          <td ><?php echo $row['fecharetiro']; ?></td>
          <td ><?php echo $row['salario']; ?></td>
          <td style="text-align:center"><?php echo $row['primaria']; ?></td>
          <td style="text-align:center"><?php echo $row['estado']; ?></td>
          <td style="text-align:center"><?php echo $row['semanas']; ?></td>
          <td style="text-align:center"><?php echo $row['categoria']; ?></td>
       </tr>
<?php
}
?>
</table>       
</td>
<td class="cuerpo_de" >&nbsp;</td>
</tr>
<tr>
<td class="cuerpo_iz" >&nbsp;</td>
<td class="cuerpo_ce" ></td>
<td class="cuerpo_de" >&nbsp;</td>
</tr>
<tr>
  <td class="cuerpo_iz" >&nbsp;</td>
  <td class="cuerpo_ce" ><label style="font-size:14px;font-weight:bold;text-align:left">Afiliaciones como Conyuge con Relaci&oacute;n de Convivencia</label>&nbsp;</td>
  <td class="cuerpo_de" >&nbsp;</td>
 </tr> 
<tr>
  <td class="cuerpo_iz" >&nbsp;</td>
  <td class="cuerpo_ce" >
<table width="100%" border="0" cellspacing="0" class="tablero">
       <tr>
          <th width="7%" >Id</th>
          <th width="11%" >N&uacute;mero</th>
          <th width="52%" >Conyugue</th>
          <th width="8%" >Labora</th>
          <th width="9%" >Salario</th>
          <th width="13%" >Fe. Ingreso</th>
       </tr>
<?php
unset($row);
$rs->closeCursor();
$sql="SELECT idbeneficiario,idconyuge, aportes015.identificacion,aportes015.pnombre, aportes015.snombre, 
aportes015.papellido, aportes015.sapellido, aportes015.fechanacimiento, aportes015.sexo, aportes021.conviven, 
aportes091.detalledefinicion 
FROM aportes021 INNER JOIN aportes015 ON aportes021.idtrabajador = aportes015.idpersona  
INNER JOIN aportes091 ON aportes021.idparentesco=aportes091.iddetalledef 
WHERE idparentesco=34 AND idbeneficiario=$idPersona  and conviven='S'
UNION
SELECT idbeneficiario,idconyuge, aportes015.identificacion,aportes015.pnombre, aportes015.snombre, 
aportes015.papellido, aportes015.sapellido, aportes015.fechanacimiento, aportes015.sexo, aportes021.conviven, 
aportes091.detalledefinicion 
FROM aportes021 INNER JOIN aportes015 ON aportes021.idbeneficiario = aportes015.idpersona  
INNER JOIN aportes091 ON aportes021.idparentesco=aportes091.iddetalledef 
WHERE idparentesco=34 AND idtrabajador=$idPersona  and conviven='S'";
$rs=$db->querySimple($sql);
while($row=$rs->fetch()){
$NOMBRE=$row['pnombre']." ".$row['snombre']." ".$row['papellido']." ".$row['sapellido'];
?>
	 <tr>
          <td ><?php echo $row['idconyuge']; ?></td>
          <td ><?php echo $row['identificacion']; ?></td>
          <td ><?php echo $NOMBRE ?></td>
          <td ><?php echo ""; ?></td>
          <td ><?php echo $row['sexo']; ?></td>
          <td ><?php echo $row['fechaingreso']; ?></td>
       </tr>
<?php
}
unset($row);
$rs->closeCursor();
?>
</table>  
  &nbsp;</td>
  <td class="cuerpo_de" >&nbsp;</td>
<tr>
</tr>
<td class="cuerpo_iz" >&nbsp;</td>
<td class="cuerpo_ce" ><label style="font-size:14px;font-weight:bold;text-align:left">Afiliaciones como Beneficiario</label></td>
<td class="cuerpo_de" >&nbsp;</td>
</tr>
<tr>
<td class="cuerpo_iz" >&nbsp;</td>
<td class="cuerpo_ce" >
<table width="100%" border="0" cellspacing="0" class="tablero">
       <tr>
          <th width="7%" >Id</th>
          <th width="11%" >N&uacute;mero</th>
          <th width="14%" >Relaci&oacute;n</th>
          <th width="46%" >Relaci&oacute;n con</th>
          <th width="9%" >F. afilia</th>
          <th width="13%" >Estado</th>
       </tr>
<?php
unset($row);
$rs->closeCursor();
$sql="SELECT aportes015.idpersona, aportes015.identificacion,aportes015.pnombre, aportes015.snombre, aportes015.papellido, aportes015.sapellido, aportes015.fechanacimiento, aportes015.sexo, aportes021.estado, aportes091.detalledefinicion, aportes021.fechaafiliacion FROM aportes021 
INNER JOIN aportes015 ON aportes021.idtrabajador = aportes015.idpersona INNER JOIN aportes091 ON aportes021.idparentesco=aportes091.iddetalledef 
WHERE idparentesco <>34 AND idbeneficiario=$idPersona";
$rs=$db->querySimple($sql);
while($row=$rs->fetch()){
$NOMBRE=$row['pnombre']." ".$row['snombre']." ".$row['papellido']." ".$row['sapellido'];
?>
	 <tr>
          <td ><?php echo $row['idpersona']; ?></td>
          <td ><?php echo $row['identificacion']; ?></td>
          <td ><?php echo $row["detalledefinicion"]; ?></td>
          <td ><?php echo $NOMBRE ?></td>
          <td align="center" ><?php echo $row['fechaafiliacion']; ?></td>
          <td style="text-align:center" ><?php echo $row['estado']; ?></td>
       </tr>
<?php
}
unset($row);
$rs->closeCursor();
?>
</table>  
</td>
<td class="cuerpo_de" >&nbsp;</td>
</tr>
<tr>
<td class="abajo_iz" >&nbsp;</td>
<td class="abajo_ce" ></td>
<td class="abajo_de" >&nbsp;</td>
</tr>
</table>
</center>
</body>
</html>
