<?php
/* autor:       Orlando Puentes
 * fecha:       Julio 23 de 2010
 * objetivo:    Consultar en la base de datos de Aportes y Subsidio el trabajador afiliado a Comfamiliar Huila. 
 */
 
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$url=$_SERVER['PHP_SELF'];
include_once $raiz.DIRECTORY_SEPARATOR.'phpComunes'.DIRECTORY_SEPARATOR.'auditoria.php';
auditar($url);

include_once $raiz.DIRECTORY_SEPARATOR.'config.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'p.definiciones.class.php';
$objDefiniciones = new Definiciones;

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>::Consulta de Personas::</title>

<link type="text/css" href="<?php echo URL_PORTAL; ?>newcss/css.1.8/jquery-ui-1.8.17.custom.css" rel="stylesheet">
<link type="text/css" href="<?php echo URL_PORTAL; ?>css/marco.css" rel="stylesheet">
<link type="text/css" href="<?php echo URL_PORTAL; ?>css/Estilos.css" rel="stylesheet">

<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/1.6/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/1.6/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/comunes.js"></script>
<script type="text/javascript" src="<?php echo $url; ?>newjs/jquery.combos2.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/llenarCampos.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/direccion.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>/consultas/js/consultaPersonas.js"></script>

<script type="text/javascript">
shortcut.add("Shift+F",function() {
		var URL=src();
		var url=URL+"aportes/trabajadores/consultaTrabajador.php";
    	window.open(url,"_blank");
    },{
	'propagate' : true,
	'target' : document 
	});        
</script>


<script language="javascript">
function mostrarAyuda(){
	$("#ayuda").dialog('open' );
	}
function notas(){
	$("#dialog-form2").dialog('open');
	}	
</script>
<style type="text/css">
#trabajadores p{ cursor:pointer; color:#333; width:600px;}
#trabajadores p:hover{color:#000}
#accordion h3,div{ padding:6px;}
#accordion span.plus{ background:url("<?php echo URL_PORTAL; ?>imagenes/plus.png") no-repeat right center; margin-left:95%;}
#accordion span.minus{background:url("<?php echo URL_PORTAL; ?>imagenes/minus.png") no-repeat right center;  margin-left:95%}
div#wrapTable{padding:0px; margin-bottom:-2px}
div#icon{width:50px; margin:auto;margin-first:5px;padding:0 5px; text-align:center;display:none; cursor:pointer;}
div#icon span{ background:url("<?php echo URL_PORTAL; ?>imagenes/show.png") no-repeat center; padding:10px;}
div#icon span.toggleIcon{ background:url("<?php echo URL_PORTAL; ?>imagenes/hide.png") no-repeat center; padding:10px;}
</style>
</head>

<body>
<div id="wrapTable">
<table width="97%" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr>
    	<td width="13" height="29" class="arriba_iz">&nbsp;</td>
    	<td class="arriba_ce"><span class="letrablanca">::&nbsp;Consulta De Personas&nbsp;::</span></td>
     	<td width="13" class="arriba_de" align="right">&nbsp;</td>
	</tr>   
	<tr>     
		<td class="cuerpo_iz">&nbsp;</td>
		<td class="cuerpo_ce">			 
			<img src="<?php echo URL_PORTAL; ?>imagenes/spacer.gif" width="1" height="1"> 
			<img src="<?php echo URL_PORTAL; ?>imagenes/menu/informacion.png" width="16" height="16" style="border: none; cursor: pointer" title="Manual" onClick="mostrarAyuda();" />
			<img src="<?php echo URL_PORTAL; ?>imagenes/spacer.gif" width="2" height="1"> 
			<img src="<?php echo URL_PORTAL; ?>imagenes/menu/notas.png" width="16" height="16" style="cursor: pointer" title="Colaboracíon en línea" onclick="notas();" />
			<br>
     		<font size=1 face="arial">&nbsp;Info&nbsp;Ayuda</font>
			<div id="error" style="color:#FF0000"></div>
		</td>
		<td class="cuerpo_de">&nbsp;</td>
	</tr>
	<tr>
 		<td class="cuerpo_iz">&nbsp;</td>
		<!-- TABLAS DE FORMULARIO -->
		<td class="cuerpo_ce">
		<center>
			<table width="90%" border="0" cellspacing="0" class="tablero">
    			<tr>
    				<td width="148">Buscar Por:</td>
    				<td width="149">
    					<select name="buscarPor" class="box1" id="buscarPor">
    						<option value="3" selected="selected">IDENTIFICACION</option>
        					<option value="4">NOMBRE COMPLETO</option>
        					<!-- <option value="2">PRIMER NOMBRE</option>
                			<option value="3">PRIMER APELLIDO</option>-->
    					</select>
    				</td>
    				<td width="319">
    					<select id="tipoDocumento" name="tipoDocumento" class="box1">
    						<option value="0" selected="selected">Sin tipo Docto</option>
        				<?php
							$consulta = $objDefiniciones->mostrar_datos(1,2);
							while($row=mssql_fetch_array($consulta)){
								if($row["iddetalledef"] != 5)
									echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
							}
						?>
    					</select>
    					<input name="idT" type="text" class="box1" id="idT"  onkeypress="runSearch(event)" />
    					<input type="text" class="box" id="pn" style="display:none" /> -
              			<input type="text" class="box" id="pa" style="display:none" />
    				</td>
    				<td width="316">
    					<input name="buscarT" type="button" class="ui-state-default" id="buscarT" value="Buscar" />
    					<span class="Rojo"></span>
    				</td>
    			</tr>
    		</table>
    	</center>
    	<div id="trabajadores" align="center"> </div>
   		<td class="cuerpo_de">&nbsp;</td><!---fondo derecha--->
  	</tr>
  	<tr>
	    <td height="41" class="abajo_iz" >&nbsp;</td>
	    <td class="abajo_ce" ></td>
	    <td class="abajo_de" >&nbsp;</td>
    </tr>    
</table>
</div>
<div id="icon"><span></span></div>

<div id="tabs" style="display:none;">
	<ul>
		<li><a href='#tabs-1' alt='a'>Datos Persona</a></li>
	    <li><a href='#tabs-2' alt='b'>Afiliaciones</a></li>
	    <li><a href='#tabs-3' alt='c'>Documentos</a></li>
	</ul>
		
  	<div id="tabs-1"></div>
  	<div id="tabs-2"></div>
  	<div id="tabs-3"></div>
</div>

<!-- ayuda en linea -->
<div id="ayuda" title="Manual .:. Consulta Trabajadores" style="background-image:url(<?php echo URL_PORTAL; ?>imagenes/FondoGeneral0.png)"></div>
<!-- fin ayuda en linea -->

<!-- colaboracion en linea -->
<div id="dialogo-archivo" title="Archivo banco">
<div id="progreso" style="display: none; font-size: 15pt; font-weight: bold;">Procesado(s) <span id="pg">0</span> de <span id="tt"></span> archivo(s)</div>
<div id="log"></div>
</div>
<div id="dialog-form2" title="Colaboraci&oacute;n en l&iacute;nea" style="display:none">
<p>Por favor diligencie este formulario para enviar comentarios, errores o falencias encontradas en el proceso. M&aacute;ximo 250 caracteres </p>
<label>Tus comentarios:</label><br />
<textarea name="notas" id="notas" cols="60" rows="10"></textarea>
</div>

<!-- fin colaboracion --></div>
</body>
<script>
$("#idT").focus();
</script>
</html>