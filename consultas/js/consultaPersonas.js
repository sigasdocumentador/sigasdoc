/*
* @autor:      Ing. Orlando Puentes
* @fecha:      04/09/2010
* objetivo:
*/
var arrIde=new Array();
var idEmpresa=0;
var idPersona=0;
$(document).ready(function(){
	$("#buscarPor").val("3");
	
	$("#buscarPor").change(function(){
		$("#pn,#pa,#idT").val('');
		if($(this).val()=='4'){
			$("#tipoDocumento").hide();
			$("#pn,#pa").show();
			$("#idT").hide();
		}else{
			$("#tipoDocumento").show();
			$("#pn,#pa").hide();
			$("#idT").show();
			$("#idT").focus();
		}
	});

	var URL=src();

//Dialog Colaboracion en linea
$("#dialog-form2").dialog({
		autoOpen: false,
		height: 400,
		width: 500,
		modal: false,
		buttons: {
			'Enviar': function() {
			var bValid = true;
			var campo=$('#notas').val();
			var campo0=$.trim(campo);
			if (campo0==""){
				$(this).dialog('close');
					return false;
				}
			var campo1=$('#usuario').val();
			var campo2="consultaTrabajador.php";
			$.post(URL+'phpComunes/colaboracion.php',{v0:campo0,v1:campo1,v2:campo2},
			function(datos){
				if(datos=='1'){
					alert("Su comentario fue enviado correctamente!. Gracias por participar en el mejoramiento de SIGAS");
				}else{
					alert(datos);
				}
		});
		$(this).dialog('close');
		},
		Cancelar: function() {
			$(this).dialog('close');
			$("#dialog-form2").dialog("destroy");
		}
	}
	});

//Buscar Trabajador
$("#buscarT").click(function(){
	$("#trabajadores").find("p").remove();
	$("#buscarT").next("span.Rojo").html("");
	dialogLoading("show");
	$("#tabs").hide();
	
	if($("#buscarPor").val()=='0'){
		$(this).next("span.Rojo").html("Seleccione el criterio de b\u00FAsqueda.").hide().fadeIn("slow");
		dialogLoading("hide");		
		return false;
	} else if($("#idT").is(":visible")){
		$("#idT").val($.trim($("#idT").val()));
		if ( $("#idT").val()=='' ) { //|| isNaN($("#idT").val()) ) {
			if ( $("#idT").val()=='' ) {
				$(this).next("span.Rojo").html("Ingrese un valor a buscar.").hide().fadeIn("slow");
			} else if ( isNaN($("#idT").val()) ) {
				$(this).next("span.Rojo").html("La Identificaci\u00F3n debe ser num\u00E9rico.").hide().fadeIn("slow");
			}
			dialogLoading("hide");
			$("#idT").focus();
			return false;
		}
	} else if($ ("#pn").is(":visible")){		
		if ( ( $("#pn").val() == '' && $("#pa").val() == '' ) || ( /[^A-Za-z\d]/.test($("#pn").val()) ) || ( /[^A-Za-z\d]/.test($("#pa").val()) ) ) {			
			if ( ( $("#pn").val() == '' && $("#pa").val() == '' ) ){
				$(this).next("span.Rojo").html("Ingrese un valor a buscar.").hide().fadeIn("slow");
			} else if ( ( /[^A-Za-z\d]/.test($("#pn").val())) ) {
				$(this).next("span.Rojo").html("El Nombre debe ser solo letras.").hide().fadeIn("slow");
			} else {
				$(this).next("span.Rojo").html("El Apellido debe ser solo letras.").hide().fadeIn("slow");
			}
			
			dialogLoading("hide");
			$("#pn").focus();
			return false;
		}
	}	
	
	var v0=$.trim($("#idT").val());
	var v1=$("#buscarPor").val();
	var v2=$("#tipoDocumento").val();
	var v3=$("#pn").val();//campo nombre
	var v4=$("#pa").val();//campo nombre
	
	$.ajax({
		url: URL+'phpComunes/pdo.buscar.persona2.php',
		type: "POST",
		data: {v0:v0,v1:v1,v2:v2,v3:v3.toUpperCase(),v4:v4.toUpperCase()},
		async: false,
		dataType: 'json',
		success: function(data){
			dialogLoading("hide");
			
			if(data==0){
				$("#buscarT").next("span.Rojo").html("No se encontr&oacute; la persona").hide().fadeIn("slow");
				return false;
			}
			
			$.each(data,function(i,fila){
				$("#buscarT").next("span.Rojo").html("");
				$("#idT").val('');
				idp=fila.idpersona;
				nom =fila.pnombre+" "+fila.snombre+" "+fila.papellido+" "+fila.sapellido;
				$("#trabajadores").append('<p align="left" onclick="buscarPersona('+idp+')">'+idp+" "+nom+'</p>');			
			});//fin each		
		}
	});
});//fin click
	
//... ICONO PARA OCULTAR/MOSTRAR FORMULARIO ...
$("div#icon span").click(function(){
	$(this).toggleClass("toggleIcon");
	$("div#wrapTable").slideToggle();
	
});//fin click icon	
});//fin ready

function runSearch(e){
	if(e.keyCode == 13){
		$("#buscarT").trigger("click");
	} 
}

//... FUNCION PARA ENVIAR NIT PARA ACORDEON ...//
function buscarPersona(idp){
	var	indexTab;
	idPersona=idp;
	$("div#icon").show();
	$("#trabajadores").find("p").remove();
	$("div#wrapTable").slideUp("normal");
	$("#tabs").tabs("destroy");//Destruyo tab para nueva consulta
	$("#tabs").show("slow",function(){
		$("#tabs ul li:first a").trigger("click");
	});	//Muestro el tab
	
	//Aqui Capturo el valor del ALT para el switch que carque los formularios	 
	$("#tabs ul li a").click(function(){
		indexTab=$(this).attr("alt");
		if(indexTab=='e'){
			//$(this).unbind('click');
		}
	});
   
	$("#tabs").tabs({spinner:'<em>Loading&#8230;</em>',selected:-1,select:function(){
		//CARGAR TABLA DEACUERDO A TAB SELECCIONADO
		switch(indexTab){
			case "a": $("#tabs-1").load("tabConsulta/tabPersona.php",{v0:idp}); break;
			case "b": $("#tabs-2").load("tabConsulta/tabAfiliaciones.php",{v0:idp}); break; 
			case "c": $("#tabs-3").load(URL+"aportes/trabajadores/tabConsulta/documentosTab.php",{v0:idp}); break; 
		}	//end switch				 				 
	}});//end tab		


}//END FUNCTION BUSCAR NIT

var nuevo=0;
var modificar=0;
var continuar=true;
var msg="";

$(function() {
		$("#ayuda").dialog({
		 	autoOpen: false,
			height: 450, 
			width: 700, 
			draggable:true,
			modal:false,
			open: function(evt, ui){
					$('#ayuda').html('');
					$.get(URL+'help/Consulta/ayudaConsultaPersona.html',function(data){
							$('#ayuda').html(data);
					});
			 }
		});
	});