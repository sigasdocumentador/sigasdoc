<?php
set_time_limit ( 0 );
ini_set ( "display_errors", '1' );
date_default_timezone_set ( "America/Bogota" );

$c0=$_REQUEST["v0"];

$root = $_SERVER ['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once $root;
include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'funcionesComunes' . DIRECTORY_SEPARATOR . 'funcionesComunes.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'rsc' . DIRECTORY_SEPARATOR . 'pdo' . DIRECTORY_SEPARATOR . 'IFXDbManejador.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'rsc' . DIRECTORY_SEPARATOR . 'pdo' . DIRECTORY_SEPARATOR . 'IFXerror.php';
$db = IFXDbManejador::conectarDB ();
if ($db->conexionID == null) {
	$cadena = $db->error;
	echo -2;
	exit ();
}

$directorio = $ruta_cargados . "credito".DIRECTORY_SEPARATOR."cargados".DIRECTORY_SEPARATOR;
$dir = opendir ( $directorio );
while ( $elemento = readdir ( $dir ) ) {
	if (strlen ( $elemento ) > 2)
		$archivos [] = $elemento;
}
closedir ( $dir );
$directorioProcesado = $ruta_cargados . "credito/procesados/";
if (!file_exists($directorioProcesado)) {
	mkdir($directorioProcesado, 0777, true);
}
$archivo='reporte.csv';
$rutadelplano=$directorioProcesado.DIRECTORY_SEPARATOR.$archivo;
$cadena = "NIT;RAZON SOCIAL;DIRECCION;TELEFONO;MUNICIPIO EMPRE;IDENTIFICACION;NOMBRE AFILIADO;DIRECCION;TELEFONO;MUNICIPIO AFIL;EDAD;No HIJOS;FECHA INGRESO;DIAS LABORA ACTUAL;DIAS LABORA 2 A�OS;SALARIO;AGENCIA;CATEGORIA;ESTADO\r\n";
$fp=fopen($rutadelplano,'w');
fwrite($fp, $cadena);
fclose($fp);

$i=0;

	for(; $i<count($archivos); $i++){
		if($archivos[$i]==$c0){
			$directorio .= $archivos[$i];
			$archivoSubido = file ( $directorio );
			$totalLineas = count ( $archivoSubido );
			$cont=0;	
			for($c = 0; $c < $totalLineas; $c ++) {
				$cont++;
				$linea = $archivoSubido [$c];
				$documento = explode ( ' ', $linea );
				$num=trim($documento[0]);
				buscar($num,$db,$rutadelplano);
			}		
			break;
		}
	}


	if($cont==0){
		echo -1;
		exit();
	}
	
	unlink($directorio);


function buscar($num,$db,$rutadelplano){
	$sql="SELECT DISTINCT a48.nit,a48.razonsocial,a48.direccion dirEmpresa,a48.telefono telEmpresa,
		(SELECT TOP 1 a89.municipio FROM aportes089 a89 WHERE a48.iddepartamento=a89.coddepartamento and a48.idciudad=a89.codmunicipio) AS municipio,
		a15.idpersona,a15.identificacion,a15.papellido,
		a15.sapellido,a15.pnombre,a15.snombre,a15.direccion,a15.telefono,
		(SELECT TOP 1 t89.municipio FROM aportes089 t89 WHERE a15.iddepresidencia=t89.coddepartamento AND a15.idciuresidencia=t89.codmunicipio) AS munictraba,
		datediff(yy,a15.fechanacimiento,getdate()) edad,datediff(dd,a16.fechaingreso,getdate()) AS diasLab,a16.salario,
 		(SELECT COUNT(*) FROM aportes021 a21 WHERE a21.idtrabajador=a15.idpersona AND a21.idparentesco IN (35,38) AND a21.estado='A') hijos,
		a16.fechaingreso,a500.agencia,
		a16.categoria AS  cat,a16.estado FROM aportes016 a16 
		INNER JOIN aportes048 a48 ON a16.idempresa=a48.idempresa 
		INNER JOIN aportes015 a15 ON a16.idpersona=a15.idpersona
		LEFT JOIN aportes500 a500 ON a16.idagencia=a500.codigo
		WHERE a16.estado='A' AND a15.identificacion IN ('$num')";
	$rs=$db->querySimple($sql);
	
	while ($row=$rs->fetch()){
	
	$sql2="SELECT sum(a.dias) cant FROM (
			SELECT datediff(dd,
				       (CASE WHEN datediff(dd,CAST(dateadd(yy,-2,getDate()) AS DATE),a17.fechaingreso)>=0 THEN a17.fechaingreso ELSE CAST(dateadd(yy,-2,getDate()) AS DATE) END),
					   (CAST(getDate() AS DATE))
				   ) dias
			FROM aportes016 a17
			WHERE a17.idpersona=$row[idpersona]  
			UNION
			SELECT datediff(dd,
				       (CASE WHEN datediff(dd,CAST(dateadd(yy,-2,getDate()) AS DATE),a17.fechaingreso)>=0 THEN a17.fechaingreso ELSE CAST(dateadd(yy,-2,getDate()) AS DATE) END),
					   (CASE WHEN datediff(dd,getDate(),a17.fecharetiro)>0 THEN getDate() ELSE a17.fecharetiro END)
				   ) dias
			FROM aportes017 a17
			WHERE a17.idpersona=$row[idpersona]  AND a17.fecharetiro >= CAST(dateadd(yy,-2,getDate()) AS DATE)
			) a";
	$rs2=$db->querySimple($sql2);
	while($row2 = $rs2->fetch()){
	$contidad=$row2['cant'];
	}

	if( !$row ){
		$sql="SELECT TOP 1 a48.nit,a48.razonsocial,a48.direccion dirEmpresa,a48.telefono telEmpresa,a89.municipio,
		a15.idpersona,a15.identificacion,a15.papellido,a15.sapellido,a15.pnombre,a15.snombre,a15.direccion,a15.telefono,t89.municipio AS munictraba,datediff(yy,a15.fechanacimiento,getdate()) edad,
		(SELECT COUNT(*) FROM aportes021 a21 WHERE a21.idtrabajador=a15.idpersona AND a21.idparentesco IN (35,38) AND a21.estado='A') hijos,a16.fechaingreso,datediff(dd,a16.fechaingreso,a16.fecharetiro) AS diasLab,a16.salario,
		a500.agencia,
		a16.categoria AS  cat,a16.estado 
		FROM aportes017 a16 
		INNER JOIN aportes015 a15 ON a16.idpersona=a15.idpersona 
		INNER JOIN aportes048 a48 ON a16.idempresa=a48.idempresa 
		LEFT JOIN aportes089 a89 ON a48.idciudad=a89.codmunicipio 
		LEFT JOIN aportes089 t89 ON a15.iddepresidencia=t89.coddepartamento AND a15.idciuresidencia=t89.codmunicipio
		LEFT JOIN aportes500 a500 ON a16.idagencia=a500.codigo
		WHERE  a15.identificacion IN ('$num') ORDER BY a16.fecharetiro desc";
		$rs=$db->querySimple($sql);
		$row=$rs->fetch();
		
		$sql2="SELECT datediff(dd,
				(CASE WHEN datediff(dd,CAST(dateadd(yy,-2,getDate()) AS DATE),a17.fechaingreso)>=0 THEN a17.fechaingreso ELSE CAST(dateadd(yy,-2,getDate()) AS DATE) END),
				(CASE WHEN datediff(dd,getDate(),a17.fecharetiro)>0 THEN getDate() ELSE a17.fecharetiro END)
				) dias
				FROM aportes017 a17
				WHERE a17.idpersona=$row[idpersona]  AND a17.fecharetiro >= CAST(dateadd(yy,-2,getDate()) AS DATE)
				) a";
		$rs2=$db->querySimple($sql2);
		while($row2 = $rs2->fetch()){
		$contidad=$row2['cant'];
		}
		
		if( !$row ){
			$cade=$num."; no existe en nuestra base de datos;\r\n";
			$fp=fopen($rutadelplano,'a');
			fwrite($fp, $cade);
			fclose($fp);
		}
		else
			escribirPlano($row,$contidad,$rutadelplano);
	}
	else{
		escribirPlano($row,$contidad,$rutadelplano);
	}
	break;
 }
		
}

function escribirPlano($row,$contidad,$rutadelplano){
	$nombres=$row['pnombre'].' '.$row['snombre'].' '.$row['papellido'].' '.$row['sapellido'];
	$cadena = "$row[nit];$row[razonsocial];$row[dirEmpresa];$row[telEmpresa];$row[municipio];$row[identificacion];$nombres;$row[direccion];$row[telefono];$row[munictraba];$row[edad];$row[hijos];$row[fechaingreso];$row[diasLab];$contidad;$row[salario];$row[agencia];$row[cat];$row[estado]"."\r\n";
	$fp=fopen($rutadelplano,'a');
	fwrite($fp, $cadena);
	fclose($fp);
}

$_SESSION['ENLACE']=$rutadelplano;
$_SESSION['ARCHIVO']=$archivo;
echo 1;
?>
