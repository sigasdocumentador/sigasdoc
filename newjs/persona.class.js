function Persona(){
	var idpersona; 
	var idtipodocumento; 
	var identificacion; 
	var papellido; 
	var sapellido; 
	var pnombre; 
	var snombre; 
	var sexo; 
	var direccion; 
	var idbarrio; 
	var telefono; 
	var celular; 
	var email; 
	var idpropiedadvivienda; 
	var idtipovivienda; 
	var idciuresidencia; 
	var iddepresidencia; 
	var idzona; 
	var idestadocivil; 
	var fechanacimiento; 
	var idciunace; 
	var iddepnace; 
	var capacidadtrabajo; 
	var idprofesion; 	
	var rutadocumentos; 
	var fechaafiliacion; 
	var estado; 
	var validado; 
	var usuario; 
	var fechasistema; 
	var fechaactualizacion; 
	var nombrecorto; 
	var codigoedad; 
	var edad; 
	var fechadefuncion; 
	var tipoestado; 
	var idpais;
}

function camposPersona(objeto){
	objeto.idpersona = esUndefined(objeto.idpersona); 
	objeto.idtipodocumento = esUndefined(objeto.idtipodocumento); 
	objeto.identificacion = esUndefined(objeto.identificacion); 
	objeto.papellido = esUndefined(objeto.papellido); 
	objeto.sapellido = esUndefined(objeto.sapellido); 
	objeto.pnombre = esUndefined(objeto.pnombre); 
	objeto.snombre = esUndefined(objeto.snombre); 
	objeto.sexo = esUndefined(objeto.sexo); 
	objeto.direccion = esUndefined(objeto.direccion); 
	objeto.idbarrio = esUndefined(objeto.idbarrio); 
	objeto.telefono = esUndefined(objeto.telefono); 
	objeto.celular = esUndefined(objeto.celular); 
	objeto.email = esUndefined(objeto.email); 
	objeto.idpropiedadvivienda = esUndefined(objeto.idpropiedadvivienda); 
	objeto.idtipovivienda = esUndefined(objeto.idtipovivienda); 
	objeto.idciuresidencia = esUndefined(objeto.idciuresidencia); 
	objeto.iddepresidencia = esUndefined(objeto.iddepresidencia); 
	objeto.idzona = esUndefined(objeto.idzona); 
	objeto.idestadocivil = esUndefined(objeto.idestadocivil); 
	objeto.fechanacimiento = esUndefined(objeto.fechanacimiento); 
	objeto.idciunace = esUndefined(objeto.idciunace); 
	objeto.iddepnace = esUndefined(objeto.iddepnace); 
	objeto.capacidadtrabajo = esUndefined(objeto.capacidadtrabajo); 
	objeto.idprofesion = esUndefined(objeto.idprofesion); 
	objeto.rutadocumentos = esUndefined(objeto.rutadocumentos);
	objeto.fechaafiliacion = esUndefined(objeto.fechaafiliacion); 
	objeto.estado = esUndefined(objeto.estado ); 
	objeto.validado = esUndefined(objeto.validado ); 
	objeto.usuario = esUndefined(objeto.usuario ); 
	objeto.fechasistema = esUndefined(objeto.fechasistema ); 
	objeto.fechaactualizacion = esUndefined(objeto.fechaactualizacion ); 
	objeto.nombrecorto = esUndefined(objeto.nombrecorto ); 
	objeto.codigoedad = esUndefined(objeto.codigoedad ); 
	objeto.edad = esUndefined(objeto.edad ); 
	objeto.fechadefuncion = esUndefined(	objeto.fechadefuncion ); 
	objeto.tipoestado = esUndefined(objeto.tipoestado ); 
	objeto.idpais = esUndefined(objeto.idpais );

return objeto;
}

/**
 * Funcion que verifica si una variable esta sin definir
 * 
 *  @param variable 	elemento a validar
 *  @returns 			retorna la variable despues de validar
 */
function esUndefined(variable){
	if(variable==undefined)
		variable='';
	return variable;
}