/**
 * Funcion-clase que contiene todos los campos existentes en la base de datos de la tabla de radicacion(aportes004) 
 */
function Radicacion(){
	var idradicacion;
	var fecharadicacion;
	var identificacion;
	var idtipodocumento;
	var idtiporadicacion;
	var idtipopresentacion;
	var horainicio;
	var horafinal;
	var tiempo;
	var notas;
	var idtipoinformacion;
	var idtipodocumentoafiliado;
	var numero;
	var nit;
	var folios;
	var idtipocertificado;
	var idbeneficiario;
	var observaciones;
	var asignado;
	var fechaasignacion;
	var digitalizada;
	var fechadigitalizacion;
	var procesado;
	var fechaproceso;
	var flag;
	var tempo1;
	var fechasistema;
	var usuario;
	var usuarioasignado;
	var anulada;
	var usuarioanula;
	var idtipoformulario;
	var devuelto;
	var motivodevolucion;
	var idagencia;
	var idtipodocben;
	var numerobeneficiario;
	var afiliacionmultiple;
	var cierre;
	var recibeventanilla;
	var recibegrabacion;
	var fecharecibegraba;
	var idtipomovilizacion;
	var habeas_data;
}

/**
 * Funcion que inicializa todos los campos de la radicacion 
 */
function camposRadicacion(objeto){ var obj = new Radicacion();
	obj.idradicacion = esUndefined(objeto.idradicacion);
	obj.fecharadicacion = esUndefined(objeto.fecharadicacion);
	obj.identificacion = esUndefined(objeto.identificacion);
	obj.idtipodocumento = esUndefined(objeto.idtipodocumento);
	obj.idtiporadicacion = esUndefined(objeto.idtiporadicacion);
	obj.idtipopresentacion = esUndefined(objeto.idtipopresentacion);
	obj.horainicio = esUndefined(objeto.horainicio);
	obj.horafinal = esUndefined(objeto.horafina);
	obj.tiempo = esUndefined(objeto.tiempo);
	obj.notas = esUndefined(objeto.notas);
	obj.idtipoinformacion = esUndefined(objeto.idtipoinformacion);
	obj.idtipodocumentoafiliado = esUndefined(objeto.idtipodocumentoafiliado);
	obj.numero = esUndefined(objeto.numero);
	obj.nit = esUndefined(objeto.nit);
	obj.folios = esUndefined(objeto.folios);
	obj.idtipocertificado = esUndefined(objeto.idtipocertificado);
	obj.idbeneficiario = esUndefined(objeto.idbeneficiario);
	obj.observaciones = esUndefined(objeto.observaciones);
	obj.asignado = esUndefined(objeto.asignado);
	obj.fechaasignacion = esUndefined(objeto.fechaasignacion);
	obj.digitalizada = esUndefined(objeto.digitalizada);
	obj.fechadigitalizacion = esUndefined(objeto.fechadigitalizacion);
	obj.procesado = esUndefined(objeto.procesado);
	obj.fechaproceso = esUndefined(objeto.fechaproceso);
	obj.flag = esUndefined(objeto.flag);
	obj.tempo1 = esUndefined(objeto.tempo1);
	obj.fechasistema = esUndefined(objeto.fechasistema);
	obj.usuario = esUndefined(objeto.usuario);
	obj.usuarioasignado = esUndefined(objeto.usuarioasignado);
	obj.anulada = esUndefined(objeto.anulada);
	obj.usuarioanula = esUndefined();
	obj.idtipoformulario = esUndefined(objeto.idtipoformulario);
	obj.devuelto = esUndefined(objeto.devuelto);
	obj.motivodevolucion = esUndefined(objeto.motivodevolucion);
	obj.idagencia = esUndefined(objeto.idagencia);
	obj.idtipodocben = esUndefined(objeto.idtipodocben);
	obj.numerobeneficiario = esUndefined(objeto.numerobeneficiario);
	obj.afiliacionmultiple = esUndefined(objeto.afiliacionmultiple);
	obj.cierre = esUndefined(objeto.cierre);
	obj.recibeventanilla = esUndefined(objeto.recibeventanilla);
	obj.recibegrabacion = esUndefined(objeto.recibegrabacion);
	obj.fecharecibegraba = esUndefined(objeto.fecharecibegraba);
	obj.idtipomovilizacion = esUndefined(objeto.idtipomovilizacion);
	obj.habeas_data = esUndefined(objeto.habeas_data);
	
	return obj;
}