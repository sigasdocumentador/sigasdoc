<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$usuario=$_SESSION['USUARIO'];
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
$tipo=$_REQUEST['tipo'];
$op=isset($_REQUEST['op'])?$_REQUEST['op']:"";
$titulo='...';
$rept=$_REQUEST['tit'];
switch($rept){
	case 1:$titulo='Listado Fallecidos'; break;
	case 2:$titulo=''; break;
	case 3:$titulo=''; break;
	case 4:$titulo=''; break;
	}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Men&uacute; Reportes Empresa</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link type="text/css" rel="stylesheet" href="../../../css/css.1.8/jquery-ui-1.8.17.custom.css" />
<link type="text/css" href="../../../css/Estilos.css" rel="stylesheet"/>
<link href="../../../css/marco.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="../../../js/1.6/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="../../../js/1.6/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="../../../js/comunes.js"></script>
<script type="text/javascript" src="js/reportes.js"></script>
<script>
$(function(){

	$('#txtFechaI').datepicker({
		maxDate:"+0D",
		changeMonth: true,
		changeYear: true,
		onSelect: function(dateText, inst) {
			var lockDate = new Date($('#txtFechaI').datepicker('getDate'));
			$('input#txtFechaF').datepicker('option', 'minDate', lockDate);
		}
	});
	$("#txtFechaF").datepicker({
		maxDate:"+0D",
		changeMonth: true,
		changeYear: true
	});
   
       
	$("#txtPeriodo,#txtPeriodo2").datepicker({
        dateFormat: 'yymm',
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        onClose: function(dateText, inst) {
        	var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            $(this).datepicker('setDate', new Date(year, month, 1));
            }
        }); 

	
	$("#txtPeriodo,#txtPeriodo2").focus(function () {
		$(".ui-datepicker-calendar").hide();
		$("#ui-datepicker-div").position({
			my: "center top",
			at: "center bottom",
			of: $(this)
		});
	});

	$("#anno").css('display','none');
	$("#rangoPeriodo").css('display','none');
	$("#rangoAnno").css('display','none');

});
</script>
</head>
<body>
	<center>
	<table width="100%" border="0">
	<tr>
	<td align="center" ><img src="../../../imagenes/logo_reporte.png" width="362" height="70"></td>
	</tr>
	<tr>
	<td align="center" ><?php echo $titulo ?></td>
	</tr>
	</table>
	<br>
<?php
if($tipo==1){
	$sql="Select usuario,nombres from aportes519 where substring(usuario, 1, 3) in ('sub', 'gar', 'pit', 'pla') order by nombres";
	$rs=$db->querySimple($sql);
?>
<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
	<tr>
    	<th colspan="4"><strong>Parametros de Configuracion Reporte</strong></th>
    </tr>
    <tr>
    <td colspan="4" style="text-align:center">Usuario:
    <select id="txtUsuario2" name="txtUsuario2" class="box1">
    <option value="T">Todos</option>
<?php
	  while($row=$rs->fetch()){
		  echo "<option value=".$row['usuario'].">".$row['nombres']."</option>";
	  }
?>
      </select>
    </td>
    </tr>
	<tr>
		<td style="text-align:center">Fecha Inicial:
			<input type="text" name="txtFechaI" id="txtFechaI" readonly="readonly"/> 
		</td>
		<td style="text-align:center">Fecha Final:
			<input type="text" name="txtFechaF" id="txtFechaF" readonly="readonly"/> 
		</td>
	</tr>	
	<tr>
	  <td colspan="4">&nbsp;</td>
	</tr>
	<tr>
	  	<td style="text-align:center" colspan="4">
      	<!-- <label style="cursor:pointer" onClick="procesar11(0);" id="idProcesar004"><img src="../../../imagenes/icono_pdf.png" width="32" height="32"/></label>
      		&nbsp;&nbsp;  -->
      		<label style="cursor:pointer" onClick="procesar1(1);" id="idProcesar004"><img src="../../../imagenes/icono_excel.png" width="32" height="32"/></label>
  		</td>
	  </tr>
    </table>
<?php 
}
?>
<input type="hidden" id="txtusuario" name="txtusuario" value="<?php echo $usuario; ?>" /> 
<input type="hidden" id="txtRept" name="txtRept" value="<?php echo $rept; ?>" />
</body>

</html>

