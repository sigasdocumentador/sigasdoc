<?php
/* autor:       orlando puentes
 * fecha:       28/09/2010
 * objetivo:    

if ( !isset( $_SESSION ['USUARIO'] ) ) {session_start();}
$raiz=$_SESSION['RAIZ'];
chdir($raiz);
*/
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$usuario=$_SESSION['CEDULA'];

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Men&uacute; Reportes Radicaci&oacute;n</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link type="text/css" href="../../../css/Estilos.css" rel="stylesheet"/>
<script language="javascript" src="js/reportes.js"></script>
<script language="javascript" src="../../../js/comunes.js"></script>

<link type="text/css" href="../../../css/css.1.8/jquery-ui-1.8.17.custom.css" rel="stylesheet"/>
<link href="../../../css/jMenu.jquery.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../../../js/jquery-1.4.2.js"></script>
<script type="text/javascript" src="../../../js/jquery-ui.js"></script>
<script type="text/javascript" src="../../../js/jMenu.jquery.js"></script> 
</head>
<body>

<center>
	<table width="100%" border="0">
	<tr>
	<td align="center" ><img src="../../../imagenes/logo_reporte.png"></td>
	</tr>
	<tr>
	<td align="center" >Listado de Reportes Subsidio Cuota Monetaria</td>
	</tr>
	</table>
	<br>
  <table width="60%" border="0" cellspacing="1" class="tablero">
	  <tr>
		  <th width="16%">Reporte</th>
		  <th width="84%" colspan="2">Descripci&oacute;n</th></tr>  
	<tr>
	   <td scope="row" align="left">Reporte001</td>             
	   <td align="left"><a  href="configReporteEmpresasContrato.php?tipo=1&tit=1">Contratos liquidados por rango de fecha</a></td>
    </tr>
	<tr>
	   <td scope="row" align="left">Reporte002</td>             
	   <td align="left"></td>
    </tr>
	<tr>
	  <td>Reporte003</td>
	  <td></td>
  	</tr>  		
	<tr>
	  <td>Reporte004</td>
	  <td align="left"></td>
    </tr>
	<tr>
		<td>Reporte005</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>Reporte006</td>
		<td></td>
	</tr>
	<tr>
		<td>Reporte007</td>
		<td>&nbsp;</td>
	</tr>
    <tr>
		<td>Reporte008</td>
		<td>&nbsp;</td>
	</tr>
    <tr>
		<td>Reporte009</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
  </table>
  </center>
</body> 
</html>

