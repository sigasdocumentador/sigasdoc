<?php
/* autor:       orlando puentes
 * fecha:       28/08/2012
 * objetivo:    configuracion de los reportes 
 **********************************************************

 
*/

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$usuario=$_SESSION['CEDULA'];
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
$tipo=$_REQUEST['tipo'];
$titulo='...';
$rept=$_REQUEST['tit'];
switch($rept){
	case 1:$titulo='Contratos liquidados por rango de fecha'; break;
	case 2:$titulo=''; break;
	case 3:$titulo=''; break;
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Men&uacute; Reportes Asopagos</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link type="text/css" rel="stylesheet" href="../../../css/css.1.8/jquery-ui-1.8.17.custom.css" />
<link type="text/css" href="../../../css/Estilos.css" rel="stylesheet"/>
<link href="../../../css/marco.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="../../../js/1.6/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="../../../js/1.6/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="../../../js/comunes.js"></script>
<script type="text/javascript" src="js/reporte.js"></script>
</head>
<body>
	<center>
	<table width="100%" border="0">
	<tr>
	<td align="center" ><img src="../../../imagenes/logo_reporte.png" width="362" height="70"></td>
	</tr>
	<tr>
	<td align="center" ><?php echo $titulo ?></td>
	</tr>
	</table>
	<br>
<?php
if($tipo==1){
?>
	<div id="tipo1" style="display:block">
		<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
			<tr>
	    		<th colspan="4"><strong>Parametros de Configuracion Reporte</strong></th>
		    </tr>
		    <tr>
		      	<td width="25%" style="text-align:right">Fecha Inicial &nbsp;</td>
		      	<td width="25%"><input type="text" name="txtFechaI" size="10" id="txtFechaI" readonly="readonly"/></td>
		    </tr>
		    <tr>
      			<td width="25%" style="text-align:right">Fecha Final &nbsp;</td>
      			<td width="25%"><input type="text" name="txtFechaF" size="10" id="txtFechaF" readonly="readonly" /></td>
    		</tr>
    		<tr>
    			<td colspan="4" style="text-align:center">
    				<label style="cursor:pointer" onClick="reporte001();"><img src="../../../imagenes/icono_pdf.png" width="32" height="32"></label> &nbsp;&nbsp;
    			</td>
    		</tr>
    <tr>
      <td colspan="4">&nbsp;</td>
  	</tr>
    </table>
 <?php
}
?>

<input type="hidden" id="txtusuario" name="txtusuario" value="<?php echo $usuario; ?>" /> 
<input type="hidden" id="txtRept" name="txtRept" value="<?php echo $rept; ?>" />
</body>

</html>