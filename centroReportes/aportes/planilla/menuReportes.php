<?php
/* autor:       Lucy Tatiana Polanco Aya
 * fecha:       19/08/2011
 * objetivo:    

if ( !isset( $_SESSION ['USUARIO'] ) ) {session_start();}
$raiz=$_SESSION['RAIZ'];
chdir($raiz);
*/
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

if(!isset($_SESSION))
	session_start();
if(!defined("DIRECTORIO_RAIZ"))
	include_once($_SESSION["RAIZ"]."config.php");
$url=$url_rep;
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Men&uacute; Reportes Planilla &Ucirc;nica</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link type="text/css" href="<?php echo URL_PORTAL.DIRECTORIO_ESTILOS_CSS; ?>formularios/base/ui.all.css" rel="stylesheet" />
<link type="text/css" href="<?php echo URL_PORTAL.DIRECTORIO_ESTILOS_CSS; ?>Estilos.css" rel="stylesheet"/>
<link type="text/css" href="<?php echo URL_PORTAL.DIRECTORIO_ESTILOS_CSS; ?>estilo_tablas.css" rel="stylesheet"/>
<link type="text/css" href="<?php echo URL_PORTAL.DIRECTORIO_ESTILOS_CSS_FORMULARIOS; ?>formularios/demos.css" />
<link href="<?php echo URL_PORTAL.DIRECTORIO_ESTILOS_CSS; ?>estiloReporte.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../../js/comunes.js"></script>
</head>
<body>
	<center>
	<table width="100%" border="0">
	<tr>
	<td align="center" ><p><img src="../../imagenes/logo_reporte.png" width="362" height="70"></p>
	  <p>&nbsp;</p></td>
	</tr>
	<tr>
	<td align="center" ><strong>Listado de Reportes de Planilla &Uacute;nica</strong></td>
	</tr>
	</table>
	<br>
	<table width="60%" border="0" cellspacing="1" class="tablero">
		<tr>
			<th>Reporte</th>
			<th colspan="2">Descripci&oacute;n</th></tr>  
		<tr>
			<td>Reporte001</td>
			<td><a href="configReporte001.php">Afiliaciones Pendientes</a></td>
		</tr>
		<tr>
			<td>Reporte002</td>
			<td><a href="configReporte002.php">Empresas Aportantes por Periodo</a></td>
		</tr>
        <tr>
			<td>Reporte003</td>
			<td><a href="configReporte003.php">N&oacute;mina Faltantes de Periodo</a></td>
		</tr>
		 <tr>
			<td>Reporte004</td>
			<td><a href="configReporte004.php" >Pago con Moratoria</a></td>
		</tr>
           <tr>
			<td>Reporte005</td>
			<td><a href="configReporte005.php">Novedades por Empresas</a></td>
		</tr>
           <tr>
			<td>Reporte006</td>
			<td><a href="configReporte006.php" >N&oacute;mina Sin Aportes del Periodo Actual</a></td>
		</tr>
	</table>
	</center>
</body> 
</html>