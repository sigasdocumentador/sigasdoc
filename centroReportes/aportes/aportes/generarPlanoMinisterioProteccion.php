<?php
	date_default_timezone_set('America/Bogota');
	set_time_limit(0);
	$raiz="";
	$ruta_generados = "";
	$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
	include_once  $root;
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
	
	$db = IFXDbManejador::conectarDB();
	if($db->conexionID==null){
		$cadena = $db->error;
		echo msg_error($cadena);
		exit();
	}
	
	include_once $raiz.'/config.php';
	$path_plano =$ruta_generados.'aportes'.DIRECTORY_SEPARATOR.'empresas'.DIRECTORY_SEPARATOR;

	$ano=date('Y');
	$mes=date('m');
	
	$sql = "SELECT DISTINCT a48.razonsocial,
CASE a91.codigo WHEN 'NI' THEN '1'
				WHEN 'CC' THEN '2' END tipoDocumen,
a48.nit,a15.pnombre+' '+isnull(a15.snombre,'')+' '+a15.papellido+' '+isnull(a15.sapellido,'') AS nombreRepresentante,
a48.direccion,a89.municipio,a89.departmento,a89.codmunicipio,a48.telefono,a48.email,'' AS paginaWeb,
(SELECT count(*) FROM aportes016 a16 INNER JOIN aportes015 t15 ON t15.idpersona=a16.idpersona AND t15.categoria_persona='A' WHERE a16.idempresa=a48.idempresa) AS numeroTrabaCategoriaA,
(SELECT count(*) FROM aportes016 a16 INNER JOIN aportes015 t15 ON t15.idpersona=a16.idpersona AND t15.categoria_persona='B' WHERE a16.idempresa=a48.idempresa) AS numeroTrabaCategoriaB,
(SELECT count(*) FROM aportes016 a16 INNER JOIN aportes015 t15 ON t15.idpersona=a16.idpersona AND t15.categoria_persona='C' WHERE a16.idempresa=a48.idempresa) AS numeroTrabaCategoriaC,
(SELECT count(*) FROM aportes016 a16 INNER JOIN aportes015 t15 ON t15.idpersona=a16.idpersona WHERE a16.idempresa=a48.idempresa) AS numeroTrabaTotal,
(SELECT a48.actieconomicadane FROM aportes048 b48 INNER JOIN aportes079 a79 ON a79.clase=b48.actieconomicadane AND a79.clase!='' AND len(a79.clase)>='4' WHERE b48.idempresa=a48.idempresa) AS codActividad,
'' AS nombreTalentoHumano,'' AS telefonoTalentoHumano,'' AS celularTalentoHumano,'' AS correoTalentoHumano
FROM aportes048 a48 
LEFT JOIN aportes091 a91 ON a91.iddetalledef=a48.idtipodocumento
LEFT JOIN aportes015 a15 ON a15.idpersona=a48.idrepresentante
LEFT JOIN aportes089 a89 ON a89.coddepartamento=a48.iddepartamento AND a89.codmunicipio=a48.idciudad
WHERE a48.estado='A' AND a48.idtipoafiliacion='3316'";

	$cadena = "";
	
	$rs=$db->querySimple($sql);
	while (($row=$rs->fetch())==true){
		
		$cadena .= "{$row["razonsocial"]}|{$row["tipoDocumen"]}|{$row["nit"]}|{$row["nombreRepresentante"]}|{$row["direccion"]}|{$row["municipio"]}|{$row["departmento"]}|{$row["codmunicipio"]}|{$row["telefono"]}|{$row["email"]}|{$row["paginaWeb"]}|{$row["numeroTrabaCategoriaA"]}|{$row["numeroTrabaCategoriaB"]}|{$row["numeroTrabaCategoriaC"]}|{$row["numeroTrabaTotal"]}|{$row["codActividad"]}|{$row["nombreTalentoHumano"]}|{$row["telefonoTalentoHumano"]}|{$row["celularTalentoHumano"]}|{$row["correoTalentoHumano"]}"."\r\n"; 
	}
	
	//Nombre del archivo plano
	$archivo='Plano_Ministerio_'.$ano.'_'.$mes.'_Detallado.txt';
	$path_plano.='planos'.DIRECTORY_SEPARATOR.$archivo;
	
	$fp=fopen($path_plano,'w');
	fwrite($fp, $cadena);
	fclose($fp);
	$_SESSION['ENLACE']=$path_plano;
	$_SESSION['ARCHIVO']=$archivo;
	echo 1;

?>