<?php
	date_default_timezone_set('America/Bogota');
	set_time_limit(0);
	$raiz="";
	$ruta_generados = "";
	$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
	include_once  $root;
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
	
	$db = IFXDbManejador::conectarDB();
	if($db->conexionID==null){
		$cadena = $db->error;
		echo msg_error($cadena);
		exit();
	}
	
	include_once $raiz.'/config.php';
	$path_plano =$ruta_generados.'aportes'.DIRECTORY_SEPARATOR.'empresas'.DIRECTORY_SEPARATOR;

	$ano=date('Y');
	$mes=date('m');
	
	$sql="SELECT 
			DISTINCT a48.razonsocial,
            a48.idempresa,
            a91.codigo,
	        a48.nit,
	        a48.digito,
	        a48.direccion,
	        a48.iddepartamento,
	        a48.idciudad,
	        a89.departmento,
	        a89.municipio,
	        a48.telefono,
	        a48.email,
	        (SELECT MAX(ultactfechadatosUGPP) FROM aportes123 WHERE idempresa=a48.idempresa) AS fechaUGPP
		  FROM aportes048 a48 
		  INNER JOIN aportes091 a91 ON a48.idtipodocumento=a91.iddetalledef
		  LEFT JOIN aportes089 a89 ON a48.iddepartamento=a89.coddepartamento AND a89.codmunicipio=a48.idciudad
		  WHERE a48.estado='A'";
	
	
	$archivo='CCF_32_'.$ano.'_'.$mes.'_contacto.txt';
	$path_plano.='planos'.DIRECTORY_SEPARATOR.$archivo;
	//$cadena = "admin|adminom|razonsocial|codigo|aportante|digito|direccion|iddepartamento|idciudad|departmento|municipio|No Column Name|No Column Name|No Column Name|No Column Name|No Column Name|telefono|No Column Name|No Column Name|No Column Name|No Column Name|No Column Name|email|No Column Name|fechaactu\r\n";
	
	$cont = 0;
	$rs=$db->querySimple($sql);
	while (($row=$rs->fetch())==true){
		if($row['fechaUGPP']!=''){
			$auxFechaUGPP=explode("-",$row['fechaUGPP']);
			$FechaUGPP=$auxFechaUGPP[2].'-'.$auxFechaUGPP[1].'-'.$auxFechaUGPP['0'];
		}else{
			$FechaUGPP='';
		}
		$cadena .= "CCF32|CAJA DE COMPENSACIÓN|$row[razonsocial]|$row[codigo]|$row[nit]|$row[digito]|$row[direccion]|$row[iddepartamento]|$row[idciudad]|$row[departmento]|$row[municipio]||||||$row[telefono]||||||$row[email]|$FechaUGPP|| "."\r\n";
		$cont++;
	}

	$fp=fopen($path_plano,'w');
	fwrite($fp, $cadena);
	fclose($fp);
	$_SESSION['ENLACE']=$path_plano;
	$_SESSION['ARCHIVO']=$archivo;
	
	echo 1;
?>