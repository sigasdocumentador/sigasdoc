<?php	
	$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
	include_once  $root;
	
	$usuario=$_SESSION['USUARIO'];
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
	//include_once $raiz. DIRECTORY_SEPARATOR .'clases'. DIRECTORY_SEPARATOR . 'p.definiciones.class.php';
	
	$db = IFXDbManejador::conectarDB();
	if($db->conexionID==null){
		$cadena = $db->error;
		echo msg_error($cadena);
		exit();
	}
	
	//$Definiciones = new Definiciones();
	
	$tipo = $_REQUEST['tipo'];
	$titulo='...';
	switch($tipo){
		case 1:$titulo='Plano aportes parafiscales'; break;
		case 2:$titulo='Informe aportes parafiscales participaci&oacute;n'; break;
		case 3:$titulo='Informe UGPP Ubicacion y Contacto'; break;
		case 4:$titulo='Informe UGPP Consolidado'; break;
		case 5:$titulo='Informe UGPP Desagregado'; break;
		case 6:$titulo='Informe Novedades Ingreso y Retiro de Planilla Unica'; break;
		case 7:$titulo='Informe de los traslados de Planilla Unica'; break;
		case 8:$titulo='Informe de los traslados de aportes'; break;
		case 9:$titulo='Informe Comparativo Aportes Parafiscales'; break;
		case 10:$titulo='Informe Intereses Mora Planilla Unica'; break;
		case 11:$titulo='Informe de empresas morosas por algunos trabajadores'; break;
		case 12:$titulo='Informe Trabajadores sin Relacion PU'; break;
		case 15:$titulo='Informe Ministerio de Proteccion Social'; break;
	}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<title>Men&uacute; Reportes Aportes</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		
		<link type="text/css" rel="stylesheet" href="../../../css/css.1.8/jquery-ui-1.8.17.custom.css" />
		<link type="text/css" href="../../../css/Estilos.css" rel="stylesheet"/>
		<link href="../../../css/marco.css" rel="stylesheet" type="text/css" />

		<script type="text/javascript" src="../../../js/1.6/jquery-1.6.2.min.js"></script>
		<script type="text/javascript" src="../../../js/1.6/jquery-ui-1.8.16.custom.min.js"></script>
		<script type="text/javascript" src="../../../js/comunes.js"></script>
		<script type="text/javascript" src="js/reportes.js"></script>
	</head>
	<body>
		<center>
			<table width="100%" border="0">
				<tr>
					<td align="center" ><img src="../../../imagenes/logo_reporte.png" width="362" height="70"></td>
				</tr>
				<tr>
					<td align="center" ><?php echo $titulo ?></td>
				</tr>
			</table>
			<br>
			<?php
				if($tipo==1){
			?>
			<div id="tipo1" style="display:block">
				<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
					<tr>
	   	 				<th colspan="4"><strong>Parametros de Configuracion Reporte</strong></th>
	    			</tr>
	    			<tr>
	      				<td style="text-align: center;">
	      					Fecha Inicio
	      					<input type="text" id="txtFechaI" name="txtFechaI" readonly>
      						Fecha Final
	      					<input type="text" id="txtFechaF" name="txtFechaF" readonly></td>
	    			</tr>
	    			<tr>
			      		<td width="25%" style="text-align: center;">Instituto
				      		<select id="cmbInstituto" name="cmbInstituto" class="box1">
				      			<option value="caja">CAJA</option>
					          	<option value="icbf">ICBF</option>
					          	<option value="sena">SENA</option>
					      	</select>
	      				</td>
    				</tr>
	   				<tr>
	      				<td colspan="4">&nbsp;</td>
	  				</tr>
	    			<tr>
	      				<td colspan="4" style="text-align:center" id="tdProcesar">
	      					<img width="140" onclick="procesar001();" style="cursor: pointer;" height="24" src="../../../imagenes/procesar2.png">
	      				</td>
	    			</tr>
	    		</table>
	    	</div>
	    	<?php 
				}
				if($tipo==2){
			?>
			<div id="tipo1" style="display:block">
				<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
					<tr>
	   	 				<th colspan="4"><strong>Parametros de Configuracion Reporte</strong></th>
	    			</tr>
	    			<tr>
	      				<td style="text-align: center;">
	      					Fecha Inicio
	      					<input type="text" id="txtFechaI" name="txtFechaI" readonly>
      						Fecha Final
	      					<input type="text" id="txtFechaF" name="txtFechaF" readonly></td>
	    			</tr>
	    			<tr>
			      		<td width="25%" style="text-align: center;">Instituto
				      		<select id="cmbInstituto" name="cmbInstituto" class="box1">
				      			<option value="caja">CAJA</option>
					          	<option value="icbf">ICBF</option>
					          	<option value="sena">SENA</option>
					      	</select>
	      				</td>
    				</tr>
	   				<tr>
	      				<td colspan="4">&nbsp;</td>
	  				</tr>
	    			<tr>
	      				<td style="text-align:center" colspan="2" >
							<label style="cursor:pointer" onClick="procesar002('PDF');" ><img src="../../../imagenes/icono_pdf.png" width="32" height="32"/></label>
				    		<label style="cursor:pointer" onClick="procesar002('EXC');" ><img src="../../../imagenes/icono_excel.png" width="32" height="32"/></label>
		  				</td>
	    			</tr>
	    		</table>
	    	</div>
	    	<?php 
				}
				if($tipo==3){
	    	?>
	    	<div id="tipo1" style="display:block">
				<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
	    			<tr>
	   	 				<th colspan="4"><strong>Parametros de Configuracion Reporte</strong></th>
	    			</tr>
	    			<tr><td>&nbsp;</td></tr>
	    			<tr>
					    <td style="text-align:center">
					      <p>
					        Nit:
						   <input type="text" name="txtNit" id="txtNit" required/>
					      </p>
					    </td>
				    </tr>
				    <tr><td>&nbsp;</td></tr>
	    			<tr>
					    <td style="text-align:center">
					      <p>
					        Fecha Inicial:
						   <input type="text" name="txtFechaIni" id="txtFechaIni" readonly required/>
						    &nbsp;&nbsp;
						    Fecha Final:
						    <input type="text" name="txtFechaFin" id="txtFechaFin" readonly  required/> 
						  </p>
					    </td>
				    </tr>	
				    <tr><td>&nbsp;</td></tr>
	    			<tr>
	      				<td colspan="4" style="text-align:center" id="tdProcesar">
	      					<img width="140" onclick="_procesar003();" style="cursor: pointer;" height="24" src="../../../imagenes/procesar2.png">
	      				</td>
	    			</tr>
	    			<tr><td colspan='2'>&nbsp;</td></tr>
	    			<tr>
	      				<td colspan="4" style="text-align:center" id="tdProcesar">
	      					<img  onclick="procesar003();" style="cursor: pointer;"  src="../../../imagenes/icono_excel.png">
	      				</td>
	    			</tr>
	    			<tr><td colspan='2'>&nbsp;</td></tr>
	    		</table>
	    	</div>
	    	<?php 
				}
				if($tipo==4){
			?>
			
			<div id="tipo1" style="display:block">
				<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
					<tr>
	   	 				<th colspan="4"><strong>Parametros de Configuracion Reporte</strong></th>
	    			</tr>
	    			<tr>
	      				<td style="text-align: center;">
	      					Rango Periodos Morosidad
	      				</td>
	      			</tr>
	      			<tr>
	      				<td style="text-align: center;">
	      					<input type="text" id="txtPeriodo" name="txtPeriodo" readonly="readonly" class="box1"/>
	      					<span class="clsObligatorio">&nbsp;&nbsp;&nbsp;</span>
	      					-
	      					<input type="text" id="txtPeriodo2" name="txtPeriodo2" readonly="readonly" class="box1"/>
	      					<span class="clsObligatorio">&nbsp;&nbsp;&nbsp;</span>
	      				</td>
	    			</tr>
	    			<tr><td>&nbsp;</td></tr>
	    			<tr>
	      				<td colspan="4" style="text-align:center" id="tdProcesar">
	      					<img width="140" onclick="procesar004();" style="cursor: pointer;" height="24" src="../../../imagenes/procesar2.png">
	      				</td>
	    			</tr>
	    		</table>
	    	</div>
			
			<?php 
				}
				if($tipo==5){
	    	?>
	    	
	    	<div id="tipo1" style="display:block">
				<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
					<tr>
	   	 				<th colspan="4"><strong>Parametros de Configuracion Reporte</strong></th>
	    			</tr>
	    			<tr>
	      				<td style="text-align: center;">
	      					Rango Periodos Morosidad
	      				</td>
	      			</tr>
	    			<tr>
	      				<td style="text-align: center;">
	      					<input type="text" id="txtPeriodo" name="txtPeriodo" readonly="readonly" class="box1">
	      					<span class="clsObligatorio">&nbsp;&nbsp;&nbsp;</span>
	      					-
	      					<input type="text" id="txtPeriodo2" name="txtPeriodo2" readonly="readonly" class="box1">
	      					<span class="clsObligatorio">&nbsp;&nbsp;&nbsp;</span>
	      				</td>
	    			</tr>
	    			<tr><td>&nbsp;</td></tr>
	    			<tr>
	      				<td colspan="4" style="text-align:center" id="tdProcesar">
	      					<img width="140" onclick="procesar005();" style="cursor: pointer;" height="24" src="../../../imagenes/procesar2.png">
	      				</td>
	    			</tr>
	    		</table>
	    	</div>
	    	<?php 
				}
				if($tipo==6){
			?>
	    	
	    	<div id="tipo1" style="display:block">
				<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
					<tr>
	   	 				<th colspan="4"><strong>Parametros de Configuracion Reporte</strong></th>
	    			</tr>
	    			<tr>
	      				<td style="text-align: center;">
	      					Fecha Inicio
	      					<input type="text" id="txtFechaI" name="txtFechaI" readonly>
      						Fecha Final
	      					<input type="text" id="txtFechaF" name="txtFechaF" readonly></td>
	    			</tr>
	   				<tr>
	      				<td colspan="4">&nbsp;</td>
	  				</tr>
	    			<tr>
	      				<td style="text-align:center" colspan="2" >
							<!-- <label style="cursor:pointer" onClick="procesar006(1);" ><img src="../../../imagenes/icono_pdf.png" width="32" height="32"/></label> -->
				    		<label style="cursor:pointer" onClick="procesar006(2);" ><img src="../../../imagenes/icono_excel.png" width="32" height="32"/></label>
		  				</td>
	    			</tr>
	    		</table>
	    	</div>
	    	
	    	<?php 
				}
				if($tipo==7){
			?>
	    	
	    	<div id="tipo1" style="display:block">
				<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
					<tr>
	   	 				<th colspan="4"><strong>Parametros de Configuracion Reporte</strong></th>
	    			</tr>
	    			<tr>
	    				<td width="20%" style="text-align: right;">Consultar:</td>
	    				<td>
	    					<select name="cmbTipoFiltroTraslado" id="cmbTipoFiltroTraslado" class="box1">
	    						<option value="FechaModificacion" selected="selected">Por fecha traslado</option>
	    						<option value="Trabajador">Por trabajador</option>
	    						<option value="Planilla">Por No. planilla</option>
	    						<option value="Todos">Todos los traslados</option>
	    					</select>
	    				</td>
	    			</tr>
	    			<tr id="trFechaModificacion">
	    				<td style="text-align: right;">
	    					Fecha modificaci&oacute;n:
	    				</td>
	      				<td>	
	      					<input type="text" id="txtFechaI" name="txtFechaI" readonly class="boxfecha">
      						-
	      					<input type="text" id="txtFechaF" name="txtFechaF" readonly class="boxfecha"></td>
	    			</tr>
	    			<tr id="trTrabajador" style="display:none;">
	    				<td style="text-align: right;">
	    					Trabajador:
	    				</td>
	    				<td>
	    					<select name="cmbTipoIdentificacion" id="cmbTipoIdentificacion" class="box1">
				    			<option value="1">CEDULA DE CIUDADANIA</option>
				    			<option value="4">CEDULA DE EXTRANJERIA</option>
				    		</select>&nbsp;
	    					<input type="text" name="txtIdentificacion" id="txtIdentificacion" class="box1"/>
	    					<input type="hidden" name="hidIdTrabajador" id="hidIdTrabajador"/>
	    					<label id="lblTrabajador"></label>
	    				</td>
	    			</tr>
	    			<tr id="trPlanilla" style="display:none;">
	    				<td style="text-align: right;">
	    					Planilla:
	    				</td>
	    				<td><input type="text" name="txtPlanilla" id="txtPlanilla" class="box1"/></td>
	    			</tr>
	   				<tr>
	      				<td colspan="4">&nbsp;</td>
	  				</tr>
	    			<tr>
	      				<td style="text-align:center" colspan="2" >
							<label style="cursor:pointer" onClick="procesar007(1);" ><img src="../../../imagenes/icono_pdf.png" width="32" height="32"/></label>
				    		<label style="cursor:pointer" onClick="procesar007(2);" ><img src="../../../imagenes/icono_excel.png" width="32" height="32"/></label>
		  				</td>
	    			</tr>
	    		</table>
	    	</div>
	    	
	    	<?php 
				}
				if($tipo==8){
			?>
	    	
	    	<div id="tipo1" style="display:block">
				<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
					<tr>
	   	 				<th colspan="4"><strong>Parametros de Configuracion Reporte</strong></th>
	    			</tr>
	    			<tr>
	    				<td width="20%" style="text-align: right;">Consultar:</td>
	    				<td>
	    					<select name="cmbTipoTrasladoAporte" id="cmbTipoTrasladoAporte" class="box1">
	    						<option value="Empresa" selected="selected">Por empresa actual</option>
	    						<option value="Recibo">Por numero recibo</option>
	    						<option value="Planilla">Por No. planilla</option>
	    						<option value="Periodo">Por periodo actual</option>
	    						<option value="FechaModificacion">Por fecha traslado</option>
	    						<option value="Todos">Todos los traslados</option>
	    					</select>
	    				</td>
	    			</tr>
	    			<tr id="trEmpresa">
	    				<td style="text-align: right;">
	    					Nit:
	    				</td>
	      				<td>	
	      					<input type="text" id="txtNit" name="txtNit" class="box1">
	      					<input type="hidden" id="hidIdEmpresa" name="hidIdEmpresa" class="box1">
	      					&nbsp;
	      					<label id="lblRazonSocial"></label>
	      				</td>
	    			</tr>
	    			<tr id="trRecibo" style="display:none;">
	    				<td style="text-align: right;">
	    					N&uacute;mero Recibo:
	    				</td>
	      				<td>	
	      					<input type="text" id="txtNumeroRecibo" name="txtNumeroRecibo" class="box1">
	      				</td>
	    			</tr>
	    			<tr id="trPlanilla" style="display:none;">
	    				<td style="text-align: right;">
	    					Planilla:
	    				</td>
	    				<td><input type="text" name="txtPlanilla" id="txtPlanilla" class="box1"/></td>
	    			</tr>
	    			<tr id="trPeriodo" style="display:none;">
	    				<td style="text-align: right;">
	    					Periodo:
	    				</td>
	    				<td><input type="text" name="txtPeriodo" id="txtPeriodo" class="box1" readonly="readonly"/></td>
	    			</tr>
	    			<tr id="trFechaModificacion" style="display: none;">
	    				<td style="text-align: right;">
	    					Fecha modificaci&oacute;n:
	    				</td>
	      				<td>	
	      					<input type="text" id="txtFechaI" name="txtFechaI" readonly class="boxfecha">
      						-
	      					<input type="text" id="txtFechaF" name="txtFechaF" readonly class="boxfecha"></td>
	    			</tr>
	   				<tr>
	      				<td colspan="4">&nbsp;</td>
	  				</tr>
	    			<tr>
	      				<td style="text-align:center" colspan="2" >
							<label style="cursor:pointer" onClick="procesar008(1);" ><img src="../../../imagenes/icono_pdf.png" width="32" height="32"/></label>
				    		<label style="cursor:pointer" onClick="procesar008(2);" ><img src="../../../imagenes/icono_excel.png" width="32" height="32"/></label>
		  				</td>
	    			</tr>
	    		</table>
	    	</div>
	    	
	    	<?php 
				}
				if($tipo==9){
	    	?>
	    	
	    	<div id="tipo1" style="display:block">
				<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
					<tr>
	   	 				<th colspan="4"><strong>Parametros de Configuracion Reporte</strong></th>
	    			</tr>
	    			<tr>
	      				<td style="text-align: center;">
	      					Fecha Inicial
	      					<input type="text" id="txtFechaI" name="txtFechaI" readonly class="boxfecha">
      						Fecha Final
	      					<input type="text" id="txtFechaF" name="txtFechaF" readonly class="boxfecha">
      					</td>
	    			</tr>
	   				<tr>
	      				<td colspan="4">&nbsp;</td>
	  				</tr>
	    			<tr>
	      				<td colspan="4" style="text-align:center" id="tdProcesar">
	      					<label style="cursor:pointer" onClick="procesar009(1);" ><img src="../../../imagenes/icono_excel.png" width="32" height="32"/></label>
	      				</td>
	    			</tr>
	    		</table>
	    	</div>
	    	<?php 
				}
				
				if($tipo==10){
	    	?>
	    	
	    	
	    	<div id="tipo1" style="display:block">
				<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
					<tr>
	   	 				<th colspan="4"><strong>Parametros de Configuracion Reporte</strong></th>
	    			</tr>
	    			<tr>
	      				<td style="text-align: center;">
	      					Fecha Inicial
	      					<input type="text" id="txtFechaI" name="txtFechaI" readonly class="boxfecha">
      						Fecha Final
	      					<input type="text" id="txtFechaF" name="txtFechaF" readonly class="boxfecha">
      					</td>
	    			</tr>
	    			<tr>
	    				<td style="text-align: center;">
	    				Tipo
	    					<select id="cmbTipoInfor" name="cmbTipoInfor" class="box1">
				      			<option value="C">Consolidado</option>
					          	<option value="D">Discriminado</option>
					      	</select>
	    				</td>
	    			</tr>
	    			
	   				<tr>
	      				<td colspan="4">&nbsp;</td>
	  				</tr>
	    			<tr>
	      				<td colspan="4" style="text-align:center" id="tdProcesar">
	      					<label style="cursor:pointer" onClick="procesar010(1);" ><img src="../../../imagenes/icono_pdf.png" width="32" height="32"/></label>
	      					<label style="cursor:pointer" onClick="procesar010(2);" ><img src="../../../imagenes/icono_excel.png" width="32" height="32"/></label>
	      				</td>
	    			</tr>
	    		</table>
	    	</div>
	    	<?php 
				}
				if($tipo==11){
			?>
	    	
	    	<div id="tipo1" style="display:block">
				<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
					<tr>
	   	 				<th colspan="4"><strong>Parametros de Configuracion Reporte</strong></th>
	    			</tr>
	    			<tr>
	      				<td style="text-align: center;">
	      					Nit:
	      					<input type="text" name="txtNitMora" id="txtNitMora" class="box1"/>
	      				</td>
	      			</tr>
	      			<tr>
	      				<td style="text-align: center;">
	      					Periodo Inicial:
	      					<input type="text" name="txtPeriodo" id="txtPeriodo" class="box1"/>
	      					Periodo Final:
	      					<input type="text" name="txtPeriodo2" id="txtPeriodo2" class="box1"/>
	      				</td>
	    			</tr>
	   				<tr>
	      				<td colspan="4">&nbsp;</td>
	  				</tr>
	    			<tr>
	      				<td style="text-align:center" colspan="2" >
							<label style="cursor:pointer" onClick="procesar011('PDF');" ><img src="../../../imagenes/icono_pdf.png" width="32" height="32"/></label>
			    			<label style="cursor:pointer" onClick="procesar011('EXC');" ><img src="../../../imagenes/icono_excel.png" width="32" height="32"/></label>
		  				</td>
	    			</tr>
	    		</table>
	    	</div>
	    	<?php 
				}
				if($tipo==12){
			?>
			
			
			<div id="tipo1" style="display:block">
				<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
					<tr>
	   	 				<th colspan="4"><strong>Parametros de Configuracion Reporte</strong></th>
	    			</tr>
	    			<tr>
	      				<td style="text-align: center;">
	      					Fecha Inicial
	      					<input type="text" id="txtFechaI" name="txtFechaI" readonly class="boxfecha">
      						Fecha Final
	      					<input type="text" id="txtFechaF" name="txtFechaF" readonly class="boxfecha">
      					</td>
	    			</tr>
	   				<tr>
	      				<td colspan="4">&nbsp;</td>
	  				</tr>
	    			<tr>
	      				<td colspan="4" style="text-align:center" id="tdProcesar">
	      					<label style="cursor:pointer" onClick="procesar012(2);" ><img src="../../../imagenes/icono_excel.png" width="32" height="32"/></label>
	      				</td>
	    			</tr>
	    		</table>
	    	</div>
	    	<?php 
				}
		
				if($tipo==15){
			?>
			
			<div id="tipo1" style="display:block">
				<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
					<tr>
	   	 				<th colspan="4"><strong>Parametros de Configuracion Reporte</strong></th>
	    			</tr>
	    			<tr><td>&nbsp;</td></tr>
	    			<tr>
	      				<td colspan="4" style="text-align:center" id="tdProcesar">
	      					<img width="140" onclick="procesar015();" style="cursor: pointer;" height="24" src="../../../imagenes/procesar2.png">
	      				</td>
	    			</tr>
	    		</table>
	    	</div>
			
			<?php 
				}
	    	?>
	    	
			<input type="hidden" id="txtusuario" name="txtusuario" value="<?php echo $usuario; ?>" /> 
			<input type="hidden" id="txtRept" name="txtRept" value="<?php echo $rept; ?>" />
		</center>
	</body>
</html>