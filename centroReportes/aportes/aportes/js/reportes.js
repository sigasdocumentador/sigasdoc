// funcion para obtener cookie en javascript
	function getCookie(c_name)
	{
		var i,x,y,ARRcookies=document.cookie.split(";");
		for (i=0;i<ARRcookies.length;i++)
		{
  			x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
  			y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
  			x=x.replace(/^\s+|\s+$/g,"");
  			if (x==c_name)
    			{
    				return unescape(y);
    			}// fin if
		}// fin for
  	}// fin getCookie
//funcion para obtener cookie en javascript

$(function(){
	
	/*****************
	 * -- EVENTOS -- *
	 *****************/
	$('#txtFechaI').datepicker({
		maxDate:"+0D",
		changeMonth: true,
		changeYear: true,
		onSelect: function(dateText, inst) {
			var lockDate = new Date($('#txtFechaI').datepicker('getDate'));
			$('input#txtFechaF').datepicker('option', 'minDate', lockDate);
		}
	});
	$("#txtFechaF").datepicker({
		maxDate:"+0D",
		changeMonth: true,
		changeYear: true
	});
	
	$("#txtPeriodo,#txtPeriodo2").datepicker({
        dateFormat: 'yymm',
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        onClose: function(dateText, inst) {
        	var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            $(this).datepicker('setDate', new Date(year, month, 1));
            }
        }); 

	$("#txtPeriodo,#txtPeriodo2").focus(function () {
		$(".ui-datepicker-calendar").hide();
		$("#ui-datepicker-div").position({
			my: "center top",
			at: "center bottom",
			of: $(this)
		});
	}); 
	
	$("#txtFechaIni").datepicker({
		maxDate:"+0D",
		changeMonth: true,
		changeYear: true,
		onSelect:function(date){
		$('#txtFechaFin').val(date);
		$('#txtFechaFin').datepicker("option","minDate",new Date(date));
		}
	});
	
	$("#txtFechaFin").datepicker({
		maxDate:"+0D",
		changeMonth: true,
		changeYear: true
	});
	
	/**Eventos para generar el reporte007*/
	/*Manipular los campos de acuerdo al tipo de filtro*/
	$("#cmbTipoFiltroTraslado").change(function(){
		limpiarCampos2("#trFechaModificacion input, #trTrabajador input, #trPlanilla input,#lblTrabajador");
		var tipoFiltro = $(this).val();
		if(tipoFiltro=="FechaModificacion") {
			$("#trFechaModificacion").show();
			$("#trTrabajador,#trPlanilla").hide();
		}else if(tipoFiltro=="Trabajador") {
			$("#trTrabajador").show();
			$("#trFechaModificacion,#trPlanilla").hide();
		}else if(tipoFiltro=="Planilla") {
			$("#trPlanilla").show();
			$("#trFechaModificacion,#trTrabajador").hide();
		}else {
			$("#trPlanilla,#trFechaModificacion,#trTrabajador").hide();
		}
	});
	/*Buscar los datos del trabajador*/
	$("#cmbTipoIdentificacion").change(function(){
		limpiarCampos2("#txtIdentificacion,#hidIdTrabajador,#lblTrabajador");
	});
	$("#txtIdentificacion").blur(function(){
		var tipoIdentificacion = $("#cmbTipoIdentificacion").val();
		var identificacion = $(this).val().trim();
		limpiarCampos2("#lblTrabajador,#hidIdTrabajador");
		if(identificacion==0)return false;
		
		$.ajax({
			url:URL+"phpComunes/buscarPersona2.php",	
			type:"POST",
			data:{v0:tipoIdentificacion,v1:identificacion},
			dataType:"json",
			async:false,
			success:function(datos){
				if(typeof datos != "object") {
					$("#lblTrabajador").html("<font color='red'>El trabajador no existe en la base de datos.</font>");
					return false;
				}
				datos = datos[0];
				$("#hidIdTrabajador").val(datos.idpersona);
				$("#lblTrabajador").html(datos.pnombre+" "+datos.snombre+" "+datos.papellido+" "+datos.sapellido);
			}
		});
	});
	/**Eventos para generar el reporte008*/
	/*Manipular los campos de acuerdo al tipo de filtro*/
	$("#cmbTipoTrasladoAporte").change(function(){
		limpiarCampos2("#trEmpresa input, #trRecibo input, #trPlanilla input," +
				" #trPeriodo input, #lblRazonSocial, #trFechaModificacion input");
		var tipoFiltro = $(this).val();
		if(tipoFiltro=="Empresa") {
			$("#trEmpresa").show();
			$("#trRecibo,#trPlanilla,#trPeriodo,#trFechaModificacion").hide();
		}else if(tipoFiltro=="Recibo") {
			$("#trRecibo").show();
			$("#trEmpresa,#trPlanilla,#trPeriodo,#trFechaModificacion").hide();
		}else if(tipoFiltro=="Planilla") {
			$("#trPlanilla").show();
			$("#trEmpresa,#trRecibo,#trPeriodo,#trFechaModificacion").hide();
		}else if(tipoFiltro=="Periodo") {
			$("#trPeriodo").show();
			$("#trEmpresa,#trRecibo,#trPlanilla,#trFechaModificacion").hide();
		}else if(tipoFiltro=="FechaModificacion") {
			$("#trFechaModificacion").show();
			$("#trEmpresa,#trRecibo,#trPlanilla,,#trPeriodo").hide();
		}else {
			$("#trEmpresa,#trRecibo,#trPlanilla,#trPeriodo,#trFechaModificacion").hide();
		}
	});
	/*Buscar los datos de la empresa*/
	$("#txtNit").blur(function(){
		var nit = $(this).val().trim();
		limpiarCampos2("#lblRazonSocial,#hidIdEmpresa");
		if(nit==0)return false;
		
		$.ajax({
			url:URL+"phpComunes/buscarEmpresaNit.php",	
			type:"POST",
			data:{v0:nit},
			dataType:"json",
			async:false,
			success:function(datos){
				if(typeof datos != "object") {
					$("#lblRazonSocial").html("<font color='red'>La empresa no existe en la base de datos.</font>");
					return false;
				}
				datos = datos[0];
				$("#hidIdEmpresa").val(datos.idempresa);
				$("#lblRazonSocial").html(datos.razonsocial);
			}
		});
	});
})

function procesar001(){
	var fechaI = $("#txtFechaI").val();
	var fechaF = $("#txtFechaF").val();
	var instituto = $("#cmbInstituto").val();
	
	if(fechaI=="" && fechaF==""){
		alert("Seleccione las dos fechas");
		return false;
	}
	
	$.ajax({
		url: "generarPlano.php",
		async: false,
		data: {fechaI:fechaI,fechaF:fechaF,instituto:instituto},
		beforeSend: function(objeto){
			dialogLoading('show');
		},
		complete: function(objeto, exito){
			dialogLoading('close');
			if(exito != "success"){
				alert("No se completo el proceso, informe a T.I.");
			}
		},
		contentType: "application/x-www-form-urlencoded",
		dataType: "json",
		error: function(objeto, quepaso, otroobj){
			alert("Error pas� lo siguiente: " + otroobj + ", informe a T.I.");
		},
		global: true,
		ifModified: false,
		processData:true,
		success: function( datos ) {
			if( datos == 1 ) {
				$("#tdProcesar").html("<br><label style='cursor:pointer' onClick='descargar();'><img src='../../../imagenes/descargarArchivo.png' width=143 height=30 /></label>");
			}
		},
		type: "POST"
	});
}

function procesar002(op){
	var fechaI = $("#txtFechaI").val();
	var fechaF = $("#txtFechaF").val();
	var instituto = $("#cmbInstituto").val();
	
	if(fechaI=="" && fechaF==""){
		alert("Seleccione las dos fechas");
		return false;
	}
	
	var nombreInstitucion = ( instituto == "icbf" ) ?
									"INSTO. COL. DE BIENESTAR FLAR" :
										( instituto == "sena" ) ? 
												"SERVICIO NAL. DE APRENDIZAJE" : "" ;
	
	
	var data = "v0=" + fechaI + "&v1=" + fechaF + "&v2=" + instituto + "&v3=" + nombreInstitucion;
	if(op=="PDF"){
		url="http://"+getCookie("URL_Reportes")+"/aportes/aportes/reporte014.jsp?"+data;
		
	}else if(op=="EXC"){
		url="http://"+getCookie("URL_Reportes")+"/aportes/aportes/rptExcel014.jsp?"+data;
	}
	window.open(url,"_NEW");
}

function _procesar003(){
	
	$.ajax({
	url: "generarPlanoUbicacion.php",
	async: false,
	data: {},
	beforeSend: function(objeto){
		dialogLoading('show');
	},
	complete: function(objeto, exito){
		dialogLoading('close');
		if(exito != "success"){
			alert("No se completo el proceso, informe a T.I.");
		}
	},
	contentType: "application/x-www-form-urlencoded",
	dataType: "json",
	error: function(objeto, quepaso, otroobj){
		alert("Error pas� lo siguiente: " + otroobj + ", informe a T.I.");
	},
	global: true,
	ifModified: false,
	processData:true,
	success: function( datos ) {
		if( datos == 1 ) {
			$("#tdProcesar").html("<label style='cursor:pointer' onClick='descargar();'><img src='../../../imagenes/descargarArchivo.png' width=143 height=30 /></label>");
		}
	},
	type: "POST"
    });
	
}

function procesar003(){
	var fechaIni= $("#txtFechaIni").val();
	var fechaFin= $("#txtFechaFin").val();
	var nit=$("#txtNit").val();
	if (fechaIni==""  &&  nit=="" )
	{
		alert("Debe indicar diligencciar la fecha incial y final o el Nit de la empresa");
		return false;
	}
	
	var url=URL+'aportes/empresas/reporteLogEmpresas.php?fechaIni='+fechaIni+'&fechaFin='+fechaFin+'&nit='+nit;
	window.open(url);

}

function procesar004(){
	var periodoIni = $("#txtPeriodo").val();
	var periodoFin = $("#txtPeriodo2").val();
	
	//Validacion
	$(".ui-state-error").removeClass("ui-state-error").addClass("box1");
	if(!periodoIni){$("#txtPeriodo").addClass("ui-state-error").removeClass("box1");return false;}
	if(!periodoFin){$("#txtPeriodo2").addClass("ui-state-error").removeClass("box1");return false;}
	
	$.ajax({
		url: "generarPlanoConsolidado.php",
		async: false,
		data: {periodoIni:periodoIni,periodoFin:periodoFin},
		beforeSend: function(objeto){
			dialogLoading('show');
		},
		complete: function(objeto, exito){
			dialogLoading('close');
			if(exito != "success"){
				alert("No se completo el proceso, informe a T.I.");
			}
		},
		contentType: "application/x-www-form-urlencoded",
		dataType: "json",
		error: function(objeto, quepaso, otroobj){
			alert("Error pas� lo siguiente: " + otroobj + ", informe a T.I.");
		},
		global: true,
		ifModified: false,
		processData:true,
		success: function( datos ) {
			if( datos == 1 ) {
				$("#tdProcesar").html("<br><label style='cursor:pointer' onClick='descargar();'><img src='../../../imagenes/descargarArchivo.png' width=143 height=30 /></label>");
			}
		},
		type: "POST"
	});
}


function procesar005(){
	var periodoIni = $("#txtPeriodo").val();
	var periodoFin = $("#txtPeriodo2").val(); 
	
	//Validacion
	$(".ui-state-error").removeClass("ui-state-error").addClass("box1");
	if(!periodoIni){$("#txtPeriodo").addClass("ui-state-error").removeClass("box1");return false;}
	if(!periodoFin){$("#txtPeriodo2").addClass("ui-state-error").removeClass("box1");return false;}
	
	
	$.ajax({
		url: "generarPlanoDesagregado.php",
		async: false,
		data: {periodoIni:periodoIni,periodoFin:periodoFin},
		beforeSend: function(objeto){
			dialogLoading('show');
		},
		complete: function(objeto, exito){
			dialogLoading('close');
			if(exito != "success"){
				alert("No se completo el proceso, informe a T.I.");
			}
		},
		contentType: "application/x-www-form-urlencoded",
		dataType: "json",
		error: function(objeto, quepaso, otroobj){
			alert("Error pas� lo siguiente: " + otroobj + ", informe a T.I.");
		},
		global: true,
		ifModified: false,
		processData:true,
		success: function( datos ) {
			if( datos == 1 ) {
				$("#tdProcesar").html("<br><label style='cursor:pointer' onClick='descargar();'><img src='../../../imagenes/descargarArchivo.png' width=143 height=30 /></label>");
			}
		},
		type: "POST"
	});
}

function procesar006(op){
	var fechaI = $("#txtFechaI").val();
	var fechaF = $("#txtFechaF").val();
	var usuario = $("#txtusuario").val(); 
	
	if(fechaI=="" && fechaF==""){
		alert("Seleccione las dos fechas");
		return false;
	}
	
	var data = "v0=" + fechaI + "&v1=" + fechaF + "&v2=" + usuario;
	if(op==1){
		url="http://"+getCookie("URL_Reportes")+"/aportes/aportes/reporte015.jsp?"+data;
		
	}else{
		url="http://"+getCookie("URL_Reportes")+"/aportes/aportes/rptExcel015.jsp?"+data;
	}
	window.open(url,"_NEW");
}

function procesar007(op) {
	var tipoFiltro = $("#cmbTipoFiltroTraslado").val();
	var fechaI = $("#txtFechaI").val();
	var fechaF = $("#txtFechaF").val();
	var idTrabajador = $("#hidIdTrabajador").val().trim();
	var planilla = $("#txtPlanilla").val().trim();
	var usuario = $("#txtusuario").val(); 
	
	$(".ui-state-error").removeClass("ui-state-error");
	
	if(tipoFiltro=="FechaModificacion") {
		if(fechaI==0 || fechaF==0){
			$("#txtFechaI,#txtFechaF").addClass("ui-state-error");
			return false;
		}
	}else if(tipoFiltro=="Trabajador") {
		if(idTrabajador==0) {
			$("#txtIdentificacion,#cmbTipoIdentificacion").addClass("ui-state-error");
			return false;
		}
	}else if(tipoFiltro=="Planilla") {
		if(planilla==0) {
			$("#txtPlanilla").addClass("ui-state-error");
			return false;
		}
	}
	
	var data = "v0="+usuario+"&v1="+fechaI+"&v2="+fechaF+"&v3="+idTrabajador+"&v4="+planilla;
	if(op==1){
		url="http://"+getCookie("URL_Reportes")+"/aportes/planilla/reporte005.jsp?"+data;
		
	}else{
		url="http://"+getCookie("URL_Reportes")+"/aportes/planilla/rptExcel005.jsp?"+data;
	}
	window.open(url,"_NEW");
}

function procesar008(op) {
	var tipoFiltro = $("#cmbTipoTrasladoAporte").val();
	var idEmpresa = $("#hidIdEmpresa").val().trim();
	var numeroRecibo = $("#txtNumeroRecibo").val().trim();
	var planilla = $("#txtPlanilla").val().trim();
	var periodo = $("#txtPeriodo").val();
	var fechaI = $("#txtFechaI").val();
	var fechaF = $("#txtFechaF").val();
	var usuario = $("#txtusuario").val(); 
	
	$("input").removeClass("ui-state-error");
	
	if(tipoFiltro=="Empresa") {
		if(idEmpresa==0) {
			$("#txtNit").addClass("ui-state-error");
			return false;
		}
	}else if(tipoFiltro=="Recibo") {
		if(numeroRecibo==0) {
			$("#txtNumeroRecibo").addClass("ui-state-error");
			return false;
		}
	}else if(tipoFiltro=="Planilla") {
		if(planilla==0) {
			$("#txtPlanilla").addClass("ui-state-error");
			return false;
		}
	}else if(tipoFiltro=="Periodo") {
		if(periodo==0) {
			$("#txtPeriodo").addClass("ui-state-error");
			return false;
		}
	}else if(tipoFiltro=="FechaModificacion") {
		if(fechaI==0) {
			$("#txtFechaI").addClass("ui-state-error");
			return false;
		}
		if(fechaF==0) {
			$("#txtFechaF").addClass("ui-state-error");
			return false;
		}
	}
	
	var data = "v0="+usuario+"&v1="+periodo+"&v2="+idEmpresa+"&v3="+numeroRecibo+"&v4="+planilla
			+"&v5="+fechaI+"&v6="+fechaF;
	if(op==1){
		url="http://"+getCookie("URL_Reportes")+"/aportes/aportes/reporte016.jsp?"+data;
		
	}else{
		url="http://"+getCookie("URL_Reportes")+"/aportes/aportes/rptExcel016.jsp?"+data;
	}
	window.open(url,"_NEW");
}

function procesar009(op) {
	var fechaI = $("#txtFechaI").val();
	var fechaF = $("#txtFechaF").val();
	var usuario = $("#txtusuario").val(); 
	
	$("input").removeClass("ui-state-error");
	
	
		if(fechaI==0) {
			$("#txtFechaI").addClass("ui-state-error");
			return false;
		}
		if(fechaF==0) {
			$("#txtFechaF").addClass("ui-state-error");
			return false;
		}
	
	var data = "v0="+usuario+"&v1="+fechaI+"&v2="+fechaF;
	if(op==1){
		url="http://"+getCookie("URL_Reportes")+"/aportes/aportes/rptExcel017.jsp?"+data;
		
	}
	window.open(url,"_NEW");
}



function procesar010(op) {
	var fechaI = $("#txtFechaI").val();
	var fechaF = $("#txtFechaF").val();
	var usuario = $("#txtusuario").val(); 
	var tipo = $("#cmbTipoInfor").val(); 
	
	$("input").removeClass("ui-state-error");
	
	
		if(fechaI==0) {
			$("#txtFechaI").addClass("ui-state-error");
			return false;
		}
		if(fechaF==0) {
			$("#txtFechaF").addClass("ui-state-error");
			return false;
		}
	
	var data = "v0="+usuario+"&v1="+fechaI+"&v2="+fechaF;
	if (tipo=='D'){
			if(op==1){
				url="http://"+getCookie("URL_Reportes")+"/aportes/aportes/reporte018.jsp?"+data;
				
			}else
				{
				url="http://"+getCookie("URL_Reportes")+"/aportes/aportes/rptExcel018.jsp?"+data;
				}
			window.open(url,"_NEW");
	}else{
		if(op==1){
			url="http://"+getCookie("URL_Reportes")+"/aportes/aportes/reporte019.jsp?"+data;
			
		}else
			{
			url="http://"+getCookie("URL_Reportes")+"/aportes/aportes/rptExcel019.jsp?"+data;
			}
		window.open(url,"_NEW");
	}
}

function procesar011(op){
	var usuario = $("#txtusuario").val();
	var nit = $("#txtNitMora").val().trim();
	var periodoInicial = $("#txtPeriodo").val().trim();
	var periodoFinal = $("#txtPeriodo2").val().trim();
	
	var data =  "v0=" + usuario + "&v1=" + nit + "&v2=" + periodoInicial + "&v3=" + periodoFinal;
			
	if(op=="PDF"){
		url="http://"+getCookie("URL_Reportes")+"/aportes/aportes/reporte009.jsp?"+data;
		
	}else if(op=="EXC"){
		url="http://"+getCookie("URL_Reportes")+"/aportes/aportes/rptExcel009.jsp?"+data;
	}
	window.open(url,"_BLANK");
}


function procesar012(op) {
	var fechaI = $("#txtFechaI").val();
	var fechaF = $("#txtFechaF").val();
	var usuario = $("#txtusuario").val(); 
	
	$("input").removeClass("ui-state-error");
	
	
		if(fechaI==0) {
			$("#txtFechaI").addClass("ui-state-error");
			return false;
		}
		if(fechaF==0) {
			$("#txtFechaF").addClass("ui-state-error");
			return false;
		}
	
	var data = "v0="+usuario+"&v1="+fechaI+"&v2="+fechaF;
	
			if(op==2){
				url="http://"+getCookie("URL_Reportes")+"/aportes/aportes/rptExcel020.jsp?"+data;
				
			}
			window.open(url,"_NEW");
}

function procesar014(op){
	var usuario = $("#txtusuario").val();
	var anioIni = ($("#txtPeriodo").val()).substring(0,4);
	var anioFin = ($("#txtPeriodo2").val()).substring(0,4);	
    var mesIni = ($("#txtPeriodo").val()).substring(4,6);
	var mesFin = ($("#txtPeriodo2").val()).substring(4,6);	
	var txtNit = $("#txtNit").val();
	
	//FechaSistema
	var meses = new Array ("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
	var diasSemana = new Array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sabado");
	var f=new Date();	
	var fechaSistema = diasSemana[f.getDay()] + ", " + f.getDate() + " de " + meses[f.getMonth()] + " de " + f.getFullYear()
	var mesIniNombre='';
	var mesFinNombre='';
	
    switch (mesIni) {
    case '01':mesIniNombre='Enero';
	          mesFinNombre='Enero';
           break;
    case '02':mesIniNombre='Febrero';
	          mesFinNombre='Febrero';
           break;
    case '03':mesIniNombre='Marzo';
	          mesFinNombre='Marzo';
           break;
    case '04':mesIniNombre='Abril';
	          mesFinNombre='Abril';
           break;
    case '05':mesIniNombre='Mayo';
	          mesFinNombre='Mayo';
           break;
    case '06':mesIniNombre='Junio';
              mesFinNombre='Junio';
           break;
    case '07':mesIniNombre='Julio';
	          mesFinNombre='Julio';
           break;
    case '08':mesIniNombre='Agosto';
	          mesFinNombre='Agosto';
           break;
    case '09':mesIniNombre='Septiembre';
	          mesFinNombre='Septiembre';
           break;
    case '10':mesIniNombre='Octubre ';
	          mesFinNombre='Octubre ';
           break;
    case '11':mesIniNombre='Noviembre';
	          mesFinNombre='Noviembre';
           break;
    case '12':mesIniNombre='Diciembre';
	          mesFinNombre='Diciembre';
           break;
    } //fin del switch
	
	 switch (mesFin) {
    case '01':mesFinNombre='Enero';
           break;
    case '02':mesFinNombre='Febrero';
           break;
    case '03':mesFinNombre='Marzo';
           break;
    case '04':mesFinNombre='Abril';
           break;
    case '05':mesFinNombre='Mayo';
           break;
    case '06':mesFinNombre='Junio';
           break;
    case '07':mesFinNombre='Julio';
           break;
    case '08':mesFinNombre='Agosto';
           break;
    case '09':mesFinNombre='Septiembre';
           break;
    case '10':mesFinNombre='Octubre ';
           break;
    case '11':mesFinNombre='Noviembre';
           break;
    case '12':mesFinNombre='Diciembre';
           break;
    } //fin del switch

	
	//Validacion
	$(".ui-state-error").removeClass("ui-state-error");

	var data = "v0="+usuario+"&v1="+anioIni+"&v2="+mesIni+"&v3="+anioFin+"&v4="+mesFin+"&v5="+mesIniNombre+"&v6="+mesFinNombre+"&v7="+fechaSistema+"&v8="+txtNit;
	//var data = "v0="+usuario+"&v1="+fechaI+"&v2="+fechaF+"&v3="+idTrabajador+"&v4="+planilla;	
		//alert(anioIni);
	url="http://"+getCookie("URL_Reportes")+"/aportes/aportes/rptExcel025.jsp?"+data;
	
	window.open(url,"_NEW");
}


function procesar015(){
	
	$.ajax({
		url: "generarPlanoMinisterioProteccion.php",
		async: false,
		beforeSend: function(objeto){
			dialogLoading('show');
		},
		complete: function(objeto, exito){
			dialogLoading('close');
			if(exito != "success"){
				alert("No se completo el proceso, informe a T.I.");
			}
		},
		contentType: "application/x-www-form-urlencoded",
		dataType: "json",
		error: function(objeto, quepaso, otroobj){
			alert("Error pas� lo siguiente: " + otroobj + ", informe a T.I.");
		},
		global: true,
		ifModified: false,
		processData:true,
		success: function( datos ) {
			if( datos == 1 ) {
				$("#tdProcesar").html("<br><label style='cursor:pointer' onClick='descargar();'><img src='../../../imagenes/descargarArchivo.png' width=143 height=30 /></label>");
			}
		},
		type: "POST"
	});
}




function descargar(){
	var url=URL+'phpComunes/descargaPlanos.php';
	window.open(url);
}



