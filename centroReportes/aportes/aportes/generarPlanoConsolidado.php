<?php
	date_default_timezone_set('America/Bogota');
	set_time_limit(0);
	$raiz="";
	$ruta_generados = "";
	$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
	include_once  $root;
	include_once $raiz.'/config.php';
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
	
	$db = IFXDbManejador::conectarDB();
	if($db->conexionID==null){
		$cadena = $db->error;
		echo msg_error($cadena);
		exit();
	}
	
	//Parametros
	$periodoIni = $_REQUEST["periodoIni"];
	$periodoFin = $_REQUEST["periodoFin"];
	
	$path_plano =$ruta_generados.'aportes'.DIRECTORY_SEPARATOR.'empresas'.DIRECTORY_SEPARATOR;
	//echo $path_plano;

	$ano=date('Y');
	$mes=date('m');
	
	$sql = "--Nota: Hay empresas sin indice
		DECLARE @periodoI CHAR(6) = '$periodoIni', @periodoF CHAR(6) = '$periodoFin'
		SELECT 
				ADMINISTRADORA,NOMBREADIMINISTRADORA,TIPOCARTERA,ORIGENCARTERA,EDADCARTERA,NUMEROPERIODOSSINPAGO
				,sum(VALORCARTERA) AS VALORCARTERA
				 FROM
				(SELECT  'CCF32' as ADMINISTRADORA,'CAJA DE COMPENSACION' as NOMBREADIMINISTRADORA
								,'2' AS TIPOCARTERA,'1' AS ORIGENCARTERA,'3' AS EDADCARTERA,'4' AS NUMEROPERIODOSSINPAGO                 
								-- ##Valor Cartera
								   ,(select isnull(sum(a303.neto),'0') - isnull(sum(a303.abono),'0') from aportes302 a302 
											inner join aportes305 a305 on a305.idvisita=a302.idvisita
											inner join aportes303 a303 on a303.idliquidacion=a305.idliquidacion and a303.cancelado='N'
									where a302.idempresa=a48.idempresa) AS VALORCARTERA
				from aportes048 a48
				WHERE a48.estado='A'
								--and a48.idempresa='7196'
								AND
								0<(	SELECT count(DISTINCT a97.periodo)
									FROM aportes097 a97 
									WHERE a97.idempresa=a48.idempresa AND a97.pago='N' 
										AND a97.periodo BETWEEN @periodoI AND @periodoF
								)
				and (select isnull(sum(a303.neto),'0') - isnull(sum(a303.abono),'0') from aportes302 a302 
											inner join aportes305 a305 on a305.idvisita=a302.idvisita
											inner join aportes303 a303 on a303.idliquidacion=a305.idliquidacion and a303.cancelado='N'
									where a302.idempresa=a48.idempresa)>0
				) AS a
				GROUP BY  ADMINISTRADORA,NOMBREADIMINISTRADORA,TIPOCARTERA,ORIGENCARTERA,EDADCARTERA,NUMEROPERIODOSSINPAGO";	


	$archivo='CCF_32_'.$ano.'_'.$mes.'_consolidado.txt';
	$path_plano.='planos'.DIRECTORY_SEPARATOR.$archivo;
		
	$cadenaResultado="";
	
	$rs=$db->querySimple($sql);
	while (($row=$rs->fetch())==true){		
		
		$cadenaResultado = "{$row["ADMINISTRADORA"]}|{$row["NOMBREADIMINISTRADORA"]}|{$row["TIPOCARTERA"]}|{$row["ORIGENCARTERA"]}|{$row["EDADCARTERA"]}|{$row["NUMEROPERIODOSSINPAGO"]}|{$row["VALORCARTERA"]}.00"."\r\n";	
				
	}
	
	$fp=fopen($path_plano,'w');
	fwrite($fp, $cadenaResultado);
	fclose($fp);
	$_SESSION['ENLACE']=$path_plano;
	$_SESSION['ARCHIVO']=$archivo;
	echo 1;

?>