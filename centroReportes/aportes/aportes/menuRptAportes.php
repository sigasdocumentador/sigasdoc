<?php

	$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
	include_once  $root;
	include_once $raiz.DIRECTORY_SEPARATOR ."config.php";
	$usuario=$_SESSION['CEDULA'];
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<title>Men&uacute; Reportes Aportes</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link type="text/css" href="../../../css/Estilos.css" rel="stylesheet"/>
	</head>
	<body>
		<center>
			<table width="100%" border="0">
				<tr>	
					<td align="center" ><img src="../../../imagenes/logo_reporte.png"></td>
				</tr>
				<tr>
					<td align="center" >Listado de Reportes Aportes</td>
				</tr>
			</table>
			<br>
			<table width="60%" border="0" cellspacing="1" class="tablero">
				<tr>
					<th width="16%">Reporte</th>
					<th width="84%" colspan="2">Descripci&oacute;n</th>
				</tr>  
				<tr>
			   		<td scope="row" align="left">Reporte001</td>          
			   		<td align="left"><a href="configReporteEmpresa.php?tipo=1" target="_new">Plano aportes parafiscales</a></td>
		    	</tr>
		    	<tr>
			   		<td scope="row" align="left">Reporte002</td>             
			   		<td align="left"><a href="configReporteEmpresa.php?tipo=2" target="_new">Informe aportes parafiscales participaci&oacute;n</a></td>
		    	</tr>
		    	<tr>
			   		<td scope="row" align="left">Reporte003</td>             
			   		<td align="left"><a href="configReporteEmpresa.php?tipo=3" target="_new">Informe UGPP Ubicacion y Contacto</a></td>
		    	</tr>
		    	<tr>
			   		<td scope="row" align="left">Reporte004</td>             
			   		<td align="left"><a href="configReporteEmpresa.php?tipo=4" target="_new">Informe UGPP Consolidado</a></td>
		    	</tr>
		    	<tr>
			   		<td scope="row" align="left">Reporte005</td>             
			   		<td align="left"><a href="configReporteEmpresa.php?tipo=5" target="_new">Informe UGPP Desagregado</a></td>
		    	</tr>
		    	<tr>
			   		<td scope="row" align="left">Reporte006</td>             
			   		<td align="left"><a href="configReporteEmpresa.php?tipo=6" target="_new">Informe Novedades Ingreso y Retiro de Planilla Unica</a></td>
		    	</tr>
		    	<tr>
			   		<td scope="row" align="left">Reporte007</td>             
			   		<td align="left"><a href="configReporteEmpresa.php?tipo=7" target="_new">Informe de los traslados de Planilla Unica</a></td>
		    	</tr>
		    	<tr>
			   		<td scope="row" align="left">Reporte008</td>             
			   		<td align="left"><a href="configReporteEmpresa.php?tipo=8" target="_new">Informe de los traslados de aportes</a></td>
		    	</tr>
		    	<tr>
			   		<td scope="row" align="left">Reporte009</td>             
			   		<td align="left"><a href="configReporteEmpresa.php?tipo=9" target="_new">Informe Comparativo Aportes Parafiscales</a></td>
		    	</tr>
		    	<tr>
			   		<td scope="row" align="left">Reporte010</td>             
			   		<td align="left"><a href="configReporteEmpresa.php?tipo=10" target="_new">Informe Intereses Mora Planilla Unica</a></td>
		    	</tr>
		    	<tr>
			   		<td scope="row" align="left">Reporte011</td>             
			   		<td align="left"><a href="configReporteEmpresa.php?tipo=11" target="_new">Informe de empresas morosas por algunos trabajadores</a></td>
		    	</tr>
		    	<tr>
			   		<td scope="row" align="left">Reporte012</td>             
			   		<td align="left"><a href="configReporteEmpresa.php?tipo=12" target="_new">Informe Trabajadores sin Relacion PU</a></td>
		    	</tr>
		    	<tr>
			   		<td scope="row" align="left">Reporte013</td>             
			   		<td align="left"><a href="<?php echo $ruta_reportes;?>aportes/aportes/rptExcel021.jsp?v0=<?php echo $usuario; ?>" target="_new">Informe Empresas con mas de dos Periodos sin Cancelar</a></td>
		    	</tr>		    	
		    	<tr>
			   		<td scope="row" align="left">Reporte014</td>             
			   		<td align="left"><a href="configReporteComparaAportes.php?tipo=14" target="_new">Informe Comparativo Pago de Aportes Empresas</a></td>
		    	</tr>
		    	<tr>
			   		<td scope="row" align="left">Reporte015</td>             
			   		<td align="left"><a href="configReporteEmpresa.php?tipo=15" target="_new">Informe Ministerio de Proteccion Social</a></td>
		    	</tr>
			</table>
		</center>
	</body> 
</html>
