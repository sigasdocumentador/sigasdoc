<?php
	date_default_timezone_set('America/Bogota');
	set_time_limit(0);
	$raiz="";
	$ruta_generados = "";
	$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
	include_once  $root;
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
	
	$db = IFXDbManejador::conectarDB();
	if($db->conexionID==null){
		$cadena = $db->error;
		echo msg_error($cadena);
		exit();
	}
	
	$periodoIni = $_REQUEST["periodoIni"];
	$periodoFin = $_REQUEST["periodoFin"];
	
	include_once $raiz.'/config.php';
	$path_plano =$ruta_generados.'aportes'.DIRECTORY_SEPARATOR.'empresas'.DIRECTORY_SEPARATOR;

	$ano=date('Y');
	$mes=date('m');
	
	$sql = "DECLARE @periodoI CHAR(6) = '$periodoIni', @periodoF CHAR(6) = '$periodoFin'
			SELECT DISTINCT 'CCF32' as administradora,'CAJA DE COMPENSACION' as nombreadministradora,a48.razonsocial,a91.codigo AS tipo_documento,a48.nit,a48.digito				
				,'2' AS TipodeCartera,'1' AS OrigendeCartera
                -- ##Valor Cartera
                   ,(select isnull(sum(a303.neto),'0') - isnull(sum(a303.abono),'0') from aportes302 a302 
                            inner join aportes305 a305 on a305.idvisita=a302.idvisita
                            inner join aportes303 a303 on a303.idliquidacion=a305.idliquidacion and a303.cancelado='N'
                    where a302.idempresa=a48.idempresa) AS valor_cartera                 		
				-- ##Ultimo Periodo
				,(	SELECT substring(max(periodo),1,4) + '-' + substring(max(periodo),5,2) 
					FROM aportes011 a11 
					WHERE a11.idempresa=a48.idempresa
				) AS ultimo_periodo				
				-- ##Ultima fecha pago
				,convert(VARCHAR(20),
					(	SELECT max(a11.fechapago) 
						FROM aportes011 a11 
						WHERE a11.idempresa=a48.idempresa
					),120
				) AS ultima_fecha				
				-- ##Numero de periodos sin pago
               ,(select count(*) from aportes302 a302 
                            inner join aportes305 a305 on a305.idvisita=a302.idvisita
                            inner join aportes303 a303 on a303.idliquidacion=a305.idliquidacion and a303.cancelado='N'
                    where a302.idempresa=a48.idempresa) as numer_perio_sin_pago				
				-- ##Ultima Accion de cobro
				,isnull((select top 1 case                         
                    when (a72.id_carta_notificacion='4253' or a72.id_carta_notificacion='4254')
                        then '5'
                    when a72.id_carta_notificacion='4251'
                        then '4'
                    when (a72.id_carta_notificacion='4250' or a72.id_carta_notificacion='4252')
                        then '2' else '1' end as a                   
                      from aportes072 a72 
                      where a72.id_empresa=a48.idempresa
                      and a72.fecha_sistema=(select max(ap72.fecha_sistema) from aportes072 ap72 where ap72.id_notificacion=a72.id_notificacion)
				),'1') AS ultim_accio_cobro					
				-- ##Fecha ultima accion de cobro                
				,isnull(convert(VARCHAR(10),( select max(a72.fecha_sistema) 
                        from aportes072 a72 
                      where a72.id_empresa=a48.idempresa                     
                 ),120),(select max(a305.fechaliquidacion) from aportes302 a302 
                inner join aportes305 a305 on a305.idvisita=a302.idvisita
                inner join aportes303 a303 on a303.idliquidacion=a305.idliquidacion 
                where a302.idempresa=a48.idempresa) )AS fecha_ultim_accio_cobro                 
				, a48.estado
                ,'' as clasificacion_estado
                ,isnull((SELECT distinct 'S' AS CONVENIODEPAGO
                    FROM aportes306 a306
                            INNER JOIN aportes307 a307 ON a307.idconvenio=a306.idconvenio
                            INNER JOIN aportes308 a308 ON a308.iddetalleconvenio=a307.iddetalleconvenio
                            INNER JOIN aportes303 a303 ON a303.iddetalle=a308.idperiodoliquidacion
                            INNER JOIN aportes305 a305 ON a305.idliquidacion=a303.idliquidacion
                            INNER JOIN aportes302 a302 ON a302.idvisita=a305.idvisita            
                    WHERE a306.estado='A' and a302.idempresa=a48.idempresa 
                ),'N') AS convenio_cobro                
			FROM aportes048 a48 
				INNER JOIN aportes091 a91 ON a48.idtipodocumento=a91.iddetalledef
				LEFT JOIN aportes091 b91 ON b91.iddetalledef=a48.indicador
				LEFT JOIN aportes015 a15 ON a48.idrepresentante=a15.idpersona
			WHERE a48.estado='A'
                --and a48.idempresa='7196'
				AND
				0<(	SELECT count(DISTINCT a97.periodo)
			   		FROM aportes097 a97 
					WHERE a97.idempresa=a48.idempresa AND a97.pago='N' 
						AND a97.periodo BETWEEN @periodoI AND @periodoF
				)
and (select isnull(sum(a303.neto),'0') - isnull(sum(a303.abono),'0') from aportes302 a302 
                            inner join aportes305 a305 on a305.idvisita=a302.idvisita
                            inner join aportes303 a303 on a303.idliquidacion=a305.idliquidacion and a303.cancelado='N'
                    where a302.idempresa=a48.idempresa)>0";

	$cadena = "";
	
	$rs=$db->querySimple($sql);
	while (($row=$rs->fetch())==true){
		
		$cadena .= "{$row["administradora"]}|{$row["nombreadministradora"]}|{$row["razonsocial"]}|{$row["tipo_documento"]}|{$row["nit"]}|{$row["digito"]}|{$row["TipodeCartera"]}|{$row["OrigendeCartera"]}|{$row["valor_cartera"]}.00|{$row["ultimo_periodo"]}|{$row["ultima_fecha"]}|{$row["numer_perio_sin_pago"]}|{$row["ultim_accio_cobro"]}|{$row["fecha_ultim_accio_cobro"]}|{$row["estado"]}|{$row["clasificacion_estado"]}|{$row["convenio_cobro"]}"."\r\n";
	}
	
	//Nombre del archivo plano
	$archivo='CCF_32_'.$ano.'_'.$mes.'_desagregado.txt';
	$path_plano.='planos'.DIRECTORY_SEPARATOR.$archivo;
	
	$fp=fopen($path_plano,'w');
	fwrite($fp, $cadena);
	fclose($fp);
	$_SESSION['ENLACE']=$path_plano;
	$_SESSION['ARCHIVO']=$archivo;
	echo 1;

?>