<?php
/* autor:       orlando puentes
 * fecha:       28/08/2012
 * objetivo:    configuracion de los reportes de radicacion
 **********************************************************
 configuracion
 tipo 
 1. con tipo de radicacion
 2. solo fecha
 
 
*/

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$usuario=$_SESSION['USUARIO'];
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
$tipo=$_REQUEST['tipo'];
$titulo='...';
$rept=$_REQUEST['tit'];
switch($rept){
	case 1:$titulo='Reporte de Aportes Recaudados - Fecha'; break;
	case 2:$titulo='Gr&aacute;fico de Tiempo vr Tipo'; break;
	case 3:$titulo='Gr&aacute;fico Tiempo vr Usuario'; break;
	case 4:$titulo=''; break;
	case 5:$titulo=''; break;
	}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Men&uacute; Reportes Asopagos</title>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link type="text/css" rel="stylesheet" href="../../../css/css.1.8/jquery-ui-1.8.17.custom.css" /> 
<link type="text/css" href="../../../css/Estilos.css" rel="stylesheet"/>
 <link href="../../../css/marco.css" rel="stylesheet" type="text/css" />

 <script type="text/javascript" src="../../../js/1.6/jquery-1.6.2.min.js"></script>
 <script type="text/javascript" src="../../../js/1.6/jquery-ui-1.8.16.custom.min.js"></script> 
 <script type="text/javascript" src="../../../js/comunes.js"></script>
<script language="javascript" src="js/reporte.js"></script>
<script>
$(function(){
	$("#txtFechaI,#txtFechaF").datepicker({maxDate:"+0D",
		changeMonth: true,
		changeYear: true
	});
	
});
</script>
</head>
<body>
	<center>
	<table width="100%" border="0">
	<tr>
	<td align="center" ><img src="../../../imagenes/logo_reporte.png" width="362" height="70"></td>
	</tr>
	<tr>
	<td align="center" ><?php echo $titulo ?></td>
	</tr>
	</table>
	<br>
<?php
if($tipo==1){
	$sql="SELECT iddetalledef,detalledefinicion FROM aportes091 WHERE iddefinicion=6";
	$rs=$db->querySimple($sql);
?>
	<div id="tipo1" style="display:block">
<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
	<tr>
    <th colspan="4"><strong>Parametros de Configuracion Reporte</strong></th>
    </tr>
    <tr>
      <td width="25%">Agencia</td>
      <td width="25%">
      <select id="txtAgencia" name="txtAgencia" class="box1">
          <option value="01">Neiva</option>
          <option value="02">Garzon</option>
          <option value="03">Pitalito</option>
          <option value="04">La plata</option>
      </select>
      </td>
      <td width="25%">Tipo</td>
      <td width="25%">
      <select id="txtTipo" name="txtTipo" class="box1">
<?php
	  while($row=$rs->fetch()){
		  echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
	  }
?>
      </select>
      </td>
    </tr>
    <tr>
      <td>Fecha Inicio</td>
      <td><input type="text" id="txtFechaI" name="txtFechaI" readonly></td>
      <td>Fecha Final</td>
      <td><input type="text" id="txtFechaF" name="txtFechaF" readonly></td>
    </tr>
    <tr>
      <td colspan="4">&nbsp;</td>
  </tr>
    <tr>
      <td colspan="4" style="text-align:center"><label class="RojoGrande" style="cursor:pointer" onClick="procesar1();">Procesar</label></td>
    </tr>
    </table>
 <?php
}
if($tipo==2){
?>
    <div id="tipo2" style="display:block">
    <table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
	<tr>
    <th colspan="4"><strong>Parámetros de Configuracion Reporte</strong></th>
    </tr>
	<tr>
	  <td colspan="4" style="text-align:center">Fecha del informe: <input type="text" id="txtFechaI" name="txtFechaI" readonly></td>
	  </tr>
	<tr>
	  <td colspan="4">&nbsp;</td>
	  </tr>
	<tr>
	  <td colspan="4" style="text-align:center"><label class="RojoGrande" style="cursor:pointer" onClick="procesar2();">Procesar</label></td>
	  </tr>
    </table>

<?php
}
if($tipo==3){
?>
<div id="tipo3" style="display:block">
<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
	<tr>
    <th colspan="4"><strong>Parametros de Configuracion Reporte</strong></th>
    </tr>
    <tr>
      <td width="25%">&nbsp;</td>
      <td width="25%">&nbsp;</td>
      <td width="25%">&nbsp;</td>
      <td width="25%">&nbsp;</td>
    </tr>
    <tr>
      <td>Fecha Inicio</td>
      <td><input type="text" id="txtFechaI" name="txtFechaI" readonly></td>
      <td>Fecha Final</td>
      <td><input type="text" id="txtFechaF" name="txtFechaF" readonly></td>
    </tr>
    <tr>
      <td colspan="4">&nbsp;</td>
  </tr>
    <tr >
      <td style="text-align:center" colspan="4">
      <label style="cursor:pointer" onClick="procesar1(1);"><img src="../../../imagenes/icono_pdf.png" width="32" height="32"></label> &nbsp;&nbsp;
      <label style="cursor:pointer" onClick="procesar1(2);"><img src="../../../imagenes/icono_excel.png" width="32" height="32"></label></td>
    </tr>
    </table>
    </div>
<?php
}
?>

<input type="hidden" id="txtusuario" name="txtusuario" value="<?php echo $usuario; ?>" /> 
<input type="hidden" id="txtRept" name="txtRept" value="<?php echo $rept; ?>" />
</body>

</html>