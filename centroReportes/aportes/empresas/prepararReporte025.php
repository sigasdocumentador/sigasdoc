<?php 
	date_default_timezone_set('America/Bogota');
	set_time_limit(0);
	$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
	include_once  $root;
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
	
	$db = IFXDbManejador::conectarDB();
	if($db->conexionID==null){
		$cadena = $db->error;
		echo msg_error($cadena);
		exit();
	}
	
	$periodo =$_REQUEST["periodo"];
	$fechaI = $_REQUEST["fechaI"];
	$fechaF = $_REQUEST["fechaF"];
	$fechaAnterior = $_REQUEST["fechaAnterior"];
	$periodoAnterior = $_REQUEST["periodoAnterior"];
	
	$mes = intval( substr($periodo,4,2) );
	
	$retorno = array("recaudo"=>0, "totalNovedades"=>0, "provision"=>0, "totalRecaudo"=>0,
					 "aportesFacultativos"=>0, "aportesPensionados"=>0, "totalAportes"=>0,
					 "totalAcumulado"=>0, "acumulado"=>0, "bandera"=>0);
	
	$retorno["bandera"] = existenEmpresasPorcentaje($db, $fechaI,$fechaF);
	
	if( $mes == 1 ){
		//MAS PROVISION
		//SIN ACUMULADO
		$anno = intval( substr($periodo,0,4) )-1;
		$periodo12 = $anno."12"; 
		$retorno["recaudo"] = valorAporte($db,$fechaI,$fechaF);
		$retorno["totalNovedades"] =  valorNovedades($db,$fechaI,$fechaF);
		$retorno["provision"] = valorProvision($db,$periodo12);
		$retorno["totalRecaudo"] = $retorno["recaudo"] - $retorno["totalNovedades"] + $retorno["provision"];
		$retorno["aportesFacultativos"] = valorAportFacultativos($db,$periodoAnterior,$periodoAnterior);
		$retorno["aportesPensionados"] = valorAportPensionados($db,$periodoAnterior,$periodoAnterior);
		$retorno["totalAportes"] = $retorno["totalRecaudo"] - $retorno["aportesFacultativos"] - $retorno["aportesPensionados"];
		$retorno["totalAcumulado"] = $retorno["totalAportes"]; 
		
	}else if( $mes == 12 ){
		//MENOS PROVISION
		//CON ACUMULADO 
		
		$retorno["recaudo"] = valorAporte($db,$fechaI,$fechaF);
		$retorno["totalNovedades"] =  valorNovedades($db,$fechaI,$fechaF);
		$retorno["provision"] = valorProvision($db,$periodo);
		$retorno["totalRecaudo"] = $retorno["recaudo"] - $retorno["totalNovedades"] - $retorno["provision"];
		$retorno["aportesFacultativos"] = valorAportFacultativos($db,$periodoAnterior,$periodoAnterior);
		$retorno["aportesPensionados"] = valorAportPensionados($db,$periodoAnterior,$periodoAnterior);
		$retorno["totalAportes"] = $retorno["totalRecaudo"] - $retorno["aportesFacultativos"] - $retorno["aportesPensionados"];
		$retorno["acumulado"] = valorAcumulado($db,$fechaI,$fechaF,$fechaAnterior, $periodo, $periodoAnterior );
		$retorno["totalAcumulado"] = $retorno["totalAportes"] + $retorno["acumulado"];
		
	}else{
		//SIN PROVISION
		//CON ACUMULADO
		
		$retorno["recaudo"] = valorAporte($db,$fechaI,$fechaF);
		$retorno["totalNovedades"] =  valorNovedades($db,$fechaI,$fechaF);
		$retorno["totalRecaudo"] = $retorno["recaudo"] - $retorno["totalNovedades"];
		$retorno["aportesFacultativos"] = valorAportFacultativos($db,$periodoAnterior,$periodoAnterior);
		$retorno["aportesPensionados"] = valorAportPensionados($db,$periodoAnterior,$periodoAnterior);
		$retorno["totalAportes"] = $retorno["totalRecaudo"] - $retorno["aportesFacultativos"] - $retorno["aportesPensionados"];
		$retorno["acumulado"] = valorAcumulado($db, $fechaI,$fechaF,$fechaAnterior, $periodo, $periodoAnterior );
		$retorno["totalAcumulado"] = $retorno["totalAportes"] + $retorno["acumulado"];
	}
	
	$retorno["recaudo"] = number_format ( $retorno["recaudo"] , 0 , ',' , '.' );
	$retorno["totalNovedades"] = ($retorno["totalNovedades"]> 0 ? "-":"").number_format ( $retorno["totalNovedades"] , 0 , ',' , '.' );
	$retorno["provision"] = number_format ( $retorno["provision"] , 0 , ',' , '.' );
	$retorno["totalRecaudo"] = number_format ( $retorno["totalRecaudo"] , 0 , ',' , '.' );
	$retorno["aportesFacultativos"] = ($retorno["aportesFacultativos"]> 0 ? "-":"").number_format ( $retorno["aportesFacultativos"] , 0 , ',' , '.' );
	$retorno["aportesPensionados"] = ($retorno["aportesPensionados"]> 0 ? "-":"").number_format ( $retorno["aportesPensionados"] , 0 , ',' , '.' );
	$retorno["totalAportes"] = number_format ( $retorno["totalAportes"] , 0 , ',' , '.' );
	$retorno["acumulado"] = number_format ( $retorno["acumulado"] , 0 , ',' , '.' );
	$retorno["totalAcumulado"] = number_format ( $retorno["totalAcumulado"] , 0 , ',' , '.' );
	
	echo json_encode($retorno);
	
	function valorAcumulado( $db,$fechaI,$fechaF,$fechaAnterior,$periodo,$periodoAnterior ){
		$anno = substr($periodo,0,4);
		$periodo = ($anno-1) . "12";
		$fechaIEnero = $anno."0101";
 
		$recaudos = valorAporte($db,$fechaIEnero,$fechaAnterior);
		$valorNovedades =  valorNovedades($db,$fechaIEnero,$fechaAnterior);
		$valorProvision = valorProvision($db,$periodo );
		$totalRecaudos = $recaudos - $valorNovedades + $valorProvision;
		$valorAporFacultativo = valorAportFacultativos($db,$periodo,$periodoAnterior);
		$valorAportPensionados = valorAportPensionados($db,$periodo,$periodoAnterior);
		$totalAportes  = $totalRecaudos - $valorAporFacultativo - $valorAportPensionados;
		$totalAcumulado = $totalAportes;
		return $totalAcumulado;
	}
	
	function valorAporte($db,$fechaI,$fechaF){
		$sql = "SELECT isnull( sum(a11.valoraporte), 0) AS valoraporte
					FROM aportes011 a11
				WHERE a11.fechapago BETWEEN '$fechaI' AND '$fechaF'";
		
		$valor = $db->querySimple($sql)->fetchObject()->valoraporte;
		return $valor ? $valor : 0; 
	}
	
	function valorNovedades($db,$fechaI,$fechaF){
		$sql = "SELECT SUM( ISNULL( a58.aaportes, 0 ) - ISNULL( a58.daportes, 0 ) ) AS valornovedades
				    FROM aportes058 a58
				    INNER JOIN aportes011 a11 ON a11.idaporte=a58.idaporte
				    INNER JOIN aportes048 a48 ON a48.idempresa=a11.idempresa
				WHERE  a58.fechadevolucion BETWEEN '$fechaI' AND '$fechaF'";
	
		$valor = $db->querySimple($sql)->fetchObject()->valornovedades;
		return $valor ? $valor : 0;
	}
	
	function existenEmpresasPorcentaje($db,$fechaI,$fechaF){
		$sql = "SELECT count(a48.idempresa) AS contador
				    FROM aportes058 a58
				    INNER JOIN aportes011 a11 ON a11.idaporte=a58.idaporte
				    INNER JOIN aportes048 a48 ON a48.idempresa=a11.idempresa
				WHERE a58.fechadevolucion BETWEEN '$fechaI' AND '$fechaF'
				    AND CONVERT(int, a48.indicador ) IN(9,7,6)";
		$valor = $db->querySimple($sql)->fetchObject()->contador;
		return $valor > 0 ? true : false;
	}
	
	function valorProvision($db,$periodo12){
		$fechaI = $periodo12."01";
		$fechaF = $periodo12."31";
		$sql = "SELECT SUM( valorpagado ) AS valorprovisiones
				FROM aportes011 
				WHERE fechapago BETWEEN '$fechaI' AND '$fechaF' AND periodo='$periodo12'";
	
		$valor = $db->querySimple($sql)->fetchObject()->valorprovisiones;
		return $valor ? $valor : 0;
	}
	
	function valorAportFacultativos($db,$periodoI,$periodoF){
		$sql = "select campo36 from aportes399 WHERE periodo BETWEEN '$periodoI' AND '$periodoF'";
		$valor = $db->querySimple($sql)->fetchObject()->campo36;
		return $valor ? $valor : 0;
	}
	
	function valorAportPensionados($db,$periodoI,$periodoF){
		$sql = "select campo37 from aportes399 WHERE periodo BETWEEN '$periodoI' AND '$periodoF'";
		$valor = $db->querySimple($sql)->fetchObject()->campo37;
		return $valor ? $valor : 0;
	}

?>