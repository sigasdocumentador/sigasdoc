<?php
	/* autor:       orlando puentes
	 * fecha:       28/08/2012
	 * objetivo:    configuracion de los reportes de empresas
	 **********************************************************
	 configuracion
	 tipo 
	 1. con tipo 
	 2. solo fecha
	 3. agencias
	 
	*/
	date_default_timezone_set('America/Bogota');
	$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
	include_once  $root;
	
	$usuario=$_SESSION['USUARIO'];
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
	include_once $raiz. DIRECTORY_SEPARATOR .'clases'. DIRECTORY_SEPARATOR . 'p.definiciones.class.php';
	
	$db = IFXDbManejador::conectarDB();
	if($db->conexionID==null){
		$cadena = $db->error;
		echo msg_error($cadena);
		exit();
	}
	//Obtener Agencias
	$sql1="Select * from aportes500";
	$agencia=$db->querySimple($sql1);
	//obtener los datos del usuario
	$sql = "SELECT a15.sexo
			FROM aportes519 a519
				INNER JOIN aportes015 a15 ON a15.identificacion=a519.identificacion
			WHERE a519.usuario='$usuario'";
	$rsDatoUsuario = $db->querySimple($sql);
	$sexoUsuario = $rsDatoUsuario->fetchObject()->sexo;
	
	$Definiciones = new Definiciones();
	$rsTiposDoc = $Definiciones->mostrar_datos(1);
	
	$tipo=$_REQUEST['tipo'];
	$op=isset($_REQUEST['op'])?$_REQUEST['op']:"";
	$titulo='...';
	$rept=$_REQUEST['tit'];
	switch($rept){
		case 1:$titulo='Directorio de Empresas Activas'; break;
		case 2:$titulo=''; break;
		case 3:$titulo=''; break;
		case 4:$titulo=''; break;
		case 5:$titulo='Informe Aportes SENA e ICBF primeros diez dias'; break;
		case 6:$titulo='Informes aportes por empresa'; break;
		case 7:$titulo='Informes listado empresas nuevas'; break;
		case 8:$titulo='Informe Listado Afiliados y Beneficiarios por Empresa'; break;
		case 9:$titulo='Listado aportes por empresa'; break;
		case 11:$titulo='Listado de Afiliaciones de nuevas empresas'; break;
		case 12:$titulo='Informe Resumido de Aportes'; break;
		case 13:$titulo='Informe Pago Aportes de Empresas Nuevas'; break;
		case 14:$titulo='Listado de Afiliaciones y grupo familiar por empresa'; break;
		case 16:$titulo='Listado de Devoluciones realizadas por periodo'; break;
		case 17:$titulo='Carta aceptaci&oacute;n empresas nuevas'; break;
		case 19:$titulo='Listado Recaudos Parafiscales'; break;
		case 20:$titulo='Listado Aportes Parafiscales Fuera de Pila'; break;
		case 21:$titulo='Listado de Devoluciones realizadas'; break;
		case 22:$titulo='Informe UGPP Devoluciones'; break;
		case 23:$titulo='Recaudos de aportes parafiscales'; break;
		case 24:$titulo='Informe de empresas nuevas y su primer aporte'; break;
		case 25:$titulo='Informe del Subsidio Familiar y Aportes'; break;
		case 27:$titulo='Informe Distribuci&oacute;n de subsidios monetarios pagado'; break;
		case 28:$titulo='Informe comparativo recaudo de aportes 4%'; break;
		case 29:$titulo='Informe estado comparativo del subsidio y aportes.'; break;
		case 30:$titulo='Informe empresas inactivas.'; break;
		case 31:$titulo='Informe empresas suspendidas';break;
		case 32:$titulo='Informe empresas desafiliadas';break;
		case 33:$titulo='Carta aviso de incumplimiento';break;
		case 34:$titulo='Informe notificaciones de la empresa';break;
		case 35:$titulo='Carta notificaci&oacute;n de expulsi&oacute;n al Consejo Directivo.';break;
		case 36:$titulo='Carta notificaci&oacute;n de expulsi&oacute;n a la empresa';break;
		case 37:$titulo='Informe de las afiliaciones inactivas por la expulsi&oacute;n o activas por la re afiliaci&oacute;n de la empresa';break;
		case 39:$titulo='Carta de Afiliaciones Pendientes por PU';break;
		case 40:$titulo='Informe de Incosistencias empresas ley 1429';break;
		case 41:$titulo='Informe Renovaci&oacute;n Camara de Comercio - empresas ley 1429';break;
		case 45:$titulo='Informe historias de la modificaciones empresas Ley 1429';break;
		case 46:$titulo='Informe consolidado valor de aportes de las empresas ley 1429';break;
		case 47:$titulo='Cartas para las empresas ley 1429 con Incosistencias';break;
		case 48:$titulo='Listado Empresas que pagan con indice menos del 4%';break;
		case 49:$titulo='Listado Empresas y sus trabajadores con variaciones en d&iacute;as cotizados y aportes';break;
		case 54:$titulo='Listado de empresa con trabajadores con indice de aporte menor o igual al 4%';break;
	} 
	$fecha=date("m/d/Y");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<title>Men&uacute; Reportes Empresa</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		
		<link type="text/css" rel="stylesheet" href="../../../css/css.1.8/jquery-ui-1.8.17.custom.css" />
		<link type="text/css" href="../../../css/Estilos.css" rel="stylesheet"/>
		<link href="../../../css/marco.css" rel="stylesheet" type="text/css" />

		<script type="text/javascript" src="../../../js/1.6/jquery-1.6.2.min.js"></script>
		<script type="text/javascript" src="../../../js/1.6/jquery-ui-1.8.16.custom.min.js"></script>
		<script type="text/javascript" src="../../../js/comunes.js"></script>
		<script type="text/javascript" src="js/reportes.js"></script>
		<script src="../../../js/jquery.table2excel.min.js"></script>
	</head>
	<body>
		<center>
			<table width="100%" border="0">
				<tr>
					<td align="center" ><img src="../../../imagenes/logo_reporte.png" width="362" height="70"></td>
				</tr>
				<tr>
					<td align="center" ><?php echo $titulo ?></td>
				</tr>
			</table>
			<br>
			<?php
				if($tipo==1){
					$sql="SELECT iddetalledef,detalledefinicion FROM aportes091 WHERE iddefinicion=6";
					$rs=$db->querySimple($sql);
			?>
			<div id="tipo1" style="display:block">
				<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
					<tr>
	   	 				<th colspan="4"><strong>Parametros de Configuracion Reporte</strong></th>
	    			</tr>
	    			<tr>
			      		<td width="25%">Agencia</td>
				      	<td width="25%">
				      		<select id="txtAgencia" name="txtAgencia" class="box1">
					          	<option value="01">Neiva</option>
					          	<option value="02">Garzon</option>
					          	<option value="03">Pitalito</option>
					          	<option value="04">La plata</option>
					      	</select>
	      				</td>
	      				<td width="25%">Tipo</td>
	      				<td width="25%">
	      					<select id="txtTipo" name="txtTipo" class="box1">
							<?php
					  			while($row=$rs->fetch()){
							  		echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
							  	}
							?>
	      					</select>
	      				</td>
    				</tr>
	    			<tr>
	      				<td>Fecha Inicio</td>
	      				<td><input type="text" id="txtFechaI" name="txtFechaI" readonly></td>
      					<td>Fecha Final</td>
	      				<td><input type="text" id="txtFechaF" name="txtFechaF" readonly></td>
	    			</tr>
	   				<tr>
	      				<td colspan="4">&nbsp;</td>
	  				</tr>
	    			<tr>
	      				<td colspan="4" style="text-align:center"><label class="RojoGrande" style="cursor:pointer" onClick="procesar1();">Procesar</label></td>
	    			</tr>
	    		</table>
	    	</div>
	 		<?php
				}
				if($tipo==2){
			?>
    		<div id="tipo2" style="display:block">
	    		<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
					<tr>
	    				<th colspan="4"><strong>Parámetros de Configuracion Reporte</strong></th>
	    			</tr>
					<tr>
		  				<td colspan="4" style="text-align:center">Fecha del informe: <input type="text" id="txtFechaI" name="txtFechaI" readonly></td>
		  			</tr>
					<tr>
		  				<td colspan="4">&nbsp;</td>
		  			</tr>
					<tr>
		  				<td colspan="4" style="text-align:center"><label class="RojoGrande" style="cursor:pointer" onClick="procesar2();">Procesar</label></td>
		  			</tr>
	    		</table>
	    	</div>
			<?php
				}
				if($tipo==3){
			?>
    		<div id="tipo2" style="display:block">
	    		<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
					<tr>
	    				<th><strong>Parámetros de Configuracion Reporte</strong></th>
	    			</tr>
					<tr>
		  				<td style="text-align:center">Seccional:
		    				<select id="txtSeccional" name="txtSeccional" class="box1">
	          					<option value="0">Todas</option>
	          					<option value="01">Neiva</option>
	          					<option value="02">Garzon</option>
	          					<option value="03">Pitalito</option>
	          					<option value="04">La plata</option>
      						</select>
	        			</td>
		  			</tr>
					<tr>
		  				<td>&nbsp;</td>
		  			</tr>
					<tr>
		  				<td style="text-align:center">
	      					<label style="cursor:pointer" onClick="procesar3(1);"><img src="../../../imagenes/icono_pdf.png" width="32" height="32"></label> &nbsp;&nbsp;
	      					<label style="cursor:pointer" onClick="procesar3(2);"><img src="../../../imagenes/icono_excel.png" width="32" height="32"></label>
      					</td>
	  				</tr>
	    		</table>
	    	</div>
	    	<?php
				}
				if($tipo==200){
			?>
    		<div id="tipo31" style="display:block">
	    		<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
					<tr>
	    				<th><strong>Parámetros de Configuracion Reporte</strong></th>
	    			</tr>
					<tr>
		  				<td style="text-align:center">Seccional:
		    				<select id="txtSeccional" name="txtSeccional" class="box1">
	          					<option value="0">Todas</option>
	          					<option value="01">Neiva</option>
	          					<option value="02">Garzon</option>
	          					<option value="03">Pitalito</option>
	          					<option value="04">La plata</option>
      						</select>
	        			</td>
		  			</tr>
					<tr>
		  				<td>&nbsp;</td>
		  			</tr>
					<tr>
		  				<td style="text-align:center">
	      					<label style="cursor:pointer" onClick="procesar2(1);"><img src="../../../imagenes/icono_pdf.png" width="32" height="32"></label> &nbsp;&nbsp;
	      					<label style="cursor:pointer" onClick="procesar2(2);"><img src="../../../imagenes/icono_excel.png" width="32" height="32"></label>
      					</td>
	  				</tr>
	    		</table>
	    	</div>
			<?php
				}
				if($tipo==4){
			?>
    		<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
				<tr>
	    			<th colspan="2"><strong>Parámetros de Configuracion Reporte</strong></th>
	    		</tr>
				<tr>
	            	<td style="text-align:right;">
	            		Nit:
	            	</td> 
	            	<td>
	            		<input type="text" id="txtNit" name="txtNit">
	            	</td>
	            </tr>
	            <tr>	
	            	<td style="text-align:right;">					            		  
            	 		Estado:
            	 	</td>
            	 	<td>
	            		<select name="cmbEstado" id="cmbEstado">
	            			<option value="A" selected="selected" >A</option>
	            			<option value="P" >P</option>
	            			<option value="I" >I</option>
	            			<option value="T">Todos</option>
	            		</select>
	            	</td>
	  			</tr>
	  			<tr id="tdFechaIngreso">
	            	<td style="text-align:right;">
	            		Fecha ingreso:
	            	</td> 
	            	<td>
	            		<input type="text" id="txtFechaI" name="txtFechaI" readonly style="width: 100px" />
	            		<input type="text" id="txtFechaF" name="txtFechaF" readonly style="width: 100px" />
	            	</td>
	            </tr>
	            <tr id="tdFechaRetiro" style="display:none;">
	            	<td style="text-align:right;">
	            		Fecha retiro:
	            	</td> 
	            	<td>
	            		<input type="text" id="txtFechaRI" name="txtFechaRI" readonly style="width: 100px"  />
	            		<input type="text" id="txtFechaRF" name="txtFechaRF" readonly style="width: 100px" />
	            	</td>
	            </tr>
				<tr>
		  			<td colspan="2">&nbsp;</td>
		  		</tr>
				<tr>
		  			<td style="text-align:center" colspan="2">
	      				<label style="cursor:pointer" onClick="procesar7();"><img src="../../../imagenes/icono_excel.png" width="32" height="32"></label>
	      			</td>
		  		</tr>
	    	</table>
			<?php
				}
				if($tipo == 5){
			?>
		    <div id="tipo2" style="display:block">
	    		<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
					<tr>
	        			<th colspan="2"><strong>Par&aacute;metros de Configuracion Reporte</strong></th>
	        		</tr>
	        		<tr>	    
			  			<td style="text-align:right" width="50%">Numero Comunicacion Interna:</td>
			  			<td><input type="text" name="txtNumeroComun" id="txtNumeroComun" size="10"/></td>
			 		</tr>
		    		<tr>	    
			  			<td style="text-align:right">Fecha del Reporte:</td>
			  			<td><input type="text" name="txtFecha" id="txtFecha" readonly size="10"/></td>
					</tr>
					<tr>
	          			<td colspan="2">&nbsp;</td>
        			</tr>
					<tr>
			  			<td style="text-align:center" colspan="2">
		     				<label style="cursor:pointer" onClick="procesar5(1);"><img src="../../../imagenes/icono_pdf.png" width="32" height="32"></label> &nbsp;&nbsp;
		      			</td>
			  		</tr>
		    	</table>
		    </div>
			<?php 
				}
				if($tipo == 6){
			?>
    		<div id="tipo2" style="display:block">
	    		<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
					<tr>
			    		<th><strong><?php echo $titulo;?></strong></th>
		   			</tr>
		   			<tr>
			  			<td style="text-align:center">Nit:
			     			<input type="text" name="txtNit" id="txtNit" /> 
			        	</td>
			  		</tr>
					<tr>
				  		<td style="text-align:center">Fecha Inicial:
			     			<input type="text" name="txtFechaI" id="txtFechaI" readonly/> 
			        	</td>
			  		</tr>
			  		<tr>
				  		<td style="text-align:center">Fecha Final:
			     			<input type="text" name="txtFechaF" id="txtFechaF" readonly/> 
			        	</td>
			  		</tr>
					<tr>
			  			<td>&nbsp;</td>
			  		</tr>
					<tr>
			  			<td style="text-align:center">
			      			<label style="cursor:pointer" onClick="procesar6();"><img src="../../../imagenes/icono_pdf.png" width="32" height="32"></label> &nbsp;&nbsp;
			      		</td>
			  		</tr>	
	    		</table>
	    	</div>
			<?php 
				}
				if($tipo == 7){
			?>
    		<div id="tipo2" style="display:block">
	    		<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
					<tr>
			    		<th><strong><?php echo $titulo;?></strong></th>
			    	</tr>
			    	<tr>
			  			<td style="text-align:center">Agencia:
			  				<select name="cmbAgencia" id="cmbAgencia">
			  					<option value="T">Todas</option>
				     			<?php 
				     				$sql = "select * from aportes500";
				     				$rs = $db->querySimple($sql);
				     				while($row=$rs->fetchObject()){
				     					echo "<option value='$row->codigo'>$row->agencia</option>";
				     				}
				     			?>			     		
			     			</select>
			        	</td>
			  		</tr>
					<tr>
				  		<td style="text-align:center">Fecha Inicial:
			     			<input type="text" name="txtFechaI" id="txtFechaI" readonly/>Fecha Final: <input type="text" name="txtFechaF" id="txtFechaF" readonly/> 
			        	</td>
			  		</tr>
					<tr>
			  			<td>&nbsp;</td>
			  		</tr>
					<tr>
			  			<td style="text-align:center">
		 				<?php if($rept == 7){ ?>
			     			<label style="cursor:pointer" onClick="procesar8(1);"><img src="../../../imagenes/icono_excel.png" width="32" height="32"></label> &nbsp;&nbsp;
			 			<?php } else { ?>
			      			<label style="cursor:pointer" onClick="procesar8(2);"><img src="../../../imagenes/icono_pdf.png" width="32" height="32"></label> &nbsp;&nbsp;
			 			<?php } ?>
			      		</td>
			  		</tr>	
	    		</table>
	    	</div>
			<?php 
				}
				if($tipo==8){
			?>
			<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
				<tr>
	    			<th colspan="2"><strong>Parámetros de Configuracion Reporte</strong></th>
	    		</tr>
				<tr>
					<td style="text-align:right">Nit:</td>
					<td style="text-align:left"><input type="text" id="txtNit" name="txtNit"></td></tr>
				<tr>
					<td style="text-align:right">Estado Trabajador:</td>
					<td style="text-align:left">	
						<select name="cmbEstadoTrabajador" id="cmbEstadoTrabajador">
							<option value="">Seleccione...</option>
							<option value="A">A</option>
							<option value="P">P</option>
							<option value="I">I</option>
							<option value="T">Todos</option>
						</select>
					</td>
				</tr>
				<tr>
					<td style="text-align:center" colspan="2">
						<div id="idTipoBeneficiario">
						<?php
							$sql = "SELECT iddetalledef, detalledefinicion  FROM aportes091 WHERE iddefinicion=7";
							$resultado = $db->querySimple($sql);
							while($row = $resultado->fetch()){
								echo $row['detalledefinicion']."<input type='checkbox' name='cheTipoBeneficiario' id='cheTipoBeneficiario' value='".$row['iddetalledef']."' />";
							}
						?>
						</div>
					</td>
				</tr>
				<tr>
					<td style="text-align:right">Estado Beneficiario:</td>
					<td style="text-align:left">
						<select name="cmbEstadoBeneficiario" id="cmbEstadoBeneficiario">
							<option value="">Seleccione...</option>
							<option value="A">A</option>
							<option value="I">I</option>
							<option value="T">Todos</option>
						</select>
					</td>
  				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
	  			</tr>
				<tr>
		  			<td style="text-align:center" colspan="2">
	      				<label style="cursor:pointer"  id="idProcesar004"><img src="../../../imagenes/icono_excel.png" onClick="procesar9();" width="32" height="32"/></label>
			  		</td>
		  		</tr>
	    	</table>
			<?php
				}
				if($tipo==9){
			?>
		    <table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
				<tr>
	    			<th colspan="4"><strong>Parámetros de Configuracion Reporte</strong></th>
	    		</tr>
				<tr>
					<td style="text-align:center" width="78px">Nit: </td>
					<td><input type="text" id="txtNit" name="txtNit"></td>
					<td style="text-align:center" width="71px">Por:</td>
					<td>
						<select name="cmbFiltro" id="cmbFiltro">
							<option value="nada">Seleccione...</option>
							<option value="anno">A&ntilde;o especifico</option>				
							<option value="rangoAnno">Rango de a&ntilde;os</option>
							<option value="rangoPeriodo">Rango de Periodos</option>
						</select>
					</td>
	  			</tr>
	  			<tr>
	  				<td colspan="2">  			  			
	  					<div id="fechaini" style="display:none">Fecha Inicial:
	  						<input type="text" id="txtfini" name="txtfini" readonly style="width: 60px"  />
	  					</div>
	  				</td>
	  				<td colspan="2">
	  					<div id="fechafin" style="display:none">Fecha Final:
	  						<input type="text" id="txtffin" name="txtffin" readonly style="width: 60px" />  			
	  					</div>
	  				</td>
	  			</tr>
				<tr>
		  			<td colspan="4">&nbsp;</td>
				</tr>
				<tr>
		  			<td style="text-align:center" colspan="4">
			      		<label style="cursor:pointer" onClick="procesar10(0);" id="idProcesar004"><img src="../../../imagenes/icono_pdf.png" width="32" height="32"/></label>
			      		&nbsp;&nbsp;
			      		<label style="cursor:pointer" onClick="procesar10(1);" id="idProcesar004"><img src="../../../imagenes/icono_excel.png" width="32" height="32"/></label>
	  				</td>
		  		</tr>
    		</table>
			<?php
				}
				if($tipo==10){
			?>
	    	<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
				<tr>
	    			<th colspan="4"><strong>Parametros de Configuracion Reporte</strong></th>
	    		</tr>
				<tr>
					<td style="text-align:center">Fecha Inicial:
						<input type="text" name="txtFechaI" id="txtFechaI" readonly/> 
					</td>
					<td style="text-align:center">Fecha Final:
						<input type="text" name="txtFechaF" id="txtFechaF" readonly/> 
					</td>
				</tr>	
				<tr>
			  		<td colspan="4">&nbsp;</td>
				</tr>
				<tr>
		  			<td style="text-align:center" colspan="4">
	      				<label style="cursor:pointer" onClick="procesar11(0);" id="idProcesar004"><img src="../../../imagenes/icono_pdf.png" width="32" height="32"/></label>
			      		&nbsp;&nbsp;
			      		<label style="cursor:pointer" onClick="procesar11(1);" id="idProcesar004"><img src="../../../imagenes/icono_excel.png" width="32" height="32"/></label>
	  				</td>
		  		</tr>
    		</table>
			<?php
				}
				if($tipo==13){
			?>
	    	<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
				<tr>
	    			<th colspan="4"><strong>Parámetros de Configuracion Reporte</strong></th>
	    		</tr>
			    <tr>
			    	<td style="text-align: center;">Periodo<input type="text" id="txtPeriodo" name="txtPeriodo" readonly></td>
			    </tr>
				<tr>
		      		<td style="text-align: center;">Fecha Inicio <input type="text" id="txtFechaI" name="txtFechaI" readonly>
			      		Fecha Final <input type="text" id="txtFechaF" name="txtFechaF" readonly>
		      		</td>
	    		</tr>
				<tr>
		  			<td style="text-align: center;">
	      				<label style="cursor:pointer" onClick="procesar13();" ><img src="../../../imagenes/icono_excel.png" width="32" height="32"/></label>
	  				</td>
		  		</tr>
	    	</table>
			<?php
				}
				if($tipo==14){
			?>
	    	<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
				<tr>
	    			<th colspan="4"><strong>Parámetros de Configuracion Reporte</strong></th>
	    		</tr>
				<tr>
					<td style="text-align:right;" width="50%" >Nit: </td>
					<td><input type="text" id="txtNitEmpresa" name="txtNitEmpresa"></td>
	  			</tr>
	  			<tr>
	  				<td colspan="2" id="tdMensajeError" class="rojoGrande" align="center"></td>
	  			</tr>
				<tr>
		  			<td style="text-align:center" colspan="2" id="idTdBottonDescarga">
	      				<label style="cursor:pointer" onClick="procesar14();" id="idProcesar004"><img src="../../../imagenes/descargarArchivo.png" /></label>
	  				</td>
		  		</tr>
    		</table>
			<?php
				}
				if($tipo==16){
			?>
	    	<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
				<tr>
	    			<th colspan="4"><strong>Par&aacute;metros de Configuracion Reporte</strong></th>
	    		</tr>
				<tr>
					<td style="text-align:center;" width="50%" >
						Periodo: 
						<input type="text" id="txtPeriodo" name="txtPeriodo" readonly>
					</td>		
	  			</tr>
	  			<tr>
	  				<td colspan="2" id="tdMensajeError" class="rojoGrande" align="center"></td>  
	  			</tr>
				<tr>
		  			<td style="text-align:center" colspan="2" id="idTdBottonDescarga">
	      				<label style="cursor:pointer" onClick="procesar16(1);" id="idProcesar004"><img src="../../../imagenes/icono_pdf.png" width="32" height="32"/></label>
	  					<label style="cursor:pointer" onClick="procesar16(2);" id="idProcesar004"><img src="../../../imagenes/icono_excel.png" width="32" height="32"/></label>
	  				</td>
	  			</tr>
    		</table>
			<?php
				}
				if($tipo==17){
					list($mes,$dia,$anno) = explode( "/", $fecha );
					$mesLetra = intval($dia) . " de ";
					$mes = intval($mes);
					switch ( $mes ){
						case 1: $mesLetra .= "Enero"; break;
						case 2: $mesLetra .= "Febrero"; break;
						case 3: $mesLetra .= "Marzo"; break;
						case 4: $mesLetra .= "Abril"; break;
						case 5: $mesLetra .= "Mayo"; break;
						case 6: $mesLetra .= "Junio"; break;
						case 7: $mesLetra .= "Julio"; break;
						case 8: $mesLetra .= "Agosto"; break;
						case 9: $mesLetra .= "Septiembre"; break;
						case 10: $mesLetra .= "Octubre"; break;
						case 11: $mesLetra .= "Noviembre"; break;
						case 12: $mesLetra .= "Diciembre"; break;
					}
					$mesLetra .= " de " . $anno;
			?>
	    	<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
				<tr>
		    		<th colspan="4"><strong>Parámetros de Configuracion Reporte</strong></th>
			    </tr>
			    <tr>
		  			<td colspan="2" style="text-align:center;">  			  			
		  				Comunicaci&oacute;n:
			  			<input type="text" id="txtComunicacion" name="txtComunicacion" class="box1"/> 
			  			Tipo Empresas: 
			  			<select name="cmbTipoAfiliacion" id="cmbTipoAfiliacion" class="box1">
			  				<?php
					        	$consulta=$Definiciones->mostrar_datos(56);
					        	while($row=mssql_fetch_array($consulta)){
					        		if($row['iddetalledef']==3316 || $row['iddetalledef']==3317 || $row['iddetalledef']==3319)
						       			echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
					           	}
						    ?>
			  			</select>
		  			</td>
		  		</tr>
		  		<tr>
		  			<td colspan="2" style="text-align:center;" >
		  				Por:
		  				<select name="cmbTipoFiltroCarta" id="cmbTipoFiltroCarta" class="box1">
		  					<option value="fechas">Rango de Fechas</option>
		  					<option value="nit">Nit</option>
		  				</select>
		  				<input type="hidden" name="hidFechaLetra" id="hidFechaLetra" value="<?php echo $mesLetra;?>" />
		  			</td>
		  		</tr>
		  		<tr id="trFechas">
		  			<td colspan="2" style="text-align:center;" >  			  			
			  			Fecha Inicial:
			  			<input type="text" id="txtFechaI" name="txtFechaI" readonly class="box1"  />
			  			Fecha Final:
			  			<input type="text" id="txtFechaF" name="txtFechaF" readonly class="box1"/>  			
		  			</td>
		  		</tr>
		  		<tr id="trNit" style="display:none;">
		  			<td colspan="2" style="text-align:center;" >
		  				Nit:
		  				<input type="text" name="txtNit" id="txtNit" class="box1" />
		  			</td>
		  		</tr>
		  		<tr>
		  			<td colspan="2" style="text-align:center;" >
		  				Agencia:
		  				<select name="cmbAgencia" id="cmbAgencia" class="box1">
		  					<option value="T">Todas</option>
						  	<?php 
						  		$sql = "select * from aportes500";
						  		$consulta = $db->querySimple($sql);
						  		while($row = $consulta->fetch()){
						  			echo "<option value='{$row["codigo"]}'>{$row["agencia"]}</option>";
						  		}
						  	?>
		  				</select>
		  			</td>
		  		</tr>
				<tr>
			  		<td colspan="4">&nbsp;</td>
				</tr>
				<tr>
			  		<td style="text-align:center" colspan="4">
		      			<label style="cursor:pointer" onClick="procesar17();"><img src="../../../imagenes/icono_pdf.png" width="32" height="32"/></label>     	
		  			</td>
				</tr>
	   		</table>
			<?php
				}
				if($tipo==19){
			?>
			<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
				<tr>
	    			<th colspan="2"><strong>Par&aacute;metros de Configuracion Reporte</strong></th>
	    		</tr>
	    		<tr>
					<td style="text-align:center" colspan="3">Agencia:
			  			<select name="cmbAgencia" id="cmbAgencia">
			  				<option value="T">Todas</option>
				     		<?php 
				     			$sql = "select * from aportes500";
				     			$rs = $db->querySimple($sql);
				     			while($row=$rs->fetchObject()){
				     				echo "<option value='$row->codigo'>$row->agencia</option>";
				     			}
				     		?>			     		
			     		</select>
					</td>
	    		</tr>
				<tr>
					<td style="text-align:center;" width="30%" >
						Periodo Inicial: 
						<input type="text" id="txtPeriodo" name="txtPeriodo" readonly>
					</td>
					<td style="text-align:center;" width="30%" >
						Periodo Final: 
						<input type="text" id="txtPeriodo2" name="txtPeriodo2" readonly>
					</td>
				</tr>
				<tr>
	  				<td colspan="2" id="tdMensajeError" class="rojoGrande" align="center"></td>
	  			</tr>
				<tr>
	  				<td style="text-align:center" colspan="2" id="idTdBottonDescarga">
	      				<label style="cursor:pointer" onClick="procesar19();" id="idProcesar004"><img src="../../../imagenes/icono_excel.png" width="32" height="32"/></label>
	  				</td>
		  		</tr>
			</table>
			<?php 
				}
				if($tipo==20){
			?> 
			<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
				<tr>
	    			<th colspan="2"><strong>Par&aacute;metros de Configuracion Reporte</strong></th>
	    		</tr>
					<td style="text-align:center;" width="30%" >
						Fecha Inicial: 
						<input type="text" id="txtFechaI" name="txtFechaI" readonly>
					</td>
					<td style="text-align:center;" width="30%" >
						Fecha Final: 
						<input type="text" id="txtFechaF" name="txtFechaF" readonly>
					</td>
				</tr>
				<tr>
	  				<td colspan="2" id="tdMensajeError" class="rojoGrande" align="center"></td>
	  			</tr>
				<tr>
		  			<td style="text-align:center" colspan="2" id="idTdBottonDescarga">
	      				<label style="cursor:pointer" onClick="procesar20();" id="idProcesar004"><img src="../../../imagenes/icono_excel.png" width="32" height="32"/></label>
	  				</td>
		  		</tr>
			</table>
			<?php 
				}
				if($tipo==21){
			?> 
			<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
				<tr>
		    		<th colspan="2"><strong>Par&aacute;metros de Configuracion Reporte</strong></th>
		    	</tr>
		    	<tr style="margin:20px;">
			    	<td style="text-align:center;height: 30px;">&nbsp;&nbsp;&nbsp;Nit&nbsp;&nbsp;&nbsp;<input type="text" id="txtNit" name="txtNit" class="box1"/></td>
			    	<td style="text-align:center;">Por
			    		<select id="cmbOpcionFiltro" onchange="opcionFiltroRep21();" class="box1">
			    			<option value="1">Periodo</option>
			    			<option value="2">Rango de Periodos</option>
			    			<option value="3">Rango de fechas</option>    			
			    		</select>
		    		</td>
		    	</tr>
		    	<tr id="trPeriodo" style="height: 30px;">
					<td style="text-align:center;" colspan="2">
						Periodo: 
						<input type="text" id="txtPeriodo" name="txtPeriodo" readonly size="10" class="box1"/>
					</td>
				</tr>
		    	<tr style="display:none;height: 30px;" id="trPeriodos">
					<td style="text-align:center;" width="30%" >
						Periodo Inicial: 
						<input type="text" id="txtPeriodo1" name="txtPeriodo1" readonly size="10" class="box1"/>
					</td>
					<td style="text-align:center;" width="30%" >
						Periodo Final: 
						<input type="text" id="txtPeriodo2" name="txtPeriodo2" readonly size="10" class="box1"/>
					</td>
				</tr>
				<tr style="display:none;height: 30px;" id="trFechas" height: 30px;>
					<td style="text-align:center;" width="30%" >
						Fecha Inicial: 
						<input type="text" id="txtFechaI" name="txtFechaI" readonly size="10" class="box1"/>
					</td>
					<td style="text-align:center;" width="30%" >
						Fecha Final: 
						<input type="text" id="txtFechaF" name="txtFechaF" readonly size="10" class="box1"/>
					</td>
				</tr>
				<tr style="height: 30px;">
					<td style="text-align:center;" width="30%" >
						Tipo Identificaci&oacute;n: 
						<select id="cmbIdentificacion" class="box1">
						<?php 
							while($tipoDoc = mssql_fetch_assoc($rsTiposDoc)){ 
						?>
							<option value="<?php echo $tipoDoc["iddetalledef"];?>"<?php echo $tipoDoc["iddetalledef"]==1?"selected":""; ?> ><?php echo $tipoDoc["detalledefinicion"]; ?></option>
						<?php 
							}
						?>
						</select>
					</td>
					<td style="text-align:center;" width="30%" >
						Identificaci&oacute;n: 
						<input type="text" id="txtIdentificacion" name="txtIdentificacion" class="box1"/>
					</td>
				</tr>
				<tr style="height: 30px;">
		  			<td colspan="2" id="tdMensajeError" class="rojoGrande" align="center"></td>
		  		</tr>
				<tr>
					<td style="text-align:center" colspan="2" id="idTdBottonDescarga">
						<label style="cursor:pointer" onClick="procesar21('PDF');" id="idProcesar004"><img src="../../../imagenes/icono_pdf.png" width="32" height="32"/></label>
			    		<label style="cursor:pointer" onClick="procesar21('EXC');" id="idProcesar004"><img src="../../../imagenes/icono_excel.png" width="32" height="32"/></label>
		  			</td>
			 	</tr>
			</table>
			<?php 
				}
				if($tipo==22){
			?> 
			<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
				<tr>
			    	<th colspan="2"><strong>Par&aacute;metros de Configuracion Reporte</strong></th>
			    </tr>
				<tr  id="trFechas" height: 30px;>
					<td style="text-align:center;" width="30%" >
						Fecha Inicial: 
						<input type="text" id="txtFechaI" name="txtFechaI" readonly size="10" class="box1"/>
					</td>
					<td style="text-align:center;" width="30%" >
						Fecha Final: 
						<input type="text" id="txtFechaF" name="txtFechaF" readonly size="10" class="box1"/>
					</td>
				</tr>
				<tr>
					<td style="text-align:center" colspan="2" id="idTdBottonDescarga">
						<label style="cursor:pointer" onClick="procesar22('PDF');" id="idProcesar004"><img src="../../../imagenes/icono_pdf.png" width="32" height="32"/></label>
			    		<label style="cursor:pointer" onClick="procesar22('EXC');" id="idProcesar004"><img src="../../../imagenes/icono_excel.png" width="32" height="32"/></label>
		  			</td>
			 	</tr>
			</table>
			<?php 
				}
				if($tipo==23){
			?>
			<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
				<tr>
		    		<th colspan="2"><strong>Par&aacute;metros de Configuracion Reporte</strong></th>
		   		</tr>
				<tr >
					<td style="text-align:center;" width="30%" >
						Fecha Inicial:
						<input type="text" id="txtFechaI" name="txtFechaI" readonly size="15" class="box1"/>
						&nbsp;&nbsp;
						Fecha Final:
						<input type="text" id="txtFechaF" name="txtFechaF" readonly size="15" class="box1"/> 
					</td>
				</tr>
				<tr>
					<td style="text-align:center" colspan="2" id="idTdBottonDescarga">
						<label style="cursor:pointer" onClick="procesar23('PDF');" id="idProcesar004"><img src="../../../imagenes/icono_pdf.png" width="32" height="32"/></label>
		    			<label style="cursor:pointer" onClick="procesar23('EXC');" id="idProcesar004"><img src="../../../imagenes/icono_excel.png" width="32" height="32"/></label>
		  			</td>
			 	</tr>
			</table>
			<?php
				}
				if($tipo==24){
			?>
			<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
				<tr>
		    		<th colspan="2"><strong>Par&aacute;metros de Configuracion Reporte</strong></th>
		    	</tr>
				<tr >
					<td style="text-align:center;" width="30%" >
						Periodo:
						<input type="text" id="txtPeriodo24" name="txtPeriodo24" readonly size="10" class="box1"/>
					</td>
				</tr>
				<tr>
					<td style="text-align:center;" width="30%" >
						Fecha Inicial:
						<input type="text" id="txtFechaI24" name="txtFechaI24" readonly size="15" class="box1"/>
						&nbsp;&nbsp;
						Fecha Final:
						<input type="text" id="txtFechaF24" name="txtFechaF24" readonly size="15" class="box1"/> 
					</td>
				</tr>
				<tr>
					<td style="text-align:center" colspan="2" >
						<label style="cursor:pointer" onClick="procesar24('PDF');" ><img src="../../../imagenes/icono_pdf.png" width="32" height="32"/></label>
			    		<label style="cursor:pointer" onClick="procesar24('EXC');" ><img src="../../../imagenes/icono_excel.png" width="32" height="32"/></label>
		  			</td>
			 	</tr>
			</table>
			<?php  
				}
				if($tipo==25){
			?>
			<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
				<tr>
		    		<th colspan="2"><strong>Par&aacute;metros de Configuracion Reporte</strong></th>
		    	</tr>
		   		<tr>
					<td style="text-align:center;" width="30%" >
						Comunicaci&oacute;n:
						<input type="text" id="txtNumeroComunicacion" name="txtNumeroComunicacion" size="10" class="box1"/>
					</td>
				</tr>
				<tr>
					<td style="text-align:center;" width="30%" >
						Periodo:
						<input type="text" id="txtPeriodo" name="txtPeriodo" readonly size="10" class="box1"/>
					</td>
				</tr>
				<tr>
					<td style="text-align:center;" width="30%" >
						Fecha Inicial:
						<input type="text" id="txtFechaI" name="txtFechaI" readonly size="15" class="box1"/>
						&nbsp;&nbsp;
						Fecha Final:
						<input type="text" id="txtFechaF" name="txtFechaF" readonly size="15" class="box1"/> 
					</td>
				</tr>
				<tr>
					<td style="text-align:center" colspan="2" >
						<label style="cursor:pointer" onClick="procesar25('PDF');" ><img src="../../../imagenes/icono_pdf.png" width="32" height="32"/></label>
			    		<label style="cursor:pointer" onClick="procesar25('EXC');" ><img src="../../../imagenes/icono_excel.png" width="32" height="32"/></label>
			  		</td>
			 	</tr>
			</table>
			<?php 
				}
				if($tipo==27){
			?>
			<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
				<tr>
		    		<th colspan="2"><strong>Par&aacute;metros de Configuracion Reporte</strong></th>
		   		</tr>
				<tr>
					<td style="text-align:center;" width="30%" >
						Periodo:
						<input type="text" id="txtPeriodo" name="txtPeriodo" readonly size="10" class="box1"/>
						Giro:
						<select name="cmbTabla" id="cmbTabla" class="box1">
							<option value="aportes014">Actual</option>
							<option value="aportes009">Historico</option>
						</select> 
					</td>
				</tr>
				<tr>
					<td style="text-align:center" colspan="2" >
						<label style="cursor:pointer" onClick="procesar27('PDF');" ><img src="../../../imagenes/icono_pdf.png" width="32" height="32"/></label>
			    		<label style="cursor:pointer" onClick="procesar27('EXC');" ><img src="../../../imagenes/icono_excel.png" width="32" height="32"/></label>
		  			</td>
			 	</tr>
			</table>
			<?php 
				}
				if($tipo==28){
			?>
			<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
				<tr>
		    		<th colspan="2"><strong>Par&aacute;metros de Configuracion Reporte</strong></th>
		   		</tr>
				<tr>
					<td style="text-align:center;" width="30%" >
						Periodo:
						<input type="text" id="txtFecha" name="txtFecha" readonly size="10" class="box1"/>
					</td>
				</tr>
				<tr>
					<td style="text-align:center" colspan="2" >
						<label style="cursor:pointer" onClick="procesar28('PDF');" ><img src="../../../imagenes/icono_pdf.png" width="32" height="32"/></label>
			    		<label style="cursor:pointer" onClick="procesar28('EXC');" ><img src="../../../imagenes/icono_excel.png" width="32" height="32"/></label>
		  			</td>
			 	</tr>
			</table>
			<?php 
				}
				if($tipo==29){
			?>
			<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
				<tr>
		    		<th colspan="2"><strong>Par&aacute;metros de Configuracion Reporte</strong></th>
		   		</tr>
				<tr>
					<td style="text-align:center;" width="30%" >
						Periodo:
						<input type="text" id="txtPeriodo" name="txtPeriodo" readonly size="10" class="box1"/>
						Giro:
						<select name="cmbTabla" id="cmbTabla" class="box1">
							<option value="aportes014">Actual</option>
							<option value="aportes009">Historico</option>
						</select> 
					</td>
				</tr>
				<tr>
					<td style="text-align:center" colspan="2" >
						<label style="cursor:pointer" onClick="procesar29('PDF');" ><img src="../../../imagenes/icono_pdf.png" width="32" height="32"/></label>
			    		<label style="cursor:pointer" onClick="procesar29('EXC');" ><img src="../../../imagenes/icono_excel.png" width="32" height="32"/></label>
		  			</td>
			 	</tr>
			</table>
			<?php
				}
				if($tipo==30){
			?>
			<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
				<tr>
		    		<th colspan="2"><strong>Par&aacute;metros de Configuracion Reporte</strong></th>
		    	</tr>
				<tr >
					<td style="text-align:center;" width="30%" >
						Periodo:
						<input type="text" id="txtPeriodo24" name="txtPeriodo24" readonly size="10" class="box1"/>
					</td>
				</tr>
				<tr>
					<td style="text-align:center;" width="30%" >
						Fecha Inicial:
						<input type="text" id="txtFechaI24" name="txtFechaI24" readonly size="15" class="box1"/>
						&nbsp;&nbsp;
						Fecha Final:
						<input type="text" id="txtFechaF24" name="txtFechaF24" readonly size="15" class="box1"/> 
					</td>
				</tr>
				<tr>
					<td style="text-align:center" colspan="2" >
						<label style="cursor:pointer" onClick="procesar30('PDF');" ><img src="../../../imagenes/icono_pdf.png" width="32" height="32"/></label>
			    		<label style="cursor:pointer" onClick="procesar30('EXC');" ><img src="../../../imagenes/icono_excel.png" width="32" height="32"/></label>
		  			</td>
			 	</tr>
			</table>
			<?php
				}
				if($tipo==31){
			?>
			<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
				<tr>
		    		<th colspan="2"><strong>Par&aacute;metros de Configuracion Reporte</strong></th>
		    	</tr>
		    	<tr >
					<td style="text-align:center;" width="30%" >
						Nit:
						<input type="text" id="txtNit" name="txtNit" class="box1"/>
					</td>
				</tr>
				<tr >
					<td style="text-align:center;" width="30%" >
						Clase aportante:
						<select name="cmbClaseAportante" id="cmbClaseAportante" class="box1">
			  				<?php
					        	$consulta=$Definiciones->mostrar_datos(33);
					        	while($row=mssql_fetch_array($consulta)){
					       			echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
					           	}
						    ?>
						    <option value="">Todas</option>
			  			</select>
					</td>
				</tr>
				<tr >
					<td style="text-align:center;" width="30%" >
						Periodo:
						<input type="text" id="txtPeriodo" name="txtPeriodo" readonly size="10" class="box1"/>
					</td>
				</tr>
				<tr >
					<td style="text-align:center;" width="30%" >
						Estado Suspensi&oacute;n:
						<select name="cmbEstadoSuspension" id="cmbEstadoSuspension" class="box1">
							<option value="A">Activo</option>
							<option value="I">Inactivo</option>
							<option value="">Todos</option>
						</select>
					</td>
				</tr>
				<tr>
					<td style="text-align:center;" width="30%" >
						Fecha Suspensi&oacute;n:
						<input type="text" id="txtFechaI" name="txtFechaI" readonly size="15" class="box1"/>
						&nbsp;&nbsp;
						-
						<input type="text" id="txtFechaF" name="txtFechaF" readonly size="15" class="box1"/> 
					</td>
				</tr>
				<tr>
					<td style="text-align:center" colspan="2" >
						<label style="cursor:pointer" onClick="procesar31('PDF');" ><img src="../../../imagenes/icono_pdf.png" width="32" height="32"/></label>
			    		<label style="cursor:pointer" onClick="procesar31('EXC');" ><img src="../../../imagenes/icono_excel.png" width="32" height="32"/></label>
		  			</td>
			 	</tr>
			</table>
			<?php
				}
				if($tipo==32){
			?>
			<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
				<tr>
		    		<th colspan="2"><strong>Par&aacute;metros de Configuracion Reporte</strong></th>
		    	</tr>
		    	<tr >
					<td style="text-align:center;" width="30%" >
						Nit:
						<input type="text" id="txtNit" name="txtNit" class="box1"/>
					</td>
				</tr>
				<tr >
					<td style="text-align:center;" width="30%" >
						Estado Empresa:
						<select name="cmbEstadoEmpresa" id="cmbEstadoEmpresa" class="box1">
							<option value="A">Activo</option>
							<option value="I">Inactivo</option>
							<option value="">Todos</option>
						</select>	
					</td>
				</tr>
				<tr >
					<td style="text-align:center;" width="30%" >
						Estado Desafiliaci&oacute;n:
						<select name="cmbEstadoDesafiliacion" id="cmbEstadoDesafiliacion" class="box1">
							<option value="A">Activo</option>
							<option value="I">Inactivo</option>
							<option value="">Todos</option>
						</select>
					</td>
				</tr>
				<tr>
					<td style="text-align:center;" width="30%" >
						Fecha Desafiliaci&oacute;n:
						<input type="text" id="txtFechaI" name="txtFechaI" readonly size="15" class="box1"/>
						&nbsp;&nbsp;
						-
						<input type="text" id="txtFechaF" name="txtFechaF" readonly size="15" class="box1"/> 
					</td>
				</tr>
				<tr>
					<td style="text-align:center" colspan="2" >
						<label style="cursor:pointer" onClick="procesar32('PDF');" ><img src="../../../imagenes/icono_pdf.png" width="32" height="32"/></label>
			    		<label style="cursor:pointer" onClick="procesar32('EXC');" ><img src="../../../imagenes/icono_excel.png" width="32" height="32"/></label>
		  			</td>
			 	</tr>
			</table>
			<?php
				}
				if($tipo==33){
			?>
			<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
				<tr>
		    		<th colspan="2"><strong>Par&aacute;metros de Configuracion Reporte</strong></th>
		    	</tr>
		    	<tr >
					<td style="text-align:center;" width="30%" >
						Comunicaci&oacute;n:
						<input type="text" id="txtComunicacion" name="txtComunicacion" class="box1"/>
						<span class="clsObligatorio">&nbsp;&nbsp;&nbsp;</span>
					</td>
				</tr>
		    	<tr >
					<td style="text-align:center;" width="30%" >
						Nit:
						<input type="text" id="txtNit" name="txtNit" class="box1"/>
					</td>
				</tr>
				<tr >
					<td style="text-align:center;" width="30%" >
						Clase aportante:
						<select name="cmbClaseAportante" id="cmbClaseAportante" class="box1">
			  				<?php
					        	$consulta=$Definiciones->mostrar_datos(33);
					        	while($row=mssql_fetch_array($consulta)){
					       			echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
					           	}
						    ?>
						    <option value="">Todas</option>
			  			</select>
					</td>
				</tr>
				<tr>
					<td style="text-align:center;" width="30%" >
						Fecha Suspensi&oacute;n:
						<input type="text" id="txtFechaI" name="txtFechaI" readonly size="15" class="box1"/>
						&nbsp;&nbsp;
						-
						<input type="text" id="txtFechaF" name="txtFechaF" readonly size="15" class="box1"/> 
					</td>
				</tr>
				<tr>
					<td><b>Nota:</b> Recuerde que una vez causada la suspensi&oacute;n tiene 10 d&iacute;as calendario para generar la carta</td>
				</tr>
				<tr>
					<td style="text-align:center" colspan="2" >
						<label style="cursor:pointer" onClick="procesar33('PDF');" ><img src="../../../imagenes/icono_pdf.png" width="32" height="32"/></label>
		  			</td>
			 	</tr>
			</table>
			<?php
				}
				if($tipo==34){
			?>
			<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
				<tr>
		    		<th colspan="2"><strong>Par&aacute;metros de Configuracion Reporte</strong></th>
		    	</tr>
				<tr >
					<td style="text-align:center;" width="30%" >
						Nit:
						<input type="text" id="txtNit" name="txtNit" class="box1"/>
					</td>
				</tr>
				<tr>
					<td style="text-align:center;" width="30%" >
						Carta Notificaci&oacute;n:
						<select name="cmbCartaNotificacion" id="cmbCartaNotificacion" class="box1">
			  				<?php
					        	$consulta=$Definiciones->mostrar_datos(71);
					        	while($row=mssql_fetch_array($consulta)){
					       			echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
					           	}
						    ?>
						    <option value="">Todas</option>
			  			</select>
					</td>
				</tr>
				<tr>
					<td style="text-align:center;" width="30%" >
						Estado Notificaci&oacute;n:
						<select name="cmbEstadoNotificacion" id="cmbEstadoNotificacion" class="box1">
			  				<?php
					        	$consulta=$Definiciones->mostrar_datos(72);
					        	while($row=mssql_fetch_array($consulta)){
					       			echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
					           	}
						    ?>
						    <option value="">Todas</option>
			  			</select>
					</td>
				</tr>
				<tr>
					<td style="text-align:center;" width="30%" >
						Fecha Notificaci&oacute;n:
						<input type="text" id="txtFechaI" name="txtFechaI" readonly size="15" class="box1"/>
						&nbsp;&nbsp;
						-
						<input type="text" id="txtFechaF" name="txtFechaF" readonly size="15" class="box1"/> 
					</td>
				</tr>
				<tr>
					<td style="text-align:center" colspan="2" >
						<label style="cursor:pointer" onClick="procesar34('PDF');" ><img src="../../../imagenes/icono_pdf.png" width="32" height="32"/></label>
			    		<label style="cursor:pointer" onClick="procesar34('EXC');" ><img src="../../../imagenes/icono_excel.png" width="32" height="32"/></label>
		  			</td>
			 	</tr>
			</table>
			<?php
				}
				if($tipo==35){
			?>
			<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
				<tr>
		    		<th colspan="2"><strong>Par&aacute;metros de Configuracion Reporte</strong></th>
		    	</tr>
		    	<tr >
					<td style="text-align:center;" width="30%" >
						Comunicaci&oacute;n:
						<input type="text" id="txtComunicacion" name="txtComunicacion" class="box1"/>
						<span class="clsObligatorio">&nbsp;&nbsp;&nbsp;</span>
					</td>
				</tr>
				<tr >
					<td style="text-align:center;" width="30%" >
						Nit:
						<input type="text" id="txtNit" name="txtNit" class="box1"/>
					</td>
				</tr>
				<tr>
					<td style="text-align:center;" width="30%" >
						Fecha Expulsi&oacute;n:
						<input type="text" id="txtFechaI" name="txtFechaI" readonly size="15" class="box1"/>
						&nbsp;&nbsp;
						-
						<input type="text" id="txtFechaF" name="txtFechaF" readonly size="15" class="box1"/> 
					</td>
				</tr>
				<tr>
					<td style="text-align:center" colspan="2" >
						<label style="cursor:pointer" onClick="procesar35('PDF');" ><img src="../../../imagenes/icono_pdf.png" width="32" height="32"/></label>
		  			</td>
			 	</tr>
			</table>
			<?php
				}
				if($tipo==36){
			?>
			<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
				<tr>
		    		<th colspan="2"><strong>Par&aacute;metros de Configuracion Reporte</strong></th>
		    	</tr>
		    	<tr >
					<td style="text-align:center;" width="30%" >
						Comunicaci&oacute;n:
						<input type="text" id="txtComunicacion" name="txtComunicacion" class="box1"/>
						<span class="clsObligatorio">&nbsp;&nbsp;&nbsp;</span>
					</td>
				</tr>
				<tr >
					<td style="text-align:center;" width="30%" >
						Nit:
						<input type="text" id="txtNit" name="txtNit" class="box1"/>
					</td>
				</tr>
				<tr >
					<td style="text-align:center;" width="30%" >
						N&uacute;mero Acta:
						<input type="text" id="txtNumeroActa" name="txtNumeroActa" class="box1"/>
						<span class="clsObligatorio">&nbsp;&nbsp;&nbsp;</span>
					</td>
				</tr>
				<tr >
					<td style="text-align:center;" width="30%" >
						Fecha Acta:
						<input type="text" id="txtFechaLetra" name="txtFechaLetra" readonly class="boxmediano" />
						<span class="clsObligatorio">&nbsp;&nbsp;&nbsp;</span>
					</td>
				</tr>
				<tr>
					<td style="text-align:center;" width="30%" >
						Fecha Estado Empresa:
						<input type="text" id="txtFechaI" name="txtFechaI" readonly size="15" class="box1"/>
						&nbsp;&nbsp;
						-
						<input type="text" id="txtFechaF" name="txtFechaF" readonly size="15" class="box1"/> 
					</td>
				</tr>
				<tr>
					<td style="text-align:center" colspan="2" >
						<label style="cursor:pointer" onClick="procesar36('PDF');" ><img src="../../../imagenes/icono_pdf.png" width="32" height="32"/></label>
		  			</td>
			 	</tr>
			</table>
			<?php
				}
				if($tipo==37){
			?>
			<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
				<tr>
		    		<th colspan="2"><strong>Par&aacute;metros de Configuracion Reporte</strong></th>
		    	</tr>
				<tr >
					<td style="text-align:center;" width="30%" >
						Nit:
						<input type="text" id="txtNit" name="txtNit" class="box1"/>
					</td>
				</tr>
				<tr>
					<td style="text-align:center;" width="30%" >
						Fecha Desafiliaci&oacute;n:
						<input type="text" id="txtFechaI" name="txtFechaI" readonly size="15" class="box1"/>
						&nbsp;&nbsp;
						-
						<input type="text" id="txtFechaF" name="txtFechaF" readonly size="15" class="box1"/> 
					</td>
				</tr>
				<tr>
					<td style="text-align:center" colspan="2" >
						<label style="cursor:pointer" onClick="procesar37('PDF');" ><img src="../../../imagenes/icono_pdf.png" width="32" height="32"/></label>
			    		<label style="cursor:pointer" onClick="procesar37('EXC');" ><img src="../../../imagenes/icono_excel.png" width="32" height="32"/></label>
		  			</td>
			 	</tr>
			</table>
			<?php
				}				
				if($tipo==38){
			?>
			<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
				<tr>
		    		<th colspan="2"><strong>Par&aacute;metros de Configuracion Reporte</strong></th>
		    </tr>
            <tr>
            <td style="text-align:center;" width="48%" >Fecha Inicial :
            <input name="fechaI" type="text" class="box" id="fechaI" readonly /></td>
            <td style="text-align:center;" width="48%" >Fecha Final :
 			<input name="fechaF" type="text" class="box" id="fechaF" readonly /></td>		
            </tr>
            <tr>
                <td colspan="2" id="tdMensajeError" class="rojoGrande" align="center">&nbsp;</td>
            </tr>
            <tr>
                <td style="text-align:center" colspan="2" id="idTdBottonDescarga">              
                    <label style="cursor:pointer" onClick="procesarconsolidado(1);" id="idProcesar038"><img src="../../../imagenes/icono_excel.png" width="32" height="32"/></label>
                    <label style="cursor:pointer" onClick="procesarconsolidado(2);" id="idProcesar038"><img src="../../../imagenes/icono_pdf.png" width="32" height="32"/></label>               
             
                </td>
              </tr>
			</table>
			<?php
			}
			if($tipo==39){
			?>
			<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
				<tr>
		    		<th colspan="2"><strong>Par&aacute;metros de Configuracion Reporte</strong></th>
		    	</tr>
		    	<tr>
		    		<td style="text-align:center;">  			  			
		  				Comunicaci&oacute;n:
			  		<input type="text" id="txtComunicacion" name="txtComunicacion" class="box1"/> 
			  		</td>
			  		<td width="25%">Agencia
      				<select name="cmbAgencia" class="box1" id="cmbAgencia">
					<option value="T">TODOS</option>
					<?php 
					while ($row1=$agencia->fetch()){
						echo "<option value=".$row1['codigo'].">".$row1['agencia']."</option>";
						}
					?>
					</select>
      				</td>
		  		</tr>
				<tr>
					<td colspan="2" style="text-align:center;" width="30%" >
						Fecha Ingreso:
						<input type="text" id="txtFechaI" name="txtFechaI" readonly size="15" class="box1"/>
						&nbsp;&nbsp;
						-
						<input type="text" id="txtFechaF" name="txtFechaF" readonly size="15" class="box1"/> 
					</td>
				</tr>
				<tr>
		  			<td colspan="2" style="text-align:center;" >
		  				Nit:
		  				<input type="text" name="txtNit" id="txtNit" class="box1" />
		  			</td>
		  		</tr>
		  		<tr>
			    	<td colspan="2" style="text-align: center;">
			    	Periodo:
			    	<input type="text" id="txtPeriodo" name="txtPeriodo" class="box1"></td>
			    </tr>
				<tr>
					<td style="text-align:center" colspan="2" >
						<label style="cursor:pointer" onClick="procesar39('PDF');" ><img src="../../../imagenes/icono_pdf.png" width="32" height="32"/></label>
		  			</td>
			 	</tr>
			</table>
			<?php
			}
			if($tipo==40){
			?>
			<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
				<tr>
		    		<th colspan="2"><strong>Par&aacute;metros de Configuracion Reporte</strong></th>
		    	</tr>
				<tr>
					<td colspan="2" style="text-align:center;" width="30%" >
						Fecha Sistema:
						<input type="text" id="txtFechaI" name="txtFechaI" readonly size="15" class="box1"/>
						&nbsp;&nbsp;
						-
						<input type="text" id="txtFechaF" name="txtFechaF" readonly size="15" class="box1"/> 
					</td>
				</tr>
				<tr>
					<td style="text-align:center" colspan="2" >
						<label style="cursor:pointer" onClick="procesar40();" ><img src="../../../imagenes/icono_excel.png" width="22" height="22"></label>
		  			</td>
			 	</tr>
			</table>
			</table>
			<?php
			}
			if($tipo==45){
			?>
			<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
				<tr>
		    		<th colspan="2"><strong>Par&aacute;metros de Configuracion Reporte</strong></th>
		    	</tr>
				<tr>
					<td colspan="2" style="text-align:center;" width="30%" >
						Fecha Ingreso:
						<input type="text" id="txtFechaI" name="txtFechaI" readonly size="15" class="box1"/>
						&nbsp;&nbsp;
						-
						<input type="text" id="txtFechaF" name="txtFechaF" readonly size="15" class="box1"/> 
					</td>
				</tr>
				<tr>
		  			<td colspan="2" style="text-align:center;" >
		  				Nit:
		  				<input type="text" name="txtNit" id="txtNit" class="box1" />
		  			</td>
		  		</tr>
				<tr>
					<td style="text-align:center" colspan="2" >
						<label style="cursor:pointer" onClick="procesar45();" ><img src="../../../imagenes/icono_excel.png" width="32" height="32"/></label>
		  			</td>
			 	</tr>
			</table>
			<?php
			}
			if($tipo==46){
			?>
			<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
				<tr>
		    		<th colspan="2"><strong>Par&aacute;metros de Configuracion Reporte</strong></th>
		    	</tr>
				<tr>
					<td colspan="2" style="text-align:center;" width="30%" >
						Fecha Ingreso:
						<input type="text" id="txtFechaI" name="txtFechaI" readonly size="15" class="box1"/>
						&nbsp;&nbsp;
						-
						<input type="text" id="txtFechaF" name="txtFechaF" readonly size="15" class="box1"/> 
					</td>
				</tr>
				<tr>
					&nbsp;
		  		</tr>
				<tr>
					<td style="text-align:center" colspan="2" >
						<label style="cursor:pointer" onClick="procesar46();" ><img src="../../../imagenes/icono_excel.png" width="32" height="32"/></label>
		  			</td>
			 	</tr>
			</table>
			<?php
			}
			if($tipo==47){
				?>
				<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
					<tr>
			    		<th colspan="2"><strong>Par&aacute;metros de Configuracion Reporte</strong></th>
			    	</tr>
			    	<tr>
			    		<td style="text-align:center;">  			  			
			  				Comunicaci&oacute;n:
				  		<input type="text" id="txtComunicacion" name="txtComunicacion" class="box1"/> 
				  		</td>
			  		</tr>
					<tr>
						<td style="text-align:center;" width="30%" >
							Fecha Ingreso:
							<input type="text" id="txtFechaI" name="txtFechaI" readonly size="15" class="box1"/>
							&nbsp;&nbsp;
							-
							<input type="text" id="txtFechaF" name="txtFechaF" readonly size="15" class="box1"/> 
						</td>
					</tr>
					<tr>
			  			<td style="text-align:center;" >
			  				Nit:
			  				<input type="text" name="txtNit" id="txtNit" class="box1" />
			  			</td>
			  		</tr>
					<tr>
						<td style="text-align:center" colspan="2" >
							<label style="cursor:pointer" onClick="procesar47('PDF');" ><img src="../../../imagenes/icono_pdf.png" width="32" height="32"/></label>
			  			</td>
				 	</tr>
				</table>
			<?php
			}
			if($tipo==41){
			?>
			<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
				<tr>
		    		<th colspan="2"><strong>Par&aacute;metros de Configuracion Reporte</strong></th>
		    	</tr>
				<tr>
					<td colspan="2" style="text-align:center;" width="30%" >
						Fecha Ingreso:
						<input type="text" id="txtFechaI" name="txtFechaI" readonly size="15" class="box1"/>
						&nbsp;&nbsp;
						-
						<input type="text" id="txtFechaF" name="txtFechaF" readonly size="15" class="box1"/> 
					</td>
				</tr>
				<tr>
		  			<td colspan="2" style="text-align:center;" >
		  				Nit:
		  				<input type="text" name="txtNit" id="txtNit" class="box1" />
		  			</td>
		  		</tr>
				<tr>
					<td style="text-align:center" colspan="2" >
						<label style="cursor:pointer" onClick="procesar48();" ><img src="../../../imagenes/icono_excel.png" width="32" height="32"/></label>
		  			</td>
			 	</tr>
			</table>
			<?php
				}
				if($tipo==48){
			?>
	    	<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
				<tr>
	    			<th colspan="4"><strong>Parametros de Configuracion Reporte</strong></th>
	    		</tr>
				<tr>
					<td style="text-align:center">Periodo Inicial:
						<input type="text" name="txtPeriodo" id="txtPeriodo" readonly/> 
					</td>
					<td style="text-align:center">Periodo Final:
						<input type="text" name="txtPeriodo2" id="txtPeriodo2" readonly/> 
					</td>
				</tr>	
				<tr>
			  		<td colspan="4">&nbsp;</td>
				</tr>
				<tr>
		  			<td style="text-align:center" colspan="4">
	      				<label style="cursor:pointer" onClick="procesar49(0);" id="idProcesar004"><img src="../../../imagenes/icono_pdf.png" width="32" height="32"/></label>
			      		&nbsp;&nbsp;
			      		<label style="cursor:pointer" onClick="procesar49(1);" id="idProcesar004"><img src="../../../imagenes/icono_excel.png" width="32" height="32"/></label>
	  				</td>
		  		</tr>
    		</table>
			<?php
				}
				if($tipo==49){
			?>
	    	<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
				<tr>
	    			<th colspan="4"><strong>Parametros de Configuracion Reporte</strong></th>
	    		</tr>
				<tr>
					<td style="text-align:center">Fecha Inicial:
						<input type="text" name="txtFechaI" id="txtFechaI" readonly/> 
					</td>
					<td style="text-align:center">Fecha Final:
						<input type="text" name="txtFechaF" id="txtFechaF" readonly/> 
					</td>
				</tr>	
                <tr>
                <td colspan="4" style=" text-align:center">
                    D&iacute;as Cotizados y Aportes:
						<select name="cmbValor" id="cmbValor" class="boxmedio">
							<option value="1">Con D&iacute;as Cotizados pero sin Aportes </option>
							<option value="2">Sin D&iacute;as Cotizados pero con Aportes </option>
						</select>	
                    </td>
                </tr>
				<tr>
			  		<td colspan="4">&nbsp;</td>
				</tr>                
				<tr>
		  			<td style="text-align:center" colspan="4">
	      				<label style="cursor:pointer" onClick="procesar50(0);" id="idProcesar004"><img src="../../../imagenes/icono_pdf.png" width="32" height="32"/></label>
			      		&nbsp;&nbsp;
			      		<label style="cursor:pointer" onClick="procesar50(1);" id="idProcesar004"><img src="../../../imagenes/icono_excel.png" width="32" height="32"/></label>
	  				</td>
		  		</tr>
			<?php
			}
			if($tipo==54){
            ?>
            <table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
				<tr>
	    			<th colspan="4"><strong>Parametros de Configuracion Reporte</strong></th>
	    		</tr>
	    		<tr>
	    		    <td style="text-align:center">Seleccione el indice</td>
	    		    <td style="text-align:center">
	    		       <select name='indice' id='indice'>
	    		           <option value="0"> Todos </option>
	    		           <option value="1" selected> < 4 %</option>
	    		           <option value="2"> = 4 %</option>
	    		       </select>
	    		    </td>
	    		</tr>
				<tr>
					<td style="text-align:center">
					    <p>
					       Periodo Inicial:</br>
					       <input type="text" name="txtPeriodoIni" id="txtPeriodoIni" readonly/> 
					    </p>
					</td style="text-align:center">
					<td style="text-align:center">
					   <p>
					      Periodo Final:</br>
					      <input type="text" name="txtPeriodoFin" id="txtPeriodoFin" readonly/> 
					   </p>
					</td>
				</tr>
				<tr>
					<td style="text-align:center">
					    <p>
					       Fecha Pago Inicial:<br>
						   <input type="text" name="txtFechaPagoIni" id="txtFechaPagoIni" readonly/> 
						<p>
					</td>
					<td style="text-align:center">
					    <p>
					       Fecha Pago Final:<br>
						   <input type="text" name="txtFechaPagoFin" id="txtFechaPagoFin" readonly/> 
						</p>
					</td>
				</tr>	
				<tr>
			  		<td colspan="4"><strong>Clase Aportante:</strong></td>
				</tr>
				<tr>
			  		<td colspan="4" style="padding-left: 90px;padding-top:10px;padding-bottom:10px;">
			  		    <table border='0'>
			  		     <tr>
			  		         <td>APORTANTE CON 200 O MAS COTIZANTES:</td><td><input type="checkbox" value="2653" checked id="claseaportante"></td>
			  		     </tr>
			  		    <tr>
			  		         <td>APORTANTE CON MENOS DE 200 COTIZANTES:</td><td><input type="checkbox" value="2654" checked id="claseaportante"></td>
			  		    </tr>
			  		    <tr>
			  		         <td>INDEPENDIENTE: </td><td><input type="checkbox" value="2655" id="claseaportante"></td>
			  		    </tr>
			  		    <tr>
			  		         <td>APORTANTE MIPYME (LEY 590 DE 2000):</td><td><input type="checkbox" value="2874" id="claseaportante"></td>
			  		    </tr>
			  		    <tr>
			  		         <td>APORTANTE PRIMER EMPLEO(LEY 1429 DE 2010):</td><td><input type="checkbox" value="2875" id="claseaportante"></td>
			  		    </tr>
			  		    </table>
			  		</td>
				</tr>
				<tr>
			  		<td colspan="4">&nbsp;</td>
				</tr>
				<tr>
		  			<td style="text-align:center" colspan="4">
			      		<label style="cursor:pointer" onClick="procesar54();" id="idProcesar54"><img src="../../../imagenes/icono_excel.png" width="32" height="32"/></label>
	  				</td>
		  		</tr>
    		</table>
    		<table id="tablatempo" width="2000" border="1" cellspacing="0" style="display:none;">
    		  <thead>
    		       <tr>
		              <td colspan="6" height="80" align="center" valign="center"><img src="http://localhost/sigas/imagenes/logo_comfamiliar.jpg" border="0"></td>
		              <td colspan="13" align="center" valign="center"><font size="4"><strong>LISTADO DE EMPRESAS CON INDICE DE APORTES <span id="valindice"></span></strong></font></td>
		              <td colspan="10" align="center" width='350'><font size="2"><strong>(Conversiones: I-Ingreso. R-Retiro. VS-Variancion Salarial. V-Vacaciones. M-Maternidad. TC-Tipo cotizante. P-Procesado. C-Correccion. ITE-Incapacidad Temporal de Enfermedad. ITAT-Incapacidad Temporal Acc Trabajo. SLN-Suspension Laboral no remunerada.) </strong></font></td>
		          </tr>
		          <tr>
		              <th>Nit</th>
		              <th>Razon Socia</th>
		              <th>Clase aportante</th>
		              <th>Municipio</th>
		              <th>Direcci&oacute;n</th>
		              <th>Telefono</th>
		              <th>Email</th>
		              <th>Identificacion</th>
		              <th>Nombre</th>
		              <th>Indice</th>
		              <th>valor aporte</th>
		              <th>Salario Basico</th>
		              <th>Ingreso Base</th>
		              <th>Dias cotizados</th>
		              <th>Periodo</th>
		              <th>Horas cotizadas</th>
		              <th>Fecha de pago</th>
		              <th>Tipo afiliado</th>
		              <th>Estado Afiliado</th>
		              <th>I</th>
		              <th>R</th>
		              <th>VS</th>
		              <th>V</th>
		              <th>M</th>
		              <th>TC</th>
		              <th>C</th>
		              <th>ITE</th>
		              <th>ITAT</th>
		              <th>SLN</th>
		          </tr>
    		  </thead>
    		</table>
            
            <?php 
            
            }
            
			?>
			<input type="hidden" id="txtusuario" name="txtusuario" value="<?php echo $usuario; ?>" /> 
		  	<input type="hidden" id="txtNombreUsuario" name="txtNombreUsuario" value="<?php echo $_SESSION["USSER"]; ?>" />
			<input type="hidden" id="txtSexoUsuario" name="txtSexoUsuario" value="<?php echo $sexoUsuario; ?>" />
		  <input type="hidden" id="txtRept" name="txtRept" value="<?php echo $rept; ?>" />		
	</body>
</html>