
<?php
	date_default_timezone_set('America/Bogota');
	set_time_limit(0);
	$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
	include_once  $root;
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
	
	$db = IFXDbManejador::conectarDB();
	if($db->conexionID==null){
		$cadena = $db->error;
		echo msg_error($cadena);
		exit();
	}
	
	include_once $raiz.'/config.php';
	$path_plano =$ruta_generados.'aportes'.DIRECTORY_SEPARATOR.'empresas'.DIRECTORY_SEPARATOR;
	
	$opcion = $_REQUEST['v0'];
	switch ($opcion){
		case "1":
				
			$nit                = $_REQUEST['v1'];
			$estadoTrabajador   = $_REQUEST['v2'];
			$tipoBeneficiario   = ($_REQUEST['v3']!="")?trim($_REQUEST['v3'],","):"0";
			$estadoBeneficiario = $_REQUEST['v4'];
			$conyuge            = $_REQUEST['v5'];
			
			$sql="select a48.nit, a15.identificacion, a15.papellido, a15.sapellido, a15.pnombre, a15.snombre,a16.fechaingreso, case when a16.fecharetiro is null then '' else rtrim(a16.fecharetiro) end as fecharetiro,a16.estado, b91.detalledefinicion, a15.sexo, a15.fechanacimiento,	datediff(YY,convert(varchar(10),a15.fechanacimiento,101), convert(varchar(10),getdate(),101)) as edad,b15.identificacion as identifibenef, b15.papellido as papellbenef, b15.sapellido as sapellbenef, b15.pnombre as pnombrebenef,	b15.snombre as snombrebenef, b15.fechanacimiento as nacimibenef,datediff(YY,convert(varchar(10),b15.fechanacimiento,101),convert(varchar(10),getdate(),101)) as edadbenef,	a91.detalledefinicion as parenbenef, a21.estado as estbenef, case when a21.fechaestado is null then '' else rtrim(a21.fechaestado) end as fechaestadobenef,	b15.sexo as sexobenef,a21.giro
			from aportes015 a15
				inner join aportes016 a16 on a15.idpersona=a16.idpersona left join aportes021 a21 on a16.idpersona=a21.idtrabajador	inner join aportes048 a48 on a16.idempresa=a48.idempresa	left join aportes091 a91 on a21.idparentesco=a91.iddetalledef	left join aportes091 b91 on b91.iddetalledef=a15.idestadocivil	left join aportes015 b15 on b15.idpersona=a21.idbeneficiario
			WHERE '".$estadoTrabajador."'!='I' and (a16.estado='".$estadoTrabajador."' or '".$estadoTrabajador."'='T') and a48.nit='".$nit."' and (a21.estado='".$estadoBeneficiario."' or '".$estadoBeneficiario."'='T') and ((a21.idparentesco=".$conyuge." and a21.conviven='S') or a21.idparentesco in (".$tipoBeneficiario.")) 
			union	
			select a48.nit, a15.identificacion, a15.papellido, a15.sapellido, a15.pnombre, a15.snombre,	a16.fechaingreso, case when a16.fecharetiro is null then '' else rtrim(a16.fecharetiro) end as fecharetiro,	'I' estado, b91.detalledefinicion, a15.sexo, a15.fechanacimiento, datediff(YY,convert(varchar(10),a15.fechanacimiento,101), convert(varchar(10),getdate(),101)) as edad,b15.identificacion as identifibenef, b15.papellido as papellbenef, b15.sapellido as sapellbenef, b15.pnombre as pnombrebenef,	b15.snombre as snombrebenef, b15.fechanacimiento as nacimibenef,datediff(YY,convert(varchar(10),b15.fechanacimiento,101),convert(varchar(10),getdate(),101)) as edadbenef,	a91.detalledefinicion as parenbenef, a21.estado as estbenef, case when a21.fechaestado is null then '' else rtrim(a21.fechaestado) end as fechaestadobenef,	b15.sexo as sexobenef,a21.giro	from aportes015 a15
			inner join aportes017 a16 on a15.idpersona=a16.idpersona 	left join aportes021 a21 on a16.idpersona=a21.idtrabajador inner join aportes048 a48 on a16.idempresa=a48.idempresa left join aportes091 a91 on a21.idparentesco=a91.iddetalledef left join aportes091 b91 on b91.iddetalledef=a15.idestadocivil	left join aportes015 b15 on b15.idpersona=a21.idbeneficiario
			where ('".$estadoTrabajador."'='I' or  '".$estadoTrabajador."'='T') and a16.fecharetiro =(select max(b16.fecharetiro) from aportes017 b16 where b16.idpersona=a16.idpersona)and  a16.idpersona not in (select distinct idpersona  from aportes016 b16 where b16.idpersona=a16.idpersona)and a48.nit='".$nit."' and (a21.estado='".$estadoBeneficiario."' or '".$estadoBeneficiario."'='T') and ((a21.idparentesco=".$conyuge." and a21.conviven='S') or a21.idparentesco in (".$tipoBeneficiario.")) ";	
			
			$fecha=date('Ymd');
			$archivo='reporte_004'.$fecha.'.csv';
			$path_plano.='planos'.DIRECTORY_SEPARATOR.$archivo;
			$cadena = "nit;identificacion T;nombre T;fecha afiliacion T;fecha retiro T;estado T;estado civil T;genero T;fecha nacimiento T;edad T;ident beneficiario;nombre B;fecha nac B;edad B;parentesco B;estado B;fecha estado B;genero B;Giro\r\n";
	
			$rs=$db->querySimple($sql);
			while (($row=$rs->fetch())==true){
				//$row = array_map('utf8_decode', $row);
				
				$cadena .= "$row[nit];$row[identificacion];$row[papellido] $row[sapellido] $row[pnombre] $row[snombre];$row[fechaingreso];$row[fecharetiro];$row[estado];$row[detalledefinicion];$row[sexo];$row[fechanacimiento];$row[edad];$row[identifibenef];$row[papellbenef] $row[sapellbenef] $row[pnombrebenef] $row[snombrebenef];$row[nacimibenef];$row[edadbenef];$row[parenbenef];$row[estbenef];$row[fechaestadobenef];$row[sexobenef];$row[giro]"."\r\n";
				
			}
			
			$fp=fopen($path_plano,'w');
			fwrite($fp, $cadena);
			fclose($fp);
			$_SESSION['ENLACE']=$path_plano;
			$_SESSION['ARCHIVO']=$archivo;
			echo  1;
			break;
		case "2":
			$data = array("descargar"=>"N","error"=>0,"descripcion"=>"","empresa"=>"");
			$nit = $_POST['v1'];
			//query para saber si la empresa existe
			$sql="select idempresa from aportes048 where nit='".$nit."'";
			$rs=$db->querySimple($sql);
	
			if($row = $rs->fetch()){
				$data["empresa"][0]=$row['idempresa'];
			}else{
				$data["error"]=1;
				$data["descripcion"]="No se encontro ninguna empresas con el nit ".$nit;
			}
			if($data["error"]==0){
				//crear el archivo 
				$archivo="reporteAfiliadosPorEmpresa_$nit.csv";
				$path =$path_plano.$archivo;
				$cadena="nomafiliado, apeafiliado,idafiliado, dirafiliado, telafiliado, emaafiliado,salario ,nombeneficiario, apebeneficiario, idbeneficiario, edabeneficiario, parbeneficiario\r\n";
				$handle=fopen($path, "w");
				fwrite($handle, $cadena);
				fclose($handle);
				
				
				$sql="select distinct a15.pnombre+' '+isnull(a15.snombre,'') as nomafiliado,a15.papellido+' '+isnull(a15.sapellido,'') as apeafiliado,a15.identificacion as idafiliado,a15.direccion as dirafiliado,a15.telefono as telafiliado,a15.email as emaafiliado,a16.salario ,  	b15.pnombre+' '+isnull(b15.snombre,'')as nombeneficiario,b15.papellido+' '+isnull(b15.sapellido,'') as apebeneficiario,b15.identificacion as idbeneficiario,b15.edad as edaBeneficiario,a91.detalledefinicion
					from aportes016 a16
						inner join aportes015 a15 on a15.idpersona=a16.idpersona 	left join aportes021 a21 on a21.idtrabajador=a16.idpersona  	inner join aportes015 b15 on b15.idpersona=a21.idbeneficiario inner join aportes091 a91 on a91.iddetalledef=a21.idparentesco	where a16.idempresa=".$data["empresa"][0]." and a16.estado='A' and a21.estado='A'";
				$rs=$db->querySimple($sql);
				$con=0;
				$enlace=$path;
				
				while ($row=$rs->fetch()){
					$con++;
					$row = array_map('trim', $row);
					$row = array_map('utf8_decode', $row);
					$cadena=implode(',',$row);
					$cadena=$cadena."\r\n";
					$handle=fopen($path, "a");
					fwrite($handle, $cadena);
					fclose($handle);
					
					
					$_SESSION['ENLACE']=$enlace;
					$_SESSION['ARCHIVO']=$archivo;
					$data["descargar"]="S";
					
				}
				
				if($con==0){
					$data["error"]=1;
					$data["descripcion"]="La empresa no tiene afiliados ";
				}
			}
			echo json_encode($data);
			break;
	}
?>