<?php
/* autor:       orlando puentes
 * fecha:       28/09/2010
 * objetivo:    

if ( !isset( $_SESSION ['USUARIO'] ) ) {session_start();}
$raiz=$_SESSION['RAIZ'];
chdir($raiz);
*/
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$usuario=$_SESSION['CEDULA'];
include_once $raiz.DIRECTORY_SEPARATOR ."config.php";
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Men&uacute; Reportes Empresa</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link type="text/css" href="../../../css/Estilos.css" rel="stylesheet"/>
</head>
<body>
	<center>
	<table width="100%" border="0">
	<tr>
	<td align="center" ><img src="../../../imagenes/logo_reporte.png"></td>
	</tr>
	<tr>
	<td align="center" >Listado de Reportes Empresa</td>
	</tr>
	</table>
	<br>
	<table width="60%" border="0" cellspacing="1" class="tablero">
		<tr>
			<th width="16%">Reporte</th>
			<th width="84%" colspan="2">Descripci&oacute;n</th></tr>  
	<tr>
	   <td scope="row" align="left">Reporte001</td>             
	   <td align="left"><a href="configReporteEmpresa.php?tipo=3&tit=1" target="_new">Directorio de Empresas Activas - Agencia</a></td>
    </tr>
	<tr>
	   <td scope="row" align="left">Reporte002</td>
	   <td align="left"><a href="configReporteEmpresa.php?tipo=200&tit=200" target="_new">Empresas Excentas (Ley 1429)</a></td>            
	   <!--  <td align="left">Empresas Excentas (Ley 1429) <a href="http://:8080/sigasReportes/aportes/empresa/reporte002.jsp?v0= //echo $usuario; " style="text-decoration:none"><img src="../../../imagenes/pdf16x16.png" width="16" height="16"></a> <a href="http://:8080/sigasReportes/aportes/empresa/rptExcel002.jsp?v0= //echo $usuario; ?>" style="text-decoration:none"> <img src="../../../imagenes/excel16x16.png" width="16" height="16"></a></td>-->
    </tr>
	<tr>
	   <td scope="row" align="left">Reporte003</td>             
	   <td align="left"><a href="configReporteEmpresa.php?tipo=4&tit=3&op=1" target="_new">Listado Afiliados por Empresa</a></td>
    </tr>		
	
	<tr>
	  <td>Reporte004</td>
	  <td><a href="configReporteEmpresa.php?tipo=8&tit=8&op=8" target="_new">Listado Afiliados y Beneficiarios por Empresa</a></td>
	 </tr>
	<tr>
		<td>Reporte005</td>
		<td><a href="configReporteEmpresa.php?tipo=5&tit=5" target="_new">Informe Aportes SENA e ICBF primeros diez dias</a></td>
	</tr>
	<tr>
		<td>Reporte006</td>
		<td><a href="configReporteEmpresa.php?tipo=6&tit=6" target="_new"></a></td>
	</tr>
	<tr>
		<td>Reporte007</td>
		<td><a href="configReporteEmpresa.php?tipo=7&tit=7" target="_new">Listado empresa nuevas</a></td>
	</tr>
    <tr>
		<td>Reporte008</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>Reporte009</td>
		<td></td>
	</tr>
	<tr>
		<td>Reporte011</td>
		<td><a href="configReporteEmpresa.php?tipo=7&tit=11" target="_new">Listado de Afiliaciones de nuevas empresas</a></td>
	</tr>
		<tr>
			<td>Reporte012</td>
			<td><a href="configReporteEmpresa.php?tipo=12&tit=12" target="_new">Listado pago aportes por rango de fecha</a> </td>
		</tr>
		<tr>
			<td>Reporte013</td>
			<td><a href="configReporteEmpresa.php?tipo=13&tit=13" target="_new">Listado pago aportes de empresas nuevas</a> </td>
		</tr>
		<tr>
			<td>Reporte014</td>
			<td><a href="configReporteEmpresa.php?tipo=14&tit=14" target="_new">Listado de afiliados y grupo familiar por empresa</a></td>
		</tr>
	<tr>
		<td>Reporte015</td>
		<td><a href="configReporteEmpresa.php?tipo=10&tit=12" target="_new">Resumen Aportes</a></td>
	</tr>
	<tr>
		<td>Reporte016</td>
		<td><a href="configReporteEmpresa.php?tipo=16&tit=16" target="_new">Devoluciones por periodo</a></td>
	</tr>
	<tr>
		<td>Reporte017</td>
		<td><a href="configReporteEmpresa.php?tipo=17&tit=17" target="_new">Carta aceptaci&oacute;n empresas nuevas</a></td>
	</tr>
	<tr>
		<td>Reporte019</td>
		<td><a href="configReporteEmpresa.php?tipo=19&tit=19" target="_new">Listado Recaudos Parafiscales</a></td>
	</tr>
	<tr>
		<td>Reporte020</td>
		<td><a href="configReporteEmpresa.php?tipo=20&tit=20" target="_new">Listado Aportes Parafiscales Fuera de Pila UGPP</a></td>
	</tr>
	<tr>
		<td>Reporte021</td>
		<td><a href="configReporteEmpresa.php?tipo=21&tit=21" target="_new">Devoluciones por Afiliado.</a></td>
	</tr>
	<tr>
		<td>Reporte022</td>
		<td><a href="configReporteEmpresa.php?tipo=22&tit=22" target="_new">UGPP Devoluciones.</a></td>
	</tr>
	<tr>
		<td>Reporte023</td>
		<td><a href="configReporteEmpresa.php?tipo=23&tit=23" target="_new">Recaudos de aportes parafiscales.</a></td>
	</tr>
	<tr>
		<td>Reporte024</td>
		<td><a href="configReporteEmpresa.php?tipo=24&tit=24" target="_new">Informe de empresas nuevas y su primer aporte.</a></td>
	</tr>
	<tr>
		<td>Reporte025</td>
		<td><a href="configReporteEmpresa.php?tipo=25&tit=25" target="_new">Informe del Subsidio Familiar y Aportes.</a></td>
	</tr>
	<tr>
		<td>Reporte027</td>
		<td><a href="configReporteEmpresa.php?tipo=27&tit=27" target="_new">Informe Distribuci&oacute;n de subsidios monetarios pagado.</a></td>
	</tr>
	<tr>
		<td>Reporte028</td>
		<td><a href="configReporteEmpresa.php?tipo=28&tit=28" target="_new">Informe comparativo recaudo de aportes 4%.</a></td>
	</tr>
	<tr>
		<td>Reporte029</td>
		<td><a href="configReporteEmpresa.php?tipo=29&tit=29" target="_new">Informe estado comparativo del subsidio y aportes.</a></td>
	</tr>
	<tr>
		<td>Reporte030</td>
		<td><a href="configReporteEmpresa.php?tipo=30&tit=30" target="_new">Informe empresas inactivas.</a></td>
	</tr>
	<tr>
		<td>Reporte031</td>
		<td><a href="configReporteEmpresa.php?tipo=31&tit=31" target="_new">Informe empresas suspendidas.</a></td>
	</tr>
	<tr>
		<td>Reporte032</td>
		<td><a href="configReporteEmpresa.php?tipo=32&tit=32" target="_new">Informe empresas desafiliadas.</a></td>
	</tr>
	<tr>
		<td>Reporte033</td>
		<td><a href="configReporteEmpresa.php?tipo=33&tit=33" target="_new">Carta aviso de incumplimiento.</a></td>
	</tr>
	<tr>
		<td>Reporte034</td>
		<td><a href="configReporteEmpresa.php?tipo=34&tit=34" target="_new">Informe notificaciones de la empresa.</a></td>
	</tr>
	<tr>
		<td>Reporte035</td>
		<td><a href="configReporteEmpresa.php?tipo=35&tit=35" target="_new">Carta notificaci&oacute;n de expulsi&oacute;n al Consejo Directivo.</a></td>
	</tr>
	<tr>
		<td>Reporte036</td>
		<td><a href="configReporteEmpresa.php?tipo=36&tit=36" target="_new">Carta notificaci&oacute;n de expulsi&oacute;n a la empresa.</a></td>
	</tr>
	<tr>
		<td>Reporte037</td>
		<td><a href="configReporteEmpresa.php?tipo=37&tit=37" target="_new">Informe de las afiliaciones inactivas por la expulsi&oacute;n o activas por la re afiliaci&oacute;n de la empresa.</a></td>
	</tr>
    <tr>
		<td>Reporte038</td>
		<td><a href="configReporteEmpresa.php?tipo=38&tit=38" target="_new">Consolidado de Empresas Nuevas y Afiladas.</a></td>
	</tr>
	<tr>
		<td>Reporte039</td>
		<td><a href="configReporteEmpresa.php?tipo=39&tit=39" target="_new">Carta de Afiliaciones Pendientes por PU.</a></td>
	</tr>
	<tr>
		<td>Reporte040</td>
		<td><a href="<?php echo $ruta_reportes;?>aportes/empresa/rptExcel040.jsp?v0=<?php echo $usuario; ?>" target="_new">Informe Empresas Agricolas.</a></td>
	</tr>
	<tr>
		<td>Reporte041</td>
		<td><a href="<?php echo $ruta_reportes;?>aportes/empresa/rptExcel041.jsp?v0=<?php echo $usuario; ?>" target="_new">Informe Empresas Estado P Y PU.</a></td>
	</tr>
	<tr>
		<td>Reporte044</td>
		<td><a href="<?php echo $ruta_reportes;?>aportes/empresa/rptExcel042.jsp?v0=<?php echo $usuario; ?>" target="_new">Informe Empresas Ley 1429 Pagan 4%</a></td>
	</tr>
	<tr>
		<td>Reporte045</td>
		<td><a href="configReporteEmpresa.php?tipo=45&tit=45" target="_new">Informe historias de la modificaciones empresas Ley 1429</a></td>
	</tr>
	<tr>
		<td>Reporte046</td>
		<td><a href="configReporteEmpresa.php?tipo=46&tit=46" target="_new">Informe consolidado valor de aportes de las empresas ley 1429</a></td>
	</tr>
	<tr>
		<td>Reporte047</td>
		<td><a href="configReporteEmpresa.php?tipo=47&tit=47" target="_new">Cartas para las empresas ley 1429 con Incosistencias</a></td>
	</tr>
	<tr>
		<td>Reporte048</td>
		<td><a href="configReporteEmpresa.php?tipo=40&tit=40" target="_new">Informe de Incosistencias empresas ley 1429</a></td>
	</tr>
		<tr>
		<td>Reporte049</td>
		<td><a href="configReporteEmpresa.php?tipo=41&tit=41" target="_new">Informe Renovaci&oacute;n Camara de Comercio - empresas ley 1429</a></td>
	</tr>
	<tr>
		<td>Reporte050</td>
		<td align="left">Empresas Inactivas Con Causal de Expulsion <a href="<?php echo $ruta_reportes; ?>aportes/empresa/rptExcel048.jsp?v0=<?php echo $usuario; ?>" style="text-decoration:none"> <img src="../../../imagenes/excel16x16.png" width="16" height="16"></a></td>
	</tr>
	<tr>
		<td>Reporte051</td>
		<td><a href="configReporteEmpresa.php?tipo=48&tit=48" target="_new">Listado Empresas que pagan con indice menos del 4%</a></td>
	</tr>
    <tr>
		<td>Reporte052</td>
		<td><a href="configReporteEmpresa.php?tipo=49&tit=49" target="_new">Listado Empresas y sus trabajadores con variaciones en d&iacute;as cotizados y aportes</a></td>
	</tr>
	<tr>
		<td>Reporte053</td>
		<td><a href="<?php echo $ruta_reportes;?>aportes/empresa/rptExcel053.jsp?v0=<?php echo $usuario; ?>" target="_new">Base de Datos ERP</a></td>
	</tr>
	<tr>
		<td>Reporte054</td>
		<td><a href="configReporteEmpresa.php?tipo=54&tit=54" target="_new">Listado de empresa con trabajadores con indice de aporte menor o igual al 4%</a></td>
	</tr>
	</table>
    <br>
	</center>
</body> 
</html>
