/*
* Autor: Orlando Puentes Andrade
* Objetivo: Archivo para informe mensual de aportes
* Fecha: 24-Mar-2011
* ----------- E M P R E S A -------------- 
*/

// funcion para obtener cookie en javascript
	function getCookie(c_name)
	{
		var i,x,y,ARRcookies=document.cookie.split(";");
		for (i=0;i<ARRcookies.length;i++)
		{
  			x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
  			y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
  			x=x.replace(/^\s+|\s+$/g,"");
  			if (x==c_name)
    			{
    				return unescape(y);
    			}// fin if
		}// fin for
  	}// fin getCookie
//funcion para obtener cookie en javascript
	
URL=src();
$(document).ready(function(){
	
	//Asignar datepicker al elemento id=txtFechaLetra
	$( "#txtFechaLetra" ).datepicker( { "dateFormat":"d 'de' MM 'de' yy" });
	
	/***********************SCRIPT INDEX***********************************/
	$('#txtFechaI,#txtFechaF24').datepicker({
		maxDate:"+0D",
		changeMonth: true,
		changeYear: true,
		onSelect: function(dateText, inst) {
			var lockDate = new Date($('#txtFechaI').datepicker('getDate'));
			$('input#txtFechaF').datepicker('option', 'minDate', lockDate);
		}
	});
	$('#fechaI').datepicker({
		maxDate:"+0D",
		changeMonth: true,
		changeYear: true,
		onSelect: function(dateText, inst) {
			var lockDate = new Date($('#fechaI').datepicker('getDate'));
			$('input#fechaF').datepicker('option', 'minDate', lockDate);
		}
	});
	$("#fechaF").datepicker({
		maxDate:"+0D",
		changeMonth: true,
		changeYear: true
	});
	$("#txtFechaPagoIni").datepicker({
		maxDate:"+0D",
		changeMonth: true,
		changeYear: true,
		onSelect:function(date){
		$('#txtFechaPagoFin').val(date);
		$('#txtPeriodoIni').val('');
		$('#txtPeriodoFin').val('');
		$('#txtFechaPagoFin').datepicker("option","minDate",new Date(date));
		}
	});
	$("#txtFechaPagoFin").datepicker({
		maxDate:"+0D",
		changeMonth: true,
		changeYear: true,
		onSelect:function(date){
			$('#txtPeriodoIni').val('');
			$('#txtPeriodoFin').val('');
		}
	});
	$("#txtFechaF").datepicker({
		maxDate:"+0D",
		changeMonth: true,
		changeYear: true
	});
	
	$('#txtFechaRI').datepicker({
		maxDate:"+0D",
		changeMonth: true,
		changeYear: true,
		onSelect: function(dateText, inst) {
			var lockDate = new Date($('#txtFechaRI').datepicker('getDate'));
			$('input#txtFechaRF').datepicker('option', 'minDate', lockDate);
		}
	});
	
	$("#txtFechaRF").datepicker({
		maxDate:"+0D",
		changeMonth: true,
		changeYear: true
	});
   
    $("#txtFecha").datepicker({
        dateFormat: 'yy-mm',
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        onClose: function(dateText, inst) {
            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            $(this).datepicker('setDate', new Date(year, month, 1));
        }
    }); 
        
	$("#txtFecha").focus(function () {
		$(".ui-datepicker-calendar").hide();
		$("#ui-datepicker-div").position({
			my: "center top",
			at: "center bottom",
			of: $(this)
		});
	});

	$("#txtPeriodo,#txtPeriodo2,#txtPeriodo1,#txtPeriodoFin").datepicker({
        dateFormat: 'yymm',
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        onClose: function(dateText, inst) {
       	    var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            $(this).datepicker('setDate', new Date(year, month, 1));
            }
        }); 
	$("#txtPeriodoIni").datepicker({
        dateFormat: 'yymm',
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        onClose: function(dateText, inst) {
        	var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            $(this).datepicker('setDate', new Date(year, month, 1));
            $('#txtPeriodoFin').datepicker('setDate', new Date(year, month, 1));
            $('#txtFechaPagoIni').val('');
    		$('#txtFechaPagoFin').val('');
            }
        }); 

	$("#txtPeriodo24").datepicker({
        dateFormat: 'yymm',
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        onClose: function(dateText, inst) {
        	var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            $(this).datepicker('setDate', new Date(year, month, 1));
            $("#txtFechaI24").val( year + "/01/01");
       	}
    });
    	
	$("#txtPeriodo,#txtPeriodo2,#txtPeriodo1,#txtPeriodo24,#txtPeriodoIni,#txtPeriodoFin").focus(function () {
		$(".ui-datepicker-calendar").hide();
		$("#ui-datepicker-div").position({
			my: "center top",
			at: "center bottom",
			of: $(this)
		});
	}); 

	$("#anno").css('display','none');
	$("#rangoPeriodo").css('display','none');
	$("#rangoAnno").css('display','none');
	/*****************************************************/
	
	$("#txtFechaI,#txtFechaF").datepicker({
		changeMonth: true,
		changeYear: true,
		maxDate: "+0D"
	});

	$("#cmbFiltro").change(function(){			
		$("#fechaini").css('display','none');
		$("#fechafin").css('display','none');
		
		var filtro = $("#cmbFiltro").val();		
		
		switch(filtro){
			case 'anno':
				$("#fechaini").css('display','block');
				$("#txtfini").datepicker("destroy");
				$("#txtfini").val("");
				
				$("#txtfini").datepicker({
			        dateFormat: 'yy',
			        changeYear: true,
			        changeMonth:false,
			        showButtonPanel: true,
			        duration: 'fast', 
			        stepMonths: 0,
			        onClose: function(dateText, inst) {			        	
			            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
			            $(this).datepicker('setDate', new Date(year, 1, 1));
			            }
			        }); 
			        
				$("#txtfini").focus(function () {
					$(".ui-datepicker-calendar").hide();
					$("#ui-datepicker-div").position({
						my: "center top",
						at: "center bottom",
						of: $(this)
					});
				});				
			break;
			
			case 'rangoPeriodo':
				$("#fechaini").css('display','block');
				$("#fechafin").css('display','block');				
				$("#txtfini").datepicker("destroy");
				$("#txtffin").datepicker("destroy");
				$("#txtfini").val("");
				$("#txtffin").val("");
				
				$("#txtfini,#txtffin").datepicker({
					dateFormat: 'yymm',
			        changeMonth: true,
			        changeYear: true,
			        showButtonPanel: true,
			        onClose: function(dateText, inst) {
			        	var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
			            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
			            $(this).datepicker('setDate', new Date(year, month, 1));
			        }
				});
				
				$("#txtfini,#txtffin").focus(function () {
					$(".ui-datepicker-calendar").hide();
					$("#ui-datepicker-div").position({
						my: "center top",
						at: "center bottom",
						of: $(this)
					});
				});
			break;
			
			case 'rangoAnno':				
				$("#fechaini").css('display','block');
				$("#fechafin").css('display','block');				
				$("#txtfini").datepicker("destroy");
				$("#txtffin").datepicker("destroy");
				$("#txtfini").val("");
				$("#txtffin").val("");
				
				
				$("#txtfini, #txtffin").datepicker({
			        dateFormat: 'yy',
			        changeYear: true,
			        changeMonth:false,
			        showButtonPanel: true,
			        duration: 'fast', 
			        stepMonths: 0,
			        onClose: function(dateText, inst) {			        	
			            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
			            $(this).datepicker('setDate', new Date(year, 1, 1));
			            }
			        }); 
			        
				$("#txtfini, #txtffin").focus(function () {
					$(".ui-datepicker-calendar").hide();
					$("#ui-datepicker-div").position({
						my: "center top",
						at: "center bottom",
						of: $(this)
					});
				});				
							
			break;
		}		
	});
	//CONTROLAR LOS CAMPOS DEL TIPO DE FILTRO EN EL REPORTE DE EMPRESAS
	$( "#cmbTipoFiltroCarta" ).change(function(){
		if( $( this ).val() == "nit" ){
			$( "#trNit" ).show();
			$( "#trFechas" ).hide();
			$( "#txtFechaI,#txtFechaF" ).val("");
		}else{
			$( "#trNit" ).hide();
			$( "#trFechas" ).show();
			$( "#txtNit" ).val("");
		}
	});
	
	//CONTROLAR LOS CAMPOS DEL REPORTE003
	$( "#cmbEstado" ).change(function(){
		var estado = $(this).val();
		$("#tdFechaIngreso input, #tdFechaRetiro input").val("");
		if(estado=="A" || estado=="P"){
			$("#tdFechaIngreso").show();
			$("#tdFechaRetiro").hide();
		}else if(estado=="I"){
			$("#tdFechaIngreso").hide();
			$("#tdFechaRetiro").show();
		}else{
			$("#tdFechaIngreso").show();
			$("#tdFechaRetiro").show();
		}
	});
	
});

function procesar3(op){
	var seccional=$("#txtSeccional").val();
	var usuario=$("#txtusuario").val();
	if(op==1){
		if($("#txtRept").val()==1)
			url="http://"+getCookie("URL_Reportes")+"/aportes/empresa/reporte001.jsp?v0="+seccional+"&v1="+usuario;
	
	}
	else{
		if($("#txtRept").val()==1)
			url="http://"+getCookie("URL_Reportes")+"/aportes/empresa/rptExcel001.jsp?v0="+seccional+"&v1="+usuario;
		
		}
	window.open(url,"_NEW");
	}

function procesar2(op){
	var seccional=$("#txtSeccional").val();
	var usuario=$("#txtusuario").val();
	var url ="";
	if(op==1){
			url="http://"+getCookie("URL_Reportes")+"/aportes/empresa/reporte002.jsp?v0="+seccional+"&v1="+usuario;
	}
	else{
			url="http://"+getCookie("URL_Reportes")+"/aportes/empresa/rptExcel002.jsp?v0="+seccional+"&v1="+usuario;
		
		}
	window.open(url,"_NEW");
	}


function procesar4(op){
	var nit=$("#txtNit").val();

	if($("#txtRept").val()!=4){
		var estado=$("#cboEstado").val();
        if(estado ==""){
            alert("Debe selecionar el estado");
            return false;
        }
    }

	var usuario=$("#txtusuario").val();
	if(op==1){
		if($("#txtRept").val()==3){
			url="http://"+getCookie("URL_Reportes")+"/aportes/empresa/rptExcel003.jsp?v0="+nit+"&v1="+usuario+"&v2="+estado;
		}
		if($("#txtRept").val()==4){
			url="http://"+getCookie("URL_Reportes")+"/aportes/empresa/rptExcel004.jsp?v0="+nit+"&v1="+usuario;
		}
	window.open(url,"_NEW");
	}
	
}

function procesar5(op){
	var fecha=$("#txtFecha").val();			
	var numero = $("#txtNumeroComun").val().trim();
	
	if(numero==""){
		alert("Debe digitar el n\u00FAmero comunicaci\u00F3n interna");
		return;
	}
	if(fecha != 0){		
		var fecha1 = fecha.substring(0,4);
		var anno = fecha1;		
		fecha = fecha.substring(5,7);
		var mesNumero = fecha;
		var mes = "";		
		var usuario=$("#txtusuario").val();		

		switch(mesNumero){
		case '01': mes = "Enero"; break; 
		case '02': mes = "Febrero"; break; 
		case '03': mes = "Marzo"; break; 
		case '04': mes = "Abril"; break; 
		case '05': mes = "Mayo"; break; 
		case '06': mes = "Junio"; break; 
		case '07': mes = "Julio"; break; 
		case '08': mes = "Agosto"; break; 
		case '09': mes = "Septiembre"; break; 
		case '10': mes = "Octubre"; break; 
		case '11': mes = "Noviembre"; break; 
		case '12': mes = "Diciembre"; break; 
		}
		
		//alert(anno + ' - ' + mes + ' - ' + mesNumero);
		//url="../secretaria/generar.php?archivo=6&modulo=0&identificacion=0&v1="+anno+"&v2="+mesNumero+"&v3="+mes+"&v4="+usuario;
		url="http://"+getCookie("URL_Reportes")+"/aportes/empresa/reporte005.jsp?v0="+numero+"&v1="+anno+"&v2="+mesNumero+"&v3="+mes+"&v4="+usuario;
		window.open(url,"_NEW");
		
	}
	else
		alert("Debe ingresar un Periodo");
}
function procesar6(){
	var fechaInicial=$("#txtFechaI").val();	
	var fechaFinal=$("#txtFechaF").val();
	var nit = $("#txtNit").val();
	if(fechaInicial == "" || fechaFinal == "" || nit==""){
		alert("Debe digitar todos los campos");
		return false;
	}
	url="http://"+getCookie("URL_Reportes")+"/aportes/empresa/reporte007.jsp?v0="+nit+"&v1="+fechaInicial+"&v2="+fechaFinal;
	window.open(url,"_BLANK");
}

function procesar7(){

	var url;
	var nit=$("#txtNit").val();
	var estado = $("#cmbEstado").val();
	var fechaI = $("#txtFechaI").val();
	var fechaF = $("#txtFechaF").val();
	var fechaRI = $("#txtFechaRI").val();
	var fechaRF = $("#txtFechaRF").val();
	var usuario = $("#txtusuario").val();
	
	$("input").removeClass("ui-state-error");
	if(nit==0){$("#txtNit").addClass("ui-state-error");return false;}
	if(estado==0){$("#cmbEstado").addClass("ui-state-error");return false;}
	
	var data = "v0="+nit+"&v1="+usuario+"&v2="+estado+"&v3="+fechaI+"&v4="+fechaF+"&v5="+fechaRI+"&v6="+fechaRF;
	url="http://"+getCookie("URL_Reportes")+"/aportes/empresa/rptExcel003.jsp?"+data;
	window.open(url,"_NEW");
}

function procesar8(op){
	
	//var url;
	var agencia=$("#cmbAgencia").val();
	var fechaI= $("#txtFechaI").val();
	var fechaF= $("#txtFechaF").val();
	if(fechaI == "" || fechaF == "" || agencia=="0"){
		alert("Debe digitar todos los campos");
		return false;
	}
	
	if(op==1){
		url="http://"+getCookie("URL_Reportes")+"/aportes/empresa/rptExcel008.jsp?v0="+agencia+"&v1="+fechaI+"&v2="+fechaF;
	} else {
		url="http://"+getCookie("URL_Reportes")+"/aportes/empresa/reporte011.jsp?v0="+agencia+"&v1="+fechaI+"&v2="+fechaF;
	}	
	
	window.open(url,"_BLANK");
}

function procesar9(){ 
	var nit                = $("#txtNit").val();
	var estadoTrabajador   = $("#cmbEstadoTrabajador").val(); 
	var tipoBeneficiario   = "";
	var estadoBeneficiario = $("#cmbEstadoBeneficiario").val();
	var conyuge            = 0;
	
	if (nit=="") {
		alert("Debe digitar el nit");
		return false;
	}	
	if(estadoTrabajador==""){
		alert("Debe seleccionar el estado del trabajador");
		return false;
	}
	 
	$("#idTipoBeneficiario :checkbox:checked").each(function(i,row){
		if(row.value==34)
			conyuge = row.value;
		else
			tipoBeneficiario += row.value+",";
	});

	if(tipoBeneficiario=="" && conyuge==0){
		alert("Debe seleccionar el tipo de beneficiario");
		return false;
	}

	if(estadoBeneficiario==""){
		alert("Debe seleccionar el estado del beneficiario");
		return false;
	}

	$("#idProcesar004").html("<img src='../../../imagenes/ajax-loader.gif'/>");
	
	$.ajax({
		url:"formarArchivo.php",
		type:"POST",
		data:{v0:1,v1:nit, v2:estadoTrabajador, v3:tipoBeneficiario, v4:estadoBeneficiario,v5:conyuge},
		dataType:"json",
		async:false,
		success:function(datos){
			if(datos==1){
				$("#idProcesar004").html("<img src='../../../imagenes/descargarArchivo.png' onclick='descargar();'/>");
			}
		}
	});
}

function procesar10(op){
	var nit=$("#txtNit").val();
	var fini=$("#txtfini").val();
	var filtro = $("#cmbFiltro").val();
	var usuario = $("#txtusuario").val();
	
	if (nit=="")
	{
		alert("Debe ingresar un nit");
		return false;
	}
	
	if (filtro=="nada")
	{
		alert("Debe seleccionar un filtro de fecha");
		return false;
	}
	
	if (fini=="")
	{
		alert("Debe ingresar una fecha Inicial");
		return false;
	}	
	var ffin;
	
	if (filtro == 'anno')
		ffin=fini;
	else
		ffin=$("#txtffin").val();
	
	if (ffin=="")
	{
		alert("Debe ingresar una fecha Final");
		return false;
	}
	
	if (fini>ffin)
		alert("Fecha inicial superior a la final");
	
	
	if(op==0){		
			url="http://"+getCookie("URL_Reportes")+"/aportes/empresa/reporte009.jsp?v0="+nit+"&v1="+fini+"&v2="+ffin+"&v3="+usuario;
	
	}
	else{		
			url="http://"+getCookie("URL_Reportes")+"/aportes/empresa/rptExcel009.jsp?v0="+nit+"&v1="+fini+"&v2="+ffin+"&v3="+usuario;
		
		}
	window.open(url,"_NEW");	
}

function procesar11(op){
	var fini=$("#txtFechaI").val();
	var ffin=$("#txtFechaF").val();
	var usuario = $("#txtusuario").val();
	
	if (fini=="")
	{
		alert("Debe ingresar una fecha Inicial");
		return false;
	}	
	if (ffin=="")
	{
		alert("Debe ingresar una fecha Final");
		return false;
	}	
	
	if (fini>ffin)
	{
		alert("Fecha inicial superior a la final");
		return false;
	}
	
	
	if(op==0){		
			url="http://"+getCookie("URL_Reportes")+"/aportes/empresa/reporte014.jsp?v0="+fini+"&v1="+ffin+"&v2="+usuario;
	
	}
	else{		
			url="http://"+getCookie("URL_Reportes")+"/aportes/empresa/rptExcel014.jsp?v0="+fini+"&v1="+ffin+"&v2="+usuario;
		
		}
	window.open(url,"_NEW");	
}

function procesar12(){
	var usuario = $("#txtusuario").val();
	var fechaInicial = $("#txtFechaInicial").val();
	var fechaFinal = $("#txtFechaFinal").val();
	if(fechaInicial=="" || fechaFinal==""){
		alert("Debe indicar las dos fechas");
		return false;
	}
	url="http://"+getCookie("URL_Reportes")+"/aportes/empresa/rptExcel012.jsp?v0="+fechaInicial+"&v1="+fechaFinal+"&v2="+usuario;
	window.open(url,"_NEW");
}

function procesar13(){
	var usuario = $("#txtusuario").val();
	var fechaInicial = $("#txtFechaI").val();
	var fechaFinal = $("#txtFechaF").val();
	var periodo = $("#txtPeriodo").val();
	if(fechaInicial=="" || fechaFinal==""){
		alert("Debe indicar las dos fechas");
		return false;
	}
	if(periodo==""){
		alert("Debe indicar el periodo");
		return false;
	}
	url="http://"+getCookie("URL_Reportes")+"/aportes/empresa/rptExcel013.jsp?v0="+fechaInicial+"&v1="+fechaFinal+"&v2="+usuario+"&v3="+periodo;
	window.open(url,"_NEW");
}

function procesar14(){
	var nit = $("#txtNitEmpresa").val().trim();
	//limpia el contenido del anterios reporte
	$("#tdMensajeError").html('');
	if(nit=="")
		return false;
	$("#idTdBottonDescarga").html("<img src='../../../imagenes/ajax-loader.gif'/>");
	$.ajax({
		url:'formarArchivo.php',
		type:"POST",
		data:{v0:"2",v1:nit},
		dataType:"json",
		async:true,
		success: function(datos){
			if(datos["error"]==1){
				$("#tdMensajeError").html(datos["descripcion"]);
				
			}else if(datos["descargar"]=="S"){
				descargar();
			}
			$("#txtNitEmpresa").val("").focus();
			$("#idTdBottonDescarga").html('<label style="cursor:pointer" onClick="procesar14();"><img src="../../../imagenes/descargarArchivo.png" /></label>');
		}
	});
}

function procesar16(opt){	
	var periodo = $("#txtPeriodo").val();
	
	if(periodo==""){
		alert("Debe indicar el periodo");
		return false;
	}
	if(opt==1){
		url="http://"+getCookie("URL_Reportes")+"/aportes/empresa/reporte016.jsp?v0="+periodo;
	}else{
		url="http://"+getCookie("URL_Reportes")+"/aportes/empresa/rptExcel016.jsp?v0="+periodo;
	}
	window.open(url,"_NEW");
}

function procesar17(op){ 
	var fini=$("#txtFechaI").val();
	var ffin=$("#txtFechaF").val();
	var comunicacion = $("#txtComunicacion").val().trim();
	var tipoAfiliacion = $("#cmbTipoAfiliacion").val().trim();
	var tipoFiltro = $("#cmbTipoFiltroCarta").val().trim();
	var nit = $( "#txtNit" ).val().trim();
	var fechaLetra = $( "#hidFechaLetra" ).val().trim();
	var agencia = $("#cmbAgencia").val();
	
	if (comunicacion==""){alert("Debe ingresar el n\u00FAmero de la comunicaci\u00F3n");return false;}
	if (tipoAfiliacion==""){alert("Debe seleccionar el tipo de afiliaci\u00FAn");return false;}
		
	if ( tipoFiltro=="fechas" && fini=="" ){alert("Debe ingresar la fecha Inicial");return false;} 		
	if ( tipoFiltro=="fechas" && ffin=="" ){alert("Debe ingresar la fecha Final");return false;}
	if ( tipoFiltro=="nit" && nit=="" ){alert("Debe ingresar el nit");return false;}
	if ( tipoFiltro == "fechas" && fini>ffin ){alert("Fecha inicial superior a la final");return false;}
			
	url="http://"+getCookie("URL_Reportes")+"/aportes/empresa/reporte017.jsp?v0="+fini+"&v1="+ffin+"&v2="+tipoAfiliacion+"&v3="+comunicacion+"&v4="+nit+"&v5="+fechaLetra+"&v6="+agencia;
	window.open(url,"_NEW");	
}
function procesar19(){
	
	var periodo1 = $("#txtPeriodo").val();
	var periodo2 = $("#txtPeriodo2").val();
	var agencia = $("#cmbAgencia").val();
	var usuario = $("#txtusuario").val();
	
	if(periodo1=="" || periodo2==""){
		
		alert("Debe indicar el periodo");
		return false;
	}
	url="http://"+getCookie("URL_Reportes")+"/aportes/empresa/rptExcel019.jsp?v0="+periodo1+"&v1="+periodo2+"&v2="+agencia+"&v3="+usuario;
	window.open(url,"_NEW");
	
}

function procesar20(){
	
	var periodo1 = $("#txtFechaI").val();
	var periodo2 = $("#txtFechaF").val();
	var usuario = $("#txtusuario").val();
	
	if(periodo1=="" || periodo2==""){
		
		alert("Debe indicar el rango de fechas");
		return false;
	}
	url="http://"+getCookie("URL_Reportes")+"/aportes/empresa/rptExcel020.jsp?v0="+periodo1+"&v1="+periodo2+"&v2="+usuario;
	window.open(url,"_NEW");
	
}

function procesar21(op){
	var usuario = $("#txtusuario").val();
	var nit = $("#txtNit").val().trim();
	var filtro = parseInt( $("#cmbOpcionFiltro").val() );
	var periodoI = $("#txtPeriodo1").val();
	var periodoF = $("#txtPeriodo2").val();
	var fechaI = $("#txtFechaI").val().trim();
	var fechaF = $("#txtFechaF").val().trim();
	var tipoIdentificacion = $("#cmbIdentificacion").val();
	var identificacion = $("#txtIdentificacion").val().trim();
	
	if (filtro==2 && (periodoI=="" || periodoF=="") && (periodoI != "" || periodoI!="")){
		alert("Digite los dos Periodos");
		return false;
	}else if (filtro==3 && (fechaI == "" || fechaF == "")  && (fechaI != "" || fechaF!="") ){
		alert("Digite las Dos Fechas");
		return false;
	}else if( $("#txtPeriodo").val().trim() != "" ){
		periodoI = $("#txtPeriodo").val().trim();
	}
	
	var data = "v0="+usuario+"&v1="+ nit +"&v2="+ fechaI +"&v3="+ fechaF +"&v4="+ periodoI +"&v5="+ periodoF +"&v6="+ tipoIdentificacion +"&v7="+identificacion;
	if(op=="PDF"){
		url="http://"+getCookie("URL_Reportes")+"/aportes/empresa/reporte021.jsp?"+data;
		
	}else if(op=="EXC"){
		url="http://"+getCookie("URL_Reportes")+"/aportes/empresa/rptExcel021.jsp?"+data;
	}
	window.open(url,"_NEW");
}

function procesar22(op){
	var fechaI = $("#txtFechaI").val().trim();
	var fechaF = $("#txtFechaF").val().trim();
	
	if( fechaI == "" || fechaF == "" ){
		alert("Digite las fechas");
		return false;
	}
	
	var data = "v0=" + fechaI + "&v1=" + fechaF;
	if(op=="PDF"){
		url="http://"+getCookie("URL_Reportes")+"/aportes/empresa/reporte022.jsp?"+data;
		
	}else if(op=="EXC"){
		url="http://"+getCookie("URL_Reportes")+"/aportes/empresa/rptExcel022.jsp?"+data;
	}
	window.open(url,"_NEW");
}

function procesar23(op){
	var usuario = $("#txtusuario").val();
	var fechaI = $("#txtFechaI").val().trim();
	var fechaF = $("#txtFechaF").val().trim();
	
	if( fechaI == "" ){	alert("Debe indicar la fecha inicial.");return false;}
	if( fechaF == "" ){	alert("Debe indicar la fecha final.");return false;}
	
	if( parseInt( fechaI.replace('/','').substring(0,6) ) != parseInt( fechaF.replace('/','').substring(0,6) ) ){
		alert("Las fechas debe ser del mismo mes.");
		return false;
	}
	
	var periodo = mesLetra( fechaI.replace('/','').substring(0,6) ) + " DE " + fechaI.replace('/','').substring(0,4);
	
	var data = "v0=" + usuario + "&v1=" + periodo.toUpperCase() + "&v2=" + fechaI + "&v3=" + fechaF;
	if(op=="PDF"){
		url="http://"+getCookie("URL_Reportes")+"/aportes/empresa/reporte023.jsp?"+data;
		
	}else if(op=="EXC"){
		url="http://"+getCookie("URL_Reportes")+"/aportes/empresa/rptExcel023.jsp?"+data;
	}
	window.open(url,"_NEW");
}

function procesar24(op){
	var usuario = $("#txtusuario").val();
	var periodo = $("#txtPeriodo24").val().trim();
	var fechaI = $("#txtFechaI24").val().trim();
	var fechaF = $("#txtFechaF24").val().trim();
	
	if( periodo == ""  ){alert("Debe indicar el Periodo.");return false;}
	if( fechaI == "" ){	alert("Debe indicar la fecha inicial.");return false;}
	if( fechaF == "" ){	alert("Debe indicar la fecha final.");return false;}
	
	var periodoTexto = mesLetra( periodo ) + " de " + periodo.substring( 0,4 );
	periodoTexto = periodoTexto.toUpperCase();
	
	var data =  "v0=" + usuario + "&v1=" + periodo + "&v2=" + fechaI + "&v3=" + fechaF + "&v4=" + periodoTexto;
			
	if(op=="PDF"){
		url="http://"+getCookie("URL_Reportes")+"/aportes/empresa/reporte024.jsp?"+data;
		
	}else if(op=="EXC"){
		url="http://"+getCookie("URL_Reportes")+"/aportes/empresa/rptExcel024.jsp?"+data;
	}
	window.open(url,"_NEW");
}

function procesar25(op){
	var numeroComunicacion = $("#txtNumeroComunicacion").val().trim();
	var usuario = $("#txtusuario").val();
	var periodo = $("#txtPeriodo").val().trim();
	var fechaI = $("#txtFechaI").val().trim();
	var fechaF = $("#txtFechaF").val().trim();
	
	if( numeroComunicacion == "" ){	alert("Digite el n\u00FAmero de la comunicaci\u00F3n.");return false;}
	if( periodo == ""  ){alert("Debe indicar el Periodo.");return false;}
	if( fechaI == "" ){	alert("Debe indicar la fecha inicial.");return false;}
	if( fechaF == "" ){	alert("Debe indicar la fecha final.");return false;}
	if( parseInt( fechaI.replace('/','').substring(0,6) ) != parseInt(periodo) || parseInt( fechaF.replace('/','').substring(0,6) ) != parseInt(periodo)  ){
		alert("Los meses de las fechas deben deben ser iguales al mes del periodo.");
		return false;
	}
	
	var  fechaMesAnterior = function( fecha ){

		var today = new Date( fecha );
		dias = fecha.substring(8,10);
		fechaDiaMesAnterior = new Date(today.getTime() - (dias * 24 * 3600 * 1000)),
		anno = fechaDiaMesAnterior.getFullYear();
		mes = parseInt( fechaDiaMesAnterior.getMonth() ) + 1;
		mes = ( mes < 10 ) ? "0" + mes : mes;
		dia = fechaDiaMesAnterior.getDate();
		fechaNueva = anno + "/" + mes + "/" + dia ;
		return fechaNueva;
	}
	
	var periodoTexto = mesLetra( periodo ) + " de " + periodo.substring( 0,4 );
	var fechaAnterior = fechaMesAnterior( fechaF );
	
	var mesA = parseInt( periodo.substring( 4,6 ) );
	var periodoAnterior = "";
	
	if ( mesA > 1 ){
		mesA = ( mesA-1 );
		mesA = ( mesA < 10 ? "0" + mesA : mesA );
		periodoAnterior = periodo.substring( 0,4 ) + "" + mesA; 
	}else{
		periodoAnterior = ( parseInt( periodo.substring( 0,4 ) ) -1 ) + "12";
	}
	
	$.ajax({
		url: "prepararReporte025.php",
		type: "POST",
		data: {periodo: periodo, fechaI: fechaI, fechaF: fechaF, fechaAnterior: fechaAnterior, periodoAnterior: periodoAnterior},
		dataType:"json",
		async:false,
		success: function(datos){
			var data = "recaudo=" + datos.recaudo + "&totalNovedades=" + datos.totalNovedades + "&provision=" + datos.provision; 
			data += "&totalRecaudo=" + datos.totalRecaudo + "&aportesFacultativos=" + datos.aportesFacultativos;
			data += "&aportesPensionados=" + datos.aportesPensionados + "&totalAportes=" + datos.totalAportes;
			data += "&totalAcumulado=" + datos.totalAcumulado + "&acumulado=" + datos.acumulado;
			data += "&periodo=" + periodo + "&periodoTexto=" + periodoTexto + "&fechaI=" + fechaI + "&fechaF=" + fechaF; 
			data += "&bandera=" + datos.bandera + "&numeroComunicacion=" + numeroComunicacion + "&fechaSistema=" + fechaHoy();
			
			if(op=="PDF"){
				url="http://"+getCookie("URL_Reportes")+"/aportes/empresa/reporte025.jsp?"+data;
				
			}else if(op=="EXC"){
				url="http://"+getCookie("URL_Reportes")+"/aportes/empresa/rptExcel025.jsp?"+data;
			}
			window.open(url,"_NEW");
		}
	});	
}


function procesar27(op){
	var usuario = $("#txtusuario").val();
	var periodo = $("#txtPeriodo").val().trim();
	var tabla = $("#cmbTabla").val().trim();
	
	if( periodo == ""  ){alert("Debe indicar el Periodo.");return false;}
	
	var periodoTexto = mesLetra( periodo ) + " de " + periodo.substring( 0,4 );
	periodoTexto = periodoTexto.toUpperCase();
	
	var data =  "v0=" + usuario + "&v1=" + periodo + " &v2=" + periodoTexto + "&v3=" + tabla; 
			
	if(op=="PDF"){
		url="http://"+getCookie("URL_Reportes")+"/aportes/empresa/reporte027.jsp?"+data;
		
	}else if(op=="EXC"){
		url="http://"+getCookie("URL_Reportes")+"/aportes/empresa/rptExcel027.jsp?"+data;
	}
	window.open(url,"_NEW");
}


function procesar28(op){
	
	var usuario = $("#txtusuario").val();
	var periodo = $("#txtFecha").val().trim();
	
	if( periodo == ""  ){alert("Debe indicar el Periodo.");return false;}
	
	var arrayPeriodo = periodo.split( "-" );
	var anno = parseInt( arrayPeriodo[0] );
	
	var periodoA = ( anno-1 ) + "-12";
	var periodoD = ( anno-2 ) + "-12";
	
	var periodoB = periodo;
	var periodoE = ( anno-1 ) + "-" + arrayPeriodo[1];
	
	var periodoC = "";
	var periodoF = "";
	
	var periodoTexto = mesLetra( periodo.replace("-","") ).toUpperCase() + "-" + anno;
	
	var mesA =  parseInt( arrayPeriodo[1] );
		
	if ( mesA > 1 ){
		mesA = ( mesA-1 );
		mesA = ( mesA < 10 ? "0" + mesA : mesA );
		periodoC = anno + "-" + mesA;
		periodoF = ( anno-1 ) + "-" + mesA;
	}
	
	var data =  "v0=" + periodoA +
				"&v1=" + periodoB +
				"&v2=" + periodoC +
				"&v3=" + periodoD +
				"&v4=" + periodoE +
				"&v5=" + periodoF +
				"&v6=" + periodoTexto;
			
	if(op=="PDF"){
		url="http://"+getCookie("URL_Reportes")+"/aportes/empresa/reporte028.jsp?"+data;
		
	}else if(op=="EXC"){
		url="http://"+getCookie("URL_Reportes")+"/aportes/empresa/rptExcel028.jsp?"+data;
	}
	window.open(url,"_NEW");
}

function procesar29(op){
	
	var usuario = $("#txtusuario").val();
	var periodoB = $("#txtPeriodo").val().trim();
	var tabla = $("#cmbTabla").val();
	if( periodoB == ""  ){alert("Debe indicar el Periodo.");return false;}
	
	var periodoA ,
		anno = parseInt( periodoB.substring(0,4) ),
		periodoTexto = mesLetra( periodoB.replace("-","") ).toUpperCase() + " DE " + anno,
		mesA =  parseInt( periodoB.substring(4,6) );
			
	if ( mesA > 1 ){
		mesA = ( mesA-1 );
		mesA = ( mesA < 10 ? "0" + mesA : mesA );
		periodoA = anno + "" + mesA;
	}else{		
		periodoA = ( anno - 1 ) + "12";
	}
	
	var data =  "v0=" + periodoA +
				"&v1=" + periodoB +
				"&v2=" + periodoTexto + 
				"&v3=" + tabla;
			
	if(op=="PDF"){
		url="http://"+getCookie("URL_Reportes")+"/aportes/empresa/reporte029.jsp?"+data;
		
	}else if(op=="EXC"){
		url="http://"+getCookie("URL_Reportes")+"/aportes/empresa/rptExcel029.jsp?"+data;
	}
	window.open(url,"_NEW");
}

function procesar30(op){
	var usuario = $("#txtusuario").val();
	var periodo = $("#txtPeriodo24").val().trim();
	var fechaI = $("#txtFechaI24").val().trim();
	var fechaF = $("#txtFechaF24").val().trim();
	
	if( periodo == ""  ){alert("Debe indicar el Periodo.");return false;}
	if( fechaI == "" ){	alert("Debe indicar la fecha inicial.");return false;}
	if( fechaF == "" ){	alert("Debe indicar la fecha final.");return false;}
	
	var periodoTexto = mesLetra( periodo ) + " de " + periodo.substring( 0,4 );
	periodoTexto = periodoTexto.toUpperCase();
	
	var data =  "v0=" + usuario + "&v1=" + periodo + "&v2=" + fechaI + "&v3=" + fechaF + "&v4=" + periodoTexto;
			
	if(op=="PDF"){
		url="http://"+getCookie("URL_Reportes")+"/aportes/empresa/reporte030.jsp?"+data;
		
	}else if(op=="EXC"){
		url="http://"+getCookie("URL_Reportes")+"/aportes/empresa/rptExcel030.jsp?"+data;
	}
	window.open(url,"_NEW");
}

function procesar31(op){
	var usuario = $("#txtusuario").val();
	var nit = $("#txtNit").val().trim();
	var claseAportante = $("#cmbClaseAportante").val();
	var periodo = $("#txtPeriodo").val().trim();
	var estado = $("#cmbEstadoSuspension").val();
	var fechaSuspensionI = $("#txtFechaI").val().trim();
	var fechaSuspensionF = $("#txtFechaF").val().trim();
	
	$(".ui-state-error").removeClass("ui-state-error").addClass("box1");
	if( (fechaSuspensionI && !fechaSuspensionF) || (!fechaSuspensionI && fechaSuspensionF) ){
		if(!fechaSuspensionI)
			$("#txtFechaI").removeClass("box1").addClass("ui-state-error").focus();
		else 
			$("#txtFechaF").removeClass("box1").addClass("ui-state-error").focus()
		return false;
	}
	
	var data =  "v0=" + usuario + "&v1=" + nit + "&v2=" + claseAportante 
		+ "&v3=" + periodo + "&v4=" + estado + "&v5=" + fechaSuspensionI + "&v6=" + fechaSuspensionF;
			
	if(op=="PDF"){
		url="http://"+getCookie("URL_Reportes")+"/aportes/empresa/reporte031.jsp?"+data;
		
	}else if(op=="EXC"){
		url="http://"+getCookie("URL_Reportes")+"/aportes/empresa/rptExcel031.jsp?"+data;
	}
	window.open(url,"_BLANK");
}

function procesar32(op){
	var usuario = $("#txtusuario").val();
	var nit = $("#txtNit").val().trim();
	var estadoEmpresa = $("#cmbEstadoEmpresa").val();
	var estadoDesafiliacion = $("#cmbEstadoDesafiliacion").val();
	var fechaDesafiliacionI = $("#txtFechaI").val().trim();
	var fechaDesafiliacionF = $("#txtFechaF").val().trim();
	
	$(".ui-state-error").removeClass("ui-state-error").addClass("box1");
	if( (fechaDesafiliacionI && !fechaDesafiliacionF) || (!fechaDesafiliacionI && fechaDesafiliacionF) ){
		if(!fechaDesafiliacionI)
			$("#txtFechaI").removeClass("box1").addClass("ui-state-error").focus();
		else 
			$("#txtFechaF").removeClass("box1").addClass("ui-state-error").focus()
		return false;
	}
	
	var data =  "v0=" + usuario + "&v1=" + nit + "&v2=" + estadoEmpresa 
		+ "&v3=" + estadoDesafiliacion + "&v4=" + fechaDesafiliacionI + "&v5=" + fechaDesafiliacionF;
			
	if(op=="PDF"){
		url="http://"+getCookie("URL_Reportes")+"/aportes/empresa/reporte032.jsp?"+data;
		
	}else if(op=="EXC"){
		url="http://"+getCookie("URL_Reportes")+"/aportes/empresa/rptExcel032.jsp?"+data;
	}
	window.open(url,"_BLANK");
}

function procesar33(op){
	var comunicacion = $("#txtComunicacion").val().trim();
	var nombreUsuario = $("#txtNombreUsuario").val();
	var nit = $("#txtNit").val().trim();
	var claseAportante = $("#cmbClaseAportante").val();
	var fechaSuspensionI = $("#txtFechaI").val();
	var fechaSuspensionF = $("#txtFechaF").val().trim();
	var sexoUsuario = $("#txtSexoUsuario").val();
	
	//Formato para el nombre del usuario
	patron = /\s+/;
	var arrNombre = nombreUsuario.split(patron);
	var nombreUsuario = "";
	for(i=0, len=arrNombre.length;i<len;i++){
		nombreUsuario += arrNombre[i].charAt(0).toUpperCase()+''+arrNombre[i].slice(1).toLowerCase()+" "; 
	}
	
	$(".ui-state-error").removeClass("ui-state-error").addClass("box1");
	if(!comunicacion){
		$("#txtComunicacion").addClass("ui-state-error").removeClass("box1");
		return false;
	}/*else{
		$("#txtComunicacion").removeClass("ui-state-error");
	}*/
	if( (fechaSuspensionI && !fechaSuspensionF) || (!fechaSuspensionI && fechaSuspensionF) ){
		if(!fechaSuspensionI)
			$("#txtFechaI").removeClass("box1").addClass("ui-state-error").focus();
		else 
			$("#txtFechaF").removeClass("box1").addClass("ui-state-error").focus()
		return false;
	}
	
	/* **** Prepara para guardar el control de la notificacion **** */
	var objDatos = {
			reporte:"EMPRESA-Reporte033",
			datos:{
				nit:nit,
				clase_aportante:claseAportante,
				fecha_suspensio_i:fechaSuspensionI,
				fecha_suspensio_f:fechaSuspensionF
			}
		};
	if(controlNotificacion(objDatos)==0)
		return false;
	
	/* **** Generar la carta **** */
	var data =  "v0=" + comunicacion + "&v1=" + nombreUsuario + "&v2=" + nit 
		+ "&v3=" + claseAportante + "&v4=" + fechaSuspensionI + "&v5=" + fechaSuspensionF
		+ "&v6=" + sexoUsuario;
			
	if(op=="PDF"){
		url="http://"+getCookie("URL_Reportes")+"/aportes/empresa/reporte033.jsp?"+data;	
	}
	window.open(url,"_BLANK");
}

function procesar34(op){
	var usuario = $("#txtusuario").val();
	var nit = $("#txtNit").val().trim();
	var cartaNotificacion = $("#cmbCartaNotificacion").val();
	var estadoNotificacion = $("#cmbEstadoNotificacion").val();
	var fechaNotificacionI = $("#txtFechaI").val();
	var fechaNotificacionF = $("#txtFechaF").val().trim();
	
	$(".ui-state-error").removeClass("ui-state-error").addClass("box1");
	if( (fechaNotificacionI && !fechaNotificacionF) || (!fechaNotificacionI && fechaNotificacionF) ){
		if(!fechaNotificacionI)
			$("#txtFechaI").removeClass("box1").addClass("ui-state-error").focus();
		else 
			$("#txtFechaF").removeClass("box1").addClass("ui-state-error").focus()
		return false;
	}
	
	/* **** Generar la carta **** */
	var data =  "v0=" + usuario + "&v1=" + nit + "&v2=" + cartaNotificacion 
		+ "&v3=" + estadoNotificacion + "&v4=" + fechaNotificacionI + "&v5=" + fechaNotificacionF;
			
	if(op=="PDF"){
		url="http://"+getCookie("URL_Reportes")+"/aportes/empresa/reporte034.jsp?"+data;
		
	}else if(op=="EXC"){
		url="http://"+getCookie("URL_Reportes")+"/aportes/empresa/rptExcel034.jsp?"+data;
	}
	window.open(url,"_BLANK");
}

function procesar35(op){
	var comunicacion = $("#txtComunicacion").val().trim();
	var nombreUsuario = $("#txtNombreUsuario").val();
	var nit = $("#txtNit").val().trim();
	var fechaExpulsionI = $("#txtFechaI").val();
	var fechaExpulsionF = $("#txtFechaF").val();
	
	$(".ui-state-error").removeClass("ui-state-error").addClass("box1");
	if(!comunicacion){
		$("#txtComunicacion").addClass("ui-state-error").removeClass("box1");
		return false;
	}/*else{
		$("#txtComunicacion").removeClass("ui-state-error");
	}*/
	
	if( (fechaExpulsionI && !fechaExpulsionF) || (!fechaExpulsionI && fechaExpulsionF) ){
		if(!fechaExpulsionI)
			$("#txtFechaI").removeClass("box1").addClass("ui-state-error").focus();
		else 
			$("#txtFechaF").removeClass("box1").addClass("ui-state-error").focus()
		return false;
	}
	
	/* **** Prepara para guardar el control de la notificacion **** */
	var objDatos = {
			reporte:"EMPRESA-Reporte035",
			datos:{
				nit:nit,
				fecha_i:fechaExpulsionI,
				fecha_f:fechaExpulsionF
			}
		};
	if(controlNotificacion(objDatos)==0)
		return false;
	
	/* **** Generar la carta **** */
	var data =  "v0=" + comunicacion + "&v1=" + nombreUsuario + "&v2=" + nit 
		+ "&v3=" + fechaExpulsionI + "&v4=" + fechaExpulsionF;
			
	if(op=="PDF"){
		url="http://"+getCookie("URL_Reportes")+"/aportes/empresa/reporte035.jsp?"+data;	
	}
	window.open(url,"_BLANK");
}

function procesar36(op){
	var comunicacion = $("#txtComunicacion").val().trim();
	var nombreUsuario = $("#txtNombreUsuario").val();
	var nit = $("#txtNit").val().trim();
	var numeroActa = $("#txtNumeroActa").val().trim();
	var fechaActa = $("#txtFechaLetra").val();
	var fechaEstadEmpreI = $("#txtFechaI").val();
	var fechaEstadEmpreF = $("#txtFechaF").val();
	
	$(".ui-state-error").removeClass("ui-state-error").addClass("box1");
	if(!comunicacion){$("#txtComunicacion").addClass("ui-state-error").removeClass("box1");return false;}
	if(!numeroActa){$("#txtNumeroActa").addClass("ui-state-error").removeClass("box1");return false;}
	if(!fechaActa){$("#txtFechaLetra").addClass("ui-state-error");return false;}
	
	//valida campos
	if( (fechaEstadEmpreI && !fechaEstadEmpreF) || (!fechaEstadEmpreI && fechaEstadEmpreF) ){
		if(!fechaEstadEmpreI)
			$("#txtFechaI").removeClass("box1").addClass("ui-state-error").focus();
		else 
			$("#txtFechaF").removeClass("box1").addClass("ui-state-error").focus()
		return false;
	}
	
	/* **** Prepara para guardar el control de la notificacion **** */
	var objDatos = {
			reporte:"EMPRESA-Reporte036",
			datos:{
				nit:nit,
				fecha_i:fechaEstadEmpreI,
				fecha_f:fechaEstadEmpreF
			}
		};
	if(controlNotificacion(objDatos)==0)
		return false;
	
	/* **** Generar la carta **** */
	var data =  "v0=" + comunicacion + "&v1=" + nombreUsuario + "&v2=" + nit 
		+ "&v3=" + fechaEstadEmpreI + "&v4=" + fechaEstadEmpreF + "&v5=" + numeroActa + "&v6=" + fechaActa;
			
	if(op=="PDF"){
		url="http://"+getCookie("URL_Reportes")+"/aportes/empresa/reporte036.jsp?"+data;	
	}
	window.open(url,"_BLANK");
}


function procesar37(opt){
	var usuario = $("#txtusuario").val(),
		nit = $("#txtNit").val().trim(),
		fechaI = $("#txtFechaI").val(),
		fechaF = $("#txtFechaF").val(),
		data = null,
		url = null;
	
	//valida campos
	$(".ui-state-error").removeClass("ui-state-error").addClass("box1");
	if( (fechaI && !fechaF) || (!fechaI && fechaF) ){
		if(!fechaI)
			$("#txtFechaI").removeClass("box1").addClass("ui-state-error").focus();
		else 
			$("#txtFechaF").removeClass("box1").addClass("ui-state-error").focus()
		return false;
	}
	
	//Parametros del reporte
	data = "v0="+usuario+"&v1="+nit+"&v2="+fechaI+"&v3="+fechaF;
	if(opt=="PDF"){
		url="http://"+getCookie("URL_Reportes")+"/aportes/empresa/reporte037.jsp?";
	}else{
		url="http://"+getCookie("URL_Reportes")+"/aportes/empresa/rptExcel037.jsp?";
	}
	url += data;
	window.open(url,"_BLANK");
}

function procesar39(op){
	var nombreUsuario = $("#txtNombreUsuario").val();
	var agencia = $("#cmbAgencia").val();
	var fechaIngresoI = $("#txtFechaI").val();
	var fechaIngresoF = $("#txtFechaF").val().trim();
	//var tipoFiltro = $("#cmbTipoFiltroPU").val().trim();
	var comunicacion = $("#txtComunicacion").val().trim();
	var nit = $( "#txtNit" ).val().trim();
	var periodo = $( "#txtPeriodo" ).val().trim();
	
	
	if (comunicacion==""){alert("Debe ingresar el n\u00FAmero de la comunicaci\u00F3n");return false;}
	if (fechaIngresoI>fechaIngresoF ){alert("Fecha inicial superior a la final");return false;}
	
	/* **** Generar la carta **** */
	var data =  "&v0=" + nombreUsuario + "&v1=" + agencia + "&v2=" + fechaIngresoI + "&v3=" + fechaIngresoF + "&v4=" + comunicacion + "&v5=" + nit + "&v6=" + periodo;
			
	if(op=="PDF"){
		url="http://"+getCookie("URL_Reportes")+"/aportes/empresa/reporte038.jsp?"+data;	
	}
	window.open(url,"_BLANK");
}

/**MANDA EL REPORTE A EXEL**/
function procesar40(){

var usuario=$("#txtusuario").val();
var fechaIngresoI = $("#txtFechaI").val();
var fechaIngresoF = $("#txtFechaF").val().trim();

if (fechaIngresoI>fechaIngresoF ){alert("Fecha inicial superior a la final");return false;}
if (fechaIngresoI==""){alert("Debe ingresar la fecha inicial");return false;}
//-------------------------------------------------------------------------------------------------------------
$.ajax({
	//buscamos empresas ley 1429 con inconsistencias
	url:URL+'aportes/empresas/empresasNOgiroLey1429.php', 
	type:"POST", 
	data: {v0:fechaIngresoI,v1:fechaIngresoF},
	beforeSend: function(objeto){
    	dialogLoading('show');
    },        
    complete: function(objeto, exito){
    	dialogLoading('close');
        if(exito != "success"){
            alert("No se completo el proceso!");
        }            
    },
    contentType: "application/x-www-form-urlencoded",
	dataType:"json",
	async:false,
	success:function(datos){
		dialogLoading("hide");
   		if(datos==0){
			alert("No hay empresas con inconsistencias en este rango de fechas.");
		    return false;
		 } else {
			url="http://"+getCookie("URL_Reportes")+"/aportes/empresa/rptExcel043.jsp?v0="+usuario+ "&v1=" + fechaIngresoI + "&v2=" + fechaIngresoF;
			//alert(url);
			window.open(url,"_NEW");
		 }
	}//succes	 
});//ajax
//-------------------------------------------------------------------------------------------------------------
}

function procesar45(){

	var usuario=$("#txtusuario").val();
	var nit = $( "#txtNit" ).val().trim();
	var fechaIngresoI = $("#txtFechaI").val();
	var fechaIngresoF = $("#txtFechaF").val().trim();

	if (fechaIngresoI>fechaIngresoF ){alert("Fecha inicial superior a la final");return false;}

		url="http://"+getCookie("URL_Reportes")+"/aportes/empresa/rptExcel044.jsp?v0="+usuario+ "&v1=" + nit+ "&v2=" + fechaIngresoI + "&v3=" + fechaIngresoF;
		//alert(url);
	window.open(url,"_NEW");

	}

function procesar46(){

	var usuario=$("#txtusuario").val();
	var fechaIngresoI = $("#txtFechaI").val();
	var fechaIngresoF = $("#txtFechaF").val().trim();
	
	if (fechaIngresoI==""){alert("Debe ingresar la fecha inicial");return false;}
	if (fechaIngresoI>fechaIngresoF ){alert("Fecha inicial superior a la final");return false;}

		url="http://"+getCookie("URL_Reportes")+"/aportes/empresa/rptExcel045.jsp?v0="+usuario+ "&v1=" + fechaIngresoI + "&v2=" + fechaIngresoF;
		//alert(url);
	window.open(url,"_NEW");

	}


function procesar47(op){
	var nombreUsuario = $("#txtNombreUsuario").val();
	var fechaIngresoI = $("#txtFechaI").val();
	var fechaIngresoF = $("#txtFechaF").val().trim();
	var comunicacion = $("#txtComunicacion").val().trim();
	var nit = $( "#txtNit" ).val().trim();
	
	
	if (comunicacion==""){alert("Debe ingresar el n\u00FAmero de la comunicaci\u00F3n");return false;}
	if (fechaIngresoI==""){alert("Debe ingresar la fecha inicial");return false;}
	if (fechaIngresoI>fechaIngresoF ){alert("Fecha inicial superior a la final");return false;}
	
	/* **** Generar la carta **** */
	var data =  "&v0=" + nombreUsuario + "&v1=" + fechaIngresoI + "&v2=" + fechaIngresoF + "&v3=" + comunicacion + "&v4=" + nit;
			
	if(op=="PDF"){
		url="http://"+getCookie("URL_Reportes")+"/aportes/empresa/reporte046.jsp?"+data;	
	}
	window.open(url,"_BLANK");
}

function procesar48(){

	var usuario=$("#txtusuario").val();
	var nit = $( "#txtNit" ).val().trim();
	var fechaRenovacionI = $("#txtFechaI").val();
	var fechaRenovacionF = $("#txtFechaF").val().trim();

	if (fechaRenovacionI>fechaRenovacionF ){alert("Fecha inicial superior a la final");return false;}

		url="http://"+getCookie("URL_Reportes")+"/aportes/empresa/rptExcel046.jsp?v0="+usuario+ "&v1=" + nit+ "&v2=" + fechaRenovacionI + "&v3=" + fechaRenovacionF;
		//alert(url);
	window.open(url,"_NEW");

	}



function procesar49(op){
	var fini=$("#txtPeriodo").val();
	var ffin=$("#txtPeriodo2").val();
	var usuario = $("#txtusuario").val();
	
	if (fini=="")
	{
		alert("Debe ingresar el periodo Inicial");
		return false;
	}	
	if (ffin=="")
	{
		alert("Debe ingresar el periodo Final");
		return false;
	}	
	
	if (fini>ffin)
	{
		alert("periodo inicial superior a la final");
		return false;
	}
	
	
	if(op==0){		
			url="http://"+getCookie("URL_Reportes")+"/aportes/empresa/reporte049.jsp?v0="+fini+"&v1="+ffin+"&v2="+usuario;
	
	}
	else{		
			url="http://"+getCookie("URL_Reportes")+"/aportes/empresa/rptExcel049.jsp?v0="+fini+"&v1="+ffin+"&v2="+usuario;
		
		}
	window.open(url,"_NEW");	
}


function procesar50(op){
	var fini=$("#txtFechaI").val();
	var ffin=$("#txtFechaF").val();
	var usuario = $("#txtusuario").val();
	
	if (fini=="")
	{
		alert("Debe ingresar una fecha Inicial");
		return false;
	}	
	if (ffin=="")
	{
		alert("Debe ingresar una fecha Final");
		return false;
	}	
	
	if (fini>ffin)
	{
		alert("Fecha inicial superior a la final");
		return false;
	}
	
	var cmbValor=$("#cmbValor").val();	
	
	if(op==0){		
			url="http://"+getCookie("URL_Reportes")+"/aportes/empresa/reporte050.jsp?v0="+fini+"&v1="+ffin+"&v2="+usuario+"&v3="+cmbValor;
	
	}
	else{		
			url="http://"+getCookie("URL_Reportes")+"/aportes/empresa/rptExcel050.jsp?v0="+fini+"&v1="+ffin+"&v2="+usuario+"&v3="+cmbValor;
		
		}
	window.open(url,"_NEW");	
	
}


function procesar54(){
	var indice= $("#indice").val();
	var periodoIni= $("#txtPeriodoIni").val();
	var periodoFin= $("#txtPeriodoFin").val();
	var fechaPagoIni= $("#txtFechaPagoIni").val();
	var fechaPagoFin= $("#txtFechaPagoFin").val();
	$("#tablatempo tbody").empty();
	if (periodoIni=="" && fechaPagoIni=="")
	{
		alert("Debe indicar ya sea el periodo incial y final o la fecha de pago inicial y final");
		return false;
	}
	var conten="";
	var validador=0;
	$("input[type=checkbox]").each(function(i,row){
		if(row.checked){
			 if (validador==0){
				 validador=1;
				 conten=row.value; 
			 }else{
				 conten+="*"+row.value; 
			 }  
		   }
	});
	if(validador == 0 ){
		alert('Debe seleccionar almenos una de las opciones de Clase aportante');	
		return false;
	}
	    
	
	dialogLoading('show');
	$.ajax({
		type:"POST",
		url: URL+"phpComunes/consultaempresasreport054.php",
		data:{indice:indice,periodoIni:periodoIni,periodoFin:periodoFin,fechaPagoIni:fechaPagoIni,fechaPagoFin:fechaPagoFin,claseAportante:conten},
		async:false,
		timeout:60000,
		dataType:"json",
		success:function(datos){
			if(datos!=0){
				//alert(datos.canreg);
				//alert(datos.sql);
				if(datos.canreg>0){
				   var tablatempo="<tbody>";
				   $.each(datos.contenido,function(index,value){
					     tablatempo+="<tr><td>"+value.nit+"</td><td>"+value.razonsocial+"</td><td>"+value.claseaportante+"</td><td>"+value.municipio+"</td><td>"+value.direccion+"</td><td>"+value.telefono+"</td><td>"+value.email+"</td><td>"+value.identificacion+"</td><td>"+value.nombreafiliado+"</td><td>"+value.tarifaaporte+"</td><td>"+value.valoraporte+"</td><td>"+value.salariobasico+"</td><td>"+value.ingresobase+"</td><td>"+value.diascotizados+"</td><td>"+value.periodo+"</td><td>"+value.horascotizadas+"</td><td>"+value.fechapago+"</td><td>"+value.tipoafiliacion+"</td><td>"+value.estadoafiliacion+"</td><td>"+value.ingreso+"</td><td>"+value.retiro+"</td><td>"+value.var_tra_salario+"</td><td>"+value.vacaciones+"</td><td>"+value.lic_maternidad+"</td><td>"+value.tipo_cotizante+"</td><td>"+value.correccion+"</td><td>"+value.inc_tem_emfermedad+"</td><td>"+value.inc_tem_acc_trabajo+"</td><td>"+value.novedad_sln+"</td></tr>";
				   });
				   tablatempo += "</tbody>";
				   $("#tablatempo").append(tablatempo);
				   dialogLoading('close');	
					switch(parseInt(indice)){
					   case 0: 
						   var descripindice="TODOS";
						   break;
					   case 1: 
						   var descripindice="MENOR QUE EL 4%";
						   break;
					   case 2: 
						   var descripindice="IGUAL AL 4%";
						   break;
					}

					$("#valindice").html(descripindice);
					
					var f = new Date();
				    $("#tablatempo").table2excel({
				       exclude: ".excludeThisClass",
				       name: "Worksheet Name",
				       filename: "reporte"+f.getFullYear()+f.getMonth()+f.getDate()+f.getHours()+":"+f.getMinutes()+":"+f.getSeconds() //do not include extension
				    });
				}else{
					alert('la consulta retorno cero registros');
					dialogLoading('close');
					return false;
				}
			}
			else{
			  alert("se retornarón 0 registros");
			}
		},
		error : function(xhr, status) {
	        alert('Disculpe, existió un problema ' + status + '  ' + xhr);
	        dialogLoading('close');
	    },
	});
	

}





/**
 * Metodo para guardar el control de notificaciones de la empresa
 * @param objDatos
 * @returns {Number}: 0:Error | 1:Ok
 */
function controlNotificacion(objDatos){
	var resultado = 0;
	$.ajax({
		url:URL+"centroReportes/controlNotifEmpresa.php",
		type:"POST",
		data:{objDatos:objDatos},
		dataType:"json",
		async:false,
		beforeSend: function(objeto){
			dialogLoading('show');
		},
		complete: function(objeto, exito){
			dialogLoading('close');	
		}, 
		success:function(datos){
			if(datos.Error==0)
				resultado = 1;
			else if(datos.Error>0)
				alert(datos.Mensaje);
		}
	});
	return resultado;
}

/*EL FORMATO DEL PERIODO DEBE SER AAAAMM */
function mesLetra( periodo ){
		var mes = parseInt( periodo.substring( 4, 6 ) );
		var mesLetra = "";
		switch (mes){
			case 1:mesLetra="Enero";break;
			case 2:mesLetra="Febrero";break;
			case 3:mesLetra="Marzo";break;
			case 4:mesLetra="Abril";break;
			case 5:mesLetra="Mayo";break;
			case 6:mesLetra="Junio";break;
			case 7:mesLetra="Julio";break;
			case 8:mesLetra="Agosto";break;
			case 9:mesLetra="Septiembre";break;
			case 10:mesLetra="Octubre";break;
			case 11:mesLetra="Noviembre";break;
			case 12:mesLetra="Diciembre";break;
		}
		return mesLetra;
}

//Funcion para reporte mensual de produccion
function rptMensual(){    
	$("#rta").empty();
	$("#nit").removeClass("ui-state-error");
 	
	if($("#fecInicial").val()==''){
    $("#fecInicial").addClass("ui-state-error").focus();
	return false;
	}
	
	if($("#fecFinal").val()==''){
    $("#fecFinal").addClass("ui-state-error").focus();
	return false;
	}
	
	var agencia=$.trim($("#agencia").val());
	var fecI=$("#fecInicial").val();
	var fecF=$("#fecFinal").val();
	
	//Envio los parametros del reporte
	switch($("#opcion").val()){
		case '2' : location.href=URL+"centroReportes/aportes/empresas/reporte002.php?agencia="+agencia+"&fecInicial="+fecI+"&fecFinal="+fecF+""; break;
		case '3' : location.href=URL+"centroReportes/aportes/empresas/reporte003.php?agencia="+agencia+"&fecInicial="+fecI+"&fecFinal="+fecF+""; break;
	}
}

//Funcion para la opcion del filtro del informe
function opcionFiltroRep21(){
	var opcion = parseInt( $( "#cmbOpcionFiltro" ).val() );
	var idTr = "";
	$( "#trPeriodo,#trPeriodos,#trFechas" ).hide();
	$( "#trPeriodo input,#trPeriodos input,#trFechas input" ).val("");
	if( opcion == 1 )
		idTr = "#trPeriodo";
	else if( opcion == 2 )
		idTr = "#trPeriodos";
	else if( opcion == 3 )
		idTr = "#trFechas";
	
	$( idTr ).show(); 
}

function procesarconsolidado(opt){	
	var usuario=$("#txtusuario").val();
	var fechini = $("#fechaI").val();
	var fechfin = $("#fechaF").val();

	if(opt==1){
		url="http://"+getCookie("URL_Reportes")+"/aportes/empresa/rptExcel039.jsp?v0="+usuario+"&v2="+fechini+"&v3="+fechfin;
		
	}else
		{
		url="http://"+getCookie("URL_Reportes")+"/aportes/empresa/reporte039.jsp?v0="+usuario+"&v2="+fechini+"&v3="+fechfin;
		}
		
	window.open(url,"_NEW");
}

function descargar(){
	var url=URL+'phpComunes/descargaPlanos.php';
	window.open(url);
}
