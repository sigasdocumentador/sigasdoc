<?php
/* autor:       orlando puentes
 * fecha:       28/09/2010
 * objetivo:    

if ( !isset( $_SESSION ['USUARIO'] ) ) {session_start();}
$raiz=$_SESSION['RAIZ'];
chdir($raiz);
*/
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$usuario=$_SESSION['CEDULA'];
include_once $raiz.DIRECTORY_SEPARATOR ."config.php";

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Men&uacute; Reportes Cuota Monetaria</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link type="text/css" href="../../../css/Estilos.css" rel="stylesheet"/>

<script language="javascript" src="../../../js/comunes.js"></script>

<link type="text/css" href="../../../css/css.1.8/jquery-ui-1.8.17.custom.css" rel="stylesheet"/>
<link href="../../../css/jMenu.jquery.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../../../js/jquery-1.4.2.js"></script>
<script type="text/javascript" src="../../../js/jquery-ui.js"></script>
<script type="text/javascript" src="../../../js/jMenu.jquery.js"></script> 
<script language="javascript" src="js/reportes.js"></script>
</head>
<body>

<center>
	<table width="100%" border="0">
	<tr>
	<td align="center" ><img src="../../../imagenes/logo_reporte.png"></td>
	</tr>
	<tr>
	<td align="center" >Listado de Reportes Subsidio Cuota Monetaria</td>
	</tr>
	</table>
	<br>
  <table width="60%" border="0" cellspacing="1" class="tablero">
	  <tr>
		  <th width="16%">Reporte</th>
		  <th width="84%" colspan="2">Descripci&oacute;n</th></tr>  
	<tr>
	   <td scope="row" align="left">Reporte001</td>             
	   <td align="left"><a href="configReporteCuota.php?tipo=3&tit=1" target="_new">Directorio de Empresas Activas - Agencia</a></td>
    </tr>
	<tr>
	   <td scope="row" align="left">Reporte002</td>             
	   <td align="left">Empresas Excentas (Ley 1429) <a href="<?php echo $ruta_reportes; ?>aportes/empresa/reporte002.jsp?v0=<?php echo $usuario; ?>" style="text-decoration:none"><img src="../../../imagenes/pdf16x16.png" width="16" height="16"></a> <a href="<?php echo $ruta_reportes?>aportes/empresa/rptExcel002.jsp?v0=<?php echo $usuario; ?>" style="text-decoration:none"> <img src="../../../imagenes/excel16x16.png" width="16" height="16"></a></td>
    </tr>
	<tr>
	  <td>Reporte003</td>
	  <td></td>
  	</tr>  		
	<tr>
	  <td>Reporte004</td>
	  <td align="left"><a href="configReporteCuota.php?tipo=5&tit=5" target="_new">Reporte validaci&oacute;n cuota monetaria</a></td>
    </tr>
	<tr>
		<td>Reporte005</td>
		<td align="left"><a href="configReporteCuota.php?tipo=18&tit=18" target="_new">Reporte Sabana Historico Cuota Monetaria</a></td>
	</tr>
	<tr>
		<td>Reporte006</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>Reporte007</td>
		<td>&nbsp;</td>
	</tr>
    <tr>
		<td>Reporte008</td>
		<td>&nbsp;</td>
	</tr>
    <tr>
		<td>Reporte009</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
        <td>Reporte010</td>
        <td><a href="../../configurarReporte.php?plano=0&tipo=36&rp=1" target="_blank">Informe Consolidado Giro</a></td>
    </tr>
	<tr>
	   <td scope="row" align="left">Reporte047</td>             
	   <td align="left"><a href="configReporteCuota.php?tipo=15&tit=15" target="_new">Valor girado mayor o igual a:</a></td>
    </tr>	
    <tr>
	   <td scope="row" align="left">Reporte048</td>             
	   <td align="left"><a href="configReporteCuota.php?tipo=8&tit=8" target="_new" >Resumen de subsidio pagado</a></td>
    </tr>
    <tr>
        <td>Reporte050</td>
        <td>Afiliados con Susidio Cuota Monetaria por Empresa y Periodo</td>
    </tr>
    <tr>
        <td>Reporte051</td>
        <td><a href="configReporteCuota.php?tipo=6&tit=6" target="_new">Relacion Giro de Subsidio por Empresa</a></td>
    </tr>
    <tr>
        <td>Reporte052</td>
        <td><a href="configReporteCuota.php?tipo=7&tit=7" target="_new">Resumen general de empresas con giro</a></td>
    </tr>
    <tr>
        <td>Reporte053</td>
        <td><a href="configReporteCuota.php?tipo=9&tit=9" target="_new">Listado embargos por periodo en una empresa</a></td>
    </tr>
    <tr>
        <td>Reporte054</td>
        <td><a href="configReporteCuota.php?tipo=10&tit=10" target="_new">Listado pago subsidio por muertos en un periodo</a></td>
    </tr>
	<tr>
        <td>Reporte055</td>
        <td>
            <a href="configReporteCuota.php?tipo=12&tit=12" target="_new">Descuento Por Pignoracion</a>
            <!--<img src="../../../imagenes/pdf16x16.png" onClick="Reporte055('<?php echo $usuario;?>','pdf');" style="text-decoration:none" width="16" height="16"> 
            <img src="../../../imagenes/excel16x16.png" onClick="Reporte055('<?php echo $usuario;?>','exc');" style="text-decoration:none" width="16" height="16">
            -->
            </td>
    </tr>
    <tr>
	   <td scope="row" align="left">Reporte056</td>             
	   <td align="left"><a href="<?php echo $ruta_reportes; ?>aportes/giro/reporte056.jsp?v0=<?php echo $usuario; ?>"  target="_new" >Subsidio Auxilio Funebre Cancelados en el Periodo Actual</a></td>
    </tr>
    <tr>
	   <td scope="row" align="left">Reporte057</td>             
	   <td align="left"><a href="<?php echo $ruta_reportes; ?>aportes/giro/reporte057.jsp?v0=<?php echo $usuario; ?>"  target="_new" >Listado Discapacitados</a></td>
    </tr>
    <tr>
        <td>Reporte058</td>
        <td><a href="configReporteCuota.php?tipo=13&tit=13" target="_new">Recuperaci&oacute;n Subsidio</a></td>
    </tr>
    <tr>
        <td>Reporte059</td>
        <td><a href="configReporteCuota.php?tipo=14&tit=14" target="_new">Listado de afiliados no procesados con novedad de retiro</a></td>
    </tr>
    <tr>
        <td>Informes</td>
        <td><a href="menuInconsistencias.php" target="_new">Informes de inconsistencias en la cuota monetaria</a></td>
    </tr>
    <tr>
        <td>Reporte060</td>
        <td><a href="configReporteCuota.php?tipo=16&tit=16" target="_new">Informe pago del subsidio por embargo o discapacitado.</a></td>
     <tr>
        <td>Reporte061</td>
        <td><a href="configReporteCuota.php?tipo=17&tit=17" target="_new">Listado de Empresas con sus Trabajadores Activos con Giro en Revisión con índice de aporte diferente a 4%.</a></td>
    </tr>
  </table>
  </center>
</body> 
</html>

