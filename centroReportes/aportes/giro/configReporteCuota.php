<?php
/* autor:       orlando puentes
 * fecha:       28/08/2012
 * objetivo:    configuracion de los reportes de radicacion
 **********************************************************
 configuracion
 tipo 
 1. con tipo de radicacion
 2. solo fecha
 3. agencias
 
*/

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$usuario=$_SESSION['USUARIO'];
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
$tipo=$_REQUEST['tipo'];
$titulo='...';
$rept=$_REQUEST['tit'];
switch($rept){
	case 1:$titulo='Directorio de Empresas Activas'; break;
	case 2:$titulo=''; break;
	case 3:$titulo=''; break;
	case 4:$titulo=''; break;
	case 5:$titulo='Reporte validacion cuota monetaria'; break;
	case 6:$titulo='Relacion Giro de Subsidio por Empresa '; break;
	case 7:$titulo='Resumen general de empresas con giro  '; break;
	case 8:$titulo='Resumen subsidio pagado '; break;
	case 9:$titulo='Resumen subsidio pagado por embargos '; break;
	case 10:$titulo='Resumen subsidio pagado por muertos '; break;
	case 11:$titulo='Informe Condiciones de la Cuota a GIRAR'; break;
	case 12:$titulo='Informe Descuento Por Pignoracion'; break;
	case 13:$titulo='Informe Recuperaci&oacute;n Subsidio'; break;
	case 14:$titulo='Listado de afiliados no procesados con novedad de retiro';break;
	case 15:$titulo='Listado planillas de giros para el control de mayor o igual valor';break;
	case 16:$titulo='Informe pago del subsidio por embargo o discapacitado.';break;
	case 17:$titulo='Listado de Empresas con sus Trabajadores Activos con Giro en Revisión con índice de aporte diferente a 4%';break;
	case 18:$titulo='Reporte Historico cuota monetaria';break;
	}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Men&uacute; Reportes Asopagos</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link type="text/css" rel="stylesheet" href="../../../css/css.1.8/jquery-ui-1.8.17.custom.css" />
<link type="text/css" href="../../../css/Estilos.css" rel="stylesheet"/>
<link href="../../../css/marco.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="../../../js/1.6/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="../../../js/1.6/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="../../../js/comunes.js"></script>
<script type="text/javascript" src="js/reportes.js"></script>
<script>
$(function(){
	$("#txtFechaI,#txtFechaF").datepicker({maxDate:"+0D",
		changeMonth: true,
		changeYear: true
	});

	$("#txtPeriodo,#txtPeriodo2").datepicker({
        dateFormat: 'yymm',
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        onClose: function(dateText, inst) {
        	var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            $(this).datepicker('setDate', new Date(year, month, 1));
            }
        }); 
        
	$("#txtPeriodo,#txtPeriodo2").focus(function () {
		$(".ui-datepicker-calendar").hide();
		$("#ui-datepicker-div").position({
			my: "center top",
			at: "center bottom",
			of: $(this)
		});
	});
	
});
</script>
</head>
<body>
	<center>
		<table width="100%" border="0">
			<tr>
				<td align="center" ><img src="../../../imagenes/logo_reporte.png" width="362" height="70"></td>
			</tr>
			<tr>
				<td align="center" ><?php echo $titulo ?></td>
			</tr>
		</table>
		<br/>
		<?php
		if($tipo==1){
			$sql="SELECT iddetalledef,detalledefinicion FROM aportes091 WHERE iddefinicion=6";
			$rs=$db->querySimple($sql);
		?>
		<div id="tipo1" style="display:block">
			<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
				<tr>
			    	<th colspan="4"><strong>Parametros de Configuracion Reporte</strong></th>
			    </tr>
    			<tr>
      				<td width="25%">Agencia</td>
      				<td width="25%">
      					<select id="txtAgencia" name="txtAgencia" class="box1">
				        	<option value="01">Neiva</option>
				          	<option value="02">Garzon</option>
				          	<option value="03">Pitalito</option>
				          	<option value="04">La plata</option>	
      					</select>
      				</td>
      				<td width="25%">Tipo</td>
      				<td width="25%">
      					<select id="txtTipo" name="txtTipo" class="box1">
						<?php
							while($row=$rs->fetch()){
								echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
							 }
						?>
      					</select>
     				</td>
    			</tr>
    			<tr>
      				<td>Fecha Inicio</td>
      				<td><input type="text" id="txtFechaI" name="txtFechaI" readonly></td>
      				<td>Fecha Final</td>
      				<td><input type="text" id="txtFechaF" name="txtFechaF" readonly></td>
    			</tr>
    			<tr>
      				<td colspan="4">&nbsp;</td>
  				</tr>
    			<tr>
    	  			<td colspan="4" style="text-align:center"><label class="RojoGrande" style="cursor:pointer" onClick="procesar1();">Procesar</label></td>
    			</tr>
    		</table>
    	</div>
 		<?php
			}
			if($tipo==2){
		?>
    	<div id="tipo2" style="display:block">
    		<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
				<tr>
    				<th colspan="4"><strong>Parámetros de Configuracion Reporte</strong></th>
    			</tr>
				<tr>
	  				<td colspan="4" style="text-align:center">Fecha del informe: <input type="text" id="txtFechaI" name="txtFechaI" readonly></td>
	  			</tr>
				<tr>
	  				<td colspan="4">&nbsp;</td>
	  			</tr>
				<tr>
	  				<td colspan="4" style="text-align:center"><label class="RojoGrande" style="cursor:pointer" onClick="procesar2();">Procesar</label></td>
	  			</tr>
    		</table>
    	</div>
		<?php
			}
			if($tipo==3){
		?>
    	<div id="tipo2" style="display:block">
    		<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
				<tr>
    				<th><strong>Parámetros de Configuracion Reporte</strong></th>
    			</tr>
				<tr>
	  				<td style="text-align:center">Seccional:
	     				<select id="txtSeccional" name="txtSeccional" class="box1">
          					<option value="0">Todas</option>
          					<option value="01">Neiva</option>
          					<option value="02">Garzon</option>
          					<option value="03">Pitalito</option>
          					<option value="04">La plata</option>
      					</select>
        			</td>
	  			</tr>
				<tr>
	  				<td>&nbsp;</td>
	  			</tr>
				<tr>
	  				<td style="text-align:center">
      					<label style="cursor:pointer" onClick="procesar3(1);"><img src="../../../imagenes/icono_pdf.png" width="32" height="32"></label> &nbsp;&nbsp;
      					<label style="cursor:pointer" onClick="procesar3(2);"><img src="../../../imagenes/icono_excel.png" width="32" height="32"></label></td>
	  				</tr>
    		</table>
		</div>
		<?php
			}
			if($tipo==4){
		?>
		<div id="tipo4" style="display:block">
    		<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
				<tr>
    				<th colspan="4"><strong>Parámetros de Configuracion Reporte</strong></th>
    			</tr>
				<tr>
	  				<td style="text-align:center">NIT</td>
	  				<td style="text-align:center"><input type="text" id="txtNit" name="txtNit" readonly></td>
	  				<td style="text-align:center">PERIODO</td>
	  				<td style="text-align:center"><input type="text" id="txtPeriodo" name="txtPeriodo" readonly></td>
	  			</tr>
				<tr>
	  				<td colspan="4">&nbsp;</td>
	  			</tr>
				<tr>
	  				<td colspan="4" style="text-align:center">
      					<label style="cursor:pointer" onClick="procesar4(1);"><img src="../../../imagenes/icono_pdf.png" width="32" height="32"></label> &nbsp;&nbsp;
      					<label style="cursor:pointer" onClick="procesar4(2);"><img src="../../../imagenes/icono_excel.png" width="32" height="32"></label></td>
	  			</tr>
    		</table>
    	</div>
		<?php
			}
			if($tipo==5){
		?>
		<div id="tipo4" style="display:block">
    		<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
				<tr>
    				<th ><strong>Reporte validacion cuota monetaria</strong></th>
    			</tr>
				<tr>
	  				<td style="text-align:center">Seleccione tipo de Informe.</td>
	  			</tr>
				<tr>
	  				<td style="text-align:center">
	  					<label>
	        				<div align="center">
	          					<select name="tipoReporte" id="tipoReporte" class="box1">
		            				<option value="0">Seleccione</option>
		            				<option value="1">Aprobados</option>
		            				<option value="2">Rechazados</option>
		            				<option value="7">Rechazados Validador</option>
		            				<option value="3">Reclamo por Empresas</option>
		            				<option value="4">Reclamo por Afiliados</option>
		            				<option value="5">Pre-Giro Normal</option>
		            				<option value="6">Todo el Pre-giro</option>
	          					</select>
	        				</div>
	  					</label>
      				</td>
	  			</tr>
				<tr>
    				<td style="text-align:center" id="tdProcesar">
    					<label style="cursor:pointer" onClick="procesar004(1);"><img src="../../../imagenes/procesar2.png" width="140" height="24"></label> 
    				</td>
	  			</tr>
 		   	</table>	
 		</div>
		<?php
			}
			if($tipo==6){
		?>
<div id="tipo6" style="display:block">
    <table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
	<tr>
    <th colspan="4"><strong>Parámetros de Configuracion Reporte</strong></th>
    </tr>
	<tr>
	  <td style="text-align:center">NIT <input type="text" id="txtNit" name="txtNit"></td>
	  <td style="text-align:center">PERIODO <input name="txtPeriodo" type="text" id="txtPeriodo" size="5"></td>
      <td colspan="2" style="text-align:center">OPCION
      <select id="txtTipo" name="txtTipo" class="box1">
          <option value="R">Revision</option>
          <option value="C">Causado</option>
      </select></td>
	  </tr>
	<tr>
      <td colspan="4">&nbsp;</td>
      </tr>
	<tr>
	  <td colspan="4" style="text-align:center">
      <label style="cursor:pointer" onClick="procesar5(1);"><img src="../../../imagenes/icono_pdf.png" width="32" height="32"></label>
      <label style="cursor:pointer" onClick="procesar5(2);"><img src="../../../imagenes/icono_excel.png" width="32" height="32"></label></td>
	  </tr>
    </table>
</div>
<?php
}
if($tipo==7){
?>
<div id="tipo7" style="display:block">
    <table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
	<tr>
    <th colspan="4"><strong>Parámetros de Configuracion Reporte</strong></th>
    </tr>
	<tr>
	  	<td style="text-align:center">Periodo de giro <input type="text" name="txtPeriodo" id="txtPeriodo" readonly size="6"></td> 
	  	<td colspan="2" style="text-align:center">Giro
      		<select id="txtTipo" name="txtTipo" class="box1">
          		<option value="A" selected>Actual</option>
          		<option value="H">Historico</option>
      		</select>
  		</td>     
	</tr>
	<tr>
      <td colspan="4">&nbsp;</td>
      </tr>
	<tr>
	  <td colspan="4" style="text-align:center">
      <label style="cursor:pointer" onClick="procesar6(1);"><img src="../../../imagenes/icono_excel.png" width="32" height="32"></label></td>
	  </tr>
    </table>
</div>
<?php
}
if($tipo==8){
?>
<div id="tipo8" style="display:block">
    <table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
	<tr>
    <th colspan="4"><strong>Parámetros de Configuracion Reporte</strong></th>
    </tr>
	<tr>
	  	<td style="text-align:center">Periodo de giro 
	  		<input type="text" name="txtPeriodo" id="txtPeriodo" readonly size="6">
	  	</td>
	  	<td style="text-align:center">
	  	Agencia:
	  	<select id="cmbAgencia" name="cmbAgencia">
		  	<option value="T">Todas</option>
		  	<?php 
		  		$sql = "select * from aportes500";
		  		$consulta = $db->querySimple($sql);
		  		while($row = $consulta->fetch()){
		  			echo "<option value='$row[codigo]'>$row[agencia]</option>";
		  		}
		  	?>	  		
	  	</select>
	  	</td>
	  	<td style="text-align:center">
	  	Giro:
      		<select id="txtTipo" name="txtTipo" class="box1">
          		<option value="aportes014" selected>Actual</option>
          		<option value="aportes009">Historico</option>
      		</select>
	  	</td> 	  	     
	</tr>
	<tr>
      <td colspan="4">&nbsp;</td>
      </tr>
	<tr>
	  <td colspan="4" style="text-align:center">
      <label style="cursor:pointer" onClick="procesar7(1);"><img src="../../../imagenes/icono_pdf.png" width="32" height="32"></label>
      <label style="cursor:pointer" onClick="procesar7(2);"><img src="../../../imagenes/icono_excel.png" width="32" height="32"></label></td>
	  </tr>
    </table>
</div>
<?php
}
if($tipo==9 or $tipo==10){
?>
<div id="tipo9" style="display:block">
    <table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
	<tr>
    <th colspan="4"><strong>Parametros de Configuracion Reporte</strong></th>
    </tr>
	<tr>
		<td style="text-align:center">Nit:  
	  		<input type="text" name="txtNit" id="txtNit" size="16">
	  	</td>
	  	<td style="text-align:center">Periodo de giro:   
	  		<input type="text" name="txtPeriodo" id="txtPeriodo" readonly size="6">
	  	</td>	  	
	  	<td style="text-align:center">
	  	Giro:
      		<select id="txtTipo" name="txtTipo" class="box1">
          		<option value="aportes014" selected>Actual</option>
          		<option value="aportes009">Historico</option>
      		</select>
	  	</td>	  	     
	</tr>
	<tr>
      <td colspan="4">&nbsp;</td>
      </tr>
	<tr>
	  <td colspan="4" style="text-align:center">
      <label style="cursor:pointer" onClick="procesar9(<?php if($tipo==9) echo "1"; else echo "2"; ?>)">
      	<img src="../../../imagenes/icono_pdf.png" width="32" height="32">
      </label>
      </td>
	  </tr>
    </table>
</div>
<?php
}
if($tipo==11){
?>
<div id="tipo9" style="display:block">
    <table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
	<tr>
    <th colspan="4">&nbsp;</th>
    </tr>
	<tr>
		<td style="text-align:center">&nbsp;</td>
	  	  	     
	</tr>
	<tr>
      <td colspan="4">&nbsp;</td>
    </tr>
	<tr>
	  <td colspan="4" style="text-align:center">&nbsp;</td>
  	</tr>
    </table>
</div>
<?php
}
if($tipo==12){
?>
<div id="tipo8" style="display:block">
    <table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
	<tr>
    <th colspan="4"><strong>Parámetros de Configuracion Reporte</strong></th>
    </tr>
	<tr>
	  	<td style="text-align:center">Periodo de giro 
	  		<input type="text" name="txtPeriodo" id="txtPeriodo" readonly size="6">
	  	</td>
	  	<td style="text-align:center">
                    Agencia:
                    <select id="cmbAgencia" name="cmbAgencia">
                            <option value="T">Todas</option>
                            <?php 
                                $sql = "select * from aportes500";
                                $consulta = $db->querySimple($sql);
                                while($row = $consulta->fetch()){
                                        echo "<option value='$row[codigo]'>$row[agencia]</option>";
                                }
                            ?>	  		
                    </select>
	  	</td>
	  	<td style="text-align:center">
	  	Giro:
      		<select id="txtTipo" name="txtTipo" class="box1">
          		<option value="aportes014" selected>Actual</option>
          		<option value="aportes009">Historico</option>
      		</select>
	  	</td> 	  	     
	</tr>
	<tr>
      <td colspan="4">&nbsp;</td>
      </tr>
	<tr>
	  <td colspan="4" style="text-align:center">
            <label style="cursor:pointer" onClick="Reporte055('pdf');"><img src="../../../imagenes/icono_pdf.png" width="32" height="32"></label>
            <label style="cursor:pointer" onClick="Reporte055('exc');"><img src="../../../imagenes/icono_excel.png" width="32" height="32"></label></td>
	  </tr>
    </table>
</div>
<?php
}
if($tipo==13){
?>
<div id="tipo8" style="display:block">
    <table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
	<tr>
    <th colspan="4"><strong>Parámetros de Configuracion Reporte</strong></th>
    </tr>
	<tr>
	  	<td style="text-align:center">Periodo Inicial
	  		<input type="text" name="txtPeriodo" id="txtPeriodo" readonly size="6">
	  		Periodo Final
	  		<input type="text" name="txtPeriodo2" id="txtPeriodo2" readonly size="6">
	  	</td>
	  	<td style="text-align:center">
                    Agencia:
                    <select id="cmbAgencia" name="cmbAgencia">
                            <option value="T">Todas</option>
                            <?php 
                                $sql = "select * from aportes500";
                                $consulta = $db->querySimple($sql);
                                while($row = $consulta->fetch()){
                                        echo "<option value='$row[codigo]'>$row[agencia]</option>";
                                }
                            ?>	  		
                    </select>
	  	</td>
	  	<td style="text-align:center">
	  	Giro:
      		<select id="txtTipo" name="txtTipo" class="box1">
          		<option value="aportes014" selected>Actual</option>
          		<option value="aportes009">Historico</option>
      		</select>
	  	</td> 	  	     
	</tr>
	<tr>
      <td colspan="4">&nbsp;</td>
      </tr>
	<tr>
	  <td colspan="4" style="text-align:center">
            <label style="cursor:pointer" onClick="Reporte058('pdf');"><img src="../../../imagenes/icono_pdf.png" width="32" height="32"></label>
            <label style="cursor:pointer" onClick="Reporte058('exc');"><img src="../../../imagenes/icono_excel.png" width="32" height="32"></label></td>
	  </tr>
    </table>
</div>
<?php
}
if($tipo==14){
?>
<div id="tipo8" style="display:block">
    <table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
	<tr>
    <th colspan="4"><strong>Parámetros de Configuracion Reporte</strong></th>
    </tr>
	<tr>
      	<td style="text-align:center">
      		Fecha Inicio
      		<input type="text" id="txtFechaI" name="txtFechaI" readonly>
      	</td>
      	<td style="text-align:center">
      		Fecha Final
      		<input type="text" id="txtFechaF" name="txtFechaF" readonly>
      	</td>
    </tr>
		<tr>
	  		<td colspan="4" style="text-align:center">
            	<label style="cursor:pointer" onClick="Reporte059('pdf');"><img src="../../../imagenes/icono_pdf.png" width="32" height="32"></label>
            	<label style="cursor:pointer" onClick="Reporte059('exc');"><img src="../../../imagenes/icono_excel.png" width="32" height="32"></label>
           	</td>
	  </tr>
    </table>
</div>
	<?php
		}
		if($tipo==15){
	?>
	<div id="tipo8" style="display:block">
	    <table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
			<tr>
	    		<th colspan="4"><strong>Parámetros de Configuracion Reporte</strong></th>
	    	</tr>
			<tr>
	      		<td style="text-align:center">
	      			Tope maximo:
	      			<input type="text" id="txtTopeMaximo" name="txtTopeMaximo" />
	      		</td>
	    	</tr>
			<tr>
		  		<td colspan="4" style="text-align:center">
	            	<label style="cursor:pointer" onClick="Reporte047('pdf');"><img src="../../../imagenes/icono_pdf.png" width="32" height="32"></label>
	            	<label style="cursor:pointer" onClick="Reporte047('exc');"><img src="../../../imagenes/icono_excel.png" width="32" height="32"></label>
	           	</td>
		  	</tr>
    	</table>
	</div>
	<?php
		}
		if($tipo==16){
	?>
	<div id="tipo8" style="display:block">
	    <table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
			<tr>
	    		<th colspan="4"><strong>Parámetros de Configuracion Reporte</strong></th>
	    	</tr>
			<tr>
	      		<td style="text-align:right;">
	      			Tipo:
	      		</td>
	      		<td>
	      			<select name="cmbTipo" id="cmbTipo">
	      				<option value="E">Embargo</option>
	      				<option value="I">Discapacidad</option>
	      				<option value="" selected="selected">Todos</option>
	      			</select>
	      		</td>
	    	</tr>
	    	<tr>
	      		<td style="text-align:right;">
	      			Identificaci&oacute;n trabajador:
	      		</td>
	      		<td>
	      			<input type="text" name="txtIdentTrabajador" id="txtIdentTrabajador"/>
	      		</td>
	    	</tr>
	    	<tr>
	      		<td style="text-align:right;">
	      			Identificaci&oacute;n beneficiario:
	      		</td>
	      		<td>
	      			<input type="text" name="txtIdentBeneficiario" id="txtIdentBeneficiario"/>
	      		</td>
	    	</tr>
	    	<tr>
	      		<td style="text-align:right;">
	      			Identificaci&oacute;n tercero:
	      		</td>
	      		<td>
	      			<input type="text" name="txtIdentTercero" id="txtIdentTercero"/>
	      		</td>
	    	</tr>
	    	<tr>
	    		<td style="text-align:right">
	      			Periodos de pago:
	      		</td>
	      		<td>
		    		<input type="text" name="txtPeriodo" id="txtPeriodo" readonly size="6">
		  			- <input type="text" name="txtPeriodo2" id="txtPeriodo2" readonly size="6">
		  		</td>
	    	</tr>
			<tr>
		  		<td colspan="4" style="text-align:center">
	            	<label style="cursor:pointer" onClick="Reporte060('pdf');"><img src="../../../imagenes/icono_pdf.png" width="32" height="32"></label>
	            	<label style="cursor:pointer" onClick="Reporte060('exc');"><img src="../../../imagenes/icono_excel.png" width="32" height="32"></label>
	           	</td>
		  	</tr>
    	</table>
	</div>
    <?php
}
if($tipo==17){
?>
<div id="tipo8" style="display:block">
    <table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
	<tr>
    <th colspan="4"><strong>Parámetros de Configuracion Reporte</strong></th>
    </tr>
	<tr>
      	<td style="text-align:center">
      		Fecha Inicio pago Planilla
      		<input type="text" id="txtFechaI" name="txtFechaI" readonly>
      	</td>
      	<td style="text-align:center">
      		Fecha Final pago Planilla
      		<input type="text" id="txtFechaF" name="txtFechaF" readonly>
      	</td>
    </tr>
		<tr>
	  		<td colspan="4" style="text-align:center">
            	<label style="cursor:pointer" onClick="Reporte061('pdf');"><img src="../../../imagenes/icono_pdf.png" width="32" height="32"></label>
            	<label style="cursor:pointer" onClick="Reporte061('exc');"><img src="../../../imagenes/icono_excel.png" width="32" height="32"></label>
           	</td>
	  </tr>
    </table>
</div>
<?php
			}
			if($tipo==18){
		?>
		<div id="tipo4" style="display:block">
    		<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
				<tr>
    				<th ><strong>Reporte Historico cuota monetaria</strong></th>
    			</tr>
				<tr>
	  				<td style="text-align:center">
	  					<label>
	        				<div align="center">
	          					Periodo de giro 
	  							<input type="text" name="txtPeriodo" id="txtPeriodo" readonly size="6">
	        				</div>
	  					</label>
      				</td>
	  			</tr>
				<tr>
    				<td style="text-align:center" id="tdProcesar">
    					<label style="cursor:pointer" onClick="procesar010(1);"><img src="../../../imagenes/procesar2.png" width="140" height="24"></label> 
    				</td>
	  			</tr>
 		   	</table>	
 		</div>

	<?php
		}
	?>    
<input type="hidden" id="txtusuario" name="txtusuario" value="<?php echo $usuario; ?>" /> 
<input type="hidden" id="txtRept" name="txtRept" value="<?php echo $rept; ?>" />
</body>
</html>