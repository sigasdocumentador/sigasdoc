/*
* Autor: Orlando Puentes Andrade
* Objetivo: Archivo para informe mensual de aportes
* Fecha: 24-Mar-2011
* ----------- E M P R E S A -------------- 
*/


// funcion para obtener cookie en javascript
	function getCookie(c_name)
	{
		var i,x,y,ARRcookies=document.cookie.split(";");
		for (i=0;i<ARRcookies.length;i++)
		{
  			x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
  			y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
  			x=x.replace(/^\s+|\s+$/g,"");
  			if (x==c_name)
    			{
    				return unescape(y);
    			}// fin if
		}// fin for
  	}// fin getCookie
//funcion para obtener cookie en javascript

function procesar3(op){
	var seccional=$("#txtSeccional").val();
	var usuario=$("#txtusuario").val();
	if(op==1){
		if($("#txtRept").val()==1)
			url="http://"+getCookie("URL_Reportes")+"/aportes/empresa/reporte001.jsp?v0="+seccional+"&v1="+usuario;
	
	}
	else{
		if($("#txtRept").val()==1)
			url="http://"+getCookie("URL_Reportes")+"/aportes/empresa/rptExcel001.jsp?v0="+seccional+"&v1="+usuario;
		
		}
	window.open(url,"_NEW");
	}
 
function procesar5(op){
	
	var usuario=$("#txtusuario").val();
	var nit=$("#txtNit").val();
	var periodo=$("#txtPeriodo").val();
	var tipo=$("#txtTipo").val();
	if(op==1){
			url="http://"+getCookie("URL_Reportes")+"/aportes/giro/reporte051.jsp?v0="+usuario+"&v1="+nit+"&v2="+periodo+"&v3="+tipo;
	}
	if(op==2){
			url="http://"+getCookie("URL_Reportes")+"/aportes/giro/rptExcel051.jsp?v0="+usuario+"&v1="+nit+"&v2="+periodo+"&v3="+tipo;
	}
	
	window.open(url,"_NEW");
	}

function procesar6(op){
	var periodo=$("#txtPeriodo").val();
	var tipo=$("#txtTipo").val();
	//var periodo=201208;
	if(op==1){
			url="http://"+getCookie("URL_Reportes")+"/aportes/giro/rptExcel052.jsp?v0="+periodo+"&v1="+tipo;
	
	}
	
	window.open(url,"_NEW");
}

function procesar7(op){
	var periodo=$("#txtPeriodo").val();
	var tipo=$("#txtTipo").val();
	//var periodo=201208;
	if (periodo=="")
	{
		alert("Debe seleccionar periodo...");
		return false;
	}
	var agencia = $("#cmbAgencia").val();
	if(op==1){
		url="http://"+getCookie("URL_Reportes")+"/aportes/giro/reporte048.jsp?v0="+periodo+"&v1="+agencia+"&v2="+tipo;
	}else{
		url="http://"+getCookie("URL_Reportes")+"/aportes/giro/rptExcel048.jsp?v0="+periodo+"&v1="+agencia+"&v2="+tipo;
	}
	
	window.open(url,"_NEW");
}

//Funcion Reporte Embargos y Muertos girados por periodo
function procesar9(op){
	var periodo=$("#txtPeriodo").val();
	var nit=$("#txtNit").val();
	var tipo=$("#txtTipo").val();
	
	if (periodo=="")
	{
		alert("Debe seleccionar periodo...");
		return false;
	}
	
	if (nit=="")
		nit="T";
	
	if(op==1)
	{
		url="http://"+getCookie("URL_Reportes")+"/aportes/giro/reporte053.jsp?v0="+periodo+"&v1="+nit+"&v2="+tipo;	
	}
	else
	{
		url="http://"+getCookie("URL_Reportes")+"/aportes/giro/reporte054.jsp?v0="+periodo+"&v1="+nit+"&v2="+tipo;
	}
	
	window.open(url,"_NEW");
}

//Funcion para reporte mensual de produccion
function rptMensual(){
    	
	$("#txtNit").removeClass("ui-state-error");
 	$("#txtPeriodo").removeClass("ui-state-error");
	
	if($("#txtNit").val()==''){
    	$("#txtNit").addClass("ui-state-error").focus();
		return false;
	}
	
	if($("#txtPeriodo").val()==''){
    	$("#txtPeriodo").addClass("ui-state-error").focus();
		return false;
	}
	
	var agencia=$.trim($("#agencia").val());
	var fecI=$("#fecInicial").val();
	var fecF=$("#fecFinal").val();
	
	//Envio los parametros del reporte
	switch($("#opcion").val()){
		case '2' :location.href=URL+"centroReportes/aportes/empresas/reporte002.php?agencia="+agencia+"&fecInicial="+fecI+"&fecFinal="+fecF+"";break;
		case '3' :location.href=URL+"centroReportes/aportes/empresas/reporte003.php?agencia="+agencia+"&fecInicial="+fecI+"&fecFinal="+fecF+"";break;
	}
}

function procesar004(){
	var op =$("#tipoReporte").val();
    $("#tdProcesar").html("<img src='../../../imagenes/ajax-loader.gif'>");
    $.getJSON('subsidioCuotaMonetaria.php',{v0:op}, function(data){
        $("#tdProcesar").html("<br><label style='cursor:pointer' onClick='descargar();'><img src='../../../imagenes/descargarArchivo.png' width=143 height=30 /></label>");
    });
}

function procesar010(){
	var op =$("#txtPeriodo").val();
    $("#tdProcesar").html("<img src='../../../imagenes/ajax-loader.gif'>");
    $.getJSON('subsidioCuotaMonetariaHistorico.php',{v0:op}, function(data){
        $("#tdProcesar").html("<br><label style='cursor:pointer' onClick='descargar();'><img src='../../../imagenes/descargarArchivo.png' width=143 height=30 /></label>");
    });
}
	
function procesar006(){
	$("#idDescaArchiPlano").html('<img src="../../../imagenes/ajaxload.gif" width="16" height="16"/>');
	//var url = "";
    //data = "0";
	var todo = $("#cmbTodo").val();
    $.ajax({
    	url:"formarArchivoPlano.php",
    	type:"POST",
    	data:{v0:todo},
    	dataType:"json",
    	async:true,
    	success:function(datos){
    		$("#idDescaArchiPlano").html('');
    		if(datos==1){
    			$("#idDescaArchiPlano").html("<img src='../../../imagenes/descargar.png' width=22 height=22 onclick='descargar()'/>");
			}
    	}
    });	
	
}

function Reporte047(p){
	var cuota = parseInt($("#txtTopeMaximo").val().trim());
	if(isNaN(cuota)){
		alert("El valor de tope no es un numero");
		return false;
	}
	var datos = "v0=" + cuota;
    if(p=="pdf"){
        url="http://"+getCookie("URL_Reportes")+"/aportes/giro/reporte047.jsp?" + datos; 
    }else if(p=="exc"){
       url="http://"+getCookie("URL_Reportes")+"/aportes/giro/rptExcel047.jsp?" + datos; 
    }
    window.open(url,"_NEW");
}

function Reporte055(p){
    var usuario=$("#txtusuario").val();
    var periodo=$("#txtPeriodo").val().trim();
    var agencia=$("#cmbAgencia").val();
    var tabla=$("#txtTipo").val();
    if(periodo==""){
        alert("Debe indicar el periodo");
        return false;
    }
    if(p=="pdf"){
        url="http://"+getCookie("URL_Reportes")+"/aportes/giro/reporte055.jsp?v0="+periodo+"&v1="+agencia+"&v2="+tabla+"&v3="+usuario; 
    }else if(p=="exc"){
       url="http://"+getCookie("URL_Reportes")+"/aportes/giro/rptExcel055.jsp?v0="+periodo+"&v1="+agencia+"&v2="+tabla+"&v3="+usuario; 
    }
    window.open(url,"_NEW");
}

function Reporte058(p){
    var usuario=$("#txtusuario").val();
    var periodo=$("#txtPeriodo").val().trim();
    var periodo2=$("#txtPeriodo2").val().trim();
    var agencia=$("#cmbAgencia").val();
    var tabla=$("#txtTipo").val();
    if(periodo=="" || periodo2==""){
        alert("Debe indicar el periodo");
        return false;
    }
    if(p=="pdf"){
        url="http://"+getCookie("URL_Reportes")+"/aportes/giro/reporte058.jsp?v0="+periodo+"&v1="+agencia+"&v2="+tabla+"&v3="+usuario+"&v4="+periodo2; 
    }else if(p=="exc"){
       url="http://"+getCookie("URL_Reportes")+"/aportes/giro/rptExcel058.jsp?v0="+periodo+"&v1="+agencia+"&v2="+tabla+"&v3="+usuario+"&v4="+periodo2; 
    }
    window.open(url,"_NEW");
}

function Reporte059(p){
    var usuario=$("#txtusuario").val();
    var fechaI=$("#txtFechaI").val();
    var fechaF=$("#txtFechaF").val();
 
    if(fechaI==""){alert("Debe indicar la fecha inicial");return false;}
    if(fechaF==""){alert("Debe indicar la fecha final");return false;}
    
    var datos = "v0=" + usuario + "&v1=" + fechaI + "&v2=" + fechaF;
    if(p=="pdf"){
        url="http://"+getCookie("URL_Reportes")+"/aportes/giro/reporte059.jsp?" + datos; 
    }else if(p=="exc"){
       url="http://"+getCookie("URL_Reportes")+"/aportes/giro/rptExcel059.jsp?" + datos; 
    }
    window.open(url,"_NEW");
}

function Reporte060(p){
	var usuario = $("#txtusuario").val();
	var tipo = $("#cmbTipo").val();
	var identTrabajador = $("#txtIdentTrabajador").val().trim();
	var identBeneficiario = $("#txtIdentBeneficiario").val().trim();
	var identTercero = $("#txtIdentTercero").val().trim();
	var periodoI = $("#txtPeriodo").val();
	var periodoF = $("#txtPeriodo2").val();
	
	var datos = "v0=" + usuario + "&v1=" + tipo + "&v2=" + identTrabajador +
			"&v3=" + identBeneficiario + "&v4=" + identTercero + "&v5=" + periodoI + "&v6=" + periodoF;
	if(p=="pdf"){
		url="http://"+getCookie("URL_Reportes")+"/aportes/giro/reporte060.jsp?" + datos; 
	}else if(p=="exc"){
		url="http://"+getCookie("URL_Reportes")+"/aportes/giro/rptExcel060.jsp?" + datos; 
	}
	window.open(url,"_NEW");
}
function Reporte061(p){
    var usuario=$("#txtusuario").val();
    var fechaI=$("#txtFechaI").val();
    var fechaF=$("#txtFechaF").val();
 
    if(fechaI==""){alert("Debe indicar la fecha inicial");return false;}
    if(fechaF==""){alert("Debe indicar la fecha final");return false;}
    
    var datos = "v0=" + usuario + "&v1=" + fechaI + "&v2=" + fechaF;
    if(p=="pdf"){
        url="http://"+getCookie("URL_Reportes")+"/aportes/giro/reporte061.jsp?" + datos; 
    }else if(p=="exc"){
       url="http://"+getCookie("URL_Reportes")+"/aportes/giro/rptExcel061.jsp?" + datos; 
    }
    window.open(url,"_NEW");
}

function descargar(){
	var url=URL+'phpComunes/descargaPlanos.php';
	window.open(url);
}
