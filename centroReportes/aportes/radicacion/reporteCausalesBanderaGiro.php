<?php
set_time_limit(0);
ini_set("display_errors",'1');
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
include_once $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'config.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';

// Conexion a la base de datos
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}


/* variable en la cual se estructura la condicion. */
$where_sql=null;

if((isset($_REQUEST['fechaIni']) and isset($_REQUEST['fechaFin'])) or isset($_REQUEST['iden'])){
	if(!empty($_REQUEST['fechaIni']) and  !empty($_REQUEST['fechaIni'])){
		$where_sql="a021.fechagiro>='".$_REQUEST['fechaIni']."' and a021.fechagiro<='".$_REQUEST['fechaFin']."'";
	}
	if(isset($_REQUEST['iden']) and !empty($_REQUEST['iden'])){
		$where_sql.=isset($where_sql)?" and a015_2.identificacion='".$_REQUEST['iden']."'":" a015_2.identificacion='".$_REQUEST['iden']."'";
	}
	if(isset($_REQUEST['idsede']) and $_REQUEST['idsede']>0){
		$where_sql.=isset($where_sql)?" and a519.idagencia=".$_REQUEST['idsede']:" a519.idagencia=".$_REQUEST['iden'];
	}
	
	
	//sentencia sql que realiza la consulta a la tabla aportes021 con los parametros recibidos
	$sql="SELECT
                a015_1.idtipodocumento AS tipoidentrab,
                a015_1.identificacion AS identraba,
                a015_1.pnombre + ' ' +a015_1.papellido AS nomtraba,
                a015_2.idtipodocumento AS tipoidenbene,
                a015_2.identificacion AS idenbene,
                a015_2.pnombre + ' ' + a015_2.papellido AS nombene,
                a021.idparentesco,
                (SELECT detalledefinicion FROM aportes091 WHERE iddetalledef=a021.idparentesco) as detalleparentesco,
                a021.giro,
                a021.fechagiro,
                a021.idrelacion,
                (SELECT max(periodo) FROM aportes009 WHERE idbeneficiario=a021.idbeneficiario AND idtrabajador=a021.idtrabajador) AS ultperidogiro,
                a124.iddetalledef,
                isnull((SELECT controlexiste FROM aportes125 WHERE idrelacion=a021.idrelacion AND iddetalledef=a124.iddetalledef),0) AS controlexiste,
                a021.usuario,
                isnull((SELECT agencia FROM aportes500 WHERE idagencia=a519.idagencia),'') as agencia
         FROM aportes021 a021  
           LEFT JOIN aportes015 a015_1 ON a015_1.idpersona=a021.idtrabajador 
           LEFT JOIN aportes015 a015_2 ON a015_2.idpersona=a021.idbeneficiario
           INNER JOIN aportes124 a124 ON a124.idparentesco=a021.idparentesco AND a124.controldocumento=1
           INNER JOIN aportes091 a091 ON a091.iddetalledef=a124.iddetalledef 
           LEFT JOIN aportes519 a519 ON a519.usuario=a021.usuario
         WHERE  {$where_sql} order by a021.idrelacion";
	
 
	/*Se efectua la consulta de los datos*/
	$rs=$db->querySimple($sql);
	$idradicacion=0;
	$datares=array();
	$objrs="";
	$ban=0;
    while($row=$rs->fetch()){
    	if($ban==0){
    		$objrs .= '{';
    		$objrs .= '"identraba":"'.$row["identraba"].'",';
    		$objrs .= '"nomtraba":"'.$row["nomtraba"].'",';
    		$objrs .= '"idenbene":"'.$row["idenbene"].'",';
    		$objrs .= '"nombene":"'.$row["nombene"].'",';
    		$objrs .= '"parentesco":"'.$row["detalleparentesco"].'",';
    		$objrs .= '"giro":"'.$row["giro"].'",';
    		$objrs .= '"fechagiro":"'.$row["fechagiro"].'",';
    		$objrs .= '"ultperidogiro":"'.$row["ultperidogiro"].'",';
    		$objrs .= '"usuario":"'.$row["usuario"].'",';
    		$objrs .= '"agencia":"'.$row["agencia"].'",';
    		$objrs .= '"'.$row["iddetalledef"].'":"'.$row["controlexiste"].'",';
    	}else if($idradicacion!=$row["idrelacion"]){
    		$objrs=substr($objrs,0, -1);
    		$objrs.='}';
    		$datares[]=$objrs;
    		$objrs='';
    		$objrs .= '{';
    		$objrs .= '"identraba":"'.$row["identraba"].'",';
    		$objrs .= '"nomtraba":"'.$row["nomtraba"].'",';
    		$objrs .= '"idenbene":"'.$row["idenbene"].'",';
    		$objrs .= '"nombene":"'.$row["nombene"].'",';
    		$objrs .= '"parentesco":"'.$row["detalleparentesco"].'",';
    		$objrs .= '"giro":"'.$row["giro"].'",';
    		$objrs .= '"fechagiro":"'.$row["fechagiro"].'",';
    		$objrs .= '"ultperidogiro":"'.$row["ultperidogiro"].'",';
    		$objrs .= '"usuario":"'.$row["usuario"].'",';
    		$objrs .= '"agencia":"'.$row["agencia"].'",';
    		$objrs .= '"'.$row["iddetalledef"].'":"'.$row["controlexiste"].'",';
    	}else if($idradicacion==$row["idrelacion"]){
    		$objrs .= '"'.$row["iddetalledef"].'":"'.$row["controlexiste"].'",';
    	}
    	$idradicacion=$row["idrelacion"];
    	$ban=1;
	}
	
$objrs=substr($objrs,0, -1);
$objrs.='}';
$datares[]=$objrs;


	
$table="<table id='tabla_causales'  border='1' cellspacing='0' class='display' style='display:none' width='90%'>
		    <thead>
		          <tr> 
		               <th colspan='21' height='70'><img src='http://localhost/sigas/imagenes/logo_comfamiliar.jpg' border='0'><font size='6'><strogn>Causales de giro de beneficiarios</strogn></font></th>
		          </tr>
		          <tr>
		             <th>Identificacion Trabajador</th>
		             <th>Nombre Trabajador</th>
		             <th>Identificacion Beneficiario</th>
		             <th>Nombre Beneficiario</th>
		             <th>Parentesco</th>
		             <th>Giro</th>
		             <th>Fecha giro</th>
		             <th>Ultimo Periodo Girado</th>
		             <th>Usuario</th>
		             <th>Agencia</th>
		             <th>Cedula de Ciudadania</th>
		             <th>Certificado de estudio</th>
		             <th>Certificado Discapacidad</th>
		             <th>Certificado EPS</th>
		             <th>Custodia Hijastros</th>
		             <th>Declaracion Juramentada</th>
		             <th>Registro Civil de Defuncion</th>
		             <th>Registro Civil Matrimonio</th>
		             <th>Registro Civil Nacimiento</th>
		             <th>Registro Civil Nacimiento Trabajador Afiliado</th>
		             <th>Tarjeta de Identidad</th>
		         </tr>
		    </thead>
		    </tbody>
	    ";

foreach ($datares as $index=>$value){
	$dataprint=json_decode($value,true);
	$cedula=isset($dataprint[4568])?$dataprint[4568]:0;
	$cerestudio=isset($dataprint[4569])?$dataprint[4569]:0;
	$cerdiscapacidad=isset($dataprint[4570])?$dataprint[4570]:0;
	$cereps=isset($dataprint[4571])?$dataprint[4571]:0;
	$cushijastros=isset($dataprint[4572])?$dataprint[4572]:0;
	$declajuramentada=isset($dataprint[4573])?$dataprint[4573]:0;
	$regcivildefuncion=isset($dataprint[4574])?$dataprint[4574]:0;
	$regcivilmatrimonio=isset($dataprint[4575])?$dataprint[4575]:0;
	$regcivilnacimiento=isset($dataprint[4576])?$dataprint[4576]:0;
	$regciviltrabajador=isset($dataprint[4577])?$dataprint[4577]:0;
	$tarjetaidentidad=isset($dataprint[4578])?$dataprint[4578]:0;
	$table.='<tr>
		         <td>'.$dataprint['identraba'].'</td>
		         <td>'.$dataprint['nomtraba'].'</td>
		         <td>'.$dataprint['idenbene'].'</td>
		         <td>'.$dataprint['nombene'].'</td>
		         <td>'.$dataprint['parentesco'].'</td>
		         <td>'.$dataprint['giro'].'</td>
		         <td>'.$dataprint['fechagiro'].'</td>		
		         <td>'.$dataprint['ultperidogiro'].'</td>
		         <td>'.$dataprint['usuario'].'</td>
		         <td>'.$dataprint['agencia'].'</td>				
		         <td>'.$cedula.'</td>
		         <td>'.$cerestudio.'</td>
		         <td>'.$cerdiscapacidad.'</td>
		         <td>'.$cereps.'</td>
		         <td>'.$cushijastros.'</td>
		         <td>'.$declajuramentada.'</td>
		         <td>'.$regcivildefuncion.'</td>
		         <td>'.$regcivilmatrimonio.'</td>
		         <td>'.$regcivilnacimiento.'</td>
		         <td>'.$regciviltrabajador.'</td>
		         <td>'.$tarjetaidentidad.'</td>
		         </tr>';
	
}	
$table.="</tbody></table>";
	
echo $table;

	
}
else{
  echo "Error con los parametros del reportes";
  die();
}

?>