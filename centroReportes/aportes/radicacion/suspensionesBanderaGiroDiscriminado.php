
<?php
date_default_timezone_set('America/Bogota');
set_time_limit(0);
$raiz="";
//ini_set("display_errors",'1');
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
include_once '../../../config.php';

$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

$path_plano =$ruta_generados.'aportes'.DIRECTORY_SEPARATOR.'suspensiones'.DIRECTORY_SEPARATOR;

$fecha=date('Ymd');
$archivo='suspensionesDiscriminado'.$fecha.'.csv';
$path_plano.=$archivo;

$fechaIni=$_REQUEST['fechaIni'];
$fechaFin=$_REQUEST['fechaFin'];
$nit=$_REQUEST['nit'];

$cadena = "NIT;RAZON SOCIAL;DIRECCION EMPRESA;AGENCIA EMPRESA;MUNICIPIO EMPRESA;"
			."BARRIO EMPRESA;TELEFONO EMPRESA;CELULAR EMPRESA;CORREO EMPRESA;"
			."TRABAJADOR;NOMBRE TRABAJADOR;DIRECCION;AGENCIA;MUNICIPIO;BARRIO;TELEFONO;"
			."CELULAR;CORREO;TIPO DOCUMENTO BENEF;BENEFICIARIO;PARENTESCO;EDAD BENEF;"
			."USUARIO;FECHA CREACION;BANDERA DE GIRO MODIFICADO;BANDERA DE GIRO ACTUAL;"
			."OBSERVACIONES\r\n";

$handle=fopen($path_plano, "w");
fwrite($handle, $cadena);
fclose($handle);
$slqT="";
if ($nit!=''){
	
	$slqT = "AND a48.nit='$nit' AND";
	
}else{$slqT = "AND";}

$querySql = "SELECT a48.nit,a48.razonsocial,a48.direccion AS direccionEmp,a500.agencia AS agenciaEmp,
(SELECT TOP 1 a89.municipio FROM aportes089 a89 WHERE a89.coddepartamento=a48.iddepartamento AND a89.codmunicipio=a48.idciudad) AS municipioEmp,
(select top 1 a87.barrio from aportes087 a87 inner join aportes089 c89 on c89.codzona=a87.codigozona where a87.idbarrio=a48.idbarrio) as BARRIOEMPRE,
a48.telefono AS teleEmp,a48.celular as celularEmp,a48.email AS emailEmp,
a15.identificacion AS trabajador,a15.pnombre+' '+isnull(a15.snombre,'')+' '+a15.papellido+' '+isnull(a15.sapellido,'') AS trabajadornombre,
a15.direccion,b500.agencia,
(SELECT TOP 1 b89.municipio FROM aportes089 b89 WHERE b89.coddepartamento=a15.iddepresidencia AND b89.codmunicipio=a15.idciuresidencia) AS municipio,
(select top 1 a87.barrio from aportes087 a87 inner join aportes089 c89 on c89.codzona=a87.codigozona where a87.idbarrio=a15.idbarrio) as BARRIO,
a15.telefono,a15.celular,a15.email,
b91.codigo AS tipoDocumBenef, b15.identificacion AS beneficiario,a91.detalledefinicion AS parentesco,datediff(dd,b15.fechanacimiento,getdate())/365.25 AS edadBenef,a519.nombres,b21.fecha_creacion,
b21.bandera_giro,a21.giro AS banderaActual,(SELECT TOP 1 b88.observaciones FROM aportes088 b88 WHERE b88.idregistro=a15.idpersona AND b88.identidad=1) AS observaciones
FROM aportes021A b21
INNER JOIN aportes021 a21 ON a21.idrelacion=b21.id_relacion AND a21.estado='A'
INNER JOIN aportes016 a16 ON a16.idpersona=a21.idtrabajador
INNER JOIN aportes048 a48 ON a48.idempresa=a16.idempresa
INNER JOIN aportes015 a15 ON a15.idpersona=a16.idpersona
INNER JOIN aportes015 b15 ON b15.idpersona=a21.idbeneficiario
INNER JOIN aportes519 a519 ON a519.usuario=b21.usuario_creacion
INNER JOIN aportes091 a91 ON a91.iddetalledef=a21.idparentesco
INNER JOIN aportes091 b91 ON b91.iddetalledef=b15.idtipodocumento
LEFT JOIN aportes500 a500 ON a500.codigo=a48.seccional
LEFT JOIN aportes500 b500 ON b500.codigo=a16.idagencia
WHERE convert(DATE,b21.fecha_creacion) BETWEEN '$fechaIni' AND '$fechaFin' $slqT
(SELECT count(*) FROM aportes088 a88 WHERE a88.idregistro=a15.idpersona AND a88.identidad=1 AND a88.observaciones LIKE '%SUSPENSION%')>0
GROUP BY a48.nit,a48.razonsocial,a48.direccion,a500.agencia,a48.iddepartamento,a48.idciudad,a48.idbarrio,a48.telefono,a48.celular,a48.email,
a15.identificacion,a15.idpersona,a15.pnombre,a15.snombre,a15.papellido,a15.sapellido,
a15.direccion,a15.iddepresidencia,a15.idciuresidencia,b500.agencia,a15.idbarrio,a15.telefono,a15.celular,a15.email,
b91.codigo,b15.identificacion,b15.fechanacimiento,a519.nombres,b21.fecha_creacion,a91.detalledefinicion,b21.bandera_giro,a21.giro
ORDER BY a15.identificacion,b15.identificacion,b21.fecha_creacion ASC";

$rsQuery = $db->querySimple($querySql);
/*
while(($row = $rsQuery->fetch())==true){
	
	$row = array_map("utf8_decode",$row);
	$observaciones = eregi_replace("[\n|\r|\n\r]",' ',$row["observaciones"]);
	//$observaciones = trim($row["observaciones"]);
	$cadena .= "{$row["nit"]};{$row["razonsocial"]};{$row["direccionEmp"]};{$row["agenciaEmp"]};{$row["municipioEmp"]};"
				."{$row["BARRIOEMPRE"]};{$row["teleEmp"]};{$row["celularEmp"]};{$row["emailEmp"]};{$row["trabajador"]};"
				."{$row["trabajador"]};{$row["trabajadornombre"]};{$row["direccion"]};{$row["agencia"]};{$row["municipio"]};"
				."{$row["BARRIO"]};{$row["telefono"]};{$row["celular"]};{$row["email"]};{$row["tipoDocumBenef"]};"
				."{$row["beneficiario"]};{$row["parentesco"]};{$row["edadBenef"]};{$row["nombres"]};{$row["fecha_creacion"]};"
				."{$row["bandera_giro"]};{$row["banderaActual"]};"
				."{$observaciones}\r\n";
}
*/
while ($row=$rsQuery->fetch()){
	$con++;
	$row = array_map('trim', $row);
	$cadena=implode(';',$row);
	$cadena=eregi_replace("[\n|\r|\n\r]",' ',$cadena)."\r\n";
	$handle=fopen($path_plano, "a");
	fwrite($handle, $cadena);
	fclose($handle);
}

/*
$fp=fopen($path_plano,'w');
fwrite($fp, $cadena);
fclose($fp);
*/
$_SESSION['ENLACE']=$path_plano;
$_SESSION['ARCHIVO']=$archivo;

echo 1;
exit();

?>
