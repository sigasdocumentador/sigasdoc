<?php
/* autor:       orlando puentes
 * fecha:       28/08/2012
 * objetivo:    configuracion de los reportes de radicacion
 **********************************************************
 configuracion
 tipo 
 1. con tipo de radicacion
 2. solo fecha
 
 
*/

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
include_once $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'config.php';
$usuario=$_SESSION['USUARIO'];
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
$tipo=$_REQUEST['tipo'];
$rept=$_REQUEST['tit'];
switch($rept){
	case 1:$titulo='Listado de Radicaciones por Agencia - Tipo - Fecha'; break;
	case 2:$titulo='Gr&aacute;fico de Tiempo vr Tipo'; break;
	case 3:$titulo='Gr&aacute;fico Tiempo vr Usuario'; break;
	case 4:$titulo=''; break;
	case 5:$titulo=''; break;
	case 10:$titulo='Causales de giro de beneficiarios'; break;
	case 14:$titulo='Reclamos trabajador por no giro'; break;
	case 15:$titulo='Radicados anulados'; break;
	case 16:$titulo='Listado de Reclamos'; break;
	case 17:$titulo='Motivos de las movilizaciones'; break;
	case 18:$titulo='Auditoria Actualizacion Persona'; break;
	case 19:$titulo='Listado de Radicaciones Tipo Informacion'; break;
	case 20:$titulo='Informe Suspensiones Realizadas (Bandera de Giro)'; break;
	case 21:$titulo='Radicado Por Empresa Pendiente De Grabar'; break;
	}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Men&uacute; Reportes Radicacion</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link type="text/css" rel="stylesheet" href="../../../css/css.1.8/jquery-ui-1.8.17.custom.css" />
<link type="text/css" href="../../../css/Estilos.css" rel="stylesheet"/>
<link href="../../../css/marco.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="../../../js/1.6/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="../../../js/1.6/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="../../../js/comunes.js"></script>
<script src="../../../js/jquery.table2excel.min.js"></script>
<script type="text/javascript" src="config.js"></script>
<script>
$(function(){
	$("#txtFechaI,#txtFechaF").datepicker({maxDate:"+0D",
		changeMonth: true,
		changeYear: true
	});	
});
</script>
</head>
<body>
	<center>
	<table width="100%" border="0">
	<tr>
	<td align="center" ><img src="../../../imagenes/logo_reporte.png" width="362" height="70"></td>
	</tr>
	<tr>
	<td align="center" ><?php echo $titulo ?></td>
	</tr>
	</table>
	<br>
<?php
if($tipo==1){
	$sql="SELECT iddetalledef,detalledefinicion FROM aportes091 WHERE iddefinicion=6";
	$rs=$db->querySimple($sql);
	
	$sql1="Select * from aportes500";
	$agencia=$db->querySimple($sql1);
?>
	<div id="tipo1" style="display:block">
<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
	<tr>
    <th colspan="4"><strong>Par&aacute;metros de Configuracion Reporte</strong></th>
    </tr>
    <tr>
      <td width="25%">Agencia</td>
      <td width="25%">
      	<select name="cmbAgencia" class="box1" id="cmbAgencia">
					<option value="T">TODOS</option>
				<?php 
					while ($row1=$agencia->fetch()){
						echo "<option value=".$row1['codigo'].">".$row1['agencia']."</option>";
					}
				?>
		</select>
      <!--<select id="txtAgencia" name="txtAgencia" class="box1">
          <option value="01">Neiva</option>
          <option value="02">Garzon</option>
          <option value="03">Pitalito</option>
          <option value="04">La plata</option>
      </select>-->
      </td>
      <td width="25%">Tipo</td>
      <td width="25%">
      <select id="txtTipo" name="txtTipo" class="box1">
<?php
	  while($row=$rs->fetch()){
		  echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
	  }
?>
      </select>
      </td>
    </tr>
    <tr>
      <td>Fecha Inicio</td>
      <td><input type="text" id="txtFechaI" name="txtFechaI" readonly></td>
      <td>Fecha Final</td>
      <td><input type="text" id="txtFechaF" name="txtFechaF" readonly></td>
    </tr>
    <tr>
      <td colspan="4">&nbsp;</td>
  </tr>
    <tr>
      <td colspan="4" style="text-align:center"><label class="RojoGrande" style="cursor:pointer" onClick="procesar1();">Procesar</label></td>
    </tr>
    </table>
 <?php
}
if($tipo==2){
?>
    <div id="tipo2" style="display:block">
    <table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
	<tr>
    <th colspan="4"><strong>Par&aacute;metros de Configuracion Reporte</strong></th>
    </tr>
	<tr>
	  <td colspan="4" style="text-align:center">Fecha del informe: <input type="text" id="txtFechaI" name="txtFechaI" readonly></td>
	  </tr>
	<tr>
	  <td colspan="4">&nbsp;</td>
	  </tr>
	<tr>
	  <td colspan="4" style="text-align:center"><label class="RojoGrande" style="cursor:pointer" onClick="procesar2();">Procesar</label></td>
	  </tr>
    </table>

<?php
}
?>

<?php
if($tipo==3){
	
	$sql="SELECT usuario from aportes519 where estado='A' order by usuario asc";
	$rs=$db->querySimple($sql);
?>
    <div id="tipo3" style="display:block">
    <table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
	<tr>
    <th colspan="4"><strong>Par&aacute;metros de Configuracion Reporte</strong></th>
    </tr>
	<tr>
	  <td colspan="4" style="text-align:center">Usuario: 
      <select id="txtUsuario2" name="txtUsuario2" class="box1">
<?php
	  while($row=$rs->fetch()){
		  echo "<option value=".$row['usuario'].">".$row['usuario']."</option>";
	  }
?>
      </select>
      </td>
	  </tr>
	<tr>
	  <td colspan="4">&nbsp;</td>
	  </tr>
	<tr>
	  <td colspan="4" style="text-align:center">
      <label style="cursor:pointer" onClick="procesar3(1);"><img src="../../../imagenes/icono_pdf.png" width="32" height="32"></label>
      </td>
	  </tr>
    </table>

<?php
}
if($tipo==14){
?>
    <div id="tipo14" style="display:block">
    <table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
	<tr>
    <th colspan="4"><strong>Par&aacute;metros de Configuracion Reporte</strong></th>
    </tr>
	<tr>
		<td>
			Fecha Inicial:
		</td>
		<td>
			<input type="text" id="txtFechaI" name="txtFechaI" readonly>
		</td>
		<td>
			Fecha Final:
		</td>
		<td>
			<input type="text" id="txtFechaF" name="txtFechaF" readonly>
		</td>  
	</tr>
	<tr>	  
		<td>  Agencia: </td>
		<td>
			<select id="cmbAgencia" name="cmbAgencia" onchange="changeAgencia();">
                <option value="all" selected="selected" >Todas</option>
                <option value="sub">Neiva</option>
                <option value="gar">Garzon</option>
                <option value="pit">Pitalito</option>
                <option value="pla">La Plata</option>                                	  		
            </select>
		</td>
		<td>  Usuario: </td>
		<td>
			<select name="cmbUsuario" class="boxmediano" id="cmbUsuario" >				
				<option value="all">Debe seleccionar una agencia</option>
			</select>
		</td>		
	</tr>
	<tr>
	  <td colspan="4" style="text-align:center">
	  <label style="cursor:pointer" onClick="procesar14(1);"><img src="../../../imagenes/icono_pdf.png" width="32" height="32"></label>
      <label style="cursor:pointer" onClick="procesar14(2);"><img src="../../../imagenes/icono_excel.png" width="32" height="32"></label>
      </td>
	  </tr>
    </table>

<?php
}
if($tipo==15){
?>
    <div id="tipo15" style="display:block">
    <table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
	<tr>
    <th colspan="4"><strong>Par&aacute;metros de Configuracion Reporte</strong></th>
    </tr>
	<tr>
		<td>
			Fecha Inicial:
		</td>
		<td>
			<input type="text" id="txtFechaI" name="txtFechaI" readonly>
		</td>
		<td>
			Fecha Final:
		</td>
		<td>
			<input type="text" id="txtFechaF" name="txtFechaF" readonly>
		</td>  
	</tr>
	<tr>
	  <td colspan="4" style="text-align:center">
	  <label style="cursor:pointer" onClick="procesar15(1);"><img src="../../../imagenes/icono_pdf.png" width="32" height="32"></label>
      <label style="cursor:pointer" onClick="procesar15(2);"><img src="../../../imagenes/icono_excel.png" width="32" height="32"></label>
      </td>
	  </tr>
    </table>

<?php
}

if($tipo==16){
	?>
    <div id="tipo14" style="display:block">
    <table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
	<tr>
    <th colspan="4"><strong>Par&aacute;metros de Configuracion Reporte</strong></th>
    </tr>
	<tr>
		<td>
			Fecha Inicial:
		</td>
		<td>
			<input type="text" id="txtFechaI" name="txtFechaI" readonly>
		</td>
		<td>
			Fecha Final:
		</td>
		<td>
			<input type="text" id="txtFechaF" name="txtFechaF" readonly>
		</td>  
	</tr>
	<tr>	  
		<td>  Agencia: </td>
		<td>
			<select id="cmbAgencia" name="cmbAgencia" onchange="changeAgencia();">
                <option value="all" selected="selected" >Todas</option>
                <option value="sub">Neiva</option>
                <option value="gar">Garzon</option>
                <option value="pit">Pitalito</option>
                <option value="pla">La Plata</option>                                	  		
            </select>
		</td>
		<td> Usuario: </td>
		<td>
			<select name="cmbUsuario" class="boxmediano" id="cmbUsuario" >				
				<option value="all">Debe seleccionar una agencia</option>
			</select>
		</td>		
	</tr>
	<tr>
	  <td colspan="4" style="text-align:center">
	  <label style="cursor:pointer" onClick="procesar16(1);"><img src="../../../imagenes/icono_pdf.png" width="32" height="32"></label>
      <label style="cursor:pointer" onClick="procesar16(2);"><img src="../../../imagenes/icono_excel.png" width="32" height="32"></label>
      </td>
	  </tr>
    </table>

<?php
}
if($tipo==17){
	?>
    <div id="tipo14" style="display:block">
    <table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
	<tr>
    <th colspan="4"><strong>Par&aacute;metros de Configuracion Reporte</strong></th>
    </tr>
	<tr>
		<td>
			Fecha Inicial:
		</td>
		<td>
			<input type="text" id="txtFechaI" name="txtFechaI" readonly>
		</td>
		<td>
			Fecha Final:
		</td>
		<td>
			<input type="text" id="txtFechaF" name="txtFechaF" readonly>
		</td>  
	</tr>
	<tr>
	  <td colspan="4" style="text-align:center">
	  <label style="cursor:pointer" onClick="procesar17(1);"><img src="../../../imagenes/icono_pdf.png" width="32" height="32"></label>
      <label style="cursor:pointer" onClick="procesar17(2);"><img src="../../../imagenes/icono_excel.png" width="32" height="32"></label>
      </td>
	  </tr>
    </table>

<?php
}
if($tipo==18){
	
	$sql="SELECT usuario, nombres from aportes519 where estado='A' order by usuario asc";
	$rs=$db->querySimple($sql);
	
	?>
    <div id="tipo14" style="display:block">
    <table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
	<tr>
    <th colspan="4"><strong>Par&aacute;metros de Configuracion Reporte</strong></th>
    </tr>
	<tr>
		<td style="text-align:center">
			Fecha Inicial:
		</td>
		<td>
			<input type="text" id="txtFechaI" name="txtFechaI" readonly>
		</td>
		<td style="text-align:center">
			Fecha Final:
		</td>
		<td>
			<input type="text" id="txtFechaF" name="txtFechaF" readonly>
		</td>  
	</tr>
	<tr>
	  <td style="text-align:center">Usuario: </td>
		  <td>
	      <select id="txtUsuario2" name="txtUsuario2" class="box1">
	      <option value="T">TODOS</option>
		<?php
	 	 while($row=$rs->fetch()){
		  echo "<option value=".$row['usuario'].">".$row['nombres']."</option>";
	  		}
			?>
	      </select>
      	</td>
      <td> Sitio de actualizaci&oacute;n: </td>
		<td>
			<select id="cmbtipo" name="cmbtipo">
                <option value=0 selected="selected" >Todas</option>
                <option value=1>Radicacion</option>
                <option value=2>Afiliaciones</option>                               	  		
            </select>
		</td>
	  </tr>
	<tr>
	  <td colspan="4" style="text-align:center">
	  <label style="cursor:pointer" onClick="procesar18(1);"><img src="../../../imagenes/icono_pdf.png" width="32" height="32"></label>
      <label style="cursor:pointer" onClick="procesar18(2);"><img src="../../../imagenes/icono_excel.png" width="32" height="32"></label>
      </td>
	  </tr>
    </table>
<?php 
}

if($tipo==19){
	$sql="SELECT usuario, nombres from aportes519 where estado='A' order by usuario asc";
	$rs=$db->querySimple($sql);
	
	$sql1="Select * from aportes500";
	$agencia=$db->querySimple($sql1);
?>
	<div id="tipo1" style="display:block">
<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
	<tr>
    <th colspan="4"><strong>Par&aacute;metros de Configuracion Reporte</strong></th>
    </tr>
    <tr>
      <td width="25%">Agencia</td>
      <td width="25%">
      	<select name="cmbAgencia" class="box1" id="cmbAgencia">
					<option value="T">TODOS</option>
				<?php 
					while ($row1=$agencia->fetch()){
						echo "<option value=".$row1['codigo'].">".$row1['agencia']."</option>";
					}
				?>
		</select>
      </td>
      <td width="25%">Usuario</td>
      <td width="25%">
       <select id="txtUsuario2" name="txtUsuario2" class="box1">
	      <option value="T">TODOS</option>
		<?php
	 	 while($row=$rs->fetch()){
		  echo "<option value=".$row['usuario'].">".$row['nombres']."</option>";
	  		}
			?>
	      </select>
      </td>
    </tr>
    <tr>
      <td>Fecha Inicio</td>
      <td><input type="text" id="txtFechaI" name="txtFechaI" readonly></td>
      <td>Fecha Final</td>
      <td><input type="text" id="txtFechaF" name="txtFechaF" readonly></td>
    </tr>
    <tr>
      <td colspan="4">&nbsp;</td>
  </tr>
    <tr>
      <td colspan="4" style="text-align:center"><label class="RojoGrande" style="cursor:pointer" onClick="procesar19();">Procesar</label></td>
    </tr>
    </table>
 <?php
}
if($tipo==20){

	?>
    <div id="tipo14" style="display:block">
    <table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
	<tr>
    <th colspan="4"><strong>Par&aacute;metros de Configuracion Reporte</strong></th>
    </tr>
	<tr>
		<td style="text-align:center">
			Fecha Inicial:
		</td>
		<td>
			<input type="text" id="txtFechaI" name="txtFechaI" readonly>
		</td>
		<td style="text-align:center">
			Fecha Final:
		</td>
		<td>
			<input type="text" id="txtFechaF" name="txtFechaF" readonly>
		</td>  
	</tr>
		<td style="text-align:center">
			Nit:
		</td>
		<td>
			<input type="text" id="txtNit" name="txtNit" >
		</td>
		<td style="text-align:center">
			Tipo:
		</td>
		<td>
			<select id="cmbtipo" name="cmbtipo">
                <!--<option value=1>Consolidado</option>
                <option value=2>Discriminado</option>  --> 
                <option value=3>Nit</option>                               	  		
            </select>
		</td>  
	<tr>
	</tr>
	<tr>
	  <td colspan="4" style="text-align:center">
	  <label style="cursor:pointer" onClick="procesar20(1);"><img src="../../../imagenes/icono_pdf.png" width="32" height="32"></label>
      <label style="cursor:pointer" onClick="procesar20(2);"><img src="../../../imagenes/icono_excel.png" width="32" height="32"></label>
      </td>
	  </tr>
    </table>
<?php 
}

if($tipo==21){

	?>
    <div id="tipo14" style="display:block">
    <table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
	<tr>
    <th colspan="4"><strong>Par&aacute;metros de Configuracion Reporte</strong></th>
    </tr>
	<tr>
		<td style="text-align:center">
			Fecha Inicial:
		</td>
		<td>
			<input type="text" id="txtFechaI" name="txtFechaI" readonly>
		</td>
		<td style="text-align:center">
			Fecha Final:
		</td>
		<td>
			<input type="text" id="txtFechaF" name="txtFechaF" readonly>
		</td>  
	</tr>
	<tr>
		<td style="text-align:center">
			Nit:
		</td>
		<td colspan="3">
			<input type="text" id="txtNit" name="txtNit">
		</td>
	</tr>
	<tr>
	  <td colspan="4" style="text-align:center">
	  <label style="cursor:pointer" onClick="procesar21(1);"><img src="../../../imagenes/icono_pdf.png" width="32" height="32"></label>
      <label style="cursor:pointer" onClick="procesar21(2);"><img src="../../../imagenes/icono_excel.png" width="32" height="32"></label>
      </td>
	  </tr>
    </table>
<?php 
}

/*
 * Reporte Causales de giro de beneficiarios
 * 
 */
if($tipo==10){

	?>
    <div id="tipo14" style="display:block">
    <table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
	<tr>
    <th colspan="4"><strong>Par&aacute;metros de Configuracion Reporte</strong></th>
    </tr>
	<tr>
		<td style="text-align:center">
			Fecha Inicial:
		</td>
		<td>
			<input type="text" id="txtFechaIni" name="txtFechaIni" readonly>
		</td>
		<td style="text-align:center">
			Fecha Final:
		</td>
		<td>
			<input type="text" id="txtFechaFin" name="txtFechaFin" readonly>
		</td>  
	</tr>
	<tr>
		<td style="text-align:center">
			Identificacion Beneficiario:
		</td>
		<td colspan="3">
			<input type="text" id="txtIdentificacion" name="txtIdentificacion">
		</td>
	</tr>
	<tr>
		<td style="text-align:center">
			Sede:
		</td>
		<td colspan="3">
			<select name="cmbSedes" id="cmbSedes">
			    <option value="0">..Selccione..</option>
			    <option value="1">Neiva</option>
			    <option value="2">Garzon</option>
			    <option value="3">Pitalito</option>
			    <option value="4">La Plata</option>
			</select>
		</td>
	</tr>
	<tr>
	   <td colspan="4">&nbsp;</td>
	</tr>
	<tr>
	  <td colspan="4" style="text-align:center">
	        <label style="cursor:pointer" onClick="procesar10();"><img src="../../../imagenes/icono_excel.png" width="32" height="32"></label>
      </td>
	  </tr>
    </table>
    <div id="conten_table"></div></table>
<?php 
}
?>




<input type="hidden" id="txtusuario" name="txtusuario" value="<?php echo $usuario; ?>" /> 
<input type="hidden" id="txtRept" name="txtRept" value="<?php echo $rept; ?>" />
</body>

</html>