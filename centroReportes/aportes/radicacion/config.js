// funcion para obtener cookie en javascript
	function getCookie(c_name)
	{
		var i,x,y,ARRcookies=document.cookie.split(";");
		for (i=0;i<ARRcookies.length;i++)
		{
  			x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
  			y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
  			x=x.replace(/^\s+|\s+$/g,"");
  			if (x==c_name)
    			{
    				return unescape(y);
    			}// fin if
		}// fin for
  	}// fin getCookie
//funcion para obtener cookie en javascript
	
function procesar1(){
	var agencia=$("#cmbAgencia").val();
	var radica=$("#txtTipo").val();
	var fechaI=$("#txtFechaI").val();
	var fechaF=$("#txtFechaF").val();
	var usuario=$("#txtusuario").val();
	if(fechaI.length==0) return false;
	if(fechaF.length==0) return false;
	
	url="http://"+getCookie("URL_Reportes")+"/aportes/radicacion/reporte005.jsp?v0="+agencia+"&v1="+radica+"&v2="+fechaI+"&v3="+fechaF+"&v4="+usuario;
	window.open(url,"_NEW");
}

function procesar10(){
	//validacion de campos
	
	if($("#txtFechaIni").val()!="" || $("#txtIdentificacion").val()!=""){
		$.ajax({
			url:URL+"centroReportes/aportes/radicacion/reporteCausalesBanderaGiro.php",
			type:"POST",
			data:{
				  fechaIni:$("#txtFechaIni").val(),
				  fechaFin:$("#txtFechaFin").val(),
				  iden:$("#txtIdentificacion").val(),
				  idsede:$("#cmbSedes").val()
				  },
			//dataType:"json",
			async:false,
			beforeSend: function(objeto){
				dialogLoading('show');
			},
			complete: function(objeto, exito){
				dialogLoading('close');
				if(exito != "success"){
					alert("No se completo el proceso!");
				}
			}, 
			success:function(datos){
				$("#conten_table").html(datos);
			}
		});
		
		var f = new Date();
        $("#tabla_causales").table2excel({
           exclude: ".excludeThisClass",
           name: "Worksheet Name",
           fileext:'.xls',
           filename: "reporte"+f.getFullYear()+f.getMonth()+f.getDate()+f.getHours()+":"+f.getMinutes()+":"+f.getSeconds() //do not include extension
        });
        
	}else{
		alert('Debe diligenciar el rango de fechas o el Numero de Identificacion para continuar con el proceso.');
	}
}

function procesar2(){
	var fechaI=$("#txtFechaI").val();
	var usuario=$("#txtusuario").val();
	if(fechaI.length==0) return false;
	if($("#txtRept").val()==2)
		url="http://"+getCookie("URL_Reportes")+"/aportes/radicacion/reporte006.jsp?v0="+fechaI+"&v1="+usuario;
	if($("#txtRept").val()==3)
		url="http://"+getCookie("URL_Reportes")+"/aportes/radicacion/reporte007.jsp?v0="+fechaI+"&v1="+usuario;
	if($("#txtRept").val()==4)
		url="http://"+getCookie("URL_Reportes")+"/aportes/radicacion/reporte008.jsp?v0="+fechaI+"&v1="+usuario;
	window.open(url,"_NEW");
	}
	
function procesar3(op){
	var usuario=$("#txtUsuario2").val();
	if(op==1)
		url="http://"+getCookie("URL_Reportes")+"/aportes/radicacion/reporte013.jsp?v0="+usuario;
	window.open(url,"_NEW");
	}

function procesar14(op){
	var usuario=$("#txtusuario").val();
	var fechaIni=$("#txtFechaI").val();
	var fechaFin=$("#txtFechaF").val();
	var agencia=$("#cmbAgencia").val();
	var userAgencia=$("#cmbUsuario").val().split('-')[0];					
	var userNombre=$("#cmbUsuario").val().split('-')[1];
	
	if(fechaIni.length==0){
		alert("Ingrese la fecha inicial");
		return false;
	}
	if(fechaFin.length==0){
		alert("Ingrese la fecha final");
		return false;
	}
	if((fechaIni) > (fechaFin)){
		alert("Fecha inicial mayor a la final");
		return false;
	}
	
	if(userAgencia!="all")
		agencia=userAgencia;
	else
		userNombre="Sin nombre";
	
	if(op==1)
		url="http://"+getCookie("URL_Reportes")+"/aportes/radicacion/reporte014.jsp?v0="+fechaIni+"&v1="+fechaFin+"&v2="+agencia+"&v3="+usuario+"&v4="+userNombre;
	else if(op==2)
		url="http://"+getCookie("URL_Reportes")+"/aportes/radicacion/rptExcel014.jsp?v0="+fechaIni+"&v1="+fechaFin+"&v2="+agencia+"&v3="+usuario+"&v4="+userNombre;
	
	window.open(url,"_NEW");
	
}

function procesar15(op){
	var usuario=$("#txtusuario").val();
	var fechaIni=$("#txtFechaI").val();
	var fechaFin=$("#txtFechaF").val();				
	
	if(fechaIni.length==0){
		alert("Ingrese la fecha inicial");
		return false;
	}
	if(fechaFin.length==0){
		alert("Ingrese la fecha final");
		return false;
	}
	if((fechaIni) > (fechaFin)){
		alert("Fecha inicial mayor a la final");
		return false;
	}

	if(op==1)
		url="http://"+getCookie("URL_Reportes")+"/aportes/radicacion/reporte015.jsp?v0="+fechaIni+"&v1="+fechaFin+"&v2="+usuario;
	else if(op==2)
		url="http://"+getCookie("URL_Reportes")+"/aportes/radicacion/rptExcel015.jsp?v0="+fechaIni+"&v1="+fechaFin+"&v2="+usuario;
	
	window.open(url,"_NEW");
	
}


function procesar16(op){
	var usuario=$("#txtusuario").val();
	var fechaIni=$("#txtFechaI").val();
	var fechaFin=$("#txtFechaF").val();
	var agencia=$("#cmbAgencia").val();
	var userAgencia=$("#cmbUsuario").val().split('-')[0];					
	var userNombre=$("#cmbUsuario").val().split('-')[1];
	
	if(fechaIni.length==0){
		alert("Ingrese la fecha inicial");
		return false;
	}
	if(fechaFin.length==0){
		alert("Ingrese la fecha final");
		return false;
	}
	if((fechaIni) > (fechaFin)){
		alert("Fecha inicial mayor a la final");
		return false;
	}
	
	if(userAgencia!="all")
		agencia=userAgencia;
	else
		userNombre="Sin nombre";
	
	if(op==1)
		url="http://"+getCookie("URL_Reportes")+"/aportes/radicacion/reporte017.jsp?v0="+fechaIni+"&v1="+fechaFin+"&v2="+agencia+"&v3="+usuario+"&v4="+userNombre;
	else if(op==2)
		url="http://"+getCookie("URL_Reportes")+"/aportes/radicacion/rptExcel017.jsp?v0="+fechaIni+"&v1="+fechaFin+"&v2="+agencia+"&v3="+usuario+"&v4="+userNombre;
	
	window.open(url,"_NEW");
	
}

function procesar17(op){
	var usuario=$("#txtusuario").val();
	var fechaIni=$("#txtFechaI").val();
	var fechaFin=$("#txtFechaF").val();
	
	if(fechaIni.length==0){
		alert("Ingrese la fecha inicial");
		return false;
	}
	if(fechaFin.length==0){
		alert("Ingrese la fecha final");
		return false;
	}
	if((fechaIni) > (fechaFin)){
		alert("Fecha inicial mayor a la final");
		return false;
	}
	
	if(op==1)
		url="http://"+getCookie("URL_Reportes")+"/aportes/radicacion/reporte018.jsp?v0="+fechaIni+"&v1="+fechaFin+"&v2="+usuario;
	else if(op==2)
		url="http://"+getCookie("URL_Reportes")+"/aportes/radicacion/rptExcel018.jsp?v0="+fechaIni+"&v1="+fechaFin+"&v2="+usuario;
	
	window.open(url,"_NEW");
	
}


function procesar18(op){
	var usuario=$("#txtusuario").val();
	var fechaIni=$("#txtFechaI").val();
	var fechaFin=$("#txtFechaF").val();
	var usuario2=$("#txtUsuario2").val();
	var tipo=$("#cmbtipo").val();
	
	if((fechaIni) > (fechaFin)){
		alert("Fecha inicial mayor a la final");
		return false;
	}
	
	if(op==1)
		url="http://"+getCookie("URL_Reportes")+"/aportes/radicacion/reporte019.jsp?v0="+fechaIni+"&v1="+fechaFin+"&v2="+usuario+"&v3="+usuario2+"&v4="+tipo;
	else if(op==2)
		url="http://"+getCookie("URL_Reportes")+"/aportes/radicacion/rptExcel019.jsp?v0="+fechaIni+"&v1="+fechaFin+"&v2="+usuario+"&v3="+usuario2+"&v4="+tipo;
	
	window.open(url,"_NEW");
	
}

function procesar19(){
	var agencia=$("#cmbAgencia").val();
	var usuario=$("#txtUsuario2").val();
	var fechaI=$("#txtFechaI").val();
	var fechaF=$("#txtFechaF").val();
	if(fechaI.length==0) return false;
	if(fechaF.length==0) return false;
	
	url="http://"+getCookie("URL_Reportes")+"/aportes/radicacion/rptExcel020.jsp?v0="+agencia+"&v1="+usuario+"&v2="+fechaI+"&v3="+fechaF;
	window.open(url,"_NEW");
}


function procesar20(op){
	var usuario=$("#txtusuario").val();
	var fechaIni=$("#txtFechaI").val();
	var fechaFin=$("#txtFechaF").val();
	var nit=$("#txtNit").val();
	var tipo=$("#cmbtipo").val();
	
	if(fechaIni.length==0){
		alert("Ingrese la fecha inicial");
		return false;
	}
	if(fechaFin.length==0){
		alert("Ingrese la fecha final");
		return false;
	}
	
	if((fechaIni) > (fechaFin)){
		alert("Fecha inicial mayor a la final");
		return false;
	}
	
	if (tipo==1){
		
		if(op==1)
			url="http://"+getCookie("URL_Reportes")+"/aportes/radicacion/reporte021A.jsp?v0="+fechaIni+"&v1="+fechaFin+"&v2="+usuario+"&v3="+nit;
		else if(op==2)
			url="http://"+getCookie("URL_Reportes")+"/aportes/radicacion/rptExcel021A.jsp?v0="+fechaIni+"&v1="+fechaFin+"&v2="+usuario+"&v3="+nit;
	}
	
	if (tipo==2){
		
		 if(op==2)
			 $.ajax({
					url:URL+"centroReportes/aportes/radicacion/suspensionesBanderaGiroDiscriminado.php",
					type:"POST",
					data:{fechaIni:fechaIni,fechaFin:fechaFin,nit:nit},
					//dataType:"json",
					async:false,
					beforeSend: function(objeto){
						dialogLoading('show');
					},
					complete: function(objeto, exito){
						dialogLoading('close');
						if(exito != "success"){
							alert("No se completo el proceso!");
						}
					}, 
					success:function(datos){
						if(datos==1){
							var url=URL+'phpComunes/descargaPlanos.php';
							window.open(url);
						}
					}
				});
		
	}
	
if (tipo==3){
		 if(op==2)
			 $.ajax({
					url:URL+"centroReportes/aportes/radicacion/suspensionesBanderaGiro.php",
					type:"POST",
					data:{fechaIni:fechaIni,fechaFin:fechaFin,nit:nit},
					//dataType:"json",
					async:false,
					beforeSend: function(objeto){
						dialogLoading('show');
					},
					complete: function(objeto, exito){
						dialogLoading('close');
						if(exito != "success"){
							alert("No se completo el proceso!");
						}
					}, 
					success:function(datos){
						if(datos==1){
							var url=URL+'phpComunes/descargaPlanos.php';
							window.open(url);
						}
					}
				});
	}
	
	//window.open(url,"_NEW");
	
}

function procesar21(op){
	var usuario=$("#txtusuario").val();
	var fechaIni=$("#txtFechaI").val();
	var fechaFin=$("#txtFechaF").val();
	var nit=$("#txtNit").val();
	
	if((fechaIni) > (fechaFin)){
		alert("Fecha inicial mayor a la final");
		return false;
	}
	
	if(op==1)
		url="http://"+getCookie("URL_Reportes")+"/aportes/radicacion/reporte022.jsp?v0="+fechaIni+"&v1="+fechaFin+"&v2="+usuario+"&v3="+nit;
	else if(op==2)
		url="http://"+getCookie("URL_Reportes")+"/aportes/radicacion/rptExcel022.jsp?v0="+fechaIni+"&v1="+fechaFin+"&v2="+usuario+"&v3="+nit;
	
	window.open(url,"_NEW");
	
}


function changeAgencia(){	
		var idAgencia = $("#cmbAgencia").val();
		if(idAgencia!="all"){
			
			if (idAgencia=="sub") idAgencia="01";
			else if (idAgencia=="gar") idAgencia="02";
			else if (idAgencia=="pit") idAgencia="03";
			else idAgencia="04";
			
			var combo = "<option value='all'>Todos</option>";
			$.ajax({
				type: "POST",
				url: URL+"phpComunes/buscarUsuario.php",
				data: {v0: idAgencia},
				async: false,
				dataType: "json",
				success:function(datos){
					if(datos != 0){
						$.each(datos,function(c,data){
							combo +="<option value='"+data.usuario+"-"+data.nombres+"'>"+data.nombres+"</option>";
						});
					}
				}
			});
			$("#cmbUsuario").html(combo);
		}
		else{
			$("#cmbUsuario").html("<option value='all'>Debe seleccionar una agencia</option>");
		}
}
$(function (){
	$("#txtFechaIni").datepicker({
		maxDate:"+0D",
		changeMonth: true,
		changeYear: true,
		onSelect:function(date){
		$('#txtFechaFin').val(date);
		$('#txtFechaFin').datepicker("option","minDate",new Date(date));
		}
	});
	$("#txtFechaFin").datepicker({
		maxDate:"+0D",
		changeMonth: true,
		changeYear: true,
		onSelect:function(date){
		}
	});
});
