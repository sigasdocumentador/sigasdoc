
<?php
date_default_timezone_set('America/Bogota');
set_time_limit(0);
$raiz="";
//ini_set("display_errors",'1');
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
include_once '../../../config.php';

$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

$path_plano =$ruta_generados.'aportes'.DIRECTORY_SEPARATOR.'suspensiones'.DIRECTORY_SEPARATOR;

$fecha=date('Ymd');
$archivo='suspensiones'.$fecha.'.csv';
$path_plano.=$archivo;

$fechaIni=$_REQUEST['fechaIni'];
$fechaFin=$_REQUEST['fechaFin'];
$nit=$_REQUEST['nit'];

$querySql = "SELECT a48.nit,a48.razonsocial,a48.direccion AS direccionEmp,a500.agencia AS agenciaEmp,
(SELECT TOP 1 a89.municipio FROM aportes089 a89 WHERE a89.coddepartamento=a48.iddepartamento AND a89.codmunicipio=a48.idciudad) AS municipioEmp,
(select top 1 a87.barrio from aportes087 a87 inner join aportes089 c89 on c89.codzona=a87.codigozona where a87.idbarrio=a48.idbarrio) as BARRIOEMPRE,
a48.telefono AS teleEmp,a48.celular celularEmp,a48.email AS emailEmp
FROM aportes021A b21
INNER JOIN aportes021 a21 ON a21.idrelacion=b21.id_relacion AND a21.estado='A'
INNER JOIN aportes016 a16 ON a16.idpersona=a21.idtrabajador
INNER JOIN aportes048 a48 ON a48.idempresa=a16.idempresa
INNER JOIN aportes015 a15 ON a15.idpersona=a16.idpersona
INNER JOIN aportes015 b15 ON b15.idpersona=a21.idbeneficiario
INNER JOIN aportes519 a519 ON a519.usuario=b21.usuario_creacion
INNER JOIN aportes091 a91 ON a91.iddetalledef=a21.idparentesco
INNER JOIN aportes091 b91 ON b91.iddetalledef=b15.idtipodocumento
LEFT JOIN aportes500 a500 ON a500.codigo=a48.seccional
LEFT JOIN aportes500 b500 ON b500.codigo=a16.idagencia
WHERE convert(DATE,b21.fecha_creacion) BETWEEN '$fechaIni' AND '$fechaFin' AND a48.nit='$nit' AND
(SELECT count(*) FROM aportes088 a88 WHERE a88.idregistro=a15.idpersona AND a88.identidad=1 AND a88.observaciones LIKE '%SUSPENSION%')>0
GROUP BY a48.nit,a48.razonsocial,a48.direccion,a500.agencia,a48.iddepartamento,a48.idciudad,a48.idbarrio,a48.telefono,a48.celular,a48.email
ORDER BY a48.nit,a48.razonsocial ASC";
$rsQuery = $db->querySimple($querySql);

$cadena2 = "NIT;RAZON SOCIAL;DIRECCION;AGENCIA;MUNICIPIO;BARRIO;TELEFONO;CELULAR;CORREO\r\n";

while(($row = $rsQuery->fetch())==true){
	
	$cadena2 .= "{$row["nit"]};{$row["razonsocial"]};{$row["direccionEmp"]};{$row["agenciaEmp"]};{$row["municipioEmp"]};{$row["BARRIOEMPRE"]};{$row["teleEmp"]};{$row["celularEmp"]};{$row["emailEmp"]}\r\n\r\n";
	
}

$cadena = "TRABAJADOR;NOMBRE TRABAJADOR;DIRECCION;MUNICIPIO;BARRIO;TELEFONO;"
			."CELULAR;CORREO;FECHA CREACION;BANDERA DE GIRO MODIFICADO;"
			."BANDERA DE GIRO ACTUAL;ULTIMA FECHA ACTUALIZACION (S);ULTIMO PERIODO GIRO;OBSERVACIONES\r\n";


$querySql = "SELECT
a15.identificacion AS trabajador,
a15.pnombre+' '+isnull(a15.snombre,'')+' '+a15.papellido+' '+isnull(a15.sapellido,'') AS trabajadornombre,
a15.direccion,
(SELECT TOP 1 b89.municipio FROM aportes089 b89 WHERE b89.coddepartamento=a15.iddepresidencia AND b89.codmunicipio=a15.idciuresidencia) AS municipio,
(select top 1 a87.barrio from aportes087 a87 inner join aportes089 c89 on c89.codzona=a87.codigozona where a87.idbarrio=a15.idbarrio) as BARRIO,
a15.telefono,a15.celular,a15.email,b21.fecha_creacion,
b21.bandera_giro,a21.giro AS banderaActual,
(SELECT max(c21.fecha_creacion) FROM aportes021A c21 WHERE c21.id_relacion=b21.id_relacion AND c21.bandera_giro='S') AS ult_fecha_actua_banderaS,
(SELECT max(a9.periodo) FROM aportes009 a9 WHERE a9.idtrabajador=a15.idpersona AND a9.idbeneficiario=b15.idpersona AND a9.periodo<>'BNSP99' AND a9.periodo<>'BN0297') AS ult_periodo_giro,
(SELECT TOP 1 b88.observaciones FROM aportes088 b88 WHERE b88.idregistro=a15.idpersona AND b88.identidad=1) AS observaciones
FROM aportes021A b21
INNER JOIN aportes021 a21 ON a21.idrelacion=b21.id_relacion AND a21.estado='A'
INNER JOIN aportes016 a16 ON a16.idpersona=a21.idtrabajador
INNER JOIN aportes048 a48 ON a48.idempresa=a16.idempresa
INNER JOIN aportes015 a15 ON a15.idpersona=a16.idpersona
INNER JOIN aportes015 b15 ON b15.idpersona=a21.idbeneficiario
INNER JOIN aportes519 a519 ON a519.usuario=b21.usuario_creacion
INNER JOIN aportes091 a91 ON a91.iddetalledef=a21.idparentesco
INNER JOIN aportes091 b91 ON b91.iddetalledef=b15.idtipodocumento
LEFT JOIN aportes500 a500 ON a500.codigo=a48.seccional
LEFT JOIN aportes500 b500 ON b500.codigo=a16.idagencia
WHERE convert(DATE,b21.fecha_creacion) BETWEEN '$fechaIni' AND '$fechaFin' AND a48.nit='$nit' AND
(SELECT count(*) FROM aportes088 a88 WHERE a88.idregistro=a15.idpersona AND a88.identidad=1 AND a88.observaciones LIKE '%SUSPENSION%')>0
GROUP BY a15.identificacion,a15.idpersona,b15.idpersona,
a15.pnombre,a15.snombre,a15.papellido,a15.sapellido,
a15.direccion,a15.iddepresidencia,a15.idciuresidencia,a15.idbarrio,a15.telefono,a15.celular,a15.email,
b21.fecha_creacion,a91.detalledefinicion,b21.bandera_giro,a21.giro,b21.id_relacion
ORDER BY a15.identificacion,b21.fecha_creacion ASC";

$rsQuery = $db->querySimple($querySql);
while(($row = $rsQuery->fetch())==true){
	
	$row = array_map("utf8_decode",$row);
	$observaciones = eregi_replace("[\n|\r|\n\r]",' ',$row["observaciones"]);
	//$observaciones = trim($row["observaciones"]);
	$cadena .= "{$row["trabajador"]};{$row["trabajadornombre"]};{$row["direccion"]};{$row["municipio"]};{$row["BARRIO"]};{$row["telefono"]};"
				."{$row["celular"]};{$row["email"]};{$row["fecha_creacion"]};{$row["bandera_giro"]};{$row["banderaActual"]};{$row["ult_fecha_actua_banderaS"]};{$row["ult_periodo_giro"]};"
				."{$observaciones}\r\n";
}

$fp=fopen($path_plano,'w');
fwrite($fp, $cadena2);
fwrite($fp, $cadena);
fclose($fp);

$_SESSION['ENLACE']=$path_plano;
$_SESSION['ARCHIVO']=$archivo;
echo 1;
exit();

?>

