<?php
/* autor:       orlando puentes
 * fecha:       28/09/2010
 * objetivo:    

if ( !isset( $_SESSION ['USUARIO'] ) ) {session_start();}
$raiz=$_SESSION['RAIZ'];
chdir($raiz);
*/
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$usuario=$_SESSION['USUARIO'];
include_once $raiz.DIRECTORY_SEPARATOR ."config.php";

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Men&uacute; Reportes Radicaci&oacute;n</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link type="text/css" href="../../../css/Estilos.css" rel="stylesheet"/>
</head>
<body>
	<center>
	<table width="100%" border="0">
	<tr>
	<td align="center" ><img src="../../../imagenes/logo_reporte.png"></td>
	</tr>
	<tr>
	<td align="center" >Listado de Reportes Radicaci&oacute;n</td>
	</tr>
	</table>
	<br>
	<table width="60%" border="0" cellspacing="1" class="tablero">
		<tr>
			<th>Reporte</th>
			<th colspan="2">Descripci&oacute;n</th></tr>  
	<tr>
	   <td scope="row" align="left">Reporte001</td>             
	   <td align="left"><a style="text-decoration:none" target='_blank' href="../../configurarReporte.php?tipo=<?php echo 7;?>&modulo=aportes/radicacion&v0=<?php echo '001';?>&menurpte=aportes/radicacion/menuRptRadicacion.php&plano=<?php echo 0;?>" >Listado de Radicaciones  </a></td>
    </tr>
	<tr>
	   <td scope="row" align="left">Reporte002</td>             
	   <td align="left"><a style="text-decoration:none" target='_blank' href="../../configurarReporte.php?tipo=<?php echo 11;?>&modulo=aportes/radicacion&v0=<?php echo '002';?>&menurpte=aportes/radicacion/menuRptRadicacion.php&plano=<?php echo 0;?>" >Listado Consolidado de Radicaciones  </a></td>
    </tr>
	<tr>
	   <td scope="row" align="left">Reporte003</td>             
	   <td align="left"><a style="text-decoration:none" target='_blank' href="../../configurarReporte.php?tipo=<?php echo 11;?>&modulo=aportes/radicacion&v0=<?php echo '003';?>&menurpte=aportes/radicacion/menuRptRadicacion.php&plano=<?php echo 0;?>" >Listado Afiliaciones Multiples Radicadas  </a></td>
    </tr>		
	
	<tr>
	  <td scope="row" align="left">Reporte004</td>  
	  <td align="left"><a style="text-decoration:none" target='_blank' href="../../configurarReporte.php?tipo=13&modulo=aportes/radicacion&v0=004" >Reporte Habeas Data</a></td>
	 </tr>
	<tr>
		<td>Reporte005</td>
		<td><a href="configReporteRadica.php?tipo=1" target="_new">Radicaciones por Agencia - Tipo - Fecha</a></td>
	</tr>
	<!-- <tr>
		<td>Reporte006</td>
		<td><a href="<?php echo URL_PORTAL.DIRECTORIO_CENTRO_REPORTES; ?>aportes/radicacion/configReporte004.php">Resumen de radicaci&oacute;n por usuario</a></td>
	</tr>
	<tr>
		<td>Reporte007</td>
		<td><a href="<?php echo URL_PORTAL.DIRECTORIO_CENTRO_REPORTES; ?>aportes/radicacion/reporte005.php">Reporte de radicaci&oacute;n por tipo</a></td>
	</tr>
        <tr>
			<td>Reporte008</td>
			<td><a href="<?php echo URL_PORTAL.DIRECTORIO_CENTRO_REPORTES; ?>aportes/radicacion/configReporte004.php">Planilla de Cierre de Ventanilla</a></td>
		</tr>
        <tr>
			<td>Reporte009</td>
			<td><a href="<?php echo URL_PORTAL.DIRECTORIO_CENTRO_REPORTES; ?>aportes/radicacion/configReporte004.php">Resumen Cierre</a></td>
		</tr> -->
         <tr>
			<td>Reporte010</td>
			<td><a href="configReporteRadica.php?tipo=10&tit=10" target="_new">Causales de giro beneficiario</a></td>
		</tr>
        <tr>
			<td>Reporte011</td>
			<td></td>
		</tr>
        <tr>
			<td>Reporte012</td>
			<td></td>
		</tr>
        <tr>
			<td>Reporte013</td>
			<td><a href="configReporteRadica.php?tipo=3" target="_new">Reclamos Pendientes por Procesar</a></td>
		</tr>
		<tr>
			<td>Reporte014</td>
			<td><a href="configReporteRadica.php?tipo=14&tit=14" target="_new">Listado de reclamos no Giro por trabajador</a></td>
		</tr>
		<tr>
			<td>Reporte015</td>
			<td><a href="configReporteRadica.php?tipo=15&tit=15" target="_new">Listado de radicados anulados</a></td>
		</tr>
		<tr>
			<td>Reporte016</td>
			<td><a href="configReporteRadica.php?tipo=16&tit=16" target="_new">Listado de Reclamos (Servicio al Cliente)</a></td>
		</tr>
		<tr>
			<td>Reporte017</td>
			<td><a href="configReporteRadica.php?tipo=17&tit=17" target="_new">Motivos de las movilizaciones</a></td>
		</tr>
		<tr>
			<td>Reporte018</td>
			<td><a href="configReporteRadica.php?tipo=18&tit=18" target="_new">Informe actualizaci&oacute;n datos persona</a></td>
		</tr>
		<tr>
	   <td scope="row" align="left">Reporte019</td>             
	   <td align="left"><a style="text-decoration:none" target='_blank' href="configReporteRadica.php?tipo=19&tit=19" >Listado de Radicaciones Tipo Informacion </a></td>
    </tr>
    <tr>
			<td>Reporte020</td>
			<td><a href="configReporteRadica.php?tipo=20&tit=20" target="_new">Informe Cambio de Bandera de Giro</a></td>
	</tr>
	<tr>
			<td>Reporte021</td>
			<td><a href="configReporteRadica.php?tipo=21&tit=21" target="_new">Radicado Por Empresa Pendiente De Grabar</a></td>
	</tr>	
	<tr>
			<td>Reporte022</td>
			<td><a href="<?php echo $ruta_reportes;?>aportes/radicacion/rptExcel023.jsp?v0=<?php echo $usuario; ?>" target="_new">Relacion de Reclamos No Giro por Beneficiarios</a></td>
	</tr>	
	</table>
	</center>
</body> 
</html>
