// funcion para obtener cookie en javascript
	function getCookie(c_name)
	{
		var i,x,y,ARRcookies=document.cookie.split(";");
		for (i=0;i<ARRcookies.length;i++)
		{
  			x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
  			y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
  			x=x.replace(/^\s+|\s+$/g,"");
  			if (x==c_name)
    			{
    				return unescape(y);
    			}// fin if
		}// fin for
  	}// fin getCookie
//funcion para obtener cookie en javascript


$(document).ready(function(){
	$("#txtFecha").datepicker({
		changeMonth: true,
		changeYear: true,
		maxDate: "+0D"
	});		
});

$(document).ready(function(){
	$("#txtFecha2").datepicker({
		changeMonth: true,
		changeYear: true,
		maxDate: "+0D"
	});		
});

$(document).ready(function(){
	$("#txtFecha3").datepicker({
		changeMonth: true,
		changeYear: true		
	});		
});


$(document).ready(function(){
	$('#txtFechaI').datepicker({
		maxDate:"+0D",
		changeMonth: true,
		changeYear: true,
		onSelect: function(dateText, inst) {
			var lockDate = new Date($('#txtFechaI').datepicker('getDate'));
			$('input#txtFechaF').datepicker('option', 'minDate', lockDate);
		}
	});
	$("#txtFechaF").datepicker({
		maxDate:"+0D",
		changeMonth: true,
		changeYear: true
	});
});


function mostrar(){
	var opt=$("#txtTipo").val();
	if(opt=="A"){
		$("#TRperiodo").hide();
		}
	if(opt=="H"){
		$("#TRperiodo").show();
		}
	
	}

function procesar11(op){
	var usuario=$("#txtusuario").val();
	var fechaCorte=$("#txtFecha").val();
	var fechaCorte2=$("#txtFecha2").val();
	var agencia=$("#cmbAgencia").val();
	
	if(fechaCorte.length==0 || fechaCorte2.length==0){
		alert("Ingrese la fecha de corte");
		return false;
	}
	
	if(op==0){		
		url="http://"+getCookie("URL_Reportes")+"/aportes/trabajadores/reporte011.jsp?v0="+fechaCorte+"&v1="+agencia+"&v2="+usuario+"&v3="+fechaCorte2;	
	} else if (op==1) {
		url="http://"+getCookie("URL_Reportes")+"/aportes/trabajadores/rptExcel011.jsp?v0="+fechaCorte+"&v1="+agencia+"&v2="+usuario+"&v3="+fechaCorte2;
	}
	
	window.open(url,"_NEW");
}


function procesar13(op){
	var usuario=$("#txtusuario").val();
	var fechaCorte=$("#txtFechaI").val();
	var fechaCorte2=$("#txtFechaF").val();
	var TipAfil=$("#cmbAfiliacion").val();
	
	if(fechaCorte.length==0 || fechaCorte2.length==0){
		alert("Ingrese la fecha de corte");
		return false;
	}
	
	if(op==0){		
		url="http://"+getCookie("URL_Reportes")+"/aportes/trabajadores/reporte014.jsp?v0="+fechaCorte+"&v1="+TipAfil+"&v2="+usuario+"&v3="+fechaCorte2;	
	} else if (op==1) {
		url="http://"+getCookie("URL_Reportes")+"/aportes/trabajadores/rptExcel014.jsp?v0="+fechaCorte+"&v1="+TipAfil+"&v2="+usuario+"&v3="+fechaCorte2;
	}
	
	window.open(url,"_NEW");
}

function procesar18(op){
	var usuario=$("#txtusuario").val();
	var periodoIni=$("#txtPeriodo").val();
	var periodoFin=$("#txtPeriodo2").val();				
	
	if(periodoIni.length==0){
		alert("Ingrese el periodo inicial");
		return false;
	}
	if(periodoFin.length==0){
		alert("Ingrese el periodo final");
		return false;
	}
	if((periodoIni) > (periodoFin)){
		alert("Fecha inicial mayor a la final");
		return false;
	}

	if(op==1)
		url="http://"+getCookie("URL_Reportes")+"/aportes/trabajadores/reporte018.jsp?v0="+periodoIni+"&v1="+periodoFin+"&v2="+usuario;
	else if(op==2)
		url="http://"+getCookie("URL_Reportes")+"/aportes/trabajadores/rptExcel018.jsp?v0="+periodoIni+"&v1="+periodoFin+"&v2="+usuario;
	
	window.open(url,"_NEW");
	
}

function procesar19(op){
	var usuario=$("#txtusuario").val();
	var fechaInicio=$("#txtFechaI").val();
	var fechaFin=$("#txtFechaF").val();				
	
	if(fechaInicio.length==0){
		alert("Ingrese la fecha inicial");
		return false;
	}
	if(fechaFin.length==0){
		alert("Ingrese la fecha final");
		return false;
	}

	if(op==1)
		url="http://"+getCookie("URL_Reportes")+"/aportes/trabajadores/reporte019.jsp?v0="+fechaInicio+"&v1="+fechaFin+"&v2="+usuario;
	else if(op==2)
		url="http://"+getCookie("URL_Reportes")+"/aportes/trabajadores/rptExcel019.jsp?v0="+fechaInicio+"&v1="+fechaFin+"&v2="+usuario;
	
	window.open(url,"_NEW");
	
}

function procesar20(op){
	var usuario=$("#txtusuario").val();
	var periodoIni=$("#txtPeriodo").val();			
	
	if(periodoIni.length==0){
		alert("Ingrese el periodo");
		return false;
	}

	if(op==1)
		url="http://"+getCookie("URL_Reportes")+"/aportes/trabajadores/reporte020.jsp?v0="+periodoIni+"&v1="+usuario;
	else if(op==2)
		url="http://"+getCookie("URL_Reportes")+"/aportes/trabajadores/rptExcel020.jsp?v0="+periodoIni+"&v1="+usuario;
	
	window.open(url,"_NEW");
	
}

function procesar23(op){
	var usuario=$("#txtusuario").val();
	var periodoIni=$("#txtPeriodo").val();
	var periodoFin=$("#txtPeriodo2").val();	
	var tipo=$("#txtTipo").val();
	
	
	if (tipo=="H"){
	if(periodoIni.length==0){
		alert("Ingrese el periodo inicial");
		return false;
	}
	if(periodoFin.length==0){
		alert("Ingrese el periodo final");
		return false;
	}
	if((periodoIni) > (periodoFin)){
		alert("Fecha inicial mayor a la final");
		return false;
	}
		}//Fin if (tipo=="H"){

	if(op==1)
		url="http://"+getCookie("URL_Reportes")+"/aportes/trabajadores/reporte023.jsp?v0="+periodoIni+"&v1="+periodoFin+"&v2="+usuario+"&v3="+tipo;
	else if(op==2)
		url="http://"+getCookie("URL_Reportes")+"/aportes/trabajadores/rptExcel023.jsp?v0="+periodoIni+"&v1="+periodoFin+"&v2="+usuario+"&v3="+tipo;
	
	window.open(url,"_NEW");
	
}


function procesar25(op){
	var usuario=$("#txtusuario").val();
	var fechaInicial=$("#txtFechaI").val();
	var fechaFinal=$("#txtFechaF").val();				
	
	if(fechaInicial.length==0){
		alert("Ingrese la fecha inicial");
		return false;
	}
	if(fechaFinal.length==0){
		alert("Ingrese la fecha final");
		return false;
	}

	if(op==1)
		url="http://"+getCookie("URL_Reportes")+"/aportes/planilla/reporte007.jsp?v0="+fechaInicial+"&v1="+fechaFinal+"&v2="+usuario;
	else if(op==2)
		url="http://"+getCookie("URL_Reportes")+"/aportes/planilla/rptExcel007.jsp?v0="+fechaInicial+"&v1="+fechaFinal+"&v2="+usuario;
	
	window.open(url,"_NEW");
	
}





function procesar10(op){
	var usuario=$("#txtusuario").val();
	var fechaini=$("#txtFecha").val();
	var fechafin=$("#txtFecha3").val();
	var periodoini=$("#txtPeriodo").val();
	var periodofin=$("#txtPeriodo2").val();
	var agencia=$("#cmbAgencia").val();
	
	if(fechaini.length==0 || fechafin.length==0){
		alert("Ingrese el rango de fechas");
		return false;
	}
	
	if(periodoini.length==0 || periodofin.length==0){
		alert("Ingrese el rango de periodos");
		return false;
	}
	
	
	var fechanac = fechaini.substring(0,4);
	var fechaano = fechanac - 12;
	
	fechaini = fechaano+"/"+fechaini.substring(5,10);
	fechafin = fechaano+"/"+fechafin.substring(5,10);
	
	if (op==1) {
		url="http://"+getCookie("URL_Reportes")+"/aportes/trabajadores/rptExcel013.jsp?v0="+fechaini+"&v1="+fechafin+"&v2="+periodoini+"&v3="+periodofin+"&v4="+agencia+"&v5="+usuario;
	}else
		{
		url="http://"+getCookie("URL_Reportes")+"/aportes/trabajadores/reporte013.jsp?v0="+fechaini+"&v1="+fechafin+"&v2="+periodoini+"&v3="+periodofin+"&v4="+agencia+"&v5="+usuario;
		}
	
	window.open(url,"_NEW");
}