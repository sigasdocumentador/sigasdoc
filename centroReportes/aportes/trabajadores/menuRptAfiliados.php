<?php
/* autor:       orlando puentes
 * fecha:       28/09/2010
 * objetivo:    

if ( !isset( $_SESSION ['USUARIO'] ) ) {session_start();}
$raiz=$_SESSION['RAIZ'];
chdir($raiz);
*/
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$usuario=$_SESSION['USUARIO'];
include_once $raiz.DIRECTORY_SEPARATOR ."config.php";

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Men&uacute; Reportes Afiliados</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link type="text/css" href="../../../css/Estilos.css" rel="stylesheet"/>
</head>
<body>
	<center>
	<table width="100%" border="0">
	<tr>
	<td align="center" ><img src="../../../imagenes/logo_reporte.png"></td>
	</tr>
	<tr>
	<td align="center" >Listado de Reportes Afiliados</td>
	</tr>
	</table>
	<br>
	<table width="60%" border="0" cellspacing="1" class="tablero">
		<tr>
			<th>Reporte</th>
			<th colspan="2">Descripci&oacute;n</th></tr>  
	<tr>
	   <td scope="row" align="left">Reporte001</td>             
	   <td align="left"><a style="text-decoration:none" target='_blank' href="../../configurarReporte.php?tipo=<?php echo 7;?>&modulo=aportes/radicacion&v0=<?php echo '001';?>&menurpte=aportes/radicacion/menuRptRadicacion.php&plano=<?php echo 0;?>" >Listado de Radicaciones  </a></td>
    </tr>
	<tr>
	   <td scope="row" align="left">Reporte002</td>             
	   <td align="left"><a style="text-decoration:none" target='_blank' href="../../configurarReporte.php?tipo=<?php echo 11;?>&modulo=aportes/radicacion&v0=<?php echo '002';?>&menurpte=aportes/radicacion/menuRptRadicacion.php&plano=<?php echo 0;?>" >Listado Consolidado de Radicaciones  </a></td>
    </tr>
		<tr>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>
	  </tr>
		<tr>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>
	    </tr>
	    
		<tr>
			<td>Reporte003</td>
			<td></td>
		</tr>
		<tr>
			<td>Reporte004</td>
			<td></td>
		</tr>
		<tr>
			<td>Reporte005</td>
			<td></td>
		</tr>
		<tr>
			<td>Reporte006</td>
			<td></td>
		</tr>
		<tr>
			<td>Reporte007</td>
			<td></td>
		</tr>
        <tr>
			<td>Reporte008</td>
			<td></td>
		</tr>
        <tr>
			<td>Reporte009</td>
			<td></td>
		</tr>
		<!--  <tr>
			<td>Reporte010</td>
			<td align="left"><a href="http://:8080/sigasReportes/aportes/defunciones/reporte001.jsp?v0=<?php /*echo $usuario;*/?>" style="text-decoration:none" target="_blank">Subsidio auxilio funebre pendiente de cancelar</a></td>
		</tr>-->
		<tr>
		   	<td scope="row" align="left">Reporte010</td>             
		   	<td align="left">Subsidio auxilio funebre pendiente de cancelar <a href="<?php echo $ruta_reportes; ?>/aportes/defunciones/reporte001.jsp?v0=<?php echo $usuario; ?>" style="text-decoration:none" target="_blank"><img src="../../../imagenes/pdf16x16.png" width="16" height="16"></a> <a href="<?php echo $ruta_reportes; ?>aportes/defunciones/rptExcel001.jsp?v0=<?php echo $usuario; ?>" style="text-decoration:none"> <img src="../../../imagenes/excel16x16.png" width="16" height="16"></a></td>
   		</tr>
		<tr>
			<td>Reporte011</td>
			<td><a href="configRptAfiliados.php?tipo=11&tit=11" target="_new">Listado afiliados inactivos a fecha de corte</a></td>
		</tr>
		<tr>
			<td>Reporte012</td>
			<td><a href="../../aportes/defunciones/menuRptDefunciones.php?tipo=1&tit=1">Listado Fallecidos</a></td>
		</tr>
		<tr>
			<td>Reporte013</td>
			<td><a href="configRptAfiliados.php?tipo=12&tit=12">Listado Beneficiarios 12 a&ntilde;os</a></td>
		</tr>
		<tr>
			<td>Reporte014</td>
    		<td ><a style="text-decoration:none" target='_blank' href="../../configurarReporte.php?tipo=<?php echo 25;?>" >Informes Prescripciones</a></td>
    	</tr>
    	 <tr>
	   		<td scope="row" align="left">Reporte015</td>
	   		<td align="left"><a style="text-decoration:none" target='_blank' href="../../configurarReporte.php?tipo=<?php echo 26;?>" >Listado de ABONOS pendientes de procesar</a></td>
    	</tr>
    	<tr>
			<td>Reporte016</td>
			<td><a href="configRptAfiliados.php?tipo=13&tit=13" target="_new">Listado Trabajadores Tipo Afiliacion</a></td>
		</tr>
		<tr>
			<td>Reporte017</td>
			<td><a href="configRptAfiliados.php?tipo=18&tit=18" target="_new">Reporte de discapacidad</a></td>
		</tr>
		<tr>
			<td>Reporte018</td>
			<td><a href="configRptAfiliados.php?tipo=19&tit=19" target="_new">Beneficiarios Inactivos Mayores De 19 A&ntilde;os</a></td>
		</tr>
		<tr>
			<td>Reporte019</td>
			<td><a href="../../../aportes/secretaria/subirPlanoDian.html" target="_new">Reporte DIAN</a></td>
		</tr>
		<tr>
			<td>Reporte020</td>
			<td><a href="configRptAfiliados.php?tipo=20&tit=20" target="_new">Reporte salario del a&ntilde;o anterior</a></td>
		</tr>
		<tr>
			<td>Reporte021</td>
			<td align="left"><a href="<?php echo $ruta_reportes; ?>/aportes/trabajadores/rptExcel021.jsp?v0=<?php echo $usuario;?>" style="text-decoration:none" target="_blank">Listado afiliados inactivos (Fidelidad)</a></td>
		</tr
		<tr>
	        <td>Reporte022</td>
	        <td><a href="../../aportes/giro/configReporteCuota.php?tipo=16&tit=16" target="_new">Informe pago del subsidio por embargo o discapacitado.</a></td>
    	</tr>
    	<tr>
			<td>Reporte023</td>
			<td><a href="configRptAfiliados.php?tipo=23&tit=23" target="_new">Reporte de embargos</a></td>
		</tr>
		<tr>
			<td>Reporte024</td>
			<td><a href="<?php echo $ruta_reportes; ?>/aportes/trabajadores/rptExcel024.jsp?v0=<?php echo $usuario;?>" target="_new">Reporte de Pensionados e Independientes</a></td>
		</tr>
		<tr>
			<td>Reporte025</td>
			<td><a href="configRptAfiliados.php?tipo=25&tit=25" target="_new">Listado Trabajadores Novedad Ingreso y Retiro</a></td>
		</tr>
	</table>
	</center>
</body> 
</html>
