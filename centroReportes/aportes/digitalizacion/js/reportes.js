/*
* Autor: Orlando Puentes Andrade
* Objetivo: Archivo para informe mensual de aportes
* Fecha: 24-Mar-2011
* ----------- E M P R E S A -------------- 
*/


// funcion para obtener cookie en javascript
	function getCookie(c_name)
	{
		var i,x,y,ARRcookies=document.cookie.split(";");
		for (i=0;i<ARRcookies.length;i++)
		{
  			x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
  			y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
  			x=x.replace(/^\s+|\s+$/g,"");
  			if (x==c_name)
    			{
    				return unescape(y);
    			}// fin if
		}// fin for
  	}// fin getCookie
//funcion para obtener cookie en javascript
//var URL=src();

	
	
function procesar01(op){				
		if($("#fechaI").val()==''||$("#fechaF").val()=='')
		{			
			$("p.Rojo").show();
			return false;
		}
     	
     	$("p.Rojo").hide();
		
		var v1=$("#fechaI").val();
    	var v2=$("#fechaF").val();
    	var v3=$('#txtusuarioSesion').val();
		
		var v0=$("#txtusuario").val().split('-')[0];					
		var v4=$("#txtusuario").val().split('-')[1];
		
		if(op==1)
			url="http://"+getCookie("URL_Reportes")+"/aportes/digitalizacion/reporte001.jsp?v0="+v0+'&v1='+v1+'&v2='+v2+'&v3='+v3+'&v4='+v4;
		else if(op==2)
			url="http://"+getCookie("URL_Reportes")+"/aportes/digitalizacion/rptExcel001.jsp?v0="+v0+'&v1='+v1+'&v2='+v2+'&v3='+v3+'&v4='+v4;
		
		window.open(url,"_NEW");

}

function procesar02(op){
	
	var usuario=$("#txtusuarioSesion").val();
	var fechaIni=$("#txtFechaI").val();
	var fechaFin=$("#txtFechaF").val();
	var agencia=$("#cmbAgencia").val();
	var usuario2=$("#txtUsuario2").val();
	var tipo=$("#txtipo").val();
	
	if(tipo==0){
		alert("Ingrese el tipo consulta");
		return false;
	}
	
	if((fechaIni) > (fechaFin)){
		alert("Fecha inicial mayor a la final");
		return false;
	}
	
	if(tipo==1){
		
	if(op==1)
		url="http://"+getCookie("URL_Reportes")+"/aportes/digitalizacion/reporte02.jsp?v0="+fechaIni+"&v1="+fechaFin+"&v2="+usuario+"&v3="+usuario2+"&v4="+agencia;
	else if(op==2)
		url="http://"+getCookie("URL_Reportes")+"/aportes/digitalizacion/rptExcel02.jsp?v0="+fechaIni+"&v1="+fechaFin+"&v2="+usuario+"&v3="+usuario2+"&v4="+agencia;
	
	window.open(url,"_NEW");
	}else{
		
		if(op==1)
			url="http://"+getCookie("URL_Reportes")+"/aportes/digitalizacion/reporte02_1.jsp?v0="+fechaIni+"&v1="+fechaFin+"&v2="+usuario+"&v3="+usuario2+"&v4="+agencia;
		else if(op==2)
			url="http://"+getCookie("URL_Reportes")+"/aportes/digitalizacion/rptExcel02_1.jsp?v0="+fechaIni+"&v1="+fechaFin+"&v2="+usuario+"&v3="+usuario2+"&v4="+agencia;
		
		window.open(url,"_NEW");
		
	}
}

function procesar03(op){				
	if($("#fechaI").val()==''||$("#fechaF").val()=='')
	{			
		$("p.Rojo").show();
		return false;
	}
 	
 	$("p.Rojo").hide();
	
	var v1=$("#fechaI").val();
	var v2=$("#fechaF").val();
	var v3=$('#txtusuarioSesion').val();
	
	var v0=$("#txtusuario").val().split('-')[0];					
	var v4=$("#txtusuario").val().split('-')[1];
	var v5=$("#txtusuario").val().split('-')[2];
	//FechaSistema
	var meses = new Array ("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
	var diasSemana = new Array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sabado");
	var f=new Date();	
	var v6 = diasSemana[f.getDay()] + ", " + f.getDate() + " de " + meses[f.getMonth()] + " de " + f.getFullYear()
	
	if(op==1)
		url="http://"+getCookie("URL_Reportes")+"/aportes/digitalizacion/reporte003D.jsp?v0="+v0+'&v1='+v1+'&v2='+v2+'&v3='+v3+'&v4='+v4+'&v5='+v5+'&v6='+v6;
	else if(op==2)
		url="http://"+getCookie("URL_Reportes")+"/aportes/digitalizacion/rptExcel003D.jsp?v0="+v0+'&v1='+v1+'&v2='+v2+'&v3='+v3+'&v4='+v4+'&v5='+v5+'&v6='+v6;
	
	window.open(url,"_NEW");

}