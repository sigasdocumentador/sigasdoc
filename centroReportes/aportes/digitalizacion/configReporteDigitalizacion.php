<?php

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
include_once $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'config.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

$sql="Select idusuario, nombres, usuario from aportes519 order by nombres";
$funcionario=$db->querySimple($sql);

$usuario=$_SESSION['USUARIO'];
$tipo=$_REQUEST['tipo'];
$titulo='...';
$rept=$_REQUEST['tit'];

switch($rept){
	case 1:$titulo='Listado de digitalizaciones por usuario'; break;
	case 2:$titulo='Digitalizacion por Agencia'; break;
	case 3:$titulo='Auditado por Usuario'; break;
	case 4:$titulo=''; break;

	}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Men&uacute; Reportes Digitalizaciones</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link type="text/css" rel="stylesheet" href="../../../css/css.1.8/jquery-ui-1.8.17.custom.css" />
<link type="text/css" href="../../../css/Estilos.css" rel="stylesheet"/>
<link href="../../../css/marco.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="../../../js/1.6/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="../../../js/1.6/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="../../../js/comunes.js"></script>
<script type="text/javascript" src="js/reportes.js"></script>
<script>
$(function(){
	$("#fechaR,#periodos").datepicker({maxDate:"+0D",
		changeMonth: true,
		changeYear: true
	});

	$('#fechaI, #txtFechaI').datepicker({
		maxDate:"+0D",
		changeMonth: true,
		changeYear: true,
		onSelect: function(dateText, inst) {
			var lockDate = new Date($('#fechaI').datepicker('getDate'));
			$('input#fechaF').datepicker('option', 'minDate', lockDate);
		}
	});
	$("#fechaF, #txtFechaF").datepicker({
		maxDate:"+0D",
		changeMonth: true,
		changeYear: true
	});
	
	$("#periodoI").datepicker({ dateFormat: 'yy/mm/dd',
		changeMonth: true,
		changeYear: true
	});

	$( "#periodoI" ).bind("change",function(){
		$("#periodoF").val('');
		$("#periodoF").datepicker({ dateFormat: 'yy/mm/dd',
			minDate: new Date($("#periodoI").val()),
			changeMonth: true,
			changeYear: true
		});
		$("#periodoF").datepicker("option","minDate",new Date($("#periodoI").val()));
	});

	$("#nombre").bind("keyup",function(){
		$(this).val($(this).val().toUpperCase());
	});
	
});
</script>
</head>

<body>
	<center>
	<table width="100%" border="0">
	<tr>
	<td align="center" ><img src="../../../imagenes/logo_reporte.png" width="362" height="70"></td>
	</tr>
	<tr>
	<td align="center" ><?php echo $titulo ?></td>
	</tr>
	</table>
	<br>
	<table width="60%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
	<tr>
    <th colspan="4"><strong>Parametros de Configuracion Reporte</strong></th>
    </tr>
    <tr>
      <td colspan="4">&nbsp;</td>
    </tr>
<!-- RANGO DE FECHAS   -->
<?php if($tipo==1){
	?>
<div id="div-2" style="display:none" >
  <tr>
    <td width="21%">Fecha Inicial :</td>
    <td width="19%"><input name="fechaI" type="text" class="box" id="fechaI" readonly="readonly" /></td>
    <td width="25%" >Fecha Final :</td>
    <td width="42%" ><input name="fechaF" type="text" class="box" id="fechaF" readonly="readonly" /></td>
  </tr>
</div>
<?php } ?>

<!-- POR USUARIO, sin todos, con nombre  -->
<?php if( $tipo==1){ ?>
<div id="div-6" style="display:none">
	<tr>
		<td>  usuario: </td>
			<td>
			<select name="txtusuario" class="boxmediano" id="txtusuario" >				
				<?php 
				while ($row=$funcionario->fetch()){
				echo "<option value='".$row['idusuario']."-".$row['nombres']."'>".$row['nombres']."</option>";
				}
				?>
			</select>
		</td>
		<td>
		</td>
		<td>
		</td>
	</tr>
  <tr>
	  <td colspan="4" style="text-align:center">
	  <label style="cursor:pointer" onClick="procesar01(1);"><img src="../../../imagenes/icono_pdf.png" width="32" height="32"></label>
      <label style="cursor:pointer" onClick="procesar01(2);"><img src="../../../imagenes/icono_excel.png" width="32" height="32"></label>
      </td>
	  </tr>
</div>
<?php } 
if($tipo==2){
	
	$sql="SELECT usuario from aportes519 where estado='A' order by usuario asc";
	$rs=$db->querySimple($sql);
	
	$sql1="Select * from aportes500";
	$agencia=$db->querySimple($sql1);
	
	?>
	<tr>
		<td>
			Fecha Inicial:
		</td>
		<td>
			<input type="text" id="txtFechaI" name="txtFechaI" readonly>
		</td>
		<td>
			Fecha Final:
		</td>
		<td>
			<input type="text" id="txtFechaF" name="txtFechaF" readonly>
		</td>  
	</tr>
	<tr>
	<td width="25%">Agencia</td>
      <td width="25%">
      	<select name="cmbAgencia" class="box1" id="cmbAgencia">
					<option value=0>TODOS</option>
				<?php 
					while ($row1=$agencia->fetch()){
						echo "<option value=".$row1['idagencia'].">".$row1['agencia']."</option>";
					}
				?>
		</select>
      </td>
	  <td colspan="4">Usuario: 
      <select id="txtUsuario2" name="txtUsuario2" class="box1">
      <option value="T">TODOS</option>
	<?php
	  while($row=$rs->fetch()){
		  echo "<option value=".$row['usuario'].">".$row['usuario']."</option>";
	  }
	?>
      </select>
      </td>
	  </tr>
		  <tr>
	      <td colspan="4" style="text-align:center">Tipo de consulta
	      <select id="txtipo" name="txtipo" class="box1">
	      	  <option value="0">:: Seleccione ::</option>
	          <option value="1">Afiliado</option>
	          <option value="2">Empresa</option>
	      </select>
	      </td>
	    </tr>
	<tr>
	  <td colspan="4" style="text-align:center">
	  <label style="cursor:pointer" onClick="procesar02(1);"><img src="../../../imagenes/icono_pdf.png" width="32" height="32"></label>
      <label style="cursor:pointer" onClick="procesar02(2);"><img src="../../../imagenes/icono_excel.png" width="32" height="32"></label>
      </td>
	  </tr>
<?php 
}
if($tipo==3){
	?>
<div id="div-2" style="display:none" >
  <tr>
    <td width="21%">Fecha Inicial :</td>
    <td width="19%"><input name="fechaI" type="text" class="box" id="fechaI" readonly="readonly" /></td>
    <td width="25%" >Fecha Final :</td>
    <td width="42%" ><input name="fechaF" type="text" class="box" id="fechaF" readonly="readonly" /></td>
  </tr>
  
  <tr>
		<td>  usuario: </td>
			<td>
			<select name="txtusuario" class="boxmediano" id="txtusuario" >				
				<?php 
				while ($row=$funcionario->fetch()){
				echo "<option value='".$row['idusuario']."-".$row['nombres']."- ".$row['usuario']."'>".$row['nombres']."</option>";
				}
				?>
			</select>
		</td>
		<td>
		</td>
		<td>
		</td>
	</tr>
  <tr>
	  <td colspan="4" style="text-align:center">
	  <label style="cursor:pointer" onClick="procesar03(1);"><img src="../../../imagenes/icono_pdf.png" width="32" height="32"></label>
      <label style="cursor:pointer" onClick="procesar03(2);"><img src="../../../imagenes/icono_excel.png" width="32" height="32"></label>
      </td>
	  </tr>
  
  
</div>
<?php } ?>

<input type="hidden" id="txtusuarioSesion" name="txtusuarioSesion" value="<?php echo $usuario; ?>" /> 
<input type="hidden" id="txtRept" name="txtRept" value="<?php echo $rept; ?>" /> 
 </table>
<p class="Rojo" align="center" style="display:none">Ingrese la fecha del reporte.</p>
<br><br>
<p align="center"><a href="menuRptDigitalizacion.php" ><img  alt="Volver al Listado" style="border:none"/></a></p>

</body>

</html>