<?php
/* autor:       Oswaldo Gonzalez
 * fecha:       05/08/2012
 * objetivo:    Verifica si una persona est� afiliada como trabajador o beneficiario
 */
ini_set("display_errors","1");
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$tipoDoc=$_REQUEST['tipoDoc'];//cedula 
$numDoc=$_REQUEST['numeroDoc'];//buscar por

include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'p.persona.class.php';
$objPersona = new Persona;
$consulta = $objPersona->mostrar_registro_para($numDoc,1,$tipoDoc);
$resultadoPeticion = array();
$conr=0;
$data = array();
while($row=mssql_fetch_array($consulta)){
	$data[]=array_map("utf8_encode",$row);
	$conr++;
}
mssql_free_result($consulta);
if($conr>0){
	// Existe como afiliado activo
	$resultadoPeticion["persona"] = $data;
	$resultadoPeticion["estado"] = "activo";
	$resultadoPeticion["tipo"] = "Afiliado";
	echo json_encode($resultadoPeticion);
}else {
	// se busca por afiliado inactivo
	$consulta = $objPersona->mostrar_registro_inactivos($numDoc,1,$tipoDoc);
	$resultadoPeticion["estado"] = "inactivo";
	$conr=0;
	$data = array();
	while($row = mssql_fetch_array($consulta)){
		$data[]=$row;
		$conr++;
	}
	mssql_free_result($consulta);
	if($conr>0){
		$resultadoPeticion["persona"] = $data;
		$resultadoPeticion["tipo"] = "Afiliado";
		echo json_encode($resultadoPeticion);
	}else{
		// buscar tercero, no tiene afiliaciones
		$registro = $objPersona->obtener_persona_tercero_por_identificacion($numDoc);
		$resultadoPeticion["estado"] = "inactivo";
		if($registro !== false){
			$data[] = array_map("utf8_encode",$registro);
			$resultadoPeticion["persona"] = $data;
			$resultadoPeticion["tipo"] = "Tercero";
			echo json_encode($data);
		}else{
			$resultadoPeticion["persona"] = null;
			$resultadoPeticion["estado"] = "inactivo";
			$resultadoPeticion["tipo"] = null;
			echo json_encode($resultadoPeticion);
		}
	}
}
die();
?>