<?php
/* autor:       orlando puentes
 * fecha:       08/09/2010
 * objetivo:    Procesar los movimientos de los Bonos almacenados en la base de datos. 
 */
 $root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

?>
<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Men&uacute;n Reportes Secretaria</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link type="text/css" href="../../../css/Estilos.css" rel="stylesheet"/>
<script language="javascript" src="../../../js/comunes.js"></script>
</head>
<body>
<center>
  <table width="100%" border="0">
  <tr>
    <td align="center" ><img src="../../imagenes/razonSocialRpt.png" width="518" height="91" /></td>
  </tr>
  <tr>
    <td align="center" >&nbsp;</td>
  </tr>
  <tr>
    <td class="fecha" ></td>
  </tr>
</table>

<table width="60%" border="0" cellspacing="1" class="tablero">
<tr><th>Reporte</th><th>Descripción</th></tr>  
      <tr>
      <td scope="row" align="left" width="20%">Reporte001</td>
      <td align="left">
          <label style="cursor:pointer" onClick="configurar(1)">Certificado Trabajador Activo</label></td>
    </tr>
    <tr>
      <td >Reporte002</td>
      <td><label style="cursor:pointer" onClick="configurar(2)">Certificado de empresas</label></td>
    </tr>
	<tr>
      <td >Reporte003</td>
      <td><label style="cursor:pointer" onClick="configurar(3)">Certificado Trabajador Inactivo</label></td>
    </tr>
    <tr>
      <td >Reporte004</td>
      <td><label style="cursor:pointer" onClick="configurar(4)">Certificado Trabajador no Afiliado</label></td>
    </tr>
    <tr>
      <td >Reporte005</td>
      <td><label style="cursor:pointer" onClick="configurar(5)">Certificado Contratista</label></td>
    </tr>
    <tr>
      <td >Reporte006</td>
      <td>&nbsp;</td>
    </tr>
  </table>
  </center>
  </body>
<script language="javascript">
var URL=src();
function configurar(nu){
	switch (nu){
		//case 1 : url=URL+"centroReportes/configurarReporte.php?tipo=20&archivo=aportes/secretaria/reporte001.php";
		case 1 : url=URL+"centroReportes/configurarReporte.php?tipo=9&v0=1&modulo=secretaria&archivo=aportes/secretaria/certificadoTrabActivo.php";
			break;
        //case 2 : url=URL+"centroReportes/configurarReporte.php?tipo=21&archivo=aportes/secretaria/reporte002.php";
		case 2 : url=URL+"centroReportes/configurarReporte.php?tipo=10&v0=2&modulo=secretaria&archivo=aportes/secretaria/certificadoEmpresa.php";
			break;
		case 3 : url=URL+"centroReportes/configurarReporte.php?tipo=13&v0=3&modulo=secretaria";
			break;
		case 4 : url=URL+"centroReportes/configurarReporte.php?tipo=12&v0=4&modulo=secretaria";
			break;
		case 5 : url=URL+"centroReportes/configurarReporte.php?tipo=14&v0=5&modulo=secretaria";
			break;
	}
	window.open(url,"_self");
}
</script>  
</html>
