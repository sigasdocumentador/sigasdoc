<?php

/* autor:       orlando puentes
 * fecha:       28/09/2010
 * objetivo:    
*/

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
include_once $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'config.php';
$fechaHoy=date("Y/m/d");
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
global $comprobantesAportes;
$sql="Select * from aportes516";
$rol=$db->querySimple($sql);
$sql="Select * from aportes500";
$agencia=$db->querySimple($sql);
$sql="Select idusuario, nombres from aportes519 order by nombres";
$funcionario=$db->querySimple($sql);
$sql="Select * from aportes091 where iddefinicion = 6";
$tiporadic=$db->querySimple($sql);

$usuario=$_SESSION['USUARIO'];
$url=$_SERVER['PHP_SELF'];
include_once $raiz.DIRECTORY_SEPARATOR.'phpComunes'.DIRECTORY_SEPARATOR.'auditoria.php';
auditar($url);
include_once($raiz. DIRECTORY_SEPARATOR . 'config.php');
include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'p.definiciones.class.php';

$objDefiniciones = new Definiciones();
$opcion1=isset($_REQUEST['v0'])?$_REQUEST['v0']:"";
$tipo1=intval($_REQUEST['tipo']);
$modulo1= isset($_REQUEST['modulo'])?$_REQUEST['modulo']:"";
$menu1= (isset($_REQUEST['menurpte']))?$_REQUEST['menurpte']:"";
$plano1= (isset($_REQUEST['plano']))?$_REQUEST['plano']:"0";
$titulo="";
$opcion2=intval($opcion1);
$verBotonGeneraReporte = true;


/*
echo $menu1;
echo $opcion1;
echo $modulo1;
echo $tipo1;
echo $opcion2;
echo $plano1;
*/
if ($modulo1 == 'aportes/radicacion') {
   switch($opcion2){
	   case 1  : $titulo="Listado de Radicacion"; break;
	   case 2  : $titulo=" Listado Consolidado Radicaciones"; break;
	   case 4  : $titulo="Reporte Habeas Data"; break;
       case 7  : $titulo="Traslados de saldos de REDEBAN a ASOPAGOS por Fecha"; break; 
	   case 12 : $titulo="Traslados de saldos de REDEBAN a ASOPAGOS por Fecha"; break;
	   case 13 : $titulo="Listado de Tarjetas Asignadas por Fecha"; break;
	}
}
  
if ($modulo1 == 'asopagos') {
   switch($opcion2){
	   case 5  : $titulo="Listado de Transacciones por Negocio por Fecha"; break;
	   case 6  : $titulo=" Listado Detallado de Transacciones por Fecha"; break;
       case 7  : $titulo="Traslados de saldos de REDEBAN a ASOPAGOS por Fecha"; break;
	   case 12 : $titulo="Traslados de saldos de REDEBAN a ASOPAGOS por Fecha"; break;
	   case 13 : $titulo="Listado de Tarjetas Asignadas por Fecha"; break;
	}
}

if ($modulo1 == 'seguridad') {
   switch($opcion2){
   	   case 13  : $titulo="Listado de Usuarios Activos por Agencia"; break;
       case 14  : $titulo="Listado de Usuarios Activos por Agencia"; break;
	   break;
	}
}

if($modulo1 == 'secretaria'){
	switch($opcion2){
		case 1: $titulo = "Certificado de afiliaci&oacute;n"; break;
		case 2: $titulo = "Certificado de Empresa";	break;
		case 3: $titulo = "Certificado de Afiliado inactivo"; break;
		case 4: $titulo = "Certificado de Trabajador no afiliado"; break;
		case 5: $titulo = "Certificado de Contratista";	break;
	}
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Men&uacute; Reportes Asopagos</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link type="text/css" rel="stylesheet" href="../css/css.1.8/jquery-ui-1.8.17.custom.css" />
<link type="text/css" href="../css/Estilos.css" rel="stylesheet"/>
<link href="../css/marco.css" rel="stylesheet" type="text/css" />
<link type="text/css" href="../css/jquery.dataTables.css" rel="stylesheet"/>
<link type="text/css" href="../css/ventanamodal.css" rel="stylesheet"/>


<!-- jquery-1.12.0.min.js se actualizo para que permita trabajar con jquery.dateTables.js -->
<?php 
if($tipo1 ==26){ // se utilizar para manejar el jquery por problemas de compatibilidad con versiones anteriores, el datapiker
	echo "<script type='text/javascript' language='javascript' src='../js/jquery-1.12.0.min.js'></script>";
}
else{
   echo "<script type='text/javascript' src='../js/1.6/jquery-1.6.2.min.js'></script>";
}
?>
<script type="text/javascript" src="../js/1.6/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="../js/comunes.js"></script>
<script type="text/javascript" src="../js/jquery.dataTables.js"></script>
<script lenguaje type="text/javascript" src="../js/ventanamodal.js"></script>
<script src="../js/jquery.table2excel.min.js"></script>





<script>
$(function(){
    // genera reporte excel
	$("#btexcel").click(function(){
		var f = new Date();
        $("#tablatempo").table2excel({
           exclude: ".excludeThisClass",
           name: "Worksheet Name",
           fileext:'.xls',
           filename: "reporte"+f.getFullYear()+f.getMonth()+f.getDate()+f.getHours()+":"+f.getMinutes()+":"+f.getSeconds() //do not include extension
        });
    });
	// funcion agregada el dia 18 de marzo de 2016
	$("#btabonopenprocesar").click(function(){
        //opci�n para traer datos de los abonados sin procesar reporte No. 016
		var URL=src();
        
        //cargar gif que muestre que se esta procesando
		//A�adimos la imagen de carga en el contenedor
        $('#content_carga').html('<img src="../imagenes/ajax-loader.gif"/>');

		//var auxprueba='1';
		var tipo=$("#cmbtipo").val();
		var tipoiden=$("#cmbIdTipoDocumento").val();
		var identificacion=$("#txtIdentificacion").val();
		var tipoinforme=$("#cmbtipoinforme").val();
		var arrayaux= new Array();
		var campo="";
		$("#conten_table").html('<table id="relabonospenprocesar" class="display" width="90%"><table id="tablatempo"  style="display:none" width="90%"></table>');
		var tablatempo="<thead><tr><th>Nombre</th><th>Identificacion</th><th>Periodo</th><th>Bono</th><th>Tipo</th><th>Valor</th><th>Direcci&oacute;n</th><th>Telefono</th></tr></thead><tbody>";
		$.ajax({
			type:"POST",
			url: URL+"phpComunes/abonospenprocesar.php",
			data:{tipo:tipo,tipoiden:tipoiden,identificacion:identificacion,tipoinforme:tipoinforme,solajax:1},
			async:false,
			dataType:"json",
			success:function(datos){
				$('#content_carga').html('');
				if(datos!=0){
					$.each(datos,function(index,value){
						tablatempo += '<tr><td>'+value.nombre+'</td><td>'+value.identificacion+'</td><td>'+value.periodogiro+'</td><td>'+value.bono+'</td><td>'+value.tipopago+'</td><td>'+value.valor+'</td><td>'+value.direccion+'</td><td>'+value.telefono+'</td>/tr>';
						var add='<input type="button" href="otros/addseguimiento.php?tipo=1&identificacion='+value.identificacion+'&nombre='+value.nombre+'&idtrabajador='+value.idtrabajador+'&tipoevento=1" value="Adicionar" class="clsVentanaIFrame clsBoton" rel="::SIGAS::  Adicionar seguimiento">';
						var consu = value.num_seguimiento>0 ? '<input type="button" href="otros/addseguimiento.php?tipo=2&identificacion='+value.identificacion+'&nombre='+value.nombre+'&idtrabajador='+value.idtrabajador+'&tipoevento=2" value="Consultar" class="clsVentanaIFrame clsBoton" rel="::SIGAS:: Seguimientos Registrados">' : '';
 						arrayaux[index]=[value.nombre,value.identificacion,value.periodogiro,value.bono,value.tipopago,value.valor,value.direccion,value.telefono,add,consu];
					});
					tablatempo += "</tbody>";
					$("#tablatempo").append(tablatempo);
					var tiperiodo = tipoinforme==1 ? "Periodo" : "Acomulados";
					$('#relabonospenprocesar').DataTable( {
					        data: arrayaux ,
					        columns: [
					            { title: "Nombre" },
					            { title: "Identificaci\u00f3n" },
					            { title: tiperiodo },
					            { title: "Bono" },
					            { title: "Tipo" },
					            { title: "Valor" },
					            { title: "Direccion" },
					            { title: "Telefono" },
					            { title: "Add seguimiento" },
					            { title: "Consutar seguimiento" }
					        ]
					    } );
				}
				else{
				  alert("se retornar�n 0 registros");
				}
			}
		});
		
   });

   //rueltados enviados a la tablas
	
	   
	
   //fin envio resultados tabla	   
	   
	   
	$("#fechaR,#periodos").datepicker({maxDate:"+0D",
		changeMonth: true,
		changeYear: true
	});

	$('#fechaI').datepicker({
		maxDate:"+0D",
		changeMonth: true,
		changeYear: true,
		onSelect: function(dateText, inst) {
			var lockDate = new Date($('#fechaI').datepicker('getDate'));
			$('input#fechaF').datepicker('option', 'minDate', lockDate);
		}
	});
	$("#fechaF").datepicker({
		maxDate:"+0D",
		changeMonth: true,
		changeYear: true
	});

	$("#txtFechaIni").datepicker({
		maxDate:"+0D",
		changeMonth: true,
		changeYear: true,
		onSelect:function(date){
		$('#txtFechaFin').val(date);
		$('#txtFechaFin').datepicker("option","minDate",new Date(date));
		}
	});
	$("#txtFechaFin").datepicker({
		maxDate:"+0D",
		changeMonth: true,
		changeYear: true,
		onSelect:function(date){
		}
	});
	
	$("#periodoI").datepicker({ dateFormat: 'yy/mm/dd',
		changeMonth: true,
		changeYear: true
	});

	$("#periodoI").bind("change",function(){
		$("#periodoF").val('');
		$("#periodoF").datepicker({ dateFormat: 'yy/mm/dd',
			minDate: new Date($("#periodoI").val()),
			changeMonth: true,
			changeYear: true
		});
		$("#periodoF").datepicker("option","minDate",new Date($("#periodoI").val()));
	});

	$("#nombre").bind("keyup",function(){
		$(this).val($(this).val().toUpperCase());
	});

	$("#txtPeriodoI,#txtPeriodoF").datepicker({
		dateFormat: 'yymm',
		changeMonth: true,
		changeYear: true,
		showButtonPanel: true,
		onClose: function(dateText, inst) {
			var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
			var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
			$(this).datepicker('setDate', new Date(year, month, 1));
		}
	});
	
	$("#txtPeriodoI,#txtPeriodoF").focus(function () {
		$(".ui-datepicker-calendar").hide();
		$("#ui-datepicker-div").position({
			my: "center top",
			at: "center bottom",
			of: $(this)
		});
	});
			
	
});
</script>
</head>
<body>
	<div id="dlgLoading"></div>
	<center>
	<table width="100%" border="0">
	<tr>
	<td align="center" ><img src="../imagenes/logo_reporte.png"></td>
	</tr>
	<tr>
	<td align="center" ><?php echo $titulo; ?></td>
	</tr>
	</table>
	<br>
	<table width="60%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
	<tr>
    <th colspan="4"><strong>Parametros de Configuracion Reporte</strong></th>
    </tr>
    <tr>
      <td colspan="4">&nbsp;</td>
    </tr>

<!-- POR UNA FECHA   -->
<?php if( $tipo1 ==1) { ?>
<div id="div-1" style="display:none">
  <tr>
    <td width="32%">Digite la fecha del reporte    </td>
    <td width="25%"><input name="fechaR" type="text" class="box" id="fechaR" readonly /></td>
    <td width="43%" ><input type="button" onclick="buscarReporte();" style="cursor:pointer" class="ui-state-default"  value="Ver reporte"/></td>
  </tr>
  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
</div>
<?php } ?>

<!-- INFORME PRESCRIPCIONES -->
<?php if($tipo1 ==25){ ?>

	<tr>
		<td style="text-align:center;" width="50%" >
			Fecha Corte: 
			<input type="text" id="fechaF" name="fechaF" readonly>
		</td>		
  	</tr>
  	<tr>
  		<td colspan="2" id="tdMensajeError" class="rojoGrande" align="center">  			  			
  			
  		</td>
  	</tr>
	<tr>
	  	<td style="text-align:center" colspan="2" id="idTdBottonDescarga">
      		<label style="cursor:pointer" onClick="procesar1(1);" id="idProcesar025"><img src="../imagenes/icono_excel.png" width="32" height="32"/></label>
  		</td>
	  </tr>
<?php $verBotonGeneraReporte = false; } ?>


<!-- INFORME SALDO TARJETAS PENDIENTE DE RECLAMAR SE ADICIONAN CAMPOR COMO TIPO DE IDENTIFICACION Y TIPO DE INFOROME--> 
<?php if($tipo1 ==26){ 
?>
    <script lenguaje type="text/javascript" src="../js/ventanamodal.js"></script>

	<tr>
		<td style="text-align:center;border-top-style:hidden;border-bottom-style:hidden;" width="50%" >
		  <table align="center" boder="5">
		      <tr>
		          <td style="border-style: hidden;">Tipo: </td>
		          <td style="border-style: hidden;">
		             <select name="cmbtipo" class="boxmediano" id="cmbtipo" style="width:100%">				
			           <option value="0">Todos</option>
			           <option value="1">T - Subsidio</option>
			           <option value="2">F - Fonede</option>
		             </select>
		          </td>
		      </tr>
		      <tr>
		          <td style="border-style: hidden;">Tipo Documento </td>
		          <td style="border-style: hidden;">
		              <select name="cmbIdTipoDocumento" id="cmbIdTipoDocumento" class="box1" style="width: 250px" onchange="$('#txtIdentificacion').val('').trigger('blur');" >
			            <option value="0" selected="selected">Seleccione...</option>
			            <?php
				        $rs = $db->Definiciones ( 1, 1 );
				        while ( $row = $rs->fetch() ) {
					           echo "<option value=" . $row ['iddetalledef'] . ">" . $row ['detalledefinicion'] . "</option>";
				        } 
			            ?>
                      </select>
		          </td>
		      <tr>
		      <tr>
		          <td style="border-style: hidden;"> Identificaci&oacute;n</td>
		           <td style="border-style: hidden;">
		              <input name="txtIdentificacion" type="text" class="box1" id="txtIdentificacion"  maxlength="17">											
				  </td>
		      </tr>
		      <tr>
		              <td style="border-style: hidden;">Tipo reporte: </td>
		              <td style="border-style: hidden;">
		                 <select name="cmbtipoinforme" class="boxmediano" id="cmbtipoinforme" style="width:100%">				
			               <option value="1">Detallado</option>
			               <option value="2">Consolidado</option>
		                 </select>
		              </td>
		       </tr>
		       <tr>
		           <td colspan="4"  style="border-style: hidden;">&nbsp;</td>
		       </tr>
		    
		       <tr>
		          <td colspan="4"  style="border-style: hidden;"><center><input type="button" id="btabonopenprocesar"  value="Generar reporte" name="btabonadopenprocesar"/></center></td></td>
		       </tr> 
		     
		  </table>
		</td>		
  	</tr>
  	<tr>
  		<td colspan="2" id="tdMensajeError" class="rojoGrande" align="center">  			  			
  			
  		</td>
  	</tr>
	
<?php $verBotonGeneraReporte = false; } ?>


<!-- INFORME SALDO EN TARJETAS -->
<?php if($tipo1 ==27){ ?>

	<tr>
		<td style="text-align:center;" width="50%" >
			Tipo: 
			<select name="cmbtipo" class="boxmediano" id="cmbtipo" >				
				<option value="T">Todos</option>
				<option value="S">S - Subsidio</option>
				<option value="F">F - Fonede</option>
			</select>
		</td>		
  	</tr>
  	<tr>
  		<td colspan="2" id="tdMensajeError" class="rojoGrande" align="center">  			  			
  			
  		</td>
  	</tr>
	<tr>
	  	<td style="text-align:center" colspan="2" id="idTdBottonDescarga">
      		<label style="cursor:pointer" onClick="procesar3(1);" id="idProcesar025"><img src="../imagenes/icono_excel.png" width="32" height="32"/></label>
      		<label style="cursor:pointer" onClick="procesar3(2);" id="idProcesar025"><img src="../imagenes/icono_pdf.png" width="32" height="32"/></label>
  		</td>
	  </tr>
<?php $verBotonGeneraReporte = false; } ?>

<!-- INFORME TARJETAS ASIGNADAS GENERAL -->
<?php if($tipo1 ==29){ ?>
	
	<tr>
	<td width="50%" align="right">Fecha Inicial :
    <input name="fechaI" type="text" class="box" id="fechaI" readonly /></td>
    <td width="50%">Fecha Final :
    <input name="fechaF" type="text" class="box" id="fechaF" readonly /></td>
	
  	</tr>
  	<tr>
  		<td colspan="2" id="tdMensajeError" class="rojoGrande" align="center"></td>
  	</tr>
	<tr>
	  	<td style="text-align:center" colspan="2" id="idTdBottonDescarga">
      		<label style="cursor:pointer" onClick="procesar5(1);" id="idProcesar025"><img src="../imagenes/icono_excel.png" width="32" height="32"/></label>
      		<label style="cursor:pointer" onClick="procesar5(2);" id="idProcesar025"><img src="../imagenes/icono_pdf.png" width="32" height="32"/></label>
  		</td>
	  </tr>
<?php $verBotonGeneraReporte = false; } ?>


<!-- INFORME TARJETAS ASIGNADAS DETALLADO -->
<?php if($tipo1 ==13){ ?>
    <tr>
    <td>
	<table >
	<tr>
    <td style="text-align:center;" width="50%" >
			Habeas Data: 
			<select name="cmbhd" class="boxmediano" id="cmbhd" >				
				<option value="1" selected>Si</option>
			</select>
	</td>		
  	</tr>
  	<tr>
    <td style="text-align:center;" width="50%" >
			Tipo: 
			<select name="cmbtipo" class="boxmediano" id="cmbtipo" >				
				<option value="0">[Todos]</option>
				<option value="1">Afiliados</option>
				<option value="2">Desafiliados</option>
			</select>
	</td>		
  	</tr>
  	<tr>
  	    <td style="text-align:center;">Agencia:
			<select name="txtagencia" class="boxmediano" id="txtagencia">
				<option value="0">[Todos]</option>
				<?php 
				while ($row=$agencia->fetch()){
				echo "<option value=".$row['idagencia'].">".$row['agencia']."</option>";
				}
				?>
			</select>
		</td>
  	</tr>
	<tr>
	<td style="text-align:center;">Fecha Inicial :
    <input name="txtFechaIni" type="text" class="box" id="txtFechaIni" readonly />
    Fecha Final :
    <input name="txtFechaFin" type="text" class="box" id="txtFechaFin" readonly /></td>
    </tr>
    <tr>
  		<td colspan="2" id="tdMensajeError" class="rojoGrande" align="center">  			  			
  			
  		</td>
  	</tr>
	<tr>
	  	<td style="text-align:center" colspan="2" id="idTdBottonDescarga">
      		<label style="cursor:pointer" onClick="procesar13();" id="idProcesar025"><img src="../imagenes/icono_excel.png" width="32" height="32"/></label>
  		</td>
	  </tr>
	</table>  
	</td>
	</tr>
<?php $verBotonGeneraReporte = false; } ?>



<!-- INFORME TARJETAS ASIGNADAS DETALLADO -->
<?php if($tipo1 ==28){ ?>
	
	<tr>
	<td width="50%">Fecha Inicial :
    <input name="fechaI" type="text" class="box" id="fechaI" readonly />
    Fecha Final :
    <input name="fechaF" type="text" class="box" id="fechaF" readonly /></td>
		<td style="text-align:center;" width="50%" >
			Tipo: 
			<select name="cmbtipo" class="boxmediano" id="cmbtipo" >				
				<option value="0">Todos</option>
				<option value="S">T - Subsidio</option>
				<option value="F">F - Fonede</option>
			</select>
		</td>		
  	</tr>
  	<tr>
  		<td colspan="2" id="tdMensajeError" class="rojoGrande" align="center">  			  			
  			
  		</td>
  	</tr>
	<tr>
	  	<td style="text-align:center" colspan="2" id="idTdBottonDescarga">
      		<label style="cursor:pointer" onClick="procesar4(1);" id="idProcesar025"><img src="../imagenes/icono_excel.png" width="32" height="32"/></label>
      		<label style="cursor:pointer" onClick="procesar4(2);" id="idProcesar025"><img src="../imagenes/icono_pdf.png" width="32" height="32"/></label>
  		</td>
	  </tr>
<?php $verBotonGeneraReporte = false; } ?>

<!-- INFORME INVENTARIO TARJETAS ENTREGADAS POR AGENCIA -->
<?php if($tipo1 ==40){ ?>
	
	<tr>
	<td width="50%">Fecha Inicial :
    <input name="fechaI" type="text" class="box" id="fechaI" readonly />
    Fecha Final :
    <input name="fechaF" type="text" class="box" id="fechaF" readonly /></td>
		<td style="text-align:center;" width="50%" >
			Agencia: 
			<select name="cmbAgencia37" class="boxmediano" id="cmbAgencia37">
					<option value="T">TODOS</option>
				<?php 
					while ($row=$agencia->fetch()){
						echo "<option value=".$row['codigo'].">".$row['agencia']."</option>";
					}
				?>
			</select>
		</td>		
  	</tr>
  	<tr>
  		<td colspan="2" id="tdMensajeError" class="rojoGrande" align="center">  			  			
  			
  		</td>
  	</tr>
	<tr>
	  	<td style="text-align:center" colspan="2" id="idTdBottonDescarga">
      		<label style="cursor:pointer" onClick="procesar6(1);" id="idProcesar025"><img src="../imagenes/icono_excel.png" width="32" height="32"/></label>
      		<label style="cursor:pointer" onClick="procesar6(2);" id="idProcesar025"><img src="../imagenes/icono_pdf.png" width="32" height="32"/></label>
  		</td>
	  </tr>
<?php $verBotonGeneraReporte = false; } ?>

<!-- INFORME TARJETAS ASIGANADAS POR RANGO DE FECHA Y AGENCIA -->
<?php if($tipo1 ==51){ ?>
	
	<tr>
	<td width="50%">Fecha Inicial :
    <input name="fechaI" type="text" class="box" id="fechaI" readonly />
    Fecha Final :
    <input name="fechaF" type="text" class="box" id="fechaF" readonly /></td>
		<td style="text-align:center;" width="50%" >
			Agencia: 
			<select name="cmbAgencia37" class="boxmediano" id="cmbAgencia37">
				<option value="T">TODOS</option>
				<?php 
					while ($row=$agencia->fetch()){
						echo "<option value=".$row['codigo'].">".$row['agencia']."</option>";
					}
				?>
			</select>
		</td>		
  	</tr>
  	<tr>
  		<td colspan="2" id="tdMensajeError" class="rojoGrande" align="center">  			  			
  			
  		</td>
  	</tr>
	<tr>
	  	<td style="text-align:center" colspan="2" id="idTdBottonDescarga">
      		<label style="cursor:pointer" onClick="procesar15(1);" id="idProcesar025"><img src="../imagenes/icono_excel.png" width="32" height="32"/></label>
      		<label style="cursor:pointer" onClick="procesar15(2);" id="idProcesar025"><img src="../imagenes/icono_pdf.png" width="32" height="32"/></label>
  		</td>
	  </tr>
<?php $verBotonGeneraReporte = false; } ?>

<!-- INFORME CONSOLIDADO TARGETAS REEXPEDIDAS-->
<?php if($tipo1 ==42){ ?>
	
	<tr>
	<td width="50%">Fecha Inicial :
    <input name="fechaI" type="text" class="box" id="fechaI" readonly />
    Fecha Final :
    <input name="fechaF" type="text" class="box" id="fechaF" readonly /></td>
		<td style="text-align:center;" width="50%" >
			Agencia: 
			<select name="cmbAgencia37" class="boxmediano" id="cmbAgencia37">
					<option value="T">TODOS</option>
				<?php 
					while ($row=$agencia->fetch()){
						echo "<option value=".$row['codigo'].">".$row['agencia']."</option>";
					}
				?>
			</select>
		</td>		
  	</tr>
  	<tr>
  		<td colspan="2" id="tdMensajeError" class="rojoGrande" align="center">  			  			
  			
  		</td>
  	</tr>
	<tr>
	  	<td style="text-align:center" colspan="2" id="idTdBottonDescarga">
      		<label style="cursor:pointer" onClick="procesar7(1);" id="idProcesar026"><img src="../imagenes/icono_excel.png" width="32" height="32"/></label>
      		<label style="cursor:pointer" onClick="procesar7(2);" id="idProcesar026"><img src="../imagenes/icono_pdf.png" width="32" height="32"/></label>
  		</td>
	  </tr>
<?php $verBotonGeneraReporte = false; } ?>


<!-- INFORME DE REVERSOS DE TRANSLADOS -->
<?php if($tipo1 ==43){ ?>

	<tr>
		<td colspan="2" style="text-align:center;" width="30%" >
			Fecha Reverso:
			<input type="text" id="fechaI" name="fechaI" readonly size="15" class="box1"/>
			&nbsp;&nbsp;
			-
			<input type="text" id="fechaF" name="fechaF" readonly size="15" class="box1"/> 
		</td>
	</tr>
  	<tr>
  		<td colspan="2" id="tdMensajeError" class="rojoGrande" align="center">  			  			
  			
  		</td>
  	</tr>
	<tr>
	  	<td style="text-align:center" colspan="2" id="idTdBottonDescarga">
      		<label style="cursor:pointer" onClick="procesar8(1);" id="idProcesar027"><img src="../imagenes/icono_excel.png" width="32" height="32"/></label>
      		<label style="cursor:pointer" onClick="procesar8(2);" id="idProcesar027"><img src="../imagenes/icono_pdf.png" width="32" height="32"/></label>
  		</td>
	  </tr>
<?php $verBotonGeneraReporte = false; } ?>

<!-- INFORME DE SOLICITUD DE RESETEO CLAVE -->
<?php if($tipo1 ==44){ ;?>
	<tr>
		<td colspan="2" style="text-align:center;" width="30%" >
			Fecha de Solicitud:
			<input type="text" id="fechaI" name="fechaI" readonly size="15" class="box1"/>
			&nbsp;&nbsp;
			-
			<input type="text" id="fechaF" name="fechaF" readonly size="15" class="box1"/> 
		</td>
	</tr>
	<tr>
	 <td colspan="2" style="text-align:center;">
	  			Identificacion:&nbsp;<input type="text" id="documento" size="25" name="documento" class="box1" >
	  			Bono:&nbsp;<input type="text" id="bono" size="25" name="bono" class="box1" >
	  			Estado:&nbsp;
      			<select id="txtEstado" name="txtEstado" class="box1">
      			<option value="">[Todos]</option>
      			<option value="I">Pendientes</option>
      			<option value="A">Asignadas</option>
      		</select>
     		</td>
	</tr>
  	<tr>
  	<tr>
		<td colspan="2" style="text-align:center;">  
			Agencia:
			<select name="txtagencia" class="box1" id="txtagencia">
				<option value="">[Todos]</option>
				<?php 
				while ($row=$agencia->fetch()){
				echo "<option value=".$row['idagencia'].">".$row['agencia']."</option>";
				}
				?>
			</select>
			
			 Usuario: 
			      <select id="txtUsuario2" name="txtUsuario2" class="boxmediano">
			      <option value="">[Todos]</option>
				<?php
					  while($row=$funcionario->fetch()){
						  echo "<option value=".$row['idusuario'].">".$row['nombres']."</option>";
					  }
				?>
      </select>
		</td>
	</tr>
  		<td colspan="2" id="tdMensajeError" class="rojoGrande" align="center">  			  			
  			
  		</td>
  	</tr>
	<tr>
	  	<td style="text-align:center" colspan="2" id="idTdBottonDescarga">
      		<label style="cursor:pointer" onClick="procesar9(1);" id="idProcesar028"><img src="../imagenes/icono_excel.png" width="32" height="32"/></label>
      		<label style="cursor:pointer" onClick="procesar9(2);" id="idProcesar028"><img src="../imagenes/icono_pdf.png" width="32" height="32"/></label>
  		</td>
	  </tr>
<?php $verBotonGeneraReporte = false; } ?>

<!-- INFORME DE PRESCRIPCIONES -->
<?php if($tipo1 ==45){ ;?>
	<tr>
		<td colspan="2" style="text-align:center;" width="30%" >
			Fecha de Prescripcion:
			<input type="text" id="fechaI" name="fechaI" readonly size="15" class="box1"/>
			&nbsp;&nbsp;
			-
			<input type="text" id="fechaF" name="fechaF" readonly size="15" class="box1"/> 
		</td>
	</tr>
  	<tr>
  		<td colspan="2" id="tdMensajeError" class="rojoGrande" align="center"></td>
  	</tr>
	<tr>
	  	<td style="text-align:center" colspan="2" id="idTdBottonDescarga">
      		<label style="cursor:pointer" onClick="procesar10(1);" id="idProcesar029"><img src="../imagenes/icono_excel.png" width="32" height="32"/></label>
      		<label style="cursor:pointer" onClick="procesar10(2);" id="idProcesar029"><img src="../imagenes/icono_pdf.png" width="32" height="32"/></label>
  		</td>
	</tr>
<?php $verBotonGeneraReporte = false; } ?>

<!-- RANGO DE FECHAS   -->
<?php if(( $tipo1 ==2) || ($tipo1 ==7) || ($tipo1 ==11) || ($tipo1 ==31) || ($tipo1 ==32) || ($tipo1 ==34)){ ?>
<div id="div-2" style="display:none" >
  <tr>
    <td width="21%">Fecha Inicial :</td>
    <td width="19%"><input name="fechaI" type="text" class="box" id="fechaI" readonly /></td>
    <td width="25%" >Fecha Final :</td>
    <td width="42%" ><input name="fechaF" type="text" class="box" id="fechaF" readonly /></td>
  </tr>
</div>
<?php } ?>


<!-- PERIODO -->
<?php if( $tipo1 ==3) { ?>
<div id="div-3" style="display:none" >
   <tr>
   <!--   <td width="24%" > Periodos:      
      &nbsp;<img src="../imagenes/menu/calendario.png" alt="Elegir periodo" width="16" height="16" align="absmiddle" id="periodos" />
          <input type="hidden" id="array" />
	  </td>
  -->  <td width="36%">Periodo :  </td>
    <td width="24%" ><input name="periodo" type="text" class="box" id="periodo"  /></td>
    <td width="40%" >
   </tr>
</div>
<?php } ?>

<!-- dos periodos -->
<!-- INFORME DE LAS EMPRESAS MOROSAS -->
	<?php 
		if( $tipo1 ==30) {
	?>
	<div id="div-30" style="display:none;" >
   			<td style="text-align: center;"> 
   				Nit
				<input type="text" name="txtNit" id="txtNit" class="box1" />
    		</td>   
   		</tr>
   			<td style="text-align: center;"> 
   				Rango Periodo Morosidad
   			</td>
   		</tr>
   		<tr>
   			<td style="text-align: center;">
				Inicial <input name="txtPeriodoI" type="text" class="box1" id="txtPeriodoI" readonly='readonly'/>
    			Final<input name="txtPeriodoF" type="text" class="box1" id="txtPeriodoF" readonly='readonly'/>
    		</td>   
   		</tr>
   		<tr><td>&nbsp;</td></tr>
	</div>
	<?php } ?>
	
	<!-- ******** -->
<!-- *************************** -->
	<?php 
		if( $tipo1 ==41) {
	?>
	<div id="div-41" style="display:none;" >
		<tr>
   			<td style="text-align: center;">
   				Por
   				<select name="cmbFiltro41" id="cmbFiltro41" onchange="managerFiltro41();" class="box">
   					<option value="Mes" selected="selected">Analisis Recaudo Por Mes</option>
   					<option value="PagoNuevo">Analisis Recaudo Pagos Nuevos</option>
   					<option value="Periodo41">Analisis Recaudo Por Periodos</option>
   				</select> 
   			</td>
   		</tr>
   		<tr id="trFechas">
   			<td style="text-align: center;"> 
	   			Fecha Inicial
			   	<input name="fechaI" type="text" class="box" id="fechaI" readonly />
			   	Fecha Final
			    <input name="fechaF" type="text" class="box" id="fechaF" readonly />
		  	</td>
   		</tr>
   		<tr id="trPeriodo">
   			<td style="text-align: center;"> 
   			Periodo  
		    <input name="txtPeriodoI" type="text" class="box" id="txtPeriodoI" readonly='readonly'/>
		</tr>
	</div>
	<?php } ?>
	
	<!-- INFORME CONCILIACION APORTES Y NOMINA  -->
	<?php 
		if( $tipo1 ==46) {
	?>
	<div id="div-41" style="display:none;" >
   		<tr id="trFechas">
   			<td style="text-align: center;"> 
	   			Fecha Pago
			   	<input name="fechaI" type="text" class="box" id="fechaI" readonly />
		  	</td>
   		</tr>
 
	</div>
	<?php } ?>
	

<!-- POR USUARIO, sin todos, con nombre  -->
<?php if( $tipo1 ==31){ ?>
<div id="div-6" style="display:none">
	<tr>
	<td>  agencia: </td>
			<td>
			<select name="cmbAgencia" class="boxmediano" id="cmbAgencia">
				<option value="">Seleccione...</option>
				<?php 
				while ($row=$agencia->fetch()){
				echo "<option value=".$row['idagencia'].">".$row['agencia']."</option>";
				}
				?>
			</select>
			</td>
		<td>  usuario: </td>
			<td>
			<select name="cmbUsuario" class="boxmediano" id="cmbUsuario" >				
				<option value="">Debe seleccionar una agencia</option>
			</select>
			</td>
			<td>
			</td>
			<td>
			</td>
	</tr>
</div>
<?php } ?>

<!-- POR AGENCIA - USUARIO o TODOS  -->
<?php if( $tipo1 ==34){ ?>
<div id="div-6" style="display:none">
	<tr>
		<td>Agencia: </td>
		<td>
			<select name="cmbAgencia2" class="boxmediano" id="cmbAgencia2">
				<option value="0">TODOS</option>
			<?php 
				while ($row=$agencia->fetch()){
					echo "<option value=".$row['idagencia'].">".$row['agencia']."</option>";
				}
			?>
			</select>
		</td>
	<?php if( !( $plano1 == 3 || $plano1 == 5) ){ ?>
		<td>Usuario: </td>
		<td>
			<select name="cmbUsuario2" class="boxmediano" id="cmbUsuario2" >				
				<option value="0">TODOS</option>
			</select>
		</td>
	<?php } else { ?>
		<td colspan="2"></td>
	<?php } ?>
		<td></td>
	</tr>
</div>
<?php } ?>


<!-- POR ROL    --->
<?php if( $tipo1 ==4) { ?>
<div id="div-4" style="display:none">
	<tr>
		<td>   Rol: </td>
			<td>
			<select name="txtRol" class="boxmediano" id="txtRol">
				<option value="0">[Todos]</option>
				<?php 
				while ($row=$rol->fetch()){
				echo "<option value=".$row['idrol'].">".$row['rol']."</option>";
				}
				?>
			</select>
			</td>
			<td>
			</td>
			<td>
			</td>
	</tr>
</div>
<?php } ?>

<!-- POR AGENCIA   --->
<?php if( ($tipo1 ==5) || ($tipo1 ==7) || ($tipo1 ==11)) { ?>
<div id="div-5" style="display:none">
	<tr>
		<td>  agencia: </td>
			<td>
			<select name="txtagencia" class="boxmediano" id="txtagencia">
				<option value="0">[Todos]</option>
				<?php 
				while ($row=$agencia->fetch()){
				echo "<option value=".$row['idagencia'].">".$row['agencia']."</option>";
				}
				?>
			</select>
			</td>
			<td>
			</td>
			<td>
			</td>
	</tr>
</div>
<?php } ?>





<!-- POR USUARIO   --->
<?php if( ($tipo1 ==6) || ($tipo1==7)){ ?>
<div id="div-6" style="display:none">
	<tr>
		<td>  usuario: </td>
			<td>
			<select name="txtusuario" class="boxmediano" id="txtusuario" >
				<option value="0">[Todos]</option>
				<?php 
				while ($row=$funcionario->fetch()){
				echo "<option value=".$row['idusuario'].">".$row['nombres']."</option>";
				}
				?>
			</select>
			</td>
			<td>
			</td>
			<td>
			</td>
	</tr>
</div>
<?php } ?>

<!-- POR TIPO RADICACION    --->
<?php if( ($tipo1==7) || ($tipo1==31 && ($plano1==2 || $plano1==3)) || ($tipo1==32 && ($plano1==2 || $plano1==3))){ ?>
<div id="div-7" style="display:none">
	<tr>
		<td>  Tipo Radicacion</td>
			<td>
			<select name="txttipord" class="boxmediano" id="txttipord" >
				<option value="0">[Todos]</option>
				<?php 
				while ($row=$tiporadic->fetch()){
				echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>"; 
				}
				?>
			</select>
			<td>
			</td>
			<td>
			</td>
	</tr>
</div>
<?php } ?>

<!-- POR TIPO RADICACION    --->
<?php if( ( $tipo1 == 34 && ( $plano1 == 4 ) ) ){ ?>
<div id="div-7" style="display:none">
	<tr>
		<td>  Tipo Radicacion</td>
			<td>
			<select name="txttipord" class="boxmediano" id="txttipord" >
				<option value="0">[Todos]</option>
				<?php 
				while ($row=$tiporadic->fetch()){
					if (( $tipo1 == 34 && ( $plano1 == 4 ) ) && ( $row['iddetalledef'] == 32 || $row['iddetalledef'] == 70 || $row['iddetalledef'] == 191 || $row['iddetalledef'] == 211 ) ) { 
						echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
					} 
				}
				?>
			</select>
			<td>
			</td>
			<td>
			</td>
	</tr>
</div>
<?php } ?>

<!-- recibo de caja -->
<?php if( $tipo1 ==8) { ?>
<div id="div-20" style="display:none">
  <tr>
    <td width="36%">Digite el N�mero de Recibo de Caja  </td>
    <td width="24%" >
    	<label for="comprobante">Comprobante: </label>
		<select id="comprobante" name="comprobante" class="box1">
			<option value="0" selected="selected">Seleccione</option>
			<?php foreach($comprobantesAportes as $comprobante): ?>
			<option value="<?php echo $comprobante; ?>"><?php echo $comprobante; ?></option>
			<?php endforeach; ?>
		</select>
		<label for="numComprobante">N&uacute;mero: </label> <input type="text" onkeyup="solonumeros(this)" onkeydown="solonumeros(this);" name="numComprobante" id="numComprobante" />
    </td>
    <td width="40%" >
    <input type="button" onclick="buscarReporte();" style="cursor:pointer" class="ui-state-default"  value="Generar certificado"/></td>
  </tr>
</div>
<?php
	$verBotonGeneraReporte = false;
}
?>


<!-- certificado de trab. activo -->
<?php if( $tipo1 ==9) { ?>
	<tr>
		<td width="24%" >
			<p>Digite el N&uacute;mero de Recibo de Caja:</p>
	    	<label for="comprobante">Comprobante: </label>
			<select id="comprobante" name="comprobante" class="box1">
				<option value="0" selected="selected">Seleccione</option>
				<?php foreach($comprobantesAportes as $comprobante): ?>
				<option value="<?php echo $comprobante; ?>"><?php echo $comprobante; ?></option>
				<?php endforeach; ?>  
			</select>
			<label for="numComprobante">N&uacute;mero: </label> <input type="text" onkeyup="solonumeros(this)" onkeydown="solonumeros(this);" name="numComprobante" id="numComprobante" />
	    </td>
	</tr>
	<tr>
	    <td width="40%" >
	    	<input type="button" onclick="buscarReporte();" style="cursor:pointer" class="ui-state-default"  value="Generar certificado"/>
	    </td>
	</tr>
 </div>
<?php 
$verBotonGeneraReporte = false;
} ?>

  
  <!-- certificado de empresas -->
<?php if( $tipo1 == 10) { ?>
<div id="div-21" style="display:none">
  <tr>
  	<td colspan="2"><p>Digite el N&uacute;mero de Recibo de Caja:</p></td>
  </tr>
  <tr>
    <td>
    	<label for="comprobante">Comprobante: </label>
		<select id="comprobante" name="comprobante" class="box1">
			<option value="0" selected="selected">Seleccione</option>
		<?php foreach($comprobantesAportes as $comprobante): ?>
		<option value="<?php echo $comprobante; ?>"><?php echo $comprobante; ?></option>
		<?php endforeach; ?>  
		</select>
	</td>
	<td>
		<label for="numComprobante">N&uacute;mero: </label> <input type="text" onkeyup="solonumeros(this)" onkeydown="solonumeros(this);" name="numComprobante" id="numComprobante" />
    </td>
  </tr>
  <tr>
  	<td colspan="2">
  		<br/>
  		<label for="">Digite el a&ntilde;o del k&aacute;rdex</label> <input name="kardex" type="text" class="box" id="kardex" />
  	</td>
  </tr>
  <tr>
	<td colspan="2"><br/><input type="button" onclick="buscarReporte();" style="cursor:pointer" class="ui-state-default"  value="Ver certificado"/></td>
  </tr>
</div>
<?php 
$verBotonGeneraReporte = false;
} ?>


<!-- certificado de trab. no afiliado -->
<?php if( $modulo1 == 'secretaria' && $tipo1 == 12) { ?>
	<tr>
		<td colspan="2">
			<p>Digite el N&uacute;mero de Recibo de Caja:</p>
	    	<label for="comprobante">Comprobante: </label>
			<select id="comprobante" name="comprobante" class="box1">
				<option value="0" selected="selected">Seleccione</option>
				<?php foreach($comprobantesAportes as $comprobante): ?>
				<option value="<?php echo $comprobante; ?>"><?php echo $comprobante; ?></option>
				<?php endforeach; ?>  
			</select>
			<label for="numComprobante">N&uacute;mero: </label> <input type="text" onkeyup="solonumeros(this)" onkeydown="solonumeros(this);" name="numComprobante" id="numComprobante" />
			<br/>
	    </td>
	</tr>
	<tr>
		<td>
			<br/>
			<label for="tipoDoc">Tipo documento:</label>
			<select name="tipoDoc" class="box1" id="tipoDoc">
				<option value="0">Seleccione..</option>
				<?php
					$consulta = $objDefiniciones->mostrar_datos(1,2);
				    while( $row = mssql_fetch_array( $consulta ) ){
						echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
					}
				?>
			</select>
		</td>
		<td>
			<br/>
			<label for="numeroDoc">N&uacute;mero de documento:</label>
			<input type="text" name="numeroDoc" id="numeroDoc" onblur="buscarPersona(document.getElementById('tipoDoc').value, this.value)" />
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<input type="text" name="nombre" id="nombre" size="80" />
		</td>
	</tr>
	<tr>
	    <td colspan="2" >
	    	<br/>
	    	<input type="button" onclick="buscarReporte();" style="cursor:pointer" class="ui-state-default"  value="Generar certificado"/>
	    </td>
	</tr>
<?php $verBotonGeneraReporte = false; } ?>


<!-- certificado de trab. inactivo -->
<?php if( $modulo1 == 'secretaria' && $tipo1 == 13) { ?>
	<tr>
		<td colspan="2">
			<p>Digite el N&uacute;mero de Recibo de Caja:</p>
	    	<label for="comprobante">Comprobante: </label>
			<select id="comprobante" name="comprobante" class="box1">
				<option value="0" selected="selected">Seleccione</option>
			<?php foreach($comprobantesAportes as $comprobante): ?>
			<option value="<?php echo $comprobante; ?>"><?php echo $comprobante; ?></option>
			<?php endforeach; ?>  
			</select>
			<label for="numComprobante">N&uacute;mero: </label> <input type="text" onkeyup="solonumeros(this)" onkeydown="solonumeros(this);" name="numComprobante" id="numComprobante" />
			<br/>
	    </td>
	</tr>
	<tr>
		<td>
			<br/>
			<label for="tipoDoc">Tipo documento:</label>
			<select name="tipoDoc" class="box1" id="tipoDoc">
				<option value="0">Seleccione..</option>
				<?php
					$consulta = $objDefiniciones->mostrar_datos(1,2);
				    while( $row = mssql_fetch_array( $consulta ) ){
						echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
					}
				?>
			</select>
		</td>
		<td>
			<br/>
			<label for="numeroDoc">N&uacute;mero de documento:</label>
			<input type="text" name="numeroDoc" id="numeroDoc" onblur="buscarPersona(document.getElementById('tipoDoc').value, this.value)" />
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<input type="text" name="nombre" id="nombre" size="80" />
		</td>
	</tr>
	<tr>
	    <td colspan="2" >
	    	<br/>
	    	<input type="button" onclick="buscarReporte();" style="cursor:pointer" class="ui-state-default"  value="Generar certificado"/>
	    </td>
	</tr>
<?php $verBotonGeneraReporte = false; } ?>


<!-- certificado de contratista -->
<?php if( $modulo1 == 'secretaria' && $tipo1 == 14) { ?>
	<tr>
		<td colspan="2">
			<p>Digite el N&uacute;mero de Recibo de Caja:</p>
	    	<label for="comprobante">Comprobante: </label>
			<select id="comprobante" name="comprobante" class="box1">
				<option value="0" selected="selected">Seleccione</option>
			<?php foreach($comprobantesAportes as $comprobante): ?>
			<option value="<?php echo $comprobante; ?>"><?php echo $comprobante; ?></option>
			<?php endforeach; ?>  
			</select>
			<label for="numComprobante">N&uacute;mero: </label> <input type="text" onkeyup="solonumeros(this)" onkeydown="solonumeros(this);" name="numComprobante" id="numComprobante" />
			<br/>
	    </td>
	</tr>
	<tr>
	    <td colspan="2" >
	    	<br/>
	    	<input type="button" onclick="buscarReporte();" style="cursor:pointer" class="ui-state-default"  value="Generar certificado"/>
	    </td>
	</tr>
<?php $verBotonGeneraReporte = false; } ?>

<?php 
//configuracion reporte Saldo Consolidado
if( $tipo1 == 33 ) { ?>
<div id="div-1" style="display:none;align:center">

<?php if($_REQUEST["rp"]==2){ ?>
	<tr>
		<td colspan="3" >
			<center>Digite la fecha del reporte
			<input name="fechaR" type="text" class="box" id="fechaR" readonly /></center>
		</td>
	</tr>
<?php }?>
	<tr align="center">
		<td colspan="3" >
		<center><label style="cursor:pointer" onClick="reporteSaldoConso(<?php echo $_REQUEST["rp"];?>,1);" id="idProcesar025"><img src="../imagenes/icono_excel.png" width="32" height="32"/></label>
      		<label style="cursor:pointer" onClick="reporteSaldoConso(<?php echo $_REQUEST["rp"];?>,2);" id="idProcesar025"><img src="../imagenes/icono_pdf.png" width="32" height="32"/></label>
      	</center>	
			
		</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
</div>
<?php $verBotonGeneraReporte = false; } ?>


<?php 
//configuracion reporte Saldo Cuota Monetaria Consolidado y Discriminado
if( $tipo1 == 50 ) { ?>
<div id="div-1" style="display:none;align:center">

	<tr>
		<td colspan="3" >
			<center>Digite la fecha del reporte
			<input name="fechaR" type="text" class="box" id="fechaR" readonly /></center>
		</td>
	</tr>
	<tr>
	<td colspan="3" style="text-align:center ">
	Tipo
			<select id="tipoInf" name="tipoInf" class="box1">
				<option value="0" selected="selected">Seleccione</option>
				<option value="1">Consolidado</option>
				<option value="2">Discriminado</option>  
			</select>
		</td>
	</tr>
	<tr align="center">
		<td colspan="3" >
		<center><label style="cursor:pointer" onClick="reporteSaldoCuotaMone(2);" id="idProcesar025"><img src="../imagenes/icono_excel.png" width="32" height="32"/></label>
      		<label style="cursor:pointer" onClick="reporteSaldoCuotaMone(1);" id="idProcesar025"><img src="../imagenes/icono_pdf.png" width="32" height="32"/></label>
      	</center>	
			
		</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
</div>
<?php $verBotonGeneraReporte = false; } ?>




<?php 
//HISTORICO CATEGORIAS
if( $tipo1 == 35 ) { ?>
<div id="div-1" style="display:none;align:center">


	<tr>
    <td width="21%">Fecha Inicial :</td>
    <td width="19%"><input name="fechaI" type="text" class="box" id="fechaI" readonly /></td>
    <td width="25%" >Fecha Final :</td>
    <td width="42%" ><input name="fechaF" type="text" class="box" id="fechaF" readonly /></td>
  </tr>
	
	<tr align="center">
		<td colspan="4" >
		<center><label style="cursor:pointer" onClick="reporteHistoricoCategorias(<?php echo $_REQUEST["rp"];?>,1);" id="idProcesar035"><img src="../imagenes/icono_excel.png" width="32" height="32"/></label>
      	<label style="cursor:pointer" onClick="reporteHistoricoCategorias(<?php echo $_REQUEST["rp"];?>,2);" id="idProcesar035"><img src="../imagenes/icono_pdf.png" width="32" height="32"/></label>
      	</center>	
			
		</td>
	</tr>
	<tr>
		<td colspan="4">&nbsp;</td>
	</tr>
</div>
<?php $verBotonGeneraReporte = false; } ?>



<?php 
//INFORME CONSOLIDADO GIRO
if( $tipo1 == 36 ) { ?>
<div id="div-1" style="display:none;align:center">


	<tr align="center">
    <td colspan="4" width="21%"><center>Periodo Giro :<input name="txtPeriodoI" type="text" class="box" id="txtPeriodoI" readonly /></center>
    
    <center>Agencia:
			<select name="cmbAgencia" class="boxmediano" id="cmbAgencia">
					<option value="T">TODOS</option>
				<?php 
					while ($row=$agencia->fetch()){
						echo "<option value=".$row['codigo'].">".$row['agencia']."</option>";
					}
				?>
			</select>
	</center>	
    
    </td>
  </tr>
	
	<tr align="center">
		<td colspan="4" >
		<center><label style="cursor:pointer" onClick="reporteConsolidadoGiro(<?php echo $_REQUEST["rp"];?>,1);" id="idProcesar036"><img src="../imagenes/icono_excel.png" width="32" height="32"/></label>
      	<label style="cursor:pointer" onClick="reporteConsolidadoGiro(<?php echo $_REQUEST["rp"];?>,2);" id="idProcesar036"><img src="../imagenes/icono_pdf.png" width="32" height="32"/></label>
      	</center>	
		</td>
	</tr>
	<tr>
		<td colspan="4">&nbsp;</td>
	</tr>
</div>
<?php $verBotonGeneraReporte = false; } ?>


<?php 
//INFORME CONSOLIDADO RADICACION Y GRABACION
if( $tipo1 == 37 ) { ?>
<div id="div-1" style="display:none;align:center">


	<tr align="center">
    <td width="21%">Fecha Inicial:</td><td><input name="fechaI" type="text" class="box" id="fechaI" readonly /></td>
    <td width="21%">Fecha Final:</td><td><input name="fechaF" type="text" class="box" id="fechaF" readonly /></td>
  </tr>
  <tr>
	  <td colspan="2" width="21%">Agencia:
		  <select name="cmbAgencia37" class="boxmediano" id="cmbAgencia37">
					<option value="T">TODOS</option>
				<?php 
					while ($row=$agencia->fetch()){
						echo "<option value=".$row['codigo'].">".$row['agencia']."</option>";
					}
				?>
			</select>
	  
	  </td>
	  <td colspan="2" width="21%">Tipo:
		  <select name="cmbAgencia37a" class="boxmediano" id="cmbAgencia37a">
					<option value="0">Seleccione</option>
					<option value="R">Radicacion</option>
					<option value="G">Grabacion</option>
			</select>
	  
	  </td>
	  
  </tr>
	
	<tr align="center">
		<td colspan="4" >
		<center><label style="cursor:pointer" onClick="reporteConsolidoRadicaGraba(<?php echo $_REQUEST["rp"];?>,1);" id="idProcesar037"><img src="../imagenes/icono_excel.png" width="32" height="32"/></label>
      	<label style="cursor:pointer" onClick="reporteConsolidoRadicaGraba(<?php echo $_REQUEST["rp"];?>,2);" id="idProcesar037"><img src="../imagenes/icono_pdf.png" width="32" height="32"/></label>
      	</center>	
			
		</td>
	</tr>
	<tr>
		<td colspan="4">&nbsp;</td>
	</tr>
</div>
<?php $verBotonGeneraReporte = false; } ?>


<?php 
//INFORME VARIACION AFILIADO
if( $tipo1 == 38 ) { ?>
<div id="div-1" style="display:none;align:center">


	<tr align="center">
    <td colspan="4" width="21%"><center>Fecha de Corte :<input name="fechaI" type="text" class="box" id="fechaI" readonly /></center></td>
  </tr>
	
	<tr align="center">
		<td colspan="4" >
		<center><label style="cursor:pointer" onClick="reporteVariacionAfiliado(<?php echo $_REQUEST["rp"];?>,1);" id="idProcesar036"><img src="../imagenes/icono_excel.png" width="32" height="32"/></label>
      	<label style="cursor:pointer" onClick="reporteVariacionAfiliado(<?php echo $_REQUEST["rp"];?>,2);" id="idProcesar036"><img src="../imagenes/icono_pdf.png" width="32" height="32"/></label>
      	</center>	
			
		</td>
	</tr>
	<tr>
		<td colspan="4">&nbsp;</td>
	</tr>
</div>
<?php $verBotonGeneraReporte = false; } ?>


<?php 
//LISTADO AFILIACIONES ESTADOS PU - SEGUIMIENTO
if( $tipo1 == 39 ) { ?>
<div id="div-1" style="display:none;align:center">
		<tr>
		<td colspan="2" style="text-align:center;" width="30%" >
			Fecha Ingreso:
			<input type="text" id="fechaI" name="fechaI" readonly size="15" class="box1"/>
			&nbsp;&nbsp;
			-
			<input type="text" id="fechaF" name="fechaF" readonly size="15" class="box1"/> 
		</td>
		</tr>
		 <tr>
		 <td colspan="2" style="text-align:center;" >
  				Nit:
  			<input type="text" name="txtNit" id="txtNit" class="box1" />
  			</td>
		 </tr>
		 <tr>
			<td colspan="2" style="text-align: center;">
			 Periodo:
			 <input type="text" id="txtPeriodo" name="txtPeriodo" class="box1"></td>
		</tr>
	
		<tr align="center">
		<td colspan="4" >
		<center><label style="cursor:pointer" onClick="reporteEstadoPu(<?php echo $_REQUEST["rp"];?>,1);" id="idProcesar036"><img src="../imagenes/icono_excel.png" width="32" height="32"/></label>
      	<label style="cursor:pointer" onClick="reporteEstadoPu(<?php echo $_REQUEST["rp"];?>,2);" id="idProcesar036"><img src="../imagenes/icono_pdf.png" width="32" height="32"/></label>
      	</center>	
			
		</td>
	</tr>
	<tr>
		<td colspan="4">&nbsp;</td>
	</tr>
</div>
<?php $verBotonGeneraReporte = false; } ?>



<?php if($verBotonGeneraReporte): ?>
  <tr>
    <td colspan="4">

     <?php if($opcion1=='020'){?>
     <center>
    	<label style="cursor:pointer" onClick="buscarReporte020('excel');" id="idProcesar027"><img src="../imagenes/icono_excel.png" width="32" height="32"/></label>
      	<label style="cursor:pointer" onClick="buscarReporte020('pdf');" id="idProcesar027"><img src="../imagenes/icono_pdf.png" width="32" height="32"/></label>		
     </center>
    <?php }
	
	else
		 {?>
  		    <center><input type="button" onclick="buscarReporte();" style="cursor:pointer" class="ui-state-default" value="Generar reporte"/></center></td>
    <?php }?>
  </tr>
<?php  endif; ?>
  <?php if ($plano1==1){ ?>
  <tr>
    <td colspan="4"><input type="button" onclick="generarPlano();" style="cursor:pointer" class="ui-state-default" value="Generar Plano"/></td>
  </tr>
  <?php } ?>
 </table> 
<p class="Rojo" align="center" style="display:none">Ingrese la fecha del reporte.</p>
<!-- Mostrar los datos optenidos por ajax del reporte 016 Abonos pendientes de procesar -->
<br><br>
<?php  if($tipo1 ==26){?>
<button id="btexcel" class="btexcel" >Excel</button>
<?php }?>
<div id="content_carga"></div>
<div id="conten_table"></div></table>
<!-- fin tabla reporte 016 Abonos pendientes de procesar -->
<br><br>
<p align="center"><a href="asopagos/menuAsopagos.php" ><img  alt="Volver al Listado" style="border:none"/></a></p>
</body>

		
</body> 

<script language="javascript">
var URL=src();

//Funcion para manejar el tipo de filtro para el reporte de las morosas
/*function managerFiltro30(){
	var tipoFiltro = $("#cmbFiltro30").val();
	$("#trPeriodos input, #trNit input").val("");
	
	if(tipoFiltro=="Rango") {
		$("#trPeriodos").show();
		$("#trNit").hide();
	}else if(tipoFiltro=="Empresa") {
		$("#trPeriodos").hide();
		$("#trNit").show();
	}else if(tipoFiltro=="EmpresaRango") {
		$("#trPeriodos").show();
		$("#trNit").show();
	}
		
}*/

//Funcion para manejar los pagos de aportes parafiscales
function managerFiltro41(){
	var tipoFiltro = $("#cmbFiltro41").val();
	$("#trFechas input, #trPeriodo input").val("");
	
	if(tipoFiltro=="Mes") {
		$("#trFechas").show();
		$("#trPeriodo").hide();
	}else if(tipoFiltro=="PagoNuevo") {
		$("#trFechas").hide();
		$("#trPeriodo").show();
	}else if(tipoFiltro=="Periodo41") {
		$("#trPeriodo").hide();
		$("#trFechas").show();
	}
		
}

function procesar1(opt){	
	var fecha = $("#fechaF").val();
	
	if(fecha==""){
		alert("Debe ingresar la fecha de corte");
		return false;
	}
	url="<?php echo $ruta_reportes;?>asopagos/rptExcel025.jsp?v0="+fecha;
	window.open(url,"_NEW");
}


function procesar2(opt){	
	var tip = $("#cmbtipo").val();

	if(opt==1){
		url="<?php echo $ruta_reportes;?>asopagos/rptExcel016.jsp?v0=<?php echo $usuario; ?>&v1="+tip;
		
	}else
		{
		url="<?php echo $ruta_reportes;?>asopagos/reporte016.jsp?v0=<?php echo $usuario; ?>&v1="+tip;
		}
		
	window.open(url,"_NEW");
}

function procesar3(opt){	
	var tip = $("#cmbtipo").val();

	if(opt==1){
		url="<?php echo $ruta_reportes;?>asopagos/rptExcel024.jsp?v0=<?php echo $usuario; ?>&v1="+tip;
		
	}else
		{
		url="<?php echo $ruta_reportes;?>asopagos/reporte024.jsp?v0=<?php echo $usuario; ?>&v1="+tip;
		}
		
	window.open(url,"_NEW");
}

function procesar4(opt){	
	var tip = $("#cmbtipo").val();
	var fechini = $("#fechaI").val();
	var fechfin = $("#fechaF").val();

	if(opt==1){
		url="<?php echo $ruta_reportes;?>asopagos/rptExcel026.jsp?v0=<?php echo $usuario; ?>&v1="+tip+"&v2="+fechini+"&v3="+fechfin;
		
	}else
		{
		url="<?php echo $ruta_reportes;?>asopagos/reporte026.jsp?v0=<?php echo $usuario; ?>&v1="+tip+"&v2="+fechini+"&v3="+fechfin;
		}
		
	window.open(url,"_NEW");
}


function procesar6(opt){	
	var tip = $("#cmbAgencia37").val();
	var fechini = $("#fechaI").val();
	var fechfin = $("#fechaF").val();

	if(opt==1){
		url="<?php echo $ruta_reportes; ?>/asopagos/rptExcel027.jsp?v0=<?php echo $usuario; ?>&v1="+tip+"&v2="+fechini+"&v3="+fechfin;
		
	}else
		{
		url="<?php echo $ruta_reportes; ?>/asopagos/reporte027.jsp?v0=<?php echo $usuario; ?>&v1="+tip+"&v2="+fechini+"&v3="+fechfin;
		}
		
	window.open(url,"_NEW");
}
function procesar15(opt){	
	var tip = $("#cmbAgencia37").val();
	var fechini = $("#fechaI").val();
	var fechfin = $("#fechaF").val();

	if(opt==1){
		url="<?php echo $ruta_reportes; ?>/asopagos/rptExcel041.jsp?v0=<?php echo $usuario; ?>&v1="+tip+"&v2="+fechini+"&v3="+fechfin;
		
	}else
		{
		url="<?php echo $ruta_reportes; ?>/asopagos/reporte041.jsp?v0=<?php echo $usuario; ?>&v1="+tip+"&v2="+fechini+"&v3="+fechfin;
		}
		
	window.open(url,"_NEW");
}

function procesar7(opt){	
	var tip = $("#cmbAgencia37").val();
	var fechini = $("#fechaI").val();
	var fechfin = $("#fechaF").val();

	if(opt==1){
		url="<?php echo $ruta_reportes;?>asopagos/rptExcel028.jsp?v0=<?php echo $usuario; ?>&v1="+tip+"&v2="+fechini+"&v3="+fechfin;
		
	}else
		{
		url="<?php echo $ruta_reportes;?>asopagos/reporte028.jsp?v0=<?php echo $usuario; ?>&v1="+tip+"&v2="+fechini+"&v3="+fechfin;
		}
		
	window.open(url,"_NEW");
}

function procesar8(opt){	
	var fechini = $("#fechaI").val();
	var fechfin = $("#fechaF").val();

	if(opt==1){
		url="<?php echo $ruta_reportes;?>asopagos/rptExcel030.jsp?v0=<?php echo $usuario; ?>&v1="+fechini+"&v2="+fechfin;
		
	}else
		{
		url="<?php echo $ruta_reportes;?>asopagos/reporte030.jsp?v0=<?php echo $usuario; ?>&v1="+fechini+"&v2="+fechfin;
		}
		
	window.open(url,"_NEW");
}

function procesar9(op){
	var fecha=$("#txtFecha").val(); //Fecha del sistema	
	var fechini = $("#fechaI").val();
	var fechfin = $("#fechaF").val();
	var identificacion = $("#documento").val();
	var bono = $("#bono").val();
	var estado  = $("#txtEstado").val().trim();
	var agencia = $("#txtagencia").val();
	var usuario2 = $("#txtUsuario2").val();

	
	var url = "";
	
	if(op==1){
		url="<?php echo $ruta_reportes;?>asopagos/rptExcel037.jsp?v0=<?php echo $usuario; ?>&v1=" + fecha + "&v2=" + fechini+ "&v3=" + fechfin + "&v4=" + identificacion+ "&v5=" + bono + "&v6=" + estado+ "&v7=" + agencia + "&v8=" + usuario2;	
	}else if (op==2){
		url="<?php echo $ruta_reportes;?>asopagos/reporte037.jsp?v0=<?php echo $usuario; ?>&v1=" + fecha + "&v2=" + fechini+ "&v3=" + fechfin + "&v4=" + identificacion+ "&v5=" + bono+ "&v6=" + estado+ "&v7=" + agencia + "&v8=" + usuario2;
	}
	window.open(url,"_NEW");
}


function procesar13(){
	var uri = URL+"phpComunes/buscarHabeasData.php";
	var data={
		   tipop:$("#cmbtipo").val(),	
           fechaIni:$("#txtFechaIni").val(),
           fechaFin:$("#txtFechaFin").val(),
           hd:$("#cmbhd").val(),
           agencia:$("#txtagencia").val()
		 };
	var request = call_Ajax(uri,data);
	
	if(request.canreg==0){
		alert("No se encontraron registros que cumplan con los criterios indicados");
	}else if (request.canreg>0){
		$("#conten_table").html('<table id="tablatempo"  border="1"   style="display:none" width="90%"></table>');
		var tablatempo="<thead><tr><th>Identificacion</th><th>Nombre</th><th>Departamento</th><th>Municipio</th><th>Direccion</th><th>Telefono</th><th>Celular</th><th>Email</th><th>Habeas Data</th><th>Fecha HD</th><th>Usuario HD</th><th>Agencia</th><th>Tipo Persona</th></tr></thead>";
		    tablatempo+="<tbody>"
		$.each(request.data,function(index,value){ 
			tablatempo += '<tr><td>'+value.identificacion+'</td><td>'+value.nombre+'</td><td>'+value.departamento+'</td><td>'+value.municipio+'</td><td>'+value.direccion+'</td><td>'+value.telefono+'</td><td>'+value.celular+'</td><td>'+value.email+'</td><td>'+value.habeas_data+'</td><td>'+value.fechahd+'</td><td>'+value.usuariohd+'</td><td>'+value.agencia+'</td><td>'+value.tipopersona+'</td></tr>';
			//arrayaux[index]=[value.nombre,value.identificacion,value.periodogiro,value.bono,value.tipopago,value.valor,value.direccion,value.telefono,add,consu];
		});
		tablatempo += "</tbody>";
		$("#tablatempo").append(tablatempo);
		var f = new Date();
        $("#tablatempo").table2excel({
           exclude: ".excludeThisClass",
           name: "Worksheet Name",
           fileext:'.xls',
           filename: "reporte"+f.getFullYear()+f.getMonth()+f.getDate()+f.getHours()+":"+f.getMinutes()+":"+f.getSeconds() //do not include extension
        });
     }
	
}



function procesar10(op){
	var fecha=$("#txtFecha").val(); //Fecha del sistema	
	var fechini = $("#fechaI").val();
	var fechfin = $("#fechaF").val();

	if (fechini=="")
	{
		alert("Debe ingresar una fecha Inicial");
		return false;
	}
	
	var url = "";
	if(op==1){
		url="<?php echo $ruta_reportes;?>asopagos/rptExcel039.jsp?v0=<?php echo $usuario; ?>&v1=" + fecha + "&v2=" + fechini+ "&v3=" + fechfin;	
	}else if (op==2){
		url="<?php echo $ruta_reportes;?>asopagos/reporte039.jsp?v0=<?php echo $usuario; ?>&v1=" + fecha + "&v2=" + fechini+ "&v3=" + fechfin;
	}
	window.open(url,"_NEW");
}

function procesar5(opt){	

	var fechini = $("#fechaI").val();
	var fechfin = $("#fechaF").val();

	if(opt==1){
		url="<?php echo $ruta_reportes;?>asopagos/rptExcel013.jsp?v0=<?php echo $usuario; ?>&v1="+fechini+"&v2="+fechfin;
		
	}else
		{
		url="<?php echo $ruta_reportes;?>asopagos/reporte013.jsp?v0=<?php echo $usuario; ?>&v1="+fechini+"&v2="+fechfin;
		}
		
	window.open(url,"_NEW");
}


function generarPlano(){
    var op =  <?php echo $tipo1; ?>;
	var url="";
	
	if(op==1){
        if($("#fechaR").val()==''){
		$("p.Rojo").show();
		return false;
		}
     $("p.Rojo").hide();
	var fechaInicio=$("#fechaR").val();
    url='<?php echo $ruta_reportes,$modulo1; ?>/reporte<?php echo $opcion1; ?>.jsp?v0=<?php echo $usuario; ?> &v1='+fechaInicio; 
    }
	
 	 if((op==2)){
        if($("#fechaI").val()==''||$("#fechaF").val()==''){
		$("p.Rojo").show();
		return false;
		}
     $("p.Rojo").hide();
	var fechaInicio=$("#fechaI").val();
    var fechaFinal=$("#fechaF").val();
     url='http://<?php echo $ip;?>/sigas/centroReportes/reporteTesoreria/reporte<?php echo $opcion1; ?>Plano.php?v1='+fechaInicio+'&v2='+fechaFinal; 
	//url='http://:8080/CentroReportes/<?php echo $modulo1; ?>/reporte<?php echo $opcion1; ?>Plano.php?v1='+fechaInicio+'&v2='+fechaFinal; 
    }
	window.open(url,"_blank");
	window.close('configurarReporte.php');

}

function buscarReporte020(op){	

	var fechaInicio=$("#fechaI").val();
	var fechaFinal=$("#fechaF").val();
	var plano1 =  <?php echo $plano1; ?>;
	var url;
	if($("#fechaI").val()==''||$("#fechaF").val()=='')
	{
		$("p.Rojo").show();
		return false;
	}
	$("p.Rojo").hide();
	
	if(op=='pdf')
	{			
	   url='<?php echo $ruta_reportes,$modulo1; ?>/reporte<?php echo $opcion1; ?>.jsp?v0=<?php echo $usuario; ?> &v1='+fechaInicio+'&v2='+fechaFinal;   
    }
	else
	if(op=='excel')
	{
		url='<?php echo $ruta_reportes,$modulo1; ?>/rptExcel<?php echo $opcion1; ?>.jsp?v0=<?php echo $usuario; ?> &v1='+fechaInicio+'&v2='+fechaFinal;   
		
	}	
		
	window.open(url,"_NEW");
}

function buscarReporte(){
    var op =  <?php echo $tipo1; ?>;
	var plano1 =  <?php echo $plano1; ?>;
	var url;
	if(op==1){
        if($("#fechaR").val()==''){
		$("p.Rojo").show();
		return false;
		}
     $("p.Rojo").hide();
	var fechaInicio=$("#fechaR").val();
    url='<?php echo $ruta_reportes,$modulo1; ?>/reporte<?php echo $opcion1; ?>.jsp?v0=<?php echo $usuario; ?> &v1='+fechaInicio; 
    }
	
 	 if((op==2)){
        if($("#fechaI").val()==''||$("#fechaF").val()==''){
		$("p.Rojo").show();
		return false;
		}
     $("p.Rojo").hide();
	var fechaInicio=$("#fechaI").val();
    var fechaFinal=$("#fechaF").val();

	
	if (plano1 == 2) {
      //url='http://:8080/sigasReportes/<?php echo $modulo1; ?>/reporte<?php echo $opcion1; ?>.php?v0=<?php echo $usuario; ?> &v1='+fechaInicio+'&v2='+fechaFinal; 	
	    url='http://<?php echo $ip;?>/sigas/centroReportes/<?php echo $modulo1; ?>/reporte<?php echo $opcion1; ?>.php?v0=<?php echo $usuario; ?> &v1='+fechaInicio+'&v2='+fechaFinal; 	
	   } 
	   else{
	url='<?php echo $ruta_reportes,$modulo1; ?>/reporte<?php echo $opcion1; ?>.jsp?v0=<?php echo $usuario; ?> &v1='+fechaInicio+'&v2='+fechaFinal;   
	   }
	}
	
	if(op==5){
	    var url="";
		var agenciaC=$("#txtagencia").val();

		if ( '<?php echo $opcion1; ?>' != '013' ) {
	        url='<?php echo $ruta_reportes,$modulo1; ?>/reporte<?php echo $opcion1; ?>.jsp?v0=<?php echo $usuario; ?> &v1='+agenciaC; 
	
	        if(( $("#txtagencia").val())==0 && '<?php echo $opcion1; ?>' != '013' ){
			  url='<?php echo $ruta_reportes,$modulo1;?>/reporte<?php echo $opcion1; ?>T.jsp?v0=<?php echo $usuario; ?>'; 
		    }
		} else {
			url='<?php echo $ruta_reportes,$modulo1; ?>/rptExcel<?php echo $opcion1; ?>.jsp?v0=<?php echo $usuario; ?> &v1='+agenciaC;
		}
	}
	
	if((op==7)){
		$("p.Rojo").hide();

		if($("#fechaI").val()==''||$("#fechaF").val()==''){
			$("p.Rojo").show();
			return false;
		}
     	
		var fechaInicio=$("#fechaI").val();
		var fechaFinal=$("#fechaF").val();
		var agencia=$("#txtagencia").val();		
		var tiporad=$("#txttipord").val();
		var funcionario=$("#txtusuario").val();

		url='<?php echo $ruta_reportes,$modulo1; ?>/reporte001_01.jsp?v0=<?php echo $usuario; ?>&v1='+fechaInicio+'&v2='+fechaFinal+'&v3='+agencia+'&v4='+tiporad+'&v5='+funcionario;
	}

    if((op==11)){
    	$("p.Rojo").hide();
        if($("#fechaI").val()==''||$("#fechaF").val()==''){
            $("p.Rojo").show();
            return false;
		}

        var opcion =  <?php echo $opcion2; ?>;
		var fechaInicio=$("#fechaI").val();
		var fechaFinal=$("#fechaF").val();
		var agencia=$("#txtagencia").val();

		if(opcion==2){
			if  ($("#txtagencia").val() == "0" )  {
				url='<?php echo $ruta_reportes,$modulo1; ?>/reporte<?php echo $opcion1; ?>.jsp?v0=<?php echo $usuario; ?> &v1='+fechaInicio+'&v2='+fechaFinal; 
			} else {
				url='<?php echo $ruta_reportes,$modulo1; ?>/reporte<?php echo $opcion1; ?>_1.jsp?v0=<?php echo $usuario; ?> &v1='+fechaInicio+'&v2='+fechaFinal+'&v3=0'+agencia;
			}
		} else if(opcion==3) {
			url='<?php echo $ruta_reportes,$modulo1; ?>/reporte<?php echo $opcion1; ?>.jsp?v0='+fechaInicio+'&v1='+fechaFinal+'&v2=<?php echo $usuario; ?>&v3=0'+agencia;
		}
	}

	//Informe de morosas
	if(op==30){
		//var comboFiltro = $("#cmbFiltro30").val();
		var periodoMoraI = $("#txtPeriodoI").val();
		var periodoMoraF = $("#txtPeriodoF").val();
		var nitEmpresaMora = $("#txtNit").val().trim();

		
		$(".ui-state-error").removeClass("ui-state-error").addClass("box1");
		//Validar valores vacios
		if(!periodoMoraI && periodoMoraF){
			$("#txtPeriodoI").addClass("ui-state-error").removeClass("box1");
			return false;
		}else if(periodoMoraI && !periodoMoraF){
			$("#txtPeriodoF").addClass("ui-state-error").removeClass("box1");
			return false;
		}

		$.ajax({
			url:URL+"centroReportes/gestion/morosas.php",
			type:"POST",
			data:{periodoI:periodoMoraI,periodoF:periodoMoraF,nit:nitEmpresaMora},
			//dataType:"json",
			async:false,
			beforeSend: function(objeto){
				dialogLoading('show');
			},
			complete: function(objeto, exito){
				dialogLoading('close');
				if(exito != "success"){
					alert("No se completo el proceso!");
				}
			}, 
			success:function(datos){
				if(datos==1){
					var url=URL+'phpComunes/descargaPlanos.php';
					window.open(url);
				}
			}
		});
	}
	
	//Analisis de periodos
	if(op==41){

		var comboFiltro = $("#cmbFiltro41").val();
		var objPeriodoI = $("#txtPeriodoI");
		var objFechaI = $("#fechaI");
		var objFechaF = $("#fechaF");
		
		objPeriodoI.removeClass("ui-state-error");
		objFechaI.removeClass("ui-state-error");
		objFechaF.removeClass("ui-state-error");
		//Validaciones
		if(comboFiltro=="Mes" 
			&& (objFechaI.val()==0 || objFechaF.val()==0)) {

		objFechaI.addClass("ui-state-error");
		objFechaF.addClass("ui-state-error");
		return false;
		}else if(comboFiltro=="PagoNuevo" && objPeriodoI.val()==0) {
			objPeriodoI.addClass("ui-state-error");
		return false;
		}else if(comboFiltro=="Periodo41" 
			&& (objFechaI.val()==0 || objFechaF.val()==0)) {
			
		objFechaI.addClass("ui-state-error");
		objFechaF.addClass("ui-state-error");
		return false;
		}

		if(comboFiltro=="Mes"){
			ruta = URL+"centroReportes/gestion/analisisempresa.php";
			datos = {fechaI:objFechaI.val(),fechaF:objFechaF.val()}
		} else if(comboFiltro=="PagoNuevo"){
			ruta = URL+"centroReportes/gestion/empresanueva.php";
			datos = {periodoI:objPeriodoI.val()};
		} else if(comboFiltro=="Periodo41") {
			ruta = URL+"centroReportes/gestion/analisisperiodo.php";
			datos = {fechaI:objFechaI.val(),fechaF:objFechaF.val()};
				}
			$.ajax({
	            url:ruta,
				type:"POST",
				data:datos,
				dataType:"json",
				async:false,
				beforeSend: function(objeto){
					dialogLoading('show');
				},
				complete: function(objeto, exito){
					dialogLoading('close');
					if(exito != "success"){
						alert("No se completo el proceso!");
					}
				}, 
				success:function(datos){
					if(datos==1){
						var url=URL+'phpComunes/descargaPlanos.php';
						window.open(url);
					}
				}
			});
	}



	//INFORME CONCILIACION APORTES Y NOMINA
	if(op==46){

		var objFechaI = $("#fechaI");
		
		objFechaI.removeClass("ui-state-error");

		ruta = URL+"centroReportes/gestion/conciliacionAportes.php";
		datos = {fechaI:objFechaI.val()};

			$.ajax({
	            url:ruta,
				type:"POST",
				data:datos,
				dataType:"json",
				async:false,
				beforeSend: function(objeto){
					dialogLoading('show');
				},
				complete: function(objeto, exito){
					dialogLoading('close');
					if(exito != "success"){
						alert("No se completo el proceso!");
					}
				}, 
				success:function(datos){
					if(datos==1){
						var url=URL+'phpComunes/descargaPlanos.php';
						window.open(url);
					}
				}
			});
	}


	//Reportes Radicaciones - Grabaciones
	if(op==31){
		$("p.Rojo").hide();
				
		if($("#fechaI").val()==''||$("#fechaF").val()=='')
		{
			$("p.Rojo").show();
			return false;
		}

		var v0=$("#cmbUsuario").val().split('-')[0];					
		var v4=$("#cmbUsuario").val().split('-')[1];

		if(v0==""){
			alert("Debe seleccionar un usuario");
			return false;
		}

		var v1=$("#fechaI").val();
    	var v2=$("#fechaF").val();

    	if(plano1==0) {
			url='<?php echo $ruta_reportes;?>gestion/reporte006.jsp?v0='+v0+'&v1='+v1+'&v2='+v2+'&v3=<?php echo $usuario; ?>&v4='+v4;
		} else if (plano1==2) {
			var tiporad=parseInt($("#txttipord").val());
			url='<?php echo $ruta_reportes;?>gestion/reporte008.jsp?v0='+v0+'&v1='+v1+'&v2='+v2+'&v3=<?php echo $usuario; ?>&v4='+v4+'&v5='+tiporad;
		} else if (plano1==3){
			var tiporad=parseInt($("#txttipord").val());
			url='<?php echo $ruta_reportes;?>gestion/rptExcel018.jsp?v0='+v0+'&v1='+v1+'&v2='+v2+'&v3=<?php echo $usuario; ?>&v4='+v4+'&v5='+tiporad;
		}
	}

	//Reportes Radicaciones - Grabaciones
	if(op==32){		
		if($("#fechaI").val()==''||$("#fechaF").val()=='')
		{
			$("p.Rojo").show();
			return false;
		}

		$("p.Rojo").hide();
		
		var v0=$("#fechaI").val();
    	var v1=$("#fechaF").val();

    	if(plano1==0)
			url='<?php echo $ruta_reportes;?>gestion/reporte007.jsp?v0='+v0+'&v1='+v1+'&v2=<?php echo $usuario; ?>';
		else if (plano1==2) {
			var tiporad=parseInt($("#txttipord").val());
			url='<?php echo $ruta_reportes;?>gestion/rptExcel016.jsp?v0='+v0+'&v1='+v1+'&v2='+tiporad;
		} else if (plano1==3) {
			var tiporad=parseInt($("#txttipord").val());
			url='<?php echo $ruta_reportes;?>gestion/reporte009.jsp?v0='+v0+'&v1='+v1+'&v2=<?php echo $usuario; ?>&v3='+tiporad;
		}
	}

	//Reportes Auditoria
	if(op==34){
		$("p.Rojo").hide();
				
		if($("#fechaI").val()==''||$("#fechaF").val()=='') {
			$("p.Rojo").show();
			return false;
		} else if($("#fechaI").val() > $("#fechaF").val()) {
			alert("Fecha Inicial no puede ser mayor a la Final");
			return false;
		}
		
		var v0=$("#cmbAgencia2").val();
		var v1=$("#fechaI").val();
    	var v2=$("#fechaF").val();

    	if(!( plano1 == 3 || plano1 == 5)){
    		v0=$("#cmbUsuario2").val().split('-')[0];					
	    	var v3=$("#cmbAgencia2").val();				
	
			if(v3!="0" && v0==""){
				alert("Debe seleccionar un usuario");
				return false;
			}
    	}		

		if(plano1==0) {
			url='<?php echo $ruta_reportes;?>gestion/reporte013.jsp?v0='+v0+'&v1='+v1+'&v2='+v2+'&v3=<?php echo $usuario; ?>';
		} else if (plano1==2) {
			url='<?php echo $ruta_reportes;?>gestion/reporte014.jsp?v0='+v0+'&v1='+v1+'&v2='+v2+'&v3=<?php echo $usuario; ?>';
		} else if (plano1==3) {
			url='<?php echo $ruta_reportes;?>gestion/reporte017.jsp?v0='+v0+'&v1='+v1+'&v2='+v2+'&v3=<?php echo $usuario; ?>';
		} else if (plano1==4) {
			var v4=$("#txttipord").val();
			
			url='<?php echo $ruta_reportes;?>gestion/reporte020.jsp?v0='+v0+'&v1='+v1+'&v2='+v2+'&v3=<?php echo $usuario; ?>&v4='+v4;
		} else if (plano1==5) {
			url='<?php echo $ruta_reportes;?>gestion/reporte021.jsp?v0='+v0+'&v1='+v1+'&v2='+v2+'&v3=<?php echo $usuario; ?>';
		}
	}

	// certificados de afiliaci�n
	if(op == 8 || op == 9){
		if($("#comprobante").val() != "" && $("#numComprobante").val() != ""){
			var comprobante = $("#comprobante").val();
			var numComprobante = $("#numComprobante").val();
			
			$.ajax({
				type:"POST",
				url: URL+"aportes/empresas/buscarComprobante.php",
				data: {v0:comprobante,v1:numComprobante},
				async:false,
				dataType: "json",
				success:function(data){
				if(data==0){
					alert("No existe el comprobante!");
					return false;
				}
				url = URL +'centroReportes/aportes/secretaria/generar.php?modulo=<?php echo $modulo1; ?>&archivo=<?php echo $opcion1; ?>&identificacion='+ data[0].nit;
				//url='http://:8080/sigasReportes/<?php echo $modulo1; ?>/reporte<?php echo $opcion1; ?>.jsp?v0=<?php echo $usuario; ?> &v1='+ fila.nit;
				window.open(url,"_blank");
			}});

		}else{
			alert("Verifique el num. de recibo.");
		}
	}

	if(op == 10){
		// certificado de afiliaci�n de empresa con kardex
		if($("#comprobante").val() != "" && $("#numComprobante").val() != ""){
			var comprobante = $("#comprobante").val();
			var numComprobante = $("#numComprobante").val();
			
			$.ajax({
				type:"POST",
				url: URL+"aportes/empresas/buscarComprobante.php",
				data: {v0:comprobante,v1:numComprobante},
				async:false,
				dataType: "json",
				success:function(data){
				if(data==0){
					alert("No existe el comprobante!");
					return false;
				}
				url = URL +'centroReportes/aportes/secretaria/generar.php?modulo=<?php echo $modulo1; ?>&archivo=<?php echo $opcion1; ?>&identificacion='+ data[0].nit+"&kardex="+$("#kardex").val();
				//url='http://:8080/sigasReportes/<?php echo $modulo1; ?>/reporte<?php echo $opcion1; ?>.jsp?v0=<?php echo $usuario; ?> &v1='+ fila.nit;
				window.open(url,"_blank");
			}});

		}else{
			alert("Verifique el num. de recibo.");
		}
	}

	if(op == 12){
		// certificado trabajador no afiliado
		if($("#comprobante").val() != "" && $("#numComprobante").val() != ""){
			var comprobante = $("#comprobante").val();
			var numComprobante = $("#numComprobante").val();
			var tipoDoc = $("#tipoDoc").val();
			var numeroDoc = $("#numeroDoc").val();
			var continuar = false;
			
			$.ajax({
				type:"POST",
				url: URL+"aportes/empresas/buscarComprobante.php",
				data: {v0:comprobante,v1:numComprobante},
				async:false,
				dataType: "json",
				success:function(data){
				if(data==0){
					alert("No existe el comprobante!");
					return false;
				}

				$.ajax({
					type: "POST",
					url: URL+"phpComunes/buscarPersona2.php",
					data: {v0: tipoDoc, v1: numeroDoc},
					async: false,
					dataType: "json",
					success:function(datos){
						continuar = true;
						if(datos != 0){
							$.each(datos,function(c,persona){
								$("#nombre").val($.trim(persona.pnombre) +" "+ $.trim(persona.snombre) +" "+ $.trim(persona.papellido) +" "+ $.trim(persona.sapellido));
								$.ajax({
									type: "POST",
									url: URL+"centroReportes/aportes/secretaria/verificaPersonaReporte4.php",
									data: {tipoDoc: tipoDoc, numeroDoc: numeroDoc},
									async: false,
									dataType: "json",
									success:function(resultado){
										if(resultado.persona == null){
											continuar = true;
										}else{
											$.each(resultado.persona,function(c,persona){
												if(resultado.estado == "inactivo")
													continuar = true;
												else if(resultado.estado == "activo"){
													continuar = false;
													alert("La persona se encuentra activa en la base de datos. ("+ resultado.tipo +")");
												}
													
											});
										}
									}
								});
							});
						}

						if(continuar){
							url = URL +'centroReportes/aportes/secretaria/generar.php?modulo=<?php echo $modulo1; ?>&archivo=<?php echo $opcion1; ?>&tipodocumento='+ $("#tipoDoc").val() +'&identificacion='+ $("#numeroDoc").val() +"&nombre="+ $("#nombre").val();
							window.open(url,"_blank");
						}
						
					}
				});
			}});

		}else{
			alert("Verifique el num. de recibo.");
		}
	}

	if(op == 13){
		// certificado trabajador no afiliado
		if($("#comprobante").val() != "" && $("#numComprobante").val() != ""){
			var comprobante = $("#comprobante").val();
			var numComprobante = $("#numComprobante").val();
			var tipoDoc = $("#tipoDoc").val();
			var numeroDoc = $("#numeroDoc").val();
			var continuar = false;
			
			$.ajax({
				type:"POST",
				url: URL+"aportes/empresas/buscarComprobante.php",
				data: {v0:comprobante,v1:numComprobante},
				async:false,
				dataType: "json",
				success:function(data){
				if(data==0){
					alert("No existe el comprobante!");
					return false;
				}

				$.ajax({
					type: "POST",
					url: URL+"phpComunes/buscarPersona2.php",
					data: {v0: tipoDoc, v1: numeroDoc},
					async: false,
					dataType: "json",
					success:function(datos){
						continuar = true;
						if(datos != 0){
							$.each(datos,function(c,persona){
								$("#nombre").val($.trim(persona.pnombre) +" "+ $.trim(persona.snombre) +" "+ $.trim(persona.papellido) +" "+ $.trim(persona.sapellido));
								$.ajax({
									type: "POST",
									url: URL+"centroReportes/aportes/secretaria/verificaPersonaReporte5.php",
									data: {tipoDoc: tipoDoc, numeroDoc: numeroDoc},
									async: false,
									dataType: "json",
									success:function(resultado){
										if(resultado.persona == null){
											continuar = true;
										}else{
											$.each(resultado.persona,function(c,persona){
												if(resultado.estado == "inactivo")
													continuar = true;
												else if(resultado.estado == "activo"){
													continuar = false;
													alert("La persona se encuentra activa en la base de datos. ("+ resultado.tipo +")");
												}
													
											});
										}
									}
								});
							});
						}

						if(continuar){
							url = URL +'centroReportes/aportes/secretaria/generar.php?modulo=<?php echo $modulo1; ?>&archivo=<?php echo $opcion1; ?>&tipodocumento='+ $("#tipoDoc").val() +'&identificacion='+ $("#numeroDoc").val() +"&nombre="+ $("#nombre").val();
							window.open(url,"_blank");
						}
						
					}
				});
			}});

		}else{
			alert("Verifique el num. de recibo.");
		}
	}

	if(op == 14){
		// certificado contratista
		if($("#comprobante").val() != "" && $("#numComprobante").val() != ""){
			var comprobante = $("#comprobante").val();
			var numComprobante = $("#numComprobante").val();
			
			$.ajax({
				type:"POST",
				url: URL+"aportes/empresas/buscarComprobante.php",
				data: {v0:comprobante,v1:numComprobante},
				async:false,
				dataType: "json",
				success:function(data){
					if(data==0){
						alert("No existe el comprobante!");
						return false;
					}
					
					url = URL +'centroReportes/aportes/secretaria/generar.php?modulo=<?php echo $modulo1; ?>&archivo=<?php echo $opcion1; ?>&identificacion='+ data[0].nit;
					window.open(url,"_blank");
				}
			});
		}
	}

	if(op != 9 && op != 8 && op != 10 && op != 12 && op != 13 && op != 14 && op != 30){
		window.open(url,"_blank");
		window.close('configurarReporte.php');
	}
}

function reporteSaldoConso(rptcontrol,op){
	var rpt =rptcontrol;
	var op =op;

	if (rpt == 2){
		var fecha = $("#fechaR").val().trim();
		if( fecha !="" ){

			if(op==1){
			var url;
			url='<?php echo $ruta_reportes;?>gestion/rptExcel01<?php echo @$_REQUEST["rp"];?>.jsp?v0=<?php echo $usuario; ?>&v1='+fecha;
			window.open(url,"_blank");

			}else{
				var url;
				url='<?php echo $ruta_reportes;?>gestion/reporte01<?php echo @$_REQUEST["rp"];?>.jsp?v0=<?php echo $usuario; ?>&v1='+fecha;
				window.open(url,"_blank");

			}
		}
	}else{

		if(op==1){
			var url;
			url='<?php echo $ruta_reportes;?>gestion/rptExcel01<?php echo @$_REQUEST["rp"];?>.jsp?v0=<?php echo $usuario; ?>';
			window.open(url,"_blank");
		}else{
			var url;
			url='<?php echo $ruta_reportes;?>gestion/reporte01<?php echo @$_REQUEST["rp"];?>.jsp?v0=<?php echo $usuario; ?>';
			window.open(url,"_blank");
		}
	}
}


function reporteSaldoCuotaMone(op){
	var op =op;

	
		var fecha = $("#fechaR").val().trim();
		var tipoInf = $("#tipoInf").val().trim();
		
		if( fecha !="" ){

			if(op==1){
				if(tipoInf==1){
					var url;
					url='<?php echo $ruta_reportes;?>gestion/reporte040.jsp?v0=<?php echo $usuario; ?>&v1='+fecha;
					window.open(url,"_blank");
				}else{
					var url;
					url='<?php echo $ruta_reportes;?>gestion/reporte041.jsp?v0=<?php echo $usuario; ?>&v1='+fecha;
					window.open(url,"_blank");

					var url2;
					url2='<?php echo $ruta_reportes;?>gestion/reporte042.jsp?v0=<?php echo $usuario; ?>&v1='+fecha;
					window.open(url2,"_blank");

				}
			
			}else{

				if(tipoInf==1){
					var url;
					url='<?php echo $ruta_reportes;?>gestion/rptExcel040.jsp?v0=<?php echo $usuario; ?>&v1='+fecha;
					window.open(url,"_blank");
				}else{
					var url;
					url='<?php echo $ruta_reportes;?>gestion/rptExcel041.jsp?v0=<?php echo $usuario; ?>&v1='+fecha;
					window.open(url,"_blank");

					var url2;
					url2='<?php echo $ruta_reportes;?>gestion/rptExcel042.jsp?v0=<?php echo $usuario; ?>&v1='+fecha;
					window.open(url2,"_blank");

				}

			}
		}

	}


function reporteHistoricoCategorias(rptcontrol,op){
	var rpt =rptcontrol;
	var op =op;
	var fechaI = $("#fechaI").val().trim();
	var fechaF = $("#fechaF").val().trim();

			if(op==1){
			var url;
			url='<?php echo $ruta_reportes;?>gestion/rptExcel035.jsp?v0=<?php echo $usuario; ?>&v1='+fechaI+'&v2='+fechaF;
			window.open(url,"_blank");

			}else{
				var url;
				url='<?php echo $ruta_reportes;?>gestion/reporte035.jsp?v0=<?php echo $usuario; ?>&v1='+fechaI+'&v2='+fechaF;
				window.open(url,"_blank");

			}
}


function reporteConsolidadoGiro(rptcontrol,op){
	var rpt =rptcontrol;
	var op =op;
	var fechaI = $("#txtPeriodoI").val().trim();
	var agencia = $("#cmbAgencia").val();

			if(op==1){
			var url;
			url='<?php echo $ruta_reportes;?>gestion/rptExcel036.jsp?v0=<?php echo $usuario; ?>&v1='+fechaI+'&v2='+agencia;
			window.open(url,"_blank");

			}else{
				var url;
				url='<?php echo $ruta_reportes;?>gestion/reporte036.jsp?v0=<?php echo $usuario; ?>&v1='+fechaI+'&v2='+agencia;
				window.open(url,"_blank");

			}
}



function reporteVariacionAfiliado(rptcontrol,op){
	var rpt =rptcontrol;
	var op =op;
	var fechaI = $("#fechaI").val().trim();

			if(op==1){
			var url;
			url='<?php echo $ruta_reportes;?>gestion/rptExcel038.jsp?v0=<?php echo $usuario; ?>&v1='+fechaI;
			window.open(url,"_blank");

			}else{
				var url;
				url='<?php echo $ruta_reportes;?>gestion/reporte038.jsp?v0=<?php echo $usuario; ?>&v1='+fechaI;
				window.open(url,"_blank");

			}
}


function reporteEstadoPu(rptcontrol,op){
	var rpt =rptcontrol;
	var op =op;
	var fechaI = $("#fechaI").val().trim();
	var fechaF = $("#fechaF").val().trim();
	var nit = $( "#txtNit" ).val().trim();
	var periodo = $( "#txtPeriodo" ).val().trim();
	
	if (fechaI>fechaF ){alert("Fecha inicial superior a la final");return false;}

			if(op==1){
			var url;
			url='<?php echo $ruta_reportes;?>gestion/rptExcel039.jsp?v0=<?php echo $usuario; ?>&v1='+fechaI+'&v2='+fechaF+'&v3='+nit+'&v4='+periodo;
			window.open(url,"_blank");

			}else{
				var url;
				url='<?php echo $ruta_reportes;?>gestion/reporte039.jsp?v0=<?php echo $usuario; ?>&v1='+fechaI+'&v2='+fechaF+'&v3='+nit+'&v4='+periodo;
				window.open(url,"_blank");

			}
}


function reporteConsolidoRadicaGraba(rptcontrol,op){
	var rpt =rptcontrol;
	var op =op;
	var fechaI = $("#fechaI").val().trim();
	var fechaF = $("#fechaF").val().trim();
	var cmbAgencia37 = $("#cmbAgencia37").val().trim();
	var cmbAgencia37a = $("#cmbAgencia37a").val().trim();

			if(op==1){
			var url;
			url='<?php echo $ruta_reportes;?>gestion/rptExcel037.jsp?v0=<?php echo $usuario; ?>&v1='+fechaI+'&v2='+fechaF+'&v3='+cmbAgencia37+'&v4='+cmbAgencia37a;
			window.open(url,"_blank");

			}else{
				var url;
				url='<?php echo $ruta_reportes;?>gestion/reporte037.jsp?v0=<?php echo $usuario; ?>&v1='+fechaI+'&v2='+fechaF+'&v3='+cmbAgencia37+'&v4='+cmbAgencia37a;
				window.open(url,"_blank");

			}
}



function buscarPersona(tipoDoc, numeroDoc){
	$("#nombre").val('');
	if(tipoDoc != '' && numeroDoc != ''){
		$.ajax({
			type: "POST",
			url: URL+"phpComunes/buscarPersona2.php",
			data: {v0: tipoDoc, v1: numeroDoc},
			async: false,
			dataType: "json",
			success:function(datos){
				if(datos != 0){
					$.each(datos,function(c,persona){
						$("#nombre").val($.trim(persona.pnombre) +" "+ $.trim(persona.snombre) +" "+ $.trim(persona.papellido) +" "+ $.trim(persona.sapellido));
					});
				}else{
					$("#nombre").val('');
					alert("La persona no existe.");
				}
			}
		});
	}
}

$("#cmbAgencia").change(function(){
	var idAgencia = $("#cmbAgencia").val();
	if(idAgencia!=""){
		var combo = "";
		$.ajax({
			type: "POST",
			url: URL+"phpComunes/buscarUsuario.php",
			data: {v0: idAgencia},
			async: false,
			dataType: "json",
			success:function(datos){
				if(datos != 0){
					$.each(datos,function(c,data){
						combo +="<option value='"+data.idusuario+"-"+data.nombres+"'>"+data.nombres+"</option>";
					});
				}else{
					alert("No hay usuarios para la agencia.");
					combo="";
				}
			}
		});
		$("#cmbUsuario").html(combo);
	} else {
		$("#cmbUsuario").html("<option value=''>Debe seleccionar una agencia</option>");
	}
});

<?php if( $tipo1 == 34 && $plano1 != 3){ ?>
$("#cmbAgencia2").change(function(){
	var idAgencia = $("#cmbAgencia2").val();
	if(idAgencia!="0"){
		var combo = "";
		$.ajax({
			type: "POST",
			url: URL+"phpComunes/buscarUsuario.php",
			data: {v0: idAgencia},
			async: false,
			dataType: "json",
			success:function(datos){
				if(datos != 0){
					$.each(datos,function(c,data){
						combo +="<option value='"+data.idusuario+"-"+data.nombres+"'>"+data.nombres+"</option>";
					});
				}else{
					alert("No hay usuarios para la agencia.");
					combo="";
				}
			}
		});
		$("#cmbUsuario2").html(combo);
	} else {
		$("#cmbUsuario2").html("<option value='0'>TODOS</option>");
	}
});
<?php } ?>

</script>
<input type="hidden" id="txtFecha" name="txtFecha" value="<?php echo $fechaHoy; ?>" /> 
</html>