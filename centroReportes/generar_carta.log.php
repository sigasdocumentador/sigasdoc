<?php
set_time_limit(0);
date_default_timezone_set("America/Bogota");
ini_set('memory_limit', '1000M');

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
include_once $raiz.DIRECTORY_SEPARATOR.'phpComunes'.DIRECTORY_SEPARATOR.'exportar_pdf'.DIRECTORY_SEPARATOR.'util.class.php';
include_once $raiz.DIRECTORY_SEPARATOR.'aportes'.DIRECTORY_SEPARATOR.'empresas'.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'empresas.class.php';
include_once $raiz.DIRECTORY_SEPARATOR.'centroReportes'.DIRECTORY_SEPARATOR.'promotoria'.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'carta_liqui_aforo.class.php';
include_once $raiz.DIRECTORY_SEPARATOR.'centroReportes'.DIRECTORY_SEPARATOR.'promotoria'.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'carta_conve_pago.class.php';
include_once $raiz.DIRECTORY_SEPARATOR.'centroReportes'.DIRECTORY_SEPARATOR.'promotoria'.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'carta_cobro_persu.class.php';

$objEmpresa = new Empresa();

$arrDatosCarta = array();
$codigoCarta = $_REQUEST["codigo_carta"];
$arrFiltro = isset($_REQUEST["filtro"])?$_REQUEST["filtro"]:array();
$usuario = $_SESSION["USUARIO"];

$contenidoHtml = "";
switch ($codigoCarta){
	case "CARTA_LIQUI_AFORO":
		
		$arrFiltro["comunicacion"]=isset($_REQUEST["v0"])?$_REQUEST["v0"]:"";
		$arrFiltro["fecha_inicial"] = isset($_REQUEST["v1"])?$_REQUEST["v1"]:"";
		$arrFiltro["fecha_final"]= isset($_REQUEST["v2"])?$_REQUEST["v2"]:"";
		$arrFiltro["id_visita"] = isset($_REQUEST["v3"])?$_REQUEST["v3"]:"";
		$arrFiltro["nit"] = isset($_REQUEST["v4"])?$_REQUEST["v4"]:"";
		$arrFiltro["usuario"] = isset($_REQUEST["v5"])?$_REQUEST["v5"]:"";
		$arrFiltro["id_agencia"] = isset($_REQUEST["v6"])?$_REQUEST["v6"]:"";

		$objCartaLiquiAforo = new CartaLiquiAforo();
		
		$arrDatosCarta = $objCartaLiquiAforo->fetch_carta($arrFiltro);
		
		break;
	
	case "CARTA_CONVE_PAGO":
		
		$arrFiltro["nit"]=isset($_REQUEST["v0"])?$_REQUEST["v0"]:"";
		$arrFiltro["fecha_convenio_i"] = isset($_REQUEST["v2"])?$_REQUEST["v2"]:"";
		$arrFiltro["fecha_convenio_f"]= isset($_REQUEST["v3"])?$_REQUEST["v3"]:"";
		$arrFiltro["id_agencia"] = isset($_REQUEST["v4"])?$_REQUEST["v4"]:"";
			
		$objCartaConvePago = new CartaConvePago();
		
		$arrDatosCarta = $objCartaConvePago->fetch_carta($arrFiltro);
	
		break;
	

	case "CARTA_COBRO_PERSU":

		$arrFiltro["id_visita"]=isset($_REQUEST["v0"])?$_REQUEST["v0"]:"";
		$arrFiltro["fechaI"] = isset($_REQUEST["v1"])?$_REQUEST["v1"]:"";
		$arrFiltro["fechaF"]= isset($_REQUEST["v2"])?$_REQUEST["v2"]:"";
		$arrFiltro["comunicacion"]= isset($_REQUEST["v3"])?$_REQUEST["v3"]:"";
		$arrFiltro["numero_reitero"] = isset($_REQUEST["v4"])?$_REQUEST["v4"]:"";
		$arrFiltro["nit"] = isset($_REQUEST["v5"])?$_REQUEST["v5"]:"";
		$arrFiltro["usuario"] = isset($_REQUEST["v6"])?$_REQUEST["v6"]:"";
		$arrFiltro["id_agencia"] = isset($_REQUEST["v7"])?$_REQUEST["v7"]:"";
		
		$objCartaCobroPersu = new CartaCobroPersu();
		
		$arrDatosCarta = $objCartaCobroPersu->fetch_carta($arrFiltro);
		
		break;
	
	case "GENERAR_NOTIFICACION_PDF":
		
		$idNotificacion = $_REQUEST["id_notificacion"];
		$datoNotificacion = $objEmpresa->buscar_notificacion(array("id_notificacion"=>$idNotificacion));
		
		if(count($datoNotificacion)>0){
			$contenidoHtml = $datoNotificacion[0]["contenido_carta"];
		}else{
			$contenidoHtml = "<b>ERROR AL GENERAR LA CARTA</b>";
		}
		
		break;
		
}

if($codigoCarta!="GENERAR_NOTIFICACION_PDF"){
	$objEmpresa->inicioTransaccion();
	$banderaError = 0;
	
	if(count($arrDatosCarta)>0){
		//Guardar control de notificaciones
		foreach($arrDatosCarta as $carta){
			$contenidoHtml .= $carta["contenido_carta"];
			
			if($objEmpresa->guardar_notificacion($carta,$usuario)==0){
				$banderaError++;
				break;
			}
		}
		
		//Control de error
		if($banderaError==0){
			//Ok
			$objEmpresa->confirmarTransaccion();
		}else{
			//Error
			$objEmpresa->cancelarTransaccion();
			$contenidoHtml = "<b>ERROR AL GENERAR LAS CARTAS</b>";
		}	
	}else{
		$contenidoHtml = "<b>NO SE ENCONTRARON DATOS</b>";
	}
	
}

//Generar las cartas en formato pdf
Util::archivoPDF($contenidoHtml);


?>