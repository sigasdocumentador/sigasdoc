<?php 
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

if ( !isset( $_SESSION ['USUARIO'] ) ) {session_start();}
$raiz = $_SESSION ['RAIZ'];
chdir ( $raiz );
include_once("config.php");
include_once ("clases/p.definiciones.class.php");
$objDefiniciones = new Definiciones();
$resultConvenios = $objDefiniciones->mostrar_datos(25);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>::Reporte de pignoraciones por convenio::</title>
<link type="text/css" href="../../css/Estilos.css" rel="stylesheet"/>
<link type="text/css" href="../../css/formularios/base/ui.all.css" rel="stylesheet" />
<script src="../../js/jquery-1.3.2.min.js"></script>
<script src="../../js/formularios/ui/ui.datepicker.js"></script>
<script language="javascript" src="../../js/comunes.js"></script>
<script src="js/reporte.js" type="text/javascript"></script>
<script type="text/javascript">
	$(function() {
		$('#txtFInicial').datepicker({
			changeMonth: true,
			changeYear: true
 });
 		$('#txtFFinal').datepicker({
			changeMonth: true,
			changeYear: true
 });
 });
</script>
</head>
<body>
<table width="100%" border="0">
  <tr>
    <td align="center" ><img src="../../imagenes/logo_reporte.png" /></td>
  </tr>
  <tr>
    <td align="center" ><strong class="titulo"><br />Configuraci&oacute;n Reporte Pagares</strong></td>
  </tr>
</table>
<form name="frm1" id="frm1" action="<?php echo URL_PORTAL.DIRECTORIO_CENTRO_REPORTES ."pignoracion/reporte002.php"; ?>" method="post" >
<center>
<br />
<table width="25%" border="0" >
<tr>
<th colspan="2"></th>
</tr>
<tr>
<td>Convenio:&nbsp;</td>
<td><select name="idconvenio" class="boxlargo" id="idconvenio">
  <?php while($convenio = mssql_fetch_array($resultConvenios)): ?>
  <option value="<?php echo $convenio["iddetalledef"]; ?>"><?php echo $convenio["detalledefinicion"]; ?></option>
  <?php endwhile; ?>
</select></td>
</tr>
<tr>
<td>Parametro:&nbsp;</td>
<td><select name="parametro" class="boxlargo" id="parametro" onchange="mostrar(this.value);">
  <option value="1" selected="selected">Todos</option>
  <option value="2">Por Agencia</option>
  <option value="3">Por Fecha</option>
  </select></td>
</tr>
</table>
<div id="porFecha" style="display:none">
<table width="25%" border="0" >
<tr><td>Fecha Inicial</td><td><input type="text" name="txtFInicial" id="txtFInicial" /></td></tr>
<tr><td>Fecha Inicial</td><td><input type="text" name="txtFFinal" id="txtFFinal" /></td></tr>
</table>
</div>
<div id="porAgencia" style="display:none">
<table width="25%" border="0" >
<tr><td>Agencia</td><td>
<select id="agencia" name="agencia" class="box1">
<option value="01" selected="selected">Neiva</option>
<option value="02">Garz&oacute;n</option>
<option value="03">Pitalito</option>
<option value="04">La Plata</option>
</select>
</td></tr>

</table>
</div>
<table width="25%" border="0" >
<tr>
<td colspan="2" align="center"><label style="cursor:pointer" onclick="procesar('<?php echo URL_REPORTES_JSP; ?>');">
<img src="../../imagenes/procesar2.png" /></label>
</td>
</tr>
</table>
</center>
</form>
<?php
  $usuario=$_SESSION['USUARIO'];
?>
<input type="hidden" id="txtusuario" name="txtusuario" value="<?php echo $usuario; ?>" /> 
</body>
</html>