<?php
	$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
	include_once  $root;
	
	$usuario=$_SESSION['USUARIO'];
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
	include_once $raiz.'/config.php';
	$db = IFXDbManejador::conectarDB();
	if($db->conexionID==null){
		$cadena = $db->error;
		echo msg_error($cadena);
		exit();
	}
	$titulo = "Informes de inconsistencias";
?>

<html>
	<head>
		<title>Men&uacute; Reportes</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link type="text/css" rel="stylesheet" href="<?php echo URL_PORTAL; ?>css/css.1.8/jquery-ui-1.8.17.custom.css" />
		<link type="text/css" href="<?php echo URL_PORTAL; ?>css/Estilos.css" rel="stylesheet"/>
		<link href="<?php echo URL_PORTAL; ?>css/marco.css" rel="stylesheet" type="text/css" />

		<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/1.6/jquery-1.6.2.min.js"></script>
		<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/1.6/jquery-ui-1.8.16.custom.min.js"></script>
		<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/comunes.js"></script>
		<script type="text/javascript" src="js/reporteInconsistencia.js"></script>
	</head>
	<body style="text-align: center;">
		<table width="100%" border="0">
			<tr>
				<td align="center" ><img src="<?php echo URL_PORTAL; ?>/imagenes/logo_reporte.png" width="362" height="70"></td>
			</tr>
			<tr>
				<td align="center" ><?php echo $titulo ?></td>
			</tr>
		</table>
		<div id="tipo1" style="display:block">
			<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
				<tr>
			    	<th colspan="4"><strong>Parametros de Configuracion Reporte</strong></th>
			    </tr>
    			<tr>
      				<td style="text-align: center;" >
      					Informe:
      					<select id="cmbInforme" name="cmbInforme" class="box1" onchange="controlChange();">
      						<option value="0">Seleccione...</option>
      						<option value="EmpreSinCiiu">Empresas sin codigo CIIU</option>
      						<option value="EmpreSinSecci">Empresas sin seccional</option>
      						<option value="EmpreTipoAfiliIncoh">Empresa con tipo afiliaci&oacute;n incoherente</option>
      						<option value="EmpreDiferEmpleYConTraba">Empresa diferente a empleador y con mas de un trabajadro</option>
      						<option value="EmpreSinSecto">Empresas sin sector</option>      						
      						<option value="FechaNacimNull">Personas sin fecha nacimiento</option>
      						<option value="PersoSinSexo">Personas sin genero</option>
      						<option value="RelacBenefHuerf">Relaciones de Beneficiarios huerfanas</option>
      						<option value="RelacBenefSinParen">Relaciones de Beneficiarios sin parentesco</option>
      						<option value="ConyuDobleConvi">Conyuges en doble convivencia</option>
      						<option value="TrabaConviMasUnConyu">Trabajadores que conviven con mas de un conyuge</option>      						
      						<option value="AfiliacionesHuerfanas">Afiliaciones huerfanas</option>      						
      						<option value="TrabaTipoAfiliIncoh">Trabajadores con tipo afiliaci&oacute;n incoherente</option>
      					</select>
     				</td>
    			</tr>
    			<tr class="clsCampos tr0">
			    	<td>&nbsp;</td>
			    </tr>
    			<!-- Descripcion -->
    			<tr style="height: 30px;" >
      				<td style="text-align: center; ">
      					<p style="text-align:center; font-size: 12px; font-weight: bold;">Descripci&oacute;n</p>
      					<hr width="10%"/>
      					<p style="text-align:justify; font-size: 12px; width: 80%; margin: 0px auto;" align="center" id="pDescripcion"></p>
      					<br/>
      				</td>
    			</tr>
    			<!-- BOTTON -->
    			<tr >
    	  			<td colspan="4" style="text-align:center;height: 40px; cursor: pointer;" id="tdBoton"><img onclick="generarPlano();" src="<?php echo URL_PORTAL; ?>imagenes/procesar2.png"></td>
    			</tr>
    		</table>
    	</div>
	</body>
</html>