<?php
/* autor:       orlando puentes
 * fecha:       28/09/2010
 * objetivo:    
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$usuario=$_SESSION['USUARIO'];
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Men&uacute; Reportes de subsidio cuota monetaria</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="../../css/estiloReporte.css" rel="stylesheet" type="text/css">
<script src="js/reportes.js" type="text/javascript"></script>
</head>
<body>
<center>
  <table width="100%" border="0">
  <tr>
    <td align="center" ><img src="../../imagenes/razonSocial.png" width="615" height="55"></td>
  </tr>
  <tr>
    <td align="center" >&nbsp;</td>
  </tr>
  <tr>
    <td class="fecha" ></td>
  </tr>
</table>
<table width="60%" border="0" cellspacing="1" class="tablero">
	<tr><th width="20%">Reporte</th>
	<th>Listado de Reportes de Tarjeta ASOPAGOS</th></tr>  
	  <tr>
	    <td scope="row" align="left">Reporte001</td>
      <td colspan="14" width="845"><a href="configReporEstad.php?tipo=1&tit=1"> VARIABLES TRAZADORAS DEL SISTEMA DEL SUBSIDIO FAMILIAR</a></td>
	  </tr>
    <tr>
      <td scope="row" align="left">Reporte002</td>
      <td colspan="14" width="845"><a href="configReporEstad.php?tipo=2&tit=2">AFILIADOS SEG&Uacute;N ACTIVIDAD ECON&Iacute;MICA DE LAS EMPRESAS</label></td>
    </tr>
    <tr>
      <td scope="row" align="left">Reporte003</td>
      <td colspan="14" width="845"><a href="configReporEstad.php?tipo=3&tit=3">AFILIADOS POR TIPO DE SECTOR DE LAS EMPRESAS</label></td>
    </tr>
    <tr>
      <td scope="row" align="left">Reporte004</td>
      <td colspan="14" width="845"><a href="configReporEstad.php?tipo=4&tit=4">AFILIADOS SEG&Uacute;N TAMA&Ntilde;O DE LAS EMPRESAS</label></td>
    </tr>
    <tr>
      <td scope="row" align="left">Reporte005</td>
      <td colspan="14" width="845"><a href="configReporEstad.php?tipo=5&tit=5">AFILIADOS POR NIVEL DE INGRESO</label></td>
    </tr>
    <tr>
      <td scope="row" align="left">Reporte006</td>
      <td colspan="14" width="845"><a href="configReporEstad.php?tipo=6&tit=6">AFILIADOS POR RANGO DE EDAD</label></td>
    </tr>
    <tr>
      <td scope="row" align="left">Reporte007</td>
      <td colspan="14" width="845"><a href="configReporEstad.php?tipo=7&tit=7">PERSONAS A CARGO POR RANGOS DE EDAD</label></td>
    </tr>
    <tr>
        <td>Informes</td>
        <td><a href="menuInconsistencias.php" target="_new">Informes de inconsistencias</a></td>
    </tr>
    <tr>
        <td>Reporte009</td>
        <td><a href="../aportes/empresas/configReporteEmpresa.php?tipo=9&tit=9" target="_new">Listado aportes por empresa</a></td>
    </tr>
</table>
</center>
</body>

</html>

