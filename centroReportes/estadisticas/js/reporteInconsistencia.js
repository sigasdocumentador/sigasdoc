var objDescripcion = {
		TrabaConviMasUnConyu: "Informe de trabajadores que conviven con mas de un conyuge.",
		EmpreSinCiiu: "Informe de empresas sin codigo CIIU.",
		FechaNacimNull: "Personas con fecha nacimiento NULL.",
		EmpreSinSecci: "Empresas sin seccional.",
		RelacBenefHuerf: "Relaciones de beneficiarios huerfanas.",
		ConyuDobleConvi: "Conyuges en doble convivencia",
		EmpreTipoAfiliIncoh: "Empresa con tipo afiliaci&oacute;n incoherente referente a la clase de aportante",
		EmpreDiferEmpleYConTraba: "Empresa diferente a empleador y con mas de un trabajador",
		PersoSinSexo: "Personas sin sexo",
		AfiliacionesHuerfanas: "Afiliaciones huerfanas",
		EmpreSinSecto: "Empresas sin sector",
		TrabaTipoAfiliIncoh: "Trabajadores con tipo afiliaci&oacute;n incoherente con el de la empresa",
		RelacBenefSinParen: "Relaciones de Beneficiarios sin parentesco"
		
};

function controlChange(){
	var option = $("#cmbInforme").val();
	var idTr = ".tr" + option;
	$(".clsCampos").hide();
	$("input").val("");
	$(idTr).show();
	$("#pDescripcion").html("").html(objDescripcion[option]);
}

function generarPlano(){
	var caso = $("#cmbInforme").val(),
		datos = false;
	$(".ui-state-error").removeClass("ui-state-error");
	if( caso == 0 ){$("#cmbInforme").addClass("ui-state-error");return false;}
	switch( caso ){
		case "TrabaConviMasUnConyu": case "EmpreSinCiiu": case "FechaNacimNull": case "EmpreSinSecci":
		case "RelacBenefHuerf": case "ConyuDobleConvi": case "EmpreTipoAfiliIncoh": case "EmpreDiferEmpleYConTraba":
		case "PersoSinSexo": case "AfiliacionesHuerfanas": case "EmpreSinSecto": case "TrabaTipoAfiliIncoh":
		case "RelacBenefSinParen":
			var data = {caso:caso};
			datos = enviarPeticion(data);
		break;
	}
	
	if( datos ){
		$("#tdBoton").html("<img src='" + URL + "imagenes/descargarArchivo.png' style='cursor:pinter' onclick='descargar()'/>");
	}else{
		$("#tdBoton").html$("#tdBoton").html("<img src='" + URL + "imagenes/descargarArchivo.png' style='cursor:pinter' onclick='descargar()'/>");( "Error al generar el archivo" );
	}
}

/*data:{caso:caso,parametros}*/
function enviarPeticion(data){
	var datosRetorno = false;
	$.ajax({
		url:"reporteInconsistencia.php",
		type:"POST",
		data:data,
		dataType:"json",
		async:false,
		beforeSend: function(objeto){
			$("#tdBoton").html("<img src='" + URL + "imagenes/ajax-loader-fon.gif'/>");
        },        
        complete: function(objeto, exito){
        	$("#tdBoton").html("");            
        },
		success:function(datos){
			if( parseInt( datos ) === 1 ){
				datosRetorno = true;
			}else{
				datosRetorno = false;
			}
		}
	});
	return datosRetorno;
}

function descargar(){
	var url=URL+'phpComunes/descargaPlanos.php';
	window.open(url);
	$("#cmbInforme").val(0).trigger("change");
	$("#tdBoton").html("<img src='" + URL + "imagenes/procesar2.png' style='cursor:pinter' onclick='generarPlano();'/>");
}