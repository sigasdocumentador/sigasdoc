<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
include_once $raiz . DIRECTORY_SEPARATOR . 'promotoria' . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'promotoria.class.php';

$usuario=$_SESSION['USUARIO'];
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
include_once $raiz.DIRECTORY_SEPARATOR.'seguridad'.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'agencia.php';
include_once $raiz. DIRECTORY_SEPARATOR .'clases' . DIRECTORY_SEPARATOR . 'p.definiciones.class.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

//obtener los datos del usuario
$sql = "SELECT a15.sexo
		FROM aportes519 a519
			INNER JOIN aportes015 a15 ON a15.identificacion=a519.identificacion
		WHERE a519.usuario='$usuario'";
$rsDatoUsuario = $db->querySimple($sql);
$sexoUsuario = $rsDatoUsuario->fetchObject()->sexo;

$tipo=$_REQUEST['tipo'];
$titulo='...';

$rept=isset($_REQUEST['tit'])?$_REQUEST['tit']:0;
switch($rept){
	case 1:$titulo='Reporte de visitas por empresa'; break;
	case 2:$titulo='Reporte de liquidaciones por empresa'; break;
	case 3:$titulo='Reporte Promotor&iacute;a'; break;
	case 4:$titulo='Reporte de la auditoria del periodo liquidado'; break;
	case 5:$titulo='Carta liquidaci&oacute;n de aforo'; break;
	case 6:$titulo='';break;
	case 7:$titulo='Carta de los convenios de pago de aportes parafiscales';break;
	case 8:$titulo='Informe convenios de pago de aportes parafiscales';break;
	case 9:$titulo='Informe general de promotoria';break;
	case 10:$titulo='Informe discriminado de las liquidaciones';break;
	case 11:$titulo='Informe del historial de los seguimientos de la visita';break;
	case 12:$titulo='Reporte de los ajustes al periodo liquidado';break;
	case 13:$titulo='Carta de cobro persuasivo.';break;
	case 14:$titulo='Informe notificaci&oacute;n de liquidaci&oacute;n de aforo que no han pagado';break;
	case 15:$titulo='Reporte consolidado del historial de los abonos del periodo liquidado';break;
	case 16:$titulo='Reporte discriminado del historial de los abonos del periodo liquidado';break;
	case 17:$titulo='Reporte cierre promotoria';break;
	case 18:$titulo='Reporte consolidado del cierre promotoria';break;
}

$objPro=new Promotoria;
$objDefiniciones = new Definiciones();

//Obtener los promotores
$optionPromotor = "";
//if($tipo==3 || $tipo==10 || $tipo==12){
$consulta=$objPro->buscar_promotor_visita(null);
foreach( $consulta as  $row ) {
	$nombre=trim($row['pnombre'])." ".trim($row['snombre'])." ".trim($row['papellido'])." ".trim($row['sapellido']);
	$optionPromotor .= "<option value=".$row['idpromotor'].">".$nombre."</option>";
}
//}

//Obtener los asesores juridicos
$optionAsesorJuridI = "";
$optionAsesorJuridE = "";
if($tipo==11){
	$datosAsesores=$objPro->buscar_asesor(null);
	foreach ($datosAsesores as $row) {
		//[4225]ASESOR JURIDICO INTERNO
		if($row['idtipoasesor']==4225){
			$optionAsesorJuridI .= "<option value=".$row['idasesor'].">".$row['asesor']."</option>";
		
		//[4226]ASESOR JURIDICO EXTERNO
		}else if($row['idtipoasesor']==4226)
			$optionAsesorJuridE .= "<option value=".$row['idasesor'].">".$row['asesor']."</option>";
	}
}

$comboAgencia = "";
//Obtener el combo de las agencias
$datosAgencias = Agencia::listarTodas();
foreach ($datosAgencias as $rowAgencia){
	$comboAgencia .= "<option value=".$rowAgencia->getIdAgencia().">".$rowAgencia->getAgencia()."</option>";
}

//Obtener las causales de la visita
$comboCausalVisita = "";
$datosCausalVisita = $objDefiniciones->buscar_detalle_definicion(array("iddefinicion"=>44)); // [44][CAUSALES MOROSIDAD]
foreach ($datosCausalVisita as $row){
	$comboCausalVisita .= "<option value=".$row["iddetalledef"].">".$row["detalledefinicion"]."</option>";
}

//Obtener el combo de la causales del ajuste
$comboCausalAjuste = "";
$datosCausalAjuste=$objPro->buscar_causal_ajuste( array() );
foreach ($datosCausalAjuste as $row){
	$comboCausalAjuste .= "<option value=".$row["id_causal_ajuste"].">".$row["nombre"]."</option>";
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<title>Men&uacute; Reportes Promotoria</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link type="text/css" rel="stylesheet" href="../../css/css.1.8/jquery-ui-1.8.17.custom.css" />
		<link type="text/css" href="../../css/Estilos.css" rel="stylesheet"/>
		<link href="../../css/marco.css" rel="stylesheet" type="text/css" />
		
		<script type="text/javascript" src="../../js/1.6/jquery-1.6.2.min.js"></script>
		<script type="text/javascript" src="../../js/1.6/jquery-ui-1.8.16.custom.min.js"></script>
		<script type="text/javascript" src="../../js/comunes.js"></script>
		<script type="text/javascript" src="js/reporte.js"></script>
	</head>
	<body>
		<center>
			<table width="100%" border="0">
				<tr>
					<td align="center" ><img src="../../imagenes/logo_reporte.png" width="362" height="70"></td>
				</tr>
				<tr>
					<td align="center" ><?php echo $titulo ?></td>
				</tr>
			</table>
		<br>
		<?php
			if($tipo==1){
		?>
		<div id="tipo1" style="display:block">
			<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
				<tr>
			    	<th colspan="2"><strong>Parametros de Configuracion Reporte</strong></th>
			    </tr>
			    <tr>
			      	<td  style="text-align: right;">Ingresar Nit: </td>
			      	<td width="50%">
			      		<input name="txtNit" type="text" class="box" id="txtNit"/>
			      		<span class="clsObligatorio">&nbsp;&nbsp;&nbsp;</span>
			      	</td>
			    </tr>
			    <tr>
		      		<td colspan="2">&nbsp;</td>
			  	</tr>
			    <tr>
			      <td colspan="2" style="text-align:center"><label style="cursor:pointer" onClick="reporte001();"><img src="../../imagenes/icono_pdf.png" width="32" height="32"></label></td>
			    </tr>
	    	</table>
	    </div>
	 	<?php
			}
			if($tipo==2){
		?>
		<div id="tipo1" style="display:block">
			<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
				<tr>
			    	<th colspan="2"><strong>Parametros de Configuracion Reporte</strong></th>
			    </tr>
			    <tr>
			      	<td width="50%" style="text-align: right;">Ingresar Nit: </td>
			      	<td>
			      		<input name="txtNit" type="text" class="box" id="txtNit" />
			      		<span class="clsObligatorio">&nbsp;&nbsp;&nbsp;</span>
			      	</td>
			    </tr>
			    <tr>
		      		<td colspan="2">&nbsp;</td>
			  	</tr>
			    <tr>
		      		<td colspan="2" style="text-align:center"><label style="cursor:pointer" onClick="reporte002();"><img src="../../imagenes/icono_pdf.png" width="32" height="32"></label></td>
			    </tr>
	    	</table>
	    </div>
	 	<?php
			}
			if($tipo==3){
		?>
		<div id="tipo1" style="display:block">
			<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
				<tr>
			    	<th colspan="2"><strong>Parametros de Configuracion Reporte</strong></th>
			    </tr>
			    <tr>
				    <td width="50%" style="text-align: right;">Promotor: </td>
				    <td>
		    			<select name="cmbPromotor" class="box1" id="cmbPromotor">
				        	<option value="0">Seleccione</option>
				        	<?php
								echo $optionPromotor;
							?>
			      		</select>
				    </td>
			    </tr>
			    <tr>
				    <td style="text-align: right;">Fecha Inicial: </td>
				    <td>
				    	<input name="txtFechaInicial" type="text" class="box1" id="txtFechaInicial" readonly="readonly"/>
				    </td>
			    </tr>
			    <tr>
				    <td style="text-align: right;">Fecha Final: </td>
				    <td>
				    	<input name="txtFechaFinal" type="text" class="box1" id="txtFechaFinal" readonly="readonly"/>
				    </td>
			    </tr>
			    <tr>
			    	<td width="50%" style="text-align: right;">Agencia: </td>
			    	<td>
			    		<select name="cmbIdAgencia" id="cmbIdAgencia" class="box1">
			    			<option value="">Seleccione</option>
			    			<?php
			    				echo $comboAgencia;
			    			?>
			    		</select>
			    	</td>
			    </tr>
			    <tr>
			    	<td width="50%" style="text-align: right;">Causal Visitas:</td>
				    <td >
		    			<select name="cmbCausalVisita" class="boxfecha" id="cmbCausalVisita">
				        	<option value="">Seleccione</option>
		    				<?php 
				        		echo $comboCausalVisita;
				        	?>
			      		</select>
				    </td>
			    </tr>
			    <tr>
		      		<td colspan="2">&nbsp;</td>
			  	</tr>
			    <tr>
					<td colspan="2" style="text-align:center">
						<label style="cursor:pointer" onClick="reporte003('PDF');"><img src="../../imagenes/icono_pdf.png" width="32" height="32"></label>
		      			<label style="cursor:pointer" onClick="reporte003('EXCEL');"><img src="../../imagenes/icono_excel.png" width="32" height="32"></label>
					</td>
			    </tr>
	    	</table>
	    </div>
	   <?php
			}
			if($tipo==4){
		?>
		<div style="display:block">
			<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
				<tr>
			    	<th colspan="2"><strong>Parametros de Configuracion Reporte</strong></th>
			    </tr>
			     <tr >
				    <td style="text-align: center;">
				    	N&uacute;mero Visita:
				    	<input type="text" name="txtNumeroVisita" id="txtNumeroVisita" class="boxfecha"/>
				    </td>
			    </tr>
			    <tr>
				    <td style="text-align: center;">
				    	Visitas a paz y salvo:
		    			<select name="cmbPazYSalvo" class="boxfecha" id="cmbPazYSalvo">
				        	<option value="">Todo</option>
				        	<option value="A">No</option>
				        	<option value="P">Si</option>
			      		</select>
				    </td>
			    </tr>
			    <tr>
					<td style="text-align: center;">
						Nit:
			    		<input type="text" name="txtNit" id="txtNit" class="boxfecha"/>
			      	</td>
			    </tr>
			    <tr >
					<td style="text-align: center;">
						Promotor:
			    		<select name="cmbPromotor" class="boxlargo" id="cmbPromotor">
				        	<option value="">Seleccione</option>
				        	<?php echo $optionPromotor;?>
			      		</select>
			      	</td>
			    </tr>
			    <tr>
				    <td style="text-align: center;">
				    	Fecha visita
				    </td>
			    </tr>
			    <tr >
				    <td style="text-align: center;">
				    	Inicial: 
				    	<input name="txtFechaInicial" type="text" class="boxfecha" id="txtFechaInicial" readonly="readonly"/>
				    	Final: 
				    	<input name="txtFechaFinal" type="text" class="boxfecha" id="txtFechaFinal" readonly="readonly"/>
				    </td>
			    </tr>
			    <tr>
				    <td style="text-align: center;">
				    	Causal Visitas:
		    			<select name="cmbCausalVisita" class="boxfecha" id="cmbCausalVisita">
				        	<option value="">Seleccione</option>
		    				<?php 
				        		echo $comboCausalVisita;
				        	?>
			      		</select>
				    </td>
			    </tr>
			    <tr>
			    	<td style="text-align: center;">
			    		Agencia
			    		<select name="cmbIdAgencia" id="cmbIdAgencia" class="boxfecha">
			    			<option value="">Seleccione</option>
			    			<?php
			    				echo $comboAgencia;
			    			?>
			    		</select>
			    	</td>
			    </tr>
			    <tr>
		      		<td colspan="2">&nbsp;</td>
			  	</tr>
			    <tr>
		      		<td colspan="2" style="text-align:center">
		      			<label style="cursor:pointer" onClick="reporte004('PDF');"><img src="../../imagenes/icono_pdf.png" width="32" height="32"></label>
		      			<label style="cursor:pointer" onClick="reporte004('EXCEL');"><img src="../../imagenes/icono_excel.png" width="32" height="32"></label>
		      		</td>
			    </tr>
	    	</table>
	    </div>
		<?php
			}
			if($tipo==5){
		?>
		<div style="display:block">
			<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
				<tr>
			    	<th colspan="2"><strong>Parametros de Configuracion Reporte</strong></th>
			    </tr>
			    <tr>
				    <td width="50%" style="text-align: center;">
				    	Comunicaci&oacute;n:
				    	<input type="text" name="txtComunicacion" id="txtComunicacion" class="box1"/>
				    	<input type="hidden" name="hidIdCarta" id="hidIdCarta" value="4250" />
				    	<span class="clsObligatorio">&nbsp;&nbsp;&nbsp;</span>
				    </td>
			    </tr>
			    <!-- <tr>
				    <td style="text-align: center;">
				    	Por:
				    	<select name="cmbFiltro" id="cmbFiltro" onchange="tdFiltroReporte005()" class="box1">
				    		<option value="VISITA">Visita</option>
				    		<option value="FECHAS">Fecha</option>
				    	</select> 
				    </td>
			    </tr>-->
			    <tr >
				    <td style="text-align: center;">
				    	Fecha Inicial: 
				    	<input name="txtFechaInicial" type="text" class="box1" id="txtFechaInicial" readonly="readonly"/>
				    	Fecha Final: 
				    	<input name="txtFechaFinal" type="text" class="box1" id="txtFechaFinal" readonly="readonly"/>
				    </td>
			    </tr>
			    <tr>
				    <td width="50%" style="text-align: center;">
				    	Nit:
				    	<input type="text" name="txtNit" id="txtNit" class="box1"/>
				    </td>
			    </tr>
			    <tr>
				    <td width="50%" style="text-align: center;">
				    	Visita:
				    	<input type="text" name="txtVisita" id="txtVisita" class="box1"/>
				    </td>
			    </tr>
			    <tr>
			    	<td style="text-align: center;">
			    		Agencia
			    		<select name="cmbIdAgencia" id="cmbIdAgencia" class="box1">
			    			<option value="">Seleccione</option>
			    			<?php
			    				echo $comboAgencia;
			    			?>
			    		</select>
			    	</td>
			    </tr>
			    <tr>
		      		<td colspan="2">&nbsp;</td>
			  	</tr>
			    <tr>
		      		<td colspan="2" style="text-align:center">
		      			<label style="cursor:pointer" onClick="reporte005();"><img src="../../imagenes/icono_pdf.png" width="32" height="32"></label>
		      		</td>
			    </tr>
	    	</table>
	    </div>
	 	<?php
			}
			if($tipo==6){
		?>
		<!-- <div style="display:block">
			<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
				<tr>
			    	<th colspan="2"><strong>Parametros de Configuracion Reporte</strong></th>
			    </tr>
			    <tr >
				    <td style="text-align: center;">
				    	Fecha Pago
				    </td>
			    </tr>
			    <tr >
				    <td style="text-align: center;">
				    	Inicial: 
				    	<input name="txtFechaInicial" type="text" class="box1" id="txtFechaInicial" readonly="readonly"/>
				    	<span class="clsObligatorio">&nbsp;&nbsp;&nbsp;</span>
				    	Final: 
				    	<input name="txtFechaFinal" type="text" class="box1" id="txtFechaFinal" readonly="readonly"/>
				    	<span class="clsObligatorio">&nbsp;&nbsp;&nbsp;</span>
				    </td>
			    </tr>
			    <tr>
				    <td width="50%" style="text-align: center;">
				    	Visitas a paz y salvo: 
		    			<select name="cmbPazYSalvo" class="box1" id="cmbPazYSalvo">
				        	<option value="P">Si</option>
				        	<option value="A">No</option>
			      		</select>
				    </td>
			    </tr>
			    <tr>
		      		<td colspan="2">&nbsp;</td>
			  	</tr>
			    <tr>
		      		<td colspan="2" style="text-align:center">
		      			<label style="cursor:pointer" onClick="reporte006();"><img src="../../imagenes/icono_pdf.png" width="32" height="32"></label>
		      		</td>
			    </tr>
	    	</table>
	    </div>-->
	    <?php
			}
			if($tipo==7){
		?>
		<div style="display:block">
			<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
				<tr>
			    	<th colspan="2"><strong>Parametros de Configuracion Reporte</strong></th>
			    </tr>
			    <tr >
				    <td style="text-align: center;">
				    	Nit &nbsp;<input type="text" name="txtNit" id="txtNit" class="box1"/>
				    	<input type="hidden" name="hidIdCarta" id="hidIdCarta" value="4252" />
				    </td>
			    </tr>
			    <tr>
			    	<td style="text-align: center;">Fecha convenio</td>
			    </tr>
			    <tr>
			    	<td style="text-align: center;">
				    	Fecha Inicial: 
				    	<input name="txtFechaInicial" type="text" class="box1" id="txtFechaInicial" readonly="readonly"/>
				    	Fecha Final: 
				    	<input name="txtFechaFinal" type="text" class="box1" id="txtFechaFinal" readonly="readonly"/>
			    	</td>
			    </tr>
			    <tr>
			    	<td style="text-align: center;">
			    		Agencia
			    		<select name="cmbIdAgencia" id="cmbIdAgencia" class="box1">
			    			<option value="">Seleccione</option>
			    			<?php
			    				echo $comboAgencia;
			    			?>
			    		</select>
			    	</td>
			    </tr>
			    <tr>
		      		<td colspan="2">&nbsp;</td>
			  	</tr>
			    <tr>
		      		<td colspan="2" style="text-align:center">
		      			<label style="cursor:pointer" onClick="reporte007();"><img src="../../imagenes/icono_pdf.png" width="32" height="32"></label>
		      		</td>
			    </tr>
	    	</table>
	    </div>
	    <?php
			}
			if($tipo==8){
		?>
		<div style="display:block">
			<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
				<tr>
			    	<th colspan="2"><strong>Parametros de Configuracion Reporte</strong></th>
			    </tr>
			    <tr >
				    <td style="text-align: center;">
				    	Nit &nbsp;<input type="text" name="txtNit" id="txtNit" class="box1"/>
				    </td>
			    </tr>
			    <tr>
			    	<td style="text-align: center;">Fecha convenio</td>
			    </tr>
			    <tr>
			    	<td style="text-align: center;">
				    	Fecha Inicial: 
				    	<input name="txtFechaInicial" type="text" class="box1" id="txtFechaInicial" readonly="readonly"/>
				    	Fecha Final: 
				    	<input name="txtFechaFinal" type="text" class="box1" id="txtFechaFinal" readonly="readonly"/>
			    	</td>
			    </tr>
			    <tr >
				    <td style="text-align: center;">
				    	Estado del convenio &nbsp;
				    	<select name="cmbEstadoConvenio" id="cmbEstadoConvenio" class="box1">
				    		<option value="A">Activos</option>
				    		<option value="I">Inactivos</option>
				    		<option value="C">Cerrados</option>
				    		<option value="">Todos</option>
				    	</select>
				    </td>
			    </tr>
			    <tr>
			    	<td style="text-align: center;">
			    		Agencia
			    		<select name="cmbIdAgencia" id="cmbIdAgencia" class="box1">
			    			<option value="">Seleccione</option>
			    			<?php
			    				echo $comboAgencia;
			    			?>
			    		</select>
			    	</td>
			    </tr>
			    <tr>
		      		<td colspan="2">&nbsp;</td>
			  	</tr>
			    <tr>
		      		<td colspan="2" style="text-align:center">
		      			<label style="cursor:pointer" onClick="reporte008();"><img src="../../imagenes/icono_excel.png" width="32" height="32"></label>
		      		</td>
			    </tr>
	    	</table>
	    </div>
	    <?php
			}
			if($tipo==9){
		?>
		<div style="display:block">
			<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
				<tr>
			    	<th colspan="2"><strong>Parametros de Configuracion Reporte</strong></th>
			    </tr>
			    <tr >
				    <td style="text-align: center;">
				    	Nit &nbsp;<input type="text" name="txtNit" id="txtNit" class="box1"/>
				    </td>
			    </tr>
			    <tr >
				    <td style="text-align: center;">
				    	Visita &nbsp;<input type="text" name="txtIdVisita" id="txtIdVisita" class="box1"/>
				    </td>
			    </tr>
			    <tr>
			    	<td style="text-align: center;">
			    		Agencia
			    		<select name="cmbIdAgencia" id="cmbIdAgencia" class="box1">
			    			<option value="">Seleccione</option>
			    			<?php
			    				echo $comboAgencia;
			    			?>
			    		</select>
			    	</td>
			    </tr>
			    <tr>
				    <td style="text-align: center;">
				    	Causal Visitas:
		    			<select name="cmbCausalVisita" class="boxfecha" id="cmbCausalVisita">
				        	<option value="">Seleccione</option>
		    				<?php 
				        		echo $comboCausalVisita;
				        	?>
			      		</select>
				    </td>
			    </tr>
			    <tr >
				    <td style="text-align: center;">
				    	Fecha visita
				    </td>
			    </tr>
			    <tr >
				    <td style="text-align: center;">
				    	Inicial: 
				    	<input name="txtFechaInicial" type="text" class="box1" id="txtFechaInicial" readonly="readonly"/> 
				    	Final: 
				    	<input name="txtFechaFinal" type="text" class="box1" id="txtFechaFinal" readonly="readonly"/>
				    </td>
			    </tr>
			    <tr>
		      		<td colspan="2">&nbsp;</td>
			  	</tr>
			    <tr>
		      		<td colspan="2" style="text-align:center">
		      			<label style="cursor:pointer" onClick="reporte009('PDF');"><img src="../../imagenes/icono_pdf.png" width="32" height="32"></label>
		      			<label style="cursor:pointer" onClick="reporte009('EXCEL');"><img src="../../imagenes/icono_excel.png" width="32" height="32"></label>
		      		</td>
			    </tr>
	    	</table>
	    </div>
	    <?php
			}
			if($tipo==10){
		?>
		<div style="display:block">
			<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
				<tr>
			    	<th colspan="2"><strong>Parametros de Configuracion Reporte</strong></th>
			    </tr>
			     <tr >
				    <td style="text-align: center;">
				    	N&uacute;mero Visita:
				    	<input type="text" name="txtNumeroVisita" id="txtNumeroVisita" class="box1"/>
				    </td>
			    </tr>
			    
			    <tr>
				    <td style="text-align: center;">
				    	Visitas a paz y salvo:
		    			<select name="cmbPazYSalvo" class="box1" id="cmbPazYSalvo">
				        	<option value="">Todo</option>
				        	<option value="A">No</option>
				        	<option value="P">Si</option>
			      		</select>
				    </td>
			    </tr>
			    <tr>
					<td style="text-align: center;">
						Nit:
			    		<input type="text" name="txtNit" id="txtNit" class="box1"/>
			      	</td>
			    </tr>
			    <tr >
					<td style="text-align: center;">
						Promotor:
			    		<select name="cmbPromotor" class="box1" id="cmbPromotor">
				        	<option value="">Seleccione</option>
				        	<?php echo $optionPromotor;?>
			      		</select>
			      	</td>
			    </tr>
			    <tr >
				    <td style="text-align: center;">
				    	Fecha visita
				    </td>
			    </tr>
			    <tr >
				    <td style="text-align: center;">
				    	Inicial: 
				    	<input name="txtFechaInicial" type="text" class="box1" id="txtFechaInicial" readonly="readonly"/>
				    	Final: 
				    	<input name="txtFechaFinal" type="text" class="box1" id="txtFechaFinal" readonly="readonly"/>
				    </td>
			    </tr>
			    <tr >
				    <td style="text-align: center;">
				    	Fecha Abono
				    </td>
			    </tr>
			    <tr >
				    <td style="text-align: center;">
				    	Inicial: 
				    	<input name="txtFechaInicial2" type="text" class="box1" id="txtFechaInicial2" readonly="readonly"/>
				    	Final: 
				    	<input name="txtFechaFinal2" type="text" class="box1" id="txtFechaFinal2" readonly="readonly"/>
				    </td>
			    </tr>
			    <tr>
			    	<td style="text-align: center;">
			    		Agencia
			    		<select name="cmbIdAgencia" id="cmbIdAgencia" class="box1">
			    			<option value="">Seleccione</option>
			    			<?php
			    				echo $comboAgencia;
			    			?>
			    		</select>
			    	</td>
			    </tr>
			    <tr>
					<td style="text-align: center;">
				    	Causal Visitas:
		    			<select name="cmbCausalVisita" class="boxfecha" id="cmbCausalVisita">
				        	<option value="">Seleccione</option>
		    				<?php 
				        		echo $comboCausalVisita;
				        	?>
			      		</select>
				    </td>
			    </tr>
			    <tr>
		      		<td colspan="2">&nbsp;</td>
			  	</tr>
			    <tr>
		      		<td colspan="2" style="text-align:center">
		      			<label style="cursor:pointer" onClick="reporte010('PDF');"><img src="../../imagenes/icono_pdf.png" width="32" height="32"></label>
		      			<label style="cursor:pointer" onClick="reporte010('EXCEL');"><img src="../../imagenes/icono_excel.png" width="32" height="32"></label>
		      		</td>
			    </tr>
	    	</table>
	    </div>
	    <?php
			}
			if($tipo==11){
		?>
		<div style="display:block">
			<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
				<tr>
			    	<th colspan="2"><strong>Parametros de Configuracion Reporte</strong></th>
			    </tr>
			    <tr>
					<td style="text-align: center;">
						Nit:
			    		<input type="text" name="txtNit" id="txtNit" class="box1"/>
			      	</td>
			    </tr>
			    <tr >
				    <td style="text-align: center;">
				    	N&uacute;mero Visita:
				    	<input type="text" name="txtNumeroVisita" id="txtNumeroVisita" class="box1"/>
				    </td>
			    </tr>
			    <tr>
				    <td style="text-align: center;">
				    	Fecha visita
				    </td>
			    </tr>
			    <tr >
				    <td style="text-align: center;">
				    	Inicial: 
				    	<input name="txtFechaInicial" type="text" class="box1" id="txtFechaInicial" readonly="readonly"/>
				    	Final: 
				    	<input name="txtFechaFinal" type="text" class="box1" id="txtFechaFinal" readonly="readonly"/>
				    </td>
			    </tr>
			    <tr>
				    <td style="text-align: center;">
				    	Asesor Juridico Interno:
				    	<select name="cmbAsesorJuridicoI" id="cmbAsesorJuridicoI" class="box1">
				    		<option value="">Seleccione</option>
				    		<?php echo $optionAsesorJuridI;?>
				    	</select>
				    </td>
			    </tr>
			    <tr>
				    <td style="text-align: center;">
				    	Asesor Juridico Externo:
				    	<select name="cmbAsesorJuridicoE" id="cmbAsesorJuridicoE" class="box1">
				    		<option value="">Seleccione</option>
				    		<?php echo $optionAsesorJuridE;?>
				    	</select>
				    </td>
			    </tr>
			    <tr>
			    	<td style="text-align: center;">
			    		Agencia
			    		<select name="cmbIdAgencia" id="cmbIdAgencia" class="box1">
			    			<option value="">Seleccione</option>
			    			<?php
			    				echo $comboAgencia;
			    			?>
			    		</select>
			    	</td>
			    </tr>
			    <tr>
					<td style="text-align: center;">
				    	Causal Visitas:
		    			<select name="cmbCausalVisita" class="boxfecha" id="cmbCausalVisita">
				        	<option value="">Seleccione</option>
		    				<?php 
				        		echo $comboCausalVisita;
				        	?>
			      		</select>
				    </td>
			    </tr>
			    <tr>
		      		<td colspan="2">&nbsp;</td>
			  	</tr>
			    <tr>
		      		<td colspan="2" style="text-align:center">
		      			<label style="cursor:pointer" onClick="reporte011('PDF');"><img src="../../imagenes/icono_pdf.png" width="32" height="32"></label>
		      			<label style="cursor:pointer" onClick="reporte011('EXCEL');"><img src="../../imagenes/icono_excel.png" width="32" height="32"></label>
		      		</td>
			    </tr>
	    	</table>
	    </div>
	    <?php
			}
			if($tipo==12){
		?>
		<div style="display:block">
			<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
				<tr>
			    	<th colspan="2"><strong>Parametros de Configuracion Reporte</strong></th>
			    </tr>
			     <tr >
				    <td style="text-align: center;">
				    	N&uacute;mero Visita:
				    	<input type="text" name="txtNumeroVisita" id="txtNumeroVisita" class="boxfecha"/>
				    </td>
			    </tr>
			    <tr>
				    <td style="text-align: center;">
				    	Visitas a paz y salvo:
		    			<select name="cmbPazYSalvo" class="boxfecha" id="cmbPazYSalvo">
				        	<option value="">Todo</option>
				        	<option value="A">No</option>
				        	<option value="P">Si</option>
			      		</select>
				    </td>
			    </tr>
			    <tr>
					<td style="text-align: center;">
						Nit:
			    		<input type="text" name="txtNit" id="txtNit" class="boxfecha"/>
			      	</td>
			    </tr>
			    <tr >
					<td style="text-align: center;">
						Promotor:
			    		<select name="cmbPromotor" class="boxlargo" id="cmbPromotor">
				        	<option value="">Seleccione</option>
				        	<?php echo $optionPromotor;?>
			      		</select>
			      	</td>
			    </tr>
			    <tr>
				    <td style="text-align: center;">
				    	Fecha visita
				    </td>
			    </tr>
			    <tr >
				    <td style="text-align: center;">
				    	Inicial: 
				    	<input name="txtFechaInicial" type="text" class="boxfecha" id="txtFechaInicial" readonly="readonly"/>
				    	Final: 
				    	<input name="txtFechaFinal" type="text" class="boxfecha" id="txtFechaFinal" readonly="readonly"/>
				    </td>
			    </tr>
			    <tr>
				    <td style="text-align: center;">
				    	Causal Visitas:
		    			<select name="cmbCausalVisita" class="boxfecha" id="cmbCausalVisita">
				        	<option value="">Seleccione</option>
		    				<?php 
				        		echo $comboCausalVisita;
				        	?>
			      		</select>
				    </td>
			    </tr>
			    <tr>
				    <td style="text-align: center;">
				    	Causal Ajuste:
		    			<select name="cmbCausalAjuste" class="boxfecha" id="cmbCausalAjuste">
		    				<option value="">Seleccione</option>
		    				<?php 
				        		echo $comboCausalAjuste;
				        	?>
			      		</select>
				    </td>
			    </tr>
			    <tr>
			    	<td style="text-align: center;">
			    		Agencia
			    		<select name="cmbIdAgencia" id="cmbIdAgencia" class="boxfecha">
			    			<option value="">Seleccione</option>
			    			<?php
			    				echo $comboAgencia;
			    			?>
			    		</select>
			    	</td>
			    </tr>
			    <tr>
		      		<td colspan="2">&nbsp;</td>
			  	</tr>
			    <tr>
		      		<td colspan="2" style="text-align:center">
		      			<label style="cursor:pointer" onClick="reporte012('PDF');"><img src="../../imagenes/icono_pdf.png" width="32" height="32"></label>
		      			<label style="cursor:pointer" onClick="reporte012('EXCEL');"><img src="../../imagenes/icono_excel.png" width="32" height="32"></label>
		      		</td>
			    </tr>
	    	</table>
	    </div>
	    <?php
			}
			if($tipo==13){
		?>
		<div style="display:block">
			<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
				<tr>
			    	<th colspan="2"><strong>Parametros de Configuracion Reporte</strong></th>
			    </tr>
			    <tr>
				    <td width="50%" style="text-align: center;">
				    	Comunicaci&oacute;n:
				    	<input type="text" name="txtComunicacion" id="txtComunicacion" class="box1"/>
				    	<input type="hidden" name="hidIdCarta" id="hidIdCarta" value="4251" />
				    	<span class="clsObligatorio">&nbsp;&nbsp;&nbsp;</span>
				    </td>
			    </tr>
			    <tr>
				    <td style="text-align: center;">
				    	N&uacute;mero Visita:
				    	<input type="text" name="txtNumeroVisita" id="txtNumeroVisita" class="box1"/>
				    </td>
			    </tr>
			    <tr>
				    <td style="text-align: center;">
				    	Fecha visita
				    </td>
			    </tr>
			    <tr>
				    <td style="text-align: center;">
				    	Inicial: 
				    	<input name="txtFechaInicial" type="text" class="box1" id="txtFechaInicial" readonly="readonly"/>
				    	Final: 
				    	<input name="txtFechaFinal" type="text" class="box1" id="txtFechaFinal" readonly="readonly" />
				    </td>
			    </tr>
			    <tr>
					<td style="text-align: center;">
						Nit:
			    		<input type="text" name="txtNit" id="txtNit" class="box1"/>
			      	</td>
			    </tr>
			    <tr>
					<td style="text-align: center;">
						Numero Reitero:
			    		&nbsp;1<input type="radio" name="radNumeroReitero" id="radNumeroReitero1" value="1"/>
			    		&nbsp;2<input type="radio" name="radNumeroReitero" id="radNumeroReitero2" value="2"/>
			      	</td>
			    </tr>
			    <tr>
			    	<td style="text-align: center;">
			    		Agencia
			    		<select name="cmbIdAgencia" id="cmbIdAgencia" class="box1">
			    			<option value="">Seleccione</option>
			    			<?php
			    				echo $comboAgencia;
			    			?>
			    		</select>
			    	</td>
			    </tr>
			    <tr>
		      		<td colspan="2">&nbsp;</td>
			  	</tr>
			    <tr>
		      		<td colspan="2" style="text-align:center">
		      			<label style="cursor:pointer" onClick="reporte013('PDF');"><img src="../../imagenes/icono_pdf.png" width="32" height="32"></label>
		      		</td>
			    </tr>
	    	</table>
	    </div>
	    <?php
			}
			if($tipo==14){
		?>
		<div style="display:block">
			<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
				<tr>
			    	<th colspan="2"><strong>Parametros de Configuracion Reporte</strong></th>
			    </tr>
			    <tr>
					<td style="text-align: center;">
						Nit:
			    		<input type="text" name="txtNit" id="txtNit" class="box1"/>
			      	</td>
			    </tr>
			    <tr >
				    <td style="text-align: center;">
				    	N&uacute;mero Visita:
				    	<input type="text" name="txtNumeroVisita" id="txtNumeroVisita" class="box1"/>
				    </td>
			    </tr>
			    <tr>
				    <td style="text-align: center;">
				    	Fecha Notificaci&oacute;n
				    </td>
			    </tr>
			    <tr >
				    <td style="text-align: center;">
				    	Inicial:
				    	<input name="txtFechaInicial" type="text" class="box1" id="txtFechaInicial" readonly="readonly"/>
				    	Final:
				    	<input name="txtFechaFinal" type="text" class="box1" id="txtFechaFinal" readonly="readonly"/>
				    </td>
			    </tr>
			    <tr>
			    	<td style="text-align: center;">
			    		Agencia
			    		<select name="cmbIdAgencia" id="cmbIdAgencia" class="box1">
			    			<option value="">Seleccione</option>
			    			<?php
			    				echo $comboAgencia;
			    			?>
			    		</select>
			    	</td>
			    </tr>
			    <tr>
					<td style="text-align: center;">
				    	Causal Visitas:
		    			<select name="cmbCausalVisita" class="boxfecha" id="cmbCausalVisita">
				        	<option value="">Seleccione</option>
		    				<?php 
				        		echo $comboCausalVisita;
				        	?>
			      		</select>
				    </td>
			    </tr>
			    <tr>
		      		<td colspan="2">&nbsp;</td>
			  	</tr>
			    <tr>
		      		<td colspan="2" style="text-align:center">
		      			<label style="cursor:pointer" onClick="reporte014('PDF');"><img src="../../imagenes/icono_pdf.png" width="32" height="32"></label>
		      			<label style="cursor:pointer" onClick="reporte014('EXCEL');"><img src="../../imagenes/icono_excel.png" width="32" height="32"></label>
		      		</td>
			    </tr>
	    	</table>
	    </div>
	    <?php
			}
			if($tipo==15){
		?>
		<div style="display:block">
			<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
				<tr>
			    	<th colspan="2"><strong>Parametros de Configuracion Reporte</strong></th>
			    </tr>
			     <tr >
				    <td style="text-align: center;">
				    	N&uacute;mero Visita:
				    	<input type="text" name="txtNumeroVisita" id="txtNumeroVisita" class="boxfecha"/>
				    </td>
			    </tr>
			    <tr>
				    <td style="text-align: center;">
				    	Visitas a paz y salvo:
		    			<select name="cmbPazYSalvo" class="boxfecha" id="cmbPazYSalvo">
				        	<option value="">Todo</option>
				        	<option value="A">No</option>
				        	<option value="P">Si</option>
			      		</select>
				    </td>
			    </tr>
			    <tr>
					<td style="text-align: center;">
						Nit:
			    		<input type="text" name="txtNit" id="txtNit" class="boxfecha"/>
			      	</td>
			    </tr>
			    <tr >
					<td style="text-align: center;">
						Promotor:
			    		<select name="cmbPromotor" class="boxlargo" id="cmbPromotor">
				        	<option value="">Seleccione</option>
				        	<?php echo $optionPromotor;?>
			      		</select>
			      	</td>
			    </tr>
			    <tr>
				    <td style="text-align: center;">
				    	Fecha visita
				    </td>
			    </tr>
			    <tr >
				    <td style="text-align: center;">
				    	Inicial: 
				    	<input name="txtFechaInicial" type="text" class="boxfecha" id="txtFechaInicial" readonly="readonly"/>
				    	Final: 
				    	<input name="txtFechaFinal" type="text" class="boxfecha" id="txtFechaFinal" readonly="readonly"/>
				    </td>
			    </tr>
			    <tr >
				    <td style="text-align: center;">
				    	Fecha Abono
				    </td>
			    </tr>
			    <tr >
				    <td style="text-align: center;">
				    	Inicial: 
				    	<input name="txtFechaInicial2" type="text" class="boxfecha" id="txtFechaInicial2" readonly="readonly"/>
				    	Final: 
				    	<input name="txtFechaFinal2" type="text" class="boxfecha" id="txtFechaFinal2" readonly="readonly"/>
				    </td>
			    </tr>
			    <tr>
				    <td style="text-align: center;">
				    	Causal Visitas:
		    			<select name="cmbCausalVisita" class="boxfecha" id="cmbCausalVisita">
				        	<option value="">Seleccione</option>
		    				<?php 
				        		echo $comboCausalVisita;
				        	?>
			      		</select>
				    </td>
			    </tr>
			    <tr>
			    	<td style="text-align: center;">
			    		Agencia
			    		<select name="cmbIdAgencia" id="cmbIdAgencia" class="boxfecha">
			    			<option value="">Seleccione</option>
			    			<?php
			    				echo $comboAgencia;
			    			?>
			    		</select>
			    	</td>
			    </tr>
			    <tr>
		      		<td colspan="2">&nbsp;</td>
			  	</tr>
			    <tr>
		      		<td colspan="2" style="text-align:center">
		      			<label style="cursor:pointer" onClick="reporte015('PDF');"><img src="../../imagenes/icono_pdf.png" width="32" height="32"></label>
		      			<label style="cursor:pointer" onClick="reporte015('EXCEL');"><img src="../../imagenes/icono_excel.png" width="32" height="32"></label>
		      		</td>
			    </tr>
	    	</table>
	    </div>
	    <?php
			}
			if($tipo==16){
		?>
		<div style="display:block">
			<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
				<tr>
			    	<th colspan="2"><strong>Parametros de Configuracion Reporte</strong></th>
			    </tr>
			     <tr >
				    <td style="text-align: center;">
				    	N&uacute;mero Visita:
				    	<input type="text" name="txtNumeroVisita" id="txtNumeroVisita" class="boxfecha"/>
				    </td>
			    </tr>
			    <tr>
				    <td style="text-align: center;">
				    	Visitas a paz y salvo:
		    			<select name="cmbPazYSalvo" class="boxfecha" id="cmbPazYSalvo">
				        	<option value="">Todo</option>
				        	<option value="A">No</option>
				        	<option value="P">Si</option>
			      		</select>
				    </td>
			    </tr>
			    <tr>
					<td style="text-align: center;">
						Nit:
			    		<input type="text" name="txtNit" id="txtNit" class="boxfecha"/>
			      	</td>
			    </tr>
			    <tr >
					<td style="text-align: center;">
						Promotor:
			    		<select name="cmbPromotor" class="boxlargo" id="cmbPromotor">
				        	<option value="">Seleccione</option>
				        	<?php echo $optionPromotor;?>
			      		</select>
			      	</td>
			    </tr>
			    <tr>
				    <td style="text-align: center;">
				    	Fecha visita
				    </td>
			    </tr>
			    <tr >
				    <td style="text-align: center;">
				    	Inicial: 
				    	<input name="txtFechaInicial" type="text" class="boxfecha" id="txtFechaInicial" readonly="readonly"/>
				    	Final: 
				    	<input name="txtFechaFinal" type="text" class="boxfecha" id="txtFechaFinal" readonly="readonly"/>
				    </td>
			    </tr>
			    <tr >
				    <td style="text-align: center;">
				    	Fecha Abono
				    </td>
			    </tr>
			    <tr >
				    <td style="text-align: center;">
				    	Inicial: 
				    	<input name="txtFechaInicial2" type="text" class="box1" id="txtFechaInicial2" readonly="readonly"/>
				    	Final: 
				    	<input name="txtFechaFinal2" type="text" class="box1" id="txtFechaFinal2" readonly="readonly"/>
				    </td>
			    </tr>
			    <tr>
				    <td style="text-align: center;">
				    	Causal Visitas:
		    			<select name="cmbCausalVisita" class="boxfecha" id="cmbCausalVisita">
				        	<option value="">Seleccione</option>
		    				<?php 
				        		echo $comboCausalVisita;
				        	?>
			      		</select>
				    </td>
			    </tr>
			    <tr>
			    	<td style="text-align: center;">
			    		Agencia
			    		<select name="cmbIdAgencia" id="cmbIdAgencia" class="boxfecha">
			    			<option value="">Seleccione</option>
			    			<?php
			    				echo $comboAgencia;
			    			?>
			    		</select>
			    	</td>
			    </tr>
			    <tr>
		      		<td colspan="2">&nbsp;</td>
			  	</tr>
			    <tr>
		      		<td colspan="2" style="text-align:center">
		      			<label style="cursor:pointer" onClick="reporte016('PDF');"><img src="../../imagenes/icono_pdf.png" width="32" height="32"></label>
		      			<label style="cursor:pointer" onClick="reporte016('EXCEL');"><img src="../../imagenes/icono_excel.png" width="32" height="32"></label>
		      		</td>
			    </tr>
	    	</table>
	    </div>
	    <?php
			}
			if($tipo==17){
		?>
		<div style="display:block">
			<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
				<tr>
			    	<th colspan="2"><strong>Parametros de Configuracion Reporte</strong></th>
			    </tr>
			    <tr>
				    <td style="text-align: center;">
				    	Periodo Cierre:
				    </td>
			    </tr>
			    <tr >
				    <td style="text-align: center;">
				    	Inicial: 
				    	<input name="txtPeriodoInicial" type="text" class="boxfecha" id="txtPeriodoInicial" readonly="readonly"/>
				    	Final: 
				    	<input name="txtPeriodoFinal" type="text" class="boxfecha" id="txtPeriodoFinal" readonly="readonly"/>
				    </td>
			    </tr>
			    <tr>
				    <td style="text-align: center;">
				    	Causal Visita:
		    			<select name="cmbCausalVisita" class="boxfecha" id="cmbCausalVisita">
				        	<option value="">Seleccione</option>
		    				<?php 
				        		echo $comboCausalVisita;
				        	?>
			      		</select>
				    </td>
			    </tr>
			    <tr>
			    	<td style="text-align: center;">
			    		Agencia:
			    		<select name="cmbIdAgencia" id="cmbIdAgencia" class="boxfecha">
			    			<option value="">Seleccione</option>
			    			<?php
			    				echo $comboAgencia;
			    			?>
			    		</select>
			    	</td>
			    </tr>
			    <tr>
		      		<td colspan="2">&nbsp;</td>
			  	</tr>
			    <tr>
		      		<td colspan="2" style="text-align:center">
		      			<label style="cursor:pointer" onClick="reporte017('PDF');"><img src="../../imagenes/icono_pdf.png" width="32" height="32"></label>
		      			<label style="cursor:pointer" onClick="reporte017('EXCEL');"><img src="../../imagenes/icono_excel.png" width="32" height="32"></label>
		      		</td>
			    </tr>
	    	</table>
	    </div>
	    <?php
			}
			if($tipo==18){
		?>
		<div style="display:block">
			<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
				<tr>
			    	<th colspan="2"><strong>Parametros de Configuracion Reporte</strong></th>
			    </tr>
			    <tr>
				    <td style="text-align: center;">
				    	Periodo Cierre:
				    	<input name="txtPeriodo" type="text" class="boxfecha element-required" id="txtPeriodo" readonly="readonly"/>
				    	<span class="clsObligatorio">&nbsp;&nbsp;&nbsp;</span>
				    </td>
			    </tr>
			    <tr>
				    <td style="text-align: center;">
				    	Causal Visita:
		    			<select name="cmbCausalVisita" class="boxfecha" id="cmbCausalVisita">
				        	<option value="">Seleccione</option>
		    				<?php 
				        		echo $comboCausalVisita;
				        	?>
			      		</select>
				    </td>
			    </tr>
			    <tr>
		      		<td colspan="2">&nbsp;</td>
			  	</tr>
			    <tr>
		      		<td colspan="2" style="text-align:center">
		      			<label style="cursor:pointer" onClick="reporte018('PDF');"><img src="../../imagenes/icono_pdf.png" width="32" height="32"></label>
		      			<label style="cursor:pointer" onClick="reporte018('EXCEL');"><img src="../../imagenes/icono_excel.png" width="32" height="32"></label>
		      		</td>
			    </tr>
	    	</table>
	    </div>
	    <?php
			}
		?>
	
		<input type="hidden" id="txtUsuario" name="txtUsuario" value="<?php echo $usuario; ?>" /> 
		<input type="hidden" id="txtNombreUsuario" name="txtNombreUsuario" value="<?php echo $_SESSION["USSER"]; ?>" />
		<input type="hidden" id="txtSexoUsuario" name="txtSexoUsuario" value="<?php echo $sexoUsuario; ?>" 
		<input type="hidden" id="txtRept" name="txtRept" value="<?php echo $rept; ?>" />
		
		<!-- DIV PARA MOSTRAR LAS NOTIFICACIONES DE LA EMPRESA SEGUN LA CARTA A GENERAR -->
		<div id="divControlNotificacion" style="display: none;" title="CONTROL NOTIFICACI&Oacute;N"> 
			<table width="90%" border="0" align="center"  class="tablero" id="tbControlNotificacion">
				<thead>
					<tr>
						<th>CARTA</th>
						<th>FECHA NOTIFICACI&Oacute;N</th>
						<th>ESTADO</th>
						<th>FECHA ESTADO</th>
						<th>INFORMACI&Oacute;N</th>
						<th>...</th>
					</tr>
				</thead>
				<tbody>
					
				</tbody>
			</table>
		</div>
		
	</body>
</html>