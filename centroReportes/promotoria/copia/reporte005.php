<?php
/* @Autor	Ing. Oswaldo Gonz�lez
 * @fecha	noviembre 3 de 2010
 * @objeto	Reporte de gesti�n de radicaciones - afiliaciones fonede
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

 ini_set("display_errors",'1');
if ( !isset( $_SESSION ['USUARIO'] ) ) {session_start();}
$raiz=$_SESSION['RAIZ'];
chdir($raiz);
$idl=$_REQUEST['v0'];
include_once 'config.php';
include("centroReportes/ReporteController.php");
$nombreReporte = "reporte005";
$dir = "promotoria/";
$objReporte = new ReporteController();
$parametros = array(
					array(
							"nombre" => "usuario",
						 	"tipo" => "String",
						 	"valor" => $_SESSION["USUARIO"]),
					array(
							"nombre" => "numero",
						 	"tipo" => "Long",
						 	"valor" => $idl)
					);
$objReporte->generarReporte($nombreReporte,$parametros,$dir);
?>
