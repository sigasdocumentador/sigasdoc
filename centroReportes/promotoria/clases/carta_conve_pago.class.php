<?php
set_time_limit(0);
date_default_timezone_set("America/Bogota");
ini_set('memory_limit', '1000M');

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
include_once $raiz.DIRECTORY_SEPARATOR.'config.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
include_once $raiz.DIRECTORY_SEPARATOR.'aportes'.DIRECTORY_SEPARATOR.'empresas'.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'empresas.class.php';
include_once $raiz.DIRECTORY_SEPARATOR.'parametrizacion'.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'modelo_documento_html.class.php';
include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'utilitario.class.php';

class CartaConvePago{
	
	private static $conPDO = null;
	
	private $idCarta = 4252; //[4252][CARTA CONVENIOS DE PAGO]
	private $modeloDocumento = "CARTA_CONVE_PAGO_DEPEN";
	private $arrResultado = null;
	private $arrFiltro = null;
	
	private $fechaSistema;
	
	private $objModeloDocumentoHtml = null;
	
	function __construct(){
		try{
			self::$conPDO = IFXDbManejador::conectarDB();
			if( self::$conPDO->conexionID==null ){
				throw new Exception(self::$conPDO->error);
			}
		}catch(Exception $e){
			echo $e->getMessage();
			exit();
		}
		
		$meses = array(
				"Enero"
				,"Febrero"
				,"Marzo"
				,"Abril"
				,"Mayo"
				,"Junio"
				,"Julio"
				,"Agosto"
				,"Septiembre"
				,"Octubre"
				,"Noviembre"
				,"Diciembre");
		
		$dias = array(
				"Lunes"
				,"Martes"
				,"Miercoles"
				,"Jueves"
				,"Viernes"
				,"Sabado"
				,"Domingo");
		
		$this->fechaSistema = $dias[date("N")-1] . " " . date("d") . " " . $meses[date("n")-1] . " " . date("Y");
		$this->objModeloDocumentoHtml = new ModeloDocumentoHtml();
	}
	
	/**
	 * Metodo encargado de preparar el contenido de las cartas
	 *  
	 * @param unknown_type $arrFiltro
	 */
	public function fetch_carta($arrFiltro){
		$this->arrFiltro = $arrFiltro;		
		
		$this->preparar_datos();
		
		return $this->arrResultado;
	}
	
	private function fetch_datos_convenio(){
		$arrData = array();
		$query = "DECLARE @nit VARCHAR(12) = '{$this->arrFiltro["nit"]}'
					, @fecha_convenio_i DATE = '{$this->arrFiltro["fecha_convenio_i"]}'
					, @fecha_convenio_f DATE = '{$this->arrFiltro["fecha_convenio_f"]}'
					, @id_agencia INT = '{$this->arrFiltro["id_agencia"]}'
				
				SELECT DISTINCT
					a48.idempresa,a48.nit,a48.razonsocial, a48.telefono, a48.direccion, a48.telefono, a48.idtipoafiliacion
					, a15.pnombre + ' ' + isnull(a15.snombre,'') + ' ' + a15.papellido + ' ' + isnull(a15.sapellido,'') AS representante
					, a15.identificacion, 'xxxxxxxx' AS ciudadexpedicion
					, a306.idconvenio, a306.consecutivo, a306.valor, a306.indicador
					, a307.iddetalleconvenio, a307.fechapago, a307.interes, a307.interes + (SELECT SUM(valor) FROM aportes308 b308 WHERE b308.iddetalleconvenio=a307.iddetalleconvenio) AS totalfechas
					, a519.nombres
				FROM aportes306 a306
				INNER JOIN aportes307 a307 ON a307.idconvenio=a306.idconvenio
					INNER JOIN aportes308 a308 ON a308.iddetalleconvenio=a307.iddetalleconvenio
					INNER JOIN aportes303 a303 ON a303.iddetalle=a308.idperiodoliquidacion
					INNER JOIN aportes305 a305 ON a305.idliquidacion=a303.idliquidacion
					INNER JOIN aportes302 a302 ON a302.idvisita=a305.idvisita
					INNER JOIN aportes048 a48 ON a48.idempresa=a302.idempresa
					INNER JOIN aportes015 a15 ON a15.idpersona=a48.idrepresentante
					INNER JOIN aportes519 a519 ON a519.usuario=a306.usuario
				WHERE (@nit = '' OR a48.nit=@nit)
					AND (@fecha_convenio_i='' OR a306.fechasistema BETWEEN @fecha_convenio_i AND @fecha_convenio_f)
					AND (@id_agencia='' OR a302.idagencia=@id_agencia)
				ORDER BY a306.consecutivo";
		
		$arrData = Utilitario::fetchConsulta($query, self::$conPDO);
		return $arrData; 
	}
	
	private function fetch_datos_encabezado(){
		$arrData = array();
		
		$statement = self::$conPDO->conexionID->prepare("SET LANGUAGE Spanish");
		$statement->execute();
		
		$query = "
				DECLARE @id_convenio INT = '{$this->arrFiltro["id_convenio"]}'
					, @periodos NVARCHAR(MAX) = ''
					, @valor_letras VARCHAR(max) = NULL
					, @valor INT
				
				SELECT @periodos = @periodos + ' ' + DATENAME(m, CONVERT(DATE,periodo + '01')) + ' '+ SUBSTRING(periodo,1,4) + ' '
				FROM aportes308 a308
					INNER JOIN aportes307 a307 ON a307.iddetalleconvenio=a308.iddetalleconvenio
					INNER JOIN aportes303 a303 ON a303.iddetalle=a308.idperiodoliquidacion
				WHERE a307.idconvenio = @id_convenio
				GROUP BY periodo
				ORDER BY periodo
				
				SELECT @valor = sum(valor) FROM aportes306 WHERE idconvenio = @id_convenio
				
				/*EXEC sp_Dinero_a_Texto @Numero = @valor, @numero_letra = @valor_letras OUTPUT*/
				SELECT @valor_letras = dbo.fn_Cast_Numer_A_Texto(@valor)
				
				/*SET @valor_letras = SUBSTRING(@valor_letras,1,(LEN(@valor_letras)-2))*/
				SELECT  @periodos AS periodo, @valor_letras AS valor_letra, @valor AS valor";
		$arrData = Utilitario::fetchConsulta($query, self::$conPDO);
		return $arrData;
	}
	
	private function fetch_datos_fecha_pago($interes){
		$arrData = array();
		
		$statement = self::$conPDO->conexionID->prepare("SET LANGUAGE Spanish");
		$statement->execute();
		
		$query = "
				DECLARE @periodos NVARCHAR(MAX) = ''
					, @valor_fecha DECIMAL(20) = 0
					, @id_detalle_convenio INT = {$this->arrFiltro["id_detalle_convenio"]}
					, @fecha_paga DATE = '{$this->arrFiltro["fecha_paga"]}'
					, @interes DECIMAL(20) = $interes
				
				SELECT @valor_fecha = interes FROM aportes307 WHERE iddetalleconvenio = @id_detalle_convenio
				
				SELECT @periodos = @periodos + ' ' + DATENAME(m, CONVERT(DATE,a303.periodo + '01')) + '/'+ SUBSTRING(a303.periodo,3,2) + ' $ ' + CONVERT(VARCHAR(20),a308.valor)
				, @valor_fecha = @valor_fecha + a308.valor
				FROM aportes308 a308
				    INNER JOIN aportes303 a303 ON a303.iddetalle=a308.idperiodoliquidacion
				WHERE a308.iddetalleconvenio = @id_detalle_convenio
				ORDER BY periodo
				
				SET @periodos += ' Interes $' + CONVERT(VARCHAR(20), @interes)
				
				SELECT CONVERT(CHAR(2),DAY(@fecha_paga)) + ' ' + upper(DATENAME(m, CONVERT(DATE,@fecha_paga))) + ' ' + CONVERT(CHAR(4), YEAR(@fecha_paga)) AS fechapago, @periodos AS periodo, @valor_fecha AS valor";
		$arrData = Utilitario::fetchConsulta($query, self::$conPDO);
		return $arrData;
	}
	
	/**
	 * Obtener la plantilla de la carta en formato Html
	 * @return string
	 */
	private function fetch_plantilla_carta(){
		
		$contenidoCarta = "";
		
		$arrDatos = $this->objModeloDocumentoHtml->fetch_modelo_documento(array("modelo_documento"=>$this->modeloDocumento));
		
		if(count($arrDatos)>0){
			$contenidoCarta = $arrDatos[0]["contenido_html"];
		}
		
		return $contenidoCarta;
	}
	
	private function preparar_datos(){
		
		//Obtener los datos de la visita
		$arrDatosConvenio = $this->fetch_datos_convenio();
		
		$arrDatos = array();
		
		////Recorrer los convenios
		foreach($arrDatosConvenio as $datosConvenio){
			
			if(!isset($arrDatos[$datosConvenio["idconvenio"]])){
				
				$modeloDocumento = "";
				if($datosConvenio["indicador"]==2)
					$modeloDocumento = "CARTA_CONVE_PAGO_INDEP";
				else
					$modeloDocumento = "CARTA_CONVE_PAGO_DEPEN";
				
				$arrBandera = array(
						"idempresa"=>$datosConvenio["idempresa"],
						"razonsocial"=>$datosConvenio["razonsocial"],
						"consecutivo"=>$datosConvenio["consecutivo"],
						"indicador"=>$datosConvenio["indicador"],
						"representante"=>$datosConvenio["representante"],
						"identificacion"=>$datosConvenio["identificacion"],
						"nit"=>$datosConvenio["nit"],
						"direccion"=>$datosConvenio["direccion"],
						"telefono"=>$datosConvenio["telefono"],
						"fecha_sistema"=>$this->fechaSistema,
				
						"valor_encabezado"=>"",
						"valor_letra_encabezado"=>"",
						"periodo_encabezado"=>"",
				
						"valor_fechas"=>0,
				
						"nombres"=>$datosConvenio["nombres"],
						
						"tabla_fecha_pago"=>"",
						"modelo_documento"=>$modeloDocumento);
				//Obtener el encabezado
				$this->arrFiltro["id_convenio"] = $datosConvenio["idconvenio"];
				
				$arrDatosEncabezado = $this->fetch_datos_encabezado();
				
				if(count($arrDatosEncabezado)>0){
					$arrDatosEncabezado = $arrDatosEncabezado[0];
					$arrBandera["valor_encabezado"] = $arrDatosEncabezado["valor"];
					$arrBandera["valor_letra_encabezado"] = $arrDatosEncabezado["valor_letra"];
					$arrBandera["periodo_encabezado"] = $arrDatosEncabezado["periodo"];
				}
				
				$arrDatos[$datosConvenio["idconvenio"]] = $arrBandera;
			}
			
			
			
			//Acumulado del total de las fechas de pago
			$totalFecha = (float)(isset($datosConvenio["totalfechas"])?$datosConvenio["totalfechas"]:0);
			$arrDatos[$datosConvenio["idconvenio"]]["valor_fechas"] = (float)$arrDatos[$datosConvenio["idconvenio"]]["valor_fechas"] + $totalFecha;
			//var_dump($datosConvenio["totalfechas"]);exit();
			
			
			//Obtener los datos de la fecha de pago
			$this->arrFiltro = array(
					"id_detalle_convenio" => $datosConvenio["iddetalleconvenio"]
					,"fecha_paga"=>$datosConvenio["fechapago"]);
			
			$arrDatosFechaPago = $this->fetch_datos_fecha_pago($datosConvenio["interes"]);
			
			if(count($arrDatosFechaPago)>0){
				$arrDatosFechaPago = $arrDatosFechaPago[0];
				
				$arrDatos[$datosConvenio["idconvenio"]]["tabla_fecha_pago"] .= "<tr>
							<td style='border:1px solid #000000' >{$arrDatosFechaPago["fechapago"]}</td>
							<td style='border:1px solid #000000' colspan='2'>{$arrDatosFechaPago["periodo"]}</td>
							<td style='border:1px solid #000000; text-align:right;'>$".Util::trasformaNumero("NORMAL",$arrDatosFechaPago["valor"])."</td></tr>";
			}
		}
		
		//Obtener los modelos de las cartas
		$this->modeloDocumento = "CARTA_CONVE_PAGO_INDEP";
		$plantCartaIndep = $this->fetch_plantilla_carta();
	
		$this->modeloDocumento = "CARTA_CONVE_PAGO_DEPEN";
		$plantCartaDepen = $this->fetch_plantilla_carta();
		
		//Recorrer
		foreach($arrDatos as $datos){
			
			if($datos["modelo_documento"]=="CARTA_CONVE_PAGO_INDEP")
				$plantillaCarta = $plantCartaIndep;
			else 
				$plantillaCarta = $plantCartaDepen;
			
			$contenidoCarta = $this->preparar_contenido_html($plantillaCarta,$datos);
			$informacion = "";
			
			$this->arrResultado[] = array(
					"id_carta_notificacion"=>$this->idCarta
					,"id_empresa"=>$datos["idempresa"]
					,"informacion"=>$informacion
					,"id_estado"=>0
					,"contenido_carta"=>$contenidoCarta);
		}
	}
	
	private function preparar_contenido_html($plantillaHtml, $datos){
		
		$arrIndiceModelo = array(
				"razonsocial"=>"razonsocial",
				"consecutivo"=>"consecutivo",
				"indicador"=>"indicador",
				"representante"=>"representante",
				"identificacion"=>"identificacion",
				"nit"=>"nit",
				"direccion"=>"direccion",
				"telefono"=>"telefono",
				"fecha_sistema"=>"fecha_sistema",
				
				"valor_encabezado_nform"=>"valor_encabezado",
				"valor_letra_encabezado"=>"valor_letra_encabezado",
				"periodo_encabezado"=>"periodo_encabezado",
				
				"valor_fechas_nform"=>"valor_fechas",
				
				"nombres"=>"nombres",
				
				"tabla_fecha_pago"=>"tabla_fecha_pago",
		);
		
		//Datos referencia uno
		$plantillaHtml = Util::reempDatosPlant($plantillaHtml,$arrIndiceModelo,$datos);
		
		return $plantillaHtml;
	}
}

?>