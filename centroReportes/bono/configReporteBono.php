<?php
/* autor:       Luis Rojas
 * fecha:       11/12/2015
 * objetivo:    configuracion de los reportes de bono educativo
 **********************************************************
 configuracion
 tipo 
 1. año y agencia

 
*/

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$usuario=$_SESSION['CEDULA'];
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
$tipo=$_REQUEST['tipo'];
$titulo='...';
$rept=$_REQUEST['tit'];
switch($rept){
	case 1:$titulo='BONOS REDIMIDOS POR A&ntilde;O Y AGENCIA'; break;
	}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Men&uacute; Reportes Bono Educativo</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link type="text/css" rel="stylesheet" href="../../css/css.1.8/jquery-ui-1.8.17.custom.css" />
<link type="text/css" href="../../css/Estilos.css" rel="stylesheet"/>
<link href="../../css/marco.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="../../js/1.6/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="../../js/1.6/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="../../js/comunes.js"></script>
<script type="text/javascript" src="js/reporte.js"></script>
<script>
$(function(){
	$("#txtFechaI,#txtFechaF").datepicker({maxDate:"+0D",
		changeMonth: true,
		changeYear: true
	});
	$("#periodop").datepicker({
		dateFormat: 'yymm',
		changeMonth: true,
		changeYear: true,
		showButtonPanel: true,
		onClose: function(dateText, inst) {
			var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
			var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
			$(this).datepicker('setDate', new Date(year, month, 1));
		}
	});
	$("#periodop").focus(function () {
		$(".ui-datepicker-calendar").hide();
		$("#ui-datepicker-div").position({
			my: "center top",
			at: "center bottom",
			of: $(this)
		});
	}); 
	
});
</script>
</head>
<body>
	<center>
	<table width="100%" border="0">
	<tr>
	<td align="center" ><img src="../../imagenes/logo_reporte.png" width="362" height="70"></td>
	</tr>
	<tr>
	<td align="center" ><?php echo $titulo ?></td>
	</tr>
	</table>
	<br>
<?php
if($tipo==1){
	
?>
	<div id="tipo1" style="display:block">
	<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
	<tr>
    <th colspan="3"><strong>Parametros de Configuracion Reporte</strong></th>
    </tr>
    <tr>
      <td width="20%">&nbsp;</td>
      <td width="16%">A&NtildeO</td>
      <td width="64%">
      <select id="txtAnno" name="txtAnno" class="box1">
          <option value="2016">2016</option>
      </select>
      </td>
    </tr>
    <tr>
      <td colspan="3">&nbsp;</td>
    </tr>
    <tr>
      <td colspan="3">&nbsp;</td>
  </tr>
    <tr>
      <td colspan="3" style="text-align:center"><label class="RojoGrande" style="cursor:pointer" onClick="procesar1();">Procesar</label></td>
    </tr>
    </table>
 <?php
}

?>
<?php
if($tipo==51){
	
?>
	<div id="tipo2" style="display:block">
	<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
	<tr>
    <th colspan="3"><strong>Parametros de Configuracion Reporte</strong></th>
    </tr>
    <tr>
      <td style="text-align:center;" width="100%" >
			Motivo: 
			<select name="cmbMotivo" class="boxmediano" id="cmbMotivo">
					<option value="0">TODOS</option>
					<option value="01">01 EMPRESA HIZO APORTE EXTEMPORANEO</option>
					<option value="02">02 EMPRESA PAGO APORTE CON CEDULA ERRADA</option>
					<option value="03">03 EMPRESA MANDO HORAS INCORRECTAS</option>
					<option value="04">04 RETROACTIVIDAD</option>
					<option value="05">05 ERROR AL RETIRAR EN NOMINA</option>
					<option value="06">06 BANDERA DE NO GIRO TRABAJADOR Y/O BENEFICIARIO</option>
					<option value="07">07 ERROR FECHA DE NACIMIENTO DEL BENEFICIARIO</option>
			        <option value="08">08 SISTEMA TOMO EL INGRESO BASE PARA EL GIRO</option>
					<option value="09">09 BENEFICIARIO NO MIGRADO</option>
					<option value="10">10 CARACTERES ESPECIALES EN EL NOMBRE</option>
					<option value="11">11 BENEFICIARIO DISCAPACITADO</option>
					<option value="12">12 AFILIADO INACTIVO</option>
					<option value="13">13 GIRO OCTUBRE POR NIT DIFERENTE AL QUE APORTA EL MES DE NOVIMEBRE</option>
					<option value="14">14 TIENE DOS PU</option>
					<option value="15">15 LA EMPRESA QUE APORTE SI ES LA MISMA QUE LO TIENE AFILIADO</option>
					<option value="16">16 SOPORTA DOCUMENTOS A 31 DE DICIEMBRE DE 2015</option>
					<option value="99"> OTROS</option>
			</select>
		</td>
    </tr>
    <tr>
      <td colspan="3" style="text-align:center"><label class="RojoGrande" style="cursor:pointer" onClick="procesar2();">Procesar</label></td>
    </tr>
    </table>
 <?php
}

?>
<input type="hidden" id="txtusuario" name="txtusuario" value="<?php echo $usuario; ?>" /> 
<input type="hidden" id="txtRept" name="txtRept" value="<?php echo $rept; ?>" />
</body>

</html>