<?php
/* autor:       orlando puentes
 * fecha:       28/09/2010
 * objetivo:    
 */


$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Menú Reportes Secretaria</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="../../../css/estiloReporte.css" rel="stylesheet" type="text/css">
<link type="text/css" rel="stylesheet" href="../../../css/css.1.8/jquery-ui-1.8.17.custom.css" />
<link type="text/css" href="../../../css/Estilos.css" rel="stylesheet"/>
<link href="../../../css/marco.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../../../js/1.6/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="../../../js/1.6/jquery-ui-1.8.16.custom.min.js"></script>
<script language="javascript" src="../../../js/comunes.js"></script>
<script type="text/javascript" src="js/reportes.js"></script>
<script type="text/javascript">
shortcut.add("Shift+F",function() {
		var URL=src();
		var url=URL+"aportes/trabajadores/consultaTrabajador.php";
    	window.open(url,"_blank");
    },{
	'propagate' : true,
	'target' : document 
});        
		
function bono(){
	var bono = prompt("Digite el Numero de Bono");
	url= "reporte001.php?v0="+bono;
	window.open(url,'_blank');
}
</script>
</head>
<body>
<center>
  <table width="100%" border="0">
  <tr>
    <td align="center" ><img src="../../../centroReportes/imagenes/razonSocialRpt.png" width="518" height="91" /></td>
  </tr>
  <tr>
    <td align="center" >&nbsp;</td>
  </tr>
  <tr>
    <td class="fecha" ></td>
  </tr>
</table>

<table width="60%" border="0" cellspacing="1" class="tablero">
<tr><th>Reporte</th>
<th>Listado de Reportes de bonos para Computadores</th></tr>  
     <tr>
      	<td scope="row" align="left" width="20%">Reporte002</td>
     	<td align="left">
      		<label onclick="reporte002();" target="_blank" style="cursor:pointer">Saldo Fondo Bono Computadores Aprender</label>
      	</td>
    </tr>
    <tr>
      <td scope="row">Reporte003</td>
      <td><a onclick="reporte003();" target="_blank" style="cursor:pointer" >Listado de Bonos Asignados General</a></td>
    </tr>
	<tr>
      <td scope="row">Reporte004</td>
      <td>
      <a href="configReporteBono.php?tipo=4&tit=4" style="cursor:pointer" target="_blank">Listado de Bonos por Agencia</a>
      </td>
    </tr>
    <tr>
      <td scope="row">Reporte005</td>
      <td><a href="configReporteBono.php?tipo=5&tit=5" style="cursor:pointer" target="_blank">Listado de Bonos por Fecha</a></td>
    </tr>
    <tr>
      <td scope="row">Reporte006</td>
      <td><a href="configReporteBono.php?tipo=6&tit=6" style="cursor:pointer" target="_blank">Listado de Bonos por Fecha y Agencia</a></td>
    </tr>
    <tr>
      <td scope="row">Reporte007</td>
      <td><label style="cursor:pointer" onclick="reporte007()" target="_blank">Listado de Bonos Anulados</label></td>
    </tr>
    <tr>
      <td scope="row">Reporte008</td>
      <td><a href="configReporteBono.php?tipo=8&tit=8" style="cursor:pointer" target="_blank">Reimpresion de Bonos</a></td>
    </tr>
     <tr>
      <td scope="row">Reporte009</td>
      <td><label style="cursor:pointer" onclick="reporte009()" target="_blank">Listado de Bonos Autorizados</label></td>
    </tr>
    <tr>
      <td scope="row">Reporte010</td>
      <td><label style="cursor:pointer" onclick="reporte010()" target="_blank">Listado de Bonos Entregados por Empresa</label></td>
    </tr>
  </table>
  </center>
  </body>
</html>
