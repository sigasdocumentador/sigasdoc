<?php
date_default_timezone_set('America/Bogota');
set_time_limit(0);
$raiz="";
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
include_once '../../config.php';
$path_plano =$ruta_generados.'gestion'.DIRECTORY_SEPARATOR;

$fecha=date('Ymd');
$archivo='empresa'.$fecha.'.csv';
$path_plano.=$archivo;

$fp=fopen($path_plano,'w');
//$cadena="";
$cadena="NIT;RAZON SOCIAL;CODIGO CIUU;DESCRIPCION ACTIVIDAD;ESTADO;TIPO APORTANTE;TIPO AFILIACION;VALOR PAGADO;FECHA DE PAGO\r\n";
//fwrite($fp, $cadena);
//fclose($fp);

$periodoI=$_REQUEST['periodoI'];

	$sql = "DECLARE @periodo_pago_i CHAR(6)=$periodoI, @periodo_pago_f CHAR(6)
		SET @periodo_pago_f = convert(CHAR(6),DATEADD(mm,-1,convert(DATE,@periodo_pago_i + '01',112)),112)
		SELECT a48.idempresa,a48.nit,a48.razonsocial,a48.actieconomicadane,a79.descripcion,a48.estado,a91.detalledefinicion,b91.detalledefinicion as tipoAfiliacion,sum(a11.valorpagado) AS valorpagado,a11.fechapago
		,(SELECT count(*) 
			FROM aportes011 b11 
				WHERE CONVERT(CHAR(6),b11.fechapago,112) = @periodo_pago_f AND b11.idempresa=a48.idempresa) AS PeriodoAnt
		FROM aportes048 a48
			INNER JOIN aportes011 a11 ON a11.idempresa=a48.idempresa
			left join aportes079 a79 on a79.clase=a48.actieconomicadane and a79.clase!='' and len(a79.clase)>='4'
			left join aportes091 a91 on a48.tipoaportante=a91.iddetalledef
			left join aportes091 b91 on a48.idtipoafiliacion=b91.iddetalledef
		WHERE (SELECT count(*) 
					FROM aportes011 b11 
						WHERE CONVERT(CHAR(6),b11.fechapago,112) = @periodo_pago_f AND b11.idempresa=a48.idempresa)=0 
						AND convert(CHAR(6),a11.fechapago,112) = @periodo_pago_i
	  	GROUP BY a48.idempresa,a48.nit,a48.razonsocial,a48.actieconomicadane,a79.descripcion,a48.estado,a91.detalledefinicion,b91.detalledefinicion,a11.fechapago
		ORDER BY a48.nit DESC";

$rs=$db->querySimple($sql);
while (($row=$rs->fetch())==true){
	$ide = $row["idempresa"];
	$nit = $row["nit"];
	$razonsocial = $row["razonsocial"];
	
	$actieconomicadane = $row["actieconomicadane"];
	$descripcion = $row["descripcion"];
	$estado = $row["estado"];
	$detalledefinicion = $row["detalledefinicion"];
	$tipoAfiliacion = $row["tipoAfiliacion"];
	
	$valorpagado = $row["valorpagado"];
	$fechapago = $row["fechapago"];
	
	$cadena .= "$nit;$razonsocial;$actieconomicadane;$descripcion;$estado;$detalledefinicion;$tipoAfiliacion;$valorpagado;$fechapago"."\r\n";
}

$fp=fopen($path_plano,'a');
fwrite($fp, $cadena);
fclose($fp);

$_SESSION['ENLACE']=$path_plano;
$_SESSION['ARCHIVO']=$archivo;
echo 1;
?>