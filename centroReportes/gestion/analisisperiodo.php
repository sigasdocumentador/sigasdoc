<?php
date_default_timezone_set('America/Bogota');
set_time_limit(0);
$raiz="";
//ini_set("display_errors",'1');
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
include_once '../../config.php';
$path_plano =$ruta_generados.'gestion'.DIRECTORY_SEPARATOR;

$fecha=date('Ymd');
$archivo='empresa'.$fecha.'.csv';
$path_plano.=$archivo;

$fp=fopen($path_plano,'w');
$cadena="NIT;RAZON SOCIAL;CODIGO CIUU;DESCRIPCION ACTIVIDAD;ESTADO;TIPO APORTANTE;TIPO AFILIACION;PERIODOS;VALOR PAGADO;FECHA DE PAGO\r\n";


$fechaI=$_REQUEST['fechaI'];
$fechaF=$_REQUEST['fechaF'];

$sql = "DECLARE @fecha_pago_i DATE='$fechaI', @fecha_pago_f DATE ='$fechaF' 
		SELECT a48.idempresa,a48.nit,a48.razonsocial,a48.actieconomicadane,a79.descripcion,a48.estado,a91.detalledefinicion,b91.detalledefinicion as tipoAfiliacion,sum(a11.valorpagado) AS valorpagado,a11.periodo,a11.fechapago
		FROM aportes048 a48
			INNER JOIN aportes011 a11 ON a11.idempresa=a48.idempresa
			left join aportes079 a79 on a79.clase=a48.actieconomicadane and a79.clase!='' and len(a79.clase)>='4'
			left join aportes091 a91 on a48.tipoaportante=a91.iddetalledef
			left join aportes091 b91 on a48.idtipoafiliacion=b91.iddetalledef
		WHERE fechapago BETWEEN @fecha_pago_i AND @fecha_pago_f
		GROUP BY a48.idempresa,a48.nit,a48.razonsocial,a48.actieconomicadane,a79.descripcion,a48.estado,a91.detalledefinicion,b91.detalledefinicion,a11.valorpagado,a11.periodo,a11.fechapago
		ORDER BY a48.nit, a11.periodo DESC";

$rs=$db->querySimple($sql);
while (($row=$rs->fetch())==true){
	$ide = $row["idempresa"];
	$nit = $row["nit"];
	$razonsocial = $row["razonsocial"];
	
	$actieconomicadane = $row["actieconomicadane"];
	$descripcion = $row["descripcion"];
	$estado = $row["estado"];
	$detalledefinicion = $row["detalledefinicion"];
	$tipoAfiliacion = $row["tipoAfiliacion"];
	
	$valorpagado = $row["valorpagado"];
	$periodo = $row["periodo"];
	$fechapago = $row["fechapago"];
	
	$cadena .= "$nit;$razonsocial;$actieconomicadane;$descripcion;$estado;$detalledefinicion;$tipoAfiliacion;$periodo;$valorpagado;$fechapago"."\r\n";
}

$fp=fopen($path_plano,'a');
fwrite($fp, $cadena);
fclose($fp);

$_SESSION['ENLACE']=$path_plano;
$_SESSION['ARCHIVO']=$archivo;
echo 1;
?>