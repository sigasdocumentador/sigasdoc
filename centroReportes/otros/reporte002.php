<?php
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$fecha=date('Ymd');
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

//Recibo los parametros del formulario
$agencia=$_REQUEST['agencia'];
$tamanio=$_REQUEST['tamanio'];

include_once '../../config.php';
//include_once('clases/reportes.otros.class.php');

$archivo="empresas_por_tamanio.csv";
$path_plano=$ruta_generados.$archivo;
$f=fopen($path_plano,"w");
fclose($f);

$cadena="NIT;EMPRESA;DIRECCION;TELEFONO;CORREO;MUNICIPIO;DEPARTAMENTO;REPRESENTANTE;SECCIONAL;TRABAJADORES;TIPOAFILIACION;FECHAAFILIACION\r\n";
$f=fopen($path_plano,"a");//a = append
fwrite($f,$cadena);
fclose($f);

//$objReporte=new Reportes_otros();

if($agencia==0){
$sql="SELECT DISTINCT aportes048.nit,aportes048.razonsocial,aportes048.direccion, 
aportes048.telefono,aportes048.email
,isnull((select top 1 a89.municipio from aportes089 a89 where a89.codmunicipio=aportes048.idciudad),'') as municipioempresa
,isnull((select top 1 a89.departmento from aportes089 a89 where a89.coddepartamento=aportes048.iddepartamento),'') as departamentoempresa
,aportes048.idrepresentante,aportes048.seccional,count(aportes016.idempresa) trabajadores,
aportes015.pnombre,aportes015.snombre,aportes015.papellido,aportes015.sapellido,a91.detalledefinicion,aportes048.fechaafiliacion FROM aportes048
INNER JOIN aportes015 ON aportes048.idrepresentante=aportes015.idpersona
INNER JOIN aportes016 ON aportes048.idempresa=aportes016.idempresa AND aportes016.estado='A'
left join aportes091 a91 on aportes048.idtipoafiliacion=a91.iddetalledef
WHERE aportes048.estado='A'";
}else{
$sql="SELECT DISTINCT aportes048.nit,aportes048.razonsocial,aportes048.direccion, 
aportes048.telefono,aportes048.email
,isnull((select top 1 a89.municipio from aportes089 a89 where a89.codmunicipio=aportes048.idciudad),'') as municipioempresa
,isnull((select top 1 a89.departmento from aportes089 a89 where a89.coddepartamento=aportes048.iddepartamento),'') as departamentoempresa
,aportes048.idrepresentante,aportes048.seccional,count(aportes016.idempresa) trabajadores,
aportes015.pnombre,aportes015.snombre,aportes015.papellido,aportes015.sapellido,a91.detalledefinicion,aportes048.fechaafiliacion FROM aportes048
INNER JOIN aportes015 ON aportes048.idrepresentante=aportes015.idpersona
INNER JOIN aportes016 ON aportes048.idempresa=aportes016.idempresa AND aportes016.estado='A'
left join aportes091 a91 on aportes048.idtipoafiliacion=a91.iddetalledef
WHERE aportes048.estado='A' and aportes048.seccional='$agencia'";
}

$cadena="";
switch($tamanio){
case "1":$cadena=' and aportes048.trabajadores > 10';break;
case "2":$cadena=' and aportes048.trabajadores > 20';break;
case "3":$cadena=' and aportes048.trabajadores > 30';break;
case "4":$cadena=' and aportes048.trabajadores > 50';break;
case "5":$cadena=' and aportes048.trabajadores > 100';break;
default: $cadena=' and aportes048.trabajadores <> 0';
}

$cadenagroup= ' GROUP BY aportes048.nit,aportes048.razonsocial,aportes048.direccion, 
aportes048.telefono,aportes048.email
,aportes048.idciudad,aportes048.iddepartamento
,aportes048.idrepresentante,aportes048.seccional,
aportes015.pnombre,aportes015.snombre,aportes015.papellido,aportes015.sapellido,a91.detalledefinicion,aportes048.fechaafiliacion';

$consulta=$sql.$cadena.$cadenagroup;
//$consulta=$objReporte->reporte002($sql);
$rs=$db->querySimple($consulta);
$w=$rs->fetch();

	while($row = $rs->fetch()){
		$nit=trim($row['nit']);
		$empresa=trim($row['razonsocial']);
		$direccion=trim($row['direccion']);
		$nombre=trim($row['pnombre'])." ".trim($row['snombre'])." ".trim($row['papellido'])." ".trim($row['sapellido']);
		$telefono=trim($row['telefono']);
 		switch($row['seccional']){
	  	case "01": $seccional="Neiva";  	break;
	  	case "02": $seccional="Garzon"; 	break;
	  	case "03": $seccional="Pitalito"; 	break;
	  	case "04": $seccional="La plata"; 	break;
	 	}
		$trabajadores=trim($row['trabajadores']);
		$email=trim($row['email']);
		$municipio=trim($row['municipioempresa']);
		$departamento=trim($row['departamentoempresa']);		
		$detalledefinicion=trim($row['detalledefinicion']);
		$fechaafiliacion=trim($row['fechaafiliacion']);
		$cadena=$nit.";".$empresa.";".$direccion.";".$telefono.";".$email.";".$municipio.";".$departamento.";".$nombre.";".$seccional.";".$trabajadores.";".$detalledefinicion.";".$fechaafiliacion."\r\n"; 
		$f=fopen($path_plano,"a");
		fwrite($f,$cadena);
		fclose($f);
	}//while
//Validar si se creo el archivo
	$enlace=$path_plano;
	$_SESSION['ENLACE']=$enlace;
	$_SESSION['ARCHIVO']=$archivo;
	echo 1;
?>