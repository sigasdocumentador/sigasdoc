<?php
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$fecha=date('Ymd');
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
$sql="select
 aportes048.nit,
 aportes048.razonsocial,
 aportes048.direccion,
 aportes048.telefono,
 aportes048.fechaafiliacion,
 count(aportes016.idempresa) as suma 
from 
 aportes048
inner join aportes016 on aportes048.idempresa = aportes016.idempresa 
where 
 aportes048.principal='S' and
 aportes048.estado='A' and
 aportes016.estado='A'
group by 
aportes048.nit,
 aportes048.razonsocial,
 aportes048.direccion,
 aportes048.telefono,
 aportes048.fechaafiliacion";
include_once '../../config.php';
 $archivo="ListadoEmpresas.csv";
 $path_plano=$ruta_generados.$archivo;
 $handle=fopen($path_plano, "w");
 fclose($handle);
 $cadena="NIT;NOMBRE EMPRESA;DIRECCION;TELEFONO;FECHA DE AFILIACION;NUMERO DE AFILIADOS\r\n";
 $handle=fopen($path_plano, "a");
 fwrite($handle, $cadena);
 fclose($handle);
 $rs=$db->querySimple($sql);
 $w=$rs->fetch();
 while ($row = $rs->fetch()){
        $cadena=$row["nit"].";".$row["razonsocial"].";".$row["direccion"].";".$row["telefono"].";".$row["fechaafiliacion"].";".$row["suma"]."\r\n";
 		$handle=fopen($path_plano, "a");
 		fwrite($handle, $cadena);
 		fclose($handle);
 }
 $enlace=$path_plano;
$_SESSION['ENLACE']=$enlace;
$_SESSION['ARCHIVO']=$archivo;

echo 1;
?>