<?php 
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
$funcionescomunes= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'funcionesComunes'. DIRECTORY_SEPARATOR . 'funcionesComunes.php';
include  $funcionescomunes;

$fecha=fecha_hora_();

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<style type="text/css">
	    *{
	       padding: 0;
	       margin: 0;
         }
        body{
	       font: normal 12px Tahoma, Verdana, Arial, sans-serif;
         }
        #divContenedor{
	       width: 400px;
	       position: absolut
	       left: 100%;
	       margin-left: 120px;
	       top: 100px;
	    }
	    #divLogo{
		   padding-bottom: 20px;
	    }
        .clsBoton{
	      padding: 7px;
	      background: #000;
	      display: inline-block;
	      border-radius: 5px;
	      text-decoration: none;
	      color: #fff;
	      box-shadow: 0 0 10px #f1f1f1;
	      border: solid 1px #000;
         }
	      .clsBoton:hover{
		     background: #fff;
		     color: #000;
		     border-color: #000;
	      }

	     .textbox{
	         border: 1px solid #DBE1EB;
	         font-size: 18px;
	         font-family: Arial, Verdana;
	         padding: 3px;
	         border-radius: 4px;
	         -moz-border-radius: 4px;
	         -webkit-border-radius: 4px;
	         -o-border-radius: 4px;
	         background: #FFFFFF;
	         background: linear-gradient(left, #FFFFFF, #F7F9FA);
	         background: -moz-linear-gradient(left, #FFFFFF, #F7F9FA);
	         background: -webkit-linear-gradient(left, #FFFFFF, #F7F9FA);
	         background: -o-linear-gradient(left, #FFFFFF, #F7F9FA);
	         color: #2E3133;
	      }
	   
	         .textbox:focus
	          {
	           color: #2E3133;
	           border-color: #FBFFAD;
	           }
	  
           .tblregobservaciones {
	           font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
               font-size: 12px;    
               margin: 0px;     
               width: 400px; 
               text-align: left;    
               border-collapse: collapse; 
           }
           .tblregobservaciones th {
                 font-size: 13px;     
                 font-weight: normal;     
                 padding: 8px;     
                 background: #b9c9fe;
                 border-top: 4px solid #aabcfe;    
                 border-bottom: 1px solid #fff; 
                 color: #039; }
           .tblregobservaciones td {    
                 padding: 8px;     
                 background: #e8edff;     
                 border-bottom: 1px solid #fff;
                 color: #669;    
                 border-top: 1px solid transparent; 
            }
            .tblregobservaciones tr:hover td { 
                 background: #d0dafd; 
                 color: #339; 
             }
	</style>
	<script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.12.0.min.js"></script>
	<script type="text/javascript" src="../../js/comunes.js"></script>
	<script>
        //funcion para limitar el numero de caracteres que se pueden ingresar en el textarea
	        contenido_textarea = "";
		    num_caracteres_permitidos = 150;
     		function valida_longitud(){
	        	  var observacion= document.getElementById("observacion");
		          num_caracteres = observacion.value.length;
		          if (num_caracteres > num_caracteres_permitidos){
		        	  observacion.value = contenido_textarea
		          }else{
		             contenido_textarea = observacion.value
		          }
		          if (num_caracteres >= num_caracteres_permitidos){
		        	  observacion.style.color="#ff0000";
		          }else{
		        	  observacion.style.color="#000000";
		          }
		}
        //fin limite de caracteres

        function _id(selector){
            return document.getElementById(selector);
        }    
	
	    function adicionar(){
	      var URL="http://localhost/sigas/";
	      var observacion= _id("observacion");
	      var efectiva=_id("efectiva");
	      if(efectiva.value==0){
	    	  alert("Debe indicar si el seguimiento fue efectivo");
	    	  efectiva.style.color="#ff0000";
	    	  efectiva.focus();
	    	  return;
		  }
          if(observacion.value==""){
             alert("Debe indicar la observaci\u00F3n");
             observacion.focus();
          }   
          else{
              var confirmacion= confirm("Esta seguro de registrar el seguimiento");
              if(confirmacion){
            	  var URL=src();
            	  var nombre=$('#nombre').val();
            	  var identificacion=$('#identificacion').val();
            	  var idtrabajador=$('#idtrabajador').val();
            	  var fecha=$('#fecha').val();
            	  var hora=$('#hora').val();
            	  var efectiva = $('#efectiva option:selected').html(); 
                  // se hace el ajax para registrar el seguimiento
            	  $.ajax({
            			type:"POST",
            			url: URL+"phpComunes/abonospenprocesar.php",
            			data:{nombre:nombre,identificacion:identificacion,idtrabajador:idtrabajador,fecha:fecha,hora:hora,solajax:2,observacion:observacion.value,efectiva:efectiva},
            			async:false,
            			dataType:"json",
            			success:function(datos){
            				if(datos.validacion>0){
            					alert("El seguimiento se registro exitosamente");
            					window.parent.document.getElementById('clsVentanaCerrar').click();
            			    }
            				else{
            				  alert("Hubo un error en la insercion"+datos.error);
            				}
            			}
            		});
            	  }
          }
	   }
	</script>
</head>
<body>
<div id="divContenedor">
	<div id="divLogo">
		
	</div>
	<div id="divContenido">
		<table border="0" align="center">
            <tbody>
 	           <tr>
 		           <td>Nombre afiliado:</td><td><input class="textbox" id="nombre" type="text" name="nombre" onfocus = "this.blur()" value="<?php echo $_GET['nombre']?>"></td>
 	            </tr>
 	             <tr>
 		            <td>Identificación:</td><td><input class="textbox" type="text" name="identificacion" onfocus = "this.blur()" id="identificacion" value="<?php echo $_GET['identificacion'];?>"></td>
 	             </tr>
 	             <tr>
 		            <td>Id Trabajador:</td><td><input class="textbox" type="text" name="idtrabajador" onfocus = "this.blur()" id="idtrabajador" value="<?php echo $_GET['idtrabajador'];?>"></td>
 	             </tr>
 	             <tr>
 		             <td>Fecha:</td><td><input class="textbox" type="text" name="fecha" onfocus = "this.blur()" id="fecha" value="<?php echo $fecha["fecha"];?>"></td>
 	             </tr>
 	             <tr>
 		             <td>Hora:</td><td><input class="textbox" type="text" name="hora" onfocus = "this.blur()" id="hora" value="<?php echo $fecha["hora"];?>"></td>
 	             </tr>
 	             <?php 
 	                 if($_GET['tipoevento']==1){
 	             ?>
 	             <tr>
 		             <td>Efectiva</td>
 		             <td>
 		                 <select name='efectiva' id="efectiva"  class="textbox" >
 		                      <option value="0">Seleccione....</option>
 		                      <option value="1">Si</option>
 		                      <option value="2">No</option>
 		                 </select>
 		             </td>
 	             </tr>
 	             <tr>
 		             <td colspan="2">Observcion:</td>
 	             </tr>
 	             <tr>
    	             <td colspan="2" align="center">
    	                <textarea id="observacion" class="textbox" rows="4" cols="30" name="txtobservacion" onKeyDown="valida_longitud()" onKeyUp="valida_longitud()"></textarea>
    	             </td>
                 </tr>
                 <tr>
    	             <td colspan="2">&nbsp;</td>
                 </tr>
                 
                 <tr>
    	             <td colspan="2" align="center"><input type="button" value='Guardar' class="btguardar clsBoton" onclick="adicionar();"></td>
                 </tr>
                 <?php 
                  }elseif ($_GET['tipoevento']==2){
                   //se invoncan los archivos necesarios para hacer la consulta a la tabla observaciones registradas
                   include_once $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'config.php';
                   include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
                   include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
                   $db=IFXDbManejador::conectarDB();
                   if($db->conexionID==null){
	                  $cadena = $db->error;
	                  echo msg_error($cadena);
	                  exit();
                   }
                   $sql="select * from aportes122 where idpersona='".$_GET['idtrabajador']."'";
                   $manejador=$db->querySimple($sql);
                   
                 ?>
                 <tr>
    	             <td colspan="2">&nbsp;</td>
                 </tr>
                 <tr>
 		             <td colspan="2">Seguimientos registrados:</td>
 	             </tr>
 	             <tr align="left">
 		             <td colspan="2">
 		             <table class="tblregobservaciones" align="left" >
 		                 <thead>
 		                      <tr>
 		                      <th>Fecha</th>
 		                      <th>Hora</th>
 		                      <th>Observaci&#243;n</th>
 		                      </tr>
 		                 </thead>
 		                 <tbody>
 		                      <?php 
 		                        while($row = $manejador->fetch()){
 		                          //$file="<td>".$rowfecha."</td><td>".$row->hora."</td>"."</td><td>".$row->observacion."</td>";
 		                          //echo $file;
 		                          echo "<tr><td>".$row['fecha']."</td><td>".substr($row['hora'],0,8)."</td><td>".$row['observacion']."</td></tr>";
 		                          
 		                        }
 		                      ?>
 		                      
 		                 </tbody>
 		             </table>
 		             </td>
 	             </tr>
                 <?php  	
                  }
                 ?>
                 
           </tbody>
       </table>
	</div>
</div>
 </body>
</html>