<?php
/* autor:       orlando puentes
 * fecha:       28/08/2012
 * objetivo:    configuracion de los reportes de radicacion
 **********************************************************
 configuracion
 tipo 
 1. año y agencia

 
*/

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$usuario=$_SESSION['CEDULA'];
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
$tipo=$_REQUEST['tipo'];
$titulo='...';
$rept=$_REQUEST['tit'];
switch($rept){
	case 1:$titulo='PAQUETES ENTREGADOS POR AGENCIA'; break;
	case 2:$titulo=''; break;
	case 3:$titulo=''; break;
	case 4:$titulo=''; break;
	case 5:$titulo='Cargue Tarjetas DF0703'; break;
	case 6:$titulo='Informe Reversos No Aplican'; break;
	}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Men&uacute; Reportes Asopagos</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link type="text/css" rel="stylesheet" href="../../css/css.1.8/jquery-ui-1.8.17.custom.css" />
<link type="text/css" href="../../css/Estilos.css" rel="stylesheet"/>
<link href="../../css/marco.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="../../js/1.6/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="../../js/1.6/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="../../js/comunes.js"></script>
<script type="text/javascript" src="js/reporte.js"></script>
<script>
$(function(){
	$("#txtFechaI,#txtFechaF").datepicker({maxDate:"+0D",
		changeMonth: true,
		changeYear: true
	});
	$("#periodop").datepicker({
		dateFormat: 'yymm',
		changeMonth: true,
		changeYear: true,
		showButtonPanel: true,
		onClose: function(dateText, inst) {
			var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
			var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
			$(this).datepicker('setDate', new Date(year, month, 1));
		}
	});
	$("#periodop").focus(function () {
		$(".ui-datepicker-calendar").hide();
		$("#ui-datepicker-div").position({
			my: "center top",
			at: "center bottom",
			of: $(this)
		});
	}); 
	
});
</script>
</head>
<body>
	<center>
	<table width="100%" border="0">
	<tr>
	<td align="center" ><img src="../../imagenes/logo_reporte.png" width="362" height="70"></td>
	</tr>
	<tr>
	<td align="center" ><?php echo $titulo ?></td>
	</tr>
	</table>
	<br>
<?php
if($tipo==1){
	
?>
	<div id="tipo1" style="display:block">
	<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
	<tr>
    <th colspan="3"><strong>Parametros de Configuracion Reporte</strong></th>
    </tr>
    <tr>
      <td width="20%">&nbsp;</td>
      <td width="16%">A&NtildeO</td>
      <td width="64%">
      <select id="txtAnno" name="txtAnno" class="box1">
          <option value="2010">2010</option>
          <option value="2011">2011</option>
          <option value="2012">2012</option>
          <option value="2013">2013</option>
      </select>
      </td>
    </tr>
    <tr>
      <td colspan="3">&nbsp;</td>
    </tr>
    <tr>
      <td colspan="3">&nbsp;</td>
  </tr>
    <tr>
      <td colspan="3" style="text-align:center"><label class="RojoGrande" style="cursor:pointer" onClick="procesar1();">Procesar</label></td>
    </tr>
    </table>
 <?php
}
if($tipo==2){
?>
    <div id="tipo2" style="display:block">
    <table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
	<tr>
    <th colspan="4"><strong>Parámetros de Configuracion Reporte</strong></th>
    </tr>
	<tr>
	  <td colspan="4" style="text-align:center">Fecha del informe: <input type="text" id="txtFechaI" name="txtFechaI" readonly></td>
	  </tr>
	<tr>
	  <td colspan="4">&nbsp;</td>
	  </tr>
	<tr>
	  <td colspan="4" style="text-align:center"><label class="RojoGrande" style="cursor:pointer" onClick="procesar2();">Procesar</label></td>
	  </tr>
    </table>
<?php
}
if($tipo==3){
?>
    <div id="tipo3" style="display:block">
<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
	<tr>
    <th><strong>Parámetros de Configuracion Reporte</strong></th>
    </tr>
	<tr>
	  <td style="text-align:center">Agencia:
	     <select id="txtSeccional" name="txtSeccional" class="box1">
          <option value="0">Todas</option>
          <option value="01">Neiva</option>
          <option value="02">Garzon</option>
          <option value="03">Pitalito</option>
          <option value="04">La plata</option>
      </select>
      </td>
  </tr>
	<tr>
	  <td>&nbsp;</td>
  </tr>
	<tr>
	  <td style="text-align:center">
      <label style="cursor:pointer" onClick="procesar3(1);"><img src="../../imagenes/icono_pdf.png" width="32" height="32"></label> &nbsp;&nbsp;
      <label style="cursor:pointer" onClick="procesar3(2);"><img src="../../imagenes/icono_excel.png" width="32" height="32"></label></td>
  </tr>
    </table>
<?php
}
if($tipo==4){
?>
    <div id="tipo4" style="display:block">
    <table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
	<tr>
    <th colspan="2"><strong>Parámetros de Configuracion Reporte</strong></th>
    </tr>
	<tr>
	  <td style="text-align:center">Agencia:
        </td>
	  <td ><select id="txtSeccional" name="txtSeccional" class="box1">
	    <option value="0">Todas</option>
	    <option value="01">Neiva</option>
	    <option value="02">Garzon</option>
	    <option value="03">Pitalito</option>
	    <option value="04">La plata</option>
      </select></td>
	  </tr>
	<tr>
	  <td style="text-align:center">Fecha Inventario:&nbsp;</td>
	  <td ><input type="text" id="txtFechaI" name="txtFechaI" readonly></td>
	  </tr>
	<tr>
	  <td colspan="2" style="text-align:center">
      <label style="cursor:pointer" onClick="procesar4(1);"><img src="../../imagenes/icono_pdf.png" width="32" height="32"></label> &nbsp;&nbsp;
      <label style="cursor:pointer" onClick="procesar4(2);"><img src="../../imagenes/icono_excel.png" width="32" height="32"></label></td>
	  </tr>
    </table>
<?php
}
if ($tipo==5){
?>
 <div id="tipo4" style="display:block">
    <table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
	<tr>
    <th colspan="2"><strong>Parámetros de Configuracion Reporte</strong></th>
    </tr>
	<tr>
	  <td style="text-align:center">Periodo Proceso:&nbsp;<input type="text" size="10" readonly id="periodop" name="periodop" ></td>
	  </tr>
	<tr>
	  <td colspan="2" style="text-align:center">
      <label style="cursor:pointer" onClick="procesar5();"><input type="button" value="Generar" class="ui-state-default"/></label> &nbsp;&nbsp;</td>
	  </tr>
    </table>
<?php
}
if ($tipo==6){
?>
 <div id="tipo4" style="display:block">
    <table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablero">
	<tr>
    <th colspan="2"><strong>Parámetros de Configuracion Reporte</strong></th>
    </tr>
	<tr>
	  <td style="text-align:center;">Fecha Inicial:&nbsp;<input type="text" readonly id="txtFechaI" size="12" name="txtFechaI" >
	  
	 Fecha Final:&nbsp;<input size="12" type="text" readonly id="txtFechaF" name="txtFechaF" ></td>
	  </tr>
	<tr>
	  <td colspan="2" style="text-align:center">
      <label style="cursor:pointer" onClick="procesar6();"><input type="button" value="Generar" class="ui-state-default"/></label> &nbsp;&nbsp;</td>
	  </tr>
    </table>
<?php
}
?>

<input type="hidden" id="txtusuario" name="txtusuario" value="<?php echo $usuario; ?>" /> 
<input type="hidden" id="txtRept" name="txtRept" value="<?php echo $rept; ?>" />
</body>

</html>