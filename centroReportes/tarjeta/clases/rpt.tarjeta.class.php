<?php
/* autor:       orlando puentes
 * fecha:       28/09/2010
 * objetivo:     
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once("rsc/conexion.class.php");

class RptTarjeta{
 //constructor
var $con;
function RptTarjeta(){
 		$this->con=new DBManager;
 	}

function contar_bonos(){
    if($this->con->conectar()==true){
        $sql="select count(*) as cuenta from aportes105";
	return mssql_query($sql,$this->con->conect);
	}
}

function contar_bonos_asignados(){
    if($this->con->conectar()==true){
        $sql="select count(*) as cuenta from aportes105 where asignada='S'";
	return mssql_query($sql,$this->con->conect);
	}
}

function contar_bonos_NO_asignados(){
    if($this->con->conectar()==true){
        $sql="select count(*) as cuenta from aportes105 where asignada='N'";
	return mssql_query($sql,$this->con->conect);
	}
}

function contar_bonos_activos(){
    if($this->con->conectar()==true){
        $sql="select count(*) as cuenta from aportes105 where estado='A'";
	return mssql_query($sql,$this->con->conect);
	}
}

function contar_bonos_inactivos(){
    if($this->con->conectar()==true){
        $sql="select count(*) as cuenta from aportes105 where estado='I'";
	return mssql_query($sql,$this->con->conect);
	}
}

function periodo_actual(){
	if($this->con->conectar()==true){
		$sql="SELECT top 1 periodo FROM aportes012 WHERE procesado='N'";
		return mssql_query($sql,$this->con->conect);
	}
}

function valores_cargados_1($per){
	if($this->con->conectar()==true){
		$sql="SELECT sum(valor) AS suma FROM aportes104 WHERE tipopago='T' AND periodogiro='$per' AND procesado='S'";
		return mssql_query($sql,$this->con->conect);
	}	
}

function valores_cargados_2($per){
	if($this->con->conectar()==true){
		$sql="SELECT sum(valor) AS suma FROM aportes104 WHERE tipopago='E' AND periodogiro='$per' AND procesado='S'";
		return mssql_query($sql,$this->con->conect);
	}
}

function valores_cargados_3($per){
	if($this->con->conectar()==true){
		$sql="SELECT sum(valor) AS suma FROM aportes104 WHERE tipopago='T' AND periodogiro='$per' AND procesado='N'";
		return mssql_query($sql,$this->con->conect);
	}
}

function valores_cargados_4($per){
	if($this->con->conectar()==true){
		$sql="SELECT sum(valor) AS suma FROM aportes104 WHERE tipopago='E' AND periodogiro='$per' AND procesado='N'";
		return mssql_query($sql,$this->con->conect);
	}
}

function archivos_envio_asesor(){
	if($this->con->conectar()==true){
		$sql="SELECT idenvio,aportes130.fechasistema,razonsocial,cantidad, (today-aportes130.fechasistema) AS dias, codigosucursal, CASE WHEN aportes130.tipoenvio = 1 THEN 'Asesor' WHEN aportes130.tipoenvio= 2 THEN 'Correo' END AS tipo FROM aportes130 INNER JOIN aportes048 ON aportes130.idempresa = aportes048.idempresa WHERE aportes130.estado='A'";
		return mssql_query($sql,$this->con->conect);
	}
	}



}
?>