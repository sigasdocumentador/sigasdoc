<?php
/* autor:       orlando puentes
 * fecha:       28/09/2010
 * objetivo:    
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$usuario=$_SESSION['USUARIO'];
$menu="../menuAsopagos.php";
include_once $raiz.DIRECTORY_SEPARATOR ."config.php";
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Men&uacute; Reportes de subsidio cuota monetaria</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="../../css/estiloReporte.css" rel="stylesheet" type="text/css">
<script src="../../js/comunes.js" type="text/javascript"></script>
<script type="text/javascript" src="js/reporte.js"></script>
</head>
<body>
<center>
  <table width="100%" border="0">
  <tr>
    <td align="center" ><img src="../../imagenes/razonSocial.png" width="615" height="55"></td>
  </tr>
  <tr>
    <td align="center" >&nbsp;</td>
  </tr>
  <tr>
    <td class="fecha" ></td>
  </tr>
</table>
<table width="60%" border="0" cellspacing="1" class="tablero">
	<tr><th>Reporte</th>
	<th>Listado de Reportes de Tarjeta ASOPAGOS</th></tr>  
	  <tr>
	    <td scope="row" align="left">Reporte001</td>
	    <td align="left"><a style="text-decoration:none" target='_blank' ">Listado de tarjetas activas</a>
	    <label style="cursor:pointer" onClick="procesar7('PDF','<?php echo $usuario; ?>');" ><img src="../../imagenes/pdf16x16.png" /></label>
    	<label style="cursor:pointer" onClick="procesar7('EXC','<?php echo $usuario; ?>');" ><img src="../../imagenes/excel16x16.png" /></label>
	    </td>
	  </tr>
	 <tr>
	  <td scope="row" align="left" width="20%">Reporte002</td>
	  <td align="left"><a style="text-decoration:none" target='_blank' ">Listado de tarjetas pendientes de entregar</a>
	  <label style="cursor:pointer" onClick="procesar8('PDF','<?php echo $usuario; ?>');" ><img src="../../imagenes/pdf16x16.png" /></label>
      <label style="cursor:pointer" onClick="procesar8('EXC','<?php echo $usuario; ?>');" ><img src="../../imagenes/excel16x16.png" /></label>
	  </td>
	</tr>
	 <tr>
	   <td scope="row" align="left">Reporte003</td>
	   <td align="left"><a style="text-decoration:none" target='_blank' href="<?php echo $ruta_reportes;?>asopagos/reporte003.jsp?v0=<?php echo $usuario; ?>">Inventario de bonos y plasticos</a></td>
    </tr>
	 <tr>
	   <td scope="row" align="left">Reporte004</td>
	   <td align="left"><a style="text-decoration:none" target='_blank' href="<?php echo $ruta_reportes;?>asopagos/reporte004.jsp?v0=<?php echo $usuario; ?>">Listado de Traslados de dinero pendientes de procesar</a></td>
    </tr>
	 <tr>
	   <td scope="row" align="left">Reporte005</td>
	   <td align="left"><a style="text-decoration:none" target='_blank' href="../configurarReporte.php?tipo=<?php echo 2;?>&modulo=asopagos&v0=<?php echo '005';?>&menurpte=asopagos/menuAsopagos.php&plano=<?php echo 1;?>" >Listado de Transacciones por Negocio  por fecha</a></td>
    </tr>
	 <tr>
	   <td scope="row" align="left">Reporte006</td>
	   <td align="left"><a style="text-decoration:none" target='_blank' href="../configurarReporte.php?tipo=<?php echo 2;?>&modulo=asopagos&v0=<?php echo '006';?>&menurpte=asopagos/menuAsopagos.php&plano=<?php echo 1;?>" >Listado Detallado de Transacciones  por fecha</a></td>
    </tr>
	 <tr>
	   <td scope="row" align="left">Reporte007</td>
	   <td align="left"><a style="text-decoration:none" target='_blank' href="../configurarReporte.php?tipo=<?php echo 1;?>&modulo=asopagos&v0=<?php echo '007';?>&menurpte=asopagos/menuAsopagos.php&plano=<?php echo 0;?>" >Listado de Entrega de Tarjetas por usuario por fecha</a></td>
    </tr>
	 <tr>
	   <td scope="row" align="left">Reporte008</td>
	   <td align="left"><a style="text-decoration:none" target='_blank' href="<?php echo $ruta_reportes;?>asopagos/reporte008.jsp?v0=<?php echo $usuario; ?>">Consolidado de entregas de tarjetas</a></td>
    </tr>
	 <tr>
	   <td scope="row" align="left">Reporte009</td>
	   <td align="left"><a style="text-decoration:none" target='_blank' href="<?php echo $ruta_reportes;?>asopagos/reporte009.jsp?v0=<?php echo $usuario; ?>">Inventario tarjetas</a></td>
    </tr>
	 <tr>
	   <td scope="row" align="left">Reporte010</td>
	   <td align="left"><a style="text-decoration:none" target='_blank' href="<?php echo $ruta_reportes;?>asopagos/reporte010.jsp?v0=<?php echo $usuario; ?>">Listado de Tarjetas Pendienrtes de Imprimir</a></td>
    </tr>
	 <tr>
	   <td scope="row" align="left">Reporte011</td>
	   <td align="left"><a style="text-decoration:none" target='_blank' href="<?php echo $ruta_reportes;?>asopagos/reporte011.jsp?v0=<?php echo $usuario; ?>">Listado de Saldos pendientes de trasladar de REDEBAN</a></td>
    </tr>
	 <tr>
	   <td scope="row" align="left">Reporte012</td>
	    <td align="left"><a style="text-decoration:none" target='_blank' href="../configurarReporte.php?tipo=<?php echo 2;?>&modulo=asopagos&v0=<?php echo '012';?>&menurpte=asopagos/menuAsopagos.php&plano=<?php echo 0;?>" >Traslados de saldos de REDEBAN a ASOPAGOS por fecha</a></td>
    </tr>
	<tr>
	   <td scope="row" align="left">Reporte013</td>
	   <td align="left"><a style="text-decoration:none" target='_blank' href="../configurarReporte.php?tipo=<?php echo 29;?>" >Listado de Tarjetas Asignadas (general)</a></td>
    </tr>
	 <tr>
	   <td scope="row" align="left">Reporte014</td>
	   <td align="left"><a style="text-decoration:none" target='_blank' href="<?php echo $ruta_reportes;?>asopagos/reporte014.jsp?v0=<?php echo $usuario; ?>">Tarjetas BLOQUEADAS pendientes de procesar</a></td>
    </tr>
	 <tr>
	   <td scope="row" align="left">Reporte015</td>
	   <td align="left"><a style="text-decoration:none" target='_blank' href="<?php echo $ruta_reportes;?>asopagos/reporte015.jsp?v0=<?php echo $usuario; ?>">Listado de Reversos pendientes de procesar</a></td>
    </tr>
	 <tr>
	   <td scope="row" align="left">Reporte016</td>
	   <td align="left"><a style="text-decoration:none" target='_blank' href="../configurarReporte.php?tipo=<?php echo 26;?>" >Listado de ABONOS pendientes de procesar</a></td>
    </tr>
	 <tr>
	   <td scope="row" align="left">Reporte017</td>
	   <td align="left"><a style="text-decoration:none" target='_blank' href="<?php echo $ruta_reportes;?>asopagos/reporte017.jsp?v0=<?php echo $usuario; ?>">Actualizacion de Datos del Maestro pendiente de procesar</a></td>
    </tr>
	 <tr>
	   <td scope="row" align="left">Reporte018</td>
	   <td align="left"><a style="text-decoration:none" target='_blank' href="<?php echo $ruta_reportes;?>asopagos/reporte018.jsp?v0=<?php echo $usuario; ?>">Listado de Reexpediciones de tarjetas pendientes de procesar</a></td>
    </tr>
	 <tr>
	   <td scope="row" align="left">Reporte019</td>
	   <td align="left"><a style="text-decoration:none" target='_blank' href="<?php echo $ruta_reportes;?>asopagos/reporte019.jsp?v0=<?php echo $usuario; ?>">Actualizacion de Datos del Maestro pendientes de procesar por Reexpedicion</a></td>
    </tr>
	 <tr>
	   <td scope="row" align="left">Reporte020</td>
	   <td align="left"><a style="text-decoration:none" target='_blank' href="../configurarReporte.php?tipo=<?php echo 2;?>&modulo=asopagos&v0=<?php echo '020';?>&menurpte=asopagos/menuAsopagos.php&plano=<?php echo 1;?>" >Listado de Transacciones Totales  por fecha</a></td>
    </tr>
	 <tr>
	   <td scope="row" align="left">Reporte021</td>
        <td align="left"><a style="text-decoration:none" target='_NEW' href="configReporteAportes.php?tipo=4&tit=1">Inventario Realizado en Agencias</a></td>
	 </tr>
	 <tr>
	   <td scope="row" align="left">Reporte022</td>
	   <td align="left"><a style="text-decoration:none" target='_NEW' href="configReporteAportes.php?tipo=5&tit=5">Cargue Tarjetas DF0703</a></td>
    </tr>
    <tr>
	   <td scope="row" align="left">Reporte023</td>
	   <td align="left"><a style="text-decoration:none" target='_NEW' href="configReporteAportes.php?tipo=6&tit=6">Informe Reversos No Aplican</a></td>
    </tr>
    <tr>
	   <td scope="row" align="left">Reporte024</td>
	   <td align="left"><a style="text-decoration:none" target='_blank' href="../configurarReporte.php?tipo=<?php echo 27;?>">Saldos no cobrados</a></td>
    </tr>
    <tr>
    <td scope="row" alingn="left">Reporte025</td>
    <td align="left"><a style="text-decoration:none" target='_blank' href="../configurarReporte.php?tipo=<?php echo 25;?>" >Informes Prescripciones</a></td>
    </tr>
    <tr>
	   <td scope="row" align="left">Reporte026</td>
	   <td align="left"><a style="text-decoration:none" target='_blank' href="../configurarReporte.php?tipo=<?php echo 28;?>" >Listado de Tarjetas Asignadas (detallado)</a></td>
    </tr>
     <tr>
	   <td scope="row" align="left">Reporte027</td>
	   <td align="left"><a style="text-decoration:none" target='_blank' href="../configurarReporte.php?tipo=<?php echo 40;?>" >Inventario de Tarjetas Entregadas por Agencia</a></td>
    </tr>
    <tr>
	   <td scope="row" align="left">Reporte028</td>
	   <td align="left"><a style="text-decoration:none" target='_blank' href="../configurarReporte.php?tipo=<?php echo 42;?>" >Consolidado de Tarjetas Reexpedidas por Aegncia</a></td>
    </tr>
     <tr>
	   <td scope="row" align="left">Reporte029</td>
	   <td align="left"><a style="text-decoration:none" target='_blank' href="../configurarReporte.php?tipo=<?php echo 43;?>" >Informe Reversos No Aplican sin Bono</a></td>
    </tr>
    <tr>
	   <td scope="row" align="left">Reporte030</td>
	   <td align="left"><a style="text-decoration:none" target='_blank' href="../configurarReporte.php?tipo=<?php echo 44;?>" >Informe Solicitud Reseteo Clave Tarjetas</a></td>
    </tr>
    <tr>
	   <td scope="row" align="left">Reporte031</td>
	   <td align="left"><a style="text-decoration:none" target='_blank' href="../configurarReporte.php?tipo=<?php echo 45;?>" >Informe de prescripciones con fecha de corte</a></td>
    </tr>
     <tr>
	    <td scope="row" align="left">Reporte032</td>
	    <td align="left"><a style="text-decoration:none" target='_blank' ">Listado Bonos Pendientes Por Cancelar</a>
	    <label style="cursor:pointer" onClick="procesar14('PDF','<?php echo $usuario; ?>');" ><img src="../../imagenes/pdf16x16.png" /></label>
    	<label style="cursor:pointer" onClick="procesar14('EXC','<?php echo $usuario; ?>');" ><img src="../../imagenes/excel16x16.png" /></label>
	    </td>
	 </tr>
	 <tr>
	   <td scope="row" align="left">Reporte033</td>
	   <td align="left"><a style="text-decoration:none" target='_blank' href="../configurarReporte.php?tipo=<?php echo 51;?>" >Informe Nuevas Asignaciones de Tarjetas por rango de fecha y Agencia</a></td>
    </tr>
    <tr>
	    <td scope="row" align="left">Reporte034</td>
	    <td align="left"><a style="text-decoration:none" target='_blank' ">Informe Trabajadores con Tarjeta Bloqueada</a>
	    <label style="cursor:pointer" onClick="procesar16('PDF','<?php echo $usuario; ?>');" ><img src="../../imagenes/pdf16x16.png" /></label>
    	<label style="cursor:pointer" onClick="procesar16('EXC','<?php echo $usuario; ?>');" ><img src="../../imagenes/excel16x16.png" /></label>
	    </td>
	 </tr>
    <tr>
	   <td scope="row" align="left">Reporte035</td>
	   <td align="left"><a style="text-decoration:none" target='_NEW' href="configReporteAportes.php?tipo=12&tit=12">Listado de Transacciones Detallado por fecha y Comercio</a></td>
    </tr>
</table>
</center>
</body>
<script language="javascript">
var URL=src();


	
</script>  
</html>

