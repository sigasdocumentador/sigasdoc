<?php
/* autor:       orlando puentes
 * fecha:       28/09/2010
 * objetivo:    
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';

include_once $raiz.DIRECTORY_SEPARATOR ."config.php";
include_once  $root;

$usuario=$_SESSION['USUARIO'];
$menu="../menuAsopagos.php";
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Men&uacute; Reportes de subsidio cuota monetaria</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="../../css/estiloReporte.css" rel="stylesheet" type="text/css">
<script src="../../js/comunes.js" type="text/javascript"></script>
</head>
<body>
<center>
  <table width="100%" border="0">
  <tr>
    <td align="center" ><img src="../../imagenes/razonSocial.png" width="615" height="55"></td>
  </tr>
  <tr>
    <td align="center" >&nbsp;</td>
  </tr>
  <tr>
    <td class="fecha" ></td>
  </tr>
</table>
<table width="60%" border="0" cellspacing="1" class="tablero">
	<tr><th>Reporte</th>
	<th>Listado de Reportes de Tarjeta ASOPAGOS</th></tr>  
	  <tr>
	    <td scope="row" align="left">Reporte001</td>
	    <td align="left"><a style="text-decoration:none" target='_blank' href="<?php echo $ruta_reportes; ?>asopagos/reporte001.jsp?v0=<?php echo $usuario; ?>">Listado de tarjetas activas</a></td>
	  </tr>
	 <tr>
	  <td scope="row" align="left" width="20%">Reporte002</td>
	  <td align="left"><a style="text-decoration:none" target='_blank' href="<?php echo $ruta_reportes; ?>asopagos/reporte002.jsp?v0=<?php echo $usuario; ?>">Listado de tarjetas pendientes de entregar</a></td>
	</tr>
	 <tr>
	   <td scope="row" align="left">Reporte003</td>
	   <td align="left"><a style="text-decoration:none" target='_blank' href="<?php echo $ruta_reportes; ?>asopagos/reporte003.jsp?v0=<?php echo $usuario; ?>">Inventario de bonos y plasticos</a></td>
    </tr>
	 <tr>
	   <td scope="row" align="left">Reporte004</td>
	   <td align="left"><a style="text-decoration:none" target='_blank' href="<?php echo $ruta_reportes; ?>asopagos/reporte004.jsp?v0=<?php echo $usuario; ?>">Listado de Traslados de dinero pendientes de procesar</a></td>
    </tr>
	 <tr>
	   <td scope="row" align="left">Reporte005</td>
	   <td align="left"><a style="text-decoration:none" target='_blank' href="../configurarReporte.php?tipo=<?php echo 2;?>&modulo=asopagos&v0=<?php echo '005';?>&menurpte=asopagos/menuAsopagos.php&plano=<?php echo 1;?>" >Listado de Transacciones por Negocio  por fecha</a></td>
    </tr>
	 <tr>
	   <td scope="row" align="left">Reporte006</td>
	   <td align="left"><a style="text-decoration:none" target='_blank' href="../configurarReporte.php?tipo=<?php echo 2;?>&modulo=asopagos&v0=<?php echo '006';?>&menurpte=asopagos/menuAsopagos.php&plano=<?php echo 1;?>" >Listado Detallado de Transacciones  por fecha</a></td>
    </tr>
	 <tr>
	   <td scope="row" align="left">Reporte007</td>
	   <td align="left"><a style="text-decoration:none" target='_blank' href="../configurarReporte.php?tipo=<?php echo 1;?>&modulo=asopagos&v0=<?php echo '007';?>&menurpte=asopagos/menuAsopagos.php&plano=<?php echo 0;?>" >Listado de Entrega de Tarjetas por usuario por fecha</a></td>
    </tr>
	 <tr>
	   <td scope="row" align="left">Reporte008</td>
	   <td align="left"><a style="text-decoration:none" target='_blank' href="<?php echo $ruta_reportes; ?>asopagos/reporte008.jsp?v0=<?php echo $usuario; ?>">Consolidado de entregas de tarjetas</a></td>
    </tr>
	 <tr>
	   <td scope="row" align="left">Reporte009</td>
	   <td align="left"><a style="text-decoration:none" target='_blank' href="<?php echo $ruta_reportes; ?>asopagos/reporte009.jsp?v0=<?php echo $usuario; ?>">Inventario tarjetas</a></td>
    </tr>
	 <tr>
	   <td scope="row" align="left">Reporte010</td>
	   <td align="left"><a style="text-decoration:none" target='_blank' href="<?php echo $ruta_reportes; ?>asopagos/reporte010.jsp?v0=<?php echo $usuario; ?>">Listado de Tarjetas Pendienrtes de Imprimir</a></td>
    </tr>
	 <tr>
	   <td scope="row" align="left">Reporte011</td>
	   <td align="left"><a style="text-decoration:none" target='_blank' href="<?php echo $ruta_reportes; ?>asopagos/reporte011.jsp?v0=<?php echo $usuario; ?>">Listado de Saldos pendientes de trasladar de REDEBAN</a></td>
    </tr>
	 <tr>
	   <td scope="row" align="left">Reporte012</td>
	    <td align="left"><a style="text-decoration:none" target='_blank' href="../configurarReporte.php?tipo=<?php echo 2;?>&modulo=asopagos&v0=<?php echo '012';?>&menurpte=asopagos/menuAsopagos.php&plano=<?php echo 0;?>" >Traslados de saldos de REDEBAN a ASOPAGOS por fecha</a></td>
    </tr>
	<tr>
	   <td scope="row" align="left">Reporte013</td>
	   <td align="left"><a style="text-decoration:none" target='_blank' href="../configurarReporte.php?tipo=<?php echo 2;?>&modulo=asopagos&v0=<?php echo '013';?>&menurpte=asopagos/menuAsopagos.php&plano=<?php echo 0;?>" >Listado de Tarjetas Asignadas por fecha</a></td>
    </tr>
	 <tr>
	   <td scope="row" align="left">Reporte014</td>
	   <td align="left"><a style="text-decoration:none" target='_blank' href="<?php echo $ruta_reportes; ?>asopagos/reporte014.jsp?v0=<?php echo $usuario; ?>">Tarjetas BLOQUEADAS pendientes de procesar</a></td>
    </tr>
	 <tr>
	   <td scope="row" align="left">Reporte015</td>
	   <td align="left"><a style="text-decoration:none" target='_blank' href="<?php echo $ruta_reportes; ?>asopagos/reporte015.jsp?v0=<?php echo $usuario; ?>">Listado de Reversos pendientes de procesar</a></td>
    </tr>
	 <tr>
	   <td scope="row" align="left">Reporte016</td>
	   <td align="left"><a style="text-decoration:none" target='_blank' href="<?php echo $ruta_reportes; ?>asopagos/reporte016.jsp?v0=<?php echo $usuario; ?>">Listado de ABONOS pendientes de procesar</a></td>
    </tr>
	 <tr>
	   <td scope="row" align="left">Reporte017</td>
	   <td align="left"><a style="text-decoration:none" target='_blank' href="<?php echo $ruta_reportes; ?>asopagos/reporte017.jsp?v0=<?php echo $usuario; ?>">Actualizacion de Datos del Maestro pendiente de procesar</a></td>
    </tr>
	 <tr>
	   <td scope="row" align="left">Reporte018</td>
	   <td align="left"><a style="text-decoration:none" target='_blank' href="<?php echo $ruta_reportes; ?>asopagos/reporte018.jsp?v0=<?php echo $usuario; ?>">Listado de Reexpediciones de tarjetas pendientes de procesar</a></td>
    </tr>
	 <tr>
	   <td scope="row" align="left">Reporte019</td>
	   <td align="left"><a style="text-decoration:none" target='_blank' href="<?php echo $ruta_reportes; ?>asopagos/reporte019.jsp?v0=<?php echo $usuario; ?>">Actualizacion de Datos del Maestro pendientes de procesar por Reexpedicion</a></td>
    </tr>
	 <tr>
	   <td scope="row" align="left">Reporte020</td>
	   <td align="left"><a style="text-decoration:none" target='_blank' href="../configurarReporte.php?tipo=<?php echo 2;?>&modulo=asopagos&v0=<?php echo '020';?>&menurpte=asopagos/menuAsopagos.php&plano=<?php echo 1;?>" >Listado de Transacciones Totales  por fecha</a></td>
    </tr>
	 <tr>
	   <td scope="row" align="left">Reporte021</td>
        <td align="left"><a style="text-decoration:none" target='_blank' href="../configurarReporte.php?tipo=<?php echo 1;?>&modulo=asopagos&v0=<?php echo '007';?>&menurpte=asopagos/menuAsopagos.php&plano=<?php echo 0;?>" >Inventario Realizado en Agencias</a></td>
	 </tr>
	 <tr>
	   <td scope="row" align="left">Reporte022</td>
	   <td align="left">&nbsp;</td>
    </tr>
</table>
</center>
</body>
<script language="javascript">
var URL=src();


	
</script>  
</html>

