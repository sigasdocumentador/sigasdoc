<?php
set_time_limit ( 0 );
ini_set ( "display_errors", '1' );
date_default_timezone_set ( "America/Bogota" );

$c0=$_REQUEST["v0"];

$root = $_SERVER ['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once $root;
include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'funcionesComunes' . DIRECTORY_SEPARATOR . 'funcionesComunes.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'rsc' . DIRECTORY_SEPARATOR . 'pdo' . DIRECTORY_SEPARATOR . 'IFXDbManejador.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'rsc' . DIRECTORY_SEPARATOR . 'pdo' . DIRECTORY_SEPARATOR . 'IFXerror.php';
$db = IFXDbManejador::conectarDB ();
if ($db->conexionID == null) {
	$cadena = $db->error;
	echo -2;
	exit ();
}

$directorio = $ruta_cargados . "colegio".DIRECTORY_SEPARATOR."cargados".DIRECTORY_SEPARATOR;
$dir = opendir ( $directorio );
while ( $elemento = readdir ( $dir ) ) {
	if (strlen ( $elemento ) > 2)
		$archivos [] = $elemento;
}
closedir ( $dir );
$directorioProcesado = $ruta_cargados . "colegio/procesados/";
if (!file_exists($directorioProcesado)) {
	mkdir($directorioProcesado, 0777, true);
}
$archivo='reporte.csv';
$rutadelplano=$directorioProcesado.DIRECTORY_SEPARATOR.$archivo;
$cadena = "NIT;IDENTIFICACION;NOMBRETRABAJADOR;ESTADO;TIPO AFILIACION;FECHA RADICACION;CATEGORIA;SALARIO;PERIODO APORTE;ULTIMO APORTE;FECHA PROTECCION;IDENTBENEFICIARIO;NOMBENEFICIARIO;PARENTESCO;EDADBENEFICIARIO;ESTADOGIRO\r\n";
$fp=fopen($rutadelplano,'w');
fwrite($fp, $cadena);
fclose($fp);

$i=0;
$fp=fopen($rutadelplano,'a');
for(; $i<count($archivos); $i++){
	if($archivos[$i]==$c0){
		$directorio .= $archivos[$i];
		$archivoSubido = file ( $directorio );
		$totalLineas = count ( $archivoSubido );
		$cont=0;	
		for($c = 0; $c < $totalLineas; $c ++) {
			$cont++;
			$linea = $archivoSubido [$c];
			$documento = explode ( ' ', $linea );
			$num=trim($documento[0]);
			buscar($num,$db,$fp);
		}		
		break;
	}
}
fclose($fp);

if($cont==0){
	echo -1;
	exit();
}

unlink($directorio);

function buscar($num,$db,$fp){
	$sql="SELECT DISTINCT a48.nit, a15.identificacion, isnull(a15.pnombre,'') + ' ' + isnull(a15.snombre,'') + ' ' + isnull(a15.papellido,'') + ' ' + isnull(a15.sapellido,'') nombre,
					CASE WHEN a16.estado='P' THEN ( isnull((SELECT TOP 1 'PU' FROM aportes010 b10 WHERE b10.idtrabajador=a16.idpersona AND b10.idempresa=a16.idempresa AND b10.fechasistema=a16.fechasistema 
						AND (a16.idradicacion IS NULL OR 0=isnull(( SELECT count(*) cuenta FROM aportes004 b4 WHERE b4.idradicacion=a16.idradicacion AND b4.numero=b10.identificacion AND b4.nit=b10.nit AND (b4.procesado='N' OR b4.procesado IS NULL) 
						AND (b4.idtiporadicacion=28 or b4.idtiporadicacion=2926 or b4.idtiporadicacion=29)), 0))), 'P') ) ELSE a16.estado END estado,
					a91.detalledefinicion tipoafiliacion, a16.fechasistema fechaafiliacion, isnull(( SELECT max(a10.fechapago) FROM aportes010 a10 WHERE a10.idtrabajador=a16.idpersona AND a10.idempresa=a16.idempresa ),'') ultimoAporte,
					isnull(( SELECT max(a10.periodo) FROM aportes010 a10 WHERE a10.idtrabajador=a16.idpersona AND a10.idempresa=a16.idempresa ),'') periodoUltimoAporte,
					isnull(a16.fechafidelidad,'') fechaProteccion, isnull(a16.estadofidelidad,'') estadofidelidad,
					(SELECT TOP 1 max(c16.categoria) FROM aportes016 c16 WHERE c16.idpersona=a15.idpersona GROUP BY c16.estado ORDER BY c16.estado) categoria,
					(SELECT TOP 1 max(c16.salario) FROM aportes016 c16 WHERE c16.idpersona=a15.idpersona) salario,
					b15.identificacion as idenbenef, isnull(b15.pnombre,'') + ' ' + isnull(b15.snombre,'') + ' ' + isnull(b15.papellido,'') + ' ' + isnull(b15.sapellido,'') beneficiario,
					b91.detalledefinicion AS parentesco,datediff(dd,b15.fechanacimiento,getdate())/365.25 AS fechanacimiento,a21.giro as estadogiro
			FROM aportes015 a15 INNER JOIN aportes016 a16 ON a16.idpersona=a15.idpersona
			INNER JOIN aportes048 a48 ON a48.idempresa=a16.idempresa INNER JOIN aportes091 a91 ON a91.iddetalledef=a16.tipoafiliacion 
			LEFT JOIN aportes021 a21 ON a16.idpersona=a21.idtrabajador AND a21.idparentesco IN (35,38) AND a21.estado='A'
			LEFT JOIN aportes015 b15 ON a21.idbeneficiario=b15.idpersona AND a21.idparentesco IN (35,38) AND a21.estado='A'
			LEFT JOIN aportes091 b91 ON a21.idparentesco=b91.iddetalledef
			WHERE a15.identificacion='$num' AND a16.salario=(SELECT TOP 1 max(c16.salario) FROM aportes016 c16 WHERE c16.idpersona=a15.idpersona)
			ORDER BY a15.identificacion";
	
	$cont=0;
	$rs=$db->querySimple($sql);
	while($row=$rs->fetch()){		
		escribirPlano($row,$fp);
		$cont++;
	}
	
	if($cont==0){
		$sql="SELECT a48.nit, a15.identificacion, isnull(a15.pnombre,'') + ' ' + isnull(a15.snombre,'') + ' ' + isnull(a15.papellido,'') + ' ' + isnull(a15.sapellido,'') nombre, 'I' estado,
						a91.detalledefinicion tipoafiliacion, a16.fechasistema fechaafiliacion, isnull(( SELECT max(a10.fechapago) FROM aportes010 a10 WHERE a10.idtrabajador=a16.idpersona AND a10.idempresa=a16.idempresa ),'') ultimoAporte,
						isnull(( SELECT max(a10.periodo) FROM aportes010 a10 WHERE a10.idtrabajador=a16.idpersona AND a10.idempresa=a16.idempresa ),'') periodoUltimoAporte,
						isnull(a16.fechafidelidad,'') fechaProteccion, isnull(a16.estadofidelidad,'') estadofidelidad, a16.categoria,
						a16.salario,
						b15.identificacion as idenbenef, isnull(b15.pnombre,'') + ' ' + isnull(b15.snombre,'') + ' ' + isnull(b15.papellido,'') + ' ' + isnull(b15.sapellido,'') beneficiario,
						b91.detalledefinicion AS parentesco,datediff(dd,b15.fechanacimiento,getdate())/365.25 AS fechanacimiento,a21.giro as estadogiro
				FROM aportes015 a15 INNER JOIN aportes017 a16 ON a16.idpersona=a15.idpersona
				INNER JOIN aportes048 a48 ON a48.idempresa=a16.idempresa INNER JOIN aportes091 a91 ON a91.iddetalledef=a16.tipoafiliacion 
				LEFT JOIN aportes021 a21 ON a16.idpersona=a21.idtrabajador AND  a21.idparentesco IN (35,38) AND a21.estado='A'
				LEFT JOIN aportes015 b15 ON a21.idbeneficiario=b15.idpersona AND  a21.idparentesco IN (35,38) AND a21.estado='A'
				LEFT JOIN aportes091 b91 ON a21.idparentesco=b91.iddetalledef
				WHERE a15.identificacion='$num' AND a16.fecharetiro=(SELECT max(c16.fecharetiro) FROM aportes017 c16 WHERE c16.idpersona=a15.idpersona)
				ORDER BY a15.identificacion";
				
		$rs=$db->querySimple($sql);				
		while($row=$rs->fetch()){			
			escribirPlano($row,$fp);
			$cont++;
		}
		
		if($cont==0){
			$cade=$num."; no existe en nuestra base de datos;\r\n";
			fwrite($fp, $cade);
		}
	}
}

function escribirPlano($row,$fp){
	$periodoaporte=$row["periodoUltimoAporte"];
	$fechaaporte=$row["ultimoAporte"];
	$fechaafiliacion=$row["fechaafiliacion"];
	$fechaproteccion=$row["fechaProteccion"];	
	if($fechaaporte=="1900-01-01"){ $fechaaporte=""; }
	if($fechaafiliacion=="1900-01-01"){ $fechaafiliacion=""; }
	if($row["estadofidelidad"]=="I" || $fechaproteccion=="1900-01-01"){ $fechaproteccion=""; }
	
	$cadena = "$row[nit];$row[identificacion];$row[nombre];$row[estado];$row[tipoafiliacion];$fechaafiliacion;$row[categoria];$row[salario];$periodoaporte;$fechaaporte;$fechaproteccion;$row[idenbenef];$row[beneficiario];$row[parentesco];$row[fechanacimiento];$row[estadogiro]"."\r\n";
	fwrite($fp, $cadena);
}

$_SESSION['ENLACE']=$rutadelplano;
$_SESSION['ARCHIVO']=$archivo;
echo 1;
?>
