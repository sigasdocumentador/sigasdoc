<?php

/**
 * Encapsula las fechas manejadas en el conjunto
 * de clases del servicio
 * @author Juan Fernando Tamayo Puertas
 * @version 1.1
 * @tutorial El constructor tiene 4 sobre cargas:<br>
 * 1) $obj = new Fecha(); //La fecha por defecto es 1900/01/01<br>
 * 2) $obj = new Fecha('2012-11-24T20:15:26'); //Formato de fecha internacional<br>
 * 3) $obj = new Fecha(2012,11,24); //A�o, Mes, Dia<br>
 * 4) $obj = new Fecha(2012,11,24,20,15,26); //A�o, Mes, Dia, Hora, Minutos, Segundos
 */
class Fecha {
	
	public $Anio = '';
	public $Mes = '';
	public $Dia = '';
	public $Hora = '';
	public $Minutos = '';
	public $Segundos = '';
	
	function __construct() {
		try {
			$numArgs = count(func_get_args());
			$args = func_get_args();
			if($numArgs==0){
				$this->Anio = '1900';
				$this->Mes = '01';
				$this->Dia = '01';
				$this->Hora = '00';
				$this->Minutos = '00';
				$this->Segundos = '00';
			}else{
				if($numArgs==1){
					if(gettype($args[0])=='string'){
						if(preg_match('/^(19[0-9]{2}|2[0-9]{3})-(0[1-9]|1[012])-([123]0|[012][1-9]|31)T([0-2][0-4]):([0-5][0-9]):([0-5][0-9])$/', trim($args[0]))){
							$this->Anio = intval(substr(trim($args[0]), 0, 4));
							$this->Mes = intval(substr(trim($args[0]), 5, 2));
							$this->Dia = intval(substr(trim($args[0]), 8, 2));
							$this->Hora = intval(substr(trim($args[0]), 11, 2));
							$this->Minutos = intval(substr(trim($args[0]), 14, 2));
							$this->Segundos = intval(substr(trim($args[0]), 17, 2));
						}else{
							echo "El formato de la cadena es invalido para la primera implementacion del constructor.<br>ej. 1900-01-01T00:00:00";
							exit();
						}
					}else{
						echo "La segunda implementacion del constructor Fecha exige un argumento de tipo string";
						exit();
					}
				}else if(($numArgs==3)||($numArgs==6)){
					$this->Anio = intval($args[0]);
					$this->Mes = intval($args[1]);
					$this->Dia = intval($args[2]);
					if($numArgs==6){
						$this->Hora = intval($args[3]);
						$this->Minutos = intval($args[4]);
						$this->Segundos = intval($args[5]);
					}else{
						$this->Hora = '00';
						$this->Minutos = '00';
						$this->Segundos = '00';
					}
				}else{
					echo "El numero de argumentos no corresponde a ninguna implementacion del constructor";
					exit();
				}
			}
		}catch(Exception $ex){
			echo $ex->getMessage();
			exit();
		}
	}
	
	/**
	 * Retorna la fecha en el formato establecido por el patron
	 * @param string $patter Cadena de texto donde se esoecifica la posicion en la
	 * que va el dia(d), el mes(m) y el a�o(y). ej m/d/y
	 */
	public function format($patter = 'M/d/y h:m:s'){
		if((strchr(trim($patter), 'd')==false)&&(strchr(trim($patter), 'M')==false)&&(strchr(trim($patter), 'y')==false)&&(strchr(trim($patter), 'h')==false)&&(strchr(trim($patter), 'm')==false)&&(strchr(trim($patter), 's')==false)){
			return $this->__toString();
		}
		$patter = str_replace('d', str_pad($this->Dia,2,'0',STR_PAD_LEFT), trim($patter));
		$patter = str_replace('M', str_pad($this->Mes,2,'0',STR_PAD_LEFT), trim($patter));
		$patter = str_replace('y', str_pad($this->Anio,4,'0',STR_PAD_LEFT), trim($patter));
		
		$patter = str_replace('h', str_pad($this->Hora,2,'0',STR_PAD_LEFT), trim($patter));
		$patter = str_replace('m', str_pad($this->Minutos,2,'0',STR_PAD_LEFT), trim($patter));
		$patter = str_replace('s', str_pad($this->Segundos,2,'0',STR_PAD_LEFT), trim($patter));
		return $patter;
	} 
	
	public function __toString(){
		return str_pad($this->Anio,4,'0',STR_PAD_LEFT).'-'.str_pad($this->Mes,2,'0',STR_PAD_LEFT).'-'.str_pad($this->Dia,2,'0',STR_PAD_LEFT).'T'.str_pad($this->Hora,2,'0',STR_PAD_LEFT).':'.str_pad($this->Minutos,2,'0',STR_PAD_LEFT).':'.str_pad($this->Segundos,2,'0',STR_PAD_LEFT);
	}
}

?>