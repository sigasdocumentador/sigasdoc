<?php

include_once 'DetalleDefinicion.php';
include_once 'Aporte.php';
include_once 'Tercero.php';
include_once 'Fecha.php';

/**
 * Encapsula el periodo actual en proceso
 * @author Juan Fernando Tamayo Puertas
 * @version 1.0
 */
class Empresa {
	
	public $Idempresa;
	public $TipoDocEmpresa;
	public $Nit;
	public $Digito;
	public $Sector;
	public $RazonSocial;
	public $Sigla;
	public $Direccion;
	public $Telefono;
	public $Fax;
	public $Email;
	public $CodDepartamento;
	public $Departmento;
	public $CodMunicipio;
	public $Municipio;
	public $Representante;
	public $JefePersonal;
	public $UltimoAporte;
	public $FechaSistema;
	public $Creador;
	public $Estado;
	
	function __construct() {
		try{
			$numArgs = count(func_get_args());
			$args = func_get_args();
			if($numArgs==0){
				$this->Idempresa = 0;
				$this->TipoDocEmpresa = new DetalleDefinicion();
				$this->Nit = '';
				$this->Digito = '';
				$this->Sector = new DetalleDefinicion();
				$this->RazonSocial = '';
				$this->Sigla = '';
				$this->Direccion = '';
				$this->Telefono = '';
				$this->Fax = '';
				$this->Email = '';
				$this->CodDepartamento = '';
				$this->Departmento = '';
				$this->CodMunicipio = '';
				$this->Municipio = '';
				$this->Representante = new Tercero();
				$this->JefePersonal = new Tercero();
				$this->UltimoAporte = new Aporte();
				$this->FechaSistema = new Fecha();
				$this->Creador = '';
				$this->Estado = '';
			}elseif($numArgs==1){
				if(gettype($args[0])=='array'){
					if(count($args[0])==21){						
						$this->Idempresa = $args[0]['Id'];
						$this->TipoDocEmpresa = new DetalleDefinicion($args[0]['TipoDocEmpresa']);
						$this->Nit = trim($args[0]['Nit']);
						$this->Digito = trim($args[0]['Digito']);
						$this->Sector = new DetalleDefinicion($args[0]['Sector']);
						$this->RazonSocial = strtoupper(trim($args[0]['RazonSocial']));
						$this->Sigla = strtoupper(trim($args[0]['Sigla']));
						$this->Direccion = strtoupper(trim($args[0]['Direccion']));
						$this->Telefono = trim($args[0]['Telefono']);
						$this->Fax = trim($args[0]['Fax']);
						$this->Email = trim($args[0]['Email']);
						$this->CodDepartamento = trim($args[0]['CodDepartamento']);
						$this->Departmento = strtoupper(trim($args[0]['Departmento']));
						$this->CodMunicipio = trim($args[0]['CodMunicipio']);
						$this->Municipio = strtoupper(trim($args[0]['Municipio']));
						$this->Representante = new Tercero($args[0]['Representante']);
						$this->JefePersonal = new Tercero($args[0]['JefePersonal']);
						$this->UltimoAporte = new Aporte($args[0]['UltimoAporte']);
						$this->FechaSistema = $args[0]['FechaSistema']=='0001-01-01T00:00:00'?'':new Fecha($args[0]['FechaSistema']);
						$this->Creador = trim($args[0]['Creador']);
						$this->Estado = strtoupper(trim($args[0]['Estado']));
					}else{
						echo "El numero de elementos en el array debe ser de 21, como lo exige la segunda implementacion del constructor";
						exit();
					}
				}else{
					echo "La segunda implementacion del constructor Empresa exige un argumento de tipo array";
					exit();
				}
			}elseif($numArgs==17){
				if((gettype($args[0])=='integer')&&(gettype($args[1])=='object')&&(gettype($args[2])=='string')&&(gettype($args[3])=='string')&&(gettype($args[3])=='object')&&(gettype($args[4])=='string')&&(gettype($args[5])=='string')&&(gettype($args[6])=='string')&&(gettype($args[7])=='string')&&(gettype($args[8])=='string')&&(gettype($args[9])=='string')&&(gettype($args[10])=='string')&&(gettype($args[10])=='string')&&(gettype($args[10])=='string')&&(gettype($args[10])=='string')&&(gettype($args[11])=='object')&&(gettype($args[12])=='object')&&(gettype($args[13])=='object')&&(gettype($args[14])=='object')&&(gettype($args[15])=='string')&&(gettype($args[16])=='string')){
					if((get_class($args[18])!='Fecha')||(get_class($args[17])!='Aporte')||(get_class($args[1])!='DetalleDefinicion')||(get_class($args[4])!='DetalleDefinicion')||(get_class($args[15])!='Tercero')||(get_class($args[16])!='Tercero')){
						$this->Idempresa = $args[0];
						$this->TipoDocEmpresa = $args[1];
						$this->Nit = iconv('', 'UTF-8', trim($args[2]));
						$this->Digito = iconv('', 'UTF-8', trim($args[3]));
						$this->Sector = $args[4];
						$this->RazonSocial = strtoupper(iconv('', 'UTF-8', trim($args[5])));
						$this->Sigla = strtoupper(iconv('', 'UTF-8', trim($args[6])));
						$this->Direccion = strtoupper(iconv('', 'UTF-8', trim($args[7])));
						$this->Telefono = iconv('', 'UTF-8', trim($args[8]));
						$this->Fax = iconv('', 'UTF-8', trim($args[9]));
						$this->Email = iconv('', 'UTF-8', trim($args[10]));
						$this->CodDepartamento = iconv('', 'UTF-8', trim($args[11]));
						$this->Departmento = strtoupper(iconv('', 'UTF-8', trim($args[12])));
						$this->CodMunicipio = iconv('', 'UTF-8', trim($args[13]));
						$this->Municipio = strtoupper(iconv('', 'UTF-8', trim($args[14])));
						$this->Representante = $args[15];
						$this->JefePersonal = $args[16];
						$this->UltimoAporte = $args[17];
						$this->FechaSistema = $args[18];
						$this->Creador = iconv('', 'UTF-8', trim($args[19]));
						$this->Estado = strtoupper(iconv('', 'UTF-8', trim($args[20])));
					}else{
						echo "Los argumentos 15,16 deben ser de tipo Tercero, 1,4 debe ser de tipo DetalleDefinicion, 18 debe ser de tipo Fecha y 17 debe ser de tipo Aporte, exigida en la segunda implementacion del constructor";
						exit();
					}
				}else{
					echo "El tipo de argumentos no corresponde a la exigida en la segunda implementacion del constructor";
					exit();
				}
			}else{
				echo "El numero de argumentos no corresponde a ninguna implementacion del constructor";
				exit();
			}
		}catch (\Exception $ex){
			echo $ex->getMessage();
			exit();
		}
	}
}

?>