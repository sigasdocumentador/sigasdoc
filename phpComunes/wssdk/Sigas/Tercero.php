<?php

include_once 'Fecha.php';
include_once 'DetalleDefinicion.php';

/**
 * Encapsula los datos de un tercero en sigas
 * @author Juan Fernando Tamayo Puertas
 * @version 1.0
 */
class Tercero {
	
	public $Idpersona;
	public $TipoDocumento;
	public $Identificacion;
	public $CodCiudadResidencia;
	public $CiudadResidencia;
	public $PApellido;
	public $SApellido;
	public $PNombre;
	public $SNombre;
	public $Sexo;
	public $Direccion;
	public $Telefono;
	public $Celular;
	public $Email;
	public $EstadoCivil;
	public $FechaNacimiento;
	public $Edad;
	public $Estado;
	public $FechaSistema;
	public $Creador;
	
	function __construct() {
		try{
			$numArgs = count(func_get_args());
			$args = func_get_args();
			if($numArgs==0){
				$this->Idpersona = 0;
				$this->TipoDocumento = new DetalleDefinicion();
				$this->Identificacion = '';
				$this->CodCiudadResidencia = '';
				$this->CiudadResidencia = '';
				$this->PApellido = '';
				$this->SApellido = '';
				$this->PNombre = '';
				$this->SNombre = '';
				$this->Sexo = '';
				$this->Direccion = '';
				$this->Telefono = '';
				$this->Celular = '';
				$this->Email = '';
				$this->EstadoCivil = new DetalleDefinicion();
				$this->FechaNacimiento = new Fecha();
				$this->Edad = 0;
				$this->Estado = '';
				$this->FechaSistema = new Fecha();
				$this->Creador = '';
			}elseif($numArgs==1){
				if(gettype($args[0])=='array'){
					if(count($args[0])==20){						
						$this->Idpersona = $args[0]['Id'];
						$this->TipoDocumento = new DetalleDefinicion($args[0]['TipoDocumento']);
						$this->Identificacion = $args[0]['Identificacion'];
						$this->CodCiudadResidencia = $args[0]['CodCiudadResidencia'];
						$this->CiudadResidencia = $args[0]['CiudadResidencia'];
						$this->PApellido = $args[0]['PrimerApellido'];
						$this->SApellido = $args[0]['SegundoApellido'];
						$this->PNombre = $args[0]['PrimerNombre'];
						$this->SNombre = $args[0]['SegundoNombre'];
						$this->Sexo = $args[0]['Sexo'];
						$this->Direccion = $args[0]['Direccion'];
						$this->Telefono = $args[0]['Telefono'];
						$this->Celular = $args[0]['Celular'];
						$this->Email = $args[0]['Email'];
						$this->EstadoCivil = new DetalleDefinicion($args[0]['EstadoCivil']);
						$this->FechaNacimiento = $args[0]['FechaNacimiento']=='0001-01-01T00:00:00'?'':new Fecha($args[0]['FechaNacimiento']);
						$this->Edad = $args[0]['Edad'];
						$this->Estado = $args[0]['Estado'];
						$this->FechaSistema = $args[0]['FechaSistema']=='0001-01-01T00:00:00'?'':new Fecha($args[0]['FechaSistema']);
						$this->Creador = $args[0]['Creador'];
					}else{
						echo "El numero de elementos en el array debe ser de 20, como lo exige la segunda implementacion del constructor";
						exit();
					}
				}else{
					echo "La segunda implementacion del constructor Tercero exige un argumento de tipo array";
					exit();
				}
			}elseif($numArgs==20){
				if((gettype($args[0])=='integer')&&(gettype($args[1])=='object')&&(gettype($args[2])=='string')&&(gettype($args[3])=='string')&&(gettype($args[4])=='string')&&(gettype($args[5])=='string')&&(gettype($args[6])=='string')&&(gettype($args[7])=='string')&&(gettype($args[8])=='string')&&(gettype($args[9])=='string')&&(gettype($args[10])=='string')&&(gettype($args[11])=='string')&&(gettype($args[12])=='string')&&(gettype($args[13])=='string')&&(gettype($args[14])=='object')&&(gettype($args[15])=='object')&&(gettype($args[16])=='integer')&&(gettype($args[17])=='string')&&(gettype($args[18])=='object')&&(gettype($args[19])=='string')){
					if((get_class($args[15])!='Fecha')||(get_class($args[18])!='Fecha')||(get_class($args[1])!='DetalleDefinicion')||(get_class($args[14])!='DetalleDefinicion')){						
						$this->Idpersona = $args[0];
						$this->TipoDocumento = $args[1];
						$this->Identificacion = iconv('', 'UTF-8', trim($args[2]));
						$this->CodCiudadResidencia = iconv('', 'UTF-8', trim($args[3]));
						$this->CiudadResidencia = strtoupper(iconv('', 'UTF-8', trim($args[4])));
						$this->PApellido = strtoupper(iconv('', 'UTF-8', trim($args[5])));
						$this->SApellido = strtoupper(iconv('', 'UTF-8', trim($args[6])));
						$this->PNombre = strtoupper(iconv('', 'UTF-8', trim($args[7])));
						$this->SNombre = strtoupper(iconv('', 'UTF-8', trim($args[8])));
						$this->Sexo = strtoupper(iconv('', 'UTF-8', trim($args[9])));
						$this->Direccion = strtoupper(iconv('', 'UTF-8', trim($args[10])));
						$this->Telefono = iconv('', 'UTF-8', trim($args[11]));
						$this->Celular = iconv('', 'UTF-8', trim($args[12]));
						$this->Email = strtolower(iconv('', 'UTF-8', trim($args[13])));
						$this->EstadoCivil = $args[14];
						$this->FechaNacimiento = $args[15];
						$this->Edad = $args[16];
						$this->Estado = strtoupper(iconv('', 'UTF-8', trim($args[17])));
						$this->FechaSistema = $args[18];
						$this->Creador = iconv('', 'UTF-8', trim($args[19]));
					}else{
						echo "Los argumentos 15,18 deben ser de tipo Fecha y 1,14 deben ser de tipo DetalleDefinicion, exigida en la segunda implementacion del constructor";
						exit();
					}
				}else{
					echo "El tipo de argumentos no corresponde a la exigida en la segunda implementacion del constructor";
					exit();
				}
			}else{
				echo "El numero de argumentos no corresponde a ninguna implementacion del constructor";
				exit();
			}
		}catch (\Exception $ex){
			echo $ex->getMessage();
			exit();
		}
	}
}

?>