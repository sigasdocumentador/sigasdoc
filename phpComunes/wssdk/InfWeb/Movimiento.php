<?php

/**
 * Encapsula la información de un movimiento contable
 * @author Juan Fernando Tamayo Puertas
 * @version 1.0
 */
class Movimiento {
	
	public $Comprob = '';
	public $Numero = 0;
	public $Fecha = '';
	public $Cuenta = '';
	public $Nit = 0;
	public $Centro = '';
	public $Valor = 0;
	public $DebCre = '';
	public $Descripcion = '';
	public $TipoDoc = '';
	public $NumeroDoc = 0;
	public $Conciliado = '';
	public $CuentaT = '';
	public $BaseMovimiento = 0;
	public $FechaGrab = '';
	public $Grabador = '';
	public $Nota = '';
	public $FVence = '';
	public $Concepto = 0;
	public $Activi = '';
	public $CodActi = '';
	public $Observacion='';
	/**
	 * Constructor de la clase
	 * @param int $Numero Numero del comprobante
	 * @param Fecha $Fecha Fecha del movimiento contable
	 * @param string $Cuenta Cuenta contable del movimiento
	 * @param int $Nit Nit o identificacion del tercero
	 * @param string $Centro Centro contable del movimiento
	 * @param decimal $Valor Valor en dinero del movimiento
	 * @param string $DebCre Naturaleza contable del movimiento. D-Debito, C-Credito
	 * @param string $Descripcion Descripcion del movimiento
	 * @param string $Grabador Usuario logueado en el cliente quien consume el servicio
	 * @param int $Concepto Numero de concepto
	 * @param Fecha $FVence Fecha en que vence
	 * @param string $Comprob Comprobante
	 * @param string $TipoDoc Tipo de documento
	 * @param int $NumeroDoc Numero de documento
	 * @param string $Nota
	 * @param string $Conciliado
	 * @param string $CuentaT
	 * @param decimal $BaseMovimiento	 
	 * @param string $Activi
	 * @param string $CodActi
	 */
	function __construct($Numero, $Fecha, $Cuenta, $Nit, $Centro, $Valor, $DebCre, $Descripcion, $Grabador, $Concepto = 0, $FVence = '1900-01-01T00:00:00', $Comprob = '', $TipoDoc = '', $NumeroDoc = 0, $Nota = '', $Conciliado = '', $CuentaT = '', $BaseMovimiento = 0, $Activi = '', $CodActi = '',$Observacion='') {
		$this->Comprob = strtoupper(trim($Comprob));
		$this->Numero = $Numero;
		if($Fecha!=NULL){
			if(gettype($Fecha)=='object'){
				if(get_class($Fecha)=='Fecha'){
					$this->Fecha = $Fecha->__toString();
				}else{
					echo "El parámetro Fecha en el constructor del objeto Movimiento, NO es un objeto de tipo Fecha";
					exit();
				}
			}
		}
		$this->Cuenta = iconv('', 'UTF-8', trim($Cuenta));
		$this->Nit = $Nit;
		$this->Centro = iconv('', 'UTF-8', trim($Centro));
		$this->Valor = $Valor;
		$this->DebCre = strtoupper(iconv('', 'UTF-8', trim($DebCre)));
		$this->Descripcion = strtoupper(iconv('', 'UTF-8', trim($Descripcion)));
		$this->TipoDoc = strtoupper(iconv('', 'UTF-8', trim($TipoDoc)));
		$this->NumeroDoc = $NumeroDoc;
		$this->Conciliado = iconv('', 'UTF-8', trim($Conciliado));
		$this->CuentaT = iconv('', 'UTF-8', trim($CuentaT));
		$this->BaseMovimiento = $BaseMovimiento;
		$this->FechaGrab = '1900-01-01T00:00:00';
		$this->Grabador = iconv('', 'UTF-8', trim($Grabador));
		$this->Nota = strtoupper(iconv('', 'UTF-8', trim($Nota)));
		if($FVence != NULL){
			if(gettype($FVence)=='object'){
				if(get_class($FVence)=='Fecha'){
					$this->FVence = $FVence->__toString();
				}else{
					echo "El parámetro FVence en el constructor del objeto Movimiento, NO es un objeto de tipo Fecha";
					exit();
				}
			}else{
				$this->FVence = $this->Fecha;
			}
		}
		$this->Concepto = $Concepto;
		$this->Activi = iconv('', 'UTF-8', trim($Activi));
		$this->CodActi = iconv('', 'UTF-8', trim($CodActi));
		$this->Observacion = iconv('', 'UTF-8', trim($Observacion));
	}
	
	function constructByResult($result) {
		$this->Comprob = $result['Comprob'];
		$this->Numero = $result['Numero'];
		if($result['Fecha']!=NULL){
			$this->Fecha = new Fecha($result['Fecha']);
		}else{
			$this->Fecha = "";
		}
		$this->Cuenta = trim($result['Cuenta']);
		$this->Nit = $result['Nit'];
		$this->Centro = trim($result['Centro']);
		$this->Valor = $result['Valor'];
		$this->DebCre = strtoupper(trim($result['DebCre']));
		$this->Descripcion = strtoupper(trim($result['Descripcion']));
		$this->TipoDoc = strtoupper(trim($result['TipoDoc']));
		$this->NumeroDoc = $result['NumeroDoc'];
		$this->Conciliado = trim($result['Conciliado']);
		$this->CuentaT = trim($result['CuentaT']);
		$this->BaseMovimiento = $result['BaseMovimiento'];
		if($result['FechaGrab']!=NULL){
			$this->FechaGrab = new Fecha($result['FechaGrab']);
		}else{
			$this->FechaGrab = "";
		}
		if($result['FVence']!=NULL){
			$this->FVence = new Fecha($result['FVence']);
		}else{
			$this->FVence = "";
		}
		$this->Grabador = trim($result['Grabador']);
		$this->Nota = strtoupper(trim($result['Nota']));
		$this->Concepto = $result['Concepto'];
		$this->Activi = trim($result['Activi']);
		$this->CodActi = trim($result['CodActi']);
		$this->Observacion = trim($result['Observacion']);
	}
}

?>