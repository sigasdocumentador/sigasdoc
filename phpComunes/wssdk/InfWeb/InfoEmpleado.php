<?php

/**
 * Encapsula la información basica de un empleado
 * @author Juan Fernando Tamayo Puertas
 * @version 1.1
 * @tutorial Se corrige la creacion de los campos fecha para que sean de tipo Fecha
 */
class InfoEmpleado {
	
	public $Codigo = "";
	public $Nombre = "";
	public $Cedula = 0;
	public $Direccion = "";
	public $Telefono = "";
	public $FNace = "";
	public $FIngreso = "";
	public $FRetiro = "";
	public $Estado = "";
	public $FVence = "";
	public $Grabador = "";
	public $FechaGrab = "";
	public $FechaMod = "";
	
	function __construct($result = array()) {
		$this->Codigo = $result['Codigo'];
		$this->Nombre = $result['Nombre'];
		$this->Cedula = $result['Cedula'];
		$this->Direccion = $result['Direccion'];
		$this->Telefono = $result['Telefono'];
		if($result['FNace']!=NULL){
			if($result['FNace']!=""){
				$this->FNace = new Fecha($result['FNace']);
			}else{
				$this->FNace = "";
			}
		}else{
			$this->FNace = "";
		}
		if($result['FIngreso']!=NULL){
			if($result['FIngreso']!=""){
				$this->FIngreso = new Fecha($result['FIngreso']);
			}else{
				$this->FIngreso = "";
			}
		}else{
			$this->FIngreso = "";
		}
		$this->FRetiro = $result['FRetiro'];
		$this->Estado = $result['Estado'];
		if($result['FVence']!=NULL){
			if($result['FVence']!=""){
				$this->FVence = new Fecha($result['FVence']);
			}else{
				$this->FVence = "";
			}
		}else{
			$this->FVence = "";
		}
		$this->Grabador = $result['Grabador'];
		$this->FechaGrab = $result['FechaGrab'];
		$this->FechaMod = $result['FechaMod'];
	}
}

?>