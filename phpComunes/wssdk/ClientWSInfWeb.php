<?php
date_default_timezone_set('America/Bogota');

include_once 'InfWeb/Tercero.php';
include_once 'InfWeb/WSError.php';
include_once 'Fecha.php';
include_once 'InfWeb/Movimiento.php';
include_once 'InfWeb/InfoEmpleado.php';

/**
 * Provee m�todos para consumir los servicios web de informa web
 * @author Juan Fernando Tamayo Puertas
 * @version 1.2.2
 * @tutorial Se corrigi� bug en el constructor de la clase InfoEmpleado en el manejo de las fechas.
 */
class ClientWSInfWeb {
	
	private $appId;
	private $appPasswd;
	private $proxy;
	
	/**
	 * Constructor de la clase
	 * @param string $appId C�digo de identificacion del cliente
	 * @param string $appPasswd Contrase�a del cliente
	 */
	public function __construct($appId, $appPasswd){
		try {
			$this->proxy = new SoapClient('http://10.10.1.60/WebServices/Services/WSInformaWeb.svc?wsdl', array('cache_wsdl' => 0));
			$this->appId = $appId;
			$this->appPasswd = $appPasswd;
		} catch (Exception $e) {
			echo $e->__toString();
			exit();
		}
	}
	
	/**
	 * Consulta un tercero por su numero de Nit
	 * @param int $nit Numero Nit SIN DIGITO DE VERIFICACI�N
	 * @return Tercero que encapsula la informaci�n.
	 * Si el tercero no es encontrado se retorna un objeto Tercero vac�o
	 */
	public function terceroPorNit($nit){
		try {
			$param = array();
			$param['appId'] = $this->appId;
			$param['appPwd'] = $this->appPasswd;
			$param['nit'] = $nit;
			$result = $this->proxy->TerceroPorNit($param);
			$result = $this->obj2array($result);
			$aux = new Tercero(0, '', '');
			$aux->constructByResult($result['TerceroPorNitResult']);
			return $aux;
		} catch (Exception $e) {
			echo $e->__toString();
			exit();
		}
	}

	/**
	 * Consulta si un tercero existe
	 * @param int $nit Numero Nit SIN DIGITO DE VERIFICACI�N
	 * @return boolean valor que indica si el tercero existe
	 */
	public function terceroExiste($nit){
		try {
			$param = array();
			$param['appId'] = $this->appId;
			$param['appPwd'] = $this->appPasswd;
			$param['nit'] = $nit;
			$result = $this->proxy->TerceroExiste($param);
			$result = $this->obj2array($result);
			return $result['TerceroExisteResult'];
		} catch (Exception $e) {
			echo $e->__toString();
			exit();
		}
	}
	
	/** Consulta si un tercero existe
	* @param int $nit Numero Nit SIN DIGITO DE VERIFICACI�N
	* @return boolean valor que indica si el tercero existe
	*/
	public function terceroExisteStr($nit){
		try {
			$param = array();
			$param['appId'] = $this->appId;
			$param['appPwd'] = $this->appPasswd;
			$param['nit'] = $nit;
			$result = $this->proxy->TerceroExisteStr($param);
			//var_dump($nit);exit();
			
			$result = $this->obj2array($result);
			return $result['TerceroExisteStrResult'];
		} catch (Exception $e) {
			echo $e->__toString();
			exit();
		}
	}
	
	/**
	 * Permite grabar un tercero. Si el tercero ya existe, lo actualiza
	 * @param Tercero $tercero Objeto de tipo Tercero que encapsula los datos
	 * del tercero a grabar
	 * @return boolean valor que indica si el tercero se grabo
	 */
	public function terceroGrabar($tercero){
		try {
			if(gettype($tercero)!='object'){
				echo "El parametro \$tercero debe ser de tipo Tercero.<br>Linea 97 en ClientWSInfWeb.php, metodo terceroGrabar";
				exit();
			}else{
				if(get_class($tercero)!='Tercero'){
					echo "El parametro \$tercero debe ser de tipo Tercero.<br>Linea 97 en ClientWSInfWeb.php, metodo terceroGrabar";
					exit();
				}
			}
			$param = array();
			$param['appId'] = $this->appId;
			$param['appPwd'] = $this->appPasswd;
			$param['obj'] = $tercero;
			$result = $this->proxy->TerceroGrabar($param);
			$result = $this->obj2array($result);
			return $result['TerceroGrabarResult'];
		} catch (Exception $e) {
			echo $e->__toString();
			exit();
		}
	}
	
	
	/**
	 * Permite grabar un tercero. Si el tercero ya existe, lo actualiza
	 * @param Tercero $tercero Objeto de tipo Tercero que encapsula los datos
	 * del tercero a grabar
	 * @return boolean valor que indica si el tercero se grabo
	 */
	public function terceroGrabarStr($tercero){
		try {
			if(gettype($tercero)!='object'){
				echo "El parametro \$tercero debe ser de tipo Tercero.<br>Linea 97 en ClientWSInfWeb.php, metodo terceroGrabar";
				exit();
			}else{
				if(get_class($tercero)!='Tercero'){
					echo "El parametro \$tercero debe ser de tipo Tercero.<br>Linea 97 en ClientWSInfWeb.php, metodo terceroGrabar";
					exit();
				}
			}
			$param = array();
			$param['appId'] = $this->appId;
			$param['appPwd'] = $this->appPasswd;
			$param['obj'] = $tercero;
			$result = $this->proxy->TerceroGrabarStr($param);
			$result = $this->obj2array($result);
			return $result['TerceroGrabarStrResult'];
		} catch (Exception $e) {
			echo $e->__toString();
			exit();
		}
	}
	
	
	/**
	 * Permite grabar una lista de movimientos contables, los cuales
	 * debe ser del mismo tipo (mismo tipo de comprobante)
	 * @param string $comprob Comprobante que va ser usado en 
	 * todos los movimientos contables
	 * @param array<Movimiento> $listMovi Arreglo de objetos tipo Movimiento
	 * @return array<Error> Arreglo de objetos Error, los cuales encasulan
	 * la lista de errores encontrados al procesar la transaccion. Si
	 * la transaccion se grabo con exito, el arreglo se retorna con
	 * cero elementos 
	 */
	public function movimientoGrabar($comprob, $listMovi = array()){
		try {
			if(gettype($comprob)!='string'){
				echo "El parametro \$comprob debe ser de tipo string.<br>Linea 132 en ClientWSInfWeb.php, metodo movimientoGrabar";
				exit();
			}
			if(gettype($listMovi)!='array'){
				echo "El parametro \$listMovi debe ser de tipo array<Movimiento> (Arreglo de objetos Movimiento).<br>Linea 132 en ClientWSInfWeb.php, metodo movimientoGrabar";
				exit();
			}else{
				foreach ($listMovi as $value){
					if(gettype($value)!='object'){
						echo "El parametro \$listMovi debe ser de tipo array<Movimiento> (Arreglo de objetos Movimiento).<br>Linea 132 en ClientWSInfWeb.php, metodo movimientoGrabar";
						exit();
					}else{
						if(get_class($value)!='Movimiento'){
							echo "El parametro \$listMovi debe ser de tipo array<Movimiento> (Arreglo de objetos Movimiento).<br>Linea 132 en ClientWSInfWeb.php, metodo movimientoGrabar";
							exit();
						}
					}
				}
			}
			$param = array();
			$param['appId'] = $this->appId;
			$param['appPwd'] = $this->appPasswd;
			$param['comprob'] = strtoupper(iconv('', 'UTF-8', trim($comprob)));
			$param['listMovi'] = $listMovi;
			$result = $this->proxy->MovimientoGrabar($param);
			$result = $this->obj2array($result);
			$return = array();
			if($result['MovimientoGrabarResult']!=null){
				$count = 0;
				foreach ($result['MovimientoGrabarResult'] as $value){
					if(count($value)>1){
						foreach ($value as $v){
							$return[$count] = new WSError($v);
							$count++;
						}
					}else{
						$return[$count] = new WSError($value);
						$count++;
					}
				}
				return $return;
			}else{
				return array();
			}
		} catch (Exception $e) {
			echo $e->__toString();
			exit();
		}
	}
	
	
	/** Permite grabar una lista de movimientos contables, los cuales
	* debe ser del mismo tipo (mismo tipo de comprobante)
	* @param string $comprob Comprobante que va ser usado en
	* todos los movimientos contables
	* @param array<Movimiento> $listMovi Arreglo de objetos tipo Movimiento
	* @return array<Error> Arreglo de objetos Error, los cuales encasulan
	* la lista de errores encontrados al procesar la transaccion. Si
	* la transaccion se grabo con exito, el arreglo se retorna con
	* cero elementos
	*/
	public function movimientoGrabarSV($comprob, $listMovi = array()){
		try {
			if(gettype($comprob)!='string'){
				echo "El parametro \$comprob debe ser de tipo string.<br>Linea 132 en ClientWSInfWeb.php, metodo movimientoGrabar";
				exit();
			}
			if(gettype($listMovi)!='array'){
				echo "El parametro \$listMovi debe ser de tipo array<Movimiento> (Arreglo de objetos Movimiento).<br>Linea 132 en ClientWSInfWeb.php, metodo movimientoGrabar";
				exit();
			}else{
				foreach ($listMovi as $value){
					if(gettype($value)!='object'){
						echo "El parametro \$listMovi debe ser de tipo array<Movimiento> (Arreglo de objetos Movimiento).<br>Linea 132 en ClientWSInfWeb.php, metodo movimientoGrabar";
						exit();
					}else{
						if(get_class($value)!='Movimiento'){
							echo "El parametro \$listMovi debe ser de tipo array<Movimiento> (Arreglo de objetos Movimiento).<br>Linea 132 en ClientWSInfWeb.php, metodo movimientoGrabar";
							exit();
						}
					}
				}
			}
			$param = array();
			$param['appId'] = $this->appId;
			$param['appPwd'] = $this->appPasswd;
			$param['comprob'] = strtoupper(iconv('', 'UTF-8', trim($comprob)));
			$param['listMovi'] = $listMovi;
			$result = $this->proxy->MovimientoGrabarSV($param);
			$result = $this->obj2array($result);
			$return = array();
			if($result['MovimientoGrabarSVResult']!=null){
				$count = 0;
				foreach ($result['MovimientoGrabarSVResult'] as $value){
					if(count($value)>1){
						foreach ($value as $v){
							$return[$count] = new WSError($v);
							$count++;
						}
					}else{
						$return[$count] = new WSError($value);
						$count++;
					}
				}
				return $return;
			}else{
				return array();
			}
		} catch (Exception $e) {
			echo $e->__toString();
			exit();
		}
	}
	
	
	public function moviDocGrabar($comprob, $listMovi = array()){
		try {
			if(gettype($comprob)!='string'){
				echo "El parametro \$comprob debe ser de tipo string.<br>Linea 132 en ClientWSInfWeb.php, metodo moviDocGrabar";
				exit();
			}
			if(gettype($listMovi)!='array'){
				echo "El parametro \$listMovi debe ser de tipo array<Movimiento> (Arreglo de objetos MoviDoc).<br>Linea 132 en ClientWSInfWeb.php, metodo moviDocGrabar";
				exit();
			}else{
				if(count($listMovi)>1){
					foreach ($listMovi as $value){
						if(gettype($value)!='object'){
							echo "El parametro \$listMovi debe ser de tipo array<Movimiento> (Arreglo de objetos MoviDoc).<br>Linea 132 en ClientWSInfWeb.php, metodo moviDocGrabar";
							exit();
						}else{
							if(get_class($value)!='MoviDoc'){
								echo "El parametro \$listMovi debe ser de tipo array<MoviDoc> (Arreglo de objetos MoviDoc).<br>Linea 132 en ClientWSInfWeb.php, metodo moviDocGrabar";
								exit();
							}
						}
					}
				}else{
					echo "El parametro \$listMovi debe tener como minimo 2 objetos de tipo array<MoviDoc> (Arreglo de objetos MoviDoc).<br>Linea 132 en ClientWSInfWeb.php, metodo moviDocGrabar";
					exit();
				}
			}
			$param = array();
			$param['appId'] = $this->appId;
			$param['appPwd'] = $this->appPasswd;
			$param['comprob'] = strtoupper(iconv('', 'UTF-8', trim($comprob)));
			$param['listMovi'] = $listMovi;
			$result = $this->proxy->MoviDocGrabar($param);
			$result = $this->obj2array($result);
			$return = array();
			if($result['MoviDocGrabarResult']!=null){
				$count = 0;
				foreach ($result['MoviDocGrabarResult'] as $value){
					if(count($value)>1){
						foreach ($value as $v){
							$return[$count] = new WSError($v);
							$count++;
						}
					}else{
						$return[$count] = new WSError($value);
						$count++;
					}
				}
				return $return;
			}else{
				return array();
			}
		} catch (Exception $e) {
			echo $e->__toString();
			exit();
		}
	}
	
	/**
	 * Permite consultar y obtener un movimiento contable
	 * @param string $comprob
	 * @param int $num
	 * @return array Arreglo de movimientos 
	 */
	public function movimientoPorComprobante($comprob, $num){
		try {
			$param = array();
			$param['appId'] = $this->appId;
			$param['appPwd'] = $this->appPasswd;
			$param['comprob'] = strtoupper(iconv('', 'UTF-8', trim($comprob)));
			$param['num'] = $num;
			$result = $this->proxy->MovimientoPorComprobante($param);
			$result = $this->obj2array($result);
			$return = array();
			if($result['MovimientoPorComprobanteResult']!=null){
				$count = 0;
				foreach ($result['MovimientoPorComprobanteResult'] as $value){
					if(count($value)>1){
						foreach ($value as $v){
							$return[$count] = new Movimiento(0, new Fecha('1900-01-01T00:00:00'), '', 0, '', 0, '', '', '');
							$return[$count]->constructByResult($v);
							$count++;
						}
					}else{
						$return[$count] = new Movimiento(0, new Fecha('1900-01-01T00:00:00'), '', 0, '', 0, '', '', '');
						$return[$count]->constructByResult($v);
						$count++;
					}
				}
				return $return;
			}else{
				return array();
			}
		} catch (Exception $e) {
			echo $e->__toString();
			exit();
		}
	}
	
	/**
	 * Permite consultar si un comprobante existe
	 * @param string $comprob Comprobante a consultar
	 * @return boolean Valor que indica si el comprobante existe
	 */
	public function comprobanteUltimo($comprob){
		try {
			if(gettype($comprob)!='string'){
				echo "El parametro \$comprob debe ser de tipo string.<br>Linea 190 en ClientWSInfWeb.php, metodo comprobanteUltimo";
				exit();
			}
			$param = array();
			$param['appId'] = $this->appId;
			$param['appPwd'] = $this->appPasswd;
			$param['comprob'] = strtoupper(iconv('', 'UTF-8', trim($comprob)));
			$result = $this->proxy->ComprobanteUltimo($param);
			$result = $this->obj2array($result);
			return $result['ComprobanteUltimoResult'];
		} catch (Exception $e) {
			echo $e->__toString();
			exit();
		}
	}
	
	/**
	 * Permite consultar si un movimiento contable existe
	 * @param string $comprob Comprobante del movimiento contable
	 * @param int $numero Numero del movimiento contable
	 * @return boolean Valor que indica si el movimiento existe
	 */
	public function movimientoExiste($comprob, $numero){
		try {
			if(gettype($comprob)!='string'){
				echo "El parametro \$comprob debe ser de tipo string.<br>Linea 215 en ClientWSInfWeb.php, metodo movimientoExiste";
				exit();
			}
			if(gettype($numero)!='integer'){
				echo "El parametro \$numero debe ser de tipo integer (Entero).<br>Linea 215 en ClientWSInfWeb.php, metodo movimientoExiste";
				exit();
			}
			$param = array();
			$param['appId'] = $this->appId;
			$param['appPwd'] = $this->appPasswd;
			$param['comprob'] = strtoupper(iconv('', 'UTF-8', trim($comprob)));
			$param['numero'] = $numero;
			$result = $this->proxy->MovimientoExiste($param);
			$result = $this->obj2array($result);
			return $result['MovimientoExisteResult'];
		} catch (Exception $e) {
			echo $e->__toString();
			exit();
		}
	}
	
	public function movimientoNumero($comprob, $nit, $fecha, $valor){
		try {
			if(gettype($fecha)!='object'){
				echo "El parametro \$fecha debe ser de tipo Fecha.<br>Linea 344 en ClientWSInfWeb.php, metodo movimientoNumero";
				exit();
			}else{
				if(get_class($fecha)!='Fecha'){
					echo "El parametro \$fecha debe ser de tipo Fecha.<br>Linea 344 en ClientWSInfWeb.php, metodo movimientoNumero";
					exit();
				}
			}
			$param = array();
			$param['appId'] = $this->appId;
			$param['appPwd'] = $this->appPasswd;
			$param['comprob'] = strtoupper(iconv('', 'UTF-8', trim($comprob)));
			$param['nit'] = $nit;
			$param['fecha'] = $fecha->__toString();
			$param['valor'] = $valor;
			$result = $this->proxy->MovimientoNumero($param);
			$result = $this->obj2array($result);
			return $result['MovimientoNumeroResult'];
		} catch (Exception $e) {
			echo $e->__toString();
			exit();
		}
	}
	
	public function movimientoNumeroStr($comprob, $nit, $fecha, $valor){
		try {
			if(gettype($fecha)!='object'){
				echo "El parametro \$fecha debe ser de tipo Fecha.<br>Linea 344 en ClientWSInfWeb.php, metodo movimientoNumero";
				exit();
			}else{
				if(get_class($fecha)!='Fecha'){
					echo "El parametro \$fecha debe ser de tipo Fecha.<br>Linea 344 en ClientWSInfWeb.php, metodo movimientoNumero";
					exit();
				}
			}
			$param = array();
			$param['appId'] = $this->appId;
			$param['appPwd'] = $this->appPasswd;
			$param['comprob'] = strtoupper(iconv('', 'UTF-8', trim($comprob)));
			$param['nit'] = $nit;
			$param['fecha'] = $fecha->__toString();
			$param['valor'] = $valor;
			$result = $this->proxy->MovimientoNumeroStr($param);
			$result = $this->obj2array($result);
			return $result['MovimientoNumeroStrResult'];
		} catch (Exception $e) {
			echo $e->__toString();
			exit();
		}
	}
	
	/**
	 * Permite consultar la informacion basica de un empleado
	 * @param int $ident Numero de identificacion del empleado
	 * @return InfoEmpleado Objeto que encapsula la informacion basica del empleado
	 */
	public function empleadoInfo($ident){
		try {
			if(gettype($ident)!='integer'){
				echo "El parametro \$ident debe ser de tipo integer (Entero).<br>Linea 244 en ClientWSInfWeb.php, metodo empleadoInfo";
				exit();
			}
			$param = array();
			$param['appId'] = $this->appId;
			$param['appPwd'] = $this->appPasswd;
			$param['cedula'] = iconv('', 'UTF-8', trim($ident));
			$result = $this->proxy->EmpleadoInfo($param);
			$result = $this->obj2array($result);
			$aux = new InfoEmpleado($result['EmpleadoInfoResult']);
			return $aux;
		} catch (Exception $e) {
			echo $e->__toString();
			exit();
		}
	}
	
	/**
	 * Permite consultar el sueldo de un empleado por su numero de identificacion
	 * @param int $ident Numero de identificacion del empleado
	 * @return double Sueldo del empleado
	 */
	public function empleadoSueldo($ident){
		try {
			if(gettype($ident)!='integer'){
				echo "El parametro \$ident debe ser de tipo integer (Entero).<br>Linea 269 en ClientWSInfWeb.php, metodo empleadoSueldo";
				exit();
			}
			$param = array();
			$param['appId'] = $this->appId;
			$param['appPwd'] = $this->appPasswd;
			$param['cedula'] = iconv('', 'UTF-8', trim($ident));
			$result = $this->proxy->EmpleadoSueldo($param);
			$result = $this->obj2array($result);
			return round($result['EmpleadoSueldoResult'],2);
		} catch (Exception $e) {
			echo $e->__toString();
			exit();
		}
	}
	
	/**
	 * Permite consultar la ultima fecha de cierre
	 * @return fecha ultimo cierra
	 */
	public function ultimoCierreContable(){
		try {
			$param = array();
			$param['appId'] = $this->appId;
			$param['appPwd'] = $this->appPasswd;
			$result = $this->proxy->UltimoCierreContable($param);
			$result = $this->obj2array($result);
			$return = array();
			if($result['UltimoCierreContableResult']!=null)
			{
				if($result['UltimoCierreContableResult']=="")
				{
					return array('Tipo'=>'Error','valor'=>'Error Desconocido, comunicarse con TI');
				}
				else
				{
					$fecha=substr($result['UltimoCierreContableResult'],4,4)."-".substr($result['UltimoCierreContableResult'],0,2)."-".substr($result['UltimoCierreContableResult'],2,2);
					return array('Tipo'=>'FechaUltimoCierre','valor'=>date('Y-m-d',strtotime($fecha)));
					//return array('Tipo'=>'FechaUltimoCierre','valor'=>$fecha);
				}
			}
			else
			{
				return array('Tipo'=>'Error','valor'=>'Error Desconocido, comunicarse con TI');
			}
		} catch (Exception $e) {
			echo $e->__toString();
			exit();
		}
	}
	
	private function obj2array($obj) {
		$out = array();
		foreach ($obj as $key => $val) {
			switch(true) {
				case is_object($val): $out[$key] = $this->obj2array($val); break;
				case is_array($val): $out[$key] = $this->obj2array($val); break;
				default: $out[$key] = $val;
			}
		}
		return $out;
	}
}

?>