<?php
date_default_timezone_set('America/Bogota');

include_once 'Sigas/Periodo.php';
include_once 'Sigas/Afiliacion.php';
include_once 'Sigas/Afiliado.php';
include_once 'Sigas/SAfiliado.php';
include_once 'Sigas/Empresa.php';

/**
 * Provee m�todos para consumir los servicios web de sigas
 * @author Juan Fernando Tamayo Puertas
 * @version 2.0
 * @tutorial Se agrego 4 m�todo: trayectoriaAfiliadoConsultar, empresaConsultar, afiliadoConsultar, periodoActual.
 */
class ClientWSSigas {
	
	private $appId;
	private $appPasswd;
	private $proxy;
	
	/**
	 * Constructor de la clase
	 * @param string $appId C�digo de identificacion del cliente
	 * @param string $appPasswd Contrase�a del cliente
	 */
	public function __construct($appId, $appPasswd){
		try {
			$this->proxy = new SoapClient('http://10.10.1.60/WebServices/Services/WSSigas.svc?wsdl', array('cache_wsdl' => 0));
			$this->appId = $appId;
			$this->appPasswd = $appPasswd;
		} catch (Exception $e) {
			echo $e->__toString();
			exit();
		}
	}
	
	/**
	 * Permite consultar si una ruta existe en el repositorio de digitalizacion
	 * @param string $ruta
	 * @return boolean valor que indica si la ruta existe
	 */
	public function rutaDigitalizacionExiste($ruta){
		try {
			$param = array();
			$param['appId'] = $this->appId;
			$param['appPwd'] = $this->appPasswd;
			$param['path'] = iconv('', 'UTF-8', trim($ruta));
			$result = $this->proxy->RutaDigitalizacionExiste($param);
			$result = $this->obj2array($result);
			return $result['RutaDigitalizacionExisteResult'];
		} catch (Exception $e) {
			echo $e->__toString();
			exit();
		}
	}
	
	/**
	 * Permite crear una ruta en el repositorio de digitalizacion
	 * @param string $ruta
	 * @return boolean valor que indica si la ruta se creo
	 */
	public function rutaDigitalizacionCrear($ruta){
		try {
			$param = array();
			$param['appId'] = $this->appId;
			$param['appPwd'] = $this->appPasswd;
			$param['path'] = iconv('', 'UTF-8', trim($ruta));
			$result = $this->proxy->RutaDigitalizacionCrear($param);
			$result = $this->obj2array($result);
			return $result['RutaDigitalizacionCrearResult'];
		} catch (Exception $e) {
			echo $e->__toString();
			exit();
		}
	}
	
	/**
	 * Permite migrar (mover) los archivos de una ruta a otra en el repositorio de digitalizacion
	 * @param string $origen
	 * @param string $destino
	 * @return boolean valor que indica si se migro los archivos
	 */
	public function rutaDigitalizacionMigrar($origen, $destino){
		try {
			$param = array();
			$param['appId'] = $this->appId;
			$param['appPwd'] = $this->appPasswd;
			$param['sourcePath'] = iconv('', 'UTF-8', trim($origen));
			$param['destinationPath'] = iconv('', 'UTF-8', trim($destino));
			$result = $this->proxy->RutaDigitalizacionMigrar($param);
			$result = $this->obj2array($result);
			return $result['RutaDigitalizacionMigrarResult'];
		} catch (Exception $e) {
			echo $e->__toString();
			exit();
		}
	}
	
	/**
	 * Permite consultar los datos del periodo actual o en curso
	 * @return Periodo El periodo en curso
	 */
	public function periodoActual(){
		try {
			$param = array();
			$param['appId'] = $this->appId;
			$param['appPwd'] = $this->appPasswd;
			$result = $this->proxy->PeriodoActual($param);
			$result = $this->obj2array($result);
			return new Periodo($result['PeriodoActualResult']);
		} catch (Exception $e) {
			echo $e->__toString();
			exit();
		}
	}
	
	public function afiliadoConsultar($tipoDocumento, $identificacion){
		try {
			$param = array();
			$param['appId'] = $this->appId;
			$param['appPwd'] = $this->appPasswd;
			$param['tipoDocumento'] = $tipoDocumento;
			$param['identificacion'] = iconv('', 'UTF-8', trim($identificacion));
			$result = $this->proxy->AfiliadoConsultar($param);
			$result = $this->obj2array($result);
			return new Afiliado($result['AfiliadoConsultarResult']);
		} catch (Exception $e) {
			echo $e->__toString();
			exit();
		}
	}
	
	public function empresaConsultar($tipoDocumento, $nit){
		try {
			$param = array();
			$param['appId'] = $this->appId;
			$param['appPwd'] = $this->appPasswd;
			$param['tipoDocumento'] = $tipoDocumento;
			$param['nit'] = iconv('', 'UTF-8', trim($nit));
			$result = $this->proxy->EmpresaConsultar($param);
			$result = $this->obj2array($result);
			return new Empresa($result['EmpresaConsultarResult']);
		} catch (Exception $e) {
			echo $e->__toString();
			exit();
		}
	}
	
	public function trayectoriaAfiliadoConsultar($tipoDocumento, $identificacion){
		try {
			$param = array();
			$param['appId'] = $this->appId;
			$param['appPwd'] = $this->appPasswd;
			$param['tipoDocumento'] = $tipoDocumento;
			$param['identificacion'] = iconv('', 'UTF-8', trim($identificacion));
			$result = $this->proxy->TrayectoriaAfiliadoConsultar($param);
			$result = $this->obj2array($result);
			$ret = array();
			if($result['TrayectoriaAfiliadoConsultarResult']!=null){
				$count = 0;
				foreach ($result['TrayectoriaAfiliadoConsultarResult'] as $value) {
					foreach ($value as $v) {
						if(is_array($v)){
							$ret[$count] = new Afiliacion($v);
							$count++;
						}else{
							$ret[$count] = new Afiliacion($value);
							break;
						}
					}
				}
			}
			return $ret;
		} catch (Exception $e) {
			echo $e->__toString();
			exit();
		}
	}
	
	public function periodoConsultar($periodo){
		try {
			$param = array();
			$param['appId'] = $this->appId;
			$param['appPwd'] = $this->appPasswd;
			$param['periodo'] = iconv('', 'UTF-8', trim($periodo));
			$result = $this->proxy->PeriodoConsultar($param);
			$result = $this->obj2array($result);
			return new Periodo($result['PeriodoConsultarResult']);
		} catch (Exception $e) {
			echo $e->__toString();
			exit();
		}
	}
	
	public function afiliadoSimpleConsultar($tipoDocumento, $identificacion){
		try {
			$param = array();
			$param['appId'] = $this->appId;
			$param['appPwd'] = $this->appPasswd;
			$param['tipoDocumento'] = $tipoDocumento;
			$param['identificacion'] = iconv('', 'UTF-8', trim($identificacion));
			$result = $this->proxy->AfiliadoSimpleConsultar($param);
			$result = $this->obj2array($result);
			return new SAfiliado($result['AfiliadoSimpleConsultarResult']);
		} catch (Exception $e) {
			echo $e->__toString();
			exit();
		}
	}
	
	public function ping(){
		try {
			$result = $this->proxy->Ping();
			$result = $this->obj2array($result);
			return $result['PingResult'];
		} catch (Exception $e) {
			echo $e->__toString();
			exit();
		}
	}
	
	private function obj2array($obj) {
		$out = array();
		foreach ($obj as $key => $val) {
			switch(true) {
				case is_object($val): $out[$key] = $this->obj2array($val); break;
				case is_array($val): $out[$key] = $this->obj2array($val); break;
				default: $out[$key] = $val;
			}
		}
		return $out;
	}
}

?>