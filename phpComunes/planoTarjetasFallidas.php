<?php
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$fecha=date('Ymd');
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

function cambiarNn($cadenaD)
{
	$cadenaD=substr($cadenaD,0,15);
	if(strstr($cadenaD,"�"))
	{
		$cadenaD=str_replace("�","N",$cadenaD);	
	}
	if(strstr($cadenaD,"�"))
	{
		$cadenaD=str_replace("�","N",$cadenaD);	
	}
	return $cadenaD;
}
$ruta= $ruta_generados. DIRECTORY_SEPARATOR.'asopagos'.DIRECTORY_SEPARATOR.'novedades_no'.DIRECTORY_SEPARATOR;
//$ruta= "/var/www/html/planos_generados/asopagos/novedades_no/";
$c1='A';	// Car�cter Fijo 
$c2='01';	// C�digo del bolsillo (d�bito).
$c3='032';	// Codigo de la caja seg�n Supersubsidio (Ej: 032 Huila).
$c4=date('ymd');	// fecha envio.
$c5=str_pad("1",3,"0",STR_PAD_LEFT);
$temp=$c5;
$archivo=$c1.$c2.$c3.$c4.$c5.".txt";
$path =$ruta.$archivo;
$handle=fopen($path, "w");
fclose($handle);

$sql = "SELECT DISTINCT bono, aportes015.idtipodocumento, aportes015.idpersona, aportes015.identificacion, papellido, sapellido, pnombre, snombre, nombrecorto, fechanacimiento, sexo, idestadocivil, direccion, iddepresidencia, idciuresidencia,telefono,aportes016.estado,aportes016.primaria FROM aportes101 
INNER JOIN aportes016 ON aportes101.idpersona = aportes016.idpersona AND aportes016.estado='A' AND aportes016.primaria='S'
INNER JOIN aportes015 ON aportes101.idpersona = aportes015.idpersona 
WHERE idetapa=61 and barraok='N'";
$rs = $db->querySimple($sql);
$con=0;
$espacio=" ";
$dir="";
$tel="";
$iddepto="";
$idciud="";

while ($row = $rs->fetch()){
	$con++;
	$idpersona=$row['idpersona'];
	$idtd=$row['idtipodocumento'];
	$numero=$row['identificacion'];
	$bono=$row['bono'];
	$pnombre=$row['pnombre'];
	$snombre=$row['snombre'];
	$sapellido=$row['sapellido'];
	$papellido=$row['papellido'];
	$nombrerealce=$row['nombrecorto'];
	
	$pnombre=cambiarNn($pnombre);
	$snombre=cambiarNn($snombre);
	$papellido=cambiarNn($papellido);
	$sapellido=cambiarNn($sapellido);
	$nombrerealce=str_replace("�","N",$nombrerealce);	
	$nombrerealce=str_replace("�","N",$nombrerealce);	
	
	if(strlen(trim($nombrerealce))==0){
		echo "Faltan nombres de realce. linea ".$con;
	}
	
	if(strlen(trim($row['direccion']))==0){
		$dir="CALLE 11 5 13";
	}
	else{
		$dir=$row['direccion'];
		if(strstr($dir,"�"))
		{
			$dir=str_replace("�","N",$dir);	
		}
		if(strstr($dir,"�"))
		{
			$dir=str_replace("�","N",$dir);	
		}
	}
	
	if(strlen(trim($row['telefono']))==0){
		$tel='8759544';
	}
	else{
		$tel=$row['telefono'];
	}
	
	if(strlen(trim($row['iddepresidencia']))==0){
		$iddepto='41';
	}
	else{
		$iddepto=$row['iddepresidencia'];
	}
	
	if(strlen(trim($row['idciuresidencia']))==0){
		$idciud='41001';
	}
	else{
		$idciud=$row['idciuresidencia'];
	}
	
	$nombrecorto=$pnombre." ".$papellido;
	if(strlen(trim($row['fechanacimiento']))==0){
		echo 2;
		exit();
	}
	$fechanace=explode("-",$row['fechanacimiento']);
	$fechan=$fechanace[0].$fechanace[1].$fechanace[2];
	
	switch ($idtd){
		case 1 : $td=2; break;
		case 2 : $td=4; break;
		case 4 : $td=3; break;
		default:$td=9;
	}
	switch ($row['idestadocivil']){
		case 50 : $estado = 1; break;
		case 51 : $estado = 5; break;
		case 52 : $estado = 4; break;
		case 53 : $estado = 2; break;
		case 54 : $estado = 3; break;
		default:$estado = 9;
	}
	switch($row['sexo']){
		case 'M' : $s=1; break;
		case 'F' : $s=2; break;
		default:$s = 1;
	}
$c1='01';//tipo novedad
$c2=str_pad("0",14,"0");//num producto ref
//$c3=str_pad($bono,16,"0",STR_PAD_LEFT);//num tarjeta
$c3=str_pad($bono,19,$espacio,STR_PAD_RIGHT);
$c4="N";//tipo persona
$c5=$td;//tipo identifiacion
$c6=str_pad($numero,15,"0",STR_PAD_LEFT);//num identificacion
$c7=str_pad($papellido,15);//papellido
$c8=str_pad($sapellido,15);//sapellido
$c9=str_pad($pnombre,15);//pnombre
$c10=str_pad($snombre,15);//snombre
$nc=substr($nombrecorto,0,20);//nombre corto
$c11=str_pad($nc,20);
$nc1=substr($nombrerealce,0,26);
$c12=str_pad($nc1,26);
$c13=$fechan;//fecha nacimiento
$c14=$s;//sexo
$c15=$estado;//estadocivil
$c16=str_pad($dir,40); // str_pad($row['direccion'],40);
$c17=str_pad($dir,40); //str_pad($row['direccion'],40);
$c18=str_pad($iddepto,2);
$c19=str_pad($idciud,5);
$c20='999999'; //str_pad("9",6);
$c21=str_pad($dir,40); //str_pad($row['direccion'],40);
$c22=str_pad($dir,40); //str_pad($row['direccion'],40);
$c23=str_pad($iddepto,2);
$c24=str_pad($idciud,5);
$c25='999999'; //pad("9",6);
$c26=str_pad("0",5,'0');//oficina radicacion
$c27=str_pad($tel,14);
$c28=str_pad($tel,14);
$c29=str_pad("0",5,'0');
$c30=str_pad("0",15,'0');
$c31='00000000'; //$fecha;
$c32='000'; //str_pad($espacio,3);
$c33='002';//grupo manejo cutas
$c34='001';//tipo tarjeta
$c35='000000'; //str_pad($espacio,6);
$c36=$espacio;//estado registro recibido
$c37="    ";//cod error
$c38=str_pad('0',10,'0');//numero solicitud
$c39=str_pad($espacio,19);//tarjeta anterior
$c40='000';//cod punto de distribucion
$c41=209;//cod barras convenio mercadeo
$c42=str_pad($espacio,2);//cod bloqueo tarjeta
$c43=str_pad($espacio,2);//motivo del bloqueo
$c44='01';//tipo de cuenta
$c45=str_pad($espacio,36);
	$cadena=$c1.$c2.$c3.$c4.$c5.$c6.$c7.$c8.$c9.$c10.$c11.$c12.$c13.$c14.$c15.$c16.$c17.$c18.$c19.$c20.$c21.$c22.$c23.$c24.$c25.$c26.$c27.$c28.$c29.$c30.$c31.$c32.$c33.$c34.$c35.$c36.$c37.$c38.$c39.$c40.$c41.$c42.$c43.$c44.$c45."\r\n";
	$handle=fopen($path, "a");
	fwrite($handle, $cadena);
	fclose($handle);
}
$rs->closeCursor();

$c1="000000900319291";  //nit
$c2=$fecha;
$c3=str_pad($temp,5,"0",STR_PAD_LEFT);	//consecutivo
$c4=str_pad($con, 6, "0",STR_PAD_LEFT);			//num de registros
$c5="00"; //str_pad('0',2);		//cod area
$c6=str_pad($espacio, 427);		//filler
$c7="636480";					//prefijo convenio
$c8="001";						//subtipo
$c9='T';	//str_pad('T',3,$espacio);	//trailer
$c10=str_pad($espacio, 3);

$cadena=$c1.$c2.$c3.$c4.$c5.$c6.$c7.$c8.$c9.$c10;
$handle=fopen($path, "a");
if($handle)
{
	if($con==0){
		echo 3; // no hay registros
		exit();
	}
	fwrite($handle, $cadena);
	fclose($handle);
}
$enlace= $ruta_generados. DIRECTORY_SEPARATOR.'asopagos'.DIRECTORY_SEPARATOR.'novedades_no'.DIRECTORY_SEPARATOR.$archivo;
//$enlace="/var/www/html/planos_generados/asopagos/novedades_no/".$archivo;
$_SESSION['ENLACE']=$enlace;
?>