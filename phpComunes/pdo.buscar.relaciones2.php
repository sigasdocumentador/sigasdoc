﻿<?php
ini_set("display_errors","1");
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$idpersona= $_REQUEST['v0'];
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

$sql = "SELECT a21.idbeneficiario,b91.codigo,a15.identificacion,a15.pnombre,a15.snombre,a15.papellido,a15.sapellido,a21.conviven,a15.fechanacimiento,a21.fechasistema,
		a15.sexo,a21.estado estadoRelacion,a91.detalledefinicion,CASE WHEN a16.estado IS NULL THEN 'N' ELSE 'S' END labora, a16.salario,a16.fechaingreso,a21.idrelacion,a21.idconyuge
		FROM aportes021 a21 INNER JOIN aportes015 a15 ON a21.idbeneficiario=a15.idpersona LEFT JOIN aportes016 a16
		ON a16.idformulario=(SELECT TOP 1 b16.idformulario FROM aportes016 b16 WHERE a21.idbeneficiario=b16.idpersona AND b16.estado='A' ORDER BY b16.fechaingreso)
		INNER JOIN aportes091 a91 ON a21.idparentesco=a91.iddetalledef INNER JOIN aportes091 b91 ON b91.iddetalledef=a15.idtipodocumento WHERE a21.idparentesco=34 AND a21.idtrabajador=$idpersona";
$rs=$db->querySimple($sql);
$con=0;
$filas = array();
while($row=$rs->fetch()){
	$filas[]= $row;
	$con++;
	}
if($con==0){
	echo 0;
}
else{
	echo  json_encode($filas);
}
?>