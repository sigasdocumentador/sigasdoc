<?php
/* autor:       orlando puentes
 * fecha:       mar-05-2012
 * objetivo:    
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$idp=$_REQUEST['v0'];
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'clases' .  DIRECTORY_SEPARATOR . 'p.pignoracion.class.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
$objPignoracion = new Pignoracion();
$rsPignoracion = $objPignoracion->buscar_pignoracion_por_id($idp);
$pignoracion = mssql_fetch_assoc($rsPignoracion);
$sql="SELECT idbeneficiario, periodo, periodoproceso, dbo.aportes044.fechasistema, valor, anulado,rtrim(pnombre) + ' ' + CASE 
     WHEN snombre is NULL THEN '' 
     ELSE rtrim(snombre) END +' '+ rtrim(papellido) +' '+
     CASE 
     WHEN sapellido is NULL THEN '' 
     ELSE rtrim(sapellido) END AS nombre
FROM dbo.aportes044
INNER JOIN aportes015 ON aportes044.idbeneficiario=aportes015.idpersona where idpignoracion=$idp";

$rs=$db->querySimple($sql);
?>
<html>

<center>
	<table width="578" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" class="tablero">
		<tbody>
			<tr bgcolor="#EBEBEB">
			  <td colspan="5" style="text-align:center" >Detalle de la Pignoraci&oacute;n No. <?php echo $idp; ?>(<strong>Pagare <?php echo $pignoracion["pagare"]; ?></strong>) - <strong style="font-size:12pt">Saldo: $ <?php echo number_format($pignoracion["saldo"]); ?></strong></span></td>
			</tr>
			<tr>
				<td >Beneficiario</td>
				<td >Periodo</td>
				<td >Fecha</td>
				<td >Valor</td>
				<td >Anulado</td>
			</tr>
		<?php	while ($row=$rs->fetch()){ ?>        
			<tr>
			  <td ><?php echo $row['nombre']; ?></td>
			  <td style="text-align:center" ><?php echo $row['periodo']; ?>&nbsp;</td>
			  <td style="text-align:center" ><?php echo $row['fechasistema']; ?>&nbsp;</td>
			  <td style="text-align:center" ><?php echo number_format($row['valor']); ?>&nbsp;</td>
			  <td style="text-align:center" ><?php echo $row['anulado']; ?></td>
		  </tr>
		<?php } ?>
	    </tbody>
	</table>
</center>
</html>