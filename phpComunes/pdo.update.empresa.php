<?php 
/*
* @autor:      Ing. Orlando Puentes
* @fecha:      04/09/2010
* objetivo:
*/
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$usuario=$_SESSION['USUARIO'];

$campo0=isset($_REQUEST['v0']) ? $_REQUEST['v0'] : "";		//idempresa
$campo1=isset($_REQUEST['v1']) ? $_REQUEST['v1'] : "";		//Tipo Documento
$campo2=trim($_REQUEST['v2']);	// Nit
$campo3=$_REQUEST['v3'];	// Digito
$campo4=$campo4=isset($_REQUEST['v4']) ? $_REQUEST['v4'] : "";	// Nit RSN
$campo5=isset($_REQUEST['v5']) ? $_REQUEST['v5'] : "";	// Sucursal 
$campo6=isset($_REQUEST['v6']) ? $_REQUEST['v6'] : "";		// Principal
$campo7=isset($_REQUEST['v7']) ? strtoupper(trim(str_replace(38,"&",$_REQUEST['v7']))) : "";		// Razon Social	
$campo8=isset($_REQUEST['v8']) ? strtoupper(trim(str_replace(38,"&",$_REQUEST['v8']))) : "";		// Sigla
$campo9=isset($_REQUEST['v9']) ? $_REQUEST['v9'] : "";		// Direccion
$campo10=isset($_REQUEST['v10']) ? $_REQUEST['v10'] : "";	// Departamento
$campo11=isset($_REQUEST['v11']) ? $_REQUEST['v11'] : "";	// Ciudad

$campo12=isset($_REQUEST['v12']) ? $_REQUEST['v12'] : "";   // Zona
$campo13=isset($_REQUEST['v13']) ? $_REQUEST['v13'] : "";	// Tel
$campo14=isset($_REQUEST['v14']) ? $_REQUEST['v14'] : "";	// Fax
$campo15=isset($_REQUEST['v15']) ? $_REQUEST['v15'] : "";	// Url
$campo16=isset($_REQUEST['v16']) ? $_REQUEST['v16'] : "";	// Email
$campo18=isset($_REQUEST['v18']) ? $_REQUEST['v18'] : "";	// Repr.
$campo19=isset($_REQUEST['v19']) ? $_REQUEST['v19'] : "";	// Contacto
$campo20=isset($_REQUEST['v20']) ? $_REQUEST['v20'] : "";	// Contrato
$campo21='N';  //isset($_REQUEST['v21']) ? $_REQUEST['v21'] : "";	// Colegio
$campo22='N'; //isset($_REQUEST['v22']) ? $_REQUEST['v22'] : "";	// Excento
$campo23=isset($_REQUEST['v23']) ? $_REQUEST['v23'] : "";	// Cod. Actividad
$campo24=isset($_REQUEST['v24']) ? $_REQUEST['v24'] : "";	// Cod. Dane
$campo25=isset($_REQUEST['v25']) ? $_REQUEST['v25'] : "";	// Indicador
$campo26=isset($_REQUEST['v26']) ? $_REQUEST['v26'] : "";	// Asesor
$campo28=isset($_REQUEST['v28']) ? $_REQUEST['v28'] : "";	// F. Matricula
$campo30=isset($_REQUEST['v30']) ? $_REQUEST['v30'] : "";	// Sector
$campo31=isset($_REQUEST['v31']) ? $_REQUEST['v31'] : "";	// SEccional
$campo32=isset($_REQUEST['v32']) ? $_REQUEST['v32'] : "";	// Tipo Persona  = CLASE SOCIEDAD //OJOOOOOOOOOOOOOOO
$campo33=isset($_REQUEST['v33']) ? $_REQUEST['v33'] : "";	// Clase Aportante
$campo34=isset($_REQUEST['v34']) ? $_REQUEST['v34'] : "";	// Tipo Aportante
$campo35=isset($_REQUEST['v35']) ? $_REQUEST['v35'] : "";	// Estado
$campo36=isset($_REQUEST['v36']) ? $_REQUEST['v36'] : "";	// Cod. EStado
$campo37=isset($_REQUEST['v37']) ? $_REQUEST['v37'] : "";	// F. EStado
$campo38=isset($_REQUEST['v38']) ? $_REQUEST['v38'] : "";	// F. Aportes
$campo39=isset($_REQUEST['v39']) ? $_REQUEST['v39'] : "";	// F. Afiliacion
$campo40=isset($_REQUEST['v40']) ? $_REQUEST['v40'] : "";	// Trabajadores
$campo41=isset($_REQUEST['v41']) ? $_REQUEST['v41'] : "";	// Aportantes
$campo42=isset($_REQUEST['v42']) ? $_REQUEST['v42'] : "";	// Conyuges
$campo43=isset($_REQUEST['v43']) ? $_REQUEST['v43'] : "";	// Hijos 
$campo44=isset($_REQUEST['v44']) ? $_REQUEST['v44'] : "";	// Hermanos
$campo45=isset($_REQUEST['v45']) ? $_REQUEST['v45'] : "";	// Padres
$campo46=isset($_REQUEST['v46']) ? $_REQUEST['v46'] : "";	// Tempo
$campo47=isset($_REQUEST['v47']) ? $_REQUEST['v47'] : "";	// Flag
$campo48=$usuario; //isset($_REQUEST['v48']) ? $_REQUEST['v48'] : "";	// Usuario
$campo49=isset($_REQUEST['v49']) ? $_REQUEST['v49'] : "";	// F. Sistema
$campo50=isset($_REQUEST['v50']) ? $_REQUEST['v50'] : "";	// Clases Sociedad 
$campo51=isset($_REQUEST['v51']) ? $_REQUEST['v51'] : "";	// Ruta Documentos
$campo52=isset($_REQUEST['v52']) ? $_REQUEST['v52'] : "";	// LEgalizadas
$campo53=isset($_REQUEST['v53']) ? $_REQUEST['v53'] : "";	// Renovacion
$campo54=isset($_REQUEST['v54']) ? $_REQUEST['v54'] : "";	// Direccion correspondencia
$campo55=isset($_REQUEST['v55']) ? $_REQUEST['v55'] : "";	// Departamento correspondencia
$campo56=isset($_REQUEST['v56']) ? $_REQUEST['v56'] : "";	// Ciudad correspondencia
$campo57=isset($_REQUEST['v57']) ? $_REQUEST['v57'] : "";	// Barrio
$campo58=isset($_REQUEST['v58']) ? $_REQUEST['v58'] : "";	// Celular
$campo59=isset($_REQUEST['v59']) ? $_REQUEST['v59'] : "";	// Barrio correspondencia
$campo60=isset($_REQUEST['v60']) ? $_REQUEST['v60'] : "";	// tipo telefono

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
include_once $raiz.DIRECTORY_SEPARATOR.'aportes'.DIRECTORY_SEPARATOR.'empresas'.DIRECTORY_SEPARATOR .'clases'.DIRECTORY_SEPARATOR .'empresas.class.php';

$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

$objEmpresa = new Empresa();
$consulta = $objEmpresa->buscar_empresa_por_idempresa($campo0);
$empresa = mssql_fetch_array($consulta);


//Control para el estado
if($campo35=="null")$campo35 = "";
// si cambia el estado, debe cambiar la fecha de estado

if($empresa["estado"] != $campo35 && $campo35 != "")
	$campo37 = "cast(getdate() as date)";

if($campo37=="")$campo37 = "null";

$sql="UPDATE aportes048 SET idtipodocumento = '$campo1', nit = '$campo2', digito = '$campo3', nitrsn = '$campo4', codigosucursal = '$campo5', principal = '$campo6', razonsocial = '$campo7', sigla = '$campo8', direccion = '$campo9', iddepartamento = '".$campo10."', idciudad = '".$campo11."', idzona = '$campo12', telefono = '$campo13', fax = '$campo14', url = '$campo15', email = '$campo16', idrepresentante = '$campo18', idjefepersonal = '$campo19', contratista = '$campo20', colegio = '$campo21', exento = '$campo22', idcodigoactividad = '$campo23', actieconomicadane = '$campo24', indicador = '$campo25', idasesor = '$campo26', fechamatricula = '$campo28', idsector = '$campo30', seccional = '$campo31', tipopersona = '$campo32', claseaportante = '$campo33', tipoaportante = '$campo34', estado = '$campo35', codigoestado = '$campo36', fechaestado = $campo37, fechaaportes = '$campo38', fechaafiliacion = '$campo39', trabajadores = '$campo40', aportantes = '$campo41', conyuges = '$campo42', hijos = '$campo43', hermanos = '$campo44', padres = '$campo45', tempo = '$campo46', flag = '$campo47', usuario = '$campo48', fechasistema = cast(getdate() as date), idclasesociedad = '$campo50', rutadocumentos = '$campo51', legalizada = '$campo52', renovacion = '$campo53', direcorresp = '$campo54', iddepcorresp = '".$campo55."', idciucorresp = '".$campo56."', idbarrio = '".$campo57."', celular = '".$campo58."', idbarriocorresp='".$campo59."',id_tipo_tel ='".$campo60."' WHERE idempresa=$campo0";
$rs=$db->queryActualiza($sql,'aportes048');
$error=$db->error;
if(is_array($error)){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
echo $rs;
?>
