<?php
// @autor:		Orlando Puentes A
// @fecha:		mayo 31/2010
// @formulario:	guardar datos persona - modulo empresa
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'p.persona.class.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

$campo0 = $_REQUEST['v0'];
$campo1 = trim($_REQUEST['v1']);
$campo2 = $_REQUEST['v2'];
$campo3 = $_REQUEST['v3'];
$campo4 = strtoupper(htmlspecialchars(trim($_REQUEST['v4'])));
$campo5 = strtoupper(htmlspecialchars(trim($_REQUEST['v5'])));
$campo6 = strtoupper(htmlspecialchars(trim($_REQUEST['v6'])));
$campo7 = strtoupper(htmlspecialchars(trim($_REQUEST['v7'])));
$campo8 = $_REQUEST['v8'];
$campo9 = $_REQUEST['v9'];
$campo10 = (isset($_REQUEST['v10']) && $_REQUEST['v10'] != 'null')?$_REQUEST['v10']:null;
$campo11 = $_REQUEST['v11'];
$campo12 = $_REQUEST['v12'];
$campo13 = $_REQUEST['v13'];
$campo14 = $_REQUEST['v14'];
$campo15 = $_REQUEST['v15'];
$campo16 = (isset($_REQUEST['v16']) && $_REQUEST['v16'] != 'null')?$_REQUEST['v16']:null;
$campo17 = (isset($_REQUEST['v17']) && $_REQUEST['v17'] != 'null')?$_REQUEST['v17']:null;
$campo18 = $_REQUEST['v18'];
$campo19 = $_REQUEST['v19'];
$campo20 = $_REQUEST['v20'];
$campo21 = (isset($_REQUEST['v21']) && $_REQUEST['v21'] != 'null')?$_REQUEST['v21']:null;
$campo22 = $_REQUEST['v22'];
$campo23 = $_REQUEST['v23'];
$campo24 = $_REQUEST['v24'];
$campo25 = $_SESSION['USUARIO'];
$campo26 = $_REQUEST['v25'];
$campo27 = $_REQUEST['v26'];

//idtipodocumento identificacion papellido sapellido pnombre snombre sexo fechanacimiento capacidadtrabajo

$sql="insert into aportes015(idtipodocumento, identificacion, pnombre,snombre, papellido,sapellido, sexo,direccion,idbarrio, telefono, celular,email, idtipovivienda, iddepresidencia,idciuresidencia,idzona,idestadocivil,fechanacimiento,iddepnace,idciunace,capacidadtrabajo,idprofesion,ruaf,biometria, fechaafiliacion,estado, validado,usuario,fechasistema,nombrecorto) values(:campo2,:campo3,:campo4,:campo5,:campo6,:campo7,:campo8,:campo9,:campo10,:campo11,:campo12,:campo13,:campo14,:campo15,:campo16,:campo17,:campo18,:campo19,:campo20,:campo21,:campo22,:campo23,'N','N',cast(getdate() as date),'N','N',:campo25,cast(getdate() as date),:campo27)";
$statement = $db->conexionID->prepare($sql);
$guardada = false;

$statement->bindParam(":campo2", $campo2, PDO::PARAM_INT);
$statement->bindParam(":campo3", $campo3, PDO::PARAM_STR);
$statement->bindParam(":campo4", $campo4, PDO::PARAM_STR);
$statement->bindParam(":campo5", $campo5, PDO::PARAM_STR);
$statement->bindParam(":campo6", $campo6, PDO::PARAM_STR);
$statement->bindParam(":campo7", $campo7, PDO::PARAM_INT);
$statement->bindParam(":campo8", $campo8, PDO::PARAM_STR);
$statement->bindParam(":campo9", $campo9, PDO::PARAM_STR);
$statement->bindParam(":campo10", $campo10, PDO::PARAM_INT);
$statement->bindParam(":campo11", $campo11, PDO::PARAM_STR);
$statement->bindParam(":campo12", $campo12, PDO::PARAM_STR);
$statement->bindParam(":campo13", $campo13, PDO::PARAM_STR);
$statement->bindParam(":campo14", $campo14, PDO::PARAM_INT);
$statement->bindParam(":campo15", $campo15, PDO::PARAM_STR);
$statement->bindParam(":campo16", $campo16, PDO::PARAM_STR);
$statement->bindParam(":campo17", $campo17, PDO::PARAM_STR);
$statement->bindParam(":campo18", $campo18, PDO::PARAM_INT);
$statement->bindParam(":campo19", $campo19, PDO::PARAM_STR);
$statement->bindParam(":campo20", $campo20, PDO::PARAM_STR);
$statement->bindParam(":campo21", $campo21, PDO::PARAM_STR);
$statement->bindParam(":campo22", $campo22, PDO::PARAM_STR);
$statement->bindParam(":campo23", $campo23, PDO::PARAM_INT);
$statement->bindParam(":campo25", $campo25, PDO::PARAM_STR);
$statement->bindParam(":campo27", $campo27, PDO::PARAM_STR);


$guardada = $statement->execute();
if($guardada){
	// buscar id de la persona creada
	$rs = $db->conexionID->lastInsertId();
	echo trim($rs);
}else{
	// errores
	//$error = $statement->errorInfo();
	//echo "Error codigo {$error[0]} con el mensaje >>> {$error[2]}";
	echo 0;
}
die();
?>