<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
include_once $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'config.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';

include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'utilitario.class.php';

$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

$arrAtributoValor = isset($_REQUEST["objDatosFiltro"])?$_REQUEST["objDatosFiltro"]:array();

$attrEntidad = array(
		array("nombre"=>"idusuario","tipo"=>"NUMBER","entidad"=>"a519")
		,array("nombre"=>"usuario","tipo"=>"TEXT","entidad"=>"a519")
		,array("nombre"=>"identificacion","tipo"=>"TEXT","entidad"=>"a519"));
$filtroSql = Utilitario::obtenerFiltro($attrEntidad,$arrAtributoValor);

$sql="SELECT a519.idusuario, a519.usuario, a519.identificacion, a519.nombres, a519.idagencia, a519.fechacreacion, a519.fechainicio, a519.fechaterminacion, a519.clave, a519.estado, a519.cambio, a519.claveanterior, a519.fechacambio, a519.idrol, a519.fechaexpira, a519.inactivaciontemporal, a519.idarea, a519.notas, a519.idproceso, a519.responsable, a519.planta
	FROM aportes519 a519
		LEFT JOIN aportes015 a15 ON a15.identificacion=a519.identificacion
	$filtroSql";

echo json_encode(Utilitario::fetchConsulta($sql,$db));
?>