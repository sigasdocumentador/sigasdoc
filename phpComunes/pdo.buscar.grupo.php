<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$idpersona= $_REQUEST['v0'];
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
$sql="SELECT idrelacion, idtrabajador, idbeneficiario, idparentesco, idconyuge, conviven, aportes021.fechaafiliacion, 
fechaasignacion, giro, aportes021.estado, idtiporelacion, postulacionfonede, embarga,aportes015.capacidadtrabajo, 
aportes015.papellido,aportes015.sapellido, aportes015.pnombre,aportes015.snombre, year(getdate()-aportes015.fechanacimiento -1)-1900 as edad,aportes015.identificacion, p.detalledefinicion, d.codigo, aportes015.fechanacimiento,co.identificacion as ced_con 
FROM aportes021 
INNER JOIN aportes015 ON aportes021.idbeneficiario=aportes015.idpersona
LEFT JOIN aportes091 p ON aportes021.idparentesco=p.iddetalledef 
left join aportes015 co on aportes021.idconyuge=co.idpersona
INNER JOIN aportes091 d ON aportes015.idtipodocumento=d.iddetalledef WHERE idtrabajador=$idpersona AND idparentesco <> 34";
$rs=$db->querySimple($sql);
$con=0;
$filas = array();
while($row = $rs->fetch()){
	$filas[] = array_map("utf8_encode",$row);
	$con++;
}
if($con==0){
	echo 0;
}else{
	echo  json_encode($filas);
}
?>