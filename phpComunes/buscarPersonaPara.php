<?php
/* autor:       orlando puentes
 * fecha:       10/07/2010
 * objetivo:    
 */
ini_set("display_errors","1");
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$campo0=$_REQUEST['v0'];//cedula 
$campo1=$_REQUEST['v1'];//buscar por
$campo2=$_REQUEST['v2'];//tipo doc
$campo3=$_REQUEST['v3'];//pnombre
$campo4=$_REQUEST['v4'];//papellido

include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'p.persona.class.php';
$objClase=new Persona;
$consulta = $objClase->mostrar_registro_para($campo0,$campo1,$campo2,$campo3,$campo4);
$conr=0;
$data = array();
while($row=mssql_fetch_array($consulta)){
	$data[]=array_map("utf8_encode",$row);
	$conr++;
}
mssql_free_result($consulta);
if($conr>0){
	echo json_encode($data);
}else {
	// se busca por afiliado inactivo
	$consulta = $objClase->mostrar_registro_inactivos($campo0,$campo1,$campo2,$campo3,$campo4);
	$conr=0;
	$data = array();
	while($row = mssql_fetch_array($consulta)){
		$data[] = array_map("utf8_encode",$row);
		$conr++;
	}
	mssql_free_result($consulta);
	if($conr>0)
		echo json_encode($data);
	else{
		// buscar tercero, no tiene afiliaciones
		$registro = $objClase->obtener_persona_tercero_por_identificacion($campo0);
		if($registro !== false){
			$data[] = array_map("utf8_encode",$registro);
			echo json_encode($data);
		}else
			echo 0;
	}
}
die();
?>