<?php
// NO     T O C A R      OJO

if (!isset( $_SESSION) ) {
	session_start();
}

function microtime_float()
{
    list($useg, $seg) = explode(" ", microtime());
    return ((float)$useg + (float)$seg);
}

function session(){
	$ultima=isset($_SESSION['ultima'])?$_SESSION['ultima']:0;
	$nueva=microtime_float();
	$tiempo=$nueva-$ultima;
	if ($tiempo>10000){
		session_destroy();
		cerrar();
		return false;
	}
	else {
		$_SESSION['ultima']=$nueva;
	}
}

function iniciar(){
	$nueva=microtime_float();
	$_SESSION['ultima']=$nueva;
	}

function cerrar(){
	$serv = isset($_SERVER['SERVER_ADDR'])?$_SERVER['SERVER_ADDR']:$_SERVER['LOCAL_ADDR'];
	?>
    <script language="javascript">
		window.open("http://<?php echo $serv?>/sigas/error.php","_self");
	</script>
    <?php
	}
?>