<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;



include_once $raiz. DIRECTORY_SEPARATOR .'clases' . DIRECTORY_SEPARATOR . 'p.definiciones.class.php';

//Parametros recibidos
$objDatosFiltro = $_REQUEST['objDatosFiltro'];

$objDefiniciones = new Definiciones();

//Consultar la definicion si se envio es codigo_definicion
if(isset($objDatosFiltro["codigo_definicion"]) && !empty($objDatosFiltro["codigo_definicion"])){
	
	//Consultar la definicion
	$dataDefinicion = $objDefiniciones->buscar_definicion(array("codigo"=>$objDatosFiltro["codigo_definicion"]));
	if(count($dataDefinicion)>0)
		$objDatosFiltro["iddefinicion"] = $dataDefinicion[0]["iddefinicion"];
}

$consulta = $objDefiniciones->buscar_detalle_definicion($objDatosFiltro);
echo (count($consulta)>0) ? json_encode($consulta) : 0;

?>