<?php
// @autor:		Luis Rojas A
// @fecha:		Octubre 30/2012
// @formulario:	Actualizar datos certificado - modulo grabaciones

ini_set("display_errors","1");
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
include_once $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'config.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'aportes' . DIRECTORY_SEPARATOR . 'otrosProcesos' . DIRECTORY_SEPARATOR . 'certificados' . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'certificados.class.php';
$idcertificado = $_REQUEST['v0'];
$idtipocertificado = $_REQUEST['v1'];
$pinicial = $_REQUEST['v2'];
$pfinal = $_REQUEST['v3'];
$estado = $_REQUEST['v4'];
$fpresentacion = $_REQUEST['v5'];
$objClase = new Certificados;
$r=$objClase->actualizar_certificado_beneficiario($idcertificado,$idtipocertificado,$pinicial,$pfinal,$estado,$fpresentacion);
if ($r==true){
       echo 1;
}else{
	echo 0;
}
?>