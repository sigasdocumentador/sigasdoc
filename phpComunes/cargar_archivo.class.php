<?php

class CargarArchivo{
    private $directorio = null;
    private $nombreArchivo = null;
    private $extencionArchivo = null;
    private $tipoArchivo = null;
    private $file = null;
    
    private $banderaError = false;
    
    private $arrError = null;
    
    function __construnct(){
        $this->arrError = array("error"=>0,"descripcion"=>null);
    }
    
    //Directorio
    public function getDirectorio(){return $this->directorio;}
    public function setDirectorio($directorio){$this->directorio = $directorio;}
    
    //nombreArchivo
    public function getNombreArchivo(){return $this->nombreArchivo;}
    public function setNombreArchivo($nombreArchivo){$this->nombreArchivo = $nombreArchivo;}
    
    //extencionArchivo
    public function getExtencionArchivo(){return $this->extencionArchivo;}
    public function setExtencionArchivo($extencionArchivo){$this->extencionArchivo = $extencionArchivo;}
    
    //tipoArchivo
    public function getTipoArchivo(){return $this->tipoArchivo;}
    public function setTipoArchivo($tipoArchivo){$this->tipoArchivo = $tipoArchivo;}
    
    //file
    public function getFile(){return $this->file;}
    public function setFile($file){$this->file = $file;}
    
    public function getError(){return $this->arrError;}
    
    public function cargar(){
        $this->verificarCarga();
        
        if($this->banderaError===false){
            $this->verificaError();
            
            if($this->banderaError===false){    
                $this->verificaSize();
                
                if($this->banderaError===false){    
                    $this->verificaType();
                    
                    if($this->banderaError===false){ 
                        //Verificar el directorio
                        $this->subirArchivo();
                    }
                }
            }
        }
        
        return !$this->banderaError;
    }
    
    private function verificarCarga(){
        if($this->file["size"]===0){
            $this->banderaError = true;
            $this->arrError["error"] = 1;
            $this->arrError["descripcion"] = "Falta cargar el archivo";
        }
    }
    
    private function verificaError(){
        if($this->file["error"]>0){
            $this->banderaError = true;
            $this->arrError["error"] = 1;
            $this->arrError["descripcion"] = "Error al cargar el archivo";
        }
    }
    private function verificaSize(){
        //El archivo debe pesar 2MB
        //size del archivo en byte; 1024Kb = 1Mb
        //Obtenemos los Kb
        //$size = $this->file["size"]/1024;
        //if($size>2048){
        //    $this->banderaError = true;
        //    $this->arrError["error"] = 1;
        //    $this->arrError["descripcion"] = "Error el archivo debe pesar menos de 2MB";
        //}
    }
    private function verificaType(){
        //El typo del archivo debe ser el indicado
        if(!in_array($this->file["type"],$this->tipoArchivo)){
            $this->banderaError = true;
            $this->arrError["error"] = 1;
            $this->arrError["descripcion"] = "Error tipo de archivo no es el indicado";
        }
    }
    
    private function subirArchivo(){
        $direccion = $this->directorio."".$this->nombreArchivo;
        
        if( !move_uploaded_file($this->file["tmp_name"], $direccion) ){
            //No fue posible cargar el archivo
            return false;
        }
        return true;
    }
}
?>