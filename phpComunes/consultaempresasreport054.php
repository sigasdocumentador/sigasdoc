<?php
ini_set('display_errors', '1');
ini_set('memory_limit', '-1');
set_time_limit(0);
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
include_once $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'config.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

$indice=$_REQUEST['indice'];
$periodoIni=$_REQUEST['periodoIni'];
$periodoFin=$_REQUEST['periodoFin'];
$fechaPagoIni=$_REQUEST['fechaPagoIni'];
$fechaPagoFin=$_REQUEST['fechaPagoFin'];
$claseAportante=explode('*',$_REQUEST['claseAportante']);
$canvar=count($claseAportante);
/*
$indice=0;
$periodoIni='';
$periodoFin='';
$fechaPagoIni='2015/04/01';
$fechaPagoFin='2015/04/01';
$claseAportante=explode('*',$_REQUEST['claseAportante']);*/

$sqlca=" and ( ";
for($i=0;$i<$canvar;$i++){
	if($i==0){
		$sqlca.="a048.claseaportante IS NULL or a048.claseaportante='".$claseAportante[$i]."'";
	}
	else{
		$sqlca.=" or a048.claseaportante='".$claseAportante[$i]."'";
	}
}
$sqlca.=" )";

if(!empty($periodoIni) || !empty($fechaPagoIni)){
	if($indice!=0){
		switch ($indice){
			case 1: $sqlc="a010.tarifaaporte < 4";break;
			case 2: $sqlc="a010.tarifaaporte = 4";break;
		}
		if(!empty($periodoIni))
		   $sqlc.=" and (a010.periodo>= '".$periodoIni."' and a010.periodo<='".$periodoFin."')";
        else
		   $sqlc.=" and (a010.fechapago>= '".$fechaPagoIni."' and a010.fechapago<='".$fechaPagoFin."')";		   
	}
	else{
		if(!empty($periodoIni))
			$sqlc="(a010.periodo>= '".$periodoIni."' and a010.periodo<='".$periodoFin."')";
		else
			$sqlc="(a010.fechapago>='".$fechaPagoIni."' and a010.fechapago<='".$fechaPagoFin."')";
	}
}


$sqlc.=$sqlca;

$sql="SELECT a010.idplanilla,a048.nit,a048.razonsocial,(SELECT TOP 1 municipio FROM aportes089 WHERE codmunicipio=a048.idciudad  AND coddepartamento=a048.iddepartamento) AS municipio,a048.direccion,a048.telefono,a048.email,a015.identificacion,(a015.pnombre + ' ' + a015.papellido) AS nombreafiliado,
round(CAST(a010.tarifaaporte AS FLOAT),2) AS tarifaaporte,CAST (a010.valoraporte AS int) AS valoraporte,CAST(a010.salariobasico AS INt) AS salariobasico,CAST(a010.ingresobase AS int) AS ingresobase,a010.diascotizados,a010.periodo,
a010.idempresa,a010.idtrabajador,a010.horascotizadas,a010.fechapago,a010.fechasistema,
(SELECT TOP 1 estado FROM aportes016 WHERE idempresa=a010.idempresa) AS estadoafiliacion,
(SELECT detalledefinicion FROM aportes091 WHERE iddefinicion='2' AND iddetalledef=(SELECT TOP 1 tipoafiliacion FROM aportes016 WHERE idempresa=a010.idempresa)) AS tipoafiliacion,
(SELECT detalledefinicion fROM aportes091 WHERE iddefinicion='33' AND iddetalledef=a048.claseaportante) AS claseaportante,
isnull(a010.ingreso,'') AS ingreso,isnull(a010.retiro,'') AS retiro,isnull(a010.var_tra_salario,'') AS var_tra_salario,isnull(a010.vacaciones,'') AS vacaciones,isnull(a010.lic_maternidad,'') AS lic_maternidad,isnull(a010.tipo_cotizante,'') AS tipo_cotizante,isnull(a010.correccion,'') AS correccion,isnull(a010.inc_tem_emfermedad,'') AS inc_tem_emfermedad,isnull(a010.inc_tem_acc_trabajo,'') AS inc_tem_acc_trabajo,isnull(a010.novedad_sln,'') AS novedad_sln
FROM aportes010 a010
inner JOIN aportes015 a015 ON a010.idtrabajador=a015.idpersona
inner JOIN aportes048 a048 ON a010.idempresa=a048.idempresa
WHERE {$sqlc} ORDER BY a048.claseaportante";


$resultr=$db->querySimple($sql);
$datar = array();
while ($row=$resultr->fetch()){
	  //verificar si tiene registrado seguimientos a bonos no procesados
	  $datar[]=$row;
}

$canreg=count($datar);    

$data= new stdClass();
$data->sql=$sql;
$data->canreg=$canreg;
$data->contenido=$datar;

$db=null;
echo json_encode($data);

?>