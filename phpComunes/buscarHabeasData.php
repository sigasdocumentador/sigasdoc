<?php
set_time_limit(0);
ini_set("display_errors",'1');
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
include_once $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'config.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

$consu_sql="";

if(isset($_REQUEST['hd']))
switch ($_REQUEST['hd']){
	case 1: 
		$consu_sql.=" and a015.habeas_data IN ('1')";
		break;
	case 2:
		$consu_sql.=" and a015.habeas_data IN ('2')";
		break;
	default:
		$consu_sql.=" and a015.habeas_data IN ('1','2')";
		break;
}

if(isset($_REQUEST['tipop']))
switch ($_REQUEST['tipop']){
	case 1:
		$consu_sql.=" and (select count(*) from aportes016 where idpersona=a015.idpersona) > 0";
		break;
	case 2:
		$consu_sql.=" and ((select count(*) from aportes016 where idpersona=a015.idpersona)=0 and (select count(*) from aportes017 where idpersona=a015.idpersona)>0)";
		break;
	case 3:
		$consu_sql.=" and ((select count(*) from aportes016 where idpersona=a015.idpersona)=0 and (select count(*) from aportes017 where idpersona=a015.idpersona)= 0)";
		break;
	default:
		break;
}

if(isset($_REQUEST['agencia']))
switch ($_REQUEST['agencia']){
	case 1:
		$consu_sql.=" and a519.idagencia in (1)";
		break;
	case 2:
		$consu_sql.=" and a519.idagencia in (2)";
		break;
	case 3:
		$consu_sql.=" and a519.idagencia in (3)";
		break;
	case 4:
		$consu_sql.=" and a519.idagencia in (4)";
		break;
    default:
    	break;		
	
}

if(!empty($_REQUEST['fechaIni']) and !empty($_REQUEST['fechaFin'])){
	$consu_sql.=" and (a015.fechahd>='".$_REQUEST['fechaIni']."' and a015.fechahd<='".$_REQUEST['fechaFin']."') ";
}

$consu_sql=substr($consu_sql, 4);


$result = array("error"=>0,"canreg"=>0,"descripcion"=>"","data"=>"");


$sql="SELECT 
        a015.identificacion,
        ltrim(a015.pnombre) + ' ' + ltrim(a015.snombre) + ' ' + ltrim(a015.papellido) + ' ' + ltrim(a015.sapellido) AS nombre,  
        (SELECT TOP 1 departmento FROM aportes089 WHERE codmunicipio=a015.idciuresidencia) AS departamento,
        (SELECT TOP 1 municipio FROM aportes089 WHERE codmunicipio=a015.idciuresidencia) AS municipio,
        isnull(a015.direccion,'') as direccion,
        isnull(a015.telefono,'') as telefono,
        isnull(a015.celular,'') as celular,
        isnull(a015.email,'') as email,
        'habeas_data'=CASE 
                          WHEN a015.habeas_data = 1 THEN 'Si'
                          WHEN a015.habeas_data = 2 THEN 'No'
                          ELSE ''
                       END,
        a015.fechahd,
        a015.usuariohd,
        a500.agencia,
        'tipopersona' =CASE 
                              WHEN (SELECT count(*) FROM aportes016 WHERE idpersona=a015.idpersona) >= 1 THEN 'Afiliado'
                              WHEN (SELECT count(*) FROM aportes017 WHERE idpersona=a015.idpersona) >= 1 THEN 'Desafiliado'
                              ELSE 'tercero'
                        END      
     FROM aportes015 a015
     LEFT JOIN aportes519 a519 ON a015.usuariohd= a519.usuario
     LEFT JOIN aportes500 a500 ON a519.idagencia=a500.idagencia 
     WHERE $consu_sql 
     ORDER BY a015.idpersona";

$res = $db->querySimple($sql);
$data= array();
while($row=$res->fetch()){
	$data[]=$row;
}

$result["data"]=$data;
$result["canreg"]=count($data);

echo json_encode($result);

?>