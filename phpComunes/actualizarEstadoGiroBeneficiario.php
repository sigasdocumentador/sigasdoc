<?php
/*
* @autor:      Ing. Orlando Puentes
* @fecha:      ago/10/2011
* objetivo:
*/
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
include_once $_SESSION['RAIZ'] . DIRECTORY_SEPARATOR . 'pdo/controldocumentos.class.php';

$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
$c0=$_REQUEST['v0'];
$c1=$_REQUEST['v1'];
$c2=(empty($_REQUEST['v2'])) ? 'NULL' : "'".$_REQUEST['v2']."'";	//fecha asignacion
$observacion=(empty($_REQUEST['ctrobservacion'])) ? NULL :$_REQUEST['ctrobservacion'];	//Observacion



/*
 * inici control documentos
 * extraemos los documentos asociados al beneficiario con su indicador si exit
 */

$ctrdocumento = new controDocumentos($c0);
//obtenemos el control de documentos con su indicador si existen
$ctrcadena = $ctrdocumento->controlcadenadocumentos();
//obtenemos el estado actual del giro del beneficiario
$giroAnt=$ctrdocumento->consultardatosaportes21();

$campos->observacion=$observacion;

/*
 * fin control documentos
 */


// PENDIENTE COLUMNA fechaestadoCambioGiro

$sql="Update aportes021 set giro='$c1', fechaasignacion=$c2, fechagiro=cast(getDate() as DATE), usuario='".$_SESSION["USUARIO"]."' where idrelacion=$c0";
$rs=$db->queryActualiza($sql);

/*
 * Se actualiza el ultimo registro del beneficiario con la observacion digitada por el funcionario
 */

if($giroAnt['giro']!=$c1){
	$ctrdocumento->registrarSeguimiento($campos,$ctrcadena,$c1,2);
}else if($giroAnt['giro']==$c1){
	$ctrdocumento->registrarSeguimiento($campos,$ctrcadena,$c1,1);
}

/*
 * fin actualizar seguimiento
 */
 


echo $rs;






?>