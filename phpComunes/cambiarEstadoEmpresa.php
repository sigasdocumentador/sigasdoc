<?php 
/*
* @autor: 	
* @fecha:      20/11/2012
* objetivo: 	Cambiar estado de una empresa
*/

$idEmpresa = $_REQUEST["idEmpresa"];
$estado = $_REQUEST["estado"];
$usuario = (empty($_REQUEST["usuario"])) ? 'NULL' : $_REQUEST["usuario"];
$opcion = (empty($_REQUEST["opcion"])) ? 'NULL' : $_REQUEST["opcion"];

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
include_once $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'config.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
include_once $raiz.DIRECTORY_SEPARATOR.'aportes'.DIRECTORY_SEPARATOR.'empresas'.DIRECTORY_SEPARATOR .'clases'.DIRECTORY_SEPARATOR .'empresas.class.php';

$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
$objEmpresa = new Empresa();
$consulta = $objEmpresa->buscar_empresa_por_idempresa($idEmpresa);
$empresa = mssql_fetch_array($consulta);
// comfirmar el cambio de estado

if($empresa['estado']!=$estado){
	if($opcion=='1'){		//Si viene de liquidacion de contrato
		$sql="UPDATE aportes048 SET contratista='S', legalizada='N', estado='$estado', usuario='$usuario', fechaestado=CAST(getDate() AS DATE) WHERE idempresa=$idEmpresa";
	} else {
		$sql="UPDATE aportes048 SET estado='$estado', usuario='$usuario', fechaestado=CAST(getDate() AS DATE) WHERE idempresa=$idEmpresa";
	}
} else {
	echo 0;
	exit();
}

$rs=$db->queryActualiza($sql,'aportes048');
$error=$db->error;
if(is_array($error)){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
echo $rs;
?>