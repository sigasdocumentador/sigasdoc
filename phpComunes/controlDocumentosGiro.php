<?php
  set_time_limit(0);
  ini_set("display_errors",'1');
  date_default_timezone_set("America/Bogota");

  $root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
  include_once  $root;
  include_once $_SESSION['RAIZ'] . DIRECTORY_SEPARATOR . '/rsc/pdo/IFXDbManejador.php';
  
  include_once $_SESSION['RAIZ'] . DIRECTORY_SEPARATOR . 'pdo/controldocumentos.class.php';
  
  $datares= array("error"=>0,"data"=>"","descripcionerror"=>"");
  
  $campos=json_decode($_REQUEST['v1']);
  
  $ctrdocumento = new controDocumentos($campos->idr);
    
  if($campos->even=="consulta"){
  	
  	$datares["data"] = $ctrdocumento->valorDocumentos();
  	$datares["error"] = $ctrdocumento->errordocu;
  	$datares["descripcionerror"] = $ctrdocumento->descriperror;
  	
  }else if($campos->even=="actualizar"){
  	
  	$ctrdocumento->inicioTransaccion();
  	
  	$datares["error"] = $ctrdocumento->actualizarControlDocument($campos);
  	$datares["data"]= $ctrdocumento->revisardatosobligatorios();
  	
  	/* Verificamos que tenga asociado documentos, haya algun campo obligatorio no marcado y no hayan errores de momento para hacer la actualizacion*/
  	if($datares["data"]['oblinomarcados']>0 and $datares["error"]==0){
  		 $ctrdocumento->actualizarBanderaGiro($campos,'N');
  		 $datares["error"] = $ctrdocumento->errordocu;
  		 $datares["descripcionerror"] = $ctrdocumento->descriperror;
  	}
  	$datares["error"]==0?$ctrdocumento->confirmarTransaccion():$ctrdocumento->cancelarTransaccion();
  	
  }
 
  
  
  echo json_encode($datares);
  
  
  
  
  
?>