<?php

/* autor:       orlando puentes
 * fecha:       05/10/2010
 * objetivo:    
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>Documento sin t&iacute;tulo</title>
<link href="../../css/Estilos.css" rel="stylesheet" type="text/css" />
<link href="../../css/marco.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../../js/jquery-1.4.2.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.core.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.tabs.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.resizable.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.draggable.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.dialog.js"></script>
<script type="text/javascript" src="../../js/comunes.js"></script>
<script language="javascript" src="js/subsiPlanos.js"></script>

<script type="text/javascript">
shortcut.add("Shift+F",function() {
		                                var URL=src();
		var url=URL+"aportes/trabajadores/consultaTrabajador.php";
    	window.open(url,"_blank");
    },{
	'propagate' : true,
	'target' : document 
        });        
</script>

<style type="text/css">
.download{background:url(../../imagenes/descargar.png) no-repeat left center; padding:8px 8px 8px 25px;}
#tooltip{width:270px; margin:5px; text-align:center; padding:8px; position:absolute; left:43%;first:100px; display:none}
</style>
<script>
$(document).ready(function(){
	$("#ayuda").dialog({
		autoOpen: false,
		height: 450,
		width: 700,
		draggable:true,
		modal:false,
		open: function(evt, ui){
				$('#ayuda').html('');
				$.get('../../help/tarjetas/Ayudacarguevaloresdelsubsidio.html',function(data){
						$('#ayuda').html(data);
				})
		 }
	});
	
	$("#dialog-form2").dialog({
		autoOpen: false,
		height: 400,
		width: 500,
		modal: true,
		buttons: {
			'Enviar': function() {
			var bValid = true;
			var campo=$('#notas').val();
			var campo0=$.trim(campo);
			if (campo0==""){
				$(this).dialog('close');
					return false;
				}
			var campo1=$('#usuario').val();
			var campo2="cargarValoresSubsidio.php";
			$.post(URL+'phpComunes/colaboracion.php',{v0:campo0,v1:campo1,v2:campo2},
			function(datos){
				if(datos=='1'){
					alert("Su comentario fue enviado correctamente!. Gracias por participar en el mejoramiento de SIGAS");
				}else{
					alert(datos);
				}
		});
		$(this).dialog('close');
		},
		Cancelar: function() {
			$(this).dialog('close');
			$("#dialog-form2").dialog("destroy");
		}
	}
	});
	
});
function mostrarAyuda(){
	$("#ayuda").dialog('open' );
}

function notas(){
	$("#dialog-form2").dialog('open');
}	
</script>
</head>
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr> 
		 <td width="13" height="29" class="arriba_iz">&nbsp;</td>
		<td class="arriba_ce"><span class="letrablanca">::Procesar Archivo En Especie::::</span></td>
		<td width="13" class="arriba_de" align="right">&nbsp;</td>
	</tr>
	<tr> 
		<td class="cuerpo_iz">&nbsp;</td>
		<td class="cuerpo_ce">
			<img height="1" width="1" src="../../imagenes/spacer.gif"> 
			<img height="1" width="1" src="../../imagenes/spacer.gif"/> 
			<img src="../../imagenes/menu/informacion.png" width="16" style="border:none; cursor:pointer" title="Manual" onClick="mostrarAyuda();" /> 
			<img src="../../imagenes/menu/notas.png" width="16" height="16" style="cursor: pointer" title="Colaborac&oacute;on en l&oacute;nea" onClick="notas();" /> 
		</td>
		<td class="cuerpo_de">&nbsp;</td>
	</tr>
	<tr> 
		<td class="cuerpo_iz">&nbsp;</td>
		<td> 
			<table>
				<tr> 
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				
				<tr> 
					<td>&nbsp;</td>
					<td align="center" class="big" width="100%">PROCESA LA INFORMACI&Oacute;N DEL SUBSIDIO DE ESPECIE CON DESTINO A MINPROTECCION SOCIAL</td>
					<td>&nbsp;</td>
				</tr>
				<tr> 
					<td>&nbsp;</td>
					<td align="center" class="normal"><p>&nbsp;</p>
				    <p>
				      Valor Subsidio:<input type="text" name="valor" id="valor" />
				    </p></td>
					<td>&nbsp;</td>
				</tr>
				<tr> 
					<td>&nbsp;</td>
					<td align="center"> <div id="boton"> 
					<label onclick="procesarPlanoSubEsp(2);" style="cursor:pointer" class="RojoGrande">Procesar</label>
					</div>
					<div id="mensaje" align="center"> </div>
					</td>
					<td>&nbsp;</td>
				</tr>
				<tr> 
					<td>&nbsp;</td>
					<td align="center">&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr> 
					<td>&nbsp;</td>
					<td align="center"> 
					<div id="mensaje" align="center" style="margin:10px; width:150px; padding:10px; display:none " class="ui-corner-all ui-state-default"> 
Informacin procesada satisfactoriamente.<!--<a href="#" class="download" onclick="return click(event);">Descargar Archivo</a>--> </div></td>
				</tr>
				<tr>
				  <td>&nbsp;</td>
				  <td align="center">
                  <!--<a href="http://10.10.1.121/sigas/phpComunes/descargarArchivo.php?archivo=CNCCF3220101209&componente=ruafModi" class="download">descargar</a>-->
                  </td>
			  </tr>
			</table>
	  </td>
		 <td class="cuerpo_de">&nbsp;</td>
	</tr>
	<tr>
    <td class="abajo_iz" >&nbsp;</td>
    <td class="abajo_ce" ></td>
    <td class="abajo_de" >&nbsp;</td>
  </tr>
</table>

 <div id="tooltip" class="ui-state-highlight ui-corner-all">
    <small>D&eacute; click derecho y a continuaci&oacute;n eliga la opcion <b>Guardar enlace como..</b></small>
    

</div>
<!--colaboracion en linea-->
<div id="dialogo-archivo" title="Archivo banco">
<div id="progreso" style="display: none; font-size: 15pt; font-weight: bold;">Procesado(s) <span id="pg">0</span> de <span id="tt"></span> archivo(s)</div>
<div id="log"></div>
</div>
<div id="dialog-form2" title="Colaboraci&oacute;n en l&iacute;nea" style="display:none">
<p>Por favor diligencie este formulario para enviar comentarios, errores o falencias encontradas en el proceso. M&aacute;ximo 250 caracteres </p>
<label>Tus comentarios:</label><br />
<textarea name="notas" id="notas" cols="60", rows="10"></textarea>
</div>
<!--fin de colaboracion en linea-->
<!-- Manual Ayuda -->
	<div id="ayuda" title="Manual Cargar Valores Subsidio" style="background-image:url(../../imagenes/FondoGeneral0.png)"> </div>
	  
</body>

</html>