<?php
/* autor:       felipe rodríguez
 * fecha:       28/12/2010
 * objetivo:    
 */
ini_set("display_errors",'1');
set_time_limit(0);
date_default_timezone_set('America/Bogota');
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$usuario=$_SESSION['USUARIO'];
$op=2;
global $archivoplano;
include_once 'clases' . DIRECTORY_SEPARATOR . 'ruaf1.class.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';
$objRuaf = new Ruaf;

$mes=$_GET['v1'];

$tipoarchivo=22;

$consulcontrol=$objRuaf->seleccionar_control_subsi($tipoarchivo);
$veces=0;
while($row=mssql_fetch_array($consulcontrol))
	 {
	  $veces++;
	 }

if ($veces>0)
   {
    //existe al menos uno
    if ($mes==0)
       {
	    exit();
       }
    //armar plano
    if(@chdir('./RUAF/subsidios/'))
      { 
       //echo 'esta!'; 
      }else
          { 
           //echo 'no esta!';
           mkdir('./RUAF/subsidios/'); 
          }   
    $fechaHoy=date("m/d/Y");
    $f=date("Y");
    $fecha=date("m");
    $fec=date("d");
    chdir ($raiz2."/planos_generados/RUAF/subsidios/");

    $archivo='ArchivoDes';

    $handle=fopen($archivo, "w");
    fclose($handle);

    $archivotemp='veztemp';
    $handle1=fopen($archivotemp, "w");
    fclose($handle1);
    $handle1=fopen($archivotemp, "a");
    $vez="1";
    fwrite($handle1, $vez);
    fclose($handle1);

    $acumula=0;
    $con=0;
    $consesubsi=$objRuaf->conse_subsidio();
    $rows=mssql_fetch_array($consesubsi);

    $consulta=$objRuaf->procesar_subsi_des_mes();
    if ($consulta==false)
       {
        echo 0;
 	   }
    //detalle
    $consecu=0;
    //DESEMPLEO
    while ($row=mssql_fetch_array($consulta))
          {
           $consecu=$consecu+$rows['consesubsidio'] + 1;

   	       if ($row['aficaja'] == 'S')
		      {
		 	   $tipoper=1;
		      }
		   else
		      {
			   $tipoper=2;
		      }
		   $tiposubsi=2;
		   if ($row['detalledefinicion'] == 'OTORGADO') 
		      {
		 	   $estadosubsi=1;
			  }
		   elseif ($row['detalledefinicion'] == 'SUSPENDIDO') 
		          {
			       $estadosubsi=2;
			      }
		   elseif ($row['detalledefinicion'] == 'PAGADO')
		          {
				   $estadosubsi=3;
				  }
	       elseif ($row['detalledefinicion'] == 'TERMINADO') 
	              {
				   $estadosubsi=4;
				  }
    $dpto=41;
    $ciudad="001";

    $ape1=trim($row['papellido']);
    $ape2=trim($row['sapellido']);
    $nom1=trim($row['pnombre']);
    $nom2=trim($row['snombre']);

    $consultaid=$objRuaf->actualizar_subsi_209($row['idsubsidioperiodo']);
    mssql_free_result($consultaid);
					
    $fechaestado1= explode("-",$row['fechaestado']);
    $fece1=$fechaestado1[2].'-'.$fechaestado1[0].'-'.$fechaestado1[1];
    
    $fechagiro1= explode("-",$row['fechagiro']);
    $fecg1=$fechagiro1[2].'-'.$fechagiro1[0].'-'.$fechagiro1[1];
    
    $fechanacimiento1= explode("-",$row['fechanacimiento']);
    $fecn1=$fechanacimiento1[2].'-'.$fechanacimiento1[0].'-'.$fechanacimiento1[1];
	
    $cadena="2,".$consecu.","."CCF32,".$tipoper.",".$row['tipoid'].",".$row['identificacion'].",".$ape1.",".$ape2.",".$nom1.",".$nom2.",".$fece1.",".intval($row['valorgiradoperiodo']).",".$tiposubsi.",".$estadosubsi.",".$dpto.",".$ciudad.",".$fecg1.",".$row['tipoid'].",".$row['identificacion'].",".$row['sexo'].",".$fecn1.",".$ape1.",".$ape2.",".$nom1.",".$nom2.",".",".",".","."\r\n";

    $handle=fopen($archivo, "a");
    fwrite($handle, $cadena);
    fclose($handle);
    $con++;
   }
   mssql_free_result($consulta);
  }
else
   {
	//primera vez
	if ($mes<>0) 
	   {
		//al ser la primera vez, no se debe seleccionar mes
		exit;
	   }
    //armar plano
    if(@chdir($raiz2."/planos_generados/RUAF/subsidios/"))
      { 
       //echo 'esta!'; 
      }else
          { 
           //echo 'no esta!'; 
           mkdir($raiz2."/planos_generados/RUAF/subsidios/");
          }                 
    $fechaHoy=date("m/d/Y");
    $f=date("Y");
    $fecha=date("m");
    $fec=date("d");    

    $archivo=$raiz2."/planos_generados/RUAF/subsidios/ArchivoDes";
    $handle=fopen($archivo, "w");
   fclose($handle);

   $archivotemp='veztemp';
   $handle1=fopen($archivotemp, "w");
   fclose($handle1);
   $handle1=fopen($archivotemp, "a");
   $vez="0";
   fwrite($handle1, $vez);
   fclose($handle1);

   $acumula=0;
   $con=0;

   //consultamos la tabla 405 para saber si tiene datos temporales
   $consulta405=$objRuaf->consultar_aportes405_des();
   //echo "valido si hay registros en la 405";
   $fil=mssql_fetch_array($consulta405);
   $x=$fil['cuenta'];
   if ($x==0)
      {
       //echo "no hay registros en la 405";
       //////////////////////////////////////////////////////////////
       $consesubsi=$objRuaf->conse_subsidio();
       $rows=mssql_fetch_array($consesubsi);
       $consecu=$rows['consesubsidio'];
       mssql_free_result($consesubsi);

       $consulta=$objRuaf->procesar_subsi_des();
       if ($consulta==false)
          {
           echo 0;
          }
       //detalle
       $consecu=0;
       //DESEMPLEO
       $cr=0;
       while ($row=mssql_fetch_array($consulta))
             {
	          $consecu+= 1;
	          if ($row['aficaja'] == 'S')
	             {
		          $tipoper=1;
	             }
	          else
	             {
	              $tipoper=2;
	             }
	          $tiposubsi=2;
	          if ($row['detalledefinicion'] == 'OTORGADO') 
	             {
		          $estadosubsi=1;
	             }
	          elseif ($row['detalledefinicion'] == 'SUSPENDIDO') 
	                 {
			          $estadosubsi=2;
			         }
			  elseif ($row['detalledefinicion'] == 'PAGADO') 
			         {
					  $estadosubsi=3;
					 }
			  elseif ($row['detalledefinicion'] == 'TERMINADO')
			         {
					  $estadosubsi=4;
					 }
			  $dpto=41;
	          $ciudad="001";
			  $ape1=trim($row['papellido']);
			  $ape2=trim($row['sapellido']);
			  $nom1=trim($row['pnombre']);
			  $nom2=trim($row['snombre']);
			  $consultaid=$objRuaf->actualizar_subsi_209($row['idsubsidioperiodo']);
	
			  mssql_free_result($consultaid);
	
			  $fechaestado1= explode("-",$row['fechaestado']);
			  $fece1=$fechaestado1[2].'-'.$fechaestado1[0].'-'.$fechaestado1[1];
	
			  $fechagiro1= explode("-",$row['fechagiro']);
			  $fecg1=$fechagiro1[2].'-'.$fechagiro1[0].'-'.$fechagiro1[1];
	
			  $fechanacimiento1= explode("-",$row['fechanacimiento']);
			  $fecn1=$fechanacimiento1[2].'-'.$fechanacimiento1[0].'-'.$fechanacimiento1[1];
	
			  $cadena="2,".$consecu.","."CCF32,".$tipoper.",".$row['tipoid'].",".$row['identificacion'].",".$ape1.",".$ape2.",".$nom1.",".$nom2.",".$fece1.",".intval($row['valorgiradoperiodo']).",".$tiposubsi.",".$estadosubsi.",".$dpto.",".$ciudad.",".$fecg1.",".$row['tipoid'].",".$row['identificacion'].",".$row['sexo'].",".$fecn1.",".$ape1.",".$ape2.",".$nom1.",".$nom2.",".",".",".","."\r\n";
	
			  $handle=fopen($archivo, "a");
			  fwrite($handle, $cadena);
			  fclose($handle);
			  $con++;
		      //agregamos los registros a la 405
			  $valores = array();
			  $valores[0]=$consecu;
			  $valores[1]=2;
			  $valores[2]='CCF32';
			  $valores[3]=$row['tipoid'];
			  $valores[4]=$row['identificacion'];
			  $valores[5]=$ape1;
			  $valores[6]=$ape2;
			  $valores[7]=$nom1;
			  $valores[8]=$nom2;
			  $valores[9]=$row['fechaestado'];
			  $valores[10]=intval($row['valorgiradoperiodo']);
			  $valores[11]=$tiposubsi;
			  $valores[12]=$estadosubsi;
			  $valores[13]=$dpto;
			  $valores[14]=$ciudad;
			  $valores[15]=$row['fechagiro'];
			  $valores[16]=$row['tipoid'];
			  $valores[17]=$row['identificacion'];	
			  $valores[18]=$row['sexo'];
			  $valores[19]=$row['fechanacimiento'];
			  $valores[20]=$ape1;
			  $valores[21]=$ape2;
			  $valores[22]=$nom1;
			  $valores[23]=$nom2;
			  $valores[24]=$tipoper;
			  $valores[25]=$row['idsubsidioperiodo'];
			  
			  $guarda405=$objRuaf->guardar405($valores);
			  mssql_free_result($guarda405);
	 		 }
	   mssql_free_result($consulta);
	   $consesubsidio=$objRuaf->actualizar_consesubsi($consecu);
	   mssql_free_result($consesubsidio);	
	   //////////////////////////////////////////////////////////////
      }
   else
	  {
	   //////////////////////////////////////////////////////////////
	   //detalle
	   $consulta405=$objRuaf->consultar_aportes405();
	   //DESEMPLEO
	   while ($row=mssql_fetch_array($consulta405))
	         {
			  $ape1=trim($row['papellidoafiliado']);
			  $ape2=trim($row['sapellidoafiliado']);
			  $nom1=trim($row['pnombreafiliado']);
			  $nom2=trim($row['snombreafiliado']);
			  $dpto=41;
			  $ciudad="001";
			  $tiposubsi=2;
			  $estadosubsi=$row['estadosubsidio'];

			  $fechaestado= explode("-",$row['fechaestado']);
			  $fece=$fechaestado[2].'-'.$fechaestado[0].'-'.$fechaestado[1];

			  $fechagiro= explode("-",$row['fechagiro']);
			  $fecg=$fechagiro[2].'-'.$fechagiro[0].'-'.$fechagiro[1];

			  $fechanacimiento= explode("-",$row['fechanacimiento']);
			  $fecn=$fechanacimiento[2].'-'.$fechanacimiento[0].'-'.$fechanacimiento[1];
			
			  $cadena="2,".intval($row['consesubsido']).","."CCF32,".$row['tipoper'].",".$row['tipoidafiliado'].",".$row['numidafiliado'].",".$ape1.",".$ape2.",".$nom1.",".$nom2.",".$fece.",".intval($row['valorgirado']).",".$tiposubsi.",".$estadosubsi.",".$dpto.",".$ciudad.",".$fecg.",".$row['tipoidbeneficiario'].",".$row['numidbeneficiario'].",".$row['sexo'].",".$fecn.",".$ape1.",".$ape2.",".$nom1.",".$nom2.",".",".",".","."\r\n";

			  $handle=fopen($archivo, "a");
			  fwrite($handle, $cadena);
			  fclose($handle);
			  $con++;
			 }
	   mssql_free_result($consulta405);
	   //////////////////////////////////////////////////////////////
      }
}
echo "Información procesada satisfactoriamente.";
?>

