<?php
/* autor:       felipe rodr�guez
 * fecha:       20/01/2011
 * objetivo:    
 */
 
function sanear_string($string)
{

    $string = trim($string);

    $string = str_replace(
        array('�', '�', '�', '�', '�', '�', '�', '�', '�'),
        array('A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A'),
        $string
    );

    $string = str_replace(
        array('�', '�', '�', '�', '�', '�', '�', '�'),
        array('E', 'E', 'E', 'E', 'E', 'E', 'E', 'E'),
        $string
    );

    $string = str_replace(
        array('�', '�', '�', '�', '�', '�', '�', '�'),
        array('I', 'I', 'I', 'I', 'I', 'I', 'I', 'I'),
        $string
    );

    $string = str_replace(
        array('�', '�', '�', '�', '�', '�', '�', '�'),
        array('O', 'O', 'O', 'O', 'O', 'O', 'O', 'O'),
        $string
    );

    $string = str_replace(
        array('�', '�', '�', '�', '�', '�', '�', '�'),
        array('U', 'U', 'U', 'U', 'U', 'U', 'U', 'U'),
        $string
    );
    
    $string = str_replace(
        array('�', '�', '�', '�'),
        array('�', '�', 'C', 'C'),
        $string
    );
	
	
	$string = str_replace(
        array('ñ', '�ñ', '�', 'Ã','Ñ','�?','Ã?','�?�?','?','��','у�','�A�','ё','�A�','уA��','уA�','���','Ã','т�','Ã±Â','ÃÂ','Ã±','уA�тA�','Â','уA�','тA�','ѱ','�A�','NULL'),
        array('�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','',),
        $string
    );


    //Esta parte se encarga de eliminar cualquier caracter extra�o
    $string = str_replace(
        array("\\", "�", "�", "-", "~",
             "#", "@", "|", "!", "\"",
             "�", "$", "%", "&", "/",
             "(", ")", "?", "'", "�",
             "�", "[", "^", "`", "]",
             "+", "}", "{", "�", "�",
             ">", "< ", ";", ",", ":",
             ".", " "),
        '',
        $string
    );


    return $string;
}

set_time_limit(0);
date_default_timezone_set('America/Bogota'); 
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$usuario=$_SESSION['USUARIO'];
$op=2;
global $archivoplano;
include_once 'clases' . DIRECTORY_SEPARATOR . 'ruaf1.class.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';
$objRuaf = new Ruaf;

if (!file_exists($raiz2."/planos_generados/RUAF/subsidios/")) {

	mkdir($raiz2."/planos_generados/RUAF/subsidios/", 0777, true);
}
 
$consulta_periodo=$objRuaf->consulta_periodo2();
while($row2=mssql_fetch_array($consulta_periodo)){
	$periodoini=$row2['fechainicio'];
	$periodofin=$row2['fechafin'];
}

$fechaHoy=strtotime($periodofin);
$f=date("Y",$fechaHoy);
$fecha=date("m",$fechaHoy);
$fec=date("d",$fechaHoy);

$path_plano =$ruta_generados.'RUAF'.DIRECTORY_SEPARATOR.'subsidios'.DIRECTORY_SEPARATOR;
$archivo="CMSCCF32".$f.$fecha.$fec;

$handle=fopen($path_plano.$archivo, "w");
fclose($handle);

$consulta406=$objRuaf->contar_aportes406_cm();
$fil=mssql_fetch_array($consulta406);
$x = $fil['cuenta'];

if ( $x == 0 ) {
	$consulta2=$objRuaf->llenar406();
	
	$consulta406=$objRuaf->contar_aportes406_cm();
	$fil=mssql_fetch_array($consulta406);
	$x = $fil['cuenta'];
}

$consulta406=$objRuaf->consultar_aportes406_cm();
$cont=0;

$cadena="1,CCF32,".$periodoini.",".$periodofin.",".intval($x).",".$archivo."\r\n";
$handle=fopen($path_plano.$archivo, "a");
fwrite($handle, $cadena);

while ($row=mssql_fetch_array($consulta406)) {	
	$cont++;
	$cadena = "2," . intval($row['consesubsido']) . ",CCF32," . $row['tipoper'] . "," . $row['tipoidafiliado'] . "," . $row['numidafiliado'] . "," . sanear_string(trim($row['papellidoafiliado'])) . "," . sanear_string(trim($row['sapellidoafiliado'])) . "," . sanear_string(trim($row['pnombreafiliado'])) . "," . sanear_string(trim($row['snombreafiliado'])) . "," . $row['fechaestado'] . "," . intval($row['valorgirado']) . "," . $row['tiposubsidio'] . "," . $row['estadosubsidio'] . "," . $row['departamento'] . "," .	$row['ciudad'] . "," . $row['fechagiro'] . "," . $row['tipoidbeneficiario'] . "," . $row['numidbeneficiario'] . "," . $row['sexo'] . "," . $row['fechanacimiento'] . "," . sanear_string(trim($row['papellidobeneficiario'])) . "," . sanear_string(trim($row['sapellidobeneficiario'])) . "," . sanear_string(trim($row['pnombrebeneficiario'])) . "," . sanear_string(trim($row['snombrebeneficiario'])) . ",,,,\r\n";	
	fwrite($handle, $cadena);
}

mssql_free_result($consulta406);
//////////////////////////////////////////////////////////////
fclose($handle);
echo $cont;

$_SESSION['ENLACE']=$path_plano.$archivo;
$_SESSION['ARCHIVO']=$archivo;
?>