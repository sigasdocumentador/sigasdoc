<?php
/* autor:       felipe rodríguez
 * fecha:       20/01/2011
 * objetivo:    
 */
set_time_limit(0);
date_default_timezone_set('America/Bogota'); 
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$usuario=$_SESSION['USUARIO'];
$op=2;
global $archivoplano;
include_once 'clases' . DIRECTORY_SEPARATOR . 'ruaf1.class.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';
$objRuaf = new Ruaf;

//$mes=$_GET['v1'];
//armar plano
$fechaHoy=date("m/d/Y");
$f=date("Y");
$fecha=date("m");
$fec=date("d");

chdir ($raiz2."/planos_generados/RUAF/subsidios/");

$archivo='CMSCCF32'.$f.'08'.'31';
$handle=fopen($archivo, "w");
fclose($handle);

$con=0;

//consultamos la tabla 405 para saber si tiene datos temporales
$consulta406=$objRuaf->consultar_aportes406_cm();
//echo "valido si hay registros en la 405";
$x=0;
while ($fil=mssql_fetch_array($consulta406))
      {
	   $x++;
      }
if ($x==0)
   {
    //echo "no hay registros en la 406";
    //////////////////////////////////////////////////////////////
    $consulta=$objRuaf->procesar_subsi_cm();
    if ($consulta==false)
       {
   		echo 0;
   		exit();
       }
    //detalle
    //CM
       $cont = 0;
    while ($row=mssql_fetch_array($consulta))
          {
		   $tipoper=1;

		   $tiposubsi=1;

		   $dpto=41;
		   $ciudad="001";
		   $estadosubsi=3;

		   $ape1afi=trim($row['papellidoafi']);
		   $ape2afi=trim($row['sapellidoafi']);
		   $nom1afi=trim($row['pnombreafi']);
		   $nom2afi=trim($row['snombreafi']);

		   $ape1ben=trim($row['papellidoben']);
		   $ape2ben=trim($row['sapellidoben']);
		   $nom1ben=trim($row['pnombreben']);
	 	   $nom2ben=trim($row['snombreben']);
			
		   $fechagiro1= explode("-",$row['fechagiro']);
		   
		   $fecg1=$fechagiro1[2].'-'.$fechagiro1[0].'-'.$fechagiro1[1];

		   $fechanacimiento1= explode("-",$row['fechanacimientoben']);
		   $fecn1=$fechanacimiento1[2].'-'.$fechanacimiento1[0].'-'.$fechanacimiento1[1];

		   $fechanacimiento2= explode("-",$row['fechanacimientoafi']);
		   $fecn2=$fechanacimiento2[2].'-'.$fechanacimiento2[0].'-'.$fechanacimiento2[1];

		   $cadena="2,".$row['idcuota'].","."CCF32,".$tipoper.",".$row['tipoidafi'].",".$row['identificacionafi'].",".$ape1afi.",".$ape2afi.",".$nom1afi.",".$nom2afi.",".$fecg1.",".intval($row['valor']).",".$tiposubsi.",".$estadosubsi.",".$dpto.",".$ciudad.",".$fecg1.",".$row['tipoidafi'].",".$row['identificacionafi'].",".$row['sexoafi'].",".$fecn2.",".$ape1afi.",".$ape2afi.",".$nom1afi.",".$nom2afi.",".",".",".","."\r\n";

		   $handle=fopen($archivo, "a");
		   fwrite($handle, $cadena);
		   fclose($handle);

		   //agregamos los registros a la 406
		   $valores = array();

		   $valores[0]=$row['idcuota'];
		   $valores[1]=2;
		   $valores[2]='CCF32';
		   $valores[3]=$row['tipoidafi'];
		   $valores[4]=$row['identificacionafi'];
		   $valores[5]=$ape1afi;
		   $valores[6]=$ape2afi;
		   $valores[7]=$nom1afi;
		   $valores[8]=$nom2afi;
		   $valores[9]=$row['fechagiro'];
		   $valores[10]=intval($row['valor']);
		   $valores[11]=$tiposubsi;
		   $valores[12]=$estadosubsi;
		   $valores[13]=$dpto;
		   $valores[14]=$ciudad;
		   $valores[15]=$row['fechagiro'];
		   $valores[16]=$row['tipoidafi'];
		   $valores[17]=$row['identificacionafi'];	
		   $valores[18]=$row['sexoafi'];
		   $valores[19]=$row['fechanacimientoafi'];
		   $valores[20]=$ape1afi;
		   $valores[21]=$ape2afi;
		   $valores[22]=$nom1afi;
		   $valores[23]=$nom2afi;
		   $valores[24]=$tipoper;

		   $guarda406=$objRuaf->guardar406($valores);
		   mssql_free_result($guarda406);
			
		   $cadena="2,".$row['idcuota'].","."CCF32,".$tipoper.",".$row['tipoidafi'].",".$row['identificacionafi'].",".$ape1afi.",".$ape2afi.",".$nom1afi.",".$nom2afi.",".$fecg1.",".intval($row['valor']).",".$tiposubsi.",".$estadosubsi.",".$dpto.",".$ciudad.",".$fecg1.",".$row['tipoidben'].",".$row['identificacionben'].",".$row['sexoben'].",".$fecn1.",".$ape1ben.",".$ape2ben.",".$nom1ben.",".$nom2ben.",".",".",".","."\r\n";

		   $handle=fopen($archivo, "a");
		   fwrite($handle, $cadena);
		   fclose($handle);
		   $con++;

		   //agregamos los registros a la 406
		   $valores = array();

		   $valores[0]=$row['idcuota'];
		   $valores[1]=2;
		   $valores[2]='CCF32';
		   $valores[3]=$row['tipoidafi'];
		   $valores[4]=$row['identificacionafi'];
		   $valores[5]=$ape1afi;
		   $valores[6]=$ape2afi;
		   $valores[7]=$nom1afi;
		   $valores[8]=$nom2afi;
		   $valores[9]=$row['fechagiro'];
		   $valores[10]=intval($row['valor']);
		   $valores[11]=$tiposubsi;
		   $valores[12]=$estadosubsi;
		   $valores[13]=$dpto;
		   $valores[14]=$ciudad;
		   $valores[15]=$row['fechagiro'];
		   $valores[16]=$row['tipoidben'];
		   $valores[17]=$row['identificacionben'];	
		   $valores[18]=$row['sexoben'];
		   $valores[19]=$row['fechanacimientoben'];
		   $valores[20]=$ape1ben;
		   $valores[21]=$ape2ben;
		   $valores[22]=$nom1ben;
		   $valores[23]=$nom2ben;
		   $valores[24]=$tipoper;

		   $guarda406=$objRuaf->guardar406($valores);
		   mssql_free_result($guarda406);
		   $cont++;
		  }
		  echo $cont;
	mssql_free_result($consulta);
	//////////////////////////////////////////////////////////////
   }
else
   {
	//////////////////////////////////////////////////////////////
	//detalle
	$consulta406=$objRuaf->consultar_aportes406_cm();
	//CM
	while ($row=mssql_fetch_array($consulta406))
	      {
		   $ape1=trim($row['papellidoafiliado']);
		   $ape2=trim($row['sapellidoafiliado']);
		   $nom1=trim($row['pnombreafiliado']);
		   $nom2=trim($row['snombreafiliado']);

		   $ape1ben=trim($row['papellidobeneficiario']);
		   $ape2ben=trim($row['sapellidobeneficiario']);
		   $nom1ben=trim($row['pnombrebeneficiario']);
		   $nom2ben=trim($row['snombrebeneficiario']);

		   $dpto=41;
		   $ciudad="001";
		   $tiposubsi=2;
		   $estadosubsi=$row['estadosubsidio'];

		   $fechaestado= explode("-",$row['fechaestado']);
		   $fece=$fechaestado[2].'-'.$fechaestado[1].'-'.$fechaestado[0];

		   $fechagiro= explode("-",$row['fechagiro']);
		   $fecg=$fechagiro[2].'-'.$fechagiro[1].'-'.$fechagiro[0];

		   $fechanacimiento= explode("-",$row['fechanacimiento']);
		   $fecn=$fechanacimiento[2].'-'.$fechanacimiento[1].'-'.$fechanacimiento[0];
			
		   $cadena="2,".intval($row['consesubsido']).","."CCF32,".$row['tipoper'].",".$row['tipoidafiliado'].",".$row['numidafiliado'].",".$ape1.",".$ape2.",".$nom1.",".$nom2.",".$fece.",".intval($row['valorgirado']).",".$tiposubsi.",".$estadosubsi.",".$dpto.",".$ciudad.",".$fecg.",".$row['tipoidbeneficiario'].",".$row['numidbeneficiario'].",".$row['sexo'].",".$fecn.",".$ape1ben.",".$ape2ben.",".$nom1ben.",".$nom2ben.",".",".",".","."\r\n";

		   $handle=fopen($archivo, "a");
		   fwrite($handle, $cadena);
		   fclose($handle);
		   $con++;
		  }
	mssql_free_result($consulta406);
	//////////////////////////////////////////////////////////////
   }
echo "Información procesada satisfactoriamente.";
?>