<?php

// @autor:		Orlando Puentes A
// @fecha:		mayo 31/2010
// @formulario:	guardar datos persona - modulo empresa

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

?>
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
<head> 
 <title>Procesar archivos</title>
 <link href="../../css/Estilos.css" rel="stylesheet" type="text/css">
 <script language="javascript" src="../../js/jquery-1.4.2.js"></script>
 <script language="javascript" src="js/subirPlanos.js"></script>
 </head>
 <body>
 <br />
 <p align="center" class="Rojo">Resultado de la copia de archivos</p>
 <br />
 <?php 
$directorio= $ruta_generados.DIRECTORY_SEPARATOR.'RUAF'.DIRECTORY_SEPARATOR.'modificaciones'.DIRECTORY_SEPARATOR.'recibidos'.DIRECTORY_SEPARATOR;
//$directorio = "/var/www/html/planos_cargados/RUAF/modificaciones/recibidos/"; 
echo "<br><br><br><br>";
if(isset($_FILES['archivo'])){
	foreach ($_FILES['archivo']['error'] as $key => $error) {
	   if ($error == UPLOAD_ERR_OK) {
		   $archivos[]= $_FILES["archivo"]["name"][$key];
		   move_uploaded_file($_FILES["archivo"]["tmp_name"][$key],$directorio.$_FILES["archivo"]["name"][$key]) 
		   or die("Ocurrio un problema al intentar subir el archivo.");
	   }
	}
 }
 else{
 	echo "<p class=Rojo>No se pudo cargar el archivo!</p>";
 	exit();
 }
 
for($i=0; $i<count($archivos); $i++){
	echo "<p align=center>Archivo copiado en el servidor: <strong>".$archivos[$i]."</strong></p>"; 
}
if($i>0){
?>
<center>
    <div id="boton">    
    <p style="text-decoration:none; font-size:12px; color:#F00; font-weight:bold; cursor:pointer" onclick="procesarper();">Procesar estos archivos</p>
</div>
</center>
<?php
	}
 ?> 
 <div id="ajax" align="center"> </div>
<div id="mensajes" align="center">..:..</div>
 </body>
 </html>
 
