<?php
/* autor:       orlando puentes
 * fecha:       agosto 09 de 2012
 * objetivo:    preparar archivo supervivencia
 */
ini_set("display_errors",'1'); 
set_time_limit(0);
date_default_timezone_set("America/Bogota");
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$usuario=$_SESSION['USUARIO'];

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
include_once $raiz.DIRECTORY_SEPARATOR.'funcionesComunes'.DIRECTORY_SEPARATOR.'funcionesComunes.php';
include_once $raiz.DIRECTORY_SEPARATOR.'config.php';
$anno = date('Y');
$mes = date('m');
$dia = date('d');
$dir=$ruta_generados.'RUAF'.DIRECTORY_SEPARATOR.'supervivencia'.DIRECTORY_SEPARATOR. $anno. DIRECTORY_SEPARATOR . $mes . DIRECTORY_SEPARATOR . $dia . DIRECTORY_SEPARATOR;
if(!verificaDirectorio($dir)){
	exit(0);
}

$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
$c0=$_REQUEST['v0'];
switch ($c0) {
	case 1: $sql="SELECT codigo, identificacion FROM aportes015 INNER JOIN aportes091 ON aportes015.idtipodocumento=aportes091.iddetalledef  WHERE idpersona IN (SELECT idpersona FROM aportes016)"; break;
	case 2: $sql="SELECT codigo, identificacion FROM aportes015 INNER JOIN aportes091 ON aportes015.idtipodocumento=aportes091.iddetalledef  WHERE aportes015.estado='N' and idpersona IN (SELECT distinct idbeneficiario FROM aportes021 WHERE idparentesco IN(35,36,37,38) AND estado='A' and idtrabajador in (SELECT idpersona FROM aportes016))"; break;
	case 3: $sql="SELECT codigo, identificacion FROM aportes015 INNER JOIN aportes091 ON aportes015.idtipodocumento=aportes091.iddetalledef  WHERE aportes015.estado='N' and idpersona IN (SELECT distinct idbeneficiario FROM aportes021 WHERE idparentesco IN(36) AND estado='A' and idtrabajador in (SELECT idpersona FROM aportes016))"; break;
	case 4: $sql="SELECT codigo, identificacion FROM aportes015 INNER JOIN aportes091 ON aportes015.idtipodocumento=aportes091.iddetalledef  WHERE aportes015.estado='N' and idpersona IN (SELECT distinct idbeneficiario FROM aportes021 WHERE idparentesco IN(35,37,38) AND estado='A'  and aportes015.capacidadtrabajo='I'  and idtrabajador in (SELECT idpersona FROM aportes016))"; break;
}

$nomFile="tempo";
$ruta2=$dir.$nomFile;
$handlerRuta2=fopen($ruta2, "w");
//fclose($f);

$nomFile="errore.txt";
$ruta3=$dir.$nomFile;
$handlerRuta3=fopen($ruta3, "w");
//fclose($f);

$rs=$db->querySimple($sql);
$con=0;
while ($row=$rs->fetch()) {
	$td=$row['codigo'];
	$num=trim($row['identificacion']);
	$x=strlen($num);
	$flag='N';
	if($td=='CC' && ( $x==5 || $x==6 || $x==7 || $x==8 || $x==10 )){
		$flag='S';
	}
	if($td=='TI' && ( $x==10 || $x==11) ){
		$flag='S';
	}
	if($td=='CE' && ( $x<7) ){
		$flag='S';
	}
	if(($td=='RC' || $td=='NUIP') && ( $x==8 || $x==10 || $x==11) ){
		//EN CASO DE QUE VENGA CODIGO NUIP SE CONVIERTE A RC
		$td='RC';
		$flag='S';
	}
	
	if($flag=='S'){
		$cadena="2,$td,$num\r\n";
		//$f=fopen($ruta2, "a");
		fwrite($handlerRuta2, $cadena);
		fflush($handlerRuta2);
		//fclose($f);
		$con++;
	}
	else{
		$cadena="$td,$num\r\n";
		fwrite($handlerRuta3, $cadena);
		fflush($handlerRuta3);
		
		/*$f=fopen($ruta3, "a");
		fwrite($f, $cadena);
		fclose($f);*/
	}
	
}
fclose($handlerRuta2);
fclose($handlerRuta3);

$FH=date('Ymd');
$fecha=date("Y-m-d");
//------------Restar un mes a la fecha--------------------------
$fechaIni = strtotime ( '-1 month' , strtotime ( $fecha ) ) ; 
$fechaIni = date ( 'Y-m-d' , $fechaIni );
//---------------------------------------------------------------
$nomFile="RUA250SMCS".$FH."NI000891180008CO0CCF32.txt";
$ruta=$dir.$nomFile;
$f=fopen($ruta, 'w');
$cadena="1,CCF32,$fechaIni,$fecha,$con,$nomFile\r\n";
fwrite($f, $cadena);
fclose($f);

$f1=fopen($ruta2, "r");
$f=fopen($ruta, 'a');
while (!feof($f1)) {
	fwrite($f, fgets($f1, 100));
}
fclose($f);
fclose($f1);

$_SESSION['ENLACE']=$ruta;
$_SESSION['ARCHIVO']=$nomFile;
echo 1;
?>