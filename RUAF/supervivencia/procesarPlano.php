<?php
/* autor:       orlando puentes
 * fecha:       octubre 4 de2010
 * objetivo:    Almacenar los movimientos diarios de las tarjetas.
 */
set_time_limit(0);
date_default_timezone_set("America/Bogota");

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'config.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
$usuario=$_SESSION['USUARIO'];
include_once $raiz . DIRECTORY_SEPARATOR . 'funcionesComunes' . DIRECTORY_SEPARATOR . 'funcionesComunes.php';
$anno=date("Y");
$mes=date("m");
$dia=date("d"); 
$directorio = $ruta_cargados. DIRECTORY_SEPARATOR. 'RUAF' .DIRECTORY_SEPARATOR.'supervivencia'.DIRECTORY_SEPARATOR.$anno.DIRECTORY_SEPARATOR.$mes.DIRECTORY_SEPARATOR.$dia.DIRECTORY_SEPARATOR; 

$tipo=$_REQUEST["v0"];

$dir = opendir($directorio);
while ($elemento = readdir($dir))
{
		if(strlen($elemento)>2)
		$archivos[]=$elemento;
} 
closedir($dir);
$arcProc=$archivos[0];
$mesP=substr($arcProc,13,2);
$diaP=substr($arcProc,15,2);
for($i=0; $i<count($archivos); $i++){
	echo "<br>Procesando archivo: ".$archivos[$i]."<br>";
	$directorio.=$archivos[$i];
	$archivoS = file($directorio); 
	$lineas = count($archivoS);
	foreach ($archivoS as $linea=>$archivoS){ 
		$cad=explode(",", $archivoS);
		if($cad[0]==3 || $cad[0]==2){
			switch ($cad[1]){
				case 'CC' : $td=1;break;
				case 'RC' : $td=6;break;
				case 'TI' : $td=2;break;
				case 'PA' : $td=3;break;
				case 'CE' : $td=4;break;
			}
			$num=$cad[2];
			if(($cad[0]==3 && $cad[11] == "Cancelada por Muerte") || $cad[0]==2){
				//VERIFICACION DE SI YA ESTA REGISTRADA LA DEFUNCION
				$sql ="select idpersona,estado from aportes015 where identificacion='$num'";
				$rs1=$db->querySimple($sql);
				$row1=$rs1->fetch();
				//echo $row2; 
				if($row1['estado']!='M'){

					//echo $sql ="update aportes015 set estado='M',ruaf='".$cad[11]." ".$cad[14]."' where idtipodocumento=$td and identificacion='$num'";
					//ESTRUCTURA TIPO 3 SEGUN RESOLUCION 1537 DE 2015
					if($cad[0]==3){
						$sql1 ="update aportes015 set estado='M',ruaf='S',fechadefuncion='$cad[12]' where identificacion='$num'";
					}else{
						$sql1 ="update aportes015 set estado='M',ruaf='S',fechadefuncion='$cad[18]' where identificacion='$num'";
					}
					//echo $sql1;
					$rs2=$db->queryActualiza($sql1);
					
					if ($tipo==2){
							
						$sql2 ="select idpersona,estado from aportes015 where identificacion='$num'";
						$rs3=$db->querySimple($sql2);
						$row2=$rs3->fetch();
					
						$sql3 ="UPDATE aportes021 SET estado='I', idmotivo=4242, fechaestado=getdate() WHERE idbeneficiario='".$row2['idpersona']."'";
						$rs4=$db->queryActualiza($sql3);
					}
					
					//echo $rs;
					if($cad[0]==3) echo "<br>Procesado: IDENTIFICACION = ".$cad[3]." FECHA DEFUNCION = ".$cad[12];
					else echo "<br>Procesado: IDENTIFICACION = ".$cad[8]." FECHA DEFUNCION = ".$cad[18];
				}else{
					if($cad[0]==3) echo "<br>No Procesado: IDENTIFICACION = ".$cad[3]." MOTIVO = Defuncion ya registrada";
					else echo "<br>No Procesado: IDENTIFICACION = ".$cad[8]." MOTIVO = Defuncion ya registrada";
				}
				
			}
		}
	} 
	
	/* $viejo=$ruta_cargados."asopagos/saldos/cargados/".$archivos[$i];
	$nuevo=$ruta_cargados."asopagos/saldos/procesados/".$archivos[$i];
	if(moverArchivo($viejo,$nuevo)==false){
		echo "<br>No se pudo mover el archivo: $viejo";
	} */
	
}

 ?>
