// JavaScript Document
var modifica=0;
var nuevo=0;
var idrol=0;

function nuevoR(){
	nuevo=1;
	$("#bGuardar").show();
	limpiarCampos();
	$("#txtRol").focus();
	}
	
function validar(){
	$("#bGuardar").hide();
	error=0;
	$(".ui-state-error").removeClass("ui-state-error");//Limpiar la clase de error
	if(nuevo==0){
		alert("Haga click primero en Nuevo");
		return false;
    }
	var idrol=$("#txtIdRol").val();
	var rol=$.trim($("#txtRol").val());
	var estado=$("#txtEstado").val();
	var insert=$("#txtInsert").val();
	var update=$("#txtUpdate").val();
	var restric=$("#txtRestricciones").val();
	if(rol==''){
   		$("#txtRol").addClass("ui-state-error");
   		error++;
   	}
	if(error>0){return false; }
	$.getJSON('rolesGuardar.php',{v0:idrol,v1:rol,v2:estado,v3:insert,v4:update,v5:restric,v99:1},function(datos){
		if(datos==0){
			MENSAJE("Lo lamento, su registro no fue grabado!");
			return false;
			}
			MENSAJE("El registro se guardo!");
			limpiarCampos(); 
		})
}	
	
function actualizar(){
	var error=0;
	var idrol=$("#txtIdRol").val();
	var rol=$.trim($("#txtRol").val());
	var estado=$("#txtEstado").val();
	var insert=$("#txtInsert").val();
	var update=$("#txtUpdate").val();
	var restric=$("#txtRestricciones").val();
	
	if(rol==''){
   		$("#txtRol").addClass("ui-state-error");
   		error++;
   	}
	if(error>0){return false; }
	$.getJSON('rolesGuardar.php',{v0:idrol,v1:rol,v2:estado,v3:insert,v4:update,v5:restric,v99:2},function(datos){
		if(datos==0){
			MENSAJE("Lo lamento, su registro no fue actualizado!");
			return false;
			}
			MENSAJE("El registro se actualizo!");
			limpiarCampos(); 
		})	
}

function consultar(){
	if($.trim($("#txtRol").val())==''){
		MENSAJE("Digite el Rol a consultar!");
		$("#txtRol").focus();
		return false;
		}
		var rol=$("#txtRol").val();
	$.getJSON('rolesConsulta.php',{v0:rol},function(datos){
		if(datos==0){
			MENSAJE("Lo lamento, no encontre el registro!");
			return false;
			}
		$.each(datos,function(i,f){
			$("#txtIdRol").val(f.IDROL);
			$("#txtRol").val(f.ROL);
			$("#txtEstado").val(f.ESTADO);
			$("#txtInsert").val(f.ADICIONA);
			$("#txtUpdate").val(f.MODIFICA);
			$("#txtRestricciones").val(f.ESTRICCION);
			});
		})
	}
	
//Controla las opciones de navegacion de registros
function navegarRegistro(op){
	$.getJSON('rolNavegar.php',{v0:op},function(datos){
		if(datos!=0){
			$.each(datos, function(i,f){
				$("#txtIdRol").val(f.IDROL);
				$("#txtRol").val(f.ROL);
				$("#txtEstado").val(f.ESTADO);
				$("#txtInsert").val(f.ADICIONA);
				$("#txtUpdate").val(f.MODIFICA);
				$("#txtRestricciones").val(f.ESTRICCION);
				return;
			});
		}
	});
}
