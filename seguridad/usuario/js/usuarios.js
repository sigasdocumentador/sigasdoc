/**
 * @author Juan Fernando Tamayo
 * Creada: 13 de Junio de 2011
 */
var modifica=0;
var nuevo=0;
var idusuario=0;
var consulta=0;

$(document).ready(function(){
	//Activo los detapicker en el formulario
	//$("#txtFecTerminacion").datepicker({ showAnim:'slideDown',changeMonth: true,changeYear: true,defaultDate: null,dateFormat: "mm/dd/yy" });
	$("#txtFechaInicio").datepicker({ showAnim:'slideDown',changeMonth: true,changeYear: true,minDate:"+0D",defaultDate: null,dateFormat: "mm/dd/yy" });
	$("#txtFechaDigita").datepicker({ showAnim:'slideDown',changeMonth: true,changeYear: true,defaultDate: null,dateFormat: "mm/dd/yy" });
	$("#txtFechaCambio").datepicker({ showAnim:'slideDown',changeMonth: true,changeYear: true,defaultDate: null,dateFormat: "mm/dd/yy" });
	
	$("#txtFechaInicio").bind("change",function(){
		$("#txtFecTerminacion").datepicker({ showAnim:'slideDown',changeMonth: true,changeYear: true,defaultDate: null,dateFormat: "mm/dd/yy",
											minDate: new Date($("#txtFechaInicio").val())
		});
	});
	
	$("#txtFechaInicio").bind("change",function(){
		$("#txtFecTerminacion").val('');
	});
	
	
	});

//Dialogo de Ayuda
$("#ayuda").dialog({
		 	      autoOpen: false,
			      height: 450,
			      width: 700,
			      modal:false,
			      open: function(evt, ui){
					$('#ayuda').html('');
						$.get(URL +'/help/seguridad/manual_ayuda_usuarios.html',function(data){
							$('#ayuda').html(data);
					});
			      }
		});
//Validar solo numeros en los input
function esNumero(evt){
	var charCode = (evt.which) ? evt.which : evt.keyCode;
	if (charCode > 31 && (charCode < 48 || charCode > 57)){		return false;
	}
	return true;
}

function nuevoR(){
	nuevo=1;
	$("#bGuardar").show();
	limpiarCampos();
	$("#txtFechaDigita").val(fechaCortaHoy());
	$("#txtFechaCambio").val(mas90dias());
	$("#txtUsuario").focus();
	$("#select3").val(1);
	}
	
//Controla las opciones de navegacion de registros
function navegarRegistro(op){
	$.getJSON('usuariosNavegar.php',{v0:op},function(datos){
		if(datos!=0){
			$.each(datos, function(i,f){
				idusuario=f.idusuario;
			$("#txtUsuario").val(f.usuario);
	 		$("#txtIdentificacion").val(f.identificacion);
	 		$("#txtNombre").val(f.nombres);
	 		$("#cbxProceso").val(f.idproceso).trigger('change');
	 		$("#cbxAgencia").val(f.idagencia);
	 		$("#cbxRol").val(f.idrol);
	 		$("#txtFechaInicio").val(f.fechainicio);
	 		$("#txtFecTerminacion").val(f.fechaterminacion);
	 		$("#txtEstado").val(f.estado);
	 		$("#txtFechaDigita").val(f.fechacreacion);
	 		$("#txtFechaCambio").val(f.fechaexpira);
	 		$("#txtNotas").val(f.notas);
			setTimeout((function(){
				$("#cbxArea").val(f.idarea);
			}),500);
			return;
				return;
			});
		}
	});
}

function buscarUsuario(){
	var  user=$.trim($("#txtUsuario").val());
	if(user.length==0){
		return false;
	}
	if(nuevo==1){
		$.getJSON('buscarUsuario.php', {v0:user,v1:1}, function(datos){
			if(datos!=0){
				MENSAJE("Lo lamento, ese nombre de usuario ya exite!"+user);
				$("#txtUsuario").val('');
				return false;
			}
		})
		}
	else{
		//consulta
		}
	}
	
function buscarRegistro(){
	var numero=$.trim($("#txtIdentificacion").val());
	var usuario=$("#txtUsuario").val();
	if(numero.length==0) return false
	if(nuevo==0){
		//consulta
		
		}
	else{
		//nuevo
		$.ajax({
		url: 'buscarUsuario.php',
		type: 'POST',
		async: false,
		dataType: "json",
		data: {v0:numero,v1:2},
		success: function(datos){
			cuenta=0;
			$.each(datos,function(i,f){
				cuenta=f.cuenta;
				});
			if(cuenta != 0){
				MENSAJE("El numero "+numero+" ya esta registrado en el Sistema!, utilice la opcion modificar");
				limpiarCampos();
				return false;
				}
			else{
				$.ajax({ //consulta informa
					url: 'buscarInforma.php',
					type: 'POST',
					async: false,
					dataType: 'json',
					data: {v0:numero},
					success: function(data){
						if(data==0){
							MENSAJE("Lo lamento, pero ese numero NO existe en el maestro de empleados!");
							//habilitar textos fechainicio y fechafin
							}else{
								$.each(data,function(i,fila){
									$("#txtNombre").val($.trim(fila.nombre));
									$("#txtFechaInicio").val(fila.f_ingreso);
									$("#txtFecTerminacion").val(fila.f_vence);
									$("#txtEstado").val(fila.estado);
									return;
									});
								if($("#txtEstado").val()=="R"){
										MENSAJE("El trabajador figura como RETIRADO!");
										//limpiarCampos();
										//nuevo=0;
								}
								}
						}// fin succes informa
					});//fin informa
				}//fin buscar en informa web
		}
		});
	}//fin else
	}

function validar(){
	$("#bGuardar").hide();
	error=0;
	$(".ui-state-error").removeClass("ui-state-error");//Limpiar la clase de error
	  	  
	if(nuevo==0){
		alert("Haga click primero en Nuevo");
		return false;
    }
	var usuario=$("#txtUsuario").val();
	var identificacion=$("#txtIdentificacion").val();
	var nombre=$("#txtNombre").val();
	var proceso=$("#cbxProceso").val();
	var area=$("#cbxArea").val()
	var agencia=$("#cbxAgencia").val();
	var rol=$("#cbxRol").val();
	var finicio=$("#txtFechaInicio").val();
	var ffin=$("#txtFecTerminacion").val();
	var estado=$("#txtEstado").val();
	var fdigita=$("#txtFechaDigita").val();
	var fcambio=$("#txtFechaCambio").val();
	var notas=$("#txtNotas").val();
	
	if(usuario==''){
   	$("#txtUsuario").addClass("ui-state-error");
   	error++;
   		}
   	if(identificacion==''){
   	$("#txtIdentificacion").addClass("ui-state-error");
   	error++;
   		}
	if(nombre==''){
   	$("#txtNombre").addClass("ui-state-error");
   	error++;
   		}
	if(proceso==0){
	   	$("#cbxProceso").addClass("ui-state-error");
   		error++;
   		}
	if(area==0){
	   	$("#cbxArea").addClass("ui-state-error");
   		error++;
   		}
	if(agencia==0){
   	$("#cbxAgencia").addClass("ui-state-error");
   	error++;
   		}
	if(rol==0){
	   	$("#cbxRol").addClass("ui-state-error");
   		error++;
   		}
	if(finicio==''){
	   	$("#txtFechaInicio").addClass("ui-state-error");
   		error++;
   		}
	if(ffin==''){
	   	$("#txtFecTerminacion").addClass("ui-state-error");
   		error++;
   		}
	if(error>0) return false
	$.getJSON('usuarioGuardar.php',{v0:usuario,v1:identificacion,v2:nombre,v3:proceso,v4:area,v5:agencia,v6:rol,v7:finicio,v8:ffin,v9:estado,v10:fdigita,v11:fcambio,v12:notas,v99:1},function(datos){
		if(datos==0){
			MENSAJE("Lo lamento, el registro no fue guardado!");
			return false
			}
			
		MENSAJE("El registro fue almacenado!");
		nuevo=0;
		limpiarCampos();
		})
	}	
	
function buscarInforma(){
	var numero=$("#txtIdentificacion").val();
	$.ajax({ //consulta informa
	url: 'buscarInforma.php',
	type: 'POST',
	async: false,
	dataType: 'json',
	data: {v0:numero},
	success: function(data){
	if(data==0){
		MENSAJE("Lo lamento, pero ese numero NO existe en el maestro de empleados!");
		//habilitar textos fechainicio y fechafin
	}else{
		$.each(data,function(i,fila){
		$("#txtNombre").val($.trim(fila.nombre));
		//fecha=fila.f_ingreso;
		//elem = fecha.split('-'); 
		//$("#txtFechaInicio").val(elem[1]+'/'+elem[2]+'/'+elem[0]);
		$("#txtFechaInicio").val(fila.f_ingreso);
		//fecha=fila.f_vence;
		//elem = fecha.split('-'); 
		//$("#txtFecTerminacion").val(elem[1]+'/'+elem[2]+'/'+elem[0]);
		$("#txtFecTerminacion").val(fila.f_vence);
		$("#txtEstado").val(fila.estado);
		return;
	});
	if($("#txtEstado").val()=="R"){
		MENSAJE("El trabajador figura como RETIRADO!");
		limpiarCampos();
		nuevo=0;
	}
	}
	}// fin succes informa
	});//fin informa
	}
	
function consultar(){
	consulta=1;
	var  user=$.trim($("#txtUsuario").val());
	var op = 3;
	if(user.length==0){
		var  user=$.trim($("#txtIdentificacion").val());
		var op = 4;
		}
	$.getJSON('buscarUsuario.php', {v0:user,v1:op}, function(datos){
			if(datos==0){
				MENSAJE("Lo lamento, el Usuario NO exite!");
				$("#txtUsuario").val('');
				return false;
			}
		$.each(datos,function(i,f){
			idusuario=f.idusuario;
			$("#txtUsuario").val(f.usuario);
	 		$("#txtIdentificacion").val(f.identificacion);
	 		$("#txtNombre").val(f.nombres);
	 		$("#cbxProceso").val(f.idproceso).trigger('change');
	 		$("#cbxAgencia").val(f.idagencia);
	 		$("#cbxRol").val(f.idrol);
	 		$("#txtFechaInicio").val(f.fechainicio);
	 		$("#txtFecTerminacion").val(f.fechaterminacion);
	 		$("#txtEstado").val(f.estado);
	 		$("#txtFechaDigita").val(f.fechacreacion);
	 		$("#txtFechaCambio").val(f.fechaexpira);
	 		$("#txtNotas").val(f.notas);
			setTimeout((function(){
				$("#cbxArea").val(f.idarea);
			}),500);
			return;
			});
		})
	}	
	
function actualizar(){
	var error=0;
	if(idusuario==0 || modifica==0){
		MENSAJE("Primero haga clik en el boton Limpiar Campos, luego busque el usuario que va a modificar!");
		return false;
		}
	$(".ui-state-error").removeClass("ui-state-error");//Limpiar la clase de error
	var usuario=$("#txtUsuario").val();
	var identificacion=$("#txtIdentificacion").val();
	var nombre=$("#txtNombre").val();
	var proceso=$("#cbxProceso").val();
	var area=$("#cbxArea").val()
	var agencia=$("#cbxAgencia").val();
	var rol=$("#cbxRol").val();
	var finicio=$("#txtFechaInicio").val();
	var ffin=$("#txtFecTerminacion").val();
	var estado=$("#txtEstado").val();
	var fdigita=$("#txtFechaDigita").val();
	var fcambio=$("#txtFechaCambio").val();
	var notas=$("#txtNotas").val();
	
	if(usuario==''){
   	$("#txtUsuario").addClass("ui-state-error");
   	error++;
   		}
   	if(identificacion==''){
   	$("#txtIdentificacion").addClass("ui-state-error");
   	error++;
   		}
	if(nombre==''){
   	$("#txtNombre").addClass("ui-state-error");
   	error++;
   		}
	if(proceso==0){
	   	$("#cbxProceso").addClass("ui-state-error");
   		error++;
   		}
	if(area==0){
	   	$("#cbxArea").addClass("ui-state-error");
   		error++;
   		}
	if(agencia==0){
   	$("#cbxAgencia").addClass("ui-state-error");
   	error++;
   		}
	if(rol==0){
	   	$("#cbxRol").addClass("ui-state-error");
   		error++;
   		}
	if(finicio==''){
	   	$("#txtFechaInicio").addClass("ui-state-error");
   		error++;
   		}
	if(ffin==''){
	   	$("#txtFecTerminacion").addClass("ui-state-error");
   		error++;
   		}
	if(error>0) return false
	$.getJSON('usuarioGuardar.php',{v0:usuario,v1:identificacion,v2:nombre,v3:proceso,v4:area,v5:agencia,v6:rol,v7:finicio,v8:ffin,v9:estado,v10:fdigita,v11:fcambio,v12:notas,v99:2,v100:idusuario},function(datos){
		if(datos==0){
			MENSAJE("Lo lamento, el registro NO fue actualizado!");
			return false
			}
			
		MENSAJE("El registro fue actualizado!");
		nuevo=0;
		limpiarCampos();
		})
	}

function fechaCortaHoy(){
	var mydate=new Date()
	var year=mydate.getYear()
	if (year < 1000)
		year+=1900
	var day=mydate.getDay()
	var month=mydate.getMonth()
	var daym=mydate.getDate()
	month=parseInt(month)+1;
	return month+'/'+daym+'/'+year;
	
}

function mas90dias(){
	var mydate=new Date()
	var year=mydate.getYear()
	if (year < 1000)
		year+=1900
	var day=mydate.getDay()
	var month=mydate.getMonth()
	var daym=mydate.getDate()
	month=parseInt(month)+4;
	if(month>12){
		month=1;
		year++;
		}
	return month+"/" + daym +"/"+year;
	}