
var URL=src();

function validarCampos(){
	error=0;
	$(".ui-state-error").removeClass("ui-state-error");//Limpiar la clase de error
	var usuario =$.trim($("#txtUsuario").val());
	var clave =$.trim($("#txtClave").val());
	var clave2=$.trim($("#txtClave2").val());
	var clave3=$.trim($("#txtClave3").val());
	if(clave2.length<6){
		alert("Su clave debe tener minimo SEIS caracteres");
		return false; 
		}
	if(document.forma.txtClave.value.toUpperCase()==document.forma.txtClave2.value.toUpperCase()){
		alert("La clave actual y la nueva clave son iguales...!!!");
		return false;
		}
	if(clave2 != clave3){
		alert("La confirmaci\u00F3n de la clave no coincide!");
		$("#txtClave2").val('');
		$("#txtClave3").val('')
		return false
		}
	if(usuario.length==0){
		$("#txtUsuario").addClass("ui-state-error");
   		error++;
		}
	if(clave.length==0){
		$("#txtClave").addClass("ui-state-error");
   		error++;
		}
	if(clave2.length==0){
		$("#txtClave2").addClass("ui-state-error");
   		error++;
		}
	if(clave3.length==0){
		$("#txtClave3").addClass("ui-state-error");
   		error++;
		}
	if(error>0) return false
	var idu;
	var clavea;
	var estado;
	$.ajax({
		url: 'buscarUsuarioCambio.php',
		cache: false,
		type: "GET",
		dataType: "json",
		data: {v0:usuario,v1:clave},
		success: function(datos){
			//console.log(datos);
			if(datos==0){
				alert("Lo lamento, no encontre el usuario!");
				return false;
			}
			$.each(datos,function(i,fila){
				idu=fila.idusuario;
				estado=fila.estado;
				clavea=fila.claveanterior;
				return;
			});	
			if(estado != "A"){
				alert("El usuario esta INACTIVO!");
				return false;
			}
			else{ 
				$.ajax({
					url: 'nuevaClave.php',
					cache: false,
					type: 'GET',
					dataType: 'json',
					data: {v0:idu,v1:clave2,v2:clavea,v3:clave},
					success: function(dato){
						if(dato==0){
							alert("No se pudo actualizar la nueva clave!");
						}
						else if (dato==9) {
							alert("La clave ya fue usada, intente con otra!");
						}
						else{
							
							alert("Clave actualizada!");
							url=URL+"index.php";
							window.open(url,'_first');
						}
					}
				});// fin success 2
			}
		}
		});//fin success 	
	}
	