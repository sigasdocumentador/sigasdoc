<?php
ini_set("display_errors",'1');
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'Area.php';

$idProceso = $_POST['v0'];
$seleccionado = $_POST['v1'];
$listaAreas = Area::listarPorIdProceso($idProceso);

if($seleccionado != null){
	echo '<option value="0" selected>Seleccione</option>';
	foreach ($listaAreas as $value) {
		echo '<option value="'.$value->getIdArea().'">'.htmlentities($value->getArea()).'</option>';
	}
}else{
	echo '<option value="0">Seleccione</option>';
	foreach ($listaAreas as $value) {
		if($seleccionado == $value->getIdArea()){
			echo '<option value="'.$value->getIdArea().' selected">'.htmlentities($value->getArea()).'</option>';
		}else{
			echo '<option value="'.$value->getIdArea().'">'.htmlentities($value->getArea()).'</option>';
		}
	}
}
?>