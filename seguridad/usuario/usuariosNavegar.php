<?php
ini_set("display_errors",'1');
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$registroActual = $_SESSION['R_USUARIO_ACTUAL'];

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'seguridad' . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'funciones.php';

$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

$sql = "SELECT COUNT(aportes519.idusuario) AS TOTAL FROM aportes519";
$rs = $db->querySimple($sql);
$row=$rs->fetch();
$max=$row['total']-1;
$op=$_REQUEST['v0'];

switch($op){
	case 1 : $sql="SELECT top 1 * FROM aportes519 ORDER BY aportes519.idusuario ASC"; $_SESSION['R_USUARIO_ACTUAL']=0; break;
	case 2 : 
		$registroActual++;
		$sql="SELECT SKIP $registroActual top 1 * FROM aportes519"; 
		if($registroActual>=$max)$registroActual=$max; 
		$_SESSION['R_USUARIO_ACTUAL']=$registroActual; 
		break;
	case 3 : 
		$registroActual--;
		$sql="SELECT SKIP $registroActual top 1 * FROM aportes519";
		if($registroActual<=0)$registroActual=0;
		$_SESSION['R_USUARIO_ACTUAL']=$registroActual; break;
	case 4 : $sql="SELECT top 1 * FROM aportes519 ORDER BY aportes519.idusuario DESC"; $_SESSION['R_USUARIO_ACTUAL']=$max; break;
}

$rs = $db->querySimple($sql);
$con=0;
$filas = array();
while($row = $rs->fetch()){
	$filas[]=$row;
	$filas[0]['fechainicio']=f_reconvertir($filas[0]['fechainicio']);
	$filas[0]['fechaterminacion']=f_reconvertir($filas[0]['fechaterminacion']);
	$filas[0]['fechacreacion']=f_reconvertir($filas[0]['fechacreacion']);
	$filas[0]['fechaexpira']=f_reconvertir($filas[0]['fechaexpira']);
	$con++;
}
if($con==0){
	echo 0;
}
else{
	echo json_encode($filas);
}
?>