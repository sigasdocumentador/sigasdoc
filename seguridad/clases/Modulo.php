<?php
//modificado 29/08/2012 -$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
//include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'ArrayList.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';

class Modulo {
	
	//Atributos
	private $_idModulo;
	private $_modulo;
	private $_servidor;
	private $_path;
	private $_baseDatos;
	private $_tipoAplicacion;
	private $_lenguajeBase;
	private $_plataforma;
	private $_tipoAdquisicion;
	private $_fechaSistema;
	private $_usuario;
	private $_host;
	private $_instancia;
	///////////
	
	//Propiedades
	public function getIdModulo() {
		return $this->_idModulo;
	}
	
	public function setIdModulo($idModulo) {
		$this->_idModulo = $idModulo;
	}
	
	public function getModulo() {
		return $this->_modulo;
	}
	
	public function setModulo($modulo) {
		$this->_modulo = $modulo;
	}
	
	public function getServidor() {
		return $this->_servidor;
	}
	
	public function setServidor($servidor) {
		$this->_servidor = $servidor;
	}
	
	public function getPath() {
		return $this->_path;
	}
	
	public function setPath($path) {
		$this->_path = $path;
	}
	
	public function getBaseDatos() {
		return $this->_baseDatos;
	}
	
	public function setBaseDatos($baseDatos) {
		$this->_baseDatos = $baseDatos;
	}
	
	public function getTipoAplicacion() {
		return $this->_tipoAplicacion;
	}
	
	public function setTipoAplicacion($tipoAplicacion) {
		$this->_tipoAplicacion = $tipoAplicacion;
	}
	
	public function getLenguajeBase() {
		return $this->_lenguajeBase;
	}
	
	public function setLenguajeBase($lenguajeBase) {
		$this->_lenguajeBase = $lenguajeBase;
	}
	
	public function getPlataforma() {
		return $this->_plataforma;
	}
	
	public function setPlataforma($plataforma) {
		$this->_plataforma = $plataforma;
	}
	
	public function getTipoAdquisicion() {
		return $this->_tipoAdquisicion;
	}
	
	public function setTipoAdquisicion($tipoAdquisicion) {
		$this->_tipoAdquisicion = $tipoAdquisicion;
	}
	
	public function getFechaSistema() {
		return $this->_fechaSistema;
	}
	
	public function setFechaSistema($fechaSistema) {
		$this->_fechaSistema = $fechaSistema;
	}
	
	public function getUsuario() {
		return $this->_usuario;
	}
	
	public function setUsuario($usuario) {
		$this->_usuario = $usuario;
	}
	
	public function getHost() {
		return $this->_host;
	}
	
	public function setHost($host) {
		$this->_host = $host;
	}
	
	public function getInstancia() {
		return $this->_instancia;
	}
	
	public function setInstancia($instancia) {
		$this->_instancia = $instancia;
	}
	/////////////
	
	//Constructor
	function __construct($idModulo=0, $modulo="", $servidor="",
						 $path="", $baseDatos="", $tipoAplicacion="",
						 $lenguajeBase="", $plataforma="", $tipoAdquisicion="", $fechaSistema="",
						 $usuario="", $host="", $instancia="") {
		$this->_idModulo = $idModulo;
		$this->_modulo = $modulo;
		$this->_servidor = $servidor;
		$this->_path = $path;
		$this->_baseDatos = $baseDatos;
		$this->_tipoAplicacion = $tipoAplicacion;
		$this->_lenguajeBase = $lenguajeBase;
		$this->_plataforma = $plataforma;
		$this->_tipoAdquisicion = $tipoAdquisicion;
		$this->_fechaSistema = $fechaSistema;
		$this->_usuario = $usuario;
		$this->_host = $host;
		$this->_instancia = $instancia;
	}
	/////////////
	
	//Metodos staticos
	public static function listarPorRol($idRol){
		try {
			$db = IFXDbManejador::conectarDB();
			if($db->conexionID==null){
				$cadena = $db->error;
				echo msg_error($cadena);
				exit();
			}
			$rs = $db->querySimple("SELECT aportes511.idmodulo, aportes511.modulo, aportes511.servidor, aportes511.path, aportes511.basededatos, aportes511.tipoaplicacion, aportes511.lenguajebase, aportes511.plataforma, aportes511.tipoadquisicion, aportes511.fechasistema, aportes511.usuario, aportes511.host, aportes511.instancia FROM aportes511 INNER JOIN aportes512 ON aportes511.idmodulo=aportes512.idmodulo WHERE aportes512.idrol='$idRol'");
			$listModulos = new ArrayList();
			while($row = $rs->fetch()){
				$listModulos->add(new Modulo($row["idmodulo"], $row["modulo"], $row["servidor"], $row["path"], $row["basedatos"], $row["tipoaplicacion"], $row["lenguajebase"], $row["plataforma"], $row["tipoadquisicion"], $row["fechasistema"], $row["usuario"], $row["host"], $row["instancia"]));
			}
			return $listModulos->toArray();
		} catch (Exception $e) {
			echo msg_error($e->getMessage());
			exit();
		}
	}
	//////////////////
}

?>