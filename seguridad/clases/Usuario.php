
<?php

//modificado 29/08/2012 -$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
//include_once  $root;

class Usuario {
	
	//Atributos
	private $_idusuario;
	private $_usuario;
	private $_identificacion;
	private $_nombres;
	private $_idagencia;
	private $_fechacreacion;
	private $_fechainicio;
	private $_fechaterminacion;
	private $_clave;
	private $_estado;
	private $_cambio;
	private $_claveanterior;
	private $_fechacambio;
	private $_idrol;
	private $_fechaexpira;
	private $_inactivaciontemporal;
	private $_idarea;
	private $_notas;
	private $_idproceso;
	private $_responsable;
	private $_planta;
	
	//Propiedades
	public function getIdUsuario(){
		return $this->_idusuario;
	}
	
	public function setIdUsuario($idUsuario){
		$this->_idusuario = $idUsuario;
	}
	
	public function getUsuario(){
		return $this->_usuario;
	}
	
	public function setUsuario($usuario){
		$this->_usuario = $usuario;
	}
	
	public function getIdentificacion(){
		return $this->_identificacion;
	}
	
	public function setIdentificacion($identificacion){
		$this->_identificacion = $identificacion;
	}
	
	public function getNombres(){
		return $this->_nombres;
	}
	
	public function setNombres($nombres){
		$this->_nombres = $nombres;
	}
	
	public function getIdAgencia(){
		return $this->_idagencia;
	}
	
	public function setIdAgencia($idAgencia){
		$this->_idagencia = $idAgencia;
	}
	
	public function getAgencia(){
		return Agencia::consultarPorId($this->_idagencia);
	}
	
	public function getFechaCreacion(){
		return $this->_fechacreacion;
	}
	
	public function setFechaCreacion($fechaCreacion){
		$this->_fechacreacion = $fechaCreacion;
	}
	
	public function getFechaInicio(){
		return $this->_fechainicio;
	}
	
	public function setFechaInicio($fechaInicio){
		$this->_fechainicio = $fechaInicio;
	}
	
	public function getFechaTerminacion(){
		return $this->_fechaterminacion;
	}
	
	public function setFechaTerminacion($fechaTerminacion){
		$this->_fechaterminacion = $fechaTerminacion;
	}
	
	public function getClave(){
		return $this->_clave;
	}
	
	public function setClave($clave){
		$this->_clave = md5($clave);
	}
	
	public function getEstado(){
		return $this->_estado;
	}
	
	public function setEstado($estado){
		$this->_estado = $estado;
	}
	
	public function getCambio(){
		return $this->_cambio;
	}
	
	public function setCambio($cambio){
		$this->_cambio = $cambio;
	}
	
	public function getClaveAnterior(){
		return $this->_claveanterior;
	}
	
	public function setClaveAnterior($claveAnterior){
		$this->_claveanterior = $claveAnterior;
	}
	
	public function getFechaCambio(){
		return $this->_fechacambio;
	}
	
	public function setFechaCambio($fechaCambio){
		$this->_fechacambio = $fechaCambio;
	}
	
	public function getIdRol(){
		return $this->_idrol;
	}
	
	public function setIdRol($idRol){
		$this->_idrol = $idRol;
	}
	
	public function getRol(){
		return Rol::consultarPorId($this->_idrol);
	}
	
	public function getFechaExpira(){
		return $this->_fechaexpira;
	}
	
	public function setFechaExpira($fechaExpira){
		$this->_fechaexpira = $fechaExpira;
	}
	
	public function getInactivacionTemporal(){
		return $this->_inactivaciontemporal;
	}
	
	public function setInactivacionTemporal($inactivacionTemporal){
		$this->_inactivaciontemporal = $inactivacionTemporal;
	}
	
	public function getIdArea(){
		return $this->_idarea;
	}
	
	public function setIdArea($idArea){
		$this->_idarea = $idArea;
	}
	
	public function getArea(){
		return Area::consultarPorId($this->_idarea);
	}
	
	public function getNotas(){
		return $this->_notas;
	}
	
	public function setNotas($notas){
		$this->_notas = $notas;
	}
	
	public function getIdProceso(){
		return $this->_idproceso;
	}
	
	public function setIdProceso($idProceso){
		$this->_idproceso = $idProceso;
	}
	
	public function getProceso(){
		return Proceso::consultarPorId($this->_idproceso);
	}
	
	public function getResponsable(){
		return $this->_responsable;
	}
	
	public function setResponsable($responsable){
		$this->_responsable = $responsable;
	}
	
	public function getPlanta(){
		return $this->_planta;
	}
	
	public function setPlanta($planta){
		$this->_planta = $planta;
	}
	/////////////
	
	//Constructor
	function __construct($idusuario=0, $usuario="", $identificacion="", $nombres="", $idagencia=0, 
						 $fechacreacion="", $fechainicio="", $fechaterminacion="", $clave="", $estado="", 
						 $cambio="", $claveanterior="", $fechacambio="", $idrol=0, $fechaexpira="", $inactivaciontemporal="", 
						 $idarea=0, $notas="", $idproceso=0, $responsable="", $planta="") {
						 	
		$this->_idusuario = $idusuario;
		$this->_usuario = $usuario;
		$this->_identificacion = $identificacion;
		$this->_nombres = $nombres;
		$this->_idagencia = $idagencia;
		$this->_fechacreacion = $fechacreacion;
		$this->_fechainicio = $fechainicio;
		$this->_fechaterminacion = $fechaterminacion;
		$this->_clave = $clave;
		$this->_estado = $estado;
		$this->_cambio = $cambio;
		$this->_claveanterior = $claveanterior;
		$this->_fechacambio = $fechacambio;
		$this->_idrol = $idrol;
		$this->_fechaexpira = $fechaexpira;
		$this->_inactivaciontemporal = $inactivaciontemporal;
		$this->_idarea = $idarea;
		$this->_notas = $notas;
		$this->_idproceso = $idproceso;
		$this->_responsable = $responsable;
		$this->_planta = $planta;
	}
	//////////////
	
	//Metodos estaticos
	public static function consultarPorUsuario($usuario){
		try {
			$db = IFXDbManejador::conectarDB();
			if($db->conexionID==null){
				$cadena = $db->error;
				echo msg_error($cadena);
				exit();
			}
			$rs = $db->querySimple("SELECT aportes519.idusuario, aportes519.usuario, aportes519.identificacion, aportes519.nombres, aportes519.idagencia, aportes519.fechacreacion, aportes519.fechainicio, aportes519.fechaterminacion, aportes519.clave, aportes519.estado, aportes519.cambio, aportes519.claveanterior, aportes519.fechacambio, aportes519.idrol, aportes519.fechaexpira, aportes519.inactivaciontemporal, aportes519.idarea, aportes519.notas, aportes519.idproceso, aportes519.responsable, aportes519.planta FROM aportes519 WHERE aportes519.usuario = '$usuario'");
			$objUsuario = new Usuario();
			if($row = $rs->fetch()){
				$objUsuario = new Usuario($row["IDUSUARIO"], $row["USUARIO"], $row["IDENTIFICACION"], $row["NOMBRES"], $row["IDAGENCIA"], $row["FECHACREACION"], $row["FECHAINICIO"], $row["FECHATERMINACION"], $row["CLAVE"], $row["ESTADO"], $row["CAMBIO"], $row["CLAVEANTERIOR"], $row["FECHACAMBIO"], $row["IDROL"], $row["FECHAEXPIRA"], $row["INACTIVACIONTEMPORAL"], $row["IDAREA"], $row["NOTAS"], $row["IDPROCESO"], $row["RESPONSABLE"], $row["PLANTA"]);
			}
			return $objUsuario;
		} catch (Exception $e) {
			echo msg_error($e->getMessage());
			exit();
		}
	}
	
	public static function consultarPorNombres($nombres){
		try {
			$db = IFXDbManejador::conectarDB();
			if($db->conexionID==null){
				$cadena = $db->error;
				echo msg_error($cadena);
				exit();
			}
			$rs = $db->querySimple("SELECT aportes519.idusuario, aportes519.usuario, aportes519.identificacion, aportes519.nombres, aportes519.idagencia, aportes519.fechacreacion, aportes519.fechainicio, aportes519.fechaterminacion, aportes519.clave, aportes519.estado, aportes519.cambio, aportes519.claveanterior, aportes519.fechacambio, aportes519.idrol, aportes519.fechaexpira, aportes519.inactivaciontemporal, aportes519.idarea, aportes519.notas, aportes519.idproceso, aportes519.responsable, aportes519.planta FROM aportes519 WHERE aportes519.nombres LIKE '%$nombres%'");
			$objUsuario = new Usuario();
			if($row = $rs->fetch()){
				$objUsuario = new Usuario($row["IDUSUARIO"], $row["USUARIO"], $row["IDENTIFICACION"], $row["NOMBRES"], $row["IDAGENCIA"], $row["FECHACREACION"], $row["FECHAINICIO"], $row["FECHATERMINACION"], $row["CLAVE"], $row["ESTADO"], $row["CAMBIO"], $row["CLAVEANTERIOR"], $row["FECHACAMBIO"], $row["IDROL"], $row["FECHAEXPIRA"], $row["INACTIVACIONTEMPORAL"], $row["IDAREA"], $row["NOTAS"], $row["IDPROCESO"], $row["RESPONSABLE"], $row["PLANTA"]);
			}
			return $objUsuario;
		} catch (Exception $e) {
			echo msg_error($e->getMessage());
			exit();
		}
	}
	
	public static function consultarPorRegistro($numeroRegistro=0){
		try {
			$db = IFXDbManejador::conectarDB();
			if($db->conexionID==null){
				$cadena = $db->error;
				echo msg_error($cadena);
				exit();
			}
			if($numeroRegistro < 0){
				$rs = $db->querySimple("SELECT first 1 aportes519.idusuario, aportes519.usuario, aportes519.identificacion, aportes519.nombres, aportes519.idagencia, aportes519.fechacreacion, aportes519.fechainicio, aportes519.fechaterminacion, aportes519.clave, aportes519.estado, aportes519.cambio, aportes519.claveanterior, aportes519.fechacambio, aportes519.idrol, aportes519.fechaexpira, aportes519.inactivaciontemporal, aportes519.idarea, aportes519.notas, aportes519.idproceso, aportes519.responsable, aportes519.planta FROM aportes519 ORDER BY aportes519.idusuario DESC");
			}
			if($numeroRegistro == 0){
				$rs = $db->querySimple("SELECT first 1 aportes519.idusuario, aportes519.usuario, aportes519.identificacion, aportes519.nombres, aportes519.idagencia, aportes519.fechacreacion, aportes519.fechainicio, aportes519.fechaterminacion, aportes519.clave, aportes519.estado, aportes519.cambio, aportes519.claveanterior, aportes519.fechacambio, aportes519.idrol, aportes519.fechaexpira, aportes519.inactivaciontemporal, aportes519.idarea, aportes519.notas, aportes519.idproceso, aportes519.responsable, aportes519.planta FROM aportes519 ORDER BY aportes519.idusuario ASC");
			}else{
				$numeroRegistro--;
				$rs = $db->querySimple("SELECT SKIP $numeroRegistro first 1 aportes519.idusuario, aportes519.usuario, aportes519.identificacion, aportes519.nombres, aportes519.idagencia, aportes519.fechacreacion, aportes519.fechainicio, aportes519.fechaterminacion, aportes519.clave, aportes519.estado, aportes519.cambio, aportes519.claveanterior, aportes519.fechacambio, aportes519.idrol, aportes519.fechaexpira, aportes519.inactivaciontemporal, aportes519.idarea, aportes519.notas, aportes519.idproceso, aportes519.responsable, aportes519.planta FROM aportes519");
			}
			$objUsuario = new Usuario();
			if($row = $rs->fetch()){
				$objUsuario = new Usuario($row["IDUSUARIO"], $row["USUARIO"], $row["IDENTIFICACION"], $row["NOMBRES"], $row["IDAGENCIA"], $row["FECHACREACION"], $row["FECHAINICIO"], $row["FECHATERMINACION"], $row["CLAVE"], $row["ESTADO"], $row["CAMBIO"], $row["CLAVEANTERIOR"], $row["FECHACAMBIO"], $row["IDROL"], $row["FECHAEXPIRA"], $row["INACTIVACIONTEMPORAL"], $row["IDAREA"], $row["NOTAS"], $row["IDPROCESO"], $row["RESPONSABLE"], $row["PLANTA"]);
			}
			return $objUsuario;
		} catch (Exception $e) {
			echo msg_error($e->getMessage());
			exit();
		}
	}
	///////////////////
	
	//Metodos
	private function esValido(){
		if(($this->_idusuario != 0) && ($this->_usuario != "") && ($this->_identificacion != "") && 
		   ($this->_nombres != "") && ($this->_idagencia != 0) && ($this->_fechacreacion != "") && 
		   ($this->_fechainicio != "") && ($this->_fechaterminacion != "") && ($this->_clave != "") && 
		   ($this->_estado != "") && ($this->_cambio != "") && ($this->_claveanterior != "") && 
		   ($this->_fechacambio != "") && ($this->_idrol != 0) && ($this->_fechaexpira != "") && 
		   ($this->_inactivaciontemporal != "") && ($this->_idarea != 0) && ($this->_notas != "") && 
		   ($this->_idproceso != 0) && ($this->_responsable != "") && ($this->_planta != "")){
			
		   	return true;
		}
		return false;
	}
	
	public function guardar(){
		try {
			$db = IFXDbManejador::conectarDB();
			if($db->conexionID==null){
				$cadena = $db->error;
				echo msg_error($cadena);
				exit();
			}
			if($this->esValido()){
				$con = 0;
				$rs = $db->querySimple("SELECT aportes519.idusuario FROM aportes519 WHERE aportes519.idusuario = ".$this->_idusuario);
				if($rs->rowCount() > 0){
					$con = $db->queryActualiza("UPDATE aportes519 SET usuario='".trim($this->_usuario)."', identificacion='".trim($this->_identificacion)."', 
													nombres='".trim(strtoupper($this->_nombres))."', idagencia=$this->_idagencia, fechacreacion='".trim($this->_fechacreacion)."', 
													fechainicio='".trim($this->_fechainicio)."', fechaterminacion='".trim($this->_fechaterminacion)."', 
													clave='".trim($this->_clave)."', estado='".trim($this->_estado)."', cambio='".trim($this->_cambio)."', 
													claveanterior='".trim($this->_claveanterior)."', fechacambio='".trim($this->_fechacambio)."', idrol=$this->_idrol, 
													fechaexpira='".trim($this->_fechaexpira)."', inactivaciontemporal='".trim($this->_inactivaciontemporal)."', 
													idarea=$this->_idarea, notas='".trim(strtoupper($this->_notas))."', idproceso=$this->_idproceso, responsable='".trim($this->_responsable)."' 
												WHERE aportes519.idusuario=$this->_idusuario");
				}else{
					$con = $db->queryActualiza("INSERT INTO aportes519 (usuario, identificacion, nombres, idagencia, 
													fechacreacion, fechainicio, fechaterminacion, clave, estado, cambio, 
													claveanterior, fechacambio, idrol, fechaexpira, inactivaciontemporal, 
													idarea, notas, idproceso, responsable) 
												VALUES('".trim($this->_usuario)."', '".trim($this->_identificacion)."',
													'".trim(strtoupper($this->_nombres))."', $this->_idagencia, '".trim($this->_fechacreacion)."',
													'".trim($this->_fechainicio)."', '".trim($this->_fechaterminacion)."', '".trim($this->_clave)."',
													'".trim($this->_estado)."', '".trim($this->_cambio)."', '".trim($this->_claveanterior)."',
													'".trim($this->_fechacambio)."', $this->_idrol, '".trim($this->_fechaexpira)."', '".trim($this->_inactivaciontemporal)."',
													$this->_idarea, '".trim(strtoupper($this->_notas))."', $this->_idproceso, '".trim($this->_responsable)."')");
				}
				if($con > 0){
					return true;
				}
				return false;
			}
			return false;
		} catch (Exception $e) {
			echo msg_error($e->getMessage());
			exit();
		}
	}
	//////////
}

?>