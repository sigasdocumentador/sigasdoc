<?php
/* autor:       orlando puentes
 * fecha:       20/09/2010
 * objetivo:     
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

function fecha_hora_impresion(){
	$dia=date("j");
	$mes=date("n");
	$ano=date("Y");
	$hora=date("h:i:s A");
	switch($mes){
		case 1 : $m="Enero"; break;
		case 2 : $m="Febrero"; break;
		case 3 : $m="Marzo"; break;
		case 4 : $m="Abril"; break;
		case 5 : $m="Mayo"; break;
		case 6 : $m="Junio"; break;
		case 7 : $m="Julio"; break;
		case 8 : $m="Agosto"; break;
		case 9 : $m="Septiembre"; break;
		case 10 : $m="Octubre"; break;
		case 11 : $m="Noviembre"; break;
		case 12 : $m="Diciembre"; break;
		}
	echo $m.", ".$dia." de ".$ano." - hora: ".$hora; 
	}

//funci�n creada el dia 29 de marzo de 2016
function fecha_hora_(){
	$dia=date("j");
	$mes=date("n");
	$mes=str_pad($mes,2, "0", STR_PAD_LEFT);
	$ano=date("Y");
	$hora=date("H:i:s");
	$data = array("hora"=>$hora,"fecha"=>$ano."-".$mes."-".$dia);
	return $data;
}


function fun_digvernit($nit)
{
    //$vpri, $x, $y, $z, $i, $dv1;
    $nit1=$nit;
    if (!is_numeric($nit1))
    {
        //document.form1.dv.value="X";
        return false;
    } else {
        $vpri = array();
        $x=0 ; $y=0 ; $z=strlen($nit1);
        $vpri[1]=3;
        $vpri[2]=7;
        $vpri[3]=13;
        $vpri[4]=17;
        $vpri[5]=19;
        $vpri[6]=23;
        $vpri[7]=29;
        $vpri[8]=37;
        $vpri[9]=41;
        $vpri[10]=43;
        $vpri[11]=47;
        $vpri[12]=53;
        $vpri[13]=59;
        $vpri[14]=67;
        $vpri[15]=71;
        for($i=0 ; $i<$z ; $i++)
        {
            $y=(substr($nit1,$i,1));
            //document.write(y+"x"+ vpri[z-i] +":");
            $x+=($y*$vpri[$z-$i]);
            //document.write(x+"<br>");
        }
        $y=$x%11;
        //document.write(y+"<br>");
        if ($y > 1)
        {
            $dv1=11-$y;
        } else {
            $dv1=$y;
        }
        return $dv1;
    }
}

function moverArchivo($oldfile,$newfile){
    if(!rename($oldfile,$newfile)){
        if(copy($oldfile,$newfile)){
            unlink($oldfile);
            return TRUE;
        }
	return FALSE;
    }
    return TRUE;
}

function voltiarFecha($fecha){
	$a=substr($fecha,0,4);
	$m=substr($fecha,4,2);
	$d=substr($fecha,8,2);
	$f=$m."/".$d."/".$a;
	return $f;
	}
	
function verificaDirectorio($ruta){
	if (is_dir($ruta)){
		return true;
	}
	if(!mkdir($ruta, 0, true)){
		return false;
	}
	return true;
}

function nombreCorto($pnombre, $snombre = '', $papellido, $sapellido = ''){
	$lensnom=strlen($snombre);
	$lensape=strlen($sapellido);
	$cadena=$pnombre;
	if($lensnom>0) $cadena.= " ".$snombre;
	$cadena.= " ".$papellido;
	if($lensape>0) $cadena.= " ".$sapellido;
	$len=strlen($cadena);
	$vuel=0;
	if($len>26){
		do{
			if($lensape>1 && $lensnom>1){
				$cadena=$pnombre;
				$snombre=substr($snombre,0,1);
				if($lensnom>0) $cadena.= " ".$snombre;
				$cadena.= " ".$papellido;
				if($lensape>0) $cadena.= " ".$sapellido;
				$lensnom=strlen($snombre);
				$lensape=strlen($sapellido);
			}elseif($lensnom<2 && $lensape>1){
				$lensnom=strlen($snombre);
				$lensape=strlen($sapellido);
				$cadena=$pnombre;
				if($lensnom>0) $cadena.= " ".$snombre;
				$cadena.= " ".$papellido;
				if($lensape>0) $cadena.= " ".substr($sapellido,0,1);
			}else {
				//echo "<br>$idpersona - $cadena";
				break;
			}
			if($lensape==0 && $lensnom==0){
				//echo "<br>** $idpersona - $cadena";
				break;
			}
			$x=strlen($cadena);
			//echo "<br>".$cadena;
			$vuel++;
			if($vuel>4){
				//echo "<br> ......$idpersona - $cadena";
				$cadena='0';
				$x=0;
			}
		}while($x > 26);
	}
	return $cadena;
}
?>