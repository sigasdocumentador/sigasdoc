/*
* @autor:      Ing. Orlando Puentes
* @fecha:      8/10/2010
* objetivo:
*/
// funcion para obtener cookie en javascript
	function getCookie(c_name)
	{
		var i,x,y,ARRcookies=document.cookie.split(";");
		for (i=0;i<ARRcookies.length;i++)
		{
  			x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
  			y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
  			x=x.replace(/^\s+|\s+$/g,"");
  			if (x==c_name)
    			{
    				return unescape(y);
    			}// fin if
		}// fin for
  	}// fin getCookie
//funcion para obtener cookie en javascript
	
var URL=src();
var idt=0;
var continuar=1;
var max=0;
var nuevoR=0;
var aprobado=0;
var totalp =0;

$().ready(function(){
//Dialog ayuda
$("#ayuda").dialog({
		 	autoOpen: false,
			height: 500,
			width: 800,
			draggable:true,
			modal:false,
			open: function(evt, ui){
					$('#ayuda').html('');
					$.get(URL +'help/pignoracion/ayudaPignoracion.html',function(data){
							$('#ayuda').html(data);
					});
			 }
		});
//Dialog Colaboracion en linea
$("#dialog-form2").dialog({
		autoOpen: false,
		height: 400,
		width: 500,
		modal: true,
		buttons: {
			'Enviar': function() {
			var bValid = true;
			var campo=$('#notas').val();
			var campo0=$.trim(campo);
			if (campo0==""){
				$(this).dialog('close');
					return false;
				}
			var campo1=$('#usuario').val();
			var campo2="radicacion.php";
			$.post(URL+'phpComunes/colaboracion.php',{v0:campo0,v1:campo1,v2:campo2},
			function(datos){
				if(datos=='1'){
					alert("Su comentario fue enviado correctamente!. Gracias por participar en el mejoramiento de SIGAS");
				}else{
					alert(datos);
				}
		});
		$(this).dialog('close');
		},
		Cancelar: function() {
			$(this).dialog('close');
			$("#dialog-form2").dialog("destroy");
		}
	}
	});


//Dialog Ver mas
$("#verMas").dialog({
		 	autoOpen: false,
			modal: true,
		    width:600,
		   	resizable:false

   });//end dialog



});//fin ready
//-------------Nueva pignoracion

function nuevaPignoracion(){
limpiarCampos();
//$("#datost").empty(); mientras tanto lo oculto..
$("#datost").hide();
$("#select").focus();
$("#txtfecha").val(fechaHoy());
nuevoR=1;
$("#bGuardar").show();
}


//Dialog Ayuda
function mostrarAyuda(){
	$("#ayuda").dialog('open' );
	}

function notas(){
	$("#dialog-form2").dialog('open');
	}
//_-------------------------------------------------------

//Dialog ver Mas
function verMas(){
	$("#verMas").dialog("open");
}
//_-------------------------------------------------------
function buscarTrabajador(num){
//ocultar tabal beneficiarios
$("#capaBeneficiarios").hide();
continuar=1;
if(nuevoR==0){
    alert("Haga Click primero en nuevo, gracias!");
    $("#select").focus();
    return false;
}
var nit="";
if($.trim(num)==''){
	$("#datost").hide();
	return false;
}
var campo1=$("#select").val();
if(campo1==0){
	$("#select").focus();
	alert("Falta tipo documento!");
	return false;
	}
$.getJSON(URL+'phpComunes/buscarPersonaAfiliadaActiva.php',{v0:num,v1:campo1},function (datos){
	if(datos==0){
		var str="La persona no tiene afiliacion Activa!";
		MENSAJE(str);
		return false;
		}
	$.each(datos,function(i,fila){
		var nom=fila.pnombre+" "+fila.snombre+" "+fila.papellido+" "+fila.sapellido;
		nit=fila.nit;
        idt=fila.idpersona;
		$("#tdNombre").html(nom);
		$("#tdSalario").html(fila.salario);
		$("#tdFechaAfilia").html(fila.fechaingreso);
		$("#tdEmpresa").html(fila.nit+" - "+fila.razonsocial);
		return
	});
	
	
	$.post('validarTrabajador.php',{v0:idt,v1:nit},function(data){	
		var error=$.trim(data);
		switch(error){
                    case "1" :
					  var msje ="El Afiliado ya pignor\u00F3!";
                        continuar=0;
                    break;
                    case "2" :
                        var msje="El Afiliado tiene afiliaci\u00F3n por Servicios";
                        continuar=0;
                    break;
                    case "3" :
                        var msje ="El Afiliado tiene pignoraci\u00F3n pendiente de cancelar!";
                        continuar=0;
                    break;
                    case "4":
                        var msje="El Afiliado tiene el subsidio embargado!";
                        continuar=0;
                    break;
                    case "5" :
                        var msje="La empresa requiere autorizaci\u00F3n!";
                        continuar=0;
                    break;
                    case "6" :
                        var msje="Hace firste salarial de 4 smlv!";
                        continuar=0;
                    break;
                    case "7" :
                        var msje="Hace firste salarial con la c\u00F3nyuge!";
                        continuar=0;
                    break;
                    case "8" :
                        var msje="La empresa requiere autorizaci\u00F3n!";
                        continuar=0;
                    break;
                }
		if(continuar==0){
			MENSAJE(msje);
			limpiarCampos();
		}
	
	$.getJSON(URL+'phpComunes/buscarPignoracion.php',{v0:idt},function(data){
            if(data != 0){
            $("#historico tr:not(':first')").remove();
            $.each(data, function(i,fila){
      			$("#historico").append("<tr><td>"+fila.idpignoracion+"</td><td>"+fila.detalledefinicion+"</td><td>"+formatCurrency(fila.valorpignorado)+"</td><td>"+fila.fechapignoracion+"</td><td>"+fila.estado+"</td><td>"+formatCurrency(fila.saldo)+"</td><td>"+fila.fechacancelacion+"</td></tr>");
            });
            }
        
	//andres mostrar SOLO datos del trabajador e historico de pignoraciones
	
	
if(continuar==1){
	$("#capaBeneficiarios").show();
    $.getJSON('buscarPeriodos.php',{v0:idt},function(datas){
    if(datas != 0){
    $("#beneficiarios tr:not(':first')").remove();
    $.each(datas,function(i,fila){
        var nom =fila.pnombre+" "+fila.snombre+" "+fila.papellido+" "+fila.sapellido;
        var pago = (fila.tipogiro='N') ? 'Normal' : 'Mora';
        $("#beneficiarios").append("<tr><td>"+fila.periodo+"</td><td>"+fila.idbeneficiario+"</td><td>"+nom+"</td><td>"+fila.valor+"</td><td>"+fila.fechanacimiento+"</td><td>"+fila.edad+"</td><td>"+fila.capacidadtrabajo+"</td><td>"+fila.detalledefinicion+"</td><td>"+ pago+"</td></tr>")
    });
    }
    $.getJSON('comprobarUltimos.php', {v0:idt}, function(datos){
        if(datos<3){
        	var str="El afiliado NO cumple con los TRES subsidios consecutivos!";
        	MENSAJE(str)
            return false;
        }
   
    $.getJSON('buscarUltimo.php', {v0:idt}, function(datos){
       var numb=0;
       var valor=0;
       $.each(datos,function(i,fila){
            if(fila.cuenta>0){
                numb+=fila.cuenta;
                valor+=parseInt(fila.valor);
            }
       
       });
       max=parseInt(valor)*3;
       $("#valor").val(max);
       //andres mostrar el mensaje en ventana flotante, grande y rojo
       var str="El trabajador tiene CUPO MAXIMO de: "+formatCurrency(max);
       MENSAJE(str);
       $("#valor2").css({fontSize:"18px",color:"red"}).html(formatCurrency(max));
       aprobado=1;
        });
    });
    });//periodos
	};

   $("#datost").fadeIn();
   });     //pignoraciones
	
   });     //validacion
   })
}
//_-------------------------------------------------------

function guardarPignoracion(){
// andres obligatorios tipo,identificacion,convenio,valor
$("#bGuardar").hide();
if(nuevoR==0){
	MENSAJE("Primero haga click en Nuevo!");
	return false;
}	
if(aprobado==0){
		$("#bGuardar").show();
	    alert("La pignoracion no fue APROBADA!");
	    return false;
	}
if($("#select").val()=='0'){
	alert("Ingrese el tipo de identificacion");
	$("#select").focus();
        $("#bGuardar").show();
	return false;
	}

if($("#cedula").val()==''){
	alert("Ingrese la cedula del trabajador");
	$("#cedula").focus();
        $("#bGuardar").show();
	return false;
	}
	
	if($("#convenio").val()==0){
	alert("Ingrese el convenio.");
	$("#convenio").focus();
        $("#bGuardar").show();
	return false;
	}
	
if($("#valor").val()<=0) {
	
	alert("Ingrese un valor valido.");
        $("#bGuardar").show();
	return false;}

if($("#valor").val()>max){
    alert("El valor a pignorar no puede ser mayor a: " + formatCurrency(valor));
    $("#valor").val(max);
    $("#bGuardar").show();
    return false;
}

nuevoR=0;
var campo0=idt;
var campo1=$("#valor").val();
var campo2=$("#convenio").val();
totalp=campo1;
$.getJSON('grabarPagare.php', {v0:campo0,v1:campo1,v2:campo2}, function(datos){
    if(datos==0){
        alert("No se pudo guardar el Pagare!");
        limpiarCampos();
        $("#bGuardar").show();
        return false;
    }
    imprimir(datos);
    
});
}
//_-------------------------------------------------------

function imprimir(idp){
	var letras = covertirNumLetras(totalp);
    var url="http://"+getCookie("URL_Reportes")+"/pignoracion/reporte001.jsp?v0="+idp+'&letras='+letras;
    window.open(url,"_blank");
}

function limpiarCampos(){
$("table.tablero input:text").val('');
$("table.tablero  select").val('0');
$("#beneficiarios tr:not(':first')").remove();
$("#valor2").css({fontSize:"18px",color:"red"}).html(formatCurrency(0));
max=0;
aprobado=0;
}

function MENSAJE(str){
	var dialog="<div title='MENSAJE SIGAS' id='info'></div>";
	$("#info").dialog("destroy").remove();
	$(dialog).appendTo("body");
    $("#info").append("<p>"+str+"</p>");
    $("#info").dialog({
		  modal:true,
		  resizable:false
		  });      
    $("#convenio").focus();
}
