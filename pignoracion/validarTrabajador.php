<?php
/*
 * autor        Ing. Orlando Puentes
 * fecha        agosto 26 de 2010
 * objetivo
 */
ini_set("display_errors",'1');
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
		exit();
}
$idt = $_REQUEST['v0'];
$nit = $_REQUEST['v1'];

$sql2 = "SELECT top 1 smlv FROM aportes012 WHERE procesado='S' ORDER BY periodo DESC";
$rs2=$db->querySimple($sql2);
$row2=$rs2->fetch();

$sml = $row2['smlv'];

$sql = "Select count (*) as cuenta from aportes093 where idpersona=$idt and estado='A'";
$rs=$db->querySimple($sql);
$row=$rs->fetch();
if($row['cuenta'] > 0){
	echo 1; //ya pignoro
	exit();
}

$sql = "Select tipoformulario from aportes016 where idpersona=$idt and estado='A'";
$rs=$db->querySimple($sql);
$row=$rs->fetch();
if($row['tipoformulario'] == 49){
	echo 2; //afiliacion servicios
	exit();
}

$sql = "Select sum(salario) as suma from aportes016 where idpersona=$idt and estado='A'";
$rs=$db->querySimple($sql);
$row=$rs->fetch();
$salario=$row['suma']; //Obtener la sumatoria del salario del trabajador


$sql = "select count(*) as cuenta from aportes043 WHERE idtrabajador=$idt AND estado='A' AND saldo>0";
$rs=$db->querySimple($sql);
$row=$rs->fetch();
if($row['cuenta'] > 0){
	echo 3; //pignoracion pediente de pago
	exit();
}

$sql = "select idembargo,count(*) as cuenta from aportes018 WHERE idtrabajador=$idt and estado='A' group by idembargo";
$rs=$db->querySimple($sql);
$row=$rs->fetch();
if($row['cuenta'] > 0){
	
	$sql = "select count(*) as cuenta2emba from aportes022 where idembargo=".$row["idembargo"]." ";
	$rs=$db->querySimple($sql);
	$row2=$rs->fetch();

	$sql = "select count(*) as cuentabene from aportes021 where idtrabajador=$idt and estado='A' and idparentesco <>34 ";
	$rs=$db->querySimple($sql);
	$row3=$rs->fetch();
	
	if($row2['cuenta2emba']==$row3['cuentabene']){
		echo 4; //EMBARGO
		exit();
	}
	
	
}

$castigados = array('813013059', '813010026', '900271946', '900105441', '813003621', '813011917', '813011201', '900073160', '830507408', '813010430', '900169264', '813013031', '813013303', '813010121', '813008629', '830077367', '900267036');
if(in_array($nit,$castigados)){
	echo 5; //CASTIGADO
}

$sql="select count(idconyuge) as cuenta,idconyuge from aportes021 where conviven='S' and idparentesco=34 and idtrabajador=$idt GROUP by idconyuge";
$rs=$db->querySimple($sql);
$row=$rs->fetch();
$idc=0;
if($row['cuenta']>0)
    $idc=$row['idconyuge'];
if($idc==0){
    //solteros
    $smd=$salario/$sml;
    if($smd>4){
        echo 6;
    }
}
else{
    //casados
    $sql="SELECT sum(salario) as suma from aportes016 where idpersona in($idt,$idc) and estado='A'";
	$rs=$db->querySimple($sql);
	$row=$rs->fetch();
    $salario=$row['suma']/$sml;
    if($salario>6){
        echo 7;
    }
}

$sql = "select count(*) as cuenta from aportes048 WHERE nit='".$nit."' AND estado <>'A'";
$rs=$db->querySimple($sql);
$row=$rs->fetch();
if($row['cuenta'] > 0){
	echo 8; //la empresa esta inactiva
	exit();
}

/*
$sql = "select count(distinct periodo) as cuenta  from aportes048 a48 
inner join aportes011 a11 on a48.idempresa=a11.idempresa and periodo between '201305' and '201307' 
WHERE a48.nit='".$nit."'";
$rs=$db->querySimple($sql);
$row=$rs->fetch();
if($row['cuenta'] < 3){
	echo 9; //la empresa no tiene como minimo los tres ultimos pagos
	exit();
}
*/
echo 0;
?>
