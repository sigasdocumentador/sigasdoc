<?php 
ini_set("display_errors",1);

if ( !isset( $_SESSION ['USUARIO'] ) ) {
	session_start();
	$raiz=$_SESSION['RAIZ'];
	include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'session.php';
	session();
	}
else{
	//header("Location: http://10.10.1.121/sigas/error.html");
//exit();
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>::Reporte de afiliaciones P.U. No legalizadas::</title>
<link type="text/css" href="<?php echo URL_PORTAL.DIRECTORIO_ESTILOS_CSS_FORMULARIOS ?>base/ui.all.css" rel="stylesheet" />
<link type="text/css" href="<?php echo URL_PORTAL.DIRECTORIO_ESTILOS_CSS; ?>Estilos.css" rel="stylesheet"/>
<link type="text/css" href="<?php echo URL_PORTAL.DIRECTORIO_ESTILOS_CSS; ?>estilo_tablas.css" rel="stylesheet"/>
<link type="text/css" href="<?php echo URL_PORTAL.DIRECTORIO_ESTILOS_CSS; ?>formularios/demos.css" />
<link type="text/css" href="<?php echo URL_PORTAL.DIRECTORIO_ESTILOS_CSS; ?>estiloReporte.css" rel="stylesheet"/>
<script type="text/javascript" src="<?php echo URL_PORTAL.DIRECTORIO_SCRIPTS_JS; ?>jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL.DIRECTORIO_SCRIPTS_JS_FORMULARIOS; ?>ui/ui.core.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL.DIRECTORIO_SCRIPTS_JS; ?>comunes.js"></script>
</head>
<script>
var urlPortal = src();
$(document).ready(function(){
	
	$("#nitBuscar").keyup(function(){
		$("#empresas").html("");
	});
	
	$("#nitBuscar").blur(function(){
		$("#empresas").html("");
		buscarSucursales();
	});
	
	$("#buscar").click(function(){
		buscarSucursales();
	});
	
	$("#frm1").submit(function(){
		if( isNaN($("#nitBuscar").val()) ){
			alert("El NIT debe ser num\u00E9rico.");
			return false;
		}
		
		if( isNaN($("#periodo").val()) ){
			alert("El PERIODO debe ser num\u00E9rico.");
			return false;
		}
	});
	
	function buscarSucursales (){
		if( isNaN($("#nitBuscar").val()) ){
			alert("El NIT debe ser num\u00E9rico.");
			return false;
		}
		
		$("#empresas").html("Buscando...");
		var nit=$("#nitBuscar").val();
		$.getJSON(urlPortal+'aportes/empresas/buscarTodas.php',{v0:nit.toUpperCase(),v1:1},function(data){
			$("#empresas").html("");
			if(data!=''){
				$.each(data,function(i,fila){
					$("#empresas").append('<input type="radio" name="idEmpresa" id="idEnviar_'+ fila.idempresa +'" value="'+ fila.idempresa +'" > <label for="idEnviar_'+ fila.idempresa +'" >'+fila.nit+" - "+fila.razonsocial+'</label><br/>');
				});
			}else{
				//$("#buscarEmpresa").next("span.Rojo").html("No se encontraron resultados.").hide().fadeIn("slow");
				$("#empresas").html("No se encontraron empresas");
			}
		});
	};
});//Fin ready
</script>
<table width="100%" border="0" align="center">
	<tbody>
	<tr>
		<td align="center"><img src="<?php echo URL_PORTAL.DIRECTORIO_IMAGENES; ?>logo_reporte.png" width="707" height="86" /></td>
	</tr>
	<tr>
		<td align="center" ><strong class="titulo">Reporte de afiliaciones P.U. No legalizadas</strong></td>
	</tr>
	<tr>
		<td align="center">
			<form name="frm1" id="frm1" action="<?php echo URL_PORTAL.DIRECTORIO_CENTRO_REPORTES; ?>aportes/nomina/reporte007.php" method="post" >
				<table class="tablaR hover">
					<tr>
						<th>Per&iacute;odo</th>
						<td colspan="2"><input type="text" name="periodo" id="periodo" size="6" /></td>
					</tr>
					<tr>
						<th>Nit</th>
						<td><input type="text" name="nitBuscar" id="nitBuscar" /></td>
						<td><input type="button" name="buscar" id="buscar" value="Buscar" /></td>
					</tr>
					<tr>
						<th colspan="3">Empresas</th>
					</tr>
					<tr>
						<td colspan="3" id="empresas">
						</td>
					</tr>
					<tr>
						<td align="center" colspan="3"><input type="submit" value="Consultar" /></td>
					</tr>
				</table>
			</form>	
		</td>
	</tr>
</tbody></table>
</html>