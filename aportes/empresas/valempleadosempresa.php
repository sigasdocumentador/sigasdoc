<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';

$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

$data= array("error"=>0,"descripcion"=>"","activos"=>0,"pendientes"=>0);

if(isset($_REQUEST['idempresa']) and isset($_REQUEST['fechaestado'])){
	$sql="SELECT
                (SELECT count(*) FROM aportes016 WHERE idempresa=a048.idempresa AND estado='A' AND fechaingreso>='{$_REQUEST['fechaestado']}') AS Activos,
                (SELECT count(*) FROM aportes016 WHERE idempresa=a048.idempresa AND estado='P' AND fechaingreso>='{$_REQUEST['fechaestado']}') AS Pendientes
   FROM aportes048 a048 WHERE idempresa={$_REQUEST['idempresa']}";
	$res=$db->querySimple($sql);
	while($row=$res->fetch()){
		$data["activos"]=$row['Activos'];
		$data["pendientes"]=$row['Pendientes'];
	}
}else{
	$data["error"]=1;
	$data["descripcion"]="Los parametros no estan definidos";
} 
	
echo json_encode($data);

?>