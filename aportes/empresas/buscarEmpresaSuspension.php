<?php
	set_time_limit(0);
	ini_set("display_errors",'1');
	date_default_timezone_set("America/Bogota");
	$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
	include_once  $root;
	include_once $raiz.DIRECTORY_SEPARATOR.'aportes'.DIRECTORY_SEPARATOR.'empresas'.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'empresas.class.php';
	include_once $raiz.DIRECTORY_SEPARATOR.'promotoria'.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'promotoria.class.php';
	
	$objEmpresa = new Empresa();
	$objPromotoria = new Promotoria();
	
	$arrAtributoValor = $_REQUEST["objDatos"];
	$datoSuspension = $objEmpresa->buscar_empresa_suspendida($arrAtributoValor);
	
	$arrAttrLiquidacion = array("idempresa"=>"","periodo"=>"");
	
	//Buscar las liquidaciones de los periodos
	foreach($datoSuspension as &$rowDato){
		
		$arrAttrLiquidacion["idempresa"]=$rowDato["id_empresa"];
		$arrAttrLiquidacion["periodo"]=$rowDato["periodo"];
		$arrDatoLiquidacion = $objPromotoria->empresa_liquidacion($arrAttrLiquidacion);
		
		$rowDato["liquidacion"] = "N";
		if(count($arrDatoLiquidacion)>0){
			$rowDato["liquidacion"] = "S";
		}
	}
	
	unset($rowDato);
	
	$datoSuspension = count($datoSuspension)>0?json_encode($datoSuspension):0;
	echo $datoSuspension;
?>