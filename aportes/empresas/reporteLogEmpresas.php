<?php
   ini_set('display_errors','1');
   include_once  $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas'. DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'ArrayList.php';
   include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
   include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php'; 
   if(($_GET['fechaIni']=='' or $_GET['fechaFin']=='') and $_GET['nit']=='') {
   	  echo "<script>alert('Error parametros inclompletos'); window.close();</script>";
   	  exit();
   }
   $db = IFXDbManejador::conectarDB();
   if($db->conexionID==null){
   	$cadena = $db->error;
   	echo msg_error($cadena);
   	exit();
   }
   
   //datos pasados como parametros 
   $fechaIni=$_GET['fechaIni'];
   $fechaFin=$_GET['fechaFin'];
   $nit=$_GET['nit'];
   // se crea el where de la consulta
   $ban=0;
   $where='';
   if($nit!=''){
   	  $where = "a048.nit = '".$nit."'";
   	  $ban=1;
   }
   
   if($fechaIni!='' and $fechaFin!=''){
   	 if($ban==1){
   	 	$where .= " and (a123.fechaSistema>='".$fechaIni."' and a123.fechasistema<='".$fechaFin."')";
   	 }else{
   	 	$where .= "(a123.fechaSistema>='".$fechaIni."' and a123.fechasistema<='".$fechaFin."')";
   	 }
   	 
   }
   
   	  
   
   
   $array= new ArrayList();
   $dataresOld = Array();
   $dataresNew = Array();
   $fechaini='2016/05/18';
   $fechafin='2016/05/18';
   $sql="SELECT
           DISTINCT a123.id,
           a048.nit,
           a048.razonsocial,
           COALESCE(a015.pnombre + ' ' + a015.papellido,'') AS replegal,
           a048.direccion,
           a048.iddepartamento,
           a089.departmento,
           a048.idciudad,
           a089.municipio,
           a048.telefono,
           a048.email, 
           a123.* 
         FROM aportes123 a123 
           LEFT JOIN aportes048 a048 ON a123.idempresa=a048.idempresa
           LEFT JOIN aportes089 a089 ON a048.idciudad=a089.codmunicipio
           LEFT JOIN aportes015 a015 ON a048.idrepresentante=a015.idpersona
         WHERE {$where}";
   
   
   $result= $db->querySimple($sql);

   $i=0;
   while($row = $result->fetch()){
   	     $dataOld = $array->JSON2Array($row['dataOld']);
      	 $dataNew = $array->JSON2Array($row['dataNew']);
      	 //ajuste de datos a reportar
      	 $resComparacion[$i]['nit']=$row['nit'];
      	 $resComparacion[$i]['razonsocial']=$row['razonsocial'];
      	 $resComparacion[$i]['direccion']=$row['direccion'];
      	 $resComparacion[$i]['iddepartamento']=$row['departmento'];
      	 $resComparacion[$i]['idciudad']=$row['municipio'];
      	 $resComparacion[$i]['telefono']=$row['telefono'];
      	 $resComparacion[$i]['email']=$row['email'];
      	 $resComparacion[$i]['replegal']=$row['replegal'];
      	 $resComparacion[$i]['evento']=$row['evento'];
      	 $resComparacion[$i]['fechaSistema']=$row['fechaSistema'];
      	 $resComparacion[$i]['data'] = $array->compareTwoArray($dataOld,$dataNew);
      	 $i++;
   }
   // se verifica el numero de registros cargados
   if($i<=0){
     echo "<script>alert('Cero registros encontrados'); window.close();</script>";
   }
   
   $valspanauto= (count($array->arrayCols) * 2) + 2 ;
   ?>
   <html>
	<head>
		<title>Log Empresas UGPP</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<script type="text/javascript" src="../../js/1.6/jquery-1.6.2.min.js"></script>
		<script type="text/javascript" src="../../js/1.6/jquery-ui-1.8.16.custom.min.js"></script>
		<script src="../../js/jquery.table2excel.min.js"></script>
		<script>
		        function generar_excel(){
		        	var f = new Date();
				    $("#tablatempo").table2excel({
				       exclude: ".excludeThisClass",
				       name: "Worksheet Name",
				       filename: "reporte"+f.getFullYear()+f.getMonth()+f.getDate()+f.getHours()+":"+f.getMinutes()+":"+f.getSeconds() //do not include extension
				    });
				    setTimeout(cerrarVentana,2000);
			    }
			    function cerrarVentana(){
                  window.close();	    
				}
		</script>
	</head>
	<body>
		<center>
   <?php 
   echo '<table border="1" cellpadding="2" cellspacing="0" id="tablatempo" style="display: none">
   		   <tr>
   		      <td colspan="8" align="center" height="80"><img src="http://localhost/sigas/imagenes/logo_comfamiliar.jpg" border="0">Datos Actuales Empresa</td>
   		      <td colspan="'.$valspanauto.'" align="center">Relaci�n campos modificados</td>
   		   </tr>
   		   <tr>
   		      <td style="border-color:#677691; border-width:2px; border-style:solid">Nit</td>
   		      <td style="border-color:#677691; border-width:2px; border-style:solid">Razon Social</td>
   		      <td style="border-color:#677691; border-width:2px; border-style:solid">Rep Legal</td>
   		      <td style="border-color:#677691; border-width:2px; border-style:solid">Direccion</td>
   		      <td style="border-color:#677691; border-width:2px; border-style:solid">Departamento</td>
   		      <td style="border-color:#677691; border-width:2px; border-style:solid">Ciudad</td>
   		      <td style="border-color:#677691; border-width:2px; border-style:solid">Telefono</td>
   		      <td style="border-color:#677691; border-width:2px; border-style:solid">Email</td>';
           foreach($array->arrayCols as $cols){
           	  echo '<td style="border-color:#A7DC15; border-width:2px; border-style:solid">'.$cols.' Antes</td>';
           	  echo '<td style="border-color:#A7DC15; border-width:2px; border-style:solid">'.$cols.' Despues</td>';
   	       }
   echo '<td style="border-color:#A7DC15; border-width:2px; border-style:solid">Fecha Registro</td>
   		 <td style="border-color:#A7DC15; border-width:2px; border-style:solid">Evento</td>';
   echo '</tr>';	       
   echo '<tr>';
           foreach($resComparacion as $index => $valor){
   	          echo '<tr>
   	          		  <td>'.$valor['nit'].'</td>'.
   	                  '<td>'.$valor['razonsocial'].'</td>'.
   	                  '<td>'.$valor['replegal'].'</td>'.
   	                  '<td>'.$valor['direccion'].'</td>'.
   	                  '<td>'.$valor['iddepartamento'].'</td>'.
   	                  '<td>'.$valor['idciudad'].'</td>'.
   	                  '<td>'.$valor['telefono'].'</td>'.
   	                  '<td>'.$valor['email'].'</td>'
   	          ;
   	          foreach($array->arrayCols as $cols){
   		           if (array_key_exists($cols,$valor['data']['Old'])) {
   			           echo '<td>'.$valor['data']['Old'][$cols].'</td>';
   			           echo '<td>'.$valor['data']['New'][$cols].'</td>';
   		           }
   		           else{
   		           	   echo '<td></td>';
   		           	   echo '<td></td>';
   		           }
     	      }
     	      echo '<td>'.$valor['fechaSistema'].'</td>';
     	      echo '<td>'.$valor['evento'].'</td>';
   	          echo '</tr>';
           }
   		
   echo '</table>';
   echo '<script>generar_excel();</script>';
?>
  </center>
</body>