<?php 

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$url=$_SERVER['PHP_SELF'];
include_once $raiz.DIRECTORY_SEPARATOR ."config.php";
//include_once $raiz.DIRECTORY_SEPARATOR .'phpComunes'.DIRECTORY_SEPARATOR.'auditoria.php';
//auditar($url);

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejaInforma.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';

include_once $raiz. DIRECTORY_SEPARATOR .'clases'. DIRECTORY_SEPARATOR . 'p.definiciones.class.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'administradoras.class.php';

$Administradoras = new Administradoras();
$rsAdministradoras = $Administradoras->listar_ccf();

global $comprobantesAportes;		
$db = IFXDbManejaInforma::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
$Definiciones = new Definiciones();
$rsTiposDoc = $Definiciones->mostrar_datos(1);
$rsBancos = $Definiciones->mostrar_datos(35);
$rsTiposCuenta = $Definiciones->mostrar_datos(51);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Devoluci&oacute;n de Aportes</title>
<link type="text/css" href="../../css/css.1.8/jquery-ui-1.8.17.custom.css" rel="stylesheet">
<link type="text/css" href="../../css/marco.css" rel="stylesheet">
<link type="text/css" href="../../css/Estilos.css" rel="stylesheet">

<script type="text/javascript" src="../../js/1.6/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="../../js/1.6/jquery-ui-1.8.16.custom.min.js"></script>

<script type="text/javascript" src="../../js/comunes.js"></script>
<script type="text/javascript" src="js/devolucion.js" ></script>

<script type="text/javascript">
shortcut.add("Shift+F",function() {
	var url="<?php echo URL_PORTAL; ?>aportes/trabajadores/consultaTrabajador.php";
	window.open(url,"_blank");
},{
	'propagate' : true,
	'target' : document 
});       
</script>

<script language="javascript">
//$.ui.dialog.defaults.bgiframe = true;
var fechaActual = new Date();
var arregloAdministradoras = [];
var arregloBancos = [];
	$(function() {
		$("#ayuda").dialog({
		 	autoOpen: false,
			height: 450, 
			width: 700, 
			open: function(evt, ui){
					$('#ayuda').html('');
					$.get('../../help/aportes/ayudadevolucionAportes.html',function(data){
							$('#ayuda').html(data);
					});
			 }
		});
	});
</script>

<!-- colaboracion en linea  -->

<script type="text/javascript">
	$(function() {
		$("#dialog-form2").dialog("destroy");
		$("#dialog-form2").dialog({
			autoOpen: false,
			height: 400,
			width: 500,
			modal: true,
			buttons: {
				'Enviar': function() {
					var bValid = true;
					var campo=$('#notas').val();
					var campo0=$.trim(campo);
					if (campo0==""){
						$(this).dialog('close');
						return false;
						}
					var campo1=$('#usuario').val();
					var campo2="distribucion.php";
					$.post('../../phpComunes/colaboracion.php',{v0:campo0,v1:campo1,v2:campo2}, function(datos){
						if(datos=='1'){
							alert("Su comentario fue enviado correctamente!. Gracias por participar en el mejoramiento de SIGAS");
						}else{
							alert(datos);
						}
					});
				$(this).dialog('close');
				},
				Cancelar: function() {
					$(this).dialog('close');
					$("#dialog-form2").dialog("destroy");
				}
			}
			
		});
	});

	function mostrarAyuda(){
		$("#ayuda").dialog('open' );
	}

	function notas(){
		$("#dialog-form2").dialog('open');
	}
	</script>
</head>

<body>
<style type="text/css">
.errorTexto{
	background-color: #F5A9BC;
}

#tblAportes th{
	text-align: center;
}

#tblTrabajadores th{
	text-align: center;
}
</style>
<br/>
<center>
<table width="95%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td class="arriba_iz" width="10" >&nbsp;</td>
		<td class="arriba_ce" ><span class="letrablanca">::Devoluci&oacute;n de Aportes::</span></td>
		<td class="arriba_de" width="10" >&nbsp;</td>
	</tr>
	<tr>
		<td class="cuerpo_iz">&nbsp;</td>
		<td class="cuerpo_ce">
			<img src="../../imagenes/tabla/spacer.gif" width="1" height="1"/>
			<img src="../../imagenes/spacer.gif" width="1" height="1"/>
			<img src="../../imagenes/menu/nuevo.png" title="Nuevo" width="16" height="16" style="cursor:pointer" onclick="nuevoR();"/>
			<img src="../../imagenes/spacer.gif" width="12" height="1"/>
			<img src="../../imagenes/menu/grabar.png" width="16" height="16" style="cursor:pointer" title="Guardar" onclick="validarCampos();" id="bGuardar"/> 
			<img src="../../imagenes/spacer.gif" width="13" height="1"/>
			<img src="../../imagenes/menu/refrescar.png" width="16" height="16" style="cursor:pointer" title="Limpiar campos" onclick="camposLimpiar(); document.getElementById('txtNumero').focus();"/>
			<img src="../../imagenes/spacer.gif" width="7" height="1"/>
			<img src="../../imagenes/menu/informacion.png" width="16" height="16" style="border:none; cursor:pointer" title="Manual" onclick="mostrarAyuda();"/>
			<img src="../../imagenes/spacer.gif" width="2" height="1"/>
			<img src="../../imagenes/menu/notas.png" width="16" height="16" style="cursor: pointer" title="Colaboraci?n en l?nea" onClick="notas();"/>
		    <img src="../../imagenes/spacer.gif" width="1" height="1"/>
		    <br/>
		    <font size=1 face="arial">Nuevo&nbsp;Guardar&nbsp;Limpiar&nbsp;Info&nbsp;Ayuda</font>
		</td>
		<td class="cuerpo_de">&nbsp;</td>
	</tr>
	<tr>
		<td class="cuerpo_iz">&nbsp;</td>
		<td class="cuerpo_ce">&nbsp;</td>
		<td class="cuerpo_de">&nbsp;</td>
	</tr>
	<tr>
		<td class="cuerpo_iz" >&nbsp;</td>
		<td class="cuerpo_ce" >
			<table border="0" class="tablero" width="100%">
				<tr>					
					<td width="25%"><label for="txtNumero">Nit</label></td>
					<td>
						<input type="hidden" name="TrabExisten" id="TrabExisten" value='S' class="box1">
						<input type="hidden" name="hidIdEmpresa" id="hidIdEmpresa"/>
						<input type="text" class="box1" id="txtNumero" name="txtNumero" onkeydown="solonumeros(this);" onkeyup="solonumeros(this)"/></td>
				</tr>
				<tr>
				  	<td>NIT - Raz&oacute;n Social</td><td id="tdEmpresa" colspan="3">&nbsp;</td>
			  	</tr>
			  	<tr style="display:none;" id="TrIndicador">
				<td>&Iacute;ndice Aporte</td>
				<td colspan="3"><select name="Combindicador" class="box1" id="Combindicador">
				  <option value="0" selected="selected">Seleccione...</option>
				  	<?php
					$consulta=$Definiciones->mostrar_datos(18, 4);
					while($row=mssql_fetch_array($consulta)){
						echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
					}
					?>
				  </select></td>
				</tr>
			  	<tr>
					<td><label for="periodo" >Periodo</label></td><td colspan="3"><input type="text" class="box1" name="periodo" id="periodo" size="7" readonly /></td>
				</tr>		
				<tr><td colspan="4"><h3>Aportes</h3></td></tr>
				<tr>
					<td colspan="4">
						<table id="tblAportes">
							<thead>
								<tr><th>Opci&oacute;n</th><th>Planilla</th><th>Recibo</th><th>Periodo</th><th>Fecha pago</th><th>Val. Apo.</th><th>Devoluci&oacute;n</th><th>Aporte Neto</th><th>Val. Nom.</th><th>Val. Pag.</th><th>Val. Trab.</th><th>Ajuste</th><th>N&uacute;m. trab.</th></tr>
							</thead>
							<tbody></tbody>
						</table>
					</td>
				</tr>
				<tr>
				  	<td><label for="valor">Valor devoluci&oacute;n de este periodo</label></td>
				  	<td colspan="2"><input type="text" id="valor" name="valor" onkeydown="solonumeros(this);" onkeyup="solonumeros(this)" class="box1"/>
				  	&nbsp;&nbsp;&nbsp;&nbsp;Nombre Acto
				  	<input type="text" name="txtNombreActo" id="txtNombreActo" class="box1"/>
				  	&nbsp;&nbsp;&nbsp;&nbsp;N&uacute;mero Acto
				  	<input type="text" name="txtNumeroActo" id="txtNumeroActo" class="box1" onkeyup="solonumeros(this)" onkeydown="solonumeros(this);"/>
				  	&nbsp;&nbsp;&nbsp;&nbsp;Fecha Devoluci&oacute;n
				  	<input type="text" name="txtFechaDevolucion" id="txtFechaDevolucion" readonly="readonly" size="12" class="box1"/></td>
				</tr>
				<tr>
					<td colspan="4"><h3>Trabajadores</h3></td>
				</tr>
				<tr>
					<td><label for="tipoDoc">Tipo documento</label>
					
						<select name="tipoDoc" id="tipoDoc" onChange="document.getElementById('numDoc').value=''">
						<?php while($tipoDoc = mssql_fetch_assoc($rsTiposDoc)): 
								if(in_array($tipoDoc["iddetalledef"],array($tipoDoc["iddetalledef"]))):	?>
									<option value="<?php echo $tipoDoc["iddetalledef"]; ?>"><?php echo $tipoDoc["detalledefinicion"]; ?></option>
						<?php endif; 
							endwhile; ?>
						</select>
					</td>
					<td>
						&nbsp;&nbsp;&nbsp;<label for="numDoc">N&uacute;mero</label> <input type="text" id="numDoc" name="numDoc" onkeydown="solonumeros(this);" onkeyup="solonumeros(this)" class="box1" />
					</td>
				</tr>
				<tr>
					<td id="tdTrabajador" colspan="4">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="4">
						<label for="caja">Caja</label>&nbsp;&nbsp;
						<select name="caja" id="caja" >
						<?php 
						while($administradora = mssql_fetch_array($rsAdministradoras)):
							$administradora = array_map('utf8_encode',$administradora);
							$administradora = array_map('trim',$administradora);
						?>
							<option value="<?php echo $administradora['idadministradora'];?>"><?php echo $administradora['razonsocial'];?></option>
						<?php endwhile; ?>
						</select>
					</td>
				</tr>
				<tr>
					<td ><label for="banco">Banco</label>&nbsp;&nbsp;
						<select name="banco" id="banco" >
						<?php while($banco = mssql_fetch_array($rsBancos)):
							$banco = array_map('utf8_encode',$banco);
							$banco = array_map('trim',$banco);
						?>
						<option value="<?php echo $banco['iddetalledef'];?>"><?php echo $banco['detalledefinicion'];?></option>
						<?php endwhile; ?>
					</td>
					<td colspan="2"><label for="idtipocuenta">Tipo de cuenta</label>&nbsp;&nbsp;
						<select id="idtipocuenta" name="idtipocuenta">
							<?php while($tipoCuenta = mssql_fetch_array($rsTiposCuenta)): ?>
							<option value="<?php echo $tipoCuenta['iddetalledef'];?>"><?php echo $tipoCuenta['detalledefinicion'];?></option>
							<?php endwhile; ?>
						</select>
						&nbsp;&nbsp;<label for="numerocuenta">N&uacute;m. cuenta</label>&nbsp;&nbsp;<input type="text" size="30" id="numerocuenta" name="numerocuenta" class="box1"/>
						<label for="valortrabajador">Valor</label> <input type="text" id="valortrabajador" name="valortrabajador"  onkeyup="solonumeros(this)" onkeydown="solonumeros(this);" class="box1" /> 
						<input type="button" id="btn_add_persona" name="btn_add_persona" value="Agregar" />
					</td>
				</tr>
				<tr><td colspan="4"></td></tr>
				<tr><td colspan="4"><h3>Detalles de la devoluci&oacute;n</h3></td></tr>
				<tr>
					<td colspan="4" >
						<table id="tblTrabajadores" align="center">
							<thead>
								<tr>
									<th>Id.</th><th>Documento</th><th>Nombre</th><th>Caja</th><th>Banco</th><th>Tip. cuenta</th><th>N&uacute;m. cuenta</th><th>Valor</th><th>Opci&oacute;n</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
							<tfoot><tr><td colspan="6"></td><td><span style="font-weight: bold; font-size: 11pt">TOTAL</span></td><td id="td_total_devolver_trab" style="font-weight: bold; font-size: 11pt">&nbsp;</td><td>&nbsp;</td></tr></tfoot>
						</table>
					</td>
				</tr>
			</table>
		</td>
		<td class="cuerpo_de" >&nbsp;</td>
	</tr>
	<tr>
		<td class="cuerpo_iz" >&nbsp;</td>
		<td class="cuerpo_ce" >
			<table border="0" class="tablero" width="100%">
				<tr>
					<td><label for="motivo">Motivo de la devoluci&oacute;n</label></td>
					<td colspan="3"><textarea name="motivo" id="motivo" cols="75" rows="2" maxlength="200"  ></textarea></td>
				</tr>
			</table>
		</td>
		<td class="cuerpo_de" >&nbsp;</td>
	</tr>
	</tr>
	<tr>
		<td class="abajo_iz" >&nbsp;</td>
		<td class="abajo_ce" ></td>
		<td class="abajo_de" >&nbsp;</td>
	</tr>
</table>


<!-- FORMULARIO OBSERVACIONES-->
<div name="div-observaciones-tab" style="display:none" title="Observaciones de modificaci&oacute;n">
<table class="tablero">
 <tr>
   <td>Usuario</td>
   <td colspan="3" >
   <input name="usuarioObs" id="usuarioObs" class="box1" disabled="disabled" value="<?php echo $_SESSION['USUARIO']?>" /></td>
   </tr>
 <tr>
   <td>Observaciones</td>
   <td colspan="3" >
   <textarea name="observacionGrupo" id="observacionGrupo" cols="45" rows="5" class="boxlargo"></textarea></td>
   </tr>
</table>
<div class="ui-state-highlight ui-corner-all" style="margin: 9px auto; display:none;padding: 6px; text-align: center; border: 1px solid rgb(255, 255, 0); width:300px" id="rtaObservacion"><span class="ui-icon ui-icon-info" style="float: left; margin-right: 0.3em;"></span>Observaci&oacute;n guardada con &eacute;xito.</div>
</div>

<!-- fin colaboracion -->

<!-- ayuda en linea -->
<div id="ayuda" title="Manual .:. Devoluci&oacute;n Aportes" style="background-image:url('../../imagenes/FondoGeneral0.png')"></div>
<!-- fin ayuda en linea -->
</center>

<div id="dialog-persona" title="Formulario Datos Basicos" style="display:none">
<center>
<table width="578" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" class="tablero">
	<tbody>
		<tr bgcolor="#EBEBEB">
		  <td colspan="4" style="text-align:center" >Datos B&aacute;sicos de la persona</td>
	  </tr>
		<tr>
		  <td >Tipo Documento</td>
		  <td ><select name="tDocumento" id="tDocumento" class="box1">
          <option value="1" selected>Cedula Ciudadania</option>
          </select>
          </td>
		  <td >N&uacute;mero</td>
		  <td ><input name="numero" id="numero" type="text" class="box1"></td>
	  </tr>
		<tr>
		  <td >Primer Nombre</td>
		  <td ><input name="pNombre" id="tpNombre" type="text" class="box1"></td>
		  <td >Segundo Nombre</td>
		  <td ><input name="sNombre" id="tsNombre" type="text" class="box1"></td>
	  </tr>
		<tr>
		  <td >Primer Apellido</td>
		  <td ><input name="pApellido" id="tpApellido" type="text" class="box1"></td>
		  <td >Segundo Apellido</td>
		  <td ><input name="sApellido" id="tsApellido" type="text" class="box1"></td>
	  </tr>
		<tr>
		  <td >Nombre Corto</td>
		  <td colspan="3" ><input name="tncorto" type="text" class="boxlargo" id="tncorto" readonly></td>
	    </tr>
		<tr>
			<td >&nbsp;</td>
			<td >&nbsp;</td>
			<td >&nbsp;</td>
			<td >&nbsp;</td>
		</tr>
</tbody>
</table>
</center>
</div>

</body>
</html>