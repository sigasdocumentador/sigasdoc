<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'p.definiciones.class.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';
$objClase=new Definiciones();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Inactivar Empresa::</title>
<link href="<?php echo URL_PORTAL; ?>css/Estilos.css" rel="stylesheet" type="text/css" />
<link type="text/css" href="<?php echo URL_PORTAL; ?>css/estilo_tablas.css" rel="stylesheet"/>
<link type="text/css" href="<?php echo URL_PORTAL; ?>css/formularios/base/ui.all.css" rel="stylesheet" />
<link href="<?php echo URL_PORTAL; ?>css/marco.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/jquery-1.4.2.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/formularios/ui/ui.core.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/formularios/ui/ui.dialog.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/formularios/ui/ui.datepicker.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/comunes.js"></script>
<script type="text/javascript" src="js/inactivarEmpresa.js"></script>
</head>

<body>
<br /><br />
<table border="0" align="center" cellpadding="0" cellspacing="0" width="80%">
  <tbody>
 
  <!-- ESTILOS SUPERIOR TABLA-->
  <tr>
    <td width="13" height="29" class="arriba_iz">&nbsp;</td>
    <td class="arriba_ce"><span class="letrablanca">::&nbsp;Inactivar Empresa&nbsp;::</span></td>
    <td width="13" class="arriba_de" align="right">&nbsp;</td>
  </tr>
  
  <!-- ESTILOS ICONOS TABLA-->
  <tr>
   <td class="cuerpo_iz">&nbsp;</td>
	<td class="cuerpo_ce">  <img src="../../imagenes/tabla/spacer.gif" width="1" height="1">&nbsp;</td>
    <td class="cuerpo_de">&nbsp;</td>
  
  </tr>
  
  <!-- ESTILOS MEDIO TABLA-->
  <tr>
    <td class="cuerpo_iz">&nbsp;</td>
    <td class="cuerpo_ce">
   <table border="0" cellpadding="5" align="center" class="tablero" width="90%">
<tr>
<td width="17%">NIT:</td>
<td width="33%"><input id="nit" type="text" class="box1" onblur="buscarEmpresa(this.value)" /></td>
<td colspan="2"></td>
</tr>
<tr>
  <td>Razón Social</td>
  <td colspan="3"><label for="razonSocial"></label>
    <input type="text" size="80%" name="razonSocial" id="razonSocial"  class="box1"/></td>
</tr>
<tr>
  <td>Fecha Afiliación</td>
  <td><input id="fechaAfiliacion" type="text" class="box1" /></td>
  <td>Fecha Inactivación</td>
  <td><input id="fechaInactivacion"  disabled="disabled" type="text" class="box1" /></td>
</tr>
</table>
	<div></div>
    <td class="cuerpo_de">&nbsp;</td>
  </tr>
  <!-- CONTENIDO TABLA-->
  <tr>
    <td class="cuerpo_iz">&nbsp;</td>
    <td class="cuerpo_ce" align="center">
	<br/>
   	<br/>
    <input type="button" class="ui-state-default" id="siguiente" value="Inactivar" disabled="disabled" onclick="seguir()" />
    </td>
    <td class="cuerpo_de">&nbsp;</td>
  </tr>
  
  <!-- ESTILOS PIE TABLA-->
  <tr>
    <td height="34" class="abajo_iz" >&nbsp;</td>
    <td class="abajo_ce" ></td>
    <td class="abajo_de" >&nbsp;</td>
  </tr>
  
</tbody>
</table>
</body>
</html>