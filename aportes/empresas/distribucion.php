<?php 
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$url=$_SERVER['PHP_SELF'];
include_once $raiz.DIRECTORY_SEPARATOR ."config.php";
include_once $raiz.DIRECTORY_SEPARATOR .'phpComunes'.DIRECTORY_SEPARATOR.'auditoria.php';
auditar($url);
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejaInforma.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
include_once $raiz. DIRECTORY_SEPARATOR .'clases'. DIRECTORY_SEPARATOR . 'p.definiciones.class.php';
global $comprobantesAportes;		
$db = IFXDbManejaInforma::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

$Definiciones = new Definiciones();
$rsIndices = $Definiciones->mostrar_datos(18);
$fecver = date('Ymd h:i:s A',filectime('distribucion.php'));
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Distribucion de aportes</title>
<link type="text/css" href="../../css/css.1.8/jquery-ui-1.8.17.custom.css" rel="stylesheet">
<link type="text/css" href="../../css/marco.css" rel="stylesheet">
<link type="text/css" href="../../css/Estilos.css" rel="stylesheet">

<script type="text/javascript" src="../../js/1.6/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="../../js/1.6/jquery-ui-1.8.16.custom.min.js"></script>

<script type="text/javascript" src="../../js/comunes.js"></script>
<script type="text/javascript" src="js/distribucion.js" ></script>

<script type="text/javascript">
shortcut.add("Shift+F",function() {
	var url="<?php echo URL_PORTAL; ?>aportes/trabajadores/consultaTrabajador.php";
	window.open(url,"_blank");
},{
	'propagate' : true,
	'target' : document 
});       
</script>

<script language="javascript">
//$.ui.dialog.defaults.bgiframe = true;
var fechaActual = new Date();
	$(function() {
		$("#ayuda").dialog({
		 	autoOpen: false,
			height: 450, 
			width: 700, 
			open: function(evt, ui){
					$('#ayuda').html('');
					$.get('../../help/aportes/ayudadistribucionAportes.html',function(data){
							$('#ayuda').html(data);
					});
			 }
		});
	});
</script>

<!-- colaboracion en linea  -->

<script type="text/javascript">
	$(function() {
		$("#dialog-form2").dialog("destroy");
		/*var notas = $("#notas"),
			allFields = $([]).add(notas);*/
		
		$("#dialog-form2").dialog({
			autoOpen: false,
			height: 400,
			width: 500,
			modal: true,
			buttons: {
				'Enviar': function() {
					var bValid = true;
					var campo=$('#notas').val();
					var campo0=$.trim(campo);
					if (campo0==""){
						$(this).dialog('close');
						return false;
						}
					var campo1=$('#usuario').val();
					var campo2="distribucion.php";
					$.post('../../phpComunes/colaboracion.php',{v0:campo0,v1:campo1,v2:campo2}, function(datos){
						if(datos=='1'){
							alert("Su comentario fue enviado correctamente!. Gracias por participar en el mejoramiento de SIGAS");
						}else{
							alert(datos);
						}
					});
				$(this).dialog('close');
				},
				Cancelar: function() {
					$(this).dialog('close');
					$("#dialog-form2").dialog("destroy");
				}
			}
			
		});
		
		/*$('#enviar-notas')
			.button()
			.click(function() {
				$('#dialog-form2').dialog('open');
			});*/
	});

	function mostrarAyuda(){
		$("#ayuda").dialog('open' );
		}

	function notas(){
		$("#dialog-form2").dialog('open');
		};	
	</script>
	<style type="text/css">
		.errorTexto{
			background-color: #F5A9BC;
		}
	</style>
</head>

<body>
<div id="divMensaje" style="display: none;">
	<center>
		<br/>
		<strong>Datos Del Periodo</strong>
		<table border="0" class="tablero" >
			<thead>
				<tr align="center"><th> Periodo </th><th> Aportes </th><th> N&oacute;mina </th><th> Ajustes </th><th> N&uacute;m. Trabajadores </th><th> Fecha Sistema </th></tr>
			</thead>
			<tbody id="tbDatosPeriodo">
				
			</tbody>
		</table>
	</center>
</div>
<br/>
<center>
<table width="95%" border="0" cellspacing="0" cellpadding="0">
<tr>
 <td class="arriba_iz" width="10" >&nbsp;</td>
<td class="arriba_ce" ><span class="letrablanca">::Registro de Aportes::</span>
 				<div style="text-align:right;float:right;">
					<?php echo 'Versi&oacute;n: ' . $fecver; ?>
				</div>
</td>
 <td class="arriba_de" width="10" >&nbsp;</td>
</tr>
<tr>
<td class="cuerpo_iz">&nbsp;</td>
<td class="cuerpo_ce">
	<img src="../../imagenes/tabla/spacer.gif" width="1" height="1"/>
	<img src="../../imagenes/spacer.gif" width="1" height="1"/>
	<img src="../../imagenes/menu/nuevo.png" title="Nuevo" width="16" height="16" style="cursor:pointer" onclick="nuevoR();"/>
	<img src="../../imagenes/spacer.gif" width="12" height="1"/>
	<img src="../../imagenes/menu/grabar.png" width="16" height="16" style="cursor:pointer" title="Guardar" onclick="validarCampos();" id="bGuardar"/> 
	<img src="../../imagenes/spacer.gif" width="13" height="1"/>
	<img src="../../imagenes/menu/refrescar.png" width="16" height="16" style="cursor:pointer" title="Limpiar campos" onclick="camposLimpiar(); document.getElementById('comprobante').focus();"/>
	<img src="../../imagenes/spacer.gif" width="7" height="1"/>
	<img src="../../imagenes/menu/informacion.png" width="16" height="16" style="border:none; cursor:pointer" title="Manual" onclick="mostrarAyuda();"/>
	<img src="../../imagenes/spacer.gif" width="2" height="1"/>
	<img src="../../imagenes/menu/notas.png" width="16" height="16" style="cursor: pointer" title="Colaboraci?n en l?nea" onClick="notas();"/>
    <img src="../../imagenes/spacer.gif" width="1" height="1"/>
    <br>
    <font size=1 face="arial">Nuevo&nbsp;Guardar&nbsp;Limpiar&nbsp;Info&nbsp;Ayuda</font>
    </td>
<td class="cuerpo_de">&nbsp;</td>
</tr>
<tr>
<td class="cuerpo_iz">&nbsp;</td>
<td class="cuerpo_ce">&nbsp;</td>
<td class="cuerpo_de">&nbsp;</td>
</tr>
<tr>
<td class="cuerpo_iz" >&nbsp;</td>
<td class="cuerpo_ce" >
	<table border="0" class="tablero" width="100%">
	<tr>
	<td width="25%">Comprobante</td>
	<td width="25%">
    <select id="comprobante" name="comprobante" class="box1">
		<option value="0" selected="selected">Seleccione</option>
	<?php foreach($comprobantesAportes as $comprobante): ?>
	<option value="<?php echo $comprobante; ?>"><?php echo $comprobante; ?></option>
	<?php endforeach; ?>  
	</select>
	</td>
	<td width="25%">N&uacute;mero</td><td><input type="text" id="txtNumero" name="txtNumero" onblur="buscarRecibo();" onkeydown="solonumeros(this);" onkeyup="solonumeros(this)"/></td>
	</tr>
	<tr>
  	<td>NIT - Raz&oacute;n Social</td>
  	<td colspan="3" id="tdNit">&nbsp;</td>
  	</tr>
	<tr>
  	<td>Valor Aportes</td>
  	<td id="tdValor">&nbsp;</td>
  	<td id="tdFecha">Fecha Consignaci&oacute;n</td>
  	<td>&nbsp;</td>
	</tr>
	<tr>
  	<td>Indice aporte</td>
  	<td>
    <select id="indice" name="indice" class="box1" onchange="calcular();">
	    <option value="0" selected="selected">Seleccione</option>
	    <?php while($indice = mssql_fetch_assoc($rsIndices)): ?>
	    <option value="<?php echo $indice["codigo"]; ?>"><?php echo $indice["detalledefinicion"]?></option>
	    <?php endwhile;?>
    </select>
    </td>
  	<td>Nómina</td>
  	<td id="tdNomina">&nbsp;</td>
	</tr>
    <tr>
	  <td>Descripción</td>
	  <td colspan="3" id="tdDes">&nbsp;</td>
	  </tr>
	<tr>
	  <td>Notas</td>
	  <td colspan="3" id="tdNotas">&nbsp;</td>
	  </tr>
	<tr>
	  <td>Número periodos</td>
	  <td colspan="3" id="tdNotas2"><input type="text" id="numPer" name="numPer" class="box1"  onblur="crearPeriodos();"  onkeydown="solonumeros(this);" onkeyup="solonumeros(this)" maxlength="3"/></td>
	  </tr>
	</table>
</td>
<td class="cuerpo_de" >&nbsp;</td>
</tr>
<tr>
<td class="cuerpo_iz" >&nbsp;</td>
<td class="cuerpo_ce" style="text-align:center" ><label class="RojoGrande">Periodos</label></td>
<td class="cuerpo_de" >&nbsp;</td>
</tr>
<tr>
<td class="cuerpo_iz" >&nbsp;</td>
<td class="cuerpo_ce" style="text-align:center" >
<div id="periodo1">
	<table width="100%" class="tablero" id="tablaPeriodos">
		<thead>
			<tr><th >Periodo</th><th>N&oacute;mina</th><th>Aporte</th><th>Ajuste?</th><th>N&uacute;m. trabajadores</th><th>Observaci&oacute;n</th></tr>
		</thead>
	</table>
</div>
</td>
<td class="cuerpo_de" >&nbsp;</td>
</tr>
<tr>
<td width="10" class="cuerpo_iz" >&nbsp;</td>
<td class="cuerpo_ce" >&nbsp;</td>
<td width="10" class="cuerpo_de" >&nbsp;</td>
</tr>
<tr>
<td class="abajo_iz" >&nbsp;</td>
<td class="abajo_ce" ></td>
<td class="abajo_de" >&nbsp;</td>
</tr>
</table>

<!-- DIV MENSAJES -->
<div id="divMensajeError" ></div>

<!-- FORMULARIO OBSERVACIONES-->
<div name="div-observaciones-tab" style="display:none" title="Observaciones de modificaci&oacute;n">
<table class="tablero">
 <tr>
   <td>Usuario</td>
   <td colspan="3" >
   	<!-- <input name="usuarioObs" id="usuarioObs" class="box1" disabled="disabled" value="<?php echo $_SESSION['USUARIO']?>" />-->
   	<label class="box1"><?php echo $_SESSION['USUARIO']?></label>
   </td>
   </tr>
 <tr>
   <td>Observaciones</td>
   <td colspan="3" >
   <textarea name="observacionGrupo" id="observacionGrupo" cols="45" rows="5" class="boxlargo"></textarea></td>
   </tr>
</table>
<div class="ui-state-highlight ui-corner-all" style="margin: 9px auto; display:none;padding: 6px; text-align: center; border: 1px solid rgb(255, 255, 0); width:300px" id="rtaObservacion"><span class="ui-icon ui-icon-info" style="float: left; margin-right: 0.3em;"></span>Observaci&oacute;n guardada con &eacute;xito.</div>
</div>

<!-- fin colaboracion -->

<!-- ayuda en linea -->
<div id="ayuda" title="Manual .:. Distribución Aportes" style="background-image:url('../../imagenes/FondoGeneral0.png')"></div>
<!-- fin ayuda en linea -->
<input type="hidden" name="idRepresemtante" id="idRepresemtante" />
<input type="hidden" name="idContacto" id="idContacto" />
</center>
</body>
</html>