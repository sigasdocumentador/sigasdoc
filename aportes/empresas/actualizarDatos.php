<?php
/* autor:       Orlando Puentes
 * fecha:       Julio 23 de 2010
 * objetivo:    Almacenar en la base de datos Aportes la información de las empresas afiliadas a Comfamiliar Huila.
 */
 
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$url=$_SERVER['PHP_SELF'];
include_once $raiz. DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'auditoria.php';
auditar($url);

include_once $raiz.DIRECTORY_SEPARATOR.'config.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'funcionesComunes' . DIRECTORY_SEPARATOR . 'funcionesComunes.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR .'p.definiciones.class.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'ciudades.class.php';
global $arregloAgencias;
$objClase=new Definiciones();
$objCiudad=new Ciudades();
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

$sql="select distinct idciiu,seccion,descripcion FROM aportes079 where division='' and grupo ='' and clase ='' order by idciiu";
$rs=$db->querySimple($sql);

$sql_tipos_tel="select ID_detalles, Descripcion1_detalles from Detalles as D join Grupos as G on G.ID=D.Grupos_ID WHERE Grupos_ID=7";
$tipos_tel=$db->querySimple($sql_tipos_tel);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="es">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Actualizar Datos de Empresa</title>
<link type="text/css" href="../../css/formularios/base/ui.all.css" rel="stylesheet" />
<link type="text/css" href="../../css/Estilos.css" rel="stylesheet" />
<link type="text/css" href="../../css/formularios/demos.css" />
<link type="text/css" href="../../css/marco.css" rel="stylesheet" />
<script type="text/javascript" src="../../js/jquery-1.4.2.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.core.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.resizable.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.dialog.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.datepicker.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/effects.core.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.button.js"></script>
<script type="text/javascript" src="../../js/comunes.js"></script>
<script type="text/javascript" src="../../js/jquery.combos.js"></script>
<script type="text/javascript" src="../../js/direccion.js"></script>
<script type="text/javascript" src="../../js/jquery.personaSimple.js"></script>
<script type="text/javascript" src="js/empresas.js"></script>
<script type="text/javascript" src="js/actualizarDatos.js"></script>
<script type="text/javascript" src="js/Ciiu.js"></script>

<script type="text/javascript">
shortcut.add("Shift+F",function() {
		                                var URL=src();
		var url=URL+"aportes/trabajadores/consultaTrabajador.php";
    	window.open(url,"_blank");
    },{
	'propagate' : true,
	'target' : document 
        });        
</script>


<script type="text/javascript">
	$(function() {
		$('#datepicker, #textfield18').datepicker({
			changeMonth: true,
			changeYear: true
		});
	});
	</script>

<script language="javascript">
$.ui.dialog.defaults.bgiframe = true;
	$(function() {
		$("#ayuda").dialog({
			 autoOpen: false,
			 height: 550, 
			 width: 800, 
			 open: function(evt, ui){
					$('#ayuda').html('');
					$.get('../../help/aportes/ayudaEmpresa.html',function(data){
							$('#ayuda').html(data);
					})
			 }
			 });
		$("#dialog-form2").dialog({ autoOpen: false });
	});  
	
$(document).ready(function(){
	// Parametros para e combo1
	$("#combo1").change(function () {
		$("#combo1 option:selected").each(function () {
			//alert($(this).val());
			elegido=$(this).val();
			$.post("../phpComunes/ciudades.php", { elegido: elegido }, function(data){
			$("#combo2").html(data);
			$("#combo3").html("");
			});			
	      });	
	   });
	
	$("#cboCiudad2").change(function () {
		$("#cboCiudad2 option:selected").each(function () {
			elegido=$(this).val();
			$.post("../../phpComunes/zonas.php", { elegido: elegido }, function(data){
				$("#cboZonaCA").html(data);
				$("#cboBarrioCA").html("");
			});			
		});
	});
	
	//Evento onchage del estado
	$("#select9").change(function () {
			$("#fechaEstado").val("");
			$("#cmbCausalInactivacion").val(0);
			var filtro = $("#select9").val();
			if(filtro=='I'){
				$("#rowInactivo").css('display','');
			} else {
				$("#rowInactivo").css('display','none');
			}
	});
});
</script>

<!-- colaboracion en linea  -->

<script type="text/javascript">
	$(function() {
		$("#dialog-form2").dialog("destroy");
		/*var notas = $("#notas"),
			allFields = $([]).add(notas);*/
		
		$("#dialog-form2").dialog({
			autoOpen: false,
			height: 400,
			width: 500,
			modal: true,
			buttons: {
				'Enviar': function() {
					var bValid = true;
					var campo=$('#notas').val();
					var campo0=$.trim(campo);
					if (campo0==""){
						$(this).dialog('close');
						return false;
						}
					var campo1=$('#usuario').val();
					var campo2="empresas.php";
					$.post('../../phpComunes/colaboracion.php',{v0:campo0,v1:campo1,v2:campo2}, function(datos){
						if(datos=='1'){
							alert("Su comentario fue enviado correctamente!. Gracias por participar en el mejoramiento de SIGAS");
						}else{
							alert(datos);
						}
					});
				$(this).dialog('close');
				},
				Cancelar: function() {
					$(this).dialog('close');
					$("#dialog-form2").dialog("destroy");
				}
			}
		});
		$('#enviar-notas')
			.button()
			.click(function() {
				$('#dialog-form2').dialog('open');
			});
	});
	</script>
</head>
<body>
<form name="forma">
<center>
<table width="90%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="13" height="29" class="arriba_iz">&nbsp;</td>
<td class="arriba_ce"><span class="letrablanca">::&nbsp;Actualizar Datos de Empresa&nbsp;::</span></td>
<td width="13" class="arriba_de" align="right">&nbsp;</td>
</tr>
<tr>
<td class="cuerpo_iz">&nbsp;</td>
<td class="cuerpo_ce">
	<img src="../../imagenes/spacer.gif" width="1" height="1"/>
	<img src="../../imagenes/spacer.gif" width="3" height="1"/> 
	<img src="../../imagenes/menu/grabar.png" width="16" height="16" style="cursor: pointer" title="Guardar" onclick="validarCampos(2);" /> 
	<img src="../../imagenes/spacer.gif" width="17" height="1"/> 
	<img src="../../imagenes/menu/imprimir.png" width="16" height="16" style="cursor: pointer" title="Imprimir" onclick="window.print()"/> 
	<img src="../../imagenes/spacer.gif" width="16" height="1"/> 
	<img src="../../imagenes/menu/refrescar.png" width="16" height="16" style="cursor: pointer" title="Limpiar Campos" onclick="limpiar(); document.forms[0].elements[0].focus();"/> 
	<img src="../../imagenes/spacer.gif" width="6" height="1"/> 
	<img src="../../imagenes/menu/informacion.png" width="16" height="16" style="border: none; cursor: pointer" title="Manual" onclick="mostrarAyuda();" /> 
	<img src="../../imagenes/spacer.gif" width="3" height="1"/> 
	<img src="../../imagenes/menu/notas.png" width="16" height="16" style="cursor: pointer" title="Colaboración en línea" onclick="notas();" /> 
	<br>
	<font size=1 face="arial">Guargar&nbsp;Imprimir&nbsp;Limpiar&nbsp;Info&nbsp;Ayuda</font>
</td>
<td class="cuerpo_de">&nbsp;</td>
</tr>
<tr>
<td class="cuerpo_iz">&nbsp;</td>
<td align="left" class="cuerpo_ce"><div id="error" style="color: #FF0000"></div></td>
<td class="cuerpo_de">&nbsp;</td>
</tr>
<tr>
<td class="cuerpo_iz">&nbsp;</td>
<td class="cuerpo_ce">
	<table width="100%" border="0" cellspacing="0" class="tablero">
	<tr>
	<td width="17%">NIT a buscar</td>
	<td width="29%"><input name="txtNit" class="box1" id="txtNit" onblur="buscarSucursalesListar(this),buscarSucursalesCombo(this);" onkeypress="tabular(this,event);" /></td>
	          <td width="17%">Sucursal</td>
	<td width="37%">
    <select name="comboSucursal" class="box1" id="comboSucursal" >
	</select></td>
	</tr>
	<tr>
	  <td>&nbsp;</td>
	  <td colspan="3"><div id="div-sucursales" class="big"></div> &nbsp;</td>
	  </tr>
	<tr>
	
  <td width="19%">Sector</td>
    <td>
	  <select name="select12" class="box1" id="select12">
	    <option value="0" selected="selected">Seleccione...</option>
	    <option value="93" >OFICIAL</option>
	    <option value="94" >PRIVADO</option>
	    <option value="95" >MIXTA</option>
	    </select><img src="../../imagenes/menu/obligado.png" alt=""	width="12" height="12" />
    </td>
    <td width="29%">Clase de Sociedad</td>
	<td><select name="select13" class="box1" id="select13">
	    <option value="0" selected="selected">Seleccione...</option>
	    <option value="101">PERSONA NATURAL</option>
	    <option value="102">PERSONA JURIDICA</option>        
	    </select><img src="../../imagenes/menu/obligado.png" alt=""	width="12" height="12" /></td>
     </tr>
    <!-- <tr>   
    <td width="16%">Beneficio Tributario </td>
	<td width="16%"><select name="select2" class="box1" id="select2">
	    <option value="0" selected="selected">Ninguno</option>
	    <option value="2878">PIME</option>
	    <option value="2879">PRIMER EMPLEO</option>
	    </select>
   </td>
        
	</tr>-->
	<tr>
	<td><label for="iden">NIT</label></td>
	<td><label> <input name="tNit" type="text" class="box1" id="tNit" onblur="digito(this);" onchange="digito(this)" value="" onkeydown="solonumeros(this);" onkeyup="solonumeros(this);" readonly/> -</label>					D&iacute;gito <label for="textfield3"></label> <input name="tDigito"
	type="text" class="boxcorto" id="tDigito" readonly="readonly" /> <label><img
	src="../../imagenes/menu/obligado.png" width="12" height="12" /></label></td>
	<td>Tipo Documento</td>
	<td><select name="select" id="select" class="box1">
	<option value="0" selected="selected">Seleccione...</option>
    <?php
	$consulta=$objClase->mostrar_datos(1, 4);
	while($row=mssql_fetch_array($consulta)){
		if($row["iddetalledef"] == 5 ||  $row["iddetalledef"] == 1 ||  $row["iddetalledef"] == 4)
			echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
	}
	?>
    </select><img src="../../imagenes/menu/obligado.png" width="12"	height="12" /></td>
	</tr>
	<tr>
	<td>Raz&oacute;n Social</td>
	<td colspan="2"><input name="textfield" type="text" class="boxlargo" id="textfield" onkeyup="copiar(this,$('#textfield2'));alfanumerico(this);"> 
    <img src="../../imagenes/menu/obligado.png" width="12" height="12" /></td>
	<td>C&oacute;digo Sucursal - 000</td>
	</tr>
	<tr>
	<td>Nombre Comercial</td>
	<td colspan="3"><input name="textfield2" type="text" class="boxlargo" id="textfield2" onkeypress="tabular (this, event);"/>
    <img src="../../imagenes/menu/obligado.png" width="12" height="12" /></td>
	</tr>
	<tr>
	<td>Departamento</td>
	<td><select name="cboDepto2" class="box1" id="cboDepto2">
	<option value="0" selected="selected">Seleccione</option>
	<?php
	$consulta=$objCiudad->departamentos();
	while($row=mssql_fetch_array($consulta)){        
		echo "<option value=".$row["coddepartamento"].">".$row["departmento"]."</option>";
	}
	?>
	</select><img src="../../imagenes/menu/obligado.png" width="12" height="12" /></td>
    <td>Ciudad</td>
	<td><select name="cboCiudad2" class="box1" id="cboCiudad2" onchange="DuplicarCiudadCorresp();">
	</select><img src="../../imagenes/menu/obligado.png" width="12" height="12" /></td>
    </tr>
    <tr>
  		<td>Zona</td>
  		<td>
  			<select name="cboZona" class="box1" id="cboZonaCA">
  				<option value="0">Seleccione..</option>
  			</select>
  			<img src="../../imagenes/menu/obligado.png" width="12" height="12" /></td>
  		<td>Barrio</td>
  		<td>
  			<select name="cboBarrio" class="box1" id="cboBarrioCA" onchange="DuplicarBarrioCorresp();">
  				<option value="0">Seleccione..</option>
  			</select>
  			<img src="../../imagenes/menu/obligado.png" width="12" height="12" />
  		</td>
	</tr>
    <tr>
    <tr>
	<td>Departamento De Correspondencia</td>
	<td><select name="cboDeptoCorresp" class="box1" id="cboDeptoCorresp">
	<option value="0" selected="selected">Seleccione</option>
	<?php
	$consulta=$objCiudad->departamentos();
	while($row=mssql_fetch_array($consulta)){        
		echo "<option value=".$row["coddepartamento"].">".$row["departmento"]."</option>";
	}
	?>
	</select><img src="../../imagenes/menu/obligado.png" width="12" height="12" /></td>
    <td>Ciudad De Correspondencia</td>
	<td><select name="cboCiudadCorresp" class="box1" id="cboCiudadCorresp">
	</select><img src="../../imagenes/menu/obligado.png" width="12" height="12" /></td>
    </tr>
    <tr>
    	<td>Zona De Correspondencia</td>
    	<td>
    		<select name="cboZonaCorresp" class="box1" id="cboZonaCorresp"></select>
    		<img src="../../imagenes/menu/obligado.png" width="12" height="12" />
    	</td>
    	<td>Barrio De Correspondencia</td>
    	<td>
    		<select name="cboBarrioCorresp" class="box1" id="cboBarrioCorresp"></select>
    		<img src="../../imagenes/menu/obligado.png" width="12" height="12" />
    	</td>	
    </tr>
	<tr>
	<td>Direcci&oacute;n</td>
	<td colspan="3"><input name="tDireccion" type="text" class="boxlargo" id="tDireccion" onfocus="direccion(this,document.getElementById('textfield8'));"  /> 
    <img src="../../imagenes/menu/obligado.png" width="12" height="12" /></td>
	</tr>
	<tr>
	<td>Dir. Correspondencia</td>
	<td colspan="3"><input name="tDirCorresp" type="text" class="boxlargo" id="tDirCorresp" onfocus="direccion(this,document.getElementById('textfield8'));"  /> 
    <img src="../../imagenes/menu/obligado.png" width="12" height="12" /></td>
	</tr>
	<tr>
	<td>Tel&eacute;fono - Celular</td>
	<td>
		<input name="textfield8" type="text" class="box1" id="textfield8" onkeydown="solonumeros(this);" onkeyup="solonumeros(this);"/>
    	<img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
    	<input name="txtCelular" type="text" class="box1" id="txtCelular" onkeydown='solonumeros(this);' onkeyup='solonumeros(this);'/>
    </td>
    
	<td>Fax</td>
	<td><input name="textfield6" type="text" class="box1" id="textfield6" onkeydown="solonumeros(this);" onkeyup="solonumeros(this);"/></td>
	</tr>
  <tr>
    <td>Tipo Tel&eacute;fono</td>
    <td>
      <select name="tipoTel" class="box1" id="tipoTel">
      <option value="0" selected="selected">Seleccione</option>
      <?php
      foreach ($tipos_tel as $key => $value) {
        echo "<option value=".$value["ID_detalles"].">".$value["Descripcion1_detalles"]."</option>";
      }
      ?>
      </select> <img src="../../imagenes/menu/obligado.png" width="12" height="12" />
    </td>
    <td></td>
    <td></td>
  </tr>
  <tr>
	<td>URL</td>
	<td><input name="textfield9" type="text" class="box1" id="textfield9" /></td>
	<td>Email</td>
	<td><input name="textfield15" type="text" class="box1" id="textfield15" onblur="soloemail(this);"/></td>
	</tr>
	<tr>
	<td>C.C. Representante Legal</td>
	<td>
    <input name="textfield4" type="text" class="box1" id="textfield4" onblur="buscarPersona(this,0);" onkeypress="tabular(this,event);" onkeyup="copiar(this,$('#textfield5'))"/>
	<img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
    </td>
	<td colspan="2" id="nombre1"></td>
	</tr>
	<tr>
	  <td>C.C. Contacto Administrativo</td>
	  <td><label for="textfield5"></label> <input name="textfield5" type="text" class="box1" id="textfield5" onblur="buscarPersona(this,1);" onkeypress="tabular(this,event);" /></td>
	  <td colspan="2" id="nombre2">	</td>
	  </tr>
	<tr >
	<td>Colegio</td>
        <td><select name="select5" class="box1" id="select5">
        <option value="0" selected="selected">Seleccione</option>
        <option value="N">NO</option>
        <option value="S">SI</option>
        </select> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
        </td>
        <td>Excento</td>
        <td><select name="select6" class="box1" id="select6">
        <option value="0" selected="selected">Seleccione</option>
        <option value="N">NO</option>
        <option value="S">SI</option>
        </select> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
        </td>
	  </tr>
     <tr>
     	<td>Actividad Economica</td>
	 	<td><select name="select7" class="boxmediano" id="select7">
	    <option value="0" selected="selected">Seleccione...</option>
	    <?php
	    //$sql = "select distinct idciiu,seccion,descripcion FROM aportes079 where division is null and grupo ='' order by idciiu";
	    $sql = "select distinct idciiu,seccion,descripcion FROM aportes079 where division ='' and grupo ='' order by idciiu";
	    $rs = $db->querySimple($sql);
		while($row = $rs->fetch())
			echo "<option value=".$row['idciiu'].">".utf8_decode($row['descripcion'])."</option>";
		?>
	    </select>
	    <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
	  <td>Contratista</td>
	   <td><select name="select4" class="box1" id="select4">
	    <option value="0" selected="selected">Seleccione</option>
	    <option value="N">NO</option>
	    <option value="S">SI</option>
	    </select> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
     </tr>
	<tr>
	  <td>C&oacute;digo CIIU</td>
	  <td><input type="text" name="codigoCIU" id="codigoCIU" class="box1" onblur="buscarCIU();" /><img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
	  <td colspan="2" ><select name="lisCodigo" id="lisCodigo" class="boxmediano" onchange="codigoCiu()">
      <option value="0" selected="selected">Seleccione</option>
      <?php 
      $sql="select distinct * FROM aportes079 where clase != '' and len(clase)>=4 order by idciiu";
      $rs=$db->querySimple($sql);
	  while($row = $rs->fetch())
		  echo "<option value=".$row['clase'].">".utf8_decode($row['descripcion'])."</option>";
	  ?>
      </select>
      </td>
	  </tr>
	<tr>
	<td>Seccional</td>
	<td><select name="select10" class="box1" id="select10">
		<option value="0" selected="selected">Seleccione</option>
		<?php foreach ($arregloAgencias as $pos => $nombre)
		{
			echo "<option value=".$pos.">$nombre</option>";
		}?>
		</select> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
	<td>&Iacute;ndice Aporte</td>
	<td><select name="select11" class="box1" id="select11">
	  <option value="0" selected="selected">Seleccione...</option>
	  <?php
	$consulta=$objClase->mostrar_datos(18, 4);
	while($row=mssql_fetch_array($consulta)){
		echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
	}
	?>
	  </select>	  <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
	</tr>
	<tr>
	<td>Fecha Afiliaci&oacute;n</td>
	<td><input name="textfield18" type="text" class="box1" id="textfield18" value="" readonly="readonly" />
	  <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
	<td>Fecha inicio aportes</td>
	<td><input name="datepicker" type="text" class="box1" id="datepicker" readonly="readonly" value="" />
	  <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
	</tr>
	<tr>
	<td>Asesor</td>
	<td><select name="select8" class="box1" id="select8">
	  <option value="0" selected="selected">Seleccione</option>
	  <option value="1">Presentaci&oacute;n personal</option>
	  </select>
	  <!-- <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />--></td> 
	<td>
    <!-- colegio-->
    <select name="select5" class="box1" id="select5" style="display:none" >
	  <option value="0" >Seleccione</option>
	  <option value="N" selected="selected">NO</option>
	  <option value="S">SI</option>
	  </select>
      <!-- excento-->
	  <select name="select6" class="box1" id="select6" style="display:none" >
	    <option value="0" >Seleccione</option>
	    <option value="N" selected="selected">NO</option>
	    <option value="S">SI</option>
	    </select>
	  Estado</td>
	<td><select name="select9" class="box1" id="select9">
	  <option value="0" selected="selected">Seleccione</option>
	  <option value="A">Activo</option>
	  <option value="I">Inactivo</option>
	  </select>
	  <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
	</tr>	
	<tr id="rowInactivo" style="display:none">
    	<td>Causal Inactivaci&oacute;n</td>
    	<td>
    		<select name="cmbCausalInactivacion" id="cmbCausalInactivacion" class="boxmediano">
    			<option value="0" selected="selected">Seleccione...</option>
    			<?php
					$rsCausalInact=$objClase->mostrar_datos(65);
					while( ( $rowCausalInact=mssql_fetch_array($rsCausalInact) ) == true){
						echo "<option value=".$rowCausalInact['iddetalledef'].">".$rowCausalInact['detalledefinicion']."</option>";
					}
				?>
    		</select>
    		<img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
    	</td>
    	<td>Fecha estado</td>
   		<td>
   			<input type="text" id="fechaEstado" name="fechaEstado" class="box1" readonly="readonly" />
   			<img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
   		</td>
    </tr>
    <tr>
	<td>usuario</td>
	<td><input name="textfield19" type="text" class="box1" id="textfield19" readonly="readonly" /></td>
	<td>Fecha Constituci&oacute;n</td>
	<td><input name="fechaMatricula" type="text" class="box1" id="fechaMatricula" value="" /></td>
	</tr>
	<tr>
	  <td>Clase Aportante</td>
	  <td><select name="cmbClaseApo" class="box1" id="cmbClaseApo">
	    <option value="0" selected="selected">Seleccione...</option>
	    <?php
	$consulta=$objClase->mostrar_datos(33, 3);
	while($row=mssql_fetch_array($consulta)){
		echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
	}
	?>
	    </select><img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
	  <td>Tipo Aportante</td>
	  <td><select name="cmbTipoApo" class="box1" id="cmbTipoApo">
	    <option value="0" selected="selected">Seleccione...</option>
	    <?php
	$consulta=$objClase->mostrar_datos(34, 3);
	while($row=mssql_fetch_array($consulta)){
		echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
	}
	?>
	    </select><img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
	  </tr>    
	<tr>
	  <td>Histórico de Observaciones</td>
	  <td colspan="3"><div id="div-observaciones"></div>&nbsp;</td>
	 </tr>
	</table>
	<table width="100%" border="0" cellspacing="0">
	<tr>
	<td align="left">&nbsp;</td>
	</tr>
	<tr>
	<td align="left" class="Rojo"><img	src="../../imagenes/menu/obligado.png" width="16" height="12" align="left" />Campo Obligado</td>
	</tr>
	</table>
</td>
<td class="cuerpo_de"></td>
</tr>
<tr>
<td class="abajo_iz" >&nbsp;</td>
<td class="abajo_ce" ></td>
<td class="abajo_de" >&nbsp;</td>
</tr>
</table>
</center>
<input type="hidden" name="Idempresa" id="Idempresa" /> 
<input type="hidden" name="tIdRadicacion" id="tIdRadicacion" /> 
<input type="hidden" name="tNit2" id="tNit2" /> 
<input type="hidden" name="tFechaRadicacion" id="tFechaRadicacion" /> 
<input type="hidden" value="<?php echo $_SESSION["USUARIO"]; ?>" name="usuario" id="usuario" />

<!-- formulario direcciones  --> 
<div id="dialog-form" title="Formulario de direcciones"></div>
<!-- formulario para grabar persona -->
<div id="dialog-persona" title="Formulario Datos Basicos"></div>
<!-- colaboracion en linea -->

<!-- FORMULARIO OBSERVACIONES-->
<div name="div-observaciones-tab" style="display:none" title="Observaciones de modificaci&oacute;n">
<table class="tablero">
 <tr>
   <td>Usuario</td>
   <td colspan="3" >
   <input type="text" name="usuarioObs" id="usuarioObs" class="box1" disabled="disabled" value="<?php echo $_SESSION['USUARIO']?>" /></td>
   </tr>
 <tr>
   <td>Observaciones</td>
   <td colspan="3" >
   <textarea name="observacionGrupo" id="observacionGrupo" cols="45" rows="5" class="boxlargo"></textarea></td>
   </tr>
</table>
<div class="ui-state-highlight ui-corner-all" style="margin: 9px auto; display:none;padding: 6px; text-align: center; border: 1px solid rgb(255, 255, 0); width:300px" id="rtaObservacion"><span class="ui-icon ui-icon-info" style="float: left; margin-right: 0.3em;"></span>Observaci&oacute;n guardada con &eacute;xito.</div>
</div>

<!-- ayuda en linea -->
<div id="ayuda" title="Manual .:. Empresa Nueva" style="background-image:url('../../imagenes/FondoGeneral0.png')"></div>
<!-- fin ayuda en linea -->
<input type="hidden" name="idRepresemtante" id="idRepresemtante" />
<input type="hidden" name="idContacto" id="idContacto" />
<input type="hidden" name="seccion" id="seccion" />
</form>
</body>
<script language="javascript">
function mostrarAyuda(){
	$("#ayuda").dialog('open' );
	}

function notas(){
	$("#dialog-form2").dialog('open');
	}	
</script>
</html>
