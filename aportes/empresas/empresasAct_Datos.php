<?php
/* autor:       orlando puentes
 * fecha:       17/06/2010
 * objetivo:    
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$usuario=$_SESSION['USUARIO'];
$idpersona= $_REQUEST['v0'];
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
include_once 'clases' . DIRECTORY_SEPARATOR . 'empresas.class.php';

$campo0 = empty($_REQUEST['v0']) ? 'NULL' : $_REQUEST['v0']; //idsector
$campo1 = empty($_REQUEST['v1']) ? 'NULL' : $_REQUEST['v1'];//idclasesociedad
$campo2 = empty($_REQUEST['v2']) ? 'NULL' : "'".trim($_REQUEST['v2'])."'";//nit
$campo3 = empty($_REQUEST['v3']) ? 'NULL' : "'".trim($_REQUEST['v3'])."'";//digito
$campo4 = empty($_REQUEST['v4']) ? 'NULL' : $_REQUEST['v4'];//idtipodocumento
$campo5 = empty($_REQUEST['v5']) ? 'NULL' : "'".strtoupper(trim(str_replace(38,'&',$_REQUEST['v5'])))."'";//razonsocial
$campo6 = empty($_REQUEST['v6']) ? 'NULL' : "'".strtoupper(trim(str_replace(38,'&',$_REQUEST['v6'])))."'";//sigla
$campo7 = empty($_REQUEST['v7']) ? 'NULL' : $_REQUEST['v7'];//iddepartamento
$campo8 = empty($_REQUEST['v8']) ? 'NULL' : $_REQUEST['v8'];//idciudad
$campo9 = empty($_REQUEST['v9']) ? 'NULL' : "'".trim($_REQUEST['v9'])."'";//direccion
$campo10 = empty($_REQUEST['v10']) ? 'NULL' : "'".trim($_REQUEST['v10'])."'";//telefono
$campo11 = empty($_REQUEST['v11']) ? 'NULL' : "'".trim($_REQUEST['v11'])."'";//fax
$campo12 = empty($_REQUEST['v12']) ? 'NULL' : "'".trim($_REQUEST['v12'])."'";//url
$campo13 = empty($_REQUEST['v13']) ? 'NULL' : "'".trim($_REQUEST['v13'])."'";//email
$campo14 = empty($_REQUEST['v14']) ? 'NULL' : "'".trim($_REQUEST['v14'])."'";//idrepresentante
$campo15 = empty($_REQUEST['v15']) ? 'NULL' : "'".trim($_REQUEST['v15'])."'";//idjefepersonal
$campo16 = empty($_REQUEST['v16']) ? 'NULL' : "'".$_REQUEST['v16']."'";//contratista
$campo17 = empty($_REQUEST['v17']) ? 'NULL' : $_REQUEST['v17'];//idcodigoactividad
$campo18 = empty($_REQUEST['v18']) ? 'NULL' : "'".$_REQUEST['v18']."'";//colegio
$campo19 = empty($_REQUEST['v19']) ? 'NULL' : "'".$_REQUEST['v19']."'";//exento
$campo20 = empty($_REQUEST['v20']) ? 'NULL' : $_REQUEST['v20'];//actieconomicadane
$campo21 = empty($_REQUEST['v21']) ? 'NULL' : $_REQUEST['v21'];//indicador
$campo22 = empty($_REQUEST['v22']) ? 'NULL' : "'".$_REQUEST['v22']."'";//seccional
$campo23 = empty($_REQUEST['v23']) ? 'NULL' : "'".trim($_REQUEST['v23'])."'";//fechaafiliacion
$campo24 = empty($_REQUEST['v24']) ? 'NULL' : "'".trim($_REQUEST['v24'])."'";//fechaaportes
$campo25 = empty($_REQUEST['v25']) ? 'NULL' : $_REQUEST['v25'];//idasesor
$campo26 = empty($_REQUEST['v26']) ? 'NULL' : "'".$_REQUEST['v26']."'";//estado
$campo27 = empty($_REQUEST['v27']) ? 'NULL' : "'".$usuario."'";//usuario
$campo28 = empty($_REQUEST['v28']) ? 'NULL' : trim($_REQUEST['v28']);//idempresa
$campo29 = empty($_REQUEST['v29']) ? 'NULL' : "'".$_REQUEST['v29']."'"; //fechamatricula
$campo30 = empty($_REQUEST['v30']) ? 'NULL' : "'".$_REQUEST['v30']."'"; //fecha estado
$campo31 = empty($_REQUEST['v31']) ? 'NULL' : "'".$_REQUEST['v31']."'"; //clase de aportante
$campo32 = empty($_REQUEST['v32']) ? 'NULL' : "'".$_REQUEST['v32']."'"; //tipo de aportante
$campo33 = empty($_REQUEST['v33']) ? 'NULL' : "'".$_REQUEST['v33']."'"; //causal de inactivacion
$campo34 = empty($_REQUEST['v34']) ? 'NULL' : "'".trim($_REQUEST['v34'])."'";//direccion correspondencia
$campo35 = empty($_REQUEST['v35']) ? 'NULL' : $_REQUEST['v35'];//iddepartamento correspondencia
$campo36 = empty($_REQUEST['v36']) ? 'NULL' : $_REQUEST['v36'];//idciudad correspondencia
$campo37 = empty($_REQUEST['v37']) ? 'NULL' : $_REQUEST['v37'];//idbarrio correspondencia
$campo38 = empty($_REQUEST['v38']) ? 'NULL' : $_REQUEST['v38'];//idbarrio 
$campo39 = empty($_REQUEST['v39']) ? 'NULL' : $_REQUEST['v39'];//celular empresa
$campo40 = empty($_REQUEST['v40']) ? 'NULL' : $_REQUEST['v40'];//d tipo telefono

$sql="Update aportes048 set idsector=$campo0, idclasesociedad=$campo1, nit=$campo2, nitrsn=$campo2, digito=$campo3, idtipodocumento=$campo4,  razonsocial=$campo5, sigla=$campo6, iddepartamento='".$campo7."', idciudad='".$campo8."',direccion=$campo9, telefono=$campo10, fax=$campo11, url=$campo12, email=$campo13, idrepresentante=$campo14, idjefepersonal=$campo15, contratista=$campo16, idcodigoactividad=$campo17, colegio=$campo18, exento=$campo19,  actieconomicadane='".$campo20."', indicador=$campo21, seccional=$campo22, fechaafiliacion=$campo23, fechaaportes=$campo24, idasesor=$campo25, estado=$campo26, usuario=$campo27,fechamatricula=$campo29, fechaestado=$campo30, claseaportante=$campo31,tipoaportante=$campo32,codigoestado=$campo33,direcorresp=$campo34,iddepcorresp='".$campo35."',idciucorresp='".$campo36."', idbarriocorresp='".$campo37."', idbarrio='".$campo38."', celular='".$campo39."', id_tipo_tel='".$campo40."'  where idempresa=$campo28";
$rs=$db->queryActualiza($sql);
if($rs>0){
		echo 1;
}
else{
		echo 0;
	}

function cFecha($fecha){
	if(strlen($fecha)<10){
		return '';
		exit();
	}
	$a=substr($fecha,0,4);
	$m=substr($fecha,5,2);
	$d=substr($fecha,8,2);
	$f=$m."/".$d."/".$a;
	return $f;
	}
?>