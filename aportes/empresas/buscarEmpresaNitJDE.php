<?php
//ini_set('display_errors','1');
error_reporting(E_ALL);
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
include '../../terceros/Terceros.php';

$retorno = array("exito"=>1,"mensaje"=>"","AN8Tercero"=>"");

$tipoDocumento = $_REQUEST['tipoDocumento'];//Tipo Documento
$nit = $_REQUEST['nit'];//NIT
$digitoVerificacion = $_REQUEST['digitoVerificacion'];//Digito Verificación
//$tipoDocumento = $_REQUEST['tipoDocumento'];//tipo documento

//$tipoDocumento = 1;
//$nit = 900548108;
//$digitoVerificacion = 4;

//$nit = "900349154";
//$nit = "900349159";
//$digitoVerificacion = "1";


$nit = number_format($nit, 0, '', '.');//Formateo el número de documento, agrego separadores de mil
if($tipoDocumento == 5){//NIT
    $nit .= "-" . $digitoVerificacion;
    $entityTaxID = str_pad($nit, 16, " ", STR_PAD_LEFT);
}else{
    $entityTaxID = $nit;
}

$arraytercero = new TerceroProcesar();
$arraytercero->setActionType("I");
$entity = new Entity();
$entity->setEntityTaxID($entityTaxID);
$arraytercero->setEntity($entity);
$tercero=new Terceros($arraytercero);
$response=$tercero->procesar();

//var_dump($response);

if ($response->getError()===null){   
    $retorno["AN8Tercero"] = $response->getEntityAN8();
}else{
    $retorno["mensaje"] = utf8_encode("El tercero con número de identificación ".$nit." no existe en JDE");
}
echo json_encode($retorno);