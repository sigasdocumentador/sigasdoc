<?php
date_default_timezone_set('America/Bogota');
set_time_limit(0);
//ini_set("display_errors",'1');
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Interrupciones</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link type="text/css" rel="stylesheet" href="../../css/css.1.8/jquery-ui-1.8.17.custom.css" />
<link type="text/css" href="../../css/Estilos.css" rel="stylesheet"/>
<link href="../../css/marco.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="../../js/1.6/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="../../js/1.6/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="../../js/jquery.alphanumeric.js"></script>
<script type="text/javascript" src="../../js/jquery.periodos.js"></script>
<script type="text/javascript" src="../../js/effects.Jquery.js"></script>
<script type="text/javascript" src="../../js/comunes.js"></script>
<script type="text/javascript" src="js/interrupciones.js"></script>

<script language="javascript">
function descargar(){
	var URL=src();
	var url=URL+'phpComunes/descargaPlanos.php';
	window.open(url);
	}
</script>
</head>
<body>
<form name="forma1">
<center>
<br /><br />
<table width="60%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="13" height="29" class="arriba_iz">&nbsp;</td>
<td class="arriba_ce"><span class="letrablanca">::&nbsp;Interrupciones&nbsp;::</span></td>
<td width="13" class="arriba_de" align="right">&nbsp;</td>
</tr>
<tr>
<td class="cuerpo_iz">&nbsp;</td>
<td class="cuerpo_ce">
	<img src="../../imagenes/tabla/spacer.gif" width="1" height="1"/> 
	<img src="../../imagenes/spacer.gif" width="5" height="1"/>
	<img src="../../imagenes/menu/grabar.png" width="16" height="16" style="cursor:pointer" title="Guardar" onclick="guardarInterrupcion();" id="bGuardar"/>
	<img src="../../imagenes/spacer.gif" width="1" height="1"/> 
	<img src="../../imagenes/menu/refrescar.png" width="16" height="16" style="cursor: pointer" title="Limpiar Campos" onclick="limpiar(); document.forms[0].elements[0].focus();"/> 
	<img src="../../imagenes/spacer.gif" width="1" height="1"/> 
	<img src="../../imagenes/menu/buscar.png" title="Buscar" width="16" height="16" style="cursor: pointer" onclick="buscarEmpresas()"/> 
	<img src="../../imagenes/spacer.gif" width="3" height="1"/> 
	<img src="../../imagenes/menu/informacion.png" width="16" height="16" style="border: none; cursor: pointer" title="Manual" onclick="mostrarAyuda();" /> 
	<img src="../../imagenes/spacer.gif" width="2" height="1"/>
	<img src="../../imagenes/menu/notas.png" width="16" height="16" style="cursor: pointer" title="Colaborac�on en l�nea" onclick="notas();" />
	<br></td>
<td class="cuerpo_de">&nbsp;</td>
</tr>
<tr>
<td class="cuerpo_iz">&nbsp;</td>
<td align="left" class="cuerpo_ce">
<div id="error" style="color: #FF0000"></div></td>
<td class="cuerpo_de">&nbsp;</td>
</tr>
<tr>
<td class="cuerpo_iz">&nbsp;</td>
 <td align="center" class="cuerpo_ce">
	<table width="100%" border="0" cellspacing="0" class="tablero">
	<tr>
	<td width="17%">NIT Empresa</td>
	<td width="29%"><input name="txtId" class="box1" id="txtId"	onkeydown="solonumeros(this);" onkeyup="solonumeros(this);" onblur="validarLongNumIdent(5,this);buscarEmpresa(this);"/>
	  <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
	</tr>
	<tr>
	<td width="17%">Raz&oacute; Social</td>
	<td width="37%" id="txtId2"></td>
	</tr>
    <tr>
	  <td colspan="4"><label class="negrita">Informaci&oacute;n de Peri&oacute;dos</label></td>
	  </tr>
	<tr>
	  <td>
	   Periodos:       
          &nbsp;<img src="../../imagenes/menu/calendario.png" alt="Elegir periodo" width="16" height="16" align="absmiddle" id="periodos" />
          <input type="hidden" id="array" />
      </td>
      <td colspan="3">
          <div id="etiquetas" style="width:600px; height:auto;float:left"></div>
	  </td>
	  </tr>
	<tr>
	  <td>Observaciones</td>
	  <td><textarea  id="areaObserv" class="boxlargo"></textarea></td>
	</tr>
	</table>
 </td>
  <td class="cuerpo_de">&nbsp;</td>
</tr>
<tr>
<td class="abajo_iz" >&nbsp;</td>
<td class="abajo_ce" ></td>
<td class="abajo_de" >&nbsp;</td>
</tr>
</table>
</center>
</form>
<!-- ayuda en linea -->
<div id="ayuda" title="Manual .:. Actualizar NIT" style="background-image:url('../../imagenes/FondoGeneral0.png')"></div>
<!-- fin ayuda en linea -->

<!-- colaboracion en linea -->
<!-- FORMULARIO OBSERVACIONES-->
<div name="div-observaciones-tab" style="display:none" title="Observaciones de modificaci&oacute;n">
<table class="tablero">
 <tr>
   <td>Usuario</td>
   <td colspan="3" >
   <input name="usuarioObs" id="usuarioObs" class="box1" disabled="disabled" value="<?php echo $_SESSION['USUARIO']?>" /></td>
   </tr>
 <tr>
   <td>Observaciones</td>
   <td colspan="3" >
   <textarea name="observacionGrupo" id="observacionGrupo" cols="45" rows="5" class="boxlargo"></textarea></td>
   </tr>
</table>
<div class="ui-state-highlight ui-corner-all" style="margin: 9px auto; display:none;padding: 6px; text-align: center; border: 1px solid rgb(255, 255, 0); width:300px" id="rtaObservacion"><span class="ui-icon ui-icon-info" style="float: left; margin-right: 0.3em;"></span>Observaci&oacute;n guardada con &eacute;xito.</div>
</div>
<!-- fin colaboracion -->
</body>

</html>