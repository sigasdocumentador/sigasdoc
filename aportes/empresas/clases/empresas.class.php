<?php
 
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'conexion.class.php';
//include_once $raiz.DIRECTORY_SEPARATOR.'phpComunes'.DIRECTORY_SEPARATOR.'wssdk'.DIRECTORY_SEPARATOR.'ClientWSInfWeb.php';

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'utilitario.class.php';

//$cliente = new ClientWSInfWeb('c523d9f153587fbe3c96fee41c26b471', '711b60b69a49aa33bfa888930b2ecf10');

class Empresa{

	/************************************
	 *  ATRIBUTOS ENTIDADES			    *
	***********************************/
	private static $conPDO = null;

	private $con;
	private $con2;
	private $fechaSistema;

	function __construct(){
 		$this->con=new DBManager;
		$this->con2=new DBManager;

		try{
			self::$conPDO = IFXDbManejador::conectarDB();
			if( self::$conPDO->conexionID==null ){
				throw new Exception(self::$conPDO->error);
			}
		}catch(Exception $e){
			echo $e->getMessage();
			exit();
		}
 	}

 	function inicioTransaccion(){
 		self::$conPDO->inicioTransaccion();
 	}
 	function cancelarTransaccion(){
 		self::$conPDO->cancelarTransaccion();
 	}
 	function confirmarTransaccion(){
 		self::$conPDO->confirmarTransaccion();
 	}

	function buscar_nit_informa($nit){
		if($this->con2->conectarInforma()==true){
			$sql="Select count(*) as cuenta from nits where nit='$nit'";
			//echo $sql;
			return mssql_query($sql,$this->con2->conect);
		}
	}

/*	function insertar_informa($campos){
		if($this->con2->conectarInforma()==true){
			//$fechaSistema=date("m/d/Y");
			$sql="INSERT INTO nits (nit, clase, nombre, direccion, telefono, ciudad, autoret, contacto, tipoclie, aaereo, fax, depto, pais, estado, resp_iva, t_contrib, prefijo) VALUES ('".$campos[0]."','N','".$campos[1]."','".$campos[2]."','".$campos[3]."','".$campos[4]."','N','".$campos[5]."','N','".$campos[6]."','".$campos[7]."','".$campos[8]."','169','A','S','N','N')";
			//echo $sql;
			return mssql_query($sql,$this->con2->conect);
		}
	}
*/

function insertar_nueva($campos){
	if($this->con->conectar()==true){
		$a=substr($campos[24],6,4);
		$m=substr($campos[24],0,2);
		$d=substr($campos[24],3,2);
		$ruta="digitalizacion\\empresa\\$a\\$m\\$d\\$campos[4]\\";
		$sql="INSERT INTO aportes048 (idtipodocumento, nit, digito, nitrsn, codigosucursal, principal, razonsocial, sigla, direccion, iddepartamento, idciudad, idzona, telefono, fax, url, email, idrepresentante, idjefepersonal, contratista, colegio, exento, idcodigoactividad, actieconomicadane, indicador, idasesor, fechamatricula, idsector, seccional, tipopersona, claseaportante, tipoaportante, estado, fechaaportes, fechaafiliacion, usuario, fechasistema, idclasesociedad, rutadocumentos, legalizada) VALUES('".$campos[6]."','".$campos[4]."','".$campos[5]."','".$campos[4]."','000','S','".$campos[7]."','".$campos[8]."','".$campos[11]."','".$campos[9]."','".$campos[10]."',null,'".$campos[12]."','".$campos[13]."','".$campos[14]."','".$campos[15]."',".$campos[16]."','".$campos[17]."','".$campos[18]."','N','N','".$campos[19]."',".$campos[27].",'".$campos[20]."','".$campos[21]."',null,'".$campos[2]."','".$campos[22]."','".$campos[3]."',null,null,'A','".$campos[25]."','".$campos[24]."','".$campos[28]."',cast(getdate() as date),'".$campos[3]."','$ruta','S')";
			//echo $sql;
			return mssql_query($sql,$this->con->conect);
		}
	}

	/**
	 * Guarda la desafiliacion de la empresa
	 * @param unknown_type $campos
	 * @return number: 0 | idDesafiliacion
	 */
	function guardar_desafiliacion($arrDatos,$usuario){

		$sql="INSERT INTO dbo.aportes067
					(id_empresa,id_causal,usuario,fecha_estado)
			VALUES ({$arrDatos["id_empresa"]},{$arrDatos["id_causal"]},'$usuario','{$arrDatos["fechaDesafiliacion"]}')";
		
		$statement = self::$conPDO->queryInsert($sql,"aportes067");
		return $statement === null ? 0: $statement;
	}

	/**
	 * Guarda el detalle desafiliacion de la empresa
	 * @param unknown_type $campos
	 * @return number: 0 | 1
	 */
	function guardar_detalle_desafiliacion($arrDatos){

		$sql="INSERT INTO dbo.aportes068
					(id_suspension,id_desafiliacion)
			VALUES ({$arrDatos["id_suspension"]},{$arrDatos["id_desafiliacion"]})";

		$statement = self::$conPDO->queryInsert($sql,"aportes068");
				return $statement === null ? 0: 1;
	}

	/**
	 * Guarda observacion a la empresa
	 * @param unknown_type $campos
	 * @return number: 0 | 1
	 */
	function guardar_observacion($arrDatos,$usuario){
		$fecha = date("m/d/Y");
		$flagObservacion = 2;//[2][OBSERVACION EMPRESA]
		$observacionAnterior = self::$conPDO->b_obserbacion($arrDatos["id_empresa"], $flagObservacion);

		$observacionNueva = $fecha." - ".$usuario." - ".$arrDatos["observacion"]."<br>".$observacionAnterior;

		//Elimina la observacion que tenga
		$sql="delete from aportes088 WHERE idregistro={$arrDatos["id_empresa"]} AND identidad=$flagObservacion";
		self::$conPDO->queryActualiza($sql);

		$rs=self::$conPDO->i_obserbacion($arrDatos["id_empresa"], $observacionNueva, $flagObservacion, "aportes088");
		return $rs > 0 ? 1 : 0;
	}

	/**
	 * Guarda la notificacion de la empresa
	 * @param unknown_type $campos
	 * @return number: 0 | 1
	 */
	function guardar_notificacion($arrDatos,$usuario){
		$fechaEstado = empty($arrDatos["fecha_estado"])?"null":"'{$arrDatos["fecha_estado"]}'";
		
		$sql="INSERT INTO dbo.aportes072
				(id_empresa,id_carta_notificacion,informacion,id_estado,fecha_estado,contenido_carta,usuario)
			VALUES({$arrDatos["id_empresa"]},{$arrDatos["id_carta_notificacion"]},:informacion,{$arrDatos["id_estado"]},$fechaEstado,:contenido_carta,'$usuario')";
		
		$statement = self::$conPDO->conexionID->prepare($sql);
		$statement->bindValue(":informacion", $arrDatos["informacion"],  PDO::PARAM_STR);
		$statement->bindValue(":contenido_carta", $arrDatos["contenido_carta"],  PDO::PARAM_STR);
		$guardada = $statement->execute();
		return $guardada == false ? 0 : 1;
	}
	
	/**
	 * Metodo para actualizar la notificacion de la empresa
	 * El parametro debe ser un array indice valor array("atributo"=>valor,...)
	 * @param unknown_type $datos
	 * @return multitype: 0 | 1
	 */
	function actualizar_notificacion($datos,$usuario){
	
		$fechaEstado = empty($datos["fecha_estado"])?"null":"'{$datos["fecha_estado"]}'";
	
		$sql = "UPDATE dbo.aportes072 SET
					id_carta_notificacion = {$datos["id_carta_notificacion"]},
					id_empresa = {$datos["id_empresa"]},
					id_estado = {$datos["id_estado"]},
					fecha_estado = $fechaEstado,
					informacion = :informacion,
					contenido_carta = :contenido_carta,
					usuario = '$usuario'
				WHERE id_notificacion={$datos["id_notificacion"]}";

		$statement = self::$conPDO->conexionID->prepare($sql);
		$statement->bindValue(":informacion", $datos["informacion"],  PDO::PARAM_STR);
		$statement->bindValue(":contenido_carta", $datos["contenido_carta"],  PDO::PARAM_STR);
		$guardada = $statement->execute();
		return $guardada == false ? 0 : 1;
	}
	
	/**
	 * Metodo para buscar las notificaciones de la empresa
	 * El parametro debe ser un array indice valor array("atributo"=>valor,...)
	 * @param unknown_type $arrAtributoValor
	 * @return multitype: array
	 */
	function buscar_notificacion($arrAtributoValor){
		$attrEntidad = array(
				array("nombre"=>"id_notificacion","tipo"=>"NUMBER","entidad"=>"a72")
				,array("nombre"=>"id_carta_notificacion","tipo"=>"NUMBER","entidad"=>"a72")
				,array("nombre"=>"id_empresa","tipo"=>"NUMBER","entidad"=>"a72")
				,array("nombre"=>"id_estado","tipo"=>"NUMBER","entidad"=>"a72")
				,array("nombre"=>"fecha_sistema","tipo"=>"DATE","entidad"=>"a72")
				,array("nombre"=>"fecha_estado","tipo"=>"DATE","entidad"=>"a72")
				,array("nombre"=>"nit","tipo"=>"TEXT","entidad"=>"a48"));
	
		$filtroSql = Utilitario::obtenerFiltro($attrEntidad,$arrAtributoValor);
	
		$sql = "SELECT a72.id_notificacion,a72.usuario,a72.fecha_sistema,a72.fecha_estado,a72.informacion, a72.id_carta_notificacion, a72.contenido_carta
				, a48.nit, a48.razonsocial
				, a91.detalledefinicion AS carta_notificacion
				, b91.detalledefinicion AS estado_notificacion 
			FROM aportes072 a72 
				INNER JOIN aportes048 a48 ON a48.idempresa=a72.id_empresa
				INNER JOIN aportes091 a91 ON a91.iddetalledef=a72.id_carta_notificacion
				LEFT JOIN aportes091 b91 ON b91.iddetalledef=a72.id_estado
			$filtroSql";
		return $this->fetchConsulta($sql);
	}
	
	/**
	 * Metodo para actualizar el estado notificacion de la empresa
	 * El parametro debe ser un array indice valor array("atributo"=>valor,...)
	 * @param unknown_type $datos 
	 * @return multitype: 0 | 1
	 */
	function actualizar_estado_notificacion($datos){
		$sql = "UPDATE dbo.aportes072
				SET 
					id_estado = {$datos["id_estado"]},
					fecha_estado = '{$datos["fecha_estado"]}'
				WHERE id_notificacion={$datos["id_notificacion"]}";
		$statement = self::$conPDO->conexionID->prepare($sql);
		$guardada = $statement->execute();
		return $guardada == false ? 0 : 1;
	}
//Busca las historias de renovacion camara de comercio
function buscar_renovacion_empresa($idEmpresa){
	if($this->con->conectar()==true){
		$sql="SELECT *
				FROM aportes073 
				WHERE idempresa=$idEmpresa 
				ORDER BY fecharenovacion DESC, idempresa DESC";
		//echo $sql;
		return mssql_query($sql,$this->con->conect);
	}
}	

function buscar_principal($nit){
    if($this->con->conectar()==true){
        $sql="select * from aportes048 where nit='$nit' and principal='S'";
        //echo $sql;
	return mssql_query($sql,$this->con->conect);
	}
 }
 
function contar_sucursales($nit){
	if($this->con->conectar()==true){
			$sql="select count(*) as cuenta from aportes048 where nit='$nit'";
			return mssql_query($sql,$this->con->conect);
		}
	} 
 
function buscar_grupo_empresa($nit){
	if($this->con->conectar()==true){
			$sql="select idempresa,nit,digito,principal,razonsocial,codigosucursal,fechaafiliacion,rutadocumentos from aportes048 where nit='$nit'";
			return mssql_query($sql,$this->con->conect);
		}
	} 
	 
function actualizar_nit($nit,$nuevo,$digito){
	if($this->con->conectar()==true){
			$sql="update aportes048 set nit='$nuevo',digito='$digito' where nit='$nit'";
			return mssql_query($sql,$this->con->conect);
		}
	}
	
function empresasNOgiro_ley1429($empresas){
	$sql="update aportes048 set giro='N' where idempresa in ($empresas)";
	$statement = self::$conPDO->conexionID->prepare($sql);
	$guardada = $statement->execute();
	return $guardada == false ? 0 : 1;
}
		 
function buscar_empresa($ide){
	if($this->con->conectar()==true){
		$sql="SELECT aportes048.*, depto_ciud.departmento, depto_ciud.municipio, depto_ciud.zona, aportes015.identificacion as identificacion_repleg, aportes015.papellido, aportes015.sapellido, aportes015.pnombre, aportes015.snombre, aportes015.direccion direccion_repleg, aportes015.telefono telefono_repleg, aportes015.telefono, aportes015.email email_repleg, jp.papellido as pajp, jp.sapellido as sajp, jp.pnombre as pnjp, jp.snombre as snjp FROM aportes048 LEFT JOIN aportes015 ON aportes048.idrepresentante = aportes015.idpersona LEFT JOIN aportes015 jp ON aportes048.idjefepersonal = jp.idpersona LEFT JOIN aportes089 depto_ciud ON depto_ciud.coddepartamento = aportes048.iddepartamento and depto_ciud.codmunicipio = aportes048.idciudad and depto_ciud.codzona=aportes048.idzona WHERE idempresa=$ide";
		return mssql_query($sql,$this->con->conect);
	}
 }

function buscar_empresa1($ide){
	if($this->con->conectar()==true){
			$sql="SELECT 
					aportes048.*
					, aportes015.identificacion as identificacionrep
					, aportes015.papellido
					, aportes015.sapellido
					, aportes015.pnombre
					, aportes015.snombre
					, jp.identificacion as identificacionjp
					, jp.papellido as pajp
					, jp.sapellido as sajp
					, jp.pnombre as pnjp
					, jp.snombre as snjp 
					, a87.barrio AS barriorecidencia
					, a87.codigozona AS codzonacorresp
					, b87.barrio AS barrio
					, b87.codigozona AS codzona
				FROM aportes048 
					LEFT JOIN aportes015 ON aportes048.idrepresentante = aportes015.idpersona 
					LEFT JOIN aportes015 jp ON aportes048.idjefepersonal = jp.idpersona 
					LEFT JOIN aportes087 a87 ON a87.idbarrio=aportes048.idbarriocorresp
					LEFT JOIN aportes087 b87 ON b87.idbarrio=aportes048.idbarrio
				WHERE idempresa=$ide";
			return mssql_query($sql,$this->con->conect);
		}
 }
 /**
  * Obtiene la informaci�n de una empresa seg�n el id recibido
  *
  * @param int $idEmpresa
  * @return ifx result or false
  */
function buscar_empresa_por_idempresa($idEmpresa){
	if($this->con->conectar()==true){
		$query = "select * from aportes048 where idempresa = ".$idEmpresa;
		return mssql_query($query,$this->con->conect);
	}
	return false;
}

function buscar_empresa_por_NIT($nit){
	if($this->con->conectar()==true){
		$query = "select * from aportes048 where nit = '{$nit}'";		
		return mssql_query($query,$this->con->conect);
	}
	return false;
}

/**
 * Buscar las empresas morosas por periodo, clase aportante, digitos de la fecha vencimiento
 * @param unknown_type $periodo
 * @param unknown_type $claseAportante
 * @param unknown_type $digitosIniciales
 * @param unknown_type $digitosFinales
 */
function buscar_empresas_morosas($periodo,$claseAportante,$digitosIniciales,$digitosFinales){
	if($this->con->conectar()==true){
		$query = "SELECT 
						a97.periodo,a97.pago,a97.interrucion,a97.idempresa,a97.fechasistema
						,a48.nit,a48.razonsocial
						,a91.detalledefinicion AS claseaportante
					FROM aportes097 a97
						INNER JOIN aportes048 a48 ON a48.idempresa=a97.idempresa
						INNER JOIN aportes091 a91 ON a91.iddetalledef=a48.claseaportante
					WHERE a97.pago='N' AND a97.periodo='$periodo' AND a48.claseaportante=$claseAportante AND 
						substring(a48.nit,len(a48.nit)-1,2) BETWEEN $digitosIniciales AND $digitosFinales";
		$rs = mssql_query($query,$this->con->conect);
		$arrDatos = array();
		while ($row = mssql_fetch_array($rs))
			$arrDatos[] = array_map("utf8_encode",$row);
		return $arrDatos;
	}
	return false;
}

	/**
	 * Metodo para buscar empresas suspendidas
	 * El parametro debe ser un array indice valor array("atributo"=>valor,...)
	 * @param unknown_type $arrAtributoValor
	 * @return multitype:NULL | array
	 */
	function buscar_empresa_suspendida($arrAtributoValor){		
		$attrEntidad = array(
				array("nombre"=>"id_suspension","tipo"=>"NUMBER","entidad"=>"a52")
				,array("nombre"=>"id_empresa","tipo"=>"NUMBER","entidad"=>"a52")
				,array("nombre"=>"periodo","tipo"=>"TEXT","entidad"=>"a52")
				,array("nombre"=>"estado","tipo"=>"TEXT","entidad"=>"a52")
				,array("nombre"=>"fecha_sistema","tipo"=>"DATE","entidad"=>"a52")
				,array("nombre"=>"fecha_estado","tipo"=>"DATE","entidad"=>"a52")
				,array("nombre"=>"nit","tipo"=>"TEXT","entidad"=>"a48"));
		
		$filtroSql = Utilitario::obtenerFiltro($attrEntidad,$arrAtributoValor);
		
		$sql = "SELECT a52.*
					,a48.razonsocial,a48.nit, a48.estado AS estadoempresa
				FROM aportes052 a52
					INNER JOIN aportes048 a48 ON a48.idempresa=a52.id_empresa
				$filtroSql";
		
		return $this->fetchConsulta($sql);
	}
	
	/**
	 * Metodo para buscar empresas desafiliadas
	 * El parametro debe ser un array indice valor array("atributo"=>valor,...)
	 * @param unknown_type $arrAtributoValor
	 * @return multitype:NULL | array
	 */
	function buscar_empresa_desafiliada($arrAtributoValor){
		$attrEntidad = array(
				array("nombre"=>"id_desafiliacion","tipo"=>"NUMBER","entidad"=>"a67")
				,array("nombre"=>"id_empresa","tipo"=>"NUMBER","entidad"=>"a67")
				,array("nombre"=>"id_causal","tipo"=>"NUMBER","entidad"=>"a67")
				,array("nombre"=>"estado","tipo"=>"TEXT","entidad"=>"a67")
				,array("nombre"=>"fecha_sistema","tipo"=>"DATE","entidad"=>"a67")
				,array("nombre"=>"nit","tipo"=>"TEXT","entidad"=>"a48"));
	
		$filtroSql = Utilitario::obtenerFiltro($attrEntidad,$arrAtributoValor);
	
		$sql = "SELECT a67.id_desafiliacion,a67.id_empresa,a67.id_causal,a67.estado AS estado_desafiliacion, a67.usuario, a67.fecha_sistema AS fecha_desafiliacion, a67.fecha_estado
					,a48.nit,a48.razonsocial, a48.estado AS estado_empresa, a48.fechaestado AS fecha_estad_empre
					,a91.detalledefinicion AS causal
					,a91.codigo AS cod_causal
					,b91.detalledefinicion AS causa_estad_empre
					,a52.id_suspension,a52.periodo
				FROM aportes067 a67
					INNER JOIN aportes048 a48 ON a48.idempresa=a67.id_empresa
					INNER JOIN aportes091 a91 ON a91.iddetalledef=a67.id_causal
					LEFT JOIN aportes091 b91 ON b91.iddetalledef=a48.codigoestado
					LEFT JOIN aportes068 a68 ON a68.id_desafiliacion=a67.id_desafiliacion
					LEFT JOIN aportes052 a52 ON a52.id_suspension=a68.id_suspension
		$filtroSql";
	
		return $this->fetchConsulta($sql);
	}

/**
 * Inactivar las empresas
 * @param unknown_type $idEmpresa
 * @param unknown_type $fechaEstado
 * @param unknown_type $causalInactivacion
 * @return boolean
 */
function inactivar_empresa($idEmpresa,$fechaEstado,$causalInactivacion,$trabajadores=0){
	if($this->con->conectar()==true){
		$query = "UPDATE aportes048 SET 
					estado='I'
					,fechaestado='$fechaEstado'
					,codigoestado='$causalInactivacion'
					,trabajadores=$trabajadores
				WHERE idempresa=".$idEmpresa;
		return mssql_query($query,$this->con->conect);
	}
	return false;
}

	/**
	 * Actualizar la empresa expulsada
	 * @param unknown_type $idEmpresa
	 * @param unknown_type $fechaEstado
	 * @return boolean
	 */
	function actualizar_empresa_expulsada($arrDatos){
		$query = "UPDATE aportes048 SET
					estado='{$arrDatos["estado"]}'
					,fechaestado='{$arrDatos["fechaestado"]}'
					,codigoestado='{$arrDatos["codigoestado"]}'
					,trabajadores={$arrDatos["trabajadores"]}
					,fechaafiliacion='{$arrDatos["fechaafiliacion"]}'
				WHERE idempresa=".$arrDatos["idempresa"];
		$statement = self::$conPDO->conexionID->prepare($query);
		$guardada = $statement->execute();
		return $guardada == false ? 0 : 1;
	}

	function actualizar_codigo_estado($idEmpresa,$codigoEstado){
		$sql = "UPDATE dbo.aportes048
					SET codigoestado = $codigoEstado
					WHERE idempresa = $idEmpresa";
			$statement = self::$conPDO->conexionID->prepare($sql);
			$guardada = $statement->execute();
			return $guardada == false ? 0 : 1;
	}

	/**
	 * Actualizar la empresa desde el modulo de promotoria
	 * @param unknown_type $arrDatos
	 * @return boolean
	 */
	function actualizar_empresa_promotoria($arrDatos,$usuario){
		$sql = "UPDATE aportes048 SET
					direcorresp='{$arrDatos["direcorresp"]}'
					, iddepcorresp='{$arrDatos["iddepcorresp"]}'
					, idciucorresp='{$arrDatos["idciucorresp"]}'
					, telefono='{$arrDatos["telefono"]}'
					, idrepresentante={$arrDatos["idrepresentante"]}
				WHERE idempresa={$arrDatos["idempresa"]}";
		$statement = self::$conPDO->conexionID->prepare($sql);
		$guardada = $statement->execute();
		return $guardada == false ? 0 : 1;
	}
	
function inactivar_empresa_por_NIT($nit){
	if($this->con->conectar()==true){
		$query = "update aportes048 set estado='I',fechasistema=cast(getdate() as date) where nit='".$nit."'";
		return self::$conPDO->queryActualiza($query);
	}
	return false;
}

function activar_empresa_por_NIT($nit){
	if($this->con->conectar()==true){
		$query = "update aportes048 set estado='A',fechasistema=cast(getdate() as date) where nit=".$nit;
		return mssql_query($query,$this->con->conect);
	}
	return false;
}

function buscar_todas($parametro, $tipo=1){
	if($this->con->conectar()==true){
		if($tipo==1)
			$sql="SELECT idempresa,nit,razonsocial from aportes048 WHERE nit like '$parametro'";
		if($tipo==2)
			$sql="SELECT idempresa,nit,razonsocial from aportes048 WHERE razonsocial like '%$parametro%'";
		return mssql_query($sql,$this->con->conect);
	}
 }		 	

function actualizar_empresa($campo){
		if($this->con->conectar()==true){
			$sql="update aportes048 set idsector=".$campo[2].", idclasesociedad=".$campo[3].", razonsocial='".$campo[7]."', sigla='".$campo[8]."', direccion='".$campo[9]."', iddepartamento='".$campo[10]."', idciudad='".$campo[11]."', telefono='".$campo[12]."', fax='".$campo[13]."', url='".$campo[14]."', email='".$campo[15]."', idrepresentante='".$campo[16]."', idjefepersonal='".$campo[21]."', contratista='".$campo[26]."', colegio='".$campo[27]."', exento='".$campo[28]."', idcodigoactividad='".$campo[29]."', indicador='".$campo[30]."', idasesor='".$campo[31]."', seccional='".$campo[32]."', estado='".$campo[33]."', usuario='".$campo[36]."', fechasistema=cast(getdate() as date), idtipodocumento=".$campo[6].", idclasesociedad=".$campo[3]." where idempresa=".$campo[38];
			//echo $sql;
			return mssql_query($sql,$this->con->conect);
		}
	}
		 
function trayectoria($ide){
	if($this->con->conectar()==true){
		$sql="SELECT nit,razonsocial,fechaafiliacion,CASE WHEN estado='I' THEN fechaestado ELSE null END AS fechaestado FROM aportes048 WHERE idempresa=$ide";	
		return mssql_query($sql,$this->con->conect);
	}
 }		 	
function otras_afiliaciones($ide){
	if($this->con->conectar()==true){
		$sql="SELECT 
					DISTINCT au48.nit,au48.razonsocial,au48.fechaafiliacion,au48.fechaestado 
			  FROM AUDITORIA048 au48
			  WHERE au48.idempresa=$ide and estado='I' AND NOT EXists (select * from aportes048 a48 where a48.nit=au48.nit and a48.estado=au48.estado and a48.fechaestado=au48.fechaestado)";	
		return mssql_query($sql,$this->con->conect);
	}
 }		 	
 function buscar_una_sucursal($nit,$suc){
	if($this->con->conectar()==true){
	$sql="SELECT * FROM aportes048 WHERE nit='$nit' and codigosucursal='$suc'";
        //echo $sql;
	return mssql_query($sql,$this->con->conect);
	}
 }
 
 function buscar_datos_empresa($nit){
 	if($this->con->conectar()==true){
 		$sql="SELECT DISTINCT idempresa, aportes048.idtipodocumento, nit, digito, codigosucursal, principal, razonsocial, sigla, aportes048.direccion, iddepartamento, idciudad, aportes048.idzona, aportes048.telefono, fax, aportes048.url, aportes048.email, idrepresentante, idjefepersonal, contratista, idcodigoactividad, actieconomicadane, indicador, idasesor, fechamatricula, aportes048.fechaestado, idsector, seccional, tipopersona, claseaportante, tipoaportante, aportes048.estado, codigoestado, fechaestado, fechaaportes, aportes048.fechaafiliacion, trabajadores, aportantes, aportes048.usuario, aportes048.fechasistema, idclasesociedad, aportes048.rutadocumentos, legalizada,d.departmento,m.municipio, r.papellido AS par,r.sapellido AS sar,r.pnombre AS pnr,r.snombre snr,r.identificacion AS identificacionrpl, c.papellido AS pac,c.sapellido AS sac,c.pnombre AS pnc,c.snombre snc, ca.descripcion AS actividad, da.clase AS codact, da.descripcion AS dane, i.detalledefinicion AS indicador, s.detalledefinicion AS sector, p.detalledefinicion tipopersona,cs.detalledefinicion AS clasesoci, capo.detalledefinicion AS clase_apo, ta.detalledefinicion AS tipo_apo, td.detalledefinicion AS tipo_doc,a91.detalledefinicion AS tipoAfiliacion
				, direcorresp, iddepcorresp, idciucorresp
			FROM aportes048 
				LEFT JOIN aportes089 d ON aportes048.iddepartamento=d.coddepartamento
				--LEFT JOIN aportes089 m ON aportes048.idciudad=m.codmunicipio
				LEFT JOIN aportes089 m ON aportes048.idciudad=m.codmunicipio OR (isnumeric(aportes048.idciudad)=1 AND isnumeric(m.codmunicipio)=1 AND CAST(aportes048.idciudad AS INT)=CAST(m.codmunicipio AS INT)) 
				LEFT JOIN aportes015 r ON aportes048.idrepresentante=r.idpersona
				LEFT JOIN aportes015 c ON aportes048.idjefepersonal=c.idpersona
				LEFT JOIN aportes079 ca ON aportes048.idcodigoactividad=ca.idciiu
				LEFT JOIN aportes079 da ON aportes048.actieconomicadane=da.clase
				LEFT JOIN aportes091 i ON aportes048.indicador=i.iddetalledef
				LEFT JOIN aportes091 s ON aportes048.idsector=s.iddetalledef
				LEFT JOIN aportes091 p ON aportes048.tipopersona=p.iddetalledef
				LEFT JOIN aportes091 cs ON aportes048.idclasesociedad=cs.iddetalledef 
				LEFT JOIN aportes091 capo ON aportes048.claseaportante=capo.iddetalledef
				LEFT JOIN aportes091 ta ON aportes048.tipoaportante=ta.iddetalledef
				LEFT JOIN aportes091 td ON aportes048.idtipodocumento=td.iddetalledef
				LEFT JOIN aportes091 a91 ON aportes048.idtipoafiliacion=a91.iddetalledef
			where nit='{$nit}'";
 		//echo $sql;
 		return mssql_query($sql,$this->con->conect);
 	}
 }
 
	 /**
	  * Obtiene una lista de empresas seg�n la(s) actividad(es) econ�mica(s) y dem�s par�metros
	  * @param array $idsActividadEco arreglo sigle con los ids de los tipos de actividad econ�mica
	  * @param array $parametros opcional, otros par�metros de b�squeda
	  * @return ifx_result $resultEmpresas
	  */
	 function buscar_x_actividad_eco($idsActividadEco = array(), $parametros = array()){
	 	if($this->con->conectar()){
	 		$strCondiciones = "";
		 	$arrCondiciones = array();
		 	if(is_array($parametros)){
		 		if(isset($parametros["razonsocial"]))
		 			$arrCondiciones[] = " razonsocial like '%". strtoupper($parametros["razonsocial"]) ."%'";
		 		
		 		// @TODO Agregar otros par�metros de criterios de b�squeda
		 	}
		 	if(is_array($arrCondiciones) && count($arrCondiciones)>0)
		 		$strCondiciones = " AND ". implode(" AND ", $arrCondiciones);
		 	
		 	$sql = "SELECT * FROM aportes048 WHERE idcodigoactividad in (". implode(",",$idsActividadEco) .") $strCondiciones";
		 	$resultEmpresas = mssql_query($sql, $this->con->conect);
		 	return $resultEmpresas;
	 	}
	 }
	 
	 function contar_trabajadores_activos($idEmpresa){
	 	if($this->con->conectar()){
	 		$sql = "SELECT count(*) AS cantidad FROM aportes015 WHERE idpersona in (select idpersona from aportes016 where idempresa=$idEmpresa)";
	 		$result = mssql_query($sql, $this->con->conect);
	 		return $result;
	 	}
	 }
	 
	 /**
	  * Retorna los valores obtenidos en la consulta
	  * @return multitype:array
	  */
	 private function fetchConsulta($querySql){
	 	$resultado = array();
	 	$rs = self::$conPDO->querySimple($querySql);
	 	while($row = $rs->fetch())
	 		$resultado[] = array_map("trim",$row);
	 	//$resultado[] = array_map("utf8_encode",$row);
	 	return $resultado;
	 }
 }	
?>