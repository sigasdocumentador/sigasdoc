<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR. 'config.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejaInforma.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'wssdk'. DIRECTORY_SEPARATOR . 'ClientWSInfWeb.php';
$db = IFXDbManejaInforma::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
$campo0=$_REQUEST['v0'];
$campo1=$_REQUEST['v1'];
$campo2 = 0;
if( isset($_REQUEST['v2']) ) $campo2=1;

$cliente = new ClientWSInfWeb(USUARIO_WEB_SIGAS, CONTRASENA_WEB_SIGAS);
$comprobantes = $cliente->movimientoPorComprobante($campo0, $campo1);

$con=0;
$fila =array();
if ($campo2==0){
	foreach($comprobantes as $comprobante){
	$fila[]= array("valor" => $comprobante->Valor, "fecha" => $comprobante->Fecha, "descripcion" => $comprobante->Descripcion, "nota" => $comprobante->Nota, "nit" => $comprobante->Nit, "numero" => $comprobante->Numero, "observacion" => $comprobante->Observacion);
	$con++;
	}
}
else{
	foreach($comprobantes as $comprobante){
		if ($comprobante->Cuenta == '410505001'){
			$fila[]= array("valor" => $comprobante->Valor, "fecha" => $comprobante->Fecha, "descripcion" => $comprobante->Descripcion, "nota" => $comprobante->Nota, "nit" => $comprobante->Nit, "numero" => $comprobante->Numero,  "observacion" => $comprobante->Observacion);
			$con++;
		}
	}
}

if($con==0)
	echo 0;
else
	echo  json_encode($fila);

?>