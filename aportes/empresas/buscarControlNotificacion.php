<?php
	set_time_limit(0);
	ini_set("display_errors",'1');
	date_default_timezone_set("America/Bogota");
	$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
	include_once  $root;
	include_once $raiz.DIRECTORY_SEPARATOR.'aportes'.DIRECTORY_SEPARATOR.'empresas'.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'empresas.class.php';
	
	$usuario = $_SESSION["USUARIO"];
	$datos = $_POST["datos"];
	$objEmpresa = new Empresa();
	
	$datoNotificacion = $objEmpresa->buscar_notificacion($datos,$usuario);
	$datoNotificacion = count($datoNotificacion)>0?json_encode($datoNotificacion):0;
	echo $datoNotificacion;
?>