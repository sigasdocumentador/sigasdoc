<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'aportes'.DIRECTORY_SEPARATOR.'empresas'.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'empresas.class.php';
include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'p.afiliacion.class.php';

//Recibir datos del js
$objDatos=$_REQUEST['objDatos'];

$usuario=$_SESSION['USUARIO'];
$numeroTrabajadores = 0;
$fechaSistema = Date("ymd");

$banderaError = 0;
$banderaError2 = 0;
$idDesafiliacion = 0;
$codigoEstadoEmpresa = 0;
$objEmpresa = new Empresa();
$objAfiliacion = new Afiliacion();

$objEmpresa->inicioTransaccion();

//Activar los trabajadores
$numeroTrabajadores = $objAfiliacion->spProcesoActivAfiliExpul($objDatos["id_empresa"],$objDatos["id_desafiliacion"],$usuario); 
if($numeroTrabajadores==0){
	//No se realizo el proceso de activar las afiliaciones
	$banderaError++;
}

//Activar Empresa
//if($banderaError==0){
	if($objEmpresa->actualizar_empresa_expulsada(
			array("idempresa"=>$objDatos["id_empresa"]
					,"estado"=>"A"
					,"fechaestado"=>$fechaSistema
					,"codigoestado"=>0
					,"trabajadores"=>$numeroTrabajadores
					,"fechaafiliacion"=>$objDatos["fecha_afili_empre"]))>0){
		$banderaError2++;
	}
//}

//Guardar Observacion Empresa
//if($banderaError==0){
	$rsObservacion = $objEmpresa->guardar_observacion(array("id_empresa"=>$objDatos["id_empresa"]
			,"observacion"=>$objDatos["observacion"]),$usuario);
	if($rsObservacion==0)
		$banderaError++;
//}

//Fin
if($banderaError2==0){
	$objEmpresa->cancelarTransaccion();
	echo 0;
}else{
	$objEmpresa->confirmarTransaccion();
	echo 1;
}
?>