var paginaActual=null;
$(document).ready(function(){
	var URL = src();	
	
	$("#buscarEmpresa").click(function(){
		$("#tDatos tbody,tfoot").empty();
		//VALIDAR QUE EL NIT SEA NUMEROS
		if( $("#buscarPor").val()=="1"&& isNaN($("#idEmpresa").val()) ){
			$(this).next("span.Rojo").html("El NIT debe ser num\u00E9rico.").hide().fadeIn("slow");
				return false;
		}
		
		if($("#buscarPor").val()=='Seleccione..'){
			$(this).next("span.Rojo").html("Seleccione el criterio de b\u00FAsqueda.").hide().fadeIn("slow");
			return false;
		}

		if($("#idEmpresa").val()==''){
			$(this).next("span.Rojo").html("Ingrese el NIT \u00F3 RAZON SOCIAL").hide().fadeIn("slow");
			$("#idEmpresa").focus();
			return false;
		}

		empresasPendientesLey1429(1,[$("#idEmpresa").val().toUpperCase(),$("#buscarPor").val()]);
	});//fin click
	
	
	empresasPendientesLey1429(1,[$("#idEmpresa").val().toUpperCase(),$("#buscarPor").val()]);
	
}); // dom ready

function empresasPendientesLey1429(pagina,parametros){
	paginaActual = pagina;
	$("#tDatos tbody").empty();
	var razonSocial = "";
	var nit = "";
	if(parametros.length == 2){
		// buscar por
		if(parametros[1] == "nit")
			nit = parametros[0];
		else if(parametros[1] == "razonsocial"){
			razonSocial = parametros[0];
		}
	}
	$.getJSON('contarEmpresaLey1429.php?nit='+ nit +"&razonSocial="+ razonSocial,function(totalRegistros){
		var regsPorPagina = 20;
		var numPaginas = Math.ceil(totalRegistros/regsPorPagina);
		var strEnlacesPag = "";
		$("#span_num_empresas").html(totalRegistros);
		c=0;
		for(var i = pagina; i<=numPaginas; i++){
			if(c <= 10){
				strEnlacesPag += "<a href='#' id='pagina_"+ i +"' class='link_paginacion' >"+ i +"</a> ";
			}
			c++;	
		}
		
		var strEnlaceAnterior = "";
		if(pagina > 1)
			strEnlaceAnterior = "<a href='#' id='pagina_anterior'> << </a>";
		
		var strEnlaceSiguiente = "";
		if(pagina < numPaginas)
			strEnlaceSiguiente = "<a href='#' id='pagina_siguiente'> >> </a>";
		
		strEnlacesPag = strEnlaceAnterior + strEnlacesPag + strEnlaceSiguiente; 
		//carga las empresas de la BD
		$.getJSON('empresasPendientesLey1429.php?pagina='+ pagina +'&numRegistros='+regsPorPagina +'&nit='+ nit +"&razonSocial="+ razonSocial,function(data){
		  	if(data == 0){
		  		alert("no hay datos");
		  		return false;
		  		//$("#idEmpresa").val("");
		  		//$("#cmbIdCausal").val("0");
		  		//empresasPendientesLey1429(paginaActual,[$("#idEmpresa").val().toUpperCase(),$("#buscarPor").val()]);
		  	}
		  	$.each(data,function(i,fila){
				var direccion = (fila.direccion == 'null' || fila.direccion == null)?'':fila.direccion;

				$("#tDatos").append("<tr><td style=text-align:center>"+fila.nit_empresa+"</td><td style=text-align:left>"+fila.razonsocial+"</td><td style=text-align:left>"+direccion+"</td><td style=text-align:center>"+fila.trabajadores+"</td><td><img src='"+ URL+"imagenes/chk0.png' /><input type='checkbox' value='"+fila.idempresa+"' style='display:none' /></td></tr>");
		  	});
		  	
		  	$("table.tablaR tr:even").addClass("zebra");
			//Dar un ID dinamico a los check box de la tabla de empresas
			$("table.tablaR td input:checkbox").each(function(index){
				//$(this).attr({"id":"chk"+index,"value": $(this).parents("tr").children("td:first").find("a").html()}).hide();//id de los check
			});
			
			$("table.tablaR td img").each(function(index,imagen){
				$(imagen).attr({"id":"img"+index,"value": $(imagen).parents("tr").children("td:first").find("a").html()});//id de los check
				$(imagen).bind('click',function(){
					if($(imagen).next("input:checkbox").is(":checked")){
						$(imagen).attr("src",URL+"imagenes/chk0.png");
						$(imagen).next("input:checkbox").attr("checked",false);
					}else{
						$(imagen).attr("src",URL+"imagenes/chk1.png");
						$(imagen).next("input:checkbox").attr("checked",true);
					}
				});
			});
		});//end post
		
		$("#tDatos tfoot").html("<tr><td colspan='6'>"+ strEnlacesPag +"</td></tr>");
		$(".link_paginacion").each(function(ind,enlace){
			$(enlace).bind('click',function(){
				var pag = $(this).attr("id").replace("pagina_","");
				empresasPendientesLey1429(pag,[]);
			});
		});
		$("#pagina_anterior").bind("click",function(){
			if(pagina > 1)
				empresasPendientesLey1429(parseInt(pagina)-1,[$("#idEmpresa").val().toUpperCase(),$("#buscarPor").val()]);
		});
		
		$("#pagina_siguiente").bind("click",function(){
			if(pagina < numPaginas)
				empresasPendientesLey1429(parseInt(pagina)+1,[$("#idEmpresa").val().toUpperCase(),$("#buscarPor").val()]);
		});
		
		$("#pagina_"+pagina).addClass("pagina_actual");
	});	
}

function guardarMarcadas(){
	
	if($("#cmbIdCausal").val()=="0"){
		alert("Debe seleccionar una causal");
		return false;
	}
	
	if(confirm('Realmente desea Marcar para Giro ?'))
	{   
	var IdTipoCausal = $("#cmbIdCausal").val();
	var seleccionadas = [];
	var marcadas = [];
	var contador = 0;
	$("table.tablaR td input:checked").each(function(i){
		var ide = $(this).val();//extraigo el valor del value del check cargado dinamicamente
		seleccionadas[i] = ide;
		if(ide > 0){
			marcadas[contador] = ide;
			contador++;
		}
	});
	 
 
 	if(marcadas.length > 0){
 		$.getJSON('marcarEmpresasGiro.php',{v0:marcadas.join(), v1:IdTipoCausal},function(data){
			if(data == 0)
				alert("Hubo problemas al actualizar los registros");
			else{
					if(data == marcadas.length && marcadas.length == seleccionadas.length)
						alert("Registro actualizado.");
						$("#idEmpresa").val("");
						$("#cmbIdCausal").val("0");
						obsEmpresasLey1429(marcadas);
						
					/*else
						alert("Se Marcaron las empresas\n"+ marcadas.join() +". \n"+ (seleccionadas.length-marcadas.length) +" Registros no pudieron ser modificados.");
						*/
				}
			empresasPendientesLey1429(paginaActual,[$("#idEmpresa").val().toUpperCase(),$("#buscarPor").val()]);
 		});
 	}else{
 		alert("Debe marcar al menos una empresa.");
 		}
	} // Fin confirm
}


function obsEmpresasLey1429(marcadas){
	var URL=src();
	$("div[name='div-observaciones-tab']").dialog("destroy");
	$("textarea[name='observacionEmpresa']").val("");
	$("div[name='div-observaciones-tab']").dialog({
		  modal:true,
		  width:520,
		  resizable:false,
		  draggable:false,
		  closeOnEscape:false,
		  open:function(){
			  $(this).prev().children().hide();//Oculto la barra de titulo
			  $(this).prev().html("OBSERVACIONES...");//Pongo un nuevo titulo
			  $("textarea[name='observacionEmpresa']").focus();
			  },
		  buttons:{
		    'Guardar':function(){
		    	var comentario='Empresas Ley 1429 marcadas para Giro';
		    	var observacioncaptura=$("textarea[name='observacionEmpresa']").val();
				var observacion=comentario+' - '+observacioncaptura;
				var observacionSinEspacios = observacion.replace(/^\s+|\s+$/g,"");
				
				if(observacioncaptura.length==0){
			    	alert("Escriba las observaciones!");
					return false;
					}
					if(observacioncaptura.length<10){
						alert("Las observaciones son muy cortas...!");
						return false;
					}
				
			    if(observacionSinEspacios!=""){
				 $.ajax({
					   url:"insertNotasEmpresasLey1429.php",
					   type:"POST",
					   data:{v0:marcadas.join(),v1:observacion},
					   success:function(data){
					    $("[name='rtaObservacion']").fadeIn();//el mensaje esta oculto en la capa div-observacion
						$("div[name='div-observaciones-tab']").dialog("close");
					   }
			     });//ajax
				 alert("Observacion guardada"); 
				}//if observacion
				else{
					alert("No se pudo guardar la observaci\xf3n");
				}
			 }//guardar
		  }//buttons
		  });   
}