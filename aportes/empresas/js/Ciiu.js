/*
* @autor:      Ing. Carlos Parra
* @fecha:      2014-08-13
* objetivo:
*/
// JavaScript Document


$(function(){
	$("#select7").bind("change",function(){
		var idCiiu = $("#select7").val();
		opciones = "";
		tiposRegistros = "clases";
		$("#lisCodigo").empty();
		
		$.getJSON('buscarCIU.php',{idciiu: idCiiu, tiposregistros: tiposRegistros},function(resultado){
			if(resultado != 0 && resultado.length>0){
				resultado.forEach(function(elemento,ind, arreglo){
					opciones += "<option value='"+ elemento.clase +"'>"+ elemento.descripcion +"</option>";
				});
				$("#lisCodigo").html(opciones);
				$("#lisCodigo").trigger("change");
			}
		});
	});
});


function codigoCiu(){
	var cc=$("#lisCodigo").val();
	if(cc!=0)
		{
		$.getJSON('buscarCIU.php', {v0:cc}, function(datos){
			if(datos==0){
				alert("El codigo no existe!");
				$("#codigoCIU").val("");
				$("#lisCodigo").val(0);
				return false;
			}
			$("#codigoCIU").val(datos[0].clase);
			$("#seccion").val(datos[0].seccion);
			
			
			$.getJSON('buscarCIU.php',{v0: datos[0].idciiu,v1:datos[0].seccion},function(resultado){		
					$("#select7").val(resultado[0].idciiu);
				
				});
				
			
		});
		}
	}	

function buscarCIU(){
	var ciu=$.trim($("#codigoCIU").val());
	///var ciuDosPrim=ciu.substring(0,2);	
	if(ciu.length==0) return false;
	$.getJSON('buscarCIU.php', {v0:ciu}, function(datos){
		if(datos==0){
			alert("El codigo no existe!");
			//$("#select7").val(0);
			$("#codigoCIU").val("");
			$("#lisCodigo").val(0);
			return false;
			}
		//var codActvEcn=CambiarComboActEconm(ciuDosPrim);
		
		
		$("#seccion").val(datos[0].seccion);
			
			
			$.getJSON('buscarCIU.php',{v0: datos[0].idciiu,v1:datos[0].seccion},function(dato){		
					$("#select7").val(dato[0].idciiu);
				
				});
		
		opciones = "";
		tiposRegistros = "clases";
		$("#lisCodigo").empty();
				
		$.getJSON('buscarCIU.php',{v0: ciu, tiposregistros: tiposRegistros},function(resultado){
			if(resultado != 0 && resultado.length>0){
				resultado.forEach(function(elemento,ind, arreglo){
					opciones += "<option value='"+ elemento.clase +"'>"+ elemento.descripcion +"</option>";
				});
				$("#lisCodigo").html(opciones);
				$("#lisCodigo").val(datos[0].clase);
				//$("#lisCodigo").trigger("change");
			}
		});	
		
	});
}