var nuevo=0;
var modificar=0;
var continuar=true;
var idrepresentante=0;
var idjefe=0;
var msg="";
var ide=0;
var URL=src();

$(document).ready(function(){
// Parametros para e cboDepto2
$("#cboDepto2").change(function () {
	$("#cboDepto2 option:selected").each(function () {
		//alert($(this).val());
		elegido=$(this).val();
		$.post("../phpComunes/ciudades.php", { elegido: elegido }, function(data){
		$("#cboCiudad2").html(data);
		});			
      });
   });
});

function validarCampos(op){
	var cadena="<ul>";

	if($("#select12").val()==0){
		cadena+="<li>Falta sector</li>";
	}
	/****************************************/
	if($("#select13").val()==0){
		cadena+="<li>Falta clase de sociedad</li>";
	}
	/*************************************************/
	if($("#tNit").val()==""){
		cadena+="<li>Falta NIT</li>";
	}
	else{
		if(isNaN($("#tNit").val()))
		cadena+="<li>El NIT debe ser num\u00E9rico!</li>";
		var long=$("#tNit").val().length;
		if(long<5 || long>11)
		cadena+="<li>el NIT es de 5 a 11 d\xedgitos</li>";
	}
	/************************************************/
	if($("#tDigito").val==""){
		cadena+="<li>Falta d\u00EDgito de verificaci\u00FAn</li>";
	}
	/************************************************/
	if($("#textfield").val()==""){
		cadena+="<li>Falta raz\u00F3n social</li>";
	}
	else{
		if($("#textfield").val().length<3)
		cadena+="<li>La raz\u00F3n social es muy corta!</li>";
	}
	/***************************************************/
	if($("#textfield2").val()==0){
		cadena+="<li>Falta nombre comercial</li>";
	}
	else{
		if($("#textfield2").val().length<3)
		cadena+="<li>La raz\u00FAn social es muy corta!</li>";
	}
	/***************************************************/
	if($("#tDireccion").val()==""){
		cadena+="<li>Falta direcci\u00F3n</li>";
	}
	else{
		if($("#tDireccion").val().length<10){
		cadena+="<li>La direccion es muy corta!</li>";
		}
	}
	/***************************************************/
	if($("#cboDepto2").val()==0){
		cadena+="<li>Falta departamento</li>";
	}
	/***************************************************/
	if($("#cboCiudad2").val()==0){
		cadena+="<li>Falta ciudad</li>";
	}
	/*************************************************/
	if($("#textfield8").val==""){
		cadena+="<li>Falta el tel\u00E9fono</li>";
	}
	else{
		if(isNaN($("#textfield8").val()))
		cadena+="<li>El tel\u00E9fono debe ser num\u00E9rico!</li>";
		var long=$("#textfield8").val().length;
		if(long<7 || long>10)
		cadena+="<li>el tel\u00E9fono es de 7 a 10 d\xedgitos</li>";
	}
	/*************************************************/
	if($("#textfield6").val()!=""){
		if(isNaN($("#textfield6").val()))
		cadena+="<li>El fax debe ser num\u00E9rico!</li>";
		var long=$("#textfield6").val().length;
		if(long<7)
		cadena+="<li>el fax es de 7 d\xedgitos</li>";
	}
	/************************************************/
	
	//14 www 
	
	/************************************************/
	/*var email=/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test($("#email").val());
	if($("#email").val()!=''){
	    if(!email) {
			cadena+='<li>El E-mail aparentemente es incorrecto.</li>';										 				
		}else{
		  email=$("#email").val();
	  }
	}
*/
	/*************************************************/
	if($("#textfield4").val()==""){
		cadena+="<li>Falta identificaci\u00F3n del representante legal</li>";
	}
	else{
		if(isNaN($("#textfield4").val()))
		cadena+="<li>La identificacin debe ser num\u00E9rico!</li>";
		var long=$("#textfield4").val().length;
		if(long<5 || long>11)
		cadena+="<li>La identificaci\u00FAn es de 5 a 11 d\xedgitos</li>";
	}
	/****************************************/
	if($("#select4").val()==0){
		cadena+="<li>Falta contratista</li>";
	}
	/****************************************/
	if($("#select5").val()==0){
		cadena+="<li>Falta colegio</li>";
	}
	/****************************************/
	if($("#select6").val()==0){
		cadena+="<li>Falta excento</li>";
	}
	/****************************************/
	if($("#select7").val==0){
		cadena+="<li>Falta actividad</li>";
	}
	/****************************************/
	if($("#select11").val()==""){
		cadena+="<li>Falta indice de aportes</li>";
	}
	/****************************************/
	if($("#select8").val()==0){
		cadena+="<li>Falta asesor</li>";
	}
	/****************************************/
	if($("#select10").val()==0){
		cadena+="<li>Falta seccional</li>";
	}
	/****************************************/
	if($("#select9").val()==0){
		cadena+="<li>Falta estado</li>";
	}
	if($("#datepicker").val()==0){
		cadena+="<li>Falta fecha inicio aportes</li>";
	}
	
	if($("#datepickerConstitucion").val()==0){
		cadena+="<li>Falta fecha de constitución</li>";
	}
	
	/******************** FIN ****************/
	if(cadena.length>10){
		cadena+="</ul>";
		cadena="Hay errores en los datos, por favor revisar:"+cadena;
		document.getElementById('error').innerHTML=cadena;
		return;
	}
	else{
		cadena="Datos completos!";
		document.getElementById('error').innerHTML=cadena;
	}
	guardarAgencia();
}

function buscarPersona(objeto,op){
	var numero=parseInt(objeto.value);
	if(isNaN(numero)) return;
	$.ajax({
		url: URL+'phpComunes/pdo.buscar.persona.php',
		cache: false,
		type: "GET",
		data: {v0:1,v1:numero,v2:2},
		dataType: "json",
		success: function(datos){
			if(op==0){
				if(datos==0){
					alert("No existe en nuestra base, registre los datos completos!");	
					persona_simple(objeto,document.getElementById('idRepresentante'));
				}else{
					nom=datos[0].pnombre+" "+datos[0].snombre+" "+datos[0].papellido+" "+datos[0].sapellido;
					$("#idRepresentante").val(datos[0].idpersona);
					idrepresentante=datos[0].idpersona;
					$("#tNombre1").val(nom);
				}
			}else{
				if(datos==0){
					alert("No existe en nuestra base, registre los datos completos!");	
					persona_simple(objeto,document.getElementById('idJefe'));
				}else{
					nom=datos[0].pnombre+" "+datos[0].snombre+" "+datos[0].papellido+" "+datos[0].sapellido;
					$("#idContacto").val(datos[0].idpersona);
					$("#tNombre2").val(nom);
					idjefe=datos[0].idpersona;
				}
			}
		}
	});
}

function nuevoR(){
	nuevo=1;
	$("#bGuardar").show();
	camposLimpiar();
	$("#lisRadicacion").focus();
	$("#txtId2").val(fechaHoy());
	$('#lisRadicacion option').remove();
	var campo0=105;
	$.post(URL+'phpComunes/buscarRadicaTipoID.php',{v0:campo0},function(datos){
    	var cmbRadicacion = $("#lisRadicacion");
	   	$(cmbRadicacion).html(datos);
	   	if($(cmbRadicacion).children().length==1){
			alert('Lo lamento NO hay radicaciones pendientes de grabar!');
			return false;
         }
	});
	msg="";
}

function buscarRadicacion(obj){
	if(nuevo==0){
		alert("Primero haga click en Nuevo!");
		return false; 
		}
	$.getJSON('buscarRadicacion.php', {v0:obj.value}, function(datos){
		$.each(datos,function(i,fila){
			$("#tNit").val(fila.nit);
			$("#tIdRadicacion").val(fila.idtiporadicacion);
			$("#tNit2").val(fila.nit);
			$("#tFechaRadicacion").val(fila.fechasistema);
			$("#textfield18").val(fila.fechasistema);
			buscarPrincipal(fila.nit);
			return;
		});
	});
}

function buscarPrincipal(nit){
	$.getJSON('buscarPrincipal.php',{v0:nit},function(datos){
		if(datos==1){
			alert('No hay empresa principal con este NIT!');
			return false;
		}	
		$.each(datos,function(i,fila){
			$("#div-razon").html(fila.razonsocial);
			$("#select12").attr("disabled",false);
			$("#select12").val(fila.idsector);
			$("#select12").trigger("change");
			$("#select12").attr("disabled",true);
			$("#select13").attr("disabled",false);
			$("#select13").val(fila.idclasesociedad);
			$("#select13").trigger("change");
			$("#select13").attr("disabled",true);
			$("#select4").attr("disabled",false);
			$("#select4").val(fila.contratista);
			$("#select4").trigger("change");
			$("#select4").attr("disabled",true);
			$("#select6").attr("disabled",false);
			$("#select6").val(fila.exento);
			$("#select6").trigger("change");
			$("#select6").attr("disabled",true);
			$("#select5").attr("disabled",false);
			$("#select5").val(fila.colegio);
			$("#select5").trigger("change");
			$("#select5").attr("disabled",true);
			$("#select7").attr("disabled",false);
			$("#select7").val(fila.idcodigoactividad);
			$("#select7").trigger("change");
			$("#select7").attr("disabled",true);
			$("#tDigito").attr("disabled",false);
			$("#tDigito").val(fila.digito);
			$("#tDigito").trigger("change");
			$("#tDigito").attr("disabled",true);
			
			$("#datepickerConstitucion").val(fila.fechamatricula);
			ide=fila.idempresa;
			codigoSucursal();
			$.getJSON(URL+'phpComunes/pdo.buscar.notas.php',{v0:ide,v1:'2'},function(datos){
					document.getElementById('div-observaciones').innerHTML=datos;
					});
			return false;
		});	
		});
	}
	
function codigoSucursal(){
	var campo0=document.forms[0].elements[4].value;
	$.ajax({
		url: 'codigoNuevo.php',
		type: "POST",
		data: "submit=&v0="+campo0,
		success: function(datos){
			if(datos=='0'){
				alert('No hay empresa principal con este NIT!');
				return false;
			}	
			document.getElementById('div-cuenta').innerHTML="C&oacute;digo Nueva Sucursal --> "+datos;
			document.getElementById('tCodigoSucursal').value=datos;
			return false;
		}
	});
	}	
	
function digito(obj)
{
	var vpri, x, y, z, i, nit1, dv1;
 	nit1=obj.value;
 	if (isNaN(nit1))
 	{
 	alert('El valor digitado no es un n\u00FAmero valido'+nit1);
 	} 
 	else {
 	vpri = new Array(16);
 	x=0 ; y=0 ; z=nit1.length ;
	 vpri[1]=3;
	 vpri[2]=7;
	 vpri[3]=13;
	 vpri[4]=17;
	 vpri[5]=19;
	 vpri[6]=23;
	 vpri[7]=29;
	 vpri[8]=37;
	 vpri[9]=41;
	 vpri[10]=43;
	 vpri[11]=47;
	 vpri[12]=53;
	 vpri[13]=59;
	 vpri[14]=67;
	 vpri[15]=71;
 	for(i=0 ; i<z ; i++) {
 	y=(nit1.substr(i,1));
 	x+=(y*vpri[z-i]);
 	}
 	y=x%11
 	if (y > 1)
 	{
 		dv1=11-y;
 	} else {
 		dv1=y;
 	}
 	document.forma.tDigito.value=dv1;
   }
  }
  
function cerrarRadicacion(){
    var campo0=$("#lisRadicacion").val();
	$.ajax({
		url: URL+'phpComunes/cerrarRadicacion.php',
		type: "POST",
		data: "submit=&v0="+campo0,
		success: function(datos){
			limpiarCampos();
			camposLimpiar();
			document.getElementById('div-razon').innerHTML="";
			alert(datos);
		}
	});
	return false;
	  }	
	  
function camposLimpiar(){
	document.getElementById('div-razon').innerHTML="";
	document.getElementById('div-observaciones').innerHTML="";
	document.getElementById('div-cuenta').innerHTML="";
	$("#tNombre1").html('');
	$("#tNombre2").html('');
	$("#textfield19").val("");
	$("table.tablero input:text").val('');
	$("table.tablero select").val(0);
	$("#lisRadicacion").focus();
}

function copiar(obj1,obj2){
	obj1 = $("#"+obj1.id);
	obj2.val(obj1.val());
	}
	
function guardarAgencia(){
	$("#bGuardar").hide();
	var campo0=$("#lisRadicacion").val();	//idradicacion
	var campo1=$("#txtId2").val();			//fecha
	var campo2=$("#select12").val();		//sector
	var campo3=$("#select13").val();		//clase de sociedad
	var campo4=$("#tNit").val();			//Nit
	var campo5=$("#tDigito").val();			//Digito
	var campo6=1;							//Tipo Documento
	var campo7=$("#textfield").val();		//Razon Social
	var campo8=$("#textfield2").val();		//Sigla
	var campo9=$("#cboDepto2").val();		//departamento
	var campo10=$("#cboCiudad2").val();		//ciudad
	var campo11=$("#tDireccion").val();		//direccion
	var campo12=$("#textfield8").val();		//telefono
	var campo13=$("#textfield6").val();		//fax
	var campo14=$("#textfield9").val();		//URL
	var campo15=$("#textfield15").val();	//Email
	var campo16=idrepresentante;			//ID REPRESENTANTE
	var campo17=idjefe;						//ID CONTACTI
	var campo18=$("#select4").val();		//contratista
	var campo19=$("#select5").val();		//colegio
	var campo20=$("#select6").val();		//excento
	var campo21=$("#select7").val();		//Actividad Economica
	var campo22=$("#select11").val();		//Indice
	var campo23=$("#select8").val();		//Asesor
	var campo24=$("#select10").val();		//Seccional
	var campo25=$("#select9").val();		//Estado
	var campo26=$("#textfield18").val();	//Fecha Afiliacion
	var campo27=$("#datepicker").val();		//Fecha Aportes
	var campo28=$("#textfield19").val();	//usuario
	var campo29=$("#tCodigoSucursal").val();//codigo sucursal
	var campo30=$("#datepickerConstitucion").val();//Fecha constitucion=>fechamatricula
	
	$.ajax({
		url: 'empresasNew_Sucursal.php',
		type: "POST",
		data: "submit=&v0="+campo0+"&v1="+campo1+"&v2="+campo2+"&v3="+campo3+"&v4="+campo4+"&v5="+campo5+"&v6="+campo6+"&v7="+campo7+"&v8="+campo8+"&v9="+campo9+"&v10="+campo10+"&v11="+campo11+"&v12="+campo12+"&v13="+campo13+"&v14="+campo14+"&v15="+campo15+"&v16="+campo16+"&v17="+campo17+"&v18="+campo18+"&v19="+campo19+"&v20="+campo20+"&v21="+campo21+"&v22="+campo22+"&v23="+campo23+"&v24="+campo24+"&v25="+campo25+"&v26="+campo26+"&v27="+campo27+"&v28="+campo28+"&v29="+campo29+"&v30="+campo30,
		success: function(datos){
			if(datos>0){ 
				msg+="Se guard\u00F3 la agencia!! \r\n";
				alert(msg);
				cerrarRadicacion();
				observacionesTab(datos,2);
				}
			else{
				$("#bGuardar").show();
			alert(datos);
			}
		}
	});
	return false;
	}		  