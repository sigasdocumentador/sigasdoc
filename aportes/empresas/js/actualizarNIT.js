/*
* @autor:      Ing. Orlando Puentes
* @fecha:      06/10/2010
* objetivo:
*/
// JavaScript Document
//var URL=src();
var idempresa;
var fechaafiliacion;
var rutaDocsAnt;
$(document).ready(function(){	
	limpiar();
	$("#txtId").focus();
});
$(function() {
		$("#ayuda").dialog({
		 	autoOpen: false,
			height: 450, 
			width: 700, 
			open: function(evt, ui){
					$('#ayuda').html('');
					$.get('../../help/aportes/ayudaActualizarNIT.html',function(data){
							$('#ayuda').html(data);
					});
			 }
		});
	});
$(function() {
		$("#dialog-form2").dialog("destroy");
		/*var notas = $("#notas"),
			allFields = $([]).add(notas);*/
		
		$("#dialog-form2").dialog({
			autoOpen: false,
			height: 400,
			width: 500,
			modal: true,
			buttons: {
				'Enviar': function() {
					var bValid = true;
					var campo=$('#notas').val();
					var campo0=$.trim(campo);
					if (campo0==""){
						$(this).dialog('close');
						return false;
						}
					var campo1=$('#usuario').val();
					var campo2="actualizarNIT.html";
					$.post('../../phpComunes/colaboracion.php',{v0:campo0,v1:campo1,v2:campo2}, function(datos){
						if(datos=='1'){
							alert("Su comentario fue enviado correctamente!. Gracias por participar en el mejoramiento de SIGAS");
						}else{
							alert(datos);
						}
					});
				$(this).dialog('close');
				},
				Cancelar: function() {
					$(this).dialog('close');
					$("#dialog-form2").dialog("destroy");
				}
			}
			
		});
		/*
		$('#enviar-notas')
			.button()
			.click(function() {
				$('#dialog-form2').dialog('open');
			}); */
	});	
		
function buscarEmpresas(nit){
	if(nit.value==""){
			return false;
		}
	else{
		var cadena="<table class=tablero width=100%><tr><td>No</td><td>NIT</td><td>D�gito</td><td>Principal</td><td>Razon Social</td></tr>";
		var cont=0;
		$.getJSON('buscarGrupoEmpresa.php',{v0:nit.value},function(datos){
			$.each(datos, function(i, fila){
			idempresa=fila.idempresa;
			fechaafiliacion=fila.fechaafiliacion;
			rutaDocsAnt=fila.rutadocumentos;
			cadena+="<tr><td>"+fila.idempresa+"</td><td>"+fila.nit+"</td><td>"+fila.digito+"</td><td>"+fila.principal+"</td><td>"+fila.razonsocial+"</td></tr>";	
			cont++;
			});
			cadena+="</table>";
			if(cont==0){
				document.getElementById('grupo').innerHTML="<center><label class=Rojo>No hay empresas con ese NIT</label></center>";
				setTimeout((function(){
					$("#txtId").val('');
				}),1500);
				}
			else{
				document.getElementById('grupo').innerHTML=cadena;
			}
		});
	}
}

function buscarEmpresas2(nit){
	if(nit.value==""){
			return false;
		}
	else{
		$.getJSON('buscarEmpresaNIT.php',{v0:nit.value},function(datos){
			//$.each(datos, function(i, fila){
				if(datos!=0){
					alert("Ya existe nuevo NIT en la Base de Datos!");
					$("#txtId2").val('');
					return false;
				}else
					{
					return true;
					}
			//});
		});
	}
}
	
function actualizar(){
	var campo0=$("#txtId").val();//nit viejo
	var campo1=$("#txtId2").val();//nit nuevo
	var campo2=digito(campo1);
	if(campo0==0){
			alert("Digite el NIT que desea actualizar!");	
			return false;
		}
	if(campo1==0){
			alert("Falta nuevo NIT!");
			return false;
		}
	$.getJSON('buscarEmpresaNIT.php',{v0:campo1},function(datos){
		//$.each(datos, function(i, fila){
			if(datos!=0){
				alert("Ya existe \"Nuevo NIT\" en la Base de Datos!");
				$("#txtId2").val('');
			}else
				{
					if(campo1!=campo0)
					{
						$.post('actualizarNIT.php',{v0:campo0,v1:campo1,v2:campo2}, function(datos2){
						if(datos2!=0){
							$.post(URL+'phpComunes/crearRutaDocumentos.php',{idgeneral:idempresa,identificacion:campo1,tipodocumento:1,fecha:fechaafiliacion},function(data){
								if(data!="")
									{
										$.post(URL+'phpComunes/migrarDocumentosWS.php',{rutaOrigen:rutaDocsAnt,rutaDestino:data},function(data){
											if(data==0)
												{
												alert("No se pudo migrar los Documentos, informe al Administrador del sistema!");
												return false;
												}
										});
										alert("Los registros fueron actualizados!");
										limpiar();
									}
							});
						}
						else{
							alert("Los registros NO fueron actualizados!");
						}
						});
					}else
						{
							alert("Los Nit's son iguales!!");
						}
				}
		//});
	});
}	
	
function limpiar(){
	limpiarCampos();
	document.getElementById('grupo').innerHTML="";
	}	
	

