$(function(){
	$('#txtFechaEstado,#txtFechaEstadoMasiva').datepicker({
		changeMonth: true,
		changeYear: true,
		maxDate: "+0D"
	});
	
	cargarEmpresas();
	
	$("#btnBuscar,#btnInactivar,#btnLimpiar").button();
	datepickerC("PERIODO","#txtPeriodoInicial");
	datepickerC("PERIODO","#txtPeriodoFinal");
	
	//Evento para chequear todos los checkbox
	$("#imgChequear").click(function(){
		var srcImagen1 = URL+"imagenes/chk1.png";
		var srcImagen0 = URL+"imagenes/chk0.png";
		var estadoChequer = $("#hidChequearTodo").val();
		if(estadoChequer==0) {
			$("#tbodyDatosEmpresas :checkbox").parent().parent().addClass("ui-state-default");
			$("#tbodyDatosEmpresas :checkbox").attr("checked",true);
			$("#hidChequearTodo").val(1);
			$("#imgChequear").attr("src",srcImagen0);
		}else if(estadoChequer==1) {
			$("#tbodyDatosEmpresas :checkbox").parent().parent().removeClass("ui-state-default");
			$("#tbodyDatosEmpresas :checkbox").attr("checked",false);
			$("#hidChequearTodo").val(0);
			$("#imgChequear").attr("src",srcImagen1);
		}
	});
	
	//Crear dialogo para inactivar la empresa
	$("#divInactivarEmpresa").dialog({
		modal:true,
		autoOpen:false,
		width:700,
		height:300,
		buttons:{
			"Inactivar":function(){
				var idEmpresa = $("#divInactivarEmpresa #tdIdEmpresa").text().trim();
				var fechaEstado = $("#divInactivarEmpresa #txtFechaEstado").val();
				var causal = $("#divInactivarEmpresa #cmbCausalInactivacion").val();
				$("#divInactivarEmpresa #txtFechaEstado, #divInactivarEmpresa #cmbCausalInactivacion")
						.removeClass("ui-state-error");
				if(idEmpresa==0)return false;
				if(fechaEstado==0){$("#divInactivarEmpresa #txtFechaEstado").addClass("ui-state-error");return false;}
				if(causal==0){$("#divInactivarEmpresa #cmbCausalInactivacion").addClass("ui-state-error");return false;}
				
				var arrDatosEmpresa = new Array({
						idEmpresa:idEmpresa
						,fechaEstado:fechaEstado
						,causal:causal});
				var resultado = inactivarEmpresa(arrDatosEmpresa);
				if(resultado==0){
					alert("No fue posible inactivar la empresa");
				}else{
					alert("La empresa se inactivo correctamente");
					nuevo();
					removerFila(idEmpresa);
					$(this).dialog("close");
					mostrarDialogoObservaciones(arrDatosEmpresa);
				}
			}
		}
	});
	$("#divEmpresasNoInactivas").dialog({
		modal:true,
		autoOpen:false,
		width:700,
		height:300
	});
	$("#btnBuscar").click(function(){
		
		var nit = $("#txtNitBuscar").val().trim();
		var periodoInicial =  parseInt($("#txtPeriodoInicial").val());
		var periodoFinal =  parseInt($("#txtPeriodoFinal").val());
		var estado = $("#cmbEstado").val();
		
		//Realizar el filtro del nit
		if(nit!=0){
			$(".clsNit:visible").each(function(){
				if($(this).text().trim().indexOf(nit)>=0)
					$(this).parent().show();
				else
					$(this).parent().hide();
			});
			
		}/*else{
			$(".clsNit").parent().show();
		}*/
		
		//Realizar el filtro del periodo
		if(!isNaN(periodoInicial) && !isNaN(periodoFinal)){
			$(".clsPeriodo:visible").each(function(){
				var periodo = parseInt($(this).text());
				if(periodo>=periodoInicial && periodo<=periodoFinal)
					$(this).parent().show();
				else
					$(this).parent().hide();
			});
			
		}/*else{
			$(".clsPeriodo").parent().show();
		}*/
		
		//Realizar el filtro del estado
		if(estado!=0){
			$(".clsEstado:visible").each(function(){
				if(estado==$(this).text().trim())
					$(this).parent().show();
				else
					$(this).parent().hide();
			});
			
		}/*else{
			$(".clsEstado").parent().show();
		}*/
	});
	
	$("#btnLimpiar").click(function(){
		$("#tbodyDatosEmpresas tr").show();
		$("#txtNitBuscar,#txtPeriodoInicial,#txtPeriodoFinal,#cmbEstado").val("");
	});
	
	$("#btnInactivar").click(function(){
		var numCheck = $("#tbodyDatosEmpresas :checkbox:checked").length;
		var causal = $("#cmbCausalInactivacionMasiva").val();
		var fechaEstado = $("#txtFechaEstadoMasiva").val();
		if(numCheck==0){
			alert("Debe chequear las empresas a inactivar");
			return false;
		}
		$("#txtFechaEstadoMasiva").removeClass("ui-state-error");
		if(fechaEstado==0){$("#txtFechaEstadoMasiva").addClass("ui-state-error");return false;}
		var arrDatosEmpresa = new Array();
		var arrDatosEmpresa2 = new Array();
		$("#tbodyDatosEmpresas :checkbox:checked").each(function(i,row){
			arrDatosEmpresa[arrDatosEmpresa.length] = {
					idEmpresa:$(this).val()
					,fechaEstado:fechaEstado
					,causal:causal};
			arrDatosEmpresa2[arrDatosEmpresa2.length] = {
					idEmpresa:$(this).val()
					,nit:$("#"+$(this).parent().parent().get(0).id + " .clsNit").text()
					,razonSocial:$("#"+$(this).parent().parent().get(0).id + " .clsRazonSocial").text()};
			
		});
		var resultado = inactivarEmpresa(arrDatosEmpresa);
		if(typeof resultado=="object"){
			var trDatos = "";
			$.each(resultado,function(i,row){
				$.each(arrDatosEmpresa2,function(i2,row2){
					if(row2.idEmpresa==row.idEmpresa)
						trDatos += "<tr><td>"+row2.nit+"</td><td>"+row2.razonSocial+"</td></tr>";
				});
			});
			$("#divEmpresasNoInactivas table tbody").html(trDatos);
			nuevo();
		}else if(resultado==1){
			alert("La empresas se inactivaron correctamente");
			nuevo();
			mostrarDialogoObservaciones(arrDatosEmpresa);
			$.each(arrDatosEmpresa,function(i,row){
				removerFila(row.idEmpresa);
			});
		}else{
			alert("No fue posible inactivar las empresas");
		}
	});
});

//limpiar los campos
function nuevo() {
	limpiarCampos2("#hidChequearTodo");
	$("#imgChequear").attr("src","../../imagenes/chk1.png");
}

function removerFila(idEmpresa){
	$("#trIdEmpresa"+idEmpresa).remove();
}
function inactivarEmpresa(arrDatosEmpresa){
	var data = 0;
	$.ajax({
		type:"POST",
		url:"actualizarInactEmpreSinTrabajador.php",
		data:{objeto:arrDatosEmpresa},
		dataType:"json",
		async:false,
		success:function(datos){
			if(typeof datos=="object"){					
				if(datos.length>1)
					data = datos;
				else
					data = 1;
			}
		}
	});
	return data;
}

function cargarEmpresas(){
	limpiarCampos2("#tdMensaje");
	$.ajax({
		type:"POST",
		url:"buscarEmpreSinTrabajador.php",
		dataType:"json",
		async:false,
		beforeSend:function(objeto){
			dialogLoading('show');
		},
		complete:function(objeto, exito){
			dialogLoading('close');	
		},
		success:function(datos){
			if(datos==0){
				$("#tdMensaje").html("Se presento un error al momento de buscar las empresas");
				return false;
			}else if(typeof datos!="object"){
				$("#tdMensaje").html("No existen empresas con 0 trabajadores");
				return false;
			}
			var tr = "";
			$.each(datos,function(i,row){
				tr+="<tr id='trIdEmpresa"+row.idempresa+"'>" +
						"<td><input type='checkbox' id='chkIdEmpresa' name='chkIdEmpresa' value='"+row.idempresa+"'/></td>" +
						"<td >"+row.idempresa+"</td>" +
						"<td class='clsNit'>"+row.nit+"</td>" +
						"<td class='clsRazonSocial'>"+row.razonsocial+"</td>" +
						"<td>"+row.direcorresp+"</td>" +
						"<td>"+row.telefono+"</td>" +
						"<td>"+row.email+"</td>" +
						"<td>"+row.representante+" - "+row.identificacionrepresentante+"</td>" +
						"<td>"+row.agencia+"</td>" +
						"<td class='clsEstado'>"+row.estado+"</td>" +
						"<td>"+row.fechaafiliacion+"</td>" +
						"<td>"+row.fechaaportes+"</td>" +
						"<td class='clsPeriodo'>"+row.UltPeriodo+"</td>" +
						"<td>"+row.fechamatricula+"</td>" +
						"<td><img src='../../imagenes/borrar.png' onclick='dialogInactivar("+row.idempresa+");' style='cursor:pointer;'/></td></tr>";
			});
			$("#tbodyDatosEmpresas").html(tr);
			$('#tbodyDatosEmpresas tr:even').css('background-color', '#EAEAEA');
			$("#tdMensaje").html("Resultados ("+datos.length+")");
		}		
	});
}

function dialogInactivar(idEmpresa){
	limpiarCampos2("#divInactivarEmpresa #tdIdEmpresa, #divInactivarEmpresa #tdRazonSocial," +
			"#divInactivarEmpresa #tdNit, #divInactivarEmpresa #txtFechaEstado");
	var idTrDatos = "#trIdEmpresa"+idEmpresa;
	$("#divInactivarEmpresa #tdIdEmpresa").text(idEmpresa);
	$("#divInactivarEmpresa #tdRazonSocial").text($(idTrDatos+" .clsRazonSocial").text());
	$("#divInactivarEmpresa #tdNit").text($(idTrDatos+" .clsNit").text());
	$("#divInactivarEmpresa").dialog("open");
}

function mostrarDialogoObservaciones(arrDatos){
	var URL=src();
	$("div[name='div-observaciones-tab']").dialog("destroy");
	$("textarea[name='observacionGrupo']").val("");
	$("div[name='div-observaciones-tab']").dialog({
		modal:true,
		width:520,
		resizable:false,
		draggable:false,
		closeOnEscape:false,
		open:function(){
			$(this).prev().children().hide();//Oculto la barra de titulo
			$(this).prev().html("OBSERVACIONES...");//Pongo un nuevo titulo
			$("textarea[name='observacionGrupo']").focus();
		},
		buttons:{
			'Guardar':function(){
				var observacion=$("textarea[name='observacionGrupo']").val();
				//Si observacion tiene algun valor, se guarda de lo contrario no pasa nada
				if(observacion!=""){
					$.each(arrDatos,function(i,row){
						$.ajax({
							url:URL+"phpComunes/pdo.insert.notas.php",
							type:"POST",
							data:{v0:row.idEmpresa,v1:observacion,v2:2},
							async:false,
							beforeSend:function(objeto){
								dialogLoading('show');
							},
							complete:function(objeto, exito){
								dialogLoading('close');	
							},
							success:function(data){
								
							}
						});//ajax
					});
					$("[name='rtaObservacion']").fadeIn();//el mensaje esta oculto en la capa div-observacion
					$("div[name='div-observaciones-tab']").dialog("close");
				}else{
					alert("No se pudo guardar la observaci\xf3n");
					$("div[name='div-observaciones-tab']").dialog("close");
				}
			}//guardar
		}//buttons
	});
} 