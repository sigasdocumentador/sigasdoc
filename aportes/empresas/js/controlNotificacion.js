$(function(){ 
	$("#btnGuardar").button();
	datepickerC("FECHA","#txtFechaEstado");
	
	$("#txtNit").blur(function(){
		nuevo();
		
		var nit = $("#txtNit").val().trim();
		if(!nit)
			return false;
		
		var datosEmpresa = buscarDatosEmpresa(nit);				
		if(typeof datosEmpresa != "object"){
			$("#lblRazonSocial").addClass("classTextError").html(
					"No existe empresa con el nit " + nit);
			return false;
		}
		$("#hidIdEmpresa").val(datosEmpresa.idempresa);
		$("#lblRazonSocial").html(datosEmpresa.razonsocial);
	});
	
	$("#cmbCartaNotificacion").change(function(){
		var idEmpresa = $("#hidIdEmpresa").val().trim();
		var idCartaNotificacion = $("#cmbCartaNotificacion").val();
		
		$("#tdMensajeError,#tBody").html("");
		if(!$(this).val())return false;
		
		//Validar campos
		if(!idEmpresa){
			$("#tdMensajeError").html("Faltan datos, favor buscar de nuevo la empresa");
			return false;
		}
		$("#cmbCartaNotificacion").removeClass("ui-state-error");
		if(!idCartaNotificacion){
			$("#cmbCartaNotificacion").addClass("ui-state-error");
			return false;
		}
		
		var objDatos = {
				id_carta_notificacion:idCartaNotificacion,
				id_empresa:idEmpresa
			};
		//Buscar datos de la notificacion
		var objDatosNotificacion = buscarDatosNotificacion(objDatos);
		
		//Adiccionar los datos al formulario
		if(typeof objDatosNotificacion == "object"){
			var trHtml = "";
			$.each(objDatosNotificacion,function(i,row){
				trHtml += "<tr>" +
						"<td><input type='checkbox' name='chkIdNotificacion' id='chkIdNotificacion' value='"+row.id_notificacion+"' class='clsIdNotificacion'/></td>" +
						"<td>"+row.carta_notificacion+"</td>" +
						"<td>"+row.estado_notificacion+"</td>" +
						"<td>"+row.fecha_sistema+"</td>" +
						"<td>"+row.fecha_estado+"</td>"+
						"<td>"+row.informacion+"</td>"+
						"<td><label style='cursor:pointer' onClick='generarNotificacionPDf("+row.id_notificacion+");'><img src='../../imagenes/icono_pdf.png' width='24' height='24'></label></td></tr>";
			});
			$("#tBody").html(trHtml);
		}else{
			//No se encontraron datos
			$("#tdMensajeError").html("No existen notificaciones");
		}	
	});
});

function generarNotificacionPDf(idNotificacion){
	var data = "codigo_carta=GENERAR_NOTIFICACION_PDF&id_notificacion="+idNotificacion;
	
	var url=URL+"centroReportes/generar_carta.log.php?"+data;
	window.open(url,"_BLANK");
}

/**
 * Busca los datos de la empresa de acuerdo al nit
 * @param nit
 * @returns Datos de la empresa
 */
function buscarDatosEmpresa(nit) {
	var data;
	$.ajax({
		url:URL+"phpComunes/buscarEmpresaCompleta.php",	
		type:"POST",
		data:{v0:nit},
		dataType:"json",
		async:false,
		success:function(datos){
			if(typeof datos == "object")
				data = datos[0];
		}
	});
	return data;
}

/**
 * Busca los datos de la notificacion
 * @param nit
 * @returns Datos de la notificacion
 */
function buscarDatosNotificacion(objDatos) {
	var data;
	$.ajax({
		url:"buscarControlNotificacion.php",	
		type:"POST",
		data:{datos:objDatos},
		dataType:"json",
		async:false,
		success:function(datos){
			if(typeof datos == "object")
				data = datos;
		}
	});
	return data;
}

function nuevo(){
	limpiarCampos2("#hidIdEmpresa,#lblRazonSocial,#cmbCartaNotificacion," +
			"#tdMensajeError,#tBody,#cmbEstadoEmpresa,#txaObservacion,#txtFechaEstado");
}

function guardar(){
	//Validar los datos requeridos del formulario
	if(validaCampoFormu())
		return false;
	//Validar checkbox
	if($(".clsIdNotificacion:checkbox:checked").length==0){
		alert("No hay notificaciones chequeadas");
		return false;
	}
	
	//Preparar datos
	var objDatos = {
			id_estado:$("#cmbEstadoEmpresa").val(),
			id_empresa:$("#hidIdEmpresa").val(),
			fecha_estado:$("#txtFechaEstado").val(),
			observacion:$("#txaObservacion").val().trim()
		};
	
	var objDatoNotificacion = [];
	$(".clsIdNotificacion:checkbox:checked").each(function(i,row){
		objDatoNotificacion[objDatoNotificacion.length] = {
				id_notificacion:row.value
		};
	});
	
	objDatos.arrDatosNotificacion = objDatoNotificacion;
	
	$.ajax({
		url:"guardarControlNotificacion.php",	
		type:"POST",
		data:{datos:objDatos},
		dataType:"json",
		async:false,
		success:function(datos){
			if(datos==1){
				alert("El control de la notificacion se guardo correctamente");
				$("#txtNit").val("");
				nuevo();
			}else{
				alert("Error al guardar los datos");
			}
		}
	});
}

/**
 * Validar los elementos requeridos del formulario
 * @returns {Number}
 */
function validaCampoFormu(){
	var banderaError = 0;
	$( ".ui-state-error" ).removeClass( "ui-state-error" );
	$(".element-required").each(function(i,row){
		if( $( this ).val() == 0 ){
			$( this ).addClass( "ui-state-error" );
			banderaError++;
		}
	});
	return banderaError;
}