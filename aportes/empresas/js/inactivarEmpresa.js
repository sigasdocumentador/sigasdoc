URL=src();
var NIT="";
var razons="";
var fechaafi="";
var fechainact="";
var mydate=new Date()
var anio=mydate.getFullYear()
var dia=mydate.getDay()
var mes=mydate.getMonth()+1

$(document).ready(function(){
$("#nit").focus();
});//ready

//---------------------------------------BUSCAR DATOS-EMPRESA
function buscarEmpresa(numero){
	NIT=numero;
	  $.ajax({
		  url:URL+'aportes/empresas/buscarEmpresaNIT.php',
		  type:"POST",
		  dataType:"json",
		  data:{v0:numero},
		  success:function(datos){
			if(datos==0){
				MENSAJE("No existe empresa con ese NIT!.");
				$("#razonSocial").val("");
				$("#fechaAfiliacion").val("");
				$("#fechaInactivacion").val("");
				$("#nit").val("");
				return false;
			}
			$.each(datos, function(i, fila){
				razons=$.trim(fila.razonsocial);
				fechaafi=$.trim(fila.fechaafiliacion);
			})
			$("#razonSocial").val(razons);
     		$("#fechaAfiliacion").val(fechaafi);
			$("#fechaInactivacion").val(dia+"/"+mes+"/"+anio);
			$("#siguiente").attr('disabled',false);
			$("#siguiente").focus();
		  },
	  });//ajax
}
//----------------------------------SIGUIENTE EMPRESA
function seguir(){
	$.ajax({
		  url:URL+'aportes/empresas/inactivarEmpresaNIT.php',
		  type:"POST",
		  dataType:"json",
		  data:{v0:NIT},
		  success:function(datos){	
				MENSAJE("Empresa Inactivada!.");
				$("#razonSocial").val("");
				$("#fechaAfiliacion").val("");
				$("#fechaInactivacion").val("");
				$("#nit").val("");
				$("#nit").focus();
				return false;
		  },
	  });//ajax
}