/*
* @autor:      Ing. Orlando Puentes
* @fecha:      26/08/ 2010
* objetivo:
*/
$(function() {
		$("#imagen").dialog({
		 	autoOpen: false,
			modal: true,
		    hide: 'slide',
		    show:'fade',
		    width:900,
		    height:700
		});
});

$(function() {
		
	$("#dialog-form2").dialog("destroy");
	
	$("#dialog-form2").dialog({
		autoOpen: false,
		height: 400,
		width: 500,
		modal: true,
		buttons: {
			'Enviar': function() {
			var bValid = true;
			var campo=$('#notas').val();
			var campo0=$.trim(campo);
			if (campo0==""){
				$(this).dialog('close');
					return false;
				}
			var campo1=$('#usuario').val();
			var campo2="consultaEmpresa.php";
			$.post(url+"phpComunes/colaboracion.php",{v0:campo0,v1:campo1,v2:campo2}, 								
			function(datos){
				if(datos=='1'){
					alert("Su comentario fue enviado correctamente!. Gracias por participar en el mejoramiento de SIGAS");
				}else{
					alert(datos);
				}
			});
				$(this).dialog('close');
			},
			Cancelar: function() {
				$(this).dialog('close');
				$("#dialog-form2").dialog("destroy");
			}
		}		
	});
	
	$('#enviar-notas').button().click(function() {
		$('#dialog-form2').dialog('open');
	});
});

$(function() {
		$("#ayuda").dialog({
		 	autoOpen: false,
			height: 550, 
			width: 750, 
			draggable:true,
			open: function(evt, ui){
					$('#ayuda').html('');
					$.get('../../help/aportes/ayudaConsultaempresas.html',function(data){
							$('#ayuda').html(data);
					})
			 }
		});
});

/*...BUSCAR ...//
	1 nit
	2 razon social
	*/
$(document).ready(function(){
//Cargar ruta
	$.ajaxSetup({
		'beforeSend' : function(xhr) {
			try{
				xhr.overrideMimeType('text/html; charset=utf8');
			}
			catch(e){}
		}
	});	

	$("#buscarEmpresa").click(function(){
		$("#accordion h3").unbind('click');
		$("#load").show();
		
		//... LIMPIAR ACORDEON ...//
		$("#accordion").accordion("destroy");
		$("#accordion").children().remove();
		$("#empresas").find("p").remove();
		
		//... OCULTAR TABS ...//
		$("#tabsE").hide();
		
		//VALIDAR QUE EL NIT SEA NUMEROS
		if( $("#buscarPor").val()=="1"&& isNaN($("#idEmpresa").val()) ){
			$(this).next("span.Rojo").html("El NIT debe ser num\u00E9rico.").hide().fadeIn("slow");
				return false;
		}
		
		if($("#buscarPor").val()=='Seleccione..'){
			$(this).next("span.Rojo").html("Seleccione el criterio de b\u00FAsqueda.").hide().fadeIn("slow");
			return false;
		}
		if($("#idEmpresa").val()==''){
			$(this).next("span.Rojo").html("Ingrese el NIT \u00F3 RAZON SOCIAL").hide().fadeIn("slow");
			$("#idEmpresa").focus();
			return false;
		}	
				
		var v0=$("#idEmpresa").val();
		var v1=$("#buscarPor").val();
		
		$.ajax({
			url: 'buscarTodas.php',
			type: "GET",
			data: {v0:v0.toUpperCase(),v1:v1},
			async: false,
			dataType: 'json',
			success: function(data){
				if(data==''){
					$("#buscarEmpresa").next("span.Rojo").html("No se encontraron resultados.").hide().fadeIn("slow");
					$("#load").hide();
					return false;
				}
				
				$("#lstE").remove();
				$("#buscarEmpresa").next().html("");				
				var contadorEmp=0;
				var cadenaEmpresas='';
				
				$.each(data,function(i,fila){
					$("#buscarEmpresa").next("span.Rojo").html("");
					var idemp=fila.idempresa;
					var nitemp=fila.nit;
					var razemp=fila.razonsocial;
					
					contadorEmp++;
					cadenaEmpresas+='<p align="left" onclick="enviarNit('+nitemp+','+idemp+')"> '+idemp+" - "+razemp+'</p>';					
				});
				
				$("#empresas").append(cadenaEmpresas);
				$("#empresas").before("<p id='lstE'>Empresas encontradas:<b> "+contadorEmp+"</b></p>");
			}
		});//fin JSON
	});//fin click
	
	//... ICONO PARA OCULTAR/MOSTRAR FORMULARIO ...
	$("div#icon span").click(function(){
		$(this).toggleClass("toggleIcon");
		$("div#wrapTable").slideToggle();		
	});//fin click icon
	
});//Fin ready

function mostrarAyuda(){
	$("#ayuda").dialog('open' );
	}
function direccion(){
	$('#dialog-form').dialog('open');
	}
function notas(){
	$("#dialog-form2").dialog('open');
	}
function runSearch(e){
	if(e.keyCode == 13){
		$("#buscarEmpresa").trigger("click");
	}
} 

//... FUNCION PARA ENVIAR NIT PARA ACORDEON ...//
function enviarNit(nit, ide){
	$("div#icon").show();
	$("#empresas").find("p").remove();
	$("#lstE").remove();	
	var	indexTab;
	
	$("#tabsE").tabs("destroy");//Destruyo tab para nueva consulta
	$("#tabsE").show("slow",function(){
		$("#tabsE ul li:first a").trigger("click");
	});	//Muestro el tab
	
	//Aqui Capturo el valor del ALT para el switch que carque los formularios	
	$("#tabsE ul li a,#tabs2 ul li a").unbind("click"); 
	$("#tabsE ul li a").bind("click",function(){
		indexTab=$(this).attr("alt");
		
		if(indexTab=='f' || indexTab=='g'){
			$("#periodo").dialog('destroy');
			$(function(){	
				$("#periodo").dialog({
					                  autoOpen:false,
									  modal:true,
									  width:350,
									  height:20,
									  resizable:false
								   });
				});
			
		  $("#buscarPeriodo").unbind('click');
  		  $("#buscarPeriodo").click(function(){
  			  console.log(ide);
  			  var periodo=$("#txtPeriodo").val();
  			  if(periodo.length==0){
  				  $("#periodo").dialog('close');
  			  } else {
  				  if(periodo.length!=6){
  					  alert("El periodo es de 6 Digitos.");
		                  return false;
				      } else {
				    	  if(isNaN(periodo)){
				    		  alert("El periodo debe ser num\u00E9rico.");
		                      return false;
						  } else {
							  $("#periodo").dialog('close');
	                          $("#txtPeriodo").val('')	;  
	                          if(indexTab=='f')
	                        	  $('#tabs-6').load("tabs/tablaPlanillaU.php",{v0:ide,v1:periodo});
	                          else if(indexTab=='g')
	                        	  $("#tabs-7").load("tabs/tablaCuotaMonetaria.php",{v0:ide,v1:periodo});
		                  }//end else
					  }//end else
  			  }//end else
  		  }); //end click
  		  
  		  $("#periodo").dialog('open');
		}		
		
		//CARGAR TABLA DEACUERDO A TAB SELECCIONADO
		switch(indexTab){
			case "a": $("#tabs-1").load("tabs/tablaEmpresa.php",{v0:ide}); break;
			case "b": $("#tabs-2").load("tabs/tablaAportes.php",{v0:ide,v1:100}); break; 
			case "c": $("#tabs-3").load("tabs/tablaTrabajadores.php",{v0:ide}); break; 
			case "d": $("#tabs-4").load("tabs/tablaImagenesTab.php",{v0:ide}); break; 
			case "e": $("#tabs-5").load("tabs/tablaTrayec.php",{v0:ide}); break; 
			case "f": break;
			case "g": break;
			case "h": $("#tabs-8").load("tabs/tablaReclamosEmpTab.php",{v0:ide}); break;
			case "i": $("#tabs-9").load("tabs/TablaRadicacionesPendientesTab.php",{v0:nit}); break;
		}	//end switch		
	});
		
	$("div#icon span").trigger("click");
	$("#tabsE").tabs({spinner:'<em>Loading&#8230;</em>',selected:-1});//end tab
}//END FUNCTION BUSCAR NIT

//METODO PARA MOSTRAR LA OBSERVACION DEL APORTES
function mostrarObservacion(id){
	$("#div-observaciones-aporte").dialog({
		autoOpen: true,
		width: 700,
		modal: false,
		title:"Observaci\u00F3n Aporte",
		open:function(evt, ui) {
			$( this ).html( $ ("#"+id ).html() );
		},
		buttons: {
			'Aceptar': function(){
				$( this ).dialog("destroy");
			}
		},
		close:function(){
			$( this ).dialog("destroy");
			$( this ).html("");
		}
	}); 
}

//...ABRIR DOCUMENTOS...
function imagen(imgAlt){
	
   //Capturo el objeto como un ojeto jquery	
	var img=jQuery(imgAlt);
	//Borror en el div #imagen alguna imagen
	$("#imagen").find("img").remove();
	$("#imagen").dialog('open');
	
	var srcImage= img.attr("alt");
	$("#imagen").append("<img src='"+srcImage+"'");
}//end function imagen		

/*
Ã� 	&Aacute; 	\u00C1
á 	&aacute; 	\u00E1
Ã‰ 	&Eacute; 	\u00C9
Ã© 	&eacute; 	\u00E9
Ã� 	&Iacute; 	\u00CD
Ã­ 	&iacute; 	\u00ED
Ó 	&Oacute; 	\u00D3
Ã³ 	&oacute; 	\u00F3
Ãš 	&Uacute; 	\u00DA
Ãº 	&uacute; 	\u00FA
Ãœ 	&Uuml;      \u00DC
Ã¼ 	&uuml; 	    \u00FC
á¹„ 	&Ntilde; 	\u00D1
Ã± 	&ntilde; 	\u00F1
*/ 