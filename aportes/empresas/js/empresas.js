// JavaScript Document
// autor Orlando Puentes A
// fecha mayo 20 de 2010
// objeto administraci\xf3n de los eventos del formulario de empresa.php
var nuevo=0;
var modificar=0;
var continuar=true;
var ccgerente=0;
var ccjefe=0;
var msg="";
var URL=src();

function isEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}

function validarCampos(op){
	var cadena="<ul>";
	
	var campo=$("#lisRadicacion").val();	//id radicacion;
	var campo1=$("#select").val();	//tipo documento 
	var campo2=$("#tNit").val();	//nit
	var campo3=$("#tDigito").val();	//digito
	var campo7=$("#textfield").val();	//razon social
	var campo8=$("#textfield2").val().slice(0,99);	//nombre comercial
	var campo9=$("#tDireccion").val();	//direccion
	var campo10=$("#cboDepto2").val();	//departamento
	var campo11=$("#cboCiudad2").val();	//ciudad
	var campo13=$("#textfield8").val();	//telefono
	var campo14=$("#textfield6").val();	//fax
	var campo15=$("#textfield9").val();	//url
	var campo16=$("#textfield15").val();	//email
	var campo18=$("#textfield4").val();	//idrepresentante
	var campo19=$("#textfield5").val();	//idcontacto
	var campo20=$("#select4").val();	//contratista
	var campo23=$("#select7").val();	//actividad economica
	var campo24=$("#lisCodigo").val();		//listado codigo ciu  				
	var campo25=$("#select11").val();	//indice de aportes
	var campo26=$("#select8").val();	// asesor
	var campo28=$("#datepicker").val();	//fecha matricula
	var campo29=$("#select13").val();	//clase de sociedad
	var campo30=$("#select12").val();	//sector	
	var campo31=$("#select10").val();	//seccional
	var campo33=$("#claseApo").val();	//clase aportante
	var campo34=$("#tipoApo").val();	//tipo aportante	
	var campo35=$("#select9").val();	//estado	
	var campo38=$("#fechaInicioAportes").val();	//fecha inicio de aportes
	var campo39=$("#textfield18").val();	//fecha afiliacion
	var campo50=$("#select13").val();	//clase persona
	var campo54=$("#tDirCorresp").val();	//direccion correspondencia
	var campo55=$("#cboDeptoCorresp").val();	//departamento de correspondencia
	var campo56=$("#cboCiudadCorresp").val();	//ciudad de correspondencia
	var campo57=$("#cboBarrioCA").val();	//barrio
	var campo58=$("#txtCelular").val();	//celular
	var campo59=$("#cboBarrioCorresp").val();	//barrio de correspondencia
	var campo60=$("#tipoTel").val();	//tipo telefono
	if(campo <= 0)
		cadena+="<li>Verifique el n&uacute;mero de radicaci&oacute;n</li>";

	if(campo30==0){
		cadena+="<li>Falta sector</li>";
	}
	/****************************************/
	if(campo29==0){
		cadena+="<li>Falta clase de sociedad</li>";
	}
	/*************************************************/
	if(campo2==0){
		cadena+="<li>Falta NIT</li>";
	}
	else{
		if(isNaN(campo2))
		cadena+="<li>El NIT debe ser num\u00E9rico!</li>";
		var long=campo2.length;
		if(long<5 || long>11)
		cadena+="<li>el NIT es de 5 a 11 d\xedgitos</li>";
	}
	/************************************************/
	if(campo3 == ''){
		cadena+="<li>Falta d\u00EDgito de verificaci\u00FAn</li>";
	}
	/************************************************/
	if(campo1==0){
		cadena+="<li>Falta tipo de documento</li>";
	}
	/***************************************************/
	if(campo7==0){
		cadena+="<li>Falta raz\u00F3n social</li>";
	}
	else{
		if(campo7.length<3)
		cadena+="<li>La raz\u00F3n social es muy corta!</li>";
	}
	/***************************************************/
	if(campo8==0){
		cadena+="<li>Falta nombre comercial</li>";
	}
	else{
		if(campo8.length<3)
		cadena+="<li>La raz\u00FAn social es muy corta!</li>";
	}
	/***************************************************/
	if(campo9==0){
		cadena+="<li>Falta direcci\u00F3n</li>";
	}else{
		// Para las veredas, fincas y kilómetros no se requiere barrio
		patronBarrio = new RegExp(/(vereda|finca|km)/i);
		if(!patronBarrio.test(campo9)){
			if(campo9.length<10)
				cadena+="<li>La direcci\u00F3n es muy corta!</li>";	
		}
	}
	/***************************************************/
	if(campo10==0){
		cadena+="<li>Falta departamento</li>";
	}
	/***************************************************/
	if(campo11==0){
		cadena+="<li>Falta ciudad</li>";
	}	
	/*************************************************/
	if(campo13==0){
		cadena+="<li>Falta el tel\u00E9fono</li>";
	}
	else{
		if(isNaN(campo13))
			cadena+="<li>El tel\u00E9fono debe ser num\u00E9rico!</li>";
			var long=campo13.length;
			if(long<7 || long>10)
				cadena+="<li>el tel\u00E9fono es de 7 a 10 d\u00EDgitos</li>";
	}
	/*************************************************/
	if(campo14>0){
		if(isNaN(campo14))
		cadena+="<li>El fax debe ser num\u00E9rico!</li>";
			var long=campo14.length;
		if(long<7)
			cadena+="<li>el fax es de 7 d\xedgitos</li>";
	}
	/************************************************
	
	14 www y 15 email
	
	*/
	/*************************************************/
	if(!isEmail(campo16)){
		cadena+="<li>Falta el Email o Email incorrecto</li>";
	}
	/*************************************************/
	if(campo18==0){
		cadena+="<li>Falta identificaci\u00F3n del representante legal</li>";
	}
	/****************************************/
	if(campo20==0){
		cadena+="<li>Falta contratista</li>";
	}
	/****************************************/
	
	if(campo23==0 && campo34==2656){
		cadena+="<li>Falta actividad</li>";
	}
	/****************************************/
	if(campo25==0){
		cadena+="<li>Falta indice de aportes</li>";
	}
	/****************************************/
	
	if(campo31==0){
		cadena+="<li>Falta seccional</li>";
	}
	/****************************************/
	if(campo35==0){
		cadena+="<li>Falta estado</li>";
	}
	if(campo28.length==0){
		cadena+="<li>Fecha Constitucion</li>";
	}
	if(campo24==0){
		cadena+="<li>Falta codigo CIU</li>";
	}
	
	if(campo33==0){
		cadena+="<li>Falta Clase de Aportante</li>";
	}
	if(campo34==0){
		cadena+="<li>Falta Tipo Aportante</li>";
	}
	if(campo54==0){
		cadena+="<li>Falta Direccion de Correspondencia</li>";
	}
	/***************************************************/
	if(campo55==0){
		cadena+="<li>Falta departamento de correspondencia</li>";
	}
	/***************************************************/
	if(campo56==0){
		cadena+="<li>Falta ciudad de correspondencia</li>";
	}
	/*************************************************/
	if(campo57==0){
		cadena+="<li>Falta Barrio</li>";
	}
	if(!campo59){
		cadena+="<li>Falta Barrio Correspondencia</li>";
	}
	/*************************************************/
	if(campo13==0 && campo58==0){
		cadena+="<li>Falta el tel\u00E9fono o Celular</li>";
	}
	else{
		if(campo13!=0){
			if(isNaN(campo13)){
				cadena+="<li>El tel\u00E9fono debe ser num\u00E9rico!</li>";
				var long=campo13.length;
				if(long<7 || long>10){
					cadena+="<li>el tel\u00E9fono es de 7 a 10 d\u00EDgitos</li>";
				}
			}
		}
		if(campo58!=0){
			if(isNaN(campo58)){
				cadena+="<li>El Celular debe ser num\u00E9rico!</li>";
				var long=campo58.length;
				if(long<10 || long>10){
					cadena+="<li>el Celular es de 10 d\u00EDgitos</li>";
				}
			}
		}
		
	}
	/*************************************************/
	if(campo60==0){
		cadena+="<li>Falta tipo de Telefono</li>";
	}
	
	if(campo38.length==0)
		cadena+="<li>Falta Fecha de inicio de aportes</li>";
	
	
	/******************** FIN ****************/
	if(cadena.length>10){
		cadena+="</ul>";
		cadena="Hay errores en los datos, por favor revisar:"+cadena;
		document.getElementById('error').innerHTML=cadena;
		return;
	}
	else{
		cadena="Datos completos!";
		document.getElementById('error').innerHTML=cadena;
	}
	guardarEmpresa();
}

	
function nuevoR(){
	nuevo=1;
	$("#bGuardar").show();
	limpiarCampos();
	$("#select9").val("A");
	$("#nombre1").html("");
	$("#nombre2").html("");
	$("#ciu").val("");
	msg="";
	document.getElementById('error').innerHTML="";
	var campo0=30;
	$('#lisRadicacion option').remove();
	$.post(URL+'phpComunes/buscarRadicaTipoID.php',{v0:campo0},function(datos){
    	var cmbRadicacion = $("#lisRadicacion");
	   	$(cmbRadicacion).html(datos);
	   	if($(cmbRadicacion).children().length==1){
			alert('Lo lamento NO hay radicaciones pendientes de grabar!');
			return false;
         }
	});
	$("#lisRadicacion").focus();
}

function buscarRadicacion(obj){
	if(obj.value >0){
		$.getJSON('buscarRadicacion.php', {v0:obj.value}, function(datos){
			if(datos.length > 0){
				$.each(datos,function(i,fila){
					$("#tNit").val(fila.nit).trigger("blur");
					$("#tIdRadicacion").val(fila.idtiporadicacion);
					$("#tNit2").val(fila.nit);
					$("#tFechaRadicacion").val(fila.fechasistema);
					$("#textfield18").val(fila.fecharadicacion);
					$.getJSON('buscarEmpresaNIT.php',{v0: fila.nit},function(respuesta){
						if(respuesta != 0){
							$.each(respuesta,function(c,empresa){
								$("#textfield").val(empresa.razonsocial);
								$("#cboDepto2").val(empresa.iddepartamento);
								$("#cboDepto2").trigger("change");
								var deptoElegido = $("#cboDepto2").val();
								$.post(URL+"phpComunes/ciudades.php", { elegido: deptoElegido}, function(data){
									$("#cboCiudad2").html(data);
									$("#cboCiudad2").val(empresa.idciudad);
									$("#cboCiudad2").trigger("change");
								});
								$("#cboDeptoCorresp").val(empresa.iddepcorresp);
								$("#cboDeptoCorresp").trigger("change");
								var deptoElegido1 = $("#cboDeptoCorresp").val();
								$.post(URL+"phpComunes/ciudades.php", { elegido: deptoElegido1}, function(data){
									$("#cboCiudadCorresp").html(data);
									$("#cboCiudadCorresp").val(empresa.idciucorresp);
									$("#cboCiudadCorresp").trigger("change");
								});
								$("#tipoTel").val(empresa.id_tipo_tel);	//tipo telefono
								$("#tDireccion").val(empresa.direccion);
								$("#tDirCorresp").val(empresa.direcorresp);
								$("#textfield8").val(empresa.telefono);
								$("#textfield6").val(empresa.fax);
								$("#textfield9").val(empresa.url);
								$("#textfield15").val(empresa.email);
								
								// buscar representante legal
								if(Number(empresa.idrepresentante) > 0){
									$.getJSON('../../phpComunes/llenarCampos.php',{v0: empresa.idrepresentante},function(resPersona1){
										if(resPersona1 != 0){
											var persona = resPersona1[0];
											$("#textfield4").val(persona.identificacion);
											var nom = $.trim(persona.pnombre) +" "+ $.trim(persona.snombre) +" "+ $.trim(persona.papellido) +" "+ $.trim(persona.sapellido);
											$("#nombre1").html(nom);
										}
									});
								}

								// buscar jefe de personal
								if(Number(empresa.idjefepersonal) > 0){
									$.getJSON('../../phpComunes/llenarCampos.php',{v0: empresa.idjefepersonal},function(resPersona2){
										if(resPersona2 != 0){
											var persona2 = resPersona2[0];
											$("#textfield5").val(persona2.identificacion);
											var nom2 = $.trim(persona2.pnombre) +" "+ $.trim(persona2.snombre) +" "+ $.trim(persona2.papellido) +" "+ $.trim(persona2.sapellido);
											$("#nombre2").html(nom2);
										}
									});
								}						

								$("#select4").val(empresa.contratista);
								$("#select7").val(empresa.idcodigoactividad);
								$("#lisCodigo").val(empresa.actieconomicadane);
								
								// se debe obtener el código de actividad, no el id del registro de la tabla
								$.getJSON('buscarCIU.php',{iddefinicion: empresa.actieconomicadane},function(resultado){
									if(resultado > 0){
										$("#codigoCIU").val(resultado[0].idciiu);
									}
								});
								
								$.getJSON(URL+'phpComunes/buscarPeriodoMenorAporte.php',{v0: empresa.idempresa},function(respta){
									$.each(respta,function(c,per){
										if(per.mperiodo>0)
										{
											str1=per.mperiodo.substring(0,4)+"-"+per.mperiodo.substring(4,6)+"-01";
											$("#fechaInicioAportes").val(str1);
											$("#fechaInicioAportes").attr("disabled",true);
										}
										else
										{
											$("#fechaInicioAportes").attr("disabled",false);
											$("#fechaInicioAportes").val(empresa.fechaaportes);
										}
									});
								});
								
								$("#select10").val(empresa.seccional);
								$("#select11").val(empresa.indicador);
								$("#datepicker").val(empresa.fechamatricula); // fecha de constitución toma el valor de fechamatricula
								$("#select8").val(empresa.idasesor); // asesor
								$("#select5").val(); //@todo qué campo es?
								$("#select6").val(); //@todo qué campo es?
								$("#select9").val(empresa.estado);
								$("#claseApo").val(empresa.claseaportante);
								$("#tipoApo").val(empresa.tipoaportante);
							});
						}
					});
				});
			}else{
				alert("No se encuentra la radicaci\xf3n.");
				$("lisRadicacion").focus();
				return false;
			}
		});
	}else{
		alert("Debe seleccionar una radicaci\xf3n para continuar.");
		$("lisRadicacion").focus();
		return false;
	}	
}

/**
 * Copia el valor del primer objeto en el segundo
 * @param obj1 primer elemento
 * @param obj2 segundo elemento
 */
function copiar(obj1,obj2){
	obj1 = $("#"+obj1.id);
	var longitud = (obj2.maxLength > 0)?obj2.maxLength:null;
	var valor = obj1.val();
	if(longitud != null)
		valor = obj1.val().slice(0,longitud);
	obj2.val(valor);
}
//Duplicar los combos de municipio y departamento de correspondencia
function DuplicarCiudadCorresp(){
	document.getElementById('cboDeptoCorresp').value = document.getElementById('cboDepto2').value;		
	var dato=document.getElementById('cboCiudad2').value;
	
	var deptoElegido1 = $("#cboDeptoCorresp").val();
	$.post(URL+"phpComunes/ciudades.php", { elegido: deptoElegido1}, function(data){
		$("#cboCiudadCorresp").html(data);
		$("#cboCiudadCorresp").val(dato);
		
	});
	
	$.post(URL+"phpComunes/zonas.php", { elegido: dato}, function(data){
		$("#cboZonaCorresp").html(data);		
	});
	
}

//Duplicar los combos de zona y departamento de correspondencia
function DuplicarBarrioCorresp(){
	var idZona = $("#cboZonaCA").val();
	var idBarrio = $("#cboBarrioCA").val();
	$("#cboZonaCorresp").val(idZona);
	
	$.post(URL+"phpComunes/barrios.php", { elegido: idZona}, function(data){
		$("#cboBarrioCorresp").html(data);
		$("#cboBarrioCorresp").val(idBarrio);
	});	
}

/*function notas(){
	$("#dialog-form2").dialog('open');
	}
	*/
/*function buscarNitInforma(){
	var campo0= $("#tNit").val();
	$.ajax({
		url: 'buscarInforma.php',
		type: "POST",
		data: "submit=&v0="+campo0,
		success: function(datos){
			if(datos=='1'){ 
				msg+="El NIT ya existe en INFORMA WEB \r\n";
				alert(msg);
				guardarEmpresa();
				}
			else{
				//alert("EL NIT no existe" + campo0)
				guardarEmpresaInforma();
				}
		}
	});
	return false;
}*/

function buscarPersona(objeto,op){
	if(nuevo==0){
		alert("Si es un registro nuevo haga click en NUEVO!!");
		return;
	}
	var numero=parseInt(objeto.value);
	if(isNaN(numero)) return;
	$.ajax({
		url: URL+'phpComunes/pdo.buscar.persona.php',
		cache: false,
		type: "GET",
		data: {v0:1,v1:numero,v2:1},
		dataType: "json",
		success: function(datos){
			if(op==0){
				if(datos==0){
					$("#nombre1").val('');
					alert("No existe en nuestra base, registre los datos completos!");	
					persona_simple(document.getElementById('textfield4'),document.getElementById('idRepresemtante'));
				}else{
					nom = datos[0].pnombre+" "+datos[0].snombre+" "+datos[0].papellido+" "+datos[0].sapellido;
					$("#idRepresemtante").val(datos[0].idpersona);
					$("#nombre1").val(nom);
				}
			}else{
				if(datos==0){
					$("#nombre2").val('');
					alert("No existe en nuestra base, registre los datos completos!");	
					persona_simple(document.getElementById('textfield5'),document.getElementById('idRepresemtante'));
				}
					else{
					nom=datos[0].pnombre+" "+datos[0].snombre+" "+datos[0].papellido+" "+datos[0].sapellido;
					$("#idContacto").val(datos[0].idpersona);
					$("#nombre2").val(nom);
				}
			}
		}
	});
}

function guardarEmpresa(){
	$("#bGuardar").hide();
	var campo=$("#lisRadicacion").val();	//id radicacion;
	var campo1=$("#select").val();	//tipo documento 
	var campo2=$("#tNit").val();	//nit
	var campo3=$("#tDigito").val();	//digito
	var campo4=$("#tNit").val();	//nit rsn
	var campo5='000';	//sucursal
	var campo6='S';
	var campo7=$("#textfield").val();	//razon social
	var campo8=$("#textfield2").val();	//nombre comercial
	var campo9=$("#tDireccion").val();	//direccion
	var campo10=$("#cboDepto2").val();	//departamento
	var campo11=$("#cboCiudad2").val();	//ciudad
	var campo13=$("#textfield8").val();	//telefono
	var campo14=$("#textfield6").val();	//fax
	var campo15=$("#textfield9").val();	//url
	var campo16=$("#textfield15").val();	//email
	var campo18=$("#idRepresemtante").val();	//idrepresentante
	var campo19=$("#idContacto").val();	//idcontacto
	var campo20=$("#select4").val();	//contratista
	var campo23=$("#select7").val();	//actividad economica
	var campo24=$("#lisCodigo").val();		//listado codigo ciu  				
	var campo25=$("#select11").val();	//indice de aportes
	var campo26=$("#select8").val();	// asesor
	var campo28=$("#datepicker").val();	//fecha matricula
	var campo29=$("#select13").val();	//clase de sociedad
	var campo30=$("#select12").val();	//sector	
	var campo31=$("#select10").val();	//seccional
	var campo32=$("#select13").val();	//tipo persona
	var campo33=$("#claseApo").val();	//clase aportante
	var campo34=$("#tipoApo").val();	//tipo aportante	
	var campo35=$("#select9").val();	//estado
	var campo38=$("#fechaInicioAportes").val();	//fecha aportes
	var campo39=$("#textfield18").val();	//fecha afiliacion
	var campo50=$("#select13").val();	//clase persona
	var campo52='S';	//legalizada
	var campo53='N';	//renovacion
	var campo54=$("#tDirCorresp").val();	//direccion correspondencia
	var campo55=$("#cboDeptoCorresp").val();	//departamento correspondencia
	var campo56=$("#cboCiudadCorresp").val();	//ciudad correspondencia
	var campo57=$("#cboBarrioCA").val();	//barrio
	var campo58=$("#txtCelular").val();	//Celular
	var campo59=$("#cboBarrioCorresp").val();
	var campo60=$("#tipoTel").val();	//tipo telefono
	campo7 = campo7.replace(/&/g,38); //reemplazar & por 38
	campo8 = campo8.replace(/&/g,38); 
	//se debe verificar si la empresa ya existe, si es así, se debe actualizar, si no, se graba la empresa nueva.
	$.getJSON('buscarEmpresaNIT.php',{v0: campo2},function(respuesta){
		if(respuesta != 0){
			$.each(respuesta,function(c,empresa){
				$.ajax({
					url: URL+'phpComunes/pdo.update.empresa.php',
                                        async: false,
					type: "POST",
					data: {v0:empresa.idempresa,v1:campo1,v2:campo2,v3:campo3,v4:campo4,v5:campo5,v6:campo6,v7:campo7,v8:campo8,v9:campo9,v10:campo10,v11:campo11,v13:campo13,v14:campo14,v15:campo15,v16:campo16,v18:campo18,v19:campo19,v20:campo20,v23:campo23,v24:campo24,v25:campo25,v26:campo26,v28:campo28,v29:campo29,v30:campo30,v31:campo31,v32:campo32,v33:campo33,v34:campo34,v35:campo35,v38:campo38,v39:campo39,v50:campo50,v52:campo52,v53:campo53,v54:campo54,v55:campo55,v56:campo56,v57:campo57,v58:campo58,v59:campo59,v60:campo60},
					success: function(datos){
						if(datos>0){
							console.log("procesando terceros");
                                                        msg += "Se guard\u00F3 la empresa principal (Sucursal principal)\r\n";
							alert(msg);
							mostrarDialogoObservaciones(empresa.idempresa,2);
							//cerrarRadicacion();
						}else{
							$("#bGuardar").show();
							alert("ERROR Actualizando Datos "+datos);
						}
					}
				});
			});
		}else{
			$.ajax({
				url: URL+'phpComunes/pdo.insert.empresa.php',
                                async: false,
				type: "POST",
				data: {v1:campo1,v2:campo2,v3:campo3,v4:campo4,v5:campo5,v6:campo6,v7:campo7,v8:campo8,v9:campo9,v10:campo10,v11:campo11,v13:campo13,v14:campo14,v15:campo15,v16:campo16,v18:campo18,v19:campo19,v20:campo20,v23:campo23,v24:campo24,v25:campo25,v26:campo26,v28:campo28,v29:campo29,v30:campo30,v31:campo31,v32:campo32,v33:campo33,v34:campo34,v35:campo35,v38:campo38,v39:campo39,v50:campo50,v52:campo52,v53:campo53,v54:campo54,v55:campo55,v56:campo56,v57:campo57,v58:campo58,v59:campo59,v60:campo60},
				success: function(datos){
					if(datos>0){
						console.log("procesando terceros");
						msg+="Se guard\u00F3 la empresa principal (Sucursal principal).\r\n"; // es la misma sucursal 000
						alert(msg);
						mostrarDialogoObservaciones(datos,2);
						//cerrarRadicacion();
						}
					else{
						$("#bGuardar").show();
						alert("ERROR Guardando Datos"+datos);
					}
				}
			});
		}
                
				/*
                //Llamado del proceso de creación de tercero en JDE 
                if(validarExistenciaTerceroJDE(campo1,campo2,campo3) == false){
                    //Se realiza el proceso de creación del tercero
                    $.ajax({    
                        url: URL+'Terceros/Process.php',
                        type: "POST",
                        async:false,
                        dataType:"json",
                        data: {
                                action:"New",
                                type:"Terceros",
                                campo1:campo1,
                                campo2:campo2,
                                campo3:campo3,
                                campo7:campo7,
                                campo9:campo9,
                                campo10:campo10,
                                campo11:campo11,
                                campo12:$("#cboCiudad2 [value='"+campo11+"']").text(),
                                campo13:campo13,
                                campo16:campo16
                        },
                        success: function(result){
                                console.log(result)
                                var payload=$.parseJSON(result.payload);
                                console.log(payload);
                                if(result.error)
                                {
                                        alert("Error grabando en JD Edwards!\r\nMire la consola para mas informacion");
                                }
                                else
                                {
                                        alert("Se ha registrado el tercero con n\u00famero de identificaci\u00f3n " + campo2 + " en JDE con el AN8 " + result.AN8Tercero);
                                }
                        }
                    });   
                }
				*/
                cerrarRadicacion();
	});
	return false;
}
/*
function guardarEmpresaInforma(){
	var campo0 =document.forms[0].elements[4].value;
	var campo1 =document.forms[0].elements[7].value;
	var campo2 =document.forms[0].elements[9].value;
	var campo3 =document.forms[0].elements[12].value;
	var campo4 =document.forms[0].elements[11].value;
	var campo5 =document.forms[0].elements[16].value;
	var campo6 =document.forms[0].elements[15].value;
	var campo7 =document.forms[0].elements[13].value;
	var campo8 =document.forms[0].elements[10].value;
	$.ajax({
		url: 'empresasGInforma.php',
		type: "POST",
		data: "submit=&v0="+campo0+"&v1="+campo1+"&v2="+campo2+"&v3="+campo3+"&v4="+campo4+"&v5="+campo5+"&v6="+campo6+"&v7="+campo7+"&v8="+campo8,
		success: function(datos){
			if(datos=='1'){
				msg+="Se guard\u00F3 la Empresa en Informa Web \r\n";
				alert(msg);
				}
			else{
				msg+="NO se pudo guardar en Informa Web! \r\n";
				alert("No se pudo guardar la empresa en Informa WEB!, intente de nuevo");
				return false;
				}
			guardarEmpresa();
		}
	});
	return false;
	}
*/
function digito(obj){
	var vpri, x, y, z, i, nit1, dv1;
 	nit1=obj.value;
 	
 	if(nit1 != ''){
 		if (isNaN(nit1)){
	 		alert('El valor digitado no es un n\u00FAmero v\u00E1lido: '+nit1);
	 		obj.value = "";
	 	}else {
			vpri = new Array(16);
			x=0 ; y=0 ; z=nit1.length ;
			vpri[1]=3;
			vpri[2]=7;
			vpri[3]=13;
			vpri[4]=17;
			vpri[5]=19;
			vpri[6]=23;
			vpri[7]=29;
			vpri[8]=37;
			vpri[9]=41;
			vpri[10]=43;
			vpri[11]=47;
			vpri[12]=53;
			vpri[13]=59;
			vpri[14]=67;
			vpri[15]=71;
			for(i=0 ; i<z ; i++) {
				y=(nit1.substr(i,1));
				x+=(y*vpri[z-i]);
			}
			y=x%11;
			if (y > 1){
				dv1=11-y;
			}else{
				dv1=y;
			}
			document.forma.tDigito.value=dv1;
		}
 	}else{
 		obj.value = "";
 		document.forma.tDigito.value = "";
 	}
 	
 	
} 

function cerrarRadicacion(){
    var campo0=document.forms[0].elements[0].value;
	$.ajax({
		url: URL+'phpComunes/cerrarRadicacion.php',
		type: "POST",
		data: "submit=&v0="+campo0,
		success: function(datos){
			limpiarCampos();
			$("#nombre1").html("");
	        $("#nombre2").html("");
	        $("#ciu").val("");
			alert(datos);
		}
	});
	return false;
	  }
	  
//Dialog Colaboracion en linea
function notas(){
	$("#dialog-form2").dialog('open');
}

$("#dialog-form2").dialog({
		autoOpen: false,
		height: 400,
		width: 500,
		modal: true,
		buttons: {
			'Enviar': function() {
			var bValid = true;
			var campo=$('#notas').val();
			var campo0=$.trim(campo);
			if (campo0==""){
				$(this).dialog('close');
					return false;
				}
			var campo1=$('#usuario').val();
			var campo2="empresas.php";
			$.post(URL+'phpComunes/colaboracion.php',{v0:campo0,v1:campo1,v2:campo2},
			function(datos){
				if(datos=='1'){
					alert("Su comentario fue enviado correctamente!. Gracias por participar en el mejoramiento de SIGAS");
				}else{
					alert(datos);
				}
		});
		$(this).dialog('close');
		},
		Cancelar: function() {
			$(this).dialog('close');
			$("#dialog-form2").dialog("destroy");
		}
	}
	});

function mostrarDialogoObservaciones(id,tipo){
	var URL=src();
	$("div[name='div-observaciones-tab']").dialog("destroy");
	$("textarea[name='observacionGrupo']").val("");
	$("div[name='div-observaciones-tab']").dialog({
		  modal:true,
		  width:520,
		  resizable:false,
		  draggable:false,
		  closeOnEscape:false,
		  open:function(){
			  $(this).prev().children().hide();//Oculto la barra de titulo
			  $(this).prev().html("OBSERVACIONES...");//Pongo un nuevo titulo
			  $("textarea[name='observacionGrupo']").focus();
			  },
		  buttons:{
		    'Guardar':function(){
				var observacion=$("textarea[name='observacionGrupo']").val();
				//Si observacion tiene algun valor, se guarda de lo contrario no pasa nada
				if(observacion!=""){
					$.ajax({
						url:URL+"phpComunes/pdo.insert.notas.php",
						type:"POST",
						data:{v0:id,v1:observacion,v2:tipo},
						success:function(data){
							$("[name='rtaObservacion']").fadeIn();//el mensaje esta oculto en la capa div-observacion
							$("div[name='div-observaciones-tab']").dialog("close");
						}
					});//ajax
				}//if observacion
				else{
					alert("No se pudo guardar la observaci\xf3n");
					$("div[name='div-observaciones-tab']").dialog("close");
				}
		    }//guardar
		}//buttons
	});
}

/*
Ã� 	&Aacute; 	\u00C1
á 	&aacute; 	\u00E1
Ã‰ 	&Eacute; 	\u00C9
Ã© 	&eacute; 	\u00E9
Ã� 	&Iacute; 	\u00CD
Ã­ 	&iacute; 	\u00ED
Ó 	&Oacute; 	\u00D3
Ã³ 	&oacute; 	\u00F3
Ãš 	&Uacute; 	\u00DA
Ãº 	&uacute; 	\u00FA
Ãœ 	&Uuml; 	\u00DC
Ã¼ 	&uuml; 	\u00FC
á¹„ 	&Ntilde; 	\u00D1
Ã± 	&ntilde; 	\u00F1
*/

function validarExistenciaTerceroJDE(tipoDocumento,numeroIdentificacion,digitoVerificiacion){
    var existenciaTercero = false;
    $.ajax({
        url: 'buscarEmpresaNITJDE.php',
        type: "POST",
        dataType:"json",
        async:false,
        data: {
                tipoDocumento:tipoDocumento,
                nit:numeroIdentificacion,
                digitoVerificacion:digitoVerificiacion,
        },
        success: function(json){
            var an8JDE = json.AN8Tercero;
            
            if(an8JDE == "" || an8JDE == null || an8JDE == "null"){
                alert("La empresa con el n\u00famero de identificaci\u00f3n " + numeroIdentificacion + " no existe en JD Edwards");
            }else{
                alert("La empresa ya existe en JD Edwards con el AN8 " +an8JDE);
                existenciaTercero = true;
            }
        }
    });
    return existenciaTercero;
}