/*
* @autor:      Ing. Orlando Puentes
* @fecha:      6/10/2010
* objetivo:
*/
// JavaScript Document
var visualisar=0;
var idempresa=0;
var idrepresentante=0;
var idjefe=0;
var msg="";

$(function() {
		$('#datepicker,#fechaMatricula,#fechaEstado').datepicker({
			changeMonth: true,
			changeYear: true,
			maxDate: "+0D"
		});
		var anoActual = new Date();
		var strYearRange = anoActual.getFullYear()-120 +":"+ anoActual.getFullYear();
		$(".box1").datepicker( "option", "yearRange", strYearRange );
	});
	
$(function(){
	$("#combo1").change(function () {
		$("#combo1 option:selected").each(function () {
				elegido=$(this).val();
				$.post("../../phpComunes/ciudades.php", { elegido: elegido }, function(data){
				$("#combo2").html(data);
				$("#combo3").html("");
			});			
		});
	});
});

// Parametros para el combo2
$("#combo2").change(function () {
	$("#combo2 option:selected").each(function () {
		elegido=$(this).val();
		$.post("combo2.php", { elegido: elegido }, function(data){
			$("#combo3").html(data);
		});			
	});
});

$(function() {
	$("#dialog-form2").dialog("destroy");
	$("#dialog-form2").dialog({
		autoOpen: false,
		height: 400,
		width: 500,
		modal: true,
		buttons: {
			'Enviar': function() {
			var bValid = true;
			var campo=$('#notas').val();
			var campo0=$.trim(campo);
			if (campo0==""){
				$(this).dialog('close');
					return false;
				}
			var campo1=$('#usuario').val();
			var campo2="actulizacionDatos.php";
			$.post('../../phpComunes/colaboracion.php',{v0:campo0,v1:campo1,v2:campo2}, 								
			function(datos){
				if(datos=='1'){
					alert("Su comentario fue enviado correctamente!. Gracias por participar en el mejoramiento de SIGAS");
				}else{
					alert(datos);
				}
		});
		$(this).dialog('close');
		},
		Cancelar: function() {
			$(this).dialog('close');
			$("#dialog-form2").dialog("destroy");
		}
	}		
	});
	
	$('#enviar-notas')
		.button()
		.click(function() {
		$('#dialog-form2').dialog('open');
	});
});

$(function() {
		$("#ayuda").dialog({
		 	autoOpen: false,
			height: 450, 
			width: 800, 
			draggable:true,
			open: function(evt, ui){
					$('#ayuda').html('');
					$.get('../../help/aportes/ayudaActualizarDatos.html',function(data){
							$('#ayuda').html(data);
					});
			 }
		});
	});

function consultaDatos(){
	if(document.forms[0].elements[0].value.length==0){
		alert("Entre un NIT a consultar!");
		document.forms[0].elements[0].focus();
		return false;
		}
	if(document.forms[0].elements[1].value==0){
		alert("Entre la sucursal a consultar!");
		document.forms[0].elements[2].focus();
		return false;
		}
	}

function buscarEmpresa(ide){
	idempresa=ide;
	document.getElementById('div-sucursales').innerHTML="";
	var campo0=$("#txtNit").val();
	$.getJSON('buscarEmpresa1.php',{v0:ide},function(datos){
	mostrar(datos);
	$("#Idempresa").val(ide);
	$.post('buscarPrincipal.php',{v0:campo0},
		function(datos){
	$.getJSON(URL+'phpComunes/pdo.buscar.notas.php',{v0:ide,v1:'2'},function(datos){
				document.getElementById('div-observaciones').innerHTML=datos;
		});
		});		
	});
}


function copiar(obj1,obj2){
	obj2.val(obj1.value);
	}

function mostrar(datos){	
	var cont=0;
	limpiar();
	$.each(datos, function(i, fila){
		$("#select12").val(fila.idsector);
		$("#select13").val(fila.idclasesociedad);
		$("#tNit").val($.trim(fila.nit));
		$("#tDigito").val($.trim(fila.digito));
		$("#select").val(fila.idtipodocumento);
		$("#textfield").val($.trim(fila.razonsocial));
		$("#textfield2").val($.trim(fila.sigla));
		$("#cboDepto2").val($.trim(fila.iddepartamento)).trigger('change');
		
		var ciudad=$.trim(fila.idciudad);
		setTimeout((function(){
			$("#cboCiudad2").val(ciudad);
			$("#cboCiudad2").trigger("change");
		 }),1000);
		
		var codzona=$.trim(fila.codzona);
		setTimeout((function(){
			$("#cboZonaCA").val(codzona);
			$("#cboZonaCA").trigger("change");
		 }),2000);
		
		var idbarrio=$.trim(fila.idbarrio);
		setTimeout((function(){
			$("#cboBarrioCA").val(idbarrio);
		 }),3000);
		
		$("#tDireccion").val($.trim(fila.direccion));
		$("#textfield8").val($.trim(fila.telefono));
		$("#textfield6").val($.trim(fila.fax));
		$("#textfield9").val($.trim(fila.url));
		$("#textfield15").val($.trim(fila.email));
		$("#textfield4").val($.trim(fila.identificacionrep));
		idrepresentante=$.trim(fila.idrepresentante);
		$("#nombre1").html($.trim(fila.pnombre)+" "+$.trim(fila.snombre)+" "+$.trim(fila.papellido)+" "+$.trim(fila.sapellido));
		$("#textfield5").val(fila.identificacionjp);
		idjefe=$.trim(fila.idjefepersonal);
		$("#nombre2").html($.trim(fila.pnjp)+" "+$.trim(fila.snjp)+" "+$.trim(fila.pajp)+" "+$.trim(fila.sajp));
		$("#select4").val(fila.contratista);
		$("#select7").val(fila.idcodigoactividad);
		$("#select5").val(fila.colegio);
		$("#select6").val(fila.exento);
		/////////////////////////////Mostrar CIU
		$("#codigoCIU").val(fila.actieconomicadane).trigger("blur");//codigo ciu- actieconomicadane
		/*	No hace nada
		// se debe obtener el c�digo de actividad, no el id del registro de la tabla
		$.getJSON('buscarCIU.php',{iddefinicion: fila.actieconomicadane},function(idDetalleActividad){
			if(idDetalleActividad != 0){
				$("#codigoCIU").val(idDetalleActividad);//codigo ciu- actieconomicadane
				$("#codigoCIU").trigger("blur");
			}
		});
		*/
		$("#select11").val(fila.indicador);
		$("#select10").val(fila.seccional);
		$("#textfield18").val($.trim(fila.fechaafiliacion));
		$("#datepicker").val($.trim(fila.fechaaportes));
		$("#select8").val(fila.idasesor);
	
		$("#select9").val(fila.estado);
		$("#fechaEstado").val("");
		$("#cmbCausalInactivacion").val("");
		$("#rowInactivo").hide();
		
		$("#textfield19").val(fila.usuario);
		$("#fechaMatricula").val(fila.fechamatricula);
		$("#cmbClaseApo").val(fila.claseaportante); //clase de aportante
		$("#cmbTipoApo").val(fila.tipoaportante); //tipo de aportante
		$("#tipoTel").val($.trim(fila.id_tipo_tel));

		$("#tDirCorresp").val($.trim(fila.direcorresp));
		$("#cboDeptoCorresp").val($.trim(fila.iddepcorresp)).trigger('change');
		var ciucorresp=$.trim(fila.idciucorresp);
		setTimeout((function(){
			$("#cboCiudadCorresp").val(ciucorresp);
			$("#cboCiudadCorresp").trigger("change");
		 }),1000);
		
		var codzonacorresp=$.trim(fila.codzonacorresp);
		setTimeout((function(){
			$("#cboZonaCorresp").val(codzonacorresp);
			$("#cboZonaCorresp").trigger("change");
		 }),2000);
		
		var idbarriocorresp=$.trim(fila.idbarriocorresp);
		setTimeout((function(){
			$("#cboBarrioCorresp").val(idbarriocorresp);
		 }),3000);
		
		cont++;
	});
	if(cont==0){
		alert("No hay empresas con ese NIT");
		return false;
	}
}

//Duplicar los combos de municipio y departamento de correspondencia
function DuplicarCiudadCorresp(){
	document.getElementById('cboDeptoCorresp').value = document.getElementById('cboDepto2').value;		
	var dato=document.getElementById('cboCiudad2').value;
	
	var deptoElegido1 = $("#cboDeptoCorresp").val();
	$.post(URL+"phpComunes/ciudades.php", { elegido: deptoElegido1}, function(data){
		$("#cboCiudadCorresp").html(data);
		$("#cboCiudadCorresp").val(dato);
		
	});	
	
	$.post(URL+"phpComunes/zonas.php", { elegido: dato}, function(data){
		$("#cboZonaCorresp").html(data);		
	});
	
}

//Duplicar los combos de zona y departamento de correspondencia
function DuplicarBarrioCorresp(){
	var idZona = $("#cboZonaCA").val();
	var idBarrio = $("#cboBarrioCA").val();
	$("#cboZonaCorresp").val(idZona);
	
	$.post(URL+"phpComunes/barrios.php", { elegido: idZona}, function(data){
		$("#cboBarrioCorresp").html(data);
		$("#cboBarrioCorresp").val(idBarrio);
	});	
}

function limpiar(){
	limpiarCampos();
	document.getElementById('div-observaciones').innerHTML="";
	document.getElementById('div-sucursales').innerHTML="";
	document.getElementById('nombre1').innerHTML="";
	document.getElementById('nombre2').innerHTML="";
	$("#textfield19").val("");
	$("#fechaEstado").val("");
	$("#cmbCausalInactivacion").val("");
	$("#rowInactivo").hide();
	}

function validarCampos(op){
	var cadena="<ul>";
	if($("#select12").val()==0){
		cadena+="<li>Falta sector</li>";
	}
	/****************************************/
	if($("#select13").val()==0){
		cadena+="<li>Falta clase de sociedad</li>";
	}
	/*************************************************/
	if($("#tNit").val()==""){
		cadena+="<li>Falta NIT</li>";
	}
	else{
		if(isNaN($("#tNit").val()))
		cadena+="<li>El NIT debe ser num\u00E9rico!</li>";
		var long=$("#tNit").val().length;
		if(long<5 || long>11)
		cadena+="<li>el NIT es de 5 a 11 d\xedgitos</li>";
	}
	/************************************************/
	if($("#tDigito").val==""){
		cadena+="<li>Falta d\u00EDgito de verificaci\u00FAn</li>";
	}
	/************************************************/
	if($("#select").val()==0){
		cadena+="<li>Falta tipo de documento</li>";
	}
	/***************************************************/
	if($("#textfield").val()==""){
		cadena+="<li>Falta raz\u00F3n social</li>";
	}
	else{
		if($("#textfield").val().length<3)
		cadena+="<li>La raz\u00F3n social es muy corta!</li>";
	}
	/***************************************************/
	if($("#textfield2").val()==0){
		cadena+="<li>Falta nombre comercial</li>";
	}
	else{
		if($("#textfield2").val().length<3)
		cadena+="<li>La raz\u00FAn social es muy corta!</li>";
	}
	/***************************************************/
	if($("#tDireccion").val()==""){
		cadena+="<li>Falta direcci\u00F3n</li>";
	}
	else{
		if($("#tDireccion").val().length<10)
		cadena+="<li>La direccion es muy corta!</li>";
	}
	/***************************************************/
	if($("#cboDepto2").val()==0){
		cadena+="<li>Falta departamento</li>";
	}
	/***************************************************/
	if($("#cboCiudad2").val()==0){
		cadena+="<li>Falta ciudad</li>";
	}
	if($("#cboBarrioCA").val()==0){
		cadena+="<li>Falta Barrio</li>";
	}
	/*************************************************/
	if($("#textfield8").val==""){
		cadena+="<li>Falta el tel\u00E9fono</li>";
	}
	else{
		if(isNaN($("#textfield8").val()))
		cadena+="<li>El tel\u00E9fono debe ser num\u00E9rico!</li>";
		var long=$("#textfield8").val().length;
		if(long<7 || long>10)
		cadena+="<li>el tel\u00E9fono es de 7 a 10 d\xedgitos</li>";
	}
	/*************************************************/
	if($("#textfield6").val()!=""){
		if(isNaN($("#textfield6").val()))
		cadena+="<li>El fax debe ser num\u00E9rico!</li>";
		var long=$("#textfield6").val().length;
		if(long<7)
		cadena+="<li>el fax es de 7 d\xedgitos</li>";
	}
	/************************************************/
	
	//14 www 
	
	/************************************************/
	/*
	var email=/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test($("#email").val());
	if($("#email").val()!=''){
	    if(!email) {
			cadena+='<li>El E-mail aparentemente es incorrecto.</li>';										 				
		}else{
		  email=$("#email").val();
	  }
	}
*/
	/*************************************************/
	if($("#textfield4").val()==""){
		cadena+="<li>Falta identificacion del representante legal</li>";
	}
	else{
		if(isNaN($("#textfield4").val()))
		cadena+="<li>La identificacin debe ser num\u00E9rico!</li>";
		var long=$("#textfield4").val().length;
		if(long<5 || long>11)
		cadena+="<li>La identificaci\u00FAn es de 5 a 11 d\xedgitos</li>";
	}
	
	/****************************************/
	if($("#select4").val()==0){
		cadena+="<li>Falta contratista</li>";
	}
	/****************************************/
	if($("#select5").val()==0){
		cadena+="<li>Falta colegio</li>";
	}
	/****************************************/
	if($("#select6").val()==0){
		cadena+="<li>Falta excento</li>";
	}
	/****************************************/
	if($("#select7").val()==0 && $("#cmbTipoApo").val()==2656){
		cadena+="<li>Falta actividad</li>";
	}
	/****************************************/
	if($("#select11").val()==0){
		cadena+="<li>Falta indice de aportes</li>";
	}
	/****************************************/
	/*if($("#select8").val()==0){
		cadena+="<li>Falta asesor</li>";
	}*/
	/****************************************/
	if($("#select10").val()==0){
		cadena+="<li>Falta seccional</li>";
	}
	/****************************************/
	if($("#select9").val()==0){
		cadena+="<li>Falta estado</li>";
	}
	//VALIDAR CAMPO FECHA ESTADO
	if($("#select9").val()=='I'){
		if($("#cmbCausalInactivacion").val() == 0 ){
			cadena+="<li>Falta causal de inactivaci&oacute;n</li>";
		}
		if($("#fechaEstado").val()==0){
			cadena+="<li>Falta fecha estado</li>";
		}
	}
	
	if($("#datepicker").val()==0){
		cadena+="<li>Falta fecha inicio aportes</li>";
	}
	/****************************************/
	if($("#cmbClaseApo").val()==0){
		cadena+="<li>Falta Clase Aportante</li>";
	}
	/****************************************/
	if($("#cmbTipoApo").val()==0){
		cadena+="<li>Falta Tipo Aportante</li>";
	}
	if($("#tDirCorresp").val()==0){
		cadena+="<li>Falta Direccion de Correspondencia</li>";
	}
	/***************************************************/
	if($("#cboDeptoCorresp").val()==0){
		cadena+="<li>Falta departamento de correspondencia</li>";
	}
	/***************************************************/
	if($("#cboCiudadCorresp").val()==0){
		cadena+="<li>Falta ciudad de correspondencia</li>";
	}
	if($("#cboBarrioCorresp").val()==0){
		cadena+="<li>Falta barrio de correspondencia</li>";
	}
	if($("#tipoTel").val()==0){
		cadena+="<li>Falta Tipo Telefono</li>";
	}
	/******************** FIN ****************/
	if(cadena.length>10){
		cadena+="</ul>";
		cadena="Hay errores en los datos, por favor revisar:"+cadena;
		document.getElementById('error').innerHTML=cadena;
		return;
	}
	else{
		cadena="Datos completos!";
		document.getElementById('error').innerHTML=cadena;
		actualizarEmpresa();
	}
}	

function actualizarEmpresa(){
    
	
	/*
     * Verificar si la empresa a inactivar tienene empleados con fecha de ingreso superior a la fecha 
     * de inactivacion
     * //data: {idempresa:,fechaestado:},
     */
	var ctract=0;
	
    if($("#select9").val()=='I'){
    	$.ajax({
			   url: 'valempleadosempresa.php',
			   type: "POST",
			   async:false,
			   data: {idempresa:$("#Idempresa").val(),fechaestado:$("#fechaEstado").val()},
			   dataType: "json",
			   success: function(datos){
				   if(datos.error==0){
					   var cantreg=parseInt(datos.activos) + parseInt(datos.pendientes);
					   if(cantreg>0){
						   alert("La empresa registra " + cantreg + " trabajador(es) activo(s) con fecha de ingreso superior a la fecha de inactivacion. No es posible inactivar la empresa, la transaccion se ha cancelado.");
						  ctract=1;

					   }
				   }else{
					  alert(datos.descripcion);
					  ctract=1;
				   } 
			    }
		   });
    	if(ctract==1)
    		return false;
    }
	
    
	
	var campo0=$("#Idempresa").val();
	$.getJSON('buscarEmpresa1.php',{v0:campo0},function(datos){
		$.each(datos,function(i,fila){			
			/** GUARDA HISTORIA DE LAS MODIFICACIONES LEY 1429 **/
			if(fila.claseaportante==2875){
				var idempresa = fila.idempresa;
				var nit       = fila.nit;
				var	rsocial	  =	fila.razonsocial;
				var	claseaport	  = fila.claseaportante;
				var	tipoaport	  =	fila.tipoaportante;
				var	estado	  =	fila.estado;
				var usuario   = $("#usuario").val();
				var	fecha	  =	fila.fechasistema;
				var	indicador	  = fila.indicador;
					
				if($("#cmbClaseApo").val()!=claseaport)
				{			
				   $.ajax({
					   url: 'guardarRenovacionley1429.php',
					   type: "POST",
					   data: "submit=&v0="+idempresa+"&v1="+nit+"&v2="+rsocial+"&v3="+claseaport
					   +"&v4="+tipoaport+"&v5="+estado+"&v6="+indicador,
					   success: function(datos){
						   if(datos==1){
						 	   alert("Se grabo las historias de la empresas ley 1429.");
							   return false;
						   }else{
							   alert("Hubo problemas grabando la historia de empresa ley1429.");
						   }
					    }
				   });
			    } //FIN IF
		     }//FIN IF
		});
	});		
	
	var campo0=$("#select12").val();//idsector
	var campo1=$("#select13").val();//idclasesociedad
	var campo2=$("#tNit").val();//nit
	var campo3=$("#tDigito").val();//digito
	var campo4=$("#select").val();//idtipodocumento
	var campo5=$("#textfield").val();//razonsocial
	var campo6=$("#textfield2").val();//sigla
	var campo7=$("#cboDepto2").val();//iddepartamento
	var campo8=$("#cboCiudad2").val();//idciudad
	var campo9=$("#tDireccion").val();//direccion
	var campo10=$("#textfield8").val();//telefono
	var campo11=$("#textfield6").val();//fax
	var campo12=$("#textfield9").val();//url
	var campo13=$("#textfield15").val();//email
	var campo14=idrepresentante;//idrepresentante
	var campo15=idjefe;//idjefepersonal
	var campo16=$("#select4").val();//contratista
	var campo17=$("#select7").val();//idcodigoactividad
	var campo18=$("#select5").val();//colegio
	var campo19=$("#select6").val();//exento
	var campo20=$("#codigoCIU").val();//actieconomicadane
	var campo21=$("#select11").val();//indicador
	var campo22=$("#select10").val();//seccional
	var campo23=$("#textfield18").val();//fechaafiliacion
	var campo24=$("#datepicker").val();//fechaaportes
	var campo25=$("#select8").val();//idasesor
	var campo26=$("#select9").val();//estado
	var campo27=$("#textfield19").val();//usuario
	var campo29=$("#fechaMatricula").val();	//fechamatricula
	var campo30=$("#fechaEstado").val(); //fecha estado
	var campo31=$("#cmbClaseApo").val(); //clase de aportante
	var campo32=$("#cmbTipoApo").val(); //tipo de aportante
	var campo33=$("#cmbCausalInactivacion").val(); //causal de inactivacion
	var campo34=$("#tDirCorresp").val();//direccion correspondencia
	var campo35=$("#cboDeptoCorresp").val();//iddepartamento correspondencia
	var campo36=$("#cboCiudadCorresp").val();//idciudad correspondencia
	var campo37=$("#cboBarrioCorresp").val();//idbarrio correspondencia
	var campo38=$("#cboBarrioCA").val();//barrio
	var campo39=$("#txtCelular").val();//barrio
	var campo40=$("#tipoTel").val();//tipo telefono
	
	campo5 = campo5.replace(/&/g, 38);
	campo6 = campo6.replace(/&/g, 38);
	$.ajax({
		url: 'empresasAct_Datos.php',
		type: "POST",
		data: "submit=&v0="+campo0+"&v1="+campo1+"&v2="+campo2+"&v3="+campo3+"&v4="+campo4+"&v5="+campo5+"&v6="+campo6+"&v7="+campo7+"&v8="+campo8+
			  "&v9="+campo9+"&v10="+campo10+"&v11="+campo11+"&v12="+campo12+"&v13="+campo13+"&v14="+campo14+"&v15="+campo15+"&v16="+campo16+"&v17="+campo17+
			  "&v18="+campo18+"&v19="+campo19+"&v20="+campo20+"&v21="+campo21+"&v22="+campo22+"&v23="+campo23+"&v24="+campo24+"&v25="+campo25+"&v26="+campo26+
			  "&v27="+campo27+"&v28="+idempresa+"&v29="+campo29+"&v30="+campo30+"&v31="+campo31+"&v32="+campo32+"&v33="+campo33+"&v34="+campo34+"&v35="+campo35+"&v36="+campo36+"&v37="+campo37+
			  "&v38="+campo38+"&v39="+campo39+"&v40="+campo40,
		success: function(datos){
			if(datos==1){
				msg+="Modificaciones realizadas! \r\n";
				alert(msg);
				msg="";
				if(campo26 == 'I'){
					//Se inactivan los trabajadores de la empresa que se inactiva
					var objFecha = new Date();//creo un objeto tipo fecha
					var anioActual = objFecha.getFullYear();//Obtengo el a�o actual
					objFecha.setYear(anioActual+1);
					var mesActual = objFecha.getMonth()+1;
					mesActual = (mesActual <10)?"0"+mesActual.toString():mesActual.toString();
					var diaActual = objFecha.getDate();
					diaActual = (diaActual <10)?"0"+diaActual.toString():diaActual.toString();
					var fechaEnUnAnio = objFecha.getFullYear()+"-"+mesActual+"-"+diaActual;
					inactivarTrabajadores(idempresa,fechaEnUnAnio,campo30);
				}
				observacionesTab(idempresa,2);
				limpiar();
				return false;
			}
			else{
				alert("Error inesperado!");
			}
		}
	});
	return false;
}

function inactivarTrabajadores(ide,fechaFidelidad,fechaRetiro){
		//Buscar Trabajadores en BD.
		alert("Se inactivan los trabajadores de la Empresa");
		$.getJSON(URL+'phpComunes/buscarIdsPersonasEInactivarxEmpresa.php',{v0:ide},function(datos1){
			if(datos1 == 0){
				alert("La empresa NO tiene empleados Activos");
				return false;
			}else{
				arregloIds = [];
				for(i = 0; i<datos1.length;i++){
					arregloIds[i] = datos1[i].idpersona;
				}
				$.getJSON('InactivarTrabajadoresEmpresa.php',{
					idspersonas:arregloIds.join(),
					idempresa: ide,
					fechafidelidad: fechaFidelidad,
					fechaestado: fechaRetiro,
					},
					function(respuesta){
						if(respuesta.numefectuados == 0){
							alert("NO se "+respuesta.operacion+" ningun trabajador.");
						}else{
							if(respuesta.numefectuados > 0){
								alert("Se  "+respuesta.operacion+" "+ respuesta.numefectuados +" trabajadores. \nTrabajadores NO "+respuesta.operacion+" : "+ respuesta.numnoefectuados);
								observacionesTab(ide,2);
							}
						}
					}
				);
			}//end else
		});//json trbaajdores
}

function digito(obj)
{
	var vpri, x, y, z, i, nit1, dv1;
 	nit1=obj.value;
 	if (isNaN(nit1))
 	{
 	alert('El valor digitado no es un n\u00FAmero v\u00E1lido'+nit1);
 	} 
 	else {
 	vpri = new Array(16);
 	x=0 ; y=0 ; z=nit1.length ;
	 vpri[1]=3;
	 vpri[2]=7;
	 vpri[3]=13;
	 vpri[4]=17;
	 vpri[5]=19;
	 vpri[6]=23;
	 vpri[7]=29;
	 vpri[8]=37;
	 vpri[9]=41;
	 vpri[10]=43;
	 vpri[11]=47;
	 vpri[12]=53;
	 vpri[13]=59;
	 vpri[14]=67;
	 vpri[15]=71;
 	for(i=0 ; i<z ; i++) {
 	y=(nit1.substr(i,1));
 	x+=(y*vpri[z-i]);
 	}
 	y=x%11
 	if (y > 1)
 	{
 		dv1=11-y;
 	} else {
 		dv1=y;
 	}
 	document.forma.tDigito.value=dv1;
   }
  } 
  
function buscarPersona(objeto,op){
	var numero=parseInt(objeto.value);
	if(isNaN(numero)) return;
	$.ajax({
		url: URL+'phpComunes/pdo.buscar.persona.php',
		cache: false,
		type: "GET",
		data: {v0:1,v1:numero,v2:2},
		dataType: "json",
		success: function(datos){
			if(op==0){
			if(datos==0){
				alert("No existe en nuestra base, registre los datos completos!");	
				persona_simple(document.getElementById('textfield4'),document.getElementById('idRepresemtante'))
			}
				else{
				nom=datos[0].pnombre+" "+datos[0].snombre+" "+datos[0].papellido+" "+datos[0].sapellido;
				$("#idRepresemtante").val(datos[0].idpersona);
				idrepresentante=datos[0].idpersona;
				$("#nombre1").html(nom);
			}
			}
			else{
				if(datos==0){
					alert("No existe en nuestra base, registre los datos completos!");	
					persona_simple(document.getElementById('textfield4'),document.getElementById('idRepresemtante'))
				}
					else{
					nom=datos[0].pnombre+" "+datos[0].snombre+" "+datos[0].papellido+" "+datos[0].sapellido;
					$("#idContacto").val(datos[0].idpersona);
					$("#nombre2").html(nom);
					idjefe=datos[0].idpersona;
				}
				}
		}
	});
}
