// JavaScript Document
// autor Orlando Puentes A
// fecha mayo 20 de 2010
// objeto administraci\xf3n de los eventos del formulario de empresa.php
var nuevo=0;
var modificar=0;
var continuar=true;
var ccgerente=0;
var ccjefe=0;
var msg="";
var ide=0;
var idc=0;
var URL=src();
var fecrad=0;

function validarCampos(op){
	//$("#bGuardar").hide();
	var cadena="<ul>";
	var campo0=$("#lisRadicacion").val();	//Radicacion Nro
	var campo1=$("#txtId2").val();			//Fecha
	var campo2=$("#select12").val();		//Sector
	var campo3=$("#select13").val();		//Clase de Sociedad
	var campo4=$("#tNit").val();			//NIT
	var campo5=$("#tDigito").val();			//Digito
	var campo6=$("#select").val();			//Tipo Documento
	var campo7=$("#textfield").val();		//Razon Social
	var campo8=$("#textfield2").val();		//Nombre Comercial
	var campo9=$("#cboDepto2").val();		//Departamento
	var campo10=$("#cboCiudad2").val();		//Ciudad
	var campo11=$("#tDireccion").val();		//Direccion
	var campo12=$("#textfield8").val();		//Telefono
	var campo13=$("#textfield6").val();		//Fax
	var campo14=$("#textfield9").val();		//URL
	var campo15=$("#textfield15").val();	//Email
	var campo16=$("#textfield4").val();		//C.C. Representante Legal
	var campo17=$("#textfield5").val();		//C.C. Contacto Administrativo
	var campo18=$("#select4").val();		//Contratista
	var campo19=$("#select7").val();		//Actividad Economica
	var campo20=$("#select11").val();		//Indice Aporte
	var campo21=$("#select8").val();		//Asesor
	var campo22=$("#select10").val();		//Seccional
	var campo23=$("#select9").val();		//Estado
	var campo24=$("#textfield18").val();	//Fecha Afiliacion
	var campo25=$("#datepicker").val();		//Fecha Constitucion
	var campo27=$("#lisCodigo").val();		//Codigo CIIU
	var campo29=$("#fechaIaportes").val();	//Fecha inicio Aportes
	var campo30=$("#claseApo").val();		//Clase Aportante
	var campo31=$("#tipoApo").val();		//Tipo Aportante
	var campo54=$("#tDirCorresp").val();		//Direccion correspondencia
	var campo55=$("#cboDeptoCorresp").val();		//Departamento correspondencia
	var campo56=$("#cboCiudadCorresp").val();		//Ciudad correspondencia
	var campo57=$("#cboBarrioCA").val();	//barrio
	var campo58=$("#txtCelular").val();	//celular
	var campo59=$("#cboBarrioCorresp").val();	//barrio de correspondencia
	var campo60=$("#tipoTel").val();	//tipo telefono
	
	if(campo23=='I' || campo23=='P'){
		alert("Active la empresa!");
		$("#select9").focus();
		return false;
	}	
	if(campo2.length==0){
		cadena+="<li>Falta sector</li>";
	}
	/****************************************/
	if(campo3==0 || campo3==null){
		cadena+="<li>Falta clase de sociedad</li>";
	}
	/*************************************************/
	
	if(campo4.length==0){
		cadena+="<li>Falta NIT</li>";
	}
	else{
		if(isNaN(campo4))
		cadena+="<li>El NIT debe ser num\u00E9rico!</li>";
		var long=campo4.length;
		if(long<5 || long>11)
		cadena+="<li>el NIT es de 5 a 11 d\xedgitos</li>";
	}
	/************************************************/
	if(campo5.length==0){
		cadena+="<li>Falta d\u00EDgito de verificaci\u00FAn</li>";
	}
	/************************************************/
	if(campo6==0 || campo6==null){
		cadena+="<li>Falta tipo de documento</li>";
	}
	/***************************************************/
	if(campo7.length==0){
		cadena+="<li>Falta raz\u00F3n social</li>";
	}
	else{
		if(campo7.length<3)
		cadena+="<li>La raz\u00F3n social es muy corta!</li>";
	}
	/***************************************************/
	if(campo8.length==0){
		cadena+="<li>Falta nombre comercial</li>";
	}
	else{
		if(campo8.length<3)
		cadena+="<li>La raz\u00FAn social es muy corta!</li>";
	}
	/***************************************************/
	if(campo9==0){
		cadena+="<li>Falta departamento</li>";
	}
	/***************************************************/
	if(campo10==0){
		cadena+="<li>Falta ciudad</li>";
	}	
	/***************************************************/
	if(campo11.length==0){
		cadena+="<li>Falta direcci\u00F3n</li>";
	} else{
		if(campo11.length<10)
			cadena+="<li>La direccion es muy corta!</li>";
	}
	/**************************************************/
	if(campo12.length==0){
		cadena+="<li>Falta el tel\u00E9fono</li>";
	} else{
		if(isNaN(campo12))
			cadena+="<li>El tel\u00E9fono debe ser num\u00E9rico!</li>";
			var long=campo12.length;
			if(long<7 || long>10)
				cadena+="<li>el tel\u00E9fono es de 7 a 10 d\u00EDgitos</li>";
	}
	/*************************************************/
	if(campo13.length>0){
		if(isNaN(campo13))
			cadena+="<li>El fax debe ser num\u00E9rico!</li>";
		var long=campo13.length;
		if(long<7)
			cadena+="<li>el fax es de 7 d\xedgitos</li>";
	}
	/************************************************
	
	14 www y 15 email
	
	*/
	/*************************************************/
	if(campo16.length==0){
		cadena+="<li>Falta identificaci\u00F3n del representante legal</li>";
	}
	/****************************************/
	if(campo18==0){
		cadena+="<li>Falta contratista</li>";
	}
	/****************************************/
	
	if(campo19==0 && campo31==2656){
		cadena+="<li>Falta actividad</li>";
	}
	/****************************************/
	if(campo20==0){
		cadena+="<li>Falta indice de aportes</li>";
	}
	/****************************************/
	
	if(campo22==0){
		cadena+="<li>Falta seccional</li>";
	}
	/****************************************/
	if(campo23==0){
		cadena+="<li>Falta estado</li>";
	}
	if(campo25.length==0){
		cadena+="<li>Falta fecha de Constitucion</li>";
	}
	if(campo27==0){
		cadena+="<li>Falta codigo CIU</li>";
	}
	if(campo29.length==0){
		cadena+="<li>Falta fecha Inicio Aportes</li>";
	}
	if(campo30.length==0){
		cadena+="<li>Falta Clase Aportante</li>";
	}
	if(campo31.length==0){
		cadena+="<li>Falta Tipo Aportante</li>";
	}
	if(campo54==0){
		cadena+="<li>Falta Direccion de Correspondencia</li>";
	}
	/***************************************************/
	if(campo55==0){
		cadena+="<li>Falta departamento de correspondencia</li>";
	}
	/***************************************************/
	if(campo56==0){
		cadena+="<li>Falta ciudad de correspondencia</li>";
	}
	/*************************************************/
	if(campo57==0){
		cadena+="<li>Falta Barrio</li>";
	}
	if(!campo59 || campo59==0){
		cadena+="<li>Falta Barrio Correspondencia</li>";
	}
	if(campo12==0 && campo58==0){
		cadena+="<li>Falta el tel\u00E9fono o Celular</li>";
	}
	else{
		if(campo12!=0){
			if(isNaN(campo12)){
				cadena+="<li>El tel\u00E9fono debe ser num\u00E9rico!</li>";
				var long=campo12.length;
				if(long<7 || long>10){
					cadena+="<li>el tel\u00E9fono es de 7 a 10 d\u00EDgitos</li>";
				}
			}
		}
		if(campo58!=0){
			if(isNaN(campo58)){
				cadena+="<li>El Celular debe ser num\u00E9rico!</li>";
				var long=campo58.length;
				if(long<10 || long>10){
					cadena+="<li>el Celular es de 10 d\u00EDgitos</li>";
				}
			}
		}
		
	}
	/******************** FIN ****************/
	if(campo60==0){
		cadena+="<li>Falta Tipo de Telefono</li>";
	}
	if(cadena.length>10){
		cadena+="</ul>";
		cadena="Hay errores en los datos, por favor revisar:"+cadena;
		document.getElementById('error').innerHTML=cadena;
		return;
	}else{
		cadena="Datos completos!";
		document.getElementById('error').innerHTML=cadena;
	}
	//buscarNitInforma();
        guardarEmpresa();
}

function nuevoR(){
	console.log("lalmando nuevoR");
	nuevo=1;
	$("#bGuardar").show();
	camposLimpiar();
	$("#select9").val("A");
	msg="";
	document.forms[0].elements[1].value = fechaHoy();
	document.getElementById('error').innerHTML="";
	var campo0=31;
	$('#lisRadicacion option').remove();
	$.post(URL+'phpComunes/buscarRadicaTipoID.php',{v0:campo0},function(datos){
		console.log("recibi");
		console.log(datos);
    	var cmbRadicacion = $("#lisRadicacion");
	   	$(cmbRadicacion).html(datos);
	   	if($(cmbRadicacion).children().length==1){
			alert('Lo lamento NO hay radicaciones pendientes de grabar!');
			return false;
         }
	});
	$("#lisRadicacion").focus();
}

function buscarRadicacion(obj){
	var radicacion=obj.value;
	limpiarCampos();
	jQuery('td[id^=nombre]').html('');
	jQuery('#div-observaciones').html(''); 
	$("#lisRadicacion").val(radicacion);
	$.getJSON('buscarRadicacion.php', {v0:obj.value}, function(datos){
		$.each(datos,function(i,fila){
			$("#tNit").val(fila.nit);
			$("#tIdRadicacion").val(fila.idtiporadicacion);
			$("#tNit2").val(fila.nit);
			$("#tFechaRadicacion").val(fila.fechasistema);
			$("#textfield18").val(fila.fecharadicacion);
			fecrad=fila.fecharadicacion;
			buscarEmpresa();
		});
	});
}

function buscarEmpresa(){
	var v0=$("#tNit").val();
	var idr=0;
	$.getJSON(URL+'phpComunes/pdo.b.empresa.nit.php',{v0:v0},function(datos){
		if(datos==0){
			alert("Lo lamento no encontre la empresa!");
			return false;
			}
 //codigosucursal, principal, fechamatricula, claseaportante, tipoaportante,  codigoestado, fechaestado, usuario, , idclasesociedad, legalizada			
		$.each(datos,function(i,fila){
			ide=fila.idempresa;
			$("#cboDepto2").val(fila.iddepartamento);		//departamento
			$("#cboDepto2").change();
			$("#txtId2").val(fila.fechasistema);			//fecha
			$("#select12").val(fila.idsector);		//sector
			$("#select13").val(fila.idclasesociedad);		//clase de sociedad
			$("#tNit").val(fila.nit);			//Nit
			$("#tDigito").val(fila.digito);			//Digito
			$("#select").val(fila.idtipodocumento);			//Tipo Documento
			$("#textfield").val(fila.razonsocial);		//Razon Social
			$("#textfield2").val(fila.sigla);		//Sigla
			$("#tDireccion").val(fila.direccion);		//direccion
			$("#textfield8").val(fila.telefono);		//telefono
			$("#textfield6").val(fila.fax);		//fax
			$("#textfield9").val(fila.url);		//URL
			$("#textfield15").val(fila.email);	//Email
			$("#idRepresemtante").val(fila.idrepresentante);		//ID REPRESENTANTE idrepresentante
			$("#idContacto").val(fila.idjefepersonal);		//ID CONTACTO idjefepersonal
			$("#select4").val(fila.contratista);		//contratista			
			$("#select11").val(fila.indicador);		//Indice
			$("#select8").val(fila.idasesor);		//Asesor
			$("#select10").val(fila.seccional);		//Agencia
			//$("#select9").val(fila.estado);		//Estado
			$("#textfield18").val(fecrad);	//Fecha Afiliacion fechaafiliacion
			$("#datepicker").val(fila.fechamatricula);		//Fecha Constitucion
			$("#claseApo").val(fila.claseaportante);		//Clase Aportante
			$("#tipoApo").val(fila.tipoaportante);			//Tipo Aportante
			$("#tDirCorresp").val(fila.direcorresp);		//direccion correspondencia
			$("#cboDeptoCorresp").val(fila.iddepcorresp);		//departamento correspondencia
			$("#cboBarrioCA").val(fila.barrio);		//Barrio
			$("#txtCelular").val(fila.celular);   //Celular
			$("#cboDeptoCorresp").change();
			$("#tipoTel").val(fila.id_tipo_tel);		//tipo telefono
			$("#codigoCIU").val(fila.actieconomicadane).trigger("blur");
			/*$("#select7").val(fila.idcodigoactividad).trigger("change");		//Actividad Economica
			// se debe obtener el código de actividad, no el id del registro de la tabla
			$.getJSON('buscarCIU.php',{idciiu: fila.actieconomicadane},function(resultado){
				if(resultado != 0){
					$("#codigoCIU").val(resultado[0].idciiu);
				}
			});*/
			
			$("#ruta").val(fila.rutadocumentos);
			$("#fechaIaportes").val(fila.fechaaportes);
			$("#fechaI").val(fila.fechaestado);
			idr=$.trim(fila.idrepresentante);
			idc=$.trim(fila.idjefepersonal);
			
			setTimeout((function(){
				$.post(URL+"phpComunes/ciudades.php", {elegido: fila.iddepartamento}, function(data){
					$("#cboCiudad2").html(data);
					$("#cboCiudad2").val(fila.idciudad);
				});
        	}),200);
			
			setTimeout((function(){
				$.post(URL+"phpComunes/ciudades.php", {elegido: fila.iddepcorresp}, function(data){
					$("#cboCiudadCorresp").html(data);
					$("#cboCiudadCorresp").val(fila.idciucorresp);
				});
        	}),200);
			return;
		});
		if(idr.length>0){
			$.getJSON(URL+'phpComunes/pdo.buscar.persona.php',{v0:idr,v1:0,v2:5},function(data){
				if(data==0){
					//alert("No se encontro el nombre del representante legal!");
					$("#idRepresemtante").val('');
					$("#textfield4").val('');
					$("#nombre1").html('');
				}
				$.each(data,function(j,f1){
					nom=f1.pnombre+" "+f1.snombre+" "+f1.papellido+" "+f1.sapellido;
 					$("#idRepresemtante").val(f1.idpersona);
 					$("#textfield4").val(f1.identificacion);
 					$("#nombre1").html(nom);
				});
			});
		}
		
		if(idc.length>0){
			$.getJSON(URL+'phpComunes/pdo.buscar.persona.php',{v0:idr,v1:0,v2:5},function(data2){
				if(data2==0){
					//alert("No se encontro el nombre del representante legal!");
					$("#idRepresemtante").val('');
	 				$("#textfield4").val('');
	 				$("#nombre1").html('');
				}			
				$.each(data2,function(j,f2){
					nom=f2.pnombre+" "+f2.snombre+" "+f2.papellido+" "+f2.sapellido;
 					$("#idContacto").val(f2.idpersona);
 					$("#textfield5").val(f2.identificacion);
					$("#nombre2").html(nom);
				});
			});
		}
		
		$.getJSON(URL+'phpComunes/pdo.buscar.notas.php',{v0:ide,v1:'2'},function(datos){
				document.getElementById('div-observaciones').innerHTML=datos;
		});
	});	
}
	
function copiar(obj1,obj2){
	obj2.val(obj1.value); //nombre comercial
	}
/*
function buscarNitInforma(){
	var campo0 = $("#tNit").val();
	$.ajax({
		url: 'buscarInforma.php',
		type: "POST",
		data: "submit=&v0="+campo0,
		success: function(datos){
			if(datos==1){
				msg+="El NIT ya existe en INFORMA WEB \r\n";
				guardarEmpresa();
			}else{
				guardarEmpresaInforma();
			}
		}
	});
	return false;
}
*/

//Duplicar los combos de municipio y departamento de correspondencia
function DuplicarCiudadCorresp(){
	document.getElementById('cboDeptoCorresp').value = document.getElementById('cboDepto2').value;		
	var dato=document.getElementById('cboCiudad2').value;
	
	var deptoElegido1 = $("#cboDeptoCorresp").val();
	$.post(URL+"phpComunes/ciudades.php", { elegido: deptoElegido1}, function(data){
		$("#cboCiudadCorresp").html(data);
		$("#cboCiudadCorresp").val(dato);
		
	});	

	$.post(URL+"phpComunes/zonas.php", { elegido: dato}, function(data){
		$("#cboZonaCorresp").html(data);		
	});
	
}

//Duplicar los combos de zona y departamento de correspondencia
function DuplicarBarrioCorresp(){
	var idZona = $("#cboZonaCA").val();
	var idBarrio = $("#cboBarrioCA").val();
	$("#cboZonaCorresp").val(idZona);
	
	$.post(URL+"phpComunes/barrios.php", { elegido: idZona}, function(data){
		$("#cboBarrioCorresp").html(data);
		$("#cboBarrioCorresp").val(idBarrio);
	});	
}
		
function buscarPersona(objeto,op){
	if(nuevo==0){
		alert("Si es un registro nuevo haga click en NUEVO!!");
		return;
	}
	var numero=parseInt(objeto.value);
	if(isNaN(numero)) return;
	$.ajax({
		url: URL+'phpComunes/pdo.buscar.persona.php',
		cache: false,
		type: "GET",
		data: {v0:1,v1:numero,v2:2},
		dataType: "json",
		success: function(datos){
			if(op==0){
			if(datos==0){
				alert("No existe en nuestra base, registre los datos completos!");	
				persona_simple(document.getElementById('textfield4'),document.getElementById('idRepresemtante'))
			}
				else{
				nom=datos[0].pnombre+" "+datos[0].snombre+" "+datos[0].papellido+" "+datos[0].sapellido;
				$("#idRepresemtante").val(datos[0].idpersona);
				$("#nombre1").html(nom);
			}
			}
			else{
				if(datos==0){
					alert("No existe en nuestra base, registre los datos completos!");	
					persona_simple(document.getElementById('textfield4'),document.getElementById('idRepresemtante'))
				}
					else{
					nom=datos[0].pnombre+" "+datos[0].snombre+" "+datos[0].papellido+" "+datos[0].sapellido;
					$("#idContacto").val(datos[0].idpersona);
					$("#nombre2").html(nom);
				}
				}
		}
	});
}

function guardarEmpresa(){
	var v0=$("#tNit").val();
	var idr=0;
	$.getJSON(URL+'phpComunes/pdo.b.empresa.nit.php',{v0:v0},function(datos){
		$.each(datos,function(i,fila){			
			/** GUARDA HISTORIA DE LAS MODIFICACIONES LEY 1429 **/
			if(fila.claseaportante==2875){
				var idempresa = fila.idempresa;
				var nit       = fila.nit;
				var	rsocial	  =	fila.razonsocial;
				var	claseaport	  = fila.claseaportante;
				var	tipoaport	  =	fila.tipoaportante;
				var	estado	  =	fila.estado;
				var usuario   = $("#usuario").val();
				var	fecha	  =	fila.fechasistema;
				var	indicador	  = fila.indicador;
					
				if($("#claseApo").val()!=claseaport)
				{			
				$.ajax({
					url: 'guardarRenovacionley1429.php',
					type: "POST",
					data: "submit=&v0="+idempresa+"&v1="+nit+"&v2="+rsocial+"&v3="+claseaport
					+"&v4="+tipoaport+"&v5="+estado+"&v6="+indicador,
					success: function(datos){
						if(datos==1){
							alert("Se grabo las historias de la empresas ley 1429.");
							return false;
						}else{
							alert("Hubo problemas grabando la historia de empresa ley1429.");
						}
					}
				});
			} //FIN IF
		}//FIN IF
		});
	});	
	var campo0=$("#lisRadicacion").val();	//idradicacion
	var campo1=$("#txtId2").val();			//fecha
	var campo2=$("#select12").val();		//sector
	var campo3=$("#select13").val();		//clase de sociedad
	var campo4=$("#tNit").val();			//Nit
	var campo5=$("#tDigito").val();			//Digito
	var campo6=$("#select").val();			//Tipo Documento
	var campo7=$("#textfield").val();		//Razon Social
	var campo8=$("#textfield2").val();		//Sigla
	var campo9=$("#cboDepto2").val();		//departamento
	var campo10=$("#cboCiudad2").val();		//ciudad
	var campo11=$("#tDireccion").val();		//direccion
	var campo12=$("#textfield8").val();		//telefono
	var campo13=$("#textfield6").val();		//fax
	var campo14=$("#textfield9").val();		//URL
	var campo15=$("#textfield15").val();	//Email
	var campo16=$("#idRepresemtante").val();//ID REPRESENTANTE
	var campo17=$("#idContacto").val();		//ID CONTACTI
	var campo18=$("#select4").val();		//contratista
	var campo19=$("#select7").val();		//Actividad Economica
	var campo20=$("#select11").val();		//Indice
	var campo21=$("#select8").val();		//Asesor
	var campo22=$("#select10").val();		//Agencia
	var campo23=$("#select9").val();		//Estado
	var campo24=$("#textfield18").val();	//Fecha Afiliacion
	var campo25=$("#datepicker").val();		//Fecha Constitucion
	var campo27=$("#lisCodigo").val();		//codigo ciu-
	var campo28=$("#ruta").val();			//ruta documentos
	var campo29=$("#fechaIaportes").val();	//Fecha inicio Aportes
	var campo30=$("#claseApo").val();		//Clase Aportante
	var campo31=$("#tipoApo").val();		//Tipo Aportante
	var campo54=$("#tDirCorresp").val();		//Direccion correspondencia
	var campo55=$("#cboDeptoCorresp").val();		//Departamento correspondencia
	var campo56=$("#cboCiudadCorresp").val();		//Ciudad correspondencia
	var campo57=$("#cboBarrioCA").val();	//barrio
	var campo58=$("#txtCelular").val();	//Celular
	var campo59=$("#cboBarrioCorresp").val(); //barrio de correspondencia
	var campo60=$("#tipoTel").val();	//tipo telefono
	var campo99=ide;
	
	
	/*var campo29=$("#").val();	//document.forms[0].elements[29].value;
	var campo30=$("#").val();	//document.forms[0].elements[30].value;
	var campo31=$("#").val();	//document.forms[0].elements[31].value;
	var campo32=$("#").val();	//document.forms[0].elements[32].value;
	var campo33=$("#").val();	//document.forms[0].elements[33].value;
	var campo34=$("#").val();	//document.forms[0].elements[34].value;
	var campo35=$("#").val();	//document.forms[0].elements[35].value;
	var campo36=$("#").val();	//document.forms[0].elements[36].value;
	var campo37=$("#").val();	//document.forms[0].elements[37].value;*/
	//+"&v28="+campo28+"&v29="+campo29+"&v30="+campo30+"&v31="+campo31+"&v32="+campo32+"&v33="+campo33+"&v34="+campo34+"&v35="+campo35+"&v36="+campo36+"&v37="+campo37
	
	$.ajax({
		url: 'guardarRenovacion.php',
		type: "POST",
                async:false,
		data: "submit=&v0="+campo0+"&v1="+campo1+"&v2="+campo2+"&v3="+campo3+"&v4="+campo4+"&v5="+campo5+"&v6="+campo6+"&v7="+campo7+"&v8="+campo8+
		"&v9="+campo9+"&v10="+campo10+"&v11="+campo11+"&v12="+campo12+"&v13="+campo13+"&v14="+campo14+"&v15="+campo15+"&v16="+campo16+"&v17="+campo17+
		"&v18="+campo18+"&v19="+campo19+"&v20="+campo20+"&v21="+campo21+"&v22="+campo22+"&v23="+campo23+"&v24="+campo24+"&v25="+campo25+"&v27="+campo27+
		"&v28="+campo28+"&v29="+campo29+"&v30="+campo30+"&v31="+campo31+"&v54="+campo54+"&v55="+campo55+"&v56="+campo56+"&v99="+campo99+"&v57="+campo57+"&v58="+campo58+"&v59="+campo59+"&v60="+campo60,
		success: function(datos){
			if(datos==1){
				msg+="Se actualiz\u00F3 la empresa principal \r\n";
				alert(msg);
				msg="";
				//cerrarRadicacion();
				observacionesTab(ide,2);
				$("#bGuardar").show();
				return false;
			}else{
				alert("Hubo problemas grabando la renovaci\xf3n.");
			}
		}
	});
        
		/*
		
        //Llamado del proceso de creación de tercero en JDE 
        if(validarExistenciaTerceroJDE(campo6,campo4,campo5) == false){
            //Se realiza el proceso de creación del tercero
            $.ajax({    
                url: URL+'Terceros/Process.php',
                type: "POST",
                async:false,
                dataType:"json",
                data: {
                        action:"New",
                        type:"Terceros",
                        campo1:campo6,
                        campo2:campo4,
                        campo3:campo5,
                        campo7:campo7,
                        campo9:campo11,
                        campo10:campo9,
                        campo11:campo10,
                        campo12:$("#cboCiudad2 [value='"+campo10+"']").text(),
                        campo13:campo12,
                        campo16:campo15
                },
                success: function(result){
                        console.log(result)
                        var payload=$.parseJSON(result.payload);
                        console.log(payload);
                        if(result.error)
                        {
                                alert("Error grabando en JD Edwards!\r\nMire la consola para mas informacion");
                        }
                        else
                        {
                                alert("Se ha registrado el tercero con n\u00famero de identificaci\u00f3n " + campo4 + " en JDE con el AN8 " + result.AN8Tercero);
                        }
                }
            });   
        }
		*/
        cerrarRadicacion();
	return false;
	}
	
function guardarEmpresaInforma(){
	var campo0 =document.forms[0].elements[4].value;
	var campo1 =document.forms[0].elements[7].value;
	var campo2 =document.forms[0].elements[9].value;
	var campo3 =document.forms[0].elements[12].value;
	var campo4 =document.forms[0].elements[11].value;
	var campo5 =document.forms[0].elements[16].value;
	var campo6 =document.forms[0].elements[15].value;
	var campo7 =document.forms[0].elements[13].value;
	var campo8 =document.forms[0].elements[10].value;
	$.ajax({
		url: 'empresasGInforma.php',
		type: "POST",
		data: "submit=&v0="+campo0+"&v1="+campo1+"&v2="+campo2+"&v3="+campo3+"&v4="+campo4+"&v5="+campo5+"&v6="+campo6+"&v7="+campo7+"&v8="+campo8,
		success: function(datos){
			if(datos==1){
				msg+="Se guard\u00F3 la Empresa en Informa Web \r\n";
				}
			else{
				msg+="NO se pudo guardar en Informa Web! \r\n";
				alert("No se pudo guardar la empresa en Informa WEB!, intente de nuevo");
				return false;
				}
			guardarEmpresa();
		}
	});
	return false;
	}
	
function digito(obj)
{
	var vpri, x, y, z, i, nit1, dv1;
 	nit1=obj.value;
 	if (isNaN(nit1))
 	{
 	alert('El valor digitado no es un n\u00FAmero v\u00E1lido'+nit1);
 	} 
 	else {
 	vpri = new Array(16);
 	x=0 ; y=0 ; z=nit1.length ;
	 vpri[1]=3;
	 vpri[2]=7;
	 vpri[3]=13;
	 vpri[4]=17;
	 vpri[5]=19;
	 vpri[6]=23;
	 vpri[7]=29;
	 vpri[8]=37;
	 vpri[9]=41;
	 vpri[10]=43;
	 vpri[11]=47;
	 vpri[12]=53;
	 vpri[13]=59;
	 vpri[14]=67;
	 vpri[15]=71;
 	for(i=0 ; i<z ; i++) {
 	y=(nit1.substr(i,1));
 	x+=(y*vpri[z-i]);
 	}
 	y=x%11
 	if (y > 1)
 	{
 		dv1=11-y;
 	} else {
 		dv1=y;
 	}
 	document.forma.tDigito.value=dv1;
   }
  } 

function cerrarRadicacion(){
    var campo0=document.forms[0].elements[0].value;
	$.ajax({
		url: 'cerrarRadicacion.php',
		type: "POST",
		data: "submit=&v0="+campo0,
		success: function(datos){
			camposLimpiar();
			alert(datos);
		}
	});
	return false;
	  }

		  
function camposLimpiar() {
  for (var i=0;i<document.forms[0].elements.length;i++){
  	if (document.forms[0].elements[i].type=="text")
		 document.forms[0].elements[i].value="";
	if (document.forms[0].elements[i].type=="select-one")
		 document.forms[0].elements[i].value="0";
	if (document.forms[0].elements[i].type=="textarea")
		document.forms[0].elements[i].value="";
  }
$("#nombre1").html("");
$("#nombre2").html("");
$("#ciu").val("");
$("#div-observaciones").html("");
}

/*
Ã� 	&Aacute; 	\u00C1
á 	&aacute; 	\u00E1
Ã‰ 	&Eacute; 	\u00C9
Ã© 	&eacute; 	\u00E9
Ã� 	&Iacute; 	\u00CD
Ã­ 	&iacute; 	\u00ED
Ó 	&Oacute; 	\u00D3
Ã³ 	&oacute; 	\u00F3
Ãš 	&Uacute; 	\u00DA
Ãº 	&uacute; 	\u00FA
Ãœ 	&Uuml; 	\u00DC
Ã¼ 	&uuml; 	\u00FC
á¹" 	&Ntilde; 	\u00D1
Ã± 	&ntilde; 	\u00F1
*/
function validarExistenciaTerceroJDE(tipoDocumento,numeroIdentificacion,digitoVerificiacion){
    var existenciaTercero = false;
    $.ajax({
        url: 'buscarEmpresaNITJDE.php',
        type: "POST",
        dataType:"json",
        async:false,
        data: {
                tipoDocumento:tipoDocumento,
                nit:numeroIdentificacion,
                digitoVerificacion:digitoVerificiacion,
        },
        success: function(json){
            var an8JDE = json.AN8Tercero;
            
            if(an8JDE == "" || an8JDE == null || an8JDE == "null"){
                alert("La empresa con el n\u00famero de identificaci\u00f3n " + numeroIdentificacion + " no existe en JD Edwards");
            }else{
                alert("La empresa ya existe en JD Edwards con el AN8 " +an8JDE);
                existenciaTercero = true;
            }
        }
    });
    return existenciaTercero;
}