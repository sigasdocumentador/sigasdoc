var URL=src();
var nomina=0;
var aportes=0;
var ide=0;
var nuevo=0;
var fechapago=0;
var comprobante = null;
var documento = null;
var numTrabActivos;
var datos1=0;
function nuevoR(){
	$("#bGuardar").show();
	camposLimpiar();
}


//"&v0="+com+"&v1="+num,
function buscarRecibo(){
	var com=$("#comprobante").val();
	var num=$.trim($("#txtNumero").val());
	if(num.length==0){
		return false;
	}
	if(com==0){
		MENSAJE("Seleccione un tipo de comprobante!");
		return false;
	}
	
	$("#indice").val(0);
	$("#tdNomina").text("");
	var nit=0;
	$.ajax({
		type:"POST", 
		url: "buscarComprobante.php",
		data: {v0:com,v1:num,v2:1},
		async:false,
		dataType: "json",
		success:function(data){
			if(data==0){
				alert("No existe el comprobante!");
				return false;
			}
			$.each(data,function(i,fila){
				fechapago=(fila.fecha);
				$("#tdValor").html(formatCurrency(fila.valor));
				$("#tdFecha").html(fechapago);
				$("#tdDes").html(fila.descripcion);
				$("#tdNotas").html(fila.nota);
				aportes=fila.valor;
				nit=fila.nit;
				documento = fila.numero;
				
				if($("#comprobante").val()=='PU2')
				Noplanilla = fila.observacion.split("-");
				
			});
		},
		complete: function(){
			if(nit == 0)
				return false;
			$.getJSON(URL+'phpComunes/pdo.b.empresa.nit.php',{v0:nit},function(datos){
				if(datos==0){
					MENSAJE("No existe la empresa o no hay una afiliacion Principal, contacte al administrador del sistema!");
					camposLimpiar();
					}
				$.each(datos,function(i,fila){
					ide=fila.idempresa;
					rz=fila.razonsocial;
					ca=fila.detalledefinicion;
					t=ide+" - "+nit+" - "+rz+" - "+ca;
					$("#tdNit").html(t);
					idEmpresa = ide;
					numTrabActivos = fila.trabajadores;
				});
			});
		}
	});
}

function crearPeriodos(){
	var num=$.trim($("#numPer").val());
	
	if(num.length==0){
		return false;
	}
	if(nomina==0){
		MENSAJE("No hay nomina para procesar!");
		return false;
	}
	num=parseInt(num);
	nomina=parseInt(nomina);
	var salario=Math.round(nomina/num);
	var apoMen=Math.round(aportes/num);
	var acumSalario = 0; 
	var acumMen = 0;
	$("#tablaPeriodos tr:not(':first')").remove();
	for(i=0; i<num; i++){
		var idCampoPeriodo = 'per'+i;
		var idCampoNomina = 'nom'+i;
		
		if ( (num-1) == i ){
			salario = nomina-acumSalario;
			apoMen = aportes-acumMen;
		}
		acumSalario += salario;
		acumMen += apoMen;
		var datosHtml = "<tr><td style=text-align:center><input type='text' id='per"+i+"' name='per"+i+"' maxlength='6' onkeydown='solonumeros(this);' onkeyup='solonumeros(this)'  /></td>";
		datosHtml += "<td style=text-align:center><input type='text' id='nom"+i+"' name='nom"+i+"' value='"+salario+"' onkeydown='solonumeros(this);' onkeyup='solonumeros(this)' /></td>";
		datosHtml += "<td style=text-align:center><input type='text' id='apo"+i+"' name='apo"+i+"' value='"+apoMen+"' onkeydown='solonumeros(this);' onkeyup='solonumeros(this)' /></td>";
		datosHtml += "<td style=text-align:center><input type='checkbox' id='aju"+i+"' name='ajs"+i+"' /></td><td style=text-align:center><input type='text' id='afi"+i+"' name='afi"+i+"' value='"+ numTrabActivos +"' onkeydown='solonumeros(this);' onkeyup='solonumeros(this)' /></td><td style=text-align:center clss=box1><textarea id='txaObservacionPeriodo" + i + "' style='height:20px;' cols='50' class=ui-state-default maxlength='200'></textarea></td></tr>";
		//$( "#tablaPeriodos" ).show().append( "<tr><td style=text-align:center><input type='text' id='per"+i+"' name='per"+i+"' maxlength='6' onkeydown='solonumeros(this);' onkeyup='solonumeros(this)'  /></td><td style=text-align:center><input type='text' id='nom"+i+"' name='nom"+i+"' value='"+salario+"' onkeydown='solonumeros(this);' onkeyup='solonumeros(this)' /></td><td style=text-align:center><input type='text' id='apo"+i+"' name='apo"+i+"' value='"+apoMen+"' onkeydown='solonumeros(this);' onkeyup='solonumeros(this)' /></td><td style=text-align:center><input type='checkbox' id='aju"+i+"' name='ajs"+i+"' /></td><td style=text-align:center><input type='text' id='afi"+i+"' name='afi"+i+"' value='"+ numTrabActivos +"' onkeydown='solonumeros(this);' onkeyup='solonumeros(this)' /></td><td></td></tr>");
		$( "#tablaPeriodos" ).show().append( datosHtml );
		$( "#" + idCampoPeriodo ).bind( 'blur', buscarPeriodoCompare );
		
		$( "#" + idCampoNomina ).bind( 'blur', calculoAporte );
	}
	$("#tablaPeriodos tr:last").show("highlight",1000);
	$("#tablaPeriodos tr input:text").addClass("ui-state-default");
	$("#per0").focus();
}

//METODO PARA BUSCAR PERIODO EL PERIODO EN LA BD,
//SI EXISTE SE MUESTRA LA INFORMACION EN UN DIALOGO 
function buscarPeriodoCompare(){
	//28671
	var idCampo = "#" + $( this ).attr( 'id' );
	if($( idCampo ).val() != ''){
		validaPeriodo( idCampo );
		$.ajax({
			type:"POST", 
			url: "buscarAportesDePeriodoPorNit.php",
			data: {idEmpresa: ide, periodo: $( idCampo ).val()},
			async:false,
			dataType: "json",
			success:function( aportes ){
				$.each(aportes,function(indData,aporte){
					if(aporte.numerorecibo == $("#txtNumero").val() && aporte.periodo == $(idCampo).val()){
						$(idCampo).val("");
						
						alert("El recibo para el periodo " + aporte.periodo + " ya esta grabado.");
						var estilo = "style='text-align:center;'";
						var datosHtml = "<tr><td " + estilo + " >" + aporte.periodo + "</td><td align='center' " + estilo + " >" + aporte.valoraporte + "</td> <td align='center' " + estilo + " >" + aporte.valornomina + "</td> <td align='center' " + estilo + " >" + aporte.ajuste + "</td> <td align='center' " + estilo + " >" + aporte.trabajadores + "</td> <td align='center' " + estilo + " >" + aporte.fechasistema + "</td> </tr>";
						$( "#tbDatosPeriodo" ).html( datosHtml );
						
						//Dialog para mostrar la informacion del periodo
						$( "#divMensaje" ).show();
						$( "#divMensaje" ).dialog({
							autoOpen:true,
							modal:true,
							width:500,
							height:300,
							buttons:{
								Aceptar: function(){
									$( this ).dialog( "close" );
								}
							},
							close: function(){
								$( "#tbDatosPeriodo" ).html("");
								$( this ).focus();
							}
						});
						return false;
					}
				});
				if( aportes.length > 0 )
					$("#aju"+idCampo.replace("#per","")).attr("checked",true);
				else
					$("#aju"+idCampo.replace("#per","")).attr("checked",false);
			}
		});
	}
}

//METODO PARA CALCULAR EL VALOR DEL APORTE,
//DEPENDE DE LA NOMINA Y EL INDICE
function calculoAporte() {
	var idCampoAporte = 'apo'+ $(this).attr('id').replace('nom','');
	var valorNominaPer = parseInt($(this).val());
	var valorAporte = 0;
	
	valorAporte = Math.round((valorNominaPer * parseFloat($("#indice").val())) / 100);
	$("#"+idCampoAporte).val(valorAporte);

}
//METODO PARA VERIFICAR PERIODOS IGUALES EN LOS CAMPOS 
function validaPeriodo(idCampo){
	var periodoIguales = 0;
	$("#tablaPeriodos tr:not(':first')").each(function(n){
		var banderaP = $("#per"+n).val();
		if( $(idCampo).val() == banderaP && idCampo != ("#per"+n)){
			periodoIguales = 1;
		}
	});
	if( periodoIguales > 0 ){
		MENSAJE("Hay periodos iguales al " + $(idCampo).val() + "!");
		$(idCampo).val('').focus();
	}
}

//calcular el valor nomina dependiendo del indice seleccionado
function calcular(){
	var indice = parseFloat($("#indice").val());
	
	if(isNaN(aportes) || indice==0){
		MENSAJE("No hay aportes para calcular!");
		return false;
	}
	
	
	nomina = Math.round((aportes * 100) / indice);
	$("#tdNomina").html(formatCurrency(nomina));
	crearPeriodos();
} 
	
function validarCampos(){
	if(!confirm("Desea guardar los datos!"))
		return false;
	var continuar=0;
//comprobante, documento, codigopun, numerorecibo, planilla, idempresa, periodo, valornomina, valortrabajador, valoraporte, valorpagado, diferencia, trabajadores, ajuste, fechapago, fechasistema, usuario
	var totalN = 0;
	var totalA = 0;
	var indice = parseFloat( $("#indice").val() );
	$("#tablaPeriodos tr:not(':first')").each(function(n){
        var p = $("#per"+n).val();
        var campoPeriodo = $("#per"+n); 
        var observacion = $( "#txaObservacionPeriodo" + n );
		if(p.length != 6){
			MENSAJE("El periodo esta mal escrito!");
			campoPeriodo.removeClass('ui-state-default');
			campoPeriodo.addClass('errorTexto');
			continuar=1;
			return false;
		}
		campoPeriodo.removeClass('errorTexto');
		campoPeriodo.addClass('ui-state-default');
		var a=parseInt(p.substr(0,4));
		if(a<2000 || a>fechaActual.getFullYear()){
			MENSAJE("El a&ntilde;o no corresponde!");
			continuar=1;
			return false;
		}	
		var m=p.substr(4,2);
		if(m=="08"){m=8}
		else if(m=="09"){m=9;}
		else{
		var m=parseInt(p.substr(4,2));
		if(m<1 || m>12){
			MENSAJE("El mes no corresponde!");
			continuar=1;
			return false;
			}
		}
		/*observacion.removeClass('errorTexto').addClass('ui-state-default');
		if( observacion.val().trim() == "" || observacion.val().trim().length > 295){
			MENSAJE("Hay problemas con la observaci\u00F3n!");
			observacion.removeClass('ui-state-default');
			observacion.addClass('errorTexto');
			continuar=1;
			return false;
		}*/
		totalN += parseInt($("#nom"+n).val());
		totalA += parseInt($("#apo"+n).val());
	});

	if(nomina==0 || aportes==0 || ide==0 || indice==0){
		MENSAJE("La informacion esta incompleta!");
		continuar=1;
		return false;
	}
	
	if(continuar==1){
		return false;
	}
	
	var mensaje = "";
	if ( nomina != totalN ){ 
		mensaje = "El total del valor de la nomina de los periodo ( <span style='color:red'>" + formatCurrency( totalN ) + "</span> ) es diferente al original ( <span style='color:red'>" + formatCurrency( nomina ) + "</span> )!";
	}else if ( aportes != totalA ) {
		mensaje = "El total del valor del aporte de los periodo ( <span style='color:red'>" + formatCurrency( totalA ) + "</span> ) es diferente al original ( <span style='color:red'>" + formatCurrency( aportes ) + "</span> )!";
	}
	
	if( mensaje != "" ){
		$( "#divMensajeError" ).html( mensaje );
		$( "#divMensajeError" ).dialog({
			autoOpen:true,
			modal:true,
			width:460,
			title:"REGISTRO DE APORTES | SIGAS",
			buttons: {
				Aceptar:function(){
					$( this ).dialog( "close" );
					$( this ).dialog( "destroy" );
					guardarAporte();
				},
				Cancelar:function(){
					$( this ).dialog( "close" );
					$( this ).dialog( "destroy" );
				}		
			}
		});
		
	}else{
		guardarAporte();
	}
}

function guardarAporte(){
	var indice = parseFloat( $("#indice").val() );
	var bandera = "";
	var comprobante =$("#comprobante").val();
	var documento =$("#txtNumero").val();
	var codigopun="00";
	var numerorecibo =$("#txtNumero").val();
	
	$("#tablaPeriodos tr:not(':first')").each(function(n){
		var caja = 0;
		var icbf = 0;
		var sena = 0;
		var periodo = $("#per"+n).val().trim();
		var no = $("#nom"+n).val();
		var va = $("#apo"+n).val();
		var t = $("#afi"+n).val();
		var a=($("#aju"+n).is(":checked")? 'S' :'N' );
		var fp = fechapago.Anio+"/"+fechapago.Mes+"/"+fechapago.Dia;
		var observacion = $( "#txaObservacionPeriodo" + n ).val().trim();
		
		switch (indice){
			case 0.6:
				caja=va;
				break;
			case 2:
				caja=va;
				break;
			case 4:
				caja=va;
				break;
			case 9:
				caja=parseInt(no*0.04);
				icbf=parseInt(no*0.03);
				sena=va-caja-icbf;
				break;
		}
		
		if (comprobante=='PU2'){
			
			if(Noplanilla[1] == undefined){
				MENSAJE("No existe el Numero de Planilla");
				return false;
			}else{
				var planilla = Noplanilla[1].trim();
			}
			
			caja = 0;
			$.ajax({
				type:"POST",
				url:"ValidarPlanilla.php",
				data: {v0:planilla,v1:ide},
			    async:false,
			    dataType:"json", 
			    success:function(datos1){
					if(datos1==0){
						MENSAJE("No se encontro el numero de planilla " + planilla);
						return false;
					}else{
						
						$.ajax({
							type:"POST",
							url:"guardarAporte.php",
							data: {v0:comprobante,v1:documento,v2:codigopun,v3:numerorecibo,v4:planilla,v5:ide,v6:periodo,v7:no,v8:no,v9:va,v10:va,v11:0,v12:t,v13:a,v14:fp,v15:indice,v16:observacion,v17:caja,v18:icbf,v19:sena},
						    async:false,
						    dataType:"json", 
						    success:function(datos){
								if(datos==0){
									MENSAJE("Se presentaron errores al guardar el aporte del periodo " + periodo);
									return false;
								}else{
									alert("Aporte periodo: " + periodo + " guardado!");
									
								}
						    }
						});
						
					}
					
			    }
			});
			
		}else{
			var planilla = "CAJA";
			
			$.ajax({
				type:"POST",
				url:"guardarAporte.php",
				data: {v0:comprobante,v1:documento,v2:codigopun,v3:numerorecibo,v4:planilla,v5:ide,v6:periodo,v7:no,v8:no,v9:va,v10:va,v11:0,v12:t,v13:a,v14:fp,v15:indice,v16:observacion,v17:caja,v18:icbf,v19:sena},
			    async:false,
			    dataType:"json", 
			    success:function(datos){
					if(datos==0){
						MENSAJE("Se presentaron errores al guardar el aporte del periodo " + periodo);
						return false;
					}else{
						alert("Aporte periodo: " + periodo + " guardado!");
						
					}
			    }
			});
		}
		
	});
	//MENSAJE("Proceso terminado!");
	camposLimpiar();

}	
		
function camposLimpiar(){
	$("#comprobante").focus();
	$("table.tablero input:text").val('');
	$("table.tablero select").val(0);
	$("#tdValor").html("");
	$("#tdFecha").html("");
	$("#tdDes").html("");
	$("#tdNotas").html("");
	$("#tdNit,#tdNomina").html("");
	$("#tablaPeriodos tr:not(':first')").remove();
	nomina=0;
	aportes=0;
	ide=0;
	nuevo=0;
}