<?php
/* autor:       Orlando Puentes 
 * fecha:       Julio 23 de 2010
 * objetivo:    Registrar en la base de datos la informaci�n de la nueva Sucursal de la empresa que ya ha sido radicada.
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
 
$usuario= $_SESSION['USUARIO'];

$url=$_SERVER['PHP_SELF'];
include_once $raiz.DIRECTORY_SEPARATOR.'phpComunes'.DIRECTORY_SEPARATOR.'auditoria.php';
auditar($url);

include_once $raiz.DIRECTORY_SEPARATOR.'config.php';
include_once $raiz.DIRECTORY_SEPARATOR.'funcionesComunes'.DIRECTORY_SEPARATOR.'funcionesComunes.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'p.definiciones.class.php';
include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'ciudades.class.php';
include_once $raiz.DIRECTORY_SEPARATOR.'aportes'.DIRECTORY_SEPARATOR.'radicacion'.DIRECTORY_SEPARATOR.'clases'. DIRECTORY_SEPARATOR.'radicacion.class.php';

$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
global $arregloAgencias;
$objClase=new Definiciones();
$objCiudad=new Ciudades();
$objRadicacion=new Radicacion();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="es">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Sucursales</title>
<link type="text/css" href="../../css/css.1.8/jquery-ui-1.8.17.custom.css" rel="stylesheet">
<link type="text/css" href="../../css/marco.css" rel="stylesheet">
<link type="text/css" href="../../css/Estilos.css" rel="stylesheet">
<script type="text/javascript" src="../../js/1.6/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="../../js/1.6/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="../../js/comunes.js"></script>
<script type="text/javascript" src="../../js/direccion.js"></script>
<script type="text/javascript" src="../../js/jquery.personaSimple.js"></script>
<script type="text/javascript" src="../../js/jquery.combos.js"></script>
<script type="text/javascript" src="js/nuevaSucursal.js"></script>

<script type="text/javascript">
shortcut.add("Shift+F",function() {
		var url="http://10.10.1.121/sigas/aportes/trabajadores/consultaTrabajador.php";
    	window.open(url,"_blank");
    },{
	'propagate' : true,
	'target' : document 
        });        
</script>

<script type="text/javascript">
	$(function() {
		$('#datepicker').datepicker({
			changeMonth: true,
			changeYear: true,
			maxDate: "+0D",
		});
		var anoActual = new Date();
		var strYearRange = anoActual.getFullYear()-120 +":"+ anoActual.getFullYear();
		$(".box1").datepicker( "option", "yearRange", strYearRange );
	});
</script>

<script language="javascript">
//$.ui.dialog.defaults.bgiframe = true;
	$(function() {
		$("#ayuda").dialog({
		 	autoOpen: false,
			height: 450, 
			width: 700, 
			open: function(evt, ui){
					$('#ayuda').html('');
					$.get('../../help/aportes/ayudaSucursal.html',function(data){
							$('#ayuda').html(data);
					});
			 }
		});
	});
</script>

<!-- colaboracion en linea  -->

<script type="text/javascript">
	$(function() {
		$("#dialog-form2").dialog("destroy");
		/*var notas = $("#notas"),
			allFields = $([]).add(notas);*/
		
		$("#dialog-form2").dialog({
			autoOpen: false,
			height: 400,
			width: 500,
			modal: true,
			buttons: {
				'Enviar': function() {
					var bValid = true;
					var campo=$('#notas').val();
					var campo0=$.trim(campo);
					if (campo0==""){
						$(this).dialog('close');
						return false;
						}
					var campo1=$('#usuario').val();
					var campo2="agencias.php";
					$.post('../../phpComunes/colaboracion.php',{v0:campo0,v1:campo1,v2:campo2}, function(datos){
						if(datos=='1'){
							alert("Su comentario fue enviado correctamente!. Gracias por participar en el mejoramiento de SIGAS");
						}else{
							alert(datos);
						}
					});
				$(this).dialog('close');
				},
				Cancelar: function() {
					$(this).dialog('close');
					$("#dialog-form2").dialog("destroy");
				}
			}
			
		});
		
		$('#enviar-notas')
			.button()
			.click(function() {
				$('#dialog-form2').dialog('open');
			});
	});
	</script>
</head>
<body>
<form name="forma">
<div class="demo">
<br />
<center>
<table width="98%" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td width="13" height="29" class="arriba_iz">&nbsp;</td>
	<td class="arriba_ce"><span class="letrablanca">::&nbsp;Administraci&oacute;n - Nueva Sucursal&nbsp;::</span></td>
	 <td width="13" class="arriba_de" align="right">&nbsp;</td>
</tr>
<tr>
	<td class="cuerpo_iz">&nbsp;</td>
	<td class="cuerpo_ce">
    <img src="../../imagenes/spacer.gif" width="4" height="1"/> 
    <img src="../../imagenes/menu/nuevo.png" title="Nuevo" width="16" height="16" style="cursor: pointer" onclick="nuevoR();"/> 
    <img src="../../imagenes/spacer.gif" width="14" height="1"/> 
    <img src="../../imagenes/menu/grabar.png" width="16" height="16" style="cursor: pointer" title="Guardar" onclick="validarCampos(1);" id="bGuardar" /> 
    <img src="../../imagenes/spacer.gif" width="20" height="1"/> 
    <img src="../../imagenes/menu/modificar.png" title="Actualizar" width="16" height="16" onclick="validarCampos(2)" style="cursor: pointer"/> 
	<img src="../../imagenes/spacer.gif" width="22" height="1"/>
	<img src="../../imagenes/menu/imprimir.png" width="16" height="16" style="cursor: pointer" title="Imprimir" onclick="window.print()"/> 
	<img src="../../imagenes/spacer.gif" width="16" height="1"/> 
	<img src="../../imagenes/menu/refrescar.png" width="16" height="16" style="cursor: pointer" title="Limpiar Campos" onclick="limpiarCampos(); camposLimpiar(); document.forms[0].elements[0].focus();"/> 
    <img src="../../imagenes/spacer.gif" width="14" height="1"/> 
    <img src="../../imagenes/menu/buscar.png" title="Buscar" width="16" height="16" style="cursor: pointer" onclick="consultaDatos()"/> 
    <img src="../../imagenes/spacer.gif" width="3" height="1"/> 
    <img src="../../imagenes/menu/informacion.png" width="16" height="16" style="border: none; cursor: pointer" title="Manual" onclick="mostrarAyuda();" /> 
    <img src="../../imagenes/spacer.gif" width="2" height="1"/> 
    <img src="../../imagenes/menu/notas.png" width="16" height="16" style="cursor: pointer" title="Colaborac�on en l�nea" onclick="notas();" /> 
	<br>
	<font size=1 face="arial">Nuevo&nbsp;Guardar&nbsp;Actualizar&nbsp;Imprimir&nbsp;Limpiar&nbsp;Buscar&nbsp;Info&nbsp;Ayuda</font>
	</td>
<td class="cuerpo_de">&nbsp;</td>
</tr>
<tr>
	<td class="cuerpo_iz">&nbsp;</td>
	<td align="left" class="cuerpo_ce"><div id="error" style="color: #FF0000"></div></td>
	<td class="cuerpo_de">&nbsp;</td>
</tr>
<tr>
	<td class="cuerpo_iz">&nbsp;</td>
	<td align="center" class="cuerpo_ce">
	<table width="100%" border="0" cellspacing="0" class="tablero" id="tPrincipal">
	<tr>
	<td width="17%">Radicaci&oacute;n Nro</td>
	<td width="29%"><select id="lisRadicacion" name="lisRadicacion" onblur="buscarRadicacion(this);" >
    <option value="0" selected="selected">Seleccione</option>
    <?php 
    $consulta=$objRadicacion->buscarRadicacionTipoID(105);
    while($row=mssql_fetch_array($consulta)){
		echo "<option value=".$row['idradicacion'].">".$row['idradicacion']."</option>";
	}
    ?>
    </select>
    </td>
	<td width="17%">Fecha</td>
	<td width="37%"><input name=txtId2 class="box1" id="txtId2" readonly="readonly"></td>
	</tr>
	<tr>
	<td>Sector</td>
	<td><select name="select12" class="box1" id="select12" disabled="disabled" >
	<option value="0" selected="selected">Seleccione...</option>
    <?php
	$consulta=$objClase->mostrar_datos(16, 2);
	while($row=mssql_fetch_array($consulta)){
		echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
	}
	?>
    </select> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
	<td>Clase de Sociedad</td>
	<td><select name="select13" class="box1" id="select13" disabled="disabled" >
	<option value="0" selected="selected">Seleccione...</option>
    <?php
	$consulta=$objClase->mostrar_datos(17, 4);
	while($row=mssql_fetch_array($consulta)){
		echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
	}
	?>
    </select> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
	</tr>
	<tr>
	<td><label for="iden">NIT</label></td>
	<td><label> <input name="tNit" type="text" class="box1" id="tNit" readonly="readonly" /> -</label> D&iacute;gito <label for="textfield3"></label> <input name="tDigito" type="text" class="boxcorto" id="tDigito" readonly="readonly" /> 
    <label><img src="../../imagenes/menu/obligado.png" width="12" height="12" /></label></td>
	<td colspan="2"><input type="hidden" name="tidtipo" id="tidtipo" /><div id="div-razon"> </div></td>
	</tr>
	<tr>
	<td>Raz&oacute;n Social</td>
	<td colspan="2"><input name="textfield" type="text" class="boxlargo" id="textfield" onkeyup="copiar(this,$('#textfield2'))"> 
	<img src="../../imagenes/menu/obligado.png" width="12" height="12" /></td>
	<td><div id="div-cuenta" class="Rojo"></div></td>
	</tr>
	<tr>
	<td>Nombre Comercial</td>
	<td colspan="3"><input name="textfield2" type="text" class="boxlargo" id="textfield2" onkeypress="tabular (this, event);" onblur=""> 
	<img src="../../imagenes/menu/obligado.png" width="12" height="12" /></td>
	</tr>
	<tr>
	  <td>Departamento</td>
	  <td><select name="cboDepto2" class="box1" id="cboDepto2">
	    <option value="0" selected="selected">Seleccione</option>
	    <?php
$consulta=$objCiudad->departamentos();
while($row=mssql_fetch_array($consulta)){
	?>
	    <option value="<?php echo $row["coddepartamento"]; ?>"><?php echo $row["departmento"]?></option>
	    <?php
}
?>
	    </select>
	    <img src="../../imagenes/menu/obligado.png" width="12" height="12" /></td>
	  <td>Ciudad</td>
	  <td><select name="cboCiudad2" class="box1" id="cboCiudad2">
	    </select>
	    <img src="../../imagenes/menu/obligado.png" width="12" height="12" /></td>
	  </tr>
	<tr>
	  <td>Direcci&oacute;n</td>
	  <td colspan="3"><input name="tDireccionA" type="text" class="boxlargo" id="tDireccion" onfocus="direccion(this,document.getElementById('textfield8'));" />
        <img src="../../imagenes/menu/obligado.png" width="12" height="12" /></td>
	  </tr>
	<tr>
	  <td>Tel&eacute;fono</td>
	  <td><input name="textfield8" type="text" class="box1" id="textfield8" onkeydown='solonumeros(this);' onkeyup='solonumeros(this);'/> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
	  <td>Fax</td>
	  <td><input name="textfield6" type="text" class="box1" id="textfield6" onkeydown='solonumeros(this);' onkeyup='solonumeros(this);'/></td>
	  </tr>
	<tr>
	<td>URL</td>
	<td><input name="textfield9" type="text" class="box1" id="textfield9" /></td>
	<td>Email</td>
	<td><input name="email" type="text" class="box1" id="textfield15" onblur="soloemail(this);"/> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
	</tr>
	<tr>
	<td>C.C. Representante Legal</td>
	<td><label for="textfield4"></label> <input name="textfield4" type="text" class="box1" id="textfield4" onblur="buscarPersona(this,0);" onkeypress="tabular(this,event);" onkeydown="solonumeros(this);" onkeyup="copiar(this,$('#textfield41'));solonumeros(this);"/>
	<img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
	<td colspan="2"><input type="text" class="boxlargo" id="tNombre1" readonly="readonly"/></td>
	</tr>
	<tr>
	<td>C.C. Jefe Personal</td>
	<td><label for="textfield4"></label> <input name="textfield41" type="text" class="box1" id="textfield41" onblur="buscarPersona(this,1);" onkeypress="tabular(this,event);" /></td>
	<td colspan="2"><input type="text" class="boxlargo" id="tNombre2" readonly="readonly"/></td>
	</tr>
	<tr>
	<td>Contratista</td>
	<td><select name="select4" class="box1" id="select4" disabled="disabled">
	<option value="0" selected="selected">Seleccione</option>
	<option value="N">NO</option>
	<option value="S">SI</option>
	</select> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
	<td>Colegio</td>
	<td><select name="select5" class="box1" id="select5" disabled="disabled"> 
	<option value="0" selected="selected">Seleccione</option>
	<option value="N">NO</option>
	<option value="S">SI</option>
	</select> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
	</tr>
	<tr>
	<td>Excento</td>
	<td><select name="select6" class="box1" id="select6" disabled="disabled">
	<option value="0" selected="selected">Seleccione</option>
	<option value="N">NO</option>
	<option value="S">SI</option>
	</select> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
	<td>Actividad</td>
	<td><select name="select7" class="box1" id="select7" disabled="disabled"> 
	<option value="0" selected="selected">Seleccione...</option>
    <?php
    $sql = "select distinct idciiu,seccion,descripcion FROM aportes079 where division is null and grupo ='' and clase ='' order by idciiu";
    $rs = $db->querySimple($sql);
	while($row=$rs->fetch())
		echo "<option value=".$row['idciiu'].">".$row['descripcion']."</option>";
	?>
    </select> </select> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
	</tr>
	<tr>
	<td>Indice Aporte</td>
	<td><select name="select11" class="box1" id="select11" >
	<option value="0" selected="selected">Seleccione...</option>
    <?php
	$consulta=$objClase->mostrar_datos(18, 4);
	while($row=mssql_fetch_array($consulta)){
		echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
	}
	?>
	</select> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
	<td>Asesor</td>
	<td><select name="select8" class="box1" id="select8">
	<option value="0" selected="selected">Seleccione</option>
	<option value="1">Presentaci&oacute;n personal</option>
	</select> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
	</tr>
	<tr>
	<td>Seccional</td>
	<td><select name="select10" class="box1" id="select10">
	<option value="0" selected="selected">Seleccione</option>
	<?php foreach ($arregloAgencias as $pos => $nombre)
	{
		echo "<option value=".$pos.">$nombre</option>";
	}?>
	</select><img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
	<td>Estado</td>
	<td><select name="select9" class="box1" id="select9">
	<option value="0" selected="selected">Seleccione</option>
	<option value="A">Activo</option>
	<option value="I">Inactivo</option>
	</select> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
	</tr>
	<tr>
	<td>Fecha Afiliaci&oacute;n</td>
	<td><input name="textfield18" type="text" class="box1" id="textfield18" readonly="readonly" value="" /></td>
	<td>Fecha inicio aportes</td>
	<td><input name="datepicker" type="text" class="box1" id="datepicker" readonly="readonly" value="" /></td>
	</tr>
	<tr>
	<td>usuario</td>
	<td><input name="textfield19" type="text" class="box1" id="textfield19" readonly="readonly" value="<?php echo $_SESSION["USUARIO"]; ?>" /></td>
	<td>Fecha Constituci&oacute;n</td>
	<td><input name="datepickerConstitucion" disabled="disabled" type="text" class="box1" id="datepickerConstitucion" readonly="readonly" value="" /></td>
	</tr>
	<tr>
	<td>Hist�rico de Observaciones</td>
	<td colspan="3"><div id="div-observaciones"></div>&nbsp;</td>
	</tr>
	</table>
	<table width="100%" border="0" cellspacing="0">
    <tr>
	<td align="left">&nbsp;</td>
	</tr>
	<tr>
	<td align="left" class="Rojo"><img src="../../imagenes/menu/obligado.png" width="16" height="12" align="left" />Campo Obligado</td>
	</tr>
	</table>
	</td>
	<td class="cuerpo_de">&nbsp;</td>
	</tr>
	<tr>
	<td class="abajo_iz" >&nbsp;</td>
<td class="abajo_ce" ></td>
<td class="abajo_de" >&nbsp;</td>
	</tr>
</table>
</center>
<input type="hidden" name="tIdRadicacion" id="tIdRadicacion" /> 
<input type="hidden" name="tNit2" id="tNit2" /> 
<input type="hidden" name="tFechaRadicacion" id="tFechaRadicacion" /> 
<input type="hidden" value="<?php echo $usuario; ?>" name="usuario" id="usuario" />
<input type="hidden" name="tCodigoSucursal" id="tCodigoSucursal" />

<!-- direcciones -->

<div id="dialog-form" title="Formulario de direcciones"></div>

<!-- colaboracion en linea -->
<!-- FORMULARIO OBSERVACIONES-->
<div name="div-observaciones-tab" style="display:none" title="Observaciones de modificaci&oacute;n">
<table class="tablero">
 <tr>
   <td>Usuario</td>
   <td colspan="3" >
   <input name="usuarioObs" id="usuarioObs" class="box1" disabled="disabled" value="<?php echo $_SESSION['USUARIO']?>" /></td>
   </tr>
 <tr>
   <td>Observaciones</td>
   <td colspan="3" >
   <textarea name="observacionGrupo" id="observacionGrupo" cols="45" rows="5" class="boxlargo"></textarea></td>
   </tr>
</table>
<div class="ui-state-highlight ui-corner-all" style="margin: 9px auto; display:none;padding: 6px; text-align: center; border: 1px solid rgb(255, 255, 0); width:300px" id="rtaObservacion"><span class="ui-icon ui-icon-info" style="float: left; margin-right: 0.3em;"></span>Observaci&oacute;n guardada con &eacute;xito.</div>
</div>

<!-- fin colaboracion -->
<div id="dialog-persona" title="Formulario Datos Basicos"></div>
<!-- ayuda en linea -->
<div id="ayuda" title="Manual .:. Nueva Sucursal" style="background-image:url('../../imagenes/FondoGeneral0.png')">
</div>
<input type="hidden" name="idRepresentante" id="idRepresentante" />
<input type="hidden" name="idJefe" id="idJefe" />
</form>

</body>
<script language="javascript">
function mostrarAyuda(){
	$("#ayuda").dialog('open' );
	}

function notas(){
	$("#dialog-form2").dialog('open');
	}	
</script>
</html>
