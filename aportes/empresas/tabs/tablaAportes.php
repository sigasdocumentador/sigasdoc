<?php
/* autor:       orlando puentes
 * fecha:       01/07/2010
 * objetivo:    
 */
 
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$url=$_SERVER['PHP_SELF'];
include_once $raiz.DIRECTORY_SEPARATOR ."config.php";
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejaInforma.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';

$db = IFXDbManejaInforma::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

$idemp= $_POST['v0'];
$limite=$_POST['v1'];
include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'p.aportes.class.php';
//include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';
$objAportes = new Aportes;
$consulta=$objAportes->buscar_aportes($idemp,$limite);

 ?>
 <head>
 <title>APORTES EMPRESA</title>
 <link href="<?php echo URL_PORTAL; ?>css/sorterStyle.css" rel="stylesheet" type="text/css" media="screen" />
 <script type="text/javascript" src="../../js/script3.js"></script>
<script type="text/javascript">
  var sorter = new TINY3.table3.sorter("sorter");
	sorter.head = "head";
	sorter.asc = "asc";
	sorter.desc = "desc";
	sorter.even = "evenrow";
	sorter.odd = "oddrow";
	sorter.evensel = "evenselected";
	sorter.oddsel = "oddselected";
	sorter.paginate = true;
	sorter.pagesize = (5);
	sorter.currentid = "currentpage";
	sorter.limitid = "pagelimit";
	sorter.init("table",2);
</script>
 
 </head>
 <body>
 <table border="0">
 	<tbody>
		<tr>
			<td>
				<h4>Consulta Aportes </h4>
			</td>
			<td class="t1">&nbsp;&nbsp;(Convensiones: C-Comprobante. Rec-Recibo Cont. Per-Periodo TotalA-Total aporte Vr.Dev.-Valor Devoluci&oacute;n. I-Indice. T-Trabajs. A-Ajuste. FechaP-Fecha Pago. FechaS-Fecha Sis. Ob-Observaci&oacute;n)</td>
		</tr>
	</tbody>
 </table>
<table  cellpadding="0" cellspacing="0"  id="table" border="0"  class="sortable" width="90%">
  <thead>
  <tr>
    <th><h3>C</h3></th>
    <th><h3>Rec</h3></th>
    <!--<th><h3>Número&nbsp;</h3></th>-->
    <th><h3>Per</h3></th>
    <th><h3>Nomina</h3></th>
    <th><h3>Aporte</h3></th>
    <th><h3>Mora</h3></th>
    <th><h3>TotalA</h3></th>
    <th><h3>CAJA</h3></th>
    <th><h3>ICBF</h3></th>
    <th><h3>SENA</h3></th>
    <th><h3>Vr.Dev</h3></th>
    <th><h3>I%</h3></th>
    <th><h3>T</h3></th>
    <th><h3>A</h3></th>
    <th><h3>FechaP</h3></th>
    <th><h3>FechaS</h3></th>
    <th><h3>Usuario</h3></th>
    <th><h3>Ob</h3></th>
    <th style="display: none;"><h3>Observaci&oacute;n</h3></th>
   </tr>
 </thead>
 <tbody>
<?php 
$cont = 0;
while($row=mssql_fetch_array($consulta)){
	
	//Proceso para obtener el motivo de la devolucion
	$valorDev = 0;
	$sqlDev = "SELECT (aaportes-daportes) as valordevolucion, motivo, fechadevolucion FROM aportes058 WHERE idaporte=".$row["idaporte"];
	$rsDev = $db->querySimple($sqlDev);
	$b = 0;
	$motivo = "";
	while( ($rowDev = $rsDev->fetch() ) == true ){
		$b++;
		$valorDev += intval( $rowDev["valordevolucion"] );
		$motivo .= "<tr><td style='text-align:right'>$b</td><td>".$rowDev["motivo"]."</td><td>".$rowDev["fechadevolucion"]."</td></tr>";
	}
	
	//Proceso para obtener las observaciones del registro y traslados de aportes
	$rsNota = $objAportes->consultar_notas($row["idaporte"]);
	$b = 0;
	$notas = "";
	$observacionTraslado = "";
	while( $rsNot = $rsNota->fetch()){
		if($rsNot["tipo"]=="G" || $rsNot["tipo"]=="M"){
			$b++;
			$notas .= "<tr><td style='text-align:right'>$b</td><td>".$rsNot["notas"]."</td></tr>";
		}else if($rsNot["tipo"]=="T"){
			$observacionTraslado .= "<tr><td>{$rsNot["notas"]}</td><td>{$row["fechatraslado"]}</td><td>{$row["usuariotraslado"]}</td></tr>";
		}
	}
	
	$rsMora = $db->querySimple( "SELECT isnull(sum(DISTINCT mora),0) as mora FROM aportes031 WHERE nit='".$row["nit"]."' AND planilla='".$row["planilla"]."' AND periodo='".$row["periodo"]."' AND fechapago='".$row["fechapago"]."'");
	$mora = 0;
	if( $rowMora = $rsMora->fetch() )
		$mora = $rowMora["mora"];
	
	//Proceso para obtener las observaciones de las interrupciones
	$rsInterrupcion = $db->querySimple(
			"SELECT 
				a37.observaciones, a37.usuario, a37.fechasistema
			FROM aportes037 a37
				INNER JOIN aportes038 a38 ON a38.idinterrupcion=a37.idinterrupcion
			WHERE a37.idempresa=$idemp AND a38.periodo='{$row['periodo']}'");
	
	$observacionInterrupcion = "";
	while( $rowInterrupcion = $rsInterrupcion->fetch() ) {
		$observacionInterrupcion .= "<tr><td>".$rowInterrupcion["observaciones"]."</td><td>".$rowInterrupcion["fechasistema"]."</td></tr>";
	}
	
	//Proceso para obtener las observaciones de los traslados
?>  

  <tr>
    <td style="text-align:right"><?php echo $row['comprobante']; ?>&nbsp;</td>
    <td style="text-align:right"><?php echo $row['numerorecibo']; ?>&nbsp;</td>
    <td style="text-align:right"><?php echo $row['periodo']; ?>&nbsp;</td>
    <td style="text-align:right" ><?php echo number_format($row['valornomina']); ?>&nbsp;</td>
    <td style="text-align:right"><?php echo number_format($row['valoraporte']-$mora); ?>&nbsp;</td>
    <td style="text-align:right"><?php echo number_format($mora); ?>&nbsp;</td>
    <td style="text-align:right"><?php echo number_format($row['valoraporte']); ?>&nbsp;</td>
    <td style="text-align:right"><?php echo number_format($row['caja']); ?>&nbsp;</td>
    <td style="text-align:right"><?php echo number_format($row['icbf']); ?>&nbsp;</td>
    <td style="text-align:right"><?php echo number_format($row['sena']); ?>&nbsp;</td>
    <td style="text-align:right"><?php echo number_format($valorDev); ?>&nbsp;</td>
    <td style="text-align:right"><?php echo $row['indicador']; ?>&nbsp;</td>
    <td style="text-align:right"><?php echo $row['trabajadores']; ?>&nbsp;</td>
    <td style="text-align:center"><?php echo $row['ajuste']; ?>&nbsp;</td>
    <td><?php echo $row['fechapago']; ?>&nbsp;</td>
    <td><?php echo $row['fechasistema']; ?>&nbsp;</td>
    <td><?php echo $row['usuario']; ?>&nbsp;</td>
    <td style="text-align: center;">
    	<?php 
    		if( $notas != "" || $motivo != "" || $observacionInterrupcion != "" || $observacionTraslado != "")
    			echo "<img src='../../imagenes/menu/informacion.png' width='14' height='14' style='cursor:pointer' onclick='mostrarObservacion(\"tdObservacion$cont\");' />";
    	?>&nbsp;
    </td>
    <td style="display: none; "id="tdObservacion<?php echo $cont?>">
    	<center>
    		<!-- ### Observacion Registro de Aportes -->
    		<h3>Observaciones Registro de Aportes</h3>
    		<table class="tablero" width="80%">
    			<thead>
		    		<tr>
		    			<th width="5%">Cont</th><th width="90%">Observaci&oacute;n</th>
		    		</tr>
		    	</thead>
	    		<tbody><?php echo $notas; ?></tbody>
    		</table>
    		<!-- ### Motivo -->
    		<h3>Motivo Devoluci&oacute;n de Aportes</h3>
    		<table class="tablero" width="80%">
    			<thead>
		    		<tr>
		    			<th width="5%">Cont</th><th width="90%">Motivo</th><th>Fecha</th>
		    		</tr>
		    	</thead>
	    		<tbody><?php echo $motivo; ?></tbody>
    		</table>
    		<!-- ### Observacion Traslados -->
    		<h3>Observaciones de los Traslados</h3>
    		<table class="tablero" width="80%">
    			<thead>
		    		<tr>
		    			<th width="90%">Observaci&oacute;n</th><th>Fecha Traslado</th><th>Usuario Traslado</th>
		    		</tr>
		    	</thead>
	    		<tbody><?php echo $observacionTraslado; ?></tbody>
    		</table>
    		<!-- ### Observacion Interrupcion -->
    		<h3>Observaciones de las Interrupciones</h3>
    		<table class="tablero" width="80%">
    			<thead>
		    		<tr>
		    			<th width="90%">Observaci&oacute;n</th><th>Fecha</th>
		    		</tr>
		    	</thead>
	    		<tbody><?php echo $observacionInterrupcion; ?></tbody>
    		</table>
    	</center>	
    </td>
  </tr>
<?php $cont++;}?>  
</tbody>
</table>
    	<div id="controls">
		<div id="perpage">
			<select onChange="sorter.size(this.value)">
			<option value="5" selected="selected">5</option>
				<option value="10" >10</option>
				<option value="20">20</option>
				<option value="50">50</option>
				<option value="100">100</option>
			</select>
			<label>Registros Por P&aacute;gina</label>
		</div>
		<div id="navigation">
			<img src="../../imagenes/imagesSorter/first.gif" width="16" height="16" alt="first Page" onClick="sorter.move(-1,true)" />
			<img src="../../imagenes/imagesSorter/previous.gif" width="16" height="16" alt="first Page" onClick="sorter.move(-1)" />
			<img src="../../imagenes/imagesSorter/next.gif" width="16" height="16" alt="first Page" onClick="sorter.move(1)" />
			<img src="../../imagenes/imagesSorter/last.gif" width="16" height="16" alt="Last Page" onClick="sorter.move(1,true)" />
		</div>
		<div id="text">P&aacute;gina <label id="currentpage"></label> de <label id="pagelimit"></label></div>
	</div>
	    	</body>