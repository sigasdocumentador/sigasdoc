<?php
/* autor:       orlando puentes
 * fecha:       06/07/2010
 * objetivo:    
 */
 
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;


$idemp=$_POST['v0'];
$per=$_POST['v1'];
include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'p.cuotaMonetaria.class.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';
$objCuota = new CuotaMonetaria;
$consulta = $objCuota->Select_Cuota_Monetaria($idemp,$per);
?>
<head>
  <title>CUOTA MONETARIA</title>
  <link href="<?php echo URL_PORTAL; ?>css/sorterStyle.css" rel="stylesheet" type="text/css" media="screen" />
<script type="text/javascript" src="../../js/script.js"></script>
<script type="text/javascript">
  var sorter = new TINY.table.sorter("sorter");
	sorter.head = "head";
	sorter.asc = "asc";
	sorter.desc = "desc";
	sorter.even = "evenrow";
	sorter.odd = "oddrow";
	sorter.evensel = "evenselected";
	sorter.oddsel = "oddselected";
	sorter.paginate = true;
	sorter.pagesize = (5);
	sorter.currentid = "currentpageCM";
	sorter.limitid = "pagelimitCM";
	sorter.init("tableCM",1);
  </script>
 
 </head>
<body>
<h4>Cuota Monetaria</h4>
<table  cellpadding="0" cellspacing="0" border="0" id="tableCM" class="sortable" width="74%"  >
  <thead>
  <tr >
	    <th width="8%"><h4><strong>Identificacion</strong></h4></th>
	    <th width="25%"><h4><strong>Nom. Afiliado</strong></h4></th>
	    <th width="8%"><h4><strong>idBeneficiario</strong></h4></th>
	    <th width="25%"><h4><strong>Nom. Beneficiario</strong></h4></th>
	    <th width="8%"><h4><strong>Valor</strong></h4></th>	    
  </tr>
  </thead>
  <tbody>
<?php 
$cont=0;
while($row=mssql_fetch_array($consulta)){
	$nomA=$row['pnombreA']." ".$row['snombreA']." ".$row['papellidoA']." ".$row['sapellidoA'];
	$nomB=$row['pnombreB']." ".$row['snombreB']." ".$row['papellidoB']." ".$row['sapellidoB'];
?>	
	<tr>
	    <td align=right style="font-size:9px "><h4><?php echo $row['identificacion']; ?></h4></td>
	    <td style="font-size:9px"><h4><?php echo $nomA; ?>&nbsp;</h4></td>
	    <td align="right" style="font-size:9px"><h4><?php echo $row['idbeneficiario']; ?></h4></td>
	    <td style="font-size:9px"><h4><?php echo $nomB; ?>&nbsp;</h4></td>	    
	    <td align="right" style="font-size:9px"><h4><?php echo number_format($row['valor']); ?></h4></td>
    </tr>
<?php $cont++;}
if($cont==0){
	echo "<tr><td class='Rojo' colspan='26' align='center'>No hay informaci&oacute;n del periodo</td></tr>";
	exit();
	}
 ?> 
</tbody>
</table>
<div id="controls">
<div id="perpage">
			<select onChange="sorter.size(this.value)">
			<option value="5" selected="selected">5</option>
				<option value="10" >10</option>
				<option value="20">20</option>
				<option value="50">50</option>
				<option value="100">100</option>
			</select>
			<label>Registros Por P&aacute;gina</label>
  </div>
		<div id="navigation">
			<img src="../../imagenes/imagesSorter/first.gif" width="16" height="16" alt="first Page" onClick="sorter.move(-1,true)" />
			<img src="../../imagenes/imagesSorter/previous.gif" width="16" height="16" alt="first Page" onClick="sorter.move(-1)" />
			<img src="../../imagenes/imagesSorter/next.gif" width="16" height="16" alt="first Page" onClick="sorter.move(1)" />
			<img src="../../imagenes/imagesSorter/last.gif" width="16" height="16" alt="Last Page" onClick="sorter.move(1,true)" />
		</div>
		<div id="text">P&aacute;gina <label id="currentpageCM"></label> de <label id="pagelimitCM"></label></div>
</div>
    

</body>
