<?php
/* autor:       orlando puentes
 * fecha:       03/08/2010
 * objetivo:    
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;


$ide = $_SESSION['v0'];
$servidor='http://10.10.1.55:8080/sigas_imagenes/';
include_once $raiz.DIRECTORY_SEPARATOR.'config.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
 	$cadena = $db->error;
 	echo msg_error($cadena);
	exit();
 }
$sql="Select rutadocumentos from aportes048 where idempresa='$ide'";
$rs=$db->querySimple($sql);
$w=$rs->fetch();
$ruta=$w['rutadocumentos'];
$ruta=str_replace("\\", "/", $ruta);
$rs->closeCursor();
$sql="Select distinct archivo from aportes051 where idempresa='$ide'";
$rs=$db->querySimple($sql);
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
<title>Documentos Digitalizados</title>
<meta name="author" content="Orlando Puentes" />
<link href="../../../css/jbgallery-3.0.css" rel="stylesheet" media="screen" />
<script src="../../../js/ui/jquery-1.4.2.js" type="text/javascript"></script>
<script src="../../../js/jbgallery-3.0.js" type="text/javascript"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
		var _obj = 
		jQuery("#hundred")
		.jbgallery({
			menu : 'slider',
			ready : function(){		
			} 
		}, true);;//API
        
	});
</script>
</head>
<body>
<?php
echo $sql;
?>
<div class="jbgallery" id="hundred">
<ul>
<?php
$con=0;
while ($w=$rs->fetch()){
	$imagen=$servidor.$ruta.$w['archivo'];
	echo "<li><a href=$imagen>$w</a></li>";
	$con++;
}
if($con==0){
?>
<li><a title="No tiene documentos digitalizados"
		href="http://10.10.1.55:8080/sigas_imagenes/digitalizacion/sindocumentos/no_hay5.png">1</a></li>
	<li><a title="No tiene documentos digitalizados"
		href="http://10.10.1.55:8080/sigas_imagenes/digitalizacion/sindocumentos/no_hay4.png">2</a></li>
	<li><a title="No tiene documentos digitalizados"
		href="http://10.10.1.55:8080/sigas_imagenes/digitalizacion/sindocumentos/no_hay3.png">3</a></li>
	<li><a title="No tiene documentos digitalizados"
		href="http://10.10.1.55:8080/sigas_imagenes/digitalizacion/sindocumentos/no_hay1.png">4</a></li>
	<li><a title="No tiene documentos digitalizados"
		href="http://10.10.1.55:8080/sigas_imagenes/digitalizacion/sindocumentos/no_hay2.png">5</a></li>
<?php }?>
</ul>
</div>
</body>
</html>