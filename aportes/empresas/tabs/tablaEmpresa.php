<?php
/* autor:       orlando puentes
 * fecha:       26/07/2010
 * objetivo:    
 */
ini_set("display_errors","1");
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;


include_once $raiz.DIRECTORY_SEPARATOR.'seguridad'.DIRECTORY_SEPARATOR.'phpComunes'.DIRECTORY_SEPARATOR. 'funciones.php';
$ide=$_REQUEST['v0'];
include_once $raiz.DIRECTORY_SEPARATOR.'config.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
global $arregloAgencias;
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
		$sql="SELECT DISTINCT a48.idempresa, a48.idtipodocumento, nit, digito, codigosucursal, principal, razonsocial,
				sigla, a48.direccion, iddepartamento, idciudad, a48.idzona, a48.telefono, fax, a48.url, a48.email, idrepresentante,
				idjefepersonal, contratista, idcodigoactividad, actieconomicadane, idasesor
				, fechamatricula,CASE WHEN a48.estado='I' THEN a48.fechaestado ELSE null END AS fechaestado,
				idsector, seccional, tipopersona, claseaportante, tipoaportante, a48.estado, codigoestado, fechaaportes,
				a48.fechaafiliacion, trabajadores, aportantes, a48.usuario, a48.fechasistema, a48.giro,idclasesociedad, a48.rutadocumentos,
				legalizada, a48.direcorresp, d.departmento,m.municipio ,a89.departmento AS depcorresp,a89.municipio AS ciucorresp
				,r.papellido AS par,r.sapellido AS sar,r.pnombre AS pnr,r.snombre snr, c.papellido AS pac
				,c.sapellido AS sac,c.pnombre AS pnc,c.snombre snc, ca.descripcion AS actividad, da.clase AS codact, da.descripcion AS dane
				,isnull(i.detalledefinicion,indicador) AS indicador, s.detalledefinicion AS sector
				,p.detalledefinicion tipopersona,cs.detalledefinicion AS clasesoci, capo.detalledefinicion AS clase_apo
				,ta.detalledefinicion AS tipo_apo, td.detalledefinicion AS tipo_doc, a91.detalledefinicion AS causal_inactivacion
				, b91.detalledefinicion AS tipo_afiliacion
				, a87.barrio AS barri_corre_empre
				, b87.barrio AS barri_empre
				, a48.celular
			FROM aportes048 a48
				LEFT JOIN aportes089 d ON a48.iddepartamento=d.coddepartamento OR (isnumeric(a48.iddepartamento)=1 AND isnumeric(d.coddepartamento)=1 AND CAST(a48.iddepartamento AS INT)=CAST(d.coddepartamento AS INT))
				LEFT JOIN aportes089 m ON a48.idciudad=m.codmunicipio OR (isnumeric(a48.idciudad)=1 AND isnumeric(m.codmunicipio)=1 AND CAST(a48.idciudad AS INT)=CAST(m.codmunicipio AS INT))
				LEFT JOIN aportes015 r ON a48.idrepresentante=r.idpersona
				LEFT JOIN aportes015 c ON a48.idjefepersonal=c.idpersona
				LEFT JOIN aportes079 ca ON a48.idcodigoactividad=ca.idciiu
				LEFT JOIN aportes079 da ON a48.actieconomicadane=da.clase
				LEFT JOIN aportes091 i ON a48.indicador=i.iddetalledef
				LEFT JOIN aportes091 s ON a48.idsector=s.iddetalledef
				LEFT JOIN aportes091 p ON a48.tipopersona=p.iddetalledef
				LEFT JOIN aportes091 cs ON a48.idclasesociedad=cs.iddetalledef
				LEFT JOIN aportes091 capo ON a48.claseaportante=capo.iddetalledef
				LEFT JOIN aportes091 ta ON a48.tipoaportante=ta.iddetalledef
				LEFT JOIN aportes091 td ON a48.idtipodocumento=td.iddetalledef
				LEFT JOIN aportes091 a91 ON a91.iddetalledef=a48.codigoestado
				LEFT JOIN aportes091 b91 ON b91.iddetalledef=a48.idtipoafiliacion
				left join aportes089 a89 on a89.coddepartamento=a48.iddepcorresp AND a89.codmunicipio=a48.idciucorresp
				left join aportes087 a87 on a87.idbarrio=a48.idbarriocorresp
				left join aportes087 b87 on b87.idbarrio=a48.idbarrio
			where idempresa=$ide";

$rs=$db->querySimple($sql);
$row=$rs->fetch();
//$row=array_map("utf8_encode",$row);

/////ENTRO POR PLANILLA UNICA
$obsPU="";
$filtro=" AND a48.idempresa=$ide ";
include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'p.empresas.class.php';
$objClase=new Empresa;
$consulta = $objClase->buscar_empresa_p_planillaUnica($filtro);
$consulta = mssql_fetch_array($consulta);
if(intval($consulta['cant']) > 0){
	$obsPU = "No se ha radicado, entr&oacute; por Planilla &Uacute;nica.";
}
///////////////////////////////
?>
<h4>Datos de la Empresa</h4>
<table width="100%" class="tablero" >
<tr>
<td width="15%">Sector</td>
<td width="30%" colspan="2"><?php echo $row['sector']; ?></td>
<td width="15%">Clase de Sociedad</td>
<td width="40%"><?php echo $row['clasesoci'];?></td>
</tr>
<tr>
<td><label for="iden">NIT</label></td>
<td width="15%"><label class="box1" ><?php echo $row['idempresa'] . " - " . $row['nit']; ?></label></td>
<td width="15%">D&iacute;gito <label ><?php echo $row['digito']; ?></label></td>
<td>Tipo Documento</td>
<td><label><?php echo $row['tipo_doc'];?>
</label></td>
</tr>
<tr>
<td>Raz&oacute;n Social</td>
<td colspan="3"><label ><?php echo $row['razonsocial']; ?></label></td>
<td>C&oacute;digo Sucursal - <label ><?php echo $row['codigosucursal']; ?></label></td>
</tr>
<tr>
<td>Nombre Comercial</td>
<td colspan="4"><label ><?php echo $row['sigla']; ?></label></td>
</tr>
<tr>
<td>Direcci&oacute;n</td>
<td colspan="4"><label ><?php echo $row['direccion']; ?></label></td>
</tr>
<tr>
<td>Departamento</td>
<td colspan="2"><?php echo $row['departmento']; ?> </td>
<td>Ciudad</td>
<td><label ><?php echo $row['municipio']; ?> </label></td>
</tr>

<tr>
	<td>Barrio</td>
	<td colspan="2"><?php echo $row['barri_empre']; ?> </td>
	<td></td>
	<td></td>
</tr>

<tr>
<td>Direcci&oacute;n De Correspondencia</td>
<td colspan="4"><label ><?php echo $row['direcorresp']; ?></label></td>
</tr>
<tr>
<td>Departamento De Correspondencia</td>
<td colspan="2"><?php echo $row['depcorresp']; ?> </td>
<td>Ciudad De Correspondencia</td>
<td><label ><?php echo $row['ciucorresp']; ?> </label></td>
</tr>

<tr>
	<td>Barrio De Correspondencia</td>
	<td colspan="2"><?php echo $row['barri_corre_empre']; ?> </td>
	<td></td>
	<td></td>
</tr>

<tr>
<td>Tel&eacute;fono</td>
<td colspan="2"><label ><?php echo $row['telefono']." - ".$row['celular']; ?></label></td>
<td>Fax</td>
<td><label ><?php echo $row['fax']; ?></label></td>
</tr>
<tr>
<td>URL</td>
<td colspan="2"><label ><?php echo $row['url']; ?></label></td>
<td>Email</td>
<td><label ><?php echo $row['email']; ?></label></td>
</tr>
<tr>
<td> Representante Legal</td>
<td colspan="4">
<?php 
$nom= $row['pnr']." ".$row['snr']." ".$row['par']." ".$row['sar'];
echo $nom; 
?>
</td>
</tr>
<tr>
  <td>Contacto</td>
  <td colspan="4"><?php 
$nom= $row['pnc']." ".$row['snc']." ".$row['pac']." ".$row['sac'];
echo $nom; 
?>	</td>
</tr>
<tr>
  <td>Contratista</td>
  <td colspan="2"><label ><?php echo $row['contratista']; ?></label></td>
  <td>Actividad</td>
  <td><?php echo $row['actividad'];?></td>
</tr>
<tr>
  <td>Actividad Econ&oacute;mica DANE</td>
  <td colspan="4"><?php echo $row['codact'];?> - <?php echo $row['dane'];?></td>
</tr>
<tr>
  <td>&Iacute;ndice Aporte</td>
  <td colspan="2"><label>
  <?php 
echo $row['indicador'];
?>
  </label></td>
  <td>Asesor</td>
  <td><label>
  <?php 
echo $row['idasesor'];
?>
  </label></td>
</tr>
<tr>
<td><label >Legalizada: <?php echo $row['legalizada']; ?></label></td>
<td>Seccional</td>
<td><label >
<?php 
echo $arregloAgencias[$row['seccional']]; ?>
</label></td>
<td>Tipo Afiliaci&oacute;n</td>
<td><?php echo $row["tipo_afiliacion"];?></td>
</tr>

<tr>
<td>Clase Aportante</td>
<td colspan="2"><?php echo $row['clase_apo']; ?></td>
<td>Tipo Aportante</td>
<td><?php echo $row['tipo_apo']; ?></td>
</tr>
<tr>
	<td>Fecha Afiliaci&oacute;n</td>
	<td colspan="2"><label ><?php echo $row['fechaafiliacion']; ?></label></td>
	<td>Fecha Estado</td>
	<td>
		<label >
			<?php echo $row['fechaestado']; if($obsPU != ""): ?>
				<img src="<?php echo URL_PORTAL; ?>imagenes/messagebox_warning.png" style="height:14px; width:14px;" title="<?php echo $obsPU; ?>" />
			<?php endif; ?>
		</label>
	</td>
</tr>

<tr>
<td>Estado</td>
<td ><?php echo $row['estado'];?></td>
<td>Causal</td>
<td colspan="2">
	<?php echo $row['causal_inactivacion'];//($row['estado']=='I')?$row['causal_inactivacion']:"";?>
</td>
</tr>


<tr>
  <td>Fecha Constituci&oacute;n</td>
  <td colspan="2"><?php echo $row['fechamatricula']; ?></td>
  <td>Fecha Inicio Aportes</td>
  <td><label ><?php echo $row['fechaaportes']; ?></label></td>
</tr>
<tr>
<td>Usuario</td>
<td colspan="2"><label ><?php echo $row['usuario']; ?></label></td>
<td>Fecha Grabación</td>
<td><?php echo $row['fechasistema']; ?></td>
</tr>
<tr>
<td>Observaciones</td>
<td colspan="4">
<?php 
$rs=$db->b_obserbacion($ide, 2);
echo $rs;
?>
</td>
</tr>
<tr>
  <td>Giro</td>
  <td colspan="4"><label>
  <?php 
echo $row['giro'];
?>
  </label></td></tr>
</table>
