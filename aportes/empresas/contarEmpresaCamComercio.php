<?php
set_time_limit(0);
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$usuario= $_SESSION["USUARIO"];

include_once $raiz.DIRECTORY_SEPARATOR.'funcionesComunes'.DIRECTORY_SEPARATOR.'funcionesComunes.php';

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

$razonSocial = (isset($_REQUEST["razonSocial"]) && $_REQUEST["razonSocial"] != "")?$_REQUEST["razonSocial"]:null;
$nit = (isset($_REQUEST["nit"]) && $_REQUEST["nit"] != "")?$_REQUEST["nit"]:null;
$strCondicionBusqueda = "";

if($razonSocial != null)
	$strCondicionBusqueda = " AND razonsocial like '%{$razonSocial}%' ";
else if($nit != null)
	$strCondicionBusqueda = " AND nit like '%{$nit}%' ";

$sql="SELECT count(*) as total 
		FROM aportes048 a48
		INNER JOIN aportes091 td ON a48.idtipodocumento = td.iddetalledef
		WHERE a48.claseaportante=2875
		AND 'I'= isnull(
			(
				SELECT TOP 1 b73.estado 
				FROM aportes073 b73 
				WHERE b73.idempresa=a48.idempresa
				ORDER BY b73.fecharenovacion DESC
			)
		,'I') {$strCondicionBusqueda}";
$result = $db->querySimple($sql);
$registro = $result->fetch();
if(is_array($registro))
	echo intval($registro["total"]);
else
	echo 0;
?>