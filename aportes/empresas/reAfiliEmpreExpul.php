<?php
	set_time_limit(0);
	ini_set("display_errors",'1');
	date_default_timezone_set("America/Bogota");
	$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
	include_once  $root;
	include_once  $raiz.DIRECTORY_SEPARATOR .'config.php';
	include_once $raiz. DIRECTORY_SEPARATOR .'clases'. DIRECTORY_SEPARATOR . 'p.definiciones.class.php';
	
	$FrmActual=basename($_SERVER['PHP_SELF']);
	$fecver = date('Ymd h:i:s A',filectime($FrmActual));
	
	//Objeto de la definicion
	$objDefiniciones = new Definiciones();
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<title>Re Afiliacion Empresas Expulsada</title>
		<link type="text/css" href="../../css/Estilos.css" rel="stylesheet"/>
		<link type="text/css" href="../../css/marco.css" rel="stylesheet"/>
		<link type="text/css" rel="stylesheet" href="<?php echo URL_PORTAL; ?>css/css.1.8/jquery-ui-1.8.17.custom.css" />
		<script language="javascript" src="<?php echo URL_PORTAL; ?>js/1.6/jquery-1.6.2.min.js"></script>
		<script language="javascript" src="<?php echo URL_PORTAL; ?>js/1.6/jquery-ui-1.8.16.custom.min.js"></script>
		<script language="javascript" src="<?php echo URL_PORTAL; ?>js/comunes.js"></script>
		<script language="javascript" src="js/reAfiliEmpreExpul.js"></script>
		<style type="text/css">
			.classCampoCorto {background: none repeat scroll 0 0 #F7F7F7; border-style: hidden; border-width: 0; font-size: 11px; padding: 0;}
			.classTextError {color:#ff0000;font-weight: bold;}
		</style>
	</head>
	<body>
		<center>
			<br /><br />
			<table width="80%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="arriba_iz" >&nbsp;</td>
					<td class="arriba_ce" ><span class="letrablanca">::Re Afiliaci&oacute;n Empresa Expulsada::</span>
						<div style="text-align:right;float:right;">
							<?php echo 'Versi&oacute;n: ' . $fecver; ?>
						</div>
					</td>
					<td class="arriba_de" >&nbsp;</td>
				</tr>
				<tr>
					<td class="cuerpo_iz">&nbsp;</td>
					<td class="cuerpo_ce"><img src="../../imagenes/spacer.gif" alt="" width="2" height="1" /></td>
					<td class="cuerpo_de">&nbsp;</td>
				</tr>
				<tr>
					<td class="cuerpo_iz" >&nbsp;</td>
					<td class="cuerpo_ce" >
						<table border="0" class="tablero" cellspacing="0" width="90%" style="margin:0 auto;">
							<tr>
						    	<td width="20%" style="text-align:right;">
						    		Radicaci&oacute;n:
						    	</td>
						    	<td colspan="5" >
						    		<select name="cmbIdRadicacion" id="cmbIdRadicacion" class="element-required box1">
						    		</select>
						    		<span class="clsObligatorio">&nbsp;&nbsp;&nbsp;</span>
						    	</td>
						  	</tr>
						  	<tr>
						    	<td colspan="7" id="tdMensajeError" class="classTextError"></td>
						  	</tr>
							<tr>
								<th colspan="7">
									Datos Empresa
									<input type="hidden" name="hidIdEmpresa" id="hidIdEmpresa"/>
								</th>
							</tr>
							<tr>
								<td style="text-align:right;">Nit:</td>
								<td colspan="5">&nbsp; <label id="lblNit" >&nbsp;</label></td>
							</tr>
							<tr>
								<td style="text-align:right;">Raz&oacute;n Social:</td>
								<td colspan="5">&nbsp; <label id="lblRazonSocial" >&nbsp;</label></td>
							</tr>
							<tr>
								<td style="text-align:right;">Causal:</td>
								<td width="40%">&nbsp; <label id="lblCausal" >&nbsp;</label></td>
								<td style="text-align:right;">Fecha Estado:</td>
								<td width="16%">&nbsp; <label id="lblFechaEstado" >&nbsp;</label></td>
								<td style="text-align:right;">Estado:</td>
								<td width="6%">&nbsp; <label id="lblEstado" >&nbsp;</label></td>
							</tr>
							<tr>
								<th colspan="7">
									Desafiliaci&oacute;n
								</th>
							</tr>
							<tr>
								<td colspan="7">
									<table id="tblDesafiliacion" class="tablero" style="margin:0 auto;" width="100%">
										<thead>
											<tr>
												<th width="5%"></th>
												<th width="5%">Id</th>
												<th width="20%">Causal</th>
												<th width="5%">Estado</th>
												<th width="5%">Usuario</th>
												<th width="10%">Fecha Sistema</th>
												<th width="10%">Fecha Estado</th>
											</tr>
										</thead>
										<tbody>
										</tbody>
									</table>
								</td>
							</tr>
							<tr>
								<th colspan="7">
									Datos
								</th>
							</tr>
							<tr>
						    	<td style="text-align:right;">
						    		Fecha Afiliaci&oacute;n Empresa:
						    	</td>
						    	<td colspan="5" >
						    		<input type="text" name="txtFechaAfiliEmpre" id="txtFechaAfiliEmpre" class="box1 element-required"/>
						    		<span class="clsObligatorio">&nbsp;&nbsp;&nbsp;</span>
						    	</td>
						  	</tr>
						  	<tr>
						  		<td style="text-align:right;">Observaci&oacute;n:</td>
						  		<td colspan="5">
									<textarea name="txaObservacion" id="txaObservacion" cols="80" rows="3" maxlength="250" class="boxlargo element-required"></textarea>
									<span class="clsObligatorio">&nbsp;&nbsp;&nbsp;</span>
						    	</td>
						    </tr>
						    <tr>
								<td style="text-align:center;" colspan="6">
									<input type="button" name="btnGuardar" id="btnGuardar" value="Guardar" onclick="guardar();"/>
								</td>
							</tr>
					  	</table>
					</td>
					<td class="cuerpo_de" >&nbsp;</td>
				</tr>
				<tr>
					<td class="cuerpo_iz" >&nbsp;</td>
					<td class="cuerpo_ce" >&nbsp;</td>
					<td class="cuerpo_de" >&nbsp;</td>
				</tr>
				<tr>
					<td class="abajo_iz" >&nbsp;</td>
					<td class="abajo_ce" ></td>
					<td class="abajo_de" >&nbsp;</td>
				</tr>
			</table>
		</center>
	</body>
</html>
