<?php

/* autor:       Orlando Puentes
 * fecha:       Julio 23 de 2010
 * objetivo:    Consultar en la base de datos Aportes la información de las empresas afiliadas a Comfamiliar Huila.
 */
 
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'config.php';
$url=$_SERVER['PHP_SELF'];
include_once $raiz.DIRECTORY_SEPARATOR.'phpComunes'.DIRECTORY_SEPARATOR.'auditoria.php';
auditar($url);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<title>::CONSULTA EMPRESA::</title>
		<link type="text/css" href="../../css/formularios/base/ui.all.css" rel="stylesheet" /> 
		<link type="text/css" href="../../css/Estilos.css" rel="stylesheet"/>
		<link type="text/css" href="../../css/estilo_tablas.css" rel="stylesheet"/>
		<link href="../../css/marco.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="../../js/jquery-1.3.2.min.js"></script>
		<script type="text/javascript" src="../../js/formularios/ui/ui.core.js"></script>
		<script type="text/javascript" src="../../js/formularios/ui/ui.tabs.js"></script>
		<script type="text/javascript" src="../../js/formularios/ui/ui.draggable.js"></script>
		<script type="text/javascript" src="../../js/formularios/ui/ui.resizable.js"></script>
		<script type="text/javascript" src="../../js/formularios/ui/ui.dialog.js"></script>
		<script type="text/javascript" src="../../js/formularios/ui/jquery.ui.widget.js"></script>
		<script type="text/javascript" src="../../js/formularios/ui/ui.datepicker.js"></script>
		<script type="text/javascript" src="../../js/formularios/ui/ui.button.js"></script>
		<script type="text/javascript" src="../../js/formularios/ui/jquery.ui.accordion.js"></script>
		<script type="text/javascript" src="js/consultaEmpresa.js"></script>
		<script type="text/javascript" src="../../js/comunes.js"></script>
		<script type="text/javascript">
			shortcut.add("Shift+F",function() {
					var URL=src();
					var url=URL+"aportes/trabajadores/consultaTrabajador.php";
			    	window.open(url,"_blank");
		    	},{
					'propagate' : true,
					'target' : document 
			});        
		</script>
		<style type="text/css">
			#empresas p{ cursor:pointer; color:#333; width:600px;}
			#empresas p:hover{color:#000}
			#accordion h3,div{ padding:6px;}
			#accordion span.plus{ background:url(../../imagenes/plus.png) no-repeat right center; margin-left:95%;}
			#accordion span.minus{background:url(../../imagenes/minus.png) no-repeat right center;  margin-left:95%}
			div#wrapTable{padding:0px; margin-bottom:-2px}
			div#icon{width:50px; margin:auto;margin-first:5px;padding:10px; text-align:center;display:none; cursor:pointer;}
			div#icon span{background:url("../../imagenes/show.png") no-repeat; padding:12px;}
			div#icon span.toggleIcon{background:url("../../imagenes/hide.png") no-repeat; padding:12px;}
			#controls { margin:0 auto; height:20px;}
			#perpage {float:left; width:200px; margin-left:40px;}
			#perpage select {float:left; font-size:11px}
			#perpage label {float:left; margin:2px 0 0 5px; font-size:10px}
			#navigation {float:left;/* width:600px;*/ text-align:center}
			#navigation img {cursor:pointer}
			#text {float:left; width:200px; text-align:left; margin-first:2px; font-size:10px}
			#periodo p {
				text-align: center;
				font-style: italic;
				color: #CCC;
			}	
		</style>
	</head>
	<body>
		<!-- TABLA DE PARAMEROS BUSQUEDA -->
		<div id="wrapTable">
			<table width="97%" border="0" cellspacing="0" cellpadding="0" align="center">
       			<tr>
        			<td width="13" height="29" class="arriba_iz">&nbsp;</td>
        			<td class="arriba_ce"><span class="letrablanca">::&nbsp;Consulta De Empresas&nbsp;::</span></td>
        			<td width="13" class="arriba_de" align="right">&nbsp;</td>
      			</tr>
     			<tr>     
     				<td class="cuerpo_iz">&nbsp;</td>
					<td class="cuerpo_ce">
     					<img src="../../imagenes/spacer.gif" width="1" height="1"/>
					    <img src="../../imagenes/menu/informacion.png" width="16" height="16" style="border: none; cursor: pointer" title="Manual" onclick="mostrarAyuda();" /> 
					    <img src="../../imagenes/spacer.gif" width="3" height="1"/>
					    <img src="../../imagenes/menu/notas.png" width="16" height="16" style="cursor: pointer" title="Colaboracion en linea" onclick="notas();" /> 
     					<br/>
	 					<font size=1 face="arial">&nbsp;Info&nbsp;Ayuda</font>
     					<div id="error" style="color:#FF0000"></div>
       				</td>
       				<td background="../../imagenes/tabla/derecha.gif">&nbsp;</td>
    			</tr>
    			<tr>
     				<td background="../../imagenes/tabla/izquierda2.jpg">&nbsp;</td>
     				<!-- TABLAS DE FORMULARIO -->
     				<td align="center" background="../../imagenes/tabla/centro.gif">
     					<center>
	     					<input name="idEmpresaRadicacion" type="hidden" class="box1" id="idEmpresaRadicacion"  />
		    				<table width="90%" border="0" cellspacing="0" class="tablero">
	          					<tr>
	             					<td width="186">Buscar Por:</td>
	             					<td width="153">
	             						<select name="buscarPor" class="box1" id="buscarPor">
	               							<option selected="selected">Seleccione..</option>
	               							<option value="1" selected="selected">NIT o CC</option>
	               							<option value="2">RAZON SOCIAL</option>
	             						</select>
	             					</td>
	             					<td width="127"><input name="idEmpresa" type="text" class="box1" id="idEmpresa"  onkeypress="runSearch(event);"/></td>
	             					<td width="641">
	             						<input name="buscarEmpresa" type="button" class="ui-state-default" id="buscarEmpresa" value="Buscar" />
	              						<span class="Rojo"></span>
	             					</td>
	          					</tr>
	        				</table>
  						</center>
        				<div id="empresas" align="center"></div>
        			</td>
      				<td background="../../imagenes/tabla/derecha.gif"></td><!-- FONDO DERECHA -->
      			</tr>
  				<tr>
    				<td height="41" background="../../imagenes/tabla/abajo_izq2.gif">&nbsp;</td>
    				<td background="../../imagenes/tabla/abajo_central.gif" ></td>
    				<td background="../../imagenes/tabla/abajo_der.gif">&nbsp;</td>
  				</tr>
  				<tr>
			</table>
		</div>
		<div id="icon"><span></span></div>
		<!-- div para el acordeon -->
  		<div id="tabsE" style="display:none;">
	  		<ul>
				<li><a href='#tabs-1' alt='a'>Datos Empresa</a></li>
				<li><a href='#tabs-2' alt='b'>Aportes</a></li>
				<li><a href='#tabs-3' alt='c'>Trabajadores</a></li>
				<li><a href='#tabs-4' alt='d'>Documentos</a></li>
				<li><a href='#tabs-5' alt='e'>Trayectoria</a></li>
				<li><a href='#tabs-6' alt='f'>Planilla &Uacute;nica</a></li>
				<li><a href='#tabs-7' alt='g'>Cuota Monetaria</a></li>
				<li><a href='#tabs-8' alt='h'>Reclamos</a></li>
				<li><a href='#tabs-9' alt='i'>Radicaciones Pendientes</a></li>
	  		</ul>
	  		<div id="tabs-1"></div>
  	  		<div id="tabs-2"></div>
  	  		<div id="tabs-3"></div>
  	  		<div id="tabs-4"></div>
  	  		<div id="tabs-5"></div>
  	  		<div id="tabs-6"></div>
	  		<div id="tabs-7"></div>
  	  		<div id="tabs-8"></div>
  	  		<div id="tabs-9"></div>  
  		</div>
  		<!-- div para la imagen ampliada -->
  		<div id="imagen" title="DOCUMENTOS"></div>
		<!-- mostrar observacino aporte -->
		<div name="div-observaciones-aporte" id="div-observaciones-aporte" style="display:none" ></div>
		<!-- fin observacino aporte -->
		<!-- colaboracion en linea -->
		<!-- FORMULARIO OBSERVACIONES-->
		<div name="div-observaciones-tab" style="display:none" title="Observaciones de modificaci&oacute;n">
			<table class="tablero">
 				<tr>
   					<td>Usuario</td>
   					<td colspan="3" ><input name="usuarioObs" id="usuarioObs" class="box1" disabled="disabled" value="<?php echo $_SESSION['USUARIO']?>" /></td>
   				</tr>
 				<tr>
   					<td>Observaciones</td>
   					<td colspan="3" >
   						<textarea name="observacionGrupo" id="observacionGrupo" cols="45" rows="5" class="boxlargo"></textarea>
   					</td>
   				</tr>
			</table>
			<div class="ui-state-highlight ui-corner-all" style="margin: 9px auto; display:none;padding: 6px; text-align: center; border: 1px solid rgb(255, 255, 0); width:300px" id="rtaObservacion"><span class="ui-icon ui-icon-info" style="float: left; margin-right: 0.3em;"></span>Observaci&oacute;n guardada con &eacute;xito.</div>
		</div>
		<!-- fin colaboracion -->
		<!-- Periodo PLANILLA UNICA -->
		<div id="periodo" title="PLANILLA UNICA" style="display:none; padding-first:40px"  >
			<label>Digite el peri&oacute;do</label>
			<input name="txtPeriodo" class="box1"  id="txtPeriodo" maxlength="6"  />
			<button id="buscarPeriodo" class="ui-state-default">Buscar</button>
			<p>Ejemplo: 201001</p>
		</div>
		<!-- Fin perido -->
		<!-- ayuda en linea -->
		<div id="ayuda" title="Manual .:. Consulta Empresa" style="background-image:url('../../imagenes/FondoGeneral0.png')"></div>
		<!-- fin ayuda en linea -->
	</body>
</html>