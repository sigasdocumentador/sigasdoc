<?php
/* autor:       orlando puentes
 * fecha:       17/06/2010
 * objetivo:    
 */
 
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$idpersona= $_REQUEST['v0'];
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
include_once 'clases' . DIRECTORY_SEPARATOR . 'empresas.class.php';

$campo0 = empty($_REQUEST['v0'])? 'NULL' : "'".trim($_REQUEST['v0'])."'";					//idradicacion
$campo1 = empty($_REQUEST['v1'])? 'NULL' : "'".trim($_REQUEST['v1'])."'";					//fecha
$campo2 = empty($_REQUEST['v2'])? 'NULL' : "'".trim($_REQUEST['v2'])."'";					//sector
$campo3 = empty($_REQUEST['v3'])? 'NULL' : "'".trim($_REQUEST['v3'])."'";					//clase de sociedad
$campo4 = empty($_REQUEST['v4'])? 'NULL' : "'".trim($_REQUEST['v4'])."'";					//Nit
$campo5 = empty($_REQUEST['v5'])? 'NULL' : "'".trim($_REQUEST['v5'])."'";					//Digito
$campo6 = empty($_REQUEST['v6'])? 'NULL' : "'".trim($_REQUEST['v6'])."'";					//Tipo Documento
$campo7 = empty($_REQUEST['v7']) ? 'NULL' : "'".strtoupper(trim($_REQUEST['v7']))."'";		//Razon Social
$campo8 = empty($_REQUEST['v8']) ? 'NULL' : "'".strtoupper(trim($_REQUEST['v8']))."'";		//Sigla
$campo9 = empty($_REQUEST['v9'])? 'NULL' : "'".trim($_REQUEST['v9'])."'";					//departamento
$campo10 = empty($_REQUEST['v10'])? 'NULL' : "'".trim($_REQUEST['v10'])."'";				//ciudad
$campo11 = empty($_REQUEST['v11'])? 'NULL' : "'".trim($_REQUEST['v11'])."'";				//direccion
$campo12 = empty($_REQUEST['v12'])? 'NULL' : "'".trim($_REQUEST['v12'])."'";				//telefono
$campo13 = strtoupper(trim($_REQUEST['v13']));												//fax
$campo14 = empty($_REQUEST['v14'])? 'NULL' : "'".trim($_REQUEST['v14'])."'";				//URL
$campo15 = empty($_REQUEST['v15'])? 'NULL' : "'".trim($_REQUEST['v15'])."'";				//Email
$campo16 = empty($_REQUEST['v16'])? 'NULL' : "'".trim($_REQUEST['v16'])."'";				//ID REPRESENTANTE
$campo17 = empty($_REQUEST['v17']) ? 'NULL' : $_REQUEST['v17'];								//ID CONTACTI
$campo18 = empty($_REQUEST['v18']) ? 'NULL' : $_REQUEST['v18'];								//contratista
$campo19 = empty($_REQUEST['v19']) ? 'NULL' : $_REQUEST['v19'];								//Id codigo Actividad
$campo20 = empty($_REQUEST['v20']) ? 'NULL' : $_REQUEST['v20'];								//Indice
$campo21 = empty($_REQUEST['v21'])? 'NULL' : "'".trim($_REQUEST['v21'])."'";				//Asesor
$campo22 = empty($_REQUEST['v22']) ? 'NULL' : $_REQUEST['v22'];								//Agencia
$campo23 = empty($_REQUEST['v23']) ? 'NULL' : $_REQUEST['v23'];								//Estado
$campo24 = empty($_REQUEST['v24']) ? 'NULL' : $_REQUEST['v24'];								//Fecha Afiliacion
$campo25 = empty($_REQUEST['v25']) ? 'NULL' : $_REQUEST['v25']; 							// fecha de constitución
$campo27 = empty($_REQUEST['v27'])? 'NULL' : $_REQUEST['v27'];								//Actividad Economica
$campo28 = empty($_REQUEST['v28'])? 'NULL' : "'".trim($_REQUEST['v28'])."'";				//ruta documentos
$campo29 = empty($_REQUEST['v29']) ? 'NULL' : $_REQUEST['v29']; 							//fecha Inicio Aportes
$campo30 = empty($_REQUEST['v30']) ? 'NULL' : $_REQUEST['v30']; 							//Clase Aportante
$campo31 = empty($_REQUEST['v31']) ? 'NULL' : $_REQUEST['v31']; 							//Tipo Aportante
$campo54 = empty($_REQUEST['v54'])? 'NULL' : "'".trim($_REQUEST['v54'])."'";				//direccion correspondencia
$campo55 = empty($_REQUEST['v55'])? 'NULL' : "'".trim($_REQUEST['v55'])."'";				//departamento correspondencia
$campo56 = empty($_REQUEST['v56'])? 'NULL' : "'".trim($_REQUEST['v56'])."'";				//ciudad correspondencia
$campo57=isset($_REQUEST['v57']) ? $_REQUEST['v57'] : "";	// Barrio
$campo58=isset($_REQUEST['v58']) ? $_REQUEST['v58'] : "";	// Celular
$campo59=isset($_REQUEST['v59']) ? $_REQUEST['v59'] : "";	// Celular
$campo60 = empty($_REQUEST['v60'])? 'NULL' : "'".trim($_REQUEST['v60'])."'";


if(strlen($campo28)==0){
	$fec=$campo24;
	$a=substr($fec,6,4);
	$m=substr($fec,0,2);
	$d=substr($fec,3,2);	
	$campo28="digitalizacion\\empresa\\$a\\$m\\$d\\$campo4\\";	
}
$campo99 = $_REQUEST['v99'];

$sql="Select * from aportes048 where idempresa=$campo99";
$rs=$db->querySimple($sql);
$row=$rs->fetch();

$idempresa=$row['idempresa']; 
$idtipodocumento=$row['idtipodocumento']; 
$nit=$row['nit']; 
$digito=$row['digito']; 
$nitrsn=$row['nitrsn']; 
$codigosucursal=$row['codigosucursal']; 
$principal=$row['principal']; 
$razonsocial=$row['razonsocial']; 
$sigla=$row['sigla']; 
$direccion=$row['direccion']; 
$iddepartamento=$row['iddepartamento']; 
$idciudad=$row['idciudad']; 
$idzona=$row['idzona']; 
$telefono=$row['telefono']; 
$fax=$row['fax']; 
$url=$row['url']; 
$email=$row['email']; 
$idrepresentante=$row['idrepresentante']; 
$idjefepersonal=$row['idjefepersonal']; 
$contratista=$row['contratista']; 
$colegio=$row['colegio']; 
$exento=$row['exento']; 
$idcodigoactividad=$row['idcodigoactividad']; 
$actieconomicadane=$row['actieconomicadane']; 
$indicador=$row['indicador']; 
$idasesor=$row['idasesor']; 
$fechamatricula=($row['fechamatricula']); 
$idsector=$row['idsector']; 
$seccional=$row['seccional']; 
$tipopersona=$row['tipopersona']; 
$claseaportante=$row['claseaportante']; 
$tipoaportante=$row['tipoaportante']; 
$estado=$row['estado']; 
$codigoestado=$row['codigoestado']; 
$fechaestado=$row['fechaestado']; 
$fechaaportes=$row['fechaaportes']; 
$fechaafiliacion=$row['fechaafiliacion']; 
$trabajadores=$row['trabajadores']; 
$aportantes=$row['aportantes']; 
$conyuges=$row['conyuges']; 
$hijos=$row['hijos']; 
$hermanos=$row['hermanos']; 
$padres=$row['padres']; 
$tempo=$row['tempo']; 
$flag=$row['flag']; 
$usuario=$row['usuario']; 
$fechasistema=$row['fechasistema']; 
$idclasesociedad=$row['idclasesociedad']; 
$rutadocumentos=$row['rutadocumentos']; 
$legalizada=$row['legalizada'];
$direcorresp=$row['direcorresp'];
$iddepcorresp=$row['iddepcorresp'];
$idciucorresp=$row['idciucorresp'];
$barrio=$row['idbarrio'];
$celular=$row['celular'];
$id_tipo_tel=$row['id_tipo_tel'];

if($campo29==null) { $campo29=$row['fechaaportes']; }

$sql="insert into aportes049 (idempresa, idtipodocumento, nit, digito, nitrsn, codigosucursal, principal, razonsocial, sigla, direccion, iddepartamento, idciudad, idzona, telefono, fax, url, email, idrepresentante, idjefepersonal, contratista, colegio, exento, idcodigoactividad, actieconomicadane, indicador, idasesor, fechamatricula, idsector, seccional, tipopersona, claseaportante, tipoaportante, estado, codigoestado, fechaestado, fechaaportes, fechaafiliacion, trabajadores, aportantes, conyuges, hijos, hermanos, padres, tempo, flag, usuario, fechasistema, idclasesociedad, rutadocumentos, legalizada, renovacion, direcorresp, iddepcorresp, idciucorresp, idbarrio, celular,id_tipo_tel)
					   values('$idempresa', '$idtipodocumento', '$nit', '$digito', '$nitrsn', '$codigosucursal', '$principal', '$razonsocial',' $sigla', '$direccion', '$iddepartamento', '$idciudad', '$idzona', '$telefono', '$fax', '$url', '$email', '$idrepresentante', '$idjefepersonal', '$contratista', '$colegio', '$exento', '$idcodigoactividad', '$actieconomicadane', '$indicador', '$idasesor', '$fechamatricula', '$idsector', '$seccional', '$tipopersona', '$claseaportante', '$tipoaportante', '$estado', '$codigoestado', '$fechaestado', '$fechaaportes', '$fechaafiliacion', '$trabajadores', '$aportantes', '$conyuges', '$hijos', '$hermanos', '$padres', '$tempo', '$flag', '$usuario', '$fechasistema', '$idclasesociedad', '$rutadocumentos', '$legalizada','S', '$direcorresp', '$iddepcorresp', '$idciucorresp', '$barrio', '$celular','$id_tipo_tel')";
$rs=$db->queryActualiza($sql);
if($rs>0){
	$usuario=$_SESSION['USUARIO'];
	$sql="Update aportes048 set idtipodocumento=$campo6, nit=$campo4, digito=$campo5, nitrsn=$campo4, codigosucursal='000', principal='S', razonsocial=$campo7, sigla=$campo8, direccion=$campo11, iddepartamento=$campo9, idciudad=$campo10, idzona=NULL, telefono=$campo12, fax='$campo13', url=$campo14, email=$campo15, idrepresentante=$campo16, idjefepersonal=$campo17, contratista='$campo18', colegio='N', exento='N', idcodigoactividad=$campo19, actieconomicadane='$campo27', indicador=$campo20, idasesor=$campo21, idsector=$campo2, seccional='$campo22', tipopersona=$campo3, claseaportante=$campo30, tipoaportante=$campo31, estado='A', fechamatricula='$campo25', fechaaportes='$campo29', fechaafiliacion='$campo24', usuario='$usuario', idclasesociedad=$campo3, rutadocumentos=$campo28, legalizada='S', fechaestado=null, codigoestado=null, renovacion='S', direcorresp=$campo54, iddepcorresp=$campo55, idciucorresp=$campo56, idbarrio='$campo57', celular='$campo58', idbarriocorresp='$campo59',id_tipo_tel=$campo60 where idempresa='$campo99'";
	$rs=$db->queryActualiza($sql);
	print_r($rs);
}else{
	echo 0;
}
?>