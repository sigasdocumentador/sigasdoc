
$(document).ready(function(){
	
	//ASIGNAR EVENTO CLICK
	$( "#imgBuscarRadicacion" ).bind( "click", buscarRadicacion );
	$( "#btnGuardar" ).bind( "click", guardar ).button();
	
	//ASIGNAR EVENTO BLUR
	$( "#txtNumeroRadicacion" ).bind( "blur", buscarRadicacion );
	$( "#txtCiiu" ).bind( "blur", buscarActividadEco );
	
	//LLENAR COMBOS: DEPARTAMENTOS, MUNICIPIOS, ZONA
	$( "#cmbDepartamento" ).jCombo( URL+"pdo/departamentos.php?id=", { 
		parent: "#cmbPais", 
		selected_value: '41' 
	});	
	$( "#cmbCiudad" ).jCombo( URL+"pdo/municipios.php?id=", { 
		parent: "#cmbDepartamento"
	});
	
	$( "#cmbZona" ).jCombo( URL+"pdo/zonas.php?id=", { 
		parent: "#cmbCiudad"
	});	
	
	//CONTROL DE LOS CAMPOS DEL FORMULARIO
	$( "#txtNumeroRadicacion" ).focus();
	$( "input[type=text], select" ).attr( "disabled", "disabled" );
	$( "#txtNumeroRadicacion" ).removeAttr( "disabled", "disabled"  );
	$( "#imgMostrar, #imgOcultar" ).css("cursor","pointer");
	$( "#cmbTipoFormulario" ).val( 3316 );
	$( "#cmbTipoDocumRepre" ).val(1);
	$( "#cmbTipoDocumAdmin" ).val(1);
	
	//ASIGNAR DATEPICKER
	actDatepicker( $( "#txtFechaConstitucion") );
	actDatepicker( $( "#txtFechaMatricula" ) );
	actDatepicker( $( "#txtInicioApor" ) );
	
	var anoActual = new Date();
	var strYearRange = anoActual.getFullYear()-120 +":"+ anoActual.getFullYear();
	$("#txtFechaConstitucion").datepicker( "option", "yearRange", strYearRange );
	$("#txtFechaMatricula").datepicker( "option", "yearRange", strYearRange );
	$("#txtInicioApor").datepicker( "option", "yearRange", strYearRange );	
});

function nuevo(){
	$( "input:text, input:hidden" ).val( "" );
	$( "select" ).val( 0 );
	$( "#cmbTipoFormulario" ).val( 3316 );
	$( "input[type=text], select" ).attr( "disabled", "disabled" );
	$( "#txtNumeroRadicacion" ).removeAttr( "disabled", "disabled" );
	$( "table.tablero :input.ui-state-error,select.ui-state-error").removeClass("ui-state-error");
	$( "#txtNumeroRadicacion" ).focus();
	$( "#tdNombrRepre, #tdNombrAdmin" ).text("");
	$( "#cmbTipoDocumRepre" ).val(1);
	$( "#cmbTipoDocumAdmin" ).val(1);
	cargoAdmin = 0;
	telefonoAdmin = "";
	celularAdmin = "";
	idAfiliacion = "";
}

function buscarRadicacion() {
	
	$("table.tablero :input.ui-state-error,select.ui-state-error").removeClass("ui-state-error");
	
	var idRadicacion = parseInt( $( "#txtNumeroRadicacion" ).val().trim());
	if( isNaN( idRadicacion ) ){
		nuevo();
		return false;
	}
	
	var retornoEmpresa = new Array();
	var $retornoAjax = new Array();
	var $url = URL_PHP + "buscarRadicacion.php";
	var $data = {v0:idRadicacion, v1:30};
	var $async = false;
	
	$retornoAjax = metodoAjax( $url, $data, $async );
	if ( $retornoAjax.error == 1 ) {
		alert( "La radicacion no esta en nuestra base de datos" );
		nuevo();
	}else  if ( $retornoAjax.error == 2 ) {
		return false;
	}else {
		
		retornoEmpresa = buscarEmpresa( $retornoAjax.datos.nit );
		
		if( retornoEmpresa.error == 1 ) {
			alert( "La empresa con el nit " + $retornoAjax.datos.nit + ", no esta en nuestra base de datos" );
		}else if ( retornoEmpresa.error == 2) { 
			return false;
		}else {
		
			$( "input[type=text], select" ).removeAttr( "disabled", "disabled" );
			$( "#txtNumeroRadicacion, #cmbTipoFormulario" ).attr("disabled","disabled");
			$( "#comboTipoDocumento" ).val( retornoEmpresa.datos.idtipodocumento );
			$( "#txtNit" ).val( retornoEmpresa.datos.nit );
			$( "#txtRazonSocial" ).val( retornoEmpresa.datos.razonsocial );
			$( "#txtIdEmpresa" ).val( retornoEmpresa.datos.idempresa );
			$("#txtFechaAfiliacion").val( retornoEmpresa.datos.fechasistema );
		}
	}
}

function guardar(){	
	
	var idRadicacion   = $( "#txtNumeroRadicacion" ).val().trim(); 
	if( idRadicacion == "" ) return false;
	if( validarCampor() > 0 ) return false;
	
	var campos = {};
	
	campos.idtipoafiliacion = $( "#cmbTipoFormulario" ).val().trim();        // Tipo Formulario | TIPO AFILIACION
	
	//INFORMACION GENERAL
	campos.idtipodocumento = $( "#comboTipoDocumento" ).val().trim();        // Tipo Documento
	campos.nit             = $( "#txtNit" ).val().trim();                    // Nit
	//campos.digito          = $( "#txtDigito" ).val().trim();               // D�gito
	campos.razonsocial     = $( "#txtRazonSocial" ).val().trim();            // Raz�n Social
	campos.idempresa         = $( "#txtIdEmpresa" ).val().trim();            // Id Empresa
	campos.idclasesociedad   = $( "#comboClaseSociedad" ).val();             // Clase de Sociedad
	campos.idsector          = $( "#comboSector" ).val();                    // Sector
	campos.fechamatricula    = $( "#txtFechaConstitucion" ).val().trim();    // Fecha Constituci�n
	campos.idcodigoactividad = $( "#txtCiiu" ).val().trim();                 // CIIU
	campos.seccional         = $( "#comboSeccional" ).val().trim();          // Seccional
	
	//INFORMACION ESPECIFICA
	campos.direccion       = $( "#txtDireccion" ).val().trim();              // Direccion
	//campos. idPais         = $( "#cmbPais" ).val();                        // Pais
	campos.iddepartamento  = $( "#cmbDepartamento" ).val();                  // Departamento
	campos.idciudad        = $( "#cmbCiudad" ).val();                        // Ciudad
	campos.idzona          = $( "#cmbZona" ).val();                          // Zona
	campos.telefono        = $( "#txtTelFijo" ).val().trim();                // Telefono
	//campos. celular        = $( "#txtTelCel" ).val().trim();               // Celular
	campos.email           = $( "#txtEmail" ).val().trim();                  // Correo
	campos.fax             = $( "#txtFax" ).val().trim();                    // Fax
	campos.idrepresentante = $( "#hidIdRepre" ).val();                       // Id Representante
	campos.cedrep          = $( "#txtIdentRepre" ).val();                    // identificacion representante
	campos.representante   = $( "#tdNombrRepre" ).text();
	campos.idjefepersonal  = $( "#hidIdAdmin" ).val();                       // Id administrador
	var idCargoAdmin   = $( "#cmbCargo" ).val().trim();                      // Cargo Administrador
	var telefonoAdminA  = $( "#txtTelFijoCon" ).val().trim();                // Telefono Administrador
	var celularAdminA  = $( "#txtTelCelCon" ).val().trim();                  // Celular Administrador
	campos.contratista     = $( "#cmbContratista" ).val();                   // Contratista
	
	//OTROS DATOS 
	campos.indicador      = $( "#cmbIndApor" ).val();                        // Indice del Aporte
	campos.fechaaportes   = $( "#txtInicioApor" ).val();                     // Fecha Inicio Aporte
	campos.trabajadores   = $( "#txtNumTrab" ).val().trim();                 // Numero Trabajadores
	campos.estado         = $( "#cmbEstado" ).val().trim();                  // Estado
	campos.claseaportante = $( "#cmbClaseApor" ).val().trim();               // Clase Aportante
	campos.tipoaportante  = $( "#cmbTipoApor" ).val().trim();                // Tipo Aportante
	campos.porplanilla    = "N";                                             // Por planilla
	
	var bandera = "";
	
	var $retornoAjax = new Array();
	var $url = URL_PHP + "guardarEmpresa.php";
	var $data = {v0:JSON.stringify( campos )};
	var $async = false;
	
	$retornoAjax = metodoAjax( $url, $data, $async );
	if ( $retornoAjax.error == 1 ) {
		alert( "Error al guardar los datos de la empresa, informe a T.I." );
	}else if ( $retornoAjax.error == 2 ) {
		return false;
	}else {
		alert( "La datos de la empresa se guardaron correctamente" );
		if( idCargoAdmin != cargoAdmin && campos.idjefepersonal != "" ){
			if( actualizaCargoAdmin( idAfiliacion, idCargoAdmin ) )
				alert( "El cargo del Contacto Administrativo fue actualizado" );
			else
				alert( "No fue posible actualizar el cargo del Contacto Administrativo, informe a T.I." );
		}
		if( campos.idjefepersonal != "" && ( telefonoAdmin != telefonoAdminA || celularAdminA != celularAdmin ) ){
			if( actualizaDatosAdmin( campos.idjefepersonal, telefonoAdminA, celularAdminA ) )
				alert( "El tel\u00E9fono y celular del Contacto Administrativo se actualizaron" );
			else
				alert( "No fue posible actualizar el tel\u00E9fono y celular del Contacto Administrativo se actualizaron, informe a T.I." );
		}
		observacionesTab( campos.idempresa, 2 );
		nuevo();
		//cerrarRadicacion( idRadicacion );
	}
}

function cerrarRadicacion( idRadicacion ){
	
	var $retornoAjax = new Array();
	var $url = URL_PHP + "cerrarRadicacion.php";
	var $data = {v0:idRadicacion};
	var $async = false;
	
	$retornoAjax = metodoAjax( $url, $data, $async );
	if ( $retornoAjax.error == 1 ) {
		alert( "Error al cerrar la radicaci\u00F3n, informe a T.I." );
	}else if ( $retornoAjax.error == 2 ) {
		return false;
	}else {
		alert( "La radicaci\u00F3n se cerro correctamente" );
	}
}
