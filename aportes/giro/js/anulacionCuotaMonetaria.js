/** VARIABLES **/
URL=src();
nueva=0;
tiempoFuera=30000;

/** DOCUMENT READY **/
$(document).ready(function(){	
	nuevo();
   
	actDatepickerPeriodo($("#txtPeriodo"));
	shortcut.add("Shift+F",function() {		
		var url=URL+"aportes/trabajadores/consultaTrabajador.php";
	    window.open(url,"_blank");
	    },{
		'propagate' : true,
		'target' : document 
	});
});

/**
 * Funcion que permite iniciar
 */
function nuevo(){
	nueva=1;	
	limpiarCampos(0);		
	$("#tblCuotas").hide();
	$("#cmbIdTipoDocumento").val('1');
	$("#tbCuotasMonetarias").html('');
	$("table.tablero :input.ui-state-error,select.ui-state-error").removeClass("ui-state-error");
}

/**
 * Funcion que busca si la persona existe en nuestra base de datos
 * 
 * @param cmbtd 	Select que contiene el tipo de documento a buscar
 * @param txtd 		Text que contiene el numero de documento a buscar
 * @param tdnom 	Td donde es visible el nombre de la persona encontrada
 * @param nuevo 	Opcion que determina si se crea nueva persona si no existe 
 * @returns {persona}
 */
function buscarPersona(cmbtd, txtd, tdnom, nuevo, id){
	$("#tblCuotas").hide();
	$("#cmbIdCausal").val('0');
	$("#tbCuotasMonetarias").html('');
	
	var persona=null;	
	flag=7;
	tdnom.html('');
	id.val('');
	txtd.removeClass("ui-state-error");
	
	if(validarTexto(txtd,0)>0){return null;}
	if(validarSelect(cmbtd,0)>0){return null;}
	
	var tipodoc=cmbtd.val();
	var doc =txtd.val();
	
	$.ajax({
		url: 'buscarPersona.php',
		async: false,
		data: {v0:tipodoc,v1:doc,v2:flag},		
		beforeSend: function(objeto){
        	dialogLoading('show');
        },        
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }            
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("En buscarPersona Paso lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,		
		success: function(datoPersona){
			if(datoPersona==0){
				alert("Error en la consulta!");
				txtd.val('');
				txtd.addClass("ui-state-error");
			} else if (datoPersona==1){
				id.val('');
				alert("No existe en nuestra base!");
				if(nuevo){
					validarIdentificacionInsert(cmbtd,txtd);					
					if(validarTexto(txtd,0)>0){ alert("Documento no valido!"); return null;}
					newPersonaSimple(cmbtd,txtd);
				} else {
					txtd.val('');					
					txtd.addClass("ui-state-error");
					txtd.focus();
				}
			} else {
				$.each(datoPersona,function(i,fila){
					persona=fila;
					id.val(fila.idpersona);
					var nom = "&nbsp;&nbsp;&nbsp;&nbsp;" + $.trim(fila.pnombre) + " " + $.trim(fila.snombre) + " " + $.trim(fila.papellido) + " " + $.trim(fila.sapellido);					
					tdnom.html(nom);								
					return;
				});
			}
		},
		timeout: tiempoFuera,
        type: "GET"
	});	
	
	return persona;
}

/**
 * Funcion que permite buscar las cuotas monetarias
 */
function buscarCuotasMonetarias(){
	$("#tblCuotas").hide();
	$("#cmbIdCausal").val('0');
	$("#lblPeriodo").html("");
	$("#tbCuotasMonetarias").html('');
	$("#txtPeriodo").removeClass("ui-state-error");	
	var idPersona = $("#txtIdPersona").val();	
	var txtPeriodo = $("#txtPeriodo").val();	
	var error = 0;
	
	if(esNumeroRespuesta(idPersona)==false){
		error++;
		$("#txtIdPersona").val('');
		$("#txtIdentificacion").val('');		
	}
	
	error = validarTexto($("#txtIdentificacion"),error);
	error = validarTexto($("#txtPeriodo"),error);
	
	if(error>0){
		alert("Introduzca todos los datos para continuar con la consulta");
		return false;
	} else {
		$.ajax({
			url: 'buscarCuotasPeriodo.php',
			async: false,
			data: {v0:idPersona,v1:txtPeriodo},		
			beforeSend: function(objeto){
	        	dialogLoading('show');
	        },        
	        complete: function(objeto, exito){
	        	dialogLoading('close');
	            if(exito != "success"){
	                alert("No se completo el proceso!");
	            }            
	        },
	        contentType: "application/x-www-form-urlencoded",
	        dataType: "json",
	        error: function(objeto, quepaso, otroobj){
	            alert("En buscarCuotasMonetarias Paso lo siguiente: "+quepaso);
	        },
	        global: true,
	        ifModified: false,
	        processData:true,		
			success: function(datoCuotas){
				if(datoCuotas==0 || datoCuotas==""){
	    			alert("No hay cuotas monetarias que anular!");	    			
	    			return false;
	    		}	
				$("#tblCuotas").show();
				$("#lblPeriodo").html(txtPeriodo);
				actDatepickerPeriodo($("#txtPeriodo"));
	    		$.each(datoCuotas,function(i,f){
	    			nom = "&nbsp;&nbsp;&nbsp;&nbsp;" + f.pnombre + " " +f.snombre +" "+ f.papellido + " " + f.sapellido;
					$("#tbCuotasMonetarias").append("<tr>" +							
							"<td style='text-align: center' ><input id='chkIdCuota' type='checkbox' onclick='anular(" + f.idcuota +",$(this));' ></td><td style='text-align: center' >" + f.idcuota + "</td><td style='text-align: center' >" + f.idbeneficiario + "</td><td>" + nom + "</td><td style='text-align: center' >$ " + formatNumber(f.valor) + "</td></tr>")
	    			return;
	    		})
				
			},
			timeout: tiempoFuera,
	        type: "GET"
		});	
	}
	
}

/**
 * Funcion que permite anular la cuota monetaria
 */
function anular(idCuota,chk){	
	$("#cmbIdCausal").removeClass("ui-state-error");
	var error = validarSelect($("#cmbIdCausal"),0);	
	if(error>0){ alert("Seleccione un motivo de la anulacion"); chk.attr('checked', false); return false; }
	if(esNumeroRespuesta(idCuota)==false){ alert("Ocurrio un error, no se recibio la idCuota, no se puede continuar"); chk.attr('checked', false); return false; }
	
	if(confirm("Esta seguro de ANULAR la cuota monetaria?")==true){
		var idCausal=$("#cmbIdCausal").val();		
		$.ajax({
			url: 'procesoAnulacionCuotaMonetaria.php',
			async: false,
			data: {v0:idCuota,v1:idCausal},		
			beforeSend: function(objeto){
	        	dialogLoading('show');
	        },        
	        complete: function(objeto, exito){
	        	dialogLoading('close');
	            if(exito != "success"){
	                alert("No se completo el proceso!");
	            }            
	        },
	        contentType: "application/x-www-form-urlencoded",
	        dataType: "json",
	        error: function(objeto, quepaso, otroobj){
	            alert("En buscarCuotasMonetarias Paso lo siguiente: "+quepaso);
	        },
	        global: true,
	        ifModified: false,
	        processData:true,		
			success: function(datoCuotas){
				if(esNumeroRespuesta(datoCuotas)){
					alert("Cuota monetaria Anulada con exito");	
					buscarCuotasMonetarias();
					return;
				} else {
					alert("Ocurrio un error al anular la cuota monetaria");
					chk.attr('checked', false);	
					return;
				}
			},
			timeout: tiempoFuera,
	        type: "GET"
		});			
	} else {
		chk.attr('checked', false);		
	}	
}

/**
 * Activa datapicker del Objeto pasado para Periodo
 * 
 * @param objeto
 */
function actDatepickerPeriodo(objeto){
	var fecha = objeto.val() + '01';	
	if(fecha.length<6){
		var date = new Date();
		var month = date.getMonth()<9 ? '0'+ (date.getMonth()+1) : (date.getMonth()+1);		
		fecha = date.getFullYear() + '' + month + '01';
	}
	objeto.datepicker('destroy');
	objeto.datepicker({
		dateFormat: 'yymm',
		defaultDate: $.datepicker.parseDate("yymmdd", fecha),
		changeMonth: true,
		changeYear: true,
		showButtonPanel: true,
		onClose: function(dateText, inst) {
		var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
		var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
			$(this).datepicker('setDate', new Date(year, month, 1));
		} 
	});
	objeto.focus(function () {
		$(".ui-datepicker-calendar").hide();
		$("#ui-datepicker-div").position({
			my: "center top",
			at: "center bottom",
			of: $(this)
		});
	}); 
}