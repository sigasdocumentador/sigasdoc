/**
 * 
 */
$(function(){
	$('#tabs-admin-periodo').tabs();
	
	$('#adm-txtSmlv, #adm-txtValsub, #adm-txtValcam').bind('click', validar);

	$('#slt-periodos').bind('change', infoPeriodo);
	
	$('#a-peract').bind('click', function(){
		var peract = $(this).html();
		$('#slt-periodos').val(peract);
		$('#slt-periodos').trigger('change');
	}, false);
	
	$('#btn-abrir-giro').bind('click', abrirGiro);
	
	$('#btn-cerrar-giro').bind('click', cerrarGiro);
	
	$( "input:submit" ).button();
	$('#buttonset').buttonset();
	
	//$('#adm-txtPeriodo').bind('blur', vrfPeriodo);
	
	$.datepicker.setDefaults( $.datepicker.regional[ "es" ] );
	$('#adm-txtFecfin, #adm-txtFecini, #adm-txtFeclim').datepicker({
		dateFormat:"yy-mm-dd",
		changeMonth: true,
		changeYear: true,
		firstDay: 1,
		beforeShow: function(inp, ins){
			var dvDp = $(ins)[0].dpDiv;
			$(dvDp).css('z-index','9999');
		}
	});
	
	$('#adm-txtValsub').bind('blur',function(){
		var val = $(this).val()*1;
		$('#adm-txtValcam').val(Math.round(val*1.15));
	});
	
	$('#div-adm-periodo').dialog({ 
		autoOpen: false,
		resizable: false,
		modal: true,
		stack: false,
		width: 500,
		close:function(){
			$('#div-adm-periodo :text').val('');
			$('.ui-state-error').removeClass('ui-state-error');
		},
		buttons:{
			'Guardar':guardarPeriodo,
			'Cancelar': function(){
				$(this).dialog("close");
			}
		}
	});
	
	
	
	$('#btn-adm-periodo-nuevo').bind('click', function(){
		$('#div-adm-periodo').dialog("open");
		$('#adm-txtPeriodo').bind('blur', vrfPeriodo);
		//$('#adm-txtPeriodo').attr('readonly', '');
		$('#adm-txtPeriodo').removeClass('ui-state-error');
		$('#hdn-adm-accion').val('6');
	});
	
	$('#btn-adm-periodo-buscar').bind('click', buscar);
	
	$('#btn-adm-periodo-eliminar').bind('click', eliminar);
	
});