var errorper = 0, error = 0;

var infoPeriodo = function(){
	
	var per = $(this).val();
	var peract = $('#a-peract').html();
	var act = (per==peract)?'S':'N';
	$('#btn-cerrar-giro, #btn-abrir-giro').attr('disabled','disabled');
	
	$('.inf').html('');
	$('.tr-inf').remove();
	$.getJSON('giro-param-acc.php', {periodo: per, actual:act, acc:1}, function(data){
		$('#inf-periodo').html(data[0].periodo);
		$('#inf-fecini').html(data[0].fechainicio);
		$('#inf-fecfin').html(data[0].fechafin);
		$('#inf-feclim').html(data[0].fechalimite);
		$('#inf-valcuo').html(data[0].cuotamonetaria);
		$('#inf-valcam').html(data[0].cuotacampo);
		$('#inf-smvl').html(data[0].smlv);
		$('#inf-firstcua').html(data[0].topesalarial);
		$('#inf-procesa').html(data[0].procesado);
		$('#inf-estgir').html(data[0].estadogiro);
		
		var tb = $('#inf-giro-per');
		var c = 0;
		var g = 0;
		var d = 0;
		$.each(data, function(i, dat){
			if(i > 0){
				$(tb).append("<tr class=\"tr-inf\"><td>"+dat.tipogiro+"</td><td>"+dat.cant+"</td><td>"+dat.valor+"</td><td>"+dat.descu+"</td></tr>");
				c+=(dat.cant*1);
				g+=(dat.valor*1);
				d+=(dat.descu*1);
			}
		});
		$(tb).append("<tr class=\"tr-inf\"><td>Total</td><td>"+c+"</td><td>"+g+"</td><td>"+d+"</td></tr>");
		
		if(data[0].procesado == 'S' || per > peract){
			$('#btn-abrir-giro, #btn-cerrar-giro').attr('disabled','disabled');
		}else{
			if(data[0].estadogiro == 'C'){
				$('#btn-cerrar-giro').attr('disabled','disabled');
				$('#btn-abrir-giro').attr('disabled','');
			}else if(data[0].estadogiro == 'A'){
				$('#btn-abrir-giro').attr('disabled','disabled');
				$('#btn-cerrar-giro').attr('disabled','');
			}
		}
	});
};

var abrirGiro = function(){
	var per = $("#a-peract").text().trim();
	$.get('giro-param-acc.php', {periodo: per, acc:2}, function(data){
		if(data == 1){
			alert('Se abrio el periodo '+per);
			$("#lblEstadoPeriodo").html("Cerrar");
			//$('#btn-abrir-giro').attr('disabled','disabled');
			//$('#btn-cerrar-giro').attr('disabled','');
			//$('#inf-estgir').html('A');
			$('#slt-periodos').val(per).trigger("change");
			$('#slt-periodos :selected').html(per+' - A');
			$('#btnGiroNormal, #btnDeshGiro, #btnHistGiro, #btnGiroReclamosTrabajador, #btnDeshGiroAdicIndividual, #btnGiroAdicIndividual, #btnDeshGiroAdicIndividual, #btnGiroAdicEmpresa, #btnDeshGiroAdicEmpresa').attr('disabled','');
		}
	});
}

var cerrarGiro = function(){
	var per = $("#a-peract").text().trim();
	$.get('giro-param-acc.php', {periodo: per, acc:3}, function(data){
		if(data == 1){
			alert('Se cerro el periodo '+per);
			$("#lblEstadoPeriodo").html("Abrir");
			//$('#btn-cerrar-giro').attr('disabled','disabled');
			//$('#btn-abrir-giro').attr('disabled','');
			//$('#inf-estgir').html('C');
			$('#slt-periodos').val(per).trigger("change");
			$('#slt-periodos :selected').html(per+' - C');
			$('#btnGiroNormal, #btnDeshGiro, #btnHistGiro, #btnGiroReclamosTrabajador, #btnDeshGiroAdicIndividual, #btnGiroAdicIndividual, #btnDeshGiroAdicIndividual, #btnGiroAdicEmpresa, #btnDeshGiroAdicEmpresa').attr('disabled','disabled');
		}
	});
};

var vrfPeriodo = function(){
	var txt = $(this);
	var per = $(this).val();
	
	if(per.length > 0){
		error = 0;
		$(this).removeClass('ui-state-error');

		var ano = per.substring(0,4);
		var mes = per.substring(4,6);
		
		$('#adm-txtFecini').val(ano+'-'+mes+'-01');
		$('#adm-txtFecfin').val(ano+'-'+mes+'-01');
		mes = (mes < 12)?(mes*1)+1:1;
		$('#adm-txtFeclim').val(ano+'-'+mes+'-01');
		$('#adm-txtControl').val('');
		
		$.getJSON('giro-param-acc.php', {periodo: per, acc:4}, function(data){
			if(data[0].cant == 1){
				$(txt).addClass('ui-state-error');
				alert('El periodo ya se encuentra creado');
				errorper = 1;
			}else{
				$(txt).removeClass('ui-state-error');
				$('#adm-txtControl').val(data[0].control);
				errorper = 0;
			}
		});
	}else{
		error = 1;
		$(this).addClass('ui-state-error');
	}
};

var validar = function(){
	var txt = $(this).val();
	
	if(txt.length == 0){
		error = 1;
		$(this).addClass('ui-state-error');
	}else{
		error = 0;
		$(this).removeClass('ui-state-error');
	}
};

var guardarPeriodo = function(){
	var acc = $('#hdn-adm-accion').val();
	var par = $('#tbl-adm-periodo input').serialize();
	
	var cuota = $('#adm-txtValsub').val();
	var cuotacampo = $('#adm-txtValcam').val();
	var smlv = $('#adm-txtSmlv').val();
	var periodo = $('#adm-txtPeriodo').val();
	
	
	if(cuota.length == 0){
		$('#adm-txtValsub').addClass('ui-state-error');
		error = 1;
	}
	if(cuotacampo.length == 0){
		$('#adm-txtValcam').addClass('ui-state-error');
		error = 1;
	}
	if(smlv.length == 0){
		$('#adm-txtSmlv').addClass('ui-state-error');
		error = 1;
	}
	if(periodo.length == 0){
		$('#adm-txtPeriodo').addClass('ui-state-error');
		error = 1;
	}
	
	if(error == 0 && errorper == 0){
		par = par+'&acc='+acc;
		$.post('giro-param-acc.php', par, function(data){
			if(acc == 7){
				if(data == 1){
					alert('Se modifico de manera exitosa el perido');
				}else{
					alert('No se modifico el periodo');
				}
			}else if(acc == 6){
				if(isNaN(data)){
					alert('Se presento un error al crear el periodo');
				}else{
					alert('Se creo el periodo de manera exitosa');
					
					var fecini = $('#adm-txtFecini').val();
					var fecfin = $('#adm-txtFecfin').val();
					var feclim = $('#adm-txtFeclim').val();
					
					var tope = smlv*4;
					var control = $('#adm-txtControl').val();
					
					html = "<tr class=\"rmv\"><td><input type=\"checkbox\" name=\"chk"+data+"\" id=\"chk"+data+"\" value=\"id"+data+"\" /></td><td><a href=\"#\" class=\"modf\">"+periodo+"</a></td><td>"+fecini+"</td><td>"+fecfin+"</td><td>"+feclim+"</td><td>"+cuota+"</td><td>"+cuotacampo+"</td><td>"+smlv+"</td><td>"+tope+"</td><td>"+control+"</td><td>N</td><td>C</td></tr>";
					if($('#tbl-adm-peri tbody tr:eq(0)').length == 0){
						$('#tbl-adm-peri tbody').append(html).fadeIn(1000);
					}else{
						$('#tbl-adm-peri tbody tr:eq(0)').before(html).fadeIn(1000);
					}
					
					$('.modf').bind('click', modificar, false);
				}
			}
			$('#div-adm-periodo').dialog("close");
		});
	}else {
		if(errorper == 1){
			alert('El periodo que esta intentando crear ya existe');
		}else{
			alert('Corrija los errores marcados');
		}
	}
};

var buscar = function(){
	$('.rmv').remove();
	var tip = $('input[name=chk-adm-tipBusca]:checked').val();
	var perini = '', perfin = '';
	if(tip == 'periodo'){
		perini = $('#txt-adm-periodo').val();
		perfin = $('#txt-adm-periodo').val();
	}else if(tip == 'ano'){
		var ano = $('#txt-adm-ano').val();
		perini = ano+'01';
		perfin = ano+'12';
	}
	
	$.getJSON('giro-param-acc.php', {acc:5, perini: perini, perfin: perfin}, function(data){
		var html = "";
		$.each(data, function(i, dat){
			var di = (dat.procesado == 'S')?'disabled="disabled"':'';
			var cl = "modf";	
			html = "<tr class=\"rmv\"><td><input type=\"checkbox\" name=\"chk"+dat.idperiodo+"\" id=\"chk"+dat.idperiodo+"\" value=\"id"+dat.idperiodo+"\" "+di+" /></td><td><a href=\"#\" class=\""+cl+"\">"+dat.periodo+"</a></td><td>"+dat.fechainicio+"</td><td>"+dat.fechafin+"</td><td>"+dat.fechalimite+"</td><td>"+dat.cuotamonetaria+"</td><td>"+dat.cuotacampo+"</td><td>"+dat.smlv+"</td><td>"+dat.topesalarial+"</td><td>"+dat.controlproceso+"</td><td>"+dat.procesado+"</td><td>"+dat.estadogiro+"</td></tr>";
			$('#tbl-adm-peri tbody').append(html);
		});
		$('.modf').bind('click', modificar);
	});
};

var modificar = function(){
	var per = $(this).html();
	var td = $(this).parent().nextAll();
	var procesado = $(td).eq(8).html().trim();
	if(procesado.toUpperCase()=="S"){
		alert("El periodo "+per+" ya se encuentra procesado ");
		return;
	}
	$('#hdn-adm-accion').val('7');
	$('#adm-txtPeriodo').removeClass('ui-state-error');
	$('#adm-txtPeriodo').attr('readonly', 'readonly');
	$('#adm-txtPeriodo').unbind('blur');
	$('#adm-txtPeriodo').val(per);
	$('#adm-txtFecini').val($(td).eq(0).html()).parent().hide();
	$('#adm-txtFecfin').val($(td).eq(1).html()).parent().hide();
	$('#adm-txtFeclim').val($(td).eq(2).html());
	$('#adm-txtSmlv').val($(td).eq(5).html());
	$('#adm-txtValsub').val($(td).eq(3).html());
	$('#adm-txtValcam').val($(td).eq(4).html());
	$('#adm-txtControl').val($(td).eq(7).html());
	$('#div-adm-periodo').dialog("open");
};

var eliminar = function(){
	if($('#tbl-adm-peri :checkbox:checked').length > 0){
		var c = confirm('Desea borrar');
		if(c == true){
		var par = $('#tbl-adm-peri :checkbox:checked').serialize();
		par += '&acc=8';
			$.post('giro-param-acc.php', par, function(data){
				if(data == 1){
					alert('Se elimino el periodo correctamente');
					$('#tbl-adm-peri :checkbox:checked').parents('tr').remove();
				}else{
					alert('No se elimino el periodo');
				}
			});
		}
	}else{
		alert('Debe seleccionar el periodo a eliminar');
	}
};

function estadoGiro(){
	var estado = $("#lblEstadoPeriodo").text().trim();
	if(estado=="Abrir"){
		abrirGiro();
	}else{
		cerrarGiro();
	}
	/*
	return false;
	var acc = (estado=="Abrir")?2:3;
	var periodo = $("#a-peract").text().trim();
	$.ajax({
		url:"giro-param-acc.php",
		type:"POST",
		data:{periodo:periodo,acc:acc},
		dataType:"json",
		async:true,
		success:function(datos){
			if(datos==1){
				var mensaje = (estado=="Abrir")?" Se pueden realizar reclamos ":"No se pueden realizar reclamos";
				alert(mensaje);
				estado = (estado=="Abrir")?"Cerrar":"Abrir"; 
				$("#lblEstadoPeriodo").html(estado);
			}
		}
	});*/
}
