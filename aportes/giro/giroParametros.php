<?php
$raiz = "";
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$url=$_SERVER['PHP_SELF'];
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'auditoria.php';
auditar($url);
include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'Consultor.php';

$con = new Consultor();
$sql = "SELECT top 24 periodo, estadogiro FROM aportes012 ORDER BY periodo DESC";
$dper1 = $con->enArray($sql);
$sql = "SELECT MIN(periodo) AS peractual,MIN(estadogiro) AS estado FROM aportes012 WHERE procesado='N'";
$dperact = $con->enArray($sql);
$sql = "SELECT MAX(periodo) AS peranterior FROM aportes012 WHERE procesado='S'";
$dperant = $con->enArray($sql);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Documento sin título</title>
		<link href="../../css/css.1.8/jquery-ui-1.8.17.custom.css" rel="stylesheet" type="text/css" />
		<link type="text/css" href="../../css/Estilos.css" rel="stylesheet"/>
		<link type="text/css" href="../../css/estilo_tablas.css" rel="stylesheet"/>
		<script language="javascript" src="../../js/1.6/jquery-1.6.2.min.js"></script>
		<script language="javascript" src="../../js/1.6/jquery-ui-1.8.16.custom.min.js"></script>
		<script language="javascript" src="js/ui-acc-adm-giro.js"></script>
		<script language="javascript" src="js/ui-adm-giro.js"></script>
		<script>
			$(function() {
				$( "#tabs" ).tabs();
			});
		</script>
	</head>
	<body>
		<div class="demo">
			<div id="tabs">
				<div id="tabs-admin-periodo">
					<ul>
						<li><a href="#div-adm-giro">Administraci&oacute;n giro</a></li>
						<li><a href="#div-adm-peri">Administraci&oacute;n periodos</a></li>
					</ul>
					<div id="div-adm-giro">
					<table id="tbl-adm-giro" border="0" cellspacing="0"
						class="box-table-a" style="margin: 1em auto;">
						<tbody>
							<tr></tr>
						</tbody>
						<thead>
							<tr>
								<th><strong>Per&iacute;odos</strong></th>
								<th><strong>Informaci&oacute;n per&iacute;odo</strong></th>
								<th><strong>Informaci&oacute;n giros</strong></th>
							</tr>
						</thead>
						<tbody>
							<tr>
							<td>
							<label for="slt-periodos">Periodos</label>
							<select name="slt-periodos" id="slt-periodos">
								<option value="0">Seleccione</option>
								<?php
									foreach ($dper1 as $per) {
										echo "<option value=\"$per[periodo]\">$per[periodo] - $per[estadogiro]</option>";
									}
								?>
							</select>
							<br /><br />
							<label for="">Per&iacute;odo actual: <strong><a href="#" id="a-peract"><?php echo $dperact[0]['peractual'] ?></a></strong></label>
							<br /><br />
							
							<label for="">&Uacute;ltimo per&iacute;odo procesado: <strong><?php echo $dperant[0]['peranterior'] ?></strong></label>
							<br /><br />
							<label onclick="estadoGiro();" id="lblEstadoPeriodo" style="cursor: pointer; font-weight: bold;"><?php echo ($dperact[0]['estado']=="A")?"Cerrar":"Abrir"; ?></label> Cuota Monetaria Adicional
							<br /><br /><br /><br /></td>
							<td>
								<div id="div-info-periodo">
									<label>Per&iacute;odo: <strong><span id="inf-periodo" class="inf"></span></strong></label>
									<br /><br />
									<label>Fecha Inicio: <strong><span id="inf-fecini" class="inf"></span></strong></label>
									<br /><br />
									<label>Fecha Final: <strong><span id="inf-fecfin" class="inf"></span></strong></label>
									<br /><br />
									<label>Fecha Limite Pago: <strong><span id="inf-feclim" class="inf"></span></strong></label>
									<br /><br />
									<label>Valor Cuota: <strong><span id="inf-valcuo" class="inf"></span></strong></label>
									<br /><br />
									<label>Valor Cuota Campo: <strong><span id="inf-valcam" class="inf"></span></strong></label>
									<br /><br />
									<label>SMLV: <strong><span id="inf-smvl" class="inf"></span></strong></label>
									<br /><br />
									<label>tope 4 SMLV: <strong><span id="inf-firstcua" class="inf"></span></strong></label>
									<br /><br />
									<label>Procesado: <strong><span id="inf-procesa" class="inf"></span></strong></label>
									<br /><br />
									<label>Estado Giro: <strong><span id="inf-estgir" class="inf"></span></strong></label>
									<br /><br />
								</div>
							</td>
							<td>
								<table id="inf-giro-per" width="100%">
									<tr>
										<td>Tipo Giro</td>
										<td>Cantidad</td>
										<td>Valor girado</td>
										<td>Valor descontado</td>
									</tr>
								</table>
							</td>
							</tr>
						</tbody>
					</table>
					</div>
					<div id="div-adm-peri">
					<div id="buttonset" class="ui-dialog-buttonpane">
						<input type="submit" value="Buscar" id="btn-adm-periodo-buscar"/>
						<input type="submit" value="Nuevo" id="btn-adm-periodo-nuevo"/>
						<input type="submit" value="Eliminar" id="btn-adm-periodo-eliminar"/>
					</div>
					<br />
					<input type="radio" name="chk-adm-tipBusca" id="chk-adm-tipPeriodo" value="periodo" checked="checked"/>
					<label for="txt-adm-periodo">Periodo</label>
					<input type="text" name="txt-adm-periodo" id="txt-adm-periodo" />
					<input type="radio" name="chk-adm-tipBusca" id="chk-adm-tipAno" value="ano" />
					<label for="txt-adm-periodo">A&ntilde;o</label>
					<input type="text" name="txt-adm-ano" id="txt-adm-ano" />
					<table id="tbl-adm-peri" border="0" cellspacing="0" class="box-table-a" style="margin: 1em auto;">
						<tbody>
						</tbody>
						<thead>
							<tr>
								<!-- <th><input type="checkbox" name="slt-adm-periodos" id="slt-adm-periodos" /></th>  -->
								<th>Elim.</th>
								<th>Periodo</th>
								<th>Fec.Inicio</th>
								<th>Fec.Final</th>
								<th>Fec.Limite</th>
								<th>Cuota</th>
								<th>Cuo.Campo</th>
								<th>SMLV</th>
								<th>topex4</th>
								<th>Cod.Control</th>
								<th>Procesado</th>
								<th>Estado</th>
							</tr>
						</thead>
					</table>
					</div>
					<div id="div-adm-periodo">
						<table id="tbl-adm-periodo">
							<tr>
								<td colspan="3">
									<input type="hidden" name="hdn-adm-accion" id="hdn-adm-accion" value="0"/>
									<label for="adm-txtPeriodo">Periodo</label><br />
									<input type="text" name="adm-txtPeriodo" id="adm-txtPeriodo" />
								</td>
							</tr>
							<tr>
								<td>
									<label for="adm-txtFecini">Fecha Inicial</label>
									<input type="text" name="adm-txtFecini" id="adm-txtFecini" readonly="readonly"  />
								</td>
								<td>
									<label for="adm-txtFecfin">Fecha Final</label>
									<input type="text" name="adm-txtFecfin" id="adm-txtFecfin" readonly="readonly" />
								</td>
								<td>
									<label for="adm-txtFeclim">Fecha Limite</label>
									<input type="text" name="adm-txtFeclim" id="adm-txtFeclim" readonly="readonly" />
								</td>
							</tr>
							<tr>
								<td>
									<label for="adm-txtSmlv">SMLV</label>
									<input type="text" name="adm-txtSmlv" id="adm-txtSmlv" />
								</td>
								<td>
									<label for="adm-txtValsub">Valor Subsidio</label>
									<input type="text" name="adm-txtValsub" id="adm-txtValsub" />
								</td>
								<td>
									<label for="adm-txtValcam">Valor Subsidio Campo</label>
									<input type="text" name="adm-txtValcam" id="adm-txtValcam" />
								</td>
							</tr>
							<tr>
								<td colspan="3">
								<label for="adm-txtControl">C&oacute;digo Control</label><br />
									<input type="text" name="adm-txtControl" id="adm-txtControl" readonly="readonly"/>
								</td>
							</tr>
						</table>
					</div>	
				</div>
			</div>
		</div><!-- End demo-description -->
	</body>
</html>











