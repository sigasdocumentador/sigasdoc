<?php 
	set_time_limit(0);
	ini_set("display_errors",'1');
	date_default_timezone_set("America/Bogota");
	$raiz = "";
	$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
	include_once  $root;
	include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';
	/*include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
	$db = IFXDbManejador::conectarDB();
	if($db->conexionID==null){
		$cadena = $db->error;
		echo msg_error($cadena);
		exit();
	}*/
	//$fecver = date('Ymd h:i:s A',filectime('giroGeneral.php'));
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"> 
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Generar Estadistica Poblacional</title>
		<link type="text/css" href="<?php echo URL_PORTAL; ?>newcss/Estilos.css" rel="stylesheet" />
		<link type="text/css" href="<?php echo URL_PORTAL; ?>newcss/css.1.8/jquery-ui-1.8.17.custom.css" rel="stylesheet"/>
		<link type="text/css" href="<?php echo URL_PORTAL; ?>newcss/marco.css" rel="stylesheet"/>
		<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/1.6/jquery-1.6.2.min.js"></script>
		<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/1.6/jquery-ui-1.8.16.custom.min.js"></script>
		<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/comunes.js"></script>
		<script type="text/javascript" src="<?php echo URL_PORTAL; ?>aportes/giro/js/procesoGiro.js"></script>	
	</head>
	<body>
		<form name="forma">
		<center>
			<img src="<?php echo URL_PORTAL; ?>imagenes/logo_reporte.png" width="362" height="70" />
			<br />
			<table width="50%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="13" height="29" class="arriba_iz">&nbsp;</td>
					<td class="arriba_ce">&nbsp;</td>
					<td width="13" class="arriba_de" align="right">&nbsp;</td>
				</tr>
				<tr>
					<td class="cuerpo_iz">&nbsp;</td>
					<td class="abajo_ce">&nbsp;</td>
					<td class="cuerpo_de">&nbsp;</td>
				</tr>
				<tr>
					<td class="cuerpo_iz">&nbsp;</td>
					<td class="abajo_ce" align="center" > 
						<table width="100%" border="0" id="tblInforPeriodo" cellspacing="0" class="tablero" align="center">
							<thead>
								<tr>
									<td colspan="4" style="text-align: center">
										<h3>Informacion del periodo actual</h3>
									</td>
								</tr>
								<tr>
									<th>Periodo</th>
									<th>Valor Cuota</th>
									<th>Procesado</th>
									<th>Estado</th>
								</tr>
							</thead>
							<tbody align="center">
							</tbody>
							<tfoot align="center">
							</tfoot>
						</table>
						<br/>
						<table width="100%" border="0" cellspacing="0" class="tablero" id="tblResultado">
							<thead>
								<tr>
									<td colspan="4" style="text-align: center">
										<h3>Procedimientos de la cuota Monetaria</h3>
									</td>
								</tr>
								<tr>
									<th width="60%">Procedimiento</th>
									<th width="10%">Procesados</th>
									<th width="20%">No procesados</th>
									<th width="10%">Revisado</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
							<tfoot>
								<tr>
									<td colspan="4" style="text-align: center;">&nbsp;</td>
								</tr>
							</tfoot>
						</table>
					</td>
					<td class="cuerpo_de">&nbsp;</td>
				</tr>
				<tr>
					<td class="abajo_iz" >&nbsp;</td>
					<td class="abajo_ce" >&nbsp;</td>
					<td class="abajo_de" >&nbsp;</td>
				</tr>
			</table>
		</center>
		<input type="hidden" name="hidBandera" id="hidBandera" />
		</form>
		
	</body>
</html>
