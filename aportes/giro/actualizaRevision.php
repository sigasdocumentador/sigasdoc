<?php
	set_time_limit(0);
	ini_set("display_errors",'1');
	date_default_timezone_set("America/Bogota");
	$raiz = "";
	$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
	include_once  $root;
	include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
	$db = IFXDbManejador::conectarDB();
	if($db->conexionID==null){
		$cadena = $db->error;
		echo msg_error($cadena);
		exit();
	}
	try{
		$consec = $_POST["consec"];
		$estado = $_POST["estado"];
		$sql = "UPDATE giros.Procesamiento SET revisado={$estado} WHERE consec=".$consec;
		$rs = $db->queryActualiza($sql);
		if(intval($rs)){
			echo 1;
		}else{
			echo 0;
		}
	}catch(Exception $e){
		echo 0;
	}
?>