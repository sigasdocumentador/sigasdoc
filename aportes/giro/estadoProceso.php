<?php
	set_time_limit(0);
	ini_set("display_errors",'1');
	date_default_timezone_set("America/Bogota");
	$raiz = "";
	$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
	include_once  $root;
	include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
	$db = IFXDbManejador::conectarDB();
	if($db->conexionID==null){
		$cadena = $db->error;
		echo msg_error($cadena);
		exit();
	}
	try{
		$resultado = array("Error"=>0,"Bandera"=>null,"FechaProceso"=>null,"Descripcion"=>null,"Consec"=>null);
		$datosUltimProce = null;
		//Verificar si existen datos en la entidad Procedimiento
		$sqlNumRows = "SELECT COUNT(*) AS numrows FROM giros.Procesamiento";
		$numRows = intval(@$db->querySimple($sqlNumRows)->fetchObject()->numrows);
		
		if( intval($numRows) > 0 ){
			$sqlDatosUltimProce = "SELECT TOP 1 * FROM giros.Procesamiento ORDER BY Consec DESC";
			$rsDatos = $db->querySimple($sqlDatosUltimProce);
			$datosUltimProce = $rsDatos->fetchObject();
		}
		
		
		if( $numRows == 0 ) {
			//Bandera [AB] para activar el botton y poder seguir con un nuevo proceso
			$resultado["Bandera"] = "AB";
			
		}else if( $datosUltimProce->procedimiento == "FIN_CON_ERROR" ){
			//ERROR DEL PROCEDIMIENTO
			$resultado["Bandera"] = "ERROR";
			
		}else if( $datosUltimProce->procedimiento == "FIN_EXITOSO" ){
			
			if( intval($datosUltimProce->revisado)==1 ){
				//EL PROCESO DEL GIRO HA TERMINADO CON EXITO
				
				$fechaProceso = new DateTime($datosUltimProce->fechasistema);
				$fechaActual = new DateTime();
				$interval = $fechaProceso->diff($fechaActual);
				$resultado["Bandera"] = ( intval($interval->format('%R%a')) > 2 ) ? "AB" : "END";
				$fechaProceso->add(new DateInterval('P02D'));
				$resultado["FechaProceso"] = $fechaProceso->format('Y-m-d');
			}
			else{
				//AUN FALTA DAR EL ULTIMO PASO PARA TERMINAR EL GIRO
				$resultado["Bandera"] = "AC";
				$resultado["Consec"] = $datosUltimProce->Consec;
			}
			
		}else if( strtoupper(substr($datosUltimProce->procedimiento,0,6)) == "MANUAL" ){
			
			if( intval($datosUltimProce->revisado) == 1 ){
				//EL PROCESO ESTA LISTO PARA SEGUIR CON EL GIRO
				$resultado["Bandera"] = "EP";
			}else{
				//EL PROCESO ESTA EN ESPERA DE LA REVISION DEL USUARIO PARA SEGUIR EL PROCESO
				$resultado["Bandera"] = "IBC";
			}
			
			$resultado["Consec"] = $datosUltimProce->Consec;
		}else{
			//ALGUN PROCESO ESTA EN EJECUCION
			$resultado["Bandera"] = "EP";
		}
		
		//Extraer los datos del proceso actual de acurdo a la bandera
		if( $resultado["Bandera"] != "AB" ){
			$sqlDatosProceActua = "SELECT [Consec], [id], [procedimiento], [fechasistema], isnull([procesados],0) AS procesados, isnull([noprocesados],0) AS noprocesados, [usuario], [revisado]  FROM giros.Procesamiento WHERE id={$datosUltimProce->id} ORDER BY Consec";
			$cont = 0;
			$rsDatosProceso = $db->querySimple($sqlDatosProceActua);
			while( ( $datosProcedimiento = $rsDatosProceso->fetchObject() ) == true ){
				//Bandera [DP] para mostrar los datos del proceso actual
				////$resultado["Bandera"] = "DP";
				$resultado["Descripcion"][] = $datosProcedimiento;
				$cont++;
				//break;
			}
			if( $cont == 0 ){
				//Bandera [EDP] error al obtener los registros del proceso actual
				$resultado["Bandera"] = "EDP";
			}
		}		
	}catch(Exception $e){
		//Bandera [EE] error ejecutando de ejecucion
		$resultado["Bandera"] = "EE";
		$resultado["Descripcion"] = $e->getMessage();
	}
	echo json_encode($resultado);
?>