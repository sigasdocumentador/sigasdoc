<?php
$raiz = "";
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$url=$_SERVER['PHP_SELF'];
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'auditoria.php';
auditar($url);
include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'Consultor.php';

$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

$con = new Consultor();

$per = isset($_REQUEST['periodo'])?$_REQUEST['periodo']:"";

$acc = $_REQUEST['acc'];

if($acc == 1){
	//informacion x periodo
	$act = $_REQUEST['actual'];
	$sql = "SELECT * FROM aportes012 WHERE periodo='$per'";
	$r = $con->enArray($sql);

	$tab = ($act == 'S')?'aportes014':'aportes009';

	$sql = "SELECT COUNT(*) as cant, tipogiro, SUM(valor) as valor, SUM(descuento) as descu FROM $tab WHERE periodoproceso='$per' GROUP BY tipogiro";
	$r2 = $con->enArray($sql);

	$rf = array_merge($r, $r2);
	echo json_encode($rf);
}elseif($acc == 2){
	//Abrir Giro
	$sql = "UPDATE aportes012 SET estadogiro='A' WHERE periodo='$per'";
	$r = $con->noRetorno($sql);
	if($r){
		echo 1;
	}else{
		echo 0;
	}
}elseif($acc == 3){
	//Cerrar Giro
	$sql = "UPDATE aportes012 SET estadogiro='C' WHERE periodo='$per'";
	$r = $con->noRetorno($sql);
	if($r){
		echo 1;
	}else{
		echo 0;
	}
}elseif($acc == 4){
	//Verifica si un periodo a crear ya existe
	$sql = "SELECT COUNT(idperiodo) AS cant FROM aportes012 WHERE periodo='$per'";
	$r = $con->enArray($sql);

	$length = 10;
	$characters = '0123456789abcdefghijklmnopqrstuvwxyz';
	$string = '';

	for($p = 0; $p < $length; $p++){
		$string .= $characters[mt_rand(0, strlen($characters))];
	}

	$r[0]['control'] = $string;

	echo json_encode($r);

}elseif($acc == 5){
	//Busqueda de periodos
	$perini = $_REQUEST['perini'];
	$perfin = $_REQUEST['perfin'];

	$sql = "SELECT idperiodo, periodo, fechainicio, fechafin, fechalimite, cuotamonetaria, cuotacampo, smlv, topesalarial, controlproceso, procesado, estadogiro FROM aportes012 WHERE periodo >='$perini' AND periodo <= '$perfin' ORDER BY periodo DESC";
	$r = $con->enJSON($sql);

	echo $r;
}elseif($acc == 6){
	//Guardar
	$periodo = $_REQUEST['adm-txtPeriodo'];
	$fecini = $_REQUEST['adm-txtFecini'];
	$fecfin = $_REQUEST['adm-txtFecfin'];
	$feclim = $_REQUEST['adm-txtFeclim'];
	$smlv = intval($_REQUEST['adm-txtSmlv']);
	$valcam = intval($_REQUEST['adm-txtValcam']);
	$valcuo = intval($_REQUEST['adm-txtValsub']);
	$control = $_REQUEST['adm-txtControl'];
	$tope = $smlv * 4;
	$usuario = $_SESSION['USUARIO'];

	$sql = "INSERT INTO aportes012(periodo, fechainicio, fechafin, fechalimite, cuotamonetaria, valuni, cuotacampo, valunicampo, smlv, topesalarial, controlproceso, procesado, estadogiro, fechaestado, usuario) VALUES('$periodo', '$fecini', '$fecfin', '$feclim', $valcuo, $valcuo, $valcam, $valcam, $smlv, $tope, '$control', 'N', 'C', CAST(GETDATE() AS DATE), '$usuario')";
	$id = $con->insertId($sql);
	echo intval($id);
}elseif($acc == 7){
	//Modificar
	$periodo = $_REQUEST['adm-txtPeriodo'];
	$fecini = $_REQUEST['adm-txtFecini'];
	$fecfin = $_REQUEST['adm-txtFecfin'];
	$feclim = $_REQUEST['adm-txtFeclim'];
	$smlv = intval($_REQUEST['adm-txtSmlv']);
	$valcam = intval($_REQUEST['adm-txtValcam']);
	$valcuo = intval($_REQUEST['adm-txtValsub']);
	$control = $_REQUEST['adm-txtControl'];
	$tope = $smlv * 4;
	$usuario = $_SESSION['USUARIO'];
	
	$sql = "UPDATE aportes012 SET fechainicio = '$fecini', fechafin = '$fecfin', fechalimite = '$feclim', cuotamonetaria = $valcuo, valuni = $valcuo, cuotacampo =$valcam , valunicampo = $valcam, smlv = $smlv, topesalarial = $tope, fechaestado = CAST(GETDATE() AS DATE), usuario = '$usuario' WHERE periodo='$periodo'";
	$r = $db->queryActualiza($sql);
	if($r){
		echo '1';
	}else{
		echo '0';
	}

}elseif($acc == 8){
	$matches = preg_grep('/id[0-9]{1,}/', $_POST);
	$idperiodo = '';
	foreach($matches as $per){
		$idperiodo .= str_replace('id', '', $per).',';
	}
	$idperiodo = rtrim($idperiodo, ',');

	$sql = "DELETE FROM aportes012 WHERE idperiodo IN ($idperiodo) AND procesado='N'";
	$r = $con->noRetorno($sql);
	if($r){
		echo '1';
	}else{
		echo '0';
	}
}
?>