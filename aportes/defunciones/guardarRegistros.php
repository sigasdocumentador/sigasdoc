<?php
$raiz = "";
ini_set("display_errors",'1');
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
include_once $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'config.php';
include $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
include_once $raiz .DIRECTORY_SEPARATOR. 'clases' .DIRECTORY_SEPARATOR. 'p.afiliacion.class.php';
include_once $raiz .DIRECTORY_SEPARATOR. 'aportes'. DIRECTORY_SEPARATOR.'empresas'. DIRECTORY_SEPARATOR .'clases' .DIRECTORY_SEPARATOR. 'empresas.class.php';
include_once $raiz.DIRECTORY_SEPARATOR.'aportes'.DIRECTORY_SEPARATOR.'defunciones'.DIRECTORY_SEPARATOR.'funciones.php';

$objClase = new Afiliacion();
$objAfiEmpresa = new Empresa();

$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
$campo1 =$_REQUEST['v1'];		//idradica
$campo2 =$_REQUEST['v2'];		//idtrabajador
$campo3 =$_REQUEST['v3'];		//idfallece
$campo4 =$_REQUEST['v4'];		//fecha defuncion
$campo5 =$_REQUEST['v5'];		//forma de pago
$campo6 =$_REQUEST['v6'];		//tercero
$campo7 =$_REQUEST['v7'];		//pariente
$campo8 =$_REQUEST['v8'];		//cuotas
$campo10=$_SESSION['USUARIO'];
$campo11=$_REQUEST['v9'];		//fecha radicacion
$campo12=explode(",",(trim($_REQUEST['v10'],",")));		//idb de todos los beneficiarios del subsidio
$campo13=$_REQUEST["v11"];         //id empresa
$campo14=$_REQUEST["v12"];         //cuotaMonetaria
$campo15=$_REQUEST["v13"];         //periodo

$datos = array("error"=>0,"descripcion"=>"");
//tipo fallecido A B 
$tipoFallecido = ($campo7==0)?"A":"B";

// tipos de afiliación para independientes
$arrIdsTiposAfiliacionIndependiente = array(19,20,21,23);

$db->inicioTransaccion();
//insert registro defuncion
$sql="INSERT INTO aportes019 (idradicacion, idtrabajador, idfallecido, fechadefuncion, usuario, fechasistema,fechapresentacion, formapago, tipofallecido) VALUES ($campo1, $campo2, $campo3, '$campo4', '$campo10', cast(getdate() as date) ,'$campo11', '$campo5', '$tipoFallecido')";
$rs19=$db->queryInsert($sql, 'aportes019');
if($rs19==0){

	$datos["error"] = 1;
	$datos["descripcion"] = "La defuncion no se puede guardar, consulte con el administrador";
	echo json_encode($datos);
	$db->cancelarTransaccion();
	exit();
}

$idDefuncion=$rs19;

//Actualizar estado persona fallecida M, tipoEstado ? tipoestado=P
$sql="Update aportes015 set estado='M', fechadefuncion='$campo4' where idpersona=$campo3";
$rs=$db->queryActualiza($sql);

//datos del periodo actual
/*$sql = "SELECT TOP 1 periodo, cuotamonetaria FROM aportes012 where procesado='N' ORDER BY periodo asc";
$rs5 = $db->querySimple($sql);
$w5 = $rs5->fetch();*/
switch($campo5){
	case 'T' : $campo9='01=TARJETA'; break;
	case 'C' : $campo9='03=MUERTOS'; break;
}

for($i=0; $i<count($campo12); $i++ ){
	
	$idBeneficiario=$campo12[$i];

	//insert detalle defuncion
	$sql = "INSERT INTO aportes020 (iddefuncion, idbeneficiario, idtercero, idpariente, tipopago, codigopago, cuotas, pagadas, procesado, fechasistema, usuario, idempresa) VALUES ($idDefuncion, $idBeneficiario, $campo6, $campo7, '$campo5', '$campo9', $campo8, 0, 'N', cast(getdate() as date), '$campo10', $campo13)";
	$rs1 = $db->queryInsert($sql, 'aportes020');
	$idDetalle20 = $rs1;
	if($rs1==0){
		
		$datos["error"] = 1;
		$datos["descripcion"] = "El detalle defuncion no se puede guardar, consulte con el administrador";
		echo json_encode($datos);
		$db->cancelarTransaccion();
		exit();	
	}
	if($campo7 != 0){
		
		$sql = "UPDATE aportes021 SET estado='I',giro='N',idmotivo=2858, fechaestado=cast(getdate() as date) WHERE idbeneficiario=$idBeneficiario";
		$rs = $db->queryActualiza($sql);
	}else if($campo7==0){
		$periodo = $campo15;
		$ncuotas = intval($campo8);
		for($int =1; $int<=$ncuotas; $int++){
			
			$periodo = increPeriodo($periodo,"S");
			
			//insert detalle entidad aportes020
			$sql = "Insert into aportes023 (iddetalle019,orden,periodo,procesado,fechasistema,usuario,valor) values($idDetalle20,$int,'".$periodo."','N',cast(getdate() as date),'$campo10',".$campo14.")";
			$rs3 = $db->queryInsert($sql, 'aportes023');
			if($rs3==0){
				
				$datos["error"] = 1;
				$datos["descripcion"] = "El insert aportes023 no se puede ejecutar, consulte con el administrador";
				echo json_encode($datos);
				$db->cancelarTransaccion();
				exit();
			}
			
		}
	}
}

$db->confirmarTransaccion();

if ($campo7==0){
	
	$sql="Select idformulario,tipoafiliacion,idempresa from aportes016 where idpersona = $campo2";
	$rsAfiliacion=$db->querySimple($sql);

	while(($rowAfiliacion = $rsAfiliacion->fetchObject())==true){
		
		$idFormularioActual = intval($rowAfiliacion->idformulario);
		$idMotivoRetiro = 2860;
		$idTipoAfi = $rowAfiliacion->tipoafiliacion;
		$idEmpresa = $rowAfiliacion->idempresa;
		
		// se inactiva la afiliación
		// se inactiva estas lineas ya que no facilita el control de la respuesta del procedimiento almacenado
		/*
		$sql = "execute sp_inactivar $idFormularioActual,$idMotivoRetiro,'$campo4','{$_SESSION["USUARIO"]}', 0";		
		$rsInactivar = $db->querySimple($sql);
		*/
		// se remplaza por las siguentes
		$idUltimaTrayectoria = 0;
		$sentencia = $db->conexionID->prepare ("EXEC [dbo].[sp_inactivar]
				@idFormulario = $idFormularioActual
				, @idMotivoRetiro = $idMotivoRetiro
				, @fechaRetiro = '$campo4'
				, @usuarioInactiva = '{$_SESSION["USUARIO"]}'
				, @idUltimaTrayectoria = :idUltimaTrayectoria" );
		$sentencia->bindParam ( ":idUltimaTrayectoria", $idUltimaTrayectoria, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000 );
		$sentencia->execute ();
		
		// fin remplazo
		
		$idFormCreado = 0;
		if($idUltimaTrayectoria==0){
			$datos["error"] = 2;
			$datos["descripcion"] = "NO SE pudo inactivar la afiliacion, consulte con el administrador";			
		} else if($idUltimaTrayectoria==1) {
			$datos["error"] = 2;
			$datos["descripcion"] = "NO SE pudo en aportes017, consulte con el administrador";
		} else {			
			$idFormCreado = intval($db->conexionID->lastInsertId());
			if($idFormCreado == 0){				
				// el formulario a inactivar solicitado no existe
				$datos["error"] = 2;
				$datos["descripcion"] = "El formulario a inactivar no existe, consulte con el administrador";
			}else{				
				// Si es trabajador independiente se inactiva la empresa
				if(in_array(intval($idTipoAfi),$arrIdsTiposAfiliacionIndependiente)){
					$resultEmpresa = $objAfiEmpresa->buscar_empresa($idEmpresa);
					$empresa = mssql_fetch_array($resultEmpresa);
					$empresaInactivada = $objAfiEmpresa->inactivar_empresa_por_NIT($empresa["nit"]);
					if(!$empresaInactivada){						
						$datos["error"] = 2;
						$datos["descripcion"] = "No se puede inactivar la empresa, consulte con el administrador";
					}
				}
			}
		}
	}
}

/*
 * Para garantizar en los casos en que el afiliado al momento de aplicar la defuncion no tenga afiliaciones activas, se toma la ultima
 * desafiliacion y se cambia el estado a 2860 para anular los periodos de fidelidad si lo llega a tener
 */
$sqlfide="Select TOP 1 idformulario,tipoafiliacion,idempresa from aportes017 where idpersona = $campo2 ORDER BY fecharetiro DESC" ;
$rsfide=$db->querySimple($sqlfide);
while ($row=$rsfide->fetch()){
	$sqlupdatefide="update aportes017 set motivoretiro=2860 where idformulario=".$row['idformulario'];
	$rsupdatefide = $db->queryActualiza($sqlupdatefide);
}
/*
 * fin verificacacion.
 * 
*/


echo json_encode($datos);


?>