<?php

/* autor:       Orlando Puentes
 * fecha:       Julio 23 de 2010
 * objetivo:    Almacenar en la base de datos Aportes la información de las defunciones del cónyuge, beneficiario o trabajador afiliado. 
 */
$raiz="";
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
include_once $raiz.DIRECTORY_SEPARATOR ."config.php";
$url=$_SERVER['PHP_SELF'];
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'auditoria.php';
auditar($url);

include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'p.definiciones.class.php';
$objClase= new Definiciones;
$consulta = $objClase->mostrar_datos(1,2);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<title>Defunciones</title>
		<link type="text/css" href="../../css/css.1.8/jquery-ui-1.8.17.custom.css" rel="stylesheet">
		<link href="../../css/marco.css" rel="stylesheet" type="text/css" />
		<link href="../../css/Estilos.css" rel="stylesheet" type="text/css" />		
		
		<script type="text/javascript" src="../../js/1.6/jquery-1.6.2.min.js"></script>
		<script type="text/javascript" src="../../js/1.6/jquery-ui-1.8.16.custom.min.js"></script>
		
		<script language="javascript" src="../../js/comunes.js"></script>
		<script language="javascript" src="js/defunciones.js"></script>
		<script type="text/javascript">
			shortcut.add("Shift+F",function() {
				var URL=src();
				var url=URL+"aportes/trabajadores/consultaTrabajador.php";
		    	window.open(url,"_blank");
		    },{
				'propagate' : true,
				'target' : document 
		    });        
		</script>
	</head>
	<body>
		<div id="divMensaje" style="text-align: center;font-size: 15px;font-style: oblique;"></div>
		<form name="forma">
			<br/>
			<center>
				<table width="95%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="13" height="29" class="arriba_iz">&nbsp;</td>
						<td class="arriba_ce"><span class="letrablanca">::&nbsp;Administracion - Defunciones &nbsp;::</span></td>
						<td width="13" class="arriba_de" align="right">&nbsp;</td>
					</tr>
					<tr>
						<td class="cuerpo_iz">&nbsp;</td>                        
						<td style="background:url(../../imagenes/tabla/centro.gif);"> <img src="../../imagenes/tabla/spacer.gif" width="1" height="1"/> 
							<img src="../../imagenes/spacer.gif" width="2" height="1"/> 
							<img src="../../imagenes/menu/nuevo.png" title="Nuevo" width="16" height="16" style="cursor:pointer" onclick="nuevoR();"/> 
							<img src="../../imagenes/spacer.gif" width="2" height="1"/> 
							<img src="../../imagenes/menu/grabar.png" title="Guardar" width="16" height="16" onclick="validarCampos(1)" style="cursor:pointer" id ="bGuardar"/> 
							<img src="../../imagenes/spacer.gif" width="2" height="1"/> 
							<img src="../../imagenes/menu/imprimir.png" width="16" height="16" style="cursor:pointer" title="Imprimir" onclick="window.open('<?php echo $ruta_reportes;?>aportes/defunciones/reporte001.jsp?v0=<?php echo $_SESSION["USUARIO"];?>','mainFrame')" /> 
							<img src="../../imagenes/spacer.gif" width="2" height="1"/> 
							<img src="../../imagenes/menu/refrescar.png" width="16" height="16" style="cursor:pointer" title="Limpiar campos" onclick="limpiarCampos(); document.forms[0].elements[0].focus();"/> 
							<img src="../../imagenes/spacer.gif" width="2" height="1"/> 
							<a href="../../help/aportes/ayudaDefunciones.html" target="_blank" onclick="window.open(this.href, this.target, 'width=800,height=550,titlebar=0, resizable=no'); return false;" > 
								<img src="../../imagenes/menu/informacion.png" width="16" height="16" style="border:none" title="Manual" />
							</a> 
							<img src="../../imagenes/spacer.gif" width="2" height="1"/> 
							<img src="../../imagenes/menu/notas.png" width="16" height="16" style="cursor: pointer" title="Colaboraci�n en L�nea" onclick="notas();" /> 
							<br/>
						</td>
						<td class="cuerpo_de">&nbsp;</td>
					</tr>
					<tr>
						<td class="cuerpo_iz">&nbsp;</td>
						<td class="cuerpo_ce"><div id="resultado" style="color:#FF0000"></div></td>
						<td class="cuerpo_de">&nbsp;</td>
					</tr>
					<tr>
						<td class="cuerpo_iz">&nbsp;</td>
						<td class="cuerpo_ce">
							<table width="95%" border="0" cellspacing="0" class="tablero">
					  			<tr>
								  	<td width="25%" align="left">Id Radicacion</td>
							  		<td width="25%" align="left">
							  			<select name="pendientes" id="pendientes" class="box1" onchange="buscarRadicacion();">
								  			<option value="0" selected="selected">Seleccione</option>
                   
					  					</select>
					  				</td>
					  				<td width="25%" align="left">Fecha </td>
					  				<td width="25%" align="left"><input name="fecha" class="box1" id="fecha" readonly="readonly" /></td>
					  			</tr>
					  			<tr>
					  				<td align="left">Tipo Identificaci&oacute;n</td>
					  				<td  align="left">
				  						<select name="tipoI" class="box1" id="tipoI" disabled="disabled">
					  					<?php
					  						$consulta = $objClase->mostrar_datos(1,2);
					  						while(($row=mssql_fetch_array($consulta))==true){
					    						echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
					  						}
				  						?>
					  					</select><img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
					  				</td>
				  					<td  align="left">N&uacute;mero</td>
				  					<td  align="left">
								  		<input name="numero" class="box1" id="numero" readonly="readonly"/>
									  	<img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
					  				</td>
					  			</tr>
					  			<tr>
							  		<td align="left">Afiliado</td>
							  		<td colspan="3" align="left" id="nombreT">&nbsp;</td>
					  			</tr>
					  			<tr>
					  				<td align="left">Fecha Afiliaci&oacute;n</td>
					  				<td align="left" id="fafilia">&nbsp;</td>
					  				<td align="left">Estado</td>
					  				<td align="left" id="estado">&nbsp;</td>
					  			</tr>
					  			<tr>
					  				<td align="left">Salario</td>
					  				<td align="left" id="salario">&nbsp;</td>
					  				<td align="left">Fecha Retiro</td>
					  				<td align="left" id="fretiro">&nbsp;</td>
					  			</tr>
					  			<tr>
						  			<td width="25%" align="left">Quien falleci&oacute;?</td>
						  			<td colspan="3" align="left">
						  				<label id="tipoFallecido"></label>
										<img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
						  			</td>
						  		</tr>
					  			<tr>
					    			<td align="left">Fecha Defunci&oacute;n</td>
						    		<td colspan="3" align="left">
						    			<input type="text" name="fechaDefuncion" id="fechaDefuncion" class="box1"  />
						      			<img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
					      			</td>
						  		</tr>
						  		<tr>
						  			<td align="left" >Fallecido</td>
						  			<td align="left" >
						  				<label id="txtFallecido"></label>	
						  			</td>
						  			<td align="left" >Valor</td>
						  			<td align="left" >
						  				<label id="textcuota"></label>						  				
						  			</td>
						  		</tr>
						  		<tr>
						    		<td align="left" class="Rojo">Tercero del Subsidio</td>
						    		<td colspan="3" align="left" id="tdTercero">
							    		<img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
						    		</td>
					    		</tr>
					    		<tr id="trOtro" style="display: none;" >
									<td class="Rojo" >Tipo Documento</td>
									<td><select name="cmbIdTipoDocumento"
												id="cmbIdTipoDocumento" class="box1" style="width: 250px" 
												onchange="$('#txtIdentificacion').val('').trigger('blur');" >
													<option value="0" selected="selected">Seleccione...</option>
						            		<?php
												$consulta = $objClase->mostrar_datos(1,2);
					  							while(($row=mssql_fetch_array($consulta))==true){
					    							echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
					  						}
				  							?>
						          		</select> <img src="../../imagenes/menu/obligado.png" width="12" height="12"></td>
										<td class="Rojo" >N&uacute;mero</td>
										<td><input name="txtIdentificacion" type="text" class="box1" id="txtIdentificacion" onblur="buscarTercero();" maxlength="12">											
											<img src="../../imagenes/menu/obligado.png" width="12" height="12"></td>
								</tr>
								<tr id="trNombreOtro" style="display: none;" >
									<td class="Rojo" >Nombre Completo</td>
									<td id="tdNombreOtro" colspan="3">&nbsp;</td>
								</tr>
						  		<tr>
							    	<td align="left" class="Rojo">Tipo Pago</td>
								    <td colspan="3" align="left">
								    	<select name="tipoPago" id="tipoPago" class="box1" >
										    <option value="0" selected="selected">Seleccione</option>
										    <option value="T">Tarjeta</option>
										    <option value="C">Cheque</option>
								      	</select>
								      	<img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
							      	</td>
						    	</tr>
						  		<tr>
						    		<td align="left" class="Rojo">N&uacute;mero de cuotas</td>
						    		<td colspan="3" align="left"><input type="text" name="numCuotas" id="numCuotas" class="boxcorto" onkeyup="solonumeros(this)" />  </td>
						  		</tr>
						  		<tr>
							  		<td align="left" class="Rojo">Subsidio por:</td>
							  		<td colspan="3" align="left" id='tdBeneficiario'>
							  			<table width="95%" id="beneficiarios" style="border-collapse: collapse">
										  <tr>
										    <td align="left" width="3%"  style="border:0px" >&nbsp;</td>
										    <td align="left" width="12%" style="border:0px" ><b>Identificaci&oacute;n</b></td>
										    <td align="left" width="25%" style="border:0px" ><b>Nombres</b></td>
										    <td align="left" width="15%" style="border:0px" ><b>Parentesco</b></td>
										  </tr>										  
									    </table>
							  		</td>
					  			</tr>
						  		<tr>
							  		<td align="left" >&nbsp;</td>
							  		<td colspan="3" align="left">&nbsp;</td>
						  		</tr>
						  		<tr>
						  			<td colspan="4" align="left" class="Rojo">&nbsp;</td>
						  		</tr>
				  			</table>
						</td>
						<td class="cuerpo_de">&nbsp;</td>
					</tr>
					<tr>
						<td class="abajo_iz" >&nbsp;</td>
						<td class="abajo_ce" ></td>
						<td class="abajo_de" >&nbsp;</td>
					</tr>
				</table>
			</center>
			<input type="hidden" value="<?php echo $_SESSION["USUARIO"];?>" name="usuario" id="usuario" />
			<input type="hidden" name="idr" id="idr" />
            <input type="hidden" name="iddefuncion" id="iddefuncion" />
            <input type="hidden" name="hdnBeneficiarios" id="hdnBeneficiarios"/> 
            <input type="hidden" name="hdnFallecido" id="hdnFallecido"/> 
            <input type="hidden" name="hdnIdTercero" id="hdnIdTercero"/> 
            <input type="hidden" name="hdnIdParentesco" id="hdnIdParentesco"/> 
            <input type="hidden" name="hdnIdEmpresa" id="hdnIdEmpresa"/>
            <input type="hidden" name="hdnPeriodo" id="hdnPeriodo"/>
            <input type="hidden" name="hdnCuotaMonetaria" id="hdnCuotaMonetaria"/>
 		</form>
			
			<!-- colaboracion en linea -->
			<!-- FORMULARIO OBSERVACIONES-->
		<div name="div-observaciones-tab" style="display:none" title="Observaciones de modificaci&oacute;n">
			<table class="tablero">
		 		<tr>
				    <td>Usuario</td>
				    <td colspan="3" >
				    	<input name="usuarioObs" id="usuarioObs" class="box1" disabled="disabled" value="<?php echo $_SESSION['USUARIO']?>" /></td>
				</tr>
			 	<tr>
					<td>Observaciones</td>
					<td colspan="3" >
				    	<textarea name="observacionGrupo" id="observacionGrupo" cols="45" rows="5" class="boxlargo"></textarea></td>
				</tr>
			</table>
			<div class="ui-state-highlight ui-corner-all" style="margin: 9px auto; display:none;padding: 6px; text-align: center; border: 1px solid rgb(255, 255, 0); width:300px" id="rtaObservacion"><span class="ui-icon ui-icon-info" style="float: left; margin-right: 0.3em;"></span>Observaci&oacute;n guardada con &eacute;xito.</div>
		</div>
	
	<!-- fin colaboracion -->
	
	
	<!-- DIALOGO PERSONA SIMPLE -->
	<div id="dialog-persona" title="Formulario Datos Basicos" style="display:none">
	<center>
		<table width="578" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" class="tablero">
			<tbody>
				<tr bgcolor="#EBEBEB">
				  <td colspan="4" style="text-align:center" >Datos B&aacute;sicos de la persona</td>
			  </tr>
				<tr>
				  <td >Tipo Documento</td>
				  <td ><select name="tDocumento" id="tDocumento" class="box1" disabled>
		          	<option value="1">CEDULA DE CIUDADANIA</option>
					<option value="2">TARJETA DE IDENTIDAD</option>
					<option value="3">PASAPORTE</option>
					<option value="4">CEDULA DE EXTRANJERIA</option>
					<option value="5">NIT</option>
					<option value="6">REGISTRO CIVIL</option>
					<option value="7">MENOR SIN IDENTIFICACION</option>
					<option value="8">ADULTO SIN IDENTIFICACION</option>
		          </select>
		          </td>
				  <td >N&uacute;mero</td>
				  <td ><input name="txtNumeroP" id="txtNumeroP" type="text" class="box1" readonly></td>
			  </tr>
				<tr>
				  <td >Primer Nombre</td>
				  <td ><input name="spNombre" id="spNombre" type="text" class="box1" onkeydown='sololetras(this);' onkeyup='sololetras(this);' onkeypress="return validarEspacio(event)"></td>
				  <td >Segundo Nombre</td>
				  <td ><input name="ssNombre" id="ssNombre" type="text" class="box1" onkeydown='sololetras(this);' onkeyup='sololetras(this);'></td>
			  </tr>
				<tr>
				  <td >Primer Apellido</td>
				  <td ><input name="spApellido" id="spApellido" type="text" class="box1" onkeydown='sololetras(this);' onkeyup='sololetras(this);' onkeypress="return validarEspacio(event)"></td>
				  <td >Segundo Apellido</td>
				  <td ><input name="ssApellido" id="ssApellido" type="text" class="box1" onkeydown='sololetras(this);' onkeyup='sololetras(this);'></td>
			  </tr>
			  <tr>
		    	<td><label for="fechaNaceDialogPersona">Fecha de nacimiento</label></td>
		    	<td><input type="text" name="fechaNaceDialogPersona" id="fechaNaceDialogPersona" /></td>
		    	<td >&nbsp;</td>
				<td >&nbsp;</td>
			  </tr>
				<tr>
					<td >&nbsp;</td>
					<td >&nbsp;</td>
					<td >&nbsp;</td>
					<td >&nbsp;</td>
				</tr>
		</tbody>
		</table>
	</center>
	</div>
	</body>
</html>

<script language="javascript">
	$(function() {
		/*COLABORACI�N EN LINEA PARA OCULTAR Y MOSTRAR EL FORMULARIO*/
		$("#dialog-form2").dialog("destroy");
		/*var notas = $("#notas"),
		allFields = $([]).add(notas);*/
		$("#dialog-form2").dialog({
			autoOpen: false,
			height: 400,
			width: 500,
			modal: true,
			buttons: {
				'Enviar': function() {
					var bValid = true;
					var campo=$('#notas').val();
					var campo0=$.trim(campo);
					if (campo0==""){
						$(this).dialog('close');
						return false;
					}
					var campo1=$('#usuario').val();
					var campo2="defunciones.php";
					$.post('../../phpComunes/colaboracion.php',{v0:campo0,v1:campo1,v2:campo2}, function(datos){
						if(datos=='1'){
							alert("Su comentario fue enviado correctamente!. Gracias por participar en el mejoramiento de SIGAS");
						}
						else{
							alert(datos);
						}
					});
					$(this).dialog('close');
				},
				Cancelar: function() {
					$(this).dialog('close');
					$("#dialog-form2").dialog("destroy");
				}
			}
			
		});
		/*$('#enviar-notas')
			.button()
			.click(function() {
				$('#dialog-form2').dialog('open');
			});*/
	});
	
	function notas(){
		$("#dialog-form2").dialog('open');
	}	
	
	function mostrarAyuda(){
		$("#ayuda").dialog('open' );
	}
</script>