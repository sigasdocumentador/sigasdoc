<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';

class Preguntas{
	var $preguntas = array();
	var $tiposPreguntas = array("direccion","telefono","saldotarjeta","tienesaldo","empresa");
	var $idAfiliado = null;
	private $db;
	
	public function __construct($idAfiliado){
		$this->db = IFXDbManejador::conectarDB();
		if($this->db->conexionID==null){
			$cadena = $this->db->error;
			echo msg_error($cadena);
			exit();
		}
		$this->idAfiliado = $idAfiliado;
	}

	public function generarPreguntasJSON(){
		foreach($this->tiposPreguntas as $tipo)
			$this->generarPreguntasPorTipo($tipo);
		$this->imprimirPreguntasJson();
	}

	public function generarPreguntasPorTipo($tipo){
		switch($tipo){
			case "direccion":
				$sql = "SELECT TOP 2 idpersona,direccion FROM aportes015 WHERE idpersona <> $this->idAfiliado AND direccion is not null AND direccion <> '' ORDER BY newid()";
				$rs = $this->db->querySimple($sql);
				$cont = 0;
				while ($dato = $rs->fetch()){
					$this->preguntas[$tipo][$cont]["respuesta"] = trim($dato["direccion"]);
					$this->preguntas[$tipo][$cont]["correcta"] = false;
					$cont++;
				}
					
				$sql = "SELECT idpersona,direccion FROM aportes015 WHERE idpersona = $this->idAfiliado";
				$rs = $this->db->querySimple($sql);
				while ($dato = $rs->fetch()){
					$this->preguntas[$tipo][$cont]["respuesta"] = trim($dato["direccion"]);
					$this->preguntas[$tipo][$cont]["correcta"] = true;
					$cont++;
				}
				break;
			case "telefono":
				$sql = "SELECT TOP 2 idpersona,telefono FROM aportes015 WHERE idpersona <> $this->idAfiliado AND telefono is not null AND telefono <> '' AND telefono <> '0' ORDER BY newid()";
				$rs = $this->db->querySimple($sql);
				$cont = 0;
				while ($dato = $rs->fetch()){
					$this->preguntas[$tipo][$cont]["respuesta"] = trim($dato["telefono"]);
					$this->preguntas[$tipo][$cont]["correcta"] = false;
					$cont++;
				}
					
				$sql = "SELECT idpersona,telefono FROM aportes015 WHERE idpersona = $this->idAfiliado";
				$rs = $this->db->querySimple($sql);
				while ($dato = $rs->fetch()){
					$this->preguntas[$tipo][$cont]["respuesta"] = trim($dato["telefono"]);
					$this->preguntas[$tipo][$cont]["correcta"] = true;
					$cont++;
				}
				break;
			case "saldotarjeta":
				$sql="SELECT top 2 idpersona,saldo FROM aportes101 WHERE estado IN ('A','B','S') AND idpersona <> $this->idAfiliado AND saldo > 0 ORDER BY newid()";
				$rs = $this->db->querySimple($sql);
				$cont = 0;
				while ($dato = $rs->fetch()){
					$this->preguntas[$tipo][$cont]["respuesta"] = $dato["saldo"];
					$this->preguntas[$tipo][$cont]["correcta"] = false;
					$cont++;
				}
				
				$sql="SELECT TOP 1 * FROM aportes101 WHERE estado IN ('A','B','S') AND idpersona=$this->idAfiliado"; // se usa top 1 porque puede tener varias tarjetas
				$rs = $this->db->querySimple($sql);
				while ($dato = $rs->fetch()){
					$this->preguntas[$tipo][$cont]["respuesta"] = $dato["saldo"];
					$this->preguntas[$tipo][$cont]["correcta"] = true;
					$cont++;
				}
				break;
			case "tienesaldo":
				$sql="SELECT TOP 1 saldo FROM aportes101 WHERE estado IN ('A','B','S') AND idpersona=$this->idAfiliado"; // se usa top 1 porque puede tener varias tarjetas
				$rs = $this->db->querySimple($sql);
				while ($dato = $rs->fetch()){
					if($dato["saldo"] > 0){
						$this->preguntas[$tipo][0]["respuesta"] = "Si";
						$this->preguntas[$tipo][0]["correcta"] = true;
						$this->preguntas[$tipo][1]["respuesta"] = "No";
						$this->preguntas[$tipo][1]["correcta"] = false;
					}else{
						$this->preguntas[$tipo][0]["respuesta"] = "Si";
						$this->preguntas[$tipo][0]["correcta"] = false;
						$this->preguntas[$tipo][1]["respuesta"] = "No";
						$this->preguntas[$tipo][1]["correcta"] = true;
					}
					
				}
				break;
			case "empresa":
				$cont = 0;
				$sql="SELECT TOP 2 aportes048.idempresa,nit,razonsocial FROM aportes048,aportes016 WHERE aportes048.idempresa=aportes016.idempresa AND idpersona <> ". $this->idAfiliado ." ORDER BY newid()";
				$rs = $this->db->querySimple($sql);
				while ($dato = $rs->fetch()){
					$this->preguntas[$tipo][$cont]["respuesta"] = $dato["razonsocial"];
					$this->preguntas[$tipo][$cont]["correcta"] = false;
					$cont++;
				}
				
				$sql = "SELECT TOP 1 aportes048.idempresa,nit,razonsocial FROM aportes048,aportes016 WHERE aportes048.idempresa=aportes016.idempresa AND idpersona=". $this->idAfiliado ." AND aportes016.estado in ('A','P') ORDER BY aportes016.estado";
				$rs = $this->db->querySimple($sql);
				$cont2 = 0;
				while ($dato = $rs->fetch()){
					$this->preguntas[$tipo][$cont]["respuesta"] = $dato["razonsocial"];
					$this->preguntas[$tipo][$cont]["correcta"] = true;
					$cont++;
					$cont2++;
				}
				
				if($cont2 == 0){
					$this->preguntas[$tipo][$cont]["respuesta"] = "NO TIENE AFILIACION ACTUALMENTE";
					$this->preguntas[$tipo][$cont]["correcta"] = true;
				}
				break;
		}
	}

	public function imprimirPreguntasJson(){
		print_r(json_encode($this->preguntas));
		die();
	}
}
?>