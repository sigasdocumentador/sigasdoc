<?php
/* autor:       orlando puentes
 * fecha:       18/08/2010
 * objetivo:    Procesar los movimientos de los Bonos almacenados en la base de datos. 
 */

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$raiz=$_SESSION['RAIZ'];
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'conexion.class.php';

class Radicacion{
var $con;
var $fechaSistema;

function Radicacion(){
 		$this->con=new DBManager;
 	}

function insertar($campos){
	if($this->con->conectar()==true){
		$sql="INSERT INTO aportes004 (fecharadicacion,identificacion,idtipodocumento, idtiporadicacion, idtipopresentacion,horainicio,horafinal, notas, idtipoinformacion, numero, nit, folios, idtipocertificado, idbeneficiario, observaciones, fechasistema,usuario, idtipodocumentoafiliado,idagencia,idtipoformulario,idtipodocben,numerobeneficiario,afiliacionmultiple) VALUES (cast(getdate() as date),'".$campos[2]."','".$campos[3]."','".$campos[4]."','".$campos[5]."','".$campos[6]."','".$campos[7]."','".$campos[9]."','".$campos[10]."','".$campos[11]."','".$campos[12]."','".$campos[13]."','".$campos[14]."','".$campos[15]."','".$campos[16]."',cast(getdate() as date),'".$campos[17]."','".$campos[18]."','".$campos[19]."','".$campos[20]."','".$campos[21]."','".$campos[22]."','".$campos[23]."')";
		return mssql_query($sql,$this->con->conect);
	}
}

/**
 * Obtiene los datos de un tipo de radicaci�n seg�n el nombre recibido � el id del tipo de radicaci�n recibido
 * @param string $nombre default ''
 * @param int $idTipo default null
 */
function buscarTipoRadicacion($nombre = '', $idTipo = null){
	if($this->con->conectar()==true){
		$sql = "";
		if($nombre != '' && $nombre != null)
			$sql="SELECT * FROM aportes091 WHERE detalledefinicion='$nombre' OR concepto = '$nombre'";
		elseIf($idTipo != null && intval($idTipo)>0)
			$sql="SELECT * FROM aportes091 WHERE iddetalledef = $idTipo";
		if($sql != "")
			return mssql_query($sql,$this->con->conect);
		return false;
	}
}

/**
 * 
 * Busca todas las radicaciones de una persona seg�n el tipo de radicaci�n
 * @param int $idPersona
 * @param int $idTipoRadicacion
 * @param array $parametros opcional, arreglo asociativo con par�metros de consulta
 */
function buscarRadicacionPorIdPersonaYTipo($idPersona,$idTipoRadicacion, $parametros = array()){
	if($this->con->conectar()==true){

		$strProcesado = (isset($parametros["procesado"]))?" AND a4.procesado = '{$parametros["procesado"]}'":"";
	
		$sql="SELECT 
				a15.*,
				a4.fecharadicacion,
				a4.folios,
				a4.procesado,
				a4.fechaproceso,
				a4.usuario,
				a4.idagencia,
				a91.codigo as cod_tipo_radicacion,
				a91.detalledefinicion as tipo_radicacion
			  FROM 
			  	aportes015 a15 
			  INNER JOIN aportes004 a4 ON a4.idtipodocumentoafiliado = a15.idtipodocumento AND a4.numero = a15.identificacion AND a4.idtiporadicacion=$idTipoRadicacion
			  INNER JOIN aportes091 a91 ON a91.iddetalledef = a4.idtiporadicacion  
			  WHERE 
			  	a15.idpersona = $idPersona ".
				$strProcesado;
		return mssql_query($sql,$this->con->conect);
	}
}

/**
 * 
 * Busca todas las radicaciones de una persona seg�n el tipo de radicaci�n y n�mero de documento de identificaci�n
 * @param int $idTipoDoc
 * @param string $identificacion
 * @param int $idTipoRadicacion
 * @param array $parametros opcional, arreglo asociativo con par�metros de consulta
 */
function buscarRadicacionPorDocumentoYTipoDocumento($idTipoDoc, $identificacion, $tipoRadicacion , $parametros = array()){
	if($this->con->conectar()==true){

		$strNoIncluir = (isset($parametros["noincluir"]))?" AND a4.idradicacion NOT IN (". $parametros["noincluir"] .")":"";
		$strProcesado = (isset($parametros["procesado"]))?" AND a4.procesado = '".$parametros["procesado"]."'":"";
		$sql="SELECT 
				a4.fecharadicacion,
				a4.folios,
				a4.procesado,
				a4.fechaproceso,
				a4.usuario,
				a4.idagencia,
				a91.codigo as cod_tipo_radicacion,
				a91.detalledefinicion as tipo_radicacion
			  FROM 
			  	aportes004 a4 
			  INNER JOIN aportes091 a91 ON a91.iddetalledef = a4.idtiporadicacion
			  WHERE 
			  	a4.idtipodocumentoafiliado = $idTipoDoc AND
			  	a4.numero = '$identificacion' AND 
			  	a4.idtiporadicacion=$tipoRadicacion ".			  	
				$strProcesado .
				$strNoIncluir;
		return mssql_query($sql,$this->con->conect);
	}
}
	
function buscarRadicacionTipo($tr){
	if($this->con->conectar()==true){
			$sql="SELECT top 1 * FROM aportes004 WHERE idtiporadicacion=$tr AND procesado='N' and asignado='N'";
			return mssql_query($sql,$this->con->conect);
		}
 }

function buscarRadicacionTipo2($tr){
	if($this->con->conectar()==true){
			$sql="SELECT * FROM aportes004 WHERE idtiporadicacion=$tr AND procesado='N'";
			return mssql_query($sql,$this->con->conect);
		}
 }

function buscarRadicacionExistente($tr,$numero){
	if($this->con->conectar()==true){
	$sql="select count(*) as pendientes from aportes004 where idtiporadicacion=".$tr." and procesado='N' and numero=".$numero;
	//echo $sql;
	return mssql_query($sql,$this->con->conect);
    }
 }

function buscarRadicacionTipoID($tr){
	if($this->con->conectar()==true){
	$sql="SELECT idradicacion, fechasistema FROM aportes004 WHERE idtiporadicacion in($tr) AND procesado='N' order by idradicacion";
	return mssql_query($sql,$this->con->conect);
    }
 }
 
function buscarRadicacion($idr){
	if($this->con->conectar()==true){
	$sql="SELECT * FROM aportes004 WHERE idradicacion=$idr";
	return mssql_query($sql,$this->con->conect);
    }
 }
 
 function buscarDocumentosRadicacion($idr){
 	if($this->con->conectar()==true){
 		$sql="SELECT a91.codigo, a91.detalledefinicion, a29.cantidad FROM aportes028 a28 INNER JOIN aportes029 a29 ON a29.idregistro=a28.idregistro AND a28.idradicacion=$idr INNER JOIN aportes091 a91 ON a91.iddetalledef=a29.iddocumento ORDER BY a91.codigo";
 		return mssql_query($sql,$this->con->conect);
 	}
 }
 
function buscarRadicacionTipoUsuario($tr,$usu){
	if($this->con->conectar()==true){
			$sql="SELECT top 1 * FROM aportes004 WHERE idtiporadicacion=$tr AND procesado='N' and asignado='S' and usuarioasignado='$usu'";
			return mssql_query($sql,$this->con->conect);
		}
 }

function insert_55($campos){
	if($this->con->conectar()==true){
		$sql="insert into aportes055 (idtrabajador,fechareclamo,periodoinicial,periodofinal,idcausal,notas,usuario) values(".$campos[0].",cast(getdate() as date()),'".$campos[1]."','".$campos[2]."',".$campos[3].",'".$campos[4]."','".$campos[5]."')";
		//echo $sql;
		return mssql_query($sql,$this->con->conect);
		}
	} 
	
function contar_reclamos_t($campos){
	if($this->con->conectar()==true){
		$sql="Select count(*) as cuenta from aportes055 where idtrabajador=".$campos[0]." and periodoinicial='".$campos[1]."'";
		//echo $sql;
		return mssql_query($sql,$this->con->conect);
			} 
	}
	
function contar_reclamos_e($campos){
	if($this->con->conectar()==true){
		$sql="Select count(*) as cuenta from aportes060 where idempresa=".$campos[0]." and periodoinicial='".$campos[1]."'";
		//echo $sql;
		return mssql_query($sql,$this->con->conect);
			} 
	}	
	
function insert_60($campos){
	if($this->con->conectar()==true){
		$sql="insert into aportes060 (idempresa,fechareclamo,periodoinicial,periodofinal,idcausal,notas,usuario,estado) values(".$campos[0].",cast(getdate() as date()),'".$campos[1]."','".$campos[2]."',".$campos[3].",'".$campos[4]."','".$campos[5]."','A')";
		//echo $sql;
		return mssql_query($sql,$this->con->conect);
		}
	} 	
	
function insert_defuncion($campos){
	if($this->con->conectar()==true){
	  $sql="INSERT INTO aportes019 (idtrabajador,idfallecido,idparentesco,idautorizado,tipopago,fechadefuncion,usuario,fechasistema) VALUES(".$campos[0].",".$campos[1].",'".$campos[2]."',".$campos[3].",'".$campos[4]."','".$campos[5]."','".$campos[6]."',cast(getdate() as date()))";
		//echo $sql;
		return mssql_query($sql,$this->con->conect);
		}
	} 		
	
function buscarRadicacionIdTip($idr, $tip){
	if($this->con->conectar()==true){
			$sql="SELECT aportes004.*, a.pnombre, a.snombre, a.papellido,a.sapellido,
b.pnombre as pn2, b.snombre as sn2, b.papellido as pa2, b.sapellido as sa2, c.razonsocial FROM aportes004 INNER JOIN aportes015 a ON aportes004.identificacion=a.identificacion
left JOIN aportes015 b ON aportes004.numero=b.identificacion LEFT JOIN aportes048 c ON aportes004.nit=c.nit WHERE idradicacion=$idr AND idtiporadicacion=$tip";
			return mssql_query($sql,$this->con->conect);
		}
 }
	
function buscarRadicacionId($idr){
	if($this->con->conectar()==true){
			$sql="SELECT aportes004.*, a.idpersona as idafiliado, a.pnombre, a.snombre, a.papellido, a.sapellido, b.idpersona as idafiliado2, b.idtipodocumento as idtipodocumento2, b.identificacion as identificacion2,
b.pnombre as pn2, b.snombre as sn2, b.papellido as pa2, b.sapellido as sa2, c.razonsocial FROM aportes004 INNER JOIN aportes015 a ON aportes004.identificacion=a.identificacion
left JOIN aportes015 b ON aportes004.numero=b.identificacion LEFT JOIN aportes048 c ON aportes004.nit=c.nit WHERE idradicacion=$idr";
			return mssql_query($sql,$this->con->conect);
		}
 }

function buscarRadicacionId2($idr){
    if($this->con->conectar()==TRUE){
        $sql="select idradicacion, fecharadicacion, idtipodocumentoafiliado, numero, aportes004.nit, idempresa, idtipoformulario,
	razonsocial,idempresa from aportes004 inner join aportes048 on  aportes004.nit = aportes048.nit where idradicacion=$idr and principal='S'";
        return mssql_query($sql,$this->con->conect);
    }
}
/**
 * @author Jose Luis Rojas
 * @since 06/22/2010
 * 
 * Busca por numero de radicacion las radicaciones que no han sido digitalizadas
 * 
 * 
 * @param $nr Numero de radicacion
 * @return resource Los datos de la radicacion si es encontrada, falso si no es encontrada
 */ 
function buscarRadicacionNoDitalizada($nr){
	if($this->con->conectar()==true){
			$sql="SELECT top 1 fecharadicacion, idtiporadicacion, aportes091.detalledefinicion, numero, nit, folios FROM aportes004 INNER JOIN aportes091 ON aportes004.idtiporadicacion=aportes091.iddetalledef WHERE idradicacion=$nr AND procesado='N' and asignado='N' AND digitalizada='N'";
			return mssql_query($sql,$this->con->conect);
		}
 }
 
function asignarRadicacionTipo($idr,$usu){
	if($this->con->conectar()==true){
			$sql="update aportes004 set asignado='S', usuarioasignado='$usu' WHERE idradicacion=$idr";
			//echo $sql;
			return mssql_query($sql,$this->con->conect);
		}
 } 
 
 function cerrarRadicacion($idr,$usuario){
	if($this->con->conectar()==true){
			$sql="update aportes004 set procesado='S', fechaproceso=cast(getdate() as date),recibegrabacion='$usuario' where idradicacion=$idr";
			return mssql_query($sql,$this->con->conect);
		}
 } 
 
function anularRadicacion($campos){
	if($this->con->conectar()==true){
		$sql="INSERT INTO aportes004 (fecharadicacion, idtipodocumento, identificacion, idtiporadicacion, horainicio,horafinal,notas,fechasistema, usuario, idagencia, procesado, fechaproceso, devuelto, motivodevolucion) VALUES (cast(getdate() as date),'".$campos[0]."','".$campos[1]."','".$campos[2]."','".$campos[4]."','".$campos[5]."','".$campos[9]."', cast(getdate() as date),'".$campos[7]."','".$campos[8]."','S',cast(getdate() as date),'S',".$campos[6].")";
		return mssql_query($sql,$this->con->conect);
	}
 } 
 
function borrarTemporales($ide,$idp){
	if($this->con->conectar()==true){
		$sql="DELETE FROM aportes016 WHERE idempresa=$ide AND idpersona=$idp AND estado='P' AND idradicacion IS NULL";
		return mssql_query($sql,$this->con->conect);
	}
 } 
 
	function borrarRadicacionError($idr){
 		if($this->con->conectar()==true){
 			$sql="DELETE FROM aportes004 WHERE idradicacion=$idr";
 			return mssql_query($sql,$this->con->conect);
 		}
	} 
	
	function anularRadicacionError($idr){
		if($this->con->conectar()==true){
			$sql="UPDATE aportes004 SET asignado='S', procesado='S', anulada='S', usuarioanula=usuario, devuelto='S' WHERE idradicacion=$idr";
			return mssql_query($sql,$this->con->conect);
		}
	}
 
}	
?>