/*
* @autor:      Ing. Orlando Puentes
* @fecha:      06/10/2010
* objetivo:
*/
$(function() {
		$("#imagen").dialog({
		 	autoOpen: false,
			modal: true,
		    hide: 'slide',
		    show:'fade',
		    width:900,
		    height:700,
 });
 });

$(document).ready(function(){

//...DIALOG PENDIENTE PROFESION POR ABRIR...//
	   	$("#profesion2").change(function(){
    	if($(this).val()=='Otro..'){
        $('#profesionDialog').dialog('open');
        }//end if
		});//fin change


//...ABRIR DIALOG PROFESION...//

$("#profesionDialog").dialog({
	autoOpen:false,
	modal: true,
	width: 400,
	buttons: {'Grabar': function() {
		$(this).dialog("close");
		//alert($("#nuevaProf").val());
		var newOption= $("#nuevaProf").val();
		var iddefinicion=20;
		var codigo=$('#profesion2 option').length;
		var detalledefinicion=$("#nuevaProf").val();
		var concepto=$("#nuevaProf").val();
		$.post(URL+'phpComunes/nuevoDetalle.php',{v0:iddefinicion,v1:codigo,v2:detalledefinicion,v3:concepto},
		function(datos){
			if(datos==1){
				alert("La profesion no se pudo guardar!, intente de nuevo");
				}
			else{
				alert("La profesion:  "+ detalledefinicion +" se grab\u00F3");
				}
			});

		$.post(URL+"phpComunes/barrios.php", {v0:20,v1:1}, function(data){
				$("#profesion2").html(data);
				$('#profesion2 option:last').after("<option value='otro'>Otro..</option>");
				//$('#barrio option:fist').before("<option value=0>Seleccione..</option>");
				$("#profesionDialog").dialog("close");
		        $("#profesion2").trigger("change");
		});
		}
		}//end boton
		});

//...DIALOG BARRIO...//
	   	$("#barrio2").change(function(){
    	if($(this).val()=='Otro..'){
        $('#barrioDialog').dialog('open');
        }//end if
		});//fin change

// nuevo barrio
$("#barrioDialog").dialog({
	autoOpen:false,
	modal: true,
	width: 400,
	buttons: {'Grabar': function() {
		$(this).dialog("close");
		if($("#nuevoBarrio").val()==""){
			return false;
			}
		var newOption= $("#nuevoBarrio").val();
		var iddefinicion=19;
		var codigo=$('#barrio2 option').length;
		var detalledefinicion=$("#nuevoBarrio").val();
		var concepto=$("#nuevoBarrio").val();
		$.post(URL+'phpComunes/nuevoDetalle.php',{v0:iddefinicion,v1:codigo,v2:detalledefinicion,v3:concepto},
		function(datos){
			if(datos==1){
				alert("El barrio no se pudo guardar!, intente de nuevo");
				}
			else{
				alert("El barrio "+ detalledefinicion +" se grab\u00F3");

				}
			});

		$.post(URL+"phpComunes/barrios.php", {v0:19,v1:1}, function(data){
				$("#barrio2").html(data);
				$('#barrio2 option:last').after("<option value='otro'>Otro..</option>");
				//$('#barrio option:fist').before("<option value=0>Seleccione..</option>");

		});

		$("#barrio2").val('Seleccione..');
		}
		}//end boton
		});

	//-------------------------VALIDACION FECHA BENEFICIARIOS

		$("#fecNac2,#fecNac3").blur(function(){



								if($(this).val()=='')
								{
									$(this).val('mmddaaaa');
								}else {
								 fecha=$(this).val();
								 mes=fecha.slice(0,2);
								 mesTemp=mes;
								 dia=fecha.slice(2,4);
								 anio=fecha.slice(4,8);
								 indexMes=mes.slice(0,1);
								 anioActual=new Date();
								 hoy=anioActual.getFullYear();
								 edad=parseInt(hoy)- parseInt(anio);

								 if(fecha.length<8){

									alert("La fecha debe tener 8 digitos");
									$(this).focus();
								 }else{

									if(indexMes < 0 || indexMes > 1 || mes >12|| mes <1){

										 alert("El mes esta mal digitado");
										 $(this).focus();
									 }else{

									    if(dia>31|| dia <1){
										alert("El d\u00EDa debe tener de 1 a 31 dias");
											$(this).focus();
										}else{
									          if(anio<1900 || anio>anioActual.getFullYear()){

												  alert("El a\u00F1o esta incorrecto");
												  $(this).focus();
											  }else{


								 switch(mes){
									case '01':mes='Ene';
									          break;
									case '02':mes='Feb';
									          break;
									case '03':mes='Mar';
									          break;
									case '04':mes='Abr';
									          break;
									case '05':mes='May';
									          break;
									case '06':mes='Jun';
									          break;
									case '07':mes='Jul';
									          break;
									case '08':mes='Ago';
									          break;
									case '09':mes='Sep';
									          break;
									case '10':mes='Oct';
									          break;
									case '11':mes='Nov';
									          break;
									default:mes='Dic';
									         break;

								 }//end switch
								      //alert(mes+"-"+dia+"-"+anio);
									  $(this).val(mes+", "+dia+" de "+anio);
									  fecTemp=mesTemp+"/"+dia+"/"+anio;
									  //alert("Valor a guardar:"+fecTemp);
									  $(this).siblings(":hidden").attr("value",fecTemp);



									 }//en else
								   }//end else
								 }//end else
							  }//end else
							}//else principal
							}).focus(function(){

								  if($("#fecNac3").val()!=''){
									   $(this).val(mesTemp+dia+anio);
								  }else{
									  $(this).val('');
								  }



								});//end blur y focus


//-------------------SELECCIONAR PARENTESCO EN TABLA BENEFICIARIOS

$("#parentesco").change(function(){

	  var opcion=$(this).val();
	  //...MOSTRARA COMBO Y CAMPO DE CEDULA DE LA CONYUGE O MADRE ...//
	  $("#cedMama").removeClass("error");
	  $("#cedMama,#progenitor,#imgMama").show();
	  $("#cedMama").parent("td").prev().html('C&eacute;dula Progenitor');
	  $("#progenitor").val('');
	  $("#cedMama").val('');
	//cod 36= PADRE/MADRE cod 37 HERMANO
	if(opcion=="36"||opcion=="37"||opcion=='99'){

		$("#cedMama,#progenitor,#imgMama").hide();
		$("#cedMama").parent("td").prev().html('');
		}

	});//end change

//DEVOLUCION DE DOCUMENTOS
$("#devolucion").click(function(){
	   
	   var radicacion=$("#txtId").val();var fechaRad=$("#txtId2").val();
	   var cedula=$("#textfield22").val();var tipoDoc=$("#select").val();
 	   var pnombre=$("#textfield1").val();var snombre=$("#textfield3").val();
	   var papellido=$("#textfield2").val();var sapellido=$("#textfield4").val();
       var tipoRad=$("#select2").val();var presentacion=$("#select3").val();
	   var horaInicio=$("#textfield7").val();var horaFinal=$("#textfield8").val();
       var usuario=$("#textfield11").val();
	   validarCampos(1);
	   alert("Envio preparado en linea:245");
	   
	  //ENVIO A DEVOLUCION.PHP
	  /*
	   $.post('',{v0:radicacion,v1:fechaRad,v2:cedula,v3:tipoDoc,v4:pnombre,v5:snombre,v6:papellido,v7:sapellido,v8:tipoRad,v9:presentacion,v10:horaInicio,v11:horaFinal,v12:usuario},function(data){
		   
		   });*/
	   
});//end click devolucion


});//end ready
// JavaScript Document
// autor Orlando Puentes A
// fecha marzo 14 de 2010
// objeto administraci\u00F3n de los eventos del formulario de definiciones.php
var nuevo=0;
var modificar=0;
var continuar=true;
var paso=null;
var oktarjeta=0;
var ide=0;
var URL=src();
var existe=0;
var idnb;
var idcony=0;

function validarCampos(op){
	if(!continuar){
		alert("La informaci\u00F3n esta incompleta!");
		return false;
	}
	if(nuevo==0){
		alert('Haga click primero en Nuevo!');
		return false;
		}
	var cadena="<ul>";
	var num=0;
	document.forms[0].elements[11].value = hora();
	if(document.forms[0].elements[2].value.length==0){
		cadena+="<li>Falta n\u00FAmero de identificaci\u00F3n</li>";
	}
	else{
		num=parseInt(document.forms[0].elements[2].value);
		if(isNaN(num))
		cadena+="<li>La identificacion es nUm\u00E9rica!</li>";
		var long=document.forms[0].elements[2].value.length;
		if(long<5 || long>10)
		cadena+="<li>La identificaci\u00F3n es de 5 a 10 d\u00EDgitos</li>";
	}
	if(document.forms[0].elements[3].value==0){
		cadena+="<li>Falta tipo de identificaci\u00F3n</li>";
	}
	if(document.forms[0].elements[4].value.length==0){
		cadena+="<li>Falta Primer nombre</li>";
	}
	else{
		if(document.forms[0].elements[4].value.length<3)
		cadena+="<li>El primer nombre es muy corto!</li>";
	}
	if(document.forms[0].elements[6].value.length==0){
		cadena+="<li>Falta Primer apellido</li>";
	}
	else{
		if(document.forms[0].elements[6].value.length<3)
		cadena+="<li>El primer apellido es muy corto!</li>";
	}
	if(document.forms[0].elements[8].value==0){
		cadena+="<li>Falta Tipo de radicaci\u00F3n</li>";
	}

	var radica=parseInt(document.forms[0].elements[8].value);
//*************************1
	
	switch(radica){
		/****** INFORMACION *****/
		case 27:
		if(document.getElementById('lTipoInf').value==0){
			cadena+="<li>Falta tipo de informaci\u00F3n";
		}
		break;
		/***** AFILIACION TRABAJADOR *****/
		case 28:
			if($('#sltTipoIdentificacion2').val() == '0'){
				cadena+="<li>Falta Tipo identificaci\u00F3n del trabajador</li>";
			}
		if(document.getElementById('tNumero2').value.length==0){
			cadena+="<li>Falta identificaci\u00F3n del trabajador</li>";
		}
		else{
			num=document.getElementById('tNumero2').value.length;
			if(num <5 || num>10)
			cadena+="<li>La identificacion es de 5 a 10 digitos";
			num=parseInt(document.getElementById('tNumero2').value);
			if(isNaN(num))
			cadena+="<li>La identificacion es n\u00FAmerica!</li>";
		}
		if(document.getElementById('tNit2').value.length==0){
			cadena+="<li>Falta NIT del empleador</li>";
		}
		else{
			num=document.getElementById('tNit2').value;
			if(isNaN(num))
			cadena+="<li>El NIT es n\u00FAmericO!</li>";
			if(num.length<5 || num.length>9 )
			cadena+="<li>El NIT es de 5 a 9 d\u00CDgitos";
		}
		if(document.getElementById('tFolios2').value.length==0){
			cadena+="<li>Falta n\u00FAmero de folios</li>";
		}
		else{
			num=document.getElementById('tFolios2').value;
			if(isNaN(num))
			cadena+="<li>El folio es n\u00FAerico!</li>";
			if(num==0)
			cadena+="<li>N\u00FAmero de folios mayor que 0</li>";
		}
		if($("#tipForm").val()=='Seleccione..'){
				cadena+="<li>Ingrese el tipo de formulario.</li>";

			}
		//VALIDACION DOCUMENTOS AFILIACION
		if($("#formulario").attr("checked")==false){
			cadena+="<li>Debe tener el formulario de afiliaci\u00F3n.</li>";
		}

		if($("#docCedula").attr("checked")==false){
			cadena+="<li>Necesario fotocopia de la cedula.</li>";
			}

		if($("input[name=rhijos]:checked").attr("value")=="s"){
			 if($("#check3").attr("checked")==false){
				 cadena+="<li>Necesario el registro civil del HIJO.</li>";
			 }
		}

		if($("input[name=rpadres]:checked").attr("value")=="s"){
			 if($("#check8").attr("checked")==false){
				 cadena+="<li>Fotocopia de la cedula del PADRE.</li>";
			 }
			if($("#check9").is(":visible")){
			if($("#check9").attr("checked")==false){
				 cadena+="<li>Registro civil trabajador</li>";
				 }
			}//end if visible check9
		}
		break;
		/*****  RENOVACION TRABAJADOR *****/
		case 29:
		if($("#sltTipoIdentificacion3").val()==0){
			cadena+="<li>Ingrese tipo  identificaci\u00F3n del trabajador</li>";
			
			}
		
		
		if(document.getElementById('tNumero3').value.length==0){
			cadena+="<li>Falta identificaci\u00F3n del trabajador</li>";
		}
		else{
			num=document.getElementById('tNumero3').value.length;
			if(num <5 || num>10)
			cadena+="<li>La identificacion es de 5 a 10 d\u00CDgitos";
			num=parseInt(document.getElementById('tNumero3').value);
			if(isNaN(num))
			cadena+="<li>La identificacion es n\u00FAmerica!</li>";
		}
		if(document.getElementById('tNit3').value.length==0){
			cadena+="<li>Falta NIT del empleador</li>";
		}
		else{
			num=document.getElementById('tNit3').value;
			if(isNaN(num))
			cadena+="<li>El NIT es n\u00FAmericO!</li>";
			if(num.length<5 || num.length>9 )
			cadena+="<li>El NIT es de 5 a 9 d\u00CDgitos";
		}
		if(document.getElementById('tFolios3').value.length==0){
			cadena+="<li>Falta n\u00FAmero de folios</li>";
		}
		else{
			num=document.getElementById('tFolios3').value;
			if(isNaN(num))
			cadena+="<li>El folio es n\u00FAmerico!</li>";
			if(num==0)
			cadena+="<li>N\u00FAmero de folios mayor que 0</li>";
		}
			if($("#tipForm2").val()=='Seleccione..'){
				cadena+="<li>Ingrese el tipo de formulario.</li>";

			}
		break;
		case 30:
		/***** AFILICION EMPRESA ****/
		if(document.getElementById('tNit4').value.length==0){
			cadena+="<li>Falta NIT del empleador</li>";
		}
		else{
			num=document.getElementById('tNit4').value;
			if(isNaN(num))
			cadena+="<li>El NIT es n\u00FAmerico!</li>";
			if(num.length<5 || num.length>9 )
			cadena+="<li>El NIT es de 5 a 9 d\u00CDgitos";
		}
		if(document.getElementById('tFolios4').value.length==0){
			cadena+="<li>Falta n\u00FAmero de folios</li>";
		}
		else{
			num=document.getElementById('tFolios4').value;
			if(isNaN(num))
			cadena+="<li>El folio es n\u00FAmerico!</li>";
			if(num==0)
			cadena+="<li>N\u00FAmero de folios mayor que 0</li>";
		}
		break;
		case 31:
		/***** RENOVACION EMPRESA ****/
		if(document.getElementById('tNit5').value.length==0){
			cadena+="<li>Falta NIT del empleador</li>";
		}
		else{
			num=document.getElementById('tNit5').value;
			if(isNaN(num))
			cadena+="<li>El NIT es n\u00FAmerico!</li>";
			if(num.length<5 || num.length>9 )
			cadena+="<li>El NIT es de 5 a 9 d\u00CDgitos";
		}
		if(document.getElementById('tFolios5').value.length==0){
			cadena+="<li>Falta n\u00FAmero de folios</li>";
		}
		else{
			num=document.getElementById('tFolios5').value;
			if(isNaN(num))
			cadena+="<li>El folio es n\u00FAmerico!</li>";
			if(num==0)
			cadena+="<li>N\u00FAmero de folios mayor que 0</li>";
		}
		break;
		case 32:
		/***** CERTIFICADO ****/
		if($('#sltTipoIdentificacion6').val() == '0'){
				cadena+="<li>Falta Tipo identificaci\u00F3n del trabajador</li>";
			}
		if(document.getElementById('tNumero6').value.length==0){
			cadena+="<li>Falta identificaci\u00F3n del trabajador</li>";
		}
		if(document.getElementById('lisBeneficiarios').value==0){
			cadena+="<li>Falta identificaci\u00F3n del beneficiario</li>";
		}
		if(document.getElementById('lTipoCer').value==0){
			cadena+="<li>Falta tipo de certificado</li>";
		}
		if(document.getElementById('tFolios6').value.length==0){
			cadena+="<li>Falta n\u00FAmero de folios</li>";
		}
		else{
			num=document.getElementById('tFolios6').value;
			if(isNaN(num))
			cadena+="<li>El folio es n\u00FAmerico!</li>";
			if(num==0)
			cadena+="<li>N\u00FAmero de folios mayor que 0</li>";
		}
		break;
		case 33:
		/***** CERTIFICADO ****/
		if(document.getElementById('tNumero7').value.length==0){
			cadena+="<li>Falta identificaci\u00F3n del trabajador</li>";
		}
		break;
		case 69:
		/*****  AFILIACION A FONEDE  ****/
		if($('#sltTipoIdentificacion8').val() == '0'){
				cadena+="<li>Falta Tipo identificaci\u00F3n del postulante</li>";
		}
		if(document.getElementById('txtIdentificacion').value.length==0){
			cadena+="<li>Falta identificaci\u00F3n del postulante</li>";
		}else{
			num=document.getElementById('txtIdentificacion').value.length;
			if(num <3 || num>10)
			cadena+="<li>La identificacion es de 3 a 11 d\u00CDgitos";
			num=parseInt(document.getElementById('txtIdentificacion').value);
			if(isNaN(num))
			cadena+="<li>La identificacion es n\u00FAmerica!</li>";
		}
		if(document.getElementById('tFolios8').value.length==0){
			cadena+="<li>Falta n\u00FAmero de folios</li>";
		}else{
			num=document.getElementById('tFolios8').value;
			if(isNaN(num))
			cadena+="<li>El folio es n\u00FAmerico!</li>";
			if(num==0)
			cadena+="<li>N\u00FAmero de folios mayor que 0</li>";
		}
		break;
		case 70:
		if(document.getElementById('tNumero9').value.length==0){
			cadena+="<li>Falta identificaci\u00F3n del trabajador</li>";
		}
		if(document.getElementById('tFolios9').value.length==0){
			cadena+="<li>Falta n\u00FAmero de folios</li>";
		}
		else{
			num=document.getElementById('tFolios9').value;
			if(isNaN(num))
			cadena+="<li>El folio es n\u00FAmerico!</li>";
			if(num==0)
			cadena+="<li>N\u00FAmero de folios mayor que 0</li>";
		}
		  //...BENEFICIARIOS
			    var tipoDoc3=$("#tipoDoc3").val();
		    	var cedula5=$("#identificacion5").val();
			    var pNombre3=$("#pNombre3").val();
			    var pApellido3=$("#pApellido3").val();
				var sNombre3=$("#sNombre3").val();//no obligado
			    var sApellido3=$("#sApellido3").val();//no obligado
			    var sexo3=$("#sexo3").val();
				var fecNac3=$("#fecNac3").val();
				var parentesco=$("#parentesco").val();
				var tipoafiliacion3=$("#tipoA").val();//no obligado
				var cedMama=$("#cedMama").val();//no obligado
		        var capacidad=$("#capacidad").val();



		  //---------------------TIPO DOC----------------------
			   if(tipoDoc3=='0'){
					cadena+='<li>Seleccione un tipo de documento en BENEFICIARIO.</li>';
					}
		    //--------------------CEDULA-----------------------
			   if(cedula5==''){
					cadena+='<li>Ingrese la identificaci\u00F3n del BENEFICIARIO.</li>';

				} else {
                         if(cedula5.length<3){
					     cadena+='<li>La identificaci\u00F3n NO debe tener 2 d\u00EDgitos.</li>';

				         }else {
							 if(cedula5.length==9){
					           cadena+='<li>La identificaci\u00F3n NO debe tener 9 d\u00EDgitos.</li>';
							    }

						 }//end else
				  }//end else
		    //----------------PRIMER NOMBRE Y APELLIDO---------------------------
			   if(pNombre3==''){
					cadena+='<li>Ingrese el NOMBRE del BENEFICIARIO.</li>';

				}

				 if(pApellido3==''){
					cadena+='<li>Ingrese el APELLIDO del BENEFICIARIO.</li>';

				}


			//-------------------SEXO-----------------------
			if(sexo3=='0'){
					cadena+='<li>Seleccione el sexo del BENEFICIARIO.</li>';

				}

			//----------------------FECHA DE NACIMIENTO 3---------------
			if(fecNac3==''||fecNac3=='mmddaaaa'){
				cadena+='<li>Escriba la fecha de nacimiento del BENEFICIARIO.</li>';

			}

			//-------------------PARENTEZCO-----------------------
			if(parentesco=='0'){
					cadena+='<li>Seleccione el parentesco.</li>';
			}

		//-------------------CAPACIDAD TRABAJO-----------------------
			if(capacidad=='0'){
					cadena+='<li>Seleccione la capacidad de trabajo del BENEFICIARIO.</li>';
			}

		break;
		//-----------------------VALIDACION CREAR CONVIVENCIA-CONYUGE
		case 211:
		     var tipRel=$("#tipRel").val();
			   var conviven=$("#conviven").val();
			   var tipoDoc2=$("#tipoDoc2").val();
			   var optionSel=$("#tipoDoc2").children("option:selected").html();//valor del texto del option
			   var cedula2=$("#identificacion7").val();
			   var pNombre2=$("#pNombre2").val();
			   var pApellido2=$("#pApellido2").val();
			   var sNombre2=$("#sNombre2").val();//no obligado
			   var sApellido2=$("#sApellido2").val();//no obligado
			   var sexo2=$("#sexo2").val();
			   var barrio2=$("#barrio2").val();
			   var direccion2=$("#direccion2").val();
			   var telefono2=$("#telefono2").val();
			   var celular2=$("#celular2").val();
			   var email2=/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test($("#email2").val());
			   var tipVivienda2=$("#tipoVivienda2").val();
			   var deptRes2=$("#combo1").val();
			   var ciudadRes2=$("#combo2").val();
			   var zonaRes2=$("#combo3").val();
			   var estadoCivil2=$("#estadoCivil2").val();
			   var depNac2=$("#combo4").val();//no obligado
			   var ciudadNac2=$("#combo5").val();//no obligado
			   var capTrabajo2=$("#capTrabajo2").val();//no obligado
			   var profesion2=$("#profesion2").val();//no obligado
			   var fecNac2=$("#fecNac2").val();


			if(tipRel=='0'){
					cadena+='<li>Seleccione un tipo de relacion.</li>';

				}

			//------------------------CONVIVEN----------------------
			if(conviven=='0'){
					cadena+='<li>Seleccione el tipo de convivencia.</li>';

				}


			//---------------------TIPO DOC----------------------
			   if(tipoDoc2=='0'){
					cadena+='<li>Seleccione un tipo de documento en CONYUGE.</li>';

				}
		    //--------------------CEDULA-----------------------
			   if(cedula2==''){
					cadena+='<li>Ingrese la identificaci\u00F3n del CONYUGE.</li>';

				} else {
                         if(cedula2.length<3){
					     cadena+='<li>La identificaci\u00F3n NO debe tener 2 d\u00EDgitos.</li>';

				         }else {
							 if(cedula2.length==9){
					           cadena+='<li>La identificaci\u00F3n NO debe tener 9 d\u00EDgitos.</li>';

				              }

						 }//end else
				  }//end else
		    //----------------PRIMER NOMBRE Y APELLIDO---------------------------
			   if(pNombre2==''){
					cadena+='<li>Ingrese el NOMBRE del CONYUGE.</li>';

				}

				 if(pApellido2==''){
					cadena+='<li>Ingrese el APELLIDO del CONYUGE.</li>';

				}


			//-------------------SEXO-----------------------
			if(sexo2=='0'){
					cadena+='<li>Seleccione el sexo del CONYUGE.</li>';

				}

		//-------------------DIRECCION-----------------------
			if(direccion2==''){
					cadena+='<li>Ingrese la direcci\u00F3n.</li>';

				}

			//-------------------TELEFONO----------------------
			    if(telefono2!=''){
					 if(telefono2.length<7){
						 cadena+='<li>Faltan d\u00EDgitos en el  TELEFONO.</li>';

					 }
				}
				
				if(celular2!=''){
				if(celular2.length<10){
					 cadena+='<li>Faltan d\u00EDgitos en el  CELULAR.</li>';
					}
				}
			//---------------------EMAIL------------------------

			if($("#email2").val()!=''){

			    if(!email2) {
							cadena+='<li>El E-mail aparentemente es incorrecto.</li>';


							}else{
								  email2=$("#email2").val();
								  }
			}
			//------------------------------------------------------

			/*if(deptRes2=='Seleccione..'){cadena+='<li>Seleccione el Departamento de residencia en CONYUGE.</li>';}
			if(ciudadRes2=='Seleccione..'){cadena+='<li>Seleccione la Ciudad de residencia en CONYUGE.</li>';}
			if(zonaRes2=='Seleccione..'){cadena+='<li>Seleccione la zona de residencia en CONYUGE.</li>';}*/


			//----------------------FECHA DE NACIMIENTO---------------
			if(fecNac2==''||fecNac2=='mmddaaaa'){
				cadena+='<li>Escriba la fecha de nacimiento del CONYUGE.</li>';

			}else{
				fecha2=fecNac2;
								 mes2=fecha2.slice(0,2);
								 mesTemp2=mes2;
								 dia2=fecha2.slice(2,4);
								 anio2=fecha2.slice(4,8);
								 indexMes2=mes2.slice(0,1);
								 anioActual2=new Date();
								 hoy2=anioActual2.getFullYear();
								 edad2=parseInt(hoy2)- parseInt(anio2);

				if(edad2<15){
					alert("La edad del CONYUGE debe ser mayor a 15 a\u00F1s");
					}
				
				}
		break;
		case 105:
		if(document.getElementById('tNit10').value.length==0){
			cadena+="<li>Falta NIT empresa principal</li>";
		}
		if(document.getElementById('tFolios10').value.length==0){
			cadena+="<li>Falta n\u00FAmero de folios</li>";
		}
		else{
			num=document.getElementById('tFolios10').value;
			if(isNaN(num))
			cadena+="<li>El folio es n\u00FAmerico!</li>";
			if(num==0)
			cadena+="<li>N\u00FAmero de folios mayor que 0</li>";
		}
		break;
		case 169:
		if($("#tNit11").val()==''){
			 cadena+="<li>Ingrese el Nit de la empresa.</li>";

		}

	   if(isNaN($("#tNit11").val() ) ){
		   cadena+="<li>El Nit de la empresa es n\u00FAmerico.</li>";

		  }

		  if($("#sltTipoIdentificacion").val()=='Seleccione...'){
			cadena+="<li>Ingrese el tipo de Identificaci\u00F3n.</li>";

		 }
		 if($("#tPNombre").val()==''){
			 cadena+="<li>Ingrese el Primer Nombre del trabajador.</li>";
		}

		 if($("#tPApellido").val()==''){
			 cadena+="<li>Ingrese el Primer Apellido del trabajador.</li>";
		}

		 if($("#causal1").val()=='0'){
			 cadena+="<li>Ingrese el Causal.</li>";
		}

		 if($("#periodoI1").val()==''){
		     cadena+="<li>Ingrese el periodo Inicial.</li>";
		 }else{
		     if($("#periodoI1").val().length<6){
		        cadena+="<li>El perido Inicial debe ser de 6 D\u00FAgitos.</li>";
		      }else{
		         var mes=$("#periodoI1").val().slice(-2);
				 var anio=$("#periodoI1").val().slice(0,4);
				 fecha=new Date();
				 anioActual=fecha.getFullYear();
			    if(mes>'12'||mes=='00'){
					cadena+="<li>El mes del perido Inicial NO es v\u00C1lido.</li>";
					}
				if(anio>anioActual){
					cadena+="<li>El a\u00F1o del perido Inicial NO debe ser mayor al a\u00F1o actual.</li>";
					}
			  }//end else
		  }//end else

		  if($("#periodoF1").val()==''){
		     cadena+="<li>Ingrese el periodo Final.</li>";
		 }else{
		     if($("#periodoF1").val().length<6){
		        cadena+="<li>El perido Final debe ser de 6 D\u00FAgitos.</li>";
		      }else{
		         var mes2=$("#periodoF1").val().slice(-2);
				 var anio2=$("#periodoF1").val().slice(0,4);
				 fecha2=new Date();
				 anioActual2=fecha2.getFullYear();
			    if(mes2>'12'||mes2=='00'){
					cadena+="<li>El mes del perido Final NO es v\u00C1lido.</li>";
					}
				if(parseInt($("#periodoF1").val())-parseInt($("#periodoI1").val() ) <0){

					cadena+="<li>El periodo Final debe ser mayor al periodo Inicial.</li>";
					}
			  }//end else
		  }//end else
		  break;
		  case 170:
		  	if($("#tNit12").val()==''){
			 cadena+="<li>Ingrese el Nit de la empresa.</li>";
			}

	      if(isNaN($("#tNit12").val() ) ){
		   cadena+="<li>El Nit de la empresa es n\u00FAmerico.</li>";
		  }

		 if($("#comboSucursal").val()=='0'){
			 cadena+="<li>Seleccione una sucursal.</li>";
		}

		 if($("#causal2").val()=='0'){
			 cadena+="<li>Ingrese el Causal.</li>";
		}

		 if($("#periodoI2").val()==''){
		     cadena+="<li>Ingrese el periodo Inicial.</li>";
		 }else{
		     if($("#periodoI2").val().length<6){
		        cadena+="<li>El perido Inicial debe ser de 6 D\u00FAgitos.</li>";
		      }else{
		         var mes=$("#periodoI2").val().slice(-2);
				 var anio=$("#periodoI2").val().slice(0,4);
				 fecha=new Date();
				 anioActual=fecha.getFullYear();
			    if(mes>'12'||mes=='00'){
					cadena+="<li>El mes del perido Inicial NO es v\u00C1lido.</li>";
					}
				if(anio>anioActual){
					cadena+="<li>El a\u00F1o del perido Inicial NO debe ser mayor al a\u00F1o actual.</li>";
					}
			  }//end else
		  }//end else

		  if($("#periodoF2").val()==''){
		     cadena+="<li>Ingrese el periodo Final.</li>";
		 }else{
		     if($("#periodoF2").val().length<6){
		        cadena+="<li>El perido Final debe ser de 6 D\u00FAgitos.</li>";
		      }else{
		         var mes2=$("#periodoF2").val().slice(-2);
				 var anio2=$("#periodoF2").val().slice(0,4);
				 fecha2=new Date();
				 anioActual2=fecha2.getFullYear();
			    if(mes2>'12'||mes2=='00'){
					cadena+="<li>El mes del perido Final NO es v\u00C1lido.</li>";
					}
				if(parseInt($("#periodoF2").val())-parseInt($("#periodoI2").val() ) <0){

					cadena+="<li>El periodo Final debe ser mayor al periodo Inicial.</li>";
					}
			  }//end else
		  }//end else
		  break;
		  //VALIDACION DISOLUCION CONVIVENCIA
		  case 191:
		  if($("#sltTipoIdentificacion14").val()==0){
			  cadena+="<li>Seleccione el tipo de identificacion..</li>";
			  }
			  
		  if($("#tNumero14").val()==""){
			  cadena+="<li>Ingrese identificacion trabajador.</li>";
			  }
		  break;
		  //VALIDACION REPOSICION TARJETAS
		 case 195:

		   var tipoDoc4=$("#tipoDoc16").val();
		   var cedula4=$("#identificacion4").val();
		   var motivo=$("#motivoR option:selected").text();
           var comprobante=$("#comprobante").val();
		   var numero=$("#numero16").val();

		  //---------------------TIPO DOC----------------------
			   if(tipoDoc4=='0'){
					cadena+='<li>Seleccione un tipo de documento en REPOSICION TARJETA.</li>';
					}
		    //--------------------CEDULA-----------------------
			   if(cedula4==''){
					cadena+='<li>Ingrese la identificaci\u00F3n en REPOSICION TARJETA.</li>';

				} else {
                         if(cedula4.length<3){
					     cadena+='<li>La identificaci\u00F3n NO debe tener 2 d\u00EDgitos en REPOSICION TARJETA.</li>';

				         }else {
							 if(cedula4.length==9){
					           cadena+='<li>La identificaci\u00F3n NO debe tener 9 d\u00EDgitos en REPOSICION TARJETA.</li>';
							    }

						 }//end else
				  }//end else
		   //------------------------------MOTIVO DE REPOSICION----------------
		   if(motivo=='Seleccione..'){
		     cadena+='<li>Seleccione el motivo de la reposici\u00F3n.</li>';

		   } else{
			      if(motivo=='Perdida/Robo'||motivo=='Deteriodo'){
					  
					  if(comprobante==''){
					   cadena+='<li>Ingrese el comprobante.</li>';
					  }
					  
					  if(numero==''){
					   cadena+='<li>Ingrese el Numero.</li>';
					  }
			   
			  
				  }}//end else
		   break;

	}//end switch
	if(cadena.length>10){
		cadena+="</ul>";
		cadena="Hay errores en los datos, por favor revisar:"+cadena;
		document.getElementById('error').innerHTML=cadena;
		return;
	}
	else{
		cadena="Datos completos!";
		document.getElementById('error').innerHTML=cadena;
	}
	if(op==1){
		if(document.getElementById('tExiste').value==0)
			guardarPersona();
		guardarRadicacion();

	}
	else{
		//actualizarDatos();
	}
}

function nuevoR(){
	nuevo=1;
	limpiarCampos();
	// TABLA DINAMICA PARA DATOS EMPRESA TRABAJDOR ACTIVO
	$("table[name='dinamicTable']").remove();
	document.forms[0].elements[2].focus();
	document.forms[0].elements[1].value = fechaHoy();
	document.forms[0].elements[10].value = hora();
	document.forms[0].elements[12].value=document.getElementById('usuario').value;
	document.getElementById('error').innerHTML="";
	ocultarDiv();
}

function buscarPersona(objeto){
	if(nuevo==0){
		alert("Si es un registro nuevo haga click en NUEVO!!");
		return;
	}
	var numero=parseInt(objeto.value);
	if(isNaN(numero)) return;
	$.ajax({
		url: '../phpComunes/buscarPersona.php',
		cache: false,
		type: "GET",
		data: "submit=&v0="+numero,
		success: function(datos){
			if(datos==0){
				alert("No existe en nuestra base, registre los datos completos!");
				document.getElementById('tExiste').value=0;
				document.forms[0].elements[3].value=0;
				document.forms[0].elements[4].value="";
				document.forms[0].elements[5].value="";
				document.forms[0].elements[6].value="";
				document.forms[0].elements[7].value="";
				return false;
			}
			var result=datos.split(",");
			document.forms[0].elements[4].value=result[5];
			document.forms[0].elements[5].value=result[6];
			document.forms[0].elements[6].value=result[3];
			document.forms[0].elements[7].value=result[4];
			document.forms[0].elements[3].value=result[1];
			document.getElementById('tExiste').value=1;
			document.getElementById('idPersona').value=result[0];
		}
	});
}

function contarPersonaAfiliada(objeto){
	var numero=parseInt(objeto.value);
	if(isNaN(numero)) return;
	$.ajax({
		url: '../phpComunes/contarAfiliaciones.php',
		cache: false,
		type: "GET",
		data: "submit=&v0="+numero,
		success: function(datos){
			if(datos>0){
				alert("Existe por lo menos una afiliaci\u00F3n bajo ese n\u00FAmero de identificaci\u00F3n, cambie el tipo de radicaci\u00F3n!");
				document.forms[0].elements[8].value=0;
				document.forms[0].elements[8].focus();
				document.getElementById('tNumero2').value="";
				$('#div2').css({display:"none"});
				continuar =false;
				return false;
			}
			else{
				continuar=true;}
		}
	});
}

function buscarPersonaAfiliada(objeto){
var numero=parseInt(objeto.value);
if(isNaN(numero)) return;
$.get(URL+'phpComunes/buscarPersonaAfiliada.php',{v0:numero},function(datos){
    if(datos==0){
    	alert("El n\u00FAmero de identificaci\u00F3n no existe como afiliado a COMFAMILIAR!");
	objeto.value="";
	  $("#div17 input:text").val('');
	  $("#div17  select").val('0');
	   $("table[name='dinamicTableC']").remove();
	 objeto.focus();
	continuar =false;
	return false;
    }
    else{
        var result=datos.split(",");
	var nombre=result[3]+" "+result[4]+" "+result[5]+" "+result[6];
	$('#nombreT').html(nombre);
	$('#nombreT2').html(nombre);//Div 14 disolucion T
	$('#tNombre3').val(nombre);
	document.getElementById('tNombre6').value=nombre;
	document.getElementById('tNombre9').value=nombre;
        $('#tNombre17').val(nombre);
	document.getElementById('idPersona').value=result[1];
	$('#tPApellido').val(result[3]);
	$('#tSApellido').val(result[4]);
	$('#tPNombre').val(result[5]);
	$('#tSNombre').val(result[6]);
	continuar=true;
    }
    if($('#select2').val()==183){
            var idp=$('#idPersona').val();
            var nom=$('#nombreT').html();
            $('#comboFallecido option').remove();
            $('#comboFallecido').append("<option value=0 selected=selected>Seleccione</option>");
            $('#comboBeneficiario option').remove();
            $('#comboBeneficiario').append("<option value=0 selected=selected>Seleccione</option>");
            $('#comboBeneficiario').append("<option value="+idp+">"+nom+"</option>");
            $.getJSON(URL+'phpComunes/buscarGrupoSimpleId.php',{idp:idp},function(datos2){
                $.each(datos2,function(i, fila){
		var nom = fila.pnombre+" "+fila.snombre+" "+fila.papellido+" "+fila.sapellido;
		$('#comboBeneficiario').append("<option value="+fila.idbeneficiario+">"+nom+"</option>");
		});
            $('#comboBeneficiario').append("<option>Otro</option>");
	});
	}
    if($('#select2').val()==191){
        var idp=$('#idPersona').val();
	var idc=0;
	$('#gfami tr:not(:first)').remove();
	$.getJSON(URL+'phpComunes/buscarConvivencia.php',{v0:idp},function(datos3){
            if(datos3==0){
            alert("El trabajador no tiene relaci\u00F3n de convivencia");
            return false;
	}
	$.each(datos3,function(i,fila){
            idc = fila.idconyuge;
            $("#idConyuge").val(idc);
            var nom=fila.pnombre+" "+fila.snombre+" "+fila.papellido+" "+fila.sapellido;
            $("#gfami").append("<tr><td>"+fila.identificacion+"</td><td>"+nom+"</td><td>"+fila.detalledefinicion+"</td><td>"+fila.conviven+"</td><td>"+fila.idconyuge+"</td></tr>");
	});
	$.getJSON(URL+'phpComunes/buscarHijosConviven.php',{v0:idp,v1:idc},function(datos4){
	if(datos4==0){
            return false;
	}
	$.each(datos4,function(i,fila){
            var nom=fila.pnombre+" "+fila.snombre+" "+fila.papellido+" "+fila.sapellido;
            $("#gfami").append("<tr><td>"+fila.identificacion+"</td><td>"+nom+"</td><td>"+fila.detalledefinicion+"</td><td>"+fila.conviven+"</td><td>"+fila.idconyuge+"</td></tr>");
	});
	});
	});
	}
    if($('#select2').val()==211){
        buscarAfiliacion();
        buscarConvivencia();
    }
	});
}//end function buscarpersona
 function buscarBeneficiarios(){
       
	var numero=document.getElementById('idPersona').value;
	$.getJSON(URL+'phpComunes/buscarGrupoSimpleId.php',{idp:numero},function(data){
			$('#lisBeneficiarios option').remove();
			$.each(data,function(i, fila){
			var nom = fila.identificacion+" "+fila.pnombre+" "+fila.snombre+" "+fila.papellido+" "+fila.sapellido;
			$('#lisBeneficiarios').append("<option value="+fila.idbeneficiario+">"+nom+"</option>");
			});
			});
	}

function buscarBeneficiarioBORRAR(objeto){

	var idpersona=document.getElementById('idPersona').value;
	if(isNaN(numero)) return;
	$.ajax({
		url: '../phpComunes/buscarBeneficiario.php',
		cache: false,
		type: "GET",
		data: "submit=&v0="+numero+"&v1="+idpersona,
		success: function(datos){
			if(datos==0){
				alert("El n\u00FAmero de identificaci\u00F3n no existe como beneficiario de COMFAMILIAR!");
				document.getElementById('tBene6').value="";
				document.getElementById('tBene6').focus();
				continuar =false;
				return false;
			}
			else{
				var result=datos.split(",");
				var nombre=result[3]+" "+result[4]+" "+result[5]+" "+result[6];
				document.getElementById('tNombreB6').value=nombre;
				continuar=true;}
		}
	});
}

function buscarNit(objeto){
	var numero=parseInt(objeto.value);
	if(isNaN(numero)) return;
	$.ajax({
		url: '../phpComunes/buscarNIT.php',
		cache: false,
		type: "GET",
		data: "submit=&v0="+numero,
		success: function(datos){
			if(datos==0){
				alert("No existe el NIT en nuestra base de datos, verifique el n\u00FAmero!");
				document.getElementById('tRazon2').value="";
				document.getElementById('tRazon3').value="";
				document.getElementById('tRazon10').value="";
				document.getElementById('tRazon11').value="";
				document.getElementById('tRazon12').value="";
				document.getElementById('tRazon15').value="";
				continuar=false;
				objeto.value="";
				objeto.focus();
				return false;
			}
			var result=datos.split(",");
			if(result[27]=='I'){
				alert("El NIT esta inactivo!, verifique el n\u00FAmero o active la empresa");
				continuar=false;
				objeto.value="";
				objeto.focus();
				return false;
			}
			document.getElementById('tRazon2').value=result[6];
			document.getElementById('tRazon3').value=result[6];
			document.getElementById('tRazon5').value=result[6];
			document.getElementById('tRazon10').value=result[6];
			document.getElementById('tRazon11').value=result[6];
			document.getElementById('tRazon12').value=result[6];
			document.getElementById('tRazon15').value=result[6];
			document.getElementById('idEmpresa').value=result[0];
			continuar=true;
		}
	});
}

function contarNits(objeto){
	var numero=parseInt(objeto.value);
	if(isNaN(numero)) return;
	$.ajax({
		url: '../phpComunes/contarNits.php',
		cache: false,
		type: "GET",
		data: "submit=&v0="+numero,
		success: function(datos){
			if(datos>0){
				alert("Existe por lo menos una Empresa bajo ese n\u00FAmero de NIT, cambie el tipo de radicaci\u00F3n!");
				document.forms[0].elements[8].value=0;
				document.forms[0].elements[8].focus();
				document.getElementById('tNit4').value="";
				$('#div4').css({display:"none"});
				continuar =false;
				return false;
			}
			else{
				continuar=true;}
		}
	});
}


function buscarTarjeta(objeto){
	var numero=parseInt(objeto.value);
	if(isNaN(numero)) return;
	$.ajax({
		url: '../phpComunes/buscarTarjeta.php',
		cache: false,
		type: "GET",
		data: "submit=&v0="+numero,
		success: function(datos){
			if(datos==0){
				alert("El n\u00FAmero de identificaci\u00F3n no tiene tarjeta asignada!");
				objeto.value="";
				objeto.focus();
				continuar =false;
				return false;
			}
			else{
				var result=datos.split(",");
				var nombre=result[0]+" "+result[1]+" "+result[2]+" "+result[3];
				document.getElementById('tNombre7').value=nombre;
				document.getElementById('idT').value=result[5];
				ide = parseInt(result[22]);
				switch(ide){
					case 61 :var msg=result[23];break;
					case 62 :var msg=result[23]+" "+result[13]+" a\u00FAn no esta disponible para entregar!";break;
					case 63 :var msg=result[23]+" "+result[14]+" PUEDE SER ENTREGADA!";break;
					case 64 :var msg=result[23];break;
					case 65 :var msg=result[23]+" "+result[15];break;
					case 66 :var msg=result[23];break;
					case 67 :var msg=result[23];break;
					case 68 :var msg=result[23];break;
				}
				$("#notasTarjeta").html(msg);
				continuar=true;}
		}
	});
}

function guardarPersona(){
	var campo0 =document.forms[0].elements[2].value;
	var campo1 =document.forms[0].elements[3].value;
	var campo2 =document.forms[0].elements[4].value;
	var campo3 =document.forms[0].elements[5].value;
	var campo4 =document.forms[0].elements[6].value;
	var campo5 =document.forms[0].elements[7].value;
	var campo6 =document.forms[0].elements[12].value;
	$.ajax({
		url: 'personaG.php',
		type: "POST",
		data: "submit=&v0="+campo0+"&v1="+campo1+"&v2="+campo2+"&v3="+campo3+"&v4="+campo4+"&v5="+campo5+"&v6="+campo6,
		success: function(datos){
			alert(datos);
		}
	});
	return false;
}

function guardarRadicacion(){
	var campo0=document.forms[0].elements[0].value;
	var campo1=document.forms[0].elements[1].value;
	var campo2=document.forms[0].elements[2].value;
	var campo3=document.forms[0].elements[3].value;
	var campo4=parseInt(document.forms[0].elements[8].value);
	var campo5=document.forms[0].elements[9].value;
	var campo6=document.forms[0].elements[10].value;
	var campo7=document.forms[0].elements[11].value;
	var campo17=document.forms[0].elements[12].value;

	switch(campo4){
		case 27:
		var campo9 =document.getElementById('notas1').value;
		var campo10 =document.getElementById('lTipoInf').value;
		var campo11 ="";
		var campo12="";
		var campo13 ="";
		var campo14="";
		var campo15 ="";
		var campo16="";
		var campo18=0;
		var campo19="";
		break;
//aqui voy		
		case 28:
		var campo9 =document.getElementById('notas2').value;
		var campo10 ="";
		var campo11 =document.getElementById('tNumero2').value;
		var campo12 =document.getElementById('tNit2').value;
		var campo13=document.getElementById('tFolios2').value;
		var campo14="";
		var campo15 ="";
		var campo16="";
		var campo18=$('#sltTipoIdentificacion2').val();
		var campo19=$("#tipForm").val();
		$("input:check").attr("checked",false);
		break;
		case 29:
		var campo9 =document.getElementById('notas3').value;
		var campo10 ="";
		var campo11 =document.getElementById('tNumero3').value;
		var campo12 =document.getElementById('tNit3').value;
		var campo13=document.getElementById('tFolios3').value;
		var campo14="";
		var campo15 ="";
		var campo16="";
		var campo18=$('#sltTipoIdentificacion3').val();
		var campo19=$("#tipForm2").val();
		break;
		case 30:
		var campo9 =document.getElementById('notas4').value;
		var campo10="";
		var campo11 ="";
		var campo12 =document.getElementById('tNit4').value;
		var campo13 =document.getElementById('tFolios4').value;
		var campo14="";
		var campo15 ="";
		var campo16="";
		var campo18=0;
		var campo19="";
		break;
		case 31:
		var campo9 =document.getElementById('notas5').value;
		var campo10="";
		var campo11 ="";
		var campo12 =document.getElementById('tNit5').value;
		var campo13 =document.getElementById('tFolios5').value;
		var campo14="";
		var campo15 ="";
		var campo16="";
		var campo18=0;
		var campo19="";
		break;
		case 32:
		var campo9 =document.getElementById('notas6').value;
		var campo10="";
		var campo11 =document.getElementById('tNumero6').value;
		var campo12 ="";
		var campo13 =document.getElementById('tFolios6').value;
		var campo14=document.getElementById('lTipoCer').value;
		var campo15 =document.getElementById('lisBeneficiarios').value;
		var campo16="";
		var campo18=$('#sltTipoIdentificacion6').val();
		var campo19="";
		break;
		case 33:
		var campo9 =document.getElementById('notas7').value;
		var campo10="";
		var campo11 =document.getElementById('tNumero7').value;
		var campo12 ="";
		var campo13 ="";
		var campo14="";
		var campo15 ="";
		var campo16=$("#notasTarjeta").html();
		var campo18=0;
		var campo19="";
		break;
		case 69:
		var campo9 =document.getElementById('notas8').value;
		var campo10="";
		//id postulante
		var campo11 =document.getElementById('txtIdentificacion').value;
		var campo12 ="";
		var campo13 =document.getElementById('tFolios8').value;
		var campo14="";
		var campo15 ="";
		var campo16="";
		var campo18=$('#sltTipoIdentificacion8').val();
		var campo19="";
		break;
		case 70:
		var campo9 =document.getElementById('notas9').value;
		var campo10="";
		var campo11 =document.getElementById('tNumero9').value;
		var campo12 ="";
		var campo13 ="";
		var campo14="";
		var campo15 ="";
		var campo16="";
		var campo18=0;
		var campo19="";
		break;
		case 105:
		var campo9 =document.getElementById('notas10').value;
		var campo10="";
		var campo11 ="";
		var campo12 =document.getElementById('tNit10').value;
		var campo13 =document.getElementById('tFolios10').value;
		var campo14="";
		var campo15 ="";
		var campo16="";
		var campo18=0;
		var campo19="";
		break;
		case 169 :
			var campo9 =document.getElementById('notas11').value;
			var campo10="";
			var campo11 ="";
			var campo12 =document.getElementById('tNit11').value;
			var campo13 ="";
			var campo14="";
			var campo15 ="";
			var campo16="";
			var campo18=0;
			var campo19="";
		break;
		case 170 :
			var campo9 =document.getElementById('notas12').value;
			var campo10="";
			var campo11 ="";
			var campo12 =document.getElementById('tNit12').value;
			var campo13 ="";
			var campo14="";
			var campo15 ="";
			var campo16="";
			var campo18=0;
			var campo19="";
		break;
		case 183 :
			var campo9 =document.getElementById('notas13').value;
			var campo10="";
			var campo11 =document.getElementById('tNumero13').value;
			var campo12 ="";
			var campo13 ="";
			var campo14= $('#comboQuien').val();
			var campo15 =$('#comboFallecido').val();
			var campo16="";
			var campo18=$('#sltTipoIdentificacion13').val();
			var campo19="";
		break;
		case 191 :
			if(confirm("Esta seguro de disolver la relaci\u00F3n de convivencia?")==true){
				var campo9 =document.getElementById('notas14').value;
				var campo10="";
				var campo11 =document.getElementById('tNumero14').value;
				var campo12 ="";
				var campo13 ="";
				var campo14= "";
				var campo15 ="";
				var campo16="";
				var campo18=$('#sltTipoIdentificacion14').val();
				var campo19="";
				}
			else
			{
				limpiarCampos();
				ocultarDiv();
				return false;
				}
        break;
		case 192 : 	// afiliaciones multiples
			var campo9 =document.getElementById('notas15').value;
			var campo10="";
			var campo11 =document.getElementById('tNumero15').value;
			var campo12 =document.getElementById('tNit15').value;
			var campo13 =document.getElementById('tFolios15').value;
			var campo14="";
			var campo15 ="";
			var campo16="";
			var campo18=0;
			var campo19="";
		break;
        case 211:
            var campo9  ="";
            var campo10 ="";
            var campo11 ="";
            var campo12 ="";
            var campo13 ="";
            var campo14 ="";
            var campo15 ="";
            var campo16 ="";
            var campo18 ="";
            var campo19 ="";
        break;
		case 195 :
			 var campo9  ="";
             var campo10 ="";
             var campo11 ="";
             var campo12 ="";
             var campo13 ="";
             var campo14 ="";
             var campo15 ="";
             var campo16 ="";
             var campo18 ="";
             var campo19 ="";
        break;       
    }
	$.ajax({
		url: 'radicacionG.php',
		type: "POST",
		data: "submit=&v0="+campo0+"&v1="+campo1+"&v2="+campo2+"&v3="+campo3+"&v4="+campo4+"&v5="+campo5+"&v6="+campo6+"&v7="+campo7+"&v9="+campo9+"&v10="+campo10+"&v11="+campo11+"&v12="+campo12+"&v13="+campo13+"&v14="+campo14+"&v15="+campo15+"&v16="+campo16+"&v17="+campo17+"&v18="+campo18+"&v19="+campo19,
		success: function(datos){
			if(datos==0){
				alert("La radicaci\u00F3n no se realiz\u00F3, intente de nuevo");
				}
			else{
				$('#txtId').val(datos);

				alert("La radicaci\u00F3n se guard\u00F3 con \u00C9xito! Radicaci\u00F3n N\u00FAmero: " + datos);
				nuevo=0;
				if($('#select2').val()==70){
					guardarBeneficiario();
				}
                if($('#select2').val()==211){
					guardarConvivencia();
				}
				if($('#select2').val()==195){
					guardarReposicion();
				}
				if($('#select2').val()==169){
					var campo0=$('#idPersona').val();
					var campo1=$('#periodoI1').val();
					var campo2=$('#periodoF1').val();
					var campo3=$('#causal1').val();
					var campo4=$('#notas11').val();
					$.post('reclamoT.php',{v0:campo0,v1:campo1,v2:campo2,v3:campo3,v4:campo4},function(data){
						alert(data);
						});
					}
				if($('#select2').val()==170){
					var campo0=$('#idEmpresa').val();
					var campo1=$('#periodoI2').val();
					var campo2=$('#periodoF2').val();
					var campo3=$('#causal2').val();
					var campo4=$('#notas12').val();
					$.post('reclamoE.php',{v0:campo0,v1:campo1,v2:campo2,v3:campo3,v4:campo4},function(data){
						alert(data);
						});
					}
				if($('#select2').val()==191){
					var campo0=$('#idPersona').val();
					var campo1=$('#idConyuge').val();
					$.post('disolver.php',{v0:campo0,v1:campo1},function(data){
						alert(data);
						});
					campo0=$('#txtId').val();
					$.post('cerrarRadicacion.php',{v0:campo0},function(datos){
						});
					}
				if($('#select2').val()==32){
					var campo0=$('#idPersona').val();
					var campo1=$('#lisBeneficiarios').val();
					var campo2=$('#lTipoCer').val();
					$.post(URL+'aportes/radicacion/buscarTipoParentesco.php',{v0:campo0,v1:campo1},function(datos){
						var idtp=datos;
						if(idtp==36){
							if(campo2==55 || campo2==56){
								alert("Un padre no requiere Certificado de estudio!, intente de nuevo");
								//cerrar radicacion con error de usuario
								return false;
								}
								}
						$.post('grabarCertificado.php',{v0:campo1,v1:idtp,v2:campo2,v3:'P'},function(datos){
							alert (datos);
									});
						});
				}
			}
			if(campo4==33 && ide==63){
				var r = confirm("Confirme si la Tarjeta se va a entregar!");
				if(r){
					entregarTarjeta();
				}
				else
				{
					alert("No se entreg\u00F3 NINGUNA tarjeta!, s\u00F3lo se guard\u00F3 la atenci\u00F3n al usuario");
				}
			}
		}
	});
	return false;
}

function entregarTarjeta(){
	var campo0 =document.getElementById('idT').value;
	var campo1 =document.getElementById('usuario').value;
	$.ajax({
		url: 'entregarT.php',
		type: "POST",
		data: "submit=&v0="+campo0+"&v1="+campo1,
		success: function(datos){
			alert(datos);
		}
	});
	return false;
}

//... ABRIR VENTANA DIALOG PARA DIRECCIONES...//

function direccion(obj){
	     //$("#dialog-form").dialog("destroy"); //Destruir dialog para liberar recursos
		 $("#dialog-form").dialog({
			         height: 450,
			         width: 650,
			        modal: true,
			        open: function(){
					$('#dialog-form').html('');
					$.get('../../phpComunes/direcciones.html',function(data){
							$('#dialog-form').html(data);
					});
				       $('#tDirCompleta').val('');
				       $('#dialog-form select,#dialog-form input[type=text]').val('');
			           $('#uno').focus();
			},
			  buttons: {
				                'Crear direcci\u00F3n': function() {
								$("#dialog-form").dialog("destroy");
		                        $("#dialog-form").dialog('close');
					            obj.value=$('#tDirCompleta').val();
								$('#tDirCompleta').val('');
					            $('#dialog-form select,#dialog-form input[type=text]').val('');
					            
				}
			},
			close: function() {
				$("#dialog-form").dialog("destroy");
		        $("#dialog-form").dialog('close');
				$('#tDirCompleta').val('');
				$('#dialog-form select,#dialog-form input[type=text]').val('');
			}
	});
}//end function
//-- FIN DIRECCIONES


$(function() {
	$("#dialog-form2").dialog("destroy");
	$("#dialog-form2").dialog({
		autoOpen: false,
		height: 400,
		width: 500,
		modal: true,
		buttons: {
			'Enviar': function() {
			var bValid = true;
			var campo=$('#notas').val();
			var campo0=$.trim(campo);
			if (campo0==""){
				$(this).dialog('close');
					return false;
				}
			var campo1=$('#usuario').val();
			var campo2="radicacion.php";
			$.post(URL+'phpComunes/colaboracion.php',{v0:campo0,v1:campo1,v2:campo2},
			function(datos){
				if(datos=='1'){
					alert("Su comentario fue enviado correctamente!. Gracias por participar en el mejoramiento de SIGAS");
				}else{
					alert(datos);
				}
		});
		$(this).dialog('close');
		},
		Cancelar: function() {
			$(this).dialog('close');
			$("#dialog-form2").dialog("destroy");
		}
	}
	});
	/*
	$('#enviar-notas')
		.button()
		.click(function() {
		$('#dialog-form2').dialog('open');
	});
	*/
});

$(function() {
		$("#ayuda").dialog({
		 	autoOpen: false,
			height: 550,
			width: 850,
			draggable:true,
			modal:false,
			open: function(evt, ui){
					$('#ayuda').html('');
					$.get('../../help/aportes/ayudaRadicacion.html',function(data){
							$('#ayuda').html(data);
					});
			 }
		});
	});

function buscarFallecido(obj){
	var idf=parseInt(obj.value);
	var idp=$('#idPersona').val();
	switch(idf){
		case 1 :
		var nom =$('#nombreT').html();
		$('#comboFallecido option').remove();
		$('#comboFallecido').append('<option value='+idp+'>'+nom+'</option>');
		break;
		case 2:
		$.getJSON(URL+'phpComunes/buscarHijos.php',{v0:idp},function(data){
			$('#comboFallecido option').remove();
			$.each(data,function(i, fila){
			var nom = fila.pnombre+" "+fila.snombre+" "+fila.papellido+" "+fila.sapellido;
			$('#comboFallecido').append("<option value="+fila.idbeneficiario+">"+nom+"</option>");
			});
			});
		break;
		case 3:
		$.getJSON(URL+'phpComunes/buscarPadres.php',{v0:idp},function(data){
			$('#comboFallecido option').remove();
			$.each(data,function(i, fila){
			var nom = fila.pnombre+" "+fila.snombre+" "+fila.papellido+" "+fila.sapellido;
			$('#comboFallecido').append("<option value="+fila.idbeneficiario+">"+nom+"</option>");
			});
			});
		break;
	}
	}

$(function() {
		$('#fechaD').datepicker({
			changeMonth: true,
			changeYear: true
		});
	});

function ocultarDiv(){
	removeAllTabs();
	$('#div1,#div2,#div3,#div4,#div5,#div6,#div7,#div8,#div9,#div10,#div11,#div12,#div13,#div14,#div15,#div16,#div17').find(":text").val("").end().find("select").val(0).end();
	$('#div1,#div2,#div3,#div4,#div5,#div6,#div7,#div8,#div9,#div10,#div11,#div12,#div13,#div14,#div15,#div16,#div17').css({display:"none"});
	$('#gfami tr:not(:first)').remove();
	$('#nombreT2').empty();
	//children().empty().parent().
	}


//...ABRIR DOCUMENTOS...
function imagen(imgAlt){

   //Capturo el objeto como un ojeto jquery
	var img=jQuery(imgAlt);
	//Borror en el div #imagen alguna imagen
	$("#imagen").find("img").remove();
	$("#imagen").dialog('open');

	var srcImage= img.attr("alt");
	$("#imagen").append("<img src='"+srcImage+"' />");
}//end function imagen

function buscarRelacion(cc){
	if( $.trim($('#tNumero9').val()) == $.trim(cc)){
		alert("Las identificaciones de los padres son iguales!");
		return false;
		}
	$.getJSON(URL+'phpComunes/buscarPersona.php',{v0:cc},function(data){
		if(data==0){
			alert ("No exite en nuestra base de datos!, debe crear el c\u00F3nyuge primero");
			return false;
			}
		var nom="";
		$.each(data,function(i,fila){
			nom =fila.pnombre;
			nom+=" "+fila.snombre;
			nom+=" "+fila.papellido;
			nom+=" "+fila.sapellido
			idcony=fila.idpersona;
			})	;
		$('#progenitor').val(nom);

		});
	}

function buscarPersonaDos(ide){
	$("#nuevoB input:text,#nuevoB select").removeClass("error");
//aqui preguntar si es el div de la tabla beneficiarios el q esta visible para validarlo
if($("#div9").is(":visible")&&ide!=''){
	if($("#tipoDoc3").val()==0){
		alert("Falta el tipo de documento");
		return false;
		}
	var idt=$("#idPersona").val();
	var idb=ide;
	var idtd = $("#tipoDoc3").val();
	$.getJSON(URL+'phpComunes/buscarPersona2.php',{v0:idtd,v1:idb},function(data){
		if(data==0){
			existe=0;
			return false;
			}
		existe=1;
		$.each(data,function(i,fila){
			idnb=fila.idpersona;
			$('#pNombre3').val(fila.pnombre);
			$('#sNombre3').val(fila.snombre);
			$('#pApellido3').val(fila.papellido);
			$('#sApellido3').val(fila.sapellido);
			var fecha=fila.fechanacimiento;
			fecha=fecha.replace("/","");
			$('#fecNac3').val(fecha.replace("/",""));
			$('#fecNac3').click().blur();
			$('#sexo3').val(fila.sexo);
			}); //each

			// TABLA DINAMICA PARA DATOS EMPRESA TRABAJDOR ACTIVO
			$("table[name='dinamicTable']").remove();
			
			var tabla="<table class='tablero' width='100%' border='0' cellspacing='0' cellpadding='0'    name='dinamicTable' style='margin-bottom:5px;'>";
			
			$.getJSON(URL+'phpComunes/buscarAfiliacion.php',{v0:idnb},function(data){
				
			if(data != 0){
					alert("La persona que trata de Afiliar es un trabajador activo!");
					 $.each(data,function(i,fila){
						  tabla+="<tr><th>Salario</th><th>Nit</th><th>Raz\u00F3n social</th><th>Tipo</th><th>Detalle</th></tr>"+"<tr><td>$"+ fila.salario +"</td><td>"+ fila.nit +"</td><td>"+fila.razonsocial+"</td><td>"+fila.tipoformulario+"</td><td>"+fila.detalledefinicion+"</td></tr>";
				        //Agregar tabla despues de la tabla  beneficiarios
						$("#nuevoB").before(tabla+="</table>");
						$("table[name='dinamicTable']").hide().fadeIn(800);
					});//end each
					$('#nuevoB input:text').val("");
					$('#nuevoB select').val("0");
					return false;
					}
			$.getJSON(URL+'phpComunes/buscarRelacion.php',{v0:idnb,v1:idt},function(data){
				if(data!= 0){
					alert("La persona que trata de Afiliar ya es Beneficiario!");
				    $.each(data,function(i,fila){
			      	var nom = fila.pnomt+" "+fila.snomt+" "+fila.papet+" "+fila.sapet;
				  tabla+="<tr><th>Nombres</th><th>Parentesco</th><th>Conv</th><th>F Afilia</th><th>Relaci&oacute;n</th><th> Giro</th><th>Estado</th></tr>"+"<tr><td>"+nom+"</td><td>"+fila.detalledefinicion+"</td><td>"+fila.conviven+"</td><td>"+fila.fechaafiliacion+"</td><td>"+fila.idrelacion+"</td><td>"+fila.giro+"</td><td>"+fila.estado+"</td></tr>";
				        //Agregar tabla despues de la tabla  beneficiarios
						$("#nuevoB").before(tabla+="</table>");
						$("table[name='dinamicTable']").hide().fadeIn(800);
					});//end each

                    $('#nuevoB input:text').val("");
   					$('#nuevoB select').val("0");

					return false;
					}

			$.getJSON(URL+'phpComunes/buscarRelaciones.php',{v0:idnb},function(data){
				if(data != 0){
					    alert("La persona que trata de Afiliar tiene las siguientes relaciones!");
					    $.each(data,function(i,fila){
			      var nom = fila.pnomt+" "+fila.snomt+" "+fila.papet+" "+fila.sapet;
				  tabla+="<tr><th>Nombres</th><th>Parentesco</th><th>Conv</th><th>F Afilia</th><th>Giro</th><th>Estado</th></tr>"
				  +"<tr><td>"+nom+"</td><td>"+fila.detalledefinicion+ "</td><td>"+fila.conviven+"</td><td>"+fila.fechaafiliacion+"</td><td>"+fila.giro+"</td><td>"+fila.estado+"</td></tr>";
				      //Agregar tabla despues de la tabla  beneficiarios
						$("#nuevoB").before(tabla+="</table>");
						$("table[name='dinamicTable']").hide().fadeIn(800);
					});//end each

					   $('#nuevoB input:text').val("");
   					$('#nuevoB select').val("0");
					return false;
					}
				}); //getJSON relaciones
				}); //getJSON relacion
				}); //getJSON afiliacion
		}); //getJSON

	}//end visible div9
	}//end funcion

function guardarBeneficiario(){
	$("table[name='dinamicTable']").remove();
    var errores=0;

	$("#nuevoB input:text,#nuevoB select").removeClass("error");
	//VALIDAR CAMPOS
			    var tipoDoc3=$("#tipoDoc3").val();
		    	var cedula5=$("#identificacion5").val();
			    var pNombre3=$("#pNombre3").val();
			    var pApellido3=$("#pApellido3").val();
				var sNombre3=$("#sNombre3").val();//no obligado
			    var sApellido3=$("#sApellido3").val();//no obligado
			    var sexo3=$("#sexo3").val();
				var fecNac3=$("#fecNac3").val();
				var parentesco=$("#parentesco").val();
				var tipoafiliacion3=$("#tipoA").val();//no bligado
				var cedMama=$("#cedMama").val();//no obligado
		        var capacidad=$("#capacidad").val();

  //---------------------TIPO DOC----------------------  
			   if(tipoDoc3=='0'){
					$("#tipoDoc3").addClass("error");
					errores++;
					}
		    //--------------------CEDULA-----------------------   
			   if(cedula5==''){
				$("#identificacion5").addClass("error");
				errores++;
						
				} else {
                         if(cedula5.length<3){
					     alert('La identificaci\u00F3n NO debe tener 2 d\u00EDgitos.');
						 errores++;
				         }else {
							 if(cedula5.length==9){
					          alert('La identificaci\u00F3n NO debe tener 9 d\u00EDgitos.');
							  errores++;
							    }
							   
						 }//end else
				  }//end else
		    //----------------PRIMER NOMBRE Y APELLIDO---------------------------  
			   if(pNombre3==''){
					$("#pNombre3").addClass("error");
					errores++;
				}
				
				 if(pApellido3==''){
					$("#pApellido3").addClass("error");			
					errores++;
				}
			
			
			//-------------------SEXO-----------------------
			if(sexo3=='0'){
				$("#sexo3").addClass("error");		
				errores++;
				}
			
			//----------------------FECHA DE NACIMIENTO 3---------------
			if(fecNac3==''||fecNac3=='mmddaaaa'){
				$("#fecNac3").addClass("error");		
				errores++;
			}
			
			//-------------------PARENTEZCO-----------------------
			if(parentesco=='0'){
					$("#parentesco").addClass("error");
					errores++;
			}else{
                    if(parentesco=='36' && tipoafiliacion3=='48'){
						if (edad<=60){
						alert("La edad del padre para SUBSIDIO debe ser mayor a 60.");
						errores++;
						}
						}
				}
				
				
		//	-------------------CED MAMA
		//Si el icono de oblgado es visible significa q es necesario el campo PROGENITOR
				if($("#imgMama").is(":visible") && $("#cedMama").val()==''){
					$("#cedMama").addClass("error");
					errores++;
					}	
						
		//-------------------CAPACIDAD TRABAJO-----------------------
			if(capacidad=='0'){
				$("#capacidad").addClass("error");
				errores++;
			}
	
	if(errores>0){
	alert("Faltan datos por ingresar");
	}else{
	var idp=parseInt($('#parentesco').val());
        if(idp==99) idp=35;
	switch(idp){
		case 35:if(existe==1){
			comprobarRelacion(35);
                    }
			else{
			guardarPersona2();
			}
		break;	//hijo

		case 36:if(existe==1){
			comprobarRelacion(36);
                    }
			else{
			guardarPersona2();
			}
		break;	//padre

		case 37:if(existe==1){
                    comprobarRelacion(37);
                }
                    else{
                    guardarPersona2();
                }
                break;//hermano
		}
	}//end if clase error
	}

function comprobarRelacion(op){
	var idt=$('#idPersona').val();
	var idb=idnb;
	var fechaa='';
	if(op==35){
		//hijo
                $.getJSON(URL+'phpComunes/buscarRelacionHijo.php',{v0:idt,v1:idnb},function(data){
                    if(data==0){
                        guardarRelacion(35);
                    }
                    else{
                        $.each(data,function(i,fila){
                            fechaa=fila.fechaafiliacion;
                        })
                        alert("La persona ya tiene una relaci\u00F3n Trabajador - Hijo desde el " + fechaa +"(ver cuadro!)");
                    }
                });
		}
	if(op==36){
		//padre
		$.getJSON(URL+'phpComunes/buscarRelacionPadre.php',{v0:idb},function(data){
			if(data == 0){
				guardarRelacion(36);
			}
			else{
				$.each(data,function(i,fila){
					fechaa=fila.fechaafiliacion;
					})
				alert("La persona ya tiene una relaci\u00F3n Trabajador-Padre desde el "+fechaa + " (Ver cuadro!)");
				}
		});
		}
	if(op==37){
		//hermano
		$.getJSON(URL+'phpComunes/buscarRelacionHermano.php',{v0:idt,v1:idnb},function(data){
                    if(data==0){
                        guardarRelacion(35);
                    }
                    else{
                        $.each(data,function(i,fila){
                            fechaa=fila.fechaafiliacion;
                        })
                        alert("La persona ya tiene una relaci\u00F3n Trabajador - Hermano desde el " + fechaa +"(ver cuadro!)");
                    }
                });
		}
	}

function guardarRelacion(op){
        var campo0 = $('#idPersona').val();
        var campo1 =idnb;
	var campo2 = $('#parentesco').val();
        if(campo2==99) campo2=35;
        var campo3 = idcony;
        var campo4 = $('#tipoA').val();

//idtrabajador idbeneficiario idparentesco idconyuge giro

	$.getJSON(URL+'phpComunes/guardarBeneficiario.php',{v0:campo0,v1:campo1,v2:campo2,v3:campo3,v4:campo4},function(data){
		if(data==0)
			alert("El beneficiario NO se guard\u00F3!, intende de nuevo");
		else
			alert("El beneficiario se guard\u00F3!, bajo el n\u00FAmero: "+data);
			//andres pintar tabla con los datos del nuevo beneficiario
               $("#nuevoB input:text").val('');
			   $("#nuevoB select").val('Seleccione..');

		})
            }

function guardarPersona2(){
        var campo0 = $('#tipoDoc3').val();
	var campo1 = $('#identificacion5').val();
	var campo2 = $('#pApellido3').val();
	var campo3 = $('#sApellido3').val();
        var campo4 = $('#pNombre3').val();
	var campo5 = $('#sNombre3').val();
	var campo6 = $('#sexo3').val();
	var campo7 = $('#fecNacHide3').val();
        var campo8 = $('#capacidad').val();

//idtipodocumento identificacion papellido sapellido pnombre snombre sexo fechanacimiento capacidadtrabajo
	$.get(URL+'phpComunes/guardarPersona2.php',{v0:campo0,v1:campo1,v2:campo2,v3:campo3,v4:campo4,v5:campo5,v6:campo6,v7:campo7,v8:campo8}, function(datos){
		if(datos==0){
                    alert("No se pudo guardar los datos de la persona!");
                    return false;
                }
                else{
                    idnb=datos;
                    alert("Se gurd\u00F3 la persona!");
                    guardarRelacion(idnb);
                }
	});
}

function buscarPersona2(ncc){
    var idtdoc2=$('#tipoDoc2').val();
    var numero2=ncc;

 if(numero2==''){
	alert("Ingrese la c\u00E9dula");
	$("#identificacion7").focus();
	 }else{
	$.getJSON(URL+'phpComunes/buscarPersona2.php',{v0:idtdoc2,v1:numero2},function(datos){
	if(datos == 0 ){
			alert("No existe en nuestra base, registre los datos completos!");
			         $("#nuevoC input:text").not("#identificacion7").val('');
					 $("#nuevoC select").not("#tipRel,#conviven,#tipoDoc2").val('0');
                        existe=0;
		}
	else{
             alert("La persona ya existe en la base de datos, actualice los datos!");
             existe=1;
             //datos persona
            $.each(datos,function(i,fila){
                idcony=fila.idpersona;
		$('#pNombre2').val(fila.pnombre);
		$('#sNombre2').val(fila.snombre);
		$('#pApellido2').val(fila.papellido);
		$('#sApellido2').val(fila.sapellido);
		$('#sexo2').val(fila.sexo);
		$('#direccion2').val(fila.direccion);
		$('#barrio2').val(fila.idbarrio);
		$('#telefono2').val(fila.telefono);
		$('#celular2').val(fila.celular);
		$('#email2').val(fila.email);
                $('#tipoVivienda2').val(fila.idtipovivienda);
                var depto=fila.iddepresidencia;
                $('#combo1').val(0);
                $('#combo1').change();
                $('#combo1').val(depto);
                $('#combo1').change();
                $('#combo2').val(fila.idciuresidencia);
                $('#combo3').val(fila.idzona);
                $('#estadoCivil3').val(fila.idestadocivil);
                var fecha=fila.fechanacimiento;
		fecha=fecha.replace("/","");
                $('#fecNac2').val(fecha.replace("/",""));
		$('#fecNac2').click().blur();
                var depto2=fila.iddepnace;
                $('#combo4').val(0);
                $('#combo4').change();
                $('#combo4').val(depto2);
                $('#combo4').change();
                $('#combo5').val(fila.idciunace);
                $('#capTrabajo2').val(fila.capacidadtrabajo);
                $('#profesion2').val(fila.idprofesion);
                });
                //buscar relaciones con convivencia
                if($('#select2').val()==211){
                $.getJSON(URL+'phpComunes/buscarConvivenciaConyuge.php',{v0:idcony},function(data){
                    if(data==0){
                        alert("NO posee relaci\u00F3n de convivencia!");
                        //andres limpiar campos a partir del primer nombre
                    }
                    else{
                        //andres mostrar una tabla con la inforamcion de data
                         alert("La persona YA tiene una relaci\u00F3n de convivencia activa");
						$("#nuevoC input:text").val('');
						$("#nuevoC select").val('0');
                    }
                $.getJSON(URL+'phpComunes/buscarAfiliacion.php',{v0:idcony}, function(datos){
                    if(datos !=0){
                        $.each(datos, function(i,fila){
                        $('#nitConyuge').val(fila.nit);
                        $('#empCony').val(fila.razonsocial);
                        $('#salCony').val(formatCurrency(fila.salario));
                        $('#subCony').val(fila.detalledefinicion);
                        })
                    }

                });
                });

                }
		};
        });//end get json
		
		 }//find e else de numero2
 }//end fucntion

 function buscarAfiliacion(){
     var idp=$('#idPersona').val();
     $.getJSON(URL+'phpComunes/buscarAfiliacion.php',{v0:idp},function(datos){
         if(datos==0){
             alert("El trabajador no tiene afiliaci\u00F3n Activa");
             $("#div17 input:text").not("#tNumero17,#tNombre17").val('');
			 $("#div17  select").not("#sltTipoIdentificacion17").val('0');
			 $("#tNit17").focus();
             //validar no puede continuar
             return false
         }
     //datos persona afiliada
     $.each(datos,function(i,fila){
              $('#tNit17').val(fila.nit);
              $('#tRazon17').val(fila.razonsocial);
              $('#fafiliacion').val(fila.fechaingreso);
              $('#eafiliacion').val('Activa');
              $('#tipoAfi17').val(fila.detalledefinicion);
     });//end each
    });

 }//end function

 function guardarConvivencia(){
    var campo0  =$('#tipRel').val();
    var campo1  =$('#conviven').val();
    var campo2  =$('#tipoDoc2').val();
    var campo3  =$('#identificacion7').val();
    var campo4  =$('#pNombre2').val();
    var campo5  =$('#sNombre2').val();
    var campo6  =$('#pApellido2').val();
    var campo7  =$('#sApellido2').val();
    var campo8  =$('#sexo2').val();
    var campo9  =$('#direccion2').val();
    var campo10 =$('#barrio2').val();
    var campo11 =$('#telefono2').val();
    var campo12 =$('#celular2').val();
    var campo13 =$('#email2').val();
    var campo14 =$('#tipoVivienda2').val();
    var campo15 =$('#combo1').val();
    var campo16 =$('#combo2').val();
    var campo17 =$('#combo3').val();
    var campo18 =$('#estadoCivil2').val();
    var campo19 =$('#fecNacHide2').val();
    var campo20 =$('#combo4').val();
    var campo21 =$('#combo5').val();
    var campo22 =$('#capTrabajo2').val();
    var campo23 =$('#profesion2').val();
    var campo24 =existe;
    var campo25 =idcony;
    $.getJSON(URL+'phpComunes/guardarPersonaCompleta.php',{v0:campo0,v1:campo1,v2:campo2,v3:campo3,v4:campo4,v5:campo5,v6:campo6,v7:campo7,v8:campo8,v9:campo9,v10:campo10,v11:campo11,v12:campo12,v13:campo13,v14:campo14,v15:campo15,v16:campo16,v17:campo17,v18:campo18,v19:campo19,v20:campo20,v21:campo21,v22:campo22,v23:campo23,v24:campo24,v25:campo25},function(datos){
        if(datos==0){
            alert("No se pudo guardar los datos de la persona!");
            return false;
        }
        else{
            if(datos==1){
            alert("Los datos fueron actualizados!");
            }
            if(existe==0) idcony=datos;
            var campo0= $('#idPersona').val();  //idtrabajador
            var campo1= idcony;                  //idbeneficiario
            var campo2= 34;                     //idparentesco
            var campo3= idcony;                  //idconyuge
            var campo4= $('#conviven').val();   //conviven
            var campo5= $('#tipRel').val();     //tiporelacion

            $.getJSON(URL+'phpComunes/guardarConyuge.php',{v0:campo0,v1:campo1,v2:campo2,v3:campo3,v4:campo4,v5:campo5},function(data){
                if(data==0){
                    alert("No se pudo guardar la relaci\u00F3n!, intente de nuevo!");
                    return false;
                }
                else{
                    $.getJSON(URL+'phpComunes/guardarConyuge.php',{v0:campo1,v1:campo0,v2:campo2,v3:campo0,v4:campo4,v5:campo5},function(data){
                        alert("La convivencia se guard\u00F3!");
						  $("table[name='dinamicTableC']").remove();
                    });
                    }
                    $("#nuevoC input:text").val('');
					$("#nuevoC select").val('0');
                    // andres limpiar todo
            });
        }

    })

 }

 function buscarConvivencia(){
     var campo0=$('#idPersona').val();
	 $("table[name='dinamicTableC']").remove();
	 $("#nuevoC").hide();//mantener oculta la tabla de conyuge
     var tablaConyuge="<table class='tablero' width='100%' border='0' cellspacing='0' cellpadding='0'    name='dinamicTableC' style='margin-bottom:5px;'>";     
	
	 $.getJSON(URL+'phpComunes/buscarConvivencia.php',{v0:campo0},function(datos){
         if(datos==0){
             alert("El trabajador no tiene convivencia Activa!");
			  $("table[name='dinamicTableC']").remove();
			  $("#nuevoC").show();//mostrar tabla para llenar datos del conyuge y limpiarlos si antes habian datos
			  $("#nuevoC input:text").val('');
			  $("#nuevoC select").val('0');
         }
         else{
             alert("El trabajador tiene convivencia Activa (ver cuadro)");
			 $.each(datos,function(i,fila){
			      var nombreConyuge=fila.pnombre+" "+fila.snombre+" "+fila.papellido+" "+fila.sapellido;
				  tablaConyuge+="<tr><th>Nombres Conyuge</th><th>Identificacion</th><th>Parentesco</th><th>Conviven</th></tr><tr><td>"+nombreConyuge +"</td><td>"+ fila.identificacion +"</td><td>"+fila.detalledefinicion+"</td><td>"+fila.conviven+"</td></tr>";
				        //Agregar tabla despues de la tabla  beneficiarios
						$("#cony").before(tablaConyuge+="</table>");
						$("#dinamicTableC").hide().fadeIn(800);
					});//end each
             	
			 return false;
             
         }
     });
 }
 
function buscarCuentaHabiente(td,ni){
	if(td==0){
		alert("Falta tipo documento!");
		return false;
		}
	$.getJSON(URL+'phpComunes/buscarPersona2.php',{v1:td,v0:ni},function(data){
		if(data==0){
			alert("La persona no existe!");
			return false;
			}
		$.each(data,function(i,fila){
			idp=fila.idpersona;
			var nombres =fila.pnombre+" "+fila.snombre+" "+fila.papellido+" "+fila.sapellido;
			$("#tNombre16").val(nombres);
			}); //each
		$.getJSON(URL+'phpComunes/buscarTarjeta.php',{v0:idp},function(data){
			if(data==0){
				alert("La persona NO tiene Tarjeta asignada!");
				return false;
				}
		$.each(data,function(i,fila){
			$("#codBarras").val(fila.codigobarra);
			$("#bono").val(fila.bono);
			$("#saldo").val(formatCurrency(fila.saldo));
			});
			});
	})
	}
 
 function guardarReposicion(){
	 var campo0=$('#txtId').val();
	 var campo1=$("#idPersona").val();
	 var campo2=$("#motivoR").val();
	 var campo3=$("#codBarras").val();
	 var campo4=$("#bono").val();
	 var campo5=$("#comprobante").val();
	 var campo6=$("#numero16").val();
     $.getJSON('guardarReposicion.php',{v0:campo0,v1:campo1,v2:campo2,v3:campo3,v4:campo4,v5:campo5,v6:campo6}, function(data){
            if(data==0){
                alert("NO se pudo guardar la reposici\u00F3n!, intende de nuevo");
                return false;
            }
         alert("Se guard\u00F3 la reposici\u00F3n!");
         })
	 
	 }
 /*
� 		\u00C1
� 		\u00E1
� 		\u00C9
� 		\u00E9
� 		\u00CD
� 		\u00ED
� 		\u00D3
� 		\u00F3
� 		\u00DA
� 		\u00FA
� 		\u00DC
� 		\u00F1

� 		\u00FC
? 		\u00D1
& 		\u0022
< 	 	\u003C
> 	 	\u003E
� 		\u00ED
  	  	\u00A0
� 		\u0022
� 		\u0027
� 		\u00A9
� 		\u00AE
� 		\u20AC
� 		\u00BC
� 		\u00BD
� 		\u00BE
*/  