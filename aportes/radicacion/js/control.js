/*
* @autor:      Ing. Orlando Puentes
* @fecha:      25/08/2010
* objetivo:
*/
// JavaScript Document
var URL=src();
var cache = {}; // utilizada en el auto-completar
$(document).ready(init);

function init() {
	
initFonede();

//inicializar controles del formulario de bloqueo de tarjeta
initBloqueoTarjeta();

//inicializar controles del formulario de bloqueo de tarjeta
initNovedadTarjeta();

//ocultar todos los divs.
$('#div1,#div2,#div3,#div4,#div5,#div6,#div7,#div8,#div9,#div10,#div11,#div12,#div13,#div14,#div15,#div16,#div17,#div18,#div19,#div20').hide();
//validacion documentos afiliado
$("#docs div:not(:first)").hide();

// ocultar las capas de documentos de trabajador independiente
$("#docsInd div:not(:first)").hide();

//ocultar las capas de documentos de renovaci�n
$("#docsRen div:not(:first)").hide();

//ocultar las capas de documentos afiliacion empresa
$("#docsAfiEmpresa div:not(:first)").hide();

// Funcionamiento controles de documentaci�n de trabajador dependiente
$("input[name=rconviven],input[name=rhijos],input[name=rpadres]").click(function(){
	var name=$(this).attr("name");  
	
		switch(name){
			case 'rconviven': (   $(this).val()=='s'    ?   $("#docs div:eq(1)").show()  : $("#docs div:eq(1)").hide().find("input:checkbox").attr("checked",false) );
		    break;
		    case 'rhijos':    (   $(this).val()=='s'    ?   $("#docs div:eq(2)").show()  : $("#docs div:eq(2)").hide().find("input:checkbox").attr("checked",false) );
	 	    break;
		    case 'rpadres':   if($(this).val()=='s')    {  $("#tipoServicio").show() ;}else{   $("#tipoServicio").hide().val('0') ; $("#docs div:eq(3)").hide().find("input:checkbox").attr("checked",false);}
   	        break;
		  }
});//end click

//Combo para documentos padres padres
$("#tipoServicio").change(function(){
	var valor=$(this).val();
	//si el combo es cero oculta el DIV y desactiva los check
	if(valor=='0'){
		$("#docs div:eq(3)").hide().find("input:checkbox").attr("checked",false) ;
	}else{
		$("#docs div:eq(3)").show().find("p").hide();
		//si el combo es 1 muestra solo el primer combo SINO todos..
		if(valor=='1'){
			$("#docs div:eq(3)").find("p:first").show();
		}else{
			$("#docs div:eq(3)").find("p").show();
		}
	}//end if
});//Fin change servicio padres

 // validaciones de cocumentos FONEDE
inicializarChecklistFonede();
setValidacionesDocumentosFonede();

//mostrar documento de certificado de pensionado y recibo de pago de salud de independiente
$("#tAfiliacion19").change(function(){
	if($("#tAfiliacion19").val()==20){
		$("#p1").show();
		$("#p2").hide();
	}else if($("#tAfiliacion19").val()==46){
		$("#p2").show();
		$("#p1").hide();
	}else{
		$("#p1").hide();
		$("#p2").hide();
	}
});
//fin mostrar documentos de cert...

// Funcionamiento controles de documentaci�n de trabajador independiente
$("input[name=rconvivenInd],input[name=rhijosInd],input[name=rpadresInd]").click(function(){
	var name=$(this).attr("name");
	var valor = $(this).val();
	if(name == "rconvivenInd"){
		if(valor == 's')
			$("#docsInd div:eq(1)").show();
		else
			$("#docsInd div:eq(1)").hide().find("input:checkbox").attr("checked",false);
	}
	
	if(name == "rhijosInd"){
		if(valor == 's')
			$("#docsInd div:eq(2)").show();
		else
			$("#docsInd div:eq(2)").hide().find("input:checkbox").attr("checked",false);
	}
	if(name == "rpadresInd"){
		if(valor == 's'){
			$("#tipoServicioInd").show();
			$("#tipoServicioInd").val(1);
			$("#docsInd div:eq(3)").show().find("input:checkbox").attr("checked",false);
		}else{
			$("#tipoServicioInd").hide().val('0'); 
			$("#docsInd div:eq(3)").hide().find("input:checkbox").attr("checked",false);
		}
	}
});//end click

//Combo para documentos de padres de trabajadores independientes
$("#tipoServicioInd").change(function(){
	var valor=$(this).val();
	//si el combo es cero oculta el DIV y desactiva los check
	if(valor=='0'){
		$("#docsInd div:eq(3)").hide().find("input:checkbox").attr("checked",false) ;
	}else{
		$("#docsInd div:eq(3)").show().find("p").hide();
		//si el combo es 1 muestra solo el primer combo SINO todos..
		if(valor=='1'){
			$("#docsInd div:eq(3)").find("p").show();
		}
	}//end if
});//Fin change servicio padres

//Funcionamiento controles de documentaci�n de afiliacion empresa
$("input[name=rempresa]").click(function(){
	var valor = $(this).attr("id");
	if(valor == 'natural'){
		$("#docsAfiEmpresa div:eq(1)").show();
		$("#docsAfiEmpresa div:eq(2)").hide().find("input:checkbox").attr("checked",false);
		$("#t_personanatural").show();
		$("#pn_afiotracaja_no").trigger("click");
		$("#t_personajuridica").hide();
		$("#p_pn_pazysalvo").hide();
	}else{
		$("#docsAfiEmpresa div:eq(2)").show();
		$("#docsAfiEmpresa div:eq(1)").hide().find("input:checkbox").attr("checked",false);
		$("#t_personanatural").hide();
		$("#t_personajuridica").show();
		$("#pj_afiotracaja_no,#animodelucro_no,#cooperativa_no,#propiedadhorizontal_no,#consorcio_no").trigger("click");
		$("#p_pj_pazysalvo").hide();
		$("#p_lucro").hide();
		$("#p_ecosolida").hide();
		$("#p_propiedadhorizontal").hide();
		$("#p_consorcio").hide();
	}
});

$("input[name=pn_afiotracaja]").click(function(){
	var valor = $(this).attr("id").replace("pn_afiotracaja_","");
	if(valor == "si")
		$("#p_pn_pazysalvo").show();
	else
		$("#p_pn_pazysalvo").hide().find("input:checkbox").attr("checked",false);
});

$("input[name=pj_afiotracaja]").click(function(){
	var valor = $(this).attr("id").replace("pj_afiotracaja_","");
	if(valor == "si")
		$("#p_pj_pazysalvo").show();
	else
		$("#p_pj_pazysalvo").hide().find("input:checkbox").attr("checked",false);
});

$("input[name=animodelucro]").click(function(){
	var valor = $(this).attr("id").replace("animodelucro_","");
	if(valor == "si")
		$("#p_lucro").show();
	else
		$("#p_lucro").hide().find("input:checkbox").attr("checked",false);
});

$("input[name=cooperativa]").click(function(){
	var valor = $(this).attr("id").replace("cooperativa_","");
	if(valor == "si")
		$("#p_ecosolida").show();
	else
		$("#p_ecosolida").hide().find("input:checkbox").attr("checked",false);
});

$("input[name=propiedadhorizontal]").click(function(){
	var valor = $(this).attr("id").replace("propiedadhorizontal_","");
	if(valor == "si")
		$("#p_propiedadhorizontal").show();
	else
		$("#p_propiedadhorizontal").hide().find("input:checkbox").attr("checked",false);
});

$("input[name=consorcio]").click(function(){
	var valor = $(this).attr("id").replace("consorcio_","");
	if(valor == "si")
		$("#p_consorcio").show();
	else
		$("#p_consorcio").hide().find("input:checkbox").attr("checked",false);
});
//end click afiliacion empresa
$("#juridica").trigger("click");
//$("#t_personanatural").hide().find("input type:radio").attr("checked",false);
//$("#t_personajuridica").hide().find("input type:radio").attr("checked",false);

//Funcionamiento controles de documentaci�n de renovaci�n
$("input[name=rconvivenRen],input[name=rhijosRen],input[name=rpadresRen]").click(function(){
	var name=$(this).attr("name");
	var valor = $(this).val();
	if(name == "rconvivenRen"){
		if(valor == 's')
			$("#docsRen div:eq(1)").show();
		else
			$("#docsRen div:eq(1)").hide().find("input:checkbox").attr("checked",false);
	}
	
	if(name == "rhijosRen"){
		if(valor == 's')
			$("#docsRen div:eq(2)").show();
		else
			$("#docsRen div:eq(2)").hide().find("input:checkbox").attr("checked",false);
	}

	if(name == "rpadresRen"){
		if(valor == 's'){
			$("#tipoServicioRen").show();
			$("#tipoServicioRen").val(1);
			$("#docsRen div:eq(3)").show().find("input:checkbox").attr("checked",false);
		}else{
			$("#tipoServicioRen").hide().val('0'); 
			$("#docsRen div:eq(3)").hide().find("input:checkbox").attr("checked",false);
		}
	}
});//end click

//Combo para documentos de padres de formulario de renovaci�n
$("#tipoServicioRen").change(function(){
	var valor=$(this).val();
	//si el combo es cero oculta el DIV y desactiva los check
	if(valor=='0'){
		$("#docsRen div:eq(3)").hide().find("input:checkbox").attr("checked",false) ;
	}else{
		$("#docsRen div:eq(3)").show().find("p").hide();
		//si el combo es 1 muestra solo el primer combo SINO todos..
		if(valor=='1'){
			$("#docsRen div:eq(3)").find("p:first").show();
		}else{
			$("#docsRen div:eq(3)").find("p").show();
		}
	}//end if
});//Fin change servicio padres


//Funcionamiento controles de documentaci�n de defunci�n
$("input[name=rfallecido]").click(function(){
	var valor = $(this).val();
	if(valor == 'b')
		$("#docsDefuncion div:eq(0)").hide().find("input:checkbox").attr("checked",false);
	else
		$("#docsDefuncion div:eq(0)").show(); // se muestra la capa de documentos de trabajador fallecido
});//end click
$("input[name=rfallecido]").attr("checked",false);
$("#docsDefuncion input:checked").attr("checked",false);
$("#docsDefuncion div").hide();


$("#tabs").tabs({
	load: function(evt, ui){
		//esto hace q causales valla a radicacion  andres 28 sep 2011
		/*if(ui.index==10){
			$(this).tabs("select",0);
		}*/
	},
	select: function(evt,ui){
		var idpersona =$('#idPersona').val();
		if(!parseInt(idpersona)>0 && ui.index!=0){
			idpersona=0;
			$('#tabs').tabs({ selected: 0 });
			alert("Debe digitar primero el documento del afiliado");
			return false;
		}
			
		
		var idempresa = new Array();
		idempresa[0]=$("#idEmpresa").val();
		if($('#select2').val()==27){
			$.ajax({
				url:URL+'phpComunes/buscarAfiliacionIDP.php',
				type: "POST",
				data: {v0:idpersona},
				async: false,
				dataType: 'json',
				success: function(datos){
					if(datos==0){
					idempresa[0]=0;
				}
				else{
					$.each(datos,function(i,fila){
						idempresa[i]=fila.idempresa;
					});
				} //fin else
				}
			})
			$(this).tabs('url', 1,URL+"aportes/trabajadores/tabConsulta/fichaTab.php?v0="+idpersona+"&flag=1");
			$(this).tabs('url', 2,URL+"aportes/trabajadores/tabConsulta/reclamosTab.php?v0="+idpersona);
			$(this).tabs('url', 3,URL+"aportes/trabajadores/tabConsulta/embargosTab.php?v0="+idpersona);
			$(this).tabs('url', 4,URL+"aportes/trabajadores/tabConsulta/aportesTab.php?v0="+idempresa[0]);
			$(this).tabs('url', 5,URL+"aportes/trabajadores/tabConsulta/planillaTab.php?v0="+idpersona);
			$(this).tabs('url', 6,URL+"aportes/trabajadores/tabConsulta/girosTab.php?v0="+idpersona);
			$(this).tabs('url', 7,URL+"aportes/trabajadores/tabConsulta/tarjetaTab.php?v0="+idpersona);
			$(this).tabs('url', 8,URL+"aportes/trabajadores/tabConsulta/movimientosTab.php?v0="+idpersona+"&flag=2")
			$(this).tabs('url', 9,URL+"aportes/trabajadores/tabConsulta/causalesTab.php?v0="+idpersona);
			$(this).tabs('url', 10,URL+"aportes/trabajadores/tabConsulta/documentosTab.php?v0="+idpersona);
			$(this).tabs('url', 11,URL+"aportes/trabajadores/contenedor.php?v0="+idpersona);
			//$(this).tabs('url', 12,URL+"aportes/radicacion/tab/personaTab.php?v0="+idpersona);
		}
		if($('#select2').val()==32 ){	//certificado
				var idpersona=$("#lisBeneficiarios option:selected").val();
				$(this).tabs('url', 1,"tab/certificadosTab.php?idp="+idpersona);
			}
		
		if($('#select2').val()==70 ){
				var idpersona=$("#idPersona").val();
				$(this).tabs('url', 1,URL+"aportes/trabajadores/tabConsulta/grupoTab.php?idp="+idpersona);
			}

	//aqui
			/*if($('#select2').val()==169){
				var nit = $('#tNit11').val();
				var cc = $('#tNumero11').val();
				var idti = $('#sltTipoIdentificacion11').val();
			}*/
			if($('#select2').val()==29){
				var idpersona=$("#idPersona").val();
				var idempresa=$("#idEmpresa").val();
				$(this).tabs('url', 1,URL+"aportes/trabajadores/tabConsulta/fichaTab.php?v0="+idpersona+"&flag="+idempresa);
				$(this).tabs('url', 2,URL+"aportes/trabajadores/tabConsulta/reclamosTab.php?v0="+idpersona);
				$(this).tabs('url', 3,URL+"aportes/trabajadores/tabConsulta/embargosTab.php?v0="+idpersona);
				$(this).tabs('url', 4,URL+"aportes/trabajadores/tabConsulta/aportesTab.php?v0="+idempresa);
				$(this).tabs('url', 5,URL+"aportes/trabajadores/tabConsulta/planillaTab.php?v0="+idpersona);
				$(this).tabs('url', 6,URL+"aportes/trabajadores/tabConsulta/girosTab.php?v0="+idpersona);
				$(this).tabs('url', 7,URL+"aportes/trabajadores/tabConsulta/tarjetaTab.php?v0="+idpersona);
				$(this).tabs('url', 8,URL+"aportes/trabajadores/tabConsulta/causalesTab.php?idp="+idpersona);
				$(this).tabs('url', 9,URL+"aportes/trabajadores/tabConsulta/documentosTab.php?v0="+idpersona);
				$(this).tabs('url', 10,URL+"aportes/trabajadores/contenedor.php?v0="+idpersona);
//				$(this).tabs('url', 12,URL+"aportes/trabajadores/tabConsulta/personaTab.php?v0="+idpersona);
			}
			//RECLAMO TRABAJADOR
			if($('#select2').val()==169 ){
				var idpersona=$("#idPersona").val();
				var idempresa=$("#idEmpresa").val();
				$(this).tabs('url', 1,URL+"aportes/trabajadores/tabConsulta/fichaTab.php?v0="+idpersona+"&flag="+idempresa);
				$(this).tabs('url', 2,URL+"aportes/trabajadores/tabConsulta/reclamosTab.php?v0="+idpersona);
				$(this).tabs('url', 3,URL+"aportes/trabajadores/tabConsulta/embargosTab.php?v0="+idpersona);
				$(this).tabs('url', 4,URL+"aportes/trabajadores/tabConsulta/aportesTab.php?v0="+idempresa);
				$(this).tabs('url', 5,URL+"aportes/trabajadores/tabConsulta/planillaTab.php?v0="+idpersona);
				$(this).tabs('url', 6,URL+"aportes/trabajadores/tabConsulta/girosTab.php?v0="+idpersona);
				$(this).tabs('url', 7,URL+"aportes/trabajadores/tabConsulta/tarjetaTab.php?v0="+idpersona);
				$(this).tabs('url', 8,URL+"aportes/trabajadores/tabConsulta/causalesTab.php?v0="+idpersona);
				$(this).tabs('url', 9,URL+"aportes/trabajadores/tabConsulta/documentosTab.php?v0="+idpersona);
				$(this).tabs('url', 10,URL+"aportes/trabajadores/contenedor.php?v0="+idpersona)
				//$(this).tabs('url', 10,URL+"aportes/trabajadores/tabConsulta/conyugeTab.php?v0="+idpersona);
				//$(this).tabs('url', 2,URL+"aportes/trabajadores/tabConsulta/personaTab.v0?idp="+idpersona);
			}
			if($('#select2').val()==170 ){
				var idempresa=$("#idEmpresa").val();
				$(this).tabs('url', 1,URL+"aportes/trabajadores/tabConsulta/aportesTab.php?v0="+idempresa);
				$(this).tabs('url', 2,"tab/reclamosEmpTab.php?v0="+idempresa);
			}
		}
	});
   // wipe all the tabs so we can programatically show the ones we want
   removeAllTabs();
   // if {condition}, then show relevant tab. this is just for example purposes
   if (true) {
      $("#tabs-1-tab").css("display","Radicaci&oacute;");
	  
   }
   // now show the tabs area
   $("#tabs").css("display","block");
}

function openTab(tabNum,closeOthers) {
   if (closeOthers != false) {
      removeAllTabs();
   }
   $("#tabs-"+tabNum+"-tab").css("display","list-item");
   $("#tabs").tabs("select",tabNum-1);
}

function removeAllTabs() {
      var i=$("#tabs").tabs("length");
		  
   for(i; i>=1; i--){//2 por q borre todos menos los 2 q estan mostrandose
	      $("#tabs").tabs("remove",i);   
	 	}
}
 
function createTab1() {
   // this will add a tab via the standard method
   $("#tabs").tabs("add",URL+"aportes/trabajadores/tabConsulta/eTarjetasTab.php","Entrega Tarjetas",1);
}
  
function createTab7(){
	$("#tabs").tabs("add",URL+"aportes/trabajadores/tabConsulta/fichaTab.php","Empresa");
   }

function createTab8(){
	$("#tabs").tabs("add",URL+"aportes/trabajadores/tabConsulta/personaTab.php","Datos Persona");
   }
   
function createTab9(){
	$("#tabs").tabs("add",URL+"aportes/trabajadores/tabConsulta/grupoTab.php","Grupo Familiar");
   }
   
function createTab10(){
	$("#tabs").tabs("add",URL+"aportes/trabajadores/tabConsulta/documentosTab.php","Documentos");
   }
   
function createTab11(){
	$("#tabs").tabs("add","certificados_6.php","Certificados");
   }   

/***** */
function createTab20(){
	$("#tabs").tabs("add",URL+"aportes/trabajadores/tabConsulta/fichaTab.php","Afiliado");
   }  
function createTab21(){
	$("#tabs").tabs("add",URL+"aportes/trabajadores/tabConsulta/personaTab.php","Persona");
   }  
function createTab22(){
	$("#tabs").tabs("add",URL+"aportes/trabajadores/tabConsulta/fichaTab.php","Movimientos");
   }  
function createTab23(){
	$("#tabs").tabs("add",URL+"aportes/trabajadores/tabConsulta/grupoTab.php","Grupo");
   }  
function createTab24(){
	$("#tabs").tabs("add",URL+"aportes/trabajadores/tabConsulta/embargosTab.php","Descuentos");
   }  
function createTab25(){
	$("#tabs").tabs("add",URL+"aportes/trabajadores/tabConsulta/aportesTab.php","Aportes");
   }                  
function createTab26(){
	$("#tabs").tabs("add",URL+"aportes/trabajadores/tabConsulta/planillaTab.php","Planilla");
   }    
function createTab27(){
	$("#tabs").tabs("add",URL+"aportes/trabajadores/tabConsulta/girosTab.php","Subsidio");
   }    
function createTab28(){
	$("#tabs").tabs("add",URL+"aportes/trabajadores/tabConsulta/tarjetaTab.php","Cargues");
   }   
function createTab29(){
	$("#tabs").tabs("add",URL+"aportes/trabajadores/tabConsulta/causalesTab.php","Causales");
   }    
function createTab30(){
	$("#tabs").tabs("add",URL+"aportes/trabajadores/tabConsulta/documentosTab.php","Documentos");
   }                
//AÑADIR TAB PARA DOCUMENTOS
function createTab31(){
	$("#tabs").tabs("add",URL+"aportes/trabajadores/tabConsulta/docTrabajador.php","Documentos Afiliado");
   }          
    
function createTab32(){
	$("#tabs").tabs("add",URL+"aportes/trabajadores/tabConsulta/nuevoBeneficiarioTab.php","Nuevo Beneficiario");
   }       	  
	  
function createTab33(){
	$("#tabs").tabs("add",URL+"aportes/trabajadores/tabConsulta/reclamosTab.php","Reclamos");
   }    

function createTab34(){
	$("#tabs").tabs("add",URL+"aportes/trabajadores/tabConsulta/conyugeTab.php","Conyuge");
   }       

function createTab35(){
	$("#tabs").tabs("add",URL+"aportes/trabajadores/tabConsulta/reclamosEmpTab.php","Reclamos");
   }   
       	  
function createTab36(){
	$("#tabs").tabs("add",URL+"aportes/trabajadores/tabConsulta/certificadosTab.php","Certificados");
   }   		  
  //FIN AÑADIR TAB  
/*
function removeTab2() {
   $("#tabs").tabs("remove",$("#tabs").tabs("length")-1);
   $("#tabs").tabs("add","afiTrabajador_2.php","Afiliacion Trabajador");
}

function removeTab5() {
   // this will add a tab via the standard method. part of the problem here is I don't know the index...just assuming its on the end
   $("#tabs").tabs("remove",$("#tabs").tabs("length")-1);
   // reselect the 2nd tab or it will show tab 3
   $("#tabs").tabs("select",1);
}
*/

/**
 * @author tiogg
 * 05-08-2011
 */
function mostrarTrayectoriasPersona(persona){
	$.ajax({
		url: '../../phpComunes/buscarHistoricoFonede.php',
		async: false,
		dataType: 'json',
		data: ({idPersona:persona.idpersona}),
		success: function(datos){
			if(datos==0){
				//MENSAJE("No existe en nuestra base de datos, postulaci�n sin vinculaci�n");
				return false;
			}else{
				
				for(var c=0; c<datos.length; c++){
					var trayectoria = datos[c];0
					var htmlTr = "<tr><td>"+ (c+1) +"</td>" +
					 "<td>"+ trayectoria.razonsocial +"</td>" +
					 "<td>"+ trayectoria.fechaingreso +"</td>" +
					 "<td>"+ trayectoria.fecharetiro +"</td>" +
					 "<td>"+ trayectoria.dias +"</td>" +
					 "<td>"+ trayectoria.telefono +"</td></tr>";
					 $("#tbody_tabla_hist_laboral").append(htmlTr);
				}
			}
		}
	});
}

function calcularTiempoFonede(trayecorias){
	var x = '';
	var y = $('#txtFechaFinal').val();
	var arr1 = x.split('/');
	var arr2 = y.split('/');

	if(!arr1 || !arr2 || arr1.length != 3 || arr2.length != 3) {
		return;
	}
	 
	var dt1 = new Date();
	dt1.setFullYear(arr1[2], arr1[0], arr1[1]);
	var dt2 = new Date();
	dt2.setFullYear(arr2[2], arr2[0], arr2[1]);
	var tiempodias = (dt2.getTime() - dt1.getTime()) / (60 * 60 * 24 * 1000);
	tiempodias = parseInt(tiempodias);
		
	
	if(tiempodias <= 0){
		$('#txtFechaInicial, #txtFechaFinal').addClass('ui-state-error');
		return false;
	}else{
		$('#txtFechaInicial, #txtFechaFinal').removeClass('ui-state-error');
		$('#txtTiempo').val(tiempodias);
		
		var tt = 0;
		$('input.tiempo').each(function(){
			tt += parseInt($(this).val());
		});
		
		$('#txtTiempoTotal').val(tt);
	}
}

/*function recalcularTiempoFonede(tiempoRestar, idFilaBorrar){
	var restar = (tiempoRestar == null)? 0 : parseInt(tiempoRestar);
	var trBorrar = (idFilaBorrar == null)?'':idFilaBorrar;
	var mensajeVinculacion = '';
	var totalAfiliacionesCaja = (isNaN($("#totalTiempoAplicarFonede").val()))? 0 : parseInt($("#totalTiempoAplicarFonede").val()); // tiempo total para aplicar a fonede con las trayectorias de COMFAMILIAR HUILA
	
	if(totalAfiliacionesCaja > 0)
		mensajeVinculacion = "\nPuede realizar la postulaci�n CON VINCULACI\u00D3N.";
	
	var totalAfiliacionesOtrasCajas = 0; // n�mero de d�as que la persona ha estado afiliada con otras cajas distintas de comfamiliar huila

	var arrOtrasCajas = [];
	var tiempoMenor;
	var tiempoMayor;
	$('span[id^="span_tiempo_otras_cajas_"]').each(function(i,span){
		var tiempo = $("#"+span.id).text();
		totalAfiliacionesOtrasCajas = totalAfiliacionesOtrasCajas + parseInt(tiempo);
		arrOtrasCajas[i] = parseInt(tiempo);
	});

	return a-b;
	if(trBorrar != ''){
		$(trBorrar).remove();
	}

	var totalAfiliacionesParaAplicar = totalAfiliacionesCaja + totalAfiliacionesOtrasCajas;

	if(totalAfiliacionesParaAplicar > 0 && restar > 0){
		totalAfiliacionesParaAplicar = (totalAfiliacionesParaAplicar - restar);
	}

	$("#span_tiempo_total_fonede").text(totalAfiliacionesParaAplicar);
	if(totalAfiliacionesParaAplicar > 0){
		mensajeVinculacion = "\nPuede realizar la postulaci�n CON VINCULACION.";
		$("#si_convinculacion_fonede").attr('checked',true);
		$("#docs_fonede div:eq(1)").show()
	}else{
		mensajeVinculacion = "\nPuede realizar la postulaci�n SIN VINCULACION.";
		$("#no_convinculacion_fonede").attr('checked',true);
		$("#docs_fonede div:eq(1)").hide();
	}
	
	alert("El afiliado puede postularse a FONEDE."+ mensajeVinculacion);
	return totalAfiliacionesParaAplicar;
	
}*/

/**
 * Calcular tiempo laborado en di�logo de agregaci�n de trayectorias con otras cajas (FONEDE)
 *
 */
var tiempoLaborado = function(){
	var fl = $(this).parents('tr').attr('id');
	var x = $('#fecha_ingr_otra_caja').val();
	var y = $('#fecha_ret_otra_caja').val();
	
	var regexpFechas = /^(|(0[1-9])|(1[0-2]))\/((0[1-9])|(1\d)|(2\d)|(3[0-1]))\/((\d{4}))$/;
	if(!x.match(regexpFechas) || !y.match(regexpFechas)){
		$("#fecha_ingr_otra_caja").addClass('ui-state-error');
		$("#fecha_ret_otra_caja").addClass('ui-state-error');
	}else{
		$("#fecha_ingr_otra_caja").removeClass('ui-state-error');
		$("#fecha_ret_otra_caja").removeClass('ui-state-error');
	}
	
	
	var arr1 = x.split('/');
	var arr2 = y.split('/');

	if(!arr1 || !arr2 || arr1.length != 3 || arr2.length != 3) {
		return;
	}
	 
	var dt1 = new Date();
	dt1.setFullYear(arr1[2], arr1[0]-1, arr1[1]);
	var dt2 = new Date();
	dt2.setFullYear(arr2[2], arr2[0]-1, arr2[1]);
	
	var fechaHoy = new Date();
	var marcaTiempoHoy = fechaHoy.getTime(); // en milisegundos
	var fechaHace3Agnos = new Date();
	fechaHace3Agnos.setFullYear(fechaHoy.getFullYear()-3, fechaHoy.getMonth(), fechaHoy.getDate(), 0, 0, 0, 0);
	var marcaTiempoHace3Agnos = fechaHace3Agnos.getTime();

	var marcaTiempoDt1 = dt1.getTime();
	var marcaTiempoDt2 = dt2.getTime();
	if(marcaTiempoHoy < marcaTiempoDt2 || marcaTiempoHoy < marcaTiempoDt1){
		$('#fecha_ingr_otra_caja, #fecha_ret_otra_caja').addClass('ui-state-error');
		tt = 0;
		$('#txtTiempo').val(tt);
		return false;
	}

	if((marcaTiempoDt1 <= marcaTiempoHace3Agnos && marcaTiempoDt2 >= marcaTiempoHace3Agnos) ||
	   (marcaTiempoDt1 >= marcaTiempoHace3Agnos && marcaTiempoDt2 >= marcaTiempoHace3Agnos) ||
	   (marcaTiempoDt2 >= marcaTiempoHace3Agnos)
	  ){
		
		var marcaNivelInferior = (marcaTiempoDt1 <= marcaTiempoHace3Agnos)? marcaTiempoHace3Agnos : marcaTiempoDt1;
		var marcaNivelSuperior = (marcaTiempoDt2 >= marcaTiempoHoy)? marcaTiempoHoy : marcaTiempoDt2;
//console.log("marca tiempo hoy: "+ marcaTiempoHoy);
//console.log("marcas inf. y sup.: "+ marcaNivelInferior +", "+ marcaNivelSuperior);		
		var tiempodias = (marcaNivelSuperior - marcaNivelInferior) / (60 * 60 * 24 * 1000);
		tiempodias = tiempodias;
		
		/*var tiempodias = (dt2.getTime() - dt1.getTime()) / (60 * 60 * 24 * 1000);
		tiempodias = parseInt(tiempodias);*/
	}else{
		tiempodias = 0;
	}
	
//console.log("tiempo en d�as: "+ tiempodias);	
	
	if(tiempodias <= 0){
		$('#fecha_ingr_otra_caja, #fecha_ret_otra_caja').addClass('ui-state-error');
		tt = 0;
		$('#txtTiempo').val(tt);
		return false;
	}else{
		$('#fecha_ingr_otra_caja, #fecha_ret_otra_caja').removeClass('ui-state-error');
		$('#txtTiempo').val(tiempodias);
		
		var tt = 0;
		$('input.tiempo').each(function(){
			tt += parseInt($(this).val());
		});
		//$('#txtTiempoTotal').val(tt);
	}
};

/**
 * Valida si el campo que ejecuta el m�todo est� vac�o
 * @returns
 */
var tieneCamposVacios = function(){
	var valor = $.trim($(this).val());
	if(valor.length == 0){
		$(this).addClass('ui-state-error');
		return true;
	}else{
		if($(this).hasClass('ui-state-error')){
			$(this).removeClass('ui-state-error');
		}
		return false;
	}
};

var buscarCajas = function(request, response) {
	if ( request.term in cache ) {
		response( cache[ request.term ] );
		return;
	}
	request.tipo = 'CCF';
	$.ajax({
		url: "../../fonede/proc/buscarAdministradora.php",
		dataType: "json",
		data: request,
		success: function( data ) {
			cache[ request.term ] = data;
			response( data );
		}
	});
};

/**
 * Validaci�n de campos auto-complete para el di�logo de trayectorias en otras cajas
 * @returns
 */
var validarAutoCompletar = function(){
	var valHdn = $(this).nextAll('input[type="hidden"]').val();
	if(valHdn.length == 0){
		$(this).addClass('ui-state-error');
	}else{
		if($(this).hasClass('ui-state-error')){
			$(this).removeClass('ui-state-error');
		}
	}
};


function vertab(obj){
	limpiarCampos2(15);	//Esto es para que borre todos los campos tanto visibles como ocultos cada vez que se cambia de tipo de radicacion
	if(nuevo==0){
		alert('Haga click primero en Nuevo!');
		obj.value="0";
		return false;
	}
	
	
	var op = parseInt(obj.value);
	removeAllTabs();
	ocultarDiv();
	//$('#tabTableGrupo tr:not(:first)').remove();
//	$('#div1,#div2,#div3,#div4,#div5,#div6,#div7,#div8,#div9,#div10,#div11,#div12,#div13,#div14,#div15,#div16,#div17').css({display:"none"});
	switch (op){
		/** INFORMACION */
		case 27 : 
			//document.getElementById('div1').style.display='block';
			if($('#tExiste').val()==1){
				createTab20(); 
				//createTab21(); 
				createTab33(); 
				createTab24(); 
				createTab25(); 
				createTab26();
				createTab27();
				createTab28();
				createTab22();   //este
				createTab29();//causales
				createTab30();
				createTab34();
			}
			$('#div1').css({display:"block"});
		break;
		/** AFILIACION TRABAJADOR DEPENDIENTE */
		case 28 : 
			//createTab31();
			// inicializar las variables globales que corresponden al nombre corto
			txt = pn = sn = pa = sa = "";
			$('#sltTipoIdentificacion2').html($('#select option').clone());
			$('#div2').css({display:"block"});
			$('#sltTipoIdentificacion2').val(tempo1);
			$('#tNumero2').val(tempo2);
				
		break;
		/** RENOVACION TRABAJADOR */
		case 29 : 			//renovacion trabajador
			createTab20(); 
			//createTab21(); 
			createTab33(); 
			createTab24(); 
			createTab25(); 
			createTab26();
			createTab27();
			createTab28();
			createTab29();
			createTab30();
			createTab34();
			$('#sltTipoIdentificacion3').html($('#select option').clone());
			$('#div3').css({display:"block"});
			$('#sltTipoIdentificacion3').val(tempo1);
			$('#tNumero3').val(tempo2);
			$("#docsRen input:checked").attr("checked",false);
		break;
		/** AFILIACION EMPRESA */
		case 30 : 
			$('#div4').css({display:"block"});
			$("#docsAfiEmpresa input:checked").attr("checked",false);
			$("#juridica").trigger("click");
		break;
		/** RENOVACION EMPRESA */
		case 31 :
			limpiarVariables();	//Restaura el contenido de las variables que se usan en esta radicacion
			$('#div5').css({display:"block"});
		break;
		/** CERTIFICADOS */
		case 32 : //certificados
			createTab36();
			$("#lisBeneficiarios option:not(':first')").remove();
			$('#sltTipoIdentificacion6').html($('#select option').clone());
			$('#div6').css({display:"block"});
			$('#sltTipoIdentificacion6').val(tempo1);
			$('#tNumero6').val(tempo2);		
		break;
		/** NOVEDAD EMPRESA */
		case 33 : 
			$('#sltTipoIdentificacion7').html($('#select option').clone());
			$('#div7').css({display:"block"});
		break;
		/** POSTULACION A FONEDE */
		case 69 : 
			$('#sltTipoIdentificacion8').html($('#select option').clone());
			$('#sltTipoIdentificacion8').val(tempo1);
			$('#txtIdentificacion').val($('#textfield22').val());
			verDatosFormularioFonede();
		break;
		/** AFILIACION GRUPO FAMILIAR */
		case 70 : 
			$('#sltTipoIdentificacion9').html($('#select option').clone());
			$('#sltTipoIdentificacion9').val(tempo1);
			$('#sltTipoIdentificacion91').html($('#select option').clone());
			$('#tNumero9').val($('#textfield22').val());
			$('#div9').css({display:"block"});
			createTab23();			
		break;
		/** NUEVA SUCURSAL */
		case 105 :
			$('#div10').css({display:"block"});
		break;
		/** RECLAMO TRABAJADOR NO GIRO */
		case 169 :		//reclamo no giro trabajador
			createTab20(); 
			//createTab21(); 
			createTab33(); 
			createTab24(); 
			createTab25(); 
			createTab26();
			createTab27();
			createTab28();
			createTab29();
			createTab30();
			createTab34();
			$('#sltTipoIdentificacion11').html($('#select option').clone());
			$('#div11').css({display:"block"});
			$('#sltTipoIdentificacion11').val(tempo1);
			$('#tNumero11').val(tempo2);
		break;
		/** RECLAMO EMPRESA NO GIRO */
		case 170 :		//reclamo no giro empresa
			createTab25(); 
			createTab35();
			$('#div12').css({display:"block"});		
		break;
		/** DEFUNCION */
		case 183 : 
			$('#sltTipoIdentificacion13').html($('#select option').clone());
			$('#tNumero13').val($('#textfield22').val());
			$('#div13').css({display:"block"});
			$("#docsDefuncion input:checked").attr("checked",false);
			$("#docsDefuncion div").hide();
		break;
		/** DISOLUCION CONVIVENCIA */
		case 191 :
			$('#sltTipoIdentificacion14').html($('#select option').clone());
			$('#sltTipoIdentificacion14').val(tempo1);
			$('#tNumero14').val($('#textfield22').val());
			$('#div14').css({display:"block"});
			$("#conyuge tr:not(:first)").remove();
		break;
		/** AFILIACIONES MULTIPLES TRABAJADOR */
		case 192 : $('#div15').css({display:"block"});
		break;
		/** BLOQUEO TARJETA */
		case 195:
			//NOVEDAD TARJETA
			$("#div20").css({display:"block"});
			$("#div20 input:checked").attr("checked",false);
			$("#buscarPorNovedad").val(0).trigger("change");
			$("[name=novedad]").attr("disabled",true);
			$("#lnombrenovedad,#lbononovedad,#lestadonovedad,#lfechasolnovedad,#lsaldonovedad,#lubicacionnovedad").html(" ");
			$("#numero5,#tipoINovedad,#txtBonoNovedad").val('');
			$("#tr_verifica_afiliado").hide();
			$("#docsNovedadTarjeta input:checked").attr("checked",false);
			$("#docsNovedadTarjeta p").hide();
		break;
		/** CREAR CONVIVENCIA */
		case 211://crear convivencia
			$('#sltTipoIdentificacion17').html($('#select option').clone());
			$('#sltTipoIdentificacion17').val(tempo1);
			$('#sltTipoIdentificacion17A').html($('#select option').clone());
			$('#tNumero17').val($('#textfield22').val());
			$("#div17").css({display:"block"});
			$("#docsCrearConvivencia input:checked").attr("checked",false);
		break;
		/** EMBARGO */
		case 2919://embargos
			$('#sltTipoIdentificacion18').html($('#select option').clone());
			$('#sltTipoIdentificacion18').val(tempo1);
			$('#tNumero18').val($('#textfield22').val());
			$("#div18").css({display:"block"});
		break;
		/** AFILIACION TRABAJADOR INDEPENDIENTE */
		case 2926 :	//independiente
			//$('#sltTipoIdentificacion19').html($('#select option').clone());
			$('#sltTipoIdentificacion19').val(tempo1);
			$('#tNumero19').val($('#textfield22').val());
			$("#exento12").attr("disabled",false);
			$("#exento12").val(0);
			$("#porcentaje12").attr("disabled",false);
			$("#porcentaje12").val(0);
			$('#condicionAfiliacion').hide();
			$("#p3_ind input:checkbox").attr("checked",false);
			$("#p3_ind").hide();
			$("#div19").css({display:"block"});
		break;
		/** No existe esta Opcion */
		case 3333:
			//BLOQUEO TARJETA
			$("#div16").css({display:"block"});
			$("#lnombre,#lbono,#lestado,#lfechasol,#lsaldo").html('');
			$("#buscarPor,#tipoI,#numero4,#txtBono,#codBloqueo").val('');
			$("#tnumero,#tbono").hide();
			$("#docsBloqueoTarjeta input:checked").attr("checked",false);
		break;
	} 
}

	


/*	
27	6	A	INFORMACION
28	6	B	AFILIACION TRABAJADOR
29	6	C	RENOVACION TRABAJADOR
30	6	D	AFILICION EMPRESA
31	6	E	RENOVACION EMPRESA
105	6	F	NUEVA SUCURSAL
32	6	G	CERTIFICADOS
33	6	H	ENTREGA TARJETAS
191	6	I	DISOLUCION CONVIVENCIA
69	6	I	AFILIACION A FONEDE
70	6	J	INSCRIPCION BENEFICIARIOS
169	6	K	RECLAMO TRABAJADOR NO GIRO
170	6	M	RECLAMO EMPRESA NO GIRO
183	6	N	DEFUNCION
192	6	O	AFILIACIONES MULTIPLES TRABAJADOR	
195 6   P   BLOQUEO TARJETAS
211 6   Q   CREAR CONVIVENCIA
*/