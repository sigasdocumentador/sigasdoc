
function llenarCampos(idp){
	$.getJSON("llenarCampos.php", {v0:idp}, function(data){
		$.each(data,function(i,fila){
			$("#txtTipoD").val(fila.idtipodocumento);
			$("#txtNumero").val(fila.identificacion);
			$("#txtPNombre").val(fila.pnombre);
			$("#txtSNombre").val(fila.snombre);
			$("#txtPApellido").val(fila.papellido); 
			$("#txtSApellido").val(fila.sapellido);
			$("#txtSexo").val(fila.sexo);
			$("#txtDireccion").val(fila.direccion);
			$("#txtBarrio").val(fila.idbarrio);
			$("#txtTelefono").val(fila.telefono);
			$("#txtCelular").val(fila.celular);
			$("#txtEmail").val(fila.email);
			$("#txtCasa").val(fila.idpropiedadvivienda);
			$("#txtTipoV").val(fila.idtipovivienda);
			$("#txtDptR").val(fila.iddepresidencia);
			$("#txtCiudadR").val(fila.idciuresidencia);
			$("#txtZona").val(fila.idzona);
			$("#txtEstado2").val(fila.idestadocivil);
			$("#txtFechaN").val(fila.fechanacimiento);
			var arrFecha = fila.fechanacimiento.split("-");
			var fechaNacimiento = arrFecha[1]+"\/"+arrFecha[2]+"\/"+arrFecha[0];
			var edad = calcular_edad(fechaNacimiento);
			if($("#span_edad"))
				$("#span_edad").html(edad);
			$("#txtDptoN").val(fila.iddepnace);
			$("#txtCiudadN").val(fila.idciunace);
			$("#txtCapacidad").val(fila.capacidadtrabajo);
			$("#txtNombreC").val(fila.nombrecorto);
		});
	});
}
	
function actualizar(idp){
	var v0=idp;
	var v1= $("#txtTipoD").val();			//idtipodocumento
	var v2= $("#txtNumero").val();			//identificacion
	var v3=	$("#txtPApellido").val(); 		//papellido
	var v4=	$("#txtSApellido").val();		//sapellido
	var v5=	$("#txtPNombre").val();			//pnombre
	var v6=	$("#txtSNombre").val();			//snombre
	var v7=	$("#txtSexo").val();			//sexo
	var v8=	$("#txtDireccion").val();		//direccion
	var v9=	$("#cboBarrio").val();			//idbarrio
	var v10= $("#txtTelefono").val();		//telefono
	var v11= $("#txtCelular").val();		//celular
	var v12= $("#txtEmail").val();			//email
	var v13= $("#txtCasa").val();			//idpropiedadvivienda
	var v14= $("#txtTipoV").val();			//idtipovivienda
	var v15= $("#txtDptR").val();			//iddepresidencia
	var v16= $("#txtCiudadR").val();		//idciuresidencia
	var v17= $("#txtZona").val();			//idzona
	var v18= $("#txtEstado2").val();			//idestadocivil
	var v19= $("#txtFechaN").val();			//fechanacimiento
	var v20= $("#txtDptoN").val();			//iddepnace
	var v21= $("#txtCiudadN").val();		//idciunace
	var v22= $("#txtCapacidad").val();		//capacidadtrabajo
	var v23= $("#txtNombreC").val();		
/*	
	idtipodocumento=0, identificacion='', papellido='', sapellido='', pnombre='', snombre='', sexo='', direccion='', idbarrio=0, telefono='', celular='', email='', idpropiedadvivienda=0, idtipovivienda='', idciuresidencia='', iddepresidencia='', idzona='', idestadocivil=0, fechanacimiento='2011-1-17', idciunace='', iddepnace='', capacidadtrabajo='', desplazado='', discapacitado='', reinsertado='', idescolaridad=0, idprofesion=0, conyuges=0, hijos=0, hijosmenores=0, hermanos=0, padres=0, tarjeta='', ruaf='', biometria='', rutadocumentos='', fechaafiliacion='2011-1-17', estado='', validado='', flag='', tempo1='', tempo2='', tempo3='', usuario='', fechasistema='2011-1-17', nombrecorto=''
	*/
	if(v1==0){
		alert("Falta tipo documento");
		return false;
		}
	if(v2.length==0){
		alert("Falta numero documento");
		return false;
		}
	if(v3.length==0){
		alert("Falta Nombre");
		return false;
		}
	if(v5.length==0){
		alert("Falta Apellido");
		return false;
		}
	if(v7==0){
		alert("Falta Sexo");
		return false;
		}
	if(v8.length==0){
		alert("Falta direccion");
		return false;
		}
	if(v9==0){
		alert("Falta Barrio");
		return false;
		}
	if(v13==0){
		alert("Falta tipo propiedaad");
		return false;
		}
	if(v14==0){
		alert("Falta tipo de vivienda");
		return false;
		}
	if(v15==0){
		alert("Falta departamento residencia");
		return false;
		}
	if(v16==0){
		alert("Falta ciudad de residencia");
		return false;
		}
	if(v17==0){
		alert("Falta zona");
		return false;
		}
	if(v19.length==0){
		alert("Falta fecha de nacimiento");
		return false;
		}
	if(v20==0){
		alert("Falta departamento donde nacio");
		return false;
		}
	if(v21==0){
		alert("Falta ciudad donde nacio");
		return false;
		}
	$.post(URL+'phpComunes/actualizarPersona.php',{v0:v0,v1:v1,v2:v2,v3:v3,v4:v4,v5:v5,v6:v6,v7:v7,v8:v8,v9:v9,v10:v10,v11:v11,v12:v12,v13:v13,v14:v14,v15:v15,v16:v16,v17:v17,v18:v18,v19:v19,v20:v20,v21:v21,v22:v22},function(data){
		if(data==0){
			alert("El registro no se pudo actualizar!")
			}
		else{
			alert("El registro fue actualizado!");
			}
		})
	}