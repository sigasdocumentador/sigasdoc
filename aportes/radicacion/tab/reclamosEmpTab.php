<?php
/* autor:       Nelsn Mauricio Arias
 * fecha:       13/04/2012
 * objetivo:    
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;


include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR .'p.reclamos.class.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';
$idp=$_REQUEST['v0'];
//$idp=85818;
//$ide=31368;
$objReclamos=new Reclamos();
$result=$objReclamos->buscar_reclamos_empresa($idp);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Documento sin título</title>
<link href="<?php echo URL_PORTAL; ?>css/sorterStyle.css" rel="stylesheet" type="text/css" media="screen" />
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/jquery.tablesorter.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/jquery.tablesorter.pager.js"></script>
<script type="text/javascript">
$(document).ready(refrescar);
function refrescar(){
  //$("#table2 tbody tr").remove();	
  $("#table2").tablesorter();/*.tablesorterPager({container: $("#pager")})*/;
  $("#table2").trigger("update");
  $("#table2 tbody tr:even").addClass("evenrow");
}
 
</script>
</head>
<body>
<h4>Reclamos</h4>
<table width="100%" border="0" cellspacing="0" class="sortable hover" id="table2">
<thead>
<tr>
<th width="10%" align="center" class="head"><h3><strong>Fecha</strong></h3></th>
<th width="11%" align="center" class="head"><h3><strong>P inicio</strong></h3></th>
<th width="11%" align="center" class="head"><h3><strong>P final</strong></h3></th>
<th width="9%" align="center"  class="head"><h3><strong>Estado</strong></h3></th>
<th width="14%" align="center" class="head"><h3><strong>Fecha Giro</strong></h3></th>
<th width="45%" align="center" class="head"><h3><strong>Causal</strong></h3></th>
</tr>
</thead>
<tbody>
<?php 
while($consulta=mssql_fetch_array($result)){
?>
<tr>
         <td><?php echo $consulta['fechareclamo'];  ?></td>
         <td><?php echo $consulta['periodoinicial'];  ?></td>
         <td><?php echo $consulta['periodofinal'];  ?></td>
         <td><?php echo $consulta['estado'];  ?></td>
         <td><?php echo $consulta['fechagiro'];  ?></td>
         <td><?php echo $consulta['detalledefinicion'];  ?></td>
     </tr>
  <?php }?>
  </tbody>
</table>
<div id="div-registros">N&uacute;mero de Registros: 
<select name="nRegistros" id="nRegistros" onchange="refrescar();">
	<option selected="selected"  value="10" >10</option>
	<option value="20">20</option>
	<option value="30">30</option>
	<option  value="40">40</option>
    <option  value="50">50</option>
</select>
</div>        
</body>
</html>