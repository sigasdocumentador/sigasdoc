<?php
/* autor:       orlando puentes
 * fecha:       17/08/2010
 * objetivo:    Procesar los movimientos de los Bonos almacenados en la base de datos. 
 */
ini_set("display_errors",'1'); 
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$url=$_SESSION['URL'];
$idp= $_REQUEST['v0'];
include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'p.definiciones.class.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'ciudades.class.php';
$objDefiniciones=new Definiciones();
$objCiudad=new Ciudades();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"  />
<title>Documento sin t�tulo</title>
<script type="text/javascript" src="<?php echo $url; ?>js/jquery-1.4.2.js"></script>
<script type="text/javascript" src="<?php echo $url; ?>js/formularios/ui/ui.core.js"></script>
<script type="text/javascript" src="<?php echo $url; ?>js/formularios/ui/ui.resizable.js"></script>
<script type="text/javascript" src="<?php echo $url; ?>js/formularios/ui/ui.dialog.js"></script>
<script type="text/javascript" src="<?php echo $url; ?>js/formularios/ui/jquery.ui.widget.js"></script>
<script type="text/javascript" src="<?php echo $url; ?>js/formularios/ui/ui.datepicker.js"></script>
<script type="text/javascript" src="<?php echo $url; ?>js/formularios/ui/effects.core.js"></script>
<script type="text/javascript" src="<?php echo $url; ?>js/formularios/ui/ui.button.js"></script>
<script type="text/javascript" src="<?php echo $url; ?>js/comunes.js"></script>
<script type="text/javascript" src="<?php echo $url; ?>js/direccion.js"></script>
<script type="text/javascript" src="<?php echo $url; ?>js/jquery.combos.js"></script>
<script type="text/javascript" src="<?php echo $url; ?>js/jquery.fechaLarga.js"></script>
</head>
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
  <td width="13" height="29" background="<?php echo $url; ?>imagenes/tabla/arriba_izq.gif">&nbsp;</td>
  <td background="<?php echo $url; ?>imagenes/tabla/arriba_central2.gif"><span class="letrablanca">::&nbsp;Actualizaci&oacute;n Datos Afiliado&nbsp;::</span></td>
  <td width="13" background="<?php echo $url; ?>imagenes/tabla/arriba_der.gif" align="right">&nbsp;</td>
</tr>
  <tr>
	<td background="<?php echo $url; ?>imagenes/tabla/izquierda2.jpg">&nbsp;</td>
	<td background="<?php echo $url; ?>imagenes/tabla/centro.gif">
	<img src="<?php echo $url; ?>imagenes/tabla/spacer.gif" width="1" height="1">
	<img src="<?php echo $url; ?>imagenes/spacer.gif" width="1" height="1"/>
    <img src="<?php echo $url; ?>imagenes/spacer.gif" width="1" height="1"/>
    <img src="<?php echo $url; ?>imagenes/spacer.gif" width="1" height="1"/>
	<img src="<?php echo $url; ?>imagenes/menu/modificar.png" title="Actualizar" width="16" height="16" onClick="actualizar(<?php echo $idp; ?>);" style="cursor:pointer">
	</td>
    <td background="<?php echo $url; ?>imagenes/tabla/derecha.gif">&nbsp;</td>
  </tr>
  <tr>
    <td background="<?php echo $url; ?>imagenes/tabla/izquierda2.jpg">&nbsp;</td>
    <td align="left" background="<?php echo $url; ?>imagenes/tabla/centro.gif"><div id="error" style="color:#FF0000"></div></td>
    <td background="<?php echo $url; ?>imagenes/tabla/derecha.gif">&nbsp;</td>
  </tr>
  <tr>
    <td background="<?php echo $url; ?>imagenes/tabla/izquierda2.jpg">&nbsp;</td>
    <td align="center" background="<?php echo $url; ?>imagenes/tabla/centro.gif">
	<table width="100%" border="0" cellspacing="0" class="tablero">
      <tr>
        <td width="17%">Id Persona</td>
        <td width="29%"><input name=txtId class=boxfecha id="txtId" readonly="readonly" /></td>
        <td width="17%">Fecha</td>
        <td width="37%"><input name=txtId2 class="box1" id="txtId2" readonly="readonly" /></td>
        </tr>
      <tr>
      <td>Tipo Documento</td>
        <td><label>
          <select name="txtTipoD" id="txtTipoD" class="box1">
            <option value="0" selected="selected">Seleccione...</option>
            <?php
		$defin = $objDefiniciones->mostrar_datos(1,1);
		while($r_defi=mssql_fetch_array($defin)){
		echo "<option value=".$r_defi['iddetalledef'].">".$r_defi['detalledefinicion']."</option>";
			}
        ?>
          </select>
          <img src="<?php echo $url; ?>imagenes/menu/obligado.png" width="12" height="12" /></label></td>
        <td>N&uacute;mero</td>
        <td><input name="txtNumero" type="text" class="box1" id="txtNumero" />         
         <img src="<?php echo $url; ?>imagenes/menu/obligado.png" width="12" height="12"  /></td>
        </tr>
      <tr>
      <td>Primer Nombre</td>
        <td><input name="txtPNombre" type="text" class="box1" id="txtPNombre" />
          <img src="<?php echo $url; ?>imagenes/menu/obligado.png" width="12" height="12" /></td>
        <td>Segundo Nombre</td>
        <td><input name="textfield3" type="text" class="box1" id="txtSNombre" /></td>
        </tr>
	  <tr>
	    <td>Primer Apellido</td>
	    <td><input name="textfield2" type="text" class="box1" id="txtPApellido" />
	      <img src="<?php echo $url; ?>imagenes/menu/obligado.png" width="12" height="12" /></td>
	    <td>Segundo Apellido</td>
	    <td><input name="txtSApellido" type="text" class="box1" id="txtSApellido" /></td>
	    </tr>
	  <tr>
	    <td>Sexo</td>
	    <td><select name="txtSexo" class="box1" id="txtSexo">
        <option value="0" selected="selected">Seleccione...</option>
	      <option value="M">Masculino</option>
	      <option value="F">Femenino</option>
	      </select> <img src="<?php echo $url; ?>imagenes/menu/obligado.png" width="12" height="12" /></td>
	    <td>Estado Civil</td>
	    <td><select name="txtEstado2" id="txtEstado2" class="box1">
	      <option value="0" selected="selected">Seleccione...</option>
	      <?php
		$defin = $objDefiniciones->mostrar_datos(10,1);
		while($r_defi=mssql_fetch_array($defin)){
			if($r_defi['iddetalledef']!=54){
		echo "<option value=".$r_defi['iddetalledef'].">".$r_defi['detalledefinicion']."</option>";
			}
			}
        ?>
	      </select><img src="<?php echo $url; ?>imagenes/menu/obligado.png" width="12" height="12" /></td>
	    </tr>
	  <tr>
	    <td>Tel&eacute;fono</td>
	    <td><input name="txtTelefono" type="text" class="box1" id="txtTelefono" /></td>
	    <td>Celular</td>
	    <td><input name="txtCelular" type="text" class="box1" id="txtCelular" /></td>
	    </tr>
        <tr>
	    <td>E-mail</td>
	    <td><input name="txtEmail" type="text" class="box1" id="txtEmail" /></td>
	    <td>&nbsp;</td>
	    <td>&nbsp;</td>
	    </tr>
	  <tr>
	    <td >Depto Reside</td>
	    <td ><select name="cboDepto" id="cboDepto" class="box1">
	      <option value="0" selected="selected">Seleccione...</option>
	      <?php
		$consulta = $objCiudad->departamentos();
		while($r_defi=mssql_fetch_array($consulta)){
		echo "<option value=".$r_defi['coddepartamento'].">".utf8_encode($r_defi['departmento'])."</option>";
			}
        ?>
	      </select><img src="<?php echo $url; ?>imagenes/menu/obligado.png" width="12" height="12" /></td>
	    <td >Ciudad Reside</td>
	    <td ><select name="cboCiudad" id="cboCiudad" class="box1">
	      <option value="0" selected="selected">Seleccione...</option>
	      </select><img src="<?php echo $url; ?>imagenes/menu/obligado.png" width="12" height="12" /></td>
	    </tr>
	  <tr>
	    <td >Zona Reside</td>
	    <td ><select name="cboZona" id="cboZona" class="box1">
	      <option value="0" selected="selected">Seleccione...</option>
	      </select><img src="<?php echo $url; ?>imagenes/menu/obligado.png" width="12" height="12" /></td>
	    <td >Barrio</td>
	    <td ><select name="cboBarrio" id="cboBarrio" class="box1">
	      <option value="0" selected="selected">Seleccione...</option>
	      </select><img src="<?php echo $url; ?>imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
	    </tr>
	    <!-- onfocus="direccion(this,document.getElementById('txtTipoV'));" -->
	  <tr>
	    <td >Direcci&oacute;n</td>
	    <td ><input name="txtDireccion" type="text" class="box1" id="txtDireccion" onfocus="direccion(this,document.getElementById('txtTipoV'));" /> <img src="<?php echo $url; ?>imagenes/menu/obligado.png" width="12" height="12" /></td>
	    <td >Ubicaci&oacute;n Vivienda</td>
	    <td ><select name="txtTipoV" id="txtTipoV" class="box1">
	      <option value="0" selected="selected">Seleccione...</option>
	      <option value="U">URBANA</option>
	      <option value="R">RURAL</option>
	      </select><img src="<?php echo $url; ?>imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
	    </tr>
	  <tr>
	    <td >Tipo Propiedad</td>
	    <td ><select name="txtCasa" id="txtCasa" class="box1">
	      <option value="0" selected="selected">Seleccione...</option>
	      <?php
		$defin = $objDefiniciones->mostrar_datos(42,1);
		while($r_defi=mssql_fetch_array($defin)){
		echo "<option value=".$r_defi['iddetalledef'].">".$r_defi['detalledefinicion']."</option>";
			}
        ?>
	      </select><img src="<?php echo $url; ?>imagenes/menu/obligado.png" width="12" height="12" /></td>
	    <td >Fecha Nace</td>
	    <td ><input name="textfield6" type="text" class="box1" id="txtFechaN" /><img src="<?php echo $url; ?>imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
	    </tr>
	  <tr>
	    <td >Depto Nace</td>
	    <td ><select name="cboDepto2" id="cboDepto2" class="box1">
	      <option value="0" selected="selected">Seleccione...</option>
	      <?php
		$consulta = $objCiudad->departamentos();
		while($r_defi=mssql_fetch_array($consulta)){
		echo "<option value=".$r_defi['coddepartamento'].">".utf8_encode($r_defi['departmento'])."</option>";
			}
        ?>
	      </select>	      <img src="<?php echo $url; ?>imagenes/menu/obligado.png" width="12" height="12" /></td>
	    <td >Ciudad Nace</td>
	    <td ><select name="cboCiudad2" id="cboCiudad2" class="box1">
	      <option value="0" selected="selected">Seleccione...</option>
	      </select><img src="<?php echo $url; ?>imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
	    </tr>
	  <tr>
	    <td >Capacidad Trabajo</td>
	    <td ><select name="txtCapacidad" id="txtCapacidad" class="box1">
	      <option value="0" selected="selected">Seleccione...</option>
	      <option value="N">Normal</option>
	      <option value="I">Discapacitado</option>
	      </select></td>
	    <td >&nbsp;</td>
	    <td >&nbsp;</td>
	    </tr>
	  <tr>
	    <td >Nombre Corto</td>
	    <td colspan="3" ><input name="txtNombreC" type="text" class="boxlargo" id="txtNombreC" readonly="readonly" /></td>
	    </tr>
    </table>
    </td>
    <td background="<?php echo $url; ?>imagenes/tabla/derecha.gif">&nbsp;</td>
  </tr>
    <tr>
    <td height="38" background="<?php echo $url; ?>imagenes/tabla/abajo_izq2.gif">&nbsp;</td>
    <td background="<?php echo $url; ?>imagenes/tabla/abajo_central.gif"></td>
    <td background="<?php echo $url; ?>imagenes/tabla/abajo_der.gif">&nbsp;</td>
  </tr>
    </table>
</body>
<script>
	llenarCampos(<?php echo $idp; ?>);
</script>	
</html>