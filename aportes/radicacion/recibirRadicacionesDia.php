<?php 
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
ini_set("display_errors",'1');
$url=$_SERVER['PHP_SELF'];
include_once $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . "config.php";
include_once $raiz.DIRECTORY_SEPARATOR .'phpComunes'.DIRECTORY_SEPARATOR.'auditoria.php';
auditar($url);
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejaInforma.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
include_once $raiz. DIRECTORY_SEPARATOR .'clases'. DIRECTORY_SEPARATOR . 'p.definiciones.class.php';
date_default_timezone_set('UTC');
global $comprobantesAportes;		
$db = IFXDbManejaInforma::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
$Definiciones = new Definiciones();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Recepci&oacute;n de radicaciones por tipo</title>
	<link type="text/css" href="../../css/css.1.8/jquery-ui-1.8.17.custom.css" rel="stylesheet">
	<link type="text/css" href="../../css/marco.css" rel="stylesheet">
	<link type="text/css" href="../../css/Estilos.css" rel="stylesheet">
	<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/1.6/jquery-1.6.2.min.js"></script>
	<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/1.6/jquery-ui-1.8.16.custom.min.js"></script>
	<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/comunes.js"></script>
	<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/phpjs/phpencodeutf8.js"></script>
	<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/phpjs/phpmd5.js"></script>
	<script type="text/javascript" src="js/recibirRadicaciones.js" ></script>
	<script type="text/javascript">
	shortcut.add("Shift+F",function(){
		var url="<?php echo URL_PORTAL; ?>aportes/trabajadores/consultaTrabajador.php";
		window.open(url,"_blank");
	},{
		'propagate' : true,
		'target' : document 
	});       
	</script>
<script language="javascript">
//$.ui.dialog.defaults.bgiframe = true;
var fechaActual = new Date();
$(function() {
	$("#ayuda").dialog({
	 	autoOpen: false,
		height: 450, 
		width: 700, 
		open: function(evt, ui){
				$('#ayuda').html('');
				$.get('../../help/aportes/ayudadistribucionAportes.html',function(data){
						$('#ayuda').html(data);
				});
		 }
	});
});
</script>
<!-- colaboracion en linea  -->
<script type="text/javascript">
	$(function() {
		$("#dialog-form2").dialog("destroy");
		$("#dialog-form2").dialog({
			autoOpen: false,
			height: 400,
			width: 500,
			modal: true,
			buttons: {
				'Enviar': function(){
					var bValid = true;
					var campo = $('#notas').val();
					var campo0 = $.trim(campo);
					if (campo0==""){
						$(this).dialog('close');
						return false;
						}
					var campo1=$('#usuario').val();
					var campo2="distribucion.php";
					$.post('../../phpComunes/colaboracion.php',{v0:campo0,v1:campo1,v2:campo2}, function(datos){
						if(datos=='1'){
							alert("Su comentario fue enviado correctamente!. Gracias por participar en el mejoramiento de SIGAS");
						}else{
							alert(datos);
						}
					});
					$(this).dialog('close');
				},
				Cancelar: function() {
					$(this).dialog('close');
					$("#dialog-form2").dialog("destroy");
				}
			}
		});
	});

	function mostrarAyuda(){
		$("#ayuda").dialog('open' );
	}

	function notas(){
		$("#dialog-form2").dialog('open');
	};
	</script>
	<style type="text/css">
		.errorTexto{
			background-color: #F5A9BC;
		}
	</style>
</head>
<body>
<br/>
<center>
<table width="95%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td class="arriba_iz" width="10" >&nbsp;</td>
		<td class="arriba_ce" ><span class="letrablanca">::Recepci&oacute;n de radicaciones por tipo::</span></td>
		<td class="arriba_de" width="10" >&nbsp;</td>
	</tr>
	<tr>
		<td class="cuerpo_iz">&nbsp;</td>
		<td class="cuerpo_ce">
			<img src="../../imagenes/tabla/spacer.gif" width="1" height="1"/>
			<img src="../../imagenes/spacer.gif" width="1" height="1"/>
			<img src="../../imagenes/menu/nuevo.png" title="Nuevo" width="16" height="16" style="cursor:pointer" onclick="nuevoR();"/>
			<img src="../../imagenes/spacer.gif" width="13" height="1"/>
			<img src="../../imagenes/menu/refrescar.png" width="16" height="16" style="cursor:pointer" title="Limpiar campos" onclick="camposLimpiar();"/>
			<img src="../../imagenes/spacer.gif" width="7" height="1"/>
			<img src="../../imagenes/menu/informacion.png" width="16" height="16" style="border:none; cursor:pointer" title="Manual" onclick="mostrarAyuda();"/>
			<img src="../../imagenes/spacer.gif" width="2" height="1"/>
			<img src="../../imagenes/menu/notas.png" width="16" height="16" style="cursor: pointer" title="Colaboraci&oacute;n en l&iacute;nea" onClick="notas();"/>
		    <img src="../../imagenes/spacer.gif" width="1" height="1"/>
		    <br>
		    <font size=1 face="arial">Nuevo&nbsp;Limpiar&nbsp;Info&nbsp;Ayuda</font>
		    </td>
		<td class="cuerpo_de">&nbsp;</td>
	</tr>
	<tr>
		<td class="cuerpo_iz">&nbsp;</td>
		<td class="cuerpo_ce">&nbsp;</td>
		<td class="cuerpo_de">&nbsp;</td>
	</tr>
	<tr>
		<td class="cuerpo_iz" >&nbsp;</td>
		<td class="cuerpo_ce" >
			<table border="0" class="tablero" width="100%" id="tbl_rad_trab">
				<thead>
					<tr>
					<td width="25%" colspan="9" ><h3>Radicaciones de trabajadores</h3></td>
					</tr>
					<tr>
						<th>N&uacute;m.</th>
						<th>Rad.</th>
						<th>Tipo rad.</th>
						<th>Ident. Trab.</th>
					  	<th>Trabajador</th>
					  	<th>Folios</th>
					  	<th>Agencia</th>
					  	<th>Procesada</th>
					  	<th>Usuario</th>
				  	</tr>
				</thead>
				<tbody>
					
				</tbody>
				<tfoot>
					<tr>
						<td colspan="9" style="text-align: right;">
							<input type="button" name="btn_recibir_trab" id="btn_recibir_trab" value="Recibir" />
						</td>
					</tr>
				</tfoot>
			</table>
		</td>
		<td class="cuerpo_de" >&nbsp;</td>
	</tr>
	<tr>
		<td class="cuerpo_iz" >&nbsp;</td>
		<td class="cuerpo_ce" >
			<table border="0" class="tablero" width="100%" id="tbl_rad_emp">
				<thead>
					<tr>
						<td width="25%" colspan="10" ><h3>Radicaciones de Empresas</h3></td>
					</tr>
					<tr>
						<th>N&uacute;m.</th>
						<th>Rad.</th>
						<th>Tipo rad.</th>
						<th>Persona Radica</th>
						<th>NIT</th>
					  	<th>Empresa</th>
					  	<th>Folios</th>
					  	<th>Agencia</th>
					  	<th>Procesada</th>
					  	<th>Usuario</th>				  	
				  	</tr>
			  	</thead>
			  	<tbody>
			  	</tbody>
			  	<tfoot>
					<tr>
						<td colspan="10" style="text-align: right;">
							<input type="button" name="btn_recibir_emp" id="btn_recibir_emp" value="Recibir" />
						</td>
					</tr>
				</tfoot>
			</table>
		</td>
		<td class="cuerpo_de" >&nbsp;</td>
	</tr>
	<tr>
		<td class="cuerpo_iz" >&nbsp;</td>
		<td class="cuerpo_ce" >
			<table border="0" class="tablero" width="100%" id="tbl_rad_fonede">
				<thead>
					<tr>
						<td width="25%" colspan="9" ><h3>Radicaciones de FONEDE</h3></td>
					</tr>
					<tr>
						<th>N&uacute;m.</th>
						<th>Rad.</th>
						<th>Ident. Trab.</th>
					  	<th>Trabajador</th>
					  	<th>Folios</th>
					  	<th>Agencia</th>
					  	<th>Procesada</th>
					  	<th>Usuario</th>
				  	</tr>
			  	</thead>
				<tbody>
				</tbody>
				<tfoot>
					<tr>
						<td colspan="9" style="text-align: right;">
							<input type="button" name="btn_recibir_fonede" id="btn_recibir_fonede" value="Recibir" />
						</td>
					</tr>
				</tfoot>
			</table>
		</td>
		<td class="cuerpo_de" >&nbsp;</td>
	</tr>
	<tr>
		<td width="10" class="cuerpo_iz" >&nbsp;</td>
		<td class="cuerpo_ce" >&nbsp;</td>
		<td width="10" class="cuerpo_de" >&nbsp;</td>
	</tr>
	<tr>
		<td class="abajo_iz" >&nbsp;</td>
		<td class="abajo_ce" ></td>
		<td class="abajo_de" >&nbsp;</td>
	</tr>
</table>

<!-- FORMULARIO CONFIRMACIÓN DE RECEPCIÓN DE FORMULARIOS -->
<div id="dialog_recibir" style="display:none" title="Confirmaci&oacute;n de recepci&oacute;n de formularios" >
	<p>Digite el usuario clave del coordinador de ventanilla que recibir&aacute; los formularios de <strong id="str_tipo_info_confirmar">trabajador</strong>.</p>
	<table class="tablero">
	 	<tr>
			<td><label for="usuario_recibe">Usuario que recibe</label></td>
	  		<td><input type="text" name="usuario_recibe" id="usuario_recibe" class="box1" /></td>
		</tr>
		<tr>
			<td><label for="clave_recibe">Clave</label></td>
	  		<td><input type="password" name="clave_recibe" id="clave_recibe" class="box1" /></td>
		</tr>
	</table>
	<input type="hidden" name="tipo_info_confirmar" id="tipo_info_confirmar" value="" />
</div>

<!-- FORMULARIO OBSERVACIONES-->
<div name="div-observaciones-tab" style="display:none" title="Observaciones de modificaci&oacute;n">
<table class="tablero">
 <tr>
   <td>Usuario</td>
   <td colspan="3" >
   <input name="usuarioObs" id="usuarioObs" class="box1" disabled="disabled" value="<?php echo $_SESSION['USUARIO']?>" /></td>
   </tr>
 <tr>
   <td>Observaciones</td>
   <td colspan="3" >
   <textarea name="observacionGrupo" id="observacionGrupo" cols="45" rows="5" class="boxlargo"></textarea></td>
   </tr>
</table>
<div class="ui-state-highlight ui-corner-all" style="margin: 9px auto; display:none;padding: 6px; text-align: center; border: 1px solid rgb(255, 255, 0); width:300px" id="rtaObservacion"><span class="ui-icon ui-icon-info" style="float: left; margin-right: 0.3em;"></span>Observaci&oacute;n guardada con &eacute;xito.</div>
</div>

<!-- fin colaboracion -->

<!-- ayuda en linea -->
<div id="ayuda" title="Manual .:. Distribuci&oacute;n Aportes" style="background-image:url('../../imagenes/FondoGeneral0.png')"></div>
<!-- fin ayuda en linea -->
<input type="hidden" name="idRepresemtante" id="idRepresemtante" />
<input type="hidden" name="idContacto" id="idContacto" />
</center>
</body>
</html>