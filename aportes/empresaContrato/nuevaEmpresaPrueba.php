<?php
/* autor:       Orlando Puentes
 * fecha:       Agosto 18 de 2010
 * objetivo:    Administrar en la base de datos la nueva empresa contrato afiliadas a Comfamiliar Huila.
 */
 
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$url=$_SERVER['PHP_SELF'];
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR .'auditoria.php';
auditar($url);
include_once $raiz . DIRECTORY_SEPARATOR . 'funcionesComunes' . DIRECTORY_SEPARATOR . 'funcionesComunes.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'p.definiciones.class.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'ciudades.class.php';
$objClase=new Definiciones();
$objCiudad=new Ciudades();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="es">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Empresas</title>
<link type="text/css" href="../../css/formularios/base/ui.all.css" rel="stylesheet" />
<link type="text/css" href="../../css/Estilos.css" rel="stylesheet" />
<link type="text/css" href="../../css/formularios/demos.css" />
<link href="../../css/marco.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../../js/jquery-1.4.2.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.core.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.draggable.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.resizable.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.dialog.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/jquery.ui.widget.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.datepicker.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/effects.core.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.button.js"></script>
<script type="text/javascript" src="../../js/comunes.js"></script>
<script type="text/javascript" src="js/nuevaEmpresa.js"></script>

<script type="text/javascript">
	$(function() {
		$('#datepicker').datepicker({
			changeMonth: true,
			changeYear: true
		});
	});
	</script>

<script language="javascript">
$.ui.dialog.defaults.bgiframe = true;
	$(function() {
		$("#ayuda").dialog({
			 autoOpen: false,
			 height: 550, 
			 width: 800, 
			 open: function(evt, ui){
					$('#ayuda').html('');
					$.get('../../help/aportes/ayudaEmpresa.html',function(data){
							$('#ayuda').html(data);
					})
			 }
			 });
		$("#dialog-form2").dialog({ autoOpen: false });
	});  
	
	$(document).ready(function(){
	// Parametros para e combo1
   $("#combo1").change(function () {
   		$("#combo1 option:selected").each(function () {
			//alert($(this).val());
				elegido=$(this).val();
				$.post("../phpComunes/ciudades.php", { elegido: elegido }, function(data){
				$("#combo2").html(data);
				$("#combo3").html("");
			});			
        });
   })
	// Parametros para el combo2
	$("#combo2").change(function () {
   		$("#combo2 option:selected").each(function () {
			//alert($(this).val());
				elegido=$(this).val();
				$.post("combo2.php", { elegido: elegido }, function(data){
				$("#combo3").html(data);
			});			
        });
   })
});
</script>

<script type="text/javascript">
$(document).ready(function(){
	
	$(function() {
		// a workaround for a flaw in the demo system (http://dev.jqueryui.com/ticket/4375), ignore!
		$("#dialog-form").dialog("destroy");
		
		var uno = $("#uno"),
			dos = $("#dos"),
			tres = $("#tres"),
			allFields = $([]).add(uno).add(dos).add(tres),
			tips = $(".validateTips");
		
		$("#dialog-form").dialog({
			autoOpen: false,
			height: 450,
			width: 650,
			modal: true,
			open: function(){
				$('#dialog-form').html('');
					$.get('../../phpComunes/direcciones.html',function(data){
							$('#dialog-form').html(data);
					})
				$('#tDirCompleta').val('');
				$('#dialog-form select,#dialog-form input[type=text]').val('');
				$('#uno').focus();
			},
			buttons: {
				'Crear direcci\u00F3n': function() {
					var bValid = true;
					allFields.removeClass('ui-state-error');
					//if(uno.val().length>0) 	uno1=uno.val()+" ";
					$('#tDireccion').val($('#tDirCompleta').val());
					$('#tDirCompleta').val('');
					$('#dialog-form select,#dialog-form input[type=text]').val('');
					$(this).dialog('close');
				},
				Cancelar: function() {
					$('#tDirCompleta').val('');
					$('#dialog-form select,#dialog-form input[type=text]').val('');
					$(this).dialog('close');
				}
			},
			close: function() {
				allFields.val('').removeClass('ui-state-error');
				$('#combo1').focus();
			}
		});
		

		
	});
	});//fin ready
	</script>
<!-- colaboracion en linea  -->

<script type="text/javascript">
	$(function() {
		$("#dialog-form2").dialog("destroy");
		/*var notas = $("#notas"),
			allFields = $([]).add(notas);*/
		
		$("#dialog-form2").dialog({
			autoOpen: false,
			height: 400,
			width: 500,
			modal: true,
			buttons: {
				'Enviar': function() {
					var bValid = true;
					var campo=$('#notas').val();
					var campo0=$.trim(campo);
					if (campo0==""){
						$(this).dialog('close');
						return false;
						}
					var campo1=$('#usuario').val();
					var campo2="empresas.php";
					$.post('../../phpComunes/colaboracion.php',{v0:campo0,v1:campo1,v2:campo2}, function(datos){
						if(datos=='1'){
							alert("Su comentario fue enviado correctamente!. Gracias por participar en el mejoramiento de SIGAS");
						}else{
							alert(datos);
						}
					});
				$(this).dialog('close');
				},
				Cancelar: function() {
					$(this).dialog('close');
					$("#dialog-form2").dialog("destroy");
				}
			}
			
		});
		
		$('#enviar-notas')
			.button()
			.click(function() {
				$('#dialog-form2').dialog('open');
			});
	});
	</script>
<style type="text/css">
<!--
.Estilo3 {font-family: Arial, Helvetica, sans-serif}
-->
</style>
</head>
<body>
<form name="forma">

<br />
<center>
<table width="98%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="13" height="29" class="arriba_iz">&nbsp;</td>
		<td class="arriba_ce"><span class="letrablanca">::&nbsp;Administraci&oacute;n - Empresa Nueva&nbsp;::</span></td>
		<td width="13" class="arriba_de" align="right">&nbsp;</td>
	</tr>
	<tr>
<td class="cuerpo_iz">&nbsp;</td>
<td class="cuerpo_ce">
<img src="../../imagenes/tabla/spacer.gif" width="1" height="1"/>
<img src="../../imagenes/spacer.gif" width="1" height="1"/>
<img src="../../imagenes/menu/nuevo.png" title="Nuevo" width="16" height="16" style="cursor: pointer" onClick="nuevoR();"/>
<img src="../../imagenes/spacer.gif" width="1" height="1"/>
<img src="../../imagenes/menu/grabar.png" width:16 height=16 style="cursor: pointer" title="Guardar" onClick="validarCampos(1);" id="bGuardar"/>
<img src="../../imagenes/spacer.gif" width="1" height="1"/>
<img src="../../imagenes/menu/modificar.png" title="Actualizar" width="16" height="16" onClick="validarCampos(2)" style="cursor: pointer"/>
<img src="../../imagenes/spacer.gif" width="1" height="1"/>
<img src="../../imagenes/menu/ico_error.png" width="16" height="16" border="0" title="Eliminar" style="cursor: pointer"
			onClick="eliminarDato()"/>
<img src="../../imagenes/spacer.gif" width="1" height="1"/>
<img src="../../imagenes/menu/imprimir.png" width="16" height="16" style="cursor: pointer" title="Imprimir"
			onClick="window.print()"/>
<img src="../../imagenes/spacer.gif" width="1" height="1"/>
<img src="../../imagenes/menu/refrescar.png" width="16" height="16" style="cursor: pointer" title="Limpiar Campos"
			onClick="limpiarCampos(); document.forms[0].elements[0].focus();"/>
<img src="../../imagenes/spacer.gif" width="1" height="1"/>
<img src="../../imagenes/menu/buscar.png" title="Buscar" width="16" height="16" style="cursor: pointer" onClick="consultaDatos()"/>
<img src="../../imagenes/spacer.gif" width="1" height="1"/>
<img src="../../imagenes/menu/informacion.png" width="16" height="16" style="border: none; cursor: pointer" title="Ayuda en l�nea" onClick="mostrarAyuda();" /> 
<img src="../../imagenes/menu/notas.png" width="16" height="16" style="cursor: pointer" title="Colaborac�on en l�nea" onclick="notas();" />
<td class="cuerpo_de">&nbsp;</td>
	
	</tr>
	<tr>
		<td class="cuerpo_iz">&nbsp;</td>
		 <td align="left" class="cuerpo_ce">
		<div id="error" style="color: #FF0000"></div>
		</td>
		<td class="cuerpo_de">&nbsp;</td>
	</tr>
	<tr>
		<td class="cuerpo_iz">&nbsp;</td>
		<td align="center" class="cuerpo_ce">
		<table width="100%" border="0" cellspacing="0" class="tablero">
			<tr>
				<td width="17%">Sector</td>
				<td width="29%"><select name="select12" class="box1" id="select12">
					<option value="0" selected="selected">Seleccione...</option>
        <?php
								$consulta=$objClase->mostrar_datos(16, 2);
								while($row=mssql_fetch_array($consulta)){
									echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
								}
								?>
        </select> 
				<img src="../../imagenes/menu/obligado.png" alt=""
					width="12" height="12" />2</td>
				<td width="17%">Clase de Sociedad</td>
				<td width="37%"><select name="select13" class="box1" id="select13">
					<option value="0" selected="selected">Seleccione...</option>
        <?php
								$consulta=$objClase->mostrar_datos(17, 4);
								while($row=mssql_fetch_array($consulta)){
									echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
								}
								?>
        </select> 
				<img src="../../imagenes/menu/obligado.png" alt=""
					width="12" height="12" />3</td>
			</tr>
			<tr>
				<td><label for="iden">NIT</label></td>
				<td><label> <input name="tNit" type="text" class="box1" id="tNit"
					onblur="digito(this);" onchange="digito(this)" value="" /> -</label>
				D&iacute;gito <label for="textfield3"></label> <input name="tDigito"
					type="text" class="boxcorto" id="tDigito" readonly="readonly" /> <label><img
					src="../../imagenes/menu/obligado.png" width="12" height="12" />45</label></td>
				<td>Tipo Documento</td>
				<td><select name="select" id="select" class="box1">
					<option value="0" selected="selected">Seleccione...</option>
        <?php
								$consulta=$objClase->mostrar_datos(1, 4);
								while($row=mssql_fetch_array($consulta)){
									echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
								}
								?>
        </select> 
				<img src="../../imagenes/menu/obligado.png" width="12"
					height="12" />6</td>
			</tr>
			<tr>
				<td>Raz&oacute;n Social</td>
				<td colspan="2"><input name="textfield" type="text" class="boxlargo"
					id="textfield" onkeypress="copiar(this);"> 
				<img
					src="../../imagenes/menu/obligado.png" width="12" height="12" />7</td>
				<td>C&oacute;digo Sucursal - 000</td>
			</tr>
			<tr>
				<td>Nombre Comercial</td>
				<td colspan="3"><input name="textfield2" type="text"
					class="boxlargo" id="textfield2"
					onkeypress="tabular (this, event);"/> 
				<img
					src="../../imagenes/menu/obligado.png" width="12" height="12" />8</td>
			</tr>
			<tr>
				<td>Direcci&oacute;n</td>
				<td colspan="3"><input name="tDireccion" type="text" class="boxlargo" id="tDireccion" onfocus="direccion();" /> 
				<img
					src="../../imagenes/menu/obligado.png" width="12" height="12" />9</td>
			</tr>
			<tr>
				<td>Departamento</td>
				<td><label for="combo1"></label> <select name="combo1" class="box1"
					id="combo1">
					<option value="0" selected="selected">Seleccione</option>
<?php
$consulta=$objCiudad->departamentos();
while($row=mssql_fetch_array($consulta)){
	?>          
<option value="<?php
	echo $row["coddepartamento"];
	?>"><?php
	echo $row["departmento"]?></option>
<?php
}
?>
	      </select> 
				<img src="../../imagenes/menu/obligado.png" width="12"
					height="12" />10</td>
				<td>Ciudad</td>
				<td><select name="combo2" class="box1" id="combo2">
				</select> 
				<img src="../../imagenes/menu/obligado.png" width="12"
					height="12" />11</td>
			</tr>
			<tr>
				<td>Tel&eacute;fono</td>
				<td><input name="textfield9" type="text" class="box1"
					id="textfield8" /> 
				<img src="../../imagenes/menu/obligado.png"
					alt="" width="12" height="12" />12</td>
				<td>Fax</td>
				<td><input name="textfield8" type="text" class="box1"
					id="textfield6" />
				13</td>
			</tr>
			<tr>
				<td>URL</td>
				<td><input name="textfield10" type="text" class="box1"
					id="textfield9" />
				14</td>
				<td>Email</td>
				<td><input name="textfield6" type="text" class="box1"
					id="textfield5" /> 
				<img src="../../imagenes/menu/obligado.png"
					alt="" width="12" height="12" />15</td>
			</tr>
			<tr>
				<td>Primer Nombre</td>
				<td><input name="textfield12" type="text" class="box1"
					id="textfield11" />
				17</td>
				<td>Segundo Nombre</td>
				<td><input name="textfield14" type="text" class="box1"
					id="textfield13" />
				18</td>
			</tr>
			<tr>
				<td>Primer Apellido</td>
				<td><input name="textfield11" type="text" class="box1"
					id="textfield10" />
				19</td>
				<td>Segundo Apellido</td>
				<td><input name="textfield13" type="text" class="box1"
					id="textfield12" />
				20</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td colspan="2">
				<div class="Rojo" id="personal">Digite identificaci&oacute;n del
				Jefe de Personal</div></td>
			</tr>
			<tr>
				<td>Contratista</td>
				<td><select name="select4" class="box1" id="select4">
					<option value="0" selected="selected">Seleccione</option>
					<option value="N">NO</option>
					<option value="S">SI</option>
				</select> 
				<img src="../../imagenes/menu/obligado.png" alt=""
					width="12" height="12" />26</td>
				<td>Colegio</td>
				<td><select name="select5" class="box1" id="select5">
					<option value="0" selected="selected">Seleccione</option>
					<option value="N">NO</option>
					<option value="S">SI</option>
				</select> 
				<img src="../../imagenes/menu/obligado.png" alt=""
					width="12" height="12" />27</td>
			</tr>
			<tr>
				<td>Excento</td>
				<td><select name="select6" class="box1" id="select6">
					<option value="0" selected="selected">Seleccione</option>
					<option value="N">NO</option>
					<option value="S">SI</option>
				</select> 
				<img src="../../imagenes/menu/obligado.png" alt=""
					width="12" height="12" />28</td>
				<td>Actividad</td>
				<td><select name="select7" class="box1" id="select7">
					<option value="0" selected="selected">Seleccione...</option>
        <?php
								$consulta=$objClase->mostrar_datos(15, 4);
								while($row=mssql_fetch_array($consulta)){
									echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
								}
								?>
        </select> 
				</select> <img src="../../imagenes/menu/obligado.png"
					alt="" width="12" height="12" />29</td>
			</tr>
			<tr>
				<td>&Iacute;ndice Aporte</td>
				<td><select name="select11" class="box1" id="select11">
					<option value="0" selected="selected">Seleccione...</option>
        <?php
								$consulta=$objClase->mostrar_datos(18, 4);
								while($row=mssql_fetch_array($consulta)){
									echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
								}
								?>
	      </select> 
				<img src="../../imagenes/menu/obligado.png" alt=""
					width="12" height="12" />30</td>
				<td>Asesor</td>
				<td><select name="select8" class="box1" id="select8">
					<option value="0" selected="selected">Seleccione</option>
					<option value="1">Presentaci&oacute;n personal</option>
				</select> 
				<img src="../../imagenes/menu/obligado.png" alt="}"
					width="12" height="12" />31</td>
			</tr>
			<tr>
				<td>Seccional</td>
				<td><select name="select10" class="box1" id="select10">
					<option value="0" selected="selected">Seleccione</option>
					<option value="01">Neiva</option>
					<option value="02">Garz&oacute;n</option>
					<option value="03">Pitalito</option>
					<option value="04">La Plata</option>
				</select> 
				<img src="../../imagenes/menu/obligado.png" alt=""
					width="12" height="12" />32</td>
				<td>Estado</td>
				<td><select name="select9" class="box1" id="select9">
					<option value="0" selected="selected">Seleccione</option>
					<option value="A">Activo</option>
					<option value="I">Inactivo</option>
				</select> 
				<img src="../../imagenes/menu/obligado.png" alt=""
					width="12" height="12" />33</td>
			</tr>
			<tr>
				<td>Fecha Afiliaci&oacute;n</td>
				<td><input name="textfield19" type="text" class="box1"
					id="textfield18" readonly="readonly" value="" />
				34</td>
				<td>Fecha inicio aportes</td>
				<td><input name="datepicker" type="text" class="box1"
					id="datepicker" readonly="readonly" value="" />
				35</td>
			</tr>
			<tr>
				<td>usuario</td>
				<td><input name="textfield19" type="text" class="box1"
					id="textfield18" readonly="readonly" value="" />
				36</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>Observaciones</td>
				<td colspan="3"><textarea id="observaciones" name="observaciones"
					rows="3" class="area"></textarea>
				37</td>
			</tr>

		</table>

		<table width="100%" border="0" cellspacing="0">
			<tr>
				<td align="left">&nbsp;</td>
			</tr>
			<tr>
				<td align="left" class="Rojo"><img
					src="../../imagenes/menu/obligado.png" width="16" height="12"
					align="left" />Campo Obligado</td>
			</tr>
		</table>

		</td>
		<td class="cuerpo_de">
	</tr>
	<tr>
		<td class="abajo_iz" >&nbsp;</td>
		<td class="abajo_ce" ></td>
		<td class="abajo_de" >&nbsp;</td>
	</tr>
</table>
</center>
<input type="hidden" name="tIdRadicacion" id="tIdRadicacion" /> <input
	type="hidden" name="tNit2" id="tNit2" /> <input type="hidden"
	name="tFechaRadicacion" id="tFechaRadicacion" /> <input type="hidden"
	value="<?php echo $_SESSION["USUARIO"]; ?>" name="usuario" id="usuario" />

<!-- formulario direcciones  --> 
<div id="dialog-form" title="Formulario de direcciones"></div>

<!-- colaboracion en linea -->

<div id="dialog-form2" title="Colaboraci�n en l�nea">
<p>Por favor diligencie este formulario para enviar comentarios, errores o falencias encontradas en el proceso. M&aacute;ximo 250 caracteres </p>
<label>Tus comentarios:</label><br />
<textarea name="notas" id="notas" cols="60", rows="10"></textarea>
</div>

<!-- fin colaboracion -->

<!-- ayuda en linea -->
<div id="ayuda" title="Manual .:. Empresa Nueva"></div>
<!-- fin ayuda en linea -->

</form>
</body>
<script language="javascript">
function mostrarAyuda(){
	$("#ayuda").dialog('open' );
	}
function direccion(){
	$('#dialog-form').dialog('open');
	}
function notas(){
	$("#dialog-form2").dialog('open');
	}	
</script>
</html>
