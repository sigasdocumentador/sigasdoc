/*
* @autor:      Ing. Orlando Puentes
* @fecha:      septiembre 27 de 2010
* objetivo:
* 
* @ultima modificacion: julio 19 de 2013
*/

var nuevoEmpresa=1;
var ccgerente=1;
var URL=src();
var objEmpresa=null;
var esActualizacion=false;

$(document).ready(function(){
	$("#sociedad").bind("change",function(){
		var valor = $(this).val();
		var idTextoDoc = "tNit";
		
		$("#"+idTextoDoc).unbind("keyup");
		$("#"+idTextoDoc).unbind("change");
		
		$("#tNit").bind("keyup", function (){
			solonumeros(this);
		});		
		
		if ( valor == 102 ) { 	// si es PERSONA JURIDICA: Se debe recibir c�dulas			
			$("#"+idTextoDoc).bind("keyup",function(){
				solonumeros(document.getElementById(idTextoDoc));
			});
			
			// comportamiento para generar el d�gito de verificaci�n			
			$("#"+idTextoDoc).bind("change",function(){
				digito(document.getElementById(idTextoDoc));
			});
			
		} else {
			// si es PERSONA NATURAL,o no se selecciona, s�lo se deben recibir nits.			
			$("#"+idTextoDoc).bind("keyup",function(){
				solotexto(document.getElementById(idTextoDoc));
			});
		}
	});	
	$("#fechaEstado").datepicker({
		maxDate:"+0D",
		changeMonth: true,
		changeYear: true
	});
});

/***  INICIAL  ***/
function nuevoREmpresa(){
	nuevoEmpresa=1;
	$("#bGuardar").show();
	limpiarCamposEmpresa();
	$("#sector").focus();
	$("#fechaEstado").val("");
	$("#cmbCausalInactivacion").val("");
	$("#rowInactivo").hide();
}

function limpiarCamposEmpresa(){
	objEmpresa=null;
	esActualizacion=false;
	$("#error").html("");
	$("#tNombres").html("");	
	$(".tablero select").val("0");
	$("#sociedad").trigger("change");
	var user = $("#usuarioEmpresa").val();
	$(".tablero input:text,textarea").val('');
	$('#fecAfiliacion').val(fechaActual.toLocaleDateString());
	$("#usuarioEmpresa").val(user);
}


/*** BUSQUEDAS ***/
function buscarPersona(objeto){
	if(nuevoEmpresa==0){
		alert("Si es un registro nuevo haga click en NUEVO!!");
		return;
	}
	var numero=parseInt(objeto.value);
	if(isNaN(numero)) return;
	$.ajax({
		url: URL+'phpComunes/buscarPersona.php',
		cache: false,
		type: "GET",
		dataType:"json",
		data: "submit=&v0="+numero,//submit=&v0="+numero
		success: function(datos){
			if(datos==0){
				alert("No existe en nuestra base, registre los datos completos!");	
				ccgerente=1;
				$("#tNombres").html('');
				persona_simple(document.getElementById('ccRepresentante'),document.getElementById('idRepresentante'));
				return false;
			}
			else{
				ccgerente=0;
				var nom=datos[0].pnombre+" "+datos[0].snombre+" "+datos[0].papellido+" "+datos[0].sapellido;
				$("#tNombres").html(nom);
				$("#idRepresentante").val(datos[0].idpersona);
			}
			
		}//end succes
	});//end ajax 
}

function buscarNit(txtNit, option){
	if(validarNumero(txtNit,0)>0){return null;}
	
	var empresa=null;
	
	$.ajax({
		url: URL+'phpComunes/buscarEmpresaNit.php',
		async: false,
		data:{v0:txtNit.val()},
		beforeSend: function(objeto){
        	dialogLoading('show');
        },        
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }            
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("En buscarNit paso lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
		success:function(datoEmpresa){
			if(datoEmpresa){
				$.each(datoEmpresa,function(i,fila){
					empresa=fila; return false; 
				});
			}
		},
		timeout: 5000,
        type: "GET"
	});	
	
	return empresa;
}

function busquedaEmpresa(txtNit, lblRazonSocial){
	objEmpresa=null;
	esActualizacion=false;		
	lblRazonSocial.val('');	
	txtNit.removeClass("ui-state-error");
	
	if(validarNumero(txtNit,0)>0){return null;}
	
	var empresa=buscarNit(txtNit,1);		
	if(empresa==null || empresa==false){			
		alert("La EMPRESA NO EXISTE!!\n" +
				"Puede continuar.");			 
	} else {
		if(empresa.contratista=="S"){
			objEmpresa=empresa;
			esActualizacion=true;
			lblRazonSocial.val(empresa.razonsocial);
			$("#sector").val(empresa.idsector);
			$("#sociedad").val(empresa.idclasesociedad).trigger("change");
			$("#tipoDoc").val(empresa.idtipodocumento);
			$("#tDigito").val(empresa.digito);
			$("#nombreComercial").val(empresa.sigla);
			$("#tDireccion").val(empresa.direccion);
			$("#cboDepto2").val(empresa.iddepartamento).trigger("change");
			$("#cboCiudad2").val(empresa.idciudad);
			$("#telefono").val(empresa.telefono);
			$("#fax").val(empresa.fax);
			$("#url").val(empresa.url);
			$("#email").val(empresa.email);
			$("#idRepresentante").val(empresa.idrepresentante);
			$("#seccional").val(empresa.seccional);
			$("#estado").val(empresa.estado).trigger("change");
			setTimeout(function(){
				$("#cmbCausalInactivacion").val(empresa.codigoestado);
				$("#fechaEstado").val(empresa.fechaestado);
			}, 1000);
			alert("La EMPRESA EXISTE Y ES CONTRATISTA!!\n" +
					"Puede continuar.");
		} else {
			alert("La empresa con NIT: " + empresa.nit + " (" + empresa.razonsocial + ") EXISTE!!\n" +
					"Se encuentra como Contratista: " + empresa.contratista);
			limpiarCamposEmpresa();
			return false;
		}		
	}
}


/*** GUARDAR EMPRESA ***/
function validarCamposEmpresa(op){
	if( op == 1 ) { $("#bGuardar").hide(); }	
	$("#error").html("");
	var cadena="<ul>";
	
	if($("#tNit").val()==''){
		cadena+="<li>Ingrese el Nit de la empresa</li>";
	}
	if($("#razonsocial").val()==''){
		cadena+="<li>Falta raz\u00F3n social.</li>";
	}
	if($("#seccional").val()==0){
		cadena+="<li>Seleccione una seccional.</li>";
	}
	 if($("#estado").val()==0){
		cadena+="<li>Seleccione un estado.</li>";
	}    
	if($("#estado").val()=="I"){
		if($("#cmbCausalInactivacion").val() == 0 ){
			cadena+="<li>Seleccione la causal de inactivaci&oacute;n</li>";
		}
		if($("#fechaEstado").val()==0){
			cadena+="<li>Seleccione fecha estado</li>";
		}
	}else{
		$("#fechaEstado").val("");
		$("#cmbCausalInactivacion").val(0);
	}	
	cadena+="</ul>";
	$("#error").html(cadena);
	if($("#error ul li").size()==0){
		if( op != 1 || ( esActualizacion == false )) {
			return buscarNitInforma(op);
		} else {
			var nEmpresa=new Empresa();
			nEmpresa=llenarVariableEmpresa(objEmpresa);
			return actualizarEmpresa(nEmpresa);
		}
		
		
	} else {
		if( op == 1 ) { $("#bGuardar").show(); }
		return false;
	}
}

function buscarNitInforma(op){
	var campo0=$("#tNit").val();
	var retorno = false;
	$.ajax({
		url: 'buscarInforma.php',
		type: "POST",
		data: "submit=&v0="+campo0,
		async: false,
		success: function(datos){
			if(datos=='2'){				
				retorno = guardarEmpresa(op);
			} else{ 
				retorno = guardarEmpresaInforma(op);
			}
		}
	});
	return retorno;
}

function guardarEmpresaInforma(op){
	var campo0 =$("#tNit").val();	//nit
	var campo1 =$.trim($("#razonsocial").val()).substring(0,50);	//nombrr
	var campo2 =$("#tDireccion").val();	//direccion
	var campo3 =$("#telefono").val();	//telefono
	var campo4 =$("#cboCiudad2").val();	//ciudad
	var campo5 =$("#tNombres").html();	//contacto
	var campo6 =""; //$("#").val();	//aaereo
	var campo7 =$("#fax").val();	//fax
	var campo8 =$("#cboDepto2").val();	//depto
	var retorno = false;

	$.ajax({
		url: URL+'aportes/empresas/empresasGInforma.php',
		type: "POST",
		data: "submit=&v0="+campo0+"&v1="+campo1+"&v2="+campo2+"&v3="+campo3+"&v4="+campo4+"&v5="+campo5+"&v6="+campo6+"&v7="+campo7+"&v8="+campo8,
		async: false,
		success: function(datos){
			if ( datos == 1 ) {
				alert("Se guard\u00F3 la Empresa en Informa Web");
				retorno = guardarEmpresa(op);
			} else {
				alert("No se pudo guardar la empresa en Informa WEB!, intente de nuevo");
				if( op == 1 ) { $("#bGuardar").show(); }
				return false;
			}
		}
	});

	return retorno;
}

function guardarEmpresa(op){
	var nEmpresa=new Empresa();
	nEmpresa=llenarVariableEmpresa(nEmpresa);
	str = JSON.stringify(nEmpresa);
	var retorno = false;
	
	$.ajax({
		url: URL + 'phpComunes/guardarEmpresa.php',
		async: false,
		data: {v0:str},		
		beforeSend: function(objeto){
        	dialogLoading('show');
        },        
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }            
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("En guardarEmpresa Paso lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
		success: function(idEmpresaG){
			if(esNumeroRespuesta(idEmpresaG)){
				alert("Se Grabo la empresa ID No: " + idEmpresaG);
				if( op == 1 ) { limpiarCamposEmpresa(); }				
				retorno = true;
			} else {
				alert("No se pudo guardar la empresa Contratista");
				if( op == 1 ) { $("#bGuardar").show(); }
				return false;
			}
		},
		timeout: 5000,
        type: "GET"
	});
	
	return retorno;
}

function actualizarEmpresa(nEmpresa){
	if (nEmpresa==null) { return 0; }
	str = JSON.stringify(nEmpresa);
	
	var retorno=0;
	
	$.ajax({
		url: URL + 'phpComunes/updateEmpresa.php',
		async: false,
		data: {v0:str},		
		beforeSend: function(objeto){
        	dialogLoading('show');
        },        
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }            
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("En guardarEmpresa Paso lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
		success: function(idEmpresaG){
			if(esNumeroRespuesta(idEmpresaG)){
				alert("Se Actualizo la empresa ID No: " + nEmpresa.idempresa);
				limpiarCamposEmpresa();				
				retorno = true;
			} else {
				alert("No se pudo actualizar la empresa Contratista");
				$("#bGuardar").show();
				return false;
			}
		},
		timeout: 5000,
        type: "GET"
	});
	
	return retorno;
}

function llenarVariableEmpresa(nEmpresa){
	nEmpresa=camposEmpresa(nEmpresa);
	
	nEmpresa.idtipodocumento = ( $("#tipoDoc").val() == 0 ) ? '' : $("#tipoDoc").val();
	nEmpresa.nit=$("#tNit").val();
	nEmpresa.digito=$("#tDigito").val();
	nEmpresa.codigosucursal='000';
	nEmpresa.principal='S';
	nEmpresa.razonsocial=$("#razonsocial").val();
	nEmpresa.sigla=$("#nombreComercial").val();
	nEmpresa.direccion=$("#tDireccion").val();
	nEmpresa.iddepartamento = ( $("#cboDepto2").val() == 0 ) ? '' : $("#cboDepto2").val();
	nEmpresa.idciudad = ( $("#cboCiudad2").val() == 0 ) ? '' : $("#cboCiudad2").val();
	nEmpresa.telefono = ( $("#telefono").val() == 0 ) ? '' : $("#telefono").val();
	nEmpresa.fax = ( $("#fax").val() == 0 ) ? '' : $("#fax").val();
	nEmpresa.url = ( $("#url").val() == 0 ) ? '' : $("#url").val();
	nEmpresa.email = ( $("#email").val() == 0 ) ? '' : $("#email").val();
	nEmpresa.idrepresentante = ( $("#idRepresentante").val() == 0 ) ? '' : $("#idRepresentante").val();
	nEmpresa.contratista='S';
	nEmpresa.colegio='N';
	nEmpresa.exento='N';
	nEmpresa.idsector = ( $("#sector").val() == 0 ) ? '' : $("#sector").val();
	nEmpresa.seccional=$("#seccional").val();
	nEmpresa.tipopersona = ( $("#sociedad").val() == 0 ) ? '' : $("#sociedad").val();
	nEmpresa.estado=$("#estado").val();
	nEmpresa.fechaestado=$("#fechaEstado").val();
	nEmpresa.codigoestado=$("#cmbCausalInactivacion").val();
	nEmpresa.idclasesociedad = nEmpresa.tipopersona;
	nEmpresa.porplanilla='N';
	nEmpresa.idpais=48;
	 
	return nEmpresa;
}

/*** UTILES ***/
function copiar(obj){
	obj.value = obj.value.toUpperCase();
	$("#nombreComercial").val(obj.value);
}

function digito(obj)
{
	var vpri, x, y, z, i, nit1, dv1;
 	nit1=obj.value;
 	
 	if (isNaN(nit1)) {
 		alert('El valor digitado no es un n\u00FAmero valido'+nit1);
 	} else {
	 	vpri = new Array(16);
	 	x=0 ; y=0 ; z=nit1.length ;
		vpri[1]=3;
		vpri[2]=7;
		vpri[3]=13;
		vpri[4]=17;
		vpri[5]=19;
		vpri[6]=23;
		vpri[7]=29;
		vpri[8]=37;
		vpri[9]=41;
		vpri[10]=43;
		vpri[11]=47;
		vpri[12]=53;
		vpri[13]=59;
		vpri[14]=67;
		vpri[15]=71;
		
		for(i=0 ; i<z ; i++) {
			y=(nit1.substr(i,1));
			x+=(y*vpri[z-i]);
		}
		y=x%11
		if (y > 1) {
			dv1=11-y;
		} else {
			dv1=y;
		}
		$("#tDigito").val(dv1);
   }
}