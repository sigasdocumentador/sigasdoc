/*
* @autor:      Ing. Orlando Puentes
* @fecha:      Octubre 6 de 2010
* objetivo:
*/

// Document Ready
// funcion para obtener cookie en javascript
	function getCookie(c_name)
	{
		var i,x,y,ARRcookies=document.cookie.split(";");
		for (i=0;i<ARRcookies.length;i++)
		{
  			x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
  			y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
  			x=x.replace(/^\s+|\s+$/g,"");
  			if (x==c_name)
    			{
    				return unescape(y);
    			}// fin if
		}// fin for
  	}// fin getCookie
//funcion para obtener cookie en javascript
	
var URL = src();
var nuevo = 0;
var existe = 0;
var idexiste = 0;
var ide1 = 0;
var ide2 = 0;
var prestacionServicios = false;
var tipoPersonaContrato = null;
var campo99=-1;
var campoNuevo=0;	//1 Nuevo contratante; 2 Nuevo contratista
var nitTemp="";

$(document).ready(function(){
	var anoActual = new Date();
	var strYearRange = anoActual.getFullYear()-120 +":"+ anoActual.getFullYear();
	//DIALOG NUEVA EMPRESA CONTRATISTA
	$("#nuevaEmpresaC").dialog({
	 	autoOpen: false,
	    width: 920, 
		modal:true,
		open: function(evt, ui){
			$('#fecAfiliacion').val(fechaActual.toLocaleDateString());
			$("#usuarioEmpresa").val($("#usuario").val());
		},
		buttons:{
			'Crear Empresa':function(){
				nitTemp=$("#tNit").val();
				if ( validarCamposEmpresa(2) ) {
					$("#nuevaEmpresaC").dialog("close");
				}
			}
		},
		close:function(){
			  $("#error ul").remove();			  
			  $("#nuevaEmpresaC input:text").val('');
			  $("#nuevaEmpresaC select").val('0');
			  
			  if ( campoNuevo == 1 ) {
				  $("#txtnit1").val(nitTemp).trigger("blur");
			  } else if ( campoNuevo == 2 ) {
				  $("#txtnit2").val(nitTemp).trigger("blur");//limpio campo contratista				  
				  $("#historico tr:not(:first)").remove();//Limpia la tabla historico
			  }
			  nitTemp=""; campoNuevo=0;
		}//end boton*/
	});//end dialog
	
	$("#fechaNaceProp").datepicker({changeMonth: true,
		changeYear: true,
		maxDate: "+0D"
	});
	$("#fechaNaceProp").datepicker( "option", "yearRange", strYearRange );
	
	$("#capa_buscador_contratos").dialog({
		autoOpen: false,
	    width: 800, 
		modal:true,
		buttons:{
			'Buscar':function(){
				$("#listaContratosEncontrados tbody").empty();
				consultaDatos();
			}
		},
		close:function(){
			  $("#erroresBuscador ul").remove();
			  $("#nitContratanteB").val('');
			  $("#nitContratistaB").val('');
			  $("#numContratoB").val('');
			  $("#listaContratosEncontrados tbody").empty();
			  $("#capa_buscador_contratos input:text").val('');
		}
	});

	//---------------------------MOSTRAR AYUDA

	$("#ayuda").dialog({
	 	autoOpen: false,
		height: 450, 
		width: 700, 
		modal:false,
		open: function(evt, ui){
			$('#ayuda').html('');
			$.get(URL+'help/aportes/ayudaliquidacioncontrato.html',function(data){
					$('#ayuda').html(data);
			});
		}
	});

	//DIALOG COLABORACION EN LINEA
	$("#dialog-form2").dialog("destroy");
	$("#dialog-form2").dialog({
		autoOpen: false,
		height: 400,
		width: 500,
		modal: true,
		buttons: {
			'Enviar': function() {
			var bValid = true;
			var campo=$('#notas').val();
			var campo0=$.trim(campo);
			if (campo0==""){
				$(this).dialog('close');
					return false;
				}
			var campo1=$('#usuario').val();
			var campo2="radicacion.php";
			$.post(URL+'phpComunes/colaboracion.php',{v0:campo0,v1:campo1,v2:campo2}, 								
			function(datos){
				if(datos=='1'){
					alert("Su comentario fue enviado correctamente!. Gracias por participar en el mejoramiento de SIGAS");
				}else{
					alert(datos);
				}
			});
			$(this).dialog('close');
			},
			Cancelar: function() {
				$(this).dialog('close');
				$("#dialog-form2").dialog("destroy");
			}
		}		
	});
	
	
	$("#check_prestacionservicios").bind("click",function(){
		prestacionServicios = false;
		if($(this).attr("checked")){
			prestacionServicios = true;
			//$("#txtaiu,#txtbase,#txtbase2,#txtaporte,#txtArealizado,#txttotalaporte").val(0);
			$("#txttotalaporte").val(0);			
		}
		
		sumar();
	});
	
	nuevoR();
});//Fin document ready

/*** INICIAL ***/
function nuevoR(){
	nuevo=1;
	prestacionServicios = false;
	tipoPersonaContrato = null;
	campo99=-1;
	$("#bGuardar").show();
	$("#bActualizar").hide();
	limpiarCampos();
}

function limpiarCampos(){
	$("#errores ul.Rojo").remove();//limpia los errores en rojo
	
	$("table.tablero input:text").val('');
	$("table.tablero input:checkbox").attr('checked',false);
	$("table.tablero select").val('Seleccione..');	
	$("table.tablero  textarea").val('');
	$("#contratante,#contratista").html("");//Limpia el nombre del contratista y contratante
	$("#historico tr:not(:first)").remove();//Limpia la tabla historico
	
	$('#txtnit1').attr('disabled', false);
	$('#txtnit2').attr('disabled', false);
	$('#txtcontrato').attr('disabled', false);
	
	$('#txtfecha').val(fechaActual.toLocaleDateString());	
} 

/*** BUSQUEDAS ***/
function buscarNitContrato(objeto,celda){
	if(nuevo==0){
		alert("primero haga click en nuevo!");
		return false;
	}
	
	continuar=false;
	var numero=objeto.value;
	if ( isNaN(numero) ) { objeto.value=""; return; }
	if ( numero.length == 0 ) return;
	
	if ( $("#txtnit1").val() == $("#txtnit2").val() ) {
		MENSAJE("El contratante y el contratista no pueden ser los mismos");
		ide1=0; ide2=0;
		$("#txtnit1,#txtnit2").val('');		
		$('#historico tr:not(:first)').remove();
		$("#contratante,#contratista").html("");
		return false;
	} else if( celda == 1 ) {
		ide1=0;
		$("#contratante").html("");	
	} else {
		ide2=0;
		tipoPersonaContrato=null;
		$("#contratista").html("");
		$('#historico tr:not(:first)').remove();
	}		
	
	$.ajax({
		url: '../phpComunes/buscarNIT.php',
		cache: false,
		type: "GET",
		data: "submit=&v0="+numero,
		async: false,
		dataType:"json",
		success: function(datos){
			if(datos==0){
				if(celda == 1){					
					alert("La empresa CONTRATANTE no existe, debe crearla primero!");
					objeto.value = '';
					return false;
				} else if(celda==2){
					alert("La empresa contratista no existe, debe crearla primero!");					
				}
								
				campoNuevo=celda;
				$("#tNit").attr("disabled","disabled");
				$("#nuevaEmpresaC").dialog("open");
				$("#tNit").val(objeto.value);				
				$("#textfield").focus();
				objeto.value = '';
				return false;
			}
			
			if ( datos[0].estado == 'I' ) {		
				if(confirm("El NIT esta inactivo!, �Desea activar la empresa?")==false){
					objeto.value=""; objeto.focus();
					return false;
				} else {
					var salir=false;
					
					$.ajax({
						url: URL+'phpComunes/cambiarEstadoEmpresa.php',
						type: "GET",
						dataType: "json",
						data: ({idEmpresa: datos[0].idempresa, estado: 'A', usuario : $('#usuario').val(), opcion : '1'}),
						async:false,
						success: function(res_actualiza){
							if(!res_actualiza>0){
								alert("La Empresa no pudo ser activada");
								salir=true;
								return false;
							} else {
								alert("La Empresa fue activada correctamente");
							}								
						}
					});
					
					if(salir){
						objeto.value=""; objeto.focus();
						return false;
					}
				}
			}
			
			continuar=true;
			var empresa=value=datos[0].razonsocial;			
			switch(celda){
				case 1 :
					$('#contratante').html(empresa);
					ide1=datos[0].idempresa;
					buscarContrato2();
					break;
				case 2 :
					$('#contratista').html(empresa);
					ide2=datos[0].idempresa;
					buscarHistorico(ide2);
					tipoPersonaContrato = datos[0].tipopersona;
					break;
			}
		}
	});
}

/*** BUSQUEDA CONTRATO ***/
function buscarContrato(obj){
	var campo0=obj.value;
	$.getJSON('buscarContrato.php',{v0:campo0},function(data){
		if(data != 0){
			if(confirm("Hay una liquidacion de este contrato, desea continuar?")==false)
				nuevoR();
			return false;
		}
	});
}

function buscarContrato2(){
	var campo0=$('#txtcontrato').val();
	var campo1=$('#txtnit1').val();
	
	if(!campo0.length>0 || !campo1.length>0)
		return false;
	
	$.getJSON('buscarContrato2.php',{v0:campo0, v1:ide1},function(data){
		if(data != 0){
			if(confirm("Ya hay una una liquidacion de este contrato!, �Desea continuar?")==false)			
				nuevoR();
			return false;
		}
	});
}

function buscarHistorico(ide){
	$('#historico tr:not(:first)').remove();
	var campo0=ide;
	$.getJSON('buscarHistorico.php',{v0:campo0},function(data){
		$.each(data,function(i,fila){
			$('#historico').append("<tr><td>"+fila.idcontratante+"</td><td>"+fila.contrato+"</td><td style=text-align:right>"+formatCurrency(fila.valortotal)+"</td><td style=text-align:right>"+formatCurrency(fila.aiu)+"</td><td style=text-align:right>"+formatCurrency(fila.totalneto)+"</td><td style=text-align:right>"+formatCurrency(fila.baseaporte)+"</td><td style=text-align:right>"+formatCurrency(fila.aportes)+"</td></tr>");
		});
	});	
}

/*** OPERACIONES ***/
function sumar(){
	var a=b=c=d=e=t=0;
	a =formatNormal($('#txtinicial').val());
	b =formatNormal($('#txtadicion1').val());
	c =formatNormal($('#txtadicion2').val());
	d =formatNormal($('#txtadicion3').val());
	e =formatNormal($('#txtadicion4').val());				
	
	t = a + b + c + d + e;	
	$('#txtbruto').val(formatMoneda2(t));
	
	var aiu=formatNormal($('#txtaiu').val());		
	
	var a2 = t - aiu;
	var a3 = parseInt( a2 * 0.05 );
	var a4 = parseInt( a3 * 0.04 );		
	a4 = aproximar(a4);
	
	$('#txtneto').val(formatMoneda2(a2));
	$('#txtbase').val(formatMoneda2(a3));
	$('#txtbase2').val(formatMoneda2(a3));		
	$('#txtaporte').val(formatMoneda2(a4));
	
	restar();
}

function restar(){	
	var aporte=nomina=0;
	aporte =formatNormal($('#txtaporte').val());
	nomina =formatNormal($('#txtArealizado').val());
		
	var tapo = aporte - nomina;
	if ( tapo <= 0 ) { tapo=0; }
    else { tapo = aproximar(tapo); }
		
	if ( prestacionServicios == false ) {
		$('#txttotalaporte').val(formatMoneda2(tapo));
	}
}

function calcularApo(){
	var base2=0;
	base2 =formatNormal($('#txtbase2').val())	
	if ( isNaN(base2) || !( parseInt(base2) > 0 ) ) {
		$('#txtbase2').val($('#txtbase').val());
		base2 =formatNormal($('#txtbase2').val());
	}
		
    var apo = parseInt( base2 * 0.04 );
    apo = aproximar(apo);
    
    $('#txtaporte').val(formatMoneda2(apo));
    
    restar();
}

//*** GUARDAR O ACTUALIZAR ***/
function validarCampos(op){
	if(op == 1)
		guardarRegistro();
	else if(op == 2){
		actualizarRegistro();
	}
}

function guardarRegistro(){
	//$("#bGuardar").hide();
	campo99=-1;
	
	if(nuevo==0){
		alert("primero haga click en nuevo!");
		return false;
	}
	 $("#errores ul.Rojo").remove();
	 
	var campo0=$('#txtid').val();
	var campo1=$('#txtfecha').val();
	var campo2=ide1;  //$('#txtnit1').val();
	var campo3=ide2;  //$('#txtnit2').val();
	var campo4=$('#txtcontrato').val();
	var campo5=$('#txtobjeto').val();
	var campo6=formatNormal($('#txtinicial').val());
	var campo7=formatNormal($('#txtbruto').val());
	var campo8=formatNormal($('#txtaiu').val());
	var campo9=formatNormal($('#txtneto').val());
	var campo10=formatNormal($('#txtbase2').val());
	var campo11=formatNormal($('#txtaporte').val());
	var campo12=$('#txtobservaciones').val();
	
	var campo13=formatNormal($('#txtadicion1').val());
	var campo14=formatNormal($('#txtadicion2').val());
	var campo15=formatNormal($('#txtadicion3').val());
	var campo16=formatNormal($('#txtadicion4').val());	
	
	var campo17=formatNormal($('#txtVnomina').val());
	var campo18=formatNormal($('#txtArealizado').val());
	var campo19=formatNormal($('#txttotalaporte').val());
	
	//Validacion de campos obligatorios------
	 var lista="<ul class='Rojo'>";
	 
	 if(campo2==''){
		 lista+='<li>Ingrese el NIT del contratante.</li>';
		 }
	 if(campo3==''){
		 lista+='<li>Ingrese el NIT del contratista.</li>';
		 }
	 if(campo4==''){
		 lista+='<li>Ingrese el No del contrato.</li>';
		 }
	 if(campo5==''){
		 lista+='<li>Ingrese objeto del contrato.</li>';
		 }
		 
	   if(campo6=='' || campo6==0){
		 lista+='<li>Ingrese el VALOR inicial del contrato.</li>';
		 }	   
	   
	   // se valida cuando es prestaci�n de servicios
	   if(prestacionServicios == true){
		   //if($("#txtaiu").val() > 0 || $("#txtbase").val() > 0 || $("#txtbase2").val()>0 || $("#txtaporte").val() > 0)
		   if($("#txttotalaporte").val() > 0)
			   lista+='<li>Si es por prestaci\xf3n de servicios el aporte es de $0.</li>';
		   
		   if(tipoPersonaContrato != 101) // TIPOPERSONA: 101 persona natural
			   lista+='<li>Verifique si la empresa es PERSONA NATURAL (Contrato de prestaci\xf3n de servicios).</li>';
		   
	   }
	   
	   if($("#txtnit1").val() == $("#txtnit2").val())
		   lista+='<li>El contratante y el contratista no pueden ser los mismos.</li>';
	   
	   
			//..MOSTRAR ERRORES..//
        $(lista+"</ul>").appendTo($("#errores"));
        $("#errores ul.Rojo").hide().fadeIn("slow");	 
		 
	//----------------GUARDAR LIQUIDACION SI NO HAY ERRORES------------------------
	
        if($("#errores ul.Rojo").is(":empty")){
        	$.ajax({
				url: 'guardarLiquidacion.php',
				type:"GET",
				dataType:"json",
				data:{v0:campo0,v1:campo1,v2:campo2,v3:campo3,v4:campo4,v5:campo5,v6:campo6,v7:campo7,v8:campo8,v9:campo9,v10:campo10,v11:campo11,v12:campo12,v17:campo17,v18:campo18,v19:campo19},
				async:false,
				success:function(datos){
					if(!(parseInt(datos)>0)){
						alert("No se pudo guardar la Liquidacion, intente de nuevo!");
						$("#bGuardar").show();
						return false;
					}
					
					campo99=datos;					
					if(parseInt(campo13)>0){
						$.get('guardarDetalle.php',{v0:campo13,v99:campo99},function(data){});
					}//end if
					
					if(parseInt(campo14)>0){
						$.get('guardarDetalle.php',{v0:campo14,v99:campo99},function(data){
						});
					}//end if
					
					if(parseInt(campo15)>0){
						$.get('guardarDetalle.php',{v0:campo15,v99:campo99},function(data){
						});
					}//end if
					
					if(parseInt(campo16)>0){
						$.get('guardarDetalle.php',{v0:campo16,v99:campo99},function(data){
						});
					}//end if
					
					alert("Se guard\u00F3 la liquidaci\u00F3n No: "+campo99);
					limpiarCampos();
					$("#txtnit1,#txtnit2").next("span").html("");
					$("#historico tr:not(:first)").remove();//Limpia la tabla historico
					
					nuevo=0;
					prestacionServicios = null;
					tipoPersonaContrato = null;
					//var url=URL+'centroReportes/aportes/empresaContrato/reporte001.php?v0='+campo99;
					var url="http://"+getCookie("URL_Reportes")+"/aportes/empresaContrato/reporte001b.jsp?v0="+campo99;
					window.open(url,"_blank");
				}
			});
		} else {	//end if ul de errores
			$("#bGuardar").show();
		}
}//end function

function actualizarRegistro(){
	$("#bActualizar").hide();
	campo99=-1;
	
	if(nuevo==0){
		alert("primero haga click en nuevo!");
		return false;
	}
	$("#errores ul.Rojo").remove();
	
	var campo0=$('#txtid').val();
	var campo1=$('#txtfecha').val();
	var campo2=ide1;  //$('#txtnit1').val();
	var campo3=ide2;  //$('#txtnit2').val();
	var campo4=$('#txtcontrato').val();
	var campo5=$('#txtobjeto').val();
	var campo6=formatNormal($('#txtinicial').val());
	var campo7=formatNormal($('#txtbruto').val());
	var campo8=formatNormal($('#txtaiu').val());
	var campo9=formatNormal($('#txtneto').val());
	var campo10=formatNormal($('#txtbase2').val());
	var campo11=formatNormal($('#txtaporte').val());
	var campo12=$('#txtobservaciones').val();
	
	var campo13=formatNormal($('#txtadicion1').val());
	var campo14=formatNormal($('#txtadicion2').val());
	var campo15=formatNormal($('#txtadicion3').val());
	var campo16=formatNormal($('#txtadicion4').val());
	
	var campo17=formatNormal($('#txtVnomina').val());
	var campo18=formatNormal($('#txtArealizado').val());
	var campo19=formatNormal($('#txttotalaporte').val());
	
	//Validacion de campos obligatorios------
	 var lista="<ul class='Rojo'>";
	 
	 if(campo2=='')
		 lista+='<li>Ingrese el NIT del contratante.</li>';
	 
	 if(campo3=='')
		 lista+='<li>Ingrese el NIT del contratista.</li>';
	 
	 if(campo4=='')
		 lista+='<li>Ingrese el No del contrato.</li>';
	 
	 if(campo5=='')
		 lista+='<li>Ingrese objeto del contrato.</li>';
		 
	 if(campo6=='')
		 lista+='<li>Ingrese el VALOR inicial del contrato.</li>';

	 // se valida cuando es prestaci�n de servicios
	 if(prestacionServicios == true){
		 if($("#txtaiu").val() > 0 || $("#txtbase").val() > 0 || $("#txtbase2").val()>0 || $("#txtaporte").val() > 0)
			 lista+='<li>Si es por prestaci\xf3n de servicios el aporte es de $0.</li>';
		 if(tipoPersonaContrato != 101) // TIPOPERSONA: 101 persona natural
			 lista+='<li>Verifique si la empresa es PERSONA NATURAL (Contrato de prestaci\xf3n de servicios).</li>';
	 }
	   
	 if($("#txtnit1").val() == $("#txtnit2").val())
		 lista+='<li>El contratante y el contratista no pueden ser los mismos.</li>';
	 
	 //..MOSTRAR ERRORES..//
	 $(lista+"</ul>").appendTo($("#errores"));
	 $("#errores ul.Rojo").hide().fadeIn("slow");	 
		 
	//----------------GUARDAR LIQUIDACION SI NO HAY ERRORES------------------------
	
    if($("#errores ul.Rojo").is(":empty")){
    	
    	//si hace una b�squeda del contrato a modificar    	
    	$.ajax({
			url: URL+'aportes/empresaContrato/buscarContratoCompleto.php',
			cache: false,
			type: "POST",
			dataType:"json",
			data:{nitContratante: $("#txtnit1").val(), nitContratista: $("#txtnit2").val(), numcontrato: $('#txtcontrato').val()},
			async:false,
			success:function(respContratos){
	    		if(respContratos.length > 0){
	    			for(ind in respContratos){
	    				var contrato = respContratos[parseInt(ind)];
	    				
	    				$.ajax({
	    					url: 'guardarLiquidacion.php',
	    					type:"GET",
	    					dataType:"json",
	    					data:{v0:campo0,v1:campo1,v2:campo2,v3:campo3,v4:campo4,v5:campo5,v6:campo6,v7:campo7,v8:campo8,v9:campo9,v10:campo10,v11:campo11,v12:campo12, idContrato: contrato.idliquidacion,v17:campo17,v18:campo18,v19:campo19},
	    					async:false,
	    					success:function(datos){			    		
					    		if((parseInt(datos)>0)){
									campo99=datos;						
									if(parseInt(campo13)>0){
										$.get('guardarDetalle.php',{v0:campo13,v99:campo99},function(data){});
									}//end if
									
									if(parseInt(campo14)>0){
										$.get('guardarDetalle.php',{v0:campo14,v99:campo99},function(data){
										});
									}//end if
									
									if(parseInt(campo15)>0){
										$.get('guardarDetalle.php',{v0:campo15,v99:campo99},function(data){
										});
									}//end if
									
									if(parseInt(campo16)>0){
										$.get('guardarDetalle.php',{v0:campo16,v99:campo99},function(data){
										});
									}//end if
									
									alert("Se actualiz\u00F3 la liquidaci\u00F3n No: "+ $('#txtcontrato').val());
									limpiarCampos();
									$("#txtnit1,#txtnit2").next("span").html("");
									$("#historico tr:not(:first)").remove();//Limpia la tabla historico
									
									nuevo=0;
									prestacionServicios = null;
									tipoPersonaContrato = null;
									//var url=URL+'centroReportes/aportes/empresaContrato/reporte001.php?v0='+campo99;
									var url="http://"+getCookie("URL_Reportes")+"/aportes/empresaContrato/reporte001b.jsp?v0="+campo99;
									window.open(url,"_blank");
					    		} else {
					    			alert("No se pudo guardar la Liquidacion, intente de nuevo!");
									$("#bActualizar").show();
									return false;
					    		}					    			
	    					}
				    	});
	    			}
	    		}else{
	    			alert("No se encuentra el contrato a actualizar.");
	    			$("#bActualizar").show();
	    			return false;
	    		}
			}
    	});
    	
    	
	}//end if ul de errores
}

function consultaDatos(idContrato){
	contratoSeleccionado = null;
	var nitContratante = $("#nitContratanteB").val();
	var nitContratista = $("#nitContratistaB").val();
	var numContrato = $("#numContratoB").val();		
	if(numContrato != '' || nitContratante!= '' || nitContratista!= ''){
		$.ajax({
			url: URL+'aportes/empresaContrato/buscarContratoCompleto.php',
			cache: false,
			type: "POST",
			data: { nitContratante: nitContratante, nitContratista: nitContratista, numcontrato: numContrato },
			async: false,
			dataType:"json",
			success: function(respContratos){
		//$.getJSON(URL+'aportes/empresaContrato/buscarContratoCompleto.php',{ nitContratante: nitContratante, nitContratista: nitContratista, numcontrato: numContrato},function(respContratos){
				if(respContratos.length > 0){
					
					for(ind in respContratos){
						var contrato = respContratos[parseInt(ind)];
						var strTr = "<tr><td><a href='#' onclick='cargarContrato("+ contrato.idliquidacion +")'>"+ contrato.idliquidacion +"</a></td><td>"+ contrato.nit_contratante +"</td><td>"+ contrato.nit_contratista +"</td><td>"+ contrato.contrato +"</td><td>"+ contrato.valorinicial +"</td><td>"+ contrato.valortotal +"</td></tr>";
						$("#listaContratosEncontrados tbody").append(strTr);
					}
				}else{
					alert("No se encontraron contratos.");
				}
			}
		});
	}else{
		alert("No hay criterios de b\xfasqueda.");
	}
}

function cargarContrato(idContrato){
	$.ajax({
		url: URL+'aportes/empresaContrato/buscarContratoCompleto.php',
		cache: false,
		type: "POST",
		data: { idcontrato: idContrato },
		async: false,
		dataType:"json",
		success: function(respContratos){
	//$.getJSON(URL+'aportes/empresaContrato/buscarContratoCompleto.php',{ idcontrato: idContrato},function(respContratos){
			if(respContratos.length > 0){
				for(ind in respContratos){
					var contrato = respContratos[parseInt(ind)];
					$("#txtcontrato").val("");
					$("#txtnit1").val(contrato.nit_contratante).trigger('blur');
					$("#txtnit2").val(contrato.nit_contratista).trigger('blur');
					$("#txtcontrato").val(contrato.contrato);//.trigger('blur');
					$("#txtobjeto").val(contrato.objeto);
					$("#txtVnomina").val(contrato.nomina).trigger('keyup');
					$("#txtinicial").val(contrato.valorinicial).trigger('keyup');					
					$("#txtaiu").val(contrato.aiu).trigger('keyup');
					$("#txtadicion1").val("");
					$("#txtadicion2").val("");
					$("#txtadicion3").val("");
					$("#txtadicion4").val("");
					
					$.ajax({
						url: URL+'aportes/empresaContrato/buscarDetallesLiquidacion.php',
						cache: false,
						type: "POST",
						data: {idcontrato:contrato.idliquidacion},
						async: false,
						dataType:"json",
						success: function(respuesta){
					//$.getJSON(URL+'aportes/empresaContrato/buscarDetallesLiquidacion.php',{idcontrato:contrato.idliquidacion},function(respuesta){
							if(respuesta.length > 0){
								for(detalle in respuesta){
									$("#txtadicion"+(parseInt(detalle)+1)).val(respuesta[detalle].valoradicional).trigger('keyup');;
								}
							}
							//$("#txtadicion4").trigger("blur");
						}						
					});					
					
					$("#txtobservaciones").val(contrato.observaciones);
					$("#txtinicial").trigger('blur');
					$("#txtbase2").val(contrato.baseaporte).trigger('keyup');
					$("#txtbase2").trigger('blur');
					$("#txtArealizado").val(contrato.pagoanticipado).trigger('keyup');
					$("#txtArealizado").trigger('blur');
				}
			}
			$("#capa_buscador_contratos").dialog('close');
		}
	});
	$("#bGuardar").hide();
	$("#bActualizar").show();
	$('#txtnit1').attr('disabled', true);
	$('#txtnit2').attr('disabled', true);
	$('#txtcontrato').attr('disabled', true);
	return false;
}

function mostrarBuscador(){
	if(nuevo==0){
		alert("primero haga click en nuevo!");
		return false;
	}
	
	$("#capa_buscador_contratos").dialog('open' );
}

function formatMoneda(input){
	var num = input.value.replace(/\./g,'');
	if(!isNaN(num)){
		num = parseInt(num);
		if( isNaN(num) ) num=0;
		num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1.');
		num = num.split('').reverse().join('').replace(/^[\.]/,'');
		input.value = num;
	} else { alert('Solo se permiten numeros');
		input.value = 0; 
	}
}

function formatMoneda2(input){
	var num = input.toString().replace(/\./g,'');
	if(!isNaN(num)){
		num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1.');
		num = num.split('').reverse().join('').replace(/^[\.]/,'');
		return num;
	} else { return 0; }
}

function formatNormal(input){
	var txt=input.replace(/\./g,'');
	
	if(txt.length > 0){
		if(isNaN(txt))
			return 0;
		else{
			if(txt>0)
				return parseInt(txt);
			else
				return 0;
		}
	} else {
		return 0;
	}	
}

function aproximar(input){
	/*
	var tmp = input % 1000;
    if ( tmp < 500 ) { input = input - tmp; } 
    else { input = input + ( 1000 - tmp ); }
    */    
    return input;
}