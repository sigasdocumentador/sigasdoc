<?php
/* autor:       orlando puentes
 * fecha:       24/07/2010
 * objetivo:    Procesar los movimientos de los Bonos almacenados en la base de datos. 
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'aportes' . DIRECTORY_SEPARATOR .'empresaContrato'. DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'empresasContrato.class.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

$campo0=$_GET['v0']; //
$campo1=$_GET['v1']; //
$campo2=$_GET['v2']; //idcontratatnte
$campo3=$_GET['v3']; //idcontratista
$campo4="'".$_GET['v4']."'"; //contrato
$campo5="'".$_GET['v5']."'"; //objeto
$campo6=$_GET['v6']; //valorini
$campo7=$_GET['v7']; //valortotal
$campo8=$_GET['v8']; //aiu
$campo9=$_GET['v9']; //totalneto
$campo10=$_GET['v10']; //baseaporte
$campo11=$_GET['v11']; //aporte
$campo12="'".$_GET['v12']."'"; //observaciones
$campo13="'".$_SESSION['USUARIO']."'"; //usuario
$campo17=(isset($_REQUEST["v17"]))?intval($_REQUEST["v17"]):0; //ValorNomina
$campo18=(isset($_REQUEST["v18"]))?intval($_REQUEST["v18"]):0; //AporteRealizado
$campo19=(isset($_REQUEST["v19"]))?intval($_REQUEST["v19"]):0; //TotalAporte

$idContrato = (isset($_REQUEST["idContrato"]))?intval($_REQUEST["idContrato"]):null;
$objContrato = new EContrato();

if(intval($idContrato)>0){
	
	// limpiar las adiciones del contrato
	$objContrato->limpiar_detalles_liquidacion($idContrato);
	
	$sql="UPDATE aportes002 SET objeto=$campo5, valorinicial=$campo6, valortotal=$campo7, aiu=$campo8, totalneto=$campo9, baseaporte=$campo10, aportes=$campo11, observaciones=$campo12, fechasistema=cast(getdate() as date), usuario =$campo13, nomina=$campo17, pagoanticipado=$campo18, aporteneto=$campo19 WHERE idliquidacion=$idContrato";
	$rs=$db->queryActualiza($sql);
	if(is_null($rs) || $rs == 0)
		echo 0;
	else
		echo $idContrato;
}else{
	$sql="insert into aportes002(idcontratante, idcontratista, contrato, objeto, valorinicial, valortotal, aiu,totalneto, baseaporte, aportes, observaciones, fechasistema, usuario, nomina, pagoanticipado, aporteneto) values($campo2,$campo3,$campo4,$campo5,$campo6,$campo7,$campo8,$campo9,$campo10,$campo11,$campo12,cast(getdate() as date),$campo13,$campo17,$campo18,$campo19)";
	$rs=$db->queryInsert($sql,'aportes002');
	if(is_null($rs))
		echo 0;
	else
		echo $rs;
}



/*$objClase= new EContrato;
$r=$objClase->insert_liquidacion(array($campo0,$campo1,$campo2,$campo3,$campo4,$campo5,$campo6,$campo7,$campo8,$campo9,$campo10,$campo11,$campo12,$campo13));
if ($r == true){
		$row = ifx_getsqlca($r);
        $rowid = $row['sqlerrd1'];
		echo $rowid;
	}else{
		echo 0;
	} */
?>