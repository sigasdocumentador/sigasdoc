<?php
/**
 * @author Oswaldo Gonzalez
 * @date 5-jun-2012
 */
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$usuario = $_SESSION['USUARIO'];
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID == null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
include_once $raiz . DIRECTORY_SEPARATOR .'config.php'; 
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'session.php';
session();
include_once $raiz . DIRECTORY_SEPARATOR . 'funcionesComunes' . DIRECTORY_SEPARATOR . 'funcionesComunes.php';

$directorio = $ruta_cargados. "pignoracion" .DIRECTORY_SEPARATOR. "cargados". DIRECTORY_SEPARATOR;
$directorio2= $ruta_cargados. "pignoracion" .DIRECTORY_SEPARATOR. "procesados". DIRECTORY_SEPARATOR;
$dir = opendir($directorio);
while ($elemento = readdir($dir)){
	if(strlen($elemento)>2)
		$archivos[]=$elemento;
} 
closedir($dir);
//T636480128.TXT
$nombreArchivoProcesar = $archivos[0];
//@todo Pendiente definir validación para el nombre del archivo.

for($i=0; $i<count($archivos); $i++){
	echo "<br>Procesando archivo: ".$archivos[$i] ."<br/>";
		$nombreArchivoProcesar = $archivos[$i];
		$directorio .= $archivos[$i];
	
		$archivoS = file($directorio);
		$lineas = count($archivoS);
		$grabados = 0;
		foreach($archivoS as $ind => $strRegistro){
			// la primera cadena tiene las columnas
			if($ind > 0){
				$arrErroresTmp = array();
				$arregloTmp = explode(",",$strRegistro);
				$arregloTmp = array_map('trim', $arregloTmp);
				$tipoDoc = $arregloTmp[0];
				$cedula = $arregloTmp[1];
				$fecha = $arregloTmp[2];
				$total = $arregloTmp[3];
				$idPagare = $arregloTmp[4];
				$idConvenio = $arregloTmp[5];
				$factura = $arregloTmp[6];
				$caja = $arregloTmp[7];
				$periodo = $arregloTmp[8];
				$sql="SELECT idpersona FROM aportes015 WHERE idtipodocumento='$tipoDoc' AND identificacion='$cedula'";
				$rs=$db->querySimple($sql);
				$row=$rs->fetch();
				if(is_array($row))
					$afiliado=$row['idpersona'];
				else 
					$arrErroresTmp["afiliado"] = "No se encuentra el afiliado.";
				
				if(!is_numeric($tipoDoc))
					$arrErroresTmp["tipodoc"] = "Verifique el tipo de documento del afiliado.";
				
				if($cedula == '')
					$arrErroresTmp["tipodoc"] = "Verifique el tipo de documento del afiliado.";

				//if(!preg_match("@^([0-9]{1,2})/([0-9]{1,2})/([0-9]{4})$@", $fecha))
				if(!preg_match("@^([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})$@", date("Y/m/d",strtotime($fecha))))
					$arrErroresTmp["fecha"] = "Verifique el formato de la fecha (AAAA/MM/DD). $fecha";
				
				if(!is_numeric($total))
					$arrErroresTmp["total"] = "El total de la pignoraci&oacute;n debe ser un n&uacute;mero.";
				
				if(!is_numeric($idPagare))
					$arrErroresTmp["idpagare"] = "El ID del pagare debe ser un n&uacute;mero.";
				
				if(!is_numeric($idConvenio))
					$arrErroresTmp["idconvenio"] = "El ID del convenio debe ser un n&uacute;mero.".$idConvenio;
				
				if(!is_numeric($factura))
					$arrErroresTmp["factura"] = "La factura debe ser n&uacute;mero.";
					
				if(!is_numeric($caja))
					$arrErroresTmp["caja"] = "La Caja debe ser n&uacute;mero.";
				
				if(!is_numeric($periodo))
					$arrErroresTmp["periodo"] = "El periodo debe ser n&uacute;mero.";
				if(count($arrErroresTmp) == 0){
					$tipoPago = 'T';
					$estado = 'A';
					$observacion = "PIGNORACION FACTURA $factura, CAJA $caja";
					
					$sql = "INSERT INTO aportes043 (idtrabajador, tipopago, valorpignorado, idconvenio, fechapignoracion, estado, saldo, observaciones, pagare, facturanumero, usuario, fechasistema, periodoinicia) values ($afiliado, '$tipoPago', $total, $idConvenio, '$fecha', '$estado', $total, '$observacion', '$idPagare', '$factura', '{$_SESSION["USUARIO"]}', cast(getdate() as date), '$periodo')";
					$rs1 = $db->queryInsert($sql,"aportes043");
			 	    if($rs1 == 0) {
						echo "No se pudo grabar el registro: ". ($ind+1) ."\r\n";
						//exit();
					}else
						$grabados++;
				}else{
					//echo "<p>Verifique los datos del registro ". ($ind+1) .":</p>";
					echo "Errores en registro ". ($ind+1) .": ". implode(",",$arrErroresTmp) ."<br/>";
				}
			}
		}

		echo "<br/>Archivo procesado: ". $archivos[$i];
		$viejo = $directorio;
		$nuevo = $directorio2 . $archivos[$i];
		if(moverArchivo($viejo,$nuevo) == false)
			echo "<br>No se pudo mover el archivo: $viejo";
}
?>