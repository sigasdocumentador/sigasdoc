var url = "",
	urlProceso = "";


$(function(){
	
	url = src();
	urlProceso = url+"aportes/procesoPignoracion/procesarPignoracion.log.php";
	
	$("#btnProcesar")
			.button()
			.click(procesar);
	
});


function nuevo(){
	$("#tblProceso tbody").html("");
	$("#tdBandera,#tdDetalle").html("");
}

function procesar(){
	
	nuevo();
	
	//Validar los archivos
	if(document.getElementById("filArchivo").files.length==0){
		alert("Debe seleccionar los archivos a cargar!");
		return false;
	}
	
	var formDatos = new FormData(); 
    //Obtener los datos de la imagen 
    var inputFileImage = document.getElementById("filArchivo").files[0];
    formDatos.append('filArchivo',inputFileImage);
	
	var dataFormu = formDatos;
	
	$.ajax({
		url:urlProceso,
		type:"POST",
		data:dataFormu,
		dataType: "json",
		async:false,
        contentType:false,
        processData:false,
        cache:false,		
        beforeSend: function(objeto){
        	dialogLoading('show');
        },
        complete: function(objeto, exito){
        	dialogLoading('close');
        },
		success:function(data){
			
			var htmlTotal = "";
			
			if(data.error==0){
				alert("El proceso termino correctamente");
				htmlTotal = "<b>TOTAL:</b> VALOR (<b>"+formatCurrency(data.data.total.total_valor)+"</b>)" 
						+ " - REG GUARDADOS (<b>"+data.data.total.total_registros_guardados+"</b>)"
						+ " - REG ERROR (<b>"+data.data.total.total_registros_error+"</b>)"
						+ " - REG (<b>"+(data.data.total.total_registros_error+data.data.total.total_registros_guardados)+"</b>)";
				ctrLogError(data.data.ok,"C");
				
			}else if( typeof data != "object"){
				alert("Error desconocido");
			}else if(data.error==1){
				alert(data.descripcion);	
			}else if(data.error==2){
				//Error al validar los tipos de datos
				$("#tdBandera").html("Error al validar los tipos de datos, aun no se ha grabado ninguna pignoracion!!");
				ctrLogError(data.data.error,"A");
			}else if(data.error==3){
				//Error al guardar la pignoracion
				$("#tdBandera").html("Error al guardar alguna pignoracion");
				ctrLogError(data.data.error,"B");
				
				htmlTotal = "<b>TOTAL:</b> VALOR (<b>"+formatCurrency(data.data.total.total_valor)+"</b>)" 
						+ " - REG GUARDADOS (<b>"+data.data.total.total_registros_guardados+"</b>)"
						+ " - REG ERROR (<b>"+data.data.total.total_registros_error+"</b>)"
						+ " - REG (<b>"+(data.data.total.total_registros_error+data.data.total.total_registros_guardados)+"</b>)";
		
			}
			
			$("#tdDetalle").html(htmlTotal);
		}
	});
	
}


//funcion que genera el archivo excel ya sea de los errores o la pignoraciones registradas
function generarExcel(){
	var f = new Date();
	$("#reportePingoracion").table2excel({
	       exclude: ".excludeThisClass",
	       name: "Worksheet Name",
	       filename: "reporte"+f.getFullYear()+f.getMonth()+f.getDate()+f.getHours()+":"+f.getMinutes()+":"+f.getSeconds() //do not include extension
	 });
}

function ctrLogError(objLog,tipoLog){
	var htmlLogError = "<tr><td><label style='cursor:pointer;' onclick='generarExcel();' id='generarExcel'><img src='"+ url +"imagenes/icono_excel.png' height='32' width='32'></label></td></tr>"; 
	htmlLogError += "<tr><td><table id='reportePingoracion' class='tablero' width='100%'>";
	
	if(tipoLog=="A"){
		
		htmlLogError += "<tr><th width='10%'>Archivo</th>"
						+ "<th># Reg</th>"
						+ "<th>Error</th></tr>";
		
		$.each(objLog,function(i,row){
			htmlLogError += "<tr><td> "+row.nom_archivo+" </td>" 
								+ "<td> "+row.numero_registro_archivo+" </td>"
			                    + "<td> "+row.mensaje_error+" </td></tr>";
		});
		
	}else if(tipoLog=="B"){
		
		htmlLogError += "<tr><th width='10%'># Reg</th>"
					+ "<th>Tipo Identificacion</th>"
					+ "<th>Numero Identificacion</th>"
					+ "<th width='50%'>Mensaje</th></tr>";
		
		$.each(objLog,function(i,row){
			htmlLogError += "<tr><td>"+row.numero_registro_archivo+"</td>"
					+ "<td>"+row.tipo_documento+"</td>"
					+ "<td>"+row.numero_identificacion+"</td>"
					+ "<td>"+row.mensaje_error+"</td></tr>";
		});
		
	}else if(tipoLog=="C"){
		
		htmlLogError += "<tr><th width='10%'>Identificacion</th>"
			         + "<th>Nombre Trabajador</th>"
			         + "<th>Fecha Pignoracion</th>"
			         + "<th>valor</th>"
			         + "<th width='50%'>Detalle</th></tr>";

        $.each(objLog,function(i,row){
 	       htmlLogError += "<tr><td>"+row.numero_identificacion+"</td>"
			            + "<td>"+row.nomtrabajador+"</td>"
			            + "<td>"+row.fechapignoracion+"</td>"
			            + "<td>"+formatCurrency(row.valorpignorado)+"</td>"
			            + "<td>"+row.observaciones+"</td></tr>";
        });
		
	}
	
	htmlLogError += "</table></td></tr>";
	

	$("#tblProceso tbody").html(htmlLogError);
}