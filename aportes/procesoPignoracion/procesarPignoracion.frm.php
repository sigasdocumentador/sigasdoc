<?php

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$url=$_SERVER['PHP_SELF'];
include_once $raiz.DIRECTORY_SEPARATOR.'phpComunes'.DIRECTORY_SEPARATOR.'auditoria.php';
auditar($url);

include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';

$dirProcesar = $ruta_cargados.'planillas'. DIRECTORY_SEPARATOR . 'procesados' . DIRECTORY_SEPARATOR;
$dirPlanos = $ruta_cargados.'planillas'. DIRECTORY_SEPARATOR .'cargados'. DIRECTORY_SEPARATOR;

?>
<!DOCTYPE html">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>::Procesar Pignoraci&oacute;n::</title>
		
		<link href="../../css/Estilos.css" rel="stylesheet" type="text/css" />
		<link href="../../css/marco.css" rel="stylesheet" type="text/css" />
		<link href="../../newcss/css.1.8/jquery-ui-1.8.17.custom.css" rel="stylesheet" type="text/css" />
		<link href="../../css/fileuploader.css" rel="stylesheet" type="text/css" />
		
		<script type="text/javascript" src="../../newjs/1.6/jquery-1.6.2.min.js"></script>
		<script type="text/javascript" src="../../newjs/1.6/jquery-ui-1.8.16.custom.min.js"></script>
		<script type="text/javascript" src="../../newjs/comunes.js"></script>
		<script type="text/javascript" src="js/procesarPignoracion.js"></script>
		<script src="../../js/jquery.table2excel.min.js"></script>
</head>
<body>
	<div id="wrapTable">
		<form name="form1" enctype="multipart/form-data" action="" method="post">
			<table width="50%" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr>
					<td width="13" height="29" class="arriba_iz">&nbsp;</td>
					<td class="arriba_ce"><span class="letrablanca">::&nbsp;Procesar Pignoraci&oacute;n&nbsp;::</span></td>
					<td width="13" class="arriba_de">&nbsp;</td>
				</tr>
				<tr>
					<td class="cuerpo_iz">&nbsp;</td>
					<td class="cuerpo_ce"> <br /></td>
					<td class="cuerpo_de">&nbsp;</td>
				</tr>
				<tr>
					<td class="cuerpo_iz">&nbsp;</td>
					<!-- TABLAS DE FORMULARIO -->
					<td class="cuerpo_ce" >
						<table width="95%" border="0" cellspacing="0" class="tablero" style="margin:0 auto 0 auto;" id="tblProceso">
							<thead>								
								<tr>
									<td style="text-align: center;">
										<input type="file" name="filArchivo" id="filArchivo"/>
									</td>
								</tr>							
								<tr>
									<td style='text-align:center;'>
										<input type='button' name='btnProcesar' id='btnProcesar' value='PROCESAR'/>
									</td>
								</tr>
								<tr>
									<td id="tdDetalle" style="text-align: center;"></td>
								</tr>
								<tr>
									<td id="tdBandera" style="text-align:center; color: red; font-weight: bold;">&nbsp;</td>
								</tr>
							</thead>
							<tbody>
								<tr><td>&nbsp;</td></tr>
							</tbody>
						</table>
					<td class="cuerpo_de"></td>
				<tr>
					<td height="41" class="abajo_iz">&nbsp;</td>
					<td class="abajo_ce"></td>
					<td class="abajo_de">&nbsp;</td>
				</tr>
			</table>
		</form>
	</div>	
</body>

</html>