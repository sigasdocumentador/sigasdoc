<?php
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
include_once $raiz.DIRECTORY_SEPARATOR. 'config.php';

include_once $raiz . DIRECTORY_SEPARATOR . 'aportes' . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'maestro.proceso.class.php';

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID == null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

$uriPlanoCargado = $ruta_cargados. "pignoracion" .DIRECTORY_SEPARATOR. "cargados". DIRECTORY_SEPARATOR;
$uriPlanoProcesado = $ruta_cargados. "pignoracion" .DIRECTORY_SEPARATOR. "procesados". DIRECTORY_SEPARATOR;

//Obtener parametros
$file = isset($_FILES)?$_FILES:null;
$usuario = $_SESSION["USUARIO"];

$arrResultado = array("error"=>0,"data"=>array(),"descripcion"=>"");
$objMaestroProceso = new MaestroProceso();

$arrArchivos = array();
$arrDataPignoracion = array();

//Cargar el archivo al servidor

$resultado = $objMaestroProceso->carga_archivo_manual($uriPlanoCargado,"txt", array("txt","text","text/plain"),$file);
if($resultado){
	
	//Obtener los archivos cargados
	$arrArchivos = $objMaestroProceso->fetch_archivo_cargado($uriPlanoCargado);
	
	if(count($arrArchivos)==0){
		$arrResultado["error"]=1;
		$arrResultado["descripcion"]="No se puede obtener el archivo del servidor";
	}
	
}else{
	$arrResultado["error"]=1;
	$arrResultado["descripcion"]="No se puede cargar el archivo al servidor";
}

//Validar los datos del archivo plano

if($arrResultado["error"]==0 && count($arrArchivos)>0){
	
	$arrLogError = array();
	
	foreach($arrArchivos as $archivo){
		
		//Datos generales del archivo
		$nombreArchivo = $archivo;
		
		//Directorios
		$uriPlanoCargaActua = $uriPlanoCargado . $nombreArchivo;
		
		//Obtener las lineas del archivo
		$arrLineaArchivo = file($uriPlanoCargaActua);
		
		//Recorrer las lineas del archivo
		foreach($arrLineaArchivo as $indice =>$lineaArchivo){
			
			
			// la primera cadena tiene las columnas
			if($indice == 0 ) continue;
			
			$arrDatosPignoracion = explode(",",$lineaArchivo);
			$arrDatosPignoracion = array_map('trim', $arrDatosPignoracion);
			
			$bandeLogError = ""; 
			
			$idAfiliado = 0; 
			$tipoDocumento = $arrDatosPignoracion[0];
			$numeroIdentificacion = $arrDatosPignoracion[1];
			$fecha = $arrDatosPignoracion[2];
			$total = $arrDatosPignoracion[3];
			$idPagare = $arrDatosPignoracion[4];
			$idConvenio = $arrDatosPignoracion[5];
			$factura = $arrDatosPignoracion[6];
			$caja = $arrDatosPignoracion[7];
			$periodo = $arrDatosPignoracion[8];
			
			if(!is_numeric($tipoDocumento))
				$bandeLogError .= "Verifique el tipo de documento del afiliado ($tipoDocumento) - ";
				//$arrErroresTmp["tipodoc"] = "Verifique el tipo de documento del afiliado.";
		
			if($numeroIdentificacion == '')
				$bandeLogError .= "Verifique el tipo de documento del afiliado ($numeroIdentificacion) - ";
				//$arrErroresTmp["tipodoc"] = "Verifique el tipo de documento del afiliado.";
		
			//if(!preg_match("@^([0-9]{1,2})/([0-9]{1,2})/([0-9]{4})$@", $fecha))
			/*if(!preg_match("@^([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})$@", date("Y/m/d",strtotime($fecha))))
				$bandeLogError .= "Verifique el formato de la fecha (AAAA/MM/DD). ($fecha) - ";
				//$arrErroresTmp["fecha"] = "Verifique el formato de la fecha (AAAA/MM/DD). $fecha";*/
			if(!$objMaestroProceso->valdate($fecha))
				$bandeLogError .= "Verifique el formato de la fecha (AAAAMMDD). ($fecha) - ";
		
			if(!is_numeric($total))
				$bandeLogError .= "El total de la pignoraci&oacute;n debe ser un n&uacute;mero ($total) - ";
				//$arrErroresTmp["total"] = "El total de la pignoraci&oacute;n debe ser un n&uacute;mero.";
		
			if(!is_numeric($idPagare))
				$bandeLogError .= "El ID del pagare debe ser un n&uacute;mero ($idPagare) - ";
				//$arrErroresTmp["idpagare"] = "El ID del pagare debe ser un n&uacute;mero.";
		
			if(!is_numeric($idConvenio))
				$bandeLogError .= "El ID del convenio debe ser un n&uacute;mero ($idConvenio) - ";
				//$arrErroresTmp["idconvenio"] = "El ID del convenio debe ser un n&uacute;mero.".$idConvenio;
		
			if(!is_numeric($factura))
				$bandeLogError .= "La factura debe ser n&uacute;mero ($factura) - ";
				//$arrErroresTmp["factura"] = "La factura debe ser n&uacute;mero.";
				
			if(!is_numeric($caja))
				$bandeLogError .= "La Caja debe ser n&uacute;mero ($caja) - ";
				//$arrErroresTmp["caja"] = "La Caja debe ser n&uacute;mero.";
		
			if(!is_numeric($periodo))
				$bandeLogError .= "El periodo debe ser n&uacute;mero ($periodo) - ";
				//$arrErroresTmp["periodo"] = "El periodo debe ser n&uacute;mero.";
			
			$sql="SELECT TOP 1 idpersona,ltrim(pnombre) + ' ' + ltrim(papellido) AS nombre FROM aportes015 WHERE idtipodocumento='$tipoDocumento' AND identificacion='$numeroIdentificacion'";
			$rs=$db->querySimple($sql);
			$row=$rs->fetch();
			if(is_array($row) && count($row)>0){
				$idAfiliado= $row['idpersona'];
				$nomtrabajador= $row['nombre'];
			}else{
				$bandeLogError .= "No se encuentra el afiliado con identificacion ($numeroIdentificacion) - ";
			}
			
			// verificar que el afiliado no tenga pignoraciones activas.
			$sql="SELECT * FROM aportes043 WHERE idtrabajador = {$idAfiliado} AND estado = 'A'";
			$rs=$db->querySimple($sql);
			$rowc = $rs->fetch();
			if(is_array($rowc) && count($rowc)>0){
				$bandeLogError .= "ya existe una pignoracion Activa del afiliado con identificacion ($numeroIdentificacion) - ";
			}
			
			if($bandeLogError!=""){
				$arrLogError[]= array("numero_registro_archivo"=>($indice+1),"mensaje_error"=>$bandeLogError,"nom_archivo"=>$nombreArchivo);			
			}else{
				$tipoPago = 'T';
				$estado = 'A';
				$observacion = "PIGNORACION FACTURA $factura, CAJA $caja";
				
				$arrDataPignoracion[] = array(
						"idtrabajador"=>$idAfiliado
						,"nomtrabajador"=>$nomtrabajador
						, "tipopago"=>$tipoPago
						, "valorpignorado"=>$total
						, "idconvenio"=>$idConvenio
						, "fechapignoracion"=>$fecha
						, "estado"=>$estado
						, "saldo"=>$total
						, "observaciones"=>$observacion
						, "pagare"=>$idPagare
						, "facturanumero"=>$factura
						, "periodoinicia"=>$periodo
						, "numero_registro_archivo"=>($indice+1)
						, "tipo_documento"=>$tipoDocumento
						, "numero_identificacion"=>$numeroIdentificacion
					);
			}
		}
	}
	
	if(count($arrLogError)>0){
		$arrResultado["error"] = 2;
		$arrResultado["data"]["error"] = $arrLogError;
		$arrResultado["descripcion"] = "Error en los datos";
		$objMaestroProceso->vaciar_archivos_cargados($uriPlanoCargado);
	}
}


//Insertar los registros de la pignoracion
if($arrResultado["error"]==0 && count($arrDataPignoracion)>0){
	
	$arrLogError = array();
	$totalValor = 0;
	$totalRegistrosGuardados = 0;
	$totalRegistrosError = 0;
	$arrResultadook = array();
	
	foreach($arrDataPignoracion as $rowDataPignoracion){
		
		//Verificar que no este duplicado
		$sql="SELECT count(*) as contador 
				FROM aportes043 
				WHERE idtrabajador = {$rowDataPignoracion["idtrabajador"]}
					AND tipopago = '{$rowDataPignoracion["tipopago"]}'
					AND valorpignorado = {$rowDataPignoracion["valorpignorado"]}
					AND idconvenio = {$rowDataPignoracion["idconvenio"]}
					AND fechapignoracion = '{$rowDataPignoracion["fechapignoracion"]}'
					AND estado = '{$rowDataPignoracion["estado"]}'
					AND saldo = {$rowDataPignoracion["saldo"]}
					AND pagare = '{$rowDataPignoracion["pagare"]}'
					AND facturanumero = '{$rowDataPignoracion["facturanumero"]}'
					AND periodoinicia = '{$rowDataPignoracion["periodoinicia"]}'";
		
		$rs=$db->querySimple($sql);
		$row=$rs->fetch();
		
		
		
		if(is_array($row) && count($row)>0 && $row["contador"]>0){
						
			$arrLogError[] = array(
					"numero_identificacion"=>$rowDataPignoracion["numero_identificacion"]
					, "tipo_documento"=>$rowDataPignoracion["tipo_documento"]
					, "numero_registro_archivo"=>$rowDataPignoracion["numero_registro_archivo"]
					, "mensaje_error"=>"La pignoracion esta duplicada");
			
			$totalRegistrosError++;
			
		}else{
			
			//Insertar el registro
			$sql = "INSERT INTO aportes043
					(idtrabajador, tipopago, valorpignorado, idconvenio, fechapignoracion, estado, saldo, observaciones, pagare, facturanumero, usuario, fechasistema, periodoinicia)
				values (
					{$rowDataPignoracion["idtrabajador"]}
					, '{$rowDataPignoracion["tipopago"]}'
					, {$rowDataPignoracion["valorpignorado"]}
					, {$rowDataPignoracion["idconvenio"]}
					, '{$rowDataPignoracion["fechapignoracion"]}'
					, '{$rowDataPignoracion["estado"]}'
					, {$rowDataPignoracion["saldo"]}
					, '{$rowDataPignoracion["observaciones"]}'
					, '{$rowDataPignoracion["pagare"]}'
					, '{$rowDataPignoracion["facturanumero"]}'
					, '$usuario'
					, cast(getdate() as date)
					, '{$rowDataPignoracion["periodoinicia"]}')";
					
			$resultado = $db->queryInsert($sql,"aportes043");
			if($resultado == 0) {
				$arrLogError[] = array(
						"numero_identificacion"=>$rowDataPignoracion["numero_identificacion"]
						, "tipo_documento"=>$rowDataPignoracion["tipo_documento"]
						, "numero_registro_archivo"=>$rowDataPignoracion["numero_registro_archivo"]
						, "mensaje_error"=>"Error al guardar la pignoracion en la DB");
				
				$totalRegistrosError++;
			}else{
				$arrResultadook[]=$rowDataPignoracion;
				$totalValor += $rowDataPignoracion["valorpignorado"];
				$totalRegistrosGuardados++;
			}
		}
	}
	
	if(count($arrLogError)>0){
		$arrResultado["error"] = 3;
		$arrResultado["data"]["error"] = $arrLogError;
		$arrResultado["descripcion"] = "Error al guardar algunas pignoraciones";
	}
	
	//Adicionar el detalle de los registros
	$arrResultado["data"]["ok"]=$arrResultadook;
	$arrResultado["data"]["total"] = array("total_valor"=>$totalValor,"total_registros_guardados"=>$totalRegistrosGuardados,"total_registros_error"=>$totalRegistrosError);
	$objMaestroProceso->move_archivos_procesados_ok($uriPlanoCargado,$uriPlanoProcesado);
}

echo json_encode($arrResultado);
?>