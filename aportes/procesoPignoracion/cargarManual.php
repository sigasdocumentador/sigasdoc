<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz .DIRECTORY_SEPARATOR. 'config.php';
include_once $raiz .DIRECTORY_SEPARATOR. 'clases' .DIRECTORY_SEPARATOR. 'p.definiciones.class.php';
$objDefiniciones = new Definiciones();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>:: Carga manual de Pignoraci&oacute;n ::</title>
	<!-- <link rel="stylesheet" type="text/css" href="<?php echo URL_PORTAL; ?>newcss/Estilos.css"  />
	<link rel="stylesheet" type="text/css" href="<?php echo URL_PORTAL; ?>css/estilo_tablas.css" />
	<link type="text/css" href="<?php echo URL_PORTAL; ?>css/formularios/base/ui.all.css" rel="stylesheet" />
	<link href="<?php echo URL_PORTAL; ?>newcss/marco.css" rel="stylesheet" type="text/css" />-->
	
	<!-- <script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/jquery-1.4.2.js"></script>
	<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/formularios/ui/ui.core.js"></script>
	<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/formularios/ui/ui.dialog.js"></script>
	<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/comunes.js"></script>-->
	
	<link type="text/css" href="<?php echo URL_PORTAL; ?>newcss/Estilos.css" rel="stylesheet">
	<link type="text/css" href="<?php echo URL_PORTAL; ?>newcss/css.1.8/jquery-ui-1.8.17.custom.css" rel="stylesheet">
	<link type="text/css" href="<?php echo URL_PORTAL; ?>newcss/marco.css" rel="stylesheet">
	
	<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/1.6/jquery-1.6.2.min.js"></script>
	<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/1.6/jquery-ui-1.8.16.custom.min.js"></script>
	<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/comunes.js"></script>
	
	<script type="text/javascript" src="js/cargarManual.js"></script>
</head>

<body>
	<table border="0" align="center" cellpadding="0" cellspacing="0" width="60%">
		<tbody>
 			<!-- ESTILOS SUPERIOR TABLA-->
		  	<tr>
				<td width="13" height="29" class="arriba_iz">&nbsp;</td>
			    <td class="arriba_ce"><span class="letrablanca">:: Carga manual de Pignoraci&oacute;n ::</span></td>
			    <td width="13" class="arriba_de" align="right">&nbsp;</td>
		  	</tr>
  
  			<!-- ESTILOS ICONOS TABLA-->
		  	<tr>
				<td class="cuerpo_iz">&nbsp;</td>
				<td class="cuerpo_ce">
			  		<img src="<?php echo URL_PORTAL; ?>imagenes/menu/nuevo.png" alt="Nuevo" width="16" height="16" style="cursor:pointer" title="Nuevo" onclick="nuevo()" />
			  		<img src="<?php echo URL_PORTAL; ?>imagenes/spacer.gif" alt="" width="1" height="1" />
			  		<img src="<?php echo URL_PORTAL; ?>imagenes/menu/grabar.png" alt="Guardar" width="16" height="16" style="cursor:pointer" title="Guardar" onclick="guardar();" id="bGuardar"/>  
			  		<img src="<?php echo URL_PORTAL; ?>imagenes/spacer.gif" alt="" width="1" height="1" />
			  		<img src="<?php echo URL_PORTAL; ?>imagenes/menu/visitas.png" alt="Listar" width="16" height="16" style="cursor:pointer" title="Listar" onclick="listar();" id="btnListar"/>
				</td>
				<td class="cuerpo_de">&nbsp;</td>
		  	</tr>
  
  			<!-- ESTILOS MEDIO TABLA-->
  			<tr>
    			<td class="cuerpo_iz">&nbsp;</td>
    			<td class="cuerpo_ce">
   					<table border="0" cellpadding="5" align="center" class="tablero" width="90%">
						<tr>
							<td>Tipo Documento</td>
							<td>
								<select id="tipoDoc" class="box1">
								    <?php
										$consulta = $objDefiniciones->mostrar_datos(1,1);
										while($row = mssql_fetch_array($consulta))
											echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
									?>
		    					</select>
							</td>
							<td>N&uacute;mero</td>
							<td>
								<input id="identificacion" type="text" class="boxfecha" onblur="validarLongNumIdent(document.getElementById('tipoDoc').value,this);buscarPersona(document.getElementById('tipoDoc').value,this.value)" onkeyup="validarCaracteresPermitidos(document.getElementById('tipoDoc').value,this);"/>
								<input type="hidden" name="hidIdPignoracion" id="hidIdPignoracion"/>
								<input id="idPersona" type="hidden" />
							</td>
						</tr>
						<tr>
	  						<td>Nombre afiliado</td>
	  						<td colspan="3">
	  							<label id="lblNombreCompleto"></label>
	  						</td>
						</tr>
						<tr>
	  						<td><label for="valor">Valor a pignorar</label></td>
	  						<td>
	  							<input type="text" name="valor" id="valor" size="10" onkeydown="solonumeros(this);" onkeyup="solonumeros(this);" class="boxfecha"/>
	  						</td>
	  						<td>Fecha Pignoraci&oacute;n</td>
	  						<td>
	  							<input type="text" name="txtFechaPignoracion" id="txtFechaPignoracion" class="boxfecha" readonly="readonly" />
	  						</td>
	  					</tr>
	  					<tr>
	  						<td><label for="motivo">Motivo</label></td>
	  						<td colspan="3">
								<select name="motivo" id="motivo" class="box1">
									<?php
										$consulta = $objDefiniciones->mostrar_datos(25);
										while($row = mssql_fetch_array($consulta))
											echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
									?>
								</select>
	  						</td>
	  					</tr>
	  					<tr>
		  					<td><label for="notas">Notas</label></td>
		  					<td colspan="3"><textarea name="notas" id="notas" cols="80" rows="3"></textarea></td>
	 					</tr>
					</table>
    			</td>
    			<td class="cuerpo_de">&nbsp;</td>
  			</tr>
  
  			<!-- CONTENIDO TABLA-->
  			<tr>
    			<td class="cuerpo_iz">&nbsp;</td>
    			<td class="cuerpo_ce"></td>
    			<td class="cuerpo_de">&nbsp;</td>
  			</tr>
  
  			<!-- ESTILOS PIE TABLA-->
  			<tr>
    			<td class="abajo_iz" >&nbsp;</td>
    			<td class="abajo_ce" ></td>
    			<td class="abajo_de" >&nbsp;</td>
  			</tr>
		</tbody>
	</table>
	
	<!-- DIV LISTAR PIGNORACIONES -->
	<div id="divPignoracion" title="PIGNORACIONES">
		<table class="tablero" id="tblPignoracion" width="100%">
			<thead>
				<tr>
					<th>ID</th>
					<th>VALOR</th>
					<th>FECHA PIGNORACION</th>
					<th>MOTIVO</th>
					<th>ESTADO</th>
				</tr>
			</thead>
			<tbody></tbody>
		</table>
	</div>
</body>
</html>