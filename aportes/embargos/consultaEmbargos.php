<?php
/* autor:       Orlando Puentes
 * fecha:       Septiembre 14 de 2010
 * objetivo:    Consultar en la base de datos Aportes la información de los embargos que tiene el trabajador afiliado. 
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$url=$_SERVER['PHP_SELF'];
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'auditoria.php';
auditar($url);

include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'p.definiciones.class.php';
$objClase= new Definiciones;
$consulta = $objClase->mostrar_datos(1,2);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>::CONSULTA TRABAJADOR::</title>
<link type="text/css" href="../../css/formularios/base/ui.all.css" rel="stylesheet" /> 
<link type="text/css" href="../../css/Estilos.css" rel="stylesheet"/>
<link type="text/css" href="../../css/estilo_tablas.css" rel="stylesheet"/>
<link href="../../css/marco.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../../js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.core.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.tabs.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.draggable.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.resizable.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.dialog.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.button.js"></script>
<script type="text/javascript" src="../../js/comunes.js"></script>
<script type="text/javascript" src="js/consultaEmbargos.js"></script>

<script type="text/javascript">
shortcut.add("Shift+F",function() {
		                                var URL=src();
		var url=URL+"aportes/trabajadores/consultaTrabajador.php";
    	window.open(url,"_blank");
    },{
	'propagate' : true,
	'target' : document 
        });        
</script>

<script language="javascript">
function mostrarAyuda(){
	$("#ayuda").dialog('open' );
	}
function notas(){
	$("#dialog-form2").dialog('open');
	}	
</script>
</head>

<body>
<div id="wrapTable">
  <table width="97%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
      <td width="13" height="29" class="arriba_iz">&nbsp;</td>
      <td class="arriba_ce"><span class="letrablanca">::&nbsp;Consulta De Embargos&nbsp;::</span></td>
      <td width="13" class="arriba_de" align="right">&nbsp;</td>
      </tr>
    
    <tr>     
      <td class="cuerpo_iz">&nbsp;</td>
	<td class="cuerpo_ce">
      <img src="../imagenes/spacer.gif" width="1" height="1"/> 
        <img src="../../imagenes/menu/informacion.png" width="16" height="16" style="border: none; cursor: pointer" title="Manual" onClick="mostrarAyuda();" /> 
        <img src="../../imagenes/menu/notas.png" width="16" height="16" style="cursor: pointer" title="Colaboracíon en línea" onclick="notas();" /> 
        <div id="error" style="color:#FF0000"></div>
      </td>
      <td class="cuerpo_de">&nbsp;</td>	
      </tr>
    
    <tr>
      <td class="cuerpo_iz">&nbsp;</td>
      <!-- TABLAS DE FORMULARIO -->
      <td class="cuerpo_ce">
        <center>
          <table width="90%" border="0" cellspacing="0" class="tablero">
            <tr>
              <td >Tipo documento</td>
              <td >
              <select name="tipoI" class="box1" id="tipoI">
        <option value="0" selected="selected">Seleccione...</option>
        <?php
		while($row=mssql_fetch_array($consulta)){
		echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
			}
        ?>
        </select>
              </td>
               <td>N&uacute;mero</td>
              <td ><input name="numero" type="text" class="box1" id="numero"  /></td>
              <td >
              <input name="buscarT" type="button" class="ui-state-default" id="buscarT" value="Buscar" />
                <span class="Rojo"></span>
               </td>
            </tr>
            
          </table>        
		</center>
        <div id="trabajadores" align="center">
        </div>
		  <td class="cuerpo_de">&nbsp;</td>
      </td></tr>
      <td class="cuerpo_de"></td><!-- FONDO DERECHA -->
    <tr>
      <td class="abajo_iz" >&nbsp;</td>
      <td class="abajo_ce" ></td>
      <td class="abajo_de" >&nbsp;</td>
    </tr>
  </table>
</div>
<div id="icon"><span></span></div>

  
<!-- ayuda en linea -->
<div id="ayuda" title="Manual .:. Consulta Embargos" style="background-image:url(../../imagenes/FondoGeneral0.png)"></div>
<!-- fin ayuda en linea -->

<!-- colaboracion en linea -->

<!-- FORMULARIO OBSERVACIONES-->
<div name="div-observaciones-tab" style="display:none" title="Observaciones de modificaci&oacute;n">
<table class="tablero">
 <tr>
   <td>Usuario</td>
   <td colspan="3" >
   <input name="usuarioObs" id="usuarioObs" class="box1" disabled="disabled" value="<?php echo $_SESSION['USUARIO']?>" /></td>
   </tr>
 <tr>
   <td>Observaciones</td>
   <td colspan="3" >
   <textarea name="observacionGrupo" id="observacionGrupo" cols="45" rows="5" class="boxlargo"></textarea></td>
   </tr>
</table>
<div class="ui-state-highlight ui-corner-all" style="margin: 9px auto; display:none;padding: 6px; text-align: center; border: 1px solid rgb(255, 255, 0); width:300px" id="rtaObservacion"><span class="ui-icon ui-icon-info" style="float: left; margin-right: 0.3em;"></span>Observaci&oacute;n guardada con &eacute;xito.</div>
</div>

<!-- fin colaboracion -->

</body>
</html>