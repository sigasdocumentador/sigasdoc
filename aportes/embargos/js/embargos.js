/*
* @autor:      Ing. Orlando Puentes
* @fecha:      septiembre 6 de 2010
* objetivo:
*/
var URL=src();
var existe=0;
var nuevoR=0;
var idter=0;
var idt=0;
var tieneT=false;
var pn="";
var sn="";
var pa="";
var sa="";
var personaTercero; // contiene los datos del tercero cargado, para luego hacer la actualizaci�n de sus datos.

$(function(){
	$("#fechaE").datepicker({
		changeMonth: true,
		changeYear: true,
		yearRange: '-2:+0',
		minDate: '-6M'
	});
	
	$("#periodoI").datepicker({
		dateFormat: 'mmyy',
		changeMonth: true,
		changeYear: true,
		showButtonPanel: true,
		onClose: function(dateText, inst) {
			var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
			var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
			$(this).val($.datepicker.formatDate('yymm', new Date(year, month, 1)));
			
			$.getJSON(URL+'phpComunes/buscarGiroPorTipo.php',{idpersona: idt, periodo: $.datepicker.formatDate('yymm', new Date(year, month, 1)), tipo:'h'},function(data){
				if(data != 0){
					alert("El trabajador TIENE GIRO ACTUAL para el periodo "+ data.periodo +"\nNo puede embargar para ese periodo.");
					$("#periodoI").val('');
				}else{
					$.getJSON(URL+'phpComunes/buscarGiroPorTipo.php',{idpersona: idt, periodo: $.datepicker.formatDate('yymm', new Date(year, month, 1)), tipo:'a'},function(data2){
						if(data2 != 0){
							alert("El trabajador YA TIENE GIRO HISTORICO para el periodo "+ data2.periodo +"\nNo puede embargar para ese periodo.");
							$("#periodoI").val('');
						}
					});
				}
			});
		}
	});
	
	$("#periodoI").focus(function () {
		$(".ui-datepicker-calendar").hide();
		$("#ui-datepicker-div").position({
			my: "center top",
			at: "center bottom",
			of: $(this)
		});
	});
});
//Dialog Ayuda
function mostrarAyuda(){
	$("#ayuda").dialog('open' );
}

function notas(){
	$("#dialog-form2").dialog('open');

}

$(document).ready(function(){


	//Dialog ayuda
	$("#ayuda").dialog({
		autoOpen: false,
		height: 500,
		width: 750,
		draggable:true,
		modal:false,
		open: function(evt, ui){
			$('#ayuda').html('');
			$.get(URL +'help/aportes/ayudaEmbargos.html',function(data){
				$('#ayuda').html(data);
			});
		}
	});
	//Dialog Colaboracion en linea
	$("#dialog-form2").dialog({
		autoOpen: false,
		height: 400,
		width: 500,
		modal: true,
		buttons: {
		'Enviar': function() {
			var bValid = true;
			var campo=$('#notas').val();
			var campo0=$.trim(campo);
			if (campo0==""){
				$(this).dialog('close');
				return false;
			}
			var campo1=$('#usuario').val();
			var campo2="radicacion.php";
			$.post(URL+'phpComunes/colaboracion.php',{v0:campo0,v1:campo1,v2:campo2},
			function(datos){
				if(datos=='1'){
					alert("Su comentario fue enviado correctamente!. Gracias por participar en el mejoramiento de SIGAS");
				}else{
					alert(datos);
				}
			});
			$(this).dialog('close');
		},
		Cancelar: function() {
			$(this).dialog('close');
			$("#dialog-form2").dialog("destroy");
		}
		}
	});

	var patron=new RegExp('[\u00F1|\u00D1]');
	$("#pnombre").blur(function(){
		pn=$(this).val();
		//---Reemplazar Ñ
		for(i=1;i<=pn.length;i++){
			pn=pn.replace(patron,"&");
		}
		//-------------
		txt=pn+" "+sn+" "+pa+" "+sa;
		txt=txt.toUpperCase();


		$("#nombreCorto").val(txt)
		num=parseInt(txt.length);
		$("#nombreCorto").next("span").html(num);
		$("#nombreCorto").trigger("change");
	});

	$("#snombre").blur(function(){
		sn=$(this).val();
		//---Reemplazar Ñ
		for(i=1;i<=sn.length;i++){
			sn=sn.replace(patron,"&");
		}
		//-------------
		txt=pn+" "+sn+" "+pa+" "+sa;
		txt=txt.toUpperCase();
		$("#nombreCorto").val(txt);
		num=parseInt(txt.length);
		$("#nombreCorto").next("span").html(num);
		$("#nombreCorto").trigger("change");
	});
	$("#papellido").blur(function(){
		pa=$(this).val();
		//---Reemplazar Ñ
		for(i=1;i<=pa.length;i++){
			pa=pa.replace(patron,"&");
		}
		//-------------
		txt=pn+" "+sn+" "+pa+" "+sa;
		txt=txt.toUpperCase();
		$("#nombreCorto").val(txt)
		num=parseInt(txt.length);
		$("#nombreCorto").next("span").html(num);
		$("#nombreCorto").trigger("change");
	});
	$("#sapellido").blur(function(){
		sa=$(this).val();
		//---Reemplazar Ñ
		for(i=1;i<=sa.length;i++){
			sa=sa.replace(patron,"&");
		}
		//-------------
		txt=pn+" "+sn+" "+pa+" "+sa;
		txt=txt.toUpperCase();
		$("#nombreCorto").val(txt);
		num=parseInt(txt.length);
		$("#nombreCorto").next("span").html(num);
		$("#nombreCorto").trigger("change");
	});
	
	var nu=0;
	var i;
	$("#nombreCorto").change(function(){
		if(num>30){
			if(sn.length>0){
				sn=sn.slice(0,1);
				txt=pn+" "+sn+" "+pa+" "+sa;
				txt=txt.toUpperCase();
				$("#nombreCorto").val(txt);
				num=parseInt(txt.length);
				$("#nombreCorto").next("span").html(num);
			}
			if(num>30)
			if(sa.length>0){
				sa=sa.slice(0,1);
				txt=pn+" "+sn+" "+pa+" "+sa;
				txt=txt.toUpperCase();
				$("#nombreCorto").val(txt);
				num=parseInt(txt.length);
				$("#nombreCorto").next("span").html(num);
			}
		}
	});
});//fin ready

function nuevo(){
	nuevoR=1;
	$("#bGuardar").show();
	limpiarCampos();
	$("#beneficiarios tr:not(:first)").remove();
	existe=0;
	idter=0;
	idt=0;
	tieneT=false;
	$("#fecha").val(fechaHoy());
	
	$('#pendientesEmb option').remove();
	$.post(URL+'phpComunes/buscarRadicaTipoID.php',{v0:2919},function(datos){
    	var cmbRadicacion = $("#pendientesEmb");
	   	$(cmbRadicacion).html(datos);
	   	if($(cmbRadicacion).children().length==1){
			alert('Lo lamento NO hay radicaciones pendientes de grabar!');
			return false;
	   	}
	});
}

function buscarRadicacion(){
    var campo0 =$('#pendientesEmb').val();
	var idtd=0;
	var numero=0;
	var tipoB=0;
	var codben=0;
	//Buscar datos persona para idpersona
	$.ajax({
		url: URL+'phpComunes/buscarRadicacionIDR.php',
		type: "POST",
		data: {v0:campo0},
		async: false,
		dataType: "json",
		success:function(data){
			$.each(data,function(i,fila){
            	idtd=fila.idtipodocumentoafiliado;
				numero=fila.numero;
			});
			$("#tipoI").val(idtd);
			$("#numero").val(numero).trigger('onblur');
		}
    });
}

function buscarTrabajador(num){
	///Limpiar Variables///
	$("#nombre").val('');
	$("#beneficiarios tr:not(:first)").remove();
	idt='';
	
	if($("#tipoI").val()==0){
		alert("Falta tipo documento");
		$("#tipoI").focus();
		return false;
	}	
	
	num=$.trim(num);
	var campo1=parseInt(num);
	var nom="";
	if(isNaN(campo1))return false;
	var idtd=$("#tipoI").val();
	//q exista como activo o inactico
	$.getJSON(URL+'phpComunes/buscarPersonaAfiliadaActiva2.php',{v0:num,v1:idtd},function(data){
		if(data==0){
			alert("El trabajador no existe, verifique la identificaci\xf3n!");
			$("#numero").val('');
			$("#numero").focus();
			return false;
		}
		$.each(data,function(i,fila){
			if(fila.idformulario==0) { alert("El trabajador esta inactivo o no tiene afiliacion!"); }
			nom=fila.pnombre+" "+fila.snombre+" "+fila.papellido+" "+fila.sapellido;
			idt=fila.idpersona;
			return;
		});
		
		$.getJSON(URL+'phpComunes/buscarGrupoSinC3.php', {v0:idt}, function(data){
			if(data==0){
				alert("El trabajador no tiene Beneficiarios");
				$("#numero").val('');
				$("#numero").focus();
				return false;
			}
			
			$("#nombre").val(nom);
			
			var j=0;			
			$.each(data, function(i,fila){
				var nomb=fila.pnombre+" "+fila.snombre+" "+fila.papellido+" "+fila.sapellido;
				var tercero=fila.idenc+" "+fila.pnc+" "+fila.snc+" "+fila.pac+" "+fila.sac;
				$("#beneficiarios").append("<tr><td align=left ><input type=checkbox id=check"+j+" value="+fila.idbeneficiario+" /></td><td align=left >"+fila.identificacion+"</td><td align=left >"+nomb+"</td><td align=left >"+fila.detalledefinicion+"</td><td>"+fila.embarga+"</td><td align=left >"+tercero+"</td></tr>")
				j++;
			})
		});
	})
}

function buscarTercero(num){
    limpiarTercero();
	if($("#tipoIT").val()==0){
		alert("Falta tipo documento");
		$("#tipoIT").focus();
		return false;
	}
	var i=parseInt(num);
	if(isNaN(i))
		return false;
	var idtd=$("#tipoIT").val();
	var x=$("#tipoE").val();
	if(x==0){
		alert("Falta tipo de embargante!");
		$("#tipoE").focus();
		return false;
	}

	$.getJSON(URL+'phpComunes/buscarPersona2.php',{v0:idtd,v1:num},function(data){
		if(data==0){
			existe=0;
			alert("El tercero no existe en el sistema.\n Sera creado.");
			return false;
		}

		$.each(data,function(i,fila){
			idter=fila.idpersona;
			personaTercero = fila;
			$('#pnombre').val($.trim(fila.pnombre));
			$('#snombre').val($.trim(fila.snombre));
			$('#papellido').val($.trim(fila.papellido));
			$('#sapellido').val($.trim(fila.sapellido));
			$("#nombreCorto").val(fila.nombrecorto);
			$("#cboDepto").val(fila.iddepresidencia).trigger('change');
			setTimeout((function(){
				$("#cboCiudad").val(fila.idciuresidencia);
				$("#cboCiudad").trigger("change");
			}),1000);
			setTimeout((function(){
				$("#cboZona").val(fila.idzona);
				$("#cboZona").trigger("change");
			}),2000);
			existe=1;
			return;
		}); //each
		
		///Solicitaron dejar que todos los beneficiarios del trabajador puedan ser embargados por cualquiera 		
		if(x == 1){
			$.get(URL+'phpComunes/buscarConyugeID.php', {v0:idt,v1:idter}, function(data){
				if(data==0){
					alert("El tercero no tiene relaci\xf3n de c\xf3nyuge con el trabajador!");
					//nuevo();
					limpiarTercero();
					$('#numeroT').val('');					
					return false;
				}
			});
			//$("#beneficiarios tr:not(:first)").remove();
			//buscarBeneficiarios(idt,idter,x);
		}
		if(x==2){
			$.get(URL+'phpComunes/buscarPadreID.php', {v0:idt,v1:idter}, function(data){
				if(data==0){
					alert("El tercero no tiene relaci\xf3n de padre/madre con el trabajador!");
					//nuevo();
					$('#numeroT').val('');
					limpiarTercero();
					return false;
				}
			});
			//$("#beneficiarios tr:not(:first)").remove();
			//buscarBeneficiarios(idt,idter,x);
		}
		if(x==3){
		}
	});
}

function buscarBeneficiarios(idt,idter,x){
	$.getJSON('control.php', {v0:idt,v1:idter,v2:x}, function(datos){
		if(datos==0){
			alert("El trabajador no tiene Beneficiarios o ya tiene embargo!");
			return false;
		}
		var j=0;
		$.each(datos, function(i,fila){
			var nomb=fila.pnombre+" "+fila.snombre+" "+fila.papellido+" "+fila.sapellido;
			var tercero=fila.idenc+" "+fila.pnc+" "+fila.snc+" "+fila.pac+" "+fila.sac;
			$("#beneficiarios").append("<tr><td align=left ><input type=checkbox id=check"+j+" value="+fila.idbeneficiario+" /></td><td align=left >"+fila.identificacion+"</td><td align=left >"+nomb+"</td><td align=left >"+fila.detalledefinicion+"</td><td>"+fila.embarga+"</td><td align=left >"+tercero+"</td></tr>")
			j++;
		});
	});
}

function guardar(op){
	if(nuevoR == 0){
		alert("Debe seleccionar primero en NUEVO.");
		return false;
	}
		
	$("table.tablero td input:text.ui-state-error").removeClass("ui-state-error");
	$(".ui-state-error").removeClass("ui-state-error");
	var error=0;
	var tipoI=$("#tipoI").val();
	var numero=$("#numero").val();
	var fechaE=$("#fechaE").val();
	var periodoI=$("#periodoI").val();
	var tipoE=$("#tipoE").val();
	var tipoIT=$("#tipoIT").val();
	var numeroT=$("#numeroT").val();
	var pnombre=$("#pnombre").val();
	var snombre=$("#snombre").val();
	var papellido=$("#papellido").val();
	var sapellido=$("#sapellido").val();
	var nombreC=$("#nombreCorto").val();
	var cboDepto=$("#cboDepto").val();
	var cboCiudad=$("#cboCiudad").val();
	var cboZona=$("#cboZona").val();
	var direccion=$("#tdireccion").val();
	var telefono=$("#telefono").val();
	var celular=$("#celular").val();
	var tipoP=$("#tipoP").val();
	var tarjeta=$("#tarjeta").val();
	var cuenta=$("#cuenta").val();
	var juzgado=$("#juzgado").val();
	var banco=$("#banco").val();	
	var resolucion=$("#resolucion").val();
	var notas=$("#notas").val();		
	
	if(tipoE=='0'){
		$("#tipoE").addClass("ui-state-error");
		error++;
	}
	if(resolucion==''){
		$("#resolucion").addClass("ui-state-error");
		error++;
	}
	if(juzgado=='0'){
		$("#juzgado").addClass("ui-state-error");
		error++;
	}
	if(tipoP=='0'){
		$("#tipoP").addClass("ui-state-error");
		error++;
	}
	if(cboDepto=='0'){
		$("#cboDepto").addClass("ui-state-error");
		error++;
	}
	if(cboCiudad=='0'){
		$("#cboCiudad").addClass("ui-state-error");
		error++;
	}
	if(cboZona=='0'){
		$("#cboZona").addClass("ui-state-error");
		error++;
	}
	if(papellido==''){
		$("#papellido").addClass("ui-state-error");
		error++;
	}
	if(pnombre==''){
		$("#pnombre").addClass("ui-state-error");
		error++;
	}
	if(numeroT==''){
		$("#numeroT").addClass("ui-state-error");
		error++;
	}
	if(tipoIT=='0'){
		$("#tipoIT").addClass("ui-state-error");
		error++;
	}
	if(tipoI=='0'){
		$("#tipoI").addClass("ui-state-error");
		error++;
	}
	if(numero==''){
		$("#numero").addClass("ui-state-error");
		error++;
	}
	if(fechaE==''){
		$("#fechaE").addClass("ui-state-error");
		error++;
	}
	if(periodoI==''){
		$("#periodoI").addClass("ui-state-error");
		error++;
	}
	if(periodoI.length != 6){
		$("#periodoI").addClass("ui-state-error").val("Los periodos tienen 6 digitos");
		error++;
	}
	
	if(error>0){
		alert("Corrija los errores encontrados");
		$("#bGuardar").show();		
		return false;
	}
	
	var con = 0;
	var idb = 0;
	if($("#beneficiarios tr").length>1){
		$("#beneficiarios tr:not(':first')").each(function(index){
			if($("#check"+index).is(':checked')==true){
				idb = $("#check"+index).val();
				con++;
			}
				
		});
	} else {
		alert("No existen beneficiarios");
		return false;
	}
	
	
	if(con==0){
		alert("No ha seleccionado los beneficiaros embargantes!");
		return false;
	}
	
	var mensaje="";
	if(existe==0)
		mensaje="Se crear\u00E1 el tercero\n";
	if(tieneT==false && tipoP=='T')	
		mensaje+="Se generar\u00E1 Bono para el tercero";
	if(mensaje.length>0)
		alert(mensaje);	
	
	if(existe == 0){
		// si el tercero no existe, se debe crear
        var cont=1;
        var v="";
        $.ajax({
            type:"POST",
            url:URL+'phpComunes/guardarPersonaCompleta.php',
            data:"v0="+v+"&v1="+v+"&v2="+tipoIT+"&v3="+numeroT+"&v4="+pnombre+"&v5="+snombre+"&v6="+papellido+"&v7="+sapellido+"&v8="+v+"&v9="+direccion+"&v10=0&v11="+telefono+"&v12="+celular+"&v13="+v+"&v14="+v+"&v15="+cboDepto+"&v16="+cboCiudad+"&v17="+cboZona+"&v18="+v+"&v19="+v+"&v20="+v+"&v21="+v+"&v22="+v+"&v23="+v+"&v24="+existe+"&v25="+v+"&v26="+nombreC,
            async:false,
            success:function(datos){
	            if(datos == 0){
	            	alert("No se pudo guardar los datos del Tercero!");
	                cont = 0;
	                return false;
				}
                idter = datos;
                alert("Se guardaron los datos del tercero!");
            }
        });
    }else{
		//actalizar datos en personas...
    	var v0=idter; // idpersona
    	var v1= $("#tipoIT").val();			//idtipodocumento
    	var v2= $("#numeroT").val();			//identificacion
    	var v3=	$("#papellido").val(); 		//papellido
    	var v4=	$("#sapellido").val();		//sapellido
    	var v5=	$("#pnombre").val();			//pnombre
    	var v6=	$("#snombre").val();			//snombre
    	var v7=	personaTercero.sexo;			//sexo
    	var v8=	$("#tdireccion").val();		//direccion
    	var v9=	personaTercero.idbarrio;			//idbarrio
    	var v10= $("#telefono").val();		//telefono
    	var v11= $("#celular").val();		//celular
    	var v12= personaTercero.email;			//email
    	var v13= personaTercero.idpropiedadvivienda;			//idpropiedadvivienda
    	var v14= personaTercero.idtipovivienda;			//idtipovivienda
    	var v15= $("#cboDepto").val();			//iddepresidencia
    	var v16= $("#cboCiudad").val();		//idciuresidencia
    	var v17= $("#cboZona").val();			//idzona
    	var v18= personaTercero.idestadocivil;			//idestadocivil
    	var v19= personaTercero.fechanacimiento;			//fechanacimiento
    	var v20= personaTercero.iddepnace;			//iddepnace
    	var v21= personaTercero.idciunace;		//idciunace
    	var v22= personaTercero.capacidadtrabajo;		//capacidadtrabajo
    	var v23= $("#nombreCorto").val();		//nombrecorto
    	var v24= personaTercero.idprofesion; //idprofesion
    	var v25= personaTercero.rutadocumentos; //rutadocumentos
    	
    	$.post(URL+'phpComunes/actualizarPersona.php',
    		{v0:v0,v1:v1,v2:v2,v3:v3,v4:v4,v5:v5,v6:v6,v7:v7,v8:v8,v9:v9,v10:v10,v11:v11,v12:v12,v13:v13,v14:v14,v15:v15,v16:v16,v17:v17,v18:v18,v19:v19,v20:v20,v21:v21,v22:v22,v23:v23,v24:v24,v25:v25},function(data){
    		if(data==0){
    			MENSAJE("El registro no se pudo actualizar!");
    		}else{
    			MENSAJE("El tercero fue actualizado!");
    		}
	    });
    }	

    if (cont==0 || idter==0){
        alert("No hay ID del tercero!");
        return false;
    }
	var idconyuge =0;
	if(tipoE=='1')
		idconyuge=idter;
	if(tipoP=='T')
		var codigopago ="01=TARJETA";
	else
		var codigopago ="02=JUZGADO";
	var idemb=0;
	$.getJSON('guardarEmbargo.php',{v0:idt, v1:idter, v2:idconyuge, v3:tipoE, v4:fechaE, v5:periodoI, v6:tipoP,v7:codigopago, v8:banco, v9:cuenta, v10:juzgado, v11:resolucion , v12:notas, idBeneficiario: idb}, function(datos){
		if(datos==0){
			alert("No se pudo guardar el Embargo!, intende de nuevo");
			return  false;
		}
		idemb=datos;
		//detalle
		alert("Se guard\xf3 el embargo!");
		$("#beneficiarios tr:not(':first')").each(function(index){
			var idb=$("#check"+index).val();
			if($("#check"+index).is(':checked')==true){
				$.getJSON('guardarDetalle.php', {v0:idemb,v1:idb}, function(data){
					if(data==0){
						alert("No se pudo guardar el detalle del Embargo, favor avisar al administrador del sistema!");
						return false;
					}
					$.getJSON('actualizar21.php', {v0:idb,v1:idt}, function(datas){
						if(datas==0)
						{
							alert("No se pudo actualizar la relaci\xf3n, favor avisar al administrador del sistema!");
							return false;
						}
					});
				});

			}
		});

		if(tieneT==false && tipoP=='T')
			alert("El tercero debe solicitar la tarjeta en ventanilla para que sea asignada.");
	});
	cerrarRadicacion();
	nuevoR=0;
	personaTercero = null;
}

function cerrarRadicacion(){
    var campo0=$("#pendientesEmb").val();
	$.ajax({
		url: URL+'phpComunes/cerrarRadicacion.php',
		type: "POST",
		data: "submit=&v0="+campo0,
		success: function(datos){
			msg="Exito, "+datos;
			alert(msg);
		}
	});
}

function buscarTarjeta(){
    $("#tarjeta").val('');
    tieneT=false;
    
	if($("#tipoP").val()=='T'){
		$("#td_label_tarjeta,#td_tarjeta").show();
		if (idter!='')
	    {
			$.getJSON(URL+'phpComunes/buscarBono.php', {v0:idter}, function(data){
				if(data==0){
					alert("El tercero NO tiene BONO, se le asignar\u00E1 UNO!");
					tieneT=false;
					$("#tarjeta").val('');
				}
				else{
					$.each(data, function(i,fila){
						$("#tarjeta").val(fila.bono);
						tieneT=true;
						return;
					});
				}
			});
		}
	}	
	
	if($("#tipoP").val()=='B'){
		$("#tr_cuenta_bancaria").show();
		$("#td_label_tarjeta,#td_tarjeta").hide();
		$("#tarjeta").val("");
	}else{
		$("#cuenta").val("");
		$("#banco").val(0);
		$("#tr_cuenta_bancaria").hide();
	}
}

function limpiarCampos(){
	$("table.tablero input:text").val('');
	$("table.tablero select").val(0);
	$("table.tablero td input:text.ui-state-error").removeClass("ui-state-error");
	$("#pendientesEmb").focus();
	//$("#tipoI,tipoIT option:first-child").attr("selected","true");
}

function limpiarTercero(){
    $('#pnombre').val('');
    $('#snombre').val('');
    $('#papellido').val('');
    $('#sapellido').val('');
    $("#nombreCorto").val('');
    $("#tarjeta").val('');
    $("#tipoP").val(0);
    $("#Combo1").val(0);
    $("#cboDpto").val(0);
    $("#cboCiudad").val(0);
	$("#cboZona").val(0);
	$("#tdireccion").val('');	
	
    pn="";
    sn="";
    pa="";
    sa="";
    idter='';
    personaTercero='';
    existe=0;
}