/*
* @autor:      Ing. Orlando Puentes
* @fecha:      Octubre 7 de 2010
* objetivo:
*/

var URL=src();

$(document).ready(function(){
//Dialog Colaboracion en linea
$("#dialog-form2").dialog({
		autoOpen: false,
		height: 450,
		width: 700,
		modal: false,
		buttons: {
			'Enviar': function() {
			var bValid = true;
			var campo=$('#notas').val();
			var campo0=$.trim(campo);
			if (campo0==""){
				$(this).dialog('close');
					return false;
				}
			var campo1=$('#usuario').val();
			var campo2="consultaEmbargos.php";
			$.post(URL+'phpComunes/colaboracion.php',{v0:campo0,v1:campo1,v2:campo2},
			function(datos){
				if(datos=='1'){
					alert("Su comentario fue enviado correctamente!. Gracias por participar en el mejoramiento de SIGAS");
				}else{
					alert(datos);
				}
		});
		$(this).dialog('close');
		},
		Cancelar: function() {
			$(this).dialog('close');
			$("#dialog-form2").dialog("destroy");
		}
	}
	});


$(function() {
		$("#ayuda").dialog({
		 	autoOpen: false,
			height: 450, 
			width: 700, 
			draggable:true,
			modal:false,
			open: function(evt, ui){
					$('#ayuda').html('');
					$.get(URL+'help/aportes/ayudaConsultaEmbargos.html',function(data){
							$('#ayuda').html(data);
					})
			 }
		});
	});


//Buscar Trabajador
$("#buscarT").click(function(){
    $("#trabajadores").find("p").remove();
    $("#trabajadores").find("hr").remove();
    if($("#tipoI").val()==0) {
        alert("Falta tipo identificacion");
        $("#tipoI").focus();
        return false;
    }
  //VALIDAR QUE EL identificacion SEA NUMEROS
  var n=parseInt($("#numero").val())
  if( isNaN(n)){
	  $(this).next("span.Rojo").html("La Identificaci\u00F3n debe ser num\u00E9rico.").hide().fadeIn("slow");
	  return false;
  }

if($("#numero").val()==''){
	$(this).next("span.Rojo").html("Ingrese un valor a buscar.").hide().fadeIn("slow");
	$("#numero").focus();
	return false;
}	
		
var v1=$("#numero").val();
var v0=$("#tipoI").val();
var idt=0;
var ide=0;
$.getJSON(URL+'phpComunes/buscarPersonaAfiliada2.php',{v0:v0,v1:v1},function(data){
	if(data==0){
		$("#buscarT").next("span.Rojo").html("Lo lamento el trabajador no existe.").hide().fadeIn("slow");
                return false;
	}
	$.each(data,function(i,fila){
            idt=fila.idpersona;
	    $("#buscarT").next("span.Rojo").html("");
            //MOSTRAR RESULTADOS
            $("#trabajadores").append('<p align=left>Trabajador: <strong>'+fila.idpersona+" "+fila.pnombre+" "+fila.snombre+" "+fila.papellido+" "+fila.sapellido+'</p>');
            $("#trabajadores").append('<hr width=90%>');
            return;
	});//fin each
	$.getJSON(URL+'phpComunes/buscarEmbargos.php', {v0:idt}, function(datos){
        if(datos==0){
            alert("El trabajador NO tiene embargos!");
            return false;
        }
        else{
	        $.each(datos,function(i,fila){
	            var nom=fila.pnombre+" "+fila.snombre+" "+fila.papellido+" "+fila.sapellido;
	            ide=fila.idembargo;
	            $("#trabajadores").append('<p align=left>Embarga: <strong>'+fila.identificacion+" "+nom+'</p>');
	            switch (parseInt(fila.idtipoembargo)) {
	                case 1:var tipo='Conyuge';break;
	                case 2:var tipo='Padre';break;
	                case 3:var tipo='Otro';break;
	            }
	            $("#trabajadores").append('<p align=left>Quien embarga: <strong>'+tipo+"</strong></p>");
	            $("#trabajadores").append('<p align=left>Fecha Embargo: <strong>'+fila.fechaembargo+" Periodo inicio: "+fila.periodoinicio +"</p>");
	            $("#trabajadores").append('<p align=left>Tipo pago: <strong>'+fila.tipopago+" - "+fila.codigopago+"</p>");
	            if(fila.tipopago=='C'){
	                $("#trabajadores").append('<p align=left>Cuenta: <strong>'+fila.cuenta+" - Banco"+fila.idbanco+"</p>");
	            }
	            $("#trabajadores").append('<p align=left>Juzgado: <strong>'+fila.detalledefinicion+" - Oficio: "+fila.resolucion+"</p>");
	            $("#trabajadores").append('<p align=left>Estado: <strong>'+fila.estado+" - Fecha: "+fila.fechaestado+"</p>");
	            $("#trabajadores").append('<p align=left>Motivo: <strong>'+fila.motivoestado+"</p>");
	            $("#trabajadores").append('<p align=left>Notas: <strong>'+fila.notas+"</p>");
	            $("#trabajadores").append('<hr width=90% />');
	        });
        }
	});
	});//fin JSON*/
	});//fin click
//,  , , , , , , , , , notas, estado, , , usuarioestado, fechasistema, 	
//... ICONO PARA OCULTAR/MOSTRAR FORMULARIO ...
$("div#icon span").click(function(){
	$(this).toggleClass("toggleIcon");
	$("div#wrapTable").slideToggle();
});//fin click icon	
});//fin ready



//... FUNCION PARA ENVIAR NIT PARA ACORDEON ...//
function enviarNit(ide){
	var	indexTab;
	
	$("div#icon").show();
	$("#trabajadores").find("p").remove();
	$("div#wrapTable").slideUp("normal");
	$("#tabs").tabs("destroy");//Destruyo tab para nueva consulta
	$("#tabs").show("slow"); 		//Muestro el tab
	
	//Aqui Capturo el valor del ALT para el switch que carque los formularios	 
	$("#tabs ul li a").click(function(){
	indexTab=$(this).attr("alt");
	});
}//END FUNCTION BUSCAR NIT