<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
$usuario=$_SESSION['USUARIO'];
include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'p.definiciones.class.php';
$objClase=new Definiciones();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>...</title>
<link type="text/css" href="../../css/Estilos.css" rel="stylesheet"/>
<link type="text/css" href="../../css/marco.css" rel="stylesheet"/>
<link type="text/css" rel="stylesheet" href="../../css/css.1.8/jquery-ui-1.8.17.custom.css" />
<script language="javascript" src="../../js/1.6/jquery-1.6.2.min.js"></script>
<script language="javascript" src="../../js/1.6/jquery-ui-1.8.16.custom.min.js"></script>
<script language="javascript" src="js/consultaEmbargante.js"></script>

</head>
<body>
	<center>
		<div id="divMensaje"  ></div>
  
		<table width="80%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td class="arriba_iz" >&nbsp;</td>
				<td class="arriba_ce" ><span class="letrablanca">::Embargos a favor del Embargante::</span><input type="hidden" value="" name="hdnIdTrabajador" id="hdnIdTrabajador"/><input type="hidden" id="hdnIdBeneficiarios" name="hdnIdBeneficiarios"/></td>
				<td class="arriba_de" >&nbsp;</td>
			</tr>
			<tr>
				<td class="cuerpo_iz">&nbsp;</td>
				<td class="cuerpo_ce">&nbsp;</td>
				<td class="cuerpo_de">&nbsp;</td>
			</tr>
			<tr>
				<td class="cuerpo_iz" >&nbsp;</td>
				<td class="cuerpo_ce" >
					<table border="0" class="tablero" cellspacing="0" width="100%" >
				  		<tr>
						    <td >Tipo Documento:</td>
						    <td >
						    	<select name="cmbTipoDocumento" id="cmbTipoDocumento"/>
							     <?php 
							     	$rs = $objClase->mostrar_datos(1,1);
							     	while($row=mssql_fetch_array($rs)){
							     		echo "<option value='".$row["iddetalledef"]."'>".$row["detalledefinicion"]."</option>";
							     	}
							     ?>
							    </select>
						    </td>
						    <td>N&uacute;mero:</td>
						    <td colspan="3"><input type="text" name="txtNumero" id="txtNumero" value=""/></td>
				  		</tr>
					  	<tr>
						    <td >Embargante:</td>
						    <td id="tdNombres" colspan="5">&nbsp;</td>
					  	</tr>
					  	<tr>
				      		<td colspan="6" id="tdMensajeError" class="RojoGrande"></td>
					  	</tr>
					  	<tr>
				      		<td colspan="6">Lista de Embargos </td>
					  	</tr>
		  			</table>
				  <table border="0" class="tablero" cellspacing="0" width="100%" id="tabEmbargos"> 
				  
				</table></td>
				<td class="cuerpo_de" >&nbsp;</td>
			</tr>
			<tr>
			<td class="cuerpo_iz" >&nbsp;</td>
			<td class="cuerpo_ce" >&nbsp;</td>
			<td class="cuerpo_de" >&nbsp;</td>
			</tr>
			<tr>
			<td class="abajo_iz" >&nbsp;</td>
			<td class="abajo_ce" ></td>
			<td class="abajo_de" >&nbsp;</td>
			</tr>
</table>

</center>
</body>
</html>
