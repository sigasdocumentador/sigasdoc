/* 
* CONTROL DE VALIDACIONES DE LIQUIDACIONES
* Autor: Andr�s Felipe Lara Nieto
* Fecha: 04-Mar-2011
*/

$(document).ready(function(){
	URL=src();
	
	$("#tablaNominas tbody tr").remove();
	//Sucursales
	$("#txtNit").bind('blur', function(){
		$('option.rmv').remove();
		$.getJSON('reportes/consultarNomxNit.php', ({acc:1, nit:$(this).val()}), function(dat){
			$.each(dat, function(i, n){
				$('#sltSucursal').append('<option value="'+n.idempresa+'" class="rmv">'+n.codigosucursal+' - '+n.estado+'</option>');
			});
		});
	});
	//Razon social
	$('#sltSucursal').bind('change', function(){
		$('#lblRazsoc').html('');
		$.getJSON('reportes/consultarNomxNit.php', ({acc:2, idempresa:$(this).val()}), function(dat){
			$('#lblRazsoc').html(dat[0].razonsocial);
		});
	});
	
 // $("#tablaEmpresa").tablesorter()//.tablesorterPager({container: $("#pager")});
	
	$('#btnConsultar').bind('click', function(){
		if($('#radConsXPeriodo').is(':checked')){
			var ac = 3;
			var pari = $('#txtPerIni').val();
			var parf = $('#txtPerFin').val();
		}else if($('#radConsXFecha').is(':checked')){
			var ac = 4;
			var pari = $('#txtFecIni').val();
			var parf = $('#txtFecFin').val();
		}
		$("#tablaNominas tbody tr").remove();
			  $.ajax({
			      url:'reportes/consultarNomxNit.php',
				  type:"GET",
				  dataType:"json",
				  data: ({acc:ac,idempresa:$('#sltSucursal').val(),parini:pari, parfin:parf}),
				  async:false,
				  success:function(data){
			 			  if(data==0){MENSAJE("NO existen empresas sin convenio de pago.");return false;}
			 			  	var cantNom = 0;
							  $.each(data, function(i,n){
								  $("#tablaNominas tbody").append("<tr><td>"+n.periodo+"</td><td>"+n.tipodocumento+"</td><td>"+n.identificacion+"</td><td>"+n.pnombre+' '+n.snombre+' '+n.papellido+' '+n.sapellido+"</td><td>"+n.horascotizadas+"</td><td>"+n.diascotizados+"</td><td>"+n.salariobasico+"</td><td>"+n.ingresobase+"</td><td>"+n.ingreso+"</td><td>"+n.retiro+"</td><td>"+n.var_tra_salario+"</td><td>"+n.var_per_salario+"</td><td>"+n.sus_tem_contrato+"</td><td>"+n.inc_tem_emfermedad+"</td><td>"+n.lic_maternidad+"</td><td>"+n.vacaciones+"</td><td>"+n.inc_tem_acc_trabajo+"</td><td>"+n.usuario+"</td><td>"+n.fechapago+"</td><td>"+n.fechasistema+"</td></tr>");
								  cantNom++;
		         	          });//end each emrpesa Id
							  alert('Se encontraron '+cantNom+' nominas para la empresa seleccionada');
					},//success
					complete:function(){
						                //Actualizar Tabla de sorter 
										$("#tablaNominas tbody tr:odd").addClass("vzebra-odd");
										$("#tablaNominas").tablesorter({headers: {7: { sorter: false}}});
										$("#tablaNominas").tablesorterPager({container: $("#pager"),seperator:" de "}).trigger("updateCell");
										$("#tablaNominas").trigger("update");
					 }						
			     });//ajax 
	});
  
   
});//end ready




$.ui.dialog.defaults.bgiframe = true;
	$(function() {
		$("#ayuda").dialog({
		 	autoOpen: false,
			height: 450, 
			width: 700, 
			open: function(evt, ui){
					$('#ayuda').html('');
					$.get('../../help/aportes/ayudaConsultaPlanilla.html',function(data){
							$('#ayuda').html(data);
					})
			 }
		})
	});


<!-- colaboracion en linea  -->


	$(function() {
		$("#dialog-form2").dialog("destroy");
		/*var notas = $("#notas"),
			allFields = $([]).add(notas);*/
		
		$("#dialog-form2").dialog({
			autoOpen: false,
			height: 400,
			width: 500,
			modal: true,
			buttons: {
				'Enviar': function() {
					var bValid = true;
					var campo=$('#notas').val();
					var campo0=$.trim(campo);
					if (campo0==""){
						$(this).dialog('close');
						return false;
						}
					var campo1=$('#usuario').val();
					var campo2="agencias.php";
					$.post('../../phpComunes/colaboracion.php',{v0:campo0,v1:campo1,v2:campo2}, function(datos){
						if(datos=='1'){
							alert("Su comentario fue enviado correctamente!. Gracias por participar en el mejoramiento de SIGAS");
						}else{
							alert(datos);
						}
					});
				$(this).dialog('close');
				},
				Cancelar: function() {
					$(this).dialog('close');
					$("#dialog-form2").dialog("destroy");
				}
			}
			
		});
		
		$('#enviar-notas')
			.button()
			.click(function() {
				$('#dialog-form2').dialog('open');
			});
	});
