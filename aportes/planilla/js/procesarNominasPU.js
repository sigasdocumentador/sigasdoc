/**
 * @author JOSE LUIS ROJAS
 * @since 14/10/2010
 * 
 */
var procesarNominasPU = function(evt) {
	var nom = $("input[type=checkbox][id!=chkMarcarTodo]:checked").length;
	var parm = $.param($("input[type=checkbox][id!=chkMarcarTodo]:checked"));
	if (parm != "") {
		var conf = confirm('Desea procesar el/los ' + nom + ' archivo(s) seleccionado(s)?');
		if (conf) {
			$('#lnkCargarArchivos').focus();
			$('#log').html('');
			$('#dialogo-archivo')
					.dialog(
							{
								width : 500,
								height : 500,
								resizable : true,
								modal : true,
								open : function(evt, ui) {
									$(this)
											.append(
													'<div id="divCargando" style="margin: 0 auto">Procesando, no cierre ni interrumpa este proceso... <img id="gifCargando" src="../../imagenes/ajax-loader.gif" alt="Procesando" /></div>');
									$('#pg').html('0');
									$.ajax( {
										url : 'procesarNominasPU.php',
										type : "POST",
										context : $('#log'),
										data : parm,
										success : function(dat) {
											$(this).append(dat);
											if(dat==2)
												{
													alert("Error no se pudo crear el archivo de desbloqueo para el proceso de tesoreria");
												}
											if(dat==3)
												{
													alert("Error no se pudo grabar en el archivo de desbloqueo para el proceso de tesoreria");
												}
										},
										complete : function() {
											$('#divCargando').remove();
											$('#chkMarcarTodo').attr("checked", false);
											alert("Archivos Procesados.");
										}
									});
									nominasProcesadas();
								},
								close : function() {
									$(this).dialog('destroy');
								}
							});
		}
	} else {
		alert('Debe seleccionar por lo menos un archivo a procesar');
	}
};

var nominasProcesadas = function() {
	var total = $("input[type=checkbox][id!=chkMarcarTodo]:checked").length;
	$('#progreso').css( {
		"display" : "block"
	});
	$('#tt').html(total);
	var tid = setInterval(function() {
		$("input[type=checkbox][id!=chkMarcarTodo]:checked").each(
				function(i, dat) {
					var chkArchivo = $(this);
					var idarchivo = $(this).val();

					$.ajax({
						async : false,
						url : 'nominasProcesadas.php',
						context : $(chkArchivo),
						data : {
							id : idarchivo
						},
						success : function(data) {
							if (data == '1') {
								var t = $(this).parents('tr').get(0);
								$(t).remove();
								$('#pg').html(i + 1);
							} else {
								return false;
							}
						},
						dataType : 'html'
					}).responseText;
				});
		clearInterval(tid);
	}, 1000);
};

var nuevo=0;
var modificar=0;
var continuar=true;
var msg="";



$(function() {
		$("#ayuda").dialog({
		 	autoOpen: false,
			height: 500, 
			width: 700, 
			draggable:true,
			modal:false,
			open: function(evt, ui){
					$('#ayuda').html('');
					$.get('../../help/aportes/manualayudaplanillaunica.html',function(data){
							$('#ayuda').html(data);
					});
			 }
		});
	});

$(function() {
	$("#dialog-form2").dialog("destroy");
	$("#dialog-form2").dialog({
		autoOpen: false,
		height: 400,
		width: 500,
		modal: true,
		buttons: {
			'Enviar': function() {
			var bValid = true;
			var campo=$('#notas').val();
			var campo0=$.trim(campo);
			if (campo0==""){
				$(this).dialog('close');
					return false;
				}
			var campo1=$('#usuario').val();
			var campo2="nominaPU.php";
			$.post('../../phpComunes/colaboracion.php',{v0:campo0,v1:campo1,v2:campo2}, 								
			function(datos){
				if(datos=='1'){
					alert("Su comentario fue enviado correctamente!. Gracias por participar en el mejoramiento de SIGAS");
				}else{
					alert(datos);
				}
		});
		$(this).dialog('close');
		},
		Cancelar: function() {
			$(this).dialog('close');
			$("#dialog-form2").dialog("destroy");
		}
	}		
	});
	/*
	$('#enviar-notas')
		.button()
		.click(function() {
		$('#dialogo-dir2').dialog('open');
	});
	*/
});