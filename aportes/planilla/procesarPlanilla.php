<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>::Cargar Planilla &Uacute;nica::</title>
</head>
<body>

<?php
set_time_limit ( 0 );
ini_set ( "display_errors", '1' );
date_default_timezone_set ( "America/Bogota" );

$root = $_SERVER ['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'wssdk' . DIRECTORY_SEPARATOR . 'ClientWSInfWeb.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'rsc' . DIRECTORY_SEPARATOR . 'pdo' . DIRECTORY_SEPARATOR . 'IFXDbManejador.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'rsc' . DIRECTORY_SEPARATOR . 'pdo' . DIRECTORY_SEPARATOR . 'IFXerror.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'funcionesComunes' . DIRECTORY_SEPARATOR . 'funcionesComunes.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'pdo' . DIRECTORY_SEPARATOR . 'afiliacion.class.php';

$objAfiliacion = new Afiliacion();

$usuario = $_SESSION ["USUARIO"];
$db = IFXDbManejador::conectarDB ();
if ($db->conexionID == null) {
	$cadena = $db->error;
	echo msg_error ( $cadena );
	exit ();
}

$numPlaGrabadas = 0;
$nombreArchivo = $_REQUEST ['v0'];
$fechaHoy = date ( "Y/m/d h:i:s a.m." );

$dirPlanos = $ruta_cargados . 'planillas' . DIRECTORY_SEPARATOR . 'cargados' . DIRECTORY_SEPARATOR;
$ruta = $ruta_cargados . 'planillas' . DIRECTORY_SEPARATOR . 'cargados' . DIRECTORY_SEPARATOR . $nombreArchivo;

$nombre = substr ( $nombreArchivo, 0, - 12 );
$tipo = substr ( $nombre, - 1 );
$fechaArchivo = substr ( $nombreArchivo, 0, 10 );
$anno = substr ( $nombreArchivo, 0, 4 );
$mes = substr ( $nombreArchivo, 5, 2 );
$dia = substr ( $nombreArchivo, 8, 2 );

$dirPrint = $ruta_cargados . 'planillas' .  DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR;
$dirLog = $ruta_cargados . 'planillas' . DIRECTORY_SEPARATOR . 'procesados' . DIRECTORY_SEPARATOR . $anno . DIRECTORY_SEPARATOR . $mes . DIRECTORY_SEPARATOR;
$dir = $ruta_cargados . 'planillas' . DIRECTORY_SEPARATOR . 'procesados' . DIRECTORY_SEPARATOR . $anno . DIRECTORY_SEPARATOR . $mes . DIRECTORY_SEPARATOR . $dia . DIRECTORY_SEPARATOR;

$ok = verificaDirectorio ( $dir );
if ($ok == 0) {
	echo 2;
	exit ();
}

$log_empresas = $dirLog . $fechaArchivo . '_empresasCreadas.txt';
$log_tipodocumentoErrado = $dirLog . $fechaArchivo . '_tipodocumentoErrado.txt';
$log_personas = $dirLog . $fechaArchivo . '_personasCreadas.txt';
$log_personasDobles = $dirLog . $fechaArchivo . '_personasDobles.txt';
$log_afiliaciones = $dirLog . $fechaArchivo . '_afiliacionesCreadas.txt';
$log_errores = $dirLog . $fechaArchivo . '_log_errores.txt';
$log_retiros = $dirLog . $fechaArchivo . '_retiros.txt';
$log_print = $dirPrint . 'tmp.txt';

$fp_tipodocumentoErrado = fopen ( $log_tipodocumentoErrado, "a" );
$fp_personas = fopen ( $log_personas, "a" );
$fp_personasDobles = fopen ( $log_personasDobles, "a" );
$fp_errores = fopen ( $log_errores, "a" );
$fp_print = fopen ( $log_print, "w" );
fwrite ( $fp_print, "[\r\n" );

$indiceAportes = 0;
$archivo = file ( $ruta );
if ($tipo == 'A') {
	$fp_empresas = fopen ( $log_empresas, "a" );
	
	$ln = $archivo [0];
	$ln = utf8_decode ( $ln );
	$razon = trim ( substr ( $ln, 0, 200 ) );
	$tipoiden = tipoDocumento ( trim ( substr ( $ln, 200, 2 ) ) );
	if ($tipoiden == 0) {
		$cadena = "$fechaHoy Parece que el archivo esta vacio:  $nombreArchivo";
		planoAppend ( $cadena, $fp_errores );
	}
	$nit = trim ( substr ( $ln, 202, 16 ) );
	$digito = trim ( substr ( $ln, 218, 1 ) );
	$claseaportante = claseAportante ( trim ( substr ( $ln, 269, 1 ) ) );
	$tipoentidad = trim ( substr ( $ln, 270, 1 ) );
	$direccion = trim ( substr ( $ln, 273, 40 ) );
	$codCiud = trim ( substr ( $ln, 314, 3 ) );
	$codDept = trim ( substr ( $ln, 316, 2 ) );
	$actDane = trim ( substr ( $ln, 318, 4 ) );
	$telefono = trim ( substr ( $ln, 322, 10 ) );
	$fax = trim ( substr ( $ln, 332, 10 ) );
	$email = trim ( substr ( $ln, 342, 60 ) );
	$numero = trim ( substr ( $ln, 402, 16 ) );
	$tipoDocPer = tipoDocumento ( trim ( substr ( $ln, 419, 2 ) ) );
	$papellido = trim ( substr ( $ln, 421, 20 ) );
	$sapellido = trim ( substr ( $ln, 441, 30 ) );
	$pnombre = trim ( substr ( $ln, 471, 20 ) );
	$snombre = trim ( substr ( $ln, 491, 30 ) );
	$tipoaportante = trim ( substr ( $ln, 551, 1 ) );
	$fechaMat = trim ( substr ( $ln, 562, 10 ) );
	
	$idPersona = 0;
	
	if($tipoDocPer==0){
		$cadena = "$fechaHoy -- TD Representante Errado -- " . trim ( substr ( $ln, 419, 2 ) ) . " -- $numero -- ($nombreArchivo)";
		planoAppend ( $cadena, $fp_tipodocumentoErrado );
		$cadena = "{\"nombreArchivo\": \"$nombreArchivo\", \"error\": \"TD Representante Errado\", \"tipoDocumento\": \"" . trim ( substr ( $ln, 419, 2 ) ) . "\", \"identificacion\": \"$numero\",\"Tipo\":\"ErrorUno\"},";
		planoAppend ( $cadena, $fp_print );
	} else {
		$idPersona = existePersona ( $tipoDocPer, $numero, $db );
		if ($idPersona == 0) {
			$lin2 = "$tipoDocPer,$numero,$papellido,$sapellido,$pnombre,$snombre";
			$idPersona = crearPersona ( $lin2, $fp_errores, $db, $fp_personas, $fp_personasDobles, $nombreArchivo, $fp_print, trim ( substr ( $ln, 419, 2 ) ) );
		}
	}
	
	$idEmpresa = existeSigas ( $nit, $db );
	if ($idEmpresa == 0) {
		$idEmpresa = crearEmpresaSigas ( $razon, $tipoiden, $nit, $digito, $tipoaportante, $direccion, $codDept, $codCiud, $codCiud . '000', $telefono, $email, $claseaportante, $idPersona, $fp_empresas );
		if ($idEmpresa > 1) {
			$cadena = "$fechaHoy Creada la Empresa en APORTES bajo el ID no:  $idEmpresa ";
			planoAppend ( $cadena, $fp_empresas );
		} else {
			echo 2;
		}
	} else {
		//actualizarEmpresa ( $db, $idEmpresa, $idPersona, $tipoaportante, $telefono, $email, $indiceAportes, $claseaportante, $fp_errores );
		$sql = "select nit from aportes048 where idempresa=$idEmpresa";
		$rs = $db->querySimple ( $sql );
		$row = $rs->fetch ();
		$nit = trim ( $row ['nit'] );
	}
	
	if (! existeInforma ( ( int ) $nit )) {
		if (crearEmpresaInforma ( $nit, $razon, $_SESSION ["USUARIO"] )) {
			$cadena = "$fechaHoy Creada la Empresa en INFORMA: $nit";
			planoAppend ( $cadena, $fp_empresas );
		} else {
			$cadena = "$fechaHoy NO se pudo crear la Empresa en INFORMA: $nit " . $razon;
			planoAppend ( $cadena, $fp_empresas );
			echo 3;
		}
	}
	echo 0;
	
	$sql = "INSERT INTO aportes030 (   nombrearchivo,   ruta, tipoarchivo, tamano, procesado,             fechacargue, tipoproceso,           usuarioproceso) 
							VALUES ('$nombreArchivo', '$dir', 	 '$tipo',   NULL,       'S', cast(getdate() as date),        'PU', '{$_SESSION["USUARIO"]}')";
	$rs = $db->queryInsert ( $sql, 'aportes030' );
	copy ( $ruta, $dir . $nombreArchivo );
	unlink ( $ruta );
	planoAppend ( "]", $fp_print );
	
	fileClose ( $fp_empresas, $log_empresas );
	fileClose ( $fp_tipodocumentoErrado, $log_tipodocumentoErrado );
	fileClose ( $fp_personas, $log_personas );
	fileClose ( $fp_personasDobles, $log_personasDobles );
	fileClose ( $fp_errores, $log_errores );
	fileClose ( $fp_print, $log_print );
} elseif ($tipo == 'I') {
	
	$totalIBC = 0;
	$totalAportes = 0;
	$diasMora = 0;
	$valorMora = 0;
	$totalGeneral = 0;
	
	$fp_afiliaciones = fopen ( $log_afiliaciones, "a" );	
	$fp_retiros = fopen ( $log_retiros, "a" );
	
	$linea = $archivo [0];
	$linea = utf8_decode ( $linea );
	$codigo = trim ( substr ( $linea, 0, 5 ) );
	if ($codigo == "00000") {
		$nitCaja = trim ( substr ( $linea, 8, 16 ) );
		if ($nitCaja != "891180008") {
			echo "1";
			unlink ( $ruta );
			planoAppend ( "]", $fp_print );
						
			fileClose ( $fp_afiliaciones, $log_afiliaciones );
			fileClose ( $fp_retiros, $log_retiros );
			fileClose ( $fp_tipodocumentoErrado, $log_tipodocumentoErrado );
			fileClose ( $fp_personas, $log_personas );
			fileClose ( $fp_personasDobles, $log_personasDobles );
			fileClose ( $fp_errores, $log_errores );
			fileClose ( $fp_print, $log_print );
			
			exit ();
		} else {
			$nit = trim ( substr ( $linea, 227, 16 ) );			
			$razon = trim ( substr ( $linea, 25, 200 ) );
			$tipoDocAportante = tipoDocumento ( trim ( substr ( $linea, 225, 2 ) ) );
			$digito = trim ( substr ( $linea, 24, 1 ) );
			$tipoApo = obtenerIdTipoAportante ( trim ( substr ( $linea, 244, 1 ) ) );
			$direccion = trim ( substr ( $linea, 245, 40 ) );
			$codCiudad = trim ( substr ( $linea, 288, 2 ) ) . trim ( substr ( $linea, 285, 3 ) ) . '000';
			$codDepartamento = trim ( substr ( $linea, 288, 2 ) );
			$codMunicipio = trim ( substr ( $linea, 285, 3 ) );
			$telefono = trim ( substr ( $linea, 290, 10 ) );
			$email = trim ( substr ( $linea, 310, 60 ) );
			$periodo = trim ( substr ( $linea, 370, 4 ) ) . trim ( substr ( $linea, 375, 2 ) );
			$tipoPlanilla = trim ( substr ( $linea, 377, 1 ) );
			$fechaPago = trim ( substr ( $linea, 388, 10 ) );
			$planilla = trim ( substr ( $linea, 408, 10 ) );
			$totalEmple = intval ( substr ( $linea, 469, 5 ) );
			$totalAportantes = intval ( substr ( $linea, 474, 5 ) );
			$operador = trim ( substr ( $linea, 479, 2 ) );
			$modalidad = trim ( substr ( $linea, 481, 1 ) );
			$diasMora = intval ( substr ( $linea, 482, 4 ) );
			$claseAportante = 0;
			
			$idEmpresa = existeSigas ( $nit, $db );
			if ($idEmpresa == 0) {
				$cadena = "$fechaHoy - No existe en SIGAS la empresa $nit)";
				planoAppend ( $cadena, $fp_errores );
				$cadena="{\"error\":\"No existe en SIGAS la empresa\",\"Planilla\":\"$planilla\",\"Nit\":\"$nit\",\"Identificacion\":\"$numero\",\"Periodo\":\"$periodo\",\"FechaPago\":\"$fechaPago\",\"Tipo\":\"ErrorDos\"},";
				planoAppend ( $cadena, $fp_print );
				unlink ( $ruta );
				planoAppend ( "]", $fp_print );
				
				fileClose ( $fp_afiliaciones, $log_afiliaciones );
				fileClose ( $fp_retiros, $log_retiros );
				fileClose ( $fp_tipodocumentoErrado, $log_tipodocumentoErrado );
				fileClose ( $fp_personas, $log_personas );
				fileClose ( $fp_personasDobles, $log_personasDobles );
				fileClose ( $fp_errores, $log_errores );
				fileClose ( $fp_print, $log_print );
				
				exit ();
			}
			
			$sql = "select nit from aportes048 where idempresa=$idEmpresa";
			$rs = $db->querySimple ( $sql );
			$row = $rs->fetch ();
			$nit = trim ( $row ['nit'] );
			
			if (! existeInforma ( ( int ) $nit )) {
				$cadena = "$fechaHoy - No existe en INFORMAWEB la empresa $nit)";
				planoAppend ( $cadena, $fp_errores );
			}
			
			$conAporta = 0;
			for($c = 0; $c < count ( $archivo ); $c ++) {
				$acumulado = 0;
				if ($c > 0) {
					$linea = $archivo [$c];
					
					$tipoReg = intval ( substr ( $linea, 5, 1 ) );
					if ($tipoReg == 2) {
						$tipoDocu = tipoDocumento ( trim ( substr ( $linea, 6, 2 ) ) );
						$numero = trim ( substr ( $linea, 8, 16 ) );
						$numero = str_replace ( ".", "", $numero );
						$numero = str_replace ( " ", "", $numero );
						$tipoCotiz = obtenerIdTipoCotizante ( trim ( substr ( $linea, 24, 2 ) ) );
						$subtipoCotiz = obtenerIdSubtipoCotizante ( trim ( substr ( $linea, 26, 2 ) ) );
						$pApellido = utf8_decode ( trim ( substr ( $linea, 35, 20 ) ) );
						$sApellido = utf8_decode ( trim ( substr ( $linea, 55, 30 ) ) );
						$pNombre = utf8_decode ( trim ( substr ( $linea, 85, 20 ) ) );
						$sNombre = utf8_decode ( trim ( substr ( $linea, 105, 30 ) ) );
						$ingreso = novedad ( trim ( substr ( $linea, 135, 1 ) ) );
						$retiro = novedad ( trim ( substr ( $linea, 136, 1 ) ) );
						$vsp = novedad ( trim ( substr ( $linea, 137, 1 ) ) );
						$vst = novedad ( trim ( substr ( $linea, 138, 1 ) ) );
						$sln = novedad ( trim ( substr ( $linea, 139, 1 ) ) );
						$ige = novedad ( trim ( substr ( $linea, 140, 1 ) ) );
						$lma = novedad ( trim ( substr ( $linea, 141, 1 ) ) );
						$vac = novedad ( trim ( substr ( $linea, 142, 1 ) ) );
						$irp = intval ( trim ( substr ( $linea, 143, 2 ) ) );
						$diasCot = intval ( trim( substr ( $linea, 145, 2 ) ) );
						$horasCot = $diasCot * 8;
						$salBasico = intval ( trim ( substr ( $linea, 147, 9 ) ) );
						$ibc2 = intval ( substr ( $linea, 156, 9 ) );
						$ibc = intval ( trim ( substr ( $linea, 147, 9 ) ) );
						$tarifa =  trim ( substr ( $linea, 165, 7 ) ) * 100;
						$aporte = intval ( trim ( substr ( $linea, 172, 9 ) ) );
						$salIntegral = novedad ( trim ( substr ( $linea, 182, 1 ) ) );
						$correccion = correccion ( trim ( substr ( $linea, 181, 1 ) ) );
						
						if ( $correccion != 'A' ) { $acumulado += $salBasico; }
						if ( $correccion != 'C' ) { $idPlanillaCorreccion = 0; }
						
						if($tipoDocu==0){
							$cadena = "$fechaHoy -- TD Afiliado Errado -- " . trim ( substr ( $linea, 6, 2 ) ) . " -- $numero -- ($nombreArchivo)";
							planoAppend ( $cadena, $fp_tipodocumentoErrado );
							$cadena = "{ \"nombreArchivo\": \"$nombreArchivo\", \"error\": \"TD Afiliado Errado\", \"tipoDocumento\": \"" . trim ( substr ( $linea, 6, 2 ) ) . "\", \"identificacion\": \"$numero\",\"Tipo\":\"ErrorUno\"},";
							planoAppend ( $cadena, $fp_print );							
							continue;
						} else {
							$idPersona = existePersona ( $tipoDocu, $numero, $db );
							if ($idPersona == 0) {							
								$lin2 = "$tipoDocu,$numero,$pApellido,$sApellido,$pNombre,$sNombre";
								$idPersona = crearPersona ( $lin2, $fp_errores, $db, $fp_personas, $fp_personasDobles, $nombreArchivo, $fp_print, trim ( substr ( $linea, 6, 2 ) ) );
								if ($idPersona == 0)
									continue;
							}
						}
						
						if ( $correccion == 'C' ) {
							if ( $idPlanillaCorreccion == 0 ) {
								$cadena = "$fechaHoy - no se encontro planilla para CORRECCION: $planilla, $idEmpresa, $idPersona, ($diasCot, $ingreso, $retiro, $vst, $vsp, $sln, $ige, $lma, $vac, $irp, $periodo)";
								planoAppend ( $cadena, $fp_errores );
								$conAporta ++;
								continue;
							}
						} else {
							$idPlanilla = existePlanillaCompleta($db, $planilla, $idEmpresa, $idPersona, $diasCot, $ingreso, $retiro, $vst, $vsp, $sln, $ige, $lma, $vac, $irp, $periodo, $correccion );
							if ( $idPlanilla > 0 ) {
								if( $correccion == 'A' ){
									$idPlanillaCorreccion=$idPlanilla;
									continue;
								}
									
								$cadena = "$fechaHoy - Se intento INSERTAR planilla duplicada: $planilla, $idEmpresa, $idPersona, ($diasCot, $ingreso, $retiro, $vst, $vsp, $sln, $ige, $lma, $vac, $irp, $periodo)";
								planoAppend ( $cadena, $fp_errores );
								$cadena="{\"error\":\"Se intento INSERTAR planilla duplicada\",\"Planilla\":\"$planilla\",\"Nit\":\"$nit\",\"Identificacion\":\"$numero\",\"Periodo\":\"$periodo\",\"FechaPago\":\"$fechaPago\",\"Tipo\":\"ErrorDos\"},";
								planoAppend ( $cadena, $fp_print );
								$conAporta ++;
								continue;
							}
						}
						
						
						$sql = "select idtipoafiliacion from aportes048 where idempresa=$idEmpresa";
						$rs1 = $db->querySimple ( $sql );
						$Ind = $rs1->fetch ();
						$tipoafiliacion = $Ind ['idtipoafiliacion'];
						
						if ($tipoafiliacion == 3316){
							switch ($tarifa) {
								case 0 :
									$indiceAportes = 4268;
									break;
								case 0.6 :
									$indiceAportes = 106;
									break;
								case 1 :
									$indiceAportes = 4267;
									break;
								case 2 :
									$indiceAportes = 4266;
									break;
								case 3 :
									$indiceAportes = 4265;
									break;
								case 4 :
									$indiceAportes = 108;
									break;
							}
						} else {
							switch ($tarifa) {
								case 0 :
									$indiceAportes = 4006;
									break;
								case 0.6 :
									$indiceAportes = 106;
									break;
								case 2 :
									$indiceAportes = 107;
									break;
								case 4 :
									$indiceAportes = 108;
									break;
							}
						}
										
						$sql = "UPDATE aportes048 set indicador='$indiceAportes' WHERE idempresa = $idEmpresa";
						$rs = $db->queryActualiza ( $sql );
						if ($rs == 0) {
							$cadena = "No se pudo actualizar el indice de aportes aportes048 $sql";
							planoAppend ( $cadena, $fp_errores );
						}
						
						if ($correccion == 'C') {
							$sql = "UPDATE aportes010 SET idarchivo=:idarchivo, planilla='$planilla', tipodocumentoaportante=$tipoDocAportante, nit='$nit', tipodocumento=$tipoDocu, identificacion='$numero', tipocotizantepu=$tipoCotiz, subtipocotizantepu=:subtipoCotiz, coddepartamentotrabaja=:coddepartamentotrabaja, codmunicipiotrabaja=:codmunicipiotrabaja, papellido='$pApellido', sapellido='$sApellido', pnombre='$pNombre', snombre='$sNombre', tarifaaporte=$tarifa, valoraporte=$aporte, salariobasico=$salBasico, ingresobase=$ibc2, salariointegral=:salIntegral, diascotizados=$diasCot, ingreso=:ingreso, retiro=:retiro, var_tra_salario=:vst, var_per_salario=:vsp, sus_tem_contrato=:sln, inc_tem_emfermedad=:ige, lic_maternidad=:lma, vacaciones=:vac, inc_tem_acc_trabajo=:irp, periodo='$periodo', correccion=:correccion, idempresa=$idEmpresa, idtrabajador=$idPersona, horascotizadas=$horasCot, procesado='N', usuario='$usuario', fechapago='$fechaPago', fechasistema=cast(getdate() as date) WHERE idplanilla=$idPlanillaCorreccion";
						} else {
							$sql = "INSERT INTO aportes010 (idarchivo, planilla, tipodocumentoaportante, nit, tipodocumento, identificacion, tipocotizantepu, subtipocotizantepu, coddepartamentotrabaja, codmunicipiotrabaja, papellido, sapellido, pnombre, snombre, tarifaaporte, valoraporte, salariobasico, ingresobase, salariointegral, diascotizados, ingreso, retiro, var_tra_salario, var_per_salario, sus_tem_contrato, inc_tem_emfermedad, lic_maternidad, vacaciones, inc_tem_acc_trabajo, periodo, correccion, idempresa, idtrabajador, horascotizadas, procesado, usuario, fechapago, fechasistema) VALUES (:idarchivo, '$planilla', $tipoDocAportante, '$nit', $tipoDocu, '$numero', $tipoCotiz, :subtipoCotiz, :coddepartamentotrabaja, :codmunicipiotrabaja, '$pApellido', '$sApellido', '$pNombre', '$sNombre', $tarifa, $aporte, $salBasico, $ibc2, :salIntegral, $diasCot, :ingreso, :retiro, :vst, :vsp, :sln, :ige, :lma, :vac, :irp, '$periodo', :correccion, $idEmpresa, $idPersona, $horasCot, 'N', '$usuario', '$fechaPago', cast(getdate() as date))";
						}
						
						$statement = $db->conexionID->prepare ( $sql );
						$guardada = false;
						$idarchivo = null;
						$coddepartamentotrabaja = null;
						$codmunicipiotrabaja = null;						
						$statement->bindParam ( ":idarchivo", $idarchivo, PDO::PARAM_INT );
						$statement->bindParam ( ":subtipoCotiz", $subtipoCotiz, PDO::PARAM_INT );
						$statement->bindParam ( ":coddepartamentotrabaja", $coddepartamentotrabaja, PDO::PARAM_STR );
						$statement->bindParam ( ":codmunicipiotrabaja", $codmunicipiotrabaja, PDO::PARAM_STR );
						$statement->bindParam ( ":salIntegral", $salIntegral, PDO::PARAM_STR );
						$statement->bindParam ( ":ingreso", $ingreso, PDO::PARAM_STR );
						$statement->bindParam ( ":retiro", $retiro, PDO::PARAM_STR );
						$statement->bindParam ( ":vst", $vst, PDO::PARAM_STR );
						$statement->bindParam ( ":vsp", $vsp, PDO::PARAM_STR );
						$statement->bindParam ( ":sln", $sln, PDO::PARAM_STR );
						$statement->bindParam ( ":ige", $ige, PDO::PARAM_STR );
						$statement->bindParam ( ":lma", $lma, PDO::PARAM_STR );
						$statement->bindParam ( ":vac", $vac, PDO::PARAM_STR );
						$statement->bindParam ( ":irp", $irp, PDO::PARAM_STR );
						$statement->bindParam ( ":correccion", $correccion, PDO::PARAM_STR );
						$guardada = $statement->execute ();
						
						$idPlanilla = 0;
						if ($guardada == true) {
							if ( $correccion == 'C' ) {
								$idPlanilla=1;
							} else {
								$idPlanilla = intval ( $db->conexionID->lastInsertId () );
							}
						} else {
							$cadena = "$fechaHoy - No se pudo insertar la planilla: $sql";
							planoAppend ( $cadena, $fp_errores );
							$cadena="{\"error\":\"No se pudo insertar la planilla\",\"Planilla\":\"$planilla\",\"Nit\":\"$numero\",\"Identificacion\":\"$numero\",\"Periodo\":\"$periodo\",\"FechaPago\":\"$fechaPago\",\"Tipo\":\"ErrorDos\"},";
							planoAppend ( $cadena, $fp_print );
							$conAporta ++;
							continue;
						}
						
						if ($idPlanilla == 0) {
							$error = $statement->errorInfo ();
							$cadena = "$fechaHoy - Error codigo {$error[0]} con el mensaje >>> {$error[2]}";
							planoAppend ( $cadena, $fp_errores );
							$conAporta ++;
							continue;
						} else {
							$numPlaGrabadas ++;
							$conAporta ++;
							
							if ( $correccion == 'A' ) {
								$idPlanillaCorreccion = $idPlanilla;
								continue;
							}
						}
						
						
						$existeAfiliacion = buscarAfiliacion ( $idPersona, $idEmpresa );
						if ( ( !$existeAfiliacion ) && ( $ingreso == 'X' ) ) {
							$idAfiliacion = crearAfiliacion ( $db, $idEmpresa, $idPersona, $fechaPago, $ibc, $periodo );
							if ( $idAfiliacion == 0 ) {
								$cadena = "$fechaHoy - No se pudo crear la afiliacion idempresa: $idEmpresa, idpersona: $idPersona, $fechaPago, $ibc";
								planoAppend ( $cadena, $fp_errores );
							} else {
								$existeAfiliacion = true;
								
								$cadena = "$fechaHoy - Se creo la afiliacion de: --> idempresa: $idEmpresa, idpersona: $idPersona, $fechaPago, $ibc";
								planoAppend ( $cadena, $fp_afiliaciones );	

								//Ejecutar el procedimiento para actualizar la categoria
								ejecutarSpCategorias($idPersona,$idEmpresa,$fp_afiliaciones,$fp_errores);
								
							}
						} elseif ( $existeAfiliacion ) {
							$conAporta ++;
							$actualizada = actualizaAfiliacion ( $db, $idEmpresa, $idPersona, $ibc, $ibc2 );
							if ($actualizada == 0) {
								$cadena = "$fechaHoy - No se pudo actualizar salario y categoria del afiliado: IdEmpresa: $idEmpresa, idpersona: $idPersona, $ibc";
								planoAppend ( $cadena, $fp_errores );
							}
							
							//Ejecutar el procedimiento para actualizar la categoria
							ejecutarSpCategorias($idPersona,$idEmpresa,$fp_afiliaciones,$fp_errores);
						}
												
						if ( $existeAfiliacion == true && $retiro == 'X' ) {
							inactivar ( $db, $idPersona, $idEmpresa, $diasCot, $periodo, $usuario, $fp_retiros );
							
							//Ejecutar el procedimiento para actualizar la categoria
							ejecutarSpCategorias($idPersona,$idEmpresa,$fp_afiliaciones,$fp_errores);
						}
					} elseif ($tipoReg == 3) {
						
						
						$tipoReg2Digito = intval ( substr ( $linea, 0, 5 ) );
						if ($tipoReg2Digito == 31) {
							$totalIBC = intval ( substr ( $linea, 6, 10 ) );
							$totalAportes = intval ( substr ( $linea, 16, 10 ) );
						} elseif ($tipoReg2Digito == 36) {
							$diasMora = intval ( substr ( $linea, 6, 4 ) );
							$valorMora = intval ( substr ( $linea, 10, 10 ) );
						} elseif ($tipoReg2Digito == 39) {
							$totalGeneral = intval ( substr ( $linea, 6, 10 ) );
						}
					}
				}
			}			
		}
	} else {
		unlink ( $ruta );
		planoAppend ( "]", $fp_print );
		
		fileClose ( $fp_afiliaciones, $log_afiliaciones );
		fileClose ( $fp_retiros, $log_retiros );
		fileClose ( $fp_tipodocumentoErrado, $log_tipodocumentoErrado );
		fileClose ( $fp_personas, $log_personas );
		fileClose ( $fp_personasDobles, $log_personasDobles );
		fileClose ( $fp_errores, $log_errores );
		fileClose ( $fp_print, $log_print );
		
		exit();
	}
	
	if ( $totalIBC == 0 ) { $indice = 0; }
	else { $indice = intval ( ($totalAportes / $totalIBC) * 100 ); }
	
	if ( $indice <= 4 ) {
		$caja = $totalAportes;
		$sena = 0;
		$icbf = 0;
	} else if ( $indice == 9 ) {
		$caja = $totalAportes * 0.04;
		$sena = $totalAportes * 0.03;
		$icbf = $totalAportes * 0.02;
	} else if ( $indice == 6 ) {
		$caja = $totalAportes * 0.04;
		$sena = $totalAportes * 0.02;
		$icbf = 0;
	} else if ( $indice == 7 ) {
		$caja = $totalAportes * 0.04;
		$sena = 0;
		$icbf = $totalAportes * 0.03;
	} 
		
	$sql = "INSERT INTO aportes031 (razonsocial, idtipodocumento, nit, idtipoaportante, direccion, idciudad, telefono, correo, periodo, tipoplanilla, fechapago, planilla, empleados, aportantes, codigooperador, modalidadplanilla, diasmora, totalsalbasico, totalibc, mora, totalaportes, indice, caja, sena, icbf, idempresa, fechasistema, usuario)
	VALUES ( '$razon', $tipoDocAportante, '$nit', $tipoApo, '$direccion', '$codCiudad', '$telefono', '$email', '$periodo', '$tipoPlanilla', '$fechaPago', '$planilla', $totalEmple, $totalAportantes, '$operador', '$modalidad', $diasMora, $acumulado, $totalIBC, $valorMora, $totalAportes, $indice, $caja, $sena, $icbf, $idEmpresa, cast(getdate() as date), '{$_SESSION["USUARIO"]}')";
	$rs = $db->queryInsert ( $sql, 'aportes031' );
	if ($rs == 0) {
		$cadena = "$fechaHoy No se pudo INSERTAR la cabecera de la planilla: $sql";
		planoAppend ( $cadena, $fp_errores );
		$cadena="{\"error\":\"No se pudo INSERTAR la cabecera de la planilla\",\"Planilla\":\"$planilla\",\"Nit\":\"$numero\",\"Identificacion\":\"$numero\",\"Periodo\":\"$periodo\",\"FechaPago\":\"$fechaPago\",\"Tipo\":\"ErrorDos\"},";
		planoAppend ( $cadena, $fp_print );
		echo 4;
	}
	
	if ($conAporta >= $totalAportantes) {		
		$sql = "INSERT INTO aportes030 (nombrearchivo, ruta, tipoarchivo, tamano, procesado, fechacargue, tipoproceso, usuarioproceso) VALUES ('$nombreArchivo' ,'$dir', '$tipo', NULL, 'S', cast(getdate() as date), 'PU', '{$_SESSION["USUARIO"]}')";
		$rs = $db->queryInsert ( $sql, 'aportes030' );

		echo 0;
	} else {
		echo 1;
	}
		
	copy ( $ruta, $dir . $nombreArchivo );
	unlink ( $ruta );
	planoAppend ( "]", $fp_print );

	fileClose ( $fp_afiliaciones, $log_afiliaciones );
	fileClose ( $fp_retiros, $log_retiros );
	fileClose ( $fp_tipodocumentoErrado, $log_tipodocumentoErrado );
	fileClose ( $fp_personas, $log_personas );
	fileClose ( $fp_personasDobles, $log_personasDobles );
	fileClose ( $fp_errores, $log_errores );
	fileClose ( $fp_print, $log_print );
}

/**
 * ****************************************************************************************************************************************
 */

function existeSigas($nit, $db) {
	$sql = "select idempresa from aportes048 where nit='$nit'";
	$rs = $db->querySimple ( $sql );
	$row = $rs->fetch ();
	if (isset ( $row ['idempresa'] )) {
		return $row ['idempresa'];
	} else {
		$nit = substr ( $nit, 0, - 1 );
		$sql = "select idempresa from aportes048 where nit='$nit'";
		$rs = $db->querySimple ( $sql );
		$row = $rs->fetch ();
		return (isset ( $row ['idempresa'] )) ? $row ['idempresa'] : 0;
	}

}

function existeInforma($nit) {
	$ws = new ClientWSInfWeb ( USUARIO_WEB_SIGAS, CONTRASENA_WEB_SIGAS );
	return $ws->terceroExiste ( $nit );
}

function crearEmpresaSigas($razon, $tipoDocAportante, $nit, $digito, $tipoApo, $direccion, $codDepartamento, $codMunicipio, $codCiudad, $telefono, $email, $claseAportante = 0, $idPersona = 0, $fp_empresas) {
	$fechaHoy = date ( "Y/m/d h:i:s a" );
	$db = IFXDbManejador::conectarDB ();
	if ($db->conexionID == null) {
		$cadena = $db->error;
		echo msg_error ( $cadena );
		exit ();
	}
		
	$sql = "INSERT INTO aportes048(idtipodocumento, nit, digito, principal, razonsocial, sigla, direccion, iddepartamento, idciudad, idzona, telefono, fax, email, indicador, tipoaportante, estado, usuario, fechasistema, legalizada, renovacion, claseaportante,idrepresentante,porplanilla) VALUES('$tipoDocAportante', '$nit', '$digito', 'S', '$razon', '$razon', '$direccion', '".$codDepartamento."', '".$codMunicipio."', '$codCiudad', '$telefono', '$telefono', '$email', 0, '$tipoApo', 'P', '{$_SESSION["USUARIO"]}', cast(getdate() as date), 'N', 'N', '$claseAportante',$idPersona,'S')";
	// $sql=utf8_encode($sql);
	$rs = $db->queryInsert ( $sql, 'aportes048' );
	if ($rs == 0) {
		$cadena = "$fechaHoy NO se pudo INSERTAR la Empresa en APORTES: $sql ";
		planoAppend ( $cadena, $fp_empresas );
		$cadena="{\"error\":\"NO se pudo INSERTAR la Empresa en APORTES\",\"Planilla\":\"$planilla\",\"Nit\":\"$numero\",\"Identificacion\":\"$numero\",\"Periodo\":\"$periodo\",\"FechaPago\":\"$fechaPago\",\"Tipo\":\"ErrorDos\"},";
		planoAppend ( $cadena, $fp_print );
	}
	return $rs;
}

function crearEmpresaInforma($nit, $razonSocial, $usuario) {
	$cliente = new ClientWSInfWeb ( USUARIO_WEB_SIGAS, CONTRASENA_WEB_SIGAS );
	$tercero = new Tercero ( $nit, $razonSocial, 'N', $usuario );
	if ($cliente->terceroGrabar ( $tercero ))
		return true;
	else
		return false;
}

function existePersona($tipoDoc, $numDoc, $db) {
	$sql = "SELECT idpersona FROM aportes015 WHERE idtipodocumento=$tipoDoc AND identificacion='$numDoc'";
	$rs = $db->querySimple ( $sql );
	$idPersona = 0;
	while ( $row = $rs->fetch () )
		$idPersona = $row ["idpersona"];
	
	return $idPersona;
}

function crearPersona($linea, $file, $db, $file2, $file3, $nombreArchivo, $file4, $td ) {
	$fechaHoy = date ( "Y/m/d h:i:s a" );
	$arreglo = explode ( ",", $linea );
	
	$letra='�';
	$papellido='';
	$sapellido='';
	$pnombre='';
	$snombre='';
	
	$papellido=str_replace("?",utf8_encode($letra),$arreglo[2]);
	$sapellido=str_replace("?",utf8_encode($letra),$arreglo[3]);
	$pnombre=str_replace("?",utf8_encode($letra),$arreglo[4]);
	$snombre=str_replace("?",utf8_encode($letra),$arreglo[5]);
	
	$nomCorto = nombreCorto ( $pnombre, $snombre, $papellido, $sapellido );
	if ($nomCorto == '0') {
		$cadena = "$fechaHoy -No se pudo INSERTAR la persona por tener nombres extra\u00F1os: $linea";
		planoAppend ( $cadena, $file );
		return 0;
	}
	$identificacion = str_replace ( ".", "", $arreglo [1] );	
	
	$cuenta = buscarPersonaSimilar($db, $identificacion, $pnombre, $snombre, $papellido, $sapellido, $file3, $nombreArchivo, $file4, $td);
	
	if($cuenta==0){
		$sql = "INSERT INTO aportes015 (idtipodocumento, identificacion, papellido, sapellido, pnombre, snombre, estado, validado, nombrecorto,usuario,fechasistema) VALUES ({$arreglo[0]}, '$identificacion', '{$papellido}', '{$sapellido}', '{$pnombre}', '{$snombre}', 'N','N','$nomCorto','porplanilla',cast(getdate() as date))";
		$rs = $db->queryInsert ( $sql, 'aportes015' );
		if ($rs == 0) {
			$cadena = "$fechaHoy -No se pudo INSERTAR la persona: $sql";
			planoAppend ( $cadena, $file );
		} else {
			$cadena = "$fechaHoy - Creada la persona bajo el ID no: $rs";
			planoAppend ( $cadena, $file2 );
		}
		
		return $rs;
	} else {
		return 0;
	}
}

function buscarPersonaSimilar($db, $identificacion, $pnombre, $snombre, $papellido, $sapellido, $file3, $nombreArchivo, $file4, $td){
	$cuenta = 0;
	$fechaHoy = date ( "Y/m/d h:i:s a" );
	
	$sql = "SELECT count(*) cuenta FROM aportes015 WHERE ( identificacion='$identificacion' AND pnombre='$pnombre' AND snombre='$snombre' AND papellido='$papellido' AND sapellido='$sapellido' )";
	$rs = $db->querySimple ( $sql );

	$row = $rs->fetch ();
	$cuenta = $row ["cuenta"];
		
	if ($cuenta>0){
		$cadena = "$fechaHoy -- Existen personas con Datos Similares -- $identificacion -- $pnombre $snombre $papellido $sapellido -- ($nombreArchivo) ";
		planoAppend ( $cadena, $file3 );
		$cadena = "{\"nombreArchivo\": \"$nombreArchivo\", \"error\": \"Persona Con Datos Similares\", \"tipoDocumento\": \"$td\" , \"identificacion\": \"$identificacion\",\"Tipo\":\"ErrorUno\"},";
		planoAppend ( $cadena, $file4 );
	}

	return $cuenta;
}

function obtenerIdTipoCotizante($codigo) {
	$id = 0;
	switch ($codigo) {
		case 1 :
			$id = 3321;
			break;
		case 2 :
			$id = 3322;
			break;
		case 3 :
			$id = 3323;
			break;
		case 4 :
			$id = 3324;
			break;
		case 12 :
			$id = 3325;
			break;
		case 15 :
			$id = 3326;
			break;
		case 16 :
			$id = 3327;
			break;
		case 18 :
			$id = 3328;
			break;
		case 19 :
			$id = 3329;
			break;
		case 20 :
			$id = 3330;
			break;
		case 21 :
			$id = 3331;
			break;
		case 22 :
			$id = 3332;
			break;
		case 30 :
			$id = 3333;
			break;
		case 31 :
			$id = 3334;
			break;
		case 32 :
			$id = 3335;
			break;
		case 33 :
			$id = 3336;
			break;
		case 34 :
			$id = 3337;
			break;
		case 40 :
			$id = 3338;
			break;
	}
	return $id;
}

function obtenerIdSubtipoCotizante($codigo) {
	$id = 0;
	switch ($codigo) {
		case 1 :
			$id = 3339;
			break;
		case 2 :
			$id = 3340;
			break;
		case 3 :
			$id = 3341;
			break;
		case 4 :
			$id = 3342;
			break;
		case 5 :
			$id = 3343;
			break;
		case 6 :
			$id = 3344;
			break;
		default :
			$id = null;
			break;
	}
	return $id;
}

function novedad($valor) {
	if ($valor == 'X')
		return $valor;
	return null;
}

function correccion($valor) {
	if ($valor == 'A' || $valor == 'C')
		return $valor;
	return null;
}

function existePlanilla($planilla, $idEmpresa, $idPersona, $periodo) {
	$sql = "SELECT * FROM aportes010 WHERE planilla='$planilla' AND idempresa=$idEmpresa AND idtrabajador=$idPersona AND periodo='$periodo'";
	$db = IFXDbManejador::conectarDB ();
	if ($db->conexionID == null) {
		$cadena = $db->error;
		echo msg_error ( $cadena );
		exit ();
	}
	$rs = $db->querySimple ( $sql );
	$row = $rs->fetch ();
	if (is_array ( $row ))
		return $row ['idplanilla'];
	return 0;
}

function existePlanillaCompleta($db, $planilla, $idEmpresa, $idPersona, $diasCot, $ingreso, $retiro, $vst, $vsp, $sln, $ige, $lma, $vac, $irp, $periodo, $correccion ) {
	$sql = "SELECT ISNULL(MAX(idplanilla),0) cuenta FROM aportes010 WHERE ( planilla='$planilla' OR 'A'='$correccion' ) AND idempresa=$idEmpresa AND idtrabajador=$idPersona AND periodo='$periodo'
		AND diascotizados=$diasCot AND isnull(ingreso,'')='$ingreso' AND isnull(retiro,'')='$retiro'  AND isnull(var_tra_salario,'')='$vst' AND isnull(var_per_salario,'')='$vsp' AND isnull(sus_tem_contrato,'')='$sln' AND isnull(inc_tem_emfermedad,'')='$ige' AND isnull(lic_maternidad,'')='$lma' AND isnull(vacaciones,'')='$vac' AND isnull(inc_tem_acc_trabajo,'0')='$irp'";
	//echo $sql."<br>";	
	$rs = $db->querySimple ( $sql );
	$row = $rs->fetch ();
	return  $row ['cuenta'];
}

function obtenerIdTipoAportante($codigo) {
	$id = 0;
	switch ($codigo) {
		case 1 :
			$id = 2656;
			break;
		case 2 :
			$id = 2657;
			break;
		case 3 :
			$id = 2658;
			break;
		case 4 :
			$id = 2659;
			break;
		case 5 :
			$id = 2660;
			break;
		case 6 :
			$id = 2661;
			break;
		case 7 :
			$id = 2662;
			break;
		case 8 :
			$id = 2663;
			break;
	}
	return $id;
}

function buscarAfiliacion($idPersona, $idEmpresa) {
	$sql = "SELECT count(*) cuenta FROM aportes016 WHERE idpersona=$idPersona AND idempresa=$idEmpresa";
	$db = IFXDbManejador::conectarDB ();
	if ($db->conexionID == null) {
		$cadena = $db->error;
		echo msg_error ( $cadena );
		exit ();
	}
	$rs = $db->querySimple ( $sql );
	$row = $rs->fetch ();
	if (is_array ( $row ))
		return ($row ['cuenta'] > 0) ? true : false;
	return false;
}

function crearAfiliacion($db, $idEmpresa, $idPersona, $fechaPago, $salario, $periodo) {
	if (intval ( $salario ) == 0)
		return false;
	$sql = "select top 1 smlv from aportes012 where procesado='S' order by periodo desc";
	$rs = $db->querySimple ( $sql );
	$w = $rs->fetch ();
	$salMin = intval ( $w ['smlv'] );
	$relacion = $salario / $salMin;
	if ($relacion <= 2)
		$categoria = 'A';
	elseif ($relacion > 2 && $relacion <= 4)
		$categoria = 'B';
	else
		$categoria = 'C';
	
	$sql = "INSERT INTO aportes016b (tipoformulario,tipoafiliacion,idempresa,idpersona,fechaingreso,horasdia,horasmes,salario, primaria,estado, fechasistema,usuario,tipopago,categoria,auditado,porplanilla) values (49, 18,$idEmpresa, $idPersona, '".$periodo."01', 8, 240, $salario, 'S', 'P',cast(getdate() as date) , 'PorPlanilla', 'T', '$categoria', 'N','S')";
	$rs = $db->queryInsert ( $sql, 'aportes016b' );
	return $rs;
}

function actualizaAfiliacion($db, $idEmpresa, $idPersona, $salariobasico, $ingresobase) {
	$sql = "select top 1 isnull(smlv,1) smlv from aportes012 where procesado='N' order by periodo ASC";
	$rs = $db->querySimple ( $sql );
	$w = $rs->fetch ();
	$salMin = intval ( $w ['smlv'] );
	
	$sql = "select top 1 isnull(cargo,0) cargo from aportes016 where idpersona=$idPersona AND idempresa=$idEmpresa";
	$rs = $db->querySimple ( $sql );
	$w = $rs->fetch ();
	$idcargo = $w ['cargo'];
	
	if($idcargo==2951){
		$salario=$ingresobase;
	} else {
		$salario=$salariobasico;
	}
	
	$relacion = $salario / $salMin;
	if ($relacion <= 2)
		$categoria = 'A';
	elseif ($relacion > 2 && $relacion <= 4)
		$categoria = 'B';
	else
		$categoria = 'C';
	
	$sql = "UPDATE aportes016 SET salario=$salario, categoria ='$categoria' WHERE idempresa=$idEmpresa AND idpersona=$idPersona";
	$rs = $db->queryActualiza ( $sql );
	return $rs;
}

function claseAportante($codigo) {
	switch ($codigo) {
		case 'A' :
			$clase = 2653;
			break;
		case 'B' :
			$clase = 2654;
			break;
		case 'C' :
			$clase = 2874;
			break;
		case 'D' :
			$clase = 2875;
			break;
		case 'I' :
			$clase = 2655;
			break;
	}
	return $clase;
}

function sectorEmp($codigo) {
	switch ($codigo) {
		case '1' :
			$sector = 93;
			break;
		case '2' :
			$sector = 94;
			break;
		case '3' :
			$sector = 95;
			break;
		default :
			$sector = 94;
	}
	return $sector;
}

function tipoDocumento($cod) {
	switch ($cod) {
		case "CC" :
			$tipoDocu = 1;
			break;
		case "CE" :
			$tipoDocu = 4;
			break;
		case "TI" :
			$tipoDocu = 2;
			break;
		case "RC" :
			$tipoDocu = 6;
			break;
		case "PA" :
			$tipoDocu = 3;
			break;
		case "NI" :
			$tipoDocu = 5;
			break;
		default :
			$tipoDocu = 0;
	}
	return $tipoDocu;
}

function planoAppend($cadena, $fp) {
	$cadena .= "\r\n";	
	fwrite ( $fp, $cadena );
	fflush($fp);
}

function fileClose($fp, $file){
	fclose ( $fp );
	
	if(filesize($file)==0){
		unlink ( $file );
	}
}

function actualizarEmpresa($db, $idEmpresa, $idPersona, $tipoaportante, $telefono, $email, $indiceAportes, $claseaportante, $fp_errores) {
	$sql = "UPDATE aportes048 set indicador='$indiceAportes' WHERE idempresa = $idEmpresa";
	// $sql=utf8_encode($sql);
	$rs = $db->queryActualiza ( $sql );
	if ($rs == 0) {
		$cadena = "No se pudo actulizar aportes048 $sql";
		planoAppend ( $cadena, $fp_errores );
		$cadena="{\"error\":\"No se pudo actulizar aportes048\",\"Planilla\":\"$planilla\",\"Nit\":\"$numero\",\"Identificacion\":\"$numero\",\"Periodo\":\"$periodo\",\"FechaPago\":\"$fechaPago\",\"Tipo\":\"ErrorDos\"},";
		planoAppend ( $cadena, $fp_print );
	}
}

function inactivar($db, $idPersona, $idEmpresa, $diasCot, $periodo, $usuario, $file) {
	if (intval ( $diasCot ) == 0)
		return false;
	if (strlen ( $diasCot ) == 1) {
		$diasCot = '0' . $diasCot;
	}
	
	if( $diasCot >= 30 || ( $diasCot == 29 && substr( $periodo, 4, 2) == '02' ) ){
		$fecha = getUltimoDiaMes(substr($periodo,0,4), substr($periodo,4,2));		
	} else {
		$fecha = $periodo . $diasCot;
	}
	
	$sql = "SELECT ISNULL(MAX(idformulario),0) AS idformulario, ISNULL(MAX(tipoafiliacion),0) AS tipoafiliacion 
			FROM aportes016 WHERE fechaingreso <= '$fecha' AND idpersona=$idPersona AND idempresa=$idEmpresa";	
	$rsf = $db->querySimple ( $sql );
	$rowf = $rsf->fetch ();
	$cuenta = $rowf ['idformulario'];
	if ($cuenta > 0) {
		$idf = $rowf ['idformulario'];
		$idta = intval ( $rowf ['tipoafiliacion'] );
		$idMotivoRetiro = 2858; // falta esta variable
		
		$retorno = -1;
		$proc = "EXECUTE [dbo].sp_inactivar :idf, :idMotivoRetiro, :fecha, :usuario, :retorno";
		$stmt=$db->conexionID->prepare($proc);
		$stmt->bindValue(':idf', $idf , PDO::PARAM_STR);
		$stmt->bindValue(':idMotivoRetiro', $idMotivoRetiro, PDO::PARAM_STR);
		$stmt->bindValue(':fecha', $fecha, PDO::PARAM_STR);
		$stmt->bindValue(':usuario', $usuario, PDO::PARAM_STR);
		$stmt->bindParam(':retorno', $retorno, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000);
		
		$stmt->execute();
		
		if ($retorno == 0) {
			$cadena = "NO Se pudo INACTIVAR la afiliacion de: $idPersona --> $idEmpresa";
			planoAppend ( $cadena, $file );
		} else if ($retorno == 1) {
			$cadena = "NO Se pudo INSERTAR en aportes017 : $idPersona --> $idEmpresa";
			planoAppend ( $cadena, $file );
		} else {
			$cadena = "Se inactivo la afiliacion de: $idPersona --> $idEmpresa";
			planoAppend ( $cadena, $file );
			
			if ($idta != 18) {	// Si es trabajador independiente se inactiva la empresa
				$sql = "Update aportes048 set estado='I', fechaestado = cast(getdate() as date), flag='P' where idempresa=$idEmpresa";
				$rs = $db->queryActualiza ( $sql );
			}
		}
	} else {
		$cadena = "NO se encontro una afiliacion que cumpla con los requisitos, el afiliado no se pudo inactivas: idpersona: $idPersona -->$idEmpresa";
		planoAppend ( $cadena, $file );
	}

}

function ejecutarSpCategorias($idPersona,$idEmpresa,$fp_afiliaciones,$fp_errores){
	//Ejecutar el procedimiento para actualizar la categoria
	$rsSpProcesoIndep = $GLOBALS["objAfiliacion"]->spProcesoIndepPensCategorias($idPersona);
	if($rsSpProcesoIndep==1){
		$cadena = "Se actualizo la categoria : $idPersona --> $idEmpresa";
		planoAppend ( $cadena, $fp_afiliaciones );
	}else {
		$cadena = "NO Se pudo actualizar la categoria : $idPersona --> $idEmpresa";
		planoAppend ( $cadena, $fp_errores );
	}
}

function getUltimoDiaMes($elAnio, $elMes) {
	return date( "Ymd", ( mktime ( 0, 0, 0, $elMes + 1, 1, $elAnio ) -1 ) );
}

?>
</body>
</html>