<?php
//echo $raiz=dirname(__FILE__);
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$usuario=$_SESSION['USUARIO'];
include_once $raiz.DIRECTORY_SEPARATOR.'config.php';
include_once $raiz . DIRECTORY_SEPARATOR .'funcionesComunes'.DIRECTORY_SEPARATOR.'funcionesComunes.php';
$url=$_SERVER['PHP_SELF'];
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'auditoria.php';
auditar($url);

$dirProcesar = $ruta_cargados.'planillas'. DIRECTORY_SEPARATOR . 'procesados' . DIRECTORY_SEPARATOR;
$ok=verificaDirectorio($dirProcesar);
if($ok==0){
	echo 2;		//no se pudo crear el directorio destino
	exit();
}

$dirPlanos = $ruta_cargados.'planillas'. DIRECTORY_SEPARATOR .'cargados'. DIRECTORY_SEPARATOR;
$ok=verificaDirectorio($dirPlanos);
if($ok==0){
	echo 2;		//no se pudo crear el directorio destino
	exit();
} 

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>::Cargar Planilla &Uacute;nica::</title>
<link type="text/css" href="../../css/css.1.8/jquery-ui-1.8.17.custom.css" rel="stylesheet" />
<link type="text/css" href="../../css/Estilos.css" rel="stylesheet"/>
<link href="../../css/marco.css" rel="stylesheet" type="text/css" />
<link href="../../css/fileuploader.css" rel="stylesheet" type="text/css" />

<script language="javascript" src="../../js/1.6/jquery-1.6.2.min.js"></script>
<script language="javascript" src="../../js/1.6/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="../../js/comunes.js"></script>
<script src="../../js/fileuploader.js" type="text/javascript"></script>
<script type="text/javascript" src="../../js/jquery.copy.min.js" language="javascript"></script>

<script type="text/javascript" src="js2/buscarArchivos.js" language="javascript"></script>
<script type="text/javascript" src="js2/leerArchivo.js" language="javascript"></script>
<script type="text/javascript" src="js2/procesarNominasPU.js" language="javascript"></script>

<script type="text/javascript" src="js2/inicial.js" language="javascript"></script>

<style type="text/css">
#btnProcesarNominas{
	boder: 1px solid #AFAFAF;
	width: 180px;
	height: 50px;
	text-align: left;
	font-size: 15px;
	background-image: url('../../imagenes/procesar.png');
	background-position: right;
	background-repeat: no-repeat;
}
.box-table-a{
	width: none;
}
</style>
</head>

<body>
<div id="wrapTable">
  <table width="97%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
     <td width="13" height="29" class="arriba_iz">&nbsp;</td>
     <td class="arriba_ce">
     <span class="letrablanca">::&nbsp;Cargar N&oacute;mina Planilla &Uacute;nica::</span></td>
      <td width="13" class="arriba_de" align="right">&nbsp;</td>
      </tr>

    <tr>
     <td class="cuerpo_iz">&nbsp;</td>
	 <td class="cuerpo_ce">
        <img src="../../imagenes/spacer.gif" width="1" height="1" />
        <img src="../../imagenes/spacer.gif" width="1" height="1" />
        <img src="../../imagenes/spacer.gif" width="1" height="1" />
        <img src="../../imagenes/spacer.gif" width="1" height="1" />
        <img src="../../imagenes/menu/informacion.png" width="16" height="16" style="border: none; cursor: pointer" title="Ayuda en l&iacute;nea" onClick="mostrarAyuda();" />
  <img src="../../imagenes/menu/notas.png" width="16" height="16" style="cursor: pointer" title="Colaboraci&oacute;n en l&iacute;nea" onclick="notas();" />
        <div id="error" style="color:#FF0000"></div>
      </td>
      <td class="cuerpo_ce">&nbsp;</td>
      </tr>

    <tr>
      <td class="cuerpo_iz">&nbsp;</td>
      <!-- TABLAS DE FORMULARIO -->
      <td class="cuerpo_ce">
        <center>
          <table width="90%" border="0" cellspacing="0" class="tablero">
          <tr>
          	<td>
          		<div id="divCargarArchivos" style="display: inline-block">
					<noscript>
						<p>Please enable JavaScript to use file uploader.</p>
						<!-- or put a simple form for upload here -->
					</noscript>
				</div>
                
	          	
                
	          	<input type="hidden" name="hdnDirPlanos" id="hdnDirPlanos" value="<?php echo $dirPlanos ?>" />
	          	<input type="hidden" name="hdnDirDestino" id="hdnDirDestino" value="<?php echo $dirProcesar ?>" /></td>
          </tr>
            <tr>
              <td>
              <a href="../../centroReportes/aportes/empresas/configReporteEmpresa.php?tipo=40&tit=40" target="_new"><img src="../../imagenes/messagebox_warning.png" width="32" height="32">Informe de Incosistencias empresas ley 1429</a>
		      </td>
            </tr>
            <tr>
            	<td>
            		
            	</td>
            </tr>
			<tr>
				<td>
					<p align="center">XXX
                    <div id="boton1" style="display:block; text-align:center" >
					  <input type="button" id="btnProcesarNominas" name="btnProcesarNominas" value="Procesar Planillas" />
                      </div>
                      <div id="mensaje1" class="Rojo" style="padding-left:200px"></div>
                      <div id="mensaje2" style="padding-left:200px"></div>
					</p>
                    
				</td>
			</tr>
			<tr id="trErroresLog" style="display: none;">
				<td>
					LOG DE ERRORES:
					<br />
					<table width="75%" border="0" cellspacing="0" cellpadding="0" align="center" class="tablero" id="tblErrores">
						<tr>
							<th>Error</th>
							<th>Planilla</th>
							<th>Nit</th>
							<th>Identificacion</th>
							<th>Periodo</th>
							<th>Fecha Pago</th>
						</tr>
						<tbody id="tbErroresLog"></tbody>
					</table>
			<tr id="trErrores" style="display: none;">
				<td>
					LOG DE ERRORES CONSULTANDO PERSONAS:
					<br />
					<table width="75%" border="0" cellspacing="0" cellpadding="0" align="center" class="tablero" id="tblErrores">
						<tr>
							<th width="40%">Archivo</th>
							<th width="25%">Descripcion</th>
							<th width="6%">TD</th>
							<th width="15%">Identificacion</th>
						</tr>
						<tbody id="tbErrores"></tbody>
					</table>
				
				</td>
			</tr>
          </table>
        </center>
       <td class="cuerpo_de">&nbsp;</td><!-- FONDO DERECHA -->
      </center>
      </tr>
    <tr>
      <td class="abajo_iz" >&nbsp;</td>
      <td class="abajo_ce" ></td>
      <td class="abajo_de" >&nbsp;</td>
    </tr>
  </table>
</div>
<!-- colaboracion en linea -->
<div id="div-observaciones-tab"></div>
<div id="dialogo-archivo" title="Planilla &Uacute;nica">
<div id="progreso" style="display: none; font-size: 15pt; font-weight: bold;"><span id="pg">0</span> de <span id="tt"></span> archivos</div>
<div id="log"></div>
</div>
<div id="dialog-form2" title="Colaboraci&oacute;n en l&iacute;nea" style="display:none">
<p>Por favor diligencie este formulario para enviar comentarios, errores o falencias encontradas en el proceso. M&aacute;ximo 250 caracteres </p>
<label>Tus comentarios:</label><br />
<textarea name="notas" id="notas" cols="60" rows="10"></textarea>
</div>

<!-- fin colaboracion -->

<!-- Manual Ayuda -->
<div id="ayuda" title="Manual Ayuda Planilla �nica" style="background-image:url('../../imagenes/FondoGeneral0.png')">
</div>
<input type="hidden" id="txtusuario" name="txtusuario" value="<?php echo $usuario; ?>" /> 
</body>
<script language="javascript">
function mostrarAyuda(){
	$("#ayuda").dialog('open');
	}

function notas(){
	$("#dialog-form2").dialog('open');
	}
</script>
</html>