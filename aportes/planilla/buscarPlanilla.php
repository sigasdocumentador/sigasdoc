<?php
	$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
	include_once  $root;
	
	include_once $raiz .DIRECTORY_SEPARATOR. 'rsc' .DIRECTORY_SEPARATOR. 'pdo' .DIRECTORY_SEPARATOR. 'IFXDbManejador.php';
	include_once $raiz .DIRECTORY_SEPARATOR. 'rsc' .DIRECTORY_SEPARATOR. 'pdo' .DIRECTORY_SEPARATOR. 'IFXerror.php';
	
	$db = IFXDbManejador::conectarDB();
	if($db->conexionID==null){
		$cadena = $db->error;
		echo msg_error($cadena);
		exit();
	}
	
	$tipo = $_REQUEST["tipoIdentificacion"];
	$idIdentificacion = $_REQUEST["idIdentificacion"];
	$periodoInicial = $_REQUEST["periodoInicial"];
	$periodoFinal = $_REQUEST["periodoFinal"];
	
	$filtroSql = "";
	if($tipo=="Empresa"){
		$filtroSql = "idempresa=".$idIdentificacion;
	}else{
		$filtroSql = "idtrabajador=".$idIdentificacion;
	}
	
	$sql = "SELECT a10.*
				,a15.pnombre+' '+ISNULL(a15.snombre,'')+' '+a15.papellido+' '+ISNULL(a15.sapellido,'') AS trabajador
				,a15.identificacion AS identificaTrabajador
			FROM aportes010 a10
				INNER JOIN aportes015 a15 ON a15.idpersona=a10.idtrabajador 
			WHERE periodo BETWEEN '$periodoInicial' AND '$periodoFinal' AND $filtroSql
			ORDER BY periodo";
	$rs = $db->querySimple($sql);
	$arrDatos = array();
	while($row=$rs->fetch()){
		$arrDatos[] = $row;
	}
	echo json_encode($arrDatos);
?>