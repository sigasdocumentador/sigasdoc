<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';
//include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'wssdk' . DIRECTORY_SEPARATOR . 'ClientWSInfWeb.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';

include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'utilitario.class.php';
include_once $raiz.DIRECTORY_SEPARATOR.'terceros'.DIRECTORY_SEPARATOR.'BaseTerceros.php';

class ServicePu{
	private $idProceso;
	private $idTrazaProceso;
	private $usuario;
	private $objPuTipoA;
	private $objPuTipoI;
	private $objLogProceso;
	private $objInterfaz;
	
	private $nit;
	private $razonSocial;
	private $digito_empresa;
	private $direccion_empresa;
	private $codig_ciuda_empre;
	private $codig_depar_empre;
	private $telefono_empresa;
	private $email_empresa;
	private $tipo_docum_empre;
	
	function __construct(){
		//$this->objInterfaz = new ClientWSInfWeb ( USUARIO_WEB_SIGAS, CONTRASENA_WEB_SIGAS );
		//echo 'USUARIO_WEB_SIGAS:'.USUARIO_WEB_SIGAS.'-CONTRASENA_WEB_SIGAS'.CONTRASENA_WEB_SIGAS;
		//var_dump($this->objInterfaz);
		//exit();
	}
	
	public function getIdProceso(){return $this->idProceso;}
	public function setIdProceso($idProceso){$this->idProceso = $idProceso;}
	
	public function getIdTrazaProceso(){return $this->idTrazaProceso;}
	public function setIdTrazaProceso($idTrazaProceso){$this->idTrazaProceso = $idTrazaProceso;}
	
	public function getUsuario(){return $this->usuario;}
	public function setUsuario($usuario){$this->usuario = $usuario;}
	
	public function getObjPuTipoA(){return $this->objPuTipoA;}
	public function setObjPuTipoA($objPuTipoA){$this->objPuTipoA = $objPuTipoA;}
	
	public function getObjPuTipoI(){return $this->objPuTipoI;}
	public function setObjPuTipoI($objPuTipoI){$this->objPuTipoI = $objPuTipoI;}
	
	public function getObjLogProceso(){return $this->objLogProceso;}
	public function setObjLogProceso($objLogProceso){$this->objLogProceso = $objLogProceso;}
	
	/**
	 * Metodo encargado de procesar los datos del servicio
	 * 
	 * @return number [0:Error, 1:ok]
	 */
	public function procesar_datos(){
		$estado = 'PROCESO';
		
		$estadoProcesoA = $this->procesar_pu_tipo_a();
		if($estadoProcesoA=='FIN_EXITOSO' || $estadoProcesoA=='FIN_ERROR'){
			
			$estadoProcesoI = $this->procesar_pu_tipo_i();
			if($estadoProcesoI=='FIN_EXITOSO' || $estadoProcesoI=='FIN_ERROR'){
				
				if ($estadoProcesoA=='FIN_ERROR' || $estadoProcesoI=='FIN_ERROR'){
					$estado = 'FIN_ERROR';
				}else{
					$estado = 'FIN_EXITOSO';
				}
			}
		}
		
		return $estado;
	}
	
	private function procesar_pu_tipo_a(){
		$this->objPuTipoA->setIdProceso($this->idProceso);
		$estado = $this->objPuTipoA->estado_interfaze();
		
		if($estado=='FIN_EXITOSO' || $estado=='FIN_ERROR'){
			return $estado;
		}else{
			$estado = 'PROCESO';
		}
		
		$arrDatos = $this->objPuTipoA->fetch_datos_interfaz(100);
		
		foreach($arrDatos as $row){
			
			try{
				$banderaError = 'N';
				$this->nit = $row["nit_db"];
				//$this->tipo_docum_empre=$row["tipo_docum_empre"];
                                //$this->digito_empresa=$row["digito_verif_empre"];
                                
                                $arrInformacionEmpresa = $this->obtenerInformacionTercero($row["nit_db"]);
                                $this->tipo_docum_empre=  $arrInformacionEmpresa["idTipoDocumento"];
                                $this->digito_empresa = $arrInformacionEmpresa["digitoVerificacion"];
								
								
								$file = fopen("pruebas Envio WSDL2.txt", "a+");
								fwrite($file, " IDTIPODOCUMENTO => ".$arrInformacionEmpresa["idTipoDocumento"]. PHP_EOL);
								fwrite($file, " DIGITOVERIFICACION => ".$arrInformacionEmpresa["digitoVerificacion"]. PHP_EOL);
								fwrite($file, "-----------------------------------------------------------------------------------" . PHP_EOL);
								fclose($file);
				
				$this->razonSocial = $row["razon_social"];
				$this->direccion_empresa=$row["direccion_empresa"];
				$this->codig_ciuda_empre=$row["codig_ciuda_empre"];
				$this->codig_depar_empre=$row["codig_depar_empre"];
				$this->telefono_empresa=$row["telefono_empresa"];
				$this->email_empresa=$row["email_empresa"];


				if(!$this->existe_empresa_interfaz()){
					//Crear empresa en la interfaz
					if($this->crear_empresa_interfaz()){
						//Se creo la empresa en la interfaz
						
						//Guardar Log
						$descripcion = 'La empresa se creo en la interfaz del archivo TIPO A';
						$data = '{"id_pu_tipo_a":"'.$row["id_pu_tipo_a"].'","nombre_archivo":"'.$row["nombrearchivo"].'","tipo_identificacion":"'.$row["tipo_docum_empre"].'","numero_identificacion":"'.$row["nit_db"].'","nombre":"'.$row["razon_social"].'"}';
						$this->objLogProceso->guardar_log_db($this->idTrazaProceso,'EMPRESA','B',$descripcion, $data,'EXITO');
						
					}else{
						//No se creo la empresa en la interfaz		
						$banderaError = 'S';
						
						//Guardar Log
						$descripcion = 'Error la empresa no se creo en la interfaz del archivo TIPO A';
						$data = '{"id_pu_tipo_a":"'.$row["id_pu_tipo_a"].'","nombre_archivo":"'.$row["nombrearchivo"].'","tipo_identificacion":"'.$row["tipo_docum_empre"].'","numero_identificacion":"'.$row["nit_db"].'","nombre":"'.$row["razon_social"].'"}';
						$this->objLogProceso->guardar_log_db($this->idTrazaProceso,'EMPRESA','B',$descripcion, $data,'ERROR');
					}
				}
				
				//Update registro
				$resultado = $this->objPuTipoA->update_estado($row["id_pu_tipo_a"],$banderaError);
				
			}catch(Exception $e){
				$resultado = $this->objPuTipoA->update_estado($row["id_pu_tipo_a"],'E');
				
				//Guardar Log
				$descripcion = 'Error al procesar los datos en la interfaz del archivo TIPO A';
				$data = '{"id_pu_tipo_a":"'.$row["id_pu_tipo_a"].'","nombre_archivo":"'.$row["nombrearchivo"].'","tipo_identificacion":"'.$row["tipo_docum_empre"].'","numero_identificacion":"'.$row["nit_db"].'","nombre":"'.$row["razon_social"].'"}';
				$this->objLogProceso->guardar_log_db($this->idTrazaProceso,'EMPRESA','B',$descripcion, $data,'ERROR');
			}
		}
		
		return $estado;
	}
	
	private function procesar_pu_tipo_i(){
		$this->objPuTipoI->setIdProceso($this->idProceso);
		$estado = $this->objPuTipoI->estado_interfaze();
		
		if($estado=='FIN_EXITOSO' || $estado=='FIN_ERROR'){
			return $estado;
		}else{
			$estado = 'PROCESO';
		}
		
		$arrDatos = $this->objPuTipoI->fetch_datos_interfaz(100);
		
		foreach($arrDatos as $row){
			
			try{
				$banderaError = 'N';
                                
                                $this->nit = $row["nit_tipo1_db"];
                                $arrInformacionEmpresa = $this->obtenerInformacionTercero($row["nit_tipo1_db"]);
                                $this->tipo_docum_empre =  $arrInformacionEmpresa["idTipoDocumento"];
                                $this->digito_empresa = $arrInformacionEmpresa["digitoVerificacion"];
                                
                                /*
				$this->nit = $row["nit_tipo1_db"];
				$this->digito_empresa = $row["digit_verif_empre_tipo1"];
                                $this->tipo_docum_empre = $row["tipo_docum_empre_tipo1"];
                                 * 
                                 */
				if(!$this->existe_empresa_interfaz()){
					//No se creo la empresa en la interfaz		
					$banderaError = 'S';
					
					//Guardar Log
					$descripcion = 'Error la empresa no existe en la interfaz, archivo TIPO I';
					$data = '{"nombre_archivo":"'.$row["nombrearchivo"].'","tipo_identificacion":"'.$row["tipo_docum_empre_tipo1"].'","numero_identificacion":"'.$row["nit_tipo1_db"].'","nombre":"'.$row["razon_socia_tipo1"].'"}';
					$this->objLogProceso->guardar_log_db($this->idTrazaProceso,'EMPRESA','B',$descripcion, $data,'ERROR');
					
				}
				
				//Update registro
				$resultado = $this->objPuTipoI->update_estado($row["id_archivo"],$row["nit_tipo1_db"],$banderaError);
				
			}catch(Exception $e){
				$resultado = $this->objPuTipoI->update_estado($row["id_archivo"],$row["nit_tipo1_db"],'E');
				
				//Guardar Log
				$descripcion = 'Error al procesar los datos en la interfaz del archivo TIPO I';
				$data = '{"nombre_archivo":"'.$row["nombrearchivo"].'","tipo_identificacion":"'.$row["tipo_docum_empre_tipo1"].'","numero_identificacion":"'.$row["nit_tipo1_db"].'","nombre":"'.$row["razon_socia_tipo1"].'"}';
				$this->objLogProceso->guardar_log_db($this->idTrazaProceso,'EMPRESA','B',$descripcion, $data,'ERROR');
			}
		}
		
		return $estado;
	}

	private function existe_empresa_interfaz() {
		//return $this->objInterfaz->terceroExisteStr ( $this->nit );
		$nit=Utilitario::format_nit($this->nit);
		$terceros = new BaseTerceros();
		//if(isset($this->digito_empresa) && $this->digito_empresa != "0" && $this->tipo_docum_empre = 5){
        if(isset($this->digito_empresa) && $this->tipo_docum_empre == 5){
			return $terceros->ExisteTercero($nit."-".$this->digito_empresa );
		}else{
			return $terceros->ExisteTercero($nit);
		}
		
	}
	
	function crear_empresa_interfaz() {
		//obtener la informacion de aportes 425
		//remplazar por nuevo tercero
		//$cliente = new ClientWSInfWeb ( USUARIO_WEB_SIGAS, CONTRASENA_WEB_SIGAS );
		$terceros = new BaseTerceros();
		$nit=Utilitario::format_nit($this->nit);
		
		$tipo_documento="";
		if($this->tipo_docum_empre == 5){//NIT
                    //$tipo_documento="5";
			if(isset($this->digito_empresa)){
				$nit = $nit."-".$this->digito_empresa;
			}
		}else{
                    //$tipo_documento="1";
		}
		
		
		//$respuesta =Creacion("tilag","1.075.227.896","6","SERGIO IVAN MEDINA GOMEZ","CALLE 55 1 D- 30","41",$ciudad,"8750403","xxx@xxx.com");
		//var_dump($terceros);exit();
		$ciudad = $terceros->ObtenerCiudad($this->codig_depar_empre,$this->codig_ciuda_empre);
                if($ciudad == "" || $ciudad == null || $ciudad == "null" || $ciudad = " "){
                    $ciudad = "NEIVA";
                    $this->codig_ciuda_empre = 41001;//Neiva 
                    $this->codig_depar_empre = 41;//Huila
                    
                }
		
		$respuesta =$terceros->CreacionTercero($this->usuario,$nit,$this->razonSocial,$this->direccion_empresa,$this->codig_depar_empre,$this->codig_ciuda_empre,$ciudad,$this->telefono_empresa,$this->email_empresa,$this->tipo_docum_empre);
		$error=$respuesta->getError();
		//debug(isset($error));
		if(isset($error)&&$error->getErrorCode()!="DB-ERR-0")
		{
                    //var_dump($error); 
                    return 0;//Cero es falso
		}
		return 1;
		//return ($this->objInterfaz->terceroGrabarStr ( $objTercero )) ? true : false;
	}
	
	/**
	* Retorna los valores obtenidos en la consulta
	* @return multitype:array
	*/
	private function fetchConsulta($querySql){
		$resultado = array();
		$rs = self::$con->querySimple($querySql);
		while($row = $rs->fetch())
			$resultado[] = array_map("trim",$row);
		//$resultado[] = array_map("utf8_encode",$row);
		return $resultado;
	}
        private function obtenerInformacionTercero($nit){
            $terceros = new BaseTerceros();
            $idTipoDocumento = $terceros->obtenerInformacionEmpresa($nit);
            return $idTipoDocumento;
        }
}

?>