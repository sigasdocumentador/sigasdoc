<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';

include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'utilitario.class.php';

//include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'FileUploader.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'aportes' . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'maestro.proceso.class.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'aportes' . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'log.proceso.class.php';


include_once 'puTipoA.class.php';
include_once 'puTipoI.class.php';
include_once 'servicePU.class.php';


class MaestroPU{

	private $idTrazaProceso = 0;
	private $idProceso = 0;
	private $fechaPago = "";
	private $tipoCarge = null;
	private $arrFiles = array();
	
	/*************************************
	 * ATTR para el carge de los archivos
	 *************************************/
	
	/**
	 * Puede ser: ARCHIVO, EMPRESA, AFILIADO 
	 * @var unknown_type
	 */
	private $codigoError = "";
	
	/**
	 * Los indices de array son: nombre del error, [nombre del archivo plano], [informacion en formato json] 
	 * @var unknown_type
	 */
	private $arrError = array("error"=>0,"descripcion"=>"");//array("error"=>"","nombre_archivo"=>"","informacion"=>"");
	
	private $objMaestroProceso = null;
	private $tipoProceso = "PU";
	private $usuario = null;
	private $rutaCargados;
	
	private $bandeEtapaProceTermi = null;
	
	private $objLogProceso = null;
	
	private static $con = null;
	
	function __construct(){
		
		$this->objMaestroProceso = new MaestroProceso();
		$this->objMaestroProceso->setTipo($this->tipoProceso);
		//$this->objMaestroProceso->setUsuario($this->usuario);
		
		$this->objLogProceso = new LogProceso();
		
		//$this->arrProcesos = $this->procesos();
		
		//Conexion
		try{
			self::$con = IFXDbManejador::conectarDB();
			if( self::$con->conexionID==null ){
				throw new Exception(self::$con->error);
			}
		}catch(Exception $e){
			echo $e->getMessage();
			exit();
		}
	}
	
	public function ejecutar($idProceso = 0, $usuario, $fechaPago, $reprocesar=false, $rutaCargados, $tipoCarge,$arrFiles){
		$this->idProceso = $idProceso;
		$this->fechaPago = $fechaPago;
		$this->usuario = $usuario;
		$this->rutaCargados = $rutaCargados;
		$this->tipoCarge = $tipoCarge;
		$this->arrFiles = $arrFiles;
	
		$this->objMaestroProceso->setIdProceso($this->idProceso);
		$this->objMaestroProceso->setUsuario($this->usuario);
		$this->objMaestroProceso->setRutaCargados($this->rutaCargados);
	
		$this->objLogProceso->setUsuario($this->usuario);
	
		if($reprocesar==false){
			$this->procesar();
		}else{
			$this->reprocesar();
		}
	
		return $this->arrError;
	}
	
	private function procesar(){
		
		//Verificar si es un nuevo proceso
		if($this->idProceso==0){
			//iniciar nuevo proceso
			$arrResultado = $this->objMaestroProceso->iniciar_proceso();
			
			if($arrResultado["error"]==1){
				//Error
				$this->arrError["error"] = 1;
				$this->arrError["descripcion"] = $arrResultado["descripcion"];
			}else{
				
				//Resetear procesos antiguos
				$resultado = $this->ejecutar_sp_resetear_pu();
				if($resultado===false){
					$descripcion = 'Error al resetear los procesos anteriores';
					$this->arrError["error"] = 1;
					$this->arrError["descripcion"] = $descripcion;
					
					//Error al ejecutar el proceso
					$this->guardar_log_db($this->idTrazaProceso,'PROCESAR_DATOS_INTERFAZ','A',$descripcion, null,'ERROR');
				}else{
					$this->arrError["error"] = 0;
					$this->arrError["descripcion"] = $arrResultado["descripcion"];
					$this->idProceso = $arrResultado["data"]["id_proceso"];
				}
			}
		}
		
		if($this->idProceso>0){
			
			$this->procesar_etapa_proceso();
			
		}else if($this->arrError["error"] == 0){
			$this->arrError["error"] = 1;
			$this->arrError["descripcion"] = "Error: Inesperado metodo: MaestroPU->procesar()";
		}
	}
	
	private function reprocesar(){
		//iniciar nuevo proceso
		$arrResultado = $this->objMaestroProceso->iniciar_reproceso();
			
		if($arrResultado["error"]==1){
			//Error
			$this->arrError["error"] = 1;
			$this->arrError["descripcion"] = $arrResultado["descripcion"];
		}else{
			$this->arrError["error"] = 0;
			$this->arrError["descripcion"] = $arrResultado["descripcion"];
			$this->idProceso = $arrResultado["data"]["id_proceso"];
		}
	}
	
	private function procesar_etapa_proceso(){
		$arrDataTrazaProce = $this->objMaestroProceso->iniciar_etapa_proceso();
		
		if($arrDataTrazaProce["error"]==0){
			
			$idEtapaProceso = $arrDataTrazaProce["data"]["id_etapa_proceso"];
			$etapaProceso = $arrDataTrazaProce["data"]["etapa_proceso"];
			$idTrazaProceso = $arrDataTrazaProce["data"]["id_traza_proceso"];
			
			$this->idTrazaProceso = $idTrazaProceso;
			
			$this->arrError["error"] = 0;
			$this->arrError["descripcion"] = "";
			
			$banderaProceTermi = false;
			
			//Procesar las etapas--------------------------------------------------------
			switch ($etapaProceso){
				case "inicio": //Se inicio el proceso
					$banderaProceTermi = true;
					break;
				case "subir_archivo": //Cargar los archivos planos al servidor desde el FTP
					
					$this->subir_archivo();
					//Verificar si el proceso se completo
					if($this->bandeEtapaProceTermi === true){
						$banderaProceTermi = true;
					}
					
					break;
				case "preparar_archivo": //Preparar los archivos planos
					
					$this->preparar_archivo($idEtapaProceso);
					//Verificar si el proceso se completo
					if($this->bandeEtapaProceTermi === true){
						$banderaProceTermi = true;
					}
					
					break;
				case "procesar_dato_sigas": //Procesar la informacion en la Base de datos de SIGAS
					
					$this->procesar_dato_sigas();
					//Verificar si el proceso se completo
					if($this->bandeEtapaProceTermi === true){
						$banderaProceTermi = true;
					}
					
					break;
				case "procesar_dato_service": //Procesar la informacion en la Base de datos externa
					
					$this->procesar_dato_service();
					//Verificar si el proceso se completo
					if($this->bandeEtapaProceTermi === true){
						$banderaProceTermi = true;
					}
					
					break;
				case "log_proceso": //Logs del proceso
					
					$this->log_proceso();
					//Verificar si el proceso se completo
					if($this->bandeEtapaProceTermi === true){
						$banderaProceTermi = true;
					}
					
					break;
				case "fin": //Se termino el proceso
					
					$banderaProceTermi = true;
					
					break;
			}
			
			//Finalizar etapa proceso exitoso
			if($banderaProceTermi === true){
				$procesado = $this->arrError["error"]==0 ? "S" : "E"; //[S]Fin exitoso - [E]Fin error  
				
				//Terminar la etapa proceso
				$arrResultado = $this->objMaestroProceso->terminar_etapa_proceso($idTrazaProceso,$procesado,$etapaProceso);
				
				if($arrResultado["error"]==0){
					
					$descripcion = 'La etapa proceso se termino correctamente';
					$this->arrError["error"] = 0;
					$this->arrError["descripcion"] = $descripcion;
					$this->arrError["data"]["etapa_proceso"] = $etapaProceso;
					
					$this->guardar_log_db($this->idTrazaProceso,'PROCESAR_ETAPA','A',$descripcion, null,'EXITO');
				}else{
					$this->arrError["error"] = 1;
					$this->arrError["descripcion"] = $arrDataTrazaProce["descripcion"];
					
					$this->guardar_log_db($this->idTrazaProceso,'PROCESAR_ETAPA','A',$arrDataTrazaProce["descripcion"],null,'ERROR');
				}
				
			}else if($this->arrError["error"]==0){
				$this->arrError["error"] = 0;
				$this->arrError["descripcion"] = "La etapa esta en proceso";
				$this->arrError["data"]["etapa_proceso"] = $etapaProceso;
			}
			
		}else{
			$this->arrError["error"] = 1;
			$this->arrError["descripcion"] = $arrDataTrazaProce["descripcion"];
			
			$this->guardar_log_db($this->idTrazaProceso,'PROCESAR_ETAPA','A',$arrDataTrazaProce["descripcion"],null,'ERROR');
		}
	}
	
	

	/**
	 * Carge Archivo Dir PHP (php) 
	 */
	private function subir_archivo(){
		
		$this->bandeEtapaProceTermi = false;
		
		$uriPlanoCargado = $this->rutaCargados . 'planillas' . DIRECTORY_SEPARATOR . 'cargados' . DIRECTORY_SEPARATOR;
		
		//Cargar archivos manualmente
		if($this->tipoCarge=="CARGE"){
			
			$resultadoCarge = $this->objMaestroProceso->carga_archivo_manual($uriPlanoCargado,"txt", array("txt","text","text/plain"),$this->arrFiles);
			
			//Verificar si los archivos se cargaron correctamente
			if($resultadoCarge==false){
				$descripcion = 'Error al cargar los archivos al servidor';
				$this->arrError["error"] = 0;
				$this->arrError["descripcion"] = $descripcion;
				$this->bandeEtapaProceTermi = true;
					
				$this->guardar_log_db($this->idTrazaProceso,'CARGAR_ARCHIVO','A',$descripcion, null,'EXITO');
			
			}else if(count($this->arrFiles)==0){
				
				//Obtener los archivos cargados
				$countArchivos = count($this->objMaestroProceso->fetch_archivo_cargado($uriPlanoCargado));
				
				//El proceso termino correctamente
				$descripcion = 'Los Archivos se cargaron correctamente al servidor, Cant Archivos: ('.$countArchivos.')';
				$this->arrError["error"] = 0;
				$this->arrError["descripcion"] = $descripcion;
				$this->bandeEtapaProceTermi = true;
					
				$this->guardar_log_db($this->idTrazaProceso,'CARGAR_ARCHIVO','A',$descripcion, null,'EXITO');
			}
			
		}else if($this->tipoCarge!="CARGE"){
			
			//Obtener los archivos cargados
			$countArchivos = count($this->objMaestroProceso->fetch_archivo_cargado($uriPlanoCargado));
			
			//Validar la nomina manual
			//El proceso termino correctamente
			$descripcion = 'Los Archivos se cargaron correctamente al servidor, Nomina Manual, Cant Archivos: ('.$countArchivos.')';
			$this->arrError["error"] = 0;
			$this->arrError["descripcion"] = $descripcion;
			$this->bandeEtapaProceTermi = true;
				
			$this->guardar_log_db($this->idTrazaProceso,'CARGAR_ARCHIVO','A',$descripcion, null,'EXITO');
		}
		
		//Cargar archivos automaticamente
		
		
		//Falta configurar el FTP
		
		//$uriFTP = $this->rutaCargados . 'planillas' . DIRECTORY_SEPARATOR . 'FTP' . DIRECTORY_SEPARATOR;
		//$uriPlanoCargado = $this->rutaCargados . 'planillas' . DIRECTORY_SEPARATOR . 'cargador' . DIRECTORY_SEPARATOR;
		
		
		
		
		//Conexion FTP
		
		//Procesar Dias anteriores
		
		//Procesar dia actual
		
		
		//Pasar al directorio cargados servidor
		
		//Pasar al directorio descargados del servidor FTP
		
		
		//Verificar si los dos archivos estan Tipo A y Tipo I
		
			
	}
	
	private function preparar_archivo($idEtapaProceso){		
		$this->bandeEtapaProceTermi = false;
		
		$uriPlanoCargado = $this->rutaCargados . 'planillas' . DIRECTORY_SEPARATOR . 'cargados' . DIRECTORY_SEPARATOR;
		$uriPlanoProcesado = $this->rutaCargados . 'planillas' . DIRECTORY_SEPARATOR . 'procesados' . DIRECTORY_SEPARATOR;
		
		$arrArchivos = $this->objMaestroProceso->fetch_archivo_cargado($uriPlanoCargado);
		
		if(count($arrArchivos)==0){
			$descripcion = 'Error no se encontraron archivos para preparar';
			$this->arrError["error"] = 1;
			$this->arrError["descripcion"] = $descripcion;
				
			$this->guardar_log_db($this->idTrazaProceso,'ARCHIVO','A',$descripcion, null,'ERROR');
			return;
		}
			
		$banderaSizeFile = 0;
		$contArchivo = 0;
		
		foreach ($arrArchivos as $archivo){
		//for($i = 0, $len = count($arrArchivos); $i<$len; $i++){
			
			//$archivo = $arrArchivos[$i];
			
			$sizeArchivo = 0;
			//Datos generales del archivo
			$nombreArchivo = $archivo;
			
			$tipoArchivo = substr ( ( substr ( $nombreArchivo, 0, - 12 ) ), -1 );
			$fechaArchivo = substr ( $nombreArchivo, 0, 10 );
			$annoArchivo = substr ( $nombreArchivo, 0, 4 );
			$mesArchivo = substr ( $nombreArchivo, 5, 2 );
			$diaArchivo = substr ( $nombreArchivo, 8, 2 );
			
			//Directorios
			$uriPlanoCargaActua = $uriPlanoCargado . $nombreArchivo;
			$uriPlanoProceActua = $uriPlanoProcesado .  $annoArchivo . DIRECTORY_SEPARATOR . $mesArchivo . DIRECTORY_SEPARATOR . $diaArchivo . DIRECTORY_SEPARATOR;
			$uriPlanoHuerfActua = $uriPlanoProceActua . 'huerfanos' . DIRECTORY_SEPARATOR;
			
			//Verificar si los dos archivos existen [Tipo A - Tipo I] -- DEBE SER EN EL DIRECTORIO DE CARGADOS Y PROCESADOS
			//$tipoArchivoBandera = $tipoArchivo=="A"?"I":"A";
			$tipoArchivoBandera = $tipoArchivo=="I"?"I":"A";
			$nombreArchivoBandera = str_replace('_'.$tipoArchivo.'_','_'.$tipoArchivoBandera.'_',$nombreArchivo);
			
			//Si el archivo es de nomina manual no es necesario verificar los dos archivos
			$nominaManual = strpos($nombreArchivo,"Manual");
			
			if(		$nominaManual === false
					&& file_exists( $uriPlanoProceActua.$nombreArchivoBandera )===false
					&& file_exists( $uriPlanoCargado.$nombreArchivoBandera )===false){
				
				$this->objMaestroProceso->verifica_directorio( $uriPlanoHuerfActua );
				
				//Mover el archivo del directorio Cargados a huerfanos
				$this->objMaestroProceso->trasladar_archivo($uriPlanoCargaActua, $uriPlanoHuerfActua.$nombreArchivo);
				
				//El directorio no fue posible crearlo
				$descripcion = "Error El tipo de archivo $tipoArchivoBandera no existe";
				$data = '{"nombre_archivo":"'.$nombreArchivo.'"}';
				$this->guardar_log_db($this->idTrazaProceso,'DIRECTORIO','C',$descripcion, $data,'ERROR');
				continue;
			}
			
			
			//Obtener el tama�o del archivo
			$sizeArchivo = filesize($uriPlanoCargaActua);
			$banderaSizeFile += $sizeArchivo;
			$contArchivo++;
			//var_dump($banderaSizeFile);exit();
			//Cortar el proceso para no colgar la carga del servidor
			if($banderaSizeFile>3000000 || $contArchivo>15){
				break;
			}
			
			//Verificar directorio
			$bandeVerifDirec = $this->objMaestroProceso->verifica_directorio( $uriPlanoProceActua );
			
			if ($bandeVerifDirec !== true) {
				//El directorio no fue posible crearlo
				$descripcion = 'Error el directorio para procesar los archivos no fue posible crearlo';
				$this->arrError["error"] = 1;
				$this->arrError["descripcion"] = $descripcion;
					
				$data = '{"nombre_archivo":"'.$nombreArchivo.'"}';
				$this->guardar_log_db($this->idTrazaProceso,'DIRECTORIO','C',$descripcion, $data,'ERROR');
				break;
			}
			
			//Guardar los datos del archivo
			$idArchivo = $this->objMaestroProceso->guardar_archivo($nombreArchivo, $uriPlanoProceActua, $tipoArchivo, $sizeArchivo, 'S','PU');
			
			if($idArchivo==0){
				//El registro del archivo no se pudo guardar
				$descripcion = 'Error el registro del archivo no se pudo guardar';
				$this->arrError["error"] = 1;
				$this->arrError["descripcion"] = $descripcion;
				
				$data = '{"nombre_archivo":"'.$nombreArchivo.'"}';
				$this->guardar_log_db($this->idTrazaProceso,'ARCHIVO','C',$descripcion, $data,'ERROR');
				break;
			}

			//Obtener las lineas del archivo
			$arrLineaArchivo = file($uriPlanoCargaActua);
			
			$objPuTipoI1 = new PuTipoI();
			
			//Recorrer las lineas del archivo
			foreach($arrLineaArchivo as $lineaArchivo){
				
				if($tipoArchivo == "A"){								
					$objPuTipoA = new PuTipoA();
					
					$objPuTipoA->setIdProceso( $this->idProceso );
					$objPuTipoA->setIdEtapaProceso( $idEtapaProceso );
					$objPuTipoA->setIdArchivo( $idArchivo );
					$objPuTipoA->setError( 'N' );
					$objPuTipoA->setEstado( 'PROCESADO' );
					$objPuTipoA->setUsuario( $this->usuario );
					
					$objPuTipoA->setRazonSocial(utf8_encode(trim( substr($lineaArchivo,0,200))));
					$objPuTipoA->setTipoDocumEmpre(trim( substr($lineaArchivo,200,2)));
					$objPuTipoA->setNit(trim( substr($lineaArchivo,202,16)));
					$objPuTipoA->setDigitoVerifEmpre(trim( substr($lineaArchivo,218,1)));
					$objPuTipoA->setCodigoSucursal(trim( substr($lineaArchivo,219,10)));
					$objPuTipoA->setNombreSucursal(utf8_encode(trim( substr($lineaArchivo,229,40))));
					$objPuTipoA->setClaseAportante(trim( substr($lineaArchivo,269,1)));
					$objPuTipoA->setNaturalezaJuridica(trim( substr($lineaArchivo,270,1)));
					$objPuTipoA->setTipoPersona(trim( substr($lineaArchivo,271,1)));
					$objPuTipoA->setFormaPresentacion(trim( substr($lineaArchivo,272,1)));
					$objPuTipoA->setDireccionEmpresa(utf8_encode(trim( substr($lineaArchivo,273,40))));
					$objPuTipoA->setCodigCiudaEmpre(trim( substr($lineaArchivo,313,3)));
					$objPuTipoA->setCodigDeparEmpre(trim( substr($lineaArchivo,316,2)));
					$objPuTipoA->setActivDaneEmpre(trim( substr($lineaArchivo,318,4)));
					$objPuTipoA->setTelefonoEmpresa(trim( substr($lineaArchivo,322,10)));
					$objPuTipoA->setFaxEmpresa(trim( substr($lineaArchivo,332,10)));
					$objPuTipoA->setEmailEmpresa(trim( substr($lineaArchivo,342,60)));
					$objPuTipoA->setIdentificacionRepresentante(trim( substr($lineaArchivo,402,16)));
					$objPuTipoA->setDigitVerifRepre(trim( substr($lineaArchivo,418,1)));
					$objPuTipoA->setTipoDocumRepre(trim( substr($lineaArchivo,419,2)));
					$objPuTipoA->setPrimeApellRepre(utf8_encode(trim( substr($lineaArchivo,421,20))));
					$objPuTipoA->setSegunApellRepre(utf8_encode(trim( substr($lineaArchivo,441,30))));
					$objPuTipoA->setPrimeNombrRepre(utf8_encode(trim( substr($lineaArchivo,471,20))));
					$objPuTipoA->setSegunNombrRepre(utf8_encode(trim( substr($lineaArchivo,491,30))));
					$objPuTipoA->setFechaIniciConco(trim( substr($lineaArchivo,521,10)));
					$objPuTipoA->setTipoAccion(trim( substr($lineaArchivo,531,1)));
					$objPuTipoA->setFechaTerminacion(trim( substr($lineaArchivo,532,10)));
					$objPuTipoA->setCodigoOperador(trim( substr($lineaArchivo,542,2)));
					$objPuTipoA->setPeriodoPago(trim( substr($lineaArchivo,544,7)));
					$objPuTipoA->setTipoAportante(trim( substr($lineaArchivo,551,1)));
					$objPuTipoA->setFechaMatricula( null );
					$objPuTipoA->setCodigDeparRepre( null );
					$objPuTipoA->setEmpreExone1607( null );
					$objPuTipoA->setEmpreExone1429( null );
					
					$objPuTipoA->setTipoDocumEmpreDB( null ); 
					$objPuTipoA->setTipoDocumRepreDB( null ); 
					$objPuTipoA->setIdPersoRepreDB( null ); 
					$objPuTipoA->setIdEmpresaDB( null ); 
					$objPuTipoA->setNitDB( null ); 
					
					$resultado = $objPuTipoA->guardar();
					
					if($resultado==0){
						//Guardar el log de error
						//No se fue posible preparar la linea del archivo
						$descripcion = 'Error no fue posible preparar la linea del archivo TIPO A';
						$data = '{"nombre_archivo":"'.$nombreArchivo.'","tipo_identificacion":"'.$objPuTipoA->getTipoDocumEmpre().'","numero_identificacion":"'.$objPuTipoA->getNit().'","nombre":"'.$objPuTipoA->getRazonSocial().'"}';
						$this->guardar_log_db($this->idTrazaProceso,'EMPRESA','B',$descripcion, $data,'ERROR');
					}				
				}else if($tipoArchivo == "I"){
					
					$tipoRegistro = intval(trim(substr($lineaArchivo,5,1)));
					
					//TIPO 1
					if($tipoRegistro==1){
						$objPuTipoI1->setIdProceso( $this->idProceso );
						$objPuTipoI1->setIdEtapaProceso( $idEtapaProceso );
						$objPuTipoI1->setIdArchivo( $idArchivo );
						$objPuTipoI1->setError( 'N' );
						$objPuTipoI1->setEstado( 'PROCESADO' );
						$objPuTipoI1->setUsuario( $this->usuario );
						
						$objPuTipoI1->setNumerRegisTipo1(trim(substr($lineaArchivo,0,5)));
						$objPuTipoI1->setTipoRegisTipo1(trim(substr($lineaArchivo,5,1)));
						$objPuTipoI1->setCodigFormaTipo1(trim(substr($lineaArchivo,6,2)));
						$objPuTipoI1->setNumerIdentCCFTipo1(trim(substr($lineaArchivo,8,16)));
						$objPuTipoI1->setDigitVerifNitTipo1(trim(substr($lineaArchivo,24,1)));
						$objPuTipoI1->setRazonSociaTipo1(utf8_encode(trim(substr($lineaArchivo,25,200))));
						$objPuTipoI1->setTipoDocumEmpreTipo1(trim(substr($lineaArchivo,225,2)));
						$objPuTipoI1->setNumerIdentEmpreTipo1(trim(substr($lineaArchivo,227,16)));
						$objPuTipoI1->setDigitVerifEmpreTipo1(trim(substr($lineaArchivo,243,1)));
						$objPuTipoI1->setTipoEmpreTipo1(trim(substr($lineaArchivo,244,1)));
						$objPuTipoI1->setDirecEmpreTipo1(utf8_encode(trim(substr($lineaArchivo,245,40))));
						$objPuTipoI1->setCodigMunicTipo1(trim(substr($lineaArchivo,285,3)));
						$objPuTipoI1->setCodigDeparTipo1(trim(substr($lineaArchivo,288,2)));
						$objPuTipoI1->setTelefonoTipo1(trim(substr($lineaArchivo,290,10)));
						$objPuTipoI1->setFaxTipo1(trim(substr($lineaArchivo,300,10)));
						$objPuTipoI1->setCorreElectTipo1(trim(substr($lineaArchivo,310,60)));
						$objPuTipoI1->setPerioPagoTipo1(trim(substr($lineaArchivo,370,7)));
						$objPuTipoI1->setTipoPlaniTipo1(trim(substr($lineaArchivo,377,1)));
						$objPuTipoI1->setFechaPagoPlaniAsociTipo1(trim(substr($lineaArchivo,378,10)));
						$objPuTipoI1->setFechaPagoTipo1(trim(substr($lineaArchivo,388,10)));
						$objPuTipoI1->setNumerPlaniAsociTipo1(trim(substr($lineaArchivo,398,10)));
						$objPuTipoI1->setNumerPlaniTipo1(trim(substr($lineaArchivo,408,10)));
						$objPuTipoI1->setFormaPreseTipo1(trim(substr($lineaArchivo,418,1)));
						$objPuTipoI1->setCodigSucurTipo1(trim(substr($lineaArchivo,419,10)));
						$objPuTipoI1->setNombrSucurTipo1(utf8_encode(trim(substr($lineaArchivo,429,40))));
						$objPuTipoI1->setNumerTotalEmpleTipo1(trim(substr($lineaArchivo,469,5)));
						$objPuTipoI1->setNumerTotalAfiliTipo1(trim(substr($lineaArchivo,474,5)));
						$objPuTipoI1->setCodigOperaTipo1(trim(substr($lineaArchivo,479,2)));
						$objPuTipoI1->setModalPlaniTipo1(trim(substr($lineaArchivo,481,1)));
						$objPuTipoI1->setDiasMoraTipo1(trim(substr($lineaArchivo,482,4)));
						$objPuTipoI1->setNumerRegisSalidTipo2Tipo1(trim(substr($lineaArchivo,486,8)));
						$objPuTipoI1->setFechaMatriMercaDTipo1( null );
						$objPuTipoI1->setCodigDeparDTipo1( null );
						$objPuTipoI1->setEmpreExone1607Tipo1( null );
						$objPuTipoI1->setEmpreExone1429Tipo1( null );

					}else if($tipoRegistro==2){
						
						$objPuTipoI = new PuTipoI();
						
						$objPuTipoI->setIdProceso( $this->idProceso );
						$objPuTipoI->setIdEtapaProceso( $idEtapaProceso );
						$objPuTipoI->setIdArchivo( $idArchivo );
						$objPuTipoI->setError( 'N' );
						$objPuTipoI->setEstado( 'PROCESADO' );
						$objPuTipoI->setUsuario( $this->usuario );
						//TIPO 2
						$objPuTipoI->setSecuenciaTipo2(trim(substr($lineaArchivo,0,5)));
						$objPuTipoI->setTipoRegisTipo2(trim(substr($lineaArchivo,5,1)));
						$objPuTipoI->setTipoIdentCotizTipo2(trim(substr($lineaArchivo,6,2)));
						$objPuTipoI->setNumerIdentCotizTipo2(trim(substr($lineaArchivo,8,16)));
						$objPuTipoI->setTipoCotizTipo2(trim(substr($lineaArchivo,24,2)));
						$objPuTipoI->setSubtiCotizTipo2(trim(substr($lineaArchivo,26,2)));
						$objPuTipoI->setExtranjeroTipo2(trim(substr($lineaArchivo,28,1)));
						$objPuTipoI->setColomResidExterTipo2(trim(substr($lineaArchivo,29,1)));
						$objPuTipoI->setCodigDeparTipo2(trim(substr($lineaArchivo,30,2)));
						$objPuTipoI->setCodigMunicTipo2(trim(substr($lineaArchivo,32,3)));
						$objPuTipoI->setPrimeApellTipo2(utf8_encode(trim(substr($lineaArchivo,35,20))));
						$objPuTipoI->setSegunApellTipo2(utf8_encode(trim(substr($lineaArchivo,55,30))));
						$objPuTipoI->setPrimeNombrTipo2(utf8_encode(trim(substr($lineaArchivo,85,20))));
						$objPuTipoI->setSegunNombrTipo2(utf8_encode(trim(substr($lineaArchivo,105,30))));
						$objPuTipoI->setNovedIngreTipo2(trim(substr($lineaArchivo,135,1)));
						$objPuTipoI->setNovedRetirTipo2(trim(substr($lineaArchivo,136,1)));
						$objPuTipoI->setNovedVSPTipo2(trim(substr($lineaArchivo,137,1)));
						$objPuTipoI->setNovedVSTTipo2(trim(substr($lineaArchivo,138,1)));
						$objPuTipoI->setNovedSLNTipo2(trim(substr($lineaArchivo,139,1)));
						$objPuTipoI->setNovedIGETipo2(trim(substr($lineaArchivo,140,1)));
						$objPuTipoI->setNovedLMATipo2(trim(substr($lineaArchivo,141,1)));
						$objPuTipoI->setNovedVacacTipo2(trim(substr($lineaArchivo,142,1)));
						$objPuTipoI->setDiasIncapARLTipo2(trim(substr($lineaArchivo,143,2)));
						$objPuTipoI->setDiasCotizTipo2(trim(substr($lineaArchivo,145,2)));
						$objPuTipoI->setSalarBasicTipo2(trim(substr($lineaArchivo,147,9)));
						$objPuTipoI->setIngreBaseTipo2(trim(substr($lineaArchivo,156,9)));
						$objPuTipoI->setTarifaTipo2(trim(substr($lineaArchivo,165,7)));
						$objPuTipoI->setAportObligTipo2(trim(substr($lineaArchivo,172,9)));
						$objPuTipoI->setCorreccionTipo2(trim(substr($lineaArchivo,181,1)));
						$objPuTipoI->setSalarIntegTipo2(trim(substr($lineaArchivo,182,1)));
						$objPuTipoI->setExone1607Tipo2( null );
						
						
						$nombreCotizante = $objPuTipoI->getPrimeNombrTipo2().' '.$objPuTipoI->getSegunApellTipo2().' '.$objPuTipoI->getPrimeNombrTipo2().' '.$objPuTipoI->getSegunNombrTipo2();
						
						
						$resultado = $objPuTipoI->guardar_tipo2();
						if($resultado==0){
							//Guardar el log de error
							//No se fue posible preparar la linea del archivo
							$descripcion = 'Error no fue posible preparar la linea del archivo TIPO I';
							$data = '{"nombre_archivo":"'.$nombreArchivo.'","tipo_identificacion":"'.$objPuTipoI->getTipoIdentCotizTipo2().'","numero_identificacion":"'.$objPuTipoI->setNumerIdentCotizTipo2().'","nombre":"'.$nombreCotizante.'"}';
							$this->guardar_log_db($this->idTrazaProceso,'TRABAJADOR','B',$descripcion, $data,'ERROR');
						}
						
					}else if($tipoRegistro==3){
						//TIPO 3
						$regisTotal = intval(trim(substr($lineaArchivo,0,5)));
						if($regisTotal==31){
							$objPuTipoI1->setRegisTotalAportTipo3(trim(substr($lineaArchivo,0,5)));
							$objPuTipoI1->setTipoRegisATipo3(trim(substr($lineaArchivo,5,1)));
							$objPuTipoI1->setIngreBaseCotizTipo3(trim(substr($lineaArchivo,6,10)));
							$objPuTipoI1->setAportObligTipo3(trim(substr($lineaArchivo,16,10)));
						}else if($regisTotal==36){
							$objPuTipoI1->setRegisInterMoraTipo3(trim(substr($lineaArchivo,0,5)));
							$objPuTipoI1->setTipoRegisBTipo3(trim(substr($lineaArchivo,5,1)));
							$objPuTipoI1->setNumerDiasMoraTipo3(trim(substr($lineaArchivo,6,4)));
							$objPuTipoI1->setMoraAportTipo3(trim(substr($lineaArchivo,10,10)));
						}else if($regisTotal==39){
							$objPuTipoI1->setRegisTotalPagarTipo3(trim(substr($lineaArchivo,0,5)));
							$objPuTipoI1->setTipoRegisCTipo3(trim(substr($lineaArchivo,5,1)));
							$objPuTipoI1->setTotalAportTipo3(trim(substr($lineaArchivo,6,10)));
						}
					}
					
				}
			}
			
			//Update archivo tipo I de los registros tipo1 y tipo3
			if($tipoArchivo == "I"){
				$resultado = $objPuTipoI1->update();
				
				if($resultado==0){
					$descripcion = 'Error no fue posible preparar el tipo registro 1 y 3 del archivo TIPO I';
					$data = '{"nombre_archivo":"'.$nombreArchivo.'","tipo_identificacion":"'.$objPuTipoI1->getTipoDocumEmpreTipo1().'","numero_identificacion":"'.$objPuTipoI1->getNumerIdentEmpreTipo1().'","nombre":"'.$objPuTipoI1->getRazonSociaTipo1().'"}';
					$this->guardar_log_db($this->idTrazaProceso,'TRABAJADOR','B',$descripcion, $data,'ERROR');
				}
			}
			
			//Mover el archivo del directorio Cargados a Procesados
			$this->objMaestroProceso->trasladar_archivo($uriPlanoCargaActua, $uriPlanoProceActua . $nombreArchivo);					
		}
		
		//Comprobar si existen archivos en el directorio cargado
		$banderaArchivo = count($this->objMaestroProceso->fetch_archivo_cargado($uriPlanoCargado));
		if($this->arrError["error"]==0 && $banderaArchivo==0){
			
			//Verificar si existen archivos huerfanos
			if(!file_exists($uriPlanoHuerfActua) || (file_exists($uriPlanoHuerfActua) && count($this->objMaestroProceso->fetch_archivo_cargado($uriPlanoHuerfActua))==0)){
				$descripcion = 'Los archivos se prepararon correctamente';
				$this->arrError["error"] = 0;
			}else{
				$descripcion = 'Existen algunas inconsistencias. Pero las etapa proceso termino correctamente';
				$this->arrError["error"] = 1;
			}
			
			$this->arrError["descripcion"] = $descripcion;
			$this->bandeEtapaProceTermi = true;
			
			$this->guardar_log_db($this->idTrazaProceso,'ARCHIVO','A',$descripcion, null,'EXITO');
		}
	}
	
	private function procesar_dato_sigas(){		
		$this->bandeEtapaProceTermi = false;
		
		$objPuTipoA = new PuTipoA();
		$objPuTipoA->setIdProceso($this->idProceso);
		$objPuTipoA->setUsuario($this->usuario);
		
		$objPuTipoI = new PuTipoI();
		$objPuTipoI->setIdProceso($this->idProceso);
		$objPuTipoI->setUsuario($this->usuario);
		
		$estadoProceso = $objPuTipoA->procesar_datos();
		if($estadoProceso=='FIN_EXITOSO'){
			
			//Obtener la cantidad de PU Insertadas
			$cantidadPuInsertadas = $objPuTipoI->count_up_insert();
			
			$descripcion = 'Los datos se procesaron correctamente en la DB. Cant PU Insertas: ('.$cantidadPuInsertadas.')';
			$this->arrError["error"] = 0;
			$this->arrError["descripcion"] = $descripcion;
			$this->bandeEtapaProceTermi = true;
			
			$this->guardar_log_db($this->idTrazaProceso,'DATOS_SIGAS','A',$descripcion, null,'EXITO');
			
		}else if($estadoProceso=='FIN_ERROR'){
			
			//Obtener la cantidad de PU Insertadas
			$cantidadPuInsertadas = $objPuTipoI->count_up_insert();
			
			$descripcion = 'Existen algunas inconsistencias. Pero las etapa proceso termino correctamente. Cant PU Insertas: ('.$cantidadPuInsertadas.')';
			$this->arrError["error"] = 1;
			$this->arrError["descripcion"] = $descripcion;
			$this->bandeEtapaProceTermi = true;
				
			$this->guardar_log_db($this->idTrazaProceso,'DATOS_SIGAS','A',$descripcion, null,'EXITO');
			
		}else if($estadoProceso!='PROCESO'){
			//Error al ejecutar el proceso
			
			$descripcion = 'Error al procesar los datos en sigas';
			$this->arrError["error"] = 1;
			$this->arrError["descripcion"] = $descripcion;
			$this->guardar_log_db($this->idTrazaProceso,'PROCESAR_DATOS_SIGAS','A',$descripcion, null,'ERROR');
		}
	}
	
	private function procesar_dato_service(){
		//$this->bandeEtapaProceTermi = true;
		//return;
		
		
		$this->bandeEtapaProceTermi = false;
		
		
		$objLogProceso = new LogProceso();
		$objLogProceso->setUsuario($this->usuario);
		$objServicePu = new ServicePu();
		
		$objServicePu->setIdProceso($this->idProceso);
		$objServicePu->setIdTrazaProceso($this->idTrazaProceso);
		$objServicePu->setUsuario($this->usuario);
		$objServicePu->setObjPuTipoA(new PuTipoA());
		$objServicePu->setObjPuTipoI(new PuTipoI());
		$objServicePu->setObjLogProceso($objLogProceso);
		
		$estadoProceso = $objServicePu->procesar_datos();
		
		if($estadoProceso=='FIN_EXITOSO'){
			$descripcion = 'Los datos se procesaron correctamente en la Interfaz';
			$this->arrError["error"] = 0;
			$this->arrError["descripcion"] = $descripcion;
			$this->bandeEtapaProceTermi = true;
				
			$this->guardar_log_db($this->idTrazaProceso,'DATOS_INTERFAZ','A',$descripcion, null,'EXITO');
				
		}else if($estadoProceso=='FIN_ERROR'){
			$descripcion = 'Existen algunas inconsistencias. Pero las etapa proceso termino correctamente';
			$this->arrError["error"] = 1;
			$this->arrError["descripcion"] = $descripcion;
			$this->bandeEtapaProceTermi = true;
			
			$this->guardar_log_db($this->idTrazaProceso,'DATOS_INTERFAZ','A',$descripcion, null,'EXITO');
			
		}else if($estadoProceso!='PROCESO'){
			
			$descripcion = 'Error al procesar los datos en la Interfaz';
			$this->arrError["error"] = 1;
			$this->arrError["descripcion"] = $descripcion;
				
			//Error al ejecutar el proceso
			$this->guardar_log_db($this->idTrazaProceso,'PROCESAR_DATOS_INTERFAZ','A',$descripcion, null,'ERROR');
		}
	}
	
	private function log_proceso(){
		$this->bandeEtapaProceTermi = true;
		return;
		
		/*$this->bandeEtapaProceTermi = true;
		return;
		
		$resultado = $this->objMaestroProceso->guardar_logs_archivo();
		if($resultado==1){
			$this->arrError["error"] = 0;
			$this->arrError["descripcion"] = "Los log se crearon correctamente";
			$this->bandeEtapaProceTermi = true;
		}else{
			$this->arrError["error"] = 1;
			$this->arrError["descripcion"] = "Error al crear los logs";
		}*/		
	}
	
	private function guardar_log_db($idTrazaProceso, $codigo,$tipoLog,$descripcion, $data = null,$estado){
		$this->objLogProceso->guardar_log_db($idTrazaProceso,$codigo,$tipoLog,$descripcion,$data,$estado);
	}
	
	private function ejecutar_sp_resetear_pu(){
		//Remover estado de error para las etapas con error
			
		$resultado = 0;
		$sentencia = self::$con->conexionID->prepare ( "EXEC [pu].[sp_Resetear_Proceso_PU]
				@id_proceso = $this->idProceso
				, @usuario = '$this->usuario'
				, @resultado = :resultado" );
		$sentencia->bindParam ( ":resultado", $resultado, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000 );
		$sentencia->execute ();
			
		if($resultado==0){			
			return false;
		}
		return true;
	}
}