<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>::Modificar Planilla::</title>

<link type="text/css" href="<?php echo URL_PORTAL; ?>css/Estilos.css" rel="stylesheet"/>
<link href="../../css/marco.css" rel="stylesheet" type="text/css" />
<link type="text/css" href="<?php echo URL_PORTAL; ?>css/estilo_tablas.css" rel="stylesheet"/>
<link type="text/css" rel="stylesheet" href="../../css/css.1.8/jquery-ui-1.8.17.custom.css" />
<script language="javascript" src="<?php echo URL_PORTAL; ?>js/1.6/jquery-1.6.2.min.js"></script>
<script language="javascript" src="<?php echo URL_PORTAL; ?>js/1.6/jquery-ui-1.8.16.custom.min.js"></script>

<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/comunes.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/jquery.combos.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/jquery.fechaLarga.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/llenarCampos.js"></script>
<script language="javascript" type="text/javascript" src="<?php echo URL_PORTAL; ?>js/direccion.js"></script>
<script type="text/javascript" src="js2/modificarPlanilla.js"></script>
<script type="text/javascript" src="js2/planillaTab.js"></script>

<!-- script acceso directo a consultar trabajador -->
<script type="text/javascript">

shortcut.add("Shift+F",function() {
		                                var URL=src();
		var url=URL+"aportes/trabajadores/consultaTrabajador.php";
    	window.open(url,"_blank");
    },{
	'propagate' : true,
	'target' : document 
        });        
</script>
<script language="javascript">
function mostrarAyuda(){
	$("#ayuda").dialog('open' );
	}
function notas(){
	$("#dialog-form2").dialog('open');
	}	
</script>
<!-- estilo -->
<style type="text/css">
#planillas p{ cursor:pointer; color:#333; width:600px;}
#planillas p:hover{color:#000}
#accordion h3,div{ padding:0px;}
#accordion span.plus{ background:url(<?php echo URL_PORTAL; ?>imagenes/plus.png) no-repeat right center; margin-left:95%;}
#accordion span.minus{background:url(<?php echo URL_PORTAL; ?>imagenes/minus.png) no-repeat right center;  margin-left:95%}
div#wrapTable{padding:0px; margin-bottom:-2px}
div#icon{width:50px; margin:auto;margin-first:5px;padding:0 5px; text-align:center;display:none; cursor:pointer;}
div#icon span{ background:url(<?php echo URL_PORTAL; ?>imagenes/show.png) no-repeat center; padding:10px;}
div#icon span.toggleIcon{ background:url(<?php echo URL_PORTAL; ?>imagenes/hide.png) no-repeat center; padding:10px;}
</style>
</head>

<body>
<div id="wrapTable">
<table width="97%" border="0" cellspacing="0" cellpadding="0" align="center">
<tr>
<td width="13" height="29" class="arriba_iz">&nbsp;</td>
<td class="arriba_ce"><span class="letrablanca">::&nbsp;Modificaci&oacute;n de Planilla&nbsp;::</span></td>
<td width="13" class="arriba_de" >&nbsp;</td>
</tr>    
<tr>     
<td class="cuerpo_iz">&nbsp;</td>
<td class="cuerpo_ce">
</td>
<td class="cuerpo_de">&nbsp;</td>
</tr>
<tr>
<td class="cuerpo_iz">&nbsp;</td>
<!-- TABLAS DE FORMULARIO -->
<td class="cuerpo_ce">
<center>
	<table width="90%" border="0" cellspacing="0" class="tablero">
		<tr>
			<td width="120">NIT:</td>
			<td width="240">
				<input name="nit" type="text" class="box" id="nit"  />
			</td width="240">
			<td width="80">Periodo:</td>
			<td>
				<input name="periodo" type="text" class="box" id="periodo"  />
			</td>
			<td></td>
		</tr>
		<tr>
        	<td width="120">Tipo de Documento:</td>              
            <td width="319">
            	<select id="tipoDocumento" name="tipoDocumento" class="box1">
					<option value="1" selected="selected">C&eacute;dula ciudadanía</option>
              		<option value="2">Tarjeta de Identidad</option>
              		<option value="3">Pasaporte</option>
              		<option value="4">C&eacute;dula extranjer&iacute;a</option>  
              		<option value="6">Registro Civil</option> 
              		<option value="4562">CARNET DIPLOMATICO</option>            
              	</select>
           </td>
           <td width="80">Documento:</td>
           <td>              
              <input name="idT" type="text" class="box" id="idT"  />
           </td>
           <td width="500">
              <input name="buscarP" type="button" class="ui-state-default" id="buscarP" value="Buscar" />
              <span class="Rojo"></span>
           </td>
       </tr>            
    </table>
</center>
<div id="planillas" align="center"> </div>
<td class="cuerpo_de"></td><!-- FONDO DERECHA -->
<tr>
<td height="41" class="abajo_iz">&nbsp;</td>
<td class="abajo_ce"></td>
<td class="abajo_de">&nbsp;</td>
</tr>
</table>
</div>
<div id="icon"><span></span></div>

  <!-- div para la imagen ampliada -->
<div id="imagen" title="DOCUMENTOS"></div>
 
<div id="tabsA" style="display:none;">
 <ul>
    <li><a href="#tabs-1" id="a0">Planilla</a></li>
    <li style="display:none"><a href='#tabs-2' id='a1'></a></li>    
 </ul>
	
  <div id="tabs-1"></div>
  <div id="tabs-2"></div>    
 </div>
 
</table>

</body>
<script>
$("#nit").focus();
</script>
</html>