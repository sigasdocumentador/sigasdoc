<?php
set_time_limit(0);
ini_set ( "display_errors", '1' );
date_default_timezone_set ( "America/Bogota" );

$root = $_SERVER ['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once $root;
include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'funcionesComunes' . DIRECTORY_SEPARATOR . 'funcionesComunes.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'rsc' . DIRECTORY_SEPARATOR . 'pdo' . DIRECTORY_SEPARATOR . 'IFXDbManejador.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'rsc' . DIRECTORY_SEPARATOR . 'pdo' . DIRECTORY_SEPARATOR . 'IFXerror.php';
$db = IFXDbManejador::conectarDB ();
if ($db->conexionID == null) {
	$cadena = $db->error;
	echo -2;
	exit ();
}
$c0=$_REQUEST["v0"];
$tipo=$_REQUEST["v1"];


$directorio = $ruta_cargados . "dian".DIRECTORY_SEPARATOR."cargados".DIRECTORY_SEPARATOR;
$dir = opendir ( $directorio );
while ( $elemento = readdir ( $dir ) ) {
	if (strlen ( $elemento ) > 2)
		$archivos [] = $elemento;
}
closedir ( $dir );
$directorioProcesado = $ruta_cargados . "dian/procesados/";
if (!file_exists($directorioProcesado)) {
	mkdir($directorioProcesado, 0777, true);
}
$archivo='reporte.csv';
$rutadelplano=$directorioProcesado.DIRECTORY_SEPARATOR.$archivo;
if($tipo==1){
$cadena = "IDENTIFICACION;NOMBRETRABAJADOR;BENEFICIARIOS;IDENTIFICACION BENEFICIARIO;ESTADO_BENEFICIARIO;PARENTESCO;ESTADO_AFILIADO;FECHA INGRESO;FECHA RETIRO;SALARIO;TIPO_FORMULARIO;DIRECCION;MUNICIPIO;TELEFONO;EMAIL;NIT EMPRESA;NOMBRE EMPRESA;DIRECCION EMPRESA;TELEFONO EMPRESA;FECHA_RADICADO\r\n";
}else{
	$cadena = "NIT;NOMBRE EMPRESA;ESTADO;FECHA AFILIACION;FECHA ESTADO;DIRECCION;DIRECCION CORRESPONDENCIA;TELEFONO;MUNICIPIO;MUNICIPIO CORRESPONDENCIA;REPRESENTANTE;IDENTIFICACION REPRESENTANTE\r\n";	
}
$fp=fopen($rutadelplano,'w');
fwrite($fp, $cadena);
fclose($fp);

$i=0;
$fp=fopen($rutadelplano,'a');
for(; $i<count($archivos); $i++){
	if($archivos[$i]==$c0){
		$directorio .= $archivos[$i];
		$archivoSubido = file ( $directorio );
		$totalLineas = count ( $archivoSubido );
		$cont=0;	
		for($c = 0; $c < $totalLineas; $c ++) {
			$cont++;
			$linea = $archivoSubido [$c];
			$documento = explode ( ' ', $linea );
			$num=trim($documento[0]);
			buscar($num,$db,$fp,$tipo);
		}		
		break;
	}
}
fclose($fp);

if($cont==0){
	echo -1;
	exit();
}

unlink($directorio);

$_SESSION['ENLACE']=$rutadelplano;
$_SESSION['ARCHIVO']=$archivo;
echo 1;

function buscar($num,$db,$fp,$tipo){
	
	if($tipo==1){
	$sql="SELECT DISTINCT a15.identificacion,a15.pnombre+' '+isnull(a15.snombre,'')+' '+a15.papellido+' '+isnull(a15.sapellido,'') as trabajador
			,b15.identificacion AS identificacionbene
			,b15.pnombre+' '+isnull(b15.snombre,'')+' '+b15.papellido+' '+isnull(b15.sapellido,'') AS beneficiario
			,isnull(a91.detalledefinicion,'') AS detalledefinicion
			,CASE WHEN
			 a16.estado='P' THEN (isnull((SELECT TOP 1 'PU' FROM aportes010 b10 
			 WHERE b10.idtrabajador=a16.idpersona AND b10.idempresa=a16.idempresa AND b10.fechasistema=a16.fechasistema 
			 AND (a16.idradicacion IS NULL OR 0=isnull((  SELECT count(*) cuenta FROM aportes004 b4 
			 WHERE b4.idradicacion=a16.idradicacion AND b4.numero=b10.identificacion AND b4.nit=b10.nit 
			 AND (b4.procesado='N' OR b4.procesado IS NULL) 
			 AND (b4.idtiporadicacion=28 or b4.idtiporadicacion=2926 or b4.idtiporadicacion=29)), 0))), 'P')) ELSE a16.estado END estado
			,a16.fechaingreso,null as fecharetiro,a16.salario
			,CASE WHEN a16.tipoformulario=48 THEN 'SUBSIDIO EN DINERO' ELSE 'SERVICIOS' END tipoformulario
			,a15.direccion,a89.municipio,a15.telefono,a15.email,a48.nit,a48.razonsocial,a48.direccion,a48.telefono,a21.estado as estadoBene,
			a4.fecharadicacion
			FROM aportes015 a15
			LEFT JOIN aportes021 a21 ON a21.idtrabajador=a15.idpersona
			LEFT JOIN aportes015 b15 ON b15.idpersona=a21.idbeneficiario
			LEFT JOIN aportes091 a91 ON a91.iddetalledef=a21.idparentesco
			INNER JOIN aportes016 a16 ON a15.idpersona=a16.idpersona
			INNER JOIN aportes048 a48 ON a16.idempresa=a48.idempresa
			LEFT JOIN aportes089 a89 ON a89.codmunicipio=a15.idciuresidencia
			LEFT JOIN aportes004 a4 on a4.idradicacion=a16.idradicacion
			WHERE 
			a16.fechaingreso=(SELECT max(a016.fechaingreso) FROM aportes016 a016 WHERE a016.idpersona=a16.idpersona)
			AND a15.identificacion='$num'
			UNION			
			SELECT DISTINCT a15.identificacion,a15.pnombre+' '+isnull(a15.snombre,'')+' '+a15.papellido+' '+isnull(a15.sapellido,'') as trabajador
			,b15.identificacion AS identificacionbene
			,b15.pnombre+' '+isnull(b15.snombre,'')+' '+b15.papellido+' '+isnull(b15.sapellido,'') AS beneficiario
			,isnull(a91.detalledefinicion,'') AS detalledefinicion
			,'I' AS estado,a17.fechaingreso,a17.fecharetiro,a17.salario
			,CASE WHEN a17.tipoformulario=48 THEN 'SUBSIDIO EN DINERO' ELSE 'SERVICIOS' END tipoformulario
			,a15.direccion,a89.municipio,a15.telefono,a15.email,a48.nit,a48.razonsocial,a48.direccion,a48.telefono,a21.estado as estadoBene,
			a4.fecharadicacion
			FROM aportes015 a15
			LEFT JOIN aportes021 a21 ON a21.idtrabajador=a15.idpersona
			LEFT JOIN aportes015 b15 ON b15.idpersona=a21.idbeneficiario
			LEFT JOIN aportes091 a91 ON a91.iddetalledef=a21.idparentesco
			INNER JOIN aportes017 a17 ON a15.idpersona=a17.idpersona
			INNER JOIN aportes048 a48 ON a17.idempresa=a48.idempresa
			LEFT JOIN aportes089 a89 ON a89.codmunicipio=a15.idciuresidencia
			LEFT JOIN aportes004 a4 on a4.idradicacion=a17.idradicacion
			WHERE 0=(SELECT count(*) FROM aportes016 a16 WHERE a16.idpersona=a17.idpersona) AND
			    a17.fecharetiro=(SELECT max(a16.fecharetiro) FROM aportes017 a16 WHERE a16.idpersona=a17.idpersona)
			AND a15.identificacion='$num'";
	
	$rs=$db->querySimple($sql);
	}//Fin condicion
	else{
		$sql="SELECT DISTINCT a48.nit,a48.razonsocial,a48.estado,a48.fechaafiliacion
				,a48.fechaestado,a48.direccion,a48.direcorresp,a48.telefono,a89.municipio AS municorresp,a089.municipio
				,a15.pnombre+' '+isnull(a15.snombre,'')+' '+a15.papellido+' '+isnull(a15.sapellido,'') as representante
				,a15.identificacion
				FROM aportes048 a48
				LEFT JOIN aportes089 a089 ON a089.codmunicipio=a48.idciudad
				LEFT JOIN aportes089 a89 ON a89.codmunicipio=a48.idciucorresp
				INNER JOIN aportes015 a15 ON a15.idpersona=a48.idrepresentante
				WHERE a48.nit='$num'";

		}//Fin condicion else
		
		$cont=0;
		$rs=$db->querySimple($sql);
		while($row=$rs->fetch()){
			escribirPlano($row,$fp,$tipo);
			$cont++;
		}
		
		if($cont==0){
			if($tipo==1)
			{
			   $cade=$num.";  el trabajador no existe en nuestra base de datos;\r\n";
			}
			else
			{
				$cade=$num."; la empresa no existe en nuestra base de datos;\r\n";
			}
			
			fwrite($fp, $cade);
		}
	
}

function escribirPlano($row,$fp,$tipo){
	$cadena = "";
	if($tipo==1){	
		$cadena = "$row[identificacion];$row[trabajador];$row[beneficiario];$row[identificacionbene];$row[estadoBene];$row[detalledefinicion];$row[estado];$row[fechaingreso];$row[fecharetiro];$row[salario];$row[tipoformulario];$row[direccion];$row[municipio];$row[telefono];$row[email];$row[nit];$row[razonsocial];$row[direccion];$row[telefono];$row[fecharadicacion]"."\r\n";
	}else{
		$cadena = "$row[nit];$row[razonsocial];$row[estado];$row[fechaafiliacion];$row[fechaestado];$row[direccion];$row[direcorresp];$row[telefono];$row[municipio];$row[municorresp];$row[representante];$row[identificacion]"."\r\n";
	}
	fwrite($fp, $cadena);
}
?>
