/** VARIABLES LOCALES **/
var folios=0;
var idPersonaLocal=0;

$(document).ready(function(){
	$("#cmbIdTipoDocumentoAfiliado,#cmbIdTipoDocumentoConyuge").val(copyTipoDoc);
	$("#txtNumero").val(copyDoc);
	
	actDatepickerPeriodoImgs($("#txtPeriodoInicial"));
	actDatepickerPeriodoImgs($("#txtPeriodoFinal"));
	
	setTimeout("$('#tabs').trigger('click');$('#cmbIdTipoDocumentoAfiliado').focus();",700);
});

/**
 * Busca la persona a afiliar
 * 
 * @param cmbtd 		combo que contiene el tipo de documento
 * @param txtd 			text que contiene el documento
 * @param tdnom 		td que contiene el nombre del afiliado
 * @returns {Boolean}
 */
function busquedaPersona(cmbtd, txtd, tdnom){
	idPersonaLocal=0;
	idEmpresaLocal=0;
	tdnom.html('');	
	removeTabs(1);
	eliminarDivsBasura();
	
	var persona=buscarPersona(cmbtd, txtd, tdnom, false);
	if(persona==null || persona==false){ 
		txtd.val('');
		txtd.addClass("ui-state-error"); 
		return false; 
	}
	
	idPersonaLocal=persona.idpersona;
	
	crearTabsInformacion(idPersonaLocal, 0);
	ocultarDivs();
}

/**
 * Guardar la Radicacion
 */
function saveRadicacion(){
	if(iniciarconN()){
		if(validarTodosLosCampos()){		
			var nReclamoTrabajador=new ReclamoTrabajador();
			nReclamoTrabajador=llenarLasVariables(nReclamoTrabajador);
			
			if(nReclamoTrabajador==null){
				nRadicacion=null;
				alert("Se encontr\u00F3 errores en el procesamiento de su solicitud.");				
				return false;
			} else {	
				nRadicacion.idradicacion=guardarRadicacion(nRadicacion);
				if(esNumeroRespuesta(nRadicacion.idradicacion)){
					nuevaRad=0;
					$("#txtHoraFin").val(nRadicacion.horafinal);
					nReclamoTrabajador.idreclamo=guardarReclamoTrabajador(nReclamoTrabajador);
					
					if(esNumeroRespuesta(nReclamoTrabajador.idreclamo)){
						alert("Radicaci\u00F3n y RECLAMO guardados correctamente!! \nRADICACI\u00D3N # "+nRadicacion.idradicacion);
						$("#titRadicacion").trigger('click');
						$("#txtIdRadicacion").css({ color: "RED"});
						$("#txtIdRadicacion").val(nRadicacion.idradicacion);
					} else {
						if(anularRadicacion(nRadicacion.idradicacion)){
							alert("Ocurrio un error al guardar el reclamo!!" +
									"\nPOR FAVOR vuelva a intentarlo");
						} else {
							alert("Ocurrio un error al anular la radicacion!!" +
									"\nPOR FAVOR reporte este numero a soporte:" +
									"\# "+nRadicacion.idradicacion);
						}						
						return false;
					}					
				} else {
					alert("Radicaci\u00F3n NO FUE GUARDADA!!");
					return false;
				}
			}
		}
	}
}

/**
 * Valida todos los campos necesarios para la radicacion 
 */
function validarTodosLosCampos(){
	var error=validarCampos();
	
	error=validarSelect($("#cmbIdTipoDocumentoAfiliado"),error);
	if(esNumeroRespuesta(idPersonaLocal)==false){ $("#txtNumero").val(''); }
	error=validarTexto($("#txtNumero"),error);
	error=validarSelect($("#cmbIdCausal"),error);
	error=validarTexto($("#txtPeriodoInicial"),error);
	error=validarTexto($("#txtPeriodoFinal"),error);
	
	if(error>0){		
		alert("Llene los campos obligatorios!");
		nRadicacion=null;
		return false;
	} else {
		return true;
	}
}

/**
 * LLena las variables necesarias para la radicacion
 */
function llenarLasVariables(nReclamoTrabajador){
	var $reclamoTrabajador=$("#cmbIdTipoRadicacion").val();
	nRadicacion.idtipodocumentoafiliado=$("#cmbIdTipoDocumentoAfiliado").val();
	nRadicacion.numero=$("#txtNumero").val();
	nRadicacion.notas=$.trim($("#txtNotas").val());
	
	nReclamoTrabajador=camposReclamoTrabajador(nReclamoTrabajador);
	nReclamoTrabajador.idtrabajador=idPersonaLocal;
	nReclamoTrabajador.periodoinicial=$("#txtPeriodoInicial").val();
	nReclamoTrabajador.periodofinal=$("#txtPeriodoFinal").val();
	nReclamoTrabajador.estado="A";
	nReclamoTrabajador.idcausal=$("#cmbIdCausal").val();
	nReclamoTrabajador.notas=$.trim($("#txtNotas").val());
	if($reclamoTrabajador==4230){
		nReclamoTrabajador.pqr="S";
		}else{
		nReclamoTrabajador.pqr="N";
		}	
	return nReclamoTrabajador;
}

/**
 * Activar Rango Periodos
 */ 
function actDatepickerPeriodoImgs(objeto){
	objeto.datepicker({
		dateFormat: 'yymm',
		changeMonth: true,
		changeYear: true,
		showButtonPanel: true,
		maxDate: "+0D",
		yearRange: "-100:+0",
		onClose: function(dateText, inst) {
			var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
			var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
			$(this).datepicker('setDate', new Date(year, month, 1));
			validarDatepickerPeriodoImgs();
		} 
	});
	objeto.focus(function () {
		$(".ui-datepicker-calendar").hide();
		$("#ui-datepicker-div").position({
			my: "center top",
			at: "center bottom",
			of: $(this)
		});
	}); 
}

/**
 * Validar fecha inicial menor o igual a fecha final
 */
function validarDatepickerPeriodoImgs(){
	$("#txtPeriodoFinal").removeClass("ui-state-error");
	var fechaIni=$("#txtPeriodoInicial").val();
	var fechaFin=$("#txtPeriodoFinal").val();
	
	if(fechaIni != "" && fechaFin != ""){
		var anioIni=fechaIni.slice(0,4);
		var mesIni=eval(fechaIni.slice(-2));
		var anioFin=fechaFin.slice(0,4);
		var mesFin=eval(fechaFin.slice(-2));
		
		if((anioFin<anioIni) || (anioFin==anioIni && mesFin<mesIni)){
			$("#txtPeriodoFinal").val('');
			$("#txtPeriodoFinal").addClass("ui-state-error");
			$("#txtNotas").focus();
		}
	}
}