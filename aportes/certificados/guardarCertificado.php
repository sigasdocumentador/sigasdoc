<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
include_once $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'config.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';

$idTrabajador = (empty($_REQUEST['idTrab'])) ? 'NULL' : $_REQUEST['idTrab'];	//id persona
$idBeneficiario = (empty($_REQUEST['idBenef'])) ? 'NULL' : $_REQUEST['idBenef'];	//id beneficiario
$idParentesco = (empty($_REQUEST['idParent'])) ? 'NULL' : $_REQUEST['idParent'];	// tipo parentesco
$idTipoCerti = (empty($_REQUEST['idTipoCert'])) ? 'NULL' : $_REQUEST['idTipoCert'];	//id tipo cert
$periodoI = (empty($_REQUEST['perIni'])) ? 'NULL' : $_REQUEST['perIni'];	// periodo inicia
$periodoF = (empty($_REQUEST['perFin'])) ? 'NULL' : $_REQUEST['perFin'];	// periodo finaliza
$fechaPresentacion = (empty($_REQUEST['fechaPresentacion']))?'NULL':"'".$_REQUEST['fechaPresentacion'] ."'";
$formaPresenta = (empty($_REQUEST['formaPresentacion']))?'P':"'".$_REQUEST['formaPresentacion'] ."'";

$respuesta = array("codigo" => 0, "mensaje" => "");

$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

if($fechaPresentacion == null || $fechaPresentacion == '')
	$fechaPresentacion = "cast(getdate() as date)";

/*
if($idTipoCerti == 57){
	$sql = "SELECT count(*) as cantidad FROM aportes026 WHERE idbeneficiario = $idBeneficiario AND idtipocertificado = 57";
	$rs = $db->querySimple($sql);
	$cuenta = $rs->fetch();
	if(intval($cuenta["cantidad"])>0){
		$respuesta["codigo"] = 2;
		$respuesta["mensaje"] = "El beneficiario ya tiene certificado de supervivencia";
		print_r(json_encode($respuesta));
		die();
	}	
}
*/	

// primero se inactivan los certificados de estudio activos
// primero se inactivan los certificados anteriores sean del tipo que sea
//if($idTipoCerti == 55 || $idTipoCerti == 56){
	$sql="UPDATE aportes026 SET estado = 'I' WHERE idbeneficiario = $idBeneficiario --AND idtipocertificado in (55,56) AND estado='A'";
	$rs=$db->queryActualiza($sql,'aportes026');
	if(is_null($rs)){
		$cadena = $db->error;
		$msg="Error en la tabla aportes026: ";
		$msg.=msg_error($cadena);
		$respuesta["codigo"] = 0;
		$respuesta["mensaje"] = $msg;
		print_r(json_encode($respuesta));
		die();
	}
//}

$sql=$sql="Insert into aportes026 (idbeneficiario, idparentesco, idtipocertificado, periodoinicio, periodofinal, fechapresentacion, formapresentacion, estado, usuario, fechasistema) values ($idBeneficiario,$idParentesco,$idTipoCerti,'$periodoI','$periodoF', $fechaPresentacion, '$formaPresenta','A','".$_SESSION["USUARIO"]."',cast(getdate() as date))";
$rs=$db->queryInsert($sql, 'aportes026');
if($rs==0){
	$cadena = $db->error;
	$msg="Error en la tabla aportes026: ";
	$msg.=msg_error($cadena);
	$respuesta["codigo"] = 0;
	$respuesta["mensaje"] = $msg;
	print_r(json_encode($respuesta));
	die();
}
$respuesta["codigo"] = 1;
$respuesta["mensaje"] = "Certificado grabado exitosamente.";
print_r(json_encode($respuesta));
die();
?>