<?php
set_time_limit(0);
date_default_timezone_set('America/Bogota');
ini_set("display_errors",'1');

$root= $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Traspaso Tarjeta</title>
<link href="../css/jquery-ui-1.9.2.custom.min.css" type="text/css" rel="stylesheet"  />
<link href="../css/estilo.css" type="text/css" rel="stylesheet"  />
<script  src="../js/jquery-1.8.3.js"></script>
<script  src="../js/jquery-ui-1.9.2.custom.min.js"></script>
<script  src="../js/funciones.js"></script>
<script  src="js/traspasoTarjeta.js"></script>
<script>
$(function() {
     $( "#tabs" ).tabs({
     beforeLoad: function( event, ui ) {
     ui.jqXHR.error(function() {
     ui.panel.html(
     "No se pudo cargar esta pestaña. Vamos a tratar de solucionar este problema lo antes posible. Si esto no sería una demostración." );
     });
     }
     });
   });
</script>
</head>
<body>
<form id="forma">
<center>
<div id="tabs" style="width:90%">
<ul>
  <li><a href="#tabs-1">Afiliado</a></li>
  <!--<li><a href="grupo.php">Grupo Familiar</a></li>
  <li><a href="cuotaMonetaria.php">Cuota Monetaria</a></li>
  <li><a href="planilla.php">Planilla Unica</a></li>
  <li><a href="aportes.php">Aportes</a></li>-->
</ul>
<div id="tabs-1">
    <table width="90%" border="0" cellspacing="0" cellpadding="0">
  	<tr>
    <td width="13" height="29" background="../imagenes/tablero/arriba_izq.gif">&nbsp;</td>
    <td background="../imagenes/tablero/arriba_central2.gif">
    <span class="letrablanca">::Traspasos &nbsp;::</span></td>
    <td width="13" background="../imagenes/tablero/arriba_der.gif" align="right">&nbsp;</td>
  	</tr>
  	<tr>
    <td background="../imagenes/tablero/izquierda2.jpg">&nbsp;</td>
    <td background="../imagenes/tablero/centro.gif" style="text-align:left">
    <img src="../imagenes/iconos/spacer.gif" width="1" height="1">
	<img src="../imagenes/iconos/spacer.gif" width="1" height="1">
	<img src="../imagenes/iconos/nuevo.png" alt="Nuevo" width="16" height="16" style="cursor:pointer" title="Nuevo">
	<img src="../imagenes/iconos/spacer.gif" width="1" height="1">
	<img src="../imagenes/iconos/grabar.png" alt="Guardar" width="16" height="16" style="cursor:pointer">
	</td>
    <td background="../imagenes/tablero/derecha.gif">&nbsp;</td>
  	</tr>
  	<tr>
    <td background="../imagenes/tablero/izquierda2.jpg">&nbsp;</td>
    <td align="center" background="../imagenes/tablero/centro.gif">
	<div id="resultado" style="color:#FF0000"></div>
	</td>
    <td background="../imagenes/tablero/derecha.gif">&nbsp;</td>
  </tr>
  <tr>
    <td background="../imagenes/tablero/izquierda2.jpg">&nbsp;</td>
    <td align="center" background="../imagenes/tablero/centro.gif">
	<table width="95%" border="0" cellspacing="0" class="tablero">
      <tr>
        <td width="25%">Fecha</td>
        <td width="26%"><input name='txtfecha' class='boxfecha' id="txtfecha" readonly="" value=""></td>
        <td width="26%">&nbsp;</td>
        <td width="23%">&nbsp;</td>
      </tr>
      <tr>
      <td>Tipo Documento</td>
        <td><select name="txttipo" class="box1" id="txttipo">
        <option value="1" selected="selected">Cedula</option>
        <option value="2">TARJETA DE IDENTIDAD</option>
        <option value="3">PASAPORTE</option>
        <option value="4">CEDULA DE EXTRANJERIA</option>
        </select></td>
        <td>Numero</td>
        <td><input name="txtnumero" type="text" class="box1" id="txtnumero" onkeypress="return solonumeros(event)" onfocus="color1(this);" onblur="color2(this)" /></td>
        </tr>
      <tr>
        <td>Nombres</td>
        <td colspan="3"><input name="txtnombre" type="text" class="boxlargo" id="txtnombre" readonly="" ></td>
      </tr>
      <tr>
      <td>Estado</td>
      <td colspan="3"><input name='txtestado' class='boxfecha' id="txtestado" onFocus="color1(this);" onBlur="color2(this);"></td>
      </tr>
	  <tr>
      <td>Nit</td>
      <td colspan="3"><label>
      <input name="txtnit" type="text" class="box1" id="txtnit" onKeyPress="return solonumeros(event)" onFocus="color1(this);" onBlur="color2(this),buscarCedula(this);" >
      <input name="txtempresa" type="text" class="boxlargo" id="txtempresa" readonly="" />
      </label></td>
      </tr>
      <tr>
      <td>Tipo Afiliación</td>
      <td><input name='txtafiliacion' class='box1' id="txtafiliacion" onFocus="color1(this);" onBlur="color2(this);"></td>
      <td>Tipo Formulario</td>
      <td><input name='txtformulario' class='box1' id="txtformulario" onfocus="color1(this);" onblur="color2(this);" /></td>
      </tr>
      
      <tr>
      <td>Clase Aportante</td>
      <td colspan="3"><input name='txtclase' class='boxlargo' id="txtclase" onFocus="color1(this);" onBlur="color2(this);"></td>
      </tr>
	  </table>
 
    <h3 style="text-align:left">Tarjeta con Subsidios Asignados</h3>
    <center>
    <table width="85%" border="0" cellspacing="0" class="tablero">
    <tr>
    <th width="50%">Tarjeta</th>
	<th width="50%">Estado</th>
    </tr>
    <tbody id="ttarj">
    </tbody>
    </table>
    <h3 style="text-align:left">Ultima Tarjeta Activada</h3>
    <table width="85%" border="0" cellspacing="0" class="tablero" >
		<tr>
        <th width="50%">Tarjeta</th>
		<th width="50%">Estado</th>
		</tr>
        <tbody id="ttarja">
        </tbody>
		</table>  
	<table width="95%" border="0">
      <tr>
        <td width="67%" ><span class="Rojo">Manual:</span><span class="celda">
          <br>
        </span>
        <ol start="1" type="1">
        <li><strong>Haga click en el bot&oacute;n Nuevo<img src="../imagenes/iconos/nuevo.png" width="16" height="16"> </strong></li>
        <li><strong>Escriba el n&uacute;mero de identificaci&oacute;n del trabajador y presione la tecla tabulador.</strong></li>
        </ol>
        <p><span class="celda"> </span></p></td>
        <td width="33%" ><center><div id="imagen" style="display:none"><img src="../imagenes/27-0.gif"></div></center>&nbsp;</td>
      </tr>
    </table></td>
    <td background="../imagenes/tablero/derecha.gif">&nbsp;</td>
  </tr>
  <tr>
    <td height="38" background="../imagenes/tablero/abajo_izq2.gif">&nbsp;</td>
    <td background="../imagenes/tablero/abajo_central.gif"></td>
    <td background="../imagenes/tablero/abajo_der.gif">&nbsp;</td>
  </tr>
</table>   
    </div>
</div>
</center>
<input type="hidden" id="idpersona" />
</form>
</body>
</html>