//funcion para obtener cookie en javascript
function getCookie(c_name)
{
	var i,x,y,ARRcookies=document.cookie.split(";");
	for (i=0;i<ARRcookies.length;i++)
	{
			x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
			y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
			x=x.replace(/^\s+|\s+$/g,"");
			if (x==c_name)
			{
				return unescape(y);
			}// fin if
	}// fin for
	}// fin getCookie
//funcion para obtener cookie en javascript
	
URL=src();

$(function(){
	$("#btnEjecutar").button();
	//$("#btnEjecutar").click(ejecutar);
	
	
	$("#txtPeriodo").datepicker({
        dateFormat: 'yymm',
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        onClose: function(dateText, inst) {
        	var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            $(this).datepicker('setDate', new Date(year, month, 1));
            }
        }); 

	$("#txtPeriodo").focus(function () {
		$(".ui-datepicker-calendar").hide();
		$("#ui-datepicker-div").position({
			my: "center top",
			at: "center bottom",
			of: $(this)
		});
	}); 
});

function ejecutar(){
		var periodo = $("#txtPeriodo").val().trim();
		var valor = $("#txtValor").val().trim();
		if(periodo==0){
			alert("Ingrese el Periodo!!");
			return false;
		}
		if(valor==0){
			alert("Ingrese el Valor del Subsidio!!");
			return false;
		}
		var bandera = true;
		var procesados = 0;
		while (bandera){
		
		$.ajax({
			url:'ejecutarSabana.php',
			type:'POST',
			data:{periodo:periodo,valor:valor,procesadosI:procesados},
			dataType:"json",
			beforeSend: function(objeto){
				dialogLoading('show');
			},
			async:false,		
			success:function(data){
				dialogLoading('close');
				
				if(data instanceof Array){
					bandera = false;
					$("#ajax").html('&nbsp;');
					
				}
				
				if(data.error==0){
					if(data.procesados==-1){
						alert("El proceso se ejecuto correctamente!!");
						$("#excelImp").show();
						bandera = false;
					}else{
						procesados = data.procesados;
					}				
					
				}else if(data.error==1){
					alert("ERROR. El proceso no se ejecuto!!");
					$("#excelImp").hide();
					bandera = false;
				}
			}
		})
	}
}

/**MANDA EL REPORTE A EXEL**/
		function procesar01(){
			var usuario=$("#usuario").val();
			var periodo = $("#txtPeriodo").val().trim();		
			var url="http://"+getCookie("URL_Reportes")+"/bono/rptExcel001.jsp?v0="+usuario+"&v1="+periodo;
			window.open(url,"_NEW");
			/**OCULTAR ICONO DE EXCEL**/
			$("#excelImp").hide();
		}