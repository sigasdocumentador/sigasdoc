<?php
set_time_limit(0);
date_default_timezone_set('America/Bogota'); 
ini_set("display_errors",'1');

$root= $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $_SESSION['RAIZ'] . DIRECTORY_SEPARATOR . '/rsc/pdo/IFXDbManejador.php';

$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo $cadena;
	exit();
}
$idp =$_REQUEST['v0'];
$filas = array();
$sql="select idBono,t.identificacionCon as identificacion,b.idBeneficiario as idbeneficiario,b.edadBenef as edadbeneficiario,b.primerNombreBen as pnombre,primerApellidoBen as papellido,b.estado,t.embargo,t.razonsocial,t.idempresa  from BonosEducativos as b 
				inner join TrabajadoresBonosEducativos as t on t.idTraBono = b.idBono
where t.idPersona = ".$idp;
$rs=$db->querySimple($sql);
if(is_null($rs)){
	echo 0;
	exit();
}
$cont=0;
while ($row=$rs->fetch()){
	$filas[]=$row;
	$cont++;
}
if ($cont>0) {
	echo json_encode($filas);
}
else 
	echo 0;
?>