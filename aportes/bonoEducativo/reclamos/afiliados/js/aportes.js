

function traeraportes(idp){
	var nom;
	var html;
	continuar=0;
	if(idp==0){
		alert("No hay ID Afiliado"); 
		return false;
	}
	$.ajax({
        url: "buscarAportes.php",
        async:true,
        data:{v0:idp},
        beforeSend: function(objeto){
        	dialogLoading('show');
        },
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("Error... Pasó lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
        success: function(datos){
        	if(datos==0){
    			alert("No hay relaciones de convivencia!");
    			return false;
    			}	
    		$.each(datos,function(i,f){
    			$("#taportes").append("<tr><td style='text-align:center'>"+f.periodo+"</td><td style='text-align:right'>"+formatNumber(f.valoraporte)+"</td><td style='text-align:center'>"+f.fechapago+"</td><td style='text-align:center'>"+f.fechasistema+"</td></tr>")
    			return;
    			})
           },
        timeout: 3000,
        type: "GET"
 });
}