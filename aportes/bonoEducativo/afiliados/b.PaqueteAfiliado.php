<?php
set_time_limit(0);
date_default_timezone_set('America/Bogota'); 
ini_set("display_errors",'1');

$root= $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $_SESSION['RAIZ'] . DIRECTORY_SEPARATOR . '/rsc/pdo/IFXDbManejador.php';
$idp =$_REQUEST['v0'];

$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo $cadena;
	exit();
}
$filas = array();
//$sql="Select idbeneficiario,entregado,fechaentrega,horaentrega,observaciones,c.identificacion,b.pnombre,b.papellido,e.nit,e.razonsocial,edadbeneficiario,embargo,entregado from beneficiarios left join Aportes.dbo.aportes015 c on c.idpersona=beneficiarios.idconyuge left join Aportes.dbo.aportes015 b on b.idpersona = beneficiarios.idbeneficiario left join Aportes.dbo.aportes048 e on e.idempresa=beneficiarios.idempresa where beneficiarios.idpersona=$idp";
$sql="select b.idBeneficiario as idbeneficiario,edadBenef as edadbeneficiario,primerNombreBen as pnombre,primerApellidoBen as papellido,estado,idAgencia from BonosEducativos as b left join TrabajadoresBonosEducativos as t on t.idBeneficiario = b.idBeneficiario where t.idpersona =$idp";
$rs=$db->querySimple($sql);
if(is_null($rs)){
	echo 0;
	exit();
}
$cont=0;
while ($row=$rs->fetch()){
	$filas[]=$row;
	$cont++;
}
if ($cont>0) {
	echo json_encode($filas);
}
else 
	echo 0;
?>