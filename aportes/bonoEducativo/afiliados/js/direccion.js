//... ABRIR VENTANA DIALOG PARA DIRECCIONES...//
function direccion(obj,obj2){
	//$("#dialog-form").dialog("destroy"); //Destruir dialog para liberar recursos
	//obj.value='';
	$("#dialog-form").dialog({
		height: 450,
		width: 650,
		draggable:false,
		modal: true,
		open: function(){
			$('#dialog-form').html('');
			$.get('direcciones.php',function(data){
			$('#dialog-form').html(data);
			});
				$('#tDirCompleta').val('');
				$('#dialog-form select,#dialog-form input[type=text]').val('');
			    $('#uno').focus();
		},
		buttons: {
			'Crear direcci\u00F3n': function() {
				if($("#tDirCompleta").val()=="")
					{
						alert("Seleccione o escriba los datos requeridos!");
					}
				else
					{
						//$("#dialog-form").dialog("destroy");
					    obj.value=$('#tDirCompleta').val();
						obj2.focus();
						$(this).dialog('close');
						//console.log(obj2);
						$('#tDirCompleta').val('');
						$('#dialog-form select,#dialog-form input[type=text]').val('');					}
				
			}
		},
		close: function() {
			if(document.getElementById('tDirCorresp'))
				document.getElementById('tDirCorresp').value = document.getElementById('tDirCompleta').value;
			//$("#dialog-form").dialog('close');
			$('#tDirCompleta').val('');
			$('#dialog-form select,#dialog-form input[type=text]').val('');
			obj2.focus();
			$(this).dialog("destroy");
			//focus al siguiente cmapo de texto; 
		}
	});
}//end function
//-- FIN DIRECCIONES	
