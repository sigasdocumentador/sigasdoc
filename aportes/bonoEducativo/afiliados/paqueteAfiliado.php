<?PHP
date_default_timezone_set('America/Bogota');
session_start();
if( !isset($_SESSION['IDROLPAQUETE']) ){
	header('Location: ../index.php');
	}
$idrol = $_SESSION['IDROLPAQUETE'];
if ($idrol == 1 || $idrol==3 || $idrol == 8 || $idrol == 5){
	$agencia=$_SESSION['AGENCIA'];
	$usuario=$_SESSION['USUARIO'];
}
else{
	echo "<script> alert('Usuario sin permiso para este formulario');
	window.open('../empty.php','_self');</script>";
	exit();
	}

include_once '../rsc/pdo/SQLDbManejador.php';

$db = SQLDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo $cadena;
	exit();
}

$saldo=$db->saldo($usuario);
$mesa=$db->mesa($usuario);
$_SESSION['MESA']=$mesa;

$sql="SELECT distinct coddepartamento,departmento FROM Aportes.dbo.aportes089";
$rs=$db->querySimple($sql);	

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Entrega Paquete Afiliado</title>
<link href="../css/jquery-ui-1.9.2.custom.min.css" type="text/css" rel="stylesheet"  />
<link href="../css/estilo.css" type="text/css" rel="stylesheet"  />
<script src="../js/jquery-1.8.3.js"></script>
<script src="../js/jquery-ui-1.9.2.custom.min.js"></script>
<script src="../js/funciones.js"></script>
<script src="../js/validarCampoOPA.js"></script>
<script src="js/paqueteAfiliado.js"></script>

<script src="../js/jquery-1.6.2.min.js"></script>
<script src="../js/jquery-ui-1.8.16.custom.min.js"></script>

<script src="js/direccion.js"></script>
<script src="js/comunes.js"></script>
<script src="js/jquery.combos2.js"></script>
<script type="text/javascript">
	
	/** DOCUMENT READY **/
	$(document).ready(function(){		
		$("#cboCiudad").jCombo("2", { 
			parent: "#cboDepto",
			selected_value: '41001'
		});
		$("#cboZona").jCombo("3", {
			parent: "#cboCiudad"
		});	
		$("#cboBarrio").jCombo("4", {
			parent: "#cboZona"
		});	
	});
		</script>

</head>
<body>
<br>
<center>
<form id="afiliados">
<table width="90%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="13" height="29" background="../imagenes/tablero/arriba_izq.gif">&nbsp;</td>
<td background="../imagenes/tablero/arriba_central2.gif"><span class="letrablanca">::Paquete Escolar &nbsp;::</span></td>
<td width="13" background="../imagenes/tablero/arriba_der.gif" align="right">&nbsp;</td>
</tr>
<tr>
<td background="../imagenes/tablero/izquierda2.jpg">&nbsp;</td>
<td background="../imagenes/tablero/centro.gif">
<img src="../imagenes/tablero/spacer.gif" width="1" height="1">
<img src="../imagenes/tablero/spacer.gif" width="1" height="1">
<img src="../imagenes/iconos/nuevo.png" title="Nuevo" width="16" height="16" style="cursor:pointer" onClick="nuevo()">
<img src="../imagenes/tablero/spacer.gif" width="1" height="1">
<img src="../imagenes/tablero/spacer.gif" width="1" height="1"><span class="big">Nueva entrega </span></td>
<td background="../imagenes/tablero/derecha.gif">&nbsp;</td>
</tr>
<tr>
<td background="../imagenes/tablero/izquierda2.jpg">&nbsp;</td>
<td align="center" background="../imagenes/tablero/centro.gif">
<div id="resultado" style="color:#FF0000"></div>
<div id="embargo" style="color:#FF0000; font-size:36px"></div></td>
<td background="../imagenes/tablero/derecha.gif">&nbsp;</td>
</tr>
<tr>
<td background="../imagenes/tablero/izquierda2.jpg">&nbsp;</td>
<td align="center" background="../imagenes/tablero/centro.gif">
	<table width="95%" border="0" cellspacing="0" class="tablero">
    <tr>
    <td colspan="2">Fecha</td>
    <td width="9%"><input name='txtfecha' class='boxfecha' id="txtfecha" readonly=""></td>
    <td width="6%">Mesa</td>
    <td width="70%"><input name='txtmesa' class='boxfecha' id="txtmesa" readonly="" value="<?php echo $mesa; ?>" ></td>
    </tr>
	<tr>
      <td colspan="2">Tipo Documento</td>
        <td>
		<select name="txttipo" class="box1" id="txttipo">
        <option value="1" selected="selected">Cedula</option>
        <option value="2">TARJETA DE IDENTIDAD</option>
        <option value="3">PASAPORTE</option>
        <option value="4">CEDULA DE EXTRANJERIA</option>
        </select>
		</td>
       <td>Numero</td>
        <td><input name="txtnumero" type="text" class="box1" id="txtnumero" onkeypress="return solonumeros(event)" onfocus="color1(this);" onblur="color2(this),buscarPersona();" /></td>
    </tr>
    <!--<tr>
    <td colspan="2">Identificaci&oacute;n</td>
    <td colspan="3"><label>
    <input name="txtnumero" type="text" class="box1" id="txtnumero" onKeyPress="return solonumeros(event)" onFocus="color1(this);" onBlur="color2(this),buscarPersona();" >
          <font color="#FF0000">*</font></label></td>
        </tr>-->
      <tr>
        <td colspan="2">Nombres</td>
        <td colspan="3"><input name="txtnombres" type="text" class="boxlargo" id="txtnombres" readonly="" ></td>
      </tr>
      <tr>
      <td colspan="2">Empresa</td>
        <td colspan="3"><input name="txtempresa" type="text" class="boxlargo" id="txtempresa" readonly="" /></td>
        </tr>
     
        <td colspan="2"><span class="big">Entregado A </span></td>
        <td colspan="3"><span class="negrita">Afiliado</span>
          <input name="radio" id="radioA" type="radio" value="s1" onClick="mensaje(1);">
          <span class="negrita">Tercero</span>
          <input name="radio" id="radioT" type="radio" value="s2" onClick="mensaje(2);"></td>
        </tr>
	  <tr>
        <td colspan="2"><span class="big">Notas</span></td>
        <td colspan="3"><input name="txtnotas" type="text" class="boxlargo" id="txtnotas" readonly="readonly" ></td>
        </tr>
		</table>
		<table width="95%" border="0" cellspacing="0" class="tablero">
	  <tr>
        <td colspan="5"><span class="Rojo">Beneficiarios</span></td>
        </tr>
		
		<tr><td colspan="5">
		<table width="100%" border="0" cellspacing="0" class="tablero" >
		<tr>
        <td width="1%"><strong>N</strong></td>
        <td width="5%"><strong>CedCon</strong></td>
        <td width="5%"><strong>CodBen</strong></td>
        <td width="4%"><strong>Edad</strong></td>
        <td width="20%"><strong>Beneficiario</strong></td>
		<td width="65%"><strong>Estado</strong></td>
		</tr>
        <tbody id="tBeneficiarios">
        </tbody>
		</table>  
      <tr>
        <td colspan="5">&nbsp;</td>
      </tr>
	  </table>
	  <table width="95%" border="0" cellspacing="0" class="tablero">
      <tr>
        <td colspan="4" rowspan="2"><span class="Rojo">Manual:</span>
          <ol>
            <li><span class="celda">Haga click en el bot&oacute;n Nuevo <img src="../imagenes/iconos/nuevo.png" width="16" height="16">&nbsp; </span></li>
            <li><span class="celda">Escriba el n&uacute;mero de identificaci&oacute;n del trabajador y presione la tecla        tabulador.</span></li>
            <li><span class="celda">Para entregar el paquete haga click sobre el t&iacute;tulo <strong><font color="green" style="cursor: pointer;">Entregar</font></strong>. </span></li>
          </ol>
          <p><span class="celda"><strong>Nota:</strong>Los campos con las convenciones (<span class="Estilo4">*</span>) Son  necesarios para el proceso. </span></p>
          <span class="celda">
          </li>
		  </span></td>
        <td width="33%" rowspan="2"><div id="entregar" align="center">&nbsp;</div></td>
        <td width="11%"><div align="center"><span class="big">SALDO</span></div></td>
      </tr>
      <tr>
        <td><div id="disponible" style="font-size:24px; color:#FF0000" align="center">0</div></td>
      </tr>
    </table></td>
    <td background="../imagenes/tablero/derecha.gif">&nbsp;</td>
  </tr>
  <tr>
    <td height="38" background="../imagenes/tablero/abajo_izq2.gif">&nbsp;</td>
    <td background="../imagenes/tablero/abajo_central.gif"></td>
    <td background="../imagenes/tablero/abajo_der.gif">&nbsp;</td>
  </tr>
</table>

<input type="hidden" name="pageNum" value="0">
<input type="hidden" id="saldo" value="<?php echo $saldo; ?>">
<input type="hidden" id="mesa" value="<?php echo $mesa; ?>">
<div id="dlgLoading"></div>
</FORM>
</center>

	<!-- DIALOGO ACTUALIZAR PERSONA RADICACION -->
	<div id="dialog-actualiza-persona" title="Formulario Datos Basicos" style="display:none">
	<center>
		<table width="754" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" class="tablero">
			<tbody>
				<tr bgcolor="#EBEBEB">
				  <td colspan="4" style="text-align:center" ><h1>Datos B&aacute;sicos de la persona</h1></td>
			  	</tr>
				<tr>
					<td font><h2>Tel&eacute;fono</h2></td>
					<td><h2><input name="txtTelefono" type="text" class="box1" id="txtTelefono" onkeydown="solonumeros(this);" onkeyup="solonumeros(this);"/></h2>
					</td>
					<td><h2>Celular</h2></td>
					<td>
					<h2><input name="txtCelular" type="text" class="box1" id="txtCelular" onkeydown="solonumeros(this);" onkeyup="solonumeros(this);"/></h2>
					</td>
			  	</tr>
		        <tr>
				    <td><h2>E-mail</h2></td>
				    <td colspan="4"><h2><input name="txtEmail" type="text" class="box1" id="txtEmail" onblur="soloemail(this)"/></h2></td>
			    </tr>
			    <tr>
				    <td><h2>Depto Reside</h2></td>
				    <td ><h2><select name="cboDepto" id="cboDepto" class="box1">
				      <option value="0" selected="selected">Seleccione</option>
					     <?php
						while($row=$rs->fetch()){
						echo "<option value=".$row['coddepartamento'].">".utf8_encode($row['departmento'])."</option>";
							}
				       	?>
				      </select></h2></td>
				    <td ><h2>Ciudad Reside</h2></td>
				    <td ><h2><select name="cboCiudad" id="cboCiudad" class="box1">
				      <option value="0" selected="selected">Seleccione</option>
				      	 </select></h2></td>
				    </tr>
				  <tr>
				    <td ><h2>Zona Reside</h2></td>
				    <td ><h2><select name="cboZona" id="cboZona" class="box1">
				      <option value="0" selected="selected">Seleccione</option>
				      	 </select></h2></td>
				   	<td ><h2>Barrio</h2></td>
				      <td ><h2><select name="cboBarrio" id="cboBarrio" class="box1">
				      <option value="0" selected="selected">Seleccione</option>
				      </select></h2>
			          </td>
	    		  </tr>
	    		  <tr>
	    			  <td ><h2>Direcci&oacute;n</h2></td>
	    			  <td colspan="4"><h2><input name="txtDireccion" type="text" class="box1" id="txtDireccion" onfocus="direccion(this,document.getElementById('cboBarrio'));" /></h2></td>
	    		  </tr>
				<tr>
					<td><h2>Autorizado</h2></td>
					<h2><td colspan="4"><input type='checkbox' name='checkbox' id="check" value="1" onclick="desactivar()"/></h2></td>
				</tr>
		</tbody>
		</table>
	</center>
	</div>
	
	<!-- formulario direcciones  --> 
	<div id="dialog-form" title="Formulario de direcciones"></div>
	
</BODY>
<script language="javascript">
	//$('#disponible').html(<?php echo $saldo; ?>);
</script>
</body>
</html>