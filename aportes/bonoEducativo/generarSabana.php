<?php
set_time_limit(0);
date_default_timezone_set('America/Bogota');
ini_set("display_errors",'1');

$root= $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$fecha=date('Y-m-d');
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<title>Paquete Escolar</title>
<LINK href="css/estilo.css" rel="stylesheet" type="text/css">
<link href="css/jquery-ui-1.9.2.custom.min.css" type="text/css" rel="stylesheet"  />
<script  src="js/jquery-1.8.3.js"></script>
<script  src="js/jquery-ui-1.9.2.custom.min.js"></script>
<script  src="js/funciones.js"></script>
<script language="javascript" src="js/generarSabana.js" type="text/javascript"></script>
</HEAD>
<BODY>
<form id="forma">
<br>
<center>
<table width="90%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="13" height="29" background="imagenes/tablero/arriba_izq.gif">&nbsp;</td>
<td background="imagenes/tablero/arriba_central2.gif">
<span class="letrablanca">:: Generar Subsidio Escolar&nbsp;::</span></td>
<td width="13" background="imagenes/tablero/arriba_der.gif" align="right">&nbsp;</td>
</tr>
<tr>
<td background="imagenes/tablero/izquierda2.jpg">&nbsp;</td>
<td background="imagenes/tablero/centro.gif"></td>
<td background="imagenes/tablero/derecha.gif">&nbsp;</td>
</tr>
<tr>
<td background="imagenes/tablero/izquierda2.jpg">&nbsp;</td>
<td align="center" background="imagenes/tablero/centro.gif">
<div id="resultado" style="color:#FF0000"></div>
<div id="embargo" style="color:#FF0000; font-size:36px"></div>
</td>
<td background="imagenes/tablero/derecha.gif">&nbsp;</td>
</tr>
<tr>
<td background="imagenes/tablero/izquierda2.jpg">&nbsp;</td>
<td align="center" background="imagenes/tablero/centro.gif">
	<table width="80%" border="0" cellspacing="0" class="tablero">
	<tr>
	<td width="50%" style="text-align: right;">PERIODO:</td>
		<td >
		<input type="text" name="txtPeriodo" id="txtPeriodo" class="box1" onFocus="color1(this);"/>
		<img id="excelImp" src="imagenes/excel.png" width="16" height="16" style="display:none" title="Imprimir" onClick = "procesar01()" >
	</td>	
	</tr>
	<tr>
		<td width="50%" style="text-align: right;">VALOR SUBSIDIO:</td>
		<td >
		<input type="text" name="txtValor" id="txtValor" class="box1" onFocus="color1(this);"/></td>
	</td>
	</tr>
	<tr >
	<td colspan="2" style="text-align: center;">
	<input type="button" id="btnEjecutar" name="btnEjecutar" value="Ejecutar" onclick="ejecutar();"/>
	</td>
	</tr>
    <tr>
    </table>
</td>
<td background="imagenes/tablero/derecha.gif">&nbsp;</td>
</tr>
<tr>
<td height="38" background="imagenes/tablero/abajo_izq2.gif">&nbsp;</td>
<td background="imagenes/tablero/abajo_central.gif"></td>
<td background="imagenes/tablero/abajo_der.gif">&nbsp;</td>
</tr>
</table>
</center>
<input type="hidden" value="<?php echo $_SESSION["USUARIO"];?>" name="usuario" id="usuario" />
</form>
</BODY>

</HTML>