<?php
	date_default_timezone_set('America/Bogota');
	set_time_limit(0);
	$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
	include_once  $root;
	
	$usuario=$_SESSION['USUARIO'];
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
	include_once $raiz.'/config.php';
	
	$db = IFXDbManejador::conectarDB();
	if($db->conexionID==null){
		$cadena = $db->error;
		echo msg_error($cadena);
		exit();
	}
	
	$caso = $_REQUEST["caso"]; 	
	$resultado = null;
	switch( $caso ){
		case "archivo_plano":
			$idProceso = $_REQUEST["id_proceso"]; 
			
			$sql = "SELECT id_proceso
						, a426.id_etapa_proceso 
						, numer_secue_tipo6 
						,codig_entid_finan_tipo1 
						,numer_cuent_tipo5 
						,fecha_recau_tipo1 
						,ident_aport_tipo6 
						,ident_aport_tipo6_db
						,nombr_aport_tipo6 
						,numer_plani_liqui_tipo6 
						,perio_pago_tipo6 
						,valor_plani_tipo6 
						,error 
						,numero_recibo_db
						,a421.descripcion AS etapa_proceso
					FROM aportes426 a426
						INNER JOIN aportes421 a421 ON a421.id_etapa_proceso=a426.id_etapa_proceso
					WHERE a426.id_proceso=".$idProceso;
			$resultado = $db->querySimple($sql);			
		break;			
	}
	
	$path_plano =$ruta_generados.'tesoreria'.DIRECTORY_SEPARATOR.'reporte'.DIRECTORY_SEPARATOR;
	if( !file_exists($path_plano) ){
		mkdir($path_plano, 0777, true);
	}
	$fecha=date('Ymd');
	$archivo='reporte_'.$caso."_".$fecha.'.csv';
	$path_plano.=DIRECTORY_SEPARATOR.$archivo;
	$cont = 0;
	$indices = 0;
	$cadena = null;
	$bandera = 'w';
	$fp=fopen($path_plano,'w');
	while ( ( $row = $resultado->fetch() ) == true ){	
		if($cont == 0){
			$indices = array_keys($row);
			$cadena = join(";", $indices)."\r\n";
			fwrite($fp, $cadena);
		}
		$row = array_map("utf8_decode", $row);
		$cadena = join(";", $row)."\r\n";
		fwrite($fp, $cadena);
		$cont++;
	}
	fclose($fp);
	
	$_SESSION['ENLACE']=$path_plano;
	$_SESSION['ARCHIVO']=$archivo;
	echo 1;
?>