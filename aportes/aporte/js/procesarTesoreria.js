var url = "",
	urlProceso = "",
	urlBuscarProceso = "",
	urlUltimoProceso = "",
	urlLogProceso = "",
	urlDescargarArchivo = "",
	urlPlanoTesoreria = "",
	
	objAccion = {procesar:'PROCESAR', reprocesar:'REPROCESAR'},
	
	hiloUltimProce = null,
	
	arrFileBanera = [],
	arrFileCargado = [];


$(function(){
	
	url = src();
	urlProceso = url+"aportes/aporte/procesarTesoreria.log.php";
	urlBuscarProceso = url+"aportes/aporte/buscarProceso.php";
	urlUltimoProceso = url+"aportes/aporte/ultimoProceso.php";
	urlLogProceso = url+"aportes/aporte/buscaLogProce.php";
	urlDescargarArchivo= url+'phpComunes/descargaPlanos.php';
	urlPlanoTesoreria = url+"aportes/aporte/planoTesoreria.log.php";
	
	$("#btnProcesoNuevo")
			.button()
			.click(procesoNuevo);
	
	$("#btnProcesoError")
			.button()
			.click(procesoError);
	
	$(":radio[name='radTipoCarge']").click(controlCarge);
	
	nuevo();
	controlUltimoProceso();
	
	$("#divLogProceso").dialog({
		modal:true,
		autoOpen:false,
		width:1500,
		//maxWidth: 1500,
		buttons:{
			"Aceptar":function(){
				$(this).dialog("close");
			}
		},
		close:function(){
			//$(this).dialog("destroy");
		}
	});
});


function nuevo(){
	$("#hidAccion").val('');
	$("#hidIdProceso").val('');
	$("#hidBanderaProcesando").val('');
	$(":file,#divCargarArchivos").hide();
	$(":radio[name='radTipoCarge']").attr("checked",false)
	arrFileCargado = [];
	arrFileBanera = [];
}

/**
 * Metodo encargado de asignar el ultimo proceso al formulario
 */
function controlUltimoProceso(){
	var objProceso = fetchUltimoProceso();
	
	var htmlProceso = "<tr><td>&nbsp;</td></tr>";
	var idProceso = 0;
	var accion = $("#hidAccion").val();
	
	if(accion==objAccion.reprocesar){
		idProceso = $("#hidIdProceso").val();
	}
	
	if(typeof objProceso=="object" && objProceso.length>0){
		htmlProceso = prepareProceso(objProceso,"ULTIMO");
		
		//Verificar si el proceso esta en proceso
		if(objProceso[0].estado_proceso=='PROCESO' || objProceso[0].estado_proceso=='INICIO'){
			idProceso = objProceso[0].id_proceso
			if(accion==0){
				seguirProceso();
			}
		}
	}
	
	$("#hidIdProceso").val(idProceso);
	$("#tblProceso tbody").html(htmlProceso);
}

function prepareProceso(objData, tipo){
	var objDataProceso = objData[0];
	
	var htmlTr = "";
	
	if(tipo=="ULTIMO"){
		//Formar datos del proceso en general
		htmlTr = "<tr>" +
				"<th>id:"+objDataProceso.id_proceso+"</th>" +
				"<th>Fecha Inicio:"+objDataProceso.fecha_inici_proce+"</th>" +
				"<th>Fecha Fin:"+objDataProceso.fecha_fin_proce+"</th>" +
				"<th colspan='2'>Estado:"+objDataProceso.estado_proceso+"<img src='" + URL + "imagenes/descargar.png' style='cursor:pinter' onclick='descargar("+objDataProceso.id_proceso+")'/></th>" +
				"</tr>";
	}else{
		htmlTr = "<tr>" +
				"<th>id:"+objDataProceso.id_proceso+"</th>" +
				"<th>Fecha Inicio:"+objDataProceso.fecha_inici_proce+"</th>" +
				"<th>Fecha Fin:"+objDataProceso.fecha_fin_proce+"</th>" +
				"<th>Estado:"+objDataProceso.estado_proceso+"</th>" +
				"<th><input type='button' name='btnReprocesar' id='btnReprocesar"+objDataProceso.id_proceso+"' value='PROCESAR' onclick='reprocesar("+objDataProceso.id_proceso+");'/><img src='" + URL + "imagenes/descargar.png' style='cursor:pinter' onclick='descargar("+objDataProceso.id_proceso+")'/></th>" +
				"</tr>";
	}

	htmlTr += "<tr>" +
			"<th>Etapa</th>" +
			"<th>Fecha Inicio</th>" +
			"<th>Fecha Fin</th>" +
			"<th>Estado</th>" +
			"<th>Log</th>" +
			"</tr>";
	
	var banderaProcesando = $("#hidBanderaProcesando").val();
	
	var contImgProceso = 0;
	$.each(objData,function(i,row){
		
		var procesado = '';
		if(banderaProcesando=='S' && row.procesado=="N" && contImgProceso==0){
			procesado = '<img src="'+url+'/imagenes/ajax-loader-fon.gif" />';
			contImgProceso++;
		}else if(row.procesado=="N"){
			procesado = 'SIN PROCESAR';
		}else if(row.procesado=="S"){
			procesado = "PROCESADO";
		}else if(row.procesado=="E"){
			procesado = "ERROR";
		}
		
		//var procesado = (row.procesado=="N")?"SIN PROCESAR":(row.procesado=="S")?"PROCESADO":(row.procesado=="E")?"ERROR":"";
		
		htmlTr += "<tr>" +
				"<td>"+row.descripcion+"</td>" +
				"<td>"+row.fecha_inici_traza+"</td>" +
				"<td>"+row.fecha_fin_traza+"</td>" +
				"<td style='text-align:center;'>"+procesado+"</td>" +
				"<td>[<a onclick='verLog("+row.id_traza_proceso+");' style='cursor:pointer;'>ver</a>]</td>" +
				"</tr>";
	});
	
	return htmlTr;
}

function seguirProceso(){
	var htmlTr = "<input type='button' name='btnSeguirProceso' id='btnSeguirProceso' value='SEGUIR PROCESO' onclick='procesar();'/>";
	$("#tdBandera").html(htmlTr);
	$("#btnSeguirProceso").button();
	$("#hidAccion").val(objAccion.procesar);
	$("#btnProcesoNuevo").hide();
}

function procesoNuevo(){
	nuevo();
	$(".cls-proceso-nuevo").show();
	$("#btnProcesar").button();
	$("#hidAccion").val(objAccion.procesar);
	datepickerC("FECHA","#txtFechaPago");
	
}

function controlCarge(){
	var radTipoCarge = $(":radio[name='radTipoCarge']:checked").val();
	
	if(radTipoCarge=="FTP"){
		$(".cls-proceso-nuevo-ftp").show();
		$(".cls-proceso-nuevo-cargar").hide();
		$(":file,#divCargarArchivos").hide();
	}else{
		$(".cls-proceso-nuevo-ftp").hide();
		$(".cls-proceso-nuevo-cargar").show();
		$(":file,#divCargarArchivos").show();
	}
}

function procesoError(){
	var objFiltro = {estado:"FIN_ERROR"};
	var objProceso = fetchProceso(objFiltro);
	var htmlProceso = "";
	
	$(".cls-proceso-nuevo,.cls-proceso-nuevo-ftp,.cls-proceso-nuevo-cargar").hide();
	
	$.each(objProceso,function(i,row){
		htmlProceso += prepareProceso(row,"ERROR");
	});

	if(htmlProceso==""){
		htmlProceso = "<tr><td><b>NO HAY PROCESOS CON ERROR</b></td></tr>"
	}
	
	$("#tblProceso tbody").html(htmlProceso);
	$("input[name='btnReprocesar']").button();
}

function procesar(){
	var accion = $("#hidAccion").val();
	var tipoCarge = $(":radio[name='radTipoCarge']:checked").val();
	
	if(accion==0) return false;
	
	//Validar la fecha de pago para el proceso nuevo
	if(accion==objAccion.procesar && $(":radio[name='radTipoCarge']").is(":visible")){
		
		if( !$(":radio[name='radTipoCarge']").is(":checked") ){
			alert("Debe indicar el tipo de carge");
			return false;
		}
		
		if(tipoCarge=='FTP'){
			if($("#txtFechaPago").val().trim()==0){
				$("#txtFechaPago").addClass("ui-state-error");
				return false;
			}
			
			$("#txtFechaPago").removeClass("ui-state-error");
		}else if(tipoCarge=='CARGE'){
			//Validar los archivos
			if(arrFileCargado.length==0 && document.getElementById("filArchivo").files.length==0){
				alert("Debe seleccionar los archivos a cargar!");
				return false;
			}
		}	
	}
	
	var banderaTermino = true;
	
	$("#hidBanderaProcesando").val('S');
	$(".cls-proceso-nuevo,.cls-proceso-nuevo-ftp,.cls-proceso-nuevo-cargar").hide();
	$(":file,#divCargarArchivos").hide();
	$("#tdBandera").html('');
	
	controlUltimoProceso();
	var dataFormu = fetchDataFormu();
	
	$.ajax({
		url:urlProceso,
		type:"POST",
		data:dataFormu,
		dataType: "json",
		async:false,
        contentType:false,
        processData:false,
        cache:false,		
		error: function(XHR, textStatus, errorThrown){ 
			alert(XHR);
			console.log("ERREUR: " + textStatus); 
			console.log("ERREUR: " + errorThrown); 
		},
		complete: function(jqXHR,textStatus ){
			console.log("Com-jqXHR:"+jqXHR+"  -- textStatus:"+textStatus);
		},
		success:function(data){
			
			//Actualizar las trazas del proceso en el formulario
			controlUltimoProceso();
			banderaTermino = false;
			
			if(data.error==0 && data.data.etapa_proceso=="fin"){
				alert("El proceso termino correctamente");
				banderaTermino = true;
			}else if( typeof data != "object" || data.error==1 ){
				alert(data.descripcion);
				banderaTermino = true;
			}
			
			//Asignar valor al array Global donde almacenamos los name files ya procesados
			if(data.data.etapa_proceso=="subir_archivo"){
				arrFileCargado = arrFileCargado.concat(arrFileBanera);
				arrFileBanera = [];
			}	
		}
	});

	if(banderaTermino==false){
		$("#hidAccion").val(objAccion.procesar);
		procesar();
	}else{
		$("#hidBanderaProcesando").val('N');
		controlUltimoProceso();
	}
}

function reprocesar(idProceso){
	$("#hidIdProceso").val(idProceso);
	$("#hidAccion").val(objAccion.reprocesar);
	procesar();
}

function verLog(idTrazaProceso){
	if(!idTrazaProceso==0){
		var objFiltro = {id_traza_proceso:idTrazaProceso};
		var objLogProceso = fetchLogProceso(objFiltro);
	}
	
	if(typeof objLogProceso == "object"){
		
		var htmlTipoA = "<tr>" +
				"<th>Etapa Proceso</th>" +
				"<th>Codigo</th>" +
				"<th>Descripcion</th></tr>";
		
		var htmlTipoB = "<tr>" +
				"<th>Etapa Proceso</th>" +
				"<th>Codigo</th>" +
				"<th>Descripcion</th>" +
				"<th>Archivo</th>" +
				"<th>Tipo Identificacion</th>" +
				"<th>Numero Identificacion</th>" +
				"<th>Nombre</th></tr>";
		
		var htmlTipoC = "<tr>" +
				"<th>Etapa Proceso</th>" +
				"<th>Codigo</th>" +
				"<th>Descripcion</th>" +
				"<th>Archivo</th></tr>";
	
		var htmlTipoE = "<tr>" +
				"<th>Etapa Proceso</th>" +
				"<th>Codigo</th>" +
				"<th>Descripcion</th>" +
				"<th>Archivo</th>" +
				"<th>Planilla</th>" +
				"<th>Periodo</th>" +
				"<th>Nit</th>" +
				"<th>Razon Social</th>" +
				"<th>Fecha Pago</th>";
		
		var objHtml = {
				error:{
					html_tipo_a:htmlTipoA,
					html_tipo_b:htmlTipoB,
					html_tipo_c:htmlTipoC,
					html_tipo_e:htmlTipoE
				}
				, sin_error:{
					html_tipo_a:htmlTipoA,
					html_tipo_b:htmlTipoB,
					html_tipo_c:htmlTipoC,
					html_tipo_e:htmlTipoE
				}
			}; 
		
		var htmlTipoABand = "",
			htmlTipoBBand = "",
			htmlTipoCBand = "",
			htmlTipoEBand = "";
		
		$.each(objLogProceso,function(i,row){

			htmlTipoABand = "";
			htmlTipoBBand = "";
			htmlTipoCBand = "";
			htmlTipoEBand = "";
			
			//Tipo de log
			if(row.tipo_log=='A'){
				
				htmlTipoABand = "<tr>" +
						"<td>"+row.etapa_proceso+"</td>" +
						"<td>"+row.codigo+"</td>" +
						"<td>"+row.descripcion+"</td></tr>";
			
			}else if(row.tipo_log=='B'){
				
				htmlTipoBBand = "<tr>" +
						"<td>"+row.etapa_proceso+"</td>" +
						"<td>"+row.codigo+"</td>" +
						"<td>"+row.descripcion+"</td>" +
						"<td>"+row.data.nombre_archivo+"</td>" +
						"<td>"+row.data.tipo_identificacion+"</td>" +
						"<td>"+row.data.numero_identificacion+"</td>" +
						"<td>"+row.data.nombre+"</td></tr>";
				
			}else if(row.tipo_log=='C'){
				
				htmlTipoCBand = "<tr>" +
						"<td>"+row.etapa_proceso+"</td>" +
						"<td>"+row.codigo+"</td>" +
						"<td>"+row.descripcion+"</td>" +
						"<td>"+row.data.nombre_archivo+"</td></tr>";
				
			}else if(row.tipo_log=='E'){
				
				htmlTipoEBand = "<tr>" +
						"<td>"+row.etapa_proceso+"</td>" +
						"<td>"+row.codigo+"</td>" +
						"<td>"+row.descripcion+"</td>" +
						"<td>"+row.data.nombre_archivo+"</td>" +
						"<td>"+row.data.planilla+"</td>" +
						"<td>"+row.data.periodo+"</td>" +
						"<td>"+row.data.nit+"</td>" +
						"<td>"+row.data.razon_social+"</td>" +
						"<td>"+row.data.fecha_pago+"</td>";
		
			}
			
			if(row.estado=='ERROR'){
				objHtml.error.html_tipo_a += htmlTipoABand;
				objHtml.error.html_tipo_b += htmlTipoBBand;
				objHtml.error.html_tipo_c += htmlTipoCBand;
				objHtml.error.html_tipo_e += htmlTipoEBand;
			}else{
				objHtml.sin_error.html_tipo_a += htmlTipoABand;
				objHtml.sin_error.html_tipo_b += htmlTipoBBand;
				objHtml.sin_error.html_tipo_c += htmlTipoCBand;
				objHtml.sin_error.html_tipo_e += htmlTipoEBand;
			}
			
		});
		
		var htmlAll = 
				"<h4>LOG ERROR</h4>" +
				"<div id='divErrorA'>" +
					"<h3><a href='#'> ... </a></h3>"+
					"<table class='tablero' width='100%'>"+objHtml.error.html_tipo_a+"</table></div>" + 
		
				"<br/>" +
				"<div id='divErrorB'>" +
				"<h3><a href='#'> ... </a></h3>"+
				"<table class='tablero' width='100%'>"+objHtml.error.html_tipo_b+"</table></div>" +
				
				"<br/>" +
				"<div id='divErrorC'>" +
				"<h3><a href='#'> ... </a></h3>"+
				"<table class='tablero' width='100%'>"+objHtml.error.html_tipo_c+"</table></div>" +
				
				"<br/>" +
				"<div id='divErrorE'>" +
				"<h3><a href='#'> ... </a></h3>"+
				"<table class='tablero' width='100%'>"+objHtml.error.html_tipo_e+"</table></div>" +
				
				"<h4>LOG SIN ERROR</h4>" +
				"<div id='divSinErrorA'>" +
				"<h3><a href='#'> ... </a></h3>"+
				"<table class='tablero' width='100%'>"+objHtml.sin_error.html_tipo_a+"</table></div>" + 
	
				"<br/>" +
				"<div id='divSinErrorB'>" +
				"<h3><a href='#'> ... </a></h3>"+
				"<table class='tablero' width='100%'>"+objHtml.sin_error.html_tipo_b+"</table></div>" +
				
				"<br/>" +
				"<div id='divSinErrorC'>" +
				"<h3><a href='#'> ... </a></h3>"+
				"<table class='tablero' width='100%'>"+objHtml.sin_error.html_tipo_c+"</table></div>" +
				
				"<br/>" +
				"<div id='divSinErrorE'>" +
				"<h3><a href='#'> ... </a></h3>"+
				"<table class='tablero' width='100%'>"+objHtml.sin_error.html_tipo_e+"</table></div>";
		
		$("#divLogProceso").html(htmlAll);
		
		$( "#divErrorA,#divErrorB,#divErrorC,#divErrorE,#divSinErrorA,#divSinErrorB,#divSinErrorC,#divSinErrorE" ).accordion({
			autoHeight: false,		
			collapsible: true,
			active: false
		});
		
		$("#divLogProceso").dialog("open");
		
	}else{
		alert("No hay registros para mostrar");
	}
}

function fetchProceso(objFiltro){
	
	var resultado = null;
	objFiltro.tipo="KARDEX";
	
	$.ajax({
		url:urlBuscarProceso,
		type:"POST",
		data:{objDatosFiltro:objFiltro},
		dataType: "json",
		async:false,
		success:function(data){
			resultado = data;
		}
	});
	
	return resultado;
}

function fetchUltimoProceso(objFiltro){
	
	var resultado = null;
	objFiltro= {tipo:"KARDEX"};
	
	$.ajax({
		url:urlUltimoProceso,
		type:"POST",
		data:{objDatosFiltro:objFiltro},
		dataType: "json",
		async:false,
		success:function(data){
			resultado = data;
		}
	});
	
	return resultado;
}

function fetchLogProceso(objFiltro){
	
	var resultado = null;
	
	$.ajax({
		url:urlLogProceso,
		type:"POST",
		data:{objDatosFiltro:objFiltro},
		dataType: "json",
		async:false,
		success:function(data){
			resultado = data;
		}
	});
	
	return resultado;
}

function fetchDataFormu(){

	var idProceso = $("#hidIdProceso").val();
	var accion = $("#hidAccion").val();
	var fechaPago = $("#txtFechaPago").val().trim();
	var tipoCarge = $(":radio[name='radTipoCarge']:checked").val();
	
    var formDatos = new FormData(); 
    //Obtener los datos de los archivos
    var inputFileImage = document.getElementById("filArchivo");
    
    var contadorArchivo = 0;
    $.each(inputFileImage.files,function(i,row){
    	
    	if(arrFileCargado.indexOf(row.name)==-1 && contadorArchivo<=1){
    		arrFileBanera[arrFileBanera.length] = row.name;
    		formDatos.append('filArchivo'+i,row);
    		contadorArchivo++;
    	}
    	
    });
    
    //Limpiar el input file
    if(contadorArchivo==0){
    	document.getElementById("filArchivo").value = "";
    }
   
    formDatos.append("id_proceso",idProceso);
    formDatos.append("accion",accion);
    formDatos.append("fecha_pago",fechaPago);
    formDatos.append("tipo_carge",tipoCarge);
   
    return formDatos;
}

function descargar(idProceso){
	var datosRetorno = false;
	var caso = "archivo_plano";
	
	var data = {caso:caso,id_proceso:idProceso};
	
	$.ajax({
		url:urlPlanoTesoreria,
		type:"POST",
		data:data,
		dataType:"json",
		async:false,
		success:function(datos){
			if( parseInt( datos ) === 1 ){
				datosRetorno = true;
			}else{
				datosRetorno = false;
			}
		}
	});
	
	if(datosRetorno){
		window.open(urlDescargarArchivo);
	}else{
		alert("Error al generar el reporte");
	}
	
}