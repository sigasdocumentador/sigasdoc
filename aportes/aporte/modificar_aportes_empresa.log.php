<?php
date_default_timezone_set('America/Bogota');
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

$case = $_POST['v0'];
switch($case){
	case 'CONSULTAR_EMPRESA':
		$data = array('idEmpresa'=>0,'razonSocial'=>'','error'=>0,'descripcion'=>'');
		$nit  = $_POST['v1'];
		$sql  = "SELECT idempresa, razonsocial FROM aportes048 WHERE nit='$nit'";
		$rs   = $db->querySimple($sql);
		$row = $rs->fetch();
		if(!$row['idempresa']>0){
			$data['error'] = 1;
			$data['descripcion'] = "<label style='color:red;'>La empresa no existe<label>";
		}else{
			$data['idEmpresa']=$row['idempresa'];
			$data['razonSocial'] = $row['razonsocial'];
		}
		echo json_encode($data);
	break;
	case 'BUSCAR_APOSTES':
		$idEmpresa = $_POST['v1'];
		$periodo = $_POST['v2'];
		$data = array('aportes'=>array(),'error'=>0,'mensaje'=>'');
		$sql = "SELECT idaporte,valornomina, valoraporte,fechapago,periodo,indicador FROM Aportes011 WHERE idempresa=$idEmpresa AND periodo='$periodo'";
		$rs = $db->querySimple($sql);
		$cont = 0;
		while($row = $rs->fetch()){
			$data['aportes'][] =$row;
			$cont=1;
		}
		if($cont==0){
			$data['error'] = 1;
			$data['descripcion'] = "La empresa no tiene aportes";
		}
		echo json_encode($data);
	break;
	case 'MODIFICAR':
		$idAporte  = $_POST['v1'];
		$indicador = $_POST['v2'];
		$valorA    = $_POST['v3'];
		$valorN    = $_POST['v4'];
		$fechaP    = $_POST['v5'];
		$periodo   = $_POST['v6'];
		$caja      = $_POST['v7'];
		$icbf      = $_POST['v8'];
		$sena      = $_POST['v9'];
		$observacion = $_POST['v10'];
		$sql="update aportes011 set valoraporte=$valorA, valornomina=$valorN,  fechapago='$fechaP',periodo='$periodo',indicador='$indicador',caja=$caja, icbf=$icbf, sena=$sena where idaporte = $idAporte";
		$rs = $db->queryActualiza($sql);
		
		if( intval( $rs ) > 0 ){
				$sqlObs = "INSERT INTO aportes042 (idaporte, notas, tipo) VALUES ($idAporte, '$observacion','M')";
				$row = $db->queryInsert( $sqlObs, 'aportes042' );
				echo $rs;
		}else{
			echo 0;
		}		
	break;
}
?>