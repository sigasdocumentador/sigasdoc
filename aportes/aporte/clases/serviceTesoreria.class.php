<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';
//include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'wssdk' . DIRECTORY_SEPARATOR . 'ClientWSInfWeb.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';

include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'utilitario.class.php';
include_once $raiz.DIRECTORY_SEPARATOR.'terceros'.DIRECTORY_SEPARATOR.'BaseTerceros.php';
include_once $raiz.DIRECTORY_SEPARATOR.'ContabilidadGF'.DIRECTORY_SEPARATOR.'BaseContabilidad.php';


class ServiceTesoreria{
	private $idProceso;
	private $idTrazaProceso;
	private $usuario;
	private $objPlanoTesoreria;
	private $objLogProceso;
	private $objInterfaz;
	
	function __construct(){
		//$this->objInterfaz = new ClientWSInfWeb ( USUARIO_WEB_SIGAS, CONTRASENA_WEB_SIGAS );
	}
	
	public function getIdProceso(){return $this->idProceso;}
	public function setIdProceso($idProceso){$this->idProceso = $idProceso;}
	
	public function getIdTrazaProceso(){return $this->idTrazaProceso;}
	public function setIdTrazaProceso($idTrazaProceso){$this->idTrazaProceso = $idTrazaProceso;}
	
	public function getUsuario(){return $this->usuario;}
	public function setUsuario($usuario){$this->usuario = $usuario;}
	
	public function getObjPlanoTesoreria(){return $this->objPlanoTesoreria;}
	public function setObjPlanoTesoreria($objPlanoTesoreria){$this->objPlanoTesoreria = $objPlanoTesoreria;}
	
	public function getObjLogProceso(){return $this->objLogProceso;}
	public function setObjLogProceso($objLogProceso){$this->objLogProceso = $objLogProceso;}
	
	/**
	 * Metodo encargado de procesar los datos del servicio
	 * 
	 * @return number [0:Error, 1:ok]
	 */
	public function procesar_datos(){
		$estado = 'PROCESO';
		
		$estadoProcesoA = $this->procesar_tesoreria();
		if($estadoProcesoA=='FIN_EXITOSO'){
			$estado = 'FIN_EXITOSO';
		}else if($estadoProcesoA=='FIN_ERROR'){
			$estado = 'FIN_ERROR';
		}
		
		return $estado;
	}
	
	private function procesar_tesoreria(){
                @session_start();
                $contabilidad = new BaseContabilidad();
                $anos_incremento=array();
                $consecutivo_batch = "";
                
		$this->objPlanoTesoreria->setIdProceso($this->idProceso);
		$estado = $this->objPlanoTesoreria->estado_interfaze();
		
		if($estado=='FIN_EXITOSO' || $estado=='FIN_ERROR'){
                    $respuesta = $contabilidad->enviarMovimientoJDE($_SESSION["numeroComprobante"]);
                    
                    $_SESSION["numeroComprobante"]  = "";
                    $_SESSION["numeroLinea"] = "";

                    $contabilidad->incrementar_anos($anos_incremento);
                    $contabilidad->actualizar_consecutivo_batch();
                    $contabilidad->actualizar_consecutivo_anio();
                    return $estado;
		}else{
			$estado = 'PROCESO';
                        if(!isset($_SESSION["numeroComprobante"])){
                            $_SESSION["numeroComprobante"] = "";
                        }
                        
                        if($_SESSION["numeroComprobante"] != "" && $_SESSION["numeroComprobante"] != null && $_SESSION["numeroComprobante"] != "null" && isset($_SESSION["numeroComprobante"])){
                            $consecutivo_batch = $_SESSION["numeroComprobante"];
                            $numeroLinea = $_SESSION["numeroLinea"];
                        }else{
                            $consecutivo_batch = $contabilidad->ObtenerConsecutivoBatch();
                            $numeroLinea = 1;
                            $_SESSION["numeroLinea"] = $numeroLinea;
                            $_SESSION["numeroComprobante"] = $consecutivo_batch;
                        }
		}
		$arrDatos = $this->objPlanoTesoreria->fetch_datos_interfaz(50);
		//var_dump($arrDatos);exit();
		$tranNumber=1;
		foreach($arrDatos as $row){
			//$tranNumber++;
			try{
				$banderaError = 'N';
				//$this->nit = $row["ident_aport_tipo6_db"];
				//$this->razonSocial = $row["razon_social"];
				
				
				
				
				// EXISTENCIA MOVIMIENTO
				
				//if($row["bande_exist_aport_nomin_tipo6_db"]>0){
					
				//$a = $this->existe_movimiento($row["ident_aport_tipo6_db"], $row["fecha_recau_tipo1"],  $row["valor_plani_tipo6"]);
				//var_dump($a);exit();
				
				//OJO SI DA -1 HAY ERROR AL CONSULTAR EL NUMERO RECIBO
				//0 SI NO EXISTE, >0 SI EXISTE
					/*		
					$numeroMovimiento = $this->existe_movimiento($row["ident_aport_tipo6_db"], $row["fecha_recau_tipo1"],  $row["valor_plani_tipo6"]);
					
					if($numeroMovimiento>0 && intval($numeroMovimiento)==intval($row["numero_recibo_db"])){
						$descripcion = 'Error El aporte ya esta ingresado en Informa web';
						$data = '{"id_plano_tesoreria":"' . $row["id_plano_tesoreria"] . '","nombre_archivo":"' . $row["nombrearchivo"] . '","planilla":"' . $row["numer_plani_liqui_tipo6"] . '","periodo":"' . $row["perio_pago_tipo6"] . '","nit":"' . $row["ident_aport_tipo6_db"] . '","razon_social":"' . $row["nombr_aport_tipo6"] . '","fecha_pago":"' . $row["fecha_recau_tipo1"] . '"}';
						$this->objLogProceso->guardar_log_db($this->idTrazaProceso,'APORTE','E',$descripcion, $data,'ERROR');
						
						$banderaError = "S";
						//GOTO CONTINUAR
						goto CONTINUAR;
						
					}else if($numeroMovimiento<0){
						$descripcion = 'Error Al comprobar si existe el movimiento';
						$data = '{"id_plano_tesoreria":"' . $row["id_plano_tesoreria"] . '","nombre_archivo":"' . $row["nombrearchivo"] . '","planilla":"' . $row["numer_plani_liqui_tipo6"] . '","periodo":"' . $row["perio_pago_tipo6"] . '","nit":"' . $row["ident_aport_tipo6_db"] . '","razon_social":"' . $row["nombr_aport_tipo6"] . '","fecha_pago":"' . $row["fecha_recau_tipo1"] . '"}';
						$this->objLogProceso->guardar_log_db($this->idTrazaProceso,'APORTE','E',$descripcion, $data,'ERROR');
						
						$banderaError = "S";
						//GOTO CONTINUAR
						goto CONTINUAR;
						
					}else if($row["bande_exist_aport_nomin_tipo6_db"]>0){
						$descripcion = 'Error El aporte esta ingresado en SIGAS pero no en Informa web';
						$data = '{"id_plano_tesoreria":"' . $row["id_plano_tesoreria"] . '","nombre_archivo":"' . $row["nombrearchivo"] . '","planilla":"' . $row["numer_plani_liqui_tipo6"] . '","periodo":"' . $row["perio_pago_tipo6"] . '","nit":"' . $row["ident_aport_tipo6_db"] . '","razon_social":"' . $row["nombr_aport_tipo6"] . '","fecha_pago":"' . $row["fecha_recau_tipo1"] . '"}';
						$this->objLogProceso->guardar_log_db($this->idTrazaProceso,'APORTE','E',$descripcion, $data,'ERROR');
						
						$banderaError = "S";
						//GOTO CONTINUAR
						goto CONTINUAR;
					}
					*/
					//$banderaError = "S";
					//GOTO CONTINUAR
					//goto CONTINUAR;
				//}
				
				//EXISTENCIA EMPRESA EN INFORMA WEB
				$nit=$row["ident_aport_tipo6_db"];
				$nit_digito=$contabilidad->ObtenerDigitoNit($nit);//cual es el digito ? no digito
				
				if(!$this->existe_empresa_interfaz($nit,$nit_digito)){
					$descripcion = 'Error La empresa no existe en JD Edwards';
					$data = '{"id_plano_tesoreria":"' . $row["id_plano_tesoreria"] . '","nombre_archivo":"' . $row["nombrearchivo"] . '","planilla":"' . $row["numer_plani_liqui_tipo6"] . '","periodo":"' . $row["perio_pago_tipo6"] . '","nit":"' . $nit . '|'.$nit_digito.'","razon_social":"' . $row["nombr_aport_tipo6"] . '","fecha_pago":"' . $row["fecha_recau_tipo1"] . '"}';
					$this->objLogProceso->guardar_log_db($this->idTrazaProceso,'APORTE','E',$descripcion, $data,'ERROR');
					
					$banderaError = "S";
					//GOTO CONTINUAR
					goto CONTINUAR;
				}
				
				//GUARDAR MOVIMIENTO
				$cadena="Aportes ".$row["perio_pago_tipo6"]." ".$row["ident_aport_tipo6_db"]." ".$row["fecha_recau_tipo1"];
				//$movimientos = array();
				//$movimientos[0] = new Movimiento($row["numero_recibo_db"], new Fecha(substr($row["fecha_recau_tipo1"], 0, 4),substr($row["fecha_recau_tipo1"], 4, 2),substr($row["fecha_recau_tipo1"], 6, 2)), CUENTA_CONTABLE_DEBITO, $row["ident_aport_tipo6_db"], CENTRO_DE_COSTO, intval($row["valor_plani_tipo6"]), "D", $cadena, $this->usuario);
				//$movimientos[1] = new Movimiento($row["numero_recibo_db"], new Fecha(substr($row["fecha_recau_tipo1"], 0, 4),substr($row["fecha_recau_tipo1"], 4, 2),substr($row["fecha_recau_tipo1"], 6, 2)), CUENTA_CONTABLE_CREDITO, $row["ident_aport_tipo6_db"], CENTRO_DE_COSTO, intval($row["valor_plani_tipo6"]), "C", $cadena, $this->usuario);
				//$arrErrorIW = $this->objInterfaz->movimientoGrabarSV(COMPROBANTE_PLANILLA_UNICA, $movimientos);
				$ano_corto=substr($row["fecha_recau_tipo1"], 0, 2);
				if(!in_array($ano_corto,$anos_incremento))
				{
					$anos_incremento[]=$ano_corto;
				}
				$date=substr($row["fecha_recau_tipo1"], 0, 4).substr($row["fecha_recau_tipo1"], 4, 2).substr($row["fecha_recau_tipo1"], 6, 2);
				$periodo=$row["perio_pago_tipo6"];
				
				$amount=intval($row["valor_plani_tipo6"]);
				
				$numeroLinea = $contabilidad->contabilidad_entry($amount,$date,$periodo,$nit,$nit_digito,$ano_corto,$tranNumber,$consecutivo_batch,$this->usuario,$numeroLinea);
                                $numeroLinea++;
				
                                /*
				$error1=$respuesta[0]->getError();
				$error2=$respuesta[1]->getError();
				//var_dump("hola");
				//exit();
				$data = '{"id_plano_tesoreria":"' . $row["id_plano_tesoreria"] . '","nombre_archivo":"' . $row["nombrearchivo"] . '","planilla":"' . $row["numer_plani_liqui_tipo6"] . '","periodo":"' . $row["perio_pago_tipo6"] . '","nit":"' . $row["ident_aport_tipo6_db"] . '","razon_social":"' . $row["nombr_aport_tipo6"] . '","fecha_pago":"' . $row["fecha_recau_tipo1"] . '"}';
				if(isset($error1)||isset($error2)){					
					$descripcion = 'Error Al guardar el movimiento en JD Edwards';
					$this->objLogProceso->guardar_log_db($this->idTrazaProceso,'APORTE','E',$descripcion, $data,'ERROR', $error1.$error2);
					
					$banderaError = "S";
					//GOTO CONTINUAR
					goto CONTINUAR;
				}
				else
				{
					$descripcion = 'Movimiento Procesado en JD Edwards';
					$this->objLogProceso->guardar_log_db($this->idTrazaProceso,'APORTE','E',$descripcion, $data,'EXITO');
				}
                                 * 
                                 */
				//CONTINUAR:
				CONTINUAR:;
				
				//Update registro
				$resultado = $this->objPlanoTesoreria->update_estado($row["id_plano_tesoreria"],$banderaError);
				
			}catch(Exception $e){
				$resultado = $this->objPlanoTesoreria->update_estado($row["id_plano_tesoreria"],'E');
				
				//Guardar Log
				$descripcion = 'Error al procesar los datos en la interfaz del archivo TIPO A';
				$data = '{"id_plano_tesoreria":"' . $row["id_plano_tesoreria"] . '","nombre_archivo":"' . $row["nombrearchivo"] . '","planilla":"' . $row["numer_plani_liqui_tipo6"] . '","periodo":"' . $row["perio_pago_tipo6"] . '","nit":"' . $row["ident_aport_tipo6_db"] . '","razon_social":"' . $row["nombr_aport_tipo6"] . '","fecha_pago":"' . $row["fecha_recau_tipo1"] . '"}';
				$this->objLogProceso->guardar_log_db($this->idTrazaProceso,'EMPRESA','E',$descripcion, $data,'ERROR');
			}
		}
                $_SESSION["numeroLinea"] = $numeroLinea;
		return $estado;
	}
	
	private function existe_movimiento($nit,$fechaPago,$valorAporte){
		$anno=substr($fechaPago,0, 4);
		$mes=substr($fechaPago,4,2);
		$dia=substr($fechaPago, -2);
		
		$fecha=new Fecha($anno,$mes,$dia);
		$comprobante='CPU';
		
		return  $this->objInterfaz->movimientoNumeroStr($comprobante, $nit, $fecha, $valorAporte);
	}
	
	private function existe_empresa_interfaz($nit,$nit_digito) {
		$nit=Utilitario::format_nit($nit);
		$terceros=new BaseTerceros();
		if($nit_digito!=""){
			return $terceros->ExisteTercero($nit."-".$nit_digito);
		}
		else{
			return $terceros->ExisteTercero($nit);
		}
	}
	
	/**
	* Retorna los valores obtenidos en la consulta
	* @return multitype:array
	*/
	private function fetchConsulta($querySql){
		$resultado = array();
		$rs = self::$con->querySimple($querySql);
		while($row = $rs->fetch())
			$resultado[] = array_map("trim",$row);
		//$resultado[] = array_map("utf8_encode",$row);
		return $resultado;
	}
}

?>