<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

//include_once $raiz.DIRECTORY_SEPARATOR.'phpComunes'.DIRECTORY_SEPARATOR.'wssdk'.DIRECTORY_SEPARATOR.'ClientWSInfWeb.php';

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';

include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'utilitario.class.php';

include_once $raiz . DIRECTORY_SEPARATOR . 'aportes' . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'maestro.proceso.class.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'aportes' . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'log.proceso.class.php';

include_once 'planoTesoreria.class.php';
include_once 'serviceTesoreria.class.php';

class MaestroTesoreria{

	private $idTrazaProceso = 0;
	private $idProceso = 0;
	private $fechaPago = "";
	private $tipoCarge = null;
	private $arrFiles = array();
	
	/*************************************
	 * ATTR para el carge de los archivos
	 *************************************/
	
	/**
	 * Puede ser: ARCHIVO, EMPRESA, AFILIADO 
	 * @var unknown_type
	 */
	private $codigoError = "";
	
	/**
	 * Los indices de array son: nombre del error, [nombre del archivo plano], [informacion en formato json] 
	 * @var unknown_type
	 */
	private $arrError = array("error"=>0,"descripcion"=>"");
	
	private $objMaestroProceso = null;
	private $tipoProceso = "KARDEX";
	private $usuario = null;
	private $rutaCargados;
	
	private $bandeEtapaProceTermi = null;
	
	private $objLogProceso = null;
	
	private static $con = null;
	
	private $objService;
	
	function __construct(){
		
		$this->objMaestroProceso = new MaestroProceso();
		$this->objMaestroProceso->setTipo($this->tipoProceso);
		
		$this->objLogProceso = new LogProceso();
	
		//$this->objService = new ClientWSInfWeb(USUARIO_WEB_SIGAS, CONTRASENA_WEB_SIGAS);		
		
		//Conexion
		try{
			self::$con = IFXDbManejador::conectarDB();
			if( self::$con->conexionID==null ){
				throw new Exception(self::$con->error);
			}
		}catch(Exception $e){
			echo $e->getMessage();
			exit();
		}
	}
	
	public function ejecutar($idProceso = 0, $usuario, $fechaPago, $reprocesar=false, $rutaCargados, $tipoCarge,$arrFiles){
		$this->idProceso = $idProceso;
		$this->fechaPago = $fechaPago;
		$this->usuario = $usuario;
		$this->rutaCargados = $rutaCargados;
		$this->tipoCarge = $tipoCarge;
		$this->arrFiles = $arrFiles;
	
		$this->objMaestroProceso->setIdProceso($this->idProceso);
		$this->objMaestroProceso->setUsuario($this->usuario);
		$this->objMaestroProceso->setRutaCargados($this->rutaCargados);
	
		$this->objLogProceso->setUsuario($this->usuario);
	
		if($reprocesar==false){
			$this->procesar();
		}else{
			$this->reprocesar();
		}
	
		return $this->arrError;
	}
	
	private function procesar(){
		
		//Verificar si es un nuevo proceso
		if($this->idProceso==0){
			//iniciar nuevo proceso
			$arrResultado = $this->objMaestroProceso->iniciar_proceso();
			
			if($arrResultado["error"]==1){
				//Error
				$this->arrError["error"] = 1;
				$this->arrError["descripcion"] = $arrResultado["descripcion"];
			}else{
				
				//Resetear procesos antiguos
				$resultado = $this->ejecutar_sp_resetear_pu();
				if($resultado===false){
					$descripcion = 'Error al resetear los procesos anteriores';
					$this->arrError["error"] = 1;
					$this->arrError["descripcion"] = $descripcion;
					
					//Error al ejecutar el proceso
					$this->guardar_log_db($this->idTrazaProceso,'RESETEAR_DATOS_ANTIGUOS','A',$descripcion, null,'ERROR');
				}else{
					$this->arrError["error"] = 0;
					$this->arrError["descripcion"] = $arrResultado["descripcion"];
					$this->idProceso = $arrResultado["data"]["id_proceso"];
				}
			}
		}
		
		if($this->idProceso>0){
			
			$this->procesar_etapa_proceso();
			
		}else if($this->arrError["error"] == 0){
			$this->arrError["error"] = 1;
			$this->arrError["descripcion"] = "Error: Inesperado metodo: MaestroTesoreria->procesar()";
		}
	}
	
	private function reprocesar(){
		//iniciar nuevo proceso
		$arrResultado = $this->objMaestroProceso->iniciar_reproceso();
			
		if($arrResultado["error"]==1){
			//Error
			$this->arrError["error"] = 1;
			$this->arrError["descripcion"] = $arrResultado["descripcion"];
		}else{
			$this->arrError["error"] = 0;
			$this->arrError["descripcion"] = $arrResultado["descripcion"];
			$this->idProceso = $arrResultado["data"]["id_proceso"];
		}
	}
	
	private function procesar_etapa_proceso(){
		$arrDataTrazaProce = $this->objMaestroProceso->iniciar_etapa_proceso();
		
		if($arrDataTrazaProce["error"]==0){
			
			$idEtapaProceso = $arrDataTrazaProce["data"]["id_etapa_proceso"];
			$etapaProceso = $arrDataTrazaProce["data"]["etapa_proceso"];
			$idTrazaProceso = $arrDataTrazaProce["data"]["id_traza_proceso"];
			
			$this->idTrazaProceso = $idTrazaProceso;
			
			$this->arrError["error"] = 0;
			$this->arrError["descripcion"] = "";
			
			$banderaProceTermi = false;
			
			//Procesar las etapas--------------------------------------------------------
			switch ($etapaProceso){
				case "inicio": //Se inicio el proceso
					$banderaProceTermi = true;
					break;
				case "subir_archivo": //Cargar los archivos planos al servidor desde el FTP
					
					$this->subir_archivo();
					//Verificar si el proceso se completo
					if($this->bandeEtapaProceTermi === true){
						$banderaProceTermi = true;
					}
					
					break;
				case "preparar_archivo": //Preparar los archivos planos
					
					$this->preparar_archivo($idEtapaProceso);
					//Verificar si el proceso se completo
					if($this->bandeEtapaProceTermi === true){
						$banderaProceTermi = true;
					}
					
					break;
				case "procesar_dato_sigas": //Procesar la informacion en la Base de datos de SIGAS
					
					$this->procesar_dato_sigas();
					//Verificar si el proceso se completo
					if($this->bandeEtapaProceTermi === true){
						$banderaProceTermi = true;
					}
					
					break;
				case "procesar_dato_service": //Procesar la informacion en la Base de datos externa
					
					$this->procesar_dato_service();
					//Verificar si el proceso se completo
					if($this->bandeEtapaProceTermi === true){
						$banderaProceTermi = true;
					}
					
					break;
				case "log_proceso": //Logs del proceso
					
					$this->log_proceso();
					//Verificar si el proceso se completo
					if($this->bandeEtapaProceTermi === true){
						$banderaProceTermi = true;
					}
					
					break;
				case "fin": //Se termino el proceso
					
					$banderaProceTermi = true;
					
					break;
			}
			
			//Finalizar etapa proceso exitoso
			if($banderaProceTermi === true){
				$procesado = $this->arrError["error"]==0 ? "S" : "E"; //[S]Fin exitoso - [E]Fin error  
				
				//Terminar la etapa proceso
				$arrResultado = $this->objMaestroProceso->terminar_etapa_proceso($idTrazaProceso,$procesado,$etapaProceso);
				
				if($arrResultado["error"]==0){
					
					$descripcion = 'La etapa proceso se termino correctamente';
					$this->arrError["error"] = 0;
					$this->arrError["descripcion"] = $descripcion;
					$this->arrError["data"]["etapa_proceso"] = $etapaProceso;
					
					$this->guardar_log_db($this->idTrazaProceso,'PROCESAR_ETAPA','A',$descripcion, null,'EXITO');
				}else{
					$this->arrError["error"] = 1;
					$this->arrError["descripcion"] = $arrDataTrazaProce["descripcion"];
					
					$this->guardar_log_db($this->idTrazaProceso,'PROCESAR_ETAPA','A',$arrDataTrazaProce["descripcion"],null,'ERROR');
				}
				
			}else if($this->arrError["error"]==0){
				$this->arrError["error"] = 0;
				$this->arrError["descripcion"] = "La etapa esta en proceso";
				$this->arrError["data"]["etapa_proceso"] = $etapaProceso;
			}
			
		}else{
			$this->arrError["error"] = 1;
			$this->arrError["descripcion"] = $arrDataTrazaProce["descripcion"];
			
			$this->guardar_log_db($this->idTrazaProceso,'PROCESAR_ETAPA','A',$arrDataTrazaProce["descripcion"],null,'ERROR');
		}
	}
	
	

	/**
	 * Carge Archivo Dir PHP (php) 
	 */
	private function subir_archivo(){
		
		$this->bandeEtapaProceTermi = false;
		
		$uriPlanoCargado = $this->rutaCargados . 'tesoreria' . DIRECTORY_SEPARATOR . 'cargados' . DIRECTORY_SEPARATOR;
		
		//Cargar archivos manualmente
		if($this->tipoCarge=="CARGE"){
			
			$resultadoCarge = $this->objMaestroProceso->carga_archivo_manual($uriPlanoCargado,"txt", array("txt","text","text/plain"),$this->arrFiles);
			
			//Verificar si los archivos se cargaron correctamente
			if($resultadoCarge==false){
				$descripcion = 'Error al cargar los archivos al servidor';
				$this->arrError["error"] = 0;
				$this->arrError["descripcion"] = $descripcion;
				$this->bandeEtapaProceTermi = true;
					
				$this->guardar_log_db($this->idTrazaProceso,'CARGAR_ARCHIVO','A',$descripcion, null,'ERROR');
			
			}else if(count($this->arrFiles)==0){
				//El proceso termino correctamente
				$descripcion = 'Los Archivos se cargaron correctamente al servidor';
				$this->arrError["error"] = 0;
				$this->arrError["descripcion"] = $descripcion;
				$this->bandeEtapaProceTermi = true;
					
				$this->guardar_log_db($this->idTrazaProceso,'CARGAR_ARCHIVO','A',$descripcion, null,'EXITO');
			}
			
		}		
		//Falta configurar el FTP		
			
	}
	
	private function preparar_archivo($idEtapaProceso){		
		$this->bandeEtapaProceTermi = false;
		
		$uriPlanoCargado = $this->rutaCargados . 'tesoreria' . DIRECTORY_SEPARATOR . 'cargados' . DIRECTORY_SEPARATOR;
		$uriPlanoProcesado = $this->rutaCargados . 'tesoreria' . DIRECTORY_SEPARATOR . 'procesados' . DIRECTORY_SEPARATOR;
		
		$arrArchivos = $this->objMaestroProceso->fetch_archivo_cargado($uriPlanoCargado);
		
		if(count($arrArchivos)==0){
			$descripcion = 'Error no se encontraron archivos para preparar';
			$this->arrError["error"] = 1;
			$this->arrError["descripcion"] = $descripcion;
				
			$this->guardar_log_db($this->idTrazaProceso,'ARCHIVO','A',$descripcion, null,'ERROR');
			return;
		}
		
                /*
		//Obtener datos del cierre contable
		$bandeFechaCierrContaError = "N";
		$arrCierreContrable = $this->objService->ultimoCierreContable();
		if($arrCierreContrable["Tipo"]=='Error'){
			$bandeFechaCierrContaError = "S";
			$descripcion = 'Error al obtener los datos del cierre contable';
			$this->arrError["error"] = 1;
			$this->arrError["descripcion"] = $descripcion;
			
			$this->guardar_log_db($this->idTrazaProceso,'ARCHIVO','A',$descripcion, null,'ERROR');
			
			return;
		}
		$fechaCierreContable = $arrCierreContrable["valor"];
                 * /
                 */
		$fechaCierreContable = '20150201';
		$bandeFechaCierrContaError = "N";
		
		$banderaSizeFile = 0;
		$contArchivo = 0;
		foreach ($arrArchivos as $archivo){
			
			$sizeArchivo = 0;
			//Datos generales del archivo
			$nombreArchivo = $archivo;
			
			//Directorios
			$uriPlanoCargaActua = $uriPlanoCargado . $nombreArchivo;
			
			//Obtener las lineas del archivo
			$arrLineaArchivo = file($uriPlanoCargaActua);
				
			$fechaArchivo = substr ( $arrLineaArchivo[0], 1, 8 );
			$annoArchivo = substr ( $fechaArchivo, 0, 4 );
			$mesArchivo = substr ( $fechaArchivo, 4, 2 );
			$diaArchivo = substr ( $fechaArchivo, 6, 2 );
			
			$uriPlanoProceActua = $uriPlanoProcesado .  $annoArchivo . DIRECTORY_SEPARATOR . $mesArchivo . DIRECTORY_SEPARATOR . $diaArchivo . DIRECTORY_SEPARATOR;
			
			//Obtener el tama�o del archivo
			$sizeArchivo = filesize($uriPlanoCargaActua);
			$banderaSizeFile += $sizeArchivo;
			$contArchivo++;
			//Cortar el proceso para no colgar la carga del servidor
			if($banderaSizeFile>1500000 || $contArchivo>100){
				break;
			}
			
			//Verificar directorio
			$bandeVerifDirec = $this->objMaestroProceso->verifica_directorio( $uriPlanoProceActua );
			
			if ($bandeVerifDirec !== true) {
				//El directorio no fue posible crearlo
				$descripcion = 'Error el directorio para procesar los archivos no fue posible crearlo';
				$this->arrError["error"] = 1;
				$this->arrError["descripcion"] = $descripcion;
					
				$data = '{"nombre_archivo":"'.$nombreArchivo.'"}';
				$this->guardar_log_db($this->idTrazaProceso,'DIRECTORIO','C',$descripcion, $data,'ERROR');
				break;
			}
			
			//Guardar los datos del archivo
			$idArchivo = $this->objMaestroProceso->guardar_archivo($nombreArchivo, $uriPlanoProceActua, '', $sizeArchivo, 'S','KAR');
			
			if($idArchivo==0){
				//El registro del archivo no se pudo guardar
				$descripcion = 'Error el registro del archivo no se pudo guardar';
				$this->arrError["error"] = 1;
				$this->arrError["descripcion"] = $descripcion;
				
				$data = '{"nombre_archivo":"'.$nombreArchivo.'"}';
				$this->guardar_log_db($this->idTrazaProceso,'ARCHIVO','C',$descripcion, $data,'ERROR');
				break;
			}

			$objPlanoTesoreria2 = new PlanoTesoreria();
			
			//Recorrer las lineas del archivo
			foreach($arrLineaArchivo as $lineaArchivo){
				
				$objPlanoTesoreria = new PlanoTesoreria();
				
				$tipoRegistro = intval(trim(substr( $lineaArchivo, 0, 1 )));

				$objPlanoTesoreria->setIdProceso( $this->idProceso );
				$objPlanoTesoreria->setIdEtapaProceso( $idEtapaProceso );
				$objPlanoTesoreria->setIdArchivo( $idArchivo );
				$objPlanoTesoreria->setError( 'N' );
				$objPlanoTesoreria->setEstado( 'PROCESADO' );
				$objPlanoTesoreria->setUsuario( $this->usuario );
				
				if($tipoRegistro==1){
					
					$objPlanoTesoreria2->setIdProceso( $this->idProceso );
					$objPlanoTesoreria2->setIdEtapaProceso( $idEtapaProceso );
					$objPlanoTesoreria2->setIdArchivo( $idArchivo );
					$objPlanoTesoreria2->setError( 'N' );
					$objPlanoTesoreria2->setEstado( 'PROCESADO' );
					$objPlanoTesoreria2->setUsuario( $this->usuario );
					
					$objPlanoTesoreria2->setTipoRegisTipo1( trim(substr( $lineaArchivo, 0, 1 )));
					$objPlanoTesoreria2->setFechaRecauTipo1( trim(substr( $lineaArchivo, 1, 8 )));
					$objPlanoTesoreria2->setCodigEntidFinanTipo1( trim(substr( $lineaArchivo, 9, 3 )));
					$objPlanoTesoreria2->setNitAdminTipo1( trim(substr( $lineaArchivo, 12, 15 )));
					$objPlanoTesoreria2->setNombrAdminTipo1( utf8_encode(trim(substr( $lineaArchivo, 27, 22 ))));
					$objPlanoTesoreria2->setReservadoTipo1( trim(substr( $lineaArchivo, 49, 71 )));
				
				}else if($tipoRegistro==5){
					
					$objPlanoTesoreria2->setTipoRegisTipo5( trim(substr( $lineaArchivo, 0, 1 )));
					$objPlanoTesoreria2->setNumerCuentTipo5( trim(substr( $lineaArchivo, 1, 17 )));
					$objPlanoTesoreria2->setTipoCuentTipo5( trim(substr( $lineaArchivo, 18, 2 )));
					$objPlanoTesoreria2->setNumerLoteTipo5( trim(substr( $lineaArchivo, 20, 2 )));
					$objPlanoTesoreria2->setSistePagoTipo5( trim(substr( $lineaArchivo, 22, 2 )));
					$objPlanoTesoreria2->setReservadoTipo5( trim(substr( $lineaArchivo, 24, 96 )));
				
				}else if($tipoRegistro==6){
					
					$objPlanoTesoreria->setTipoRegisTipo6( trim(substr( $lineaArchivo, 0, 1 )));
					$objPlanoTesoreria->setIdentAportTipo6( trim(substr( $lineaArchivo, 1, 16 )));
					$objPlanoTesoreria->setNombrAportTipo6( utf8_encode(trim(substr( $lineaArchivo, 17, 16 ))));
					$objPlanoTesoreria->setCodigBancoAutorTipo6( trim(substr( $lineaArchivo, 33, 8 )));
					$objPlanoTesoreria->setNumerPlaniLiquiTipo6( trim(substr( $lineaArchivo, 41, 15 )));
					$objPlanoTesoreria->setPerioPagoTipo6( trim(substr( $lineaArchivo, 56, 6 )));
					$objPlanoTesoreria->setCanalPagoTipo6( trim(substr( $lineaArchivo, 62, 2 )));
					$objPlanoTesoreria->setNumerRegisTipo6( trim(substr( $lineaArchivo, 64, 6 )));
					$objPlanoTesoreria->setCodigOperaInforTipo6( trim(substr( $lineaArchivo, 70, 2 )));
					$objPlanoTesoreria->setValorPlaniTipo6( trim(substr( $lineaArchivo, 72, 16 )));
					$objPlanoTesoreria->setHoraMinutTipo6( trim(substr( $lineaArchivo, 90, 4 )));
					$objPlanoTesoreria->setNumerSecueTipo6( trim(substr( $lineaArchivo, 94, 6 )));
					$objPlanoTesoreria->setReservadoTipo6( trim(substr( $lineaArchivo, 100, 20 )));
					
					$objPlanoTesoreria->setFechaCierrContaDB( $fechaCierreContable );
					$objPlanoTesoreria->setBandeFechaCierrContaErrorDB( $bandeFechaCierrContaError );
									
					$resultado = $objPlanoTesoreria->guardar_tipo6();
					if($resultado==0){
						//Guardar el log de error
						//No se fue posible preparar la linea del archivo
						$descripcion = 'Error no fue posible preparar la linea del archivo';
						$data = '{"nombre_archivo":"'.$nombreArchivo.'","tipo_identificacion":"NIT","numero_identificacion":"'.$objPlanoTesoreria->getIdentAportTipo6().'","nombre":"'.$objPlanoTesoreria->getNombrAportTipo6().'"}';
						$this->guardar_log_db($this->idTrazaProceso,'EMPRESA','B',$descripcion, $data,'ERROR');
					}
					
				}else if($tipoRegistro==8){
					
					$objPlanoTesoreria2->setTipoRegisTipo8( trim(substr( $lineaArchivo, 0, 1 )));
					$objPlanoTesoreria2->setNumerPlaniTipo8( trim(substr( $lineaArchivo, 1, 6 )));
					$objPlanoTesoreria2->setNumerRegisTipo8( trim(substr( $lineaArchivo, 7, 6 )));
					$objPlanoTesoreria2->setValorRecauTipo8( trim(substr( $lineaArchivo, 13, 16 )));
					$objPlanoTesoreria2->setReservadoTipo8( trim(substr( $lineaArchivo, 31, 89 )));
					
				}else if($tipoRegistro==9){
					
					$objPlanoTesoreria2->setTipoRegisTipo9( trim(substr( $lineaArchivo, 0, 1 )));
					$objPlanoTesoreria2->setNumerTotalPlaniTipo9( trim(substr( $lineaArchivo, 1, 8 )));
					$objPlanoTesoreria2->setNumerTotalRegisTipo9( trim(substr( $lineaArchivo, 9, 8 )));
					$objPlanoTesoreria2->setValorTotalRecauTipo9( trim(substr( $lineaArchivo, 17, 16 )));
					$objPlanoTesoreria2->setNumerTotalLotesTipo9( trim(substr( $lineaArchivo, 35, 6 )));
					$objPlanoTesoreria2->setReservadoTipo9( trim(substr( $lineaArchivo, 41, 79 )));
					
				}
			}
			
			//Update archivo tipo I de los registros tipo1, tipo5, tipo8 y tipo9 
			
			$resultado = $objPlanoTesoreria2->update();
				
			if($resultado==0){
				
				$descripcion = 'Error no fue posible preparar el tipo registro 1, 5, 8 y 9 del archivo Plano';
				$data = '{"nombre_archivo":"'.$nombreArchivo.'"}';
				$this->guardar_log_db($this->idTrazaProceso,'PREPARAR_ARCHIVO','C',$descripcion, $data,'ERROR');
			}
			
			//Mover el archivo del directorio Cargados a Procesados
			$this->objMaestroProceso->trasladar_archivo($uriPlanoCargaActua, $uriPlanoProceActua . $nombreArchivo);					
		}
		
		//Comprobar si existen archivos en el directorio cargado
		$banderaArchivo = count($this->objMaestroProceso->fetch_archivo_cargado($uriPlanoCargado));
		if($this->arrError["error"]==0 && $banderaArchivo==0){
			
			$descripcion = 'Los archivos se prepararon correctamente';
			$this->arrError["error"] = 0;
					
			$this->arrError["descripcion"] = $descripcion;
			$this->bandeEtapaProceTermi = true;
			
			$this->guardar_log_db($this->idTrazaProceso,'ARCHIVO','A',$descripcion, null,'EXITO');
		}
	}
	
	private function procesar_dato_sigas(){		
		$this->bandeEtapaProceTermi = false;
		
		//$this->bandeEtapaProceTermi = true;
		//return;
		
		$objPlanoTesoreria = new PlanoTesoreria();
		$objPlanoTesoreria->setIdProceso($this->idProceso);
		$objPlanoTesoreria->setUsuario($this->usuario);
		
		$estadoProceso = $objPlanoTesoreria->procesar_datos();
		if($estadoProceso=='FIN_EXITOSO'){
			
			$descripcion = 'Los datos se procesaron correctamente en la DB';
			$this->arrError["error"] = 0;
			$this->arrError["descripcion"] = $descripcion;
			$this->bandeEtapaProceTermi = true;
			
			$this->guardar_log_db($this->idTrazaProceso,'DATOS_SIGAS','A',$descripcion, null,'EXITO');
			
		}else if($estadoProceso=='FIN_ERROR'){
			
			$descripcion = 'Existen algunas inconsistencias. Pero las etapa proceso termino correctamente';
			$this->arrError["error"] = 1;
			$this->arrError["descripcion"] = $descripcion;
			$this->bandeEtapaProceTermi = true;
				
			$this->guardar_log_db($this->idTrazaProceso,'DATOS_SIGAS','A',$descripcion, null,'EXITO');
			
		}else if($estadoProceso!='PROCESO'){
			//Error al ejecutar el proceso
			
			$descripcion = 'Error al procesar los datos en sigas';
			$this->arrError["error"] = 1;
			$this->arrError["descripcion"] = $descripcion;
			$this->guardar_log_db($this->idTrazaProceso,'PROCESAR_DATOS_SIGAS','A',$descripcion, null,'ERROR');
		}
	}
	
	private function procesar_dato_service(){
		//$this->bandeEtapaProceTermi = true;
		//return;
		
		$this->bandeEtapaProceTermi = false;
		
		
		$objLogProceso = new LogProceso();
		$objLogProceso->setUsuario($this->usuario);
		$objServiceTesoreria = new ServiceTesoreria();
		
		$objServiceTesoreria->setIdProceso($this->idProceso);
		$objServiceTesoreria->setIdTrazaProceso($this->idTrazaProceso);
		$objServiceTesoreria->setUsuario($this->usuario);
		$objServiceTesoreria->setObjPlanoTesoreria(new PlanoTesoreria());
		$objServiceTesoreria->setObjLogProceso($objLogProceso);
		
		$estadoProceso = $objServiceTesoreria->procesar_datos();
		
		if($estadoProceso=='FIN_EXITOSO'){
			$descripcion = 'Los datos se procesaron correctamente en la Interfaz';
			$this->arrError["error"] = 0;
			$this->arrError["descripcion"] = $descripcion;
			$this->bandeEtapaProceTermi = true;
				
			$this->guardar_log_db($this->idTrazaProceso,'DATOS_INTERFAZ','A',$descripcion, null,'EXITO');
				
		}else if($estadoProceso=='FIN_ERROR'){
			$descripcion = 'Existen algunas inconsistencias. Pero las etapa proceso termino correctamente';
			$this->arrError["error"] = 1;
			$this->arrError["descripcion"] = $descripcion;
			$this->bandeEtapaProceTermi = true;
			
			$this->guardar_log_db($this->idTrazaProceso,'DATOS_INTERFAZ','A',$descripcion, null,'EXITO');
			
		}else if($estadoProceso!='PROCESO'){
			
			$descripcion = 'Error al procesar los datos en la Interfaz';
			$this->arrError["error"] = 1;
			$this->arrError["descripcion"] = $descripcion;
				
			//Error al ejecutar el proceso
			$this->guardar_log_db($this->idTrazaProceso,'PROCESAR_DATOS_INTERFAZ','A',$descripcion, null,'ERROR');
		}
	}
	
	private function log_proceso(){
		$this->bandeEtapaProceTermi = true;
		return;
	}
	
	private function guardar_log_db($idTrazaProceso, $codigo,$tipoLog,$descripcion, $data = null,$estado){
		$this->objLogProceso->guardar_log_db($idTrazaProceso,$codigo,$tipoLog,$descripcion,$data,$estado);
	}
	
	private function ejecutar_sp_resetear_pu(){
		//Remover estado de error para las etapas con error

		$resultado = 0;
		$sentencia = self::$con->conexionID->prepare ( "EXEC [pu].[sp_Resetear_Proceso_PU]
				@id_proceso = $this->idProceso
				, @usuario = '$this->usuario'
				, @resultado = :resultado" );
		$sentencia->bindParam ( ":resultado", $resultado, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000 );
		$sentencia->execute ();
			
		if($resultado==0){			
			return false;
		}
		return true;
	}
}