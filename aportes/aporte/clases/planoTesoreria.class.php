<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';

include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'utilitario.class.php';

class PlanoTesoreria{
	
	private $idPuTipoA;
	private $idProceso;
	private $idEtapaProceso;
	private $idArchivo;
	private $error;
	private $estado;
	private $usuario;
	
	private $tipoRegisTipo1;
	private $fechaRecauTipo1;
	private $codigEntidFinanTipo1;
	private $nitAdminTipo1;
	private $nombrAdminTipo1;
	private $reservadoTipo1;
	
	private $tipoRegisTipo5;
	private $numerCuentTipo5;
	private $tipoCuentTipo5;
	private $numerLoteTipo5;
	private $sistePagoTipo5;
	private $reservadoTipo5;
	
	private $tipoRegisTipo6;
	private $identAportTipo6;
	private $nombrAportTipo6;
	private $codigBancoAutorTipo6;
	private $numerPlaniLiquiTipo6;
	private $perioPagoTipo6;
	private $canalPagoTipo6;
	private $numerRegisTipo6;
	private $codigOperaInforTipo6;
	private $valorPlaniTipo6;
	private $horaMinutTipo6;
	private $numerSecueTipo6;
	private $reservadoTipo6;
	
	private $tipoRegisTipo8;
	private $numerPlaniTipo8;
	private $numerRegisTipo8;
	private $valorRecauTipo8;
	private $reservadoTipo8;
	
	private $tipoRegisTipo9;
	private $numerTotalPlaniTipo9;
	private $numerTotalRegisTipo9;
	private $valorTotalRecauTipo9;
	private $numerTotalLotesTipo9;
	private $reservadoTipo9;
	
	private $numeroReciboDB;
	private $consecutivoDocumentoDB;
	private $fechaCierrContaDB;
	private $bandeFechaCierrContaErrorDB;
	private $bandeAjustTipo6DB;
	private $idEmpreTipo6DB;
	private $bandeExistAportTipo6DB;
	private $bandeExistPuTipo6DB;
	private $bandeEmpreDiferPuTipo6DB;
	private $indicEmpreTipo6DB;
	private $nominaTipo6DB;
	private $bandeExistAportNominTipo6DB;
	private $bandeExistMovimTipo6Sw;
	private $bandeExistEmpreTipo6Sw;
	private $idAportTipo6DB;
	
	private static $con = null;
	
	function __construct(){
		//Conexion
		try{
			self::$con = IFXDbManejador::conectarDB();
			if( self::$con->conexionID==null ){
				throw new Exception(self::$con->error);
			}
		}catch(Exception $e){
			echo $e->getMessage();
			exit();
		}
	}
	
	/*
	 * GETTER
	*/
	
	public function getIdPuTipoA( ){ return $this->idPuTipoA;}
	public function getIdProceso( ){ return $this->idProceso;}
	public function getIdEtapaProceso( ){ return $this->idEtapaProceso;}
	public function getIdArchivo( ){ return $this->idArchivo;}
	public function getError( ){ return $this->error;}
	public function getEstado( ){ return $this->estado;}
	public function getUsuario( ){ return $this->usuario;}
	
	public function getTipoRegisTipo1(){return $this->tipoRegisTipo1; }
	public function getFechaRecauTipo1(){return $this->fechaRecauTipo1; }
	public function getCodigEntidFinanTipo1(){return $this->codigEntidFinanTipo1; }
	public function getNitAdminTipo1(){return $this->nitAdminTipo1; }
	public function getNombrAdminTipo1(){return $this->nombrAdminTipo1; }
	public function getReservadoTipo1(){return $this->reservadoTipo1; }
					
	public function getTipoRegisTipo5(){return $this->tipoRegisTipo5; }
	public function getNumerCuentTipo5(){return $this->numerCuentTipo5; }
	public function getTipoCuentTipo5(){return $this->tipoCuentTipo5; }
	public function getNumerLoteTipo5(){return $this->numerLoteTipo5; }
	public function getSistePagoTipo5(){return $this->sistePagoTipo5; }
	public function getReservadoTipo5(){return $this->reservadoTipo5; }
					
	public function getTipoRegisTipo6(){return $this->tipoRegisTipo6; }
	public function getIdentAportTipo6(){return $this->identAportTipo6; }
	public function getNombrAportTipo6(){return $this->nombrAportTipo6; }
	public function getCodigBancoAutorTipo6(){return $this->codigBancoAutorTipo6; }
	public function getNumerPlaniLiquiTipo6(){return $this->numerPlaniLiquiTipo6; }
	public function getPerioPagoTipo6(){return $this->perioPagoTipo6; }
	public function getCanalPagoTipo6(){return $this->canalPagoTipo6; }
	public function getNumerRegisTipo6(){return $this->numerRegisTipo6; }
	public function getCodigOperaInforTipo6(){return $this->codigOperaInforTipo6; }
	public function getValorPlaniTipo6(){return $this->valorPlaniTipo6; }
	public function getHoraMinutTipo6(){return $this->horaMinutTipo6; }
	public function getNumerSecueTipo6(){return $this->numerSecueTipo6; }
	public function getReservadoTipo6(){return $this->reservadoTipo6; }
					
	public function getTipoRegisTipo8(){return $this->tipoRegisTipo8; }
	public function getNumerPlaniTipo8(){return $this->numerPlaniTipo8; }
	public function getNumerRegisTipo8(){return $this->numerRegisTipo8; }
	public function getValorRecauTipo8(){return $this->valorRecauTipo8; }
	public function getReservadoTipo8(){return $this->reservadoTipo8; }
					
	public function getTipoRegisTipo9(){return $this->tipoRegisTipo9; }
	public function getNumerTotalPlaniTipo9(){return $this->numerTotalPlaniTipo9; }
	public function getNumerTotalRegisTipo9(){return $this->numerTotalRegisTipo9; }
	public function getValorTotalRecauTipo9(){return $this->valorTotalRecauTipo9; }
	public function getNumerTotalLotesTipo9(){return $this->numerTotalLotesTipo9; }
	public function getReservadoTipo9(){return $this->reservadoTipo9; }
					
	public function getNumeroReciboDB(){return $this->numeroReciboDB; }
	public function getConsecutivoDocumentoDB(){return $this->consecutivoDocumentoDB; }
	public function getFechaCierrContaDB(){return $this->fechaCierrContaDB; }
	public function getBandeFechaCierrContaErrorDB(){return $this->bandeFechaCierrContaErrorDB; }
	public function getBandeAjustTipo6DB(){return $this->bandeAjustTipo6DB; }
	public function getIdEmpreTipo6DB(){return $this->idEmpreTipo6DB; }
	public function getBandeExistAportTipo6DB(){return $this->bandeExistAportTipo6DB; }
	public function getBandeExistPuTipo6DB(){return $this->bandeExistPuTipo6DB; }
	public function getBandeEmpreDiferPuTipo6DB(){return $this->bandeEmpreDiferPuTipo6DB; }
	public function getIndicEmpreTipo6DB(){return $this->indicEmpreTipo6DB; }
	public function getNominaTipo6DB(){return $this->nominaTipo6DB; }
	public function getBandeExistAportNominTipo6DB(){return $this->bandeExistAportNominTipo6DB; }
	public function getBandeExistMovimTipo6Sw(){return $this->bandeExistMovimTipo6Sw; }
	public function getBandeExistEmpreTipo6Sw(){return $this->bandeExistEmpreTipo6Sw; }
	public function getIdAportTipo6DB(){return $this->idAportTipo6DB; }

	/*
	 * SETTER
	 */

	public function setIdPuTipoA( $idPuTipoA ){ $this->idPuTipoA = $idPuTipoA;}
	public function setIdProceso( $idProceso ){ $this->idProceso = $idProceso;}
	public function setIdEtapaProceso( $idEtapaProceso ){ $this->idEtapaProceso = $idEtapaProceso;}
	public function setIdArchivo( $idArchivo ){ $this->idArchivo = $idArchivo;}
	public function setError( $error ){ $this->error = $error;}
	public function setEstado( $estado ){ $this->estado = $estado;}
	public function setUsuario( $usuario ){ $this->usuario = $usuario;}
	
	public function setTipoRegisTipo1( $tipoRegisTipo1 ){ $this->tipoRegisTipo1 = $tipoRegisTipo1; }
	public function setFechaRecauTipo1( $fechaRecauTipo1 ){ $this->fechaRecauTipo1 = $fechaRecauTipo1; }
	public function setCodigEntidFinanTipo1( $codigEntidFinanTipo1 ){ $this->codigEntidFinanTipo1 = $codigEntidFinanTipo1; }
	public function setNitAdminTipo1( $nitAdminTipo1 ){ $this->nitAdminTipo1 = $nitAdminTipo1; }
	public function setNombrAdminTipo1( $nombrAdminTipo1 ){ $this->nombrAdminTipo1 = $nombrAdminTipo1; }
	public function setReservadoTipo1( $reservadoTipo1 ){ $this->reservadoTipo1 = $reservadoTipo1; }
		
	public function setTipoRegisTipo5( $tipoRegisTipo5 ){ $this->tipoRegisTipo5 = $tipoRegisTipo5; }
	public function setNumerCuentTipo5( $numerCuentTipo5 ){ $this->numerCuentTipo5 = $numerCuentTipo5; }
	public function setTipoCuentTipo5( $tipoCuentTipo5 ){ $this->tipoCuentTipo5 = $tipoCuentTipo5; }
	public function setNumerLoteTipo5( $numerLoteTipo5 ){ $this->numerLoteTipo5 = $numerLoteTipo5; }
	public function setSistePagoTipo5( $sistePagoTipo5 ){ $this->sistePagoTipo5 = $sistePagoTipo5; }
	public function setReservadoTipo5( $reservadoTipo5 ){ $this->reservadoTipo5 = $reservadoTipo5; }
		
	public function setTipoRegisTipo6( $tipoRegisTipo6 ){ $this->tipoRegisTipo6 = $tipoRegisTipo6; }
	public function setIdentAportTipo6( $identAportTipo6 ){ $this->identAportTipo6 = $identAportTipo6; }
	public function setNombrAportTipo6( $nombrAportTipo6 ){ $this->nombrAportTipo6 = $nombrAportTipo6; }
	public function setCodigBancoAutorTipo6( $codigBancoAutorTipo6 ){ $this->codigBancoAutorTipo6 = $codigBancoAutorTipo6; }
	public function setNumerPlaniLiquiTipo6( $numerPlaniLiquiTipo6 ){ $this->numerPlaniLiquiTipo6 = $numerPlaniLiquiTipo6; }
	public function setPerioPagoTipo6( $perioPagoTipo6 ){ $this->perioPagoTipo6 = $perioPagoTipo6; }
	public function setCanalPagoTipo6( $canalPagoTipo6 ){ $this->canalPagoTipo6 = $canalPagoTipo6; }
	public function setNumerRegisTipo6( $numerRegisTipo6 ){ $this->numerRegisTipo6 = $numerRegisTipo6; }
	public function setCodigOperaInforTipo6( $codigOperaInforTipo6 ){ $this->codigOperaInforTipo6 = $codigOperaInforTipo6; }
	public function setValorPlaniTipo6( $valorPlaniTipo6 ){ $this->valorPlaniTipo6 = $valorPlaniTipo6; }
	public function setHoraMinutTipo6( $horaMinutTipo6 ){ $this->horaMinutTipo6 = $horaMinutTipo6; }
	public function setNumerSecueTipo6( $numerSecueTipo6 ){ $this->numerSecueTipo6 = $numerSecueTipo6; }
	public function setReservadoTipo6( $reservadoTipo6 ){ $this->reservadoTipo6 = $reservadoTipo6; }
		
	public function setTipoRegisTipo8( $tipoRegisTipo8 ){ $this->tipoRegisTipo8 = $tipoRegisTipo8; }
	public function setNumerPlaniTipo8( $numerPlaniTipo8 ){ $this->numerPlaniTipo8 = $numerPlaniTipo8; }
	public function setNumerRegisTipo8( $numerRegisTipo8 ){ $this->numerRegisTipo8 = $numerRegisTipo8; }
	public function setValorRecauTipo8( $valorRecauTipo8 ){ $this->valorRecauTipo8 = $valorRecauTipo8; }
	public function setReservadoTipo8( $reservadoTipo8 ){ $this->reservadoTipo8 = $reservadoTipo8; }
		
	public function setTipoRegisTipo9( $tipoRegisTipo9 ){ $this->tipoRegisTipo9 = $tipoRegisTipo9; }
	public function setNumerTotalPlaniTipo9( $numerTotalPlaniTipo9 ){ $this->numerTotalPlaniTipo9 = $numerTotalPlaniTipo9; }
	public function setNumerTotalRegisTipo9( $numerTotalRegisTipo9 ){ $this->numerTotalRegisTipo9 = $numerTotalRegisTipo9; }
	public function setValorTotalRecauTipo9( $valorTotalRecauTipo9 ){ $this->valorTotalRecauTipo9 = $valorTotalRecauTipo9; }
	public function setNumerTotalLotesTipo9( $numerTotalLotesTipo9 ){ $this->numerTotalLotesTipo9 = $numerTotalLotesTipo9; }
	public function setReservadoTipo9( $reservadoTipo9 ){ $this->reservadoTipo9 = $reservadoTipo9; }
		
	public function setNumeroReciboDB( $numeroReciboDB ){ $this->numeroReciboDB = $numeroReciboDB; }
	public function setConsecutivoDocumentoDB( $consecutivoDocumentoDB ){ $this->consecutivoDocumentoDB = $consecutivoDocumentoDB; }
	public function setFechaCierrContaDB( $fechaCierrContaDB ){ $this->fechaCierrContaDB = $fechaCierrContaDB; }
	public function setBandeFechaCierrContaErrorDB( $bandeFechaCierrContaErrorDB ){ $this->bandeFechaCierrContaErrorDB = $bandeFechaCierrContaErrorDB; }
	public function setBandeAjustTipo6DB( $bandeAjustTipo6DB ){ $this->bandeAjustTipo6DB = $bandeAjustTipo6DB; }
	public function setIdEmpreTipo6DB( $idEmpreTipo6DB ){ $this->idEmpreTipo6DB = $idEmpreTipo6DB; }
	public function setBandeExistAportTipo6DB( $bandeExistAportTipo6DB ){ $this->bandeExistAportTipo6DB = $bandeExistAportTipo6DB; }
	public function setBandeExistPuTipo6DB( $bandeExistPuTipo6DB ){ $this->bandeExistPuTipo6DB = $bandeExistPuTipo6DB; }
	public function setBandeEmpreDiferPuTipo6DB( $bandeEmpreDiferPuTipo6DB ){ $this->bandeEmpreDiferPuTipo6DB = $bandeEmpreDiferPuTipo6DB; }
	public function setIndicEmpreTipo6DB( $indicEmpreTipo6DB ){ $this->indicEmpreTipo6DB = $indicEmpreTipo6DB; }
	public function setNominaTipo6DB( $nominaTipo6DB ){ $this->nominaTipo6DB = $nominaTipo6DB; }
	public function setBandeExistAportNominTipo6DB( $bandeExistAportNominTipo6DB ){ $this->bandeExistAportNominTipo6DB = $bandeExistAportNominTipo6DB; }
	public function setBandeExistMovimTipo6Sw( $bandeExistMovimTipo6Sw ){ $this->bandeExistMovimTipo6Sw = $bandeExistMovimTipo6Sw; }
	public function setBandeExistEmpreTipo6Sw( $bandeExistEmpreTipo6Sw ){ $this->bandeExistEmpreTipo6Sw = $bandeExistEmpreTipo6Sw; }
	public function setIdAportTipo6DB( $idAportTipo6DB ){ $this->idAportTipo6DB = $idAportTipo6DB; }
	
	public function guardar_tipo6(){
		$query = "INSERT INTO aportes426 (
					id_proceso
					, id_etapa_proceso
					, id_archivo
					, error
					, estado
					, usuario
										
					, tipo_regis_tipo6
					, ident_aport_tipo6
					, nombr_aport_tipo6
					, codig_banco_autor_tipo6
					, numer_plani_liqui_tipo6
					, perio_pago_tipo6
					, canal_pago_tipo6
					, numer_regis_tipo6
					, codig_opera_infor_tipo6
					, valor_plani_tipo6
					, hora_minut_tipo6
					, numer_secue_tipo6
					, reservado_tipo6

					, fecha_cierr_conta_db
					, bande_fecha_cierr_conta_error_db
										
				) VALUES(
					$this->idProceso
					, $this->idEtapaProceso
					, $this->idArchivo
					, '$this->error'
					, '$this->estado'
					, '$this->usuario'
				
					, '$this->tipoRegisTipo6'
					, '$this->identAportTipo6'
					, '$this->nombrAportTipo6'
					, '$this->codigBancoAutorTipo6'
					, '$this->numerPlaniLiquiTipo6'
					, '$this->perioPagoTipo6'
					, '$this->canalPagoTipo6'
					, '$this->numerRegisTipo6'
					, '$this->codigOperaInforTipo6'
					, '$this->valorPlaniTipo6'
					, '$this->horaMinutTipo6'
					, '$this->numerSecueTipo6'
					, '$this->reservadoTipo6'
					
					, '$this->fechaCierrContaDB'
					, '$this->bandeFechaCierrContaErrorDB'
					
				)";
		$resultado = (self::$con->queryInsert($query,"aportes426")) === null ? 0 : 1;
		return $resultado;
	}
	
	public function update(){
		$query = " UPDATE aportes426 SET
						tipo_regis_tipo1 = '$this->tipoRegisTipo1'
						, fecha_recau_tipo1 = '$this->fechaRecauTipo1'
						, codig_entid_finan_tipo1 = '$this->codigEntidFinanTipo1'
						, nit_admin_tipo1 = '$this->nitAdminTipo1'
						, nombr_admin_tipo1 = '$this->nombrAdminTipo1'
						, reservado_tipo1 = '$this->reservadoTipo1'
							
						, tipo_regis_tipo5 = '$this->tipoRegisTipo5'
						, numer_cuent_tipo5 = '$this->numerCuentTipo5'
						, tipo_cuent_tipo5 = '$this->tipoCuentTipo5'
						, numer_lote_tipo5 = '$this->numerLoteTipo5'
						, siste_pago_tipo5 = '$this->sistePagoTipo5'
						, reservado_tipo5 = '$this->reservadoTipo5'
								
						, tipo_regis_tipo8 = '$this->tipoRegisTipo8'
						, numer_plani_tipo8 = '$this->numerPlaniTipo8'
						, numer_regis_tipo8 = '$this->numerRegisTipo8'
						, valor_recau_tipo8 = '$this->valorRecauTipo8'
						, reservado_tipo8 = '$this->reservadoTipo8'
							
						, tipo_regis_tipo9 = '$this->tipoRegisTipo9'
						, numer_total_plani_tipo9 = '$this->numerTotalPlaniTipo9'
						, numer_total_regis_tipo9 = '$this->numerTotalRegisTipo9'
						, valor_total_recau_tipo9 = '$this->valorTotalRecauTipo9'
						, numer_total_lotes_tipo9 = '$this->numerTotalLotesTipo9'
						, reservado_tipo9 = '$this->reservadoTipo9'
					WHERE id_proceso = $this->idProceso AND id_archivo = $this->idArchivo";
	
		return self::$con->queryActualiza($query);
	}
	

	/**
	 *
	 * @return number [PROCESO:La etapa esta en proceso,TERMINO: La etapa termino, null: Error]
	 */
	public function procesar_datos(){
		$resultado = "";
		$sentencia = self::$con->conexionID->prepare ( "EXEC [tesoreria].[sp_Maestro_Tesoreria]
				@id_proceso = $this->idProceso
				, @select_top = 500
				, @usuario = '$this->usuario'
				, @resultado = :resultado" );
		$sentencia->bindParam ( ":resultado", $resultado, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000 );
		$sentencia->execute ();

		$resultado = $resultado=='PROCESO' || $resultado=='FIN_EXITOSO'|| $resultado=='FIN_ERROR' ? $resultado: null;

		return $resultado;
	}
	
	public function estado_interfaze(){
		$estado = "PROCESO";
		$query = "-- [11][procesar_dato_sigas]
					-- [12][procesar_dato_service]
					SELECT count(*) AS contador , 'CONTADOR' AS bandera
					FROM aportes426
					WHERE id_proceso=$this->idProceso
						AND id_etapa_proceso=11
						AND ISNULL(estado,'') = 'PROCESADO'
						AND error='N'
					UNION
					SELECT COUNT(*) , 'ERROR' AS bandera
					FROM aportes426
					WHERE id_proceso=$this->idProceso
						AND id_etapa_proceso=12
						AND ISNULL(error,'')='S'
					ORDER BY bandera";
		$arrResultado = $this->fetchConsulta($query);
	
		if(intval($arrResultado[0]["contador"])==0){
			if(intval($arrResultado[1]["contador"])==0){
				$estado = "FIN_EXITOSO";
			}else{
				$estado = "FIN_ERROR";
			}
		}
		return $estado;
	}
	

	/**
	 * Metodo encargado de obtener el estado del proceso
	 *
	 * @return String [TERMINO: El proceso termino, PROCESO: El proceso no termino]
	 */
	public function fetch_datos_interfaz($topSelect){
	
		$query = "-- [4][procesar_dato_sigas]
					SELECT TOP $topSelect a426.*
						,a30.nombrearchivo
					FROM aportes426 a426
					LEFT JOIN aportes030 a30 ON a30.idarchivopu=a426.id_archivo
					WHERE id_proceso=$this->idProceso
						AND id_etapa_proceso=11
						AND ISNULL(estado,'') = 'PROCESADO'
						AND error='N'";
	
		$arrResultado = $this->fetchConsulta($query);
	
		return $arrResultado;
	}

	public function update_estado($idPlanoTesoreria,$error = 'N'){
		$query = "-- [12][procesar_dato_service]
					UPDATE aportes426 SET
						estado='PROCESADO', error='$error', id_etapa_proceso=12, fecha_modificacion=GETDATE()
					WHERE id_plano_tesoreria=$idPlanoTesoreria";
			
		return (self::$con->queryActualiza($query)>0)?1:0;
	}
	
	/**
	 * Retorna los valores obtenidos en la consulta
	 * @return multitype:array
	 */
	private function fetchConsulta($querySql){
		$resultado = array();
		$rs = self::$con->querySimple($querySql);
		while($row = $rs->fetch())
			$resultado[] = array_map("trim",$row);
		//$resultado[] = array_map("utf8_encode",$row);
		return $resultado;
	}
	
}