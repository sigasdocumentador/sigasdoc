<?php
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
$usuario=$_SESSION['USUARIO'];
include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'p.definiciones.class.php';
$objClase=new Definiciones();
$fecver = date('Ymd h:i:s A',filectime('modificar_aporte_empresa.php'));
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Modificar Aportes</title>
<link type="text/css" href="../../css/Estilos.css" rel="stylesheet"/>
<link type="text/css" href="../../css/marco.css" rel="stylesheet"/>
<link type="text/css" rel="stylesheet" href="../../css/css.1.8/jquery-ui-1.8.17.custom.css" />
<script language="javascript" src="../../js/1.6/jquery-1.6.2.min.js"></script>
<script language="javascript" src="../../js/1.6/jquery-ui-1.8.16.custom.min.js"></script>
<script language="javascript" src="../../js/comunes.js"></script>
<script language="javascript" src="js/modificar_aportes_empresa.js"></script>
<style type="text/css">
.classCampoCorto {background: none repeat scroll 0 0 #F7F7F7; border-style: hidden; border-width: 0; font-size: 11px; padding: 0;} 
</style>
</head>

<body>
<center>
<br /><br />
<table width="990" border="0" cellspacing="0" cellpadding="0">
<tr>
<td class="arriba_iz" >&nbsp;</td>
<td class="arriba_ce" ><span class="letrablanca">::Modificar Aportes::</span>
	<div style="text-align:right;float:right;">
		<?php echo 'Versi&oacute;n: ' . $fecver; ?>
	</div>
</td>
<td class="arriba_de" >&nbsp;</td>
</tr>
<tr>
<td class="cuerpo_iz">&nbsp;</td>
<td class="cuerpo_ce">

<img src="../../imagenes/spacer.gif" alt="" width="2" height="1" /></td>
<td class="cuerpo_de">&nbsp;</td>
</tr>
<tr>
<td class="cuerpo_iz" >&nbsp;</td>
<td class="cuerpo_ce" >
	<table border="0" class="tablero" cellspacing="0" width="100%" >
	  <tr>
	    <td width="100" style="text-align:right;">Nit:<input type="hidden" name="hidIdEmpresa" id="hidIdEmpresa"/></td>
	    <td ><input type="text" name="txtNit" id="txtNit" />&nbsp; <label id="lblRazonSocial" >&nbsp;</label></td>
	  </tr>
	  <tr>
	    <td style="text-align:right;">Periodo:</td>
	    <td colspan="5" style="display:none;"	id="tdPeriodo"><input  type="text" readonly="readonly"size="10" name="txtPeriodo" id="txtPeriodo"/> &nbsp;&nbsp;<input type="button" value="Buscar" id="btnBuscarAportes" class="ui-state-default"/></td>
	  </tr>
	  <tr>
	      <td colspan="6" id="tdMensajeError" style="color:#FF0000"></td>
	  </tr>
	  <tr>
	      <td colspan="6">Aportes de la Empresa</td>
	  </tr>
	  </table>
	  <table border="0" class="tablero" cellspacing="0" width="100%" id="tabAportes" style="display: none;"> 
	  	<thead width="100%">
	  		<tr><th></th><th>Id</th><th>Indice</th><th>Aporte</th><th>Valor Nomina</th><th>Fecha Pago</th><th>Periodo</th><th>Observaci&oacute;n</th><th width="20">-------</th ></tr>
	  	</thead>
	  	<tbody id="tBody" width="100%">
	  		
	  	</tbody>
	</table>
</td>
<td class="cuerpo_de" >&nbsp;</td>
</tr>
<tr>
<td class="cuerpo_iz" >&nbsp;</td>
<td class="cuerpo_ce" >&nbsp;</td>
<td class="cuerpo_de" >&nbsp;</td>
</tr>
<tr>
<td class="abajo_iz" >&nbsp;</td>
<td class="abajo_ce" ></td>
<td class="abajo_de" >&nbsp;</td>
</tr>
</table>

</center>
</body>
</html>
