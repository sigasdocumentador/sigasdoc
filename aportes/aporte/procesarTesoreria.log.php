<?php
ini_set('error_reporting', E_ALL | E_NOTICE | E_STRICT);
    ini_set('display_errors', '1');
    ini_set('track_errors', 'On');
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
include_once $raiz.DIRECTORY_SEPARATOR. 'config.php';

include_once $raiz . DIRECTORY_SEPARATOR . 'aportes' . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'maestro.proceso.class.php';
include_once 'clases' . DIRECTORY_SEPARATOR . 'maestroTesoreria.class.php';

//Obtener parametros
$idProceso = $_REQUEST["id_proceso"];
$fechaPago = $_REQUEST["fecha_pago"];
$tipoCarge = $_REQUEST["tipo_carge"];
$arrFiles = isset($_FILES)?$_FILES:null;
$accion = $_REQUEST["accion"];
$usuario = $_SESSION["USUARIO"];

$arrResultado = array();

switch ($accion){
	case "PROCESAR":
		
		$objMaestroTesoreria = new MaestroTesoreria();
		$arrResultado = $objMaestroTesoreria->ejecutar($idProceso,$usuario,$fechaPago,false,$ruta_cargados,$tipoCarge,$arrFiles);
		
		break;
	case "REPROCESAR":
		$objMaestroTesoreria = new MaestroTesoreria();
		$arrResultado = $objMaestroTesoreria->ejecutar($idProceso,$usuario,null,true,$ruta_cargados,$tipoCarge,$arrFiles);
		break;
}

echo json_encode($arrResultado);
?>