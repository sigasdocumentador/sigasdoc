<?php
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
include_once $raiz.DIRECTORY_SEPARATOR. 'funcionesComunes'.DIRECTORY_SEPARATOR.'funcionesComunes.php';
include_once $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas'.DIRECTORY_SEPARATOR. 'config.php';
include_once $raiz.DIRECTORY_SEPARATOR.'phpComunes'.DIRECTORY_SEPARATOR.'wssdk'.DIRECTORY_SEPARATOR.'ClientWSInfWeb.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
include_once $raiz.DIRECTORY_SEPARATOR. 'clases'.DIRECTORY_SEPARATOR.'Logger.php';
include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'utilitario.class.php';
include_once $raiz.DIRECTORY_SEPARATOR.'terceros'.DIRECTORY_SEPARATOR.'BaseTerceros.php';
include_once $raiz.DIRECTORY_SEPARATOR.'ContabilidadGF'.DIRECTORY_SEPARATOR.'BaseContabilidad.php';

$log = new Logger(3);

$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

$usuario=$_SESSION['USUARIO'];

$dirPrint = $ruta_cargados ."tesoreria".DIRECTORY_SEPARATOR."logs".DIRECTORY_SEPARATOR;
$directorio =$ruta_cargados."tesoreria".DIRECTORY_SEPARATOR."cargados".DIRECTORY_SEPARATOR;
$dircop=$ruta_cargados."tesoreria".DIRECTORY_SEPARATOR."procesados".DIRECTORY_SEPARATOR;
$fechaHoy = date("Y/m/d h:i:s a.m.");

$dir = opendir($directorio);
$flag=false;
while ($elemento = readdir($dir))
{
	if(strlen($elemento)>2){
		$archivos[]=$elemento;
		$flag=true;
	}
}
closedir($dir);
$arcProc=$archivos[0];

if(!$flag){
	echo "NO hay archivos de pagos de aportes!!";
	exit();
}

//Obtener el N�mero de recibo 'numrec' y documento 'documento'
/*$sql="Select numero, comprobante from aportes013 WHERE comprobante IN ('CPU','PU')";
$rsc=$db->querySimple($sql);

$documento = 0;
$recibo = 0;
while(($rowComprobante=$rsc->fetchObject())==true){
	if($rowComprobante->comprobante == "CPU")
		$recibo = $rowComprobante->numero + 1;
	else
		$documento = $rowComprobante->numero + 1;
}*/

//N�mero de recibo 'numrec'
$sql="Select numero from aportes013 where comprobante='CPU'";
$rsc=$db->querySimple($sql);
$rowc=$rsc->fetch();
$recibo=$rowc['numero'] + 1;

//Consecutivo 'documento'
$sql="Select numero from aportes013 where comprobante='PU'";
$rsc=$db->querySimple($sql);
$rowc=$rsc->fetch();
$documento=$rowc['numero'] + 1;

$ws = new ClientWSInfWeb(USUARIO_WEB_SIGAS, CONTRASENA_WEB_SIGAS);

for($i=0,$len = count($archivos); $i<$len; $i++){
	//echo "<br>Procesando archivo: ".$archivos[$i];
	/////////////
	$sql="SELECT COUNT(*) as cuenta FROM aportes030 where nombrearchivo ='$archivos[$i]' and tipoproceso='APO'";
	$rs=$db->querySimple($sql);
	$row = $rs->fetch();
	$conr=$row['cuenta'];
	if($conr>0){
		//echo "<br>El archivo: ".$archivos[$i]." ya fue procesado!";
		unlink($directorio.$archivos[$i]);
		continue;
	}
	////////////
	$arcProc=$archivos[$i];
	$directorio.=$archivos[$i]; 

	$archivoS = file($directorio);
	$lineas = count($archivoS);
	$codigo=trim(substr($archivoS[0],9,3));
	if($codigo != "007"){
		echo "El archivo no es de aportes";
		unlink($directorio);
		exit();
	}
	$codigo=trim(substr($archivoS[0],12,9));
	if($codigo != "891180008"){
		echo "El archivo no es de COMFAMILIAR";
		unlink($directorio);
		exit();
	}
	$fechaPago=trim(substr($archivoS[0],1,8));
	$anno = substr($fechaPago, 0,4);
	$mes = substr($fechaPago, 4,2);
	$dia = substr($fechaPago, 6,2);
	$directorio2 =$dircop.$anno.DIRECTORY_SEPARATOR.$mes.DIRECTORY_SEPARATOR.$dia.DIRECTORY_SEPARATOR;
	$ok=verificaDirectorio($directorio2);
	
	$log_errores=$directorio2."Errores_Tes_$arcProc.txt";
	$log_noInsert=$directorio2."Errores_Tes_NoInsert_$arcProc.txt";
	$log_procesados=$directorio2."Registros_Procesados_Bien_$arcProc.txt";
	$log_noExiste=$directorio2."PlanillaNoExiste_$arcProc.txt";
	$log_print = $dirPrint . 'tmp.txt';
	
	$fp_errores = fopen ( $log_errores, "a" );
	$fp_noInsert = fopen ( $log_noInsert, "a" );
	$fp_procesados = fopen ( $log_procesados, "a");
	$fp_noExiste = fopen ( $log_noExiste, "a" );
	$fp_print = fopen ( $log_print, "w" );
	//fwrite ( $fp_print, "[\r\n" );
	planoAppend ( "[\r\n", $fp_print, "No" );
	
	
	//VALIDAR LA FECHA DEL CIERRE CONTABLE
	$fechaCierreCont = $ws->ultimoCierreContable();
	
	if($fechaCierreCont['Tipo']=='Error')
	  die();
    else
		if (date('Y-m-d',strtotime($fechaPago)) < date('Y-m-d',strtotime($fechaCierreCont['valor']))){
		echo 1;
		/*echo "<script language='JavaScript'>alert('No se proceso el archivo la FECHA DE PAGO corresponde a un CIERRE CONTABLE');</script>";*/
		exit();
		}
	
	//--------------------------------------------------
	
	for($c=0; $c < $lineas; $c++){
		
		if($c > 1){
			$tipo=trim(substr($archivoS[$c],0,1));
			$tipo=intval($tipo);
			if($tipo != 6) continue;
			$numero2=trim(substr($archivoS[$c],1,16));
			$numero= trim(substr($archivoS[$c],1,16));
			$tempo=  trim(substr($archivoS[$c],1,16));
			//echo gettype($numero);
			$nombre=trim(substr($archivoS[$c],17,16));
			$planilla=trim(substr($archivoS[$c],41,15));
			$periodo=trim(substr($archivoS[$c],56,6));
			$aportantes=trim(substr($archivoS[$c],64,6));
			$operador=trim(substr($archivoS[$c],70,2));
			$valor=trim(substr($archivoS[$c],72,16));
			$valor=intval($valor);
			$aportantes=intval($aportantes);
			$idempresa=0;
			$ajuste='N';//pendiente
			
			/*$numeroMenUno=substr($numero, 0,-1); 
			
			$sql="Select idempresa,nit from aportes048 where nit = '$numero' OR nit ='$numeroMenUno'";
			$rse=$db->querySimple($sql);
			if($rowEmpresa = $rse->fetch()){
				$idempresa = $rowEmpresa["idempresa"]; 
				$numero = $rowEmpresa["nit"];
			}
			if($idempresa==0){
				$cadena="No Existe el NIT en SIGAS:  $numero";
				planoAppend($cadena, $fp_noExiste);
				$cadenaError="{\"error\":\"No Existe el NIT en SIGAS\",\"Planilla\":\"$planilla\",\"Nit\":\"$numero\",\"ValorPagado\":\"$valor\",\"Periodo\":\"$periodo\"},";
				planoAppend ( $cadenaError, $fp_print, "No" );
				continue;
			}*/
			
			$con2=0;
			$sql="Select idempresa,nit from aportes048 where nit = '$numero' ";
			$rse=$db->querySimple($sql);
			while ($rowe=$rse->fetch()){				
				$idempresa=$rowe['idempresa'];
				$con2++;
			}
			if($con2==0){
				$numero=substr($numero, 0,-1);
				$sql="Select idempresa,nit from aportes048 where nit = '$numero'";
				$rse=$db->querySimple($sql);				
				while ($rowe=$rse->fetch()){					
					$idempresa=$rowe['idempresa'];
					$con2++;
				}
				if($con2==0){
					$cadena="No Existe el NIT en SIGAS:  $numero";
					planoAppend($cadena, $fp_noExiste);
					$cadenaError="{\"error\":\"No Existe el NIT en SIGAS\",\"Planilla\":\"$planilla\",\"Nit\":\"$numero\",\"ValorPagado\":\"$valor\",\"Periodo\":\"$periodo\"},";
					planoAppend ( $cadenaError, $fp_print, "No" );
					//ojoooooooooooooo insertar en la bd
					continue;
				}
			}
			
			//VALIDACION PARA QUE NO SE DUPIQUEN LOS APORTES
			$sql="SELECT count(*) as cont FROM aportes011 a11 WHERE a11.idempresa=$idempresa AND a11.planilla='$planilla' AND a11.periodo='$periodo'
AND a11.valoraporte=$valor and a11.fechapago='".date('Y-m-d',strtotime($fechaPago))."'";
			$rsvd=$db->querySimple($sql);
			while ($rowvd=$rsvd->fetch()){
				$numeroDuplicado=$rowvd['cont'];
			}
				if($numeroDuplicado>=1){
					
					$cadena="Ya Existe El Aporte. Tener Encuenta Duplicado...........................:";
					planoAppend($cadena, $fp_noExiste);
					$cadenaError="{\"error\":\"Ya Existe El Aporte. Tener Encuenta Duplicado\",\"Planilla\":\"$planilla\",\"Nit\":\"$numero\",\"ValorPagado\":\"$valor\",\"Periodo\":\"$periodo\"},";
					planoAppend ( $cadenaError, $fp_print, "No" );
					continue;
					
				}
			//-------------------------------------------------------------------------------------
			
			$sql="select distinct nit, idempresa from aportes010 where planilla='$planilla'";
			$rse=$db->querySimple($sql);
			$con3=0; 
			$distinta=false;
			while ( $rowe=$rse->fetch() ){
				$con3++;
				if ( $idempresa != $rowe['idempresa'] ){
					$distinta=true;					
				}
			}
			if($con3==0){
				$cadena="No Existe la Planilla No:  $planilla";
				planoAppend($cadena, $fp_noExiste);
				$cadenaError="{\"error\":\"No Existe la Planilla\",\"Planilla\":\"$planilla\",\"Nit\":\"$numero\",\"ValorPagado\":\"$valor\",\"Periodo\":\"$periodo\"},";
				planoAppend ( $cadenaError, $fp_print, "No" );
			} else if($con3>1){
				$cadena="Existe mas de un Nit con la Planilla No:  $planilla";
				planoAppend($cadena, $fp_noExiste);
				$cadenaError="{\"error\":\"Existe mas de un Nit con la Planilla\",\"Planilla\":\"$planilla\",\"Nit\":\"$numero\",\"ValorPagado\":\"$valor\",\"Periodo\":\"$periodo\"},";
				planoAppend ( $cadenaError, $fp_print, "No" );
				continue;
			} else if($distinta==true){
				$cadena="Existe un nit distinto con la Planilla No:  $planilla";
				planoAppend($cadena, $fp_noExiste);
				$cadenaError="{\"error\":\"Existe un nit distinto con la Planilla\",\"Planilla\":\"$planilla\",\"Nit\":\"$numero\",\"ValorPagado\":\"$valor\",\"Periodo\":\"$periodo\"},";
				planoAppend ( $cadenaError, $fp_print, "No" );
				continue;
			}
			
			$continuar=true;
			$pc=substr($numero2,0, 1);
			if($pc=='0'){
				$numero2=substr($numero2, 1);
			}
			$numero=doubleval($numero);
			$numero2=$numero;
			$indicador=indicador($db,$numero);
			$nomina=($valor/$indicador)*100;
			$exis11=verificarExistencia($planilla,$idempresa,$fechaPago,$periodo,$nomina);
			$exismovi = false;
			if($exis11>0){
				$exismovi=verificarExisteMovi($numero,$fechaPago,$valor);
				if($exismovi){
					$cadena="Aporte ya registrado Nit: $numero, Fecha pago: $fechaPago, valor: $valor, Planilla - ".$planilla." IdEmpresa - ".$idempresa." Periodo - ".$periodo." Nomina - ".$nomina;
					planoAppend($cadena, $fp_errores);
					$cadenaError="{\"error\":\"Aporte ya registrado\",\"Planilla\":\"$planilla\",\"Nit\":\"$numero\",\"ValorPagado\":\"$valor\",\"Periodo\":\"$periodo\"},";
					planoAppend ( $cadenaError, $fp_print, "No" );
				} else {
					$cadena="Existe Aporte en SIGAS, pero no en INFORMA WEB: Nit: $numero, Fecha pago: $fechaPago, valor: $valor, Planilla - ".$planilla." IdEmpresa - ".$idempresa." Periodo - ".$periodo." Nomina - ".$nomina;
					planoAppend($cadena, $fp_errores);	
					$cadenaError="{\"error\":\"Existe Aporte en SIGAS, pero no en INFORMA WEB\",\"Planilla\":\"$planilla\",\"Nit\":\"$numero\",\"ValorPagado\":\"$valor\",\"Periodo\":\"$periodo\"},";
					planoAppend ( $cadenaError, $fp_print, "No" );
				}
				
				continue;
			}
			
			if(!existeSigas($numero)){
				//log NO existe en SIGAS
				$cadena="No Existe el NIT en SIGAS: $numero - Planilla No:  $planilla";
				planoAppend($cadena, $fp_noExiste);
				$cadenaError="{\"error\":\"No Existe el NIT en SIGAS\",\"Planilla\":\"$planilla\",\"Nit\":\"$numero\",\"ValorPagado\":\"$valor\",\"Periodo\":\"$periodo\"},";
				planoAppend ( $cadenaError, $fp_print, "No" );
				continue;
			}
			if(!exiteInforma($numero)){
				//log NO existe en INFORMA
				$cadena="No Existe el NIT en INFORMA WEB: Nit - ".$numero;
				planoAppend($cadena, $fp_noExiste);
				$cadenaError="{\"error\":\"No Existe el NIT en INFORMA WEB\",\"Planilla\":\"$planilla\",\"Nit\":\"$numero\",\"ValorPagado\":\"$valor\",\"Periodo\":\"$periodo\"},";
				planoAppend ( $cadenaError, $fp_print, "No" );
				continue;
			}
			
			$db->inicioTransaccion();
						
			$sql="insert into aportes011 (comprobante,documento,codigopun,numerorecibo,planilla,idempresa,periodo,valornomina,valortrabajador,valoraporte,valorpagado,trabajadores,ajuste,fechapago,fechasistema,usuario,procesado,indicador) 
					values ('PU','$documento','00','$recibo','$planilla','$idempresa','$periodo','$nomina','$nomina','$valor','$valor',$aportantes,'$ajuste','$fechaPago',CAST(getDate() as date),'$usuario','N',$indicador)";
			$rs11=$db->queryInsert($sql, "aportes011");
			if($rs11==0){
				$cadena="No se hizo insert en  aportes011:  - ".$sql;
				planoAppend($cadena, $fp_noInsert);
				
				$cadenaError="{\"error\":\"No se hizo insert en  aportes011\",\"Planilla\":\"$planilla\",\"Nit\":\"$numero\",\"ValorPagado\":\"$valor\",\"Periodo\":\"$periodo\"},";
				$cadenaError.="{\"error\":\"No se guardo el Aporte en SIGAS\",\"Planilla\":\"$planilla\",\"Nit\":\"$numero\",\"ValorPagado\":\"$valor\",\"Periodo\":\"$periodo\"},";
				planoAppend ( $cadenaError, $fp_print, "No" );
				
				$cadena="No se guardo el Aporte en SIGAS Nit: $numero, Fecha pago: $fechaPago, valor: $valor";
				planoAppend($cadena, $fp_errores);
				
				//planoAppend ( $cadenaError, $fp_print, "No" );
				$db->cancelarTransaccion();
				continue;
			}
			
			$cadena="Aportes ".$periodo." ".$numero." ".$fechaPago;
			$movimientos = array();
			$movimientos[0] = new Movimiento($recibo, new Fecha(substr($fechaPago, 0, 4),substr($fechaPago, 4, 2),substr($fechaPago, 6, 2)), CUENTA_CONTABLE_DEBITO, $numero, CENTRO_DE_COSTO, $valor, "D", $cadena, $usuario);
			$movimientos[1] = new Movimiento($recibo, new Fecha(substr($fechaPago, 0, 4),substr($fechaPago, 4, 2),substr($fechaPago, 6, 2)), CUENTA_CONTABLE_CREDITO, $numero, CENTRO_DE_COSTO, $valor, "C", $cadena, $usuario);
			$errores = $ws->movimientoGrabar(COMPROBANTE_PLANILLA_UNICA, $movimientos);
			if(count($errores)==0){
				//$cadena="Se proceso correctamente:  - ".$cadena;
				//planoAppend($cadena, $log_noInsert);
			} else {
				foreach ($errores as $value) {
					$cadena=  $value->ErrorDescription;
					planoAppend($cadena, $fp_errores);
				}

				$cadena="No se guardo el Aporte en INFORMAWEB Nit: $numero, Fecha pago: $fechaPago, valor: $valor";
				planoAppend($cadena, $fp_errores);
				$cadenaError="{\"error\":\"No se guardo el Aporte en INFORMAWEB\",\"Planilla\":\"$planilla\",\"Nit\":\"$numero\",\"ValorPagado\":\"$valor\",\"Periodo\":\"$periodo\"},";
				planoAppend ( $cadenaError, $fp_print, "No"  );
				$db->cancelarTransaccion();
				continue;
			}
				
			$sql="update aportes013 set numero=$recibo where comprobante='CPU'";
			$db->queryActualiza($sql);
			$recibo++;
			
			$sql="update aportes013 set numero=$documento where comprobante='PU'";
			$db->queryActualiza($sql);
			$documento++;
			
			$db->confirmarTransaccion();			
		}
	}
	//grabar archivo aportes30
	$tamano=filesize($directorio)/1000;
	$sql="insert into aportes030 (nombrearchivo,ruta,tipoarchivo,tamano,fechacargue,tipoproceso,procesado,fechaproceso,usuarioproceso) values ('$arcProc','$directorio2','','$tamano','$fechaPago','KAR','S',CAST(getDate() as date),'$usuario')";
	$rsgr=$db->queryInsert($sql,"aportes030");
	//actualiza informa web comprob ultimo numero
	//borrar el archivo y copiar en directorio2 tesoreria/procesados/a�o/mes/dia/archivo
	if($rsgr!=0)
	{
		if(copy($directorio, $directorio2.$arcProc))
		{
			unlink($directorio);
		}
	}
	
	planoAppend($cadena, $fp_errores);
	planoAppend($cadena, $fp_noInsert);
	planoAppend($cadena, $fp_procesados);
	planoAppend($cadena, $fp_noExiste);
	planoAppend ( "]", $fp_print, "No" );
	
	fileClose ( $fp_errores, $log_errores );
	fileClose ( $fp_noInsert, $log_noInsert );
	fileClose ( $fp_procesados, $log_procesados );
	fileClose ( $fp_noExiste, $log_noExiste );
	fileClose ( $fp_print, $log_print );
}
$cadena="  ------------------------------------------------------------";
/*planoAppend($cadena, $fp_errores);
planoAppend($cadena, $fp_noInsert);
planoAppend($cadena, $fp_procesados);
planoAppend($cadena, $fp_noExiste);
planoAppend ( "]", $fp_print, "No" );

fileClose ( $fp_errores, $log_errores );
fileClose ( $fp_noInsert, $log_noInsert );
fileClose ( $fp_procesados, $log_procesados );
fileClose ( $fp_noExiste, $log_noExiste );
fileClose ( $fp_print, $log_print );*/
echo "</br>";
echo "proceso terminado...";
echo "</br>";
echo "<br>No Procesados: $i";

function verificarExistencia($planilla,$idempresa,$fechaPago,$periodo,$nomina){
	//verificar q no se aya cargado ese registro aportes011
	$db = IFXDbManejador::conectarDB();
	if($db->conexionID==null){
		$cadena = $db->error;
		echo msg_error($cadena);
		exit();
	}
	$sql="select numerorecibo from aportes011 where planilla='$planilla' and idempresa='$idempresa' and fechapago='$fechaPago' and periodo='$periodo' and valornomina='$nomina'";
	$rsv=$db->querySimple($sql);
	$num=0;
	while ($rowv=$rsv->fetch()){
		$num=$rowv['numerorecibo'];
	}
	return $num;
}

function existeSigas($nit){
	return true;/*
	//vericar nit en sigas devol si no
	$db = IFXDbManejador::conectarDB();
	if($db->conexionID==null){
		$cadena = $db->error;
		echo msg_error($cadena);
		exit();
	}
	$sql="select count(*) as cuenta from aportes048 where nit='$nit'";
	$rses=$db->querySimple($sql);
	$rowes=$rses->fetch();
	if($rowes['cuenta']!=0)
		return true;//existe
	else
		return false;//no existe
	*/
}

function exiteInforma($nit){
	$ws = new ClientWSInfWeb(USUARIO_WEB_SIGAS, CONTRASENA_WEB_SIGAS);
	return  $ws->terceroExiste((int)$nit);
	/*$nit=Utilitario::format_nit($nit);
	$contabilidad=new ContabilidadGF();
	$nit_digito=$contabilidad->ObtenerDigitoNit($nit);
	$terceros=new BaseTerceros();
	if($nit_digito!="")){
		return $terceros->ExisteTercero($nit."-".$nit_digito);
	}
	else{
		return $terceros->ExisteTercero($nit);
	}*/
	
}

function verificarExisteMovi($nit,$fecha,$valor){
	$anno=substr($fecha,0, 4);
	$mes=substr($fecha,4,2);
	$dia=substr($fecha, -2);
	$f=new Fecha($anno,$mes,$dia);
	$comprob='CPU';
	$ws = new ClientWSInfWeb(USUARIO_WEB_SIGAS, CONTRASENA_WEB_SIGAS);
	return  $ws->movimientoNumero($comprob, $nit, $f, $valor);
}

function planoAppend($cadena,$fp,$fechaNo="Si"){
	if($fechaNo=="Si"){
	$fechaHoy=date("Y/m/d h:i:s a");
	$cadena=$fechaHoy." -".$cadena;
	}
	$cadena.="\r\n";
	fwrite ( $fp, $cadena );
	fflush($fp);
}


function fileClose($fp, $file){
	fclose ( $fp );

	if(filesize($file)==0){
		unlink ( $file );
	}
}

function indicador($db,$nit){
	$sql="SELECT codigo FROM aportes048 INNER JOIN aportes091 ON aportes048.indicador=aportes091.iddetalledef WHERE nit = '$nit'";
	$rs = $db->conexionID->query($sql)->fetch(PDO::FETCH_OBJ)->codigo;
	return $rs;
}
?>