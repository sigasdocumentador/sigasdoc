<?php
	date_default_timezone_set('America/Bogota');
	$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
	include_once  $root;
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
	
	include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'p.aportes.class.php';
	
	$db = IFXDbManejador::conectarDB();
	if($db->conexionID==null){
		$cadena = $db->error;
		echo msg_error($cadena);
		exit();
	}
	
	//Parametros
	$tipoTraslado = $_REQUEST["tipoTraslado"];
	$idEmpresaTraslado = $_REQUEST["idEmpresaTraslado"];
	$periodoTraslado = $_REQUEST["periodoTraslado"];
	$arrIdAportes = $_REQUEST["arrIdAportes"];
	$observacion = $_REQUEST["observacion"];
	
	$idAportes = join($arrIdAportes,',');
	
	$sqlDevolucion = "SELECT * FROM aportes058 WHERE idaporte IN($idAportes)";
	$rsDevolucion = $db->querySimple($sqlDevolucion);
	$arrDatosDevolucion = array();
	while($row=$rsDevolucion->fetch()){
		$arrDatosDevolucion[] = $row;
	}
	
	if(count($arrDatosDevolucion)>0)
		echo json_encode($arrDatosDevolucion);
	else {
		$objAportes = new Aportes();
		if($tipoTraslado=="Periodo") {
			$idEmpresaTraslado = null;
		}else if($tipoTraslado=="Empresa") {
			$periodoTraslado = null;
		}
		$rs = $objAportes->actualizar_traslado_aporte($idAportes,$idEmpresaTraslado,$periodoTraslado,$_SESSION["USUARIO"]);
		if($rs>0){
			$contador = 0;
			foreach($arrIdAportes as $idAporte){
				$rsGuardarNota = $objAportes->guardarNota($idAporte, $observacion, "T");
				$contador += $rsGuardarNota;
			}
			if($contador==count($arrIdAportes))
				echo 1;
			else
				echo 2;			
		}else 
			echo 0;
	}
?>