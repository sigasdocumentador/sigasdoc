<?php
date_default_timezone_set('America/Bogota');
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

$case = $_POST['v0'];
switch($case){
	case 'CONSULTAR_EMPRESA':
		$data = array('idEmpresa'=>0,'razonSocial'=>'','error'=>0,'descripcion'=>'');
		$nit  = $_POST['v1'];
		$sql  = "SELECT idempresa, razonsocial FROM aportes048 WHERE nit='$nit' AND claseaportante=2875";
		$rs   = $db->querySimple($sql);
		$row = $rs->fetch();
		if(!$row['idempresa']>0){
			$data['error'] = 1;
			$data['descripcion'] = "<label style='color:red;'>La empresa no es ley 1429<label>";
		}else{
			$data['idEmpresa']=$row['idempresa'];
			$data['razonSocial'] = $row['razonsocial'];
		}
		echo json_encode($data);
	break;
	case 'BUSCAR_APOSTES':
		$idEmpresa = $_POST['v1'];
		$periodo = $_POST['v2'];
		$data = array('indicador'=>0,'error'=>0,'descripcion'=>'');
		$sql = "SELECT count (DISTINCT tarifaaporte) AS indicador FROM aportes010 WHERE idempresa=$idEmpresa AND periodo='$periodo'";
		$rs = $db->querySimple($sql);
		$row = $rs->fetch();
		
		if($row['indicador']==0){
			$data['error'] = 1;
			$data['descripcion'] = "<label style='color:red;'>La empresa no tiene aportes<label>";
		}else{
			$data['aportes'][] =$row;
			$cont=1;
		} // Fin ELSE
		
	echo json_encode($data);
	
	break;
	case 'BUSCAR_INDICE':
		$idEmpresa = $_POST['v1'];
		$periodo = $_POST['v2'];		
		$data = array('idEmpresa'=>0,'error'=>0,'descripcion'=>'');
		$sql = "SELECT a10.tarifaaporte, a10.periodo, a10.idempresa, sum(a10.valoraporte) AS valoraporte
		FROM aportes010 a10
		WHERE a10.idempresa=$idEmpresa AND a10.periodo='$periodo'
		GROUP BY a10.tarifaaporte, a10.periodo, a10.idempresa";
		$rs = $db->querySimple($sql);
		$cont = 0;
	
			while($row = $rs->fetch()){
				$data['aportes'][] =$row;
				$cont=1;
			}
				
			if($cont==0){
				$data['error'] = 1;
				$data['descripcion'] = "La empresa no tiene aportes";
			}
				
			echo json_encode($data);
	
		break;
	case 'MODIFICAR':
		$IdEmpresa = $_POST['v1'];
		$Periodo = $_POST['v2'];
		$Indicador = $_POST['v3'].".0";
		$nomina = $_POST['v4'];
		
		//echo "Indicador".$Indicador;
			switch ($Indicador) {
				case 0.0 :
					$indiceEmpresa = 4268;
					break;
				case 1.0 :
					$indiceEmpresa = 4267;
					break;
				case 2.0 :
					$indiceEmpresa = 4266;
					break;
				case 3.0 :
					$indiceEmpresa = 4265;
					break;
				case 4.0 :
					$indiceEmpresa = 108;
					break;
			}
		
		$sql="update aportes011 set indicador='$Indicador', valornomina = $nomina where idempresa = $IdEmpresa and periodo = '$Periodo'";
		$rs = $db->queryActualiza($sql);
		
		if( intval( $rs ) > 0 ){
				$sql="update aportes048 set indicador=$indiceEmpresa where idempresa = $IdEmpresa";
				$rs = $db->queryActualiza($sql);
				echo $rs;
		}else{
			echo 0;
		}		
	break;
}
?>