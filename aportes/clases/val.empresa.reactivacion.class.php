<?php

/*
 * Clase utilizada para validar si la empresa a la que pertenece el afiliado se encuentra activa para poder
 * continuar con la reactivacion.
 * 
 */
class valEmpresasReactivacionAfiliado{
	
	  private $con=null;
	  private $idFormulario='';
	  private $idPersona='';
	  public  $idEmpresa='';
	  public  $tipoafiliacion;
	  private $nit='';
	  private $razonsocial='';
	  private $direccion='';
	  private $telefono='';
	  public  $estado='';
	  private $detalledefinicio='';
	  private $fechaestado='';
	  private $observaciones='';
	  
      public function __construct($idFormulario,$idPersona){
      	$this->idFormulario=$idFormulario;
      	$this->idPersona=$idPersona;
      	$this->con = IFXDbManejador::conectarDB();
      }
      
      public function getIdFormulario(){
      	return $this->idFormulario;
      }
      
      /*
       * Consultar el id de la empresa asociada al afiliado a reactivar 
       * mediante el idformulario pasado
       */
      public function consultarIdEmpresa(){
      	   $sql="select idempresa,
      	                tipoafiliacion 
      	         from aportes017 
      	         where idformulario=$this->idFormulario";
      	   $rs=$this->con->querySimple($sql);
     	   $rest= $rs->fetch();
     	   $this->idEmpresa=$rest['idempresa'];
     	   $this->tipoafiliacion=$rest['tipoafiliacion'];
      }
      
      /*
       * Consultamos datos del la empresa, estado y observaciones 
       * 
       */
      public function cosultarEstadoEmpresa($idEmpresa){
      	   $sql="SELECT               
                     a048.nit,
                     a048.razonsocial,
                     a048.direccion,
                     a048.telefono,
                     a048.estado,
                     a048.codigoestado,
                     a091.detalledefinicion,
                     a048.fechaestado,
                     (SELECT  TOP 1 observaciones FROM aportes088 WHERE idregistro=a048.idempresa) AS observaciones
              FROM aportes048 a048 
                 LEFT JOIN aportes091 a091 ON a048.codigoestado=a091.iddetalledef AND a091.iddefinicion='65'
              WHERE a048.idempresa=".$idEmpresa;
      	   $rs=$this->con->querySimple($sql);
      	   $result=$rs->fetchAll();
      	   if(count($result)==1){
      	   	  $this->nit=$result[0]['nit'];
      	   	  $this->razonsocial=$result[0]['razonsocial'];
      	   	  $this->direccion=$result[0]['direccion'];
      	   	  $this->telefono=$result[0]['telefono'];
      	   	  $this->estado=$result[0]['estado'];
      	   	  $this->detalledefinicion=$result[0]['detalledefinicion'];
      	   	  $this->fechaestado=$result[0]['fechaestado'];
      	   	  $this->observaciones=$result[0]['observaciones'];
      	   	  return 1;
      	   }else{
      	   	  return 0;
      	   }
      	  
      }
      
      /*
       * tabla html de respuesta cuando el estado de la empresa sea inactivo.
       */
      public function tablaRes(){
      	$tabla = '<table border="1" cellpadding="2" cellspacing="0" width="600">
           	  			<tr>
           	  			    <td colspan="2" align="center">Empresa en estado Inactivo</td>
           	  			<tr>
           	  			<tr>
           	  			    <td>Nit</td>
           	  			    <td>'.$this->nit.'</td>
           	  			<tr>
           	  			<tr>
           	  			    <td>Razon social</td>
           	  			    <td>'.$this->razonsocial.'</td>
           	  			<tr>
           	  			<tr>
           	  			    <td>Direccion</td>
           	  			    <td>'.$this->direccion.'</td>
           	  			<tr>
           	  			<tr>
           	  			    <td>Telefono</td>
           	  			    <td>'.$this->telefono.'</td>
           	  			<tr>
           	  			<tr>
           	  			    <td>Estado</td>
           	  			    <td>'.$this->estado.'</td>
           	  			<tr>
           	  			<tr>
           	  			    <td>Detalle Estado</td>
           	  			    <td>'.$this->detalledefinicion.'</td>
           	  			<tr>
           	  			<tr>
           	  			    <td>Fecha Estado</td>
           	  			    <td>'.$this->fechaestado.'</td>
           	  			<tr>
           	  			<tr>
           	  			    <td>Observaciones</td>
           	  			    <td>'.$this->observaciones.'</td>
           	  			<tr>
           	  			</table>';
      	
      	           return $tabla;
      }
      
      public function obtcausalinactivacion($ide){
      	 $this->cosultarEstadoEmpresa($ide);
      	 return $this->tablaRes();
      }
      
      
      
      // cerra la conexion a la base de datos
      public function __destruct(){
      	$this->con=null;
      }
      
}

?>