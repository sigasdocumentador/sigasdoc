<?php
/* autor:       Orlando Puentes
 * fecha:       Agosto 18 de 2010
 * objetivo:    Procesar los movimientos de los Bonos almacenados en la base de datos. 
 */
 
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
include_once $raiz . DIRECTORY_SEPARATOR . 'rsc' . DIRECTORY_SEPARATOR . 'conexion.class.php';

class Persona{
 //constructor	
var $con;
var $fechaSistema;
function Persona(){
 		$this->con=new DBManager;
 	}
	
function insertSimple($campos){
		if($this->con->conectar()==true){
			$fechaSistema=date("m/d/Y");
			$sql="INSERT INTO aportes015 (identificacion,idtipodocumento,papellido,sapellido,pnombre,snombre,usuario,fechasistema) VALUES ('".$campos[0]."','".$campos[1]."','".$campos[2]."','".$campos[3]."','".$campos[4]."','".$campos[5]."','".$campos[6]."','".$fechaSistema."')";
			//echo $sql;
			return mssql_query($sql,$this->con->conect);
		}
	}	

function insertar($campos){
		if($this->con->conectar()==true){
			$fechaSistema=date("m/d/Y");
			//print_r($campos);
			$sql="INSERT INTO aportes090 (definicion,concepto,fechacreacion,usuario) VALUES ('".$campos[1]."','".$campos[2]."','".$fechaSistema."','".$campos[4]."')";
			return mssql_query($sql,$this->con->conect);
		}
	}
 
function mostrar_registro_tipid($tipid, $id){
		if($this->con->conectar()==true){
			$sql="SELECT aportes015.*,aportes091.detalledefinicion AS ti,ap2.detalledefinicion AS ec FROM aportes015 
INNER JOIN aportes091 ON aportes015.idtipodocumento=aportes091.iddetalledef
LEFT JOIN aportes091 ap2 ON aportes015.idestadocivil=ap2.iddetalledef WHERE identificacion='$id' AND idtipodocumento=$tipid";
			return mssql_query($sql,$this->con->conect);
		}
	}
	
function mostrar_registro($id){
		if($this->con->conectar()==true){
			$sql="SELECT aportes015.*,aportes091.detalledefinicion AS ti,ap2.detalledefinicion AS ec FROM aportes015 
INNER JOIN aportes091 ON aportes015.idtipodocumento=aportes091.iddetalledef
LEFT JOIN aportes091 ap2 ON aportes015.idestadocivil=ap2.iddetalledef WHERE identificacion='$id'";
			return mssql_query($sql,$this->con->conect);
		}
	}

function actualizar($campos){
		if($this->con->conectar()==true){
			//print_r($campos);
			$sql="UPDATE aportes090 SET definicion = '".$campos[1]."', concepto = '".$campos[2]."' WHERE iddefinicion = ". $campos[0];	
			return mssql_query($sql,$this->con->conect);
		}
	}	
 
function eliminar($id){
	if($this->con->conectar()==true){
		$sql="DELETE FROM aportes090 WHERE iddefinicion=".$id;
		return mssql_query($sql,$this->con->conect);
		}
	} 
	
function mostrar_datos(){
		if($this->con->conectar()==true){
			return mssql_query("SELECT * FROM aportes090 order by definicion",$this->con->conect);
		}
	}	
 }	
?>