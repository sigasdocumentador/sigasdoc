<?php

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';

include_once $raiz.DIRECTORY_SEPARATOR. 'phpComunes' . DIRECTORY_SEPARATOR . 'cargar_archivo.class.php';

include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'utilitario.class.php';

class MaestroProceso{
	
	private $idProceso = 0;
	private $tipo = null;
	private $usuario = null;
	private $rutaCargados;
	
	private static $con = null;
	
	private $arrError = array("error"=>0,"data"=>null,"descripcion"=>"");
	
	function __construct(){
		
		//Conexion
		try{
			self::$con = IFXDbManejador::conectarDB();
			if( self::$con->conexionID==null ){
				throw new Exception(self::$con->error);
			}
		}catch(Exception $e){
			echo $e->getMessage();
			exit();
		}
	}
	
	public function setIdProceso($idProceso){
		$this->idProceso = $idProceso;
	}
	public function setTipo($tipo){
		$this->tipo = $tipo;
	}
	public function setUsuario($usuario){
		$this->usuario = $usuario;
	}
	public function setRutaCargados($rutaCargados){
		$this->rutaCargados = $rutaCargados;
	}
	
	/**
	 * Metodo encargado de inicializar el proceso
	 * 
	 * @param unknown_type $tipo [PU, KARDEX]
	 * @param unknown_type $usuario
	 * @return multitype:number NULL string [error[0:no existen error, 1: existe error], descripcion: Descripcion del proceso, data[id_proceso]]
	 */
	public function iniciar_proceso(){
		
		if($this->estado_inicio_proceso()==true){
			//Iniciar proceso
			$query = "INSERT INTO dbo.aportes420 (tipo,estado,usuario) VALUES ('$this->tipo','INICIO','$this->usuario')";
			$this->idProceso = self::$con->queryInsert($query,"aportes420");
				
			if($this->idProceso === null){
				$this->arrError["error"] = 1;
				$this->arrError["descripcion"] = "Error: No fue posible inicializar el procesos";
			}else{
				$this->arrError["error"] = 0;
				$this->arrError["descripcion"] = "El proceso se inicializo correctamente";
				$this->arrError["data"]["id_proceso"] = $this->idProceso;
			}
		}
		
		return $this->arrError;
	}
	
	public function iniciar_reproceso(){
		if($this->estado_inicio_proceso()==true){
			//Remover estado de error para las etapas con error
			
			if($this->tipo == "PU"){
				$resultado = 0;
				$sentencia = self::$con->conexionID->prepare ( "EXEC [pu].[sp_Reprocesar_PU]
						@id_proceso = $this->idProceso
						, @usuario = '$this->usuario'
						, @resultado = :resultado" );
				$sentencia->bindParam ( ":resultado", $resultado, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000 );
				$sentencia->execute ();
				
				if($resultado==0){
					$this->arrError["error"] = 0;
					$this->arrError["descripcion"] = "El reproceso se inicializo correctamente";
				}else{
					$this->arrError["error"] = 1;
					$this->arrError["descripcion"] = "Error el reproceso no fue posible actualizarlo";
				}
				
			}else if($this->tipo == "KARDEX"){
				$resultado = 0;
				$sentencia = self::$con->conexionID->prepare ( "EXEC [tesoreria].[sp_Reprocesar_Tesoreria]
						@id_proceso = $this->idProceso
						, @usuario = '$this->usuario'
						, @resultado = :resultado" );
				$sentencia->bindParam ( ":resultado", $resultado, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000 );
				$sentencia->execute ();
				
				if($resultado==0){
					$this->arrError["error"] = 0;
					$this->arrError["descripcion"] = "El reproceso se inicializo correctamente";
				}else{
					$this->arrError["error"] = 1;
					$this->arrError["descripcion"] = "Error el reproceso no fue posible actualizarlo";
				}
			}
		}
		return $this->arrError;
	}
	
	/**
	 * Metodo encargado de inicializar la etapa del proceso
	 *
	 * @param unknown_type $tipo [PU, KARDEX]
	 * @param unknown_type $usuario
	 * @return multitype:number NULL string [error[0:no existen error, 1: existe error], descripcion: Descripcion del proceso
	 * 										, data[id_etapa_proceso,etapa_proceso,id_traza_proceso]]
	 */
	public function iniciar_etapa_proceso(){
		//Obtener etapa
		$arrResultado = $this->fetch_etapa_procesar();
		$arrDataTrazaProce = array();
		
		if(count($arrResultado)>0){
			
			$arrResultado = $arrResultado[0];
			
			$idEtapaProceso = $arrResultado["id_etapa_proceso"];
			$idTrazaProceso = intval($arrResultado["id_traza_proceso"]);

			//Verificar si ya existe una traza proceso inicializada
			if($idTrazaProceso==0){
				//Guardar la traza proceso
				$query = "INSERT INTO dbo.aportes422 (id_proceso,id_etapa_proceso,usuario)VALUES ($this->idProceso, $idEtapaProceso,'$this->usuario')";
				$idTrazaProceso = self::$con->queryInsert($query,"aportes422");
				
				if($idTrazaProceso===null){
					$this->arrError["error"] = 1;
					$this->arrError["descripcion"] = "Error: No fue posible inicializar la traza";
				}else{
					$this->arrError["error"] = 0;
					$this->arrError["descripcion"] = "La traza se inicializo correctamente";
				}
			}
			
			$arrDataTrazaProce["id_etapa_proceso"] = $idEtapaProceso;
			$arrDataTrazaProce["etapa_proceso"] = $arrResultado["etapa_proceso"];
			$arrDataTrazaProce["id_traza_proceso"] = $idTrazaProceso;			
			
			$this->arrError["data"] = $arrDataTrazaProce;
			
		}else{
			$arrError["error"] = 1;
			$arrError["descripcion"] = "Error: No se puedo obtener la etapa a procesar";
		}
		
		return $this->arrError;
	}
	
	/**
	 * Metodo encargado de terminar el proceso
	 *
	 * @param unknown_type $estado Estado del proceso [FIN_EXITOSO, FIN_ERROR] 
	 * @return multitype:number NULL string [error[0:no existen error, 1: existe error], descripcion: Descripcion del proceso]
	 */
	public function terminar_proceso(){
		
		$estado = "";
		$query = "SELECT COUNT(*) AS contador FROM aportes422 WHERE id_proceso=$this->idProceso AND procesado='E'";
		$resultado = $this->fetchConsulta($query);
		
		if(intval($resultado[0]["contador"])>0)
			$estado="FIN_ERROR";
		else 
			$estado="FIN_EXITOSO";
				
		$query = "UPDATE dbo.aportes420
					SET estado = '$estado',fecha_fin = getdate()
				WHERE id_proceso=$this->idProceso";
		
		if(self::$con->queryActualiza($query)>0){
			//El proceso se cerro correctamente
			$this->arrError["error"] = 0;
			$this->arrError["descripcion"] = "El proceso se termino con: ".$estado;
		}else{
			$this->arrError["error"] = 1;
			$this->arrError["descripcion"] = "Error: No fue posible terminar el procesos";
		}
			
		return $this->arrError;
	}
	
	/**
	 * Metodo encargado de terminar la etapa proceso
	 *
	 * @param unknown_type $idTrazaProceso El id de la etapa proceso ha terminar
	 * @param unknown_type $procesado Estado de la etapa proceso [E, S]
	 * @return multitype:number NULL string [error[0:no existen error, 1: existe error], descripcion: Descripcion del proceso]
	 */
	public function terminar_etapa_proceso( $idTrazaProceso, $procesado, $etapaProceso){
		
		//Iniciar transaccion
		self::$con->inicioTransaccion();
		
		$banderaError = 0;
		
		$query = "UPDATE dbo.aportes422
					SET procesado = '$procesado',	fecha_fin = getdate()
				WHERE id_traza_proceso=$idTrazaProceso";
		
		if(self::$con->queryActualiza($query)>0){
			
			//Verificar si el proceso se debe terminar
			if($etapaProceso=="fin"){
				$arrResultado = $this->terminar_proceso();
				
				if($arrResultado["error"]==1){
					//Error al terminar el proceso
					$banderaError = 1;
					$this->arrError["error"] = 1;
					$this->arrError["descripcion"] = $arrResultado["descripcion"];
				}
			}
		}else{
			//Error al terminar la etapa proceso
			$banderaError = 1;
			$this->arrError["error"] = 0;
			$this->arrError["descripcion"] = "Error: La etapa proceso no se termino";
		}
		
		if($banderaError==1){
			//Rolback transaccion
			self::$con->cancelarTransaccion();
		}else{
			//Commit transaccion
			self::$con->confirmarTransaccion();
		}
		return $this->arrError;
	}
	
	/**
	 * Metodo encargado de guardar los datos del archivo
	 * 
	 * @param unknown_type $nombreArchivo
	 * @param unknown_type $ruta
	 * @param unknown_type $tipoArchivo
	 * @param unknown_type $tamanno
	 * @param unknown_type $procesado
	 * @param unknown_type $fechaCargue
	 * @param unknown_type $tipoProceso
	 * @return number id archivo
	 */
	public function guardar_archivo($nombreArchivo, $ruta, $tipoArchivo, $tamanno, $procesado, $tipoProceso){

		$query = "INSERT INTO aportes030 
					(nombrearchivo, ruta, tipoarchivo, tamano, procesado, fechacargue, tipoproceso, usuarioproceso)
				VALUES('$nombreArchivo','$ruta','$tipoArchivo', $tamanno, '$procesado',getdate(),'$tipoProceso','$this->usuario')";
		
		$resultado = self::$con->queryInsert($query,"aportes030");
		
		$idArchivo = 0;
		if($resultado!==null){
			$idArchivo = self::$con->conexionID->lastInsertId("aportes030");
		}
		return $idArchivo;
	}

	/**
	 * Metodo encargado de verificar el estado para el inicio del nuevo proceso o reproceso
	 * @return boolean [true: Procesar, false:no procesar]
	 */
	public function estado_inicio_proceso(){
		$query = "
					SELECT
						count(*) AS cantidad, 'inicializado' AS bandera 
					FROM aportes420
					WHERE estado NOT IN ('FIN_ERROR','FIN_EXITOSO')
					UNION
					SELECT 
						count(*) AS cantidad, 'minuto' AS bandera 
					FROM aportes420 
					WHERE dateadd(mi,5,fecha_fin)>(getdate())";
		
		$arrResultado = $this->fetchConsulta($query);
		
		$banderaCont = 0;
		foreach($arrResultado as $row){
			if($row["bandera"] == "inicializado" && $row["cantidad"]>0){
				//Existen procesos inicializados
				$this->arrError["error"] = 1;
				$this->arrError["descripcion"] = "Error: Existen procesos inicializados";
				$banderaCont++;
				break;
			}else if($row["bandera"] == "minuto" && $row["cantidad"]>0){
				//Aun no han pasado 5 minutos despues del ultimo proceso
				$this->arrError["error"] = 1;
				$this->arrError["descripcion"] = "Error: Debes dejar pasar 5 minutos despues del ultimo proceso";
				$banderaCont++;
				break;
			}
		}
		
		return $banderaCont==0?true:false;
	}
	
	/*public function fetch_proceso_inicializado(){
		$resultado = array();
	
		$query = "SELECT
					id_proceso, tipo, fecha_inicio, fecha_fin, estado, usuario, fecha_creacion
				FROM aportes420
				WHERE estado NOT IN ('FIN_ERROR','FIN_EXITOSO')";
	
		$resultado = $this->fetchConsulta($query);
		return $resultado;
	}*/

	
	public function fetch_proceso_completo($arrAtributoValor){
		
		$attrEntidad = array(
				array("nombre"=>"id_proceso","tipo"=>"NUMBER","entidad"=>"a420")
				,array("nombre"=>"tipo","tipo"=>"TEXT","entidad"=>"a420")
				,array("nombre"=>"estado","tipo"=>"TEXT","entidad"=>"a420"));
		
		$filtroSql = Utilitario::obtenerFiltro($attrEntidad,$arrAtributoValor);
	
		$query = "SELECT
					a420.id_proceso, a420.tipo, a420.fecha_inicio AS fecha_inici_proce, a420.fecha_fin AS fecha_fin_proce, a420.estado AS estado_proceso, a420.usuario, a420.fecha_creacion
					, a421.id_etapa_proceso, a421.etapa_proceso, a421.descripcion, a421.orden
					, a422.id_traza_proceso, a422.procesado, a422.fecha_inicio AS fecha_inici_traza, a422.fecha_fin AS fecha_fin_traza	
				FROM aportes420 a420
					INNER JOIN aportes421 a421 ON a421.tipo='{$arrAtributoValor["tipo"]}'
					LEFT JOIN aportes422 a422 ON a422.id_proceso=a420.id_proceso AND a422.id_etapa_proceso=a421.id_etapa_proceso
				$filtroSql
				ORDER BY a421.orden ASC";
	
		return $this->fetchConsulta($query);
	}
	
	public function fetch_ultimo_proceso($arrAtributoValor){
		$query = "SELECT
					a420.id_proceso, a420.tipo, a420.fecha_inicio AS fecha_inici_proce, a420.fecha_fin AS fecha_fin_proce, a420.estado AS estado_proceso, a420.usuario, a420.fecha_creacion
					, a421.id_etapa_proceso, a421.etapa_proceso, a421.descripcion, a421.orden
					, a422.id_traza_proceso, isnull(a422.procesado,'') AS procesado, a422.fecha_inicio AS fecha_inici_traza, a422.fecha_fin AS fecha_fin_traza
				FROM aportes420 a420
					INNER JOIN aportes421 a421 ON a421.tipo = '{$arrAtributoValor["tipo"]}'
					LEFT JOIN aportes422 a422 ON a422.id_proceso=a420.id_proceso AND a422.id_etapa_proceso=a421.id_etapa_proceso
				WHERE a420.tipo = '{$arrAtributoValor["tipo"]}'
					AND a420.id_proceso = (SELECT TOP 1 id_proceso FROM aportes420 ORDER BY fecha_inicio DESC)
				ORDER BY a421.orden ASC";
	
		return $this->fetchConsulta($query);
	}
	
	/**
	 * Metodo encargado de obtener la etapa a seguir
	 * @param unknown_type $tipo [PU, KARDEX]
	 */
	public function fetch_etapa_procesar(){
		$resultado = array();
		
		$query = "SELECT TOP 1 
					a421.id_etapa_proceso, a421.etapa_proceso, a421.descripcion, a421.tipo, a421.orden, a421.estado AS estad_etapa_proce
					, a422.id_traza_proceso, a422.procesado, a422.fecha_inicio
				FROM aportes421 a421
					LEFT JOIN aportes422 a422 ON a422.id_etapa_proceso=a421.id_etapa_proceso  AND a422.id_proceso=$this->idProceso
				WHERE a421.tipo='$this->tipo'
					AND a421.id_etapa_proceso NOT IN (
						SELECT a422.id_etapa_proceso 
						FROM aportes422 a422 
						WHERE a422.id_proceso=$this->idProceso AND a422.procesado IN ('S','E') 
					)
				ORDER BY a421.orden ASC";
	
		$resultado = $this->fetchConsulta($query);
		return $resultado;
	}
	
	public function verifica_directorio($ruta){
		if (is_dir($ruta)){
			return true;
		}
		if(!mkdir($ruta, 0, true)){
			return false;
		}
		return true;
	}
	
	
	public function fetch_archivo_cargado($uriPlanoCargado){
		$archivos = array();
		
		//Obtener los archivos del directorio
		$openDirCargados = opendir($uriPlanoCargado);
		while ($archivo = readdir($openDirCargados))
		{
			if(strlen($archivo)>2){
				$archivos[]=$archivo;
			}
		}
		closedir($openDirCargados);
		
		return $archivos;
	}
	
	/**
	 * Metodo de trasladar de directorio el archivo
	 * 
	 * @param unknown_type $uriActual
	 * @param unknown_type $uriDestino
	 */
	public function trasladar_archivo($uriActual,$uriDestino){
		copy ( $uriActual, $uriDestino );
		unlink ( $uriActual );
	}
	
	/**
	 * Metodo encargado de guardar el logs en archivo plano
	 * 
	 * @return number [0:Error, 1:ok]
	 */
	public function guardar_logs_archivo(){
		//$uriPlanoProcesado = $ruta_cargados . 'planillas' . DIRECTORY_SEPARATOR . 'procesados' . DIRECTORY_SEPARATOR;
		return 1;
	}
	
	/**
	 * Metodo encargado de cargar de forma manual los archivos al servidor
	 * 
	 * @param unknown_type $uriPlanoCargado
	 * @param unknown_type $extencionArchivo
	 * @param unknown_type $arrTipoArchivo
	 * @param unknown_type $arrFiles
	 * 
	 * @return boolean [true: Ejecucion exitosa, false: Error]
	 */
	public function carga_archivo_manual($uriPlanoCargado,$extencionArchivo, $arrTipoArchivo,$arrFiles){
		
		$resultado = true;
		
		$cargarArchivo = new CargarArchivo();
		$cargarArchivo->setDirectorio($uriPlanoCargado);
		$cargarArchivo->setExtencionArchivo($extencionArchivo);
		$cargarArchivo->setTipoArchivo($arrTipoArchivo);
		
		foreach( $arrFiles as $rowFile){
			try{
				$cargarArchivo->setNombreArchivo($rowFile['name']);
				$cargarArchivo->setFile($rowFile);
				
				if(!$cargarArchivo->cargar()){
					$resultado = false;
					break;
				}
				
			}catch(Exception $e){
				$resultado = false;
				break;
			}
		}
		
		return $resultado;
	}
	
	/**
	 * Retorna los valores obtenidos en la consulta
	 * @return multitype:array
	 */
	private function fetchConsulta($querySql){
		$resultado = array();
		$rs = self::$con->querySimple($querySql);
		while($row = $rs->fetch())
			$resultado[] = array_map("trim",$row);
		//$resultado[] = array_map("utf8_encode",$row);
		return $resultado;
	}
	
	
	/*
	 * Vaciar carpeta de archivos cargados por errores en validacion del contenido 
	 * 
	 */
	
	public function vaciar_archivos_cargados($uriActual){
		$arrArchivos = $this->fetch_archivo_cargado($uriActual);
		if(count($arrArchivos)>0){
			foreach($arrArchivos as $archivo){
				unlink($uriActual.$archivo);
			}
		}
	}
	
	
	/*
	 * mover los archivo procesados exitosamente
	*
	*/
	
	public function  move_archivos_procesados_ok($uriActual,$uriDestino){
		$arrArchivos = $this->fetch_archivo_cargado($uriActual);
		if(count($arrArchivos)>0){
			foreach($arrArchivos as $archivo){
				copy ( $uriActual.$archivo, $uriDestino.$archivo.date("YmdGis"));
				unlink($uriActual.$archivo);
			}
		}
	}
	
		
	/*
	 * valida la fecha del archivo pingnoracion en formato YYYYMMDD O YYYY/MM/DD O YYYY-MM-DD
	 * retorna bolean
	 */
	
	public function valdate($fecha){
		$fecha=str_replace("/","",$fecha);
		$fecha=str_replace("-","",$fecha);
		if(strlen($fecha)==8){
			$a�o=substr($fecha,0,4);
			$mes=substr($fecha,4,2);
			$dia=substr($fecha,6,4);
			return checkdate($mes,$dia,$a�o);
		}else{
			return false;
		}
	}	
	
	
}