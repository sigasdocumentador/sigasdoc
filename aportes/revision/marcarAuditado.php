<?php
/* autor:       orlando puentes
 * fecha:       octubre 4 de2010
 * objetivo:    Procesar los movimientos de los Bonos almacenados en la base de datos. 
 */
 set_time_limit(0);
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'funcionesComunes'.DIRECTORY_SEPARATOR.'funcionesComunes.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';

$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

$usuario= $_SESSION["USUARIO"];
$idsRadicaciones = $_REQUEST['v0']; 	
$sql="UPDATE aportes016 SET auditado='S' WHERE idradicacion IN ($idsRadicaciones)";
$rs=$db->queryActualiza($sql);
if($rs==0){
	print_r(0);
}elseif($rs>0){
	$sql2="UPDATE aportes004 SET usuarioaudita='$usuario', fechaaudita=CAST(GETDATE() AS DATE) WHERE idradicacion IN ($idsRadicaciones)";	
	$rs2=$db->queryActualiza($sql2);
	print_r($rs);
}else{
	$e=$db->error;
	echo msg_error($e);
}
?>