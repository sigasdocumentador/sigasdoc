<?php
/**
 * @author Oswaldo Gonzalez
 * @date 01-jun-2012
 */
set_time_limit(0);
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'funcionesComunes'.DIRECTORY_SEPARATOR.'funcionesComunes.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';

$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

$usuario= $_SESSION["USUARIO"];
$idsEmpresas = $_REQUEST['v0'];
$sql="UPDATE aportes048 SET auditado='S', usuarioaudita='$usuario', fechaaudita=CAST(GETDATE() AS DATE) WHERE idempresa IN ($idsEmpresas)";

$rs=$db->queryActualiza($sql);
if($rs==0){
	print_r(0);
}elseif($rs>0){
	print_r($rs);
}else{
	$e=$db->error;
	echo msg_error($e);
}
?>