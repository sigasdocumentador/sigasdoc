<?php
/* autor:       orlando puentes
 * fecha:       octubre 4 de2010
 * objetivo:    Procesar los movimientos de los Bonos almacenados en la base de datos. 
 */
set_time_limit(0);
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'funcionesComunes'.DIRECTORY_SEPARATOR.'funcionesComunes.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

$usuario = $_SESSION["USUARIO"];
$buscarPor = isset($_REQUEST["buscarPor"])?$_REQUEST["buscarPor"]:null;
$agencia = isset($_REQUEST["agencia"])?$_REQUEST["agencia"]:"s";;
$tipoIdentificacion = null;
$identificacion = null;
$nombre = null;
$apellido = null;
$nombre = null;
$strCondicionBusqueda = "";

if($buscarPor == 'identificacion'){
	$tipoIdentificacion = $_REQUEST["tipoDocumento"];
	$identificacion = $_REQUEST["numDocumento"];
	$strCondicionBusqueda = " AND aportes015.idtipodocumento = ". $tipoIdentificacion ." AND aportes015.identificacion='". $identificacion ."' ";
}

if($buscarPor == 'nombre'){
	$nombre = $_REQUEST["nombre"];
	$apellido = $_REQUEST["apellido"];

	$arrCondNombre = array();

	if($nombre != "")
		$arrCondNombre["nom"] = "aportes015.pnombre LIKE '%{$nombre}%'";

	if($apellido != "")
		$arrCondNombre["ape"] = "aportes015.papellido LIKE '%{$apellido}%'";

	if(count($arrCondNombre)>0)
		$strCondicionBusqueda = " AND (".implode(" AND ",$arrCondNombre) .")";
}

$sql="SELECT idradicacion,aportes015.idpersona, aportes015.idtipodocumento,td.codigo, identificacion,pnombre, snombre, papellido, sapellido, aportes015.direccion,aportes016.idagencia,aportes016.idempresa,nit,idformulario FROM aportes015 INNER JOIN aportes016 ON aportes015.idpersona=aportes016.idpersona INNER JOIN aportes091 td ON aportes015.idtipodocumento=td.iddetalledef INNER JOIN aportes048 ON aportes016.idempresa=aportes048.idempresa WHERE aportes016.auditado='N' AND idagencia IS NOT NULL {$strCondicionBusqueda}";
if($agencia!='s') 
	$sql="SELECT idradicacion,aportes015.idpersona, aportes015.idtipodocumento,td.codigo, identificacion,pnombre, snombre, papellido, sapellido, aportes015.direccion,aportes016.idagencia,aportes016.idempresa,nit,idformulario FROM aportes015 INNER JOIN aportes016 ON aportes015.idpersona=aportes016.idpersona INNER JOIN aportes091 td ON aportes015.idtipodocumento=td.iddetalledef INNER JOIN aportes048 ON aportes016.idempresa=aportes048.idempresa WHERE aportes016.auditado='N' AND idagencia='$agencia' ";
$rs=$db->querySimple($sql);
$con=0;
$filas=array();
while($w=$rs->fetch()){
	$filas[]=$w;
	$con++;
}
if($con==0){
	echo 0;
	} 
else{
	echo json_encode($filas);
	}
 ?>