<?php
/* autor:       Orlando Puentes
 * fecha:       Junio 9 de 2010
 * objetivo:    Diseño del formulario de digitalización  

 *///Seguridad
 $root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
$fecDigitalizacion=date('m/d/Y');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Formulario</title>
<link href="../../css/estilo_forma.css" rel="stylesheet" type="text/css" />
<link href="../../css/estilo_tablas.css" rel="stylesheet" type="text/css" />
<link href="../../js/aino_galeria/themes/lightbox/galleria.lightbox.css" rel="stylesheet" type="text/css" />
<link href="../../css/formularios/custom-theme/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" />
<script src="../../js/jquery-1.4.2.js" type="text/javascript" language="javascript"></script>
<script src="../../js/jquery.maskedinput-1.2.2.min.js" type="text/javascript" language="javascript"></script>
<script src="../../js/jquery.wizard.js" type="text/javascript" language="javascript"></script>
<script src="../../js/jquery-ui-1.8rc3.custom.min.js" type="text/javascript" language="javascript"></script>
<script src="../../js/aino_galeria/galleria.js" type="text/javascript" language="javascript"></script>
<script src="../../js/Clipboard.js" type="text/javascript" language="javascript"></script>
<script type="text/javascript" language="javascript">
$(document).ready(function(){

	$('.requerido').each(function(){
		$(this).after('<span class="indRequerido"><span>*</span></span>');
	});
	
	$('#txtNoRadicacion').mask('9?99999');

	Galleria.loadTheme('../../js/aino_galeria/themes/lightbox/galleria.lightbox.js');
	$('#galeria').galleria({
	    keep_source: false,
		image_crop: false,
		thumb_margin: 5,
		transition: 'slide',
		transition_speed: 400,
		thumb_quality: false
	});

	$('#txtNoRadicacion').bind('blur',function(){
		$('#msjs').html('<img src="../../imagenes/ajax-loader-min.gif" alt="Cargando..." />');
		$('#h2RutaDigitaliza').html('&nbsp;');
		$('#msjs').focus();
		$('input:gt(0):not(#txtFechaDigitalizacion)').val('');
		var noRadicacion = $(this).val();
		if(noRadicacion.length > 0){
			$.getJSON('buscarRadicacion.php',{rad:noRadicacion},function(datos){
				if(datos.error == '0'){
				$('#txtFechaRadicacion').val(datos.fecharadicacion);
				$('#txtTipoRadicacion').val(datos.detalledefinicion);
				$('#txtIdentificacion').val(datos.numero);
				$('#txtNoFolios').val(datos.folios);
				$('#h2RutaDigitaliza').html(datos.rutaCmp);
				if(!Clipboard.enabled()){
				   //avisa que no tiene permisos de acceso al clipboard
					   alert('No tiene permisos para copiar');
				}else{
				   //caputar el evento "onPaste" y llama a una funcion del usuario
					   Clipboard.set(datos.rutaCmp);
				}
				$('#msjs').html('&nbsp;');
				}else{
					$('#msjs').html('No se encontro la radicaci&oacute;n');
				}
			})
		}
	});
})
</script>
</head>
<body>
<div class="forma" style="width: 100%"><label class="titulo"> <span>Formulario Digitalizaci&oacute;n</span> </label> <span class="menuBar">
<button class="btnNuevo">Nuevo</button>
<button class="btnGrabar">Grabar</button>
<button class="btnModificar">Actualizar</button>
<button class="btnEliminar">Eliminar</button>
<button class="btnImprimir">Imprimir</button>
<button class="btnLimpiar">Limpiar</button>
<button class="btnBuscar">Buscar</button>
<button class="btnInformacion">Informaci&oacute;n</button>
</span>
<div id="msjs">&nbsp;</div>
<ul class="contenido">
	<li class="descripcion"><span class="principal">Digitalizaci&oacute;n de documentos</span> <span class="secundaria">Digite el n&uacute;mero de radicaci&oacute;n de la cual va a proceder a escanear los documentos</span></li>
	<li class="fila">
	<div class="compuesto"><span class="item max_3"> <label class="mediano etiqueta">Radicaci&oacute;n Nro</label> <input type="text" name="txtNoRadicacion" id="txtNoRadicacion" value="" class="pequeno campotexto requerido" /> </span> <span
		class="item max_3"
	> <label class="mediano etiqueta">Fecha radicaci&oacute;n</label> <input type="text" name="txtFechaRadicacion" id="txtFechaRadicacion" value="" class="mediano campotexto" readonly="readonly" /> </span> <span class="item"> <label
		class="mediano etiqueta"
	>Fecha digitalizaci&oacute;n</label> <input type="text" name="txtFechaDigitalizacion" id="txtFechaDigitalizacion" value="<?php
	echo $fecDigitalizacion?>" class="mediano campotexto" readonly="readonly" /> </span></div>
	</li>
	<li class="fila">
	<div class="compuesto"><span class="item max_3"> <label class="etiqueta grande">Tipo radicaci&oacute;n</label> <input type="text" name="txtTipoRadicacion" id="txtTipoRadicacion" value="" class="campotexto mediano" readonly="readonly" /> </span> <span
		class="item max_3"
	> <label class="etiqueta grande"> No. Identificaci&oacute;n </label> <input type="text" value="" name="txtIdentificacion" id="txtIdentificacion" class="campotexto mediano" readonly="readonly" /> </span> <span class="item"> <label
		class="mediano etiqueta"
	>No. Folios</label> <input type="text" name="txtNoFolios" id="txtNoFolios" value="" class="campotexto pequeno" readonly="readonly" /> </span></div>
	</li>
	<li class="fila">
	<div class="compuesto"><span class="item max_1">
	<h2 id="h2RutaDigitaliza">&nbsp;</h2>
	</span></div>
	</li>
	<li class="fila" id="galeria">
	<div class="compuesto">
	<div class="galleria-thumbnails"><img src="../../../digitalizacion/1953/junio/15/12105298/00539273.png" alt="" /> <img src="../../../digitalizacion/1953/junio/15/12105298/1060374.png" alt="" /></div>
	</div>
	</li>
</ul>
</div>
</body>
</html>
