<?php
set_time_limit(0);
ini_set("display_errors",'0');
date_default_timezone_set("America/Bogota");

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

if ( !isset( $_SESSION ['USUARIO'] ) ) {session_start();}
if(empty($_SESSION['LOGUIADO'])){
	echo "no hay session!!";
	exit();
}
$raiz=$_SESSION['RAIZ'];
include_once $raiz.DIRECTORY_SEPARATOR.'phpComunes'.DIRECTORY_SEPARATOR.'session.php';
session();

$idpersona= $_REQUEST['v0'];
$formulario = (isset($_REQUEST['v1']))?$_REQUEST['v1']:"";
include_once $raiz.DIRECTORY_SEPARATOR.'config.php';
include_once $raiz.DIRECTORY_SEPARATOR.'clases'. DIRECTORY_SEPARATOR.'ciudades.class.php';
include_once $raiz.DIRECTORY_SEPARATOR.'funcionesComunes'.DIRECTORY_SEPARATOR.'funcionesComunes.php';
include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'p.definiciones.class.php';

$isActive = true;
$fechaIngreso='';
$objClase=new Definiciones();
$objCiudad=new Ciudades();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link type="text/css" rel="stylesheet" href="<?php echo URL_PORTAL; ?>newcss/marco.css" />
<link type="text/css" rel="stylesheet" href="<?php echo URL_PORTAL; ?>css/Estilos.css" />
<link type="text/css" rel="stylesheet" href="<?php echo URL_PORTAL; ?>css/css.1.8/jquery-ui-1.8.17.custom.css" />

<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/1.6/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/1.6/jquery-ui-1.8.16.custom.min.js"></script>

<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/comunes.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/direccion.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/llenarCampos.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/jquery.combos2.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/jquery.fechaLarga.js"></script>

<script type="text/javascript" src="js/actualizarFechaAsignacion.js"></script>

<script type="text/javascript">
shortcut.add("Shift+F",function() {
		                                var URL=src();
		var url=URL+"aportes/trabajadores/consultaTrabajador.php";
    	window.open(url,"_blank");
    },{
	'propagate' : true,
	'target' : document 
        });        
</script>


<title>::Actualizar Fecha::</title>
</head>
<body>
<input type="hidden" id="formularioAfi" value="<?php echo $formulario; ?>" />
<span class="Rojo"></span></p>
<br />

<!-------------------------------------------------------------------------------------------------------------->
<div id="wrapTable">
<table width="97%" border="0" cellspacing="0" cellpadding="0" align="center">
<tr>
<td width="13" height="29" class="arriba_iz">&nbsp;</td>
<td class="arriba_ce"><span class="letrablanca">::&nbsp;Actualizaci&oacute;n Afiliado&nbsp;::</span></td>
<td width="13" class="arriba_de" >&nbsp;</td>
</tr>    
<tr>     
<td class="cuerpo_iz">&nbsp;</td>
<td class="cuerpo_ce">
	<img src="<?php echo URL_PORTAL; ?>imagenes/spacer.gif" width="1" height="1"/> 
	<img src="<?php echo URL_PORTAL; ?>imagenes/spacer.gif" width="2" height="1"/> 
    <br/>
</td>
<td class="cuerpo_de">&nbsp;</td>
</tr>
<tr>
<td class="cuerpo_iz">&nbsp;</td>
<!-- TABLAS DE FORMULARIO -->
<td class="cuerpo_ce">
<center>
     <table width="90%" border="0" cellspacing="0" class="tablero">
            <tr>
              <td width="148">Buscar Por:</td>
              <td width="149"><select name="buscarPor" class="box1" id="buscarPor">
                <option value="1" selected="selected">IDENTIFICACION</option>
                <option value="2">NOMBRE COMPLETO</option>
               <!-- <option value="2">PRIMER NOMBRE</option>
                <option value="3">PRIMER APELLIDO</option>-->
               </select></td>
              <td width="319">
               <select id="tipoDocumento" name="tipoDocumento" class="box1">
              <option value="1" selected="selected">C&eacute;dula ciudadan&iacute;a</option>
              <option value="2">Tarjeta de Identidad</option>
              <option value="3">Pasaporte</option>
              <option value="4">C&eacute;dula extranjer&iacute;a</option>
              <option value="8">Adulto sin identificaci&oacute;n</option>
              
              </select>
              
              <input name="idT" type="text" class="box" id="idT" onkeypress="runSearch(event)" /> 
              <input type="text" class="box" id="pn" style="display:none" /> -
              <input type="text" class="box" id="pa" style="display:none" />
              </td>
              <td width="316">
              <input name="buscarT" type="button" class="ui-state-default" id="buscarT" value="Buscar" />
                <span class="Rojo"></span>
               </td>
            </tr>
            
      </table>
      <div id="trabajadores" align="center"> </div>
          
    <br /><br />
	<small><strong>Grupo familiar registrado</strong></small><br />
	<table width="95%" border="0" cellspacing="0" class="tablero" id="tabTableGrupo">
	<thead>
		<tr>
			<th width="3%">TD</th>
			<th width="7%">N&uacute;mero</th>
			<th width="30%">Beneficiario</th>
			<th width="8%">Parentesco</th>
			<th width="3%">Est</th>		
			<th width="7%">F Nace</th>
			<th width="3%">Edad</th>
			<th width="4%">F Afil</th>
			<th width="4%">F Cert</th>
			<th width="4%">F Asig</th>
			<th width="9%">Relaci&oacute;n</th>
			<th width="3%">Giro.</th>
			<th width="3%">Dis</th>
			<th width="3%">Emb</th>
			<th width="4%">Est Afil</th>
			<th width="5%">Modificar</th>
		</tr>
	</thead>
	<tbody>
	
	</tbody>
	</table> 
</center>

<td class="cuerpo_de"></td><!-- FONDO DERECHA -->
<tr>
<td height="41" class="abajo_iz">&nbsp;</td>
<td class="abajo_ce"></td>
<td class="abajo_de">&nbsp;</td>
</tr>
</table>
</div>
<div id="icon"><span></span></div>

<!-------------------------------------------------------------------------------------------------------------->
<!-- DIV PARA MODIFICAR BENEFICIARIOS-->
<div id="modificarBeneficiarios" title="::FORMULARIO MODIFICACI&Oacute;N BENEFICIARIO::" style="display:none">
	<input type="hidden" name="hidIdRelacion3Upd" id="hidIdRelacion3Upd"/>	
	<table width="100%" border="0" cellspacing="0" class="tablero">
  		<tr>
    		<td>Tipo de beneficiario</td>
    		<td>
    			<select name="parentescoUpd" class="box1" id="parentescoUpd" onchange="validarComboParentesco();" disabled>
      				<option selected="selected">Seleccione..</option>
      				<option value="35">Hijo</option>
      				<option value="38">Hijastro</option>
      				<option value="36">Padre/Madre</option>
      				<option value="37">Hermano(a)</option>
    			</select>
    		</td>
    		<td>C&eacute;dula Padre &oacute; Madre</td>
    		<td>
    			<select name="cedMamaUpd" id="cedMamaUpd" class="box1" disabled>
      				<option value="0" selected="selected">Seleccione.. </option>
      				<option value="0" name="0">HIJO NATURAL </option>
    			</select>
    		</td>
  		</tr> 
		<tr>
  			<td >Tipo Documento</td>
  			<td >
  				<select name="tipoDoc3Upd" class="box1" id="tipoDoc3Upd" disabled>
  					<option selected="selected" value="0">Seleccione..</option>
  				<?php
					$consulta=$objClase->mostrar_datos(1, 2);
					while($row=mssql_fetch_array($consulta)){
						echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
				} ?>
  				</select> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
  			</td>
  			<td >No. Identificaci&oacute;n</td>
  			<td >
  				<input name="identificacion3Upd" type="text" class="box1" id="identificacion3Upd" readonly />
  				 <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
  				<input type="hidden" name="hidIdPersona3Upd" id="hidIdPersona3Upd"/>
  			</td>
		</tr>
		<tr>
			<td>Primer Nombre</td>
			<td>
				<input name="pNombre3Upd" type="text" class="box1" id="pNombre3Upd" readonly onkeypress="return vpnombre(event)"/>
				 <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
			</td>
			<td>Segundo Nombre</td>
			<td><input name="sNombre3Upd" type="text" class="box1" id="sNombre3Upd" readonly onkeypress="return vsnombre(event)"/></td>
		</tr>
		<tr>
			<td>Primer Apellido</td>
			<td>
				<input name="pApellido3Upd" type="text" class="box1" id="pApellido3Upd" readonly onkeypress="return vpnombre(event)"/>
				 <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
			</td>
			<td>Segundo Apellido</td>
			<td><input name="sApellido3Upd" type="text" class="box1" id="sApellido3Upd" readonly onkeypress="return vsnombre(event)"/></td>
		</tr>
		<tr>
			<td>Genero</td>
			<td>
				<select name="sexo3Upd" class="box1" disabled id="sexo3Upd">
					<option selected="selected" value="0">Seleccione..</option>
					<option value="M">Masculino</option>
					<option value="F">Femenino</option>
				</select> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
			</td>
			<td>Fecha de Nacimiento</td>
			<td>
				<input name="fecNac3Upd" type="text" class="box1" readonly id="fecNac3Upd" onchange="validarEdadActualizar($('#fecNac3Upd'),($('#tipoAfiliacion3Upd').val()==48?'S':'N'),$('#capTrabajoUpd').val(),$('#parentescoUpd').val(),0)"/>
				 <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
				<input type="hidden" name="fecNacHide3Upd" id="fecNacHide3Upd"/>
			</td>
		</tr>
		<tr>
  			<td>Tipo formulario</td>
  			<td>
  				<select name="tipoAfiliacion3Upd" class="box1" id="tipoAfiliacion3Upd" disabled onchange="validarCertificadosUpd();" >
  					<option value="48">Subsidio</option>
  					<option value="49">Servicios</option>
  				</select>
 			</td>
  			<td>Capacidad de trabajo</td>
  			<td>
  				<select name="capTrabajoUpd" class="box1" id="capTrabajoUpd" disabled onchange="validarCertificadosUpd();" onblur="validarEdad(1)">
     				<option value="N">Normal</option>
     				<option value="I">Discapacitado</option>
  				</select>
  			</td>
		</tr>
		<tr>
  			<td>Fecha Asignaci&oacute;n</td>
  			<td><input type="text" name="txtFAsignacion" id="txtFAsignacion"/></td>
  			<td>Fecha Afilia </td>
  			<td><input type="text" class="box1" readonly id="txtFechaAfiliacion" name="txtFechaAfiliacion" /></td>  			
		</tr>		
		<tr>
			<td>Estado Afiliaci&oacute;n</td>
  			<td>  				
  				<select name="estadoUpd" class="box1" id="estadoUpd" disabled >
    				<option value="A">Activo</option>
    				<option value="I">Inactivo</option>
  				</select>
  				<input type="hidden" id="hdnMotivoUpd" value="" />    			
  			</td>
  			<td id="labelFechaRetiro">Fecha Retiro</td>
    		<td>
       			<input type="text" id="fechaRetiro" class="box1" name="fechaRetiro" readonly/>       			 
    		</td>
		</tr>		
	</table>
</div>

<!-- FORMULARIO OBSERVACIONES-->
	<div name="div-observaciones-tab" style="display:none" title="Observaciones de modificaci&oacute;n">
	<table class="tablero">
	 <tr>
	   <td>Usuario</td>
	   <td colspan="3" >
	   <input name="usuarioObs" id="usuarioObs" class="box1" disabled="disabled" value="<?php echo $_SESSION['USUARIO']?>" /></td>
	   </tr>
	 <tr>
	   <td>Observaciones</td>
	   <td colspan="3" >
	   <textarea name="observacionGrupo" id="observacionGrupo" cols="45" rows="5" class="boxlargo"></textarea></td>
	   </tr>
	</table>
	<div class="ui-state-highlight ui-corner-all" style="margin: 9px auto; display:none;padding: 6px; text-align: center; border: 1px solid rgb(255, 255, 0); width:300px" id="rtaObservacion"><span class="ui-icon ui-icon-info" style="float: left; margin-right: 0.3em;"></span>Observaci&oacute;n guardada con &eacute;xito.</div>
	</div>

<br />
<input type="hidden" id="idPersona" name="idPersona" /> 
<input type="hidden" name="txtFechaIngresoM" id="txtFechaIngresoM" value="<?php echo $fechaIngreso; ?>" />
<input type="hidden" name="txtDiferenciaFechaIngreso" id="txtDiferenciaFechaIngreso" value="<?php echo $diasDiferenciaFechaIngreso; ?>" />
</body>

</html>