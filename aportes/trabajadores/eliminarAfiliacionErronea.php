<?php
set_time_limit(0);
ini_set("display_errors",'0');
date_default_timezone_set("America/Bogota");

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

if ( !isset( $_SESSION ['USUARIO'] ) ) {session_start();}
if(empty($_SESSION['LOGUIADO'])){
	echo "no hay session!!";
	exit();
}
$raiz=$_SESSION['RAIZ'];
include_once $raiz.DIRECTORY_SEPARATOR.'phpComunes'.DIRECTORY_SEPARATOR.'session.php';
session();

$idpersona= $_REQUEST['v0'];
$formulario = (isset($_REQUEST['v1']))?$_REQUEST['v1']:"";
include_once $raiz.DIRECTORY_SEPARATOR.'config.php';
include_once $raiz.DIRECTORY_SEPARATOR.'clases'. DIRECTORY_SEPARATOR.'ciudades.class.php';
include_once $raiz.DIRECTORY_SEPARATOR.'funcionesComunes'.DIRECTORY_SEPARATOR.'funcionesComunes.php';
include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'p.definiciones.class.php';

$isActive = true;
$fechaIngreso='';
$objClase=new Definiciones();
$objCiudad=new Ciudades();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link type="text/css" rel="stylesheet" href="<?php echo URL_PORTAL; ?>newcss/marco.css" />
<link type="text/css" rel="stylesheet" href="<?php echo URL_PORTAL; ?>css/Estilos.css" />
<link type="text/css" rel="stylesheet" href="<?php echo URL_PORTAL; ?>css/css.1.8/jquery-ui-1.8.17.custom.css" />

<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/1.6/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/1.6/jquery-ui-1.8.16.custom.min.js"></script>

<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/comunes.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/direccion.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/llenarCampos.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/jquery.combos2.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/jquery.fechaLarga.js"></script>
<script type="text/javascript" src="js/eliminarAfiliacionErronea.js"></script>
<!--  <script type="text/javascript" src="../js/eliminarRadicacion.js"></script>-->

<script type="text/javascript">
shortcut.add("Shift+F",function() {
		                                var URL=src();
		var url=URL+"aportes/trabajadores/consultaTrabajador.php";
    	window.open(url,"_blank");
    },{
	'propagate' : true,
	'target' : document 
        });        
</script>


<title>::Eliminar Afiliacion::</title>
</head>
<body>
<input type="hidden" id="formularioAfi" value="<?php echo $formulario; ?>" />
<span class="Rojo"></span></p>
<br />

<!-------------------------------------------------------------------------------------------------------------->
<div id="wrapTable">
<table width="97%" border="0" cellspacing="0" cellpadding="0" align="center">
<tr>
<td width="13" height="29" class="arriba_iz">&nbsp;</td>
<td class="arriba_ce"><span class="letrablanca">::Eliminar Afiliacion::</span></td>
<td width="13" class="arriba_de" >&nbsp;</td>
</tr>    
<tr>     
<td class="cuerpo_iz">&nbsp;</td>
<td class="cuerpo_ce">
	<img src="<?php echo URL_PORTAL; ?>imagenes/spacer.gif" width="1" height="1"/> 
	<img src="<?php echo URL_PORTAL; ?>imagenes/spacer.gif" width="2" height="1"/> 
    <br/>
</td>
<td class="cuerpo_de">&nbsp;</td>
</tr>
<tr>
<td class="cuerpo_iz">&nbsp;</td>
<!-- TABLAS DE FORMULARIO -->
<td class="cuerpo_ce">
<center>
     <table width="90%" border="0" cellspacing="0" class="tablero">
            <tr>
              <td width="148">Buscar Por:</td>
              <td width="149"><select name="buscarPor" class="box1" id="buscarPor">
                <option value="1" selected="selected">IDENTIFICACION</option>
                <!--<option value="2" >RADICADO</option>-->
				<!--<option value="2">NOMBRE COMPLETO</option> -->
               <!-- <option value="2">PRIMER NOMBRE</option>
                <option value="3">PRIMER APELLIDO</option>-->
               </select></td>
              <td width="319">
              <select id="tipoDocumento" name="tipoDocumento" class="box1">
              <option value="1" selected="selected">C&eacute;dula ciudadan&iacute;a</option>
              <option value="2">Tarjeta de Identidad</option>
              <option value="3">Pasaporte</option>
              <option value="4">C&eacute;dula extranjer&iacute;a</option>
              
              </select>
              
              <input name="idT" type="text" class="box" id="idT"/> 
              <input type="text" class="box" id="afiliacion" style="display:none" />
              </td>
              <td width="316">
              <input name="buscar" type="button" class="ui-state-default" value="Buscar" onClick="buscarGrupoAfiliacion();"/>
                <span class="Rojo"></span>
               </td>
            </tr>
            
      </table>
      <div id="trabajadores" align="center"> </div>
          
    <br /><br />
	<small><strong>Afiliaciones Activas</strong></small><br />
	<table width="95%" border="0" cellspacing="0" class="tablero" id="tabTableAfiliacion">
	<thead>
		<tr>
			<th width="7%">Nit</th>
			<th width="18%">Razon Social</th>
			<th width="8%">Fecha Ingreso</th>
			<th width="5%">Tipo Form</th>
			<th width="7%">Tipo Afi</th>
			<th width="7%">Agr</th>		
			<th width="2%">Eliminar</th>
		</tr>
	</thead>
	<tbody>
	
	</tbody>
	</table> 
</center>

<td class="cuerpo_de"></td><!-- FONDO DERECHA -->
<tr>
<td height="41" class="abajo_iz">&nbsp;</td>
<td class="abajo_ce"></td>
<td class="abajo_de">&nbsp;</td>
</tr>
</table>
</div>
<div id="icon"><span></span></div>

<!-- FORMULARIO OBSERVACIONES-->
	<div name="div-observaciones-tab" style="display:none" title="Observaciones de modificaci&oacute;n">
	<table class="tablero">
	 <tr>
	   <td>Usuario</td>
	   <td colspan="3" >
	   <input name="usuarioObs" id="usuarioObs" class="box1" disabled="disabled" value="<?php echo $_SESSION['USUARIO']?>" /></td>
	   </tr>
	 <tr>
	   <td>Observaciones</td>
	   <td colspan="3" >
	   <textarea name="observacionGrupo" id="observacionGrupo" cols="45" rows="5" class="boxlargo"></textarea></td>
	   </tr>
	</table>
	<div class="ui-state-highlight ui-corner-all" style="margin: 9px auto; display:none;padding: 6px; text-align: center; border: 1px solid rgb(255, 255, 0); width:300px" id="rtaObservacion"><span class="ui-icon ui-icon-info" style="float: left; margin-right: 0.3em;"></span>Observaci&oacute;n guardada con &eacute;xito.</div>
	</div>

<br />
<input type="hidden" id="idPersona" name="idPersona" /> 
</body>

</html>