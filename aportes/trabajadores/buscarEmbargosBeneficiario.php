<?php
date_default_timezone_set('America/Bogota');
ini_set("display_errors",'1');
session_start();
$idp =$_REQUEST['v0'];

include_once '../../rsc/pdo/IFXDbManejador.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo $cadena;
	exit();
}
$filas = array();
$sql="SELECT a22.idbeneficiario,a22.idembargo,a18.idconyuge,a18.idtrabajador,
        a15.pnombre+' '+isnull(a15.snombre,'')+' '+a15.papellido+' '+isnull(a15.sapellido,'') as conyugue,
        b15.pnombre+' '+isnull(b15.snombre,'')+' '+b15.papellido+' '+isnull(b15.sapellido,'') as beneficiario
	  FROM aportes018 a18
      INNER JOIN aportes022 a22 ON a22.idembargo=a18.idembargo
      INNER JOIN aportes015 a15 ON a15.idpersona=a18.idconyuge
      INNER JOIN aportes015 b15 ON b15.idpersona=a22.idbeneficiario
	  WHERE a22.idbeneficiario=$idp";
$rs=$db->querySimple($sql);
if(is_null($rs)){
	echo 0;
	exit();
}
while ($row=$rs->fetch()) {
		$filas[] = $row;
	}
if(count($filas)>0)
	echo json_encode($filas);
else 
	echo 0;
?>