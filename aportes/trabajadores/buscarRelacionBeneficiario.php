<?php
date_default_timezone_set('America/Bogota');
ini_set("display_errors",'1');
session_start();
$idp =$_REQUEST['v0'];

include_once '../../rsc/pdo/IFXDbManejador.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo $cadena;
	exit();
}
$filas = array();
$sql="SELECT a15.identificacion,a15.pnombre+' '+isnull(a15.snombre,'')+' '+a15.papellido+' '+isnull(a15.sapellido,'') as Afiliado,
		b15.pnombre+' '+isnull(b15.snombre,'')+' '+b15.papellido+' '+isnull(b15.sapellido,'') as Beneficiario,
		isnull(a91.detalledefinicion,'') detalledefinicion,isnull(a21.estado,'') estado,isnull(a21.giro ,'') giro
	  FROM aportes021 a21 
	  INNER JOIN aportes015 a15 ON a15.idpersona=a21.idtrabajador
	  INNER JOIN aportes015 b15 ON b15.idpersona=a21.idbeneficiario
	  LEFT JOIN aportes091 a91 ON a91.iddetalledef=a21.idparentesco
	  WHERE a21.idbeneficiario=$idp";
$rs=$db->querySimple($sql);
if(is_null($rs)){
	echo 0;
	exit();
}
while ($row=$rs->fetch()) {
		$filas[] = $row;
	}
if(count($filas)>0)
	echo json_encode($filas);
else 
	echo 0;
?>