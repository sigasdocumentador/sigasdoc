<?php
/* autor:       orlando puentes
 * fecha:       21/04/2010
 * objetivo:    Procesar los movimientos de los Bonos almacenados en la base de datos. 
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$nit= $_REQUEST['nit'];
include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR .'empresa.principal.class.php';
/*include_once("../../clases/empresa.principal.class.php"); 22-02-2012 */
$objClase=new EmpresaP;
$consulta = $objClase->mostrar_registro($nit);
$row=mssql_fetch_array($consulta);
?>

<label class="Rojo">Datos del empleador</label>
<br>
<table width="100%" border="0" cellspacing="0" class="tablero">
  <tr>
    <td width="14%">NIT</td>
    <td width="86%"><?php echo $row['nit']; ?>&nbsp;</td>
  </tr>
  <tr>
    <td>Razon Social</td>
    <td><?php echo $row['razonsocial']; ?>&nbsp;</td>
  </tr>
  <tr>
    <td><p>Principal</p></td>
    <td><?php echo $row['principal']; ?>&nbsp;</td>
  </tr>
  <tr>
    <td>Estado</td>
    <td><?php echo $row['estado']; ?>&nbsp;</td>
  </tr>
  <tr>
    <td>Direcci&oacute;n</td>
    <td><?php echo $row['direccion']; ?>&nbsp;</td>
  </tr>
  <tr>
    <td>Telefono</td>
    <td><?php echo $row['telefono']; ?>&nbsp;</td>
  </tr>
  <tr>
    <td>Contratista</td>
    <td><?php echo $row['contratista']; ?>&nbsp;</td>
  </tr>
</table>
