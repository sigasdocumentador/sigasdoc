<?php
set_time_limit(0);
ini_set("display_errors",0);
date_default_timezone_set("America/Bogota");
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

if ( !isset( $_SESSION ['USUARIO'] ) ) {session_start();}
if(empty($_SESSION['LOGUIADO'])){
	echo "no hay session!!";
	exit();
}
$raiz=$_SESSION['RAIZ'];
include_once $raiz.DIRECTORY_SEPARATOR.'phpComunes'.DIRECTORY_SEPARATOR.'session.php';
session();
 
$idpersona= $_REQUEST['v0'];
$formulario = (isset($_REQUEST['v1']))?$_REQUEST['v1']:"";
include_once $raiz.DIRECTORY_SEPARATOR.'config.php';
include_once $raiz.DIRECTORY_SEPARATOR.'clases'. DIRECTORY_SEPARATOR.'ciudades.class.php';
include_once $raiz.DIRECTORY_SEPARATOR.'funcionesComunes'.DIRECTORY_SEPARATOR.'funcionesComunes.php';
include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'p.definiciones.class.php';
include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'p.afiliacion.class.php';

$isActive = true;
$fechaIngreso='';
$diasDiferenciaFechaIngreso=0;
$objClase=new Definiciones();
$objCiudad=new Ciudades();
$objAfiliacion= new Afiliacion();
if ($formulario == "M" || $formulario == "R" ) {
	$estadoConsulta="'A'";
	if ( $formulario == "R" ){ $estadoConsulta="'A','P'"; }
	$consulta0=$objAfiliacion->buscar_primera_afiliacion_activa($idpersona,$estadoConsulta);
	$cont=mssql_num_rows($consulta0);
	if(is_numeric($cont) && $cont>0){		
		$row0 = mssql_fetch_array($consulta0);		
		if ( $row0['maxIngreso'] != "1900-01-01" ) {
			$isActive = true;
			$fechaIngreso = $row0['maxIngreso'];
			$diasDiferenciaFechaIngreso = $row0['diferencia'];
		}
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Actulizar Grupo</title>
<script type="text/javascript">
	/** DOCUMENT READY **/
	$(document).ready(function(){
		$("#divControlDocumentos").dialog({
			autoOpen:false,
			modal:true,
			width:500,
			height:400,
			resizable:true,
			close:function(){
				//$("#divControlDocumentos").dialog('close');
			},
			open:function(){
				var parm=$('#divControlDocumentos').data('idr');
				$("#idresp").val(JSON.stringify(parm))
				},
			buttons: {
				'Grabar': function() {
					    
					    objaux=JSON.parse($("#idresp").val());

	                    var ctrverificar;
                        var ctrcan=0;
                        var ctrchecked=0;
                        $.each(objctrdocumentos.data,function(index,value){
                        	ctrverificar = $("#"+value.iddatos).prop( "checked")?1:0;
                        	
                        	if(ctrverificar==1)
                        		ctrchecked=1;
                    		
                            if(value.controlexiste!=ctrverificar){
                            	ctrcan=1;
                            	return false;
                            }
                        }); 

                        if(ctrcan==0 && objaux.giroAnt=='N'){
                          alert('No se detecta modificacion alguna para poder continuar con el proceso');
                          return false;
                        }else if (ctrcan==0 && objaux.giroAnt=='S' && ctrchecked==1){
                        	alert('No ha habido ninguna modificacion para poder continuar con el proceso');
                            return false;  
                        }
			            
		            	var datosactualizar = new Array();
	                    $("div#tablactrdocumentos input:checkbox:checked").each(function (index,value){
	                        datosactualizar[index]=$(this).val();
	                    });
	                    
	                    var objdata={
	                            data:datosactualizar,
	                            even:"actualizar"
	                            }
	                    var datares=ctrcontroldocumentos($.extend(objaux,objdata));
	                    if(datares.error==0){
		                    

		                    // si los documentos obligatorios esta totalmente diligenciados se llama la funcion de validacion  para pasar a estado S
		                    if(datares.data.oblinomarcados==0){
		                    	//actualizarEstadoGiroBeneficiario(objaux->idr,'N',objaux->fechaAsignacion,objaux->modificacion,objaux->capacidad,objaux->parentesco,objaux->varFcNace,objaux->idbenef,objaux->estado21,objaux->estado15);
		                    	actualizarEstadoGiroBeneficiario(objaux.idr,'N',objaux.fechaAsignacion,objaux.modificacion,objaux.capacidad,objaux.parentesco,objaux.varFcNace,objaux.idbenef,objaux.estado21,objaux.estado15);
		                    }else{
		                    	alert('Actualizacion exitosa');
		                    	buscarGrupo();
	                            observacionesTab(idPersona,1);
			                }
		                    $(this).dialog('close');
                            
		                }else{
			                alert(datares.descripcionerror);
			                buscarGrupo();
			            }
	                
				    
				},
				'Cancelar':function(){
					$("#divControlDocumentos").dialog('close');
			    }		  
			},//fin funcion boton grabar
		});
				
		/** NUEVO CONYUGE **/		
		$("#cboCiudadC").jCombo("2", { 
			parent: "#cboDeptoC",
			selected_value: '41001'
		});
		$("#cboZonaC").jCombo("3", {
			parent: "#cboCiudadC"
		});	
		$("#cboBarrioC").jCombo("4", {
			parent: "#cboZonaC"
		});	
		
		$("#cboDeptoC2").jCombo("1", {
			parent: "#cboPaisC2",
			selected_value: '41' 
		});	
		$("#cboCiudadC2").jCombo("2", { 
			parent: "#cboDeptoC2",
			selected_value: '41001'
		});

		/** MODIFICAR CONYUGE **/		
		$("#cboCiudadCA").jCombo("2", { 
			parent: "#cboDeptoCA",
			selected_value: '41001'
		});
		$("#cboZonaCA").jCombo("3", {
			parent: "#cboCiudadCA"
		});	
		$("#cboBarrioCA").jCombo("4", {
			parent: "#cboZonaCA"
		});	
		
		$("#cboDeptoC2A").jCombo("1", {
			parent: "#cboPaisC2A",
			selected_value: '41' 
		});	
		$("#cboCiudadC2A").jCombo("2", { 
			parent: "#cboDeptoC2A",
			selected_value: '41001'
		});		

		/** NUEVO BENEFICIARIO **/
		$("#cboDeptoB").jCombo("1", {
			parent: "#cboPaisB",
			selected_value: '41' 
		});	
		$("#cboCiudadB").jCombo("2", { 
			parent: "#cboDeptoB",
			selected_value: '41001'
		});

		/** MODIFICAR BENEFICIARIO **/
		$("#cboDBDeptoNace").jCombo("1", {
			parent: "#cboDBPaisNace",
			selected_value: '41' 
		});	
		$("#cboDBCiudadNace").jCombo("2", { 
			parent: "#cboDBDeptoNace",
			selected_value: '41001'
		});

		if ( $("#formularioAfi").val() == "M"  ) {
			txtHiddenSexo = "txtHiddenSexo";
		} else if ( $("#formularioAfi").val() == "R"  ) {
			txtHiddenSexo = "txtHiddenSexoR";
		}

		$("#cedMama,#cedMamaUpd").bind("change",function(){					
			if ( ! $("#idPersona").val() > 0 ) {							
				$(this).val("undefined");
				return false; 
			} else if ( $(this).get(0).id == "cedMamaUpd" ) {
				if ( $("#estadoUpd").val() == "I" || $("#tipoAfiliacion3Upd").val() == "49" ){
					return false;
				}
				var parentesco = $("#parentescoUpd").val();
			} else {
				var parentesco = $("#parentesco").val();
			}
			
			var valor = $(this).val();
			var opcion = $(this).find("option:selected");
			var idconyTmp = opcion.attr("name");
			
			if( valor != 0 && valor != 'undefined' ){			
				if( parentesco == 38 && valor != "otro" ) {				
					var data=dataRelaciones;
					conviven = false;
					if ( !( data == 0 || data == null )){
						$.each(data,function(i,fila){
							if ( fila.idconyuge == idconyTmp && fila.conviven == "S" ) {
								conviven = true;
							}
						});
					} 
					
					if ( conviven == false ) {
						alert("La persona seleccionada no tiene convienvia (S) con el afiliado.\n No se puede afiliar al beneficiario.");
	            		$(this).val("undefined");
	            		return false;
					}				
				}
			} else if ( parentesco == 38 && valor == 0 ) {
				alert("Si es un HIJASTRO debe seleccionar la Cedula de padre o madre con la cual tenga CONVIVENCIA.");
	    		$(this).val("undefined");
	    		return false;
			}
		});

		setDatepickerFechaAsignacion();		
	});

	var anoActual = new Date();
	var strYearRange = anoActual.getFullYear()-120 +":"+ anoActual.getFullYear();
	
	$("#fAsignacion,#fEscolaridad,#fUniversidad,#fDiscapacidad,#fSupervivencia,#txtFAsignacion,#txtFechaAfiliacion").datepicker( {
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		minDate: '-6M',
		maxDate: '+0D', 
	});
	
	$(function() {
		$("#fechaRetiro").datepicker({
			changeMonth: true,
			changeYear: true,
			maxDate: "+0D"
		});
		var anoActual = new Date();
		var strYearRange = anoActual.getFullYear()-120 +":"+ anoActual.getFullYear();
		$(".box1").datepicker( "option", "yearRange", strYearRange );
	});
	
	$( "#fecNacHide3,#fecNac3Upd,#alternate2,#alternate2A,#pPresentacion,#txtDBfecNace,#txtFechaDefActEstado").datepicker({
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
	    maxDate: '+0D', 
		constrainInput: false,
	});
	
	$("#fecNacHide3").datepicker("option",{close:function(){
		//validarCertificados();
	}});
	$("#fecNacHide3Upd").datepicker("option",{close:function(){
		
	}});
	$( "#fecNacHide2" ).datepicker({
		changeMonth: true,
		changeYear: true,
		minDate: new Date(1940, 1, 1),
		maxDate: '-10Y'  
	});
	$(function(){
		$("#divMotivos").dialog({
			autoOpen:false,
			modal:true,
			width:500,
			height:150,
			resizable:false,
			open: function(){
				$("#motivo").val(0).trigger("change");
			}
		});
		$("#divDocumentoDoble").dialog({
			autoOpen:false,
			modal:true,
			width:500,
			height:150,
			resizable:true
		});
	});	
	$(function(){
		$("#divRelacionesActivas").dialog({
			autoOpen:false,
			modal:true,
			width:600,
			height:120,
			resizable:false
		});
	});
	/*
	$(function(){
		
	});	*/
	$(".ui-datepicker").css("z-index",3000);
	$(".box1").datepicker( "option", "yearRange", strYearRange );
	$(".hasDatepicker").datepicker( "option", "yearRange", strYearRange );
	buscarRelaciones();
	buscarGrupo();
</script>
<script>
	// increase the default animation speed to exaggerate the effect
	$.fx.speeds._default = 1000;
	$(function() {
		$( "#beneficiarios" ).dialog({
			autoOpen: false,
			width:735,
			show: "drop",
			hide: "clip",
			open:function(event,ui){
				$("#parentesco").focus();
				$( "#lblAsignacion,#divAsignacion" ).hide();				
		    },//fin funcion open		    
			buttons: {
				'Grabar': function() {
					verificarB();
				}		  
			},//fin funcion boton grabar			
		    close:function(){
				$("#beneficiarios ul").remove();
				$("#beneficiarios  table.tablero input:text").val('');
				$("#beneficiarios  table.tablero input:hidden").val('');//fecha nacimiento
				$("#beneficiarios  table.tablero select").val('Seleccione');
				$("#beneficiarios input:checkbox,#beneficiarios input:radio").attr("checked",false);
				// TABLA DINAMICA PARA DATOS EMPRESA TRABAJDOR ACTIVO
				$("table[name='dinamicTable']").remove();
				$("#cedMama").attr("disabled",false);
		    }//fin funcion close
		});//fin dialog

		$("#div-conyuge").dialog({
			autoOpen:false,
			width:740,
			show: "drop",
			hide: "clip",	
			open:function(event,ui){
				limpiarConyuge();				
				$("#tipoDoc2").val("1");								
			},//fin funcion open
			
			buttons: {
			    'Grabar': function() {
					verificarC();
				}		  
			},//fin funcion boton grabar
			
			close:function(){
				$("#div-conyuge ul").remove();
				$("#div-conyuge table.tablero input:text").val('');
				$("#div-conyuge table.tablero select").val('Seleccione');
				$("#div-conyuge input:checkbox").attr("checked",false);
				$("table[name='dinamicTable']").remove();
			}//fin funcion close
		});//fin dialog
	
		$( "#botonNuevo" ).click(function() {

			//Por si se requiere validar que el trabajador este activo a la hora de ingresar
			//un conyugue o beneficiario
			/*$.ajax({
				url: URL+'phpComunes/buscarAfiliacion.php',
				type: "GET",
				async:false,
				data: {v0: idPersona},
				dataType: 'json',
				success: function(datos){
					if(datos!=0){*/
						if($("#selParentesco").val()=='0'){
							alert("Seleccione un parentesco.");
						}			
						if($("#selParentesco").val()=='c'){
							$("#nuevoGrupo").next("span.Rojo").html("");
							$("#div-conyuge").dialog('open');							 
							$("#identificacion2").focus();
						}								 
						if($("#selParentesco").val()=='b'){
							$("#nuevoGrupo").next("span.Rojo").html("");
							$("#beneficiarios").dialog('open');
							validarComboParentesco();
						}
						return false;
					/*}else{
						alert("El Estado del Trabajador es Inactivo");
					}
				}
			});*/
			
			
		});//fin funcion click botonNuevo
	});
	function actDatepickerPeriodoImgs(objeto){
		objeto.datepicker({
			dateFormat: 'yymm',
			changeMonth: true,
			changeYear: true,
			showButtonPanel: true,
			maxDate: "+0D",
			minDate: "-2M",
			onClose: function(dateText, inst) {
				var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
				var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
				$(this).datepicker('setDate', new Date(year, month, 1));
				validarDatepickerPeriodoImgs();
			} 
		});
		objeto.focus(function () {
			$(".ui-datepicker-calendar").hide();
			$("#ui-datepicker-div").position({
				my: "center top",
				at: "center bottom",
				of: $(this)
			});
		}); 
	}

	/**
	 * Validar fecha inicial menor o igual a fecha final
	 */
	function validarDatepickerPeriodoImgs(){
		$("#txtPeriodoFinal").removeClass("ui-state-error");
		var fechaIni=$("#pInicialV").val();
		var fechaFin=$("#pFinalV").val();
		
		if(fechaIni != "" && fechaFin != ""){
			var anioIni=fechaIni.slice(0,4);
			var mesIni=eval(fechaIni.slice(-2));
			var anioFin=fechaFin.slice(0,4);
			var mesFin=eval(fechaFin.slice(-2));
			
			if((anioFin<anioIni) || (anioFin==anioIni && mesFin<mesIni)){
				$("#pFinalV").val('');
				$("#pFinalV").addClass("ui-state-error");
				$("#lEstado").focus();
			}
		}
	}
	
	$(function(){
		actDatepickerPeriodoImgs($("#pInicialV"));
		actDatepickerPeriodoImgs($("#pFinalV"));
		
		var fecha = new Date();
		var ano = fecha.getFullYear();		
		var ano1= ano - 1 ;		
				
		$("#div-datosCertificados").dialog({
			autoOpen:false,
			width:640,
			show: "drop",
			hide: "clip",	
			close: function(event,ui){
				$("#div-datosCertificados table tbody").empty();
			}
		});//fin dialog
	});


	//Boton modificar Certificado
	$("[name='actCer']").live("click",function(){
			//Id global del certificado seleccionado
		 idcertificado=$(this).parent().parent().children(":eq(0)").text();
		 tipocertificado=$(this).parent().parent().children(":eq(1)").text();
		 pinicial=$(this).parent().parent().children(":eq(2)").text();
		 pfinal=$(this).parent().parent().children(":eq(3)").text();
		 fpresentacion=$(this).parent().parent().children(":eq(4)").text();
		 estado=$(this).parent().parent().children(":eq(5)").text();
		 //alert(fpresentacion);
		//Enviar parametros
		 $("#div-datosCertificados").dialog('close');	
		 buscarCertificadoUpdate(idcertificado,tipocertificado,pinicial,pfinal,fpresentacion,estado);
		 
	});

	//Boton Borrar Certificado
	$("[name='BorrarCer']").live("click",function(){
			//Id global del certificado seleccionado
		 idcertificado=$(this).parent().parent().children(":eq(0)").text();
		 confirma = confirm("ESTA SEGURO DE BORRAR ESTE CERTIFICADO CON ID " +idcertificado+ " ?");
		 if(confirma){
			var datos={v0:idcertificado};
			$.ajax({
				url: URL+'phpComunes/borrarCertificado.php',
				type: "POST",
				data: datos,
				async: false,
				//dataType: "json",
				success: function(data){
			 		if(data==0){
			 			alert("No se pudo borrar el Certificado!");
			 			return false;
					}
			 		alert("Se borro el Certificado.");
			 		observacionesTab(idPersona,1);
			 		$("#div-datosCertificados").dialog('close');	
				}
			});//ajax
		 }
			 
			 
		 
		//Enviar parametros
		 //buscarCertificadoUpdate(idcertificado,tipocertificado,pinicial,pfinal,fpresentacion,estado);
	});
	
	function mostrarDatosCertificados(idBeneficiario){
		$("#div-datosCertificados table tbody").empty();
		$.getJSON(URL+"phpComunes/buscarCertificados.php", {idBeneficiario: idBeneficiario}, function(data){
			if(data.length > 0){
				$("#div-datosCertificados").dialog('open');
				$.each(data,function(i,fila){
					//var strTr = "<tr><td>"+ fila.idcertificado +"</td><td>"+ fila.tipo_certificado +"</td><td>"+ fila.periodoinicio +"</td><td>"+ fila.periodofinal +"</td><td>"+ fila.fechapresentacion +"</td><td>"+ fila.estado +"</td><td style='text-align:center'><img style='cursor:pointer' src='"+URL+"imagenes/menu/modificar.png' id='actCer"+fila.idcertificado+"' name='actCer' title='Modificar Certificado' /></td><td style='text-align:center'><img style='cursor:pointer' src='"+URL+"imagenes/borrar.png' id='BorrarCer"+fila.idcertificado+"' name='BorrarCer' title='Borrar Certificado' /></td></tr>";
					var strTr = "<tr><td>"+ fila.idcertificado +"</td><td>"+ fila.tipo_certificado +"</td><td>"+ fila.periodoinicio +"</td><td>"+ fila.periodofinal +"</td><td>"+ fila.fechapresentacion +"</td><td>"+ fila.estado +"</td><td style='text-align:center'><img style='cursor:pointer' src='"+URL+"imagenes/menu/modificar.png' id='actCer"+fila.idcertificado+"' name='actCer' title='Modificar Certificado' /></td></tr>";
					$("#div-datosCertificados table tbody").append(strTr);
				});
			}
		});
	}

	function validarPeriodosDatePicker(idCampoPeriodoI,idCampoPeriodoF,idSpamError){
		$("#"+idSpamError).html("");
		var campoI = $("#"+idCampoPeriodoI);
		var campoF = $("#"+idCampoPeriodoF);
		
		if(campoI.val() != "" && campoF.val() != ""){
			var anoI = campoI.val().slice(0,4);
			var mesI = eval(campoI.val().slice(-2));
			var anoF = campoF.val().slice(0,4);
			var mesF = eval(campoF.val().slice(-2));
			
			
			var dtI = new Date();
			if(parseInt(mesI) == 1)
				mesI = 0;
			else
				mesI = parseInt(mesI) - 1;
			dtI.setFullYear(parseInt(anoI), parseInt(mesI) , 1);
			var dtF = new Date();
			if(parseInt(mesF) == 1)
				mesF = 0;
			else
				mesF = parseInt(mesF) - 1;
			dtF.setFullYear(parseInt(anoF), parseInt(mesF), 1);
			
			if((dtF.getTime() / (60 * 60 * 24 * 1000)) < (dtI.getTime() / (60 * 60 * 24 * 1000)))
				$("#"+idSpamError).html("Periodo final menor que el periodo inicial!");
		}
	}
</script>
</head>
<body>
<input type="hidden" id="formularioAfi" value="<?php echo $formulario; ?>" />
<p style="font-weight:bold">Relación nueva de:
<select name="selParentesco" class="odd" id="selParentesco">
	<option selected="selected" value="0">Seleccione..</option>
   	<option value="c">CONYUGE</option>
   	<option value="b">BENEFICIARIO</option>
</select><!--onclick="nuevoGrupo()-->
<?php if ( $isActive == true ): ?>
    <input type="button" class="ui-state-default" value="Nuevo" id="botonNuevo"  /><!-- el evento esta directo en grupotab.js-->
<?php endif; ?>
<span class="Rojo"></span></p>
<small><strong>Relaciones de convivencia registradas</strong></small>
<br />
<table width="95%" border="0" cellspacing="0" class="tablero" id="tabTableRelaciones">
	<thead>
	<tr>		
        <th width="3%">TD</th>
		<th width="8%">N&uacute;mero</th>
		<th width="40%">Conyuge</th>
		<th width="5%">Convive</th>
		<th width="6%">F Nace</th>
		<th width="12%">Ult. Cambio Conv.</th>
		<th width="5%">Labora</th>
		<th width="7%">Salario</th>
		<th width="6%">F Ingreso</th>
		<th width="5%">Modificar</th>
	</tr>
	</thead>
	<tbody></tbody>     
</table>
<br /><br />
<small><strong>Grupo familiar registrado</strong></small><br />
<table width="95%" border="0" cellspacing="0" class="tablero" id="tabTableGrupo">
	<thead>
		<tr>
			<th width="3%">TD</th>
			<th width="7%">N&uacute;mero</th>
			<th width="30%">Beneficiario</th>
			<th width="8%">Parentesco</th>
			<th width="3%">Est</th>		
			<th width="7%">F Nace</th>
			<th width="3%">Edad</th>
			<th width="4%">F Afil</th>
			<th width="4%">F Cert</th>
			<th width="4%">F Asig</th>
			<th width="9%">Relaci&oacute;n</th>
			<th width="3%">Giro.</th>
			<th width="3%">Dis</th>
			<th width="3%">Emb</th>
			<th width="4%">Est Afil</th>
			<th width="5%">Modificar</th>
		</tr>
	</thead>
	<tbody>
	
	</tbody>
</table>  
     
   <!-- CONYUGES -->
   <table border="0"  cellpadding="0" cellspacing="0" class="tablero" style="margin:0; display:none;" id="tablaC">
   <caption><small>CONYUGE</small></caption>
    <thead>
    <tr>
    <th width="20%">Tipo Documento</th>
    <th width="10%">Identificaci&oacute;n</th>
    <th width="20%">Nombre Completo</th>
    <th width="13%">Tipo Relaci&oacute;n</th>
    <th width="8%">Conviven</th>
    <th width="8%">Acci&oacute;n</th>
  </tr>
  </thead>
  <tbody>
  </tbody>
</table>

<!-- BENEFICIARIOS -->
 <table border="0"  cellpadding="0" cellspacing="0" class="tablero" style="margin:0;display:none;" id="tablaB">
  <caption><small>BENEFICIARIO</small></caption>
  <thead>
    <tr>
    <th width="19%">Tipo Documento</th>
    <th width="9%">Identificaci&oacute;n</th>
    <th width="7%">Relaci&oacute;n</th>
    <th width="8%">Parentesco</th>
    <th width="18%">Nombre Completo</th>
    <th width="13%">Fecha de Nacimiento</th>
    <th width="7%">Tipo Afiili.</th>
    <th width="9%">Capacidad Tr.</th>
    <th width="5%">Edad</th>
    <th width="5%">Acci&oacute;n</th>
  </tr>
  </thead>
  <tbody>
  </tbody>
</table>

<!-- VENTANA CONYUGE DIALOG style="display:none;" -->
<div id="div-conyuge" title="::FORMULARIO NUEVO CONYUGE::" style="display:none">
	<h4 align="left"> DATOS DEL CONYUGE - Nueva relación</h4>
	<table width="100%"  border="0" cellspacing="0" class="tablero">		
		<tr>
			<td width="25%">Tipo Documento</td>
			<td width="25%">
				<select name="tipoDoc2" class="box1" id="tipoDoc2" onchange="$('#identificacion2').val('').trigger('blur');">
					<option value="0">Seleccione..</option>
				<?php
					$consulta=$objClase->mostrar_datos(1, 2);
					while($row=mssql_fetch_array($consulta)){
						echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
					}
				?>
				</select> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
			</td>
			<td width="25%">N&uacute;mero Documento</td>
			<td width="25%">
				<input name="identificacion2" type="text" class="box1" id="identificacion2" onblur="buscarConyugeNuevo(this);" onkeyup="validarCaracteresPermitidos(document.getElementById('tipoDoc2').value,this);" 
					onkeydown="validarCaracteresPermitidos(document.getElementById('tipoDoc2').value,this);" onkeypress="validarCaracteresPermitidos(document.getElementById('tipoDoc2').value,this);" />
				 <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
			</td>
		</tr>         
		<tr>
			<td>Primer Nombre</td>
			<td class="box1">
				<input name="pNombre2" type="text" class="box1" id="pNombre2" onkeypress="return vpnombre(event)" onchange="buscarPersonaExistente($('#identificacion2').val(),$('#pNombre2').val(),$('#sNombre2').val(),$('#pApellido2').val(),$('#sApellido2').val(),true,idcony)" />
				 <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
			</td>
			<td>Segundo Nombre</td>
			<td>
				<input name="sNombre2" type="text" class="box1" id="sNombre2" onkeypress="return vsnombre(event)" onchange="buscarPersonaExistente($('#identificacion2').val(),$('#pNombre2').val(),$('#sNombre2').val(),$('#pApellido2').val(),$('#sApellido2').val(),true,idcony)" />
			</td>
		</tr>
		<tr>
			<td>Primer Apellido</td>
			<td>
				<input name="pApellido2" type="text" class="box1" id="pApellido2" onkeypress="return vpnombre(event)" onchange="buscarPersonaExistente($('#identificacion2').val(),$('#pNombre2').val(),$('#sNombre2').val(),$('#pApellido2').val(),$('#sApellido2').val(),true,idcony)" />
				 <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
			</td>
			<td>Segundo Apellido</td>
			<td><input name="sApellido2" type="text" class="box1" id="sApellido2" onkeypress="return vsnombre(event)" onchange="buscarPersonaExistente($('#identificacion2').val(),$('#pNombre2').val(),$('#sNombre2').val(),$('#pApellido2').val(),$('#sApellido2').val(),true,idcony)" /></td>
		</tr>
		<tr>
			<td width="25%">Tipo de relaci&oacute;n</td>
			<td width="25%">
				<select name="tipRel" class="box1" id="tipRel">
					<option selected="selected" value="0">Seleccione..</option>
					<option value="2147">CONYUGE</option>
					<option value="2148">COMPA&Ntilde;ERO(A)</option>
				</select> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
			</td>
			<td width="25%">Conviven</td>
			<td width="25%">
				<select name="conviven" class="box1" id="conviven">
					<option value="0">Seleccione..</option>
					<option value="S">SI</option>
					<option value="N">NO</option>
				</select> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
			</td>
		</tr>
		<tr>
			<td>G&eacute;nero</td>
			<td>
				<select name="sexo2" class="box1" id="sexo2">
					<option selected="selected" value="0">Seleccione..</option>
					<option value="M">Masculino</option>
					<option value="F">Femenino</option>
				</select> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
			</td>
			<td>Tipo Vivienda</td>
			<td>
				<select name="tipoVivienda2" class="box1" id="tipoVivienda2">
  					<option value="0">Seleccione..</option>
  					<option value="U">Urbana</option>
  					<option value="R">Rural</option>
				</select>
			</td>
		</tr>		
		<tr>
  			<td>Departamento Residencia</td>
  			<td>
  				<select name="cboDeptoC" class="box1" id="cboDeptoC">
    				<option value="0">Seleccione..</option>
    			<?php
					$consulta=$objCiudad->departamentos();
					while($row=mssql_fetch_array($consulta)){
				?>
    				<option value="<?php echo $row["coddepartamento"]; ?>"><?php echo utf8_encode($row["departmento"]); ?></option>
    			<?php } ?>
  				</select>
  			</td>
  			<td>Ciudad Residencia</td>
  			<td>
  				<select name="cboCiudadC" class="box1" id="cboCiudadC">
  					<option value="0">Seleccione..</option>
  				</select>
  			</td>
		</tr>
		<tr>
  			<td>Zona</td>
  			<td>
  				<select name="cboZonaC" class="box1" id="cboZonaC">
  					<option value="0">Seleccione..</option>
  				</select>
  			</td>
  			<td>Barrio</td>
  			<td>
  				<select name="cboBarrioC" class="box1" id="cboBarrioC">
    				<option value="0">Seleccione..</option>
  				</select>
  			</td>
		</tr>
		<tr>
			<td>Direcci&oacute;n </td>
			<td>
				<input name="direccion2" type="text" class="box1" id="direccion2" onfocus="direccion(this,document.getElementById('cboBarrioC'));" />
			</td>
			<td>E-mail</td>
			<td><input name="email2" type="text" class="box1" id="email2" /></td>
		</tr>
		<tr>
			<td>Tel&eacute;fono</td>
			<td><input name="telefono2" type="text" class="box1" id="telefono2" maxlength="7" /></td>
			<td>Celular</td>
			<td><input name="celular2" type="text" class="box1" id="celular2" maxlength="10" /></td>
		</tr>
		<tr>
			<td>Fecha De Nacimiento</td>
			<td><input type="text" id="alternate2" readonly="readonly"/>
			<td>Pais Nacimiento</td>
			<td>
				<select name="cboPaisC2" class="box1" id="cboPaisC2">
					<option value="0">Seleccione</option>
				<?php
					$consulta=$objCiudad->paises();
					while($row=mssql_fetch_array($consulta)){
				?>          
					<option value="<?php echo $row["idpais"]; ?>"><?php echo utf8_encode($row["pais"]); ?></option>
				<?php } ?>
				</select>
			</td>
		</tr>
		<tr>
			<td>Departamento Nacimiento</td>
			<td>
				<select name="cboDeptoC2" class="box1" id="cboDeptoC2">
					<option value="0">Seleccione</option>
				</select>
			</td>
			<td>Ciudad Nacimiento</td>
			<td>
				<select name="cboCiudadC2" class="box1" id="cboCiudadC2">
					<option value="0">Seleccione</option>
				</select>
			</td>
		</tr>		
		<tr id="trInformacionAfiliacion" >
			<td>Información Afiliación</td>
			<td colspan="3">
				<select name="estadoCivil2" class="box1" id="estadoCivil2" style="display:none">
  				<?php
					$consulta=$objClase->mostrar_datos(10, 3);
					while($row=mssql_fetch_array($consulta)){
						echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
					}
				?>
				</select>
			</td>
		</tr>
		<tr id="trInformacionAfiliacion2">
  			<td>Nit</td>
  			<td><input name="nitConyuge" type="text" class="box1" id="nitConyuge" readonly="readonly" /></td>
  			<td>Empresa donde trabaja</td>
  			<td><input name="empCony" type="text" class="box1" id="empCony" readonly="readonly" /></td>
		</tr>
		<tr id="trInformacionAfiliacion3">
			<td>Salario </td>
			<td><input name="salCony" type="text" class="box1" id="salCony" readonly="readonly" /></td>
			<td>&iquest;Subsidio?</td>
			<td><input name="subCony"  type="text" class="box1" id="subCony" readonly="readonly" /></td>
		</tr>		
	</table>
	<div style="display:none">
		<input type="hidden" name="capTrabajo2" id="capTrabajo2" value="N" />
	</div>
	<div id="div-relacion"></div>
</div>

<!-- VENTANA DIALOG BENEFICIARIOS style="display:none" -->
<div id="beneficiarios" title="::Formulario Nuevo Beneficiario::" style="display:none">
	<br />
  	<table width="100%" border="0" cellspacing="0" class="tablero">
    	<tr>
    		<td>Tipo de beneficiario</td>
    		<td>
    			<select name="parentesco" class="box1" id="parentesco" onchange="validarComboParentesco();">
      				<option value='0' selected="selected">Seleccione..</option>
      				<option value="35">Hijo</option>
      				<option value="38">Hijastro</option>
      				<option value="36">Padre/Madre</option>
      				<option value="37">Hermano(a)</option>
    			</select> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
    		</td>
    		<td>C&eacute;dula Padre &oacute; Madre</td>
    		<td>
    			<select name="cedMama" id="cedMama" class="box1">
      				<option value="undefined" selected="selected">Seleccione.. </option>
      				<option value="0" name="0">HIJO NATURAL </option>
    			</select> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
    		</td>
  		</tr>
		<!-- padre biologico -->
		<tr id="padreBiologico" style="display:none">
    		<td>Tipo Id. Padre Biol&oacute;gico</td>
    		<td>
    			<select name="tipoIdPadreBiol" class="box1" id="tipoIdPadreBiol" onchange="$('#IdPadreBiol').val('').trigger('blur');" >
    				<option selected="selected" value="0">Seleccione..</option>
				<?php
					$consulta=$objClase->mostrar_datos(1, 2);
					while($row=mssql_fetch_array($consulta)){
						echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
					}
				?>
    			</select> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
    		</td>
    		<td>N&uacute;mero Padre Biol&oacute;gico</td>
    		<td>
    			<input name="IdPadreBiol" type="text" class="box1" id="IdPadreBiol" onblur="buscarPersonaMostrar($('#tipoIdPadreBiol'),$('#IdPadreBiol'),$('#hideIdbiol'),$('#nomBiologico'));"/>
    			 <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
    		</td>
    		<input type="hidden" name="hideIdbiol" id="hideIdbiol" />
		</tr>
		<tr id="nombreBiologico" style="display:none">
  			<td>Padre Biol&oacute;gico</td>
  			<td colspan="3" id="nomBiologico">&nbsp;</td>
  		</tr>
		<!-- fin biologico-->  
  		<tr>
  			<td>Tipo formulario</td>
  			<td>
  				<select name="tipoAfiliacion3" class="box1" id="tipoAfiliacion3" onchange="validarTipoFormulario();" >
  					<option selected="selected" value="0">Seleccione..</option>
  					<option value="48">Subsidio</option>
  					<option value="49">Servicios</option>
  				</select> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
  			</td>
  			<td>Discapacidad</td>
  			<td>
  				<select name="capTrabajo" class="box1" id="capTrabajo" onchange="validarEdad(0);">
     				<option value="N">NO</option>
     				<option value="I">SI</option>
  				</select> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
  			</td>
		</tr>
  		<tr>
  			<td >Tipo Documento B</td>
  			<td >
  				<select name="tipoDoc3" class="box1" id="tipoDoc3" onchange="$('#identificacion3').val('').trigger('blur');" >
  					<option selected="selected" value="0">Seleccione..</option>
  				<?php
					$consulta=$objClase->mostrar_datos(1, 2);
					while($row=mssql_fetch_array($consulta)){
						echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
					}
				?>
  				</select> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
  			</td>
  			<td >No. Identififcaci&oacute;n</td>
  			<td >
  				<input name="identificacion3" type="text" class="box1" id="identificacion3" onblur="buscarPersonaTres(this);" onkeyup="validarCaracteresPermitidos(document.getElementById('tipoDoc3').value,this);" />
  				 <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
  			</td>
		</tr>
		<tr>
			<td>Primer Nombre</td>
			<td>
				<input name="pNombre3" type="text" class="box1" id="pNombre3" onkeypress="return vpnombre(event)" onchange="buscarPersonaExistente($('#identificacion3').val(),$('#pNombre3').val(),$('#sNombre3').val(),$('#pApellido3').val(),$('#sApellido3').val(),true,existe)" />
				 <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
			</td>
			<td>Segundo Nombre</td>
			<td><input name="sNombre3" type="text" class="box1" id="sNombre3" onkeypress="return vsnombre(event)" onchange="buscarPersonaExistente($('#identificacion3').val(),$('#pNombre3').val(),$('#sNombre3').val(),$('#pApellido3').val(),$('#sApellido3').val(),true,existe)" /></td>
		</tr>
		<tr>
			<td>Primer Apellido</td>
			<td>
				<input name="pApellido3" type="text" class="box1" id="pApellido3" onkeypress="return vpnombre(event)" onchange="buscarPersonaExistente($('#identificacion3').val(),$('#pNombre3').val(),$('#sNombre3').val(),$('#pApellido3').val(),$('#sApellido3').val(),true,existe)" />
				 <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
			</td>
			<td>Segundo Apellido</td>
			<td><input name="sApellido3" type="text" class="box1" id="sApellido3" onkeypress="return vsnombre(event)" onchange="buscarPersonaExistente($('#identificacion3').val(),$('#pNombre3').val(),$('#sNombre3').val(),$('#pApellido3').val(),$('#sApellido3').val(),true,existe)" /></td>
		</tr>
		<tr>
    		<td>Fecha de Nacimiento</td>
  			<td>
  				<input name="fecNacHide3" type="text" class="box1" id="fecNacHide3" readonly="readonly" onchange="validarEdad(0);"/>
  				 <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12"/>
 			</td>
 			<td>Pais Nace</td>
 			<td>
  				<select name="cboPaisB" class="box1" id="cboPaisB"  >
    				<option value="0">Seleccione</option>
    			<?php
					$consulta=$objCiudad->paises();
					while($row=mssql_fetch_array($consulta)){ ?>
    				<option value="<?php echo $row["idpais"]; ?>"><?php echo $row["pais"]?></option>
    			<?php } ?>
  				</select> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
  			</td>
  		</tr>
		<tr>
  			<td>Depto Nace</td>
  			<td>
  				<select name="cboDeptoB" class="box1" id="cboDeptoB"  >    				
  				</select> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
  			</td>
  			<td>Ciudad Nace</td>
  			<td>
  				<select name="cboCiudadB" class="box1" id="cboCiudadB">
  				</select> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
  			</td>
		</tr>		
		<tr>
			<td>Genero</td>
			<td>
				<select name="sexo3" class="box1" id="sexo3">
					<option selected="selected" value="0">Seleccione..</option>
					<option value="M">Masculino</option>
					<option value="F">Femenino</option>
				</select> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
			</td>
			<td><label id="lblAsignacion">Fecha Asignación</label></td>
			<td>
				<div id="divAsignacion">
					<input type="text" class="box1" name="fAsignacion" id="fAsignacion" readonly="readonly" onchange="$('#pInicialV3,#pFinalV3').val(''); $('#vigencia1,#vigencia2').attr('checked',false); " />
				 	 <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
				 </div>
			</td>
		</tr>
		<tr>
  			<td colspan="4" style="text-align:center">
  				<input type="hidden" name="estadoCivil3" id="estadoCivil3" value="50" />
  				<label style="font-weight:bold; color:#F00; text-align:center">Si anexa certificados Grabelos!</label>
  			</td>
  		</tr>
  		<tr id="filaCertificados" >
	  		<td colspan="4">
		  		<table width="100%" border="0" >
		  			<tr id="filaEscolaridad" style="display:none">
						<td width="25%" >Certificado Escolaridad</td>
				  		<td width="25%" ><input type="radio" name="certificado" id="cEscolaridad" onchange="setFechaAsignaCert($('#cEscolaridad'),$('#fEscolaridad'))" /></td>
				  		<td width="25%" >Fecha aplica</td>
				  		<td width="25%" ><input type="text" class="box1" id="fEscolaridad" name="fCert" disabled="disabled" /></td>
					</tr>
					<tr id="filaUniversidad" style="display:none">
				  		<td>Certificado Universidad</td>
				  		<td><input type="radio" name="certificado" id="cUniversidad" onchange="setFechaAsignaCert($('#cUniversidad'),$('#fUniversidad'))" /></td>
				  		<td>fecha aplica</td>
				  		<td><input type="text" class="box1" id="fUniversidad" name="fCert" disabled="disabled"/></td>
					</tr>				
					<tr id="filaDiscapacidad" style="display:none">
				  		<td>Certficado Discapacidad</td>
				  		<td><input type="checkbox" name="cDiscapacidad" id="cDiscapacidad" onchange="setFechaAsignaCert($('#cDiscapacidad'),$('#fDiscapacidad'))" /></td>
				  		<td>Fecha aplica</td>
				  		<td><input name="fCert" type="text" class="box1" id="fDiscapacidad" disabled="disabled" /></td>
					</tr>
					<tr id="filaSupervivencia" style="display:none">
				  		<td>Certificado Supervivencia</td>
				  		<td><input type="checkbox" name="cSupervivencia" id="cSupervivencia" onchange="setFechaAsignaCert($('#cSupervivencia'),$('#fSupervivencia'))" /></td>
				  		<td>Fecha aplica</td>
				  		<td><input name="fCert" type="text" class="box1" id="fSupervivencia" disabled="disabled" /></td>
					</tr>
					<tr id="filaDependenciaEconomica" style="display:none">
				  		<td>Certificado Dependencia Economica <b>HIJASTROS</b></td>
				  		<td><input type="checkbox" name="cDependenciaEconomica" id="cDependenciaEconomica" onchange="setFechaAsignaCertHijastro($('#cDependenciaEconomica'),$('#fDependenciaEconomica'))" /></td>
				  		<td>Fecha aplica</td>
				  		<td><input name="fCert" type="text" class="box1" id="fDependenciaEconomica" disabled="disabled" /></td>
					</tr>
		  		</table>
		  	</td>
	  	</tr>
	  	<tr>
			<td colspan="4">
	        	<div id="cerVigencia" style="display:none">
	        		<table width="100%" border="0">
	        			<tr>    
	      					<td><center>A&ntilde;o Vigencia</center></td>
	      					<td>
	       						<input type="radio" name="vigencia" id="vigencia1" value="0" onClick="vVigencia(0)">
	       						<label id="l1" for="vigencia1" >Periodo Actual</label><br/>
	      						<input type="radio" name="vigencia" id="vigencia2" value="1" onClick="vVigencia(1)"> 
	      						<label id="l2" for="vigencia2" >Periodo Anterior</label><br/>
	      					</td>
	      					<td colspan="2" >
	      						<center>
		      						Periodo Inicial
		        					<input name="pInicialV3" type="text" class="box1 monthPicker" id="pInicialV3" value="" onBlur="validarVigencia(this.value,1); validarPeriodosDatePicker(this.id,'pFinalV3','errorVig'); " disabled="disabled" maxlength="6">
		        					Periodo Final
		        					<input name="pFinalV3" type="text" class="box1 monthPickerF" id="pFinalV3" value="" onBlur="validarVigencia(this.value,2); validarPeriodosDatePicker('pInicialV3',this.id,'errorVig');" disabled="disabled" maxlength="6">		        					
		        					<span class="Rojo" id="errorVig"></span>
	        					</center>
	        				</td>        
	    				</tr>
	        		</table>
	       		</div>
	       		
	       		<div id="cerVigenDepenEconoHija" style="display:none">
	        		<table width="100%" border="0">
	        			<tr>    
	      					<td><center>A&ntilde;o Vigencia<br/> (<b>D.E HIJASTROS</b>)</center></td>
	      					<td>
	       						<input type="radio" name="vigenciaHijatros" id="radVigenciaHijastro1" value="0" onClick="vVigencia(0,true)">
	       						<label id="l1" for="radVigenciaHijastro1" >Periodo Actual</label><br/>
	      						<input type="radio" name="vigenciaHijatros" id="radVigenciaHijastro2" value="1" onClick="vVigencia(1,true)"> 
	      						<label id="l2" for="radVigenciaHijastro2" >Periodo Anterior</label><br/>
	      					</td>
	      					<td colspan="2" >
	      						<center>
		      						Periodo Inicial
		        					<input name="txtPerioIniciVigenHijastro3" type="text" class="box1 monthPicker" id="txtPerioIniciVigenHijastro3" value="" onBlur="validarVigencia(this.value,1); validarPeriodosDatePicker(this.id,'txtPerioFinalVigenHijastro3','errorVigHijastro'); " disabled="disabled" maxlength="6">
		        					Periodo Final
		        					<input name="txtPerioFinalVigenHijastro3" type="text" class="box1 monthPickerF" id="txtPerioFinalVigenHijastro3" value="" onBlur="validarVigencia(this.value,2); validarPeriodosDatePicker('txtPerioIniciVigenHijastro3',this.id,'errorVigHijastro');" disabled="disabled" maxlength="6">		        					
		        					<span class="Rojo" id="errorVigHijastro"></span>
	        					</center>
	        				</td>        
	    				</tr>
	        		</table>
	       		</div>
	       </td>	
		</tr>		
	</table>
</div>

<!-- DIV PARA MODIFICAR BENEFICIARIOS-->
<div id="modificarBeneficiarios" title="::FORMULARIO MODIFICACI&Oacute;N BENEFICIARIO::" style="display:none">
	<input type="hidden" name="hidIdRelacion3Upd" id="hidIdRelacion3Upd"/>	
	<table width="100%" border="0" cellspacing="0" class="tablero">
  		<tr>
    		<td>Tipo de beneficiario</td>
    		<td>
    			<select name="parentescoUpd" class="box1" id="parentescoUpd" onchange="validarComboParentesco();" >
      				<option selected="selected">Seleccione..</option>
      				<option value="35">Hijo</option>
      				<option value="38">Hijastro</option>
      				<option value="36">Padre/Madre</option>
      				<option value="37">Hermano(a)</option>
    			</select>
    		</td>
    		<td>C&eacute;dula Padre &oacute; Madre</td>
    		<td>
    			<select name="cedMamaUpd" id="cedMamaUpd" class="box1" disabled="disabled">
      				<option value="0" selected="selected">Seleccione.. </option>
      				<option value="0" name="0">HIJO NATURAL </option>
    			</select>
    		</td>
  		</tr>
  		<!-- padre biologico -->
		<tr id="padreBiologicoUpd" style="display:none">
    		<td>Tipo Id. Padre Biol&oacute;gico</td>
    		<td>
    			<select name="tipoIdPadreBiolUpd" class="box1" id="tipoIdPadreBiolUpd">
    				<option selected="selected" value="0">Seleccione..</option>
				<?php
					$consulta=$objClase->mostrar_datos(1, 2);
					while($row=mssql_fetch_array($consulta)){
						echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
				} ?>
    			</select> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
    		</td>
    		<td>N&uacute;mero Padre Biol&oacute;gico</td>
    		<td>
    			<input name="IdPadreBiolUpd" type="text" class="box1" id="IdPadreBiolUpd" onblur="buscarPersonaMostrar($('#tipoIdPadreBiolUpd'),$('#IdPadreBiolUpd'),$('#hideIdbiolUpd'),$('#nomBiologicoUpd'));" />
    			 <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
    			<input type="hidden" name="hideIdbiolUpd" id="hideIdbiolUpd" />
    		</td>
		</tr>
		<tr id="nombreBiologicoUpd" style="display:none">
  			<td>Padre Biol&oacute;gico</td>
  			<td colspan="3" id="nomBiologicoUpd">&nbsp;</td>
  		</tr>
		<!-- fin biologico--> 
		<tr>
  			<td >Tipo Documento</td>
  			<td >
  				<select name="tipoDoc3Upd" class="box1" id="tipoDoc3Upd">
  					<option selected="selected" value="0">Seleccione..</option>
  				<?php
					$consulta=$objClase->mostrar_datos(1, 2);
					while($row=mssql_fetch_array($consulta)){
						echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
				} ?>
  				</select> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
  			</td>
  			<td >No. Identificaci&oacute;n</td>
  			<td >
  				<input name="identificacion3Upd" type="text" class="box1" id="identificacion3Upd" />
  				 <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
  				<input type="hidden" name="hidIdPersona3Upd" id="hidIdPersona3Upd"/>
  			</td>
		</tr>
		<tr>
			<td>Primer Nombre</td>
			<td>
				<input name="pNombre3Upd" type="text" class="box1" id="pNombre3Upd" onkeypress="return vpnombre(event)"/>
				 <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
			</td>
			<td>Segundo Nombre</td>
			<td><input name="sNombre3Upd" type="text" class="box1" id="sNombre3Upd" onkeypress="return vsnombre(event)"/></td>
		</tr>
		<tr>
			<td>Primer Apellido</td>
			<td>
				<input name="pApellido3Upd" type="text" class="box1" id="pApellido3Upd" onkeypress="return vpnombre(event)"/>
				 <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
			</td>
			<td>Segundo Apellido</td>
			<td><input name="sApellido3Upd" type="text" class="box1" id="sApellido3Upd" onkeypress="return vsnombre(event)"/></td>
		</tr>
		<tr>
			<td>Genero</td>
			<td>
				<select name="sexo3Upd" class="box1" id="sexo3Upd">
					<option selected="selected" value="0">Seleccione..</option>
					<option value="M">Masculino</option>
					<option value="F">Femenino</option>
				</select> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
			</td>
			<td>Fecha de Nacimiento</td>
			<td>
				<input name="fecNac3Upd" type="text" class="box1" id="fecNac3Upd" readonly="readonly" onchange="validarEdadActualizar($('#fecNac3Upd'),($('#tipoAfiliacion3Upd').val()==48?'S':'N'),$('#capTrabajoUpd').val(),$('#parentescoUpd').val(),0)"/>
				 <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
				<input type="hidden" name="fecNacHide3Upd" id="fecNacHide3Upd"/>
			</td>
		</tr>
		<tr>
  			<td>Tipo formulario</td>
  			<td>
  				<select name="tipoAfiliacion3Upd" class="box1" id="tipoAfiliacion3Upd" disabled="true" onchange="validarCertificadosUpd();" >
  					<option value="48">Subsidio</option>
  					<option value="49">Servicios</option>
  				</select>
 			</td>
  			<td>Capacidad de trabajo</td>
  			<td>
  				<select name="capTrabajoUpd" class="box1" id="capTrabajoUpd" disabled="true" onchange="validarCertificadosUpd();" onblur="validarEdad(1)">
     				<option value="N">Normal</option>
     				<option value="I">Discapacitado</option>
  				</select>
  			</td>
		</tr>
		<tr>
  			<td>Fecha Asignaci&oacute;n</td>
  			<td><input type="text" name="txtFAsignacion" id="txtFAsignacion" readonly="readonly" /></td>
  			<td>Fecha Afilia </td>
  			<td><input type="text" class="box1" id="txtFechaAfiliacion" name="txtFechaAfiliacion" readonly="readonly" /></td>  			
		</tr>		
		<tr>
			<td>Estado Afiliaci&oacute;n</td>
  			<td>  				
  				<select name="estadoUpd" class="box1" id="estadoUpd" disabled='disabled' >
    				<option value="A">Activo</option>
    				<option value="I">Inactivo</option>
  				</select>
  				<input type="hidden" id="hdnMotivoUpd" value="" />    			
  			</td>
  			<td id="labelFechaRetiro">Fecha Retiro</td>
    		<td>
       			<input type="text" id="fechaRetiro" class="box1" name="fechaRetiro" disabled="disabled"/>       			 
    		</td>
		</tr>		
	</table>
</div>

<!-- VENTANA MODIFICAR CONYUGE DIALOG style="display:none;" -->
<div id="div-conyugeA" title="::FORMULARIO MODIFICAR CONYUGE::" style="display:none">
	<h4 align="left"> DATOS DEL CONYUGE</h4>
	<table width="100%"  border="0" cellspacing="0" class="tablero">
		<tr>
			<td width="25%">Tipo Documento</td>
			<td width="25%">
				<select name="tipoDoc2A" class="box1" id="tipoDoc2A">
					<option value="0">Seleccione..</option>
				<?php
					$consulta=$objClase->mostrar_datos(1, 2);
					while($row=mssql_fetch_array($consulta)){
						echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
					}
				?>
				</select> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
			</td>
			<td width="25%">Identificaci&oacute;n</td>
			<td width="25%">
				<input name="identificacion2A" type="text" class="box1" id="identificacion2A" />
				 <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
				 <input type="hidden" name="hidIdentificacion2A" id="hidIdentificacion2A"/>
			</td>
		</tr>         
		<tr>
			<td>Primer Nombre</td>
			<td class="box1">
				<input name="pNombre2A" type="text" class="box1" id="pNombre2A" onkeypress="return vpnombre(event)"/>
				 <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
			</td>
			<td>Segundo Nombre</td>
			<td><input name="sNombre2A" type="text" class="box1" id="sNombre2A" onkeypress="return vsnombre(event)" /></td>
		</tr>
		<tr>
			<td>Primer Apellido</td>
			<td>
				<input name="pApellido2A" type="text" class="box1" id="pApellido2A" onkeypress="return vpnombre(event)"/>
				 <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
			</td>
			<td>Segundo Apellido</td>
			<td>
				<input name="sApellido2A" type="text" class="box1" id="sApellido2A" onkeypress="return vsnombre(event)" />
			</td>
		</tr>
		<tr>
			<td>Sexo</td>
			<td>
				<select name="sexo2A" class="box1" id="sexo2A">
					<option selected="selected" value="0">Seleccione..</option>
					<option value="M">Masculino</option>
					<option value="F">Femenino</option>
				</select> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
			</td>
			<td>Tipo Vivienda</td>
			<td>
				<select name="tipoVivienda2A" class="box1" id="tipoVivienda2A">
  					<option value="0">Seleccione..</option>
  					<option value="U">Urbana</option>
  					<option value="R">Rural</option>
				</select>
			</td>
		</tr>
		<tr>
  			<td>Departamento Residencia</td>
  			<td>
  				<select name="cboDeptoCA" class="box1" id="cboDeptoCA">
    				<option value="0">Seleccione..</option>
    			<?php
					$consulta=$objCiudad->departamentos();
					while($row=mssql_fetch_array($consulta)){
				?>
    				<option value="<?php echo $row["coddepartamento"]; ?>"><?php echo utf8_encode($row["departmento"]); ?></option>
    			<?php } ?>
  				</select>
  			</td>
  			<td>Ciudad Residencia</td>
  			<td>
  				<select name="cboCiudadCA" class="box1" id="cboCiudadCA">
  					<option value="0">Seleccione..</option>
  				</select>
  			</td>
		</tr>
		<tr>
  			<td>Zona</td>
  			<td>
  				<select name="cboZonaCA" class="box1" id="cboZonaCA">
  					<option value="0">Seleccione..</option>
  				</select>
  			</td>
  			<td>Barrio</td>
  			<td>
  				<select name="cboBarrioCA" class="box1" id="cboBarrioCA">
  					<option value="0">Seleccione..</option>
  				</select>
  			</td>
		</tr>
		<tr>
			<td>Direcci&oacute;n </td>
			<td>
				<input name="direccion2A" type="text" class="box1" id="direccion2A" onfocus="direccion(this,document.getElementById('cboBarrioC'));" />
			</td>
			<td>E-mail</td>
			<td><input name="email2A" type="text" class="box1" id="email2A" onblur="soloemail(this)" /></td>
		</tr>
		<tr>
			<td>Tel&eacute;fono</td>
			<td><input name="telefono2A" type="text" class="box1" id="telefono2A" maxlength="7" onkeyup="solonumeros(this);" onkeydown="solonumeros(this);" onkeypress="solonumeros(this);" /></td>
			<td>Celular</td>
			<td><input name="celular2A" type="text" class="box1" id="celular2A" maxlength="10" onkeyup="solonumeros(this);" onkeydown="solonumeros(this);" onkeypress="solonumeros(this);" /></td>
		</tr>
		<tr>
			<td>Fecha De Nacimiento</td>
			<td>
				<input type="text" id="alternate2A" readonly="readonly"/>
				<input type="hidden" name="hidAlternate2A" id="hidAlternate2A"/>
			</td>
			<td>Pais Nacimiento</td>
			<td>
				<select name="cboPaisC2A" class="box1" id="cboPaisC2A">
					<option value="0">Seleccione..</option>
				<?php
					$consulta=$objCiudad->paises();
					while($row=mssql_fetch_array($consulta)){
				?>          
					<option value="<?php echo $row["idpais"]; ?>"><?php echo utf8_encode($row["pais"]); ?></option>
				<?php } ?>
				</select>
			</td>
		</tr>
		<tr>
			<td>Departamento Nacimiento</td>
			<td>
				<select name="cboDeptoC2A" class="box1" id="cboDeptoC2A">
					<option value="0">Seleccione..</option>
				</select>
			</td>
			<td>Ciudad Nacimiento</td>
			<td>
				<select name="cboCiudadC2A" class="box1" id="cboCiudadC2A">
					<option value="0">Seleccione..</option>
				</select>
			</td>
		</tr>
		<input type="hidden" name="hidRutaDoc2A" id="hidRutaDoc2A"/>
	</table>
</div>
<!-- FIN DIALOG MODIFICAR CONYUGUE -->


<!-- FORMULARIO A TRAVES DEL CUAL SE CONTROLAN LOS DOCUMENTOS QUE DEBE PRESENTAR EL AFILIADO -->
<div id="divControlDocumentos" title="::CONTROL DE DOCUMENTOS::" style="display:none">
	<table width="100%"  border="0" cellspacing="0" class="tablero">
	    <thead>
	         <tr><td colspan="2">Por favor seleccione los documentos con los cuales cuenta el beneficiario ya digitalizados, para continuar con el proceso.</td></tr>
	    </thead>
	    <tbody>
	       <tr>
	           <td colspan="2">&nbsp;</td>
	       </tr>
	       <tr>
	           <td colspan="2">Nombre Beneficiario: <label id="lbnomben"><strong></strong></label></td>
	       </tr>
	       <tr>
	           <td colspan="2">&nbsp;</td>
	       </tr>
	        <tr>
	           <td colspan="2">Documentos:</td>
	       </tr>
	       <tr>
	           <td><div id="tablactrdocumentos"></div></td>
	       </tr>
	       <tr>
			    <td colspan="2">&nbsp;</td>
			</tr>
		   <tr>
			    <td colspan="2"><input type="hidden" id="idresp"></td>
			</tr>
	    </tbody>
	</table>
</div>
<!-- FIN CONTROL DOCUMENTOS -->




<div id="div-datosCertificados" title="::HISTORIAL DE CERTIFICADOS::" style="display:none">
	<table width="100%" class="tablero" >
		<thead>
			<tr>
				<th>Id Cert.</th>
				<th>Tipo</th>
				<th>Peri&oacute;do Inicial</th>
				<th>Peri&oacute;do Final</th>
				<th>Fecha Presentaci&oacute;n</th>
				<th>Estado</th>
				<th>Modificar</th>
				<!-- <th>Eliminar</th> -->
			</tr>
		</thead>
		<tbody></tbody>
	</table>
</div>

<div id="modificarCertificados" title="::MODFICAR CERTIFICADO::" style="display:none">
	<table width="100%" border="0" cellspacing="0" class="tablero">
  		<tr>
    		<td align="left" width="17%">Tipo de certificado</td>
			    <td colspan="3">
			    	<select name="lTipoCer" id="lTipoCer" class="boxlargo" onChange="validarCert(this)">
					    <option value="55" id="tc0">Escolaridad</option>
					    <option value="56" id="tc1">Universidad Semestre A</option>
					    <option value="56" id="tc2">Universidad Semestre B</option>
					    <option value="57" id="tc3">Supervivencia</option>
    					<option value="58" id="tc4">Discapacidad</option>
				    </select><img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
			    </td>
  		</tr>
  		<tr>
				<td colspan="4">
					<div id="cerVigencia">
						<table width="100%" border="0">
							<tr>
								<td>A&ntilde;o Vigencia</td>
								<td colspan="3">
									Periodo Inicial
									<input name="pInicialV" type="text" class="box1 monthPicker" id="pInicialV"  value="" maxlength="6" >
									Periodo Final
									<input name="pFinalV" type="text" class="box1 monthPickerF" id="pFinalV" value=""  maxlength="6" >
									<img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12">
									<span class="Rojo" id="errorVig"></span>
								</td>
							</tr>
						</table>
					</div>
				</td>
			</tr>
		<tr>
			<td colspan="4">
				<div id="cerFecEstado">
					<table width="100%" border="0">
						<tr colspan ="3">
							<td align="left" width="17%">Fecha Presentaci&oacute;n</td>
							<td colspan="3">
								<input name="pPresentacion" type="text" class="box1" readonly="readonly" id="pPresentacion" value="" maxlength="10" >
								<img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12">
							</td>
							<td align="left" width="17%">Estado</td>
						  	<td colspan="3">
								 	<select name="lEstado" id="lEstado" class="box1" onChange="validarCert(this)">
									    <option value="A" id="e0">Activo</option>
									    <option value="I" id="e1">Inactivo</option>
									</select>
									<img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12">
							</td>
						</tr>								
					</table>	
				</div>		
		</tr>
</table>
</div>

<br />
<label class="Rojo">Nota: Primero grabe los c&oacute;nyuges!</label>
<input type="hidden" id="idPersona" value="<?php echo $idpersona?>" /> 
<!-- formulario actuliza  --> 


<div id="dialog-actualiza" title="Formulario de actualizaci&oacute;n"></div>
<!-- formulario persona simple  -->
<div id="dialog-persona" title="Formulario Datos Basicos" style="display:none">
<center>
<table width="578" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" class="tablero">
	<tbody>
		<tr bgcolor="#EBEBEB">
		  <td colspan="4" style="text-align:center" >Datos B&aacute;sicos de la persona</td>
	  </tr>
		<tr>
		  <td >Tipo Documento</td>
		  <td ><select name="tDocumento" id="tDocumento" class="box1" onfocus="javascript:this.blur(); return false;">
          <option value="0" >Seleccione</option>
<?php
		$consulta=$objClase->mostrar_datos(1, 2);
		while($row=mssql_fetch_array($consulta)){
		echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
	}
?>
          </select>
          </td>
		  <td >N&uacute;mero</td>
		  <td ><input name="txtNumeroP" id="txtNumeroP" type="text" class="box1" readonly="readonly"></td>
	  </tr>
		<tr>
		  <td >Primer Nombre</td>
		  <td ><input name="pNombre" id="pNombre" type="text" class="box1"></td>
		  <td >Segundo Nombre</td>
		  <td ><input name="sNombre" id="sNombre" type="text" class="box1"></td>
	  </tr>
		<tr>
		  <td >Primer Apellido</td>
		  <td ><input name="pApellido" id="pApellido" type="text" class="box1"></td>
		  <td >Segundo Apellido</td>
		  <td ><input name="sApellido" id="sApellido" type="text" class="box1"></td>
	  </tr>
		<tr>
		  <td >&nbsp;</td>
		  <td >&nbsp;</td>
		  <td >&nbsp;</td>
		  <td >&nbsp;</td>
	  </tr>
		<tr>
			<td >&nbsp;</td>
			<td >&nbsp;</td>
			<td >&nbsp;</td>
			<td >&nbsp;</td>
		</tr>
</tbody>
</table>
</center>
</div>

<div id="dialog-persona2" title="Formulario Datos Basicos" style="display:none">
	<center>
	<table width="100%" border="0" cellpadding="0" cellspacing="0" class="tablero">		
		<tr>
  			<td >Tipo Documento</td>
  			<td >
  				<select name="cmbDBTipoDoc" class="box1" id="cmbDBTipoDoc">
  					<option selected="selected" value="0">Seleccione..</option>
  				<?php
					$consulta=$objClase->mostrar_datos(1, 2);
					while($row=mssql_fetch_array($consulta)){
						echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
				} ?>
  				</select> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
  			</td>
  			<td >No. Identififcaci&oacute;n</td>
  			<td >
  				<input name="txtDBIdentificacion" type="text" class="box1" id="txtDBIdentificacion" />
  				 <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
  				<input type="hidden" name="txtHiddenIdPersona" id="txtHiddenIdPersona" />
  			</td>
		</tr>
		<tr>
			<td>Primer Nombre</td>
			<td>
				<input name="txtDBpNombre" type="text" class="box1" id="txtDBpNombre" onkeypress="return vpnombre(event)"/>
				 <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
			</td>
			<td>Segundo Nombre</td>
			<td><input name="txtDBsNombre" type="text" class="box1" id="txtDBsNombre" onkeypress="return vsnombre(event)"/></td>
		</tr>
		<tr>
			<td>Primer Apellido</td>
			<td>
				<input name="txtDBpApellido" type="text" class="box1" id="txtDBpApellido" onkeypress="return vpnombre(event)"/>
				 <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
			</td>
			<td>Segundo Apellido</td>
			<td><input name="txtDBsApellido" type="text" class="box1" id="txtDBsApellido" onkeypress="return vsnombre(event)"/></td>
		</tr>
		<tr>			
			<td>Fecha de Nacimiento</td>
			<td>
				<input name="txtDBfecNace" type="text" class="box1" id="txtDBfecNace" readonly="readonly" onchange="validarEdadActualizar($('#txtDBfecNace'),$('#txtHiddenIdGiro').val(),$('#txtHiddenDiscapacitado').val(),$('#txtHiddenIdparentesco').val(),0)"/>
				 <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />				
			</td>
			<td>Pais Nacimiento</td>
			<td>
				<select name="cboDBPaisNace" class="box1" id="cboDBPaisNace">
					<option value="0">Seleccione..</option>
				<?php
					$consulta=$objCiudad->paises();
					while($row=mssql_fetch_array($consulta)){
				?>          
					<option value="<?php echo $row["idpais"]; ?>"><?php echo utf8_encode($row["pais"]); ?></option>
				<?php } ?>
				</select>
			</td>
		</tr>
		<tr>
			<td>Departamento Nacimiento</td>
			<td>
				<select name="cboDBDeptoNace" class="box1" id="cboDBDeptoNace">
					<option value="0">Seleccione..</option>
				</select>
			</td>
			<td>Ciudad Nacimiento</td>
			<td>
				<select name="cboDBCiudadNace" class="box1" id="cboDBCiudadNace">
					<option value="0">Seleccione..</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Genero</td>
			<td>
				<select name="cmbDBSexo" class="box1" id="cmbDBSexo">
					<option selected="selected" value="0">Seleccione..</option>
					<option value="M">Masculino</option>
					<option value="F">Femenino</option>
				</select> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
			</td>
			<td colspan="2">
				<input type="hidden" name="txtHiddenIdGiro" id="txtHiddenIdGiro" />
				<input type="hidden" name="txtHiddenDiscapacitado" id="txtHiddenDiscapacitado" />
				<input type="hidden" name="txtHiddenIdparentesco" id="txtHiddenIdparentesco" />
			</td>
		</tr>
	</table>
	</center>	
</div>

<div id="divMotivos" title="Motivos de Inactivacion" style="display:none; padding-first:40px" >
	<p>
		<label>Motivo</label>
		<select name="motivo" id="motivo" class="boxlargo" onchange="onchangeMotivo()">
	      	<option value="0"> Seleccione...</option>
			<?php
				$consulta=$objClase->mostrar_datos(77,2);
				while($row=mssql_fetch_array($consulta)){
					echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
				}
			?>
	    </select>
	</p>
	<p id="defuncionActEstado" >
		<label>Fecha defunci&oacute;n</label>
		<input type="text" id="txtFechaDefActEstado" readonly="readonly"/>
	</p>
	<button id="btnAcepInactivar" class="ui-state-default" onclick="updateEstadoBeneficiario($('#hideIdRel').val(),'I',$('#motivo').val());">Aceptar</button>
	<input type="hidden" name="hideIdRel" id="hideIdRel" />
	<input type="hidden" name="hideIdBeneActEstado" id="hideIdBeneActEstado" />
</div>

<div id="divFechaAsignacion" title="Fecha de Asignacion" style="display:none; padding-first:40px" >
	<p>
		<label>Fecha de Asignacion</label>&nbsp;
		<input type="hidden" name="hideIdRelFechaAsignacionUpdate" id="hideIdRelFechaAsignacionUpdate" />
		<input type="hidden" name="hideCapacidadUpdate" id="hideCapacidadUpdate" />
		<input type="hidden" name="hideParentescoUpdate" id="hideParentescoUpdate" />
		<input type="hidden" name="hideVarFcNaceUpdate" id="hideVarFcNaceUpdate" />
		<input type="hidden" name="hideIdBenefUpdate" id="hideIdBenefUpdate" />
		<input type="hidden" name="hideEstado21Update" id="hideEstado21Update" />
		<input type="hidden" name="hideEstado15Update" id="hideEstado15Update" />
		<input name="txtFechaAsignacionUpdate" style="width: 80px" type="text" id="txtFechaAsignacionUpdate" readonly="readonly" />
	</p>
	<button id="btnAcepFechaAsignacionUpdate" class="ui-state-default" onclick="actualizarEstadoGiroBeneficiario($('#hideIdRelFechaAsignacionUpdate').val(),'N',$('#txtFechaAsignacionUpdate').val(),true,$('#hideCapacidadUpdate').val(),$('#hideParentescoUpdate').val(),$('#hideVarFcNaceUpdate').val(),$('#hideIdBenefUpdate').val(),$('#hideEstado21Update').val(),$('#hideEstado15Update').val())">Aceptar</button>	
</div>

<div id="divRelacionesActivas" title="Relaciones Activas" style="display:none; padding-first:40px" >
	<table width="578" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" class="tablero">
		<tr>				
			<th class="head">TD</th>
			<th class="head">Doc</th>
			<th class="head">Nombre</th>
			<th class="head">Parentesco</th>				
			<th class="head">Conviven</th>
		</tr>
		<tbody id="tbRelacionesActivas" ></tbody>
	</table>	
</div>

<div id="divDocumentoDoble" title="Datos Persona" style="display:none; padding-first:40px" >
	<table width="460" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" class="tablero">
		<tr>
			<th class="head">TD</th>				
			<th class="head">Doc</th>
			<th class="head">Nombre</th>
		</tr>
		<tbody id="tbDocumentoDoble" ></tbody>
	</table>	
</div>


<input type="hidden" name="txtFechaIngresoM" id="txtFechaIngresoM" value="<?php echo $fechaIngreso; ?>" />
<input type="hidden" name="txtDiferenciaFechaIngreso" id="txtDiferenciaFechaIngreso" value="<?php echo $diasDiferenciaFechaIngreso; ?>" />
<input type="hidden" name="hidObservacionAutomatica" id="hidObservacionAutomatica" value="" />
</body>

</html>
