<?php
/* autor:       orlando puentes
 * fecha:       12/07/2010
 * objetivo:     
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$idpersona= $_REQUEST['v0'];
include_once $raiz.DIRECTORY_SEPARATOR.'config.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'p.afiliacion.class.php';
$objAfiliacion=new Afiliacion;
include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'p.definiciones.class.php';
$objClase=new Definiciones();

$url = URL_PORTAL;
$rs = $objAfiliacion->buscar_afiliacion($idpersona);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Afiliacion</title>
<style type="text/css">
#div-mostrar p{ cursor:pointer; color:#333; width:600px;}
#div-mostrar p:hover{color:#000}
</style>
<script type="text/javascript" src="<?php echo $url; ?>js/jquery.fechaLarga.js"></script>
<script type="text/javascript" src="<?php echo $url; ?>js/comunes.js"></script>
<script type="text/javascript" src="js/afiliacionTab.js"></script>
<script type="text/javascript">
$(function() {
	$("#fecIngreso,#datepicker").datepicker({
		changeMonth: true,
		changeYear: true,
		maxDate: "+0D"
	});
	var anoActual = new Date();
	var strYearRange = anoActual.getFullYear()-120 +":"+ anoActual.getFullYear();
	$(".box1").datepicker( "option", "yearRange", strYearRange );
});
</script>
<script language="javascript">
buscarAfiliaciones(<?php echo $idpersona; ?>);
$(document).ready(function(){
	$("#estado").change(function(){
		if($(this).val()=='I'){
			if(confirm("Esta seguro de inactivar esta afiliacion?")==true){
				$("#codigo").show();
				$("#infoRetiro").show();
			}	
			}
		else{
			$("#codigo").hide();
			$("#codigo").val(0);
			$("#infoRetiro").hide();
			$("#datepicker").val("");
			}	
		
	});	//change
	limpiarCampos_otro2();
	$("#div-msje").remove();
	$("#tablaAfiTab input,#tablaAfiTab select").change(huboCambio);
	
});
function huboCambio(){
	$("#div-msje").remove();
	div=$('<div id="div-msje" style="margin:9px auto;padding:6px;text-align:center;border:1px solid #FFFF00;width:300px" class="ui-state-highlight ui-corner-all"><span style="float: left; margin-right: 0.3em;" class="ui-icon ui-icon-info"></span><strong>Atenci\u00F3n</strong>, No olvidar actualizar tus datos.</div>')
	div.appendTo("body").hide().show("pulsate");
}

</script>
</head>

<body>
<h3> Afiliaciones Activas</h3>
<div id="div-mostrar"></div>
<div id="div-afiliacion" style="display:block">
<img src="<?php echo $url; ?>imagenes/tabla/spacer.gif" width="1" height="1">
<img src="<?php echo $url; ?>imagenes/spacer.gif" width="1" height="1"/>
<img src="<?php echo $url; ?>imagenes/spacer.gif" width="1" height="1"/>
<img src="<?php echo $url; ?>imagenes/spacer.gif" width="1" height="1"/>

<table width="100%" border="0" cellspacing="0" class="tablero" id="tablaAfiTab">
 <tr>
   <td>Afiliado</td>
   <td colspan="3" id="tdAfiliado">&nbsp;</td>
  </tr>
 <tr>
    <td>Empresa</td>
    <td>
      <input name="txtNit" type="text" class="boxfecha" id="txtNit" onblur="buscarEmpresa();" />
    </td>
    <td colspan="2" id="tdEmpresa">&nbsp;</td>
  </tr>
    <tr>
	    <td width="17%">Tipo  Formulario</td>
	    <td width="8">
	    <select name="tipForm" class="box1" id="tipForm" onblur="validarSubsidio();" >
	    <option selected="selected" value=0>Seleccione..</option>
	    <?php
		$consulta=$objClase->mostrar_datos(9, 3);
		while($row=mssql_fetch_array($consulta)){
			echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
		}
		?>
	    </select>
	    <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
	    <td width="7">Tipo Afiliaci&oacute;n</td>
	    <td width="14">
	    <select name="tipAfiliacion" class="box1" id="tipAfiliacion" onblur="validarSubsidio();" >
	    <option value=0 selected="selected">Seleccione..</option>
	    <?php
		$consulta=$objClase->mostrar_datos(2, 3);
		while($row=mssql_fetch_array($consulta)){
			echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
		}
		?>
	    </select>
	    <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
    </tr>
    <tr>
	    <td>Fecha Ingreso</td>
	    <td><input name="fecIngreso" type="text" class="box1" id="fecIngreso" onchange="validarFechaIngresoGrabacion('fecIngreso');" readonly="readonly"/>
	      <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
	    <td>Horas D&iacute;a</td>
	    <td><input name="horasDia" type="text" class="box1" id="horasDia"  maxlength="1" onkeydown="solonumeros(this);" onkeyup="solonumeros(this);"/>
	    <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
    </tr>
    <tr>
	    <td>Horas Mes</td>
	    <td><input name="horasMes" type="text" class="box1" id="horasMes" maxlength="3" onkeydown="solonumeros(this);" onkeyup="solonumeros(this);"/>
	    <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
	    <td>Tipo Pago</td>
	    <td><select name="tipoPago" class="box1" id="tipoPago" >
	    <option value="0" >Seleccione..</option>
	    <option value="T" selected="selected">Tarjeta</option>
	    <option value="C">Cheque</option>
	    </select> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
	    
	    </td>
    </tr>
    <tr>
    	<td>Salario</td>
    	<td>
    		<input name="salario" type="text" class="box1" id="salario" onblur="$('#categoria').val(calcularCategoria(this.value));" onkeydown="solonumeros(this);" onkeyup="solonumeros(this);"/>
	    	 <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />&nbsp;&nbsp;&nbsp;Categor&iacute;a&nbsp;&nbsp;
    		<input name="categoria" type="text" class="boxfecha" id="categoria" style="width:30px" disabled="disabled" readonly="readonly" />
    	</td>    
    	<td>Agr&iacute;cola</td>
    	<td><select name="agricola" class="box1" id="agricola" >
      			<option value="0">Seleccione..</option>
      			<option value="S">SI</option>
      			<option value="N" selected="selected">NO</option>
    		</select> <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
    	</td>
    </tr>    
    <tr>
    	<td>Clase Afiliacion</td>
		<td><select name="cmbClaseAfiliacion" id="cmbClaseAfiliacion" class="box1" >
				<option value="0" selected="selected">Seleccione...</option>
			<?php
				$consulta=$objClase->mostrar_datos(63, 3);
				while($row=mssql_fetch_array($consulta)){
					echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";					
				}
			?>
			</select>
		</td>
	    <td>Cargo</td>
	    <td><select name="cargo" class="box1" id="cargo" >
	      <option value="0" selected="selected">Seleccione..</option>
	      <?php
		$consulta=$objClase->mostrar_datos(21, 4);
		while($row=mssql_fetch_array($consulta)){
			echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
		}
		?>
	      <option value="0">Otro...</option>
	    </select>
	      <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
	      <input type="hidden" name="primaria" id="primaria" />
	   </td>
    </tr>
    <tr>
	    <td>Tipo</td>
	    <td><select name="cboTipo" class="box1" id="cboTipo" >
	    <option value="0">Seleccione..</option>
	    <option value="S" selected="selected">Primaria</option>
	    <option value="N">Adicional</option>
	    </select>
	    <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
	    <td>Estado</td>
	    <td><select name="estado" class="box1" id="estado" >
	      <option value="0" selected="selected">Seleccione..</option>
	      <option value="A" >ACTIVO</option>
	      <option value="I">INACTIVO</option>
	      <option value="P" >PENDIENTE</option>
	    </select>
	      <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
	    </td>
    </tr>
    <tr id="infoRetiro" style="display:none">
    <td>C&oacute;digo Estado</td>
    <td><select name="codigo" class="box1" id="codigo" >
          <option value="0" selected="selected">Seleccione..</option>
          <?php
        $consulta=$objClase->mostrar_codigo(47, 'A');
        while($row=mssql_fetch_array($consulta)){
            echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
        }
        ?> 
	</select><img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
    <td>Fecha Retiro</td>
    <td>
       <input type="text" id="datepicker" class="box1" name="datepicker" readonly="readonly"/>
       <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
    </td>
    </tr>
</table>
<br/>
 <a id="actualizarAfiliacion" value="Actualizar datos" class="ui-state-default"  onClick="actualizarAfiliacion();" style="text-decoration:none; padding:4px; cursor:pointer; margin-left:45%" >Actualizar datos</a>
<input type="hidden" id="idafiliacion" />
<input type="hidden" id="idPersona" />
<input type="hidden" id="idEmpresa" />
</div>
</body>

</html>