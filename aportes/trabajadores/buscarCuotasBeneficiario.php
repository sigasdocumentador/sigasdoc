<?php
date_default_timezone_set('America/Bogota');
ini_set("display_errors",'1');
session_start();
$idp =$_REQUEST['v0'];

include_once '../../rsc/pdo/IFXDbManejador.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo $cadena;
	exit();
}
$filas = array();
$sql="SELECT a15.pnombre,a15.snombre,a15.papellido,a15.sapellido,a.periodo,a.tabla
	  FROM (
		SELECT a09.idtrabajador,a09.periodo,a09.idbeneficiario,'a09' tabla
		FROM aportes009 a09 
		WHERE a09.idbeneficiario=$idp 
		UNION 
		SELECT a14.idtrabajador,a14.periodo,a14.idbeneficiario,'a14' tabla
		FROM aportes014 a14 
		WHERE a14.idbeneficiario=$idp 
	   ) a INNER JOIN aportes015 a15 ON a15.idpersona=a.idbeneficiario
	  ORDER BY periodo DESC";
$rs=$db->querySimple($sql);
if(is_null($rs)){
	echo 0;
	exit();
}
while ($row=$rs->fetch()) {
		$filas[] = $row;
	}
if(count($filas)>0)
	echo json_encode($filas);
else 
	echo 0;
?>