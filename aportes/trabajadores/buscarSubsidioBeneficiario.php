<?php
date_default_timezone_set('America/Bogota');
ini_set("display_errors",'1');
session_start();
$idp =$_REQUEST['v0'];

include_once '../../rsc/pdo/IFXDbManejador.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo $cadena;
	exit();
}
$filas = array();
$sql="SELECT a15.pnombre,a15.snombre,a15.papellido,a15.sapellido,a99.periodo,a99.nit
	  ,a99.procesado,a99.idbeneficiario
	  FROM aportes099 a99 
	  INNER JOIN aportes015 a15 ON a15.idpersona=a99.idbeneficiario
	  WHERE a99.idbeneficiario=$idp 
	  ORDER BY periodo DESC";
$rs=$db->querySimple($sql);
if(is_null($rs)){
	echo 0;
	exit();
}
while ($row=$rs->fetch()) {
		$filas[] = $row;
	}
if(count($filas)>0)
	echo json_encode($filas);
else 
	echo 0;
?>
