<?php

set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");

$root= $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once '../../rsc/pdo/IFXDbManejador.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="es"><head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Unificar ID</title>
<link type="text/css" href="../../newcss/Estilos.css" rel="stylesheet">
<link type="text/css" href="../../newcss/css.1.8/jquery-ui-1.8.17.custom.css" rel="stylesheet">
<link type="text/css" href="../../newcss/marco.css" rel="stylesheet">

<script type="text/javascript" src="../../newjs/1.6/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="../../newjs/1.6/jquery-ui-1.8.16.custom.min.js"></script>

<script type="text/javascript" src="../../newjs/comunes.js"></script>
<script type="text/javascript" src="../../newjs/jquery.utilitarios.js"></script>
<script type="text/javascript" src="js/unificarID.js"></script>
</head>

<body>
	<form name="forma">
		<br />		
			<div>
				<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td width="13" height="29" class="arriba_iz">&nbsp;</td>
						<td class="arriba_ce"><span class="letrablanca">:: Datos B&aacute;sicos ::</span></td>
						<td width="13" class="arriba_de" align="right">&nbsp;</td>
					</tr>
					<tr height="35" >
						<td class="cuerpo_iz">&nbsp;</td>
						<td class="cuerpo_ce">
							<table width="90%" cellspacing="0" cellpadding="0"
								align="center">
								<tr>
									<td>
										<img src="../../imagenes/menu/nuevo.png" title="Nuevo" style="cursor: pointer" onclick="nuevo();">
										<img src="../../imagenes/menu/grabar.png" title="Guardar" style="cursor: pointer" onclick="guardar();" id="btnGuardar">
									</td>
								</tr>
							</table></td>
						<td class="cuerpo_de">&nbsp;</td>
					</tr>
					<tr>
						<td class="cuerpo_iz">&nbsp;</td>
						<td class="cuerpo_ce">
							<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center" class="tablero">
								<tr>
									<td style="text-align:center" width="220px">TIPO DOCUMENTO</td>
									<td style="text-align:center" width="110px">N&Uacute;MERO</td>
									<td style="text-align:center" width="450px">NOMBRE</td>
									<td style="text-align:center" width="100px">CAPACIDAD TRABAJO</td>
									<td style="text-align:center" width="50px">ELIMINAR</td>
								</tr>
								<tr>
									<td colspan="5"></td>
								</tr>
								<tr>									
									<td ><center><label>
										<select name="cmbIdTipoDocumentoError" id="cmbIdTipoDocumentoError" class="box1" style="width: 210px" onchange="$('#txtIdentificacionError').val('').trigger('blur');" >
											<option value="0" selected="selected">Seleccione...</option>
						            		<?php
												$rs = $db->Definiciones ( 1, 1 );
												while ( $row = $rs->fetch() ) {
													echo "<option value=" . $row ['iddetalledef'] . ">" . $row ['detalledefinicion'] . "</option>";
												} 
											?>
					          			</select></label></center></td>									
									<td ><center><input name="txtIdentificacionError" type="text" class="box1" id="txtIdentificacionError" onkeyup="validarIdentificacion($('#cmbIdTipoDocumentoError'),this);"
										onkeydown="validarIdentificacion($('#cmbIdTipoDocumentoError'),this);" onkeypress="validarIdentificacion($('#cmbIdTipoDocumentoError'),this);"
										onblur="buscarPersona($('#cmbIdTipoDocumentoError'), $('#txtIdentificacionError'), $('#txtNombreCompletoTraeError'), $('#txtCapacidadTrabajoTraeError'), false, $('#txtIDError'));"  style="width: 85px" maxlength="12">
										</center>											
									</td>
									<td >
										<input id="txtIDError" type="hidden" value="" />
										<input name="txtNombreCompletoTraeError" type="text" class="box1" id="txtNombreCompletoTraeError" style="width: 320px" disabled="disabled">
									</td>
									<td >
										<input id="txtIDError" type="hidden" value="" />
										<input name="txtCapacidadTrabajoTraeError" type="text" class="box1" id="txtCapacidadTrabajoTraeError" style="width: 120px" disabled="disabled">
									</td>
									<td ><center><input type="radio" name="chkEliminar" value="0" onchange="traerDatosEliminar(0);"></center></td>
								</tr>
								<tr>									
									<td ><center><label>
										<select name="cmbIdTipoDocumentoCorrecto" id="cmbIdTipoDocumentoCorrecto" class="box1" style="width: 210px" onchange="$('#txtIdentificacionCorrecto').val('').trigger('blur');" >
											<option value="0" selected="selected">Seleccione...</option>
						            		<?php
												$rs = $db->Definiciones ( 1, 1 );
												while ( $row = $rs->fetch() ) {
													echo "<option value=" . $row ['iddetalledef'] . ">" . $row ['detalledefinicion'] . "</option>";
												} 
											?>
					          			</select></label></center></td>									
									<td ><center><input name="txtIdentificacionCorrecto" type="text" class="box1" id="txtIdentificacionCorrecto" onkeyup="validarIdentificacion($('#cmbIdTipoDocumentoCorrecto'),this);"
										onkeydown="validarIdentificacion($('#cmbIdTipoDocumentoCorrecto'),this);" onkeypress="validarIdentificacion($('#cmbIdTipoDocumentoCorrecto'),this);"
										onblur="buscarPersona($('#cmbIdTipoDocumentoCorrecto'), $('#txtIdentificacionCorrecto'), $('#txtNombreCompletoTraeCorrecto'), $('#txtCapacidadTrabajoTraeCorrecto'), false, $('#txtIDCorrecto'));"  style="width: 85px" maxlength="12">
										</center>											
									</td>
									<td >
										<input id="txtIDCorrecto" type="hidden" value="" />
										<input name="txtNombreCompletoTraeCorrecto" type="text" class="box1" id="txtNombreCompletoTraeCorrecto" style="width: 320px" disabled="disabled">
									</td>
									<td>
										<input id="txtIDCorrecto" type="hidden" value="" />
										<input name="txtCapacidadTrabajoTraeCorrecto" type="text" class="box1" id="txtCapacidadTrabajoTraeCorrecto" style="width: 120px" disabled="disabled">
									</td>
									<td ><center><input type="radio" name="chkEliminar" value="1" onchange="traerDatosEliminar(1);" ></center></td>
								</tr>								
							</table>
						</td>
						<td class="cuerpo_de">&nbsp;</td>
					</tr>
					<tr>
						<td class="abajo_iz">&nbsp;</td>
						<td class="abajo_ce"></td>
						<td class="abajo_de">&nbsp;</td>
					</tr>
				</table>
				<br />
				<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center" id="tblDatosEliminado">
					<tr>
						<td width="13" height="29" class="arriba_iz">&nbsp;</td>
						<td class="arriba_ce"><span class="letrablanca">:: Datos del Eliminado ::</span></td>
						<td width="13" class="arriba_de" align="right">&nbsp;</td>
					</tr>
					<tr>
						<td class="cuerpo_iz">&nbsp;</td>
						<td class="cuerpo_ce"><br /><label style="font-size:14px;font-weight:bold;text-align:left">Relacion</label></td>
						<td class="cuerpo_de">&nbsp;</td>
					</tr>
					<tr>
						<td class="cuerpo_iz">&nbsp;</td>
						<td class="cuerpo_ce">
							<table width="80%" border="0" cellspacing="0" cellpadding="0" align="center" class="tablero">
								<tr>
									<th width="40%">Afiliado</th>
									<th width="30%">Beneficiario</th>
									<th width="18%">Parentesco</th>
									<th width="6%">Estado</th>
									<th width="6%">Giro</th>
								</tr>
								<tbody id="tbRelacion"></tbody>
							</table>
						</td>
						<td class="cuerpo_de">&nbsp;</td>
					</tr>
					<tr>
						<td class="cuerpo_iz">&nbsp;</td>
						<td class="cuerpo_ce"><br /><label style="font-size:14px;font-weight:bold;text-align:left">Cuota Monetaria</label></td>
						<td class="cuerpo_de">&nbsp;</td>
					</tr>
					<tr>
						<td class="cuerpo_iz">&nbsp;</td>
						<td class="cuerpo_ce">
							<table width="80%" border="0" cellspacing="0" cellpadding="0" align="center" class="tablero">
								<tr>
									<th width="70%">Beneficiario</th>
									<th width="30%">Periodo</th>																		
								</tr>
								<tbody id="tbCuotaMonetaria"></tbody>
							</table>
						</td>
						<td class="cuerpo_de">&nbsp;</td>
					</tr>
					<tr>
						<td class="cuerpo_iz">&nbsp;</td>
						<td class="cuerpo_ce"><br /><label style="font-size:14px;font-weight:bold;text-align:left">Paquete Escolar</label></td>
						<td class="cuerpo_de">&nbsp;</td>
					</tr>
					<tr>
						<td class="cuerpo_iz">&nbsp;</td>
						<td class="cuerpo_ce">
							<table width="80%" border="0" cellspacing="0" cellpadding="0" align="center" class="tablero">
								<tr>
									<th width="70%">Beneficiario</th>
									<th width="30%">A�o</th>									
								</tr>
								<tbody id="tbPaqueteEscolar"></tbody>
							</table>
						</td>
						<td class="cuerpo_de">&nbsp;</td>
					</tr>
					<tr>
						<td class="cuerpo_iz">&nbsp;</td>
						<td class="cuerpo_ce"><br /><label style="font-size:14px;font-weight:bold;text-align:left">Certificados</label></td>
						<td class="cuerpo_de">&nbsp;</td>
					</tr>
					<tr>
						<td class="cuerpo_iz">&nbsp;</td>
						<td class="cuerpo_ce">
							<table width="80%" border="0" cellspacing="0" cellpadding="0" align="center" class="tablero">
								<tr>
									<th width="40%">Tipo</th>
									<th width="15%">Estado</th>
									<th width="15%">Inicial</th>
									<th width="15%">Final</th>
									<th width="15%">Presentacion</th>								
								</tr>
								<tbody id="tbCertificadoError"></tbody>
								
							</table>
						</td>
						<td class="cuerpo_de">&nbsp;</td>
					</tr>
					<tr>
						<td class="cuerpo_iz">&nbsp;</td>
						<td class="cuerpo_ce"><br /><label style="font-size:14px;font-weight:bold;text-align:left">Certificados a Eliminar</label></td>
						<td class="cuerpo_de">&nbsp;</td>
					</tr>
					<tr>
						<td class="cuerpo_iz">&nbsp;</td>
						<td class="cuerpo_ce">
							<table width="80%" border="0" cellspacing="0" cellpadding="0" align="center" class="tablero">
								<tr>
									<th width="35%">Tipo</th>
									<th width="15%">Estado</th>
									<th width="15%">Inicial</th>
									<th width="15%">Final</th>
									<th width="15%">Presentacion</th>
									<th width="5%">Eliminar</th>										
								</tr>
								<tbody id="tbCertificado"></tbody>
							</table>
						</td>
						<td class="cuerpo_de">&nbsp;</td>
					</tr>
					<tr>
						<td class="cuerpo_iz">&nbsp;</td>
						<td class="cuerpo_ce"><br /><label style="font-size:14px;font-weight:bold;text-align:left">Causales</label></td>
						<td class="cuerpo_de">&nbsp;</td>
					</tr>
					<tr>
						<td class="cuerpo_iz">&nbsp;</td>
						<td class="cuerpo_ce">
							<table width="80%" border="0" cellspacing="0" cellpadding="0" align="center" class="tablero">
								<tr>
									<th width="50%">Periodo</th>
									<th width="50%">Causal</th>								
								</tr>
								<tbody id="tbCausales"></tbody>
							</table>
						</td>
						<td class="cuerpo_de">&nbsp;</td>
					</tr>
					<tr>
						<td class="cuerpo_iz">&nbsp;</td>
						<td class="cuerpo_ce"><br /><label style="font-size:14px;font-weight:bold;text-align:left">Subsidio en revision</label></td>
						<td class="cuerpo_de">&nbsp;</td>
					</tr>
					<tr>
						<td class="cuerpo_iz">&nbsp;</td>
						<td class="cuerpo_ce">
							<table width="80%" border="0" cellspacing="0" cellpadding="0" align="center" class="tablero">
								<tr>
									<th width="40%">Beneficiario</th>
									<th width="10%">ID</th>
									<th width="20%">Nit</th>
									<th width="20%">Periodo</th>
									<th width="15%">Procesado</th>							
								</tr>
								<tbody id="tbSubsidio"></tbody>
							</table>
						</td>
						<td class="cuerpo_de">&nbsp;</td>
					</tr>
					<tr>
						<td class="cuerpo_iz">&nbsp;</td>
						<td class="cuerpo_ce"><br /><label style="font-size:14px;font-weight:bold;text-align:left">Embargos</label></td>
						<td class="cuerpo_de">&nbsp;</td>
					</tr>
					<tr>
						<td class="cuerpo_iz">&nbsp;</td>
						<td class="cuerpo_ce">
							<table width="80%" border="0" cellspacing="0" cellpadding="0" align="center" class="tablero">
								<tr>
									<th width="50%">Tercero</th>
									<th width="50%">Beneficiario</th>							
								</tr>
								<tbody id="tbEmbargos"></tbody>
							</table>
						</td>
						<td class="cuerpo_de">&nbsp;</td>
					</tr>
					<tr>
						<td class="cuerpo_iz">&nbsp;</td>
						<td class="cuerpo_ce"><br /><label style="font-size:14px;font-weight:bold;text-align:left">Pignoraciones</label></td>
						<td class="cuerpo_de">&nbsp;</td>
					</tr>
					<tr>
						<td class="cuerpo_iz">&nbsp;</td>
						<td class="cuerpo_ce">
							<table width="80%" border="0" cellspacing="0" cellpadding="0" align="center" class="tablero">
								<tr>
									<th width="40%">Beneficiario</th>
									<th width="10%">Picnoracion</th>
									<th width="10%">Periodo</th>
									<th width="20%">Fecha pignoracion</th>	
									<th width="20%">Valor pignorado</th>
									<!--<th width="20%">Saldo</th>-->
														
								</tr>
								<tbody id="tbPignoraciones"></tbody>
							</table>
						</td>
						<td class="cuerpo_de">&nbsp;</td>
					</tr>
					<tr>
						<td class="cuerpo_iz">&nbsp;</td>
						<td class="cuerpo_ce"><br /><label style="font-size:14px;font-weight:bold;text-align:left">Defunciones</label></td>
						<td class="cuerpo_de">&nbsp;</td>
					</tr>
					<tr>
						<td class="cuerpo_iz">&nbsp;</td>
						<td class="cuerpo_ce">
							<table width="80%" border="0" cellspacing="0" cellpadding="0" align="center" class="tablero">
								<tr>
									<th width="25%">Beneficiario</th>
									<th width="25%">Muerto</th>
									<th width="25%">Tercero</th>
									<th width="15%">Fecha defuncion</th>
									<th width="10%">Parentesco</th>	
									<!--<th width="5%">Periodo</th>-->					
								</tr>
								<tbody id="tbDefunciones"></tbody>
							</table>
						</td>
						<td class="cuerpo_de">&nbsp;</td>
					</tr>									
					<tr>
						<td class="abajo_iz">&nbsp;</td>
						<td class="abajo_ce"></td>
						<td class="abajo_de">&nbsp;</td>
					</tr>					
				</table>
			</div>		
	</form>
	
		<!-- FORMULARIO OBSERVACIONES-->
	<div name="div-observaciones-tab" style="display:none" title="Observaciones de modificaci&oacute;n">
	<table class="tablero">
	 <tr>
	   <td>Usuario</td>
	   <td colspan="3" >
	   <input name="usuarioObs" id="usuarioObs" class="box1" disabled="disabled" value="<?php echo $_SESSION['USUARIO']?>" /></td>
	   </tr>
	 <tr>
	   <td>Observaciones</td>
	   <td colspan="3" >
	   <textarea name="observacionUnificacion" id="observacionUnificacion" cols="45" rows="5" class="boxlargo"></textarea></td>
	   </tr>
	</table>
	<div class="ui-state-highlight ui-corner-all" style="margin: 9px auto; display:none;padding: 6px; text-align: center; border: 1px solid rgb(255, 255, 0); width:300px" id="rtaObservacion"><span class="ui-icon ui-icon-info" style="float: left; margin-right: 0.3em;"></span>Observaci&oacute;n guardada con &eacute;xito.</div>
	</div>

<br />	
</body>
</html>