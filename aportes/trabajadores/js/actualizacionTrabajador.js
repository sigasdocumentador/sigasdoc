/*
* @autor:      Ing. Orlando Puentes
* @fecha:      04/09/2010
* objetivo:
*/
var arrIde=new Array();
var arrID=new Array();
var idEmpresa=0;
var idPersona=0;
$(document).ready(function(){
	var smlv=salarioMinimo();
	smlv=(isNaN(smlv) && smlv>0)?616000:smlv;
	$("#txtSMLV").val(smlv);
	

var URL=src();
$("#buscarPor").change(function(){
$("#pn,#pa,#idT").val('');
	if($(this).val()=='2'){
		$("#tipoDocumento").hide();
		$("#pn,#pa").show();
		$("#idT").hide();
	}else{
		$("#tipoDocumento").show();
		$("#pn,#pa").hide();
		$("#idT").show();
		$("#idT").focus();
	}
});

//Dialog Colaboracion en linea
$("#dialog-form2").dialog({
		autoOpen: false,
		height: 400,
		width: 500,
		modal: false,
		buttons: {
			'Enviar': function() {
			var bValid = true;
			var campo=$('#notas').val();
			var campo0=$.trim(campo);
			if (campo0==""){
				$(this).dialog('close');
					return false;
				}
			var campo1=$('#usuario').val();
			var campo2="consultaTrabajador.php";
			$.post(URL+'phpComunes/colaboracion.php',{v0:campo0,v1:campo1,v2:campo2},
			function(datos){
				if(datos=='1'){
					alert("Su comentario fue enviado correctamente!. Gracias por participar en el mejoramiento de SIGAS");
				}else{
					alert(datos);
				}
		});
		$(this).dialog('close');
		},
		Cancelar: function() {
			$(this).dialog('close');
			$("#dialog-form2").dialog("destroy");
		}
	}
	});

//Buscar Trabajador
$("#buscarT").click(function(){
	eventClick();
});//fin click
	
//... ICONO PARA OCULTAR/MOSTRAR FORMULARIO ...
$("div#icon span").click(function(){
	$(this).toggleClass("toggleIcon");
	$("div#wrapTable").slideToggle();
	
});//fin click icon	
});//fin ready



//... FUNCION PARA ENVIAR NIT PARA ACORDEON ...//
function enviarNit(idp,ide){
	var	indexTab;
	idEmpresa=ide;
	idPersona=idp;
	$("div#icon").show();
	$("#lstT").remove();
	$("#trabajadores").find("p").remove();
	$("div#wrapTable").slideUp("normal");	
	//$("#tabs").tabs("destroy");//Destruyo tab para nueva consulta	
	jQuery('div[id=divMotivos]').remove(); jQuery('div[id=beneficiarios]').remove();
	jQuery('div[id=div-conyuge]').remove(); jQuery('div[id=div-datosCertificados]').remove();
	jQuery('div[id=div-conyugeA]').remove(); jQuery('div[id=modificarBeneficiarios]').remove();
	jQuery('div[id=divRelacionesActivas]').remove();jQuery('div[id=divDocumentoDoble]').remove();
	jQuery('div[id=modificarCertificados]').remove(); jQuery('div[id=dialog-actualiza]').remove();
	jQuery('div[id=dialog-persona]').remove(); jQuery('div[id=divFechaAsignacion]').remove();
	jQuery('div[id=dialog-persona2]').remove();
	$("#tabsA #a1").unbind("click"); $("#tabsA #a2").unbind("click"); 
	$("#tabsA #a3").unbind("click"); $("#tabsA #a4").unbind("click");
	$("#tabsA ul li a:not(':last')").one("click",function(){
		var aid=$(this).attr("id");
		 switch(aid){
			case "a0": $("#tabs-1").load("tabConsulta/personaTab.php",{v0:idPersona}); break; 
			case "a1": $("#tabs-2").load("tabModifica/afiliacionTab.php",{v0:idPersona}); break; 
			case "a2": $("#tabs-3").load("tabModifica/grupoTab.php?v0="+idPersona+"&v1=M");    break; 
			case "a3": $("#tabs-4").load("tabConsulta/documentosTab.php",{v0:idPersona}); break;
		 }
	 });
	$("#tabsA").show("slow",function(){
		$("#tabsA ul li:first a").trigger("click");
	});	//Muestro el tab

	//Carga los tabs
	$("#tabsA").tabs({
		spinner:'<em>Loading&#8230;</em>',
		selected:0,
	});//end tab		
}//END FUNCTION BUSCAR NIT

var nuevo=0;
var modificar=0;
var continuar=true;
var msg="";

$(function() {
		$("#ayuda").dialog({
		 	autoOpen: false,
			height: 450, 
			width: 700, 
			draggable:true,
			modal:false,
			open: function(evt, ui){
					$('#ayuda').html('');
					$.get(URL+'help/aportes/modificararfiliaciontrabajador.html',function(data){
							$('#ayuda').html(data);
					});
			 }
		});
	});

function buscarPersonaTres(obj){
	idnb=0;
	$('#pNombre3').val("");
	$('#sNombre3').val("");
	$('#pApellido3').val("");
	$('#sApellido3').val("");
	$("#fecNacHide3").val('');
	$("#capTrabajo").val("N"); $("#capTrabajo").attr("disabled", false);
	$("#filaDiscapacidad,#filaEscolaridad,#filaUniversidad,#filaSupervivencia,#cerVigencia").hide();
	$("#fDiscapacidad,#fEscolaridad,#fUniversidad,#fSupervivencia,#pInicialV3,#pFinalV3").val('');
	$("#cDiscapacidad,#cEscolaridad,#cUniversidad,#cSupervivencia,#vigencia1,#vigencia2").attr("checked",false);

	if(obj.value!=""){ 
		if($("#parentesco").val()=="0"){
			alert("Seleccione primero un parentesco");
			$("#parentesco").focus();
	    	obj.value='';	    	
	    	return false;
		}
		if($("#tipoDoc3").val()==0){
	   		alert("Falta tipo documento");
	   		$("#tipoDoc3").focus();
		   	obj.value='';
		   	return false;
		}	
	
		validarLongNumIdent($("#tipoDoc3").val(),obj);
		if(obj.value!=""){ buscarPersonaDos(obj.value); }
	}	
}

function buscarPersonaDos(num){
	var idtd=$("#tipoDoc3").val();
	var idp=$("#idPersona").val();
	var parentesco = $("#parentesco").val();
	
	$.ajax({
		url: URL+'phpComunes/buscarPersona2.php',
		async: false,
		data: {v0:idtd,v1:num},		
		beforeSend: function(objeto){
        	dialogLoading('show');
        },        
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }            
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("En buscarPersonaDos Paso lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
		success: function(data){	
		    if(data==0){		    			    	
		        existe=0;
		        buscarPersonaExistente($("#identificacion3").val(),$("#pNombre3").val(),$("#sNombre3").val(),$("#pApellido3").val(),$("#sApellido3").val(),false,existe);
		        return false;
		    } else {
			    $.each(data,function(i,fila){
			    	if ( fila.idpersona == idPersona ) {
						alert("No se puede crear una relacion con la misma persona");		    		
						$("#identificacion3").val(""); 
					   	return false;
					} else if ( fila.estado == "M" ){
						alert("El beneficiario se encuentra FALLECIDO!!!\n"+
							"No es posible relacionar a este beneficiario");		    		
						$("#identificacion3").val(""); 
					   	return false;
					} else {			    	
				    	var max=0;
				    	var salir=false;		    	
				    	idnb=fila.idpersona;
				    	
				    	/*** VALIDACIONES ***/
				    	if(salir==false){
							var retorno = buscarRelacionActivasBeneficiario( $("#idPersona").val(), idnb, parentesco, true, false);
							if ( retorno > 1 ){
								salir=true;
								
								if ( retorno == 4 ){
									//Ya esta la validacion. Ya esta Afiliado
								} else if ( retorno == 3 ){
									alert("Ocurrio un error validando relaciones del Beneficiario!!");						
								} else {					
									$("#divRelacionesActivas").dialog('open');
									
									if ( retorno == 2 ){
										alert("Beneficiario tiene el maximo numero de Relaciones!!");
									} else if ( retorno == 34 ) {
										alert("Tiene afiliaciones Activas como Conyuge!!");						
									} else if ( retorno == 35 ) {
										alert("El beneficiario tiene Afiliaciones Activas como Hijo!!!\n"+
												"Revise si es correcto el parentesco que esta seleccionando");						
									} else if ( retorno == 36 ) {
										alert("El beneficiario tiene Afiliaciones Activas como Padre!!!\n"+
												"Revise el parentesco que esta seleccionando");						
									} else if ( retorno == 37 ) {
										alert("El beneficiario tiene Afiliaciones Activas como Hermano!!!\n"+
												"Revise el parentesco que esta seleccionando");						
									} else if ( retorno == 38 ) {
										alert("El beneficiario tiene Afiliaciones Activas como Hijastro!!!\n"+
												"Revise el parentesco que esta seleccionando");						
									} else if ( retorno == 3538 ) {
										var cadena="EL PADRE ";
										if ( $("#txtHiddenSexo").val() == "F"){
											cadena="LA MADRE ";
										}
										alert("El beneficiario tiene Afiliacion Activa como HIJO!!!\n" +
												cadena + "LE TIENE AFILIADO");						
									} else if ( retorno == 3835 ) {
										var cadena="EL PADRASTRO ";
										if ( $("#txtHiddenSexo").val() == "F"){
											cadena="LA MADRASTRA ";
										}
										alert("El beneficiario tiene Afiliaciones Activa como HIJASTRO!!!\n" +
												cadena + "LE TIENE AFILIADO");						
									}
								}
							}
				    	}
					
						if(salir){				   	
						   	$("#identificacion3").val(""); 
						   	return false; 
						}
		
						salir = (buscarAfiliacionBeneficiario(idnb)==0) ? false : true;
						if(salir){
						   	alert("La persona que trata de Afiliar es un trabajador activo!");
						   	$("#identificacion3").val(""); return false; 
						}	 	
						
						existe=1;
						$('#pNombre3').val($.trim(fila.pnombre));
				    	$('#sNombre3').val($.trim(fila.snombre));
				    	$('#pApellido3').val($.trim(fila.papellido));
				    	$('#sApellido3').val($.trim(fila.sapellido));
				    	var fecha=$.trim(fila.fechanacimiento);
				    	fecha=fecha.replace("-","/");
				    	$('#fecNacHide3').val(fecha.replace("-","/")).trigger('change');	    	
				    	$('#sexo3').val(fila.sexo);
				    	$('#estadoCivil3').val(fila.idestadocivil);	    	
				    	$("#capTrabajo").val(fila.capacidadtrabajo).trigger('change');
				    	if(fila.capacidadtrabajo=="I"){ $("#capTrabajo").attr("disabled", true); }
				    	
				    	$("#cboPaisB").val($.trim(fila.idpais)).trigger('change');
				    	setTimeout((function(){		    		
				    		$("#cboDeptoB").val($.trim(fila.iddepnace)).trigger('change');
				    	}),1200);
				    	setTimeout((function(){		    		
				    		$("#cboCiudadB").val($.trim(fila.idciunace));
				    	}),2400);
				    	
				    	return true;
					}
				}); //each
			}
		},
		type: "GET"
	}); //getJSON
}//end funcion		

function runSearch(e){
	if(e.keyCode == 13){
		eventClick();
	} 
}

function eventClick(){
	$("#trabajadores").find("p").remove();
	//espera
	dialogLoading("show");
	$("#tabsT").hide();
	
	if($("#idT").is(":visible")&&$("#idT").val()==''){
		$(this).next("span.Rojo").html("Ingrese un valor a buscar.").hide().fadeIn("slow");
		dialogLoading("hide");
		$("#idT").focus();
		return false;
	}	
	if($("#pn").is(":visible")&&$("#pn").val()==''&&$("#pa").val()==''){
		$(this).next("span.Rojo").html("Ingrese un valor a buscar.").hide().fadeIn("slow");
		dialogLoading("hide");
		$("#pn").focus();
		return false;
	}	
		
	var v0=$("#idT").val();
	var v1=$("#buscarPor").val();
	var v2=$("#tipoDocumento").val();
	var v3=$("#pn").val();//campo nombre
	var v4=$("#pa").val();//campo nombre
	var contadorAfi=0;
	var contadorTemp=0;
	var cadenaAfiliados='';
	
	$.ajax({
		url: URL+'phpComunes/buscarPersonaPara.php',
		type: "POST",
		data: {v0:v0,v1:v1,v2:v2,v3:v3.toUpperCase(),v4:v4.toUpperCase()},
		async: false,
		dataType: 'json',
		success: function(data){
			if(data==0){
				$("#buscarT").next("span.Rojo").html("No se encontr&oacute; la persona").hide().fadeIn("slow");
				dialogLoading("hide");	
				return false;
			}		
			$("#lstT").remove();
			
			$.each(data,function(i,fila){
				$("#buscarT").next("span.Rojo").html(""); 
				$("#idT").val('');
				idp=fila.idpersona;
				nom =fila.pnombre+" "+fila.snombre+" "+fila.papellido+" "+fila.sapellido;
				$.ajax({
					url:URL+'phpComunes/buscarAfiliacionIDP.php',
					type: "POST",
					data: {v0:idp},
					async: false,
					dataType: 'json',
					success: function(datos){
						if(datos==0){
							$("#buscarT").next("span.Rojo").html("No se encontraron AFILIACIONES.").hide().fadeIn("slow");
							dialogLoading("hide");	
							return false;
						}
						$.each(datos,function(i,fila){
							if(fila.idempresa!=null) {
								arrIde[i]=fila.idempresa;
							}
							return;
						});						
						contadorAfi++;
						cadenaAfiliados+='<p align="left" onclick="enviarNit('+idp+','+arrIde+')">'+idp+" "+nom+'</p>';
					}//success
				});
			});//fin each	for
		    
			$("#trabajadores").append(cadenaAfiliados);
			dialogLoading("hide");	
			$("#trabajadores").before("<p id='lstT' align='center'>Trabajadores encontrados:<b> "+contadorAfi+"</b></p>");					
		}//success
	});
}