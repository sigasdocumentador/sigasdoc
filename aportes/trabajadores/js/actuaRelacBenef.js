var URL=src(),
	
	urlBuscarPersona = "",
	urlBeneficiarios = "",
	urlActuaRelacBenef = "",
	
	htmlComboOptioEstadRelac = null;


$(function(){
	urlBuscarPersona = URL+"phpComunes/buscarPersona.php";
	urlBeneficiarios = URL+"phpComunes/buscaGrupoFamil.php";
	
	urlActuaRelacBenef = "actuaRelacBenef.log.php";
	
	htmlComboOptioEstadRelac = fetchComboOptioEstadRelac();
	
	$("#btnBuscarPersona")
			.click(controlPersona)
			.button();
	
	$("#btnActualizar")
			.button()
			.click(function(){
				
				if(validaCampoFormu()>0)return false;
				
				var banderaCont = 0;
				//Verificar si el usuario actualizo algun estado
				$("#tabBeneficiario tbody tr").each(function(i,row){
					var idTr = "#"+$(this).attr("id");
					var estado = $(idTr + " select[name='cmbEstadoRelacion']").val().trim();
					var estadoOld = $(idTr + " input[name='hidEstadoRelacion']").val().trim();
					
					if(estado!=estadoOld) banderaCont++;
				});
				
				if(banderaCont==0){
					alert("Debe activar algun beneficiario");
					return false;
				}else{
					$("#divObservacion").dialog("open");
				}
			});
	
	$("#divObservacion").dialog({
		modal:true,
		autoOpen:false,
		width: 700,
		heigth:500,
		buttons:{
			"ACEPTAR": function(){
				var observacion = $("#txaObservacion").val().trim();
				
				if(observacion==0){
					$( "#txaObservacion" ).addClass( "ui-state-error" );
				}
				
				$( "#txaObservacion" ).removeClass( "ui-state-error" );
				
				actualizar();
				$(this).dialog("close");
			}
		},
		close: function(){
			$(this).dialog("close");
			$(this).dialog("destroy");
		}
	});
});

function controlPersona(){
	var identificacion = $("#txtNumeroIdentificacion").val().trim();
	var banderaInformacion = "";
	
	nuevo();
	
	$("#txtNumeroIdentificacion").val(identificacion);
	
	//Verificar si el usuario digito algun valor en la identificacion
	if(identificacion==0){
		nuevo();
		return false;
	}
	
	
	var objPersona = fetchDataPersona(identificacion);
	
	//Verificar existen datos 
	if(typeof objPersona == "object" && objPersona.length>0 ){
		banderaInformacion = objPersona[0].identificacion+" - "+objPersona[0].pnombre + " " +
				objPersona[0].snombre + " " + objPersona[0].papellido + " " + objPersona[0].sapellido;
		
		$("#hidIdPersona").val(objPersona[0].idpersona);
	}else{
		banderaInformacion = "La persona no existe";
	}
	
	$("#lblDatosPersona").text(banderaInformacion);
	
	controlBeneficiario();
}

function controlBeneficiario(){
	var idPersona = $("#hidIdPersona").val();
	var identificacion = $("#txtNumeroIdentificacion").val();
	if(idPersona==0)return false;
	
	var objFiltro = {idtrabajador:idPersona}
	var objDataBeneficiario = fetchDataBenef(objFiltro);
	var banderaHtml = "";
	
	//Verificar existen datos 
	if(typeof objDataBeneficiario == "object" && objDataBeneficiario.length>0 ){
		
		$("#tabBeneficiario tbody").html("");
		
		$.each(objDataBeneficiario,function(i,row){
			if(row.idparentesco!=34){
				banderaHtml = '<tr id="trIdRelacion'+row.idrelacion+'">'+
								'<td>'+
									row.identificacion+
									'<input type="hidden" name="hidIdBeneficiario" value="'+row.idbeneficiario+'"/>'+
								'</td>'+
								'<td>'+row.beneficiario+'</td>'+
								'<td>'+row.estad_perso_benef+'</td>'+
								'<td>'+row.fechanacimiento+'</td>'+
								'<td>'+
									'<select name="cmbEstadoRelacion" class="box1 element-required">'+
										htmlComboOptioEstadRelac+
									'</select>'+
									'<input type="hidden" name="hidEstadoRelacion" value="'+row.estado_relacion+'"/>'+
								'</td>'
							'</tr>';
				
				$("#tabBeneficiario tbody").append(banderaHtml); 
				
				//Seleccionar el tipo cargo, formulario y tipo afiliacion
				$("#tabBeneficiario #trIdRelacion"+row.idrelacion+ " select[name='cmbEstadoRelacion']").val(row.estado_relacion);
			}
		});
		
	}else{
		banderaHtml = "<tr><td colspan='5'>La persona no tiene beneficiarios</td></tr>";
		$("#tabBeneficiario tbody").html(banderaHtml); 
	}
}

function actualizar(){
	
	//Validar campos obligatorios
	if(validaCampoFormu()>0)return false;
	
	var observacionActiva = $("#txaObservacion").val().trim();
	var objBandera = [];
	//Obtener las afiliaciones y campos que fueron actualizados
	$("#tabBeneficiario tbody tr").each(function(i,row){
		var idTr = "#"+$(this).attr("id");
		var idRelacion = idTr.replace("#trIdRelacion","");
		
		var estadoRelacion = $(idTr + " select[name='cmbEstadoRelacion']").val().trim();
		var estadoRelacionOld = $(idTr + " input[name='hidEstadoRelacion']").val().trim();
		
		var idBeneficiario = $(idTr + " input[name='hidIdBeneficiario']").val().trim();
		
		var observacion = $("#txaObservacion").val().trim();
		
		if(estadoRelacion!=estadoRelacionOld){
			objBandera[objBandera.length] = {
					id_relacion: idRelacion,
					id_beneficiario: idBeneficiario,
					estado_relacion: estadoRelacion,
					observacion: observacion
				};
		}
	});
	
	var objDatos = {
			id_persona: $("#hidIdPersona").val().trim(),
			datos_beneficiario:objBandera
		};
	
	$.ajax({
		url:urlActuaRelacBenef,
		type:"post",
		data:{datos:objDatos},
		dataType:"json",
		async:false,
		success: function(data){
			
			if(data!=0){
				alert("La informacion se actualizo correctamente");
				nuevo();
			}else{
				alert("Error al actualizar la informacion");
			}
		}
	});
}

/**
 * METODO Encargado de preparar los option para el combo del estado relacion
 * 
 * @returns {String} Combo
 */
function fetchComboOptioEstadRelac(){
	var htmlOption = "<option value=''>Seleccione...</option>";
	htmlOption += "<option value='A'>Activa</option>";
	htmlOption += "<option value='I'>Inactiva</option>";
	
	return htmlOption;
}

/**
 * METODO Encargado de obtener los datos de la persona que se encuentra en la BD.
 * @param identificacion
 * @returns Array Datos de la persona
 */
function fetchDataPersona(identificacion){
	var resultado = [];
	$.ajax({
		url:urlBuscarPersona,
		type:"post",
		data:{v0:identificacion},
		dataType:"json",
		async:false,
		success: function(data){
			if(data!=0){
				resultado = data;
			}
		}
	});
	return resultado;
}

/**
 * METODO Encargado de obtener los datos de los beneficiarios.
 * 
 * @returns Array Datos de la afiliacion
 */
function fetchDataBenef(filtro){
	var resultado = [];
	$.ajax({
		url:urlBeneficiarios,
		type:"post",
		data:{objDatosFiltro:filtro},
		dataType:"json",
		async:false,
		success: function(data){
			if(data!=0){
				resultado = data;
			}
		}
	});
	return resultado;
}

function nuevo(){
	$("#tabBeneficiario tbody").html(""); 
	$("#hidIdPersona,#txaObservacion").val("");
	$("#lblDatosPersona").text("");
}

/**
 * METODO: Valida los elementos requeridos del formulario
 * 
 * @returns int Numero de elementos vacios
 */
function validaCampoFormu(){
	//En este metodo no se validan los periodos a eliminar o actualizar
	
	var banderaError = 0;
	$( ".ui-state-error" ).removeClass( "ui-state-error" );
	$(".element-required").each(function(i,row){
		//Verifica que los campos esten visibles porque pueden ser periodos eliminados
		if($( this ).val() == 0 && $( this ).is(":visible")){
			$( this ).addClass( "ui-state-error" );
			banderaError++;
		}
	});
	return banderaError;
}
