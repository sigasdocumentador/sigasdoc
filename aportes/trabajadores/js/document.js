/*
* @autor:      Ing. Orlando Puentes
* @fecha:      07/10/2010
* objetivo:
*/

errorTab='';
errorE='EMPRESA';
errorD='DATOS P.';
errorA='AFILIACION';
ok1=0;
ok2=0;
ok3=0; 

$(document).ready(function(){
//$("#fecIngreso").datepicker({changeMonth: true,changeYear: true});
//$("#txtFAfilia").datepicker({changeMonth: true,changeYear: true});

$("#fecIngreso,#txtFAfilia,#fEscolaridad,#fUniversidad,#fDiscapacidad,#fSupervivencia,#fAsignacion").datepicker( {changeMonth: true,changeYear: true} );
$(".ui-datepicker").css("z-index",3000);
var idDiv;
//errorTab;
var lista;
var op;

//...PLUGIN PARA EL TAB...//
//$("#ul li a").one('click',function(){
	/*
$("#ul li a").bind('click',function(){
	conClick++;
	if(conClick<=2){
	var selected = $(this).attr("id");
	if(selected=='a1'){
		preliminares();					
		$('#pNombre').click().blur();
	}
	}
});*/

$("#tabs").tabs(); //end tab

$("#tabs ul li a").click(function(){
	$("#errores ul").remove();
	var  lista="<ul class='Rojo'>";
	errorTab='';
	op=parseInt($(this).attr("id").slice(-1));
	
	conClick++;
	if(conClick<=2){
	var selected = $(this).attr("id");
	if(selected=='a1'){
		preliminares();					
		$('#pNombre').click().blur();
	}
	}
	//a=$(this).attr("id");
	switch(op){
	case 1://...EMPRESA 
		ok1=0;
		var sucursal=$("#comboSucursal").val();
		if(sucursal=="0"||sucursal==''){lista+='<li>Seleccione una sucursal.</li>';ok1++;}
		if($("#nitActual").val()==''){lista+='<li>Cree un nuevo Trabajador.</li>';ok1++}
		if(ok1>0){	  
		    empresa=false;
			if(empresa==false){
				$("#tabs").tabs("select",0);errorE="EMPRESA";
				//$(this).blur();
				//ok1=0;
			}
		}else{ errorE='';empresa=true;}
		break;
	case 2:			 
		ok2=0;
		//...DATOS PERSONALES 
		var tipoDoc=$("#tipoDoc").val();
		var cedula=$("#identificacion").val();
		var pNombre=$("#pNombre").val();
		var pApellido=$("#pApellido").val();
		var sNombre=$("#sNombre").val();//no obligado
		var sApellido=$("#sApellido").val();//no obligado
		var sexo=$("#sexo").val();
		var deptRes=$("#cboDepto").val();//$("#deptRes").val();
		var ciudadRes=$("#cboCiudad").val();//$("#ciudadRes").val();
		var cboZona=$("#cboZona").val();//$("#cboZona").val();
		var direccion=$("#tDireccion").val();
		var cboBarrio=$("#cboBarrio").val();
		var telefono=$("#telefono").val();
		var celular=$("#celular").val();//no obligado
		var email=/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test($("#email").val());
		var tipVivienda=$("#tipoVivienda").val();
		var estadoCivil=$("#estadoCivil").val();
		var fecNac=$("#fecNac").val();
		var depNac=$("#cboDepto2").val();
		var ciudadNac=$("#cboCiudad2").val();
		var capTrabajo=$("#capTrabajo").val();
		var profesion=$("#profesion").val();
		var nom=$('#pNombre').val()+" "+$('#sNombre').val()+" "+$('#pApellido').val()+" "+$('#sApellido').val(); 
		$('#trabajador').val(nom);
		//---------------------TIPO DOC----------------------  
		if(tipoDoc=='0'){lista+='<li>Seleccione un tipo de documento.</li>';ok2++}
		//--------------------CEDULA-----------------------   
		if(cedula==''){lista+='<li>Ingrese la identificaci\u00F3n.</li>';
		ok2++;
		} else {
			if(cedula.length<3){
				lista+='<li>La identificaci\u00F3n NO debe tener 2 d\u00EDgitos.</li>';
				
				ok2++;
			}else {
				if(cedula.length==9){
					lista+='<li>La identificaci\u00F3n NO debe tener 9 d\u00EDgitos.</li>';
					ok2++;
				}
			}//end else
		}//end else
		//----------------PRIMER NOMBRE Y APELLIDO---------------------------   
		if(pNombre==''){lista+='<li>Ingrese el NOMBRE del cliente.</li>';ok2++;}
		if(pApellido==''){lista+='<li>Ingrese el APELLIDO del cliente.</li>';ok2++;}
		//-------------------SEXO-----------------------
		if(sexo=='0'){lista+='<li>Seleccione el sexo.</li>';ok2++;}
		//-------------------DIRECCION-----------------------
		if(direccion==''){lista+='<li>Ingrese la direcci\u00F3n.</li>';ok2++;}
		//-------------------cboBarrio----------------------
		/*
				if(cboBarrio=='0'){
					lista+='<li>Seleccione un cboBarrio.</li>';
						
					ok2++;
				  }   
				  */
				//-------------------TELEFONO----------------------
		if(telefono==''){
			lista+='<li>Ingrese el TELEFONO del cliente.</li>';
			
			ok2++;	
		}else{ 
			if(telefono.length<7){
				lista+='<li>Faltan d\u00EDgitos en el  TELEFONO.</li>';
				
				ok2++;
			}
		}
		//---------------------EMAIL------------------------
		if($("#email").val()!=''){
			if(!email) {
				lista+='<li>El E-mail aparentemente es incorrecto.</li>';										 
				ok2++;		
			}else{
				email=$("#email").val();
			}
				}
			//------------------------------------------------------
		if(tipVivienda=='0'){lista+='<li>Seleccione el tipo de vivienda.</li>';ok2++;}
		if(deptRes=='0'){lista+='<li>Seleccione el Departamento de residencia.</li>';ok2++;}
		if(ciudadRes=='0'||ciudadRes==''){lista+='<li>Seleccione la Ciudad de residencia.</li>';ok2++;}
		if(cboZona=='0'){lista+='<li>Seleccione la zona de residencia.</li>';ok2++;}
		if(estadoCivil=='0'){lista+='<li>Seleccione el estado civil.</li>';ok2++;}
		if(depNac=='0'){lista+='<li>Seleccione el departamento de nacimiento.</li>';ok2++;}
		if(ciudadNac=='0'||ciudadNac==''){lista+='<li>Seleccione la ciudad de nacimiento.</li>';ok2++;}
		if(capTrabajo=='0'){lista+='<li>Seleccione la capacidad de trabajo.</li>';ok2++;}
		//if(profesion=='0'){lista+='<li>Seleccione una profesi\u00F3n.</li>';ok2++;}
		//----------------------FECHA DE NACIMIENTO---------------
		if(fecNac=='' || fecNac=='mmddaaaa'){
			lista+='<li>Escriba la fecha de nacimiento.</li>';
				
			ok2++;
				}
		if(ok2>0){
			datos=false;
			if(datos==false){
				$("#tabs").tabs("select",1);
				errorD="DATOS P." ;
				//$(this).blur();
				//ok2=0;
			}	
		}else{ errorD=''; datos=true;$("#tipForm").focus();}
		break; 
			case 3://...AFILIACION
				ok3=0;
				var tipForm=$("#tipForm").val();
				var tipAfiliacion=$("#tipAfiliacion").val();
				var fecIngreso=$("#fecIngreso").val();
				var horasDia=$("#horasDia").val();
				var horasMes=$("#horasMes").val();
				var tipoPago=$("#tipoPago").val();
				var salario=$("#salario").val();
				var categoria=$("#categoria").val();//NO OBLIGADO
				var agricola=$("#agricola").val();
				var cargo=$("#cargo").val();
				var estado=$("#estado").val();
				var traslado=$("#traslado").val();
				var codCaja=$("#codCaja").val();	 //OBLIGADO VARIA
				
				//----------------------TIPO FORMULARIO-----------------
				if(tipForm=='Seleccione..'){lista+='<li>Seleccione un tipo de formulario.</li>';ok3++}
			
				//------------------TIPO AFILIACION--------------------------
				if(tipAfiliacion=='Seleccione..'){lista+='<li>Seleccione un tipo de afiliaci\u00F3n.</li>';ok3++}
			
			        //------------------TIPO AFILIACION--------------------------
				if(fecIngreso==''){lista+='<li>Seleccione fecha de ingreso.</li>';ok3++}
				   
				   //----------------HORAS DIA-------------------------------
				if(horasDia==''){lista+='<li>Ingrese las HORAS al d\u00EDa.</li>';ok3++}
			
			       //--------------HORAS MES---------------------------------
				if(horasMes==''){lista+='<li>Ingrese las HORAS al mes.</li>';
				ok3++
				}else{
					if(horasMes>240||horasMes<=0){
						lista+='<li>Las HORAS al mes NO deben ser mayor a 240, ni IGUAL a cero.</li>';
							
						ok3++
					}
				}
				//------------------TIPO PAGO---------------------------------
				if(tipoPago=='0'){lista+='<li>Seleccione un tipo de pago.</li>';ok3++}
				//------------------------SALARIO-------------------------
				if(salario==''){lista+='<li>Ingrese el SALARIO.</li>';ok3++}
				//--------------AGRICOLA----------------------------------
				if(agricola=='Seleccione..'){lista+='<li>Seleccione un opci\u00F3n en AGRICOLA.</li>';ok3++}
				//-------------------CARGO-------------------------------
				if(cargo=='Seleccione..'){lista+='<li>Seleccione un cargo.</li>';ok3++}
				//---------------------ESTADO-----------------------------
				if(estado=='Seleccione..'){lista+='<li>Seleccione un estado.</li>';ok3++}
				//----------------------TRASLADO------------------------
				if(traslado=='Seleccione..'){lista+='<li>Seleccione un tipo de traslado.</li>';ok3++}
				//------------------------COD CAJA----------------------
				if(traslado=='S'){if(codCaja=='0'){lista+='<li>Seleccione un c\u00F3digo de caja.</li>';ok3++}					  
				}
				if(ok3>0){
					afiliacion=false;
					if(afiliacion==false){
						$("#tabs").tabs("select",2);
						errorA="AFILIACION";
						//$(this).blur();
				       	//ok3=0;
					}
				}else{errorA='';afiliacion=true;}
				break;
	        }//ens switch
		//si esta bien emrpesa
/*	if(ok1==0){
		empresa=true;
	}
	//si esta bien datos p		   
	if(ok2==0){
		datos=true;	
	}  
	//si esta bien afiliacion		
	if(ok3==0){
		afiliacion=true;
	}*/
	//..MOSTRAR ERRORES..//
	$(lista+"</ul>").appendTo($("#errores"));
	$("#errores ul.Rojo").hide().fadeIn("slow");		
});//---------END CLICK
 //----TRASLADO VALIDACION
$("#traslado").change(function(){
	if( $("#traslado").val()=='S'){
		$("#codCaja").next("img").show();
	}else{
		$("#codCaja").next("img").hide();
		$("#codCaja").val('Seleccione..');
	}  
  });

//...MOSTRAR NIT...//		
$("#nitOtra").blur(function(){
	var nitOtro=$("#nitOtra").val();
	if(nitOtro.length > 2) {
		alert("Buscar Sucursal.");  
	}
});//end blur nit

//...GRUPO FAMILIAR ...//
$("#conyuge").dialog({
	autoOpen:false,
	width:740,
	modal: true,
	open:function(){
		$("#tipRel").focus();
		},
	buttons: {
	    'Grabar': function() {
	     verificarC();
	}		  
	},
	close:function(){
		$("#conyuge ul").remove();
		$("#conyuge table.tablero input:text").val('');
		$("#conyuge table.tablero select").val('Seleccione');
		$("#conyuge input:checkbox").attr("checked",false);
		$("table[name='dinamicTable']").remove();
	}//end boton
});

$("#beneficiarios").dialog({
	autoOpen:false,
	width:640,
	modal: true,
	open:function(){
		$("#parentesco").focus();
		//desactivar check CERTIFICADO
		$("#cEscolaridad,#cUniversidad,#cSupervivencia").attr("disabled","disabled");
		},
	buttons: {
		'Grabar': function() {
			verificarB();
		}		  
	},
	close:function(){
		$("#beneficiarios ul").remove();
		$("#beneficiarios table.tablero input:text").val('');
		$("#beneficiarios table.tablero select").val('Seleccione');
		$("#beneficiarios input:checkbox,#beneficiarios input:radio").attr("checked",false);
		// TABLA DINAMICA PARA DATOS EMPRESA TRABAJDOR ACTIVO
		$("table[name='dinamicTable']").remove();
		$("#cedMama").attr("disabled",false);
	}//end boton												
});
	
$("#nuevoGrupo").click(function(){
	if($("#selParentesco").val()=='Seleccione..'){
		$(this).next("span.Rojo").html("Seleccione un parentesco.");
	}						
	if($("#selParentesco").val()=='c'){
		$(this).next("span.Rojo").html("");
		$("#conyuge").dialog('open');							 
		//$("#tipRel").focus();
	}								 
	if($("#selParentesco").val()=='b'){
		$(this).next("span.Rojo").html("");
		$("#beneficiarios").dialog('open');
	}
	});
	
	//... COMBO PARA MOSTRAR TABLA ...//
	$("#selParentesco").change(function(){
		$(this).siblings("span.Rojo").html("");
		$("#tablaB").hide();
		if($(this).val()=='Seleccione..'){
			$("#tablaB,#tablaC").hide();
		}
		//Si es CONYUGE MOSTRAR TABLA CONYUGE
		if($("#selParentesco").val()=='c'){	
			$("#tablaC").show();
			}
		//Si es CONYUGE MOSTRAR TABLA BENEFICIARIOS
		if($("#selParentesco").val()=='b'){
			 $("#tablaC").show();
			$("#tablaB").show();
		}
		});
//------DIALOG BENEFICIARIO CONYUGE------------------------------------------------



var nuevo=0;
var modificar=0;
var continuar=true;
var msg="";
$(function() {
	$("#ayuda").dialog({
		autoOpen: false,
		height: 500, 
		width: 700, 
		draggable:true,
		modal:false,
		open: function(evt, ui){
			$('#ayuda').html('');
			$.get('../../help/aportes/ayudaTrabajadorNuevo.html',function(data){
				$('#ayuda').html(data);
			})
		}
	});
});

$(function() {
	$("#dialog-form2").dialog("destroy");
	$("#dialog-form2").dialog({
		autoOpen: false,
		height: 400,
		width: 500,
		modal: true,
		buttons: {
			'Enviar': function() {
			var bValid = true;
			var campo=$('#notas').val();
			var campo0=$.trim(campo);
			if (campo0==""){
				$(this).dialog('close');
					return false;
				}
			var campo1=$('#usuario').val();
			var campo2="trabajadorNuevo.php";
			$.post('../../phpComunes/colaboracion.php',{v0:campo0,v1:campo1,v2:campo2}, 								
			function(datos){
				if(datos=='1'){
					alert("Su comentario fue enviado correctamente!. Gracias por participar en el mejoramiento de SIGAS");
				}else{
					alert(datos);
				}
		});
		$(this).dialog('close');
		},
		Cancelar: function() {
			$(this).dialog('close');
			$("#dialog-form2").dialog("destroy");
		}
	}		
	});
	/*
	$('#enviar-notas')
		.button()
		.click(function() {
		$('#dialogo-dir2').dialog('open');
	});
	*/
});	

var pn="";
var sn="";
var pa="";
var sa="";
var nu=0;
var i;
var patron=new RegExp('[\u00F1|\u00D1]');
$("#pNombre").blur(function(){
	pn=$(this).val();
	//---Reemplazar Ã‘
	for(i=1;i<=pn.length;i++){
	pn=pn.replace(patron,"&");
	}
	//-------------
	txt=pn+" "+sn+" "+pa+" "+sa;
	txt=txt.toUpperCase();
	
	
	$("#nombreCorto").val(txt)
	num=parseInt(txt.length);
	$("#nombreCorto").next("span").html(num);
	$("#nombreCorto").trigger("change");
});

$("#sNombre").blur(function(){
	sn=$(this).val();
	//---Reemplazar Ã‘
	for(i=1;i<=sn.length;i++){
	sn=sn.replace(patron,"&");
	}
	//-------------
	txt=pn+" "+sn+" "+pa+" "+sa;
	txt=txt.toUpperCase();
	$("#nombreCorto").val(txt)
	num=parseInt(txt.length);
	$("#nombreCorto").next("span").html(num);
	$("#nombreCorto").trigger("change");
});
$("#pApellido").blur(function(){
	pa=$(this).val();
	//---Reemplazar Ã‘
	for(i=1;i<=pa.length;i++){
	pa=pa.replace(patron,"&");
	}
	//-------------
	txt=pn+" "+sn+" "+pa+" "+sa;
	txt=txt.toUpperCase();
	$("#nombreCorto").val(txt)
	num=parseInt(txt.length);
	$("#nombreCorto").next("span").html(num);
	$("#nombreCorto").trigger("change");
});
$("#sApellido").blur(function(){
	sa=$(this).val();
	//---Reemplazar Ã‘
	for(i=1;i<=sa.length;i++){
	sa=sa.replace(patron,"&");
	}
	//-------------
	txt=pn+" "+sn+" "+pa+" "+sa;
	txt=txt.toUpperCase();
	$("#nombreCorto").val(txt)
	num=parseInt(txt.length);
	$("#nombreCorto").next("span").html(num);
	$("#nombreCorto").trigger("change");
});

$("#nombreCorto").change(function(){
	if(num>30){
		if(sn.length>0){
			sn=sn.slice(0,1);
			txt=pn+" "+sn+" "+pa+" "+sa;
			txt=txt.toUpperCase();
			$("#nombreCorto").val(txt);
			num=parseInt(txt.length);
			$("#nombreCorto").next("span").html(num);
			}
		if(num>30)
		if(sa.length>0){
			sa=sa.slice(0,1);
			txt=pn+" "+sn+" "+pa+" "+sa;
			txt=txt.toUpperCase();
			$("#nombreCorto").val(txt);
			num=parseInt(txt.length);
			$("#nombreCorto").next("span").html(num);
			}
		}
	});

//ELIMINAR FILA DE LA TABLA BENEFICIARIO


//ELIMINAR FILA DE LA TABLA CONYUGE


});	//fin document.read
