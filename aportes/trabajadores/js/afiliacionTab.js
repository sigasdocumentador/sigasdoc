var URL=src();
var nit="";
var rz="";
var ide=0;
var idp=0;
var estadoActualAfiliacion = null;

function buscarAfiliaciones(idp){
	estadoActualAfiliacion = null;
	$("#div-mostrar").empty();
	$.getJSON('buscarAfiliaciones.php',{idpersona:idp},function(datos){
		if(datos == 0){
			MENSAJE("No hay afiliaciones!");
			limpiarCampos_otro2();
			return false;
		}
		$.each(datos,function(i,f){
			nit=f.nit;
			idf=f.idformulario;
			rz=f.razonsocial;
			estado=f.estado;
			var span = "";
			var clase = "";
			if(estado == 'A'){
				clase='ui-state-highlight';
				span='<span style="float: left; margin-right: 0.3em;" class="ui-icon ui-icon-circle-check" id="span_afiliacion_'+ idf +'"></span>';
			}
			$("#div-mostrar").append(span+'<p align="left" onclick="buscarAfiliacion('+idf+',\''+ estado +'\')" class="'+clase+'"  id="p_afiliacion_'+ idf +'">' + nit +" "+ rz +' (Estado: '+ estado +')</p>');
		});
	});
}

function buscarAfiliacion(idf,estado){
	estadoActualAfiliacion = estado;
	$("#estado").attr("disabled",false);
	$("#codigo").attr("disabled",false);
	$("#div-msje").remove();
	$("table.tablero :input.ui-state-error,select.ui-state-error").removeClass("ui-state-error");//Limpiar la clase de error
	$("#tipAfiliacion").attr("disabled",false);
	$("#tipAfiliacion option[value=18]").show(); // mostrar dependiente en el tipo de afiliación
	$("#tablaAfiTab").show();//con evento one
	$("#div-afiliacion").show();
	if(estado == 'A' || estado == 'P'){
		$.ajax({
			url: 'buscarAfiliacionIda.php',
			type: "POST",
			data: {v0:idf},
			async: false,
			dataType: "json",
			success: function(datos){
				if(datos==0){
					MENSAJE("Lo lamento no encontre su registro!");
					return false;
				}
				$("#actualizarAfiliacion").show();
				$.each(datos,function(i,f){
					$("#tipForm").val(f.tipoformulario);
					$("#tipAfiliacion").attr("disabled",false);
					$("#tipAfiliacion").val(f.tipoafiliacion);
					if(f.tipoafiliacion == 19 || f.tipoafiliacion == 20 || f.tipoafiliacion == 21 || f.tipoafiliacion == 23 || f.tipoafiliacion == 2938){
						$("#tipAfiliacion").attr("disabled",false);
						$("#tipAfiliacion option[value=18]").hide(); // ocultar dependiente en el tipo de afiliación
					}else if(f.tipoafiliacion == 18){
						$("#tipAfiliacion").attr("disabled",true);
					}else{
						$("#tipAfiliacion").attr("disabled",false);
					}
					$("#fecIngreso").val(f.fechaingreso);
					$("#horasDia").val(f.horasdia);
					$("#horasMes").val(f.horasmes);
					$("#tipoPago").val(f.tipopago);
					$("#salario").val(f.salario);
					$("#categoria").val(f.categoria);
					$("#agricola").val(f.agricola);
					$("#cargo").val(f.cargo);
					$("#cboTipo").val(f.primaria);
					$("#estado").val(f.estado);
					$("#idafiliacion").val(f.idformulario);
					idp=f.idpersona;
					ide=f.idempresa;
					if(f.estado=='I'){
						$("#infoRetiro").show();
					}else{
						$("#infoRetiro").hide();
					}
					
					$("#cmbClaseAfiliacion").val(f.claseafiliacion);
				});
			},
			complete: function(){
				$.ajax({
					url: URL+'phpComunes/pdo.b.empresa.id.php', 
					type: "POST",
					data: {v0:ide,v1:1},
					dataType: "json",
					success: function(dato){
						if(dato==0){
							MENSAJE("No se encontro la Empresa!");
							$("#txtNit").val('');
							return false;
						}
						$.each(dato,function(i,f){
							$("#txtNit").val(f.nit);
							$("#tdEmpresa").html(f.razonsocial);
							return;
						});
						$.getJSON(URL+'phpComunes/pdo.buscar.persona.id.php',{v0:idp},function(data){
							nom = data[0].pnombre+" "+data[0].snombre+" "+data[0].papellido+" "+data[0].sapellido;
							$("#tdAfiliado").html(nom);
						});
					}
				});
			}			
		});
	} else if(estado == 'I') {
		$.ajax({
			url: 'buscarAfiliacionInactivaPorFormulario.php',
			type: "POST",
			data: {idformulario:idf},
			async: false,
			dataType: "json",
			success: function(datos){
				if(datos==0){
					MENSAJE("Lo lamento no se encuentra registro!");
					return false;
				}
				$("#actualizarAfiliacion").show();
				$.each(datos,function(i,f){
					$("#tipForm").val(f.tipoformulario);
					$("#tipAfiliacion").attr("disabled",false);
					$("#tipAfiliacion").val(f.tipoafiliacion);
					if(f.tipoafiliacion == 19 || f.tipoafiliacion == 20 || f.tipoafiliacion == 21 || f.tipoafiliacion == 23 || f.tipoafiliacion == 2938){
						$("#tipAfiliacion").attr("disabled",false);
						$("#tipAfiliacion option[value=18]").hide(); // ocultar dependiente en el tipo de afiliación
					}else if(f.tipoafiliacion == 18){
						$("#tipAfiliacion").attr("disabled",true);
					}else{
						$("#tipAfiliacion").attr("disabled",false);
					}
					$("#fecIngreso").val(f.fechaingreso);
					$("#horasDia").val(f.horasdia);
					$("#horasMes").val(f.horasmes);
					$("#tipoPago").val(f.tipopago);
					$("#salario").val(f.salario);
					$("#categoria").val(f.categoria);
					$("#agricola").val(f.agricola);
					$("#cargo").val(f.cargo);
					$("#cboTipo").val(f.primaria);
					$("#estado").val(f.estado);
					$("#idafiliacion").val(f.idformulario);
					$("#datepicker").val(f.fecharetiro);
					idp=f.idpersona;
					ide=f.idempresa;
					$("#infoRetiro").show();
					$("#codigo").val(f.motivoretiro);
					if(f.motivoretiro=="2860"){
						$("#codigo").attr("disabled",true);
					}
					// linea adicionada oparra
					$("#codigo").show();
					$("#estado").attr("disabled",true);
					// fin lineas adicionadas oparra
					
					$("#cmbClaseAfiliacion").val(f.claseafiliacion);
					
					document.getElementById('idEmpresa').value=ide;
				});
			},
			complete: function(){
				$.ajax({
					url: URL+'phpComunes/pdo.b.empresa.id.php', 
					type: "POST",
					data: {v0:ide,v1:1},
					dataType: "json",
					success: function(dato){
						if(dato==0){
							MENSAJE("No se encuentra la Empresa!");
							$("#txtNit").val('');
							return false;
						}
						$.each(dato,function(i,f){
							$("#txtNit").val(f.nit);
							$("#tdEmpresa").html(f.razonsocial);
							return;
						});
						$.getJSON(URL+'phpComunes/pdo.buscar.persona.id.php',{v0:idp},function(data){
							nom = data[0].pnombre+" "+data[0].snombre+" "+data[0].papellido+" "+data[0].sapellido;
							$("#tdAfiliado").html(nom);
						});
					}
				});
			}
		});
	}
	
	document.getElementById('idPersona').value=idp;	
}
	
function validarSubsidio(){
	var afilia=$("#tipAfiliacion").val();
	var formul=$("#tipForm").val();
	if(afilia==0)return;
	if(formul==48 && afilia!=18){
		MENSAJE("Solo tienen derecho a subsidio familiar las afiliaciones de trabajadores DEPENDIENTES!, cambie el tipo de formulario o el tipo de afiliaci&oacute;n");
		$("#tipAfiliacion").val(0);
		$("#tipForm").val(0);
		$("#tipForm").focus();
	}
	
}
	
function buscarEmpresa(){
	
}

function actualizarAfiliacion(){	
	var nit=				$("#txtNit").val();
	var idf=				$("#idafiliacion").val();
	var tipoformulario= 	$("#tipForm").val();
	var tipoafiliacion= 	$("#tipAfiliacion").val();
	var fechaingreso=   	$("#fecIngreso").val();
	var horasdia=	    	$("#horasDia").val();
	var horasmes=       	$("#horasMes").val();
	var tipopago=			$("#tipoPago").val();
	var salario=			$("#salario").val();
	var categoria=			$("#categoria").val();
	var agricola=			$("#agricola").val();
	var cargo=				$("#cargo").val();
	var primaria=			$("#cboTipo").val();
	var estado=				$("#estado").val();
	var idafiliacion=		$("#idafiliacion").val();
	var codigo=				$("#codigo").val();
	var fecharetiro=		$("#datepicker").val();
	var claseafiliacion=	$("#cmbClaseAfiliacion").val();
		
	var error=0;
	$("table.tablero :input.ui-state-error,select.ui-state-error").removeClass("ui-state-error");//Limpiar la clase de error
	//------------------VALIDACIONES----------------------
   	if(tipoformulario=='0'){ 
   		$("#tipForm").addClass("ui-state-error");
   		error++;
   	}
	if(nit.length==0){ 
   		$("#txtNit").addClass("ui-state-error");
   		error++;
   	}
	if(tipoafiliacion=='0'){ 
   		$("#tipAfiliacion").addClass("ui-state-error");
   		error++;
   	}
	if(fechaingreso.length==0){ 
   		$("#fecIngreso").addClass("ui-state-error");
   		error++;
   	}
	if(horasdia.length==0){ 
   		$("#horasDia").addClass("ui-state-error");
   		error++;
   	}
	if(horasmes.length==0){ 
   		$("#horasMes").addClass("ui-state-error");
   		error++;
   	}
	if(tipopago=='0'){ 
   		$("#tipoPago").addClass("ui-state-error");
   		error++;
   	}
	if(salario=='' || isNaN(salario)){ 
   		$("#salario").addClass("ui-state-error");
   		error++;
   	}
	if(categoria.lengt==0){ 
   		$("#categoria").addClass("ui-state-error");
   		error++;
   	}
	if(agricola=='0'){ 
   		$("#agricola").addClass("ui-state-error");
   		error++;
   	}
	if(cargo=='0'){ 
   		$("#cargo").addClass("ui-state-error");
   		error++;
   	}
	if(primaria=='0'){ 
   		$("#cboTipo").addClass("ui-state-error");
   		error++;
   	}
	if(estado=='0'){ 
   		$("#estado").addClass("ui-state-error");
   		error++;
   	}
	if(estado=='I' && codigo=='0'){ 
   		$("#codigo").addClass("ui-state-error");
   		error++;
   	}
	if(estado=='I' && fecharetiro==''){ 
   		$("#datepicker").addClass("ui-state-error");
   		error++;
   	}
	if(error>0){
		MENSAJE("Llene los campos obligatorios!");
		return false; 
	}
	
	/*
	 * Lineas adicionadas del dia 8 de Junio de 2016 oparra
	 * Visualizar alerta para los casos de inactivacion por fallecimiento  2860
	 */
	if(codigo==2860){
	   if(!confirm("El motivo de inactivacion del afiliado es por Muerte del Tabajador, esto hara que el estado de la persona se actualice a M (Muerto) y desactive todas las afiliaciones activas que tenga. Esta seguro de Continuar?")){
		 return false;   
	   }
    }
    // fin lineas 8 de Junio de 2016
	
	
	$.getJSON('actualizarAfiliacion.php',{v0:idafiliacion,v1:tipoformulario,v2:tipoafiliacion,v3:ide,v4:fechaingreso,v5:horasdia,v6:horasmes,v7:salario,v8:agricola,v9:cargo,v10:primaria,v11:estado,v12:tipopago,v13:categoria,v14:codigo,v15:idp,v16:fecharetiro,v17:claseafiliacion,estadoActual: estadoActualAfiliacion},function(datos){
		if(datos==0){
			MENSAJE("El registro NO fue actualizado!");
		}else if (datos==2) {
			alert("La afiliacion fue inactivada, pas\xf3 al hist\xf3rico.");
			estadoActualAfiliacion = 'I';
			limpiarCampos_otro2();
			$("#div-msje").remove();
			buscarAfiliaciones(idp);
			observacionesTab(idp,1);
		}else{
			alert("Registro actualizado!");
			limpiarCampos_otro2();
			observacionesTab(idp,1);
			$("#div-msje").remove();
			buscarAfiliaciones(idp);
		}
	});
}	

function limpiarCampos_otro2(){
	$("#actualizarAfiliacion").hide();
	$("#tdAfiliado").html("");
	$("#datepicker").val("");
	$('#tdAfiliado').val('');
	$("#txtNit").val('');
	$('#tdEmpresa').html("");
	$("#tipForm").val(0);
	$("#tipAfiliacion").val(0);
	$("#fecIngreso").val('');
	$("#horasDia").val('');
	$("#horasMes").val('');
	$("#tipoPago").val(0);
	$("#salario").val('');
	$("#categoria").val('');
	$("#agricola").val(0);
	$("#cargo").val(0);
	$("#cboTipo").val(0);
	$("#estado").val(0).trigger("change");
}

function calcularCategoria(sal){
	var salario=parseInt(sal);
	var smlv=$('#txtSMLV').val();
	smlv=(isNaN(smlv) && smlv>0)?616000:smlv;
	var sm=parseFloat(salario/smlv);
	
	if(sm<=2){
		return 'A';
	} else if(sm>2 && sm<=4){
		return 'B';
	} else if(sm>4){
		return 'C';
	} 
}	

function onChangeClaseAfiliacion(obj){
	var opt = obj.value;	
	if(opt=="0" || opt=="1"){
        $("#cargo").attr("disabled",false);    
    } else if(opt=="2") {
    	$("#cargo").val("3906"); 
    	$("#cargo").attr("disabled",true);
    } 
}