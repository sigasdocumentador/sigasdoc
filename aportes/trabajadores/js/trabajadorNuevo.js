var idPersona=0;
var idnp=0;
var idempresa=0;
var existe=0;
var existenDatos=0;
var idnb=0;
empresa=false;
datos=false;
afiliacion=false;
var URL=src();
var arrayFinalC=[];
var arrayFinalB=[];
var arrayFinalCert=[];
var arrayActual=[];
var idAfiliacion=0;
var conClick=0;
var idTarjeta=0;
var ruta=false;
var idtrelacion=0;
var chequeado1=1;
var chequeado2=1;

function nuevoR(){
	nuevo=1;
	$("#bGuardar").show();
	conClick=0;
	limpiarCampos();
	$("#errores").empty();
	$("div#tabs ul li a[href=#tabs-1]").trigger('click');
	msg="";
	var campo0="28,2926";
    $('#pendientes option').remove();
	$.post(URL+'phpComunes/buscarRadicaTipoID.php',{v0:campo0},function(datos){
    	var cmbRadicacion = $("#pendientes");
	   	$(cmbRadicacion).html(datos);
	   	if($(cmbRadicacion).children().length==1){
			alert('Lo lamento NO hay radicaciones pendientes de grabar!');
			return false;
         }
	});
	$("#email2").val('');
	$("#pendientes").focus();
	$("#cedMama").html(''); 
	$("#cedMama").append("<option value='0'>Seleccione</option>");
	$("#cedMama").append("<option value='0'>HIJO NATURAL</option>");
}

function calcularCategoria(sal){
    var salario=parseInt(sal);
    if(isNaN(salario)) return;
    var sm=parseInt(salario/ 536000);
    if(sm<=2){
        $('#categoria').val('A');
    }
    if(sm>2 && sm<=4){
        $('#categoria').val('B');
    }
    if(sm>4){
        $('#categoria').val('C');
    }
}

function buscarRadicacion(){
    var campo0 =$('#pendientes').val();
    $.getJSON(URL+'phpComunes/buscarRadicacion2.php', {v0:campo0}, function(data){
		if(data==0){
			alert("No se pudo encontrar toda la informacion de la radicacion!, si es una afiliacion Independiente, guarde primero la empresa o comuniquese con el administrador del sistema");
			return false;
			}
        $.each(data,function(i,fila){
            $('#radicacion').val(fila.idradicacion);
            $('#textfield').val(fila.fecharadicacion);
            $('#nitActual').val(fila.nit);
			$('#razonSocial').val(fila.razonsocial);
			$('#txtEmpresa').val(fila.razonsocial);
            $('#identificacion').val(fila.numero);
            $('#tipForm').val(fila.idtipoformulario);
            $('#tipoDoc').val(fila.idtipodocumentoafiliado);
            //$('#fecIngreso').val(fila.fechaingreso);
            $('#horasDia').val(8);
            $("#horasMes").val(240);
			
		    idempresa=fila.idempresa;
            //llenarCampos();
            return false;
        });
		
		$.getJSON('buscarAfiliacionIdr.php', {v0:campo0}, function(datos){
			if(datos==0){
				alert("No hay una afiliacion para esta radicacion!");
				return false;
				}
			$.each(datos,function(i,fila){
				$('#txtIdAfilia').val(fila.idformulario);
				$("#salario").val(fila.salario);
				$('#fecIngreso').val(fila.fechaingreso);
			});
			})
    });
	$("#salario").focus().blur();
    document.forms[0].nitActual.focus();
}

function preliminares(){
	//buscar todo existente
	datosPersona();
	if(idAfiliacion==1)return false;
	idPersona=$("#idPersona").val();
	if(existenDatos==1){
		$.getJSON(URL+'phpComunes/buscarTarjetaIDT.php', {v0:idPersona}, function(datos){
			if(datos==0){
				idTarjeta=0;
				$("#tarjeta").val("Sin bono");
				return false;
			}
				$.each(datos,function(i,fila){
					$("#tarjeta").val(fila.bono);
					idTarjeta=fila.idtarjeta;
					return;
				});
		});
	}
	//$("#identificacion").focus();
	//document.getElementById('identificacion').focus();
	
}

function urlImagenes(){
	
	var fecha=$("#fecNacHide").val();
	var num =$("#identificacion").val();
	var mes=fecha.substring(0,2);
	var dia=fecha.substring(3,5);
	var anio=fecha.substring(6,10);
	var ruta ='digitalizacion/afiliados/'+anio+'/'+mes+'/'+dia+'/'+num;
	$.ajax({
		url: URL+'phpComunes/crearDirectorio.php',
		type: 'POST',
		async: false,
		data: {v0:ruta},
		success: function(datos){
			if(datos==0){
				MENSAJE("El directorio de las imagenes no se pudo crear,<br> informe al administrdor del sistema");
				$("#rutaDoc").val('');
				return false;
			}
			$("#rutaDoc").val(datos);
		}
	});
	
}

function guardarAfiliacion(){	
	$("#bGuardar").hide();
	idPersona=$("#idPersona").val();
	msg="";
	var continuar=0;
	$("#errores ul").remove();
	errorTab=errorE+" "+errorD+" "+errorA; 
	if(empresa==true&&datos==true&&afiliacion==true){
    //var sucursal=$("#comboSucursal").val(); 
    urlImagenes();
    if(existenDatos==0){
    					//     tipoDoc,         cedula,                    pNombre,            pApellido,             sNombre,           sApellido,				sexo,				direccion,			cboBarrio,			telefono,				celular,			email,			tipVivienda,			deptRes,			ciudadRes,               cboZona,			estadoCivil,		     fecNac,				depNac,					ciudadNac,			capTrabajo,			profesion,				nomCorto	
    var idPersona=grabarPersona($("#tipoDoc").val(),$("#identificacion").val(),$("#pNombre").val(),$("#pApellido").val(),$("#sNombre").val(),$("#sApellido").val(),$("#sexo").val(),$("#tDireccion").val(),$("#cboBarrio").val(),$("#telefono").val(),$("#celular").val(),$("#email").val(),$("#tipoVivienda").val(),$("#cboDepto").val(),$("#cboCiudad").val(),$("#cboZona").val(),$("#estadoCivil").val(),$("#fecNacHide").val(),$("#cboDepto2").val(),$("#cboCiudad2").val(),$("#capTrabajo").val(),$("#profesion").val(),$("#nombreCorto").val());
    idPersona=parseInt(idPersona);
    if(idPersona<=0){
		$("#bGuardar").show();
    	switch (idPersona){
    	case -239 : msg="El n&uacute;mero de identificacion y tipo de documento ya existe!";
    	}
		msg+="Los datos de la persona no fueron guardados! error: "+idPersona;
		MENSAJE(msg);
		return false;
    }
    if(isNaN(idPersona)){
    	alert("No se puedo guardar el registro NaN, error desconocido");
    	return false;
    }
    msg="Se guardaron los datos de la persona bajo el Id; " +idPersona;
    $("#idPersona").val(idPersona);
    }
    else{
    	x=actualizarPersona();
    	if(x==1){
    		msg+="<br>Se actualiz&oacute; la informaci&oacute;n de la persona";
    	}
    	else{
    		msg+="<br>NO se actualiz&oacute; la informaci&oacute;n de la persona";
    	}
	}
    //AFILIACION
    if(idPersona==undefined ){
    	alert('No hay id Persona!, no se puede continuar');
    	return false;
    }
    var tipForm=$("#tipForm").val();
    var tipAfiliacion=$("#tipAfiliacion").val();
    var fecIngreso=$("#fecIngreso").val();
    var horasDia=$("#horasDia").val();
    var horasMes=$("#horasMes").val();
    var tipoPago=$("#tipoPago").val();
    var salario=$("#salario").val();
    var categoria=$("#categoria").val();//NO OBLIGADO
    var agricola=$("#agricola").val();
    var cargo=$("#cargo").val();
    var estado=$("#estado").val();
    var traslado=$("#traslado").val();
    var codCaja=$("#codCaja").val();	 //OBLIGADO VARIA
	var idafiliacion=$("#txtIdAfilia").val();
    //BENEFICIARIOS - CONYUGES-CERTIFICADOS
    arrayFinalB;
    arrayFinalC; 
    arrayFinalCert;
    $.ajax({
    	url: "actualizarAfiliacion.php",
    	type: "POST",
    	async: false,
    	data : {v0:idafiliacion,v1:tipForm,v2:tipAfiliacion,v3:idempresa,v4:fecIngreso,v5:horasDia,v6:horasMes,v7:salario,v8:agricola,v9:cargo,v10:'S',v11:'A',v12:tipoPago,v13:categoria},
    	success: function(datos){
    		if(datos<=0){
    			msg="<br>No se pudo actualizar la afiliaci&oacute;n! Error: "+datos;
    			continuar=1;
				
    			MENSAJE(msg);
    			return false;
    		}
    		else{
    			msg+="<br>Se creo la afiliacion";
				var num=$("#identificacion").val();
				var l=num.length;
				for(k=l;k<12;k++){
					var d=digito(num);
					if(k==l){
						d--;
						d=parseInt(d);
						if(d<0){
							d=9;
							}
						}
					num=num+d;
				}
				/*
				var precodigo=num;
				var dig=digito(num);
				var barra=num+dig;
				if(idTarjeta==0){
				$.ajax({
					url: URL+"phpComunes/guardarTarjeta.php",
					type: "POST",
					async: false,
					data: "submit =&v1=" + idPersona + "&v2=" + precodigo + "&v3=" + barra + "&v4="+'S' + "&v5=''",
					success: function(datos){
						if(datos==0){
							MENSAJE("No se pudo guardar la tarjeta, la afiliacion NO SE GUARDO!");
							$.getJSON('borrarPersona.php',{v0:idPersona},function(dato){
								$.getJSON('borrarAfiliacion.php', {v0:idAfiliacion}, function(dato2){
									
								});
							});
							return false;
						}
						else{
							msg+="<br>La tarjeta fue creada y asignada";
						}
					}
				});//ajax
				
				} */
    		}
    	}
    });
    if(continuar==1){return false}
//GRABAR CONYUGE
    for(i=0;i<arrayFinalC.length;i++){
    	if(arrayFinalC[i][27]==1){
    		msg+="<br>La relacion ya existe";
    		continue;
    	}
    	if(arrayFinalC[i][0]==0){
    		var nc=nomCorto(arrayFinalC[i][5],arrayFinalC[i][6],arrayFinalC[i][7],arrayFinalC[i][8]); 
   		    //0-tabla,1-tipRel,2-conviven,3-tipDoc,  4-cedula,5-pNombre,6-pApellido,7-sNombre,8-sApellido,9-sexo,10-direccion,11-cboBarrio,12-telefono,13-celular,14-email,15-tipVivienda,16-deptRes,17-ciudadRes,18-cboZona,19-estadoCivil,20-fecNac,21-deptNac,22-ciudadNac,23-capTrabajo,24-profesion,25-registroMatrimonio,26-fotocopiaCed,27 exiteRelacion
   		    //                              tipDoc,  cedula,    pNombre,  pApellido,  sNombre,  sApellido,  sexo,   direccion,   cboBarrio,   telefono,   celular,   email,   tipVivienda,   deptRes,   ciudadRes,   cboZona,   estadoCivil,   fecNac,   deptNac,   ciudadNac,   capTrabajo,   profesion,nomCorto
   	        var idnb=grabarPersona(arrayFinalC[i][3],arrayFinalC[i][4],arrayFinalC[i][5],arrayFinalC[i][6],arrayFinalC[i][7],arrayFinalC[i][8],arrayFinalC[i][9],arrayFinalC[i][10],arrayFinalC[i][11],arrayFinalC[i][12],arrayFinalC[i][13],arrayFinalC[i][14],arrayFinalC[i][15],arrayFinalC[i][16],arrayFinalC[i][17],arrayFinalC[i][18],arrayFinalC[i][19],arrayFinalC[i][20],arrayFinalC[i][21],arrayFinalC[i][22],arrayFinalC[i][23],arrayFinalC[i][24],nc );
   		    if(idnb<=0){
   		    	alert("No se pudo grabar el conyugue "+nc+" debe intentar grabar el conyugue por la opci/00uf3n ACTUALIZACI/00uf3N." + idnb);
    	        }//end if
   		    else{
   		    	msg+="<br>Se creo el conyuge: "+nc;
   		    	var flag=guardarRelacion(idPersona,idnb,34,idnb,arrayFinalC[i][2],arrayFinalC[i][1]);
   		    	if(flag==0){
   		    		alert("No se pudo crear la relacion de convivencia con " + nc + ", intente crearla por actualizacion de datos");
   		    	}
   		    	else{
   		    		msg+="<br>Se creo la relacion de conyue con " + nc;
   		    		for(l=0;l<arrayFinalB.length;l++){
   		    			if(arrayFinalB[l][10]==arrayFinalC[i][4]){
   		    				arrayFinalB[l][16]=idnb;
   		 				}
   		    		}
   		    	}
   		    }
    	}
    	else{
    		for(j=0;j<arrayFinalB.length;j++){
				if(arrayFinalB[j][10]==arrayFinalC[i][4]){
					arrayFinalB[j][16]=arrayFinalC[i][0];
				}
			}
    	}
    }
//GRABAR BENEFICIARIO
    for(i=0;i<arrayFinalB.length;i++){
    	var parent = arrayFinalB[i][9];
    	var idpadre= arrayFinalB[i][16];
    	if(idpadre==undefined){
    		idpadre=0;
    	}
    	var giro='N';
    	if(arrayFinalB[i][11]==48){
    		giro='S';
    	}
    	if(arrayFinalB[i][0]==0){
    		var nc=nomCorto(arrayFinalB[i][3],arrayFinalB[i][4],arrayFinalB[i][5],arrayFinalB[i][6]);
   		    
   	        var idnb=grabarPersona(arrayFinalB[i][1],arrayFinalB[i][2],arrayFinalB[i][3],arrayFinalB[i][4],arrayFinalB[i][5],arrayFinalB[i][6],arrayFinalB[i][7],arrayFinalB[i][0],arrayFinalB[i][0],arrayFinalB[i][0],arrayFinalB[i][0],arrayFinalB[i][0],arrayFinalB[i][0],arrayFinalB[i][0],arrayFinalB[i][0],arrayFinalB[i][0],arrayFinalB[i][15],arrayFinalB[i][8],arrayFinalB[i][13],arrayFinalB[i][14],arrayFinalB[i][12],arrayFinalB[i][0],nc);
   		    if(idnb<=0){
   		    	alert("No se pudo grabar el beneficiario "+nc+" debe intentar grabarlo por la opci/00uf3n ACTUALIZACI/00uf3N." + idnb);
    	        }//end if
   		    else{
   		    	msg+="<br>Beneficiario creado "+nc;
   		    						//idtrabajador, idbeneficiario, idparentesco, idconyuge, giro,		
   		    	var flag=guardarRelacionBene(idPersona,idnb,parent,idpadre,giro,arrayFinalB[i][17]);
   		    	if(flag<=0){
   		    		alert("No se pudo crear la relacion de beneficiario con: " + nc + ", intente crearla por actualizacion de datos");
   		    	}
   		    	else{
   		    		msg+="<br>Creada la relacion con el beneficiario";
   		    		for(k=0;k<arrayFinalCert.length;k++){
   		    			if(arrayFinalCert[k][0]==arrayFinalB[i][2]){
   		    				arrayFinalCert[k][0]=idnb;
   		    			}
   		    	    }
   		    	}
   		    }
    	}
    	else{
    		msg+="<br>La persona "+nc+" ya existe en nuestra base!\n\r";
			var flag=guardarRelacionBene(idPersona,idnb,parent,idpadre,giro,arrayFinalB[i][17]);
   		    if(flag<=0){
   		   		alert("No se pudo crear la relacion de beneficiario con: " + nc + ", intente crearla por actualizacion de datos");
   		   	}
   		   	else{
   		   		msg+="<br>Creada la relacion con el beneficiario";
   	    		for(k=0;k<arrayFinalCert.length;k++){
   	    			if(arrayFinalCert[k][0]==arrayFinalB[i][2]){
						arrayFinalCert[k][0]=idnb;
   		    		}
   		    	   }
   		    }
    	}
    }
//grabar certificados
    //idBeneficiario,55 cEscolaridad,fEscolaridad,56 cUniversidad,fUniversidad,57 cSupervivencia,fSupervivencia,58 cDiscapacidad,fDiscapacidad,parentesco
    for(i=0;i<arrayFinalCert.length;i++){
		var con=0;
    	if(arrayFinalCert[i][1]=='S'){
    		var codcer=55;
			con++;
    	}
    	if(arrayFinalCert[i][3]=='S'){
    		var codcer=56;
			con++;
    	}
    	if(arrayFinalCert[i][5]=='S'){
    		var codcer=57;
			con++;
    	}
    	if(arrayFinalCert[i][7]=='S'){
    		var codcer=58;
			con++;
    	}
		if(con>0){
    	var idb=arrayFinalCert[i][0];
    	var idp=arrayFinalCert[i][9];
    	$.ajax({
    		url: URL+'aportes/radicacion/grabarCertificado.php',
    		type: "POST",
    		async: false,
    		data: "submit &v0="+idb+"&v1="+idp+"&v2="+codcer+"&v3=P",
    		success: function(datos){
    			var cad="<br>"+datos+" idbeneficiario: "+arrayFinalCert[i][0];
    			msg+=cad; 
    		}
    	});
		}//if con
    }
    cerrarRadicacion(msg);
    limpiarCampos();
	}
else {
    alert("HAY ALGUNOS DATOS INCOMPLETOS, REVISAR EN LOS TAB: "+errorTab);
	}  // }//if end		
	}//fin grabar

function actualizarPersona(){
	idPersona=$("#idPersona").val();
	var v1= $("#tipoDoc").val();			//idtipodocumento
	var v2= $("#identificacion").val();			//identificacion
	var v3=	$("#pApellido").val(); 		//papellido
	var v4=	$("#sApellido").val();		//sapellido
	var v5=	$("#pNombre").val();			//pnombre
	var v6=	$("#sNombre").val();			//snombre
	var v7=	$("#sexo").val();			//sexo
	var v8=	$("#tDireccion").val();		//direccion
	var v9=	$("#cboBarrio").val();			//idbarrio
	var v10= $("#telefono").val();		//telefono
	var v11= $("#celular").val();		//celular
	var v12= $("#email").val();			//email
	var v13= $("#txtCasa").val();			//idpropiedadvivienda
	var v14= $("#txtTipoV").val();			//idtipovivienda
	var v15= $("#cboDepto").val();			//iddepresidencia
	var v16= $("#cboCiudad").val();		//idciuresidencia
	var v17= $("#cboZona").val();			//idzona
	var v18= $("#estadoCivil").val();			//idestadocivil
	var v19= $("#fecNacHide").val();			//fechanacimiento
	var v20= $("#cboDepto2").val();			//iddepnace
	var v21= $("#cboCiudad2").val();		//idciunace
	var v22= $("#capTrabajo").val();		//capacidadtrabajo
	var v23= $("#nombreCorto").val();     //nombre corto
	var v24= $("#profesion").val();      //profesion
	var v25=$("#rutaDoc").val();         //ruta documentos
	var x=0;
	$.ajax({
		url:URL+'phpComunes/actualizarPersona.php',
		type:"POST",
		async:false,
		data:{v0:idPersona,v1:v1,v2:v2,v3:v3,v4:v4,v5:v5,v6:v6,v7:v7,v8:v8,v9:v9,v10:v10,v11:v11,v12:v12,v13:v13,v14:v14,v15:v15,v16:v16,v17:v17,v18:v18,v19:v19,v20:v20,v21:v21,v22:v22,v23:v23,v24:v24,v25:v25},
		success:function(data){
		if(data==0){
			x=0;
			}
		else{
			x=1;
			}
		}
		});
	

	return x;
}	

function grabarPersona(tipoDoc,cedula,pNombre,pApellido,sNombre,sApellido,sexo,direccion,cboBarrio,telefono,celular,email,tipVivienda,deptRes,ciudadRes,cboZona,estadoCivil,fecNac,depNac,ciudadNac,capTrabajo,profesion,nomCorto){
	var x=0;  
	$.ajax({
		url: 'insertPersona.php',
		type: "POST",
		async:false,
		//1 idtipodocumento,2 identificacion,            3 papellido,      4 sapellido,     5 pnombre,     6 snombre,     7 sexo,     8 direccion,     9 idbarrio,      10 telefono,     11 celular,     12 email,     13 idtipovivienda,  14 iddepresidencia,15 idciuresidencia,16 idzona,  17 idestadocivil,   18 fechanacimiento, 19 idciunace,20 iddepnace,21 capacidadtrabajo, 22 idprofesion,   23 nombrecorto,24 usuario
		data: "submit=&v0=0&v1="+tipoDoc+"&v2="+cedula+"&v3="+pApellido+"&v4="+sApellido+"&v5="+pNombre+"&v6="+sNombre+"&v7="+sexo+"&v8="+direccion+"&v9="+cboBarrio+"&v10="+telefono+"&v11="+celular+"&v12="+email+"&v13="+tipVivienda+"&v14="+deptRes+"&v15="+ciudadRes+"&v16="+cboZona+"&v17="+estadoCivil+"&v18="+fecNac+"&v19="+ciudadNac+"&v20="+depNac+"&v21="+capTrabajo+"&v22="+profesion+"&v23="+nomCorto,
		success: function(datos){
			x=datos;
			$("#idPersona").val(datos);
		}
	  });
	  return x;
}

function guardarRelacionBene(idtrabajador, idbeneficiario, idparentesco, idconyuge, giro,fasigna){
	var campo0 = idtrabajador;
	var campo1 = idbeneficiario;
	var campo2 = idparentesco;
	var campo3 = idconyuge;
	var campo4 = giro;
	var campo5 = fasigna;
	
	var x=0;
	$.ajax({
		url: URL+'phpComunes/guardarBeneficiario.php',
		type: "POST",
		async: false,
		data: "submit &v0="+campo0+"&v1="+campo1+"&v2="+campo2+"&v3="+campo3+"&v4="+campo4+"&v5="+campo5,
		success: function(datos){
			x=datos;
		}
	});
	return x;
	
}
function guardarRelacion(idT,idB,idTp,idC,conviven,tipRel){
	var campo0 = idT;
	var campo1 = idB;
	var campo2 = idTp;
	var campo3 = idC;
	var campo4 = conviven;
	var campo5 = tipRel;
	var x=0;
	$.ajax({
		url: URL+'phpComunes/guardarConyuge.php',
		type: "POST",
		async: false,
		data: "submit &v0="+campo0+"&v1="+campo1+"&v2="+campo2+"&v3="+campo3+"&v4="+campo4+"&v5="+campo5,
		success: function(datos){
			x=datos;
		}
	});
	return x;
	}

function limpiarCampos(){
	$("table.tablero input:text").val('');
	$("table.tablero select").val('Seleccione..');
	$("#tablaB tbody tr").remove();
	$("#tablaC tbody tr").remove();
	arrayFinalC=[];
	arrayFinalB=[];
}

function verificarC(){
	$("#conyuge ul").remove();
	lista="<ul class='Rojo'>";
	var tipRel=$("#tipRel").val();
	var txtRelacion=$("#tipRel").children("option:selected").html();
	var conviven=$("#conviven").val();
	var txtConvive=$("#conviven").children("option:selected").html();
	var tipoDoc2=$("#tipoDoc2").val();
	var optionSel=$("#tipoDoc2").val();		//children("option:selected").html();//valor del texto del option
	var cedula2=$("#identificacion2").val();
	var pNombre2=$("#pNombre2").val();
	var pApellido2=$("#pApellido2").val();
	var sNombre2=$("#sNombre2").val();//no obligado
	var sApellido2=$("#sApellido2").val();//no obligado
	var sexo2=$("#sexo2").val();
	var barrio2=$("#cboBarrioC").val();
	var direccion2=$("#direccion2").val();
	var telefono2=$("#telefono2").val();
	var celular2=$("#celular2").val();
	var email2=/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test($("#email2").val());
	var tipVivienda2=$("#tipoVivienda2").val();
	var cboDeptoC=$("#cboDeptoC").val();
	var cboCiudadC =$("#cboCiudadC").val();
	var zonaRes2=$("#cboZonaC").val();
	var estadoCivil2=$("#estadoCivil2").val();
	var cboDeptoC2=$("#cboDeptoC2").val();//no obligado
	var cboCiudadC2=$("#cboCiudadC2").val();//no obligado
	var capTrabajo2=$("#capTrabajo2").val();//no obligado
	var profesion2=$("#profesion2").val();//no obligado
	var fecNac2=$("#fecNac2").val();
	$("#cMatrimonio").attr("value",'N');
	$("#cFotocopia").attr("value",'N');
	if(tipRel=='0'){lista+='<li>Seleccione un tipo de relacion.</li>';}
	//------------------------CONVIVEN----------------------
	if(conviven=='0'){lista+='<li>Seleccione el tipo de convivencia.</li>';}
	//---------------------TIPO DOC----------------------  
	if(tipoDoc2=='0'){lista+='<li>Seleccione un tipo de documento en CONYUGE.</li>';}
	//--------------------CEDULA-----------------------  
	/*if($("#conviven").val()=="S"){ 
	if(tipRel==2147 && estadoCivil2!=51){lista+='<li>No concuerda el tipo de relacion con el estado civil!.</li>';}
	if(tipRel==2148 && estadoCivil2!=52){lista+='<li>No concuerda el tipo de relacion con el estado civil!.</li>';}
	}*/
	if(cedula2==''){
		lista+='<li>Ingrese la identificaci\u00F3n del CONYUGE.</li>';
		} else {
	    if(cedula2.length<3){
	    	lista+='<li>La identificaci\u00F3n NO debe tener 2 d\u00EDgitos.</li>';
	    }else {
		if(cedula2.length==9){
			lista+='<li>La identificaci\u00F3n NO debe tener 9 d\u00EDgitos.</li>';
			}
		}//end else
		}//end else
	//----------------PRIMER NOMBRE Y APELLIDO---------------------------  
	if(pNombre2==''){lista+='<li>Ingrese el NOMBRE del CONYUGE.</li>';}
	if(pApellido2==''){lista+='<li>Ingrese el APELLIDO del CONYUGE.</li>';}
	//-------------------SEXO-----------------------
	if(sexo2=='0'){lista+='<li>Seleccione el sexo del CONYUGE.</li>';}
	//-------------------TELEFONO----------------------
	if(telefono2!=''){
		if(telefono2.length<7){
			lista+='<li>Faltan d\u00EDgitos en el  TELEFONO.</li>';
		}
	}
	//---------------------EMAIL------------------------
	/*if($("#email2").val()!=''){
		if(!email2) {
			lista+='<li>El E-mail aparentemente es incorrecto.</li>';	
		}else{
			email2=$("#email2").val();
		}
	}*/
	//------------------------------------------------------
	/*if(deptRes2=='Seleccione..'){lista+='<li>Seleccione el Departamento de residencia en CONYUGE.</li>';}
	if(ciudadRes2=='Seleccione..'){lista+='<li>Seleccione la Ciudad de residencia en CONYUGE.</li>';}
	if(zonaRes2=='Seleccione..'){lista+='<li>Seleccione la zona de residencia en CONYUGE.</li>';}*/
	//----------------------FECHA DE NACIMIENTO---------------
	if(fecNac2==''||fecNac2=='mmddaaaa'){lista+='<li>Escriba la fecha de nacimiento del CONYUGE.</li>';	}
	//---------------DOCUMENTOS A SOLICITAR
	/*if($("#cFotocopia").attr("checked")==false){
	lista+='<li>Seleccione la fotocopia de la c\u00E9dula .</li>';
	}else{
	$("#cFotocopia").attr("value",'S');
	}*/
	//..MOSTRAR VALIDACIONES..//
	$(lista+"</ul>").prependTo($("#conyuge"));
	$("#conyuge ul.Rojo").hide().fadeIn("slow");
	if($("#conyuge ul.Rojo").is(":empty")){				 
		//limpiarCampos();
		anioActual=new Date();
		hoy=anioActual.getFullYear();
		var edadC=parseInt(hoy)- parseInt(anio);				  
	//cargar los datos en la tabla conyuge
	if(cedula2==$("#tablaC td:eq(1)").text()){
		alert("El Conyuge con c\u00E9dula '"+cedula2+"' ya esta agregado");
	}else{
		//Verificar en cada fila si existe un conyuge con convivencia
		estado=0;
		$("#tablaC tbody tr").each(function(i){
		    con=$(this).children("td:eq(4)").text().slice(0,1);
			if(conviven=="S"){
			    alert("Ya existe una RELACION con estado de convivencia activa.");
			    estado=1;
		    }
		});
		
		if(estado==0){
		$("#tablaC tbody").append("<tr><td>"+optionSel+"</td><td>"+cedula2+"</td><td>"+pNombre2.toUpperCase()+" "+sNombre2.toUpperCase()+" "+pApellido2.toUpperCase()+" "+sApellido2.toUpperCase()+"</td><td>"+txtRelacion+"</td><td>"+txtConvive+"</td><td align='center'><img src='../../imagenes/menu/ico_error.png' width='16' height='16' alt='Eliminar' /></td></tr>");
		/*  "<img src='../../imagenes/menu/grabar.png' width='16' height='16' alt='Modificar' />*/					  
	//aqui se agrega la cedula de la conyuge en el fomrulario de NUEVO BENEFICIARIO
	$("#beneficiarios table.tablero td #cedMama").append("<option value='"+cedula2+"'>"+cedula2+"</option>");
	//LLama la funcion que genera el array	con valores de conyuge		  
	generarArrayC(idnb,tipRel,conviven,tipoDoc2,cedula2,pNombre2,sNombre2,pApellido2,sApellido2,sexo2,direccion2,barrio2,telefono2,celular2,email2,tipVivienda2,cboDeptoC,cboCiudadC,zonaRes2,estadoCivil2,$("#fecNacHide2").attr("value"),cboDeptoC,cboCiudadC,capTrabajo2,profesion2,$("#cMatrimonio").attr("value"),$("#cFotocopia").attr("value"),idtrelacion);
		}//estado
	}			
	}
	//ELIMINAR FILA DE LA TABLA CONYUGE	
	$("#tablaC td:contains('"+cedula2+"')~td img[alt='Eliminar']").click(function(){
		if(confirm("Esta seguro de ELIMINAR este CONYUGE?")){
			$(this).parents("tr").find("td:contains('"+cedula2+"')").parent("tr").remove();
			$("#tablaB").find("td:contains('"+cedula2+"')").parent("tr").remove();
			$("#beneficiarios table.tablero td #cedMama option:contains('"+cedula2+"')").remove();
			//Borrar elemento del Array conyuge
			for(i=0;i<arrayFinalC.length;++i){
				if(cedula2==arrayFinalC[i][4]){
					arrayFinalC.splice(i,1);
				}
			}//end for 
			//Borrar elemento del Array Beneficiarios
			for(i=0;i<arrayFinalB.length;++i){
				if(cedula2==arrayFinalB[i][10]){//[2] pero es  [10] por q compara la relacion y no la cedula del beneficiario
					arrayFinalB.splice(i,1);
					arrayFinalCert.splice(i,1);//eliminar de una vez el certificado de ese beneficiario
				}
			}//end for  
		}//end confirm
	});//end click eliminar
	}//end funciton veriicar CONYUGE

function verificarB(){		  
	$("#beneficiarios ul").remove();
	$("table[name='dinamicTable']").remove();
	lista="<ul class='Rojo'>";
			
	//...BENEFICIARIOS
	var parentesco=$("#parentesco").val();
	var cedMama=$("#cedMama").val();//no obligado
	var tipoDoc3=$("#tipoDoc3").val();

	var option2Sel=$("#tipoDoc3").val();	//children("option:selected").html();//valor del texto del option TIPO DOC
	var option3Sel=$("#parentesco").val();	//children("option:selected").html();//valor del texto del option PARENTESCO
	var optionTipoAf=$('#tipoAfiliacion3').val();	//children("option:selected").html();//VALOR DEL OPTION DE TIPO AFILIACION	
	
	var cedula3=$("#identificacion3").val();
	var pNombre3=$("#pNombre3").val();
	var pApellido3=$("#pApellido3").val();
	var sNombre3=$("#sNombre3").val();//no obligado
	var sApellido3=$("#sApellido3").val();//no obligado
	var sexo3=$("#sexo3").val();
	var fecNac3=$("#fecNac3").val();
	var fAsignacion=$("#fAsignacion").val();

	var tipoAfiliacion3=$('#tipoAfiliacion3').val();
	var capacidad=$('#capacidad').val();
	var cboDeptoB=$('#cboDeptoB').val();
	var cboCiudadB=$("#cboCiudadB").val();
	var estadoCivil3=$("#estadoCivil3").val();
	var fEscolaridad=$("#fEscolaridad").val();
	var fUniversidad=$("#fUniversidad").val();
	var fDiscapacidad=$("#fDiscapacidad").val();
	var fSupervivencia=$("#fSupervivencia").val();	
	if(cedMama=='undefined'){
		cedMama=0;
	}
	//VALORES DE CERTIFICADO
	$("#cEscolaridad,#cUniversidad,#cSupervivencia,#cDiscapacidad").attr("value","N");

//-------------------PARENTEZCO-----------------------
	if(parentesco==0){
		lista+='<li>Seleccione el parentesco.</li>';
		afiliacion=false;	
	}
	//---------------------TIPO DOC----------------------  
	if(tipoDoc3==0){lista+='<li>Seleccione un tipo de documento en BENEFICIARIO.</li>';}
	//--------------------CEDULA-----------------------   
	if(cedula3==''){
		lista+='<li>Ingrese la identificaci\u00F3n del BENEFICIARIO.</li>';
	} else {
		if(cedula3.length<3){
			lista+='<li>La identificaci\u00F3n NO debe tener 2 d\u00EDgitos.</li>';
		}else {
			if(cedula3.length==9){
				lista+='<li>La identificaci\u00F3n NO debe tener 9 d\u00EDgitos.</li>';
			}
		}//end else
	}//end else
	//----------------PRIMER NOMBRE Y APELLIDO---------------------------  
	if(pNombre3==''){lista+='<li>Ingrese el NOMBRE del BENEFICIARIO.</li>';}
	if(pApellido3==''){lista+='<li>Ingrese el APELLIDO del BENEFICIARIO.</li>';}
	//-------------------SEXO-----------------------
	if(sexo3=='0'){lista+='<li>Seleccione el sexo del BENEFICIARIO.</li>';}
	//----------------------FECHA DE NACIMIENTO 3---------------
	if(fecNac3==''||fecNac3=='mmddaaaa'){lista+='<li>Escriba la fecha de nacimiento del BENEFICIARIO.</li>';afiliacion=false;}
	
	//-------------------TIPO AFILIACION-----------------------
	if(tipoAfiliacion3=='0'){
		lista+='<li>Seleccione tipo de afiliacion.</li>';
		afiliacion=false;	
	}
	//-------------------CAPACIDAD--------------------
	/*if(capacidad=='0'){
		lista+='<li>Seleccione capacidad de trabajo.</li>';
		afiliacion=false;	
	}*/
	//-------------------DEPRMENTO-----------------------
	if(cboDeptoB=='0'){
		lista+='<li>Seleccione Departamento de nacimiento.</li>';
		afiliacion=false;	
	}
	if(cboCiudadB==''){
		lista+='<li>Seleccione la ciudad de nacimiento.</li>';
		afiliacion=false;	
	}
	//Validar certificados de beneficiario 
	/*if( $("#cEscolaridad").attr("disabled")==false){
		if($("#cEscolaridad").attr("checked")==false&&$("#cUniversidad").attr("checked")==false||$("#cEscolaridad").attr("checked")==true&&$("#cUniversidad").attr("checked")==true){
			lista+='<li>Seleccione al menos  un certificado de estudio.</li>';
			afiliacion=false;	
		}else{
			if($("#cEscolaridad").attr("checked")==true){
				var temp=0;
			}
			if($("#cUniversidad").attr("checked")==true){
				var temp=1;
			}
		}
		if(temp==0){
			$("#cEscolaridad").attr("value","S");
			if($("#fEscolaridad").val()==''){
				lista+='<li>Ingrese la fecha de entrega del certificado de ESCOLARIDAD.</li>';
				afiliacion=false;	
			}
		}
		if(temp==1){
			$("#cUniversidad").attr("value","S");
			if($("#fUniversidad").val()==''){
				lista+='<li>Ingrese la fecha de entrega del certificado de UNIVERSIDAD.</li>';
				afiliacion=false;	
			}
		}		
	}	*/	
	//---------------------------------------------------------------------
	if($("#cSupervivencia").attr("disabled")==false){
	if($("#cSupervivencia").attr("checked")==false){
			//lista+='<li>Seleccione el certificado de SUPERVIVENCIA.</li>';
			//afiliacion=false;	
		}else{
			$("#cSupervivencia").attr("value","S");
		}
		if($("#fSupervivencia").val()==''){
			//lista+='<li>Ingrese la fecha de entrega del certificado de SUPERVIVENCIA.</li>';
			//afiliacion=false;	
		}
	}							
	//-------------------estado civil---------------------
	if(estadoCivil3=='0'){
		lista+='<li>Seleccione el estado civil.</li>';
		afiliacion=false;	
	}					
	//..MOSTRAR VALIDACIONES..//
	$(lista+"</ul>").prependTo($("#beneficiarios"));
	$("#beneficiarios ul.Rojo").hide().fadeIn("slow");
	//Si no hay errores guardar 
	if($("#beneficiarios ul.Rojo").is(":empty")){
	
		anioActual=new Date();
		hoy=anioActual.getFullYear();
		var edadB=parseInt(hoy)- parseInt(anio);
		if(cedula3==$("#tablaB tbody td:eq(1)").text()){
			alert("El Beneficiario con c\u00E9dula '"+cedula3+"' ya esta agregado");
		}else{
			//cargar los datos en la tabla beneficiario
			$("#tablaB tbody").append("<tr><td>"+option2Sel+"</td><td>"+cedula3+"</td><td>"+cedMama+"</td><td>"+option3Sel.toUpperCase()+"</td><td>"+pNombre3.toUpperCase()+" "+sNombre3.toUpperCase()+" "+pApellido3.toUpperCase()+" "+sApellido3.toUpperCase()+"</td><td>"+fecNac3+"</td><td>"+optionTipoAf+"</td><td>"+edadB+"</td><td align='center'><img src='../../imagenes/menu/ico_error.png' width='16' height='16' alt='Eliminar' /></td></tr>");
			generarArrayB(idnb,tipoDoc3,cedula3,pNombre3,pApellido3,sNombre3,sApellido3,sexo3,$("#fecNacHide3").attr("value"),parentesco,cedMama,tipoAfiliacion3,capacidad,cboDeptoB,cboCiudadB,estadoCivil3,cedMama,fAsignacion);
			arrayCertificados(cedula3,$("#cEscolaridad").attr("value"),fEscolaridad,$("#cUniversidad").attr("value"),fUniversidad,$("#cSupervivencia").attr("value"),fSupervivencia,$("#cDiscapacidad").attr("value"),fDiscapacidad,parentesco);
		}//end if ul beneficiarios
	//ELIMINAR FILA DE LA TABLA BENEFICIARIO	
		$("#tablaB td:contains('"+cedula3+"')~td img[alt='Eliminar']").click(function(){
			if(confirm("Esta seguro de ELIMINAR este BENEFICIARIO?")){
				$(this).parents("tr").find("td:contains('"+cedula3+"')").parent("tr").remove();		  			
				//Borrar elemento del Array Beneficiarios
				for(i=0;i<arrayFinalB.length;++i){
					if(cedula3==arrayFinalB[i][2]){
						arrayFinalB.splice(i,1);
						arrayFinalCert.splice(i,1);//eliminar de una vez el certificado de ese beneficiario
					}
				}//end for	 
			}//end confirm 			  
		});//end click eliminar
	}//end si					
}//FIn function beneficiarios
	
function validarComboParentesco(){
	//setea la visiblidad de la fila #padreBiologico
	$("#padreBiologico").hide();
	
	if($("#parentesco").val()!="35"&&$("#parentesco").val()!="38"){
	$("#cedMama").attr("disabled",true);
	$("#cedMama").val(0);
	//$("#cedMama").parent("td").prev().html('');
	}else{
		
			//cod 38=hijastro
		if($("#parentesco").val()=='38'){
			$("#padreBiologico").show(); 
			$("#tipoIdPadreBiol").val(0);
			$("#IdPadreBiol").val("");
			
			}
		
	$("#cedMama").attr("disabled",false);
	//$("#cedMama").parent("td").prev().html('C&eacute;dula Padre &oacute; Madre');
	}
	validarCertificados();
}		


function validarEstado016(){
	var cedula=$("#IdPadreBiol").val();
	var tdoc=$("#tipoIdPadreBiol").val();
	if(cedula==''){
		alert("Escriba el documento del padre Biológico!!");
	}
	else{
		$.ajax({
			url: URL+'phpComunes/buscarAfiliacionCedula.php',
			type: "POST",
			data: {v0:tdoc,v1:cedula},
			async: false,
			dataType: "json",
			success:function(dato){
				if(dato==0){
				alert("No hay datos de la persona");
				return false;
				}
				$.each(dato,function(i,fila){
					var estado1=fila.estado;
					if(estado1=='A'){
						alert("El padre biológico es trabajador Activo!!");
						$("#IdPadreBiol").val('');
					}
				});
			}
		});
	}
}

//Change certificados beneficiarios del div #beneficiarios en grupoTab.php
function validarCertificados(){
	
    //--------RESETEAR CERTIFICADOS
	$("#fEscolaridad,#fUniversidad,#fSupervivencia").val('');
	$("#cEscolaridad,#cUniversidad,#cSupervivencia").attr("disabled","disabled");
	$("#cEscolaridad,#cUniversidad,#cSupervivencia").attr("checked",false);
	var p=$("#parentesco").val();
	//$("#filaEscolaridad,#filaUniversidad,#filaSupervivencia,#filaDiscapacidad").hide();
	/*if($("#fecNac3").val()==''||$("#fecNac3").val()=='mmddaaaa'){
		alert("Ingrese una fecha de nacimiento para el BENEFICIARIO.");
		$("#fecNac3").focus();
		return false;
		}*/
	 //--------VALIDAR LA EDAD DEL BENEFICIARIO	
	 var f=new Date();
	 var fechaOculta=0;
	 fechaOculta=$("#fecNacHide3").val();
	 var ano=f.getFullYear();
	 var anoFechaOculta=fechaOculta.slice(-4);
	 var edadB=parseInt(ano)-parseInt(anoFechaOculta);
	 //--------PREGUNTAR PRIMERO SI ES SUBSIDIO
	 if($("#tipoAfiliacion3").val()=='48'){
		//cod 35=HIJO    
		if(p=="35"){
			//si el hijo es mayor a 7 y tienen registro civil, cambiar a T.I(2) agosto 2011
			if(edadB>7&&$("#tipoDoc3").val()!="2"){
			alert("El tipo de documento del menor debe ser TARJETA DE IDENTIDAD");
			$("#tipoDoc3").focus().val(0);
			}
			
			if(edadB>=12&&edadB<=40){
			//desactivar check CERTIFICADO
		   $("#cEscolaridad,#cUniversidad").attr("disabled",false);
		 }
		}
		//cod 36=PADRE/MADRE    
		if(p=="36"){
			if(edadB>60){
				//$("#filaSupervivencia").show();//Mostrar certificado de supervivencia
				 $("#cSupervivencia").attr("disabled",false);
				 $("#fSupervivencia").val($("#fecIngreso").val());
			}
		}
		//cod 37=HERMANO	
		if(p=="37"){
			if(edadB>12&&edadB<=40){
			   //$("#filaEscolaridad,#filaUniversidad").show();//Mostrar certificado de estudios
				 $("#cEscolaridad,#cUniversidad").attr("disabled",false);
			}
			/*if($("#capacidad").val()=='D'){
				$("#filaDiscapacidad").show();//Certificado Discapacidad
			}*/
		}
	}//if 
//Validar el foco de combos de cedul de la madre
	/*	if($("#cedMama").is(":visible")&&$("#parentesco").is(":visible")&&$("#cedMama").val()=='0'){
			 if($("#cedMama").val()!='0'){
			return true;
			 }else{
			$("#cedMama").focus(); 
			 }
			 }else{$("#tipoAfiliacion3").focus();}*/
}

//Funcion para mostrar fecha de certificado con lso checks
function mostrarFecha(op){
	$("#beneficiarios input:text[name='fCert']").val('');
if(op==1){
	chequeado2=1;
	chequeado1++;
	if( chequeado1 % 2 != 0 ){
		$("#cEscolaridad").removeAttr("checked");
		$("#fEscolaridad").val("");
		return false;
	}
	else{
		$("#fEscolaridad").val($("#fecIngreso").val());
	}
 	
}else{
	chequeado1=1;
	chequeado2++;
	if( chequeado2 % 2 != 0 ){
		$("#cUniversidad").removeAttr("checked");
		$("#fUniversidad").val("");
		return false;
	}
	else{
	 	$("#fUniversidad").val($("#fecIngreso").val());
	}

}
}


//---FUNCION PARA GENERAR ARRAY DE CONYUGES 
function generarArrayC(tabla,tipRel,conviven,tipDoc,cedula,pNombre,pApellido,sNombre,sApellido,sexo,direccion,cboBarrio,telefono,celular,email,tipVivienda,deptRes,ciudadRes,cboZona,estadoCivil,fecNac,deptNac,ciudadNac,capTrabajo,profesion,registroMatrimonio,fotocopiaCed,idtrelacion){	
	$("#conyuge").dialog('close');
	//ARRAY  de valores de la fila  
	arrayActual=[tabla,tipRel,conviven,tipDoc,cedula,pNombre,pApellido,sNombre,sApellido,sexo,direccion,cboBarrio,telefono,celular,email,tipVivienda,deptRes,ciudadRes,cboZona,estadoCivil,fecNac,deptNac,ciudadNac,capTrabajo,profesion,registroMatrimonio,fotocopiaCed,idtrelacion];
	arrayFinalC.push(arrayActual);
	}//end funcion
		  
//---FUNCION PARA GENERAR ARRAY  BENEFICIARIOS
function generarArrayB(tabla,tipDoc,cedula,pNombre,pApellido,sNombre,sApellido,sexo,fecNac,parentesco,cedMama,tipoAfiliacion,capacidad,depNace,ciudadNace,estadoCivil,fAsignacion){
	$("#beneficiarios").dialog('close');
	//ARRAY  de valores de la fila
	arrayActual=[tabla,tipDoc,cedula,pNombre,pApellido,sNombre,sApellido,sexo,fecNac,parentesco,cedMama,tipoAfiliacion,capacidad,depNace,ciudadNace,estadoCivil,fAsignacion];
	arrayFinalB.push(arrayActual);	
}//end funcion

//---Funcion para generar array de certificados
function arrayCertificados(idBeneficiario,cEscolaridad,fEscolaridad,cUniversidad,fUniversidad,cSupervivencia,fSupervivencia,cDiscapacidad,fDiscapacidad,parentesco){
	arrayActual=[idBeneficiario,cEscolaridad,fEscolaridad,cUniversidad,fUniversidad,cSupervivencia,fSupervivencia,cDiscapacidad,fDiscapacidad,parentesco];
	arrayFinalCert.push(arrayActual);	
	}

function datosPersona(){
	if($("#tipoDoc").val()==0){
	    alert("Falta tipo documento");
	    return false;
	}
	//Dialog de imagen Cargando
	
	ruta=false;
	var idtd=$("#tipoDoc").val();
	var num = $("#identificacion").val();
	$.ajax({
		url: URL+'phpComunes/buscarPersona2.php',
		type: "POST",
		data: {v0:idtd,v1:num},
		async: false,
		dataType: "json",
		beforeSend: function(){
		dialogLoading("show");
		},
		success: function(datos){
			if(datos==0){
				existenDatos=0;
				idPersona=0;
				idAfiliacion=0;
				dialogLoading("hide");
				setTimeout("document.getElementById('pNombre').focus()",800);
				return false;
			}
			existenDatos=1;
			
			$.each(datos,function(i,fila){
				$("#idPersona").val(fila.idpersona);
				idPersona=fila.idpersona;
				$('#pNombre').val($.trim(fila.pnombre));
				//$('#pNombre').click().blur();
				$('#sNombre').val($.trim(fila.snombre));
				$('#sNombre').click().blur();
				$('#pApellido').val($.trim(fila.papellido));
				$('#pApellido').click().blur();
				$('#sApellido').val($.trim(fila.sapellido));
				$('#sApellido').click().blur();
				$('#nombreCorto').val($.trim(fila.nombrecorto));
				$('#sexo').val(fila.sexo);
				$("#tDireccion").val($.trim(fila.direccion));
				$("#telefono").val($.trim(fila.telefono));
				$("#celular").val($.trim(fila.celular));
				$("#email").val($.trim(fila.email));
				$("#tipoVivienda").val(fila.idtipovivienda);
				var fecha=fila.fechanacimiento;
				fecha=fecha.replace("/",""); 
				fecha=fecha.replace("/",""); 
				$('#fecNac').val($.trim(fecha));
				$('#fecNac').click().blur();
				$("#capTrabajo").val(fila.capacidadtrabajo);
				$("#profesion").val(fila.idprofesion);
				$("#estadoCivil").val(fila.idestadocivil);
				//Cargar combos
				$("#cboDepto").val(fila.iddepresidencia).trigger('change');
				$("#cboDepto2").val(fila.iddepnace).trigger('change');
				
				setTimeout((function(){
				$("#cboCiudad").val(fila.idciuresidencia);
				$("#cboCiudad2").val(fila.idciunace);
			    $("#cboCiudad,#cboCiudad2").trigger("change");
				 }),1000);
				
				setTimeout((function(){
				$("#cboZona").val(fila.idzona);
				$("#cboZona").trigger("change");
				}),2000);
				setTimeout((function(){
				$("#cboBarrio").val(fila.idbarrio);	
				$("#cboBarrio").trigger("change");
				}),3000);
				
				setTimeout("document.getElementById('pNombre').focus()",800);
				
				$("#rutaDoc").val(fila.rutadocumentos);
				a=$.trim(fila.rutadocumentos);
				if(a.length>0){
					ruta=fila.rutadocumentos;
				}
				return;
			}); //each
			
			$.ajax({
				url: URL+'phpComunes/buscarAfiliacion.php',
				type: "POST",
				data: {v0:idPersona},
				async: false,
				dataType: "json",
				/*complete: function(){
					dialogLoading("hide");
					},*/
				success: function(dato){
					if(dato==0){
					$.ajax({
						url: URL+'phpComunes/buscarHistorico.php',
						type: "POST",
						data: {v0:idPersona},
						async: false,
						dataType: "json",
						success: function(dato2){
							if(dato2==0){
								idAfiliacion=0;
							}
							else{
								MENSAJE("La persona que esta tratando de de afiliar Tiene por lo menos una afiliaci&oacute;<br>Cambie el tipo de afiliacion en la radicacion por Renovacion!");
								limpiarCampos();
								idAfiliacion=1;
							}
						}
					});
					}
					else{
						var nit=0;
						var rz=0;
						idAfiliacion=1;
						$.each(dato, function(i,fila){
							nit=fila.nit;
							rz=fila.razonsocial;
							return;
						});
						MENSAJE("La persona que esta tratando de afiliar ("+idPersona+") tiene una afiliaci&oacute;n activa con:<br>NIT:"+nit+"<BR>Empresa: "+rz+"<br>Verifique la informaci&oacute;n de la afiliaci&oacute;n" +
								"<br>La radicacion pudo ya ser grabada<br>Si es una renovacion cambie el tipo de radicacion!");
						//Limpiar los cmapos zona y barrio por q tienen un settimeout , por tal motivo volverlos a borrar por q aun no salen
					//VALIDO SOLO PARA ESTE FOMRULARIO
					setTimeout("limpiarCampos()",3000);
					clearInterval();
					}
					dialogLoading("hide");
					}
			});
		}
	});
}
	
function abrirTab(op){
	switch(op){
	case 1 : $("#a1").trigger('click'); break;
	case 2 : $("#a2").trigger("click"); break;
	case 3 : $("#a3").trigger("click"); break;
	}
}

function buscarPersona(num){
	if($("#conviven").val()==0){
		alert("Seleccione tipo de convivencia!")
		$("#conviven").focus();//agosto 2011
		return false;
	}
	var convive=$("#conviven").val();
	if($("#tipoDoc2").val()==0){
	    alert("Falta tipo documento");
	    return false;
	}
	var idtd=$("#tipoDoc2").val();
	$.ajax({
		url: URL+'phpComunes/buscarPersona2.php',
		type: "POST",
		data: "submit &v0="+idtd+"&v1="+num,
		async: false,
		dataType: "json",
		success: function(data){
			if(data==0){
				existe=0;
				idnb=0;
				return false;
			}
			else{
			existe=1;
			$.each(data,function(i,fila){
				idnb=fila.idpersona;
				$('#pNombre2').val($.trim(fila.pnombre));
				$('#sNombre2').val($.trim(fila.snombre));
				$('#pApellido2').val($.trim(fila.papellido));
				$('#sApellido2').val($.trim(fila.sapellido));
				$("#direccion2").val($.trim(fila.direccion));
				$("#cboBarrioC").val(fila.idbarrio);
				$("#telefono2").val(fila.telefono);
				$("#celular2").val(fila.celular);
				$("#email2").val($.trim(fila.email));
				$("#tipoVivienda2").val(fila.idtipovivienda);
				$("#cboDeptoC").val(fila.iddepresidencia).trigger('change');
				setTimeout((function(){
					$("#cboCiudadC").val(fila.idciuresidencia);
				    $("#cboCiudadC").trigger("change");
					 }),1000);
					
					setTimeout((function(){
					$("#cboZonaC").val(fila.idzona);
					$("#cboZonaC").trigger("change");
					}),2000);
					setTimeout((function(){
					$("#cboBarrioC").val(fila.idbarrio);	
					$("#cboBarrioC").trigger("change");
					}),3000);
				$("#estadoCivil2").val(fila.idestadocivil);
				$("#cboDeptoC2").val(fila.iddepnace);
				$("#ciudadNac").val(fila.idciunace);
				$("#capTrabajo2").val(fila.capacidadtrabajo);
				$("#profesion2").val(fila.idprofesion);
				var fecha=fila.fechanacimiento;
				fecha=fecha.replace("/","");
				$('#fecNac2').val(fecha.replace("/",""));
				$('#fecNac2').click().blur();
				$('#sexo2').val(fila.sexo);
			}); //each
//			TABLA DINAMICA PARA DATOS EMPRESA TRABAJDOR ACTIVO
			$("table[name='dinamicTable']").remove();
			var tabla="<table class='tablero' width='100%' border='0' cellspacing='0' cellpadding='0' name='dinamicTable' style='margin-first:8px;'>";
			$.ajax({
				url: URL+'phpComunes/buscarAfiliacion.php',
				type: "POST",
				data: "submit &v0="+idnb,
				async: false,
				dataType: "json",
				success: function(datos){
					if(datos != 0){
						alert("La persona que trata de Afiliar es un trabajador activo!");
					    $.each(datos,function(i,fila){
					    	$('#nitConyuge').val(fila.nit);
					    	$('#empCony').val(fila.razonsocial);
					    	$('#salCony').val(fila.salario);
					    	$('#subCony').val(fila.detalledefinicion);
					    	tabla+="<tr><th>Salario</th><th>Nit</th><th>Raz\u00F3n social</th><th>Tipo</th><th>Detalle</th></tr>"+"<tr><td>$"+ fila.salario +"</td><td>"+ fila.nit +"</td><td>"+fila.razonsocial+"</td><td>"+fila.tipoformulario+"</td><td>"+fila.detalledefinicion+"</td></tr>";
					    });//end each
					  //Agregar tabla despues de la tabla  beneficiarios
					    $("#conyuge .tablero").after(tabla+="</table>");
					    $("table[name='dinamicTable']").hide().fadeIn(800);
					}					
				},
				complete: function(){
					var conv=0;
					var tabla2="<table class='tablero' width='100%' border='0' cellspacing='0' cellpadding='0' name='dinamicTable' style='margin-first:8px;'>";
					if($("#conviven").val()=='S'){
						$.ajax({
							url: URL+'phpComunes/buscarConvivenciaConyuge.php',
							type: "POST",
							data: "submit &v0="+idnb,
							dataType: "json",
							success: function(dato){
								if(dato!=0){
								 alert("La persona que esta afiliando tiene relacion de convivencia (ver tabla parte inferior)");					 
								 $.each(dato,function(i,fila){
									   var nombre=fila.pnombre+" "+fila.snombre+" "+fila.papellido+" "+fila.sapellido;
									   conv=fila.conviven
									   idtrelacion=fila.idtrabajador;
									   tabla2+="<tr><th>Identificacion</th><th>Nombre</th><th>Conviven</th></tr>"+"<tr><td>"+ fila.identificacion +"</td><td>"+ nombre +"</td><td>"+fila.conviven+"</td></tr>";
									   return;
								 });
								 if(idPersona==idtrelacion){
									 idtrelacion=1;
								 }
								 $("#div-relacion").append(tabla2+="</table>");
								 $("table[name='dinamicTable']").hide().fadeIn(800);
								 $("#conviven").focus().val(0);//agosto2011
								 $("#identificacion2").val('');//agosto 2011
								 if(conv='S' && idtrelacion==0){
									 $('#conyuge .tablero input:text').val("");
									 $('#conyuge .tablero select').val("0");
								 }
							}
							}
						}); //FIN AJAX 3
					}
				}
			}); //FIN AJAX 2
			
			}//fin else primer success
		}//fin primer success
	})
}
function buscarPersonaDos(num){
	if($("#tipoDoc3").val()==0){
	    alert("Falta tipo documento");
	    return false;
	}
	if($("#tipoDoc3").val()!=4){
		var campo1=parseInt(num);
		if(isNaN(campo1))return false;
	}
	var idtd=$("#tipoDoc3").val();
	$.getJSON(URL+'phpComunes/buscarPersona2.php',{v0:idtd,v1:num},function(data){
	    if(data==0){
	        existe=0;
	        return false;
	}
	existe=1;
	$.each(data,function(i,fila){
	    idnb=fila.idpersona;
	    $('#pNombre3').val($.trim(fila.pnombre));
	    $('#sNombre3').val($.trim(fila.snombre));
	    $('#pApellido3').val($.trim(fila.papellido));
	    $('#sApellido3').val($.trim(fila.sapellido));
	    var fecha=$.trim(fila.fechanacimiento);
	    fecha=fecha.replace("/","");
	    $('#fecNac3').val(fecha.replace("/",""));
	    $('#fecNac3').click().blur();
	    $('#sexo3').val(fila.sexo);
	}); //each

	// TABLA DINAMICA PARA DATOS EMPRESA TRABAJDOR ACTIVO
	$("table[name='dinamicTable']").remove();
	var tabla="<table class='tablero' width='100%' border='0' cellspacing='0' cellpadding='0' name='dinamicTable' style='margin-first:8px;'>";
	$.ajax({
		url: URL+'phpComunes/buscarAfiliacion.php',
		type: "POST",
		data: "submit &v0="+idnb,
		async: false,
		dataType: "json",
		success: function(datos){
			if(datos != 0){
				alert("La persona que trata de Afiliar es un trabajador activo!");
				$.each(datos,function(i,fila){
					tabla+="<tr><th>Salario</th><th>Nit</th><th>Raz\u00F3n social</th><th>Tipo</th><th>Detalle</th></tr>"+"<tr><td>$"+ fila.salario +"</td><td>"+ fila.nit +"</td><td>"+fila.razonsocial+"</td><td>"+fila.tipoformulario+"</td><td>"+fila.detalledefinicion+"</td></tr>";

				});//end each
				//Agregar tabla despues de la tabla  beneficiarios
				$("#beneficiarios .tablero").after(tabla+="</table>");
				$("table[name='dinamicTable']").hide().fadeIn(800);
				$('#beneficiarios .tablero input:text').val("");
				$('#beneficiarios .tablero select').val("0");
				return false;
			}
			$.ajax({
				url: URL+'phpComunes/buscarRelaciones.php',
				type: "POST",
				data: "submit &v0="+idnb,
				async: false,
				dataType: "json",
				success: function(datos){
					if(datos != 0){
						alert("La persona que trata de Afiliar tiene las siguientes relaciones!");
						var conr=0;
						$.each(datos,function(i,fila){
							var nom = fila.pnomt+" "+fila.snomt+" "+fila.papet+" "+fila.sapet;
							tabla+="<tr><th>Nombres</th><th>Parentesco</th><th>Conv</th><th>F Afilia</th><th>Giro</th><th>Estado</th></tr><tr><td>"+nom+"</td><td>"+fila.detalledefinicion+ "</td><td>"+fila.conviven+"</td><td>"+fila.fechaafiliacion+"</td><td>"+fila.giro+"</td><td>"+fila.estado+"</td></tr>";
							conr++;
						});//end each
//						Agregar tabla despues de la tabla  beneficiarios
						$("#beneficiarios .tablero").after(tabla+="</table>");
						$("table[name='dinamicTable']").hide().fadeIn(800);
						if(conr>1){
							$('#beneficiarios .tablero input:text').val("");
							$('#beneficiarios .tablero select').val("0");
							return false;
						}
					}
				} // fin success 2
				}); //getJSON relaciones
		} // end success primero
	}); //getJSON afiliacion
	}); //getJSON
	}//end funcion			

function buscarRelacionesAnt(){
	//falta sql
	return false;
var campo0=$("#tipoDoc").val();
var campo1=$("#identificacion").val();
$.ajax({
	url: URL+"phpComunes/buscarIDPersona.php",
	type: "POST",
	async: false,
	data: "submit=&v0="+campo0+"&v1="+campo1,
	dataType: "json",
	success: function(datos){
		if(datos==0){
			MENSAJE("No se pudo el ID del conyuge!");
			return false;
		}
		else{
			alert(datos);
		}
	}
});
}

function validarNumero(obj){
	var td=$("#tipoDoc").val();
}

function nomCorto(pn,pa,sn,sa){
	var patron=new RegExp('[\u00F1|\u00D1]');
	//---Reemplazar Ã‘

	for(p=1;p<=pn.length;p++){
		pn=pn.replace(patron,"&");
		}
	for(p=1;p<=sn.length;p++){
		sn=sn.replace(patron,"&");
		}
	for(p=1;p<=pa.length;p++){
		pa=pa.replace(patron,"&");
	}
	for(p=1;p<=sa.length;p++){
		sa=sa.replace(patron,"&");
		}

	txt=pn+" "+sn+" "+pa+" "+sa;
	txt=txt.toUpperCase();
	num=parseInt(txt.length);	

	if(num>30){
		if(sn.length>0){
			sn=sn.slice(0,1);
			txt=pn+" "+sn+" "+pa+" "+sa;
			txt=txt.toUpperCase();
			num=parseInt(txt.length);
		}
		if(num>30)
			if(sa.length>0){
				sa=sa.slice(0,1);
				txt=pn+" "+sn+" "+pa+" "+sa;
				txt=txt.toUpperCase();
				num=parseInt(txt.length);
			}
	}
return txt;
}

function cerrarRadicacion(msg){
    var campo0=$("#pendientes").val();
	$.ajax({
		url: URL+'phpComunes/cerrarRadicacion.php',
		type: "POST",
		data: "submit=&v0="+campo0,
		success: function(datos){
			msg+="<br>"+datos;
			MENSAJE(msg);
			$("#errores ul li").html("Datos completos !, click en NUEVO");
		}
	});
	  }

function validarSubsidio(){
	var afilia=$("#tipAfiliacion").val();
	var formul=$("#tipForm").val();
	if(afilia==0)return;
	if(formul==48 && afilia!=18){
		MENSAJE("Solo tienen derecho a subsidio familiar las afiliaciones de trabajadores DEPENDIENTES!, cambie el tipo de formulario o el tipo de afiliaci&oacute;n");
		$("#tipAfiliacion").val(0);
		$("#tipForm").val(0);
		$("#tipForm").focus();
	}
	
}
//borrar
function llenarCampos(){
    $('#tipoDoc').val(1);
	$("#pNombre").val('orlando');
	$("#pApellido").val('puentes');
	$("#sNombre").val();//no obligado
	$("#sApellido").val('andrade');//no obligado
	$("#sexo").val('M');
	$("#tDireccion").val('calle 46 1a-53');
	$("#cboBarrio").val(136);
	$("#telefono").val('8757575');
	$("#celular").val();
	$("#tipoVivienda").val('U');
	$("#cboDepto").val('41');//$("#deptRes").val();
	$("#cboCiudad").val('41001');//$("#ciudadRes").val();
	$("#combo3").val('41001001');//$("#cboZona").val();
	$("#estadoCivil").val(50);
	$("#combo4").val('41');
	$("#combo5").val('41001');
	$("#capTrabajo").val('N');
	$("#profesion").val(212);
    $('#fecNac').val('01011980');
    $('#nombreCorto').val('ORLANDO PUENTES')
	$("#fecNacHide").val('06/15/1980');	//rescato elv alor de la fecha del cmapo oculto
	$("#tipForm").val(48);
	$("#fecIngreso").val();
	$("#horasDia").val(8);
	$("#horasMes").val(240);
	$("#salario").val(1000000);
	$("#agricola").val('N');
	$("#cargo").val(160);
	$("#estado").val('A');
	$("#traslado").val('N');
	$("#tipAfiliacion").val(18);
	$("#fecIngreso").val('01/01/2010');
	$("#tipoPago").val('T');
}