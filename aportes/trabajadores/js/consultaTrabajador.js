/*
* @autor:      Ing. Orlando Puentes
* @fecha:      04/09/2010
* objetivo:
*/
var arrIde=new Array();
var idEmpresa=0;
var idPersona=0;
var URL=src();
var tercero = false;

$(document).ready(function(){
	var URL=src();
	
	$("#buscarPor").change(function(){
		$("#pn,#pa,#idT").val('');
		if($(this).val()=='2'){
			$("#tipoDocumento").hide();
			$("#pn,#pa").show();
			$("#idT").hide();
		}else{
			$("#tipoDocumento").show();
			$("#pn,#pa").hide();
			$("#idT").show();
			$("#idT").focus();
		}
	});
	
	$("#dialog-form2").dialog({
		autoOpen: false,
		height: 400,
		width: 500,
		modal: false,
		buttons: {
			'Enviar': function() {
				var bValid = true;
				var campo=$('#notas').val();
				var campo0=$.trim(campo);
				if (campo0==""){
					$(this).dialog('close');
						return false;
				}
				var campo1=$('#usuario').val();
				var campo2="consultaTrabajador.php";
				$.post(URL+'phpComunes/colaboracion.php',{v0:campo0,v1:campo1,v2:campo2},
				function(datos){
					if(datos=='1'){
						alert("Su comentario fue enviado correctamente!. Gracias por participar en el mejoramiento de SIGAS");
					} else {
						alert(datos);
					}
				});
				$(this).dialog('close');
			},
			Cancelar: function() {
				$(this).dialog('close');
				$("#dialog-form2").dialog("destroy");
			}
		}
	});
	
	//Buscar Trabajador
	$("#buscarT").click(function(){
		$("#trabajadores").find("p").remove();
		dialogLoading("show");
		$("#tabsT").hide();
		$("#idT").val($.trim($("#idT").val()));		
		
		if($("#idT").is(":visible")){
			$("#idT").val($.trim($("#idT").val()));
			if ( $("#idT").val()=='' || isNaN($("#idT").val()) ) {
				if ( $("#idT").val()=='' ) {
					$(this).next("span.Rojo").html("Ingrese un valor a buscar.").hide().fadeIn("slow");
				} else if ( isNaN($("#idT").val()) ) {
					$(this).next("span.Rojo").html("La Identificaci\u00F3n debe ser num\u00E9rico.").hide().fadeIn("slow");
				}
				dialogLoading("hide");
				$("#idT").focus();
				return false;
			}
		} else if($ ("#pn").is(":visible")){		
			if ( ( $("#pn").val() == '' && $("#pa").val() == '' ) || ( /[^A-Za-z\d]/.test($("#pn").val()) ) || ( /[^A-Za-z\d]/.test($("#pa").val()) ) ) {			
				if ( ( $("#pn").val() == '' && $("#pa").val() == '' ) ){
					$(this).next("span.Rojo").html("Ingrese un valor a buscar.").hide().fadeIn("slow");
				} else if ( ( /[^A-Za-z\d]/.test($("#pn").val())) ) {
					$(this).next("span.Rojo").html("El Nombre debe ser solo letras.").hide().fadeIn("slow");
				} else {
					$(this).next("span.Rojo").html("El Apellido debe ser solo letras.").hide().fadeIn("slow");
				}
				
				dialogLoading("hide");
				$("#pn").focus();
				return false;
			}
		}	
				
		var v0=$("#idT").val();
		var v1=$("#buscarPor").val();
		var v2=$("#tipoDocumento").val();
		var v3=$("#pn").val();//campo nombre
		var v4=$("#pa").val();//campo nombre
		var contadorAfi=0;
		var contadorTemp=0;
		var cadenaAfiliados='';
		var idp=0;
		var nom="";
		$.ajax({
			url: URL+'phpComunes/buscarPersonaPara.php',
			type: "POST",
			data: {v0:v0,v1:v1,v2:v2,v3:v3.toUpperCase(),v4:v4.toUpperCase()},
			async: false,
			dataType: 'json',
			success: function(data){
				if(data==0){
					$("#buscarT").next("span.Rojo").html("No se encontr&oacute; la persona").hide().fadeIn("slow");
					dialogLoading("hide");	
					return false;
				}
				
				$("#lstT").remove();
					
				$.each(data,function(i,fila){
					$("#buscarT").next("span.Rojo").html(""); 
					$("#idT").val('');
					idp=fila.idpersona;
					nom =fila.pnombre+" "+fila.snombre+" "+fila.papellido+" "+fila.sapellido;
					$.ajax({
						url:URL+'phpComunes/buscarAfiliacionIDP.php',
						type: "POST",
						data: {v0:idp},
						async: false,
						dataType: 'json',
						success: function(datos){
							if(datos==0){
								tercero = true;
								arrIde[0] = 0;
							}else{
								tercero = false;
								$.each(datos,function(i,fila){
									if(fila.idempresa!=null){
											arrIde[i]=fila.idempresa;
									}
									return;
								});
							}
						
						contadorAfi++;
						cadenaAfiliados+='<p align="left" onclick="enviarNit('+idp+','+arrIde+')">'+idp+" "+nom+'</p>';						
					  }
					});
				});
				
				$("#trabajadores").append(cadenaAfiliados);
				dialogLoading("hide");	
				$("#trabajadores").before("<p id='lstT'>Trabajadores encontrados:<b> "+contadorAfi+"</b></p>");							
			}
		});
	});
		
	$("div#icon span").click(function(){
		$(this).toggleClass("toggleIcon");
		$("div#wrapTable").slideToggle();		
	});
});//fin ready

function runSearch(e){
	if(e.keyCode == 13){
		$("#buscarT").trigger("click");
	} 
}

//... FUNCION PARA ENVIAR NIT PARA ACORDEON ...//
function enviarNit(idp,ide){
	$("#lstT").remove();	
	var	indexTab;
	var idEmpresa=0;
	var idPersona=0;
	idEmpresa=ide;
	idPersona=idp;
	$("div#icon").show();
	$("#trabajadores").find("p").remove();
	//$("#tabs div").empty();
	$("div#wrapTable").slideUp("normal");
	$("#tabsT").tabs("destroy");//Destruyo tab para nueva consulta
	$("#tabsT").show("slow",function(){
		$("#tabsT ul li:first a").trigger("click");
	});	//Muestro el tab
	
	$("#tabsT div").html('');
	jQuery('div[aria-labelledby^=ui-dialog-title]').remove();
	jQuery('div[id^=div-datos]').remove();	
	
	//Aqui Capturo el valor del ALT para el switch que carque los formularios	
	$("#tabsT ul li a,#tabs2 ul li a").unbind("click"); 
	$("#tabsT ul li a").bind("click",function(){
		indexTab=$(this).attr("alt");
		hrefTab=$(this).attr("href");
		if((indexTab=="b" || indexTab=="f" || indexTab=="g") && $(hrefTab).html().trim()!=0){
			return;
		}		
		//CARGAR TABLA DEACUERDO A TAB SELECCIONADO
		switch(indexTab){
			case "a": $("#tabs-1").load("tabConsulta/fichaTab.php",{v0:idPersona,flag:1, esTercero: tercero}); break;
			case "b": $("#tabs-2").load("tabConsulta/personaTab.php",{v0:idPersona,botonActualizar:1}); break; 
			case "c": $("#tabs-3").load("tabConsulta/reclamosTab.php",{v0:idPersona}); break; 
			case "d": $("#tabs-4").load("tabConsulta/embargosTab.php",{v0:idPersona}); break; 
			case "e": $("#tabs-5").load("tabConsulta/defuncionesTab.php",{v0:idPersona}); break; 
			case "f": $("#tabs-6").load("tabConsulta/aportesTab.php",{v0:idEmpresa}); break;
			case "g": $("#tabs-7").load("tabConsulta/planillaTab.php",{v0:idPersona}); break;
			case "h": $("#tabs-8").load("tabConsulta/girosTab.php",{v0:idPersona}); break;
			case "i": $("#tabs-9").load("tabConsulta/tarjetaTab.php",{v0:idPersona}); break;
			case "j": $("#tabs-10").load("tabConsulta/movimientosTab.php",{v0:idPersona}); break;
			case "k": $("#tabs-11").load("tabConsulta/causalesTab.php",{v0:idPersona}); break;
			case "l": $("#tabs-12").load("tabConsulta/documentosTab.php",{v0:idPersona}); break;
			case "m": $("#tabs-13").load(URL+"aportes/trabajadores/contenedor.php",{v0:idPersona}); break;
			case "n": $("#tabs-14").load("tabConsulta/categoriaTab.php",{v0:idPersona}); break; 
		}	//end switch				 				 
	});
	   
	$("#tabsT").tabs({ cache:true, spinner:'<em>Loading&#8230;</em>',selected:-1});//end tab		


}//END FUNCTION BUSCAR NIT

var nuevo=0;
var modificar=0;
var continuar=true;
var msg="";

$(function() {
		$("#ayuda").dialog({
		 	autoOpen: false,
			height: 450, 
			width: 700, 
			draggable:true,
			modal:false,
			open: function(evt, ui){
					$('#ayuda').html('');
					$.get(URL+'help/aportes/ayudaConsultaTrabajador.html',function(data){
							$('#ayuda').html(data);
					})
			 }
		});

});
//---------funcion para contruir el dialog del estado del subsidio en revision----------
function cambiarCausal(estado, idRegistro,idTr){
	//destruccion del dialog por al momento de ejecutar el cambio de nit se carga con un boton "Actualizar" 
	//entonces debemos destruir el  dialog
	$("#idDialogCausal").dialog('destroy');
  	$.ajax({
		url:'tabConsulta/girosLogica.php',
		type: "POST",
		data: {v0:'CAMPOSDINAMIOS',v1:estado, v2:idRegistro,v3:idTr},
		async: false,
		dataType: 'json',
		success: function(datos){
			$("#idDialogCausal").html(datos);
		}
	});
    
    $("#idDialogCausal").css('display','block');
    $("#idDialogCausal").dialog({
    	modal: true,
    	width: 600,
    	height: 250,
    	title:'.:Subsidio cambio de Estado:.'
    });

}
//---------funcion para modificar el estado del subsidio en revision----------
function bModificarCausal(idTr){
	var flag       = $("#txtFlag").val();
    var valorCuota = $("#txtValorCuota").val();
    var idRegistro = $("#txtIdRegistro").val();
    var causal     = $("#cmbCausal").val();
    var causalL    = $("#cmbCausal option:selected").text();
    
    $.ajax({
    	url:'tabConsulta/girosLogica.php',
    	type:'POST',
    	data: {v0:'MODIFICAR',v1:flag, v2:valorCuota, v3:idRegistro, v4:causal},
		async: false,
		dataType: 'json',
		success: function(datos){
			if(datos==1){
    			alert("La informaci\u00F3n fu\u00E9 actualizada correctamente");
    			if(flag=="S"){
    				$("#"+idTr+" td#tdValorCuota").html(valorCuota);
    				$("#"+idTr+" td#tdCausal").html('');
    			}
    			if(flag=="N"){
    				$("#"+idTr+" td#tdValorCuota").html('0');
    				$("#"+idTr+" td#tdCausal").html(causalL);
    			}
    			$("#"+idTr+" td#tdEstado").html("<label style='cursor:pointer'onclick='cambiarCausal(\""+flag+"\",\""+idRegistro+"\",\""+idTr+"\");'>"+flag+"</label>");
    			
    			$("#tabs-8").html('');
    			$("a[href=#tabs-8]").trigger("click");
	    	}else{
	    		alert("La informaci\u00F3n no fu\u00E9 actualizada correctamente");
	    	}
		}
	});
    $("#idDialogCausal").dialog("destroy");
	$("#idDialogCausal").html('');
}


//---------funcion para anular el subsidio en revision----------
function bAnularCausal(idTr){
	var flag       = $("#txtFlag").val();
    var idRegistro = $("#txtIdRegistro").val();

    $.ajax({
    	url:'tabConsulta/girosLogica.php',
    	type:'POST',
    	data: {v0:'ANULAR',v1:flag, v2:idRegistro},
		async: false,
		dataType: 'json',
		success: function(datos){
			if(datos==1){
    			alert("Se anulo el giro correctamente");
    			if(flag=="S"){
    				$("#"+idTr+" td#tdValorCuota").html(valorCuota);
    				$("#"+idTr+" td#tdCausal").html('');
    			}
    			if(flag=="N"){
    				$("#"+idTr+" td#tdValorCuota").html('0');
    				$("#"+idTr+" td#tdCausal").html(causalL);
    			}
    			$("#"+idTr+" td#tdEstado").html("<label style='cursor:pointer'onclick='cambiarCausal(\""+flag+"\",\""+idRegistro+"\",\""+idTr+"\");'>"+flag+"</label>");
    			
    			$("#tabs-8").html('');
    			$("a[href=#tabs-8]").trigger("click");
	    	}else{
	    		alert("No se anulo el giro correctamente");
	    	}
		}
	});
    $("#idDialogCausal").dialog("destroy");
	$("#idDialogCausal").html('');
}


//--------funcion para cambiar el nit del subsidio en revision--------
function  cambiarNit(nit, idRegistro,idTr){

	$.ajax({
		url      :'tabConsulta/girosLogica.php',
		type     :"POST",
		data     :{v0:'CAMPOSCAMBIARNIT',v1:nit,v2:idRegistro},
		async    : false,
		dataType : 'json',
		success  : function(datos){
			$("#idDialogCausal").html(datos);
		}
	});
	
	$("#idDialogCausal").css('display','block');
	$("#idDialogCausal").dialog({
    	modal: true,
    	width: 700,
    	height: 380,
		minHeight:380,
		minWidth:700,
		maxHeight:380,
		maxWidth:700,
    	title:'.:Subsidio cambio de Nit:.',
    	buttons: {
    		Actualizar:function(){
    			var nit = $("#txtNuevoNit").val().trim();
    			if(nit=="")
    				return false;

    			$.ajax({
    				url      :"tabConsulta/girosLogica.php",
    				type     :"POST",
    				data     :{v0:"MODIFICARNIT",v1:nit,v2:idRegistro},
    				async    :false,
    				dataType :'json',
    				success:function(datos){
    					if(datos==0){
    						alert("La actualizacion no fu\u00E9 realizada");
    						return false;
    					}
						if(datos==2){
							alert("El nit no corresponde a ninguna empresa");
							return false;
						}
    					alert("La actualizacion fu\u00E9 satisfactoria");
						$("#"+idTr+" td#tdNit").html('<label onclick=\'cambiarNit("'+nit+'","'+idRegistro+'","'+idTr+'");\' style="cursor:pointer">'+nit+'</label>');
    					$("#idDialogCausal").dialog('close');
    					$("#idDialogCausal").html('');
    					$("#idDialogCausal").dialog('destroy');
						
    				}
    			});
    		}
    	}
    });

}
