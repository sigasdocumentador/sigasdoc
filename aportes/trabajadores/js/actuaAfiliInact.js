var URL=src(),
	
urlBuscarPersona = "",
	urlAfiliacionInactiva = "",
	urlDetalleDefinicion = "",
	urlActuaAfiliInact = "",
	
	htmlComboOptioTipoFormu = null,
	htmlComboOptioTipoAfili = null,
	htmlComboOptioCargo = null;


$(function(){
	urlBuscarPersona = URL+"phpComunes/buscarPersona.php";
	urlAfiliacionInactiva = URL+"phpComunes/buscaAfiliInact.php";
	urlDetalleDefinicion = URL+"phpComunes/buscaDetalDefin.php";
	urlActuaAfiliInact = "actuaAfiliInact.log.php";
	
	htmlComboOptioTipoFormu = fetchComboOptioTipoFormu();
	htmlComboOptioTipoAfili = fetchComboOptioTipoAfili();
	htmlComboOptioCargo = fetchComboOptioCargo();
	
	$("#btnBuscarPersona")
			.click(controlPersona)
			.button();
	
	$("#btnActualizar")
			.button()
			.click(function(){
				
				$("#divObservacion").dialog("open");
				
				//Verificar si el usuario desea activar alguna afiliacion
				/*if($("#tabAfiliacionInactiva tbody input[name='chkActivar']:checked").length>0){
					//Es necesario guardar la observacion
					$("#divObservacion").dialog("open");
					
				}else{
					actualizar();
				}*/
			});
	
	$("#divObservacion").dialog({
		modal:true,
		autoOpen:false,
		width: 700,
		heigth:500,
		buttons:{
			"ACEPTAR": function(){
				var observacion = $("#txaObservacion").val().trim();
				
				if(observacion==0){
					$( "#txaObservacion" ).addClass( "ui-state-error" );
				}
				
				$( "#txaObservacion" ).removeClass( "ui-state-error" );
				
				actualizar();
				$(this).dialog("close");
			}
		},
		close: function(){
			$(this).dialog("close");
			$(this).dialog("destroy");
		}
	});
	
	$("#tablaerror").dialog({
		autoOpen:false,
		width:640,
		hide: "clip",	
		open:function(event,ui){
		},//fin funcion open
		
		close:function(event,ui){
			$("#tablaerror tr td").html("");
		}
	});//fin dialog
	
});

function controlPersona(){
	var identificacion = $("#txtNumeroIdentificacion").val().trim();
	var banderaInformacion = "";
	
	nuevo();
	
	$("#txtNumeroIdentificacion").val(identificacion);
	
	//Verificar si el usuario digito algun valor en la identificacion
	if(identificacion==0){
		nuevo();
		return false;
	}
	
	
	var objPersona = fetchDataPersona(identificacion);
	
	//Verificar existen datos 
	if(typeof objPersona == "object" && objPersona.length>0 ){
		banderaInformacion = objPersona[0].identificacion+" - "+objPersona[0].pnombre + " " +
				objPersona[0].snombre + " " + objPersona[0].papellido + " " + objPersona[0].sapellido;
		
		$("#hidIdPersona").val(objPersona[0].idpersona);
	}else{
		banderaInformacion = "La persona no existe";
	}
	
	$("#lblDatosPersona").text(banderaInformacion);
	
	controlAfiliacion();
}

function controlAfiliacion(){
	var idPersona = $("#hidIdPersona").val();
	
	if(idPersona==0)return false;
	
	var objFiltro = {idpersona:idPersona};
	var objDataAfiliacion = fetchDataAfiliInact(objFiltro);
	var banderaHtml = "";
	
	//Verificar existen datos 
	if(typeof objDataAfiliacion == "object" && objDataAfiliacion.length>0 ){
		
		$("#tabAfiliacionInactiva tbody").html("");
		
		$.each(objDataAfiliacion,function(i,row){
			
			row.fecharetiro = row.fecharetiro.replace(/-/gi,"/");
			row.fechaingreso = row.fechaingreso.replace(/-/gi,"/");
			
			banderaHtml = '<tr id="trIdFormulario'+row.idformulario+'">'+
							'<td class="clsNit">'+row.nit+'</td>'+
							'<td>'+
								row.razonsocial+
								'<input type="hidden" name="hidIdEmpresa" value="'+row.idempresa+'"/>'+
							'</td>'+
							'<td>'+
								'<input type="text" name="txtFechaIngreso" id="txtFechaIngreso'+row.idformulario+'" value="'+row.fechaingreso+'" size="12" class="box1 element-required" readonly="readonly"/>'+
								'<input type="hidden" name="hidFechaIngreso" value="'+row.fechaingreso+'"/>'+
							'</td>'+
							'<td>'+
								'<input type="text" name="txtFechaRetiro" id="txtFechaRetiro'+row.idformulario+'" value="'+row.fecharetiro+'" size="12" class="box1 element-required" readonly="readonly"/>'+
								'<input type="hidden" name="hidFechaRetiro" value="'+row.fecharetiro+'"/>'+
							'</td>'+
							'<td>'+
								'<select name="cmbCargo" class="box1">'+
									htmlComboOptioCargo+
								'</select>'+
								'<input type="hidden" name="hidCargo" value="'+row.cargo+'"/>'+
							'</td>'+
							'<td>'+
								'<select name="cmbTipoFormulario" class="box1">'+ 
									htmlComboOptioTipoFormu+
								'</select>'+
								'<input type="hidden" name="hidTipoFormulario" value="'+row.tipoformulario+'"/>'+
							'</td>'+
							'<td>'+
								'<select name="cmbTipoAfiliacion" class="box1">'+
									htmlComboOptioTipoAfili+
								'</select>'+
								'<input type="hidden" name="hidTipoAfiliacion" value="'+row.tipoafiliacion+'"/>'+
							'</td>'+
							'<td>'+row.fechafidelidad+'</td>'+
							'<td>'+
								'<input type="checkbox" name="chkActivar"/>'+
							'</td>'+
						'</tr>';
			
			$("#tabAfiliacionInactiva tbody").append(banderaHtml); 
			
			//Ejecutar las acciones para adicionar los datepicker
			
			datePickerBetween("#txtFechaIngreso"+row.idformulario,"#txtFechaRetiro"+row.idformulario);
			
			//Seleccionar el tipo cargo, formulario y tipo afiliacion
			$("#tabAfiliacionInactiva #trIdFormulario"+row.idformulario+ " select[name='cmbCargo']").val(row.cargo);
			$("#tabAfiliacionInactiva #trIdFormulario"+row.idformulario+ " select[name='cmbTipoFormulario']").val(row.tipoformulario);
			$("#tabAfiliacionInactiva #trIdFormulario"+row.idformulario+ " select[name='cmbTipoAfiliacion']").val(row.tipoafiliacion);
			
		});
		
	}else{
		banderaHtml = "<tr><td colspan='9'>La persona no tiene afiliaciones</td></tr>";
		$("#tabAfiliacionInactiva tbody").html(banderaHtml); 
	}	
}

function actualizar(){
	
	//Validar campos obligatorios
	if(validaCampoFormu()>0)return false;
	
	var observacionActiva = $("#txaObservacion").val().trim();
	var objBandera = [];
	//Obtener las afiliaciones y campos que fueron actualizados
	$("#tabAfiliacionInactiva tbody tr").each(function(i,row){
		var idTr = "#"+$(this).attr("id");
		var idFormulario = idTr.replace("#trIdFormulario","");
		var nit =  $(idTr + " .clsNit").text().trim();
		var idEmpresa = $(idTr + " input[name='hidIdEmpresa']").val().trim();
		
		var fechaIngreso = $(idTr + " input[name='txtFechaIngreso']").val().trim();
		var fechaRetiro = $(idTr + " input[name='txtFechaRetiro']").val().trim();
		var cargo = $(idTr + " select[name='cmbCargo']").val();
		var tipoFormulario = $(idTr + " select[name='cmbTipoFormulario']").val();
		var tipoAfiliacion = $(idTr + " select[name='cmbTipoAfiliacion']").val();
		
		var fechaIngresoOld = $(idTr + " input[name='hidFechaIngreso']").val().trim();
		var fechaRetiroOld = $(idTr + " input[name='hidFechaRetiro']").val().trim();
		var cargoOld = $(idTr + " input[name='hidCargo']").val();
		var tipoFormularioOld = $(idTr + " input[name='hidTipoFormulario']").val();
		var tipoAfiliacionOld = $(idTr + " input[name='hidTipoAfiliacion']").val();
	
		var banderaString = "";
		var banderaCont = 0;
		var banderaActivar = "";
		
		var banderaFechaI = new Date(fechaIngreso);
		var banderaFechaF = new Date(fechaIngresoOld);
		
		if(banderaFechaI.getTime()!=banderaFechaF.getTime()){
			banderaString += " - FECHA INGRESO: "+ fechaIngresoOld;
			banderaCont++;
		}
		
		banderaFechaI = new Date(fechaRetiro);
		banderaFechaF = new Date(fechaRetiroOld);
		
		if(banderaFechaI.getTime()!=banderaFechaF.getTime()){
			banderaString += " - FECHA RETIRO: "+ fechaRetiroOld;
			banderaCont++;
		}
		if(cargo!=cargoOld){
			banderaString += " - CARGO: ";
			if(cargoOld==0){
				banderaString += "...";
			}else{
				banderaString += $(idTr + " select[name='cmbCargo'] option[value='"+cargoOld+"']").text();
			}
			//banderaString += " - CARGO: "+ $(idTr + " select[name='cmbCargo'] option[value='"+cargoOld+"']").text();
			banderaCont++;
		}
		if(tipoFormulario!=tipoFormularioOld){
			banderaString += " - TIPO FORMULARIO: ";
			if(tipoFormularioOld==0){
				banderaString += "...";
			}else{
				banderaString += $(idTr + " select[name='cmbTipoFormulario'] option[value='"+tipoFormularioOld+"']").text();
			}
			//banderaString += " - TIPO FORMULARIO: "+ $(idTr + " select[name='cmbTipoFormulario'] option[value='"+tipoFormularioOld+"']").text();
			banderaCont++;
		}
		if(tipoAfiliacion!=tipoAfiliacionOld){
			banderaString += " - TIPO AFILIACION: ";
			if(tipoAfiliacionOld==0){
				banderaString += "...";
			}else{
				banderaString += $(idTr + " select[name='cmbTipoAfiliacion'] option[value='"+tipoAfiliacionOld+"']").text();
			}
			//banderaString += " - TIPO AFILIACION: "+ $(idTr + " select[name='cmbTipoAfiliacion'] option[value='"+tipoAfiliacionOld+"']").text();
			banderaCont++;
		}
		
		//Verificar si la afiliacion se va activar
		if($(idTr + " input[name='chkActivar']").is(":checked")){
			banderaActivar = "SI";
		}else{
			banderaActivar = "NO";
		}
		
		//Asignar solo las afiliaciones modificadas
		if(banderaCont>0 || banderaActivar=="SI"){
			
			banderaString = observacionActiva+". DATOS ACTUALIZADOS PARA LA AFILIACION INACTIVA ->  NIT: " + nit + banderaString;
			
			cargo = cargo == "" ? "0" : cargo;
			tipoFormulario = tipoFormulario == "" ? "0" : tipoFormulario;
			tipoAfiliacion = tipoAfiliacion == "" ? "0" : tipoAfiliacion;
			
			objBandera[objBandera.length] = {
					id_formulario: idFormulario,
					id_empresa: idEmpresa,
					fecha_ingreso: fechaIngreso,
					fecha_retiro: fechaRetiro,
					cargo: cargo,
					tipo_formulario: tipoFormulario,
					tipo_afiliacion: tipoAfiliacion,
					activar: banderaActivar, 
					observacion_modifica: banderaString,
					observacion_activa: observacionActiva
				};
		}
	});

	if(objBandera.length==0){
		alert("Debe indicar los datos a modificar");
	}else{
		var objDatos = {
				id_persona: $("#hidIdPersona").val().trim(),
				datos_afiliacion:objBandera
			};
		
		$.ajax({
			url:urlActuaAfiliInact,
			type:"post",
			data:{datos:objDatos},
			dataType:"json",
			async:false,
			success: function(data){
				if(data.error==0){
					alert("La informacion se actualizo correctamente");
					nuevo();
				}else{
					//alert("Error al actualizar la informacion codigoerror:"+data.error);
					var	str = String(data.descripcion);
					var res = str.replace(/'/g, "|");  
					$('#tablaerror tr td').append(res);
					$('#tablaerror').dialog('open');
					
					//alert(data.descripcion);
				}
			}
		});
	}	
}

/**
 * METODO Encargado de preparar los option para el combo del tipo de formulario
 * 
 * @returns {String} Combo
 */
function fetchComboOptioTipoFormu(){
	var objFiltro = {iddefinicion:9} //[9][TIPOS DE FORMULARIO]Tipo definicion 
	var objData = fetchDataDetalDefin(objFiltro);
	var htmlOption = "<option value='0'>Seleccione...</option>";
	
	//Verificar existen datos 
	if(typeof objData == "object" && objData.length>0 ){
		$.each(objData,function(i,row){
			htmlOption += "<option value='"+row.iddetalledef+"'>"+row.detalledefinicion+"</option>";
		});
	}
	return htmlOption;
}

/**
 * METODO Encargado de preparar los option para el combo del tipo de afiliacion
 * 
 * @param idCombo id Del combo
 * @returns {String} Combo
 */
function fetchComboOptioTipoAfili(){
	var objFiltro = {iddefinicion:2} //[2][TIPOS DE AFILIADO]Tipo definicion 
	var objData = fetchDataDetalDefin(objFiltro);
	var htmlOption = "<option value='0'>Seleccione...</option>";
	
	//Verificar existen datos 
	if(typeof objData == "object" && objData.length>0 ){
		$.each(objData,function(i,row){
			htmlOption += "<option value='"+row.iddetalledef+"'>"+row.detalledefinicion+"</option>";
		});
	}
	return htmlOption;
}

/**
 * METODO Encargado de preparar los option para el combo del cargo
 * 
 * @returns {String} Combo
 */
function fetchComboOptioCargo(){
	var objFiltro = {iddefinicion:21} //[21][CARGOS]Tipo definicion 
	var objData = fetchDataDetalDefin(objFiltro);
	var htmlOption = "<option value='0'>Seleccione...</option>";
	
	//Verificar existen datos 
	if(typeof objData == "object" && objData.length>0 ){
		$.each(objData,function(i,row){
			htmlOption += "<option value='"+row.iddetalledef+"'>"+row.detalledefinicion+"</option>";
		});
	}
	return htmlOption;
}

/**
 * METODO Encargado de obtener los datos de la persona que se encuentra en la BD.
 * @param identificacion
 * @returns Array Datos de la persona
 */
function fetchDataPersona(identificacion){
	var resultado = [];
	$.ajax({
		url:urlBuscarPersona,
		type:"post",
		data:{v0:identificacion},
		dataType:"json",
		async:false,
		success: function(data){
			if(data!=0){
				resultado = data;
			}
		}
	});
	return resultado;
}

/**
 * METODO Encargado de obtener los datos de la persona que se encuentra en la BD.
 * @param identificacion
 * @returns Array Datos de la afiliacion
 */
function fetchDataAfiliInact(filtro){
	var resultado = [];
	$.ajax({
		url:urlAfiliacionInactiva,
		type:"post",
		data:{objDatosFiltro:filtro},
		dataType:"json",
		async:false,
		success: function(data){
			if(data!=0){
				resultado = data;
			}
		}
	});
	return resultado;
}

/**
 * METODO Encargado de obtener los datos del detalle definicion.
 * @param identificacion
 * @returns Array Datos de la persona
 */
function fetchDataDetalDefin(filtro){
	var resultado = [];
	$.ajax({
		url:urlDetalleDefinicion,
		type:"post",
		data:{objDatosFiltro:filtro},
		dataType:"json",
		async:false,
		success: function(data){
			if(data!=0){
				resultado = data;
			}
		}
	});
	return resultado;
}

function nuevo(){
	$("#tabAfiliacionInactiva tbody").html(""); 
	$("#hidIdPersona,#txaObservacion").val("");
	$("#lblDatosPersona").text("");
}

function datePickerBetween(selectorFechaI, selectorFechaF){
	$(selectorFechaI).datepicker({
		maxDate:"+0D",
		changeMonth: true,
		changeYear: true,
		onSelect: function(dateText, inst) {
			var lockDate = new Date($(selectorFechaI).datepicker('getDate'));
			$('input'+selectorFechaF).datepicker('option', 'minDate', lockDate);
		}
		});
	
		$(selectorFechaF).datepicker({
			maxDate:"+0D",
			changeMonth: true,
			changeYear: true
		}); 
}

/**
 * METODO: Valida los elementos requeridos del formulario
 * 
 * @returns int Numero de elementos vacios
 */
function validaCampoFormu(){
	//En este metodo no se validan los periodos a eliminar o actualizar
	
	var banderaError = 0;
	$( ".ui-state-error" ).removeClass( "ui-state-error" );
	$(".element-required").each(function(i,row){
		//Verifica que los campos esten visibles porque pueden ser periodos eliminados
		if($( this ).val() == 0 && $( this ).is(":visible")){
			$( this ).addClass( "ui-state-error" );
			banderaError++;
		}
	});
	return banderaError;
}
