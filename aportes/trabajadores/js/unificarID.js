/** VARIABLES **/
URL=src();
nueva=0;
tiempoFuera=30000;
cantRelaciones=0;
cantCuotasMonetarias09=0;
cantCuotasMonetarias14=0;
cantPaquetesEscolares=0;
cantCertificados=0;
idCertificados=0;
cantCausales=0;
cantSubsidio=0;
cantEmbargos=0;
cantPignoraciones=0;
cantDefunciones=0;


/** DOCUMENT READY **/
$(document).ready(function(){
	nuevo();
   
	shortcut.add("Shift+F",function() {		
		var url=URL+"aportes/trabajadores/consultaTrabajador.php";
	    window.open(url,"_blank");
	    },{
		'propagate' : true,
		'target' : document 
	});
});

/**
 * Funcion que permite iniciar
 */
function nuevo(){	
	limpiarCampos(0);
	nueva=1;
	cantRelaciones=0;
	cantCuotasMonetarias09=0;
	cantCuotasMonetarias14=0;
	cantPaquetesEscolares=0;
	cantCertificados=0;
	idCertificados=0;
	cantCausales=0;
	cantSubsidio=0;
	cantEmbargos=0;
	cantPignoraciones=0;
	cantDefunciones=0;
	$("#btnGuardar").show();
	$("#tblDatosEliminado").hide();
	$("#cmbIdTipoDocumentoCorrecto,#cmbIdTipoDocumentoError").val('1');
	$("table.tablero :input.ui-state-error,select.ui-state-error").removeClass("ui-state-error");
}

/**
 * Funcion que permite guardar
 */
function guardar(){
	$("#btnGuardar").hide();	
	checked=jQuery('input[name=chkEliminar]:checked').val();
	if(checked==0 || checked==1){
		var error=0;
		var idBeneEliminar=$("#txtIDError").val();
		var idBeneTrasladar=$("#txtIDCorrecto").val();
		var nomEliminar=''; var nomTrasladar='';
		
		if(esNumeroRespuesta(idBeneEliminar)==false){ $('#txtIdentificacionError').val('').trigger('blur'); }
		if(esNumeroRespuesta(idBeneTrasladar)==false){ $('#txtIdentificacionCorrecto').val('').trigger('blur'); }
		
		error=validarTexto($("#txtIdentificacionError"),error);
		error=validarTexto($("#txtIdentificacionCorrecto"),error);
		
		if(error>0){ $("#btnGuardar").show(); return;
		} else {
			if(idBeneEliminar==idBeneTrasladar){
				alert("Beneficiario a Trasladar y a Eliminar no puede ser el mismo");
				if(checked==0){ $('#txtIdentificacionError').val('').trigger('blur'); }
				else { $('#txtIdentificacionCorrecto').val('').trigger('blur'); }
				$("#btnGuardar").show(); return;
			}
			
			nomEliminar=$("#txtIdentificacionError").val() + ' - ' + $("#txtNombreCompletoTraeError").val() + ' - ' + $("#txtCapacidadTrabajoTraeError").val();		
			nomTrasladar=$("#txtIdentificacionCorrecto").val() + ' - ' + $("#txtNombreCompletoTraeCorrecto").val() + ' - ' + $("#txtCapacidadTrabajoTraeCorrecto").val();
		}
			
		if(checked==1){
			var tmp;
			tmp=nomEliminar;
			nomEliminar=nomTrasladar;
			nomTrasladar=tmp;
			tmp=idBeneEliminar;
			idBeneEliminar=idBeneTrasladar;
			idBeneTrasladar=tmp;
		}
		
		//Certificados para eliminar
		var idCertificados = "";
		var idCertificadosUnificar = "";
		$("#tbCertificado :checkbox").each(function(){
			if($(this).is(':checked'))
			{
				idCertificados += $(this).val()+","; 
			}
			else
			{
				idCertificadosUnificar += $(this).val()+",";
			}
		  			
		});
		
		idCertificados = idCertificados.substring(0,(idCertificados.length-1));
		idCertificadosUnificar = idCertificadosUnificar.substring(0,(idCertificadosUnificar.length-1));
				
		
		if(confirm("Se eliminaran todas las relaciones de: \n    " + nomEliminar + ",\nSe trasladaran a:\n    " + nomTrasladar)==true){
			$.ajax({
				url: 'procesoUnificarID.php',
				async: false,
				data: {v0:idBeneEliminar,v1:idBeneTrasladar,v2:cantRelaciones,v3:cantCuotasMonetarias09,
						v4:cantCuotasMonetarias14,v5:cantPaquetesEscolares,v6:cantCertificados,
						v10:cantCausales,v11:cantSubsidio,v12:cantEmbargos,
						v13:cantPignoraciones,v14:cantDefunciones,
						v15:idCertificados,v16:idCertificadosUnificar},		
				beforeSend: function(objeto){
		        	dialogLoading('show');
		        },        
		        complete: function(objeto, exito){
		        	dialogLoading('close');
		            if(exito != "success"){
		                alert("No se completo el proceso!");
		            }            
		        },
		        contentType: "application/x-www-form-urlencoded",
		        dataType: "json",
		        error: function(objeto, quepaso, otroobj){
		            alert("Pas� lo siguiente: "+quepaso);
		        },
		        global: true,
		        ifModified: false,
		        processData:true,		
				success: function(datoActualizacion){
					if(esNumeroRespuesta(datoActualizacion)){
						switch (datoActualizacion){
							case 1:
								alert("Ocurrio un error realizando el traslado de las relaciones");
								break;
							case 2:
								alert("Ocurrio un error realizando el traslado de las cuotas monetarias");
								break;
							case 3:
								alert("Ocurrio un error realizando el traslado de las cuotas monetarias actuales");
								break;
							case 4:
								alert("Ocurrio un error realizando el traslado de los paquetes escolares");
								break;
							case 5:
								alert("Ocurrio un error realizando el traslado de los certificados");
								break;
							case 6:
								alert("Ocurrio un error al actualizar los certificados");
								break;
							case 7:
								alert("Ocurrio un error eliminando el beneficiario");
								break;
							case 9:
								alert("Traslado se realizo con exito");
								break;
							case 10:
								alert("Ocurrio un error al actualizar las causales");
								break;
							case 11:
								alert("Ocurrio un error al actualizar el subsidio en revision");
								break;
							case 12:
								alert("Ocurrio un error al actualizar los embargos");
								break;
							case 13:
								alert("Ocurrio un error al actualizar las Pignoraciones");
								break;
							case 14:
								alert("Ocurrio un error al actualizar las Defunciones");
								break;
							case 15:
								alert("Ocurrio un error al eliminar los certificados erroneos");
								break;
						}
						observacion(idBeneTrasladar,1);
						return;
					} else {
						alert("Ocurrio un error realizando el traslado");
						return;
					}
				},
				timeout: tiempoFuera,
		        type: "GET"
			});				
		} else $("#btnGuardar").show();
	} else {
		alert("Seleccione un beneficiario a ELIMINAR");
		$("#btnGuardar").show();
		return;
	}
}

/**
 * Funcion que busca si la persona existe en nuestra base de datos
 * 
 * @param cmbtd 	Select que contiene el tipo de documento a buscar
 * @param txtd 		Text que contiene el numero de documento a buscar
 * @param tdnom 	Td donde es visible el nombre de la persona encontrada
 * @param tdcapt 	Td donde es visible la capacidad trabajo de la persona encontrada
 * @param nuevo 	Opcion que determina si se crea nueva persona si no existe 
 * @returns {persona}
 */
function buscarPersona(cmbtd, txtd, tdnom, tdcapt, nuevo, id){
	checked=jQuery('input[name=chkEliminar]:checked').val();
	if((txtd.get(0).id=="txtIdentificacionError" && checked==0) || (txtd.get(0).id=="txtIdentificacionCorrecto" && checked==1)){
		$("#tblDatosEliminado").hide();
		jQuery('input[name=chkEliminar]').removeAttr('checked');
	}
	var persona=null;	
	flag=7;
	tdnom.val('');
	tdcapt.val('');
	id.val('');
	txtd.removeClass("ui-state-error");
	
	if(validarTexto(txtd,0)>0){return null;}
	if(error=validarSelect(cmbtd,0)>0){return null;}
	
	var tipodoc=cmbtd.val();
	var doc =txtd.val();
	
	$.ajax({
		url: 'buscarPersona.php',
		async: false,
		data: {v0:tipodoc,v1:doc,v2:flag},		
		beforeSend: function(objeto){
        	dialogLoading('show');
        },        
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }            
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("Pas� lo siguiente en buscarPersona : "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,		
		success: function(datoPersona){
			if(datoPersona==0){
				alert("Error en la consulta!");
				txtd.val('');
				txtd.addClass("ui-state-error");
			} else if (datoPersona==1){
				id.val('');
				alert("No existe en nuestra base!");
				if(nuevo){
					validarIdentificacionInsert(cmbtd,txtd);					
					if(validarTexto(txtd,0)>0){ alert("Documento no valido!"); return null;}
					newPersonaSimple(cmbtd,txtd);
				} else {
					txtd.val('');
					txtd.addClass("ui-state-error");
				}
			} else {
				$.each(datoPersona,function(i,fila){
					$.ajax({
						url: 'buscarEsAfiliado.php',
						async: false,
						data: {v0:fila.idpersona},		
						beforeSend: function(objeto){
				        	dialogLoading('show');
				        },        
				        complete: function(objeto, exito){
				        	dialogLoading('close');
				            if(exito != "success"){
				                alert("No se completo el proceso!");
				            }            
				        },
				        contentType: "application/x-www-form-urlencoded",
				        dataType: "json",
				        error: function(objeto, quepaso, otroobj){
				            alert("Pas� lo siguiente en buscarEsAfiliado : "+quepaso);
				        },
				        global: true,
				        ifModified: false,
				        processData:true,		
						success: function(datoAfiliacion){
							if(datoAfiliacion==0){
								persona=fila;
								id.val(fila.idpersona);
								var nom = $.trim(fila.pnombre) + " " + $.trim(fila.snombre) + " " + $.trim(fila.papellido) + " " + $.trim(fila.sapellido);					
								tdnom.val(nom);	
								tdcapt.val($.trim(fila.capacidadtrabajo))				
								return;
							} else {
								alert("Solo ingrese datos de beneficiarios!");
								txtd.val('');
								txtd.addClass("ui-state-error");
							}							
						}
					});
				});
			}
		},
		timeout: tiempoFuera,
        type: "GET"
	});	
	
	return persona;
}

function traerDatosEliminar(valor){
	var idbene=0;
	var idbene1=0;
	cantRelaciones=0;
	cantCuotasMonetarias09=0;
	cantCuotasMonetarias14=0;
	cantPaquetesEscolares=0;
	cantCertificados=0;
	idCertificados=0;
	cantCausales=0;
	cantSubsidio=0;
	cantEmbargos=0;
	cantPignoraciones=0;
	cantDefunciones;
	
	if(valor==0){
		idbene=$("#txtIDError").val();
		idbene1=$("#txtIDCorrecto").val();		
	} else if (valor==1) {
		idbene=$("#txtIDCorrecto").val();
		idbene1=$("#txtIDError").val();
		
	}
	
	
	
	
	if(esNumeroRespuesta(idbene)){
		$("#tblDatosEliminado").show();
		traerRelacion(idbene);
		traerCuota(idbene);
		traerPaquete(idbene);
		traerCertificado(idbene);
		traerCertificadoError(idbene1);
		traerCausales(idbene);
		traerSubsidio(idbene);
		traerEmbargos(idbene);
		traerPignoraciones(idbene);
		traerDefunciones(idbene);
	} else {
		$("#tblDatosEliminado").hide();
		jQuery('input[name=chkEliminar]').removeAttr('checked');
	}
}

function observacion(id,tipo){
	var URL=src();
	$("div[name='div-observaciones-tab']").dialog("destroy");
	$("textarea[name='observacionUnificacion']").val("");
	$("div[name='div-observaciones-tab']").dialog({
		  modal:true,
		  width:520,
		  resizable:false,
		  draggable:false,
		  closeOnEscape:false,
		  open:function(){
			  $(this).prev().children().hide();//Oculto la barra de titulo
			  $(this).prev().html("OBSERVACIONES...");//Pongo un nuevo titulo
			  $("textarea[name='observacionUnificacion']").focus();
			  },
		  buttons:{
		    'Guardar':function(){
		    	var comentario='Unificacion'+': '+id;
		    	var observacioncaptura=$("textarea[name='observacionUnificacion']").val();
				var observacion=comentario+' - '+observacioncaptura;
				var observacionSinEspacios = observacion.replace(/^\s+|\s+$/g,"");
				//Si observacion tiene algun valor, se guarda de lo contrario no pasa nada
			    if(observacionSinEspacios!=""){
				 $.ajax({
					   url:URL+"phpComunes/pdo.insert.notas.php",
					   type:"POST",
					   data:{v0:id,v1:observacion,v2:tipo},
					   success:function(data){
					    $("[name='rtaObservacion']").fadeIn();//el mensaje esta oculto en la capa div-observacion
						$("div[name='div-observaciones-tab']").dialog("close");
					   }
			     });//ajax
				 alert("Observacion guardada"); 
				}//if observacion
				else{
					alert("No se pudo guardar la observaci\xf3n");
				}
			 }//guardar
		  }//buttons
		  });   
} 

function traerRelacion(idp){
	$("#tbRelacion").empty();	
	if(esNumeroRespuesta(idp)){
		$.ajax({
	        url: "buscarRelacionBeneficiario.php",
	        async:true,
	        data:{v0:idp},
	        dataType: "json",
	        success: function(datos){
	        	if(datos==0){
	    			//alert("No hay relaciones de convivencia!");
	    			return false;
	    		}	
	    		$.each(datos,function(i,f){
	    			cantRelaciones++;
	    			nom = f.identificacion + ' - ' + f.Afiliado;
					$("#tbRelacion").append("<tr><td>"+nom+"</td><td><center>"+f.Beneficiario+"</center></td><td><center>"+f.detalledefinicion+"</center></td><td><center>"+f.estado+"</center></td><td><center>"+f.giro+"</center></td></tr>")
	    			return;
	    			})
	        },
	        timeout: tiempoFuera,
	        type: "GET"
	 });
	}	
}	

function traerCuota(idp){
	$("#tbCuotaMonetaria").empty();	
	if(esNumeroRespuesta(idp)){
		$.ajax({
	        url: "buscarCuotasBeneficiario.php",	        
	        data:{v0:idp},	        
	        dataType: "json",
	        success: function(datos){
	        	if(datos==0){	    			
	    			return false;
	    		}	
	    		$.each(datos,function(i,f){
	    			if(f.tabla=='a09') cantCuotasMonetarias09++;
	    			else if(f.tabla=='a14') cantCuotasMonetarias14++;
	    			nom = f.pnombre + " " +f.snombre +" "+ f.papellido + " " + f.sapellido;
					$("#tbCuotaMonetaria").append("<tr><td>"+nom+"</td><td><center>"+f.periodo+"</center></td></tr>")
	    			return;
	    			})
	        },
	        timeout: tiempoFuera,
	        type: "GET"
	 });
	}	
}	

function traerPaquete(idp){
	$("#tbPaqueteEscolar").empty();	
	if(esNumeroRespuesta(idp)){
		$.ajax({
	        url: "buscarPaquetesBeneficiario.php",	        
	        data:{v0:idp},
	        dataType: "json",
	        success: function(datos){
	        	if(datos==0){
	    			//alert("No hay relaciones de convivencia!");
	    			return false;
	    		}	
	    		$.each(datos,function(i,f){	  
	    			cantPaquetesEscolares++;
	    			nom = f.pnombre + " " +f.snombre +" "+ f.papellido + " " + f.sapellido;
					$("#tbPaqueteEscolar").append("<tr><td>"+nom+"</td><td><center>"+f.anno+"</center></td></tr>")
	    			return;
	    			})
	        },
	        timeout: tiempoFuera,
	        type: "GET"
	 });
	}	
}	

function traerCertificado(idp){
	$("#tbCertificado").empty();
	var contar=0;
	if(esNumeroRespuesta(idp)){
		$.ajax({
	        url: "buscarCertificadosBeneficiario.php",	        
	        data:{v0:idp},
	        dataType: "json",
	        success: function(datos){
	        	if(datos==0){
	    			//alert("No hay relaciones de convivencia!");
	    			return false;
	    		}	
	    		$.each(datos,function(i,f){	
	    			contar++;
	    			cantCertificados++;	
					$("#tbCertificado").append("<tr><td>"+f.detalledefinicion+"</td>" +
					"<td><center>"+f.estado+"</center></td><td><center>"+f.periodoinicio+"</center></td>" +
					"<td><center>"+f.periodofinal+"</center></td><td><center>"+f.fechapresentacion+"</center></td>" +
					"<td><center><input type='checkbox' name='checkEliminar"+contar+"' id='checkEliminar"+contar+"' value='"+f.idcertificado+"'></center></td></tr>")
	    			return;
	    			})
	        },
	        timeout: tiempoFuera,
	        type: "GET"
	 });
	}	
}
function traerCertificadoError(idp1){
	$("#tbCertificadoError").empty();	
	
	if(esNumeroRespuesta(idp1)){
		$.ajax({
	        url: "buscarCertificadosBeneficiario.php",	        
	        data:{v0:idp1},
	        dataType: "json",
	        success: function(datos){
	        	if(datos==0){
	    			//alert("No hay relaciones de convivencia!");
	    			return false;
	    		}	
	    		$.each(datos,function(i,f){	  
	    			
					$("#tbCertificadoError").append("<tr><td>"+f.detalledefinicion+"</td>" +
					"<td><center>"+f.estado+"</center></td><td><center>"+f.periodoinicio+"</center></td>" +
					"<td><center>"+f.periodofinal+"</center></td><td><center>"+f.fechapresentacion+"</center></td></tr>")
	    			return;
	    			})
	        },
	        timeout: tiempoFuera,
	        type: "GET"
	 });
	}	
}

function traerCausales(idp){
	$("#tbCausales").empty();	
	if(esNumeroRespuesta(idp)){
		$.ajax({
	        url: "buscarCausalesBeneficiario.php",	        
	        data:{v0:idp},
	        dataType: "json",
	        success: function(datos){
	        	if(datos==0){
	    			//alert("No hay relaciones de convivencia!");
	    			return false;
	    		}	
	    		$.each(datos,function(i,f){	  
	    			cantCausales++;	 
					$("#tbCausales").append("<tr><td>"+f.periodo+"</td><td><center>"+f.detalledefinicion+"</center></td></tr>")
	    			return;
	    			})
	        },
	        timeout: tiempoFuera,
	        type: "GET"
	 });
	}	
}
function traerSubsidio(idp){
	$("#tbSubsidio").empty();	
	if(esNumeroRespuesta(idp)){
		$.ajax({
	        url: "buscarSubsidioBeneficiario.php",	        
	        data:{v0:idp},
	        dataType: "json",
	        success: function(datos){
	        	if(datos==0){
	    			//alert("No hay relaciones de convivencia!");
	    			return false;
	    		}	
	    		$.each(datos,function(i,f){	  
	    			cantSubsidio++;
	    			nom = f.pnombre + " " +f.snombre +" "+ f.papellido + " " + f.sapellido;
	    			$("#tbSubsidio").append("<tr><td>"+nom+"</td><td><center>"+f.idbeneficiario+"</center></td><td><center>"+f.nit+"</center></td><td><center>"+f.periodo+"</center></td><td><center>"+f.procesado+"</center></td></tr>")
	    			return;
	    			})
	        },
	        timeout: tiempoFuera,
	        type: "GET"
	 });
	}	
}
function traerEmbargos(idp){
	$("#tbEmbargos").empty();	
	if(esNumeroRespuesta(idp)){
		$.ajax({
	        url: "buscarEmbargosBeneficiario.php",	        
	        data:{v0:idp},
	        dataType: "json",
	        success: function(datos){
	        	if(datos==0){
	    			//alert("No hay relaciones de convivencia!");
	    			return false;
	    		}	
	    		$.each(datos,function(i,f){	  
	    			cantEmbargos++;
	    			$("#tbEmbargos").append("<tr><td>"+f.conyugue+"</td><td><center>"+f.beneficiario+"</center></td></tr>")
	    			return;
	    			})
	        },
	        timeout: tiempoFuera,
	        type: "GET"
	 });
	}	
}
function traerPignoraciones(idp){
	$("#tbPignoraciones").empty();	
	if(esNumeroRespuesta(idp)){
		$.ajax({
	        url: "buscarPignoracionesBeneficiario.php",	        
	        data:{v0:idp},
	        dataType: "json",
	        success: function(datos){
	        	if(datos==0){
	    			//alert("No hay relaciones de convivencia!");
	    			return false;
	    		}	
	    		$.each(datos,function(i,f){	  
	    			cantPignoraciones++;
	    			nom = f.pnombre + " " +f.snombre +" "+ f.papellido + " " + f.sapellido;
	    			$("#tbPignoraciones").append("<tr><td>"+nom+"</td><td><center>"+f.idpignoracion+"</center></td><td><center>"+f.periodo+"</center></td><td><center>"+f.fechapignoracion+"</center></td><td><center>"+f.valorpignorado+"</center></td></tr>")
	    			return;
	    			})
	        },
	        timeout: tiempoFuera,
	        type: "GET"
	 });
	}	
}
function traerDefunciones(idp){
	$("#tbDefunciones").empty();	
	if(esNumeroRespuesta(idp)){
		$.ajax({
	        url: "buscarDefuncionesBeneficiario.php",	        
	        data:{v0:idp},
	        dataType: "json",
	        success: function(datos){
	        	if(datos==0){
	    			//alert("No hay relaciones de convivencia!");
	    			return false;
	    		}	
	    		$.each(datos,function(i,f){	  
	    			cantDefunciones++;
	    			$("#tbDefunciones").append("<tr><td>"+f.Beneficiario+"</td><td><center>"+f.Muerto+"</center></td><td><center>"+f.Tercero+"</center></td><td><center>"+f.fechadefuncion+"</center></td></td><td><center>"+f.detalledefinicion+"</center></td></td></tr>")
	    			return;
	    			})
	        },
	        timeout: tiempoFuera,
	        type: "GET"
	 });
	}	
}