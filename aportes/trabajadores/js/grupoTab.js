var URL=src();
var existe=0;
var idnb=0;
var idbeneficiario=0;//para actualziar estado y giro
var idConyugueA=0;
var chequeado1=1;
var chequeado2=1;
var idPersona;
var fnace=0;
var msg="";
var idtipcert;
var idcert;
var idrelacion=0;
var p=0;
var cedulaPadreBeneficiario=0;
var rutaDocumentos='';
var nombreCorto='';
var idProfesion='';
var idcony=0;
var dataRelaciones=null;
var txtHiddenSexo="txtHiddenSexo";
var idCertiDepenEcono = 4551; //certificado dependencia economica de HIJASTROS
var objctrdocumentos;  // objeto que almacena el control de documentos del beneficiario


//Evento para actualizar datos de beneficiarios y conyuges ACTUALIZA BASICO
$("body").delegate("label[name='labelActualizaBasico']","click",function(){
	var idpersonaLabel=parseInt($(this).text());
    actualizaBasico(idpersonaLabel);
});





//Boton modificar beneficiario
$("[name='actBen']").live("click",function(){
	 //validarComboParentesco();
	//Id global del beneficiario seleccionado
	 idbeneficiario=$(this).parent().parent().children(":eq(0)").text();
	 var temp=($(this).parent().parent().children(":eq(5)").text()).split(' ');
	 cedulaPadreBeneficiario=temp[0];
	 idrelacion=$(this).parent().parent().children(":eq(15)").text();	 
	//Enviar parametros
	 p=$.trim($(this).parent().parent().children(":eq(4)").text());
	 buscarBeneficiarioUpdate(idbeneficiario,p,cedulaPadreBeneficiario,idrelacion);
});

function validarTipoFormulario(){
	var tf=$("#tipoAfiliacion3").val();
	var idp=$("#idPersona").val();
	var idParentesco = $("#parentesco").val();
	
	if(tf==48 || tf==49){
		validarEdad(0);
		if(tf==48){
			$( "#lblAsignacion,#divAsignacion" ).show();
		} else {
			$( "#lblAsignacion,#divAsignacion" ).hide();
		}
	} else {
		$( "#lblAsignacion,#divAsignacion" ).hide();
		$("#filaDiscapacidad,#filaEscolaridad,#filaUniversidad,#filaSupervivencia,#filaDependenciaEconomica,#cerVigencia,#cerVigenDepenEconoHija").hide();
		$("#fDiscapacidad,#fEscolaridad,#fUniversidad,#fSupervivencia,#fDependenciaEconomica,#pInicialV3,#pFinalV3,#txtPerioIniciVigenHijastro3,#txtPerioFinalVigenHijastro3").val('');
		$("#cDiscapacidad,#cEscolaridad,#cUniversidad,#cSupervivencia,#cDependenciaEconomica,#vigencia1,#vigencia2,#radVigenciaHijastro1,#radVigenciaHijastro2").attr("checked",false);
	}
	/*$.ajax({
		url:URL+'phpComunes/buscarTipoFormulario.php', 
		type: "POST",
		data: {v0:idp},
		async: false,
		dataType: "json",
		success:function(data){
			$.each(data,function(i,fila){    			
				var tipoafi=$("#tipoAfiliacion3").val();
				if((fila.tipoformulario==48 && tipoafi!=48) || (fila.tipoformulario==49 && tipoafi!=49))
					{
					document.getElementById("tipoAfiliacion3").selectedIndex = 0;
					alert("El tipo de afiliacion no es  igual a la del trabajador");
					}
       		});
		}
	})
	if(tf==48 || tf==49){
		validarEdad(0);
		if(tf==48){
			$( "#lblAsignacion,#divAsignacion" ).show();
		} else {
			$( "#lblAsignacion,#divAsignacion" ).hide();
		}
	} else {
		$( "#lblAsignacion,#divAsignacion" ).hide();
		$("#filaDiscapacidad,#filaEscolaridad,#filaUniversidad,#filaSupervivencia,#cerVigencia").hide();
		$("#fDiscapacidad,#fEscolaridad,#fUniversidad,#fSupervivencia,#pInicialV3,#pFinalV3").val('');
		$("#cDiscapacidad,#cEscolaridad,#cUniversidad,#cSupervivencia,#vigencia1,#vigencia2").attr("checked",false);
	}*/
}
	
function validarEdad(formu){
	if( formu==0 && ( $("#tipoAfiliacion3").val() == '0' ) ) { return false; }
	
	if(formu == 1){
		var fechaNaceValEdad = $("#fecNac3Upd").val();
		var capacidad = $("#capTrabajoUpd").val();
		var parentesco = $("#parentescoUpd").val();
	} else if( formu == 0 ) {
		$("#filaDiscapacidad,#filaEscolaridad,#filaUniversidad,#filaSupervivencia,#filaDependenciaEconomica,#cerVigencia,#cerVigenDepenEconoHija").hide();
		$("#fDiscapacidad,#fEscolaridad,#fUniversidad,#fSupervivencia,#fDependenciaEconomica,#pInicialV3,#pFinalV3,#txtPerioIniciVigenHijastro3,#txtPerioFinalVigenHijastro3").val('');
		$("#cDiscapacidad,#cEscolaridad,#cUniversidad,#cSupervivencia,#cDependenciaEconomica,#vigencia1,#vigencia2,#radVigenciaHijastro1,#radVigenciaHijastro2").attr("checked",false);
			
		var fechaNaceValEdad = $("#fecNacHide3").val();
		var capacidad = $("#capTrabajo").val();
		var parentesco = $("#parentesco").val();
	}
	
	salir=!validarFechaNaceBeneficiario(parentesco,$("#fecNacHide3"),0);
	if(salir){ return false; }
	
	var fechaNacimientoValEdad = new Date(fechaNaceValEdad);
	var tiempoFechaNace = fechaNacimientoValEdad.getTime();
	var fechaActualValEdad = new Date();
	var tiempoFechaActual = fechaActualValEdad.getTime();
	
	if( capacidad == 'N' ) {
		if ( parentesco == 36 ) {
			var tiempoMS60Anios = 60 * 60 * 24 * (365) * 1000 * 60;
			var tiempoMS60AnioConVisiestos = tiempoMS60Anios + ( 60 * 60 * 24 * 1000 * (60 / 4));
			if(tiempoFechaNace  >= (tiempoFechaActual - tiempoMS60AnioConVisiestos)){
				alert("La Edad Minima de un Afiliado Padre es 60 A\u00F1os");
				if (formu == 1){ 
					$("#fecNac3Upd").val(""); 	
				} else { 
					$("#fecNacHide3").val(""); 
				}			
				return false;
			} else if ( $("#tipoAfiliacion3").val()=='48' ) {
				$("#filaSupervivencia,#cerVigencia").show();
			}
		} else if ( parentesco == '35' || parentesco == '38' || parentesco == '37' ) {
			var tiempoMS19Anios = 60 * 60 * 24 * 365 * 1000 * 19;
			var tiempoMS19AnioConVisiestos = tiempoMS19Anios + (60 * 60 * 24 * 1000 * (19 / 4));					
			if(tiempoFechaNace  <= (tiempoFechaActual - tiempoMS19AnioConVisiestos)){					
				if(formu == 1){
					alert("La Edad Maxima de un Afiliado Hijo/Hijastro/Hermano es Hasta 19 A\u00F1os");
					$("#fecNac3Upd").val("");
				}else{
					alert("La Edad Maxima de un Afiliado Hijo/Hijastro/Hermano es Hasta 19 A\u00F1os");
					$("#fecNacHide3").val("");							
				}
			} else if ( $("#tipoAfiliacion3").val()=='48' ) {
				if(formu == 0) {
					//Permitir traer certificado desde los 11 anios
					var tiempoMS12Anios = 60*60*24*365*1000*11;
					var tiempoMS12AnioConVisiestos = tiempoMS12Anios + ( 60 * 60 * 24 * 1000 * (11 / 4) );
					if(tiempoFechaNace  <= (tiempoFechaActual - tiempoMS12AnioConVisiestos)){
						$("#filaEscolaridad,#filaUniversidad,#cerVigencia").show();
					}
				}												
			}
		}
	} else if ( $("#tipoAfiliacion3").val()=='48' ) {
		if(formu == 0) { $("#filaSupervivencia,#cerVigencia").show(); }
	}
	
	if(parentesco==38 && formu == 0){
		$("#filaDependenciaEconomica,#cerVigenDepenEconoHija").show();
	}
}

function validarComboParentesco(){
	$("#identificacion3").val('').trigger('blur');
	//setea la visiblidad de la fila #padreBiologico	
	$("#padreBiologico").hide();
	$("#nombreBiologico").hide();
	$("#tipoIdPadreBiol").val('0');
	$("#IdPadreBiol").val("");
	$("#nomBiologico").html(" ");
	
	$("#padreBiologicoUpd").hide();
	$("#nombreBiologicoUpd").hide();
	$("#tipoIdPadreBiolUpd").val('0');
	$("#IdPadreBiolUpd").val("");
	$("#nomBiologicoUpd").html(" ");
	
	$("#filaDependenciaEconomica,#cerVigenDepenEconoHija").hide();
	if($("#parentesco").val()=="38"){
		$("#filaDependenciaEconomica,#cerVigenDepenEconoHija").show();
	}
	
	if( ($("#parentesco").val()=="36") || ($("#parentesco").val()=="37") ){
		$("#cedMama").attr("disabled",true);
		$("#cedMama").val(0);
	}else{
			//cod 38=hijastro
		if($("#parentesco").val()=='38'){
			confirma = confirm("El HIJASTRO TIENE PADRE BIOLOGICO ?");
				if(confirma){
					$("#padreBiologico").show(); 
					$("#tipoIdPadreBiol").val("1");
					$("#nombreBiologico").show();					
					$("#IdPadreBiol").val("");
				}
				$("#cedMama").trigger("change");
			}
	$("#cedMama").attr("disabled",false);
	}
	
	if( ($("#parentescoUpd").val()=="36") || ($("#parentescoUpd").val()=="37") ){
		$("#cedMamaUpd").attr("disabled",true);
		$("#cedMamaUpd").val(0);
	}else{
			//cod 38=hijastro
		if($("#parentescoUpd").val()=='38'){
			confirma = confirm("El HIJASTRO TIENE PADRE BIOLOGICO ? ");
			if(confirma){
				$("#padreBiologicoUpd").show(); 
				$("#nombreBiologicoUpd").show();
				$("#tipoIdPadreBiolUpd").val(0);
				$("#IdPadreBiolUpd").val("");
			}
			$("#cedMamaUpd").trigger("change");	
		}
		$("#cedMamaUpd").attr("disabled",false);
	}
	
	//validarCertificados();
}	

function validarCertificados(){
	if($("#fecNacHide3").val()=="") return false;
	idPersona=parseInt($("#idPersona").val());
	var continuar=1;
	fnace=0;
	if(isNaN(idPersona)){
		alert("No hay ID de la persona!");
		limpiarBene();		
		return false;
	}
	
	$.ajax({
		url:URL+"phpComunes/pdo.b.fnaceafi.php",
		type:'POST',
		dataType:'json',
		data :{v0:idPersona},
		async:false,
		success:function(data){
			if(data==0){
				alert("No hay fecha de nacimiento del afiliado, por favor actualice primero esta informacion!");				
				limpiarBene();
				continuar=0;
			} else {
				fnace=data;
			}
		}
	});
	
	if(continuar==0) return false;
	
	var fx=fnace.split('-');
	var ax=fx[0];
	var mx=fx[1];
	var dx=fx[2];
	fnace=mx+"/"+dx+"/"+ax;
	fx=$("#fecNacHide3").val().split('/');
	ax=fx[0];
	mx=fx[1];
	dx=fx[2];
	var fbenef=mx+"/"+dx+"/"+ax;
	var dd=diferenciaFechas(fnace,fbenef);
	if($("#parentesco").val()==35){
		if((dd >= 0)  ){
			alert("El beneficiario es Mayor que el afiliado?, por favor revise las fechas de nacimiento!");
			$("#fecNacHide3").val('');
			return false;
		}
	}
	if($("#parentesco").val()==36){
		if((dd <= 0)  ){
			alert("El beneficiario es Menor que el afiliado?, por favor revise las fechas de nacimiento!");
			$("#fecNacHide3").val('');
			return false;
		}
	}
	
	$("#filaDiscapacidad").hide();
	$("#filaEscolaridad").hide();
	$("#filaUniversidad").hide();
	$("#filaSupervivencia").hide();
	$("#filaDependenciaEconomica").hide();
	$("#fEscolaridad,#fUniversidad,#fSupervivencia,#fDiscapacidad,#fDependenciaEconomica").val('');
	$("#cEscolaridad,#cUniversidad,#cSupervivencia,#cDiscapacidad,#cDependenciaEconomica").attr("checked",false);	
	var f=new Date();
	var fechaOculta=0;
	fechaOculta=$("#fecNacHide3").val();
	var ano=f.getFullYear();
	var anoFechaOculta=fechaOculta.slice(0,4);
	var edadB=parseInt(ano)-parseInt(anoFechaOculta);
	//--------PREGUNTAR PRIMERO SI ES SUBSIDIO (48)
	if($("#tipoAfiliacion3").val()=='48'){
		if(pa=="38"){
			$("#filaDependenciaEconomica").show();
		}
		
		if($("#capTrabajo").val()=='I'){
			$("#filaDiscapacidad").show();
			return false;
		}
		if($("#parentesco").val()=="36"){
			if(edadB>=60){
				$("#filaSupervivencia").show();
				return false;
			}
		}
		var pa=$("#parentesco").val();
		if( (pa=="35") || (pa=="37") || (pa=="38") ) {
			if(edadB>19){
				$("#tipoAfiliacion3").val(49);
				return false;
			}else{
				if(edadB>11){
					$("#filaEscolaridad").show();
					$("#filaUniversidad").show();
				}
			}
			
			
		}
	}
}

function buscarRelaciones(){
	$("#tabTableRelaciones tbody tr").remove();
	$("#beneficiarios #cedMama option:not(':first',:eq(1))").remove();
	$("#modificarBeneficiarios #cedMamaUpd option:not(':first',:eq(1))").remove();
	idPersona=$("#idPersona").val();
	dataRelaciones=null;
	
	$.ajax({
		url:URL+"phpComunes/pdo.buscar.relaciones2.php",
		async: false,
		data :{v0:idPersona},		
		beforeSend: function(objeto){
        	dialogLoading('show');
        },        
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }            
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("En buscarRelaciones Paso lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
		success:function(data){			
			if(data==0){
				msg="El trabajador NO tiene relaciones de convivencias.\n\r";
				return false;
			} else {			
				var cadena="";			
				dataRelaciones=data;
				
				$.each(data,function(i,fila){
					idr=fila.idrelacion;
					idc=fila.idconyuge; 
					numC=fila.identificacion;						
					nombreC=$.trim(fila.pnombre)+" "+$.trim(fila.snombre)+" "+$.trim(fila.papellido)+" "+$.trim(fila.sapellido);
					var salario =(!fila.salario) ? "" : formatCurrency(fila.salario);
					conviven=fila.conviven;
					//Insertamos las convivencias en la tablas
					if(conviven=='S')
						par=idr+",0,"+idc;
					else
						par=idr+",1,"+idc;					
					
					$("#beneficiarios #cedMama").append("<option value='"+numC+"' name='"+idc+"'>"+numC+"</option>");
					$("#modificarBeneficiarios #cedMamaUpd").append("<option value='"+numC+"' name='"+idc+"'>"+numC+"</option>");					
					cadena+="<tr>" +
		   						"<td style='text-align:center'>" + fila.codigo + "</td>" +
		   						"<td style='text-align:right' >" + fila.identificacion + "</td>" + 
		   						"<td style='text-align:left' >" + idc + " - " + nombreC + "</td>" +
		   						"<td style='text-align:center' >" + 
		   							"<label style=cursor:pointer onclick=cambiarC("+par+");>"+fila.conviven+"</label>" +
		   						"</td>" +
		   						"<td style='text-align:center' >" + fila.fechanacimiento + "</td>" +
		   						"<td style='text-align:center' >" + fila.fechasistema + "</td>" +
		   						"<td style='text-align:center' >" + fila.labora + "</td>" +
		   						"<td style='text-align:center' >" + salario + "</td>" +
		   						"<td style='text-align:center' >" + fila.fechaingreso + "</td>" +
		   						"<td style='text-align:center'>" +
		   							"<img style='cursor:pointer' src='"+URL+"imagenes/menu/modificar.png' onclick=buscarConyugueModificar("+idc+"); title='Modificar Conyugue' />" +
								"</td>" +
							"</tr>";					
				});//each
				$("#tabTableRelaciones tbody").append(cadena);
			}
		},
		type: "POST"
	});
}

function buscarGrupo(){
	//BUSCAMOS LOS BENEFICIARIO DE ACUERDO A LAS RELACIONES DE CONVIVENCIA
	$.ajax({
		url:URL+"phpComunes/pdo.buscar.grupo2.php",
		async: false,
		data :{v0:idPersona},		
		beforeSend: function(objeto){
        	dialogLoading('show');
        },        
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }            
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("En buscarGrupo Paso lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
		success:function(data){
			$("#tabTableGrupo tbody tr").remove();
			if(data){
				var cadena="";
				$.each(data,function(i,fila){
					idrel=fila.idrelacion;
					idb=fila.idbeneficiario;
					idparent=fila.idparentesco;
					idc=fila.idconyuge;
					nombreB=$.trim(fila.pnombre)+" "+$.trim(fila.snombre)+" "+$.trim(fila.papellido)+" "+$.trim(fila.sapellido);
					nombreC=$.trim(fila.pnombreC)+" "+$.trim(fila.snombreC)+" "+$.trim(fila.papellidoC)+" "+$.trim(fila.sapellidoC);
					
					identificacionConyuge=fila.identificacionConyuge;								
					if(fila.identificacionConyuge != ""){	
						identificacionConyuge="<a class='dialogoEmergente' >" +  identificacionConyuge + "<span>" + nombreC + "</span></a>"; 
					}
					motivo="<a class='dialogoEmergente' >" +  fila.estadoAfiliacion + "<span>" + fila.fechaEstadoAfiliacion + " " + fila.motivoInactivo + "</span></a>";
					fechagiro="<a class='dialogoEmergente' >" +  fila.giro + "<span>" + fila.fechaGiroAfiliacion + "</span></a>";
					
					estadoBeneficiario=fila.fechaEstadoBeneficiario;
					if(fila.estadoBeneficiario=='M'){									
						if ( fila.tipoestado == 0 ) {
							estadoBeneficiario = estadoBeneficiario + " Por Subsidio Funebre";
						} else if ( fila.tipoestado == 1 ) {
							estadoBeneficiario = estadoBeneficiario + " Por Proceso Cruce Registraduria";
						} else if ( fila.tipoestado == 2 ) {
							estadoBeneficiario = estadoBeneficiario + " Sin Subsidio Funebre";
						} 									
					}
					
					
					cadena+="<tr>" +
								"<td style='text-align:center' >" + fila.codigo + "</td>" +
								"<td style='text-align:right' ><label style='cursor:pointer;text-decoration:underline;' onclick=actualizaBasico2(" + idb + ",'" + fila.giro + "','" + fila.idparentesco + "','" + fila.discapacitado + "'); ><b>" + fila.identificacionBeneficiario + "</b></label></td>" +
							    "<td style='text-align:left' >" + idb + " - " + nombreB + "</td>" +
							    "<td style='text-align:center' >" + fila.parentesco + "</td>" +
							    "<td style='text-align:center' ><a class='dialogoEmergente' >" + fila.estadoBeneficiario + "<span>" + estadoBeneficiario + "</span></a></td>" +	    
							    "<td style='text-align:center' >" + fila.fechanacimiento + "</td>" +
							    "<td style='text-align:center' >" + Math.floor(fila.edad) + "</td>" +
							    "<td style='text-align:center' >" + fila.fechaafiliacion + "</td>" +
							    "<td style='text-align:center' ><a style='cursor: pointer;' onclick=mostrarDatosCertificados('" + idb + "') >" + fila.fechapresentacion + "</a></td>" +
							    "<td style='text-align:center' >" + fila.fechaasignacion + "</td>" +
							    "<td style='text-align:center' >" + identificacionConyuge + "</td>" +
							    "<td style='text-align:center' ><label style=cursor:pointer onclick=actualizarEstadoGiroBeneficiarioNuevo(" + idrel+",'" + fila.giro + "','" + fila.fechaasignacion + "',false,'" + fila.discapacitado + "','" + idparent+ "','" + fila.fechanacimiento + "','" + idb + "','" + fila.estadoAfiliacion+"','" + fila.estadoBeneficiario +"','" + fila.pnombre.replace(/\s/g,"_") +"','" + fila.papellido.replace(/\s/g,"_") + "');>" + fechagiro + "</td>" +
							    "<td style='text-align:center' >" + fila.discapacitado + "</td>" +
							    "<td style='text-align:center' >" + fila.embarga + "</td>" +
							    "<td style='text-align:center' ><label style=cursor:pointer onclick=actualizarEstadoBeneficiario(" + idrel + "," + idb + "," + idparent + ",'" + fila.estadoAfiliacion+"','" + fila.estadoBeneficiario + "','" + fila.giro + "','" + fila.discapacitado + "','" + fila.fechanacimiento + "','" + idc + "');>" + motivo + "</label></td>" +
							    "<td style='text-align:center' ><img style='cursor:pointer' src='"+URL+"imagenes/menu/modificar.png' id='actBen"+idb+"' title='Modificar Beneficiario' onclick=buscarBeneficiarioUpdate(" + idb + ",'" + fila.parentesco + "','" + fila.identificacionConyuge + "'," + fila.idrelacion + "); /></td>"
					    	"</tr>";					
				});//each
				
				con=data.length;
				$("#tabTableGrupo tbody").append(cadena);
			} else {
				msg+="El trababajador NO tiene beneficiarios.";
				MENSAJE(msg);
				return false;
			}			
		},//succes
		type: "POST"
	});//ajax beneficiarios		
}
	
function buscarPersona(num){
	limpiarConyuge();
	num=$.trim(num);
	if(num.length==0){return false;}
	if($("#conviven").val()==0){
		alert("Seleccione tipo de convivencia!");
		return false;
	}
	var conviven=$("#conviven").val();
	if($("#conyuge").is(":visible")){
	if($("#tipoDoc2").val()==0){
	    alert("Falta tipo documento");
	    return false;
	}
	}
	var idtd=$("#tipoDoc2").val();
	$.ajax({
		url: URL+'phpComunes/buscarPersona2.php',
		type: "POST",
		data: "submit &v0="+idtd+"&v1="+num,
		async: false,
		dataType: "json",
		success: function(data){
			if(data==0){
				existe=0;
				idnb=0;
				return false;
			}
			else{			
			$.each(data,function(i,fila){
				if(fila.estado=="M"){
		    		alert("El beneficiario se encuentra FALLECIDO!!!\n"+
							"No es posible relacionar a este beneficiario");		    		
					$('#identificacion2').val("");
					return;
		    	}
				
				existe=1;
				idnb=fila.idpersona;
				rutaDocumentos=fila.rutadocumentos;
				nombreCorto=fila.nombrecorto;
				idProfesion=fila.idprofesion;
				idcony=fila.idpersona;
				$('#pNombre2').val($.trim(fila.pnombre));
				$('#sNombre2').val($.trim(fila.snombre));
				$('#pApellido2').val($.trim(fila.papellido));
				$('#sApellido2').val($.trim(fila.sapellido));
				$("#direccion2").val($.trim(fila.direccion));
				$("#cboBarrioC").val(fila.idbarrio);
				$("#telefono2").val(fila.telefono);
				$("#celular2").val(fila.celular);
				$("#email2").val($.trim(fila.email));
				$("#tipoVivienda2").val(fila.idtipovivienda);
				$("#estadoCivil2").val(fila.idestadocivil);
				$("#capTrabajo2").val(fila.capacidadtrabajo);
				$("#profesion2").val(fila.idprofesion);
				$("#alternate2").val(fila.fechanacimiento);
				$('#sexo2').val(fila.sexo);
				$("#cboDeptoC").val(fila.iddepresidencia);
				$.post(URL+"phpComunes/ciudades.php",{elegido: $.trim(fila.iddepresidencia)}, function(data){
					$("#cboCiudadC").html(data);
					$("#cboCiudadC").val($.trim(fila.idciuresidencia));
					$.post(URL+"phpComunes/zonas.php",{elegido:$.trim(fila.idciuresidencia)}, function(data){
						$("#cboZonaC").html(data);
						$("#cboZonaC").val($.trim(fila.idzona));
						$.post(URL+"phpComunes/barrios.php",{elegido:$.trim(fila.idzona)}, function(data){
							$("#cboBarrioC").html(data);
							$("#cboBarrioC").val($.trim(fila.idbarrio));	
							$("#cboDeptoC2").val($.trim(fila.iddepnace));
							$.post(URL+"phpComunes/ciudades.php",{elegido: $.trim(fila.iddepnace)}, function(data){
								$("#cboCiudadC2").html(data);
								$("#cboCiudadC2").val($.trim(fila.idciunace));
							});
						});
					});	
				});	
			}); //each
			if(idcony == 0){
				return;
			}
			if(idPersona == idcony){
				limpiarConyuge();
				$('#identificacion2').val("");
				alert("No se puede crear una relacion con la misma persona");
			}
//			TABLA DINAMICA PARA DATOS EMPRESA TRABAJDOR ACTIVO
			$("table[name='dinamicTable']").remove();
			var tabla="<table class='tablero' width='100%' border='0' cellspacing='0' cellpadding='0' name='dinamicTable' style='margin-first:8px;'>";
			$.ajax({
				url: URL+'phpComunes/buscarAfiliacion.php',
				type: "POST",
				data: "submit &v0="+idnb,
				async: false,
				dataType: "json",
				success: function(datos){
					if(datos != 0){
						alert("La persona que trata de Afiliar es un trabajador activo!");
					    $.each(datos,function(i,fila){
					    	$('#nitConyuge').val(fila.nit);
					    	$('#empCony').val(fila.razonsocial);
					    	$('#salCony').val(fila.salario);
					    	$('#subCony').val(fila.detalledefinicion);
					    	tabla+="<tr><th>Salario</th><th>Nit</th><th>Raz\u00F3n social</th><th>Tipo</th><th>Detalle</th></tr>"+"<tr><td>$"+ fila.salario +"</td><td>"+ fila.nit +"</td><td>"+fila.razonsocial+"</td><td>"+fila.tipoformulario+"</td><td>"+fila.detalledefinicion+"</td></tr>";
					    });//end each
					  //Agregar tabla despues de la tabla  beneficiarios
					    $("#conyuge .tablero").after(tabla+="</table>");
					    $("table[name='dinamicTable']").hide().fadeIn(800);
					}
					var tabla2="<table class='tablero' width='100%' border='0' cellspacing='0' cellpadding='0' name='dinamicTable' style='margin-first:8px;'>";
					var conv=0;
					$.ajax({
						url: URL+'phpComunes/buscarConvivenciaConyuge.php',
						type: "POST",
						data: "submit &v0="+idnb,
						dataType: "json",
						success: function(dato){
							if(dato!=0){
								 alert("La persona que esta afiliando tiene relacion de convivencia (ver tabla parte inferior)");
								 $.each(dato,function(i,fila){
									   var nombre=fila.pnombre+" "+fila.snombre+" "+fila.papellido+" "+fila.sapellido;
									   conv=fila.conviven;
									   if(conv=='S'){
										   alert("El conyuge tiene relacion de convivencia Activa!");
										   $("#conviven").val('N');
										   limpiarConyuge();
										   $('#identificacion2').val("");
									   }
									   idtrelacion=fila.idtrabajador;
									   tabla2+="<tr><th>Identificacion</th><th>Nombre</th><th>Conviven</th></tr>"+"<tr><td>"+ fila.identificacion +"</td><td>"+ nombre +"</td><td>"+fila.conviven+"</td></tr>";
									   return;
								 });
								 if(idPersona==idtrelacion){
									 idtrelacion=1;
								 }
								 $("#div-relacion").append(tabla2+="</table>");
								 $("table[name='dinamicTable']").hide().fadeIn(800);
								 if(conv=='S' && idtrelacion==0){
									 $('#conyuge .tablero input:text').val("");
									 $('#conyuge .tablero select').val("0");
								 }
							}else{
								$.ajax({
									url: URL+'phpComunes/buscarConyugueActivo.php',
									type: "POST",
									data: "submit &v0="+idPersona,
									dataType: "json",
									success: function(datoC){
										if(datoC != 0){
											alert("La Relacion de Convivencia ya existe!");
											$.getJSON(URL+'phpComunes/guardarConyuge.php',{v0:idPersona,v1:idnb,v2:datoC[0].idparentesco,v3:idnb,v4:datoC[0].conviven,v5:datoC[0].idtiporelacion},function(dataC){
									    		if(dataC==0){
									    			alert("No se puede guardar la relacion");
									        		return false;
									     		}else{
									     			alert("Se Actualizo La Relacion!");
									     			if(datoC[0].conviven == "N"){
									     				alert("La Relacion tiene convivencia N \n Si se requiere cambie donde se lista el conyugue");
									     			}
									     		}
								     		});
											limpiarConyuge();
											$('#identificacion2').val("");									
										}
									}
								});	
							}							 
						}
					}); //FIN AJAX 3
				}
			}); //FIN AJAX 2
			}//fin else primer success
		}//fin primer success
	});
}	

function verificarC(){
	$("#div-conyuge ul").remove();
	lista="<ul class='Rojo'>";
	
	var tipoDoc2=$("#tipoDoc2").val();	
	var cedula2=$("#identificacion2").val();
	var pNombre2=$("#pNombre2").val();
	var pApellido2=$("#pApellido2").val();
	var sNombre2=$("#sNombre2").val();//no obligado
	var sApellido2=$("#sApellido2").val();//no obligado
	
	var tipRel=$("#tipRel").val();	
	var conviven=$("#conviven").val();	
	
	var sexo2=$("#sexo2").val();
	var tipVivienda2=$("#tipoVivienda2").val();	
	var cboDeptoC=$("#cboDeptoC").val();
	var cboCiudadC =$("#cboCiudadC").val();
	var zonaRes2=$("#cboZonaC").val();
	var barrio2=$("#cboBarrioC").val();
	var direccion2=$("#direccion2").val();
	var email2=/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test($("#email2").val());
	var telefono2=$("#telefono2").val();
	var celular2=$("#celular2").val();
	var fecNac2=$("#alternate2").val();
	var cboPaisC2=$("#cboPaisC2").val();//no obligado
	var cboDeptoC2=$("#cboDeptoC2").val();//no obligado
	var cboCiudadC2=$("#cboCiudadC2").val();//no obligado
	
	var estadoCivil2=$("#estadoCivil2").val();	
	var capTrabajo2 = (parseInt($("#capTrabajo2").val())==0)?'N':$("#capTrabajo2").val();//no obligado
	var profesion2='';//$("#profesion2").val();//no obligado
	
	//---------------------TIPO DOC----------------------  
	if(tipoDoc2=='0'){lista+='<li>Seleccione un tipo de documento en CONYUGE.</li>';}
	//--------------------CEDULA-----------------------
	if(cedula2==''){
		lista+='<li>Ingrese la identificaci\u00F3n del CONYUGE.</li>';
	}
	 else 
	{
		if(cedula2.length<3)
			{
				lista+='<li>La identificaci\u00F3n NO debe ser menor a 3 d\u00EDgitos.</li>';
			}
		/*else 
			{
		if(cedula2.length==9){
					lista+='<li>La identificaci\u00F3n NO debe tener 9 d\u00EDgitos.</li>';
					}
			}//end else*/
	}//end else
	//----------------PRIMER NOMBRE Y APELLIDO---------------------------  
	if(pNombre2==''){ lista+='<li>Ingrese el NOMBRE del CONYUGE.</li>'; }
	if(pApellido2==''){ lista+='<li>Ingrese el APELLIDO del CONYUGE.</li>'; }	
	//----------------TIPO RELACION -------------------------------------
	if(tipRel=='0'){ lista+='<li>Seleccione un tipo de relacion.</li>'; }
	//------------------------CONVIVEN----------------------
	if(conviven=='0'){ lista+='<li>Seleccione el tipo de convivencia.</li>'; }	
	//-------------------SEXO-----------------------
	if(sexo2=='0'){lista+='<li>Seleccione el sexo del CONYUGE.</li>';}
	
	//---------------------EMAIL------------------------
	if($("#email2").val()!=''){
		if(!email2) {
			lista+='<li>El E-mail aparentemente es incorrecto.</li>';	
		}else{
			email2=$("#email2").val();
		}
	}
	//-------------------TELEFONO----------------------
	if(telefono2!=''){
		if(telefono2.length<7){
			lista+='<li>Faltan d\u00EDgitos en el  TELEFONO.</li>';
		}
	}
	
	//----------------------FECHA DE NACIMIENTO---------------
	if(fecNac2==''){lista+='<li>Seleccione la fecha de nacimiento del CONYUGE.</li>';	}
	
	//..MOSTRAR VALIDACIONES..//
	$(lista+"</ul>").prependTo($("#div-conyuge"));
	$("#div-conyuge ul.Rojo").hide().fadeIn("slow");
	
	if($("#div-conyuge ul.Rojo").is(":empty")){			 
		idPersona=$("#idPersona").val();
		
		/** VALIDACIONES **/
		var retorno = 0;
		if ( parseInt(idcony) > 0 ) 
			retorno = buscarRelacionActivasBeneficiario( $("#idPersona").val(), idcony, 34, true, false);		
		if ( retorno > 1 && retorno != 3 ) {
			if ( retorno == 5 ) {
				// El beneficiario tiene relacion con este
			} else if ( retorno == 34 || (retorno == 35 && conviven == 'N') ) {
				if( conviven == "S" ) {
					$("#divRelacionesActivas").dialog('open');								
					alert("Tiene afiliaciones Activas como Conyuge!!");
					$('#conviven').val('N');
					$('#conviven').attr('disabled','disabled');
				}
			} else {
				if ( retorno != 4 ){									
					$("#divRelacionesActivas").dialog('open');
				}
				if ( retorno == 4 ) {
					// Ya tiene relacion con el afiliado
				} else if ( retorno == 34 ) {
					alert("Tiene afiliaciones Activas como Conyuge!!");
					$('#conviven').val('N');
					$('#conviven').attr('disabled','disabled');									
				} else if ( retorno == 35 ) {
					alert("La persona tiene Afiliaciones Activas como Hijo!!!\n"+
							"Inactive la relacion para poder continuar");						
				} else if ( retorno == 36 ) {
					alert("La persona tiene Afiliaciones Activas como Padre!!!\n"+
							"Inactive la relacion para poder continuar");						
				} else if ( retorno == 37 ) {
					alert("La persona tiene Afiliaciones Activas como Hermano!!!\n"+
							"Inactive la relacion para poder continuar");						
				} else if ( retorno == 38 ) {
					alert("La persona tiene Afiliaciones Activas como Hijastro!!!\n"+
							"Inactive la relacion para poder continuar");						
				} else {
					alert("Ocurrio un error validando parentesco de la persona!!!\n"+
							"Retorno:" +retorno);			
				}
				
				return;
			}
		}		
		
		if(existe==0){
			$.ajax({
				url: 'insertPersona.php',
				type: "POST",
				data: {v1:tipoDoc2,v2:cedula2,v3:pApellido2,v4:sApellido2,v5:pNombre2,v6:sNombre2,v7:sexo2,v8:direccion2,v9:barrio2,v10:telefono2,v11:celular2,v12:email2,v13:tipVivienda2,v14:cboDeptoC,v15:cboCiudadC,v16:zonaRes2,v17:estadoCivil2,v18:fecNac2,v19:cboCiudadC2,v20:cboDeptoC2,v21:capTrabajo2,v22:profesion2,v23:''},
				async: false,
				//dataType: "json",
				success: function(data){
					if(data==0) {
						alert("No se pudo guardar la persona!");
						return false;
					} else if(data<0) {
						alert("Error al insertar la persona,\ncodigo: "+data);
						return false;
					} else {	
						alert("La persona fue creada satisfactoriamente. (ID."+ data +")");
						idcony=data;
					}
				}
			});
		} else {
			var datos={v0:idcony,v1:tipoDoc2,v2:cedula2,v3:pApellido2, v4:sApellido2, v5:pNombre2, v6:sNombre2, v7:sexo2, v8:direccion2, v9:barrio2, v10:telefono2, v11:celular2, v12:email2, v13:'', v14:tipVivienda2, v15:cboDeptoC, v16:cboCiudadC, v17:zonaRes2, v18:estadoCivil2, v19:fecNac2, v20:cboDeptoC2, v21:cboCiudadC2, v22:capTrabajo2, v23:nombreCorto, v24:idProfesion, v25:rutaDocumentos};
			$.ajax({
				url: URL+'phpComunes/actualizarPersona.php',
				type: "POST",
				data: datos,
				async: false,
				//dataType: "json",
				success: function(data){
			 		if(data==0){
			 			alert("No se pudo actualizar la persona!");
			 			return false;
					} else {
						alert("Se actualizo la persona.");
					}
				}
			});//ajax*/	
		}
		
		//grabar relacion
		if(idcony>0){
			var campo0=$("#idPersona").val();
			var campo1=idcony;
			var campo2=34;			
			var campo4=conviven;
			var campo5=tipRel;
		
			$.getJSON(URL+'phpComunes/guardarConyuge.php',{v0:campo0,v1:campo1,v2:campo2,v3:campo1,v4:campo4,v5:campo5},function(data){
		    	if(data==0){
		    		alert("No se pudo guardar la relaci\u00F3n!, intente de nuevo!");
		       		return false;
		    	} else {
		    		/*$.getJSON(URL+'phpComunes/guardarConyuge.php',{v0:campo1,v1:campo0,v2:campo2,v3:campo0,v4:campo4,v5:campo5},function(data){
				       	if(data==0){
							alert("No se pudo crear la relaci\u00F3n, informe al administrdor del sistema!");
							return false;*/
						//} else {					
					       	alert("La relaci\u00F3n de convivencia fue creada!");
							$("#div-conyuge").dialog("close");
							buscarRelaciones();	
						//}//} else {
					//});//SEGUNDO $.getJSON(URL+
		    	}//PRIMER } else {
	     	});
		}		
	}		
}

function cambiarC(idr,c,idc){	
	var idc1=idPersona;
	var idc2=idc;
	var cont=true;
	$("#tbRelacionesActivas").empty();
	if ( c == 0 ) {
		if(confirm("Esta seguro de disolver la CONVIVENCIA?")==true){
			$.ajax({
				url:'buscarNomCon.php',
				type:'POST',
				dataType:'json',
				data:{v0:idr},
				async: false,
				success:function(data){
					if(data==0){
						alert("No existen datos del conyuge!");
						cont=false;
						return false;
					}
					var nom='';
					$.each(data,function(i,fila){
						if(i==0){
							nom+=fila.pnombre+" "+fila.snombre+" "+fila.papellido+" "+fila.sapellido+"\r\n";
							//idc1=fila.idtrabajador;
						}else{
							nom+=fila.pnombre+" "+fila.snombre+" "+fila.papellido+" "+fila.sapellido;
							//idc2=fila.idtrabajador;
						}
					});
					if(confirm("Se va a disolver la relacion de convivencia entre:\r\n"+nom+"\r\nSi existen hijastros, pasaran a estado Inactivo!")){
						$.ajax({
							url:'disolverC.php',
							type:'POST',
							dataType:'json',
							data:{v0:idc1,v1:idc2},
							async: false,
							success:function(data){
								if(data==0){
									alert("No se pudo disolver la relacion!");
									cont=false;
									return false;
								}else{
									alert("La relacion se disolvio!");
									$.ajax({
										url:'buscarInactivarIdBeneficiarios.php',
										type:'POST',
										dataType:'json',
										data:{v0:$("#idPersona").val(),v1:38},
										async: false,
										success:function(data){
											if(data!=0){
												alert("Se inactivaron los Hijastros!");
											}
										}
									});
									buscarGrupo();
									buscarRelaciones();
									observacionesTab(idc1,1);
									return false;
								}
							}
						});
					}
				}
			});
			}
	} else {
		if ( confirm("Esta seguro de crear la CONVIVENCIA?") == true ) {
			$.ajax({
				url:'buscarNomCon.php',
				type:'POST',
				dataType:'json',
				data:{v0:idr},
				async: false,
				success:function(data){
					var nom='';
					if(data==0){
						alert("No existen datos del conyuge!");
						cont=false;
						return false;
					}					
					
					$.each(data,function(i,fila){
						if(i==0){
							nom+=fila.pnombre+" "+fila.snombre+" "+fila.papellido+" "+fila.sapellido+"\r\n";
							//idc1=fila.idtrabajador;
						}else{
							nom+=fila.pnombre+" "+fila.snombre+" "+fila.papellido+" "+fila.sapellido;
							//idc2=fila.idtrabajador;
						}
					});
					
					$.ajax({
						url: URL+'phpComunes/buscarConvivenciaConyuge2.php',
						type: "POST",
						data: {v0:idc2},
						async: false,
						dataType: "json",
						success:function(datos){
							if(datos){
								var contador=0;
								$.each(datos,function(i,fila) {
									if((fila.idtrabajador==idc1 && fila.idbeneficiario==idc2) || (fila.idtrabajador==idc2 && fila.idbeneficiario==idc1)){ return; }
									contador++;
									nom = fila.pnombre + " " +fila.snombre +" "+ fila.papellido + " " + fila.sapellido;
									$("#tbRelacionesActivas").append(
										"<tr>" +
											"<td>"+fila.codigo+"</td>" +
											"<td>"+fila.identificacion+"</td>" +
											"<td>"+nom+"</td>" +
											"<td><center>"+fila.detalledefinicion+"</center></td>" +
											"<td><center>"+fila.conviven+"</center></td>" +
										"</tr>");
								});
								
								if(contador>0) {
									$("#divRelacionesActivas").dialog('open');
									alert("El Conyuge ya tiene una Convivencia Activa");
									return false;
								}
							}
							
							$.ajax({
								url: URL+'phpComunes/buscarConvivenciaConyuge2.php',
								type: "POST",
								data: {v0:idPersona},
								async: false,
								dataType: "json",
								success:function(data1){
									if(data1){
										var contador=0;
										$.each(data1,function(i,fila) {
											if((fila.idtrabajador==idc1 && fila.idbeneficiario==idc2) || (fila.idtrabajador==idc2 && fila.idbeneficiario==idc1)){ return; }
											contador++;
											nom = fila.pnombre + " " +fila.snombre +" "+ fila.papellido + " " + fila.sapellido;
											$("#tbRelacionesActivas").append(
												"<tr>" +
													"<td>"+fila.codigo+"</td>" +
													"<td>"+fila.identificacion+"</td>" +
													"<td>"+nom+"</td>" +
													"<td><center>"+fila.detalledefinicion+"</center></td>" +
													"<td><center>"+fila.conviven+"</center></td>" +
												"</tr>");
										});
										
										if(contador>0) {
											$("#divRelacionesActivas").dialog('open');
											alert("El Trabajador ya tiene una Convivencia Activa");
											return false;
										}
										
									}
									
									if(confirm("Se va a crear la relacion de convivencia entre:\r\n"+nom)){
										$.ajax({
											url:'crearC.php',
											type:'POST',
											dataType:'json',
											data:{v0:idc1,v1:idc2,v2:idr},
											async: false,
											success:function(data2){
												if(data2==0){
													alert("No se pudo crear la relacion!");
													cont=false;
													return false;
												}
												if(data2==2){
													alert("Una o las dos personas tienen relacion de convivencia Activa!");
													return false;
												}
												else{
													alert("La relacion se creo!");
													buscarRelaciones();
													observacionesTab(idc1,1);															
													return false;
												}
											}
										});
									}
								}
							});
						}
					});
					
				}
			});
		}
	}
}
	
function validarFecha(obj){
	
 var fecha=obj.value;
 
 //Expresion regular para validar
/* if (/^\d{2}\/\d{2}\/\d{4}$/.test(fecha)){
	 fechaTemp=obj.value;
	 obj.value='';
	 
 }
 if(fecha==''){
 obj.value=fechaTemp;
 }else{
 */
 if(( fecha.length<8) || (fecha.length>8)){
 	alert("La fecha esta mal escrita");
 	$("#txtfechanace").focus().val('');
 	return false;
 }	
 mes=fecha.slice(0,2);
 dia=fecha.slice(2,4);
 ano=fecha.slice(4,8);
 
 
 switch(mes){
        case "01": case "03":  case "05": case "07": case "08": case "10": case "12":numDias=31;break;
        case "04": case "06": case "09": case "11": numDias=30; break;
        case "02":
		       if ( ( anio % 100 != 0) && ((anio % 4 == 0) || (anio % 400 == 0))) {
			   numDias=29;
			   }else{
				   numDias=28;
				   }
                break;
        default:
	            alert("Mes introducido inv\u00C1lido.");
		        return false;
    }
       //Validar Dias
        if (parseInt(dia)>numDias || parseInt(dia)==0){
            alert("Fecha introducida inv\u00C1lida.");
            return false;
        }
		
		//Validar a�os
		var f=new Date();
		 if (parseInt(ano)>f.getFullYear()){
            alert("A\u00F1o inv\u00C1lido mayor al actual.");
            return false;
        }
		
		var fechaFinal=mes+"/"+dia+"/"+ano;
		obj.value=fechaFinal;
// }//else

	}
	
function limpiarConyuge(){
		idcony=0;
	    $('#pNombre2').val('');		
		$('#sNombre2').val('');
		$('#pApellido2').val('');
		$('#sApellido2').val('');
		$('#sexo2').val('0');
		$('#tipoVivienda2').val('0');		
		$('#cboDeptoC').val('0');
		$('#cboCiudadC').val('0');
		$('#cboZonaC').val('0');
		$('#cboBarrioC').val('0');
		$('#direccion2').val('');		
		$('#email2').val('');	
		$('#telefono2').val('');
		$('#celular2').val('');	
		$('#fecNacHide2').val('');
		$('#alternate2').val('');
		$('#cboPaisC2').val('0');
		$('#cboDeptoC2').val('0');
		$('#cboCiudadC2').val('0');
		
		$("#trInformacionAfiliacion,#trInformacionAfiliacion2,#trInformacionAfiliacion3").hide();
		
		$('#nitConyuge').val('');			
		$('#empCony').val('');
		$('#salCony').val('');	
		$('#subCony').val('');		
		
		$('#conviven').removeAttr('disabled');
		$("#identificacion2").focus();
		
		var data=dataRelaciones;
		if ( !( data == 0 || data == null )){
			$.each(data,function(i,fila){
				if ( fila.conviven == "S" ) {
					$('#conviven').val('N');
					$('#conviven').attr('disabled','disabled');
				}
			});
		}
}	

function verificarB(){
	$("#beneficiarios ul").remove();
	$("table[name='dinamicTable']").remove();
	lista="<ul class='Rojo'>";
			
	//...BENEFICIARIOS
	var parentesco=$("#parentesco").val();	//children("option:selected").html();//valor del texto del option PARENTESCO
	var cedMama=$("#cedMama").val();//no obligado
	
	var tipoDoc3=$("#tipoDoc3").val();
	var cedula3=$("#identificacion3").val();
	var pNombre3=$("#pNombre3").val();
	var pApellido3=$("#pApellido3").val();
	var sNombre3=$("#sNombre3").val();//no obligado
	var sApellido3=$("#sApellido3").val();//no obligado
	var sexo3=$("#sexo3").val();
	var fecNac3=$("#fecNacHide3").val();
	
	var tipoAfiliacion3=$('#tipoAfiliacion3').val();
	var capacidad=$('#capTrabajo').val();
	var cboPaisB=$('#cboPaisB').val();
	var cboDeptoB=$('#cboDeptoB').val();
	var cboCiudadB=$("#cboCiudadB").val();
	var estadoCivil3=$("#estadoCivil3").val();
	var fEscolaridad=$("#fEscolaridad").val();
	var fUniversidad=$("#fUniversidad").val();
	var fDiscapacidad=$("#fDiscapacidad").val();
	var fSupervivencia=$("#fSupervivencia").val();
	var fDependenciaEconomica=$("#fDependenciaEconomica").val();
	var fAsignacion=$("#fAsignacion").val();
	
	
	var padreBiologico=$("#hideIdbiol").val();
	
	//VALORES DE CERTIFICADO
	$("#cEscolaridad,#cUniversidad,#cSupervivencia").attr("value","N");
	
	//-------------------PARENTESCO-----------------------
	if(parentesco==0||parentesco=='Seleccione..'){
		lista+='<li>Seleccione el tipo de beneficiario.</li>';
	}
	//-------------------CEDULA PADRE---------------------
	if(cedMama=='undefined'){
		lista+='<li>Seleccione una Cedula Padre o Madre.</li>';		
	}
	//-------------------PADRE BIOLOGICO---------------------
	if( parentesco == '38' && ($("#padreBiologico").is(":visible")) && ( isNaN(padreBiologico) || padreBiologico<1 || ($("#IdPadreBiol").val()).length == 0 )){
		lista+='<li>Ingrese Datos del Padre Biologico.</li>';		
	}	
	//-------------------TIPO AFILIACION-----------------------
	if(tipoAfiliacion3=='0'){
		lista+='<li>Seleccione tipo formulario.</li>';
		afiliacion=false;	
	}
	//-------------------CAPACIDAD--------------------
	if(capacidad=='0'){
		lista+='<li>Seleccione capacidad de trabajo.</li>';
		afiliacion=false;	
	}
	//---------------------TIPO DOC----------------------  
	if(tipoDoc3==0){
		lista+='<li>Seleccione un tipo de documento en BENEFICIARIO.</li>';
		afiliacion=false;
	}
	//--------------------CEDULA-----------------------   
	if(cedula3==''){
		lista+='<li>Ingrese la identificaci\u00F3n del BENEFICIARIO.</li>';
	} else {
		if(cedula3.length<3){
			lista+='<li>La identificaci\u00F3n NO debe tener 2 d\u00EDgitos.</li>';
		}else {
			if(cedula3.length==9){
				lista+='<li>La identificaci\u00F3n NO debe tener 9 d\u00EDgitos.</li>';
			}
		}//end else
	}//end else
	//----------------PRIMER NOMBRE Y APELLIDO---------------------------  
	if(pNombre3==''){
		lista+='<li>Ingrese el NOMBRE del BENEFICIARIO.</li>';
	}
	if(pApellido3==''){
		lista+='<li>Ingrese el APELLIDO del BENEFICIARIO.</li>';
	}
	//----------------------FECHA DE NACIMIENTO 3---------------
	if(fecNac3==''||fecNac3=='mmddaaaa'){
		lista+='<li>Escriba la fecha de nacimiento del BENEFICIARIO.</li>';
		afiliacion=false;
	}
	//-------------------PAIS - DEPARTAMENTO - CIUDAD -----------------------
	if(cboPaisB=='0'){
		lista+='<li>Seleccione Pais de nacimiento.</li>';			
	}
	if(cboDeptoB=='0'){
		lista+='<li>Seleccione Departamento de nacimiento.</li>';			
	}
	if(cboCiudadB=='0'){
		lista+='<li>Seleccione la ciudad de nacimiento.</li>';			
	}
	//-------------------SEXO-----------------------
	if(sexo3=='0'){
		lista+='<li>Seleccione el sexo del BENEFICIARIO.</li>';
	}
	//-------------------FECHA ASIGNACION---------------------
	if($("#tipoAfiliacion3").val()=='48'){
		if(fAsignacion==''){
			lista+='<li>Ingrese la fecha de Asignacion.</li>';
			afiliacion=false;
		}
	}
	//Validar certificados de beneficiario
	if($("#cUniversidad").attr("checked")==true){
		if($("#fUniversidad").val()==''){
			lista+='<li>Ingrese la fecha de entrega del certificado de UNIVERSIDAD.</li>';
		}
	}	
	if($("#cEscolaridad").attr("checked")==true){
		if($("#fEscolaridad").val()==''){
			lista+='<li>Ingrese la fecha de entrega del certificado de ESCOLARIDAD.</li>';
		}
	}	
	if($("#cSupervivencia").attr("checked")==true){
		if($("#fSupervivencia").val()==''){
			lista+='<li>Ingrese la fecha de entrega del certificado de SUPERVIVENCIA.</li>';
		}
	}
	if($("#cDiscapacidad").attr("checked")==true){
		if($("#fDiscapacidad").val()==''){
			lista+='<li>Ingrese la fecha de entrega del certificado de DISCAPACIDAD.</li>';
		}
	}
	
	if($("#cDependenciaEconomica").attr("checked")==true){
		if($("#fDependenciaEconomica").val()==''){
			lista+='<li>Ingrese la fecha de entrega del certificado de DEPENDENCIA ECONOMICA PARA HIJASTRO.</li>';
		}
	}
					
	//..MOSTRAR VALIDACIONES..//
	$(lista+"</ul>").prependTo($("#beneficiarios"));
	$("#beneficiarios ul.Rojo").hide().fadeIn("slow");		
	
	//Si no hay errores guardar 
	if($("#beneficiarios ul.Rojo").is(":empty")){
		//VALIDACIONES DE NUEVO POR SI OCURRE UN ERROR EN VALIDACIONES ANTERIORES
		var retorno = 0;
		if ( parseInt(idnb) > 0 ) 
			retorno = buscarRelacionActivasBeneficiario( $("#idPersona").val(), idnb, parentesco, true, false);
		if ( retorno > 1 ){
			if ( retorno == 4 ){
				//Ya esta la validacion. Ya esta Afiliado
			} else if ( retorno == 3 ){
				alert("Ocurrio un error validando relaciones del Beneficiario!!");						
			} else {					
				$("#divRelacionesActivas").dialog('open');
				
				if ( retorno == 2 ){
					alert("Beneficiario tiene el maximo numero de Relaciones!!");
				} else if ( retorno == 34 ) {
					alert("Tiene afiliaciones Activas como Conyuge!!");						
				} else if ( retorno == 35 ) {
					alert("El beneficiario tiene Afiliaciones Activas como Hijo!!!\n"+
							"Revise si es correcto el parentesco que esta seleccionando");						
				} else if ( retorno == 36 ) {
					alert("El beneficiario tiene Afiliaciones Activas como Padre!!!\n"+
							"Revise el parentesco que esta seleccionando");						
				} else if ( retorno == 37 ) {
					alert("El beneficiario tiene Afiliaciones Activas como Hermano!!!\n"+
							"Revise el parentesco que esta seleccionando");						
				} else if ( retorno == 38 ) {
					alert("El beneficiario tiene Afiliaciones Activas como Hijastro!!!\n"+
							"Revise el parentesco que esta seleccionando");						
				} else if ( retorno == 3538 ) {
					var cadena="EL PADRE ";
					if ( $("#" + txtHiddenSexo ).val() == "F") {
						cadena="LA MADRE ";
					}
					alert("El beneficiario tiene Afiliacion Activa como HIJO!!!\n" +
							cadena + "LE TIENE AFILIADO");						
				} else if ( retorno == 3835 ) {
					var cadena="EL PADRASTRO ";
					if ( $("#" + txtHiddenSexo ).val() == "F") {
						cadena="LA MADRASTRA ";
					}
					alert("El beneficiario tiene Afiliaciones Activa como HIJASTRO!!!\n" +
							cadena + "LE TIENE AFILIADO");						
				}
			}
			return false;
		}		
		
		var idben=0;
		var idcony=0;
		if(existe==0){
			//alert("El beneficiario es nuevo.");
			var retorno = false;
			$.ajax({
				url: 'insertPersona.php',
				type: "POST",
				data: {v1:tipoDoc3,v2:cedula3,v3:pApellido3,v4:sApellido3,v5:pNombre3,v6:sNombre3,v7:sexo3,v17:estadoCivil3,v18:fecNac3,v19:cboCiudadB,v20:cboDeptoB,v21:capacidad,v26:cboPaisB},
				async: false,
				success: function(data){
		 			if(data==0){
						alert("No se pudo guardar la persona!");
						return false;
					} else if(data<0){
						alert("Error al insertar la persona, codigo: "+data);
						return false;
					}	
					alert("La persona fue grabada!");
				   	idnb=data;
					idben=data;	retorno = true;
				}
			});
			
			if ( retorno == false ) return false; 
		}else{				
			//actualizar persona
			idben=parseInt(idnb);
									
			var datos={v0:idben,v1:tipoDoc3,v2:cedula3,v3:pApellido3,v4:sApellido3,v5:pNombre3,v6:sNombre3,v7:sexo3,v8:'',v9:0,v10:'',v11:'',v12:'',v13:0,v14:'',v15:'',v16:'',v17:'',v18:estadoCivil3,v19:fecNac3,v20:cboDeptoB,v21:cboCiudadB,v22:capacidad,v23:'',v24:'',v25:''};
			$.ajax({
				url: URL+'phpComunes/actualizarPersona.php',
				type: "POST",
				data: datos,
				async: false,
				success: function(data){
			 		if(data==0){
						alert("No se pudo actualizar la persona!");
						return false;
					}
					alert("se actualizo la persona. idben="+idben);
				}
			});//ajax*/
		}//if existe
		//crear relacion
		if ( isNaN(idben) || !( parseInt(idben) > 0 ) ) {
			alert("NO hay datos del beneficiario!!");
			return false;
		}
		
		var giro='';
		if($("#tipoAfiliacion3").val()=='48'){
			giro='S';	
		}else{
			giro='N';
		}
		var campo0 = idPersona;
		var campo1 = idben;
		var campo2 = parentesco;
		var campo3 = ($("#cedMama").is(":disabled")?idcony: $("#cedMama option:selected").attr("name"));
		var campo4 = giro;
		var campo5 = $("#fAsignacion").val();
		$.ajax({
			url: URL+'phpComunes/guardarBeneficiario.php',
			type: "POST",
			async: false,
			data: "submit &v0="+campo0+"&v1="+campo1+"&v2="+campo2+"&v3="+campo3+"&v4="+campo4+"&v5="+campo5+"&v7="+padreBiologico,
			success: function(datos){
				if ( isNaN(datos) || !( parseInt(datos) > 0 ) ) {
					alert("NO se pudo guardar la relacion!");
					return false;
				} else {
					if ( ($("#pInicialV3").val() != "" && $("#pFinalV3").val() != "") 
							|| ($("#txtPerioIniciVigenHijastro3").val() != "" && $("#txtPerioFinalVigenHijastro3").val() != ""))  { 
						buscarVigenciaCertificado(idben,parentesco);
					}
					
					alert("Se grabo la relacion del beneficiario.");
					$("#beneficiarios").dialog("close");
					buscarGrupo();
					observacionesTab(idPersona,1);
				}
			}
		});
	}					
}

function buscarConyugueModificar(idconyugue){
	var comyugueId=0;
	var estadoCivilA='';
	var capTrabajoA='';
	var nombreCortoA='';
	var idProfesionA='';
	var rutaDocumentosA='';
	$("#div-conyugeA").dialog("destroy");	
	$("#div-conyugeA").dialog({
		show: "drop",
		hide: "clip",
		width:750,
		modal: true,
		open:function(event,ui){
			$.ajax({
				url:URL+'phpComunes/pdo.buscar.persona.id.php',
				async: false,
				data:{v0:idconyugue},
				dataType:"json",
				beforeSend: function(objeto){
		        	dialogLoading('show');
		        },        
		        complete: function(objeto, exito){
		        	dialogLoading('close');
		            if(exito != "success"){
		                alert("No se completo el proceso!");
		            }            
		        },
		        contentType: "application/x-www-form-urlencoded",
		        dataType: "json",
		        error: function(objeto, quepaso, otroobj){
		            alert("En buscarConyugueModificar Paso lo siguiente: "+quepaso);
		        },
		        global: true,
		        ifModified: false,
		        processData:true,
		        success:function(data){
				    if(data==0){
				    	return false;
				    }else
				    	{
						$.each(data,function(i,fila){
							comyugueId=fila.idpersona;
							estadoCivilA=fila.idestadocivil;
							capTrabajoA=fila.capacidadtrabajo;
							nombreCortoA=fila.nombrecorto;
							idProfesionA=fila.idprofesion;
							rutaDocumentosA=fila.rutadocumentos;
							$('#tipoDoc2A').val(fila.idtipodocumento);
							$('#identificacion2A').val(fila.identificacion);
							$('#hidIdentificacion2A').val(fila.identificacion);
					   	 	$('#pNombre2A').val(fila.pnombre);
					   	 	$('#sNombre2A').val(fila.snombre);
					   	 	$('#pApellido2A').val(fila.papellido);
					   	 	$('#sApellido2A').val(fila.sapellido);
					   	 	$('#alternate2A').val(fila.fechanacimiento);
					   	 	$('#hidAlternate2A').val(fila.fechanacimiento);
					   	 	$('#sexo2A').val(fila.sexo);
					   	 	$('#tipoVivienda2A').val(fila.idtipovivienda).trigger("change");
					   	 	$("#direccion2A").val(fila.direccion);
					   	 	$("#email2A").val(fila.email);
					   	 	$("#telefono2A").val(fila.telefono);
					   	 	$("#celular2A").val(fila.celular);
					   	 	$("#hidRutaDoc2A").val(fila.rutadocumentos);
					   	 	
					   	 	$("#cboDeptoCA").val($.trim(fila.iddepresidencia)).trigger('change');
							$("#cboCiudadCA").val($.trim(fila.idciuresidencia)).trigger("change");						    
							$("#cboZonaCA").val($.trim(fila.idzona)).trigger("change");
							$("#cboBarrioCA").val($.trim(fila.idbarrio));
							$("#cboPaisC2A").val($.trim(fila.idpais)).trigger('change');												
							$("#cboDeptoC2A").val($.trim(fila.iddepnace)).trigger('change');						    
							$("#cboCiudadC2A").val($.trim(fila.idciunace));												   						   	
						 }); //fin each
				    	}
				},
				type:"POST"
			 }); //ajax buscarpersona.php
		},
		
		buttons: {
			'Actualizar': function() {
			    var validado=validarCamposModCony();
				if(validado>0){
					return false;
				}
				//PARAMETROS PARA MODIFICAR LA INFORMACION PERSONAL DEL CONYUGUE
				var tdocA=$("#tipoDoc2A").val();
				var identificacionA=$("#identificacion2A").val();
				var hidIdentificacionA=$("#hidIdentificacion2A").val();
				var pnombreA=$("#pNombre2A").val();
				var snombreA=$("#sNombre2A").val();
			   	var papellidoA=$("#pApellido2A").val();
				var sapellidoA=$("#sApellido2A").val();			
				var sexoA=$("#sexo2A").val();
				var tipoviviendaA=$("#tipoVivienda2A").val();
				var deptoResA=$("#cboDeptoCA").val();
				var ciudadResA=$("#cboCiudadCA").val();
				var zonaA=$("#cboZonaCA").val();
				var barrioA=$("#cboBarrioCA").val();
				var direccionA=$("#direccion2A").val();
				var emailA=$("#email2A").val();
				var telefonoA=$("#telefono2A").val();			
				var celularA=$("#celular2A").val();
				var fechanaceA=$("#alternate2A").val();
				var hidFechanaceA=$("#hidAlternate2A").val();
				var paisNaceA=$("#cboPaisC2A").val();
				var deptoNaceA=$("#cboDeptoC2A").val();
				var ciudadNaceA=$("#cboCiudadC2A").val();
				var rutaDocA=$("#hidRutaDoc2A").val();
				var salir=false;
				
				$.ajax({
					url: URL +"phpComunes/verifIdentificacionDisponible2.php",
					type: "POST",
					data: {idTipo: tdocA, numero: identificacionA, idPersona: comyugueId},
					async: false,
					dataType: "json",
					success: function(respuesta){
						if(respuesta){
							$("#tbDocumentoDoble").empty();
							$.each(respuesta,function(i,fila){
								nom = fila.pnombre + " " +fila.snombre +" "+ fila.papellido + " " + fila.sapellido;
								$("#tbDocumentoDoble").append(
										"<tr>" +
											"<td>" + fila.codigo + "</td>" +
											"<td style='text-align:right'>" + fila.identificacion + "</td>" +
											"<td>" + fila.idpersona + " - " + nom + "</td>" +									
										"</tr>");
							});
							alert("Verifique la informaci\xf3n, ya existe una persona con ese tipo y numero de documento!!!");
							$("#divDocumentoDoble").dialog("open");
							return false;
						} else {
							if( identificacionA != hidIdentificacionA || fechanaceA != hidFechanaceA ) {
								$.ajax({
									url: URL+'phpComunes/crearRutaDocumentos.php',
									type: "POST",
									data: {identificacion:identificacionA,tipodocumento:tdocA,fecha:fechanaceA},
									async: false,
									//dataType: "json",
									success: function(data){
										if(data != "") {
											rutaDocA=data;										
											if(rutaDocA != ""){
												$.post(URL+'phpComunes/migrarDocumentosWS.php',{rutaOrigen:rutaDocA,rutaDestino:data},function(data2){
													if(data2==0){
														alert("No se pudo migrar los documentos.");
														salir=true;													
													}
												});
											}
										} else {
											alert("No se pudo generar la ruta de Documentos.");
											salir=true;
										}
									}
								});
							}
							if ( salir == true ) { return false; }
							
							var datos={v0:comyugueId,v1:tdocA,v2:identificacionA,v3:papellidoA, v4:sapellidoA, v5:pnombreA, v6:snombreA, v7:sexoA, 
									v8:direccionA, v9:barrioA, v10:telefonoA, v11:celularA, v12:emailA, v13:'', v14:tipoviviendaA, v15:deptoResA, 
									v16:ciudadResA, v17:zonaA, v18:estadoCivilA, v19:fechanaceA, v20:deptoNaceA, v21:ciudadNaceA, v22:capTrabajoA, 
									v23:nombreCortoA, v24:idProfesionA, v25:rutaDocA, v26:paisNaceA};
							
							$.ajax({
								url: URL+'phpComunes/actualizarPersona.php',
								type: "POST",
								data: datos,
								async: false,
								//dataType: "json",
								success: function(data){
							 		if(data==0){
							 			alert("No se pudo actualizar el Conyugue!");
							 			return false;
									} else {
								 		alert("Se actualizo el Conyuge.");
								 		$("#div-conyugeA").dialog("close");
								 		buscarRelaciones();							 		
								 		observacionesTab(idPersona,1);
									}
								}
							});
						}
					}
				});			
			}//termina actualizar		  
		},
		
		close:function(){
			$("#div-conyugeA ul").remove();
			$("#div-conyugeA  table.tablero input:text").val('');
			$("#div-conyugeA  table.tablero input:hidden").val('');//fecha nacimiento
			$("#div-conyugeA  table.tablero select").val('Seleccione');
			$("#div-conyugeA input:checkbox,#modificarBeneficiarios input:radio").attr("checked",false);
			$("#div-conyugeA .tablero :input.ui-state-error,#div-conyugeA .tablero select.ui-state-error").removeClass("ui-state-error");
			// TABLA DINAMICA PARA DATOS EMPRESA TRABAJADOR ACTIVO
			$("table[name='dinamicTable']").remove();
			//$("#cedMamaUpd").attr("disabled",false);
		}//end funcion close											
	 });//termina funciones del dialog
}

function buscarCertificadoUpdate(idcertificado,tipocertificado,pinicial,pfinal,fpresentacion,estado){

	
	$("#modificarCertificados").dialog("destroy");	
	$("#modificarCertificados").dialog({
		show: "drop",
		hide: "clip",
		width:750,
		modal: true,
		open:function(event,ui){
			tipcert = 0;
			if(tipocertificado == 'ESCOLARIDAD'){
				tipocert = '55';
			}else{
				if(tipocertificado == 'UNIVERSIDAD'){
					tipocert = '56';
				}else{
					if(tipocertificado == 'SUPERVIVENCIA'){
						tipocert = '57';
					}else{
						if(tipocertificado == 'DISCAPACIDAD'){
							tipocert = '58';
						}
					}		
				}
			}
							
			var anno = pinicial.substring(0,4);
			var mes = parseInt(pinicial.substring(4,6));
			var nuevo = "";
			
			if(mes<=2){
			    anno -= 1;
			    mes = mes==1?11:12;
			}else{
			    mes -=2;
			    mes=(mes<10?"0"+mes:mes)
			}
			fechaMin = anno+"/"+mes+"/01";
			
			fechaMaxI = pinicial.substring(0,4)+"/"+pinicial.substring(4,6)+"/01";			
			$('#pInicialV').datepicker('option', {minDate: new Date(fechaMin),maxDate: new Date(fechaMaxI)});
			
			fechaMax = pfinal.substring(0,4)+"/"+pfinal.substring(4,6)+"/01";
			$('#pFinalV').datepicker('option', {minDate:null,maxDate: new Date(fechaMax)});
			
			
			
			idcert = idcertificado;
			$("#lTipoCer").val(tipocert);
			$("#pInicialV").val(pinicial);
			$("#pFinalV").val(pfinal);
			$("#lEstado").val(estado);
			$("#pPresentacion").val(fpresentacion);
		},
		buttons: {
			'Actualizar': function() {
				var validaCer=validarCamposCer();
				if(validaCer>0){
					return false;
				}
				//PARAMETROS PARA MODIFICAR LA INFORMACION DEL CERTIFICADO
				var idtipocertificado = $("#lTipoCer").val();
				var pinicial = $("#pInicialV").val();
				var pfinal = $("#pFinalV").val();
				var estado = $("#lEstado").val();
				var fpresentacion = $("#pPresentacion").val();
				var datos={v0:idcert,v1:idtipocertificado,v2:pinicial,v3:pfinal, v4:estado, v5:fpresentacion};

				$.ajax({
					url: URL+'phpComunes/actualizarCertificado.php',
					type: "POST",
					data: datos,
					async: false,
					//dataType: "json",
					success: function(data){
				 		if(data==0){
				 			alert("No se pudo actualizar el Certificado!");
				 			return false;
						}
				 		alert("Se actualizo el Certificado.");
				 		$("#modificarCertificados").dialog("close");
				 		observacionesTab(idPersona,1);
					}
				});//ajax	
			
			}//termina actualizar
				
			
		},
		close:function(){
			
		}//end funcion close		
		
	});
}

function buscarBeneficiarioUpdate(idbenef,p,cedulaPadreBeneficiario,idrel){
	jQuery('div[aria-labelledby^=ui-dialog-title-modificarBeneficiarios]').remove();
	jQuery('div[class^=ui-effects-wrapper]').remove();
	$("#modificarBeneficiarios").dialog('destroy');
	
	idbenef=$.trim(idbenef);
	 $.ajax({
		url:URL+'phpComunes/buscarDatosRelacionID.php',
		type:"POST",
		data:{v0:idrel},
		dataType:"json",
		async:false,
		success:function(data){
			if(data==0){
				alert("No se encontro el beneficiario!! "+idbenef+"-"+p+"-"+$("#idPersona").val());
				return false;
		    } else {
		    	$("#modificarBeneficiarios").dialog({
		    		show: "drop",
		    		hide: "clip",
		    		width:750,
		    		modal: true,
		    		open:function(event,ui){
		    			$("#parentescoUpd").focus();
		    			$("#cedMamaUpd").attr("disabled",false);
		    			$("#padreBiologicoUpd").hide();
	 					$("#nombreBiologicoUpd").hide();
		    			
		    			$.each(data,function(i,fila){		    				
		    				$("#hidIdRelacion3Upd").val(idrel);
		    				$("#hidIdPersona3Upd").val(fila.idpersona);
							$('#tipoDoc3Upd').val(fila.idtipodocumento);
							$('#identificacion3Upd').val(fila.identificacion);
					   	 	$('#pNombre3Upd').val(fila.pnombre);
					   	 	$('#sNombre3Upd').val(fila.snombre);
					   	 	$('#pApellido3Upd').val(fila.papellido);
					   	 	$('#sApellido3Upd').val(fila.sapellido);
					   	 	$('#fecNac3Upd').val(fila.fechanacimiento);
					   	 	$('#sexo3Upd').val(fila.sexo);
					   	 	$('#capTrabajoUpd').val(fila.capacidadtrabajo);					   	 	
					   	 	$("#parentescoUpd").val(fila.idparentesco);
					   	 	$("#estadoUpd").val(fila.estado);
					   	 	$("#txtFAsignacion").val(fila.fechaasignacion);
					   	 	$("#cedMamaUpd").val(fila.idconyuge);
					   	 	$("#txtFechaAfiliacion").val(fila.fechaafiliacion);
					   	 	$("#hdnEstadoUpd").val(fila.estado);
					   	 	if(fila.estado == 'I'){
					   	 		$("#hdnMotivoUpd").val(fila.idmotivo);
					   	 		$("#fechaRetiro").val(fila.fechaestado);
					   	 	}
					   	 	if(fila.idparentesco==36 || fila.idparentesco==37){
				 			 	$("#cedMamaUpd").attr("disabled",true);
				 			} else {
				 				if(fila.idparentesco == 38 && fila.idbiologico>0 ){
				 					$("#padreBiologicoUpd").show();
				 					$("#nombreBiologicoUpd").show();
				 					$("#tipoIdPadreBiolUpd").val(fila.idtipodocumentoBiol);
				 					$("#IdPadreBiolUpd").val(fila.identificacionBiol).trigger("blur")
				 				}
				 				
				 			 	$("#cedMamaUpd").attr("disabled",false);				 			 	
				 			 	if(parseInt(cedulaPadreBeneficiario)>0)
				 					$('#cedMamaUpd').children('option[text='+cedulaPadreBeneficiario+']').attr('selected','selected');
				 			}			    		 
					   	 	if(fila.giro=='S'){
					   	 		$("#tipoAfiliacion3Upd").val(48);
					   	 	} else {
					   	 		$("#tipoAfiliacion3Upd").val(49);
					   	 	}					   	 	
						}); //fin each
		    		},		    		
		    		buttons: {
		    			'Actualizar': function() {
			    		    var validado=validarCamposBen();
			    			if(validado>0){ return false; }
			    			
			    			var idPersona=$("#idPersona").val();
			    			
			    			var idRelacion=$("#hidIdRelacion3Upd").val();
			    			var idBeneAct=$("#hidIdPersona3Upd").val();
			    			var parentesco=$("#parentescoUpd").val();
			    			var idpadremadre=($("#cedMamaUpd").is(":disabled") ? 0 : $("#cedMamaUpd option:selected").attr("name"));
			    			var idpadrebiol=0;
			    			var tdoc=$("#tipoDoc3Upd").val();
			    			var num=$("#identificacion3Upd").val();
			    		   	var pnom=$("#pNombre3Upd").val();
			    			var snom=$("#sNombre3Upd").val();
			    			var pape=$("#pApellido3Upd").val();
			    			var sape=$("#sApellido3Upd").val();
			    			var sexo=$("#sexo3Upd").val();
			    			var fecha=$("#fecNac3Upd").val();
			    			var tafil3=($("#tipoAfiliacion3Upd").val()==48)?'S':'N';
			    			var capTrab=$("#capTrabajoUpd").val();
			    			var fasignacion=$("#txtFAsignacion").val();
			    			var fafiliacion=$("#txtFechaAfiliacion").val();			    			
			    			var estado=$("#estadoUpd").val();
			    			var estadoAnt=$("#hdnEstadoUpd").val();
			    			var motivo="";
		         			var fecharetiro = "";
			    			
			    			if ( parentesco == '38' && ($("#padreBiologicoUpd").is(":visible")) && parseInt($("#hideIdbiolUpd").val()) > 0 ) {
			    				idpadrebiol=parseInt($("#hideIdbiolUpd").val());         			
			    			}			    			
			    			if ( estado == 'I' ){
			         			motivo=( parseInt($("#hdnMotivoUpd").val()) > 0 ) ? $("#hdnMotivoUpd").val() : 0;
			         			fecharetiro = $("#fechaRetiro").val();			         			
			    			}
			    			
			    			$.ajax({
								url: URL +"phpComunes/verifIdentificacionDisponible2.php",
								type: "POST",
								data: {idTipo: tdoc, numero: num, idPersona: idBeneAct},
								async: false,
								dataType: "json",
								success: function(respuesta){
									if(respuesta){
										$("#tbDocumentoDoble").empty();
										$.each(respuesta,function(i,fila){
											nom = fila.pnombre + " " +fila.snombre +" "+ fila.papellido + " " + fila.sapellido;
											$("#tbDocumentoDoble").append(
													"<tr>" +
														"<td>" + fila.codigo + "</td>" +
														"<td style='text-align:right'>" + fila.identificacion + "</td>" +
														"<td>" + fila.idpersona + " - " + nom + "</td>" +									
													"</tr>");
										});
										alert("Verifique la informaci\xf3n, ya existe una persona con ese tipo y numero de documento!!!");
										$("#divDocumentoDoble").dialog("open");
										return false;
									} else {
										$.ajax({
					    					url: URL+'phpComunes/pdo.modificar.persona.simple.php',
					    					type: "POST",
					    					async:false,
					    					data: {v0:tdoc,v1:num,v2:pnom,v3:snom,v4:pape,v5:sape,v6:fecha,v8:sexo,v9:idBeneAct,v10:fasignacion,v11:capTrab,v12:idPersona},
					    					success: function(datosa){
					    						if(datosa==0){
					    							alert("No se pudo actualizar los datos personales del beneficiario!");
					    							return false;
					    						} else if(datosa>=0) {
					    							alert("Datos persona actualizados!");					    							
					    						} else if($.trim(datosa) == "existe"){
					    							alert("Verifique la informaci\xf3n, ya existe una persona con ese tipo y numero de documento ("+ $("#tipoDoc3Upd option:selected").text() +" - "+ num +").");					    							
					    						} else {
					    							alert(datosa);					    							
					    						}
					    					}
					    				});
																				
										$.ajax({
						    				url:"pdo.act.afiliacion.php",
						    				type:"POST",
						    				async:false,
						    				data:{v0:idRelacion,v1:tafil3,v2:estado,v3:motivo,v4:fasignacion,v5:fafiliacion,v6:fecharetiro,v7:parentesco,v8:idpadremadre,v9:idpadrebiol},
						    				success:function(data){
						    			 		if(data==0){
						    			    		alert("No se pudo modificar la afiliaci\u00F3n del beneficiario.");
						    						return false;
						    			 		} else if (data>0) {
						    			 			alert("Afiliacion actualizada!");
						    						$("#modificarBeneficiarios").dialog("close");
						    						buscarGrupo();
						    						observacionesTab(idPersona,1);
						    					} else {
						    						alert(datos);
						    						return false;
						    					}
						    				}
						    			});
									}
								}
							});			    			
		    			}			    					    					    			
		    		},		    		
		    		close:function(){			
		    			$("#modificarBeneficiarios ul").remove();
		    			$("#modificarBeneficiarios  table.tablero input:text").val('');
		    			$("#modificarBeneficiarios  table.tablero input:hidden").val('');//fecha nacimiento
		    			$("#modificarBeneficiarios  table.tablero select").val('Seleccione');
		    			$("#modificarBeneficiarios input:checkbox,#modificarBeneficiarios input:radio").attr("checked",false);
		    			$("#modificarBeneficiarios .tablero :input.ui-state-error,#modificarBeneficiarios .tablero select.ui-state-error").removeClass("ui-state-error");
		    			// TABLA DINAMICA PARA DATOS EMPRESA TRABAJADOR ACTIVO
		    			$("table[name='dinamicTable']").remove();
		    			$("#motivoUpd").attr("disabled",true);
		    			$("#motivoUpd").val(0);
		    			$("#fechaRetiro").attr("disabled",true);
		    			$("#fechaRetiro").val('');
		    			jQuery('div[aria-labelledby^=ui-dialog-title-modificarBeneficiarios]').remove();
		    			jQuery('div[class^=ui-effects-wrapper]').remove();
		    			//$("#cedMamaUpd").attr("disabled",false);
		    		}//end funcion close											
		    	 });//termina funciones del dialog
		    }
		 }//fin funcion success
	 }); 	
}

function buscarRelacionesParaBeneficiario(){
	idPersona=$("#idPersona").val();
	var data=dataRelaciones;
	$("#cedMamaUpd option:not(':first',:eq(1))").remove();
	if(data==0 || data==null){
		MENSAJE("El trababajador NO tiene relaciones de convivencias.");
		return false;
	}
	$.each(data,function(i,fila){
		idc=fila.idconyuge; 
		numC=fila.identificacion;	
		nombreC=$.trim(fila.pnombre)+" "+$.trim(fila.snombre)+" "+$.trim(fila.papellido)+" "+$.trim(fila.sapellido);
		conviven=fila.conviven;
		//Agregar a la tabla beneficiarios oculta los datos del conyuge
		$("#cedMamaUpd").append("<option value='"+idc+"' name='"+numC+"'>"+numC+"</option>");
	});//each
}

function persona_simple(obj,obj2,td){
	//$("#dialog-persona").dialog("destroy"); //Destruir dialog para liberar recursos
	var num=obj.value;
	var id=0;
	
	$('#dialog-persona select,#dialog-persona input[type=text]').val('');
	$("#txtNumeroP").val(num);
	$('#pNombre').focus();
	$("#tDocumento").val(td);
	$("#dialog-persona").dialog({
		height: 225,
		width: 650,
		draggable:false,
		modal: true,
		/*open: function(){
			$('#dialog-persona').html('');
			$.get(URL+'phpComunes/personaSimple.php',function(data){
				$('#dialog-persona').html(data);
			});
			$('#dialog-persona select,#dialog-persona input[type=text]').val('');
			$("#txtNumeroP").val(num);
			$('#pNombre').focus();
			alert(num);
		},*/
		buttons: {
			'Guardar datos': function() {
				//$("#dialog-persona").dialog("destroy");
			    //obj.value=$('#tDirCompleta').val();
				var campo0=$("#tDocumento").val();
				var campo1=$.trim( $("#txtNumeroP").val() );
				var campo4=$.trim($("#pNombre").val());
				var campo5=$.trim($("#sNombre").val());
				var campo2=$.trim($("#pApellido").val());
				var campo3=$.trim($("#sApellido").val());
				if(campo1==""){
					alert("Falta numero de identificacion");
					return false;
					}
				if(campo4==""){
					alert("Falta primer nombre");
					return false;
					}
				if(campo2==""){
					alert("Falta primer apellido");
					return false;
					}
				$.ajax({
					url: URL+'phpComunes/pdo.insert.persona.php',
					type: "POST",
					data: "submit=&v0="+campo0+"&v2="+campo2+"&v3="+campo3+"&v4="+campo4+"&v5="+campo5+"&v1="+campo1,
					success: function(datos){
					if(datos==0){
						alert("NO se guardaron los datos de la persona!");
					}
					else{
						obj.focus();
						obj2.value=datos;
						id=datos;
					}
					}
				});
				obj.focus();
				$(this).dialog('close');
				return id;
//console.log(obj2);
				$('#dialog-persona select,#dialog-persona input[type=text]').val('');
			}
		},
		close: function() {
			//$("#dialog-persona").dialog('close');
			$('#dialog-persona select,#dialog-persona input[type=text]').val('');
			//obj2.focus();
			$(this).dialog("destroy");
			//focus al siguiente cmapo de texto; 
		}
	});
}

function validarMotivo(valor){
	 if(valor=='I'){
		if(confirm("Esta seguro de inactivar esta afiliacion?")==true){
		$("#motivoUpd").attr("disabled",false);
		$("#fechaRetiro").attr("disabled",false);
		}	
	 }else{
		$("#motivoUpd").attr("disabled",true);
		$("#motivoUpd").val(0);
		$("#fechaRetiro").attr("disabled",true);
		$("#fechaRetiro").val('');
	}	
	}//fn

function validarCamposCer(){
    var error=0;
	//reseteo clase error
	$("#modificarCertificados .tablero :input.ui-state-error,#modificarCertificados .tablero select.ui-state-error").removeClass("ui-state-error");
	
	if($("#pInicialV").val()==''){
		$("#pInicialV").addClass("ui-state-error");
		error++;
	}
	if($("pFinalV").val()==''){
		$("pFinalV").addClass("ui-state-error");
		error++;
	}
	if($("#pPresentacion").val()==''){
		$("#pPresentacion").addClass("ui-state-error");
		error++;
	}
	
	
	if(error==0){
		return 0;
	}else{
		return error;
	}
}

function validarCamposBen(){
	num=$.trim($("#identificacion3Upd").val());
    var error=0;
	//reseteo clase error
	$("#modificarBeneficiarios .tablero :input.ui-state-error,#modificarBeneficiarios .tablero select.ui-state-error").removeClass("ui-state-error");
	
	if($("#parentescoUpd").val()=='0'){
		$("#parentescoUpd").addClass("ui-state-error");
		error++;
		}
	if( $("#parentescoUpd").val() == '38' && ($("#padreBiologicoUpd").is(":visible")) && ( isNaN($("#hideIdbiolUpd").val()) || $("#hideIdbiolUpd").val()<1 || ($("#hideIdbiolUpd").val()).length == 0 )){
		$("#IdPadreBiolUpd").addClass("ui-state-error");
		error++;		
	}
	if($("#tipoDoc3Upd").val()=='0'){
	$("#tipoDoc3Upd").addClass("ui-state-error");
	error++;
	}
	if(num.length==0||num==''){
    $("#tipoDoc3Upd").addClass("ui-state-error");
	error++;
	}
	if($("#pNombre3Upd").val()==''){
	$("#pNombre3Upd").addClass("ui-state-error");
	error++;
	}
	if($("#pApellido3Upd").val()==''){
	$("#pApellido3Upd").addClass("ui-state-error");
	error++;
	}
	if($("#sexo3Upd").val()=='0'){
	$("#sexo3Upd").addClass("ui-state-error");
	error++;
	}
	if($("#fecNac3Upd").val()==''){
	$("#fecNac3Upd").addClass("ui-state-error");
	error++;
	}	
	if( $("#tipoAfiliacion3Upd").val() == "48" && $("#txtFAsignacion").val()==''){
		$("#txtFAsignacion").addClass("ui-state-error");
		error++;
	}
	if($("#txtFechaAfiliacion").val()==''){
		$("#txtFechaAfiliacion").addClass("ui-state-error");
		error++;
	}
	
	if(error==0){
	return 0;
	}else{
	return error;
	}
}

function validarCamposModCony(){
	var num=$.trim($("#identificacion2A").val());
    var error=0;
	//reseteo clase error
	$("#div-conyugeA .tablero :input.ui-state-error,#div-conyugeA .tablero select.ui-state-error").removeClass("ui-state-error");
	
	if($("#tipoDoc2A").val()=='0'){
		$("#tipoDoc2A").addClass("ui-state-error");
		error++;
	}
	if(num.length==0||num==''){
	    $("#identificacion2A").addClass("ui-state-error");
		error++;
	}
	if($("#pNombre2A").val()==''){
		$("#pNombre2A").addClass("ui-state-error");
		error++;
	}
	if($("#pApellido2A").val()==''){
		$("#pApellido2A").addClass("ui-state-error");
		error++;
	}
	if($("#sexo2A").val()=='0'){
		$("#sexo2A").addClass("ui-state-error");
		error++;
	}
	if($("#alternate2A").val()==''){
		$("#alternate2A").addClass("ui-state-error");
		error++;
	}

	if(error==0){
		return 0;
	}else{
		return error;
	}
}

function limpiarBene(){
	$("#parentesco").val(0);
	$("#tipoAfiliacion3").val(0);
	$("#tipoDoc3").val(0).trigger("change");	
	$("#fecNacHide3").val('');
	
	$("#filaDiscapacidad").hide();
	$("#filaEscolaridad").hide();
	$("#filaUniversidad").hide();
	$("#filaSupervivencia").hide();
	$("#filaDependenciaEconomica").hide();
	$("#fEscolaridad,#fUniversidad,#fSupervivencia,#fDiscapacidad,#fDependenciaEconomica").val('');
	$("#cEscolaridad,#cUniversidad,#cSupervivencia,#cDiscapacidad,#cDependenciaEconomica").attr("checked",false);
}

function actualizarEstadoBeneficiario(idr,idbenef,parentesco,estadoAnt,estadoPersona,giro,capacidad,varFcNace,idMadre){
	if(confirm("Esta seguro de cambiar el ESTADO del BENEFICIARIO?")==true){
		$('#hideIdRel').val('');
		var estado='I';		
		
		if(estadoAnt=="I"){
			if ( estadoPersona == "M" ) {
				alert("El beneficiario se encuentra FALLECIDO!!!\n"+
						"No es posible ACTIVAR a este beneficiario");
				return false;
			}
			if ( parentesco == "38" ) {
				if( idMadre > 0 ) {			
					var data=dataRelaciones;
					conviven = false;
					if ( !( data == 0 || data == null )){
						$.each(data,function(i,fila){
							if ( fila.idconyuge == idMadre && fila.conviven == "S" ) {
								conviven = true;
							}
						});
					} 
						
					if ( conviven == false ) {
						alert("EL PADRE o LA MADRE del beneficiario no tiene convienvia (S) con el afiliado.\nNo se puede activar al beneficiario.");		            	
		            	return false;
					}				
				} else {
					alert("Es un HIJASTRO debe tener una Cedula de padre o madre con la cual tenga CONVIVENCIA.!!!");
					return false;
				}
			}
			
			estado='A';
 			
			actualizar=false;
			var retorno = validarEdadActualizar(null,giro,capacidad,parentesco,varFcNace) == false ? 4 : 0;
			if ( retorno == 0 )
				retorno = buscarRelacionActivasBeneficiario( $("#idPersona").val(), idbenef, parentesco, false, false);
			if ( retorno > 1 ) {
				if ( retorno == 4 ) {
					//Ya tiene relacion con el afiliado
				} else if(retorno==3){
					alert("Ocurrio un error validando relaciones del Beneficiario!!");					
				} else {				
					$("#divRelacionesActivas").dialog('open');
					
					if ( retorno == 2 ) {
						alert("Beneficiario tiene el maximo numero de Relaciones!!");
					} else if ( retorno == 34 ) {
						alert("Tiene afiliaciones Activas como Conyuge!!");						
					} else if ( retorno == 35 ) {
						alert("El beneficiario tiene Afiliaciones Activas como Hijo!!!\n"+
								"Revise si es correcto el parentesco que esta seleccionando");						
					} else if ( retorno == 36 ) {
						alert("El beneficiario tiene Afiliaciones Activas como Padre!!!\n"+
								"Revise el parentesco que esta seleccionando");						
					} else if ( retorno == 37 ) {
						alert("El beneficiario tiene Afiliaciones Activas como Hermano!!!\n"+
								"Revise el parentesco que esta seleccionando");						
					} else if ( retorno == 38 ) {
						alert("El beneficiario tiene Afiliaciones Activas como Hijastro!!!\n"+
								"Revise el parentesco que esta seleccionando");						
					} else if ( retorno == 3538 ) {
						var cadena="EL PADRE ";
						if ( $("#" + txtHiddenSexo ).val() == "F") {
							cadena="LA MADRE ";
						}
						alert("El beneficiario tiene Afiliacion Activa como HIJO!!!\n" +
								cadena + "LE TIENE AFILIADO");						
					} else if ( retorno == 3835 ) {
						var cadena="EL PADRASTRO ";
						if ( $("#" + txtHiddenSexo ).val() == "F") {
							cadena="LA MADRASTRA ";
						}
						alert("El beneficiario tiene Afiliaciones Activa como HIJASTRO!!!\n" +
								cadena + "LE TIENE AFILIADO");						
					}
				}
				
				return false;
			}
 		} else { actualizar=true; }
		
		if(estado=='A'){
			updateEstadoBeneficiario(idr,estado,0);
		} else {				
			$('#hideIdRel').val(idr);
			$('#hideIdBeneActEstado').val(idbenef);
			$("#divMotivos").dialog('open');
		}
	}
}

function onchangeMotivo(){
	if ( $('#motivo').val() == 3706 ) {
		$('#defuncionActEstado').show();
	} else  {
		$('#txtFechaDefActEstado').val('');
		$('#defuncionActEstado').hide();
	}
}

function updateEstadoBeneficiario(idr,est,idmot){
	var idper=$("#idPersona").val();
	if( est == 'I' && idmot == 0 ) {
		alert('Seleccione un motivo');
		return;
	} else if ( est == 'I' ) {
		if ( idmot == 3706 ){
			if ( $("#txtFechaDefActEstado").val() == '' ) {
				alert("Debe ingresar la fecha de la defuncion");
				return false;
			}
			if ( confirm("Si continua NO podra acceder al AUXILIO FUNEBRE, \nEsta seguro que desea continuar?") == true ) {				
				if ( parseInt($('#hideIdBeneActEstado').val()) > 0 ){
					var salir=true;
					$.ajax({
						url: URL+'phpComunes/actualizarEstadoPersona.php',
						type:'POST',
						dataType:'json',
						async:false,
						data:{v0:$('#hideIdBeneActEstado').val(),v1:'M',v2:$('#txtFechaDefActEstado').val(),v3:2},
						success:function(data){
							if(!isNaN(data) && parseInt(data)>0){
								salir = false;
							} else {
								alert('Ocurrio un error actualizando al beneficiario');
							}					
						}
					});
					if ( salir ) { return false; }
				} else {
					alert('No se encontro datos del beneficiario a actualizar');
					return false;
				}				
			} else { return false; }
		}
		$("#divMotivos").dialog('close');
	}
		
	$.ajax({
		url: URL+'phpComunes/actualizarEstadoBeneficiario.php',
		type:'POST',
		dataType:'json',
		async:false,
		data:{v0:idr,v1:est,v2:idmot},
		success:function(data){
			if(!isNaN(data) && parseInt(data)>0){
				alert('Actualizado correctamente');
				buscarGrupo();
				observacionesTab(idper,1);
			} else {
				alert('Ocurrio un error actualizando al beneficiario');
			}					
		}
	});
}

function ctrcontroldocumentos(objdata){

	str = JSON.stringify(objdata);
	
	var objrespuesta=null;
	$.ajax({
		url: URL+'phpComunes/controlDocumentosGiro.php',
		async: false,
		data: {v1:str},		
		beforeSend: function(objeto){
        	dialogLoading('show');
        },        
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }            
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("Ocurrio el siguiente error: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
		success: function(data){
			objrespuesta=data;
		},
		timeout: 30000,
        type: "GET"
	});
	
	return objrespuesta;
	
}

function actualizarEstadoGiroBeneficiarioNuevo(idr,giroAnt,fechaAsignacion,modificacion,capacidad,parentesco,varFcNace,idbenef,estado21,estado15,pnombre,papellido){
	
	/*
	 * lineas adicionadas el 23 de Junio de 2016
	 */
	objdata={
			'idr':idr,
			'giroAnt':giroAnt,
			'fechaAsignacion':fechaAsignacion,
			'modificacion':modificacion,
			'capacidad':capacidad,
			'parentesco':parentesco,
			'varFcNace':varFcNace,
		    'idbenef':idbenef,
		    'estado21':estado21,
		    'estado15':estado15,
		    'data':'',
		    'even':'consulta',
		    'obseracion':''
		    	
	}
	
	
	objctrdocumentos=ctrcontroldocumentos(objdata);
	
	if(objctrdocumentos.error>0){
	  alert(objctrdocumentos.descripcionerror);		
	  return false;
	}
	
	
	var ctrtable='<table width="70%">';	
	$.each(objctrdocumentos.data,function(index,value){
		var auxselect= value.controlexiste==0?'':'checked';
		var auxobligatorio= value.obligatorio!=0?'<img src="../../imagenes/menu/obligado.png" alt="" height="12" width="12">':"";
		
		ctrtable+='<tr><td><input type="checkbox" name="'+value.iddatos+'" id="'+value.iddatos+'" '+auxselect+' value="'+value.iddatos+'">'+value.detalledefinicion+' '+auxobligatorio+'</td></tr>';
	});
	ctrtable+='<table>';
	$("#lbnomben>strong").html(pnombre.replace(/_/g," ")+ ' ' + papellido.replace(/_/g," "));
	$("#tablactrdocumentos").html(ctrtable);
	
	$("#divControlDocumentos").data('idr',objdata).dialog("open");
	
	
	//return false;
	
	/*
	 * fin lineas adicionadas 23 de Junio de 2016
	 */
	
	
	
	
}


function actualizarEstadoGiroBeneficiario(idr,giroAnt,fechaAsignacion,modificacion,capacidad,parentesco,varFcNace,idbenef,estado21,estado15){
	if( modificacion == false ) {
		//modificacion = confirm("Esta seguro de cambiar la bandera de GIRO del BENEFICIARIO?"); 
		modificacion = true;
	}
	if( modificacion == true ){
		
		var idper=$("#idPersona").val();	
		var estado='N';
		
		if(giroAnt=='S'){
			estado='N';
		} else {
			if ( estado21 == 'A' && estado15 == 'M' ) { 
				alert("El beneficiario se encuentra FALLECIDO!!!\nNo es posible actualizar a este beneficiario");
				return; 
			}
			
			var retorno = validarEdadActualizar(null,'S',capacidad,parentesco,varFcNace) == false ? 4 : 0;
			if ( retorno == 0 )
				retorno = buscarRelacionActivasBeneficiario( $("#idPersona").val(), idbenef, parentesco, false, true);
			if ( retorno > 1 ) {
				if ( retorno == 4 ) {
					//Ya tiene relacion con el afiliado
				} else if(retorno==3){
					alert("Ocurrio un error validando relaciones del Beneficiario!!");					
				} else {				
					$("#divRelacionesActivas").dialog('open');
					
					if ( retorno == 2 ) {
						alert("Beneficiario tiene el maximo numero de Relaciones!!");
					} else if ( retorno == 34 ) {
						alert("Tiene afiliaciones Activas como Conyuge!!");						
					} else if ( retorno == 35 ) {
						alert("El beneficiario tiene Afiliaciones Activas como Hijo!!!\n"+
								"Revise si es correcto el parentesco que esta seleccionando");						
					} else if ( retorno == 36 ) {
						alert("El beneficiario tiene Afiliaciones Activas como Padre!!!\n"+
								"Revise el parentesco que esta seleccionando");						
					} else if ( retorno == 37 ) {
						alert("El beneficiario tiene Afiliaciones Activas como Hermano!!!\n"+
								"Revise el parentesco que esta seleccionando");						
					} else if ( retorno == 38 ) {
						alert("El beneficiario tiene Afiliaciones Activas como Hijastro!!!\n"+
								"Revise el parentesco que esta seleccionando");						
					} else if ( retorno == 3538 ) {
						var cadena="EL PADRE ";
						if ( $("#" + txtHiddenSexo ).val() == "F") {
							cadena="LA MADRE ";
						}
						alert("El beneficiario tiene Afiliacion Activa como HIJO!!!\n" +
								cadena + "LE TIENE AFILIADO");						
					} else if ( retorno == 3835 ) {
						var cadena="EL PADRASTRO ";
						if ( $("#" + txtHiddenSexo ).val() == "F") {
							cadena="LA MADRASTRA ";
						}
						alert("El beneficiario tiene Afiliaciones Activa como HIJASTRO!!!\n" +
								cadena + "LE TIENE AFILIADO");						
					}
				}
				
				return false;
			}
			
			estado='S';
			if ( fechaAsignacion == "" ) {
				alert("Debe ingresar una fecha de asignacion para este Beneficiario.");
				$('#hideIdRelFechaAsignacionUpdate').val(idr);
				$('#hideCapacidadUpdate').val(capacidad);
				$('#hideParentescoUpdate').val(parentesco);
				$('#hideVarFcNaceUpdate').val(varFcNace);
				$('#hideIdBenefUpdate').val(idbenef);
				if ( $("#divFechaAsignacion").is(":visible") == false ) {
					buscarFechaAsignacion();
				}
				return false;
			} else {
				$("#divFechaAsignacion").dialog('close');
			}
		}
		
		//Adicionar la observacion automaticar para guardarla en las observaciones
		if(estado=="N"){
			$("#hidObservacionAutomatica").val("SUSPENSION");
		}else if (estado=="S"){
			$("#hidObservacionAutomatica").val("ACTUALIZACION DOCUMENTACION");
		}else{
			$("#hidObservacionAutomatica").val("");
		}
		
		
		$.ajax({
			url: URL+'phpComunes/actualizarEstadoGiroBeneficiario.php',
			type:'POST',
			dataType:'json',
			async:false,
			data:{v0:idr,v1:estado,v2:fechaAsignacion,ctrobservacion:$("#ttarobservacion").val()},
			success:function(data){
				if(!isNaN(data) && parseInt(data)>0){
					alert('Actualizado correctamente');
					buscarGrupo();
					observacionesTab(idper,1);
				} else {
					alert('Ocurrio un error actualizando al beneficiario');
				}					
			}
		});		
	}
}

function contarRelacionesBeneficiarioActivas(idbenef,max){
	var retorno=false;
	$.ajax({
		url: URL+'phpComunes/buscarRelacionesActivasBeneficiarioHijoHijastro.php',
		type: "POST",
		dataType:"json",
		async:false,
		data: {v0:idbenef},
		success: function(dataRelacion){
			if(!isNaN(dataRelacion) && parseInt(dataRelacion)<max){							
				retorno=true;				
			} 
		}
	});
	return retorno;
}

function buscarRelacionConBeneficiario(idp,idb){
	var retorno=0;
	$.ajax({
		url: URL+'phpComunes/buscarRelacionBenefTrabajador.php',
		type: "POST",
		dataType:"json",
		async:false,
		data: {idTrabajador:idp,idBeneficiario:idb},
		success: function(dataRelacion){
			if(dataRelacion){ retorno=dataRelacion; } 
		}
	});
	return retorno;
}

function buscarAfiliacionBeneficiario(idb){
	var retorno=0;
	$.ajax({
		url: URL+'phpComunes/buscarAfiliacion.php',
		type: "POST",
		dataType:"json",
		async:false,
		data: {v0:idb},
		success: function(dataAfiliacion){
			if(dataAfiliacion){ retorno=dataAfiliacion; } 
		}
	});
	return retorno;
}

function validarFechaNaceBeneficiario(parentesco,fechaNaceBene,varFcNace){
	idPersona=parseInt($("#idPersona").val());	
	if (isNaN(idPersona)) { 
		alert("No hay ID de la persona!");
		if ( varFcNace == 0 )
			limpiarBene();		
		return false;
	}	
	var fcNace = ( varFcNace == 0 ? fechaNaceBene.val() : varFcNace );
	if(parentesco==0 || fcNace==""){ return false; }
	var retorno=false;
	
	$.ajax({
		url:URL+"phpComunes/pdo.b.fnaceafi.php",
		type:'POST',
		dataType:'json',
		data :{v0:idPersona},
		async:false,
		success:function(data){
			if(data==0){
				alert("No hay fecha de nacimiento del afiliado, por favor actualice primero esta informacion!");
				if ( varFcNace == 0)
					limpiarBene();				
			} else {
				var fx=data.split('-');
				fnace=fx[2]+"/"+fx[1]+"/"+fx[0];
				fx=fcNace.split('-');
				fbenef=fx[2]+"/"+fx[1]+"/"+fx[0];				
				var dd=diferenciaFechas(fnace,fbenef);
				
				if(parentesco==35 && (dd >= 0)){
					alert("El beneficiario es Mayor que el afiliado?, por favor revise las fechas de nacimiento!");
					if ( varFcNace == 0 )
						fechaNaceBene.val('');										
				} else if(parentesco==36 && (dd <= 0)){					
					alert("El beneficiario es Menor que el afiliado?, por favor revise las fechas de nacimiento!");
					if ( varFcNace == 0 )
						fechaNaceBene.val('');										
				} else {
					retorno=true;
				}				
			}
		}
	});
	
	return retorno;
	
}

/***
 * 
 * @param idt 		//idtrabajador
 * @param idb 		//idBeneficiario
 * @param idp 		//idParentesco
 * @param nuevo 	//Es desde el Formulario Nuevo Beneficiario?
 * @returns 		// 0 Si no tiene relaciones
 * 					// Afiliacion Diferente e incompatible
 * 						// 34 relacion como conyuge activa 
 * 						// 35 relacion como hijo activa
 * 						// 36 relacion como padre activa
 * 						// 37 relacion como hermano activa
 * 						// 38 relacion como hijastro activa
 * 					// 1 Si cumple las validaciones
 * 					// 2 Si tiene el numero maximo de afiliaciones
 * 					// 3 Si existe un error con las validaciones
 * 					// 4 Ya tiene una relacion con el afiliado
 * 					// 5 Ya tiene relacion de convivencia con el afiliado, pero este otro no
 * 					
 */

function buscarRelacionActivasBeneficiario(idt,idb,idp,nuevo,modificar){
	var contA=0;
	var retorno=0;	
	$("#tbRelacionesActivas").empty();
	var rutaConsulta='buscarRelacionesActivas.php';
	
	if ( idp == 34 )
		rutaConsulta='buscarRelacionesActivasConyuge.php';
	
	$.ajax({
		url: rutaConsulta,
		type: "POST",
		dataType:"json",
		data: {v0:idb},
		async:false,		
		success: function(dataRelacion){
			if(dataRelacion){
				$.each(dataRelacion,function(i,fila){
					if(nuevo && fila.idtrabajador==idt){
						alert("La persona ya tiene una relacion con el Afiliado!!");
						retorno=4;
						return;
					//} else if ((fila.estado=="A" && fila.idparentesco != 34) || (fila.idparentesco == 34 && fila.conviven == "S")){
					} else if (fila.estado=="A" || (idp == 34 && fila.idparentesco ==34) ){
						if(fila.idtrabajador!=idt){
							contA++;							
							
							/*** VALIDACION DISTINTO TIPO PARENTESCO ***/
							if ( (retorno==0 || retorno==5 ) && fila.idparentesco == 34 ) {
								if ( fila.conviven=="S" ) {
									if ( idp == 36 ){
										var condicion = (buscarAfiliacionBeneficiario(fila.idtrabajador)==0) ? false : true;
										if ( condicion ) { retorno=34; }
										else { contA--; }
									} else if ( idp == 34 && fila.idtrabajador == idb && fila.idbeneficiario == idt ) {
										retorno=5;
									} else {
										retorno=34;
									}								
								} else {
									contA--;
								}
							} else if ( (retorno==0) && fila.idparentesco == 35 ) {
								if ( idp == 34 || idp == 36 || idp == 37 ) {
									retorno=35;
								} else if ( idp == 38 ) {
									if ( fila.sexo == $("#" + txtHiddenSexo).val() ) {
										retorno=3538;
									}
								}
							} else if ( (retorno==0) && fila.idparentesco == 36 ) {
								if ( idp == 34 || idp == 35 || idp == 36 || idp == 37 || idp == 38 ) {
									retorno=36;
								}
							} else if ( (retorno==0) && fila.idparentesco == 37 ) {
								if ( idp == 34 || idp == 35 || idp == 36 || idp == 37 || idp == 38 ) {
									retorno=37;
								}
							} else if ( (retorno==0) && fila.idparentesco == 38 ) {
								if ( idp == 34 || idp == 36 || idp == 37 ) {
									retorno=38;
								} else if ( idp == 35 ) {
									if ( fila.sexo == $("#" + txtHiddenSexo).val() ) {
										retorno=3835;
									}
								} else if ( idp == 38 ) { 
									contA++; 
								}
							}
							
							if ( !(retorno==0 || retorno==5 ) || fila.idparentesco != 34 ){
								nom = fila.pnombre + " " +fila.snombre +" "+ fila.papellido + " " + fila.sapellido;
								$("#tbRelacionesActivas").append(
									"<tr>" +
										"<td>"+fila.codigo+"</td>" +
										"<td>"+fila.identificacion+"</td>" +
										"<td>"+nom+"</td>" +
										"<td><center>"+fila.detalledefinicion+"</center></td>" +
										"<td><center>"+fila.conviven+"</center></td>" +
									"</tr>");
							}
						} else if ( modificar == false ) {
							alert("Ya tiene una relacion Activa con el Afiliado!!");
							retorno=4;
							return;
						}
					}					
				});				
			} 
		}
	});
	
	/*** VALIDACION MAXIMO AFILIACIONES ***/
	if ( (retorno==0) && idp == 35 ) {
		if(contA<2) { retorno=1; }
		else { retorno=2; }
	} else if ( (retorno==0) && idp == 36 ) {
		if(contA<1) { retorno=1; }
		else { retorno=2; }
	} else if ( (retorno==0) && idp == 37 ) {
		if(contA<1) { retorno=1; }
		else { retorno=2; }
	} else if ( (retorno==0) && idp == 38 ) {
		if(contA<2) { retorno=1; }
		else { retorno=2; }
	} else if ((retorno==0)){
		retorno=3;
	}
	
	return retorno;
}

function buscarConyugeNuevo(objNum){
	idcony=0;
	existe=0;
	limpiarConyuge();
	
	var idtd=$("#tipoDoc2").val();
	if ( (objNum.value).length == 0 ) {
		return false;
	} else if ( idtd == 0 ) {
		alert("Falta el Tipo Documento!!");
		objNum.value="";
		return false;
	} 
	
	validarLongNumIdent(document.getElementById('tipoDoc2').value,objNum);
	if ( (objNum.value).length == 0 ) {
		return false;
	}
	
	num=$.trim(objNum.value);	
	$.ajax({
		url: URL+'phpComunes/buscarConyugeNuevo.php',
		async: false,
		data: "submit &v0="+idtd+"&v1="+num,		
		beforeSend: function(objeto){
        	dialogLoading('show');
        },        
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }            
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("En buscarConyugeNuevo Paso lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
		success: function(datoPersona){
			if(datoPersona){
				$.each(datoPersona,function(i,fila){
					if ( fila.idpersona == idPersona ) {
						alert("No se puede crear una relacion con la misma persona");		    		
						objNum.value="";						
					return;
					} else if ( fila.estado15 == "M" ){
						alert("La persona se encuentra FALLECIDO!!!\n"+
							"No es posible crearle una relacion de convivencia");		    		
						objNum.value="";						
						return;
					} else {						
						idcony=fila.idpersona;
						
						/** VALIDACIONES **/
						var retorno = buscarRelacionActivasBeneficiario( $("#idPersona").val(), idcony, 34, true, false);
						if ( retorno > 1 && retorno != 3 && retorno != 35) {
							if ( retorno == 5 ) {
								// El beneficiario tiene relacion con este
							} else if ( retorno == 34 ) {	
								$("#divRelacionesActivas").dialog('open');								
								alert("Tiene afiliaciones Activas como Conyuge!!");
								$('#conviven').val('N');
								$('#conviven').attr('disabled','disabled');
							} else {
								if ( retorno != 4 ){									
									$("#divRelacionesActivas").dialog('open');
								}
								if ( retorno == 4 ) {
									// Ya tiene relacion con el afiliado
								} else if ( retorno == 34 ) {
									alert("Tiene afiliaciones Activas como Conyuge!!");
									$('#conviven').val('N');
									$('#conviven').attr('disabled','disabled');									
								} /*else if ( retorno == 35 ) {
									alert("La persona tiene Afiliaciones Activas como Hijo!!!\n"+
											"Inactive la relacion para poder continuar");						
								}*/ else if ( retorno == 36 ) {
									alert("La persona tiene Afiliaciones Activas como Padre!!!\n"+
											"Inactive la relacion para poder continuar");						
								} else if ( retorno == 37 ) {
									alert("La persona tiene Afiliaciones Activas como Hermano!!!\n"+
											"Inactive la relacion para poder continuar");						
								} else if ( retorno == 38 ) {
									alert("La persona tiene Afiliaciones Activas como Hijastro!!!\n"+
											"Inactive la relacion para poder continuar");						
								} else {
									alert("Ocurrio un error validando parentesco de la persona!!!\n"+
											"Retorno:" +retorno);			
								}
								idcony=0;
								objNum.value="";								
								return;
							}
						}
						
						existe=1;
						idcony=fila.idpersona;
						nombreCorto=fila.nombrecorto;
						idProfesion=fila.idprofesion;
						rutaDocumentos=fila.rutadocumentos;						
						
						$('#pNombre2').val($.trim(fila.pnombre));
						$('#sNombre2').val($.trim(fila.snombre));
						$('#pApellido2').val($.trim(fila.papellido));
						$('#sApellido2').val($.trim(fila.sapellido));
						$('#sexo2').val(fila.sexo);
						$("#tipoVivienda2").val(fila.idtipovivienda);
						
						
						$("#cboDeptoC").val($.trim(fila.iddepresidencia)).trigger('change');
						$("#cboCiudadC").val($.trim(fila.idciuresidencia)).trigger("change");						    
						$("#cboZonaC").val($.trim(fila.idzona)).trigger("change");
						$("#cboBarrioC").val($.trim(fila.idbarrio));
						$("#cboPaisC2").val($.trim(fila.idpais)).trigger('change');												
						$("#cboDeptoC2").val($.trim(fila.iddepnace)).trigger('change');						    
						$("#cboCiudadC2").val($.trim(fila.idciunace));							
						
						$("#direccion2").val($.trim(fila.direccion));
						$("#email2").val($.trim(fila.email));
						$("#telefono2").val(fila.telefono);
						$("#celular2").val(fila.celular);
						$("#alternate2").val(fila.fechanacimiento);						
						
						$("#estadoCivil2").val(fila.idestadocivil);
						$("#capTrabajo2").val(fila.capacidadtrabajo);
						$("#profesion2").val(fila.idprofesion);
						
						if( !isNaN(fila.idformulario) && fila.idformulario>0){
							alert("La persona que trata de Afiliar es un trabajador activo!");
							$("#trInformacionAfiliacion,#trInformacionAfiliacion2,#trInformacionAfiliacion3").show();
							$('#nitConyuge').val(fila.nit);
					    	$('#empCony').val(fila.razonsocial);
					    	$('#salCony').val(fila.salario);
					    	$('#subCony').val(fila.detalledefinicion);
						}
					}
					return;
				});
			} else {
				buscarPersonaExistente($("#identificacion2").val(),$("#pNombre2").val(),$("#pApellido2").val(),false,idcony);
				return false;
			}
		},		
        type: "GET"							
	});	
}

function buscarPersonaMostrar(td,num,id,nom){
	nom.html(" "); 
	id.val("");
	
	if ( (num.val()).length == 0 ) {
		return false;
	} else if ( td.val() == 0 ) {
		alert("Falta tipo documento del padre Biologico!!");
		num.val("");
		return false;
	} 
	
	$.ajax({
		url: URL+'phpComunes/pdo.buscar.persona.php',
		type: "POST",
		data: { v0:td.val(),v1:num.val(),v2:2},
		async: false,
		dataType: "json",
		success: function(datos){
			if(datos){
				var nombre=datos[0].pnombre+" "+datos[0].snombre+" "+datos[0].papellido+" "+datos[0].sapellido;
				id.val(datos[0].idpersona);
				nom.html(nombre);										
			} else {
				alert("La persona NO Existe en nuestra Base, guarde los datos Completos!");
				persona_simple(num.get(0),id.get(0),td.val());
				return false;
			}
		}
	});
}

function actualizaBasico2(idpersonaActualiza,giro,parentesco,discapacitado){	
	idpersonaActualiza=$.trim(idpersonaActualiza);
	
	$("#dialog-persona2").dialog("destroy");	
	$("#dialog-persona2").dialog({
		show: "drop",
		hide: "clip",
		width:750,
		modal: true,
		open:function(event,ui){
			$.ajax({
				url:URL+'phpComunes/pdo.buscar.persona.id.php',
				async: false,
				data:{v0:idpersonaActualiza},
				dataType:"json",
				beforeSend: function(objeto){
		        	dialogLoading('show');
		        },        
		        complete: function(objeto, exito){
		        	dialogLoading('close');
					if(exito != "success"){
		                alert("No se completo el proceso!");
		            }            
		        },
		        contentType: "application/x-www-form-urlencoded",
		        dataType: "json",
		        error: function(objeto, quepaso, otroobj){
		            alert("En actualizaBasico2 Paso lo siguiente: "+quepaso);
		        },
		        global: true,
		        ifModified: false,
		        processData:true,
		        success:function(data){
				    if(data==0){
				    	return false;
				    }else {
						$.each(data,function(i,fila){
							$('#txtHiddenIdPersona').val(fila.idpersona);
							$('#cmbDBTipoDoc').val(fila.idtipodocumento);
							$('#txtDBIdentificacion').val(fila.identificacion);
							$('#txtDBpNombre').val(fila.pnombre);
							$('#txtDBsNombre').val(fila.snombre);
							$('#txtDBpApellido').val(fila.papellido);
							$('#txtDBsApellido').val(fila.sapellido);
							$('#txtDBfecNace').val(fila.fechanacimiento);
							$("#cboDBPaisNace").val($.trim(fila.idpais)).trigger('change');												

							$("#cboDBDeptoNace").val($.trim(fila.iddepnace)).trigger('change');						    
							$("#cboDBCiudadNace").val($.trim(fila.idciunace));
							
							$('#cmbDBSexo').val(fila.sexo);												   						   	
							$('#txtHiddenIdGiro').val(giro);
							$('#txtHiddenIdparentesco').val(parentesco);
							$('#txtHiddenDiscapacitado').val(discapacitado);
						 }); //fin each
				    }
				},
				type:"POST"
			 }); //ajax buscarpersona.php
		},
		
		buttons: {
			'Actualizar': function() {
				var salir=false;
				var idPersonaActualizar=$('#txtHiddenIdPersona').val();
				var tipoDocActualizar=$('#cmbDBTipoDoc').val();
				var identificacionActualizar=$.trim($('#txtDBIdentificacion').val());
				var pNombreActualizar=$.trim($('#txtDBpNombre').val());
				var sNombreActualizar=$.trim($('#txtDBsNombre').val());
				var pApellidoActualizar=$.trim($('#txtDBpApellido').val());
				var sApellidoActualizar=$.trim($('#txtDBsApellido').val());
				var fNaceActualizar=$.trim($('#txtDBfecNace').val());
				var idPaisActualizar=$("#cboDBPaisNace").val();												
				var idDptoActualizar=$("#cboDBDeptoNace").val();				
				var idCiudadActualizar=$("#cboDBCiudadNace").val();						
				var sexoActualizar=$('#cmbDBSexo').val();
				
				if( tipoDocActualizar == 0 ){
					alert("Seleccione un tipo de documento!");					
					return false;
				} else if( identificacionActualizar.length == 0 || identificacionActualizar == '' ){
					alert("El campo No. Identificacion no puede ser vacio!");					
					return false;
				} else if( pNombreActualizar.length == 0 ){
					alert("Ingrese el primer nombre.!");					
					return false;
				} else if(pApellidoActualizar.length == 0){
					alert("Ingrese el primer apellido.!");
					return false;
				} else if(fNaceActualizar.length == 0){
					alert("Ingrese la fecha de nacimiento.!");					
					return false;
				}
				
				$.ajax({
					url: URL +"phpComunes/verifIdentificacionDisponible2.php",
					type: "POST",
					data: {idTipo: tipoDocActualizar, numero: identificacionActualizar, idPersona: idPersonaActualizar},
					async: false,
					dataType: "json",
					success: function(respuesta){
						if(respuesta){
							$("#tbDocumentoDoble").empty();
							$.each(respuesta,function(i,fila){
								nom = fila.pnombre + " " +fila.snombre +" "+ fila.papellido + " " + fila.sapellido;
								$("#tbDocumentoDoble").append(
										"<tr>" +
											"<td>" + fila.codigo + "</td>" +
											"<td style='text-align:right'>" + fila.identificacion + "</td>" +
											"<td>" + fila.idpersona + " - " + nom + "</td>" +									
										"</tr>");
							});
							alert("Verifique la informaci\xf3n, ya existe una persona con ese tipo y numero de documento!!!");
							$("#divDocumentoDoble").dialog("open");
							return false;
						} else {
							var datos={v0:idPersonaActualizar,v1:tipoDocActualizar,v2:identificacionActualizar,v3:pNombreActualizar,
									v4:sNombreActualizar,v5:pApellidoActualizar,v6:sApellidoActualizar,v7:fNaceActualizar,v8:sexoActualizar,
									v9:idPaisActualizar,v10:idDptoActualizar,v11:idCiudadActualizar};
							
							$.ajax({
								url: URL+'phpComunes/pdo.modificar.persona.simple2.php',
								type: "POST",
								data: datos,
								async: false,
								//dataType: "json",
								success: function(data){
							 		if(data==0){
							 			alert("No se pudo actualizar el Beneficiario!");
							 			return false;
									} else {
								 		alert("Se actualizo el Beneficiario.");
								 		$("#dialog-persona2").dialog("close");
								 		buscarGrupo();							 		
								 		observacionesTab(idPersona,1);
									}
								}
							});
						}
					}
				});	
			}
		}
	});
}
	
function validarEdadActualizar(obj,giro,capacidad,parentesco,varFcNace){
	var fechaNaceValEdad = ( varFcNace == 0 ? obj.val() : varFcNace);
	
	salir=!validarFechaNaceBeneficiario(parentesco,obj,varFcNace);
	if(salir){ return false; }
	
	var fechaNacimientoValEdad = new Date(fechaNaceValEdad);
	var tiempoFechaNace = fechaNacimientoValEdad.getTime();
	var fechaActualValEdad = new Date();
	var tiempoFechaActual = fechaActualValEdad.getTime();
	
	if( capacidad == 'N' && parentesco == 36 ){
		var tiempoMS60Anios = 60*60*24*(365)*1000*60;
		var tiempoMS60AnioConVisiestos = tiempoMS60Anios + (60*60*24*1000*(60/4));
		if(tiempoFechaNace  >= (tiempoFechaActual - tiempoMS60AnioConVisiestos)){
			alert("La Edad Minima de un Afiliado Padre es 60 A\u00F1os");
			if ( varFcNace == 0 )
				obj.val("");	
			return false;
		} 
	} else if ( capacidad == 'N' && (parentesco == '35' || parentesco == '38' || parentesco == '37') ) {
		var tiempoMS19Anios = 60*60*24*365*1000*19;
		var tiempoMS19AnioConVisiestos = tiempoMS19Anios + (60*60*24*1000*(19/4));					
		if(tiempoFechaNace  <= (tiempoFechaActual - tiempoMS19AnioConVisiestos)){
			alert("La Edad Maxima de un Afiliado Hijo/Hijastro/Hermano es Hasta 19 A\u00F1os");
			if ( varFcNace == 0 )
				obj.val("");
			return false;
		} 
	}
}

function buscarFechaAsignacion(){
	jQuery('div[aria-labelledby^=ui-dialog-title-divFechaAsignacion]').remove();
	jQuery('div[class^=ui-effects-wrapper]').remove();
	$("#divFechaAsignacion").dialog("destroy");
	$("#divFechaAsignacion").dialog({
		//show: "drop",
		hide: "clip",
		width:270,
		height:120,
		modal: true,
		resizable:false,
		open:function(event,ui){
			$("#txtFechaAsignacionUpdate").val('');
			$("#txtFechaAsignacionUpdate").datepicker('destroy');
			$("#btnAcepFechaAsignacionUpdate").focus();
			
			$.ajax({
				url:URL+'phpComunes/buscarFechaAsignacion.php',
				async: false,
				data:{v0:$("#idPersona").val()},
				dataType:"json",
		        success:function(data){
				    if(data){				    	
				    	var fechaIni; var fechaMax;				    	
				    	var diferencia = data[0].diferencia;	
				    	fechaIni= ( data[0].maxIngreso == "" ) ? "1900/01/01"  : (data[0].maxIngreso).replace("-","/");
				    	fechaIni=fechaIni.replace("-","/");
				    	fechaMax = '+0D';
				        if ( diferencia > 0 ) {
				        	fechaIni = "-3M";
				        } else {
				        	if ( fechaIni == "1900/01/01") {
				        		fechaIni = "-3M";
				        	}
				        }							
				        	
				        $("#txtFechaAsignacionUpdate").datepicker({
				    	    changeMonth: true,
				    	    changeYear: true, 
				    	    minDate: fechaIni, 
				    	    maxDate: fechaMax    
				    	});
				    } else {
				    	$("#txtFechaAsignacionUpdate").datepicker();
				    	return false;
				    }
		        }
			});
		},
		close:function(){			
			$("#txtFechaAsignacionUpdate").val('');
			$("#txtFechaAsignacionUpdate").datepicker('destroy');
			jQuery('div[aria-labelledby^=ui-dialog-title-divFechaAsignacion]').remove();
			jQuery('div[class^=ui-effects-wrapper]').remove();
			$("#divFechaAsignacion").dialog("destroy");
		}
	});
}

function setDatepickerFechaAsignacion(){
	var fechaIni; var fechaMax;
	$("#fAsignacion, #txtFAsignacion").datepicker('destroy');
	var diferencia = $("#txtDiferenciaFechaIngreso").val();	
	fechaIni= ( $("#txtFechaIngresoM").val() == "" ) ? "1900/01/01"  : ($("#txtFechaIngresoM").val()).replace("-","/");
	fechaIni=fechaIni.replace("-","/");
	fechaMax = '+1M';
    if ( diferencia > 0 ) {
    	fechaIni = "-3M";
    } else {
    	if ( fechaIni == "1900/01/01") {
    		fechaIni = "-3M";
    	}
    }						
    	
    $("#fAsignacion, #txtFAsignacion").datepicker({
	    changeMonth: true,
	    changeYear: true, 
	    minDate: fechaIni, 
	    maxDate: fechaMax    
	});
}

function buscarVigenciaCertificado(idben,parentesco){	
	var cert=0;	
	var v0,v1,v2,v3,v4,v5;				
	v0 = idben;			//idbeneficiario
	v1 = parentesco;	//idparentesco
	
	if($("#pInicialV3").val()!="" && $("#pFinalV3").val()!=""){		
		v3 = $("#pInicialV3").val();
		v4 = $("#pFinalV3").val();
		v5 = "P";
		
		if($("#cDiscapacidad").is(":checked")){
			cert=2;		
		} else if($("#cEscolaridad,#cSupervivencia,#cUniversidad").is(":checked")){
			cert=1;
			v2 = ( $("#cEscolaridad").is(":checked") ? '55' : ( $("#cUniversidad").is(":checked") ? '56' : '57' ) );		
		} else {
			alert("No se pudo determinar el tipo de certificado");
			return false;
		}
		
		var idCertificado = ( $("#cEscolaridad").is(":checked") ? 55 : ( $("#cUniversidad").is(":checked") ? 56 : ( $("#cSupervivencia").is(":checked") ? 57 : 58 ) ) );
		
		if( validarExisteCertificado(v0, idCertificado, v3, v4) ){
			//alert("Ya existe un certificado para el Beneficiario y en ese Periodo!");
			return false;
		}
		
		if(cert==1){
			$.ajax({
				url:URL+"aportes/radicacion/grabarCertificado.php",
				type:"POST",
				data:{v0:v0,v1:v1,v2:v2,v3:v3,v4:v4,v5:v5},
				async:false,
				success:function(data){
					alert(data);
				}
			});
		} else if(cert==2){
			$.ajax({
				url:URL+"aportes/trabajadores/marcarDiscapacitado.php",
				type:"POST",
			    data:{v0:idPersona,v1:v0,v2:v1},
				async:false,
				success:function(datos){
					if(datos==1){
						alert("Beneficiario marcado como Discapacitado y grabado el certificado!");
					}else{
						alert("No se pudo guardar el certificado de discapacidad");
					}
				}
			});
		}
	}
	
	if($("#cDependenciaEconomica").is(":checked")){
		//Grabar certificado de dependencia economica
		v2 = idCertiDepenEcono;
		v3 = $("#txtPerioIniciVigenHijastro3").val();
		v4 = $("#txtPerioFinalVigenHijastro3").val();
		
		if( validarExisteCertificado(v0,idCertiDepenEcono, v3, v4) )return false;
		
		$.ajax({
			url:URL+"aportes/radicacion/grabarCertificado.php",
			type:"POST",
			data:{v0:v0,v1:v1,v2:v2,v3:v3,v4:v4,v5:v5},
			async:false,
			success:function(data){
				alert(data);
			}
		});
	}
}

function validarFechaLimiteEdad(fechaIni, fechaFin, annos, formu){
	var fechaNaceValEdad;
	if(formu == 1){
		fechaNaceValEdad = $("#fecNac3Upd").val();
	} else if( formu == 0 ) {
		fechaNaceValEdad = $("#fecNacHide3").val();
	}
	var annoCumple = parseInt(fechaNaceValEdad.slice(0,4)) + annos;
	var mesCumple = fechaNaceValEdad.slice(5,7);
	var mesCumple2=mesNumero(mesCumple);
	var annoIni = parseInt(fechaIni.slice(0,4));
	var mesIni = mesNumero(fechaIni.slice(4,6));
	var annoFin = parseInt(fechaFin.slice(0,4));
	var mesFin = mesNumero(fechaFin.slice(4,6));
		
	if( annoCumple < annoIni || ( annoCumple == annoIni && mesCumple2 < mesIni ) ){				//Si cumplio antes de la fecha Inicial		
		alert("Ha salido del rango de edad permitido para presentacion del certificado" );		
		return "0";
	} else if( annoCumple > annoFin || ( annoCumple == annoFin && mesCumple2 >= mesFin ) ){		//Si cumplio despues de la fecha Final		
		return "1";
	} else {																					//Si cumplio dentro del periodo que cubre el certificado		
		return annoCumple + mesCumple;
	}
}

function comparaFecha(fechaMinima, fecha, fechaFin){
	var anno = parseInt(fecha.slice(0,4));
	var mes = mesNumero(fecha.slice(4,6));
	var annoFin = parseInt(fechaFin.slice(0,4));
	var mesFin = mesNumero(fechaFin.slice(4,6));
	var annoMinima = parseInt(fechaMinima.slice(0,4));
	var mesMinimo = fechaMinima.slice(5,7);
	var mesMinima = mesNumero(mesMinimo);
		
	if( anno < annoMinima || ( anno == annoMinima && mes < mesMinima ) ){
		if ( annoMinima > annoFin || ( annoMinima == annoFin && mesMinima > mesFin ) ) {
			return -1;
		} else {
			return annoMinima + mesMinimo;
		}
	} else {			
		return 0;
	}
}

function mesNumero(mes){
	if(mes.slice(0,1)=="0")
		return parseInt(mes.slice(1,2));
	else
		return parseInt(mes);
}

function setFechaAsignaCert(chk, obj){
	$("#pInicialV3,#pFinalV3").val("");
	$("#vigencia1,#vigencia2").attr("checked",false);
	if( chk.get(0).id == "cEscolaridad" ) {
		$("#fUniversidad").val('');		
	} else if ( chk.get(0).id == "cUniversidad" ) {
		$("#fEscolaridad").val('');
	}
	
	if(chk.is(":checked")){
		var today=new Date();
		var dd = today.getDate();
		var mm = today.getMonth() + 1 ;
		mm = mm < 10 ? "0" + mm : mm;
		var yy = today.getFullYear();
		obj.val(yy + "-" + mm + "-" + dd);
	} else {
		obj.val("");
	}
}

function setFechaAsignaCertHijastro(chk, obj){
	$("#txtPerioIniciVigenHijastro3,#txtPerioFinalVigenHijastro3").val("");
	$("#radVigenciaHijastro1,#radVigenciaHijastro2").attr("checked",false);
	if( chk.get(0).id != "cDependenciaEconomica" ) {
		$("#fDependenciaEconomica").val('');		
	}
	
	if(chk.is(":checked")){
		var today=new Date();
		var dd = today.getDate();
		var mm = today.getMonth() + 1 ;
		mm = mm < 10 ? "0" + mm : mm;
		var yy = today.getFullYear();
		obj.val(yy + "-" + mm + "-" + dd);
	} else {
		obj.val("");
	}
}

function buscarPersonaExistente(num,pnom,snom,pape,sape,blurNombre,existe){
	if ( ( existe == 1 || parseInt(existe) > 0 ) || ( blurNombre == true && ( $.trim(pnom).length == 0 || $.trim(pape).length == 0 ) ) ) return false;
	
	$.ajax({
		url:URL+'phpComunes/buscarPersonaExistente.php',
		async: false,
		data:{v0:num,v1:pnom,v2:snom,v3:pape,v4:sape},
		dataType:"json",
		beforeSend: function(objeto){
        	dialogLoading('show');
        },        
        complete: function(objeto, exito){
        	dialogLoading('close');
			if(exito != "success"){
                alert("No se completo el proceso!");
            }            
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("En buscarPersonaExistente Paso lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
        success:function(respuesta){
        	if(respuesta){
				$("#tbDocumentoDoble").empty();
				var cont=0;
				$.each(respuesta,function(i,fila){
					cont++;
					if ( fila.identificacion == num && blurNombre == true ) { cont--; } 
					nom = fila.pnombre + " " +fila.snombre +" "+ fila.papellido + " " + fila.sapellido;
					$("#tbDocumentoDoble").append(
							"<tr>" +
								"<td>" + fila.codigo + "</td>" +
								"<td style='text-align:right'>" + fila.identificacion + "</td>" +
								"<td>" + fila.idpersona + " - " + nom + "</td>" +									
							"</tr>");
				});
				if ( cont > 0 ) {
					alert("Existen personas con datos similares,\nCompruebe si es una de ellas");
					$("#divDocumentoDoble").dialog("open");
				}
				return false;
			}
        }
	});
}

function vVigencia(op, bandeCertiDepenEcono){
	var fechaActual = new Date();
	var annoActual = parseInt( fechaActual.getFullYear() );
	var mesActual = fechaActual.getMonth() + 1;
	var ciclo,tcert,idcert;
	var pi,pf,mesInicia,mesFin,mesPresenta;
	
	if(bandeCertiDepenEcono==true){
		$("#errorVigHijastro").empty();
		$("#txtPerioIniciVigenHijastro3,#txtPerioFinalVigenHijastro3").val("");
	}else{
		$("#errorVig").empty();
		$("#pInicialV3,#pFinalV3").val("");
	}
	
	if ( $("#fAsignacion").val() == 0 ) {
		alert ("Ingrese una fecha de Asignacion");
		$("#vigencia1,#vigencia2").attr("checked",false);
		$("#radVigenciaHijastro1,#radVigenciaHijastro1").attr("checked",false);
		return false;
	}
	
	var banderaError = false;
	if($("#cEscolaridad,#cSupervivencia,#cDiscapacidad").is(":checked")){
		ciclo=1;
		tcert = ( $("#cEscolaridad").is(":checked") ? 55 : ( $("#cSupervivencia").is(":checked") ? 57 : 58 ) );
		idcert = ( $("#cEscolaridad").is(":checked") ? 'tc0' : ( $("#cSupervivencia").is(":checked") ? 'tc3' : 'tc4' ) );
	} else if($("#cUniversidad").is(":checked")){
		tcert=56;
		if ( op == 0 ){
			if( mesActual >= 7 && mesActual <= 12 ){ ciclo=3; idcert= 'tc2'; }
			else { ciclo=2; idcert= 'tc1'; }
		} else {
			if(mesActual >= 7 && mesActual <= 12){ ciclo=2; idcert= 'tc1'; }
			else { ciclo=3; idcert= 'tc2'; }
		}
	
	//} else if(!$("#cDependenciaEconomica").is(":checked")) {
	}else{
		//alert ("No Hay seleccionado un certificado");
		$("#vigencia1,#vigencia2").attr("checked",false);
		//$("#radVigenciaHijastro1,#radVigenciaHijastro1").attr("checked",false);
		//return false;
		banderaError = true;
	}
	
	// Certificado dependencia economica para hijastros
	if($("#cDependenciaEconomica").is(":checked") && bandeCertiDepenEcono==true){ 
		//ciclo=0;
		tcert = idCertiDepenEcono;
		//idcert = 'tc00';
		idcert= 'tc5';
		ciclo=1; 
		
		/*if ( op == 0 ){
			if( mesActual >= 7 && mesActual <= 12 ){ ciclo=3; idcert= 'tc5'; }
			else { ciclo=2; idcert= 'tc6'; }
		} else {
			if(mesActual >= 7 && mesActual <= 12){ ciclo=2; idcert= 'tc6'; }
			else { ciclo=3; idcert= 'tc5'; }
		}*/
	}else if(!$("#cDependenciaEconomica").is(":checked") && banderaError == true){
		$("#txtPerioIniciVigenHijastro3,#txtPerioFinalVigenHijastro3").val("");
		$("#radVigenciaHijastro1,#radVigenciaHijastro1").attr("checked",false);
		alert ("No Hay seleccionado un certificado");
		return false;
	}
	//else{
	//	$("#txtPerioIniciVigenHijastro3,#txtPerioFinalVigenHijastro3").val("");
	//	$("#radVigenciaHijastro1,#radVigenciaHijastro1").attr("checked",false);
	//}
	
	$.ajax({
		url: URL+"phpComunes/buscarVigencia.php",
		cache: false,
		type: "GET",
		dataType:"json",
		data: {v0:tcert,v1:ciclo},
		async: false,
		success: function(datos){
			if ( op == 0 ) {
				if ( parseInt(idnb) > 0 ) {
					$.ajax({
						url: URL+"phpComunes/buscarCertPeriodoAnterior.php",
						cache: false,
						type: "GET",
						dataType:"json",
						data: {v0:idnb,v1:ciclo},
						async: false,
						success: function(cont){
							if ( cont == 0 ) {
								if ( ciclo == 1 || ciclo == 2 ) { datos.mesinicia="01"; }
								else if ( ciclo == 3 ) { datos.mesinicia="06"; }							
							}
						}
					});
				} else {
					if ( ciclo == 1 || ciclo == 2 ) { datos.mesinicia="01"; }
					else if ( ciclo == 3 ) { datos.mesinicia="07"; }
				}
			}			
			
			pi=annoActual+datos.mesinicia;
			pf=annoActual+datos.mesfinal;
			
			mesInicia = (datos.mesinicia.slice(0,1) == "0") ? parseInt(datos.mesinicia.slice(-1)) : parseInt(datos.mesinicia);
			mesFin = (datos.mesfinal.slice(0,1) == "0") ? parseInt(datos.mesfinal.slice(-1)) : parseInt(datos.mesfinal);
			mesPresenta = "02";		//Quemado para validar que se puede presentar hasta mayo (mes datepicker) para hacerle retroactivo de marzo
			if (idcert == 'tc1') { mesPresenta = "08"; }
			mesPresenta = (mesPresenta.slice(0,1) == "0") ? parseInt(mesPresenta.slice(-1)) : parseInt(mesPresenta);
			//if ( ( idcert == 'tc1' || idcert == 'tc2' ) || ( op == 1 && ( idcert == 'tc0' || idcert == 'tc3' || idcert == 'tc4' ) ) ) { mesPresenta++; }
			//else if ( ( idcert == 'tc1' || idcert == 'tc2' ) ) { mesPresenta--; }
		}
	});
	
	if( op == 0 ) {
		var tiempoActual = fechaActual.getTime();			
		var fechaFinal = fechaActual;
		//if ( idcert != 'tc1' ) { fechaFinal.setYear( annoActual + 1 ); }		
		fechaFinal.setMonth( mesFin - 1 );
		var tiempoMS2Meses = 60 * 60 * 24 * 30 * 1000 * 2;
		var tiempoFinal = fechaFinal.setTime(fechaFinal.getTime() + tiempoMS2Meses);
		fechaFinal.setTime( fechaFinal.getTime( )- tiempoMS2Meses );
		
		if ( tiempoActual > tiempoFinal ) {
			alert("Ha superado la fecha limite de presentacion del certificado ("+ pf +")");
			
			if(bandeCertiDepenEcono==true){
				$("#radVigenciaHijastro1,#radVigenciaHijastro2").attr("checked",false);
			}else{
				$("#vigencia1,#vigencia2").attr("checked",false);
			}
			
			return false;
		} 
		
		if( idcert == 'tc0' || idcert == 'tc1' || idcert == 'tc2' || idcert == 'tc3' || idcert == 'tc4' 
			|| idcert == 'tc5') {
			
			if( mesActual <= mesInicia || mesActual == mesInicia + 1) {
				mesInicia = (mesInicia < 10) ? "0" + mesInicia.toString() : mesInicia.toString();
				pi = annoActual + mesInicia;
			} else {
				var fechaHace2Meses = new Date(annoActual,mesActual-3,1);
				mesPeriodoI = fechaHace2Meses.getMonth() + 1;
				mesPeriodoI = (mesPeriodoI <10) ? "0" + mesPeriodoI.toString() : mesPeriodoI.toString();
				pi = annoActual + mesPeriodoI;					
			}
			mesFin = (mesFin == 1) ? 12 : mesFin;
			mesFin = (mesFin < 10) ? "0" + mesFin.toString() : mesFin.toString();
			pf = fechaFinal.getFullYear() + mesFin;								
		} else { pi = ""; pf = ""; }
		
		if ( idcert == 'tc0' || idcert == 'tc1' || idcert == 'tc2' || idcert == 'tc5') {
			var pfV=validarFechaLimiteEdad(pi, pf, 19, 0);			
			if ( pfV == "0" ) { return false; } 
			else if ( parseInt(pfV) > 1 ) { pf = pfV; }
		}
		
		if(bandeCertiDepenEcono==true && $("#cDependenciaEconomica").is(":checked")){
			$("#txtPerioIniciVigenHijastro3").val(pi);
			$("#txtPerioFinalVigenHijastro3").val(pf);
		}else if(banderaError==false){
			$("#pInicialV3").val(pi);
			$("#pFinalV3").val(pf);
		}
	} else {
		var tiempoActual = fechaActual.getTime();			
		var tiempoFinal = fechaActual;		
		tiempoFinal.setMonth( mesPresenta - 1 );
		
		if ( tiempoActual > tiempoFinal.getTime() ) {
			mf = annoActual + "-" + ((mesPresenta < 10) ? "0" + mesPresenta.toString() : mesPresenta.toString());
			alert("Ha superado la fecha limite de presentacion del certificado ("+ mf +")");
			
			if(bandeCertiDepenEcono==true){
				$("#radVigenciaHijastro1,#radVigenciaHijastro2").attr("checked",false);
			}else{
				$("#vigencia1,#vigencia2").attr("checked",false);
			}
			
			return false;
		}
		
		if( idcert == 'tc0' || idcert == 'tc1' || idcert == 'tc2' || idcert == 'tc3' || idcert == 'tc4' 
			|| idcert == 'tc5') {
			
			if ( ( mesActual <= 2 && idcert != 'tc1' ) ) { annoActual--; }			
			var fechaHace2Meses = new Date(annoActual,mesActual-3,1);
			mesPeriodoI = fechaHace2Meses.getMonth() + 1;
			mesPeriodoI = (mesPeriodoI <10) ? "0" + mesPeriodoI.toString() : mesPeriodoI.toString();
			pi = annoActual + mesPeriodoI;
			
			//if ( ( mesActual <= 2 && idcert != 'tc1' ) ) { annoActual++; }			
			mesFin = (mesFin <10)?"0"+mesFin.toString():mesFin.toString();
			pf = annoActual + mesFin;
		} else { pi = ""; pf = ""; }
		
		if ( idcert == 'tc0' || idcert == 'tc1' || idcert == 'tc2' || idcert == 'tc5') {
			var pfV=validarFechaLimiteEdad(pi, pf, 19, 0);			
			if ( pfV == "0" ) { return false; } 
			else if ( parseInt(pfV) > 1 ) { pf = pfV; }
		}
				
		if(bandeCertiDepenEcono==true && $("#cDependenciaEconomica").is(":checked")){
			$("#txtPerioIniciVigenHijastro3").val(pi);
			$("#txtPerioFinalVigenHijastro3").val(pf);
		}else{
			$("#pInicialV3").val(pi);
			$("#pFinalV3").val(pf);
		}
	}
	
	var pfV=comparaFecha($("#fAsignacion").val(),pi,pf);			
	if ( pfV > 0 ) { 
		if(bandeCertiDepenEcono==true && $("#cDependenciaEconomica").is(":checked")){
			$("#txtPerioIniciVigenHijastro3").val(pfV);
		}else if(banderaError==false){
			$("#pInicialV3").val(pfV);
		}
		 
	}
	else if (pfV == -1 ) {
		
		alert("Fecha de Asignacion Mayor que la fecha final del periodo");
		
		if(bandeCertiDepenEcono==true){
			$("#txtPerioIniciVigenHijastro3,#txtPerioFinalVigenHijastro3").val(""); 
			$("#radVigenciaHijastro1,#radVigenciaHijastro2").attr("checked",false);
		}else{
			$("#pInicialV3,#pFinalV3").val(""); $("#vigencia1,#vigencia2").attr("checked",false);
		}
	}
}

function validarExisteCertificado(v0, v1, v2, v3){
	var retorno = true;	
	//v1 = ( $("#cEscolaridad").is(":checked") ? 55 : ( $("#cUniversidad").is(":checked") ? 56 : ( $("#cSupervivencia").is(":checked") ? 57 :( $("#cDependenciaEconomica").is(":checked") ? idCertiDepenEcono : 58) ) ) );	
	
	$.ajax({
		url:URL+"phpComunes/validarExisteCertificado.php",
		cache: false,
		type: "GET",
		dataType:"json",
		data:{ v0:v0, v1:v1, v2:v2, v3:v3 },
		async: false,
		success:function(datosC){
			if( datosC == 0 ) {
				retorno = false;
			}			
		}
	});
	
	return retorno;
}