var URL = src();
var idPersona=0;
var nuevoBen=0;
var fechaCertificado="";
$(document).ready(function(){

  $("#cEscolaridad,#cUniversidad,#cSupervivencia,#cDiscapacidad").attr("disabled","disabled");

    $("#dialog-form2").dialog("destroy");
	$("#dialog-form2").dialog({
		autoOpen: false,
		height: 400,
		width: 500,
		modal: true,
		buttons: {
			'Enviar': function() {
			var bValid = true;
			var campo=$('#notas').val();
			var campo0=$.trim(campo);
			if (campo0==""){
				$(this).dialog('close');
					return false;
				}
			var campo1=$('#usuario').val();
			var campo2="radicacion.php";
			$.post(URL+'phpComunes/colaboracion.php',{v0:campo0,v1:campo1,v2:campo2},
			function(datos){
				if(datos=='1'){
					alert("Su comentario fue enviado correctamente!. Gracias por participar en el mejoramiento de SIGAS");
				}else{
					alert(datos);
				}
		});
		$(this).dialog('close');
		},
		Cancelar: function() {
			$(this).dialog('close');
			$("#dialog-form2").dialog("destroy");
		}
	}
	});
	
	$("#ayuda").dialog({
		 	autoOpen: false,
			height: 450,
			width: 600,
			draggable:true,
			modal:false,
			open: function(evt, ui){
					$('#ayuda').html('');
					$.get('../../help/aportes/ayudaRadicacion.html',function(data){
							$('#ayuda').html(data);
					});
			 }
		});
	

});//ready

function nuevoB(){
	nuevoBen=1;
	limpiarCampos();
	$("#errores").empty();
	
    
}

function limpiarCampos(){
	$("table.tablero input:text").val('');
	$("table.tablero select").val(0);
	$("#estadoGiro").empty();
	$("table[name='dinamicTable']").remove();
	$("#tipoDoc3").focus();
	$("table.tablero input:checkbox,table.tablero input:radio").attr("checked",false);
	$("table.tablero input:checkbox,table.tablero input:radio").attr("disabled",true);
	//$("#cedMama").attr("disabled",false);
	$("#cedMama option:gt(0)").remove();
}

//Buscar datos beneficiarios
function buscarPersonaDos(num){
	
	/*if($("#tipoDoc3").val()==0){
	    alert("Falta tipo documento");
	    return false;
	}*/
	if($("#tipoDoc3").val()!=4){
		var campo1=parseInt(num);
		if(isNaN(campo1))return false;
	}
	var idtd=$("#tipoDoc3").val();
	$.getJSON(URL+'phpComunes/buscarPersona2.php',{v0:idtd,v1:num},function(data){
	    if(data==0){
			alert("No existe el beneficiario");
	       	limpiarCampos();
	        return false;
	}
	
	$.each(data,function(i,fila){
	    idnb=fila.idpersona;
		$('#pNombre3').val($.trim(fila.pnombre));
	    $('#sNombre3').val($.trim(fila.snombre));
	    $('#pApellido3').val($.trim(fila.papellido));
	    $('#sApellido3').val($.trim(fila.sapellido));
	    var fecha=$.trim(fila.fechanacimiento);
	    fecha=fecha.replace("/","");
	    $('#fecNac3').val(fecha.replace("/",""));
	    $('#fecNac3').click().blur();
	    $('#sexo3').val(fila.sexo);
		//nuevo ag 2011
		$('#estadoCivil3').val(fila.idestadocivil);
		$('#capTrabajo').val(fila.capacidadtrabajo);
		$("#cboDeptoB").val(fila.iddepnace).trigger('change');
		setTimeout((function(){
		$("#cboCiudadB").val(fila.idciuresidencia);
		$("#cboCiudadB").val(fila.idciunace);
	    $("#cboCiudadB").trigger("change");
 	    }),1000);
		
	}); //each

	// TABLA DINAMICA PARA DATOS EMPRESA TRABAJDOR ACTIVO
	$("table[name='dinamicTable']").remove();
	var tabla="<table class='tablero' width='100%' border='0' cellspacing='0' cellpadding='0' name='dinamicTable' style='margin-first:8px;'>";
	$.ajax({
		url: URL+'phpComunes/buscarAfiliacion.php',
		type: "POST",
		data: "submit &v0="+idnb,
		async: false,
		dataType: "json",
		success: function(datos){
			if(datos != 0){
				alert("La persona que trata de Afiliar es un trabajador activo!");
				$.each(datos,function(i,fila){
					tabla+="<tr><th>Salario</th><th>Nit</th><th>Raz\u00F3n social</th><th>Tipo</th><th>Detalle</th></tr>"+"<tr><td>$"+ fila.salario +"</td><td>"+ fila.nit +"</td><td>"+fila.razonsocial+"</td><td>"+fila.tipoformulario+"</td><td>"+fila.detalledefinicion+"</td></tr>";

				});//end each
				//Agregar tabla despues de la tabla  beneficiarios
				$("#tablaModBen.tablero").after(tabla+="</table>");
				$("table[name='dinamicTable']").hide().fadeIn(800);
				$('#tablaModBen.tablero input:text').val("");
				$('#tablaModBen.tablero select').val("0");
				return false;
			}
			$.ajax({
				url: URL+'phpComunes/buscarRelaciones.php',
				type: "POST",
				data: "submit &v0="+idnb,
				async: false,
				dataType: "json",
				success: function(datos){
					if(datos != 0){
						//alert("La persona que trata de modificar tiene las siguientes relaciones!");
						$.each(datos,function(i,fila){
							//Mostrar datos de afiliacion
							var nom = $.trim(fila.pnomt)+" "+$.trim(fila.snomt)+" "+$.trim(fila.papet)+" "+$.trim(fila.sapet);
												
							tabla+="<tr><th>Nombres Afiliado</th><th>Parentesco</th><th>Conv</th><th>F Afilia</th><th>Giro</th><th>Estado</th></tr><tr><td>"+nom+"</td><td>"+fila.detalledefinicion+ "</td><td>"+fila.conviven+"</td><td>"+fila.fechaafiliacion+"</td><td>"+fila.giro+"</td><td>"+fila.estado+"</td></tr>";
						});//end each
//						Agregar tabla despues de la tabla  beneficiarios
						$("#tablaModBen.tablero").after(tabla+="</table>");
						$("table[name='dinamicTable']").hide().fadeIn(800);
						//$('#tablaModBen .tablero input:text').val("");
						//$('#tablaModBen .tablero select').val("0");
								
					
					}
					//Muestro datos de afiliacion dle primer trabajdor (padre)
					idPadre =parseInt(datos[0].idtrabajador);
					idBen	=datos[0].idbeneficiario;
					$("#parentesco").val(datos[0].idparentesco);
					if(datos[0].giro=='S'){
					$("#tipoAfiliacion3").val("48");
					}else{
					$("#tipoAfiliacion3").val("49");	
					}
					$("#estadoGiro").html(datos[0].giro);
					//----------------------------------------
				} ,// fin success 2
				complete:function(){buscarConyugues(idPadre,idBen)}
				}); //ajax relaciones
		} // end success primero
	}); //ajax afiliacion
	}); //getJSON
	}//end funcion			
	
//buscar conyugues y certificados del beneficiario
function buscarConyugues(idT,idB){
	$("#cedMama option:gt(0)").remove();
	conyugueActual="";
	$.ajax({
		url: 'buscarPadres.php',
		type:"POST",
		data: {v0:idT},
		async:false,
		dataType:"json",
		success: function(dato){
			$.each(dato,function(i,fila){
				var idc=$.trim(fila.idc);
				var id =$.trim(fila.identificacion);
				var pn =$.trim(fila.pnombre);
				var pa =$.trim(fila.papellido);
				var nombrec = pn+" "+pa;
				if(fila.conviven=='S'){
				conyugueActual=id;
				}
				$("#cedMama").append("<option name="+idc+" value="+id+">"+id+"-"+nombrec+"</option>");
				});//each
				
				//Seleccionar  el conyugue con convivencia S
				$("#cedMama").val(conyugueActual);
		},//succes
		complete:function(){
		
		//Buscar Certificados
		
		
		}
		});//ajax*/
}

function actualizarBeneficiario(){
	var error=0;
	
	if(nuevoBen==0){
	MENSAJE("Click en Nuevo !");
	return false;
	}
	$(".tablero input.ui-state-error,.tablero select.ui-state-error").removeClass("ui-state-error");
	if($("#identificacion3").val()==''){
		$("#identificacion3").addClass("ui-state-error");
		error++;
	}
	
	if($("#parentesco").val()=='0'){
		$("#parentesco").addClass("ui-state-error");
		error++;
	}
	
	if($("#cedMama").val()=='Seleccione..'){
		$("#cedMama").addClass("ui-state-error");
		error++;
	}
	
	if($("#pNombre3").val()==''){
		$("#pNombre3").addClass("ui-state-error");
		error++;
	}
	
	if($("#pNombre3").val()==''){
		$("#pNombre3").addClass("ui-state-error");
		error++;
	}
	
	if($("#pApellido3").val()==''){
		$("#pApellido3").addClass("ui-state-error");
		error++;
	}
	
	if($("#sexo3").val()=='0'){
		$("#sexo3").addClass("ui-state-error");
		error++;
	}
	
	if($("#fecNac3").val()==''||$("#fecNac3").val()=='mmddaaaa'){
		$("#fecNac3").addClass("ui-state-error");
		error++;
	}
	
	if($("#tipoAfiliacion3").val()=='0'){
		$("#tipoAfiliacion3").addClass("ui-state-error");
		error++;
	}
	
	if($("#estadoCivil3").val()=='0'){
		$("#estadoCivil3").addClass("ui-state-error");
		error++;
	}
	
	if($("#cboDeptoB").val()=='0'){
		$("#cboDeptoB").addClass("ui-state-error");
		error++;
	}
	
	if($("#cboCiudadB").val()=='0'){
		$("#cboCiudadB").addClass("ui-state-error");
		error++;
	}
	
	
	if($("#cedMama").val()=='undefined'){
		cedMama=0;
	}
	//VALORES DE CERTIFICADO
	$("#cEscolaridad,#cUniversidad,#cSupervivencia").attr("value","N");
	
	if(error>0){
		return false;
	}

	//Actualizar beneficiarios
	var idben=0;
	var idcony=0;
	//...BENEFICIARIOS
	var tipoDoc3=$("#tipoDoc3").val();
	var cedula3=$("#identificacion3").val();
	var parentesco=$("#parentesco").val();	//children("option:selected").html();//valor del texto del option PARENTESCO
	var cedMama=$("#cedMama").val();//no obligado
	
	var tipoDoc3=$("#tipoDoc3").val();
	var cedula3=$("#identificacion3").val();
	var pNombre3=$("#pNombre3").val();
	var pApellido3=$("#pApellido3").val();
	var sNombre3=$("#sNombre3").val();//no obligado
	var sApellido3=$("#sApellido3").val();//no obligado
	var sexo3=$("#sexo3").val();
	var fecNac3=$("#fecNacHide3").val();
	
	var tipoAfiliacion3=$('#tipoAfiliacion3').val();
	var capacidad=$('#capacidad').val();
	var cboDeptoB=$('#cboDeptoB').val();
	var cboCiudadB=$("#cboCiudadB").val();
	var estadoCivil3=$("#estadoCivil3").val();
	var fEscolaridad=$("#fEscolaridad").val();
	var fUniversidad=$("#fUniversidad").val();
	var fDiscapacidad=$("#fDiscapacidad").val();
	var fSupervivencia=$("#fSupervivencia").val();	
	
	//Guardar Persona
			$.ajax({
				url: 'insertPersona.php',
				type: "POST",
				data: {
						v1:tipoDoc3,
						v2:cedula3,
						v3:pApellido3,
						v4:sApellido3,
						v5:pNombre3,
						v6:sNombre3,
						v7:sexo3,
						v17:estadoCivil3,
						v18:fecNac3,
						v19:cboDeptoB,
						v20:cboCiudadB,
						},
				async: false,
				//dataType: "json",
				success: function(data){
	 			if(data==0){
					alert("No se pudo guardar la persona!");
					return false
					}
				if(data<0){
				alert("Error al insertar la persona, codigo: "+data);
				return false;
				}	
				
			   alert("La persona fue grabada!");
				idben=parseInt(data);//idbeneficiario	
				}
			});//ajax
			
			//crear relacion
			var giro='';
			if($("#tipoAfiliacion3").val()=='48'){
			giro='S';	
			}else{
			giro='N';
			}
			var campo0 = $("#idT").val();
			var campo1 = idben;
			var campo2 = parentesco;
			var campo3 = ($("#cedMama").is(":disabled")?idcony: $("#cedMama option:selected").attr("name"));
			var campo4 = giro;
	
			$.ajax({
			url: URL+'phpComunes/guardarBeneficiario.php',
			type: "POST",
			async: false,
			data: "submit &v0="+campo0+"&v1="+campo1+"&v2="+campo2+"&v3="+campo3+"&v4="+campo4,
			success: function(datos){
				if(datos==0){
					alert("NO se pudo guardar la relacion!");
					return false;
					}
			
				var cert=0;
				
				if($("#cDiscapacidad").is(":checked")){
					cert=2;
					var v1 = idben;			//idbeneficiario
					var v2 = parentesco;	//idparentesco
				}
				
				
				if($("#cEscolaridad,#cSupervivencia").is(":checked")){
					cert=1;
					f=$("#fEscolaridad").val();
					mes=f.slice(0,2);
					ano=f.slice(-4);
					periodo=ano+mes;
					var v0 = idben;			//idbeneficiario
					var v1 = parentesco;	//idparentesco
					var v2 = ($("#cEscolaridad").is(":checked")?'55' : '57');				//idtipocertificado
					var v3=periodo;		//periodo inicio
					var v4=ano+'12';	//periodo final
					var v5='P';			//forma presentacion
				}
				if($("#cUniversidad").is(":checked")){
					cert=1;
					f=$("#fUniversidad").val();
					mes=f.slice(0,2);
					ano=f.slice(-4);
					m=parseInt(mes);
					if(m<=6) {
						mf='06'
						}
					else{
						mf='12';
						}
					periodo=ano+mes;
					var v0 = idben;			//idbeneficiario
					var v1 = parentesco;	//idparentesco
					var v2 = '56';			//idtipocertificado
					var v3=periodo;			//periodo inicio
					var v4=ano+mf;			//periodo final
					var v5='P';				//forma presentacion
				}
				if(cert==1){
					$.ajax({
					url:URL+"aportes/radicacion/grabarCertificado.php",
					type:"POST",
					data:{v0:v0,v1:v1,v2:v2,v3:v3,v4:v4,v5:v5},
					async:false,
					success:function(data){
					alert(data);
					},
				});
				}//if cert
				//Discapacidad
				if(cert==2){
					$.ajax({
					url:URL+"aportes/trabajadores/marcarDiscapacitado.php",
					type:"POST",
					data:{v0:idPersona,v1:v1,v2:v2},
					async:false,
					success:function(datos){
					if(datos==1){
						alert("Beneficiario marcado como Discapacitado y grabado el certificado!");
					}
					else{
						alert("No se pudo guardar el certificado de discapacidad");
					}
				
					},
				});
				}//if cert
				
				alert("Se grabo la relacion del beneficiario.");
				cerrarRadicacion();
			    nuevoBen=0;
				}
			});
	
	
	}	

function cerrarRadicacion(){
    var campo0=$("#pendientes").val();
	$.ajax({
		url: URL+'phpComunes/cerrarRadicacion.php',
		type: "POST",
		data: "submit=&v0="+campo0,
		success: function(datos){
			msg="Exito, "+datos;
			alert(msg);
		}
	});
	  }

function validarComboParentesco(){
	if($("#parentesco").val()!="35"){
	$("#cedMama").attr("disabled",true);
	$("#cedMama").val(0);
	//$("#cedMama").parent("td").prev().html('');
	}else{
	$("#cedMama").attr("disabled",false);
	//$("#cedMama").parent("td").prev().html('C&eacute;dula Padre &oacute; Madre');
	}
	validarCertificados();
}	

//Change certificados beneficiarios del div #beneficiarios en grupoTab.php
function validarCertificados(){
	 
	 //Fecha fija de entrega de certificados
	 var f=new Date();
	 var dia=f.getDate();
	 var mesTemp=f.getMonth()+1;
	 var mes=(mesTemp>=9?"0"+mesTemp :mesTemp);
	 var ano=f.getFullYear();
	 fechaCertificado=mes+"/"+dia+"/"+ano; 
	 //.--------------------------------------
	 
    //--------RESETEAR CERTIFICADOS
	$("#fEscolaridad,#fUniversidad,#fSupervivencia,#fDiscapacidad").val('');
	$("#cEscolaridad,#cUniversidad,#cSupervivencia,#cDiscapacidad,#fEscolaridad,#fUniversidad,#fSupervivencia,#fDiscapacidad").attr("disabled","disabled");
	$("#cEscolaridad,#cUniversidad,#cSupervivencia,#cDiscapacidad").attr("checked",false);
	var p=$("#parentesco").val();
	//$("#filaEscolaridad,#filaUniversidad,#filaSupervivencia,#filaDiscapacidad").hide();
	/*if($("#fecNac3").val()==''||$("#fecNac3").val()=='mmddaaaa'){
		alert("Ingrese una fecha de nacimiento para el BENEFICIARIO.");
		$("#fecNac3").focus();
		return false;
		}*/
	 //--------VALIDAR LA EDAD DEL BENEFICIARIO	
	 var f=new Date();
	 var fechaOculta=0;
	 fechaOculta=$("#fecNacHide3").val();
	 var ano=f.getFullYear();
	 var anoFechaOculta=fechaOculta.slice(-4);
	 var edadB=parseInt(ano)-parseInt(anoFechaOculta);
	 //--------PREGUNTAR PRIMERO SI ES SUBSIDIO
	 if($("#tipoAfiliacion3").val()=='48'){
		 
		 if($("#capTrabajo").val()=='D'){
		 //desactivar check CERTIFICADO
		   $("#cDiscapacidad").attr("disabled",false);
		   $("#fDiscapacidad").val(fechaCertificado);
		 }else{
		 
		//cod 35=HIJO    
		if(p=="35"){
			if(edadB>=12&&edadB<=40){
			//desactivar check CERTIFICADO
		   $("#cEscolaridad,#cUniversidad").attr("disabled",false);
		   
		 }
		}
		//cod 36=PADRE/MADRE    
		if(p=="36"){
			if(edadB>60){
				//$("#filaSupervivencia").show();//Mostrar certificado de supervivencia
				 $("#cSupervivencia").attr("disabled",false);
				 $("#fSupervivencia").val(fechaCertificado);
			}
		}
		//cod 37=HERMANO	
		if(p=="37"){
			if(edadB>12&&edadB<=40){
			   //$("#filaEscolaridad,#filaUniversidad").show();//Mostrar certificado de estudios
				 $("#cEscolaridad,#cUniversidad").attr("disabled",false);
			}
			/*if($("#capacidad").val()=='D'){
				$("#filaDiscapacidad").show();//Certificado Discapacidad
			}*/
		}
	 }//if discapacidad
	}//if 
//Validar el foco de combos de cedul de la madre
	/*	if($("#cedMama").is(":visible")&&$("#parentesco").is(":visible")&&$("#cedMama").val()=='0'){
			 if($("#cedMama").val()!='0'){
			return true;
			 }else{
			$("#cedMama").focus(); 
			 }
			 }else{$("#tipoAfiliacion3").focus();}*/
}

//Funcion para mostrar fecha de certificado con lso checks
function mostrarFecha(op){
	//1= escolaridad, 2=universidad,3=superviviencia
	$("#beneficiarios input:text[name='fCert']").val('');
if(op==1){
	chequeado2=1;
	chequeado1++;
	if( chequeado1 % 2 != 0 ){
	$("#cEscolaridad").removeAttr("checked");
		$("#fEscolaridad").val("");
		return false;
	}
	else{
 		$("#fEscolaridad").val(fechaCertificado);
		$("#fUniversidad").val("");
	}
}else{
	if(op==3){
		$("#fSupervivencia").val(fechaCertificado);
	}else
	{
		if(op==4){
			$("#fDiscapacidad").val(fechaCertificado);
		}
		else{
			chequeado1=1;
			chequeado2++;
			if( chequeado2 % 2 != 0 ){
				$("#cUniversidad").removeAttr("checked");
				$("#fUniversidad").val("");
				return false;
			}
			else{
	 	  		$("#fUniversidad").val(fechaCertificado);
				$("#fEscolaridad").val("");
			}
		}
	}
}
}
