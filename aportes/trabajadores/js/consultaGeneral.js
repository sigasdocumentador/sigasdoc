/*
* @autor:      Ing. Orlando Puentes
* @fecha:      04/09/2010
* objetivo:
*/
var arrIde=new Array();
var idEmpresa=0;
var idPersona=0;
var URL=src();
var tercero = false;

$(document).ready(function(){

	$("#buscarPor").change(function(){
		$("#pn,#pa,#idT").val('');
		if($(this).val()=='2'){
			$("#tipoDocumento").hide();
			$("#pn,#pa").show();
			$("#idT").hide();
		}else{
			$("#tipoDocumento").show();
			$("#pn,#pa").hide();
			$("#idT").show();
			$("#idT").focus();
		}
	});
	
	var URL=src();
	
	//Dialog Colaboracion en linea
	$("#dialog-form2").dialog({
			autoOpen: false,
			height: 400,
			width: 500,
			modal: false,
			buttons: {
				'Enviar': function() {
				var bValid = true;
				var campo=$('#notas').val();
				var campo0=$.trim(campo);
				if (campo0==""){
					$(this).dialog('close');
						return false;
					}
				var campo1=$('#usuario').val();
				var campo2="consultaTrabajador.php";
				$.post(URL+'phpComunes/colaboracion.php',{v0:campo0,v1:campo1,v2:campo2},
				function(datos){
					if(datos=='1'){
						alert("Su comentario fue enviado correctamente!. Gracias por participar en el mejoramiento de SIGAS");
					}else{
						alert(datos);
					}
			});
			$(this).dialog('close');
			},
			Cancelar: function() {
				$(this).dialog('close');
				$("#dialog-form2").dialog("destroy");
			}
		}
		});
	
	//Buscar Trabajador
	$("#buscarT").click(function(){
		$("#trabajadores").find("p").remove();
		//espera
		dialogLoading("show");
		$("#tabsT").hide();
		if($("#idT").is(":visible")&&$("#idT").val()==''){
			$(this).next("span.Rojo").html("Ingrese un valor a buscar.").hide().fadeIn("slow");
			dialogLoading("hide");
			$("#idT").focus();
			return false;
		}	
		if($("#pn").is(":visible")&&$("#pn").val()==''&&$("#pa").val()==''){
			$(this).next("span.Rojo").html("Ingrese un valor a buscar.").hide().fadeIn("slow");
			dialogLoading("hide");
			$("#pn").focus();
			return false;
		}	
				
		var v0=$("#idT").val();
		var v1=$("#buscarPor").val();
		var v2=$("#tipoDocumento").val();
		var v3=$("#pn").val();//campo nombre
		var v4=$("#pa").val();//campo nombre
		var contadorAfi=0;
		var contadorTemp=0;
		var cadenaAfiliados='';
		var idp=0;
		var nom="";
		$.ajax({
			url: URL+'phpComunes/buscarPersonaPara.php',
			type: "POST",
			data: {v0:v0,v1:v1,v2:v2,v3:v3.toUpperCase(),v4:v4.toUpperCase()},
			async: false,
			dataType: 'json',
			success: function(data){
				if(data==0){
					$("#buscarT").next("span.Rojo").html("No se encontr&oacute; la persona").hide().fadeIn("slow");
					dialogLoading("hide");	
					return false;
				}
				
				$("#lstT").remove();
					
				$.each(data,function(i,fila){
					$("#buscarT").next("span.Rojo").html(""); 
					$("#idT").val('');
					idp=fila.idpersona;
					nom =fila.pnombre+" "+fila.snombre+" "+fila.papellido+" "+fila.sapellido;
					$.ajax({
						url:URL+'phpComunes/buscarAfiliacionIDP.php',
						type: "POST",
						data: {v0:idp},
						async: false,
						dataType: 'json',
						success: function(datos){
							if(datos==0){
								tercero = true;
								arrIde[0] = 0;
								
								/*$("#buscarT").next("span.Rojo").html("No se encontraron AFILIACIONES.").hide().fadeIn("slow");
								dialogLoading("hide");	
								return false;*/
							}else{
								tercero = false;
								$.each(datos,function(i,fila){
									if(fila.idempresa!=null){
											arrIde[i]=fila.idempresa;
									}
									return;
								});
							}
						
						//MOSTRAR RESULTADOS de 10 en 10
						//contadorTemp++;
						contadorAfi++;
						cadenaAfiliados+='<p align="left" onclick="enviarNit('+idp+','+arrIde+')">'+idp+" "+nom+'</p>';
						
						/*if(contadorTemp>=0){
							$("#trabajadores").append(cadenaAfiliados);
							contadorTemp=0;
							cadenaAfiliados='';
						}else{
						$("#trabajadores").append('<p align="left" onclick="enviarNit('+idp+','+arrIde+')">'+idp+" "+nom+'</p>');
						}*/
					  }//success
					});
				});//fin each	for
				    $("#trabajadores").append(cadenaAfiliados);
					dialogLoading("hide");	
					$("#trabajadores").before("<p id='lstT'>Trabajadores encontrados:<b> "+contadorAfi+"</b></p>");
							
			}//success
		});
	});//fin click
		
	//... ICONO PARA OCULTAR/MOSTRAR FORMULARIO ...
	$("div#icon span").click(function(){
		$(this).toggleClass("toggleIcon");
		$("div#wrapTable").slideToggle();
		
	});//fin click icon	
});//fin ready



//... FUNCION PARA ENVIAR NIT PARA ACORDEON ...//
function enviarNit(idp,ide){
	$("#lstT").remove();	
	var	indexTab;
	var idEmpresa=0;
	var idPersona=0;
	idEmpresa=ide;
	idPersona=idp;
	
	$("div#icon").show();
	$("#trabajadores").find("p").remove();
	//$("#tabs div").empty();
	$("div#wrapTable").slideUp("normal");
	$("#tabsT").tabs("destroy");//Destruyo tab para nueva consulta
	$("#tabsT").show("slow",function(){
		$("#tabsT ul li:first a").trigger("click");
	});	//Muestro el tab
	
	//Aqui Capturo el valor del ALT para el switch que carque los formularios	
	$("#tabsT ul li a,#tabs2 ul li a").unbind("click"); 
	$("#tabsT ul li a").bind("click",function(){
		indexTab=$(this).attr("alt");
		
		if(indexTab=='e'){
			//$(this).unbind('click');
		}
		//CARGAR TABLA DEACUERDO A TAB SELECCIONADO
		switch(indexTab){
			case "a": $("#tabs-1").load("tabConsulta/fichaTab.php",{v0:idPersona,flag:1, esTercero: tercero}); break;
			//case "b": $("#tabs-2").load("tabConsulta/personaTab.php",{v0:idPersona,botonActualizar:1}); break; 
			//case "c": $("#tabs-3").load("tabConsulta/reclamosTab.php",{v0:idPersona}); break; 
			case "d": $("#tabs-4").load("tabConsulta/embargosTab.php",{v0:idPersona}); break; 
			case "e": $("#tabs-5").load("tabConsulta/aportesTab.php",{v0:idEmpresa}); break; 
			case "f": $("#tabs-6").load("tabConsulta/planillaTab.php",{v0:idPersona}); break;
			//case "g": $("#tabs-7").load("tabConsulta/girosTab.php",{v0:idPersona}); break;
			//case "h": $("#tabs-8").load("tabConsulta/tarjetaTab.php",{v0:idPersona}); break;
			//case "i": $("#tabs-9").load("tabConsulta/movimientosTab.php",{v0:idPersona}); break;
			//case "j": $("#tabs-10").load("tabConsulta/causalesTab.php",{v0:idPersona}); break;
			case "k": $("#tabs-11").load("tabConsulta/documentosTab.php",{v0:idPersona}); break;
			case "l": $("#tabs-12").load(URL+"aportes/trabajadores/contenedorConsultaGeneral.php",{v0:idPersona}); break;
		}	//end switch				 				 
	});
	   
	$("#tabsT").tabs({spinner:'<em>Loading&#8230;</em>',selected:-1});//end tab		


}//END FUNCTION BUSCAR NIT

var nuevo=0;
var modificar=0;
var continuar=true;
var msg="";

$(function() {
		$("#ayuda").dialog({
		 	autoOpen: false,
			height: 450, 
			width: 700, 
			draggable:true,
			modal:false,
			open: function(evt, ui){
					$('#ayuda').html('');
					$.get(URL+'help/aportes/ayudaConsultaTrabajador.html',function(data){
							$('#ayuda').html(data);
					});
			 }
		});
	});