<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xml:lang="en-us" xmlns="http://www.w3.org/1999/xhtml">
<body>
<div id="banner">	
	<h1>table<em>sorter</em></h1>
	<h2>Pager plugin</h2>
	<h3>Flexible client-side table sorting</h3>
	<a href="http://tablesorter.com/docs/index.html">Back to documentation</a>
</div>
<div id="main">

<h1>Javascript</h1>
<pre class="javascript">$(<span class="global">document</span>).ready(<span class="keywords">function</span>()&nbsp;{&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;$(<span class="string">"table"</span>)&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;.tablesorter({widthFixed:&nbsp;<span class="keywords">true</span>,&nbsp;widgets:&nbsp;[<span class="string">'zebra'</span>]})&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;.tablesorterPager({container:&nbsp;$(<span class="string">"#pager"</span>)});&nbsp;<br>});&nbsp;<br></pre>
<h1>Demo</h1>
<table cellspacing="1" class="tablesorter"><colgroup><col style="width: 221px;"><col style="width: 221px;"><col style="width: 123px;"><col style="width: 158px;"><col style="width: 203px;"><col style="width: 183px;"><col style="width: 211px;"></colgroup><colgroup><col style="width: 217px;"><col style="width: 217px;"><col style="width: 124px;"><col style="width: 157px;"><col style="width: 200px;"><col style="width: 181px;"><col style="width: 207px;"></colgroup>
	<thead>
		<tr>
			<th class="header">Name</th>
			<th class="header headerSortDown">Major</th>
			<th class="header">Sex</th>
			<th class="header">English</th>
			<th class="header">Japanese</th>
			<th class="header">Calculus</th>
			<th class="header">Geometry</th>

		</tr>
	</thead>
	<tfoot>
		<tr>
			<th>Name</th>
			<th>Major</th>
			<th>Sex</th>
			<th>English</th>
			<th>Japanese</th>
			<th>Calculus</th>
			<th>Geometry</th>

		</tr>
	</tfoot>
	<tbody><tr class="odd">
			<td>Student01</td>
			<td>Languages</td>
			<td>male</td>

			<td>80</td>
			<td>70</td>
			<td>75</td>
			<td>80</td>
		</tr><tr class="even">
			<td>Student03</td>
			<td>Languages</td>
			<td>female</td>
			<td>85</td>
			<td>95</td>

			<td>80</td>
			<td>85</td>
		</tr><tr class="odd">
			<td>Student04</td>
			<td>Languages</td>
			<td>male</td>

			<td>60</td>
			<td>55</td>
			<td>100</td>
			<td>100</td>
		</tr><tr class="even">
			<td>Student05</td>

			<td>Languages</td>
			<td>female</td>
			<td>68</td>
			<td>80</td>
			<td>95</td>
			<td>80</td>

		</tr><tr class="odd">
			<td>Student08</td>

			<td>Languages</td>
			<td>male</td>
			<td>100</td>
			<td>90</td>
			<td>90</td>
			<td>85</td>

		</tr><tr class="even">
			<td>Student10</td>
			<td>Languages</td>
			<td>male</td>

			<td>85</td>
			<td>100</td>
			<td>100</td>
			<td>90</td>
		</tr><tr class="odd">
			<td>Student02</td>

			<td>Mathematics</td>
			<td>male</td>
			<td>90</td>
			<td>88</td>
			<td>100</td>
			<td>90</td>

		</tr><tr class="even">
			<td>Student06</td>
			<td>Mathematics</td>
			<td>male</td>
			<td>100</td>
			<td>99</td>

			<td>100</td>
			<td>90</td>
		</tr><tr class="odd">
			<td>Student07</td>
			<td>Mathematics</td>
			<td>male</td>

			<td>85</td>
			<td>68</td>
			<td>90</td>
			<td>90</td>
		</tr><tr class="even">
			<td>Student09</td>
			<td>Mathematics</td>
			<td>male</td>
			<td>80</td>
			<td>50</td>

			<td>65</td>
			<td>75</td>
		</tr></tbody>
</table>
<div class="pager" id="pager" style="first: 637px; position: absolute;">
	<form>
		<img class="first" src="example-pager_files/first.png">
		<img class="prev" src="example-pager_files/prev.png">
		<input type="text" class="pagedisplay" value="1/103">
		<img class="next" src="example-pager_files/next.png">
		<img class="last" src="example-pager_files/last.png">
		<select class="pagesize">
			<option value="10" selected="selected">10</option>
			<option value="20">20</option>
			<option value="30">30</option>
			<option value="40">40</option>
		</select>
	</form>
</div>

</div>
<script type="text/javascript" src="example-pager_files/urchin.js"></script>
<script type="text/javascript">
_uacct = "UA-2189649-2";
urchinTracker();
</script>
</body>

