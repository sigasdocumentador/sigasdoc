<?php
/* autor:       orlando puentes
 * fecha:       23/06/2010
 * objetivo:    Procesar los movimientos de los Bonos almacenados en la base de datos. 
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;


include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'conexion.class.php';

class Trabajador{
var $con;

function Trabajador(){
 		$this->con=new DBManager;
		$this->con2=new DBManager;
 	}

function buscar_trabajadores_sucursal($ide){
	if($this->con->conectar()==true){
	$sql="select aportes016.fechaingreso, aportes016.salario,aportes016.estado AS estado_afiliacion, aportes015.papellido, aportes015.sapellido, aportes015.pnombre, aportes015.snombre, aportes015.identificacion, aportes091.detalledefinicion from aportes016 inner join aportes015 on aportes016.idpersona=aportes015.idpersona inner join aportes091 on aportes015.idtipodocumento=aportes091.iddetalledef where /*aportes016.estado='A' and*/ aportes016.idempresa=$ide";
	//echo $sql;
	return mssql_query($sql,$this->con->conect);
		}
	}
		
function insertar_nueva($campos){
		if($this->con->conectar()==true){
			$fechaSistema=date("m/d/Y");
			$sql="INSERT INTO aportes048 (nit, digito, nitrsn, codigosucursal, principal, razonsocial, sigla, direccion, iddepartamento, idciudad, idzona, telefono, fax, url, email, idrepresentante, idjefepersonal, contratista, colegio, exento, idcodigoactividad, indicador, idasesor, idsector, seccional, estado, fechaaportes, fechaafiliacion, trabajadores, aportantes, conyuges, hijos, hermanos, padres, usuario, fechasistema, idtipodocumento, idclasesociedad) VALUES ('".$campos[4]."','".$campos[5]."','".$campos[4]."','000','S','".$campos[7]."','".$campos[8]."','".$campos[9]."','".$campos[10]."','".$campos[11]."','".$campos[11]."','".$campos[12]."','".$campos[13]."','".$campos[14]."','".$campos[15]."','". $campos[16] ."','". $campos[21] ."','". $campos[26] ."','". $campos[27] ."','". $campos[28] ."','". $campos[29] ."','". $campos[30] ."','". $campos[31] ."','". $campos[2] ."','". $campos[32] ."','". $campos[33] ."','". $campos[35] ."','". $campos[34] ."','0','0','0','0','0','0','". $campos[36] ."',cast(getdate) as date),". $campos[6] .",". $campos[3] .")";
			//echo $sql;
			return mssql_query($sql,$this->con->conect);
		}
	}
	
function insertar_agencia($campos){
		if($this->con->conectar()==true){
			$sql="INSERT INTO aportes048 (nit, digito, nitrsn, codigosucursal, principal, razonsocial, sigla, direccion, iddepartamento, idciudad, idzona, telefono, fax, url, email, idrepresentante, idjefepersonal, contratista, colegio, exento, idcodigoactividad, indicador, idasesor, idsector, seccional, estado, fechaaportes, fechaafiliacion, trabajadores, aportantes, conyuges, hijos, hermanos, padres, usuario, fechasistema, idtipodocumento, idclasesociedad) VALUES ('".$campos[4]."','".$campos[5]."','".$campos[4]."','".$campos[38]."','N','".$campos[7]."','".$campos[8]."','".$campos[9]."','".$campos[10]."','".$campos[11]."','".$campos[11]."','".$campos[12]."','".$campos[13]."','".$campos[14]."','".$campos[15]."','". $campos[16] ."','". $campos[21] ."','". $campos[26] ."','". $campos[27] ."','". $campos[28] ."','". $campos[29] ."','". $campos[30] ."','". $campos[31] ."','". $campos[2] ."','". $campos[32] ."','". $campos[33] ."','". $campos[35] ."','". $campos[34] ."','0','0','0','0','0','0','". $campos[36] ."',cast(getdate) as date),". $campos[6] .",". $campos[3] .")";
			//echo $sql;
			return mssql_query($sql,$this->con->conect);
		}
	}
		
function buscar_principal($nit){
	if($this->con->conectar()==true){
			echo $sql="select * from aportes048 where nit='$nit' and principal='S'";
			return mssql_query($sql,$this->con->conect);
		}
 }
 
function contar_sucursales($nit){
	if($this->con->conectar()==true){
			$sql="select count(*) as cuenta from aportes048 where nit='$nit'";
			return mssql_query($sql,$this->con->conect);
		}
	} 
 
function buscar_grupo_empresa($nit){
	if($this->con->conectar()==true){
			$sql="select idempresa,nit,digito,principal,razonsocial,codigosucursal from aportes048 where nit='$nit'";
			return mssql_query($sql,$this->con->conect);
		}
	} 
	 
function actualizar_nit($nit,$nuevo){
	if($this->con->conectar()==true){
			$sql="update aportes048 set nit='$nuevo' where nit='$nit'";
			return mssql_query($sql,$this->con->conect);
		}
	} 
		 
function buscar_empresa($ide){
	if($this->con->conectar()==true){
			$sql="
SELECT aportes048.*, aportes015.papellido, aportes015.sapellido, aportes015.pnombre, aportes015.snombre FROM aportes048 LEFT JOIN aportes015 ON aportes048.idrepresentante = aportes015.identificacion WHERE idempresa=$ide";
			return mssql_query($sql,$this->con->conect);
		}
 }		 

function buscar_todas($parametro, $tipo=1){
	if($this->con->conectar()==true){
		if($tipo==1)
			$sql="SELECT idempresa,nit,razonsocial from aportes048 WHERE nit like '$parametro'";
		if($tipo==2)
			$sql="SELECT idempresa,nit,razonsocial from aportes048 WHERE razonsocial like '%$parametro%'";	
		return mssql_query($sql,$this->con->conect);
	}
 }		 	

function actualizar_empresa($campo){
		if($this->con->conectar()==true){
			$sql="update aportes048 set idsector=".$campo[2].", idclasesociedad=".$campo[3].", razonsocial='".$campo[7]."', sigla='".$campo[8]."', direccion='".$campo[9]."', iddepartamento='".$campo[10]."', idciudad='".$campo[11]."', telefono='".$campo[12]."', fax='".$campo[13]."', url='".$campo[14]."', email='".$campo[15]."', idrepresentante='".$campo[16]."', idjefepersonal='".$campo[21]."', contratista='".$campo[26]."', colegio='".$campo[27]."', exento='".$campo[28]."', idcodigoactividad='".$campo[29]."', indicador='".$campo[30]."', idasesor='".$campo[31]."', seccional='".$campo[32]."', estado='".$campo[33]."', usuario='".$campo[36]."', fechasistema=cast(getdate) as date), idtipodocumento=".$campo[6].", idclasesociedad=".$campo[3]." where idempresa=".$campo[38];
			//echo $sql;
			return mssql_query($sql,$this->con->conect);
		}
	}
function actualizar_persona($sql,$idp){
	if($this->con->conectar()==true){

	return mssql_query($sql,$this->con->conect);
	}
}		

function contar_afiliaciones_p($idp){
	if($this->con->conectar()==true){
	$sql="SELECT count(*) as cuenta FROM aportes016 WHERE idpersona=$idp AND primaria='S'";
	return mssql_query($sql,$this->con->conect);
	}
}		

function contar_afi_pri_nit($idp,$ide){
	if($this->con->conectar()==true){
	$sql="SELECT count(*) as cuenta FROM aportes016 WHERE idpersona=$idp AND idradicacion=$ide and primaria='S'";
	return mssql_query($sql,$this->con->conect);
	}
}	
 
 }	
?>