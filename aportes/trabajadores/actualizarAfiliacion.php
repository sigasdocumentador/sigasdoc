<?php
/* autor:       orlando puentes
 * fecha:       14/09/2010
 * objetivo:     
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz .DIRECTORY_SEPARATOR. 'rsc' .DIRECTORY_SEPARATOR. 'pdo' .DIRECTORY_SEPARATOR. 'IFXDbManejador.php';
include_once $raiz .DIRECTORY_SEPARATOR. 'rsc' .DIRECTORY_SEPARATOR. 'pdo' .DIRECTORY_SEPARATOR. 'IFXerror.php';
include_once $raiz .DIRECTORY_SEPARATOR. 'clases' .DIRECTORY_SEPARATOR. 'p.afiliacion.class.php';
include_once $raiz .DIRECTORY_SEPARATOR. 'aportes'. DIRECTORY_SEPARATOR .'empresas'. DIRECTORY_SEPARATOR .'clases' .DIRECTORY_SEPARATOR. 'empresas.class.php';

$objClase = new Afiliacion();
$objAfiEmpresa = new Empresa();

$c0= $_REQUEST['v0'];	//idformulario;
$c1= $_REQUEST['v1'];	//tipoformulario;
$c2= $_REQUEST['v2'];	//tipoafiliacion;
$c3= $_REQUEST['v3'];	//idempresa;
$c4= $_REQUEST['v4'];	//fechaingreso;
$c5= $_REQUEST['v5'];	//horasdia;
$c6= $_REQUEST['v6'];	//horasmes;
$c7= $_REQUEST['v7'];	//salario;
$c8= $_REQUEST['v8'];	//agricola;
$c9= $_REQUEST['v9'];	//cargo;
$c10= $_REQUEST['v10'];	//primaria;
$c11= $_REQUEST['v11'];	//estado;
$c12= $_REQUEST['v12'];	//tipopago;
$c13= $_REQUEST['v13'];	//categoria;
$c14= (isset($_REQUEST['v14']))? $_REQUEST['v14']: null;	//codigo
$c15= (isset($_REQUEST['v15']))? $_REQUEST['v15']: null; // idpersona
$c16= (isset($_REQUEST['v16']))? $_REQUEST['v16']: null;//fecharetiro REVISAR!!!!!
$ca=explode('-', $c16);
$c16=$ca[0].$ca[1].$ca[2];
$c17= (isset($_REQUEST['v17']))? $_REQUEST['v17']: 0;

//echo $c0;

$estadoActual = (isset($_REQUEST['estadoActual']))? $_REQUEST['estadoActual']: null; // estado actual de la afiliaci�n, para saber si se busca en aportes016 � aportes017

// s�lo para independientes
$indiceAportes = (isset($_REQUEST["indice"]))?$_REQUEST["indice"]:null;

// este es el codigom para tomar la cookie en PHP
if (!isset($_COOKIE["indiceCookie"])) {
	$indiceCookie = "";
	$expire=time()+60*60*24;
	setcookie("indiceCookie", $indiceCookie, $expire);
} else {
	$indiceCookie = $_COOKIE["indiceCookie"];
}
//echo "indiceCookie=".$indiceCookie;

// tipos de afiliaci�n para independientes
$arrIdsTiposAfiliacionIndependiente = array(19,20,21,2938,4209);
$actualizar = true;
$afiliacionInactiva = null;
$db = IFXDbManejador::conectarDB();

if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}





  // se consulta la tabla aportes016 para verificar si la afiliacion esta activa o inactiva.
  $resultAfiliacion = $objClase->afiliacion_idformulario_idpersona($c0,$c15);
  $afiliacion = mssql_fetch_array($resultAfiliacion);

  /*
   * Proceso para anulacion desafiliar por muerte tanto en actualizacion como en adici�n.
  * 1. Actualiza el estado del afiliado en aportes015 en M (Muerte)
  * 2. Revisa las afiliaciones activas y las inactiva con motivo retiro 2860 MUERTE TRABAJADOR
  * adicionadas 9 de Junio de 2016
  */
  
  if($c14=='2860'){
  	$cmbm=$objClase->CambioEstadoM($c15,$c14,$c16);
  	if($cmbm==0){
  		echo 0;
  		exit();
  	}
  }
  
  /*
   * fin adicion 9 de junio
  */

if($afiliacion==false){
	//afiliacion_idafiliacion2
	
	
	
	$resultAfiliacion = $objClase->afiliacion_idformulario_idpersona_2($c0,$c15);
	$afiliacion = mssql_fetch_array($resultAfiliacion);
	if($afiliacion==false){
		echo 0;
		die();
	} else {
		// se actualizan los datos de la afiliaci�n inactiva, en aportes017, ya no se debe actualizar informaci�n en aportes016		
		/*$c16=$afiliacion["fecharetiro"];
		$c14=$afiliacion["motivoretiro"];*/
		$arrDatos = array("idformulario" => $c0, "tipoformulario" => $c1, "tipoafiliacion" => $c2, "idempresa" => $c3, "fechaingreso" => $c4, "horasdia" => $c5, "horasmes" => $c6, "salario" => $c7, "agricola" => $c8, "cargo" => $c9, "primaria" => $c10, "estado" => $c11, "fecharetiro" => $c16, "motivoretiro" => $c14 , "tipopago" => $c12, "categoria" => $c13, "claseafiliacion" => $c17);
		$r = $objClase->actualizar_afiliacion_inactiva2($arrDatos);
		if ($r  == true){
			//Categoria
			$resultCategoria = $objClase->spProcesoIndepPensCategorias($c15);
			// si es una afiliaci�n de trabajador independiente, se debe actualizar el �ndice de aportes en aportes048
			if(in_array(intval($c2),$arrIdsTiposAfiliacionIndependiente)){
				$sql = "update aportes048 set indicador=$indiceAportes,fechaaportes='".$c4."',fechaafiliacion='".$c4."',fechasistema=cast(GetDate() as date) where idempresa={$afiliacion["idempresa"]}";
				if($c2==19 && $indiceCookie==106){	
				$sql = "update aportes048 set indicador=$indiceAportes,fechaaportes='".$c4."',fechaafiliacion='".$c4."',fechasistema=cast(GetDate() as date),idtipoafiliacion=3319 where idempresa={$afiliacion["idempresa"]}";
				}
				$resultUpdate = $db->queryActualiza($sql);
				if($resultUpdate)
					echo 1;
				else
					echo 0;
			}else
				echo 1;
		}else
			echo 0;
		die();
	}
}

if($c11=='I'){
	// primero se verifica si se va a actualizar una afiliaci�n que ya est� inactiva, para buscarla en aportes017
	if($estadoActual == 'I'){
		$result = $objClase->buscar_afiliacion_inactiva_por_idformulario($c0);
		$afiliacionInactiva = mssql_fetch_array($result);
	}

	if(!is_array($afiliacionInactiva)){
		
		// se inactiva la afiliaci�n		
		$sql = "execute sp_inactivar $c0,$c14,'$c16','{$_SESSION["USUARIO"]}', 0";
		$rs = $db->querySimple($sql);
		$idFormCreado = 0;
		if($rs==0){
			// "NO SE pudo inactivar la afiliacion, consulte con el administrador";
			echo 0;
			die();
		} else {
			//Categoria
			$resultCategoria = $objClase->spProcesoIndepPensCategorias($c15);
			$idFormCreado = intval($db->conexionID->lastInsertId());
			if($idFormCreado == 0){
				// el formulario a inactivar solicitado no existe
				echo 0;
				die();
			}else{
				// Si es trabajador independiente se inactiva la empresa
				if(in_array(intval($c2),$arrIdsTiposAfiliacionIndependiente)){
					$resultEmpresa = $objAfiEmpresa->buscar_empresa($afiliacion["idempresa"]);
					$empresa = mssql_fetch_array($resultEmpresa);
					$empresaInactivada = $objAfiEmpresa->inactivar_empresa_por_NIT($empresa["nit"]);
					if(!$empresaInactivada){
						echo 0;
						die();
					}
				}
			}
		}
		
		$v0=$c15;
		$sql="Select count(*) as cuenta from aportes016 where idpersona=$v0";
		$rs=$db->querySimple($sql);
		$w=$rs->fetch();
		if($w['cuenta']>0){
			$sql="Select count(*) as cuenta from aportes016 where primaria='S' and idpersona=$v0";
			$rs=$db->querySimple($sql);
			$w=$rs->fetch();
			if($w['cuenta']==0){
				$sql = "select top 1 idformulario from aportes016 where idpersona=$v0";
				$rs = $db->querySimple($sql);
				$w2 = $rs->fetch();
				$idf = $w2['idformulario'];
				$sql = "update aportes016 set primaria='S' where idformulario=$idf";
				$up = $db->queryActualiza($sql);
			}
		}
		echo 2;
		//Afiliacion Anulada - fecha de fidelidad y estado de filelidad
		$resultAfiliacion = $objClase->spProcesoAfiliacionAnula($idPersona);
		exit();
	}else{
		// se actualizan los datos de la afiliaci�n inactiva, en aportes017, ya no se debe actualizar informaci�n en aportes016
		$actualizar = false;
		$arrDatos = array("idformulario" => $c0, "tipoformulario" => $c1, "tipoafiliacion" => $c2, "idempresa" => $c3, "fechaingreso" => $c4, "horasdia" => $c5, "horasmes" => $c6, "salario" => $c7, "agricola" => $c8, "cargo" => $c9, "primaria" => $c10, "estado" => $c11, "fecharetiro" => $c16, "motivoretiro" => $c14 , "tipopago" => $c12, "categoria" => $c13);
		$r = $objClase->actualizar_afiliacion_inactiva($arrDatos);
		if ($r  == true){
			//Categoria
			$resultCategoria = $objClase->spProcesoIndepPensCategorias($c15);
			// si es una afiliaci�n de trabajador independiente, se debe actualizar el �ndice de aportes en aportes048
			if(in_array(intval($c2),$arrIdsTiposAfiliacionIndependiente)){
					$sql = "update aportes048 set indicador=$indiceAportes,fechaaportes='".$c4."',fechaafiliacion='".$c4."',fechasistema=cast(GetDate() as date) where idempresa={$afiliacion["idempresa"]}";
				if($c2==19 && $indiceCookie==106){				
					$sql = "update aportes048 set indicador=$indiceAportes,fechaaportes='".$c4."',fechaafiliacion='".$c4."',fechasistema=cast(GetDate() as date),idtipoafiliacion=3319 where idempresa={$afiliacion["idempresa"]}";
				}
				$resultUpdate = $db->queryActualiza($sql);
				/*
				if($resultUpdate)
					echo 1;
				else
					echo 0;
				*/
			}else
				echo 1;
		}else
			echo 0;
		die();
	}
	
//------------------------------------------------------------------------------------------------------------------------------	
}else{
	if($c11=='A' && $estadoActual == 'I'){
		$sql = "select tipoformulario,tipoafiliacion,idempresa,idpersona,fechaingreso,horasdia,horasmes,salario,agricola,cargo,primaria,estado,fecharetiro,motivoretiro,fechanovedad,semanas,fechafidelidad,estadofidelidad,traslado,codigocaja,flag,tempo1,tempo2,fechasistema,usuario,tipopago,categoria,auditado,idagencia,idradicacion,null as vendedor,null as codigosalario from aportes017 where idformulario = '$c0'";
		$rs=$db->querySimple($sql);
		$w=$rs->fetch();
		$idPersonaBuscar = $w['idpersona'];
		$sql1 = "select count(*) as conteo from aportes016 where idpersona = '$idPersonaBuscar' and primaria = 'S'";
		$rs1=$db->querySimple($sql1);
		$w1=$rs1->fetch();
		if($w1['conteo']==0){
			$resp = $objAfiliacion->insert_reactivar_afiliacion($campos=array($w['tipoformulario'],$w['tipoafiliacion'],$w['idempresa'],$w['idpersona'],$w['fechaingreso'],$w['horasdia'],$w['horasmes'],$w['salario'],$w['agricola'],$w['cargo'],'S','A',$w['fecharetiro'],$w['motivoretiro'],$w['fechanovedad'],$w['semanas'],$w['fechafidelidad'],$w['estadofidelidad'],$w['traslado'],$w['codigocaja'],$w['flag'],$w['tempo1'],$w['tempo2'],$w['fechasistema'],$_SESSION["USUARIO"],$w['tipopago'],$w['categoria'],$w['auditado'],$w['idagencia'],$w['idradicacion'],'NULL','NULL'));
		}else{
			$resp = $objAfiliacion->insert_reactivar_afiliacion($campos=array($w['tipoformulario'],$w['tipoafiliacion'],$w['idempresa'],$w['idpersona'],$w['fechaingreso'],$w['horasdia'],$w['horasmes'],$w['salario'],$w['agricola'],$w['cargo'],'N','A',$w['fecharetiro'],$w['motivoretiro'],$w['fechanovedad'],$w['semanas'],$w['fechafidelidad'],$w['estadofidelidad'],$w['traslado'],$w['codigocaja'],$w['flag'],$w['tempo1'],$w['tempo2'],$w['fechasistema'],$_SESSION["USUARIO"],$w['tipopago'],$w['categoria'],$w['auditado'],$w['idagencia'],$w['idradicacion'],'NULL','NULL'));
		}
		
		if($resp){
			$resp2 = $objClase->borrar_afiliacion_inactiva($c0);
			//Categoria
			$resultCategoria = $objClase->spProcesoIndepPensCategorias($c15);
			alert("Se Activo Correctamente la Afiliacion");
			$sql1 = "select top 1 idformulario from aportes016 order by idformulario desc";
			$rs1=$db->querySimple($sql1);
			$w1=$rs1->fetch();
			$c0 = $w1['idformulario'];
		}	
	}
}
//----------------------------------------------------------------------------------------------------------------------------
if($actualizar){
	$r= $objClase->actualizar_afiliacion2($campos=array($c0,$c1,$c2,$c3,$c4,$c5,$c6,$c7,$c8,$c9,$c10,$c11,$c12,$c13,$c17));
	if ($r  == true){
		//Categoria
		$resultCategoria = $objClase->spProcesoIndepPensCategorias($c15);
		// si es una afiliaci�n de trabajador independiente, se debe actualizar el �ndice de aportes en aportes048
		if(in_array(intval($c2),$arrIdsTiposAfiliacionIndependiente)){
			$resultAfiliacion = $objClase->afiliacion_idformulario_idpersona($c0,$c15);
			$afiliacion = mssql_fetch_array($resultAfiliacion);
				$sql = "update aportes048 set indicador=$indiceAportes,fechaaportes='".$c4."',fechaafiliacion='".$c4."',fechasistema=cast(GetDate() as date) where idempresa={$afiliacion["idempresa"]}";
			if($c2==19 && $indiceCookie==106){
				$sql = "update aportes048 set indicador=$indiceAportes,fechaaportes='".$c4."',fechaafiliacion='".$c4."',fechasistema=cast(GetDate() as date),idtipoafiliacion=3319 where idempresa={$afiliacion["idempresa"]}";
			}
			$resultUpdate = $db->queryActualiza($sql);
			/*
			if($resultUpdate)
				echo 1;
			else
				echo 0;
			*/
		}else
			echo 1;
	}else
		echo 0;
	die();
}
?>