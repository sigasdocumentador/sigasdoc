<?php

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$url=$_SERVER['PHP_SELF'];
include_once $raiz.DIRECTORY_SEPARATOR.'phpComunes'.DIRECTORY_SEPARATOR.'auditoria.php';
auditar($url);

include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>::Actualizar Relacion Beneficiario::</title>
		
		<link href="../../css/Estilos.css" rel="stylesheet" type="text/css" />
		<link href="../../css/marco.css" rel="stylesheet" type="text/css" />
		<link href="../../newcss/css.1.8/jquery-ui-1.8.17.custom.css" rel="stylesheet" type="text/css" />
		
		<script type="text/javascript" src="../../newjs/1.6/jquery-1.6.2.min.js"></script>
		<script type="text/javascript" src="../../newjs/1.6/jquery-ui-1.8.16.custom.min.js"></script>
		<script type="text/javascript" src="../../newjs/comunes.js"></script>
		<script type="text/javascript" src="js/actuaRelacBenef.js"></script>
</head>
<body>
	<div id="wrapTable">
		<table width="80%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td width="13" height="29" class="arriba_iz">&nbsp;</td>
				<td class="arriba_ce"><span class="letrablanca">::&nbsp;Actualizar
						Relaci&oacute;n Beneficiario&nbsp;::</span></td>
				<td width="13" class="arriba_de">&nbsp;</td>
			</tr>
			<tr>
				<td class="cuerpo_iz">&nbsp;</td>
				<td class="cuerpo_ce"><img
					src="<?php echo URL_PORTAL; ?>imagenes/spacer.gif" width="1"
					height="1" /> <img
					src="<?php echo URL_PORTAL; ?>imagenes/spacer.gif" width="2"
					height="1" /> <br /></td>
				<td class="cuerpo_de">&nbsp;</td>
			</tr>
			<tr>
				<td class="cuerpo_iz">&nbsp;</td>
				<!-- TABLAS DE FORMULARIO -->
				<td class="cuerpo_ce">
					<center>
						<table width="95%" border="0" cellspacing="0" class="tablero">
							<tr>
								<td>
									N&uacute;mero Identificaci&oacute;n:
									&nbsp;
									<input type="text" id="txtNumeroIdentificacion" name="txtNumeroIdentificacion"/>
									<input type="hidden" id="hidIdPersona" name="hidIdPersona"/>
									&nbsp;
									<input type="button" name="btnBuscarPersona" id="btnBuscarPersona" value="BUSCAR" /> 
									&nbsp;
									<b><label id="lblDatosPersona"></label></b>
								</td>
							</tr>
						</table>
						<br /> 
						<small><strong>BENEFICIARIOS</strong></small><br /><br /> 
						<table width="95%" border="0" cellspacing="0" class="tablero" id="tabBeneficiario">
							<thead>
								<tr>
									<th >Identificaci&oacute;n</th>
									<th >Nombres</th>
									<th >Estado Persona</th>
									<th >Fecha Nacimiento</th>
									<th >Estado Afiliaci&oacute;n</th>
								</tr>
							</thead>
							<tbody>
								<tr><td colspan="5">&nbsp;</td></tr>
							</tbody>
							<tfoot>
								<tr>
									<td colspan="5" style="text-align:center;">
										<input type="button" name="btnActualizar" id="btnActualizar" value="ACTUALIZAR"/>
									</td>
								</tr>
							</tfoot>
						</table>
					</center>

				<td class="cuerpo_de"></td>
			<tr>
				<td height="41" class="abajo_iz">&nbsp;</td>
				<td class="abajo_ce"></td>
				<td class="abajo_de">&nbsp;</td>
			</tr>
		</table>
	</div>
	
	<!-- DIV OBSERVACION -->
	<div id="divObservacion" title="::OBSERVACI&Oacute;N::" style="display: none;">
		<br />
	 	<table width="90%" border="0" cellpadding="5" cellspacing="0" class="tablero">
	   		<tr>
    			<td>Observaci&oacute;n</td>
    			<td>
    				<textarea name="txaObservacion" id="txaObservacion" class="boxlargo"></textarea>
    			</td>
   			</tr>
		</table>
	</div>
	
</body>

</html>