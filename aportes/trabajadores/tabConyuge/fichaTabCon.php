<?php
/* autor:       orlando puentes
 * fecha:       Septiembre 14 de 2010
 * objetivo:    Solicitar que hagan el pl�stico de la tarjeta plata a ASSenda s. a.
 */

date_default_timezone_set("America/Bogota");
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once $root;

include_once $raiz.DIRECTORY_SEPARATOR.'config.php';
include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'p.persona.class.php';
include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'grupo.familiar.class.php';
include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'p.afiliacion.class.php';
include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'ciudades.class.php';
include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'p.definiciones.class.php';
include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'p.paquete.escolar.class.php';
include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'p.becas.class.php';
include_once $raiz.DIRECTORY_SEPARATOR.'funcionesComunes'.DIRECTORY_SEPARATOR.'funcionesComunes.php';
include_once $raiz.DIRECTORY_SEPARATOR.'seguridad'.DIRECTORY_SEPARATOR.'phpComunes'.DIRECTORY_SEPARATOR. 'funciones.php';


$idp=$_REQUEST['v0'];
$flag=$_REQUEST['flag'];
$idEmpresa='';
$idBenefParaCertificados='';
global $arregloAgencias;
$tercero = (isset($_REQUEST["esTercero"]) && $_REQUEST["esTercero"] == 'true')?true:false;
// @author Oswaldo - Par�metros FONEDE para realizar c�lculos de dias de afiliaci�n 
$fonede = (isset($_REQUEST["fonede"]))?true:false;
$fechaRetiroUltimaAfiliacion = (isset($_REQUEST["fechaRetiroUltTray"]) && $_REQUEST["fechaRetiroUltTray"] != '' && $_REQUEST["fechaRetiroUltTray"] != false)?$_REQUEST["fechaRetiroUltTray"]:"";
$expRegularFecha = '/^(|(0[1-9])|(1[0-2]))\/((0[1-9])|(1\d)|(2\d)|(3[0-1]))\/((\d{4}))$/';
if(!preg_match($expRegularFecha,$fechaRetiroUltimaAfiliacion))
	$fechaRetiroUltimaAfiliacion = "";
$ide=0;
$cad=($flag==2)? 'Ficha Conyuge' : 'Ficha Afiliado';

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
$rsp=$db->Persona_todo_id($idp);
$rowPersona=$rsp->fetch();
$nomT=trim($rowPersona['pnombre'])." ".trim($rowPersona['snombre'])." ".trim($rowPersona['papellido'])." ".trim($rowPersona['sapellido']);
$estadoTrabajador=$rowPersona['estado'];
if ($estadoTrabajador=='M' && $rowPersona['fechadefuncion'].lenght>0) {
	$estadoTrabajador ="<a class='dialogoEmergente' >".$rowPersona['estado']."<span>".$rowPersona['fechadefuncion']."</span></a>";
}
/*$_SESSION["NOMBRE"]=$nomT;*/
if(!isset($_SESSION['SMLV'])){
	$sqlSalario="select top 1 smlv from aportes012 where procesado='S' order by periodo desc";
	$rsSalario=$db->querySimple($sqlSalario);
	$rowSalario=$rsSalario->fetch();
	$smlv=intval($rowSalario['smlv']);
	$_SESSION['SMLV']=$smlv;
} else { $smlv=$_SESSION['SMLV']; }
$objClase=new Definiciones();
$objCiudad=new Ciudades();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Ficha Afiliado</title>
<link type="text/css" href="<?php echo URL_PORTAL; ?>newcss/Estilos.css" rel="stylesheet"/>
<?php if($fonede):?>
<script language="javascript" type="text/javascript" src="<?php echo $url; ?>js/jquery.combos.js"></script> 
<script language="javascript" type="text/javascript" src="<?php echo $url; ?>js/direccion.js"></script>
<script language="javascript" type="text/javascript">

$(document).ready(function(){
	tiempoFonedeCalculado = (!tiempoFonedeCalculado)?false:true;
	if(!tiempoFonedeCalculado && !radicacionesFonedePendiente && puedeEntrarProcesoFonede){
		$.ajax({
			url: '../../phpComunes/buscarConvivencia.php',
			async: false,
			dataType: 'json',
			data: ({v0:<?php echo $idp; ?>}),
			success: function(datos){
				if(datos != 0){
					//marcar checkin 'convivencia: si'
					$("#si_convivencia_fonede").attr('checked','checked');
					$("#docs_fonede div:eq(2)").show();
					
					$.ajax({
						url: '../../phpComunes/buscarAfiliacionActivaIDP.php',
						async: false,
						dataType: 'json',
						data: ({v0: datos[0].idconyuge}),
						success: function(afiliacion){
							if(afiliacion != 0){
								//("Compa&ntilde;ero permanente tiene AFILIACI&Oacute;N ACTIVA con la caja. \nDebe presentar documentos que demuestren NO CONVIVENCIA.");
								var convive = confirm("El compa\u00f1ero permanente tiene afiliaci\u00F3n activa en la caja.\n Presenta documentaci\u00F3n que certifique que no conviven?");
								if(convive){
									$("#si_convivencia_fonede").attr("checked",true);
									$("#si_conyactivo_fonede").attr("checked",true);
									$("#docs_fonede div:eq(2)").show();
									$("#conyactivo_docs").show();
								}
							}
						}
					});
					
				}
				recalcularTiempoFonede();
				tiempoFonedeCalculado = true;
			}
		});		
	}

	$( "#accordionAfiAnteriores,#accordionPaqEscolar,#accordionObservacion" ).accordion({
		autoHeight: false,		
		collapsible: true,
		active: false
	});
});

</script>
<?php else:?>
<script language="javascript" type="text/javascript">
	$(document).ready(function(){
		if(($("#tablaAfiliacionesActivas tbody tr").length-2)==0&&($("#tablaAfiliacionesInactivas tbody tr").length)>1){
			var fechaSplit=$("#tablaAfiliacionesInactivas tbody tr:eq(0) td:eq(5)").text().split("-");
			var anioProteccion=parseInt(fechaSplit[0])+1;
			$("#periodoProteccion2").html("Hasta "+anioProteccion+"-"+fechaSplit[1]+"-"+fechaSplit[2]);
			$("#periodoProteccion1,#periodoProteccion2").show();
		}else{
			$("#periodoProteccion1,#periodoProteccion2").hide();
		}

		$( "#accordionAfiAnteriores,#accordionPaqEscolar,#accordionObservacion" ).accordion({
			autoHeight: false,		
			collapsible: true,
			active: false
		});
	});
</script>
<?php endif;?>
<script language="javascript" type="text/javascript">
function alertaRazonSocial(razon){
	alert(razon);
  }

$(function(){
	$("#div-datosCertificados").dialog({
		autoOpen:false,
		width:640,
		show: "drop",
		hide: "clip",	
		open:function(event,ui){
		},//fin funcion open
		close: function(event,ui){
			$("#div-datosCertificados table tbody").empty();
		}
	});//fin dialog
});
	
function mostrarDatosCertificados(idBeneficiario){
	$("#div-datosCertificados table tbody").empty();
	$.getJSON(URL+"phpComunes/buscarCertificados.php", {idBeneficiario: idBeneficiario}, function(data){
		if(data.length > 0){
			$("#div-datosCertificados").dialog('open');
			$.each(data,function(i,fila){
				var strTr = "<tr><td>"+ fila.idcertificado +"</td><td>"+ fila.tipo_certificado +"</td><td>"+ fila.periodoinicio +"</td><td>"+ fila.periodofinal +"</td><td>"+ fila.fechapresentacion +"</td></tr>";
				$("#div-datosCertificados table tbody").append(strTr);
			});
		}
	});
}

$(function(){
	$("#div-datosEmpresa").dialog({
		autoOpen:false,
		width:740,
		hide: "clip",	
		open:function(event,ui){
		},//fin funcion open
		
		close:function(event,ui){
			$("#idEmp").html("");
			$("#sector").html("");
			$("#clasesoci").html("");
			$("#nit1").html("");
			$("#digito1").html("");
			$("#tipo_doc").html(""); 
			$("#razonsocial").html("");
			$("#codigosucursal").html("");
			$("#sigla").html("");
			$("#direccion").html("");
			$("#departmento").html("");
			$("#municipio").html("");
			$("#telefono").html("");
			$("#fax").html("");
			$("#url").html("");
			$("#repLeg").html("");
			$("#contacto").html("");
			$("#contratista").html("");
			$("#actividad").html("");
			$("#actDane").html("");
			$("#indicador").html("");
			$("#idasesor").html("");
			$("#seccional").html("");
			$("#estado").html("");
		    $("#clase_apo").html("");
			$("#tipo_apo").html("");
			$("#fechaafiliacion").html("");
			$("#fechaestado").html("");
			$("#fechamatricula").html("");
			$("#fechaaportes").html("");
			$("#usuario").html("");
			$("#fechasistema").html("");			
		}
	});//fin dialog
});
	
function mostrarDatosEmpresa(nit){
	$.getJSON(URL+"phpComunes/buscarDatosEmpresa.php", {v0:nit}, function(data){
		if(data){
			var idempresa1='';
			$.each(data,function(i,fila){				
				idempresa1=fila.idempresa;
				$("#sector").html(fila.sector);
				$("#clasesoci").html(fila.clasesoci);
				$("#nit1").html(idempresa1 + " - " + $.trim(fila.nit));
				$("#digito1").html($.trim(fila.digito));
				$("#tipo_doc").html($.trim(fila.tipo_doc)); 
				$("#razonsocial").html($.trim(fila.razonsocial));
				$("#codigosucursal").html(fila.codigosucursal);
				$("#sigla").html(fila.sigla);
				$("#direccion").html($.trim(fila.direccion));
				$("#departmento").html(fila.departmento);
				$("#municipio").html(fila.municipio);
				$("#telefono").html(fila.telefono);
				$("#fax").html(fila.fax);
				$("#url").html(fila.url);
				$("#repLeg").html($.trim(fila.par)+" "+$.trim(fila.sar)+" "+$.trim(fila.pnr)+" "+$.trim(fila.snr));
				$("#contacto").html($.trim(fila.pac)+" "+$.trim(fila.sac)+" "+$.trim(fila.pnc)+" "+$.trim(fila.snc));
				$("#contratista").html($.trim(fila.contratista));
				$("#actividad").html($.trim(fila.actividad));
				$("#actDane").html($.trim(fila.codact+" - "+fila.dane));
				$("#indicador").html(fila.indicador);
				$("#idasesor").html(fila.idasesor);
				$("#seccional").html(fila.seccional);
				$("#estado").html(fila.estado);
			    $("#clase_apo").html(fila.clase_apo);
				$("#tipo_apo").html(fila.tipo_apo);
				$("#fechaafiliacion").html(fila.fechaafiliacion);
				$("#fechaestado").html(fila.fechaestado);
				$("#fechamatricula").html(fila.fechamatricula);
				$("#fechaaportes").html(fila.fechaaportes);
				$("#usuario").html(fila.usuario);
				$("#fechasistema").html(fila.fechasistema);			
			});
			$.getJSON(URL+"phpComunes/buscarObservacion.php", {v0:idempresa1,v1:2}, function(data){
				if(data){
					$.each(data,function(j,fila1){
						$("#observas").html(fila1);
					});
				}
			});

			$("#div-datosEmpresa").dialog('open');
		}
	});	
}  
</script>
</head>
<body>
<center>
	<table width="95%" border="0" cellspacing="0" cellpadding="0">
  		<tr>
    		<td width="13" height="29" background="<?php echo URL_PORTAL; ?>imagenes/tabla/arriba_izq.gif">&nbsp;</td>
    		<td background="<?php echo URL_PORTAL; ?>imagenes/tabla/arriba_central2.gif"><span class="letrablanca">::<?php echo $cad; ?>&nbsp;::</span></td>
    		<td width="13" background="<?php echo URL_PORTAL; ?>imagenes/tabla/arriba_der.gif" align="right">&nbsp;</td>
  		</tr>
  		<tr>
			<td background="<?php echo URL_PORTAL; ?>imagenes/tabla/izquierda2.jpg">&nbsp;</td>
			<td background="<?php echo URL_PORTAL; ?>imagenes/tabla/centro.gif">&nbsp;</td>
			<td background="<?php echo URL_PORTAL; ?>imagenes/tabla/derecha.gif">&nbsp;</td>
  		</tr>
   		<tr>
    		<td background="<?php echo URL_PORTAL; ?>imagenes/tabla/izquierda2.jpg">&nbsp;</td>
    		<td background="<?php echo URL_PORTAL; ?>imagenes/tabla/centro.gif"><br />
    			<h3 class="ui-accordion-header ui-helper-reset ui-state-default ui-corner-all" style="padding: 0.5em 0.5em 0.5em 0.7em; padding-left: 2.2em;" > 
					Datos del Afiliado
				</h3>	
    		</td>
		    <td background="<?php echo URL_PORTAL; ?>imagenes/tabla/derecha.gif">&nbsp;</td>
  		</tr>
  		<tr>
    		<td background="<?php echo URL_PORTAL; ?>imagenes/tabla/izquierda2.jpg">&nbsp;</td>
    		<td align="center" background="<?php echo URL_PORTAL; ?>imagenes/tabla/centro.gif">
				<table width="95%" border="0" cellspacing="0" class="tablero">
       				<tr>
          				<th width="8%" >N&uacute;mero</th>
			          	<th width="34%" >Afiliado</th>
			          	<th width="4%" >Estado</th>
			          	<th width="10%" >Zona</th>
			          	<th width="18%" >Direccion</th>
			          	<th width="8%" >Tel</th>
			          	<th width="6%" >F Nace</th>
			          	<th width="3%" >Edad</th>
			          	<th width="3%" >RUAF</th>
          				<th id="periodoProteccion1" style="display: none;">Per&iacute;odo de Protecci&oacute;n</th>
       				</tr>
       				<tr>
         				<td style="text-align:center" ><?php echo $rowPersona['identificacion'];  ?></td>
         				<td style="text-align:left" ><?php echo $idp." - ".$nomT; ?></td>
         				<td style="text-align:center" ><?php echo $estadoTrabajador;  ?></td>
         				<td style="text-align:center" ><?php echo $rowPersona['zona'];  ?></td>
         				<td style="text-align:center" ><?php echo $rowPersona['direccion'];  ?></td>
         				<td style="text-align:center" ><?php echo $rowPersona['telefono'];  ?></td>
         				<td style="text-align:center" ><?php echo $rowPersona['fechanacimiento'];  ?></td>
         				<td style="text-align:center" ><?php echo intval($rowPersona['dias']);  ?></td>
         				<td style="text-align:center" ><?php echo $rowPersona['ruaf'];  ?></td>
         				<td id="periodoProteccion2" style="display: none; text-align:center"></td>
       				</tr>
     			</table>
				<?php
				@mssql_free_result($rsp);
				unset($rowPersona);
				$objAfiliacion=new Afiliacion();
				$conAfiliacion=$objAfiliacion->buscarFichaAfiliacionesActivasAfiliado($idp,$ide);

				// Variables utilizadas para calcular tiempo para postular a FONEDE
				$fechaHace3Anos = $fechaHoyHace3Anos = date("m") ."/". date("d") ."/". (intval(date("Y"))-3);
				$marcaF3Anos = mktime(0,0,0,date("m"),date("d"),(intval(date("Y"))-3));
				$marcaHoy = mktime(0,0,0,date("m"),date("d"),date("Y"));
				$totalTiempoFonedeActivo = 0; // tiempo en d�as de la afiliaci�n actual (activa)

				?>     
			</td>
			<td background="<?php echo URL_PORTAL; ?>imagenes/tabla/derecha.gif">&nbsp;</td>
		</tr>
   		<tr>
    		<td background="<?php echo URL_PORTAL; ?>imagenes/tabla/izquierda2.jpg">&nbsp;</td>
    		<td background="<?php echo URL_PORTAL; ?>imagenes/tabla/centro.gif"><br />
    			<h3 class="ui-accordion-header ui-helper-reset ui-state-default ui-corner-all" style="padding: 0.5em 0.5em 0.5em 0.7em; padding-left: 2.2em;" >
		    		Afiliaci&oacute;n Activa
		    	</h3>
		    </td>
    		<td background="<?php echo URL_PORTAL; ?>imagenes/tabla/derecha.gif">&nbsp;</td>
  		</tr>
  		<tr>
    		<td background="<?php echo URL_PORTAL; ?>imagenes/tabla/izquierda2.jpg">&nbsp;</td>
    		<td align="center" background="<?php echo URL_PORTAL; ?>imagenes/tabla/centro.gif">
    			<?php
		    		$flagServicios=0;
		    		$cont=mssql_num_rows($conAfiliacion);
		    		if(is_numeric($cont) && $cont>0){ 
		    	?>
				<table id="tablaAfiliacionesActivas" width="95%" border="0" cellspacing="0" class="tablero">
    				<tr>
					  	<th width="6%">Rad.</th>
						<th width="6%">Grab.</th>
						<th width="2%">Nit</th>
					    <th width="26%">Empresa</th>
					    <th width="4%">F. Ingre</th>
					    <?php if($fonede): ?>
						    <th align="center">F. Retiro</th>
					    <?php endif; ?>
					    <th width="5%">Tipo Form.</th>
					    <th width="5%">Tipo Afi.</th>
					    <th width="3%">Horas</th>
					    <th width="6%">Salario</th>
					    <th width="14%">Cargo</th>
					    <th width="2%">Ctr&iacute;a</th>
					    <th width="4%">Est.</th>
					    <th width="2%">Pri.</th>
					    <th width="2%">Agr.</th>
					    <th width="4%">D. Lab.</th>
					    <th width="4%">D. FON.</th>
       				</tr>
  					<?php
 						$totalTiempoFonedeActivo = 0;
 						$salarioAcumulado=0; 						
 						
 						while($rowAfiliacion=mssql_fetch_array($conAfiliacion)){
 							if($rowAfiliacion['primaria']=='S') { $flagServicios=$rowAfiliacion['flag']; }
 							$radicado = $rowAfiliacion["usuarioRadica"] . "<br/>" . $rowAfiliacion["fecharadicacion"];
 							$grabado = $rowAfiliacion["usuarioGraba"] . "<br/>" . $rowAfiliacion["fechaproceso"];
	 						$salarioAcumulado+=$rowAfiliacion['salario'];
	 						$obsPU = "";
	 						if($rowAfiliacion["estado"] == 'PU'){
	 							$obsPU = "No se ha radicado, entr&oacute; por Planilla &Uacute;nica.";
	 							$rowAfiliacion["estado"] = 'P';
	 						}
						 	/**
						 	 * Bloque que calcula el tiempo de afiliaci�n, para tener en cuenta para FONEDE
						 	 * @author Oswaldo 11-08-2011
						 	 * aaaa - mm - dd
						 	 */ 
							$idEmpresa=$rowAfiliacion["idempresa"];
							$fechaIngreso = $rowAfiliacion["fechaingreso"];
							$marcaFI = mktime(0,0,0,substr($fechaIngreso,5,2),substr($fechaIngreso,8,2),substr($fechaIngreso,0,4));	
							$diasLaborando =($marcaHoy-$marcaFI) /(60*60*24);	
							if($fechaRetiroUltimaAfiliacion != ""){
								$arrFechaRetUltAfi = explode("/", $fechaRetiroUltimaAfiliacion);
								$marcaFR = mktime(0,0,0,$arrFechaRetUltAfi[0],$arrFechaRetUltAfi[1],$arrFechaRetUltAfi[2]); // se toma la fecha de retiro seg�n lo digitado al verificar el certificado laboral f�sico
							} else {
								$marcaFR = mktime(0,0,0,date("m"),date("d"),date("Y")); // se toma la fecha de retiro como el d�a de hoy
							}
							$diasTotalTrayectoria = "N/A";
							$marcarFila = false;
							$estiloColorFondo = "";
							if(($marcaFI <= $marcaF3Anos && $marcaFR >= $marcaF3Anos) || ($marcaFI <= $marcaF3Anos && is_nan($marcaFR)) || ($marcaFI >= $marcaF3Anos)){
								$marcarFila = true;
								$estiloColorFondo = "background-color: #F7D358;";		
								$marcaFechaNivelInferior = ($marcaFI <= $marcaF3Anos)?$marcaF3Anos:$marcaFI;
								$marcaFechaNivelSuperior = $marcaFR; //$marcaHoy;
								$tiempoUnixTrayectoria =  ($marcaFechaNivelSuperior - $marcaFechaNivelInferior);
								$diasTotalTrayectoria = $tiempoUnixTrayectoria / (60*60*24);
							}	
							if(intval($diasTotalTrayectoria) >0 ){
								$totalTiempoFonedeActivo += intval($diasTotalTrayectoria);
							}
							////////////////////////////// fin bloque c�lculos FONEDE
						?>
					<tr style="<?php echo $estiloColorFondo; ?>">
				    	<td style="text-align:right" ><?php echo $radicado ?></td>
				       	<td style="text-align:right" ><?php echo $grabado ?></td>
				       	<td ><a style="cursor: pointer;" onclick="mostrarDatosEmpresa(this.text)"><?php echo $rowAfiliacion['nit'];?></a></td>
				        <td ><?php echo $rowAfiliacion['razonsocial'];  ?></td>
				        <td ><?php echo $rowAfiliacion['fechaingreso'];  ?></td>
				        <?php if($fonede): ?>
				        	<td ><?php echo $fechaRetiroUltimaAfiliacion; ?></td> 
				        <?php endif; ?>       
				        <td ><?php echo $rowAfiliacion['tipoformulario'];  ?></td>
				        <td ><?php echo $rowAfiliacion['clasef'];  ?></td>
				        <td ><?php echo $rowAfiliacion['horasmes'];  ?></td>
				        <td style="text-align:center"><?php echo number_format($rowAfiliacion['salario']);  ?></td>
				        <td style="text-align:center"><span title="<?php echo $rowAfiliacion['cargo'];?>"><?php echo $rowAfiliacion['nomcargo']; ?></span></td>
				        <td style="text-align:center"><?php echo $rowAfiliacion['categoria'];  ?></td>
				        <td style="text-align:center">
				        <?php 						 
							if($obsPU != ""): 
						?>	
							<center><table border="0" cellspacing="0">
								<tr>
									<td style="border: 0px; padding-right:0px; padding-left:0px;" ><?php echo $rowAfiliacion['estado']; ?></td>
									<td style="border: 0px; padding-right:0px; padding-left:0px;">
										<img src="<?php echo URL_PORTAL; ?>imagenes/messagebox_warning.png" style="height:16px; width:16px;" title="<?php echo $obsPU; ?>" />
									</td>
								</tr>
							</table></center>
						<?php 
							else:
								echo $rowAfiliacion['estado'];
							endif;
						?>				       
				        </td>
				        <td style="text-align:center"><?php echo $rowAfiliacion['primaria']; ?></td>
				        <td style="text-align:center"><?php echo $rowAfiliacion['agricola']; ?></td>
				        <td style="text-align:center; font-weight: bold"><?php echo $diasLaborando;  ?></td>
				        <td style="text-align:center; font-weight: bold"><?php echo $diasTotalTrayectoria;  ?></td>
				    </tr>
					<?php } 
					$cat=(($salarioAcumulado)/$smlv);
					if($cat <= 0){
						$c='';
					} else {
						if($cat <= 2){
							$c='A';
						} elseif($cat <=4){
							$c='B';
						} else {
							$c='C';
						}
					} ?> 
					<tr>
						<th colspan="6"></th>
						<th colspan="2">Salario Acumulado</th>
	    				<th ><?php echo number_format($salarioAcumulado); ?></th>
	    				<th >Categoria</th>
						<th ><?php echo $c; ?></th>
	    				<th colspan="6">&nbsp;</th>	
					</tr>
				</table>
				<br /> 
			<?php } ?>    
		<?php  
			mssql_free_result($conAfiliacion);
			unset($rowAfiliacion);
			$objHistorico=new Afiliacion();
			if($fonede){
				$historico = $objHistorico->buscarFichaAfiliacionesInactivasAfiliado($idp); //$objHistorico->buscar_historico_fonede($idp,false);
			} else {
				$historico = $objHistorico->buscarFichaAfiliacionesInactivasAfiliado($idp);
			}
			$cont=0;
		?>     
			</td>
     		<td background="<?php echo URL_PORTAL; ?>imagenes/tabla/derecha.gif">&nbsp;</td>
		</tr>     
		<tr>
    		<td background="<?php echo URL_PORTAL; ?>imagenes/tabla/izquierda2.jpg">&nbsp;</td>
    		<td background="<?php echo URL_PORTAL; ?>imagenes/tabla/centro.gif">
    			<div id="accordionAfiAnteriores" >
		    		<h3 id="titAfiAnteriores">
						<a href="#"> Afiliaciones Anteriores </a>
					</h3>
					<div id="divAfiAnteriores">
						<?php 
							$cont=mssql_num_rows($historico);
		    				if(is_numeric($cont) && $cont>0){  ?>
						<table id="tablaAfiliacionesInactivas" width="100%" align="center" border="0" cellspacing="0" class="newtablero">
							<thead>
						       	<tr>
									<th width="6%">Rad.</th>
									<th width="6%">Grab.</th>	
									<th width="2%">Nit</th>
									<th width="28%">Empresa</th>
									<th width="4%">F Ingreso</th>
									<th width="4%">F Retiro</th>
									<th width="5%">Tipo Form.</th>
									<th width="5%">Tipo Afil.</th>
									<th width="3%">Horas</th>
									<th width="6%">Salario</th>              
									<th width="3%">Est.</th>
									<th width="3%">Sem.</th>
									<th width="4%">D&iacute;as</th>
									<?php if($fonede):?>
										<th >D&iacute;as para FONEDE</th>
									<?php endif; ?>
									<th width="4%">Motivo</th>
									<th width="4%">F Fidelidad</th>
								</tr>
							</thead>
							<tbody>
							<?php
							$totalTiempoFonedeHistorico = 0;			
							while($rowHistorico=mssql_fetch_array($historico)){
									$radicadoI = $rowHistorico["usuarioRadica"] . "<br/>" . $rowHistorico["fecharadicacion"];
									$grabadoI = $rowHistorico["usuarioGraba"] . "<br/>" . $rowHistorico["fechaproceso"];							
									$fechaIngreso = $rowHistorico["fechaingreso"];
									$marcaFI = mktime(0,0,0,substr($fechaIngreso,5,2),substr($fechaIngreso,8,2),substr($fechaIngreso,0,4));
									$fechaRetiro = $rowHistorico["fecharetiro"];
									$marcaFR = mktime(0,0,0,substr($fechaRetiro,5,2),substr($fechaRetiro,8,2),substr($fechaRetiro,0,4));
									$diasTotalHistoria = "N/A";
									$marcarFila = false;
									$estiloColorFondo = "";
									if(($marcaFI <= $marcaF3Anos && $marcaFR >= $marcaF3Anos) || ($marcaFI <= $marcaF3Anos && is_nan($marcaFR)) || ($marcaFI >= $marcaF3Anos)){
										$marcarFila = true;
										$estiloColorFondo = "background-color: #F7D358;";								
										$marcaFechaNivelInferior = ($marcaFI <= $marcaF3Anos)?$marcaF3Anos:$marcaFI;
										$marcaFechaNivelSuperior = (intval($marcaFR)==0)?$marcaHoy:$marcaFR;								
										$tiempoUnixTrayectoria =  ($marcaFechaNivelSuperior - $marcaFechaNivelInferior);
										$diasTotalHistoria = $tiempoUnixTrayectoria / (60*60*24);
									}
									if(intval($diasTotalHistoria) >0 ){
										$totalTiempoFonedeHistorico += intval($diasTotalHistoria);
									}
									$obsPU = "";
									if(($rowHistorico["estado"] == 'P') && ($rowHistorico["idradicacion"] == 0)){
										$obsPU = "No radic&oacute;, entr&oacute; por Planilla &Uacute;nica.";										
									}									
								?>      
								<tr style="<?php echo $estiloColorFondo; ?>">
									<td style="text-align:right" ><?php echo $radicadoI ?></td>
									<td style="text-align:right" ><?php echo $grabadoI ?></td>
									<td ><?php echo $rowHistorico['nit'];  ?></td>
									<td ><?php echo $rowHistorico['razonsocial'];  ?></td>
									<td ><?php echo $rowHistorico['fechaingreso'];  ?></td>
									<td ><?php echo $rowHistorico['fecharetiro'];  ?></td>
									<td ><?php echo $rowHistorico['tipoformulario'];  ?></td>
									<td ><?php echo $rowHistorico['clasef'];  ?></td>
									<td ><?php echo $rowHistorico['horasmes'];  ?></td>
									<td ><?php echo number_format($rowHistorico['salario']);  ?></td>         
									<td style="text-align:center">
										<?php 									 
								        	if($obsPU != ""): 
								        	?>
								        		<table border="0" cellspacing="0" >
								        			<tr>
								        				<td style="border: 0px; padding-right:0px; padding-left:0px;" ><?php echo $rowHistorico['estado']; ?></td>
														<td style="border: 0px; padding-right:0px; padding-left:0px;">
															<img src="<?php echo URL_PORTAL; ?>imagenes/messagebox_warning.png" style="height:16px; width:16px;" title="<?php echo $obsPU; ?>" />
														</td>
													</tr>
												</table>
											<?php 
											else:
												echo $rowHistorico['estado'];
											endif; 
										?>
									</td>
									<td ><?php echo $rowHistorico['semanas'];  ?></td>
									<td ><?php echo $rowHistorico['dias'];  ?></td>
									<?php if($fonede): ?>
										<td ><?php echo $diasTotalHistoria;  ?></td>
									<?php endif; ?>
									<td ><a class="dialogoEmergente" ><?php echo $rowHistorico['motivoretiro'];  ?><span><?php echo $rowHistorico['definicionmotivo'];  ?></span></a></td>
									<td ><?php echo $rowHistorico['fechafidelidad'];  ?></td>
								</tr>
							</tbody>
							<?php } ?>
							<?php if($fonede): ?>
							<tfoot>
								<tr>
									<td >TOTAL DIAS FONEDE:</td>
								 	<td style="font-weight: bold;">	<?php echo $totalTiempoFonedeHistorico; ?>
								 		<input type="hidden" name="totalTiempoAplicarFonede" id="totalTiempoAplicarFonede" value="<?php echo ( $totalTiempoFonedeActivo + $totalTiempoFonedeHistorico); ?>" />
								 	</td>
								 </tr>
							</tfoot>      
							<?php 
								endif;
								mssql_free_result($historico);
								unset($rowHistorico);			
							?>
						</table>
						<br />
						<?php }
							$objGrupo=new GrupoFamiliar();
							$grupo = $objGrupo->buscarBasicoConyuges($idp);
							$cont=0;
						?>					
					</div>
				</div> <br />	
    		<td background="<?php echo URL_PORTAL; ?>imagenes/tabla/derecha.gif">&nbsp;</td>
  		</tr>
  		<tr>
    		<td background="<?php echo URL_PORTAL; ?>imagenes/tabla/izquierda2.jpg">&nbsp;</td>
    		<td background="<?php echo URL_PORTAL; ?>imagenes/tabla/centro.gif">
    			<h3 class="ui-accordion-header ui-helper-reset ui-state-default ui-corner-all" style="padding: 0.5em 0.5em 0.5em 0.7em; padding-left: 2.2em;" >
		    		Relaci&oacute;n de Convivencia
		    	</h3>
    		</td>
    		<td background="<?php echo URL_PORTAL; ?>imagenes/tabla/derecha.gif">&nbsp;</td>
  		</tr>
  		<tr>
    		<td background="<?php echo URL_PORTAL; ?>imagenes/tabla/izquierda2.jpg">&nbsp;</td>
    		<td align="center" background="<?php echo URL_PORTAL; ?>imagenes/tabla/centro.gif">
    			<?php
		    		$cont=mssql_num_rows($grupo);
		    		if(is_numeric($cont) && $cont>0){ 
		    	?>
				<table width="95%" border="0" cellspacing="0" class="tablero">
       				<tr>          				
          				<th width="4%">TD</th>
						<th width="8%">N&uacute;mero</th>
						<th width="39%">Conyuge</th>
						<th width="5%">Convive</th>
						<th width="7%">F Nace</th>
						<th width="12%">Ult. Cambio Conv.</th>
						<th width="5%">Labora</th>
						<th width="7%">Salario</th>
						<th width="7%">F Ingreso</th>
			       	</tr>
					<?php
						while($rowGrupo=mssql_fetch_array($grupo)){
							$nom = utf8_encode($rowGrupo['pnombre']." ".$rowGrupo['snombre']." ".$rowGrupo['papellido']." ".$rowGrupo['sapellido']);
							$idc = $rowGrupo['idbeneficiario'];
							//$_SESSION['IDCONYUGE']=$idc;
							$conv = $rowGrupo['conviven'];
							$fechaNacimiento = $rowGrupo['fechanacimiento'];
							$fechaUltimoCambioConv = $rowGrupo['fechasistema'];
							$labora=$rowGrupo['labora'];
							$salario=($rowGrupo['salario']==0?'':number_format($rowGrupo['salario']));
							$fechaAfi=$rowGrupo['fechaingreso'];
					?>       
       				<tr>       					
       					<td style="text-align:center"><?php echo $rowGrupo['codigo'];  ?></td>
						<td style="text-align:right" ><?php echo $rowGrupo['identificacion'];  ?></td>
						<td style="text-align:left" ><?php echo $rowGrupo['idbeneficiario']." - ".$nom; ?></td>
						<td style="text-align:center" ><?php echo $conv; ?></td>
						<td style="text-align:center" ><?php echo $fechaNacimiento; ?></td>
						<td style="text-align:center" ><?php echo $fechaUltimoCambioConv; ?></td>
						<td style="text-align:center" ><?php echo $labora;  ?></td>
						<td style="text-align:center" ><?php echo $salario;  ?></td>
						<td style="text-align:center" ><?php echo $fechaAfi; ?></td>
			       </tr>
						<?php } ?>       
     			</table>
     			<br />
     			<?php } 
					mssql_free_result($grupo);
					unset($rowGrupo);
					$objGrupo=new GrupoFamiliar();
					$grupo = $objGrupo->buscarBasicoGrupoFamiliar($idp);
				?>    
			</td>
			<td background="<?php echo URL_PORTAL; ?>imagenes/tabla/derecha.gif">&nbsp;</td>
		</tr>    
		<tr>
   			<td background="<?php echo URL_PORTAL; ?>imagenes/tabla/izquierda2.jpg">&nbsp;</td>
   			<td background="<?php echo URL_PORTAL; ?>imagenes/tabla/centro.gif">
   				<h3 class="ui-accordion-header ui-helper-reset ui-state-default ui-corner-all" style="padding: 0.5em 0.5em 0.5em 0.7em; padding-left: 2.2em;" >
	    			Grupo Familiar
	    		</h3>	
   			</td>
   			<td background="<?php echo URL_PORTAL; ?>imagenes/tabla/derecha.gif">&nbsp;</td>
		</tr>
		<tr>
  			<td background="<?php echo URL_PORTAL; ?>imagenes/tabla/izquierda2.jpg">&nbsp;</td>
  			<td align="center" background="<?php echo URL_PORTAL; ?>imagenes/tabla/centro.gif">
			<?php
	    		$cont=mssql_num_rows($grupo);
	    		if(is_numeric($cont) && $cont>0){ ?>
				<table width="95%" border="0" cellspacing="0" class="tablero" id="tabTableGrupo">
					<tr>
						<th width="3%">TD</th>
						<th width="8%">N&uacute;mero</th>
						<th width="30%">Beneficiario</th>
						<th width="10%">Parentesco</th>
						<th width="4%">Est</th>		
						<th width="8%">F Nace</th>
						<th width="3%">Edad</th>
						<th width="4%">F Afil</th>
						<th width="4%">F Cert</th>
						<th width="4%">F Asig</th>
						<th width="9%">Relaci&oacute;n</th>
						<th width="3%">Giro.</th>
						<th width="3%">Dis</th>
						<th width="3%">Emb</th>
						<th width="4%">Est Afil</th>
					</tr>
					<?php 
						while($row=mssql_fetch_array($grupo)){
							$nombres = utf8_encode($row['idbeneficiario']." - ".$row['pnombre']." ".$row['snombre']." ".$row['papellido']." ".$row['sapellido']);
							$motivo="<a class='dialogoEmergente' >".$row['estadoAfiliacion']."<span>".$row["fechaEstadoAfiliacion"]." ".$row["motivoInactivo"]."</span></a>";
							$giro="<a class='dialogoEmergente' >".$row['giro']."<span>".$row["fechaGiroAfiliacion"]."</span></a>";
							
							$estadoBeneficiario=$row['fechaEstadoBeneficiario'];
							if ( $row['estadoBeneficiario'] == 'M' ) {									
								if ( $row['tipoestado'] == 0 ) {
									$estadoBeneficiario = $estadoBeneficiario . " Por Subsidio Funebre";
								} else if ( $row['tipoestado'] == 1 ) {
									$estadoBeneficiario = $estadoBeneficiario . " Por Proceso Cruce Registraduria";
								} else if ( $row['tipoestado'] == 2 ) {
									$estadoBeneficiario = $estadoBeneficiario . " Sin Subsidio Funebre";
								}									
							}
					?>
					<tr>
						<td style="text-align:center" ><?php echo $row['codigo']; ?></td>
						<td style="text-align:right" ><?php echo $row['identificacionBeneficiario']; ?></td>
					    <td style="text-align:left" ><?php echo $nombres; ?></td>
					    <td style="text-align:center" ><?php echo utf8_encode($row['parentesco']); ?></td>
					    <td style="text-align:center" ><a class="dialogoEmergente" ><?php echo $row['estadoBeneficiario']; ?><span><?php echo $estadoBeneficiario; ?></span></a></td>	    
					    <td style="text-align:center" ><?php echo $row['fechanacimiento']; ?></td>
					    <td style="text-align:center" ><?php echo round($row['edad'],2); ?></td>
					    <td style="text-align:center" ><?php echo $row['fechaafiliacion']; ?></td>
					    <td style="text-align:center" ><a style="cursor: pointer;" onclick="mostrarDatosCertificados(<?php echo $row["idbeneficiario"]; ?>)"><?php echo $row['fechapresentacion']?></a></td>
					    <td style="text-align:center" ><?php echo $row['fechaasignacion']; ?></td>
					    <td style="text-align:center" ><?php echo $row['identificacionConyuge']; ?></td>
					    <td style="text-align:center" ><?php echo $giro; ?></td>
					    <td style="text-align:center" ><?php echo $row['discapacitado']; ?></td>
					    <td style="text-align:center" ><?php echo $row['embarga']; ?></td>
					    <td style="text-align:center" ><?php echo $motivo;  ?></td>	    
  					</tr>
					<?php }?>
				</table>
				</br>
				<?php } 
					mssql_free_result($grupo);
					unset($row);
					$paquete=new Paquete();
					$paquetes = $paquete->buscar_paquete_id($idp);
				?>
   			</td>
   			<td background="<?php echo URL_PORTAL; ?>imagenes/tabla/derecha.gif">&nbsp;</td>
		</tr>
   		<tr>
  			<td background="<?php echo URL_PORTAL; ?>imagenes/tabla/izquierda2.jpg">&nbsp;</td>
   			<td background="<?php echo URL_PORTAL; ?>imagenes/tabla/centro.gif">
   			<div id="accordionPaqEscolar" >										
				<h3 id="titPaqEscolar" >
					<a href="#"> Paquete Escolar </a>
				</h3>
				<div id="divPaqEscolar">
					<?php 
						$cont=mssql_num_rows($paquetes);
		    			if(is_numeric($cont) && $cont>0){ ?>
					<table width="100%" align="center" border="0" cellspacing="0" class="newtablero" id="tabTableGrupo">
						<tr>
							<th >A&ntilde;o</th>
							<th ">Beneficiario</th>
							<th >Edad</th>
							<th >Estado</th>
							<th >Mesa</th>
							<th >F Entrega</th>
							<th >H Entrega</th>
							<th >Agencia</th>
						</tr>
						<?php
		    				while($row=mssql_fetch_array($paquetes)){								
								$nombres = utf8_encode($row['pnombre']." ".$row['snombre']." ".$row['papellido']." ".$row['sapellido']);
								$estado=($row['entregado']=='S') ? "Entregado" : "Por entregar";
								?>
						<tr>
							<td><?php echo $row['anno']; ?></td>
						    <td align='left'><?php echo $nombres; ?></td>
						    <td><?php echo $row["edadbeneficiario"]; ?></td>
						    <td><?php echo $estado; ?></td>
						    <td><?php echo $row['mesa']; ?></td>
						    <td><?php echo $row['fechaentrega']; ?></td>
						    <td><?php echo $row['horaentrega']; ?></td>
						    <td><?php echo $arregloAgencias[$row['agencia']]; ?></td>
						</tr>
							<?php } ?>
					</table>
					<br />	
					<?php } ?>				
				</div>
			</div> 
			<?php
				mssql_free_result($paquetes);
				unset($row);
				$objBeca=new Becas();
				$beca = $objBeca->buscar_beca($idp);
			?>
			</td>
    		<td background="<?php echo URL_PORTAL; ?>imagenes/tabla/derecha.gif">&nbsp;</td>
  		</tr>
    	<tr>
    		<td background="<?php echo URL_PORTAL; ?>imagenes/tabla/izquierda2.jpg">&nbsp;</td>
    		<td background="<?php echo URL_PORTAL; ?>imagenes/tabla/centro.gif"><br />
   				<h3 class="ui-accordion-header ui-helper-reset ui-state-default ui-corner-all" style="padding: 0.5em 0.5em 0.5em 0.7em; padding-left: 2.2em;" >
	    			Subsidio Educaci&oacute;n
	    		</h3>
   			</td>
   			<td background="<?php echo URL_PORTAL; ?>imagenes/tabla/derecha.gif">&nbsp;</td>
		</tr>
		<tr>
   			<td background="<?php echo URL_PORTAL; ?>imagenes/tabla/izquierda2.jpg">&nbsp;</td>
   			<td align="center" background="<?php echo URL_PORTAL; ?>imagenes/tabla/centro.gif">
   				<?php 
					$cont=mssql_num_rows($beca);
		    		if(is_numeric($cont) && $cont>0){ ?>
				<table width="95%" border="0" cellspacing="0" class="tablero" id="tabTableGrupo">
   					<tr>
      					<th width="15%" align="center">ID Beneficiario</th>
       					<th width="15%" align="center">Nombre</th>
       					<th width="15%" align="center">Edad</th>
       					<th width="15%" align="center">Colegio</th>
       					<th width="15%" align="center">Grado</th>
   					</tr>
					<?php
						while($rowbeca=mssql_fetch_array($beca)){
							$nombre1 = utf8_encode($rowbeca['pnombre']." ".$rowbeca['snombre']." ".$rowbeca['papellido']." ".$rowbeca['sapellido']);
					?>      
    				<tr>
       					<td ><?php echo $rowbeca['idbeneficiario'];  ?></td>
       					<td ><?php echo $nombre1;  ?></td>
       					<td ><?php echo $rowbeca['edad'];  ?></td>
       					<td ><?php echo $rowbeca['razonsocial'];  ?></td>
       					<td ><?php echo $rowbeca['grado'];  ?></td>
    				</tr>
						<?php } ?>						
				</table>
				<br />	
				<?php }
					mssql_free_result($beca);
					unset($rowbeca);
					$sql="SELECT idbono,valorbono,idconvenio,anulado,aportes070.fechasistema,aportes091.concepto FROM aportes070 INNER JOIN aportes091 on aportes070.idconvenio=aportes091.iddetalledef WHERE anulado='N' and idtrabajador=".$idp;
					$rs=$db->querySimple($sql);
				?>
   			</td>
   			<td background="<?php echo URL_PORTAL; ?>imagenes/tabla/derecha.gif">&nbsp;</td>
		</tr>  
   		<tr>
   			<td background="<?php echo URL_PORTAL; ?>imagenes/tabla/izquierda2.jpg">&nbsp;</td>
   			<td background="<?php echo URL_PORTAL; ?>imagenes/tabla/centro.gif">
   				<h3 class="ui-accordion-header ui-helper-reset ui-state-default ui-corner-all" style="padding: 0.5em 0.5em 0.5em 0.7em; padding-left: 2.2em;" >
	    			Subsidio Intencional 
	    		</h3>
   			</td>
 			<td background="<?php echo URL_PORTAL; ?>imagenes/tabla/derecha.gif">&nbsp;</td>
		</tr>
		<tr>
   			<td background="<?php echo URL_PORTAL; ?>imagenes/tabla/izquierda2.jpg">&nbsp;</td>
   			<td align="center" background="<?php echo URL_PORTAL; ?>imagenes/tabla/centro.gif">
   				<?php 
    				$sqlCount="SELECT count(idbono) cont FROM aportes070 WHERE anulado='N' and idtrabajador=".$idp;
					$rsCount=$db->querySimple($sqlCount); $rowCount=$rsCount->fetch();
					$cont=$rowCount["cont"]; unset($rowCount);
		    		if(is_numeric($cont) && $cont>0){ ?>
				<table width="95%" border="0" cellspacing="0" class="tablero" id="tabTableGrupo">
    				<tr>
      					<th width="15%" align="center">Id Tarjeta</th>
       					<th width="15%" align="center">Valor Subsidio</th>
       					<th width="15%" align="center">Fecha</th>
       					<th align="center">Convenio</th>
       				</tr>
					<?php
						while($rowbeca=$rs->fetch()){ ?>      
   					<tr>
       					<td ><?php echo $rowbeca['idbono']; ?></td>
       					<td ><?php echo $rowbeca['valorbono']; ?></td>
       					<td ><?php echo $rowbeca['fechasistema']; ?></td>
       					<td ><?php echo $rowbeca['concepto']; ?></td>
       				</tr>
					<?php } ?>   
				</table>
				<br />
				<?php }
					unset($rowbeca);
					$sqlObs="SELECT observaciones FROM aportes088 WHERE identidad=1 AND idregistro=".$idp;
					$rsObs=$db->querySimple($sqlObs);
				?>    
   			</td>
   			<td background="<?php echo URL_PORTAL; ?>imagenes/tabla/derecha.gif">&nbsp;</td>
  		</tr>
     	<tr>
    		<td background="<?php echo URL_PORTAL; ?>imagenes/tabla/izquierda2.jpg">&nbsp;</td>
    		<td background="<?php echo URL_PORTAL; ?>imagenes/tabla/centro.gif">
    			<div id="accordionObservacion" >										
					<h3 id="titObservacion" >
						<a href="#"> Observaciones </a>
					</h3>
					<div id="divObservacion">
						
						<?php 
	    					$sqlCount="SELECT count(idobservacion) cont FROM aportes088 WHERE identidad=1 AND idregistro=".$idp;
							$rsCount=$db->querySimple($sqlCount); $rowCount=$rsCount->fetch();
							$cont=$rowCount["cont"]; unset($rowCount);
				    		if(is_numeric($cont) && $cont>0){ ?>												
						<table width="95%" border="0" cellspacing="0" class="tablero" id="tabTableGrupo">
			       			<tr>
								<th width="15%" align="center">Observacion</th>						
							</tr>
							<?php while($rowObs=$rsObs->fetch()){ ?>      
			       			<tr>
						         <td ><?php echo $rowObs['observaciones']; ?></td>				         
			         		</tr>
							<?php } ?>   
						</table>	
						<br />
						<?php }
							unset($rowObs);							
						?>	
					</div>
				</div> 
   			</td>
  			<td background="<?php echo URL_PORTAL; ?>imagenes/tabla/derecha.gif">&nbsp;</td>
		</tr>
		<!-- INICIO TABLA DE EMBARGOS REALIZADOS -->
		<?php if($tercero): ?>  
		<?php 
			$sql = "SELECT aportes018.*,afi.idpersona,afi.pnombre, afi.snombre, afi.papellido, afi.sapellido, afi.idtipodocumento, afi.identificacion FROM aportes018 INNER JOIN aportes015 afi ON aportes018.idtrabajador = afi.idpersona WHERE idtercero = $idp ORDER BY idembargo DESC";
			$resultado = $db->querySimple($sql);
		?>
		<tr>
			<td background="<?php echo URL_PORTAL; ?>imagenes/tabla/izquierda2.jpg">&nbsp;</td>
			<td background="<?php echo URL_PORTAL; ?>imagenes/tabla/centro.gif">
				<h3 class="ui-accordion-header ui-helper-reset ui-state-default ui-corner-all" style="padding: 0.5em 0.5em 0.5em 0.7em; padding-left: 2.2em;" >
		    		Embargos realizados
		    	</h3>				
			</td>
			<td background="<?php echo URL_PORTAL; ?>imagenes/tabla/derecha.gif">&nbsp;</td>
		</tr>
		<tr>
			<td background="<?php echo URL_PORTAL; ?>imagenes/tabla/izquierda2.jpg">&nbsp;</td>
			<td align="center" background="<?php echo URL_PORTAL; ?>imagenes/tabla/centro.gif">
				<table width="95%" border="0" cellspacing="0" class="tablero" id="tabTableGrupo">
					<tr>
						<th align="center">Afiliado</th>
						<th align="center">Estado emb.</th>
						<th align="center">Fecha emb.</th>
						<th align="center">Per. inicio</th>
						<th align="center">Cod. pago</th>
					</tr>
					<?php while($embargo = $resultado->fetch()): ?>      
					<tr>
						<td><?php echo $embargo['identificacion']; ?> - <?php echo "{$embargo["papellido"]} {$embargo["sapellido"]} {$embargo["pnombre"]} {$embargo["snombre"]}"; ?></td>
						<td align="center"><?php echo $embargo['estado']; ?></td>
						<td align="center"><?php echo $embargo['fechaembargo']; ?></td>
						<td align="center"><?php echo $embargo['periodoinicio']; ?></td>
						<td align="center"><?php echo $embargo['codigopago']; ?></td>
					</tr>
					<?php endwhile; ?>
				</table>
			</td>
			<td background="<?php echo URL_PORTAL; ?>imagenes/tabla/derecha.gif">&nbsp;</td>
		</tr>  
		<?php endif; ?>
  		<tr>
    		<td height="38" background="<?php echo URL_PORTAL; ?>imagenes/tabla/abajo_izq2.gif">&nbsp;</td>
    		<td background="<?php echo URL_PORTAL; ?>imagenes/tabla/abajo_central.gif"></td>
    		<td background="<?php echo URL_PORTAL; ?>imagenes/tabla/abajo_der.gif">&nbsp;</td>
		</tr>
	</table>
</center>

<!-- DIV EMPRESA -->
<div id="div-datosEmpresa" title="::DATOS DE EMPRESA::" style="display:none">	
	<table width="100%" class="tablero" >
		<tr>
			<td>Sector</td>
			<td id="sector" colspan="2"></td>
			<td>Clase de Sociedad</td>
			<td id="clasesoci"></td>
		</tr>
		<tr>
			<td width="15%"><label for="iden">NIT</label></td>
			<td width="15%" id="nit1"><label class="box1" ></label></td>
			<td width="14%" id="digito1">D&iacute;gito <label ></label></td>
			<td width="12%">Tipo Documento</td>
			<td width="30%" id="tipo_doc"><label></label></td>			
		</tr>
		<tr>
			<td>Raz&oacute;n Social</td>
			<td id="razonsocial" colspan="3"><label ></label></td>
			<td id="codigosucursal">C&oacute;digo Sucursal - <label ></label></td>
		</tr>
		<tr>
			<td>Nombre Comercial</td>
			<td id="sigla" colspan="4"><label ></label></td>
		</tr>
		<tr>
			<td>Direcci&oacute;n</td>
			<td id="direccion" colspan="4"><label ></label></td>
		</tr>
		<tr>
			<td>Departamento</td>
			<td id="departmento" colspan="2"></td>
			<td>Ciudad</td>
			<td id="municipio"><label ></label></td>
		</tr>
		<tr>
			<td>Tel&eacute;fono</td>
			<td id="telefono" colspan="2"><label ></label></td>
			<td>Fax</td>
			<td id="fax"><label ></label></td>
		</tr>
		<tr>
			<td>URL</td>
			<td id="url" colspan="2"><label ></label></td>
			<td>Email</td>
			<td id="email"><label ></label></td>
		</tr>
		<tr>
			<td> Representante Legal</td>
			<td id="repLeg" colspan="4"></td>
		</tr>
		<tr>
			<td>Contacto</td>
			<td id="contacto" colspan="4"></td>
		</tr>
		<tr>
  			<td>Contratista</td>
  			<td id="contratista" colspan="2"><label ></label></td>
  			<td>Actividad</td>
  			<td id="actividad"></td>
		</tr>
		<tr>
  			<td>Actividad Econ&oacute;mica DANE</td>
  			<td id="actDane" colspan="4"></td>
		</tr>
		<tr>
  			<td>&Iacute;ndice Aporte</td>
  			<td id="indicador" colspan="2"><label></label></td>
  			<td>Asesor</td>
  			<td id="idasesor"><label></label></td>
		</tr>
		<tr>
			<td>Seccional</td>
			<td id="seccional" colspan="2"><label ></label></td>
			<td>Estado</td>
			<td id="estado"><label ></label></td>
		</tr>
		<tr>
			<td>Clase Aportante</td>
			<td id="clase_apo" colspan="2"></td>
			<td>Tipo Aportante</td>
			<td id="tipo_apo"></td>
		</tr>
		<tr>
			<td>Fecha Afiliaci&oacute;n</td>
			<td id="fechaafiliacion" colspan="2"><label ></label></td>
			<td>Fecha Estado</td>
			<td id="fechaestado"><label ></label></td>
		</tr>
		<tr>
  			<td>Fecha Constituci&oacute;n</td>
  			<td id="fechamatricula" colspan="2"></td>
  			<td>Fecha Inicio Aportes</td>
  			<td id="fechaaportes"><label ></label></td>
		</tr>
		<tr>
			<td>Usuario</td>
			<td id="usuario" colspan="2"><label ></label></td>
			<td>Fecha Grabación</td>
			<td id="fechasistema"></td>
		</tr>
		<tr>
			<td>Observaciones</td>
			<td id="observas" colspan="4"></td>
		</tr>
	</table>
</div>

<div id="div-datosCertificados" title="::HISTORIAL DE CERTIFICADOS::" style="display:none">
	<table width="100%" class="tablero" >
		<thead>
			<tr>
				<th>Id Cert.</th>
				<th>Tipo</th>
				<th>Peri&oacute;do Inicial</th>
				<th>Peri&oacute;do Final</th>
				<th>Fecha Presentaci&oacute;n</th>
			</tr>
		</thead>
		<tbody></tbody>
	</table>
</div>
<input type="hidden" id="idEmpresaConsultaActiva"/>
</body>
<script>
  var flag=<?php echo "'".$flagServicios."'";?>;
  if(flag==1){
	  alert("Afiliado se paso a servicios por: \nDISCORDANCIA EN LAS RELACIONES");
  } else if(flag==2){
	  alert("Afiliado se paso a servicios por: \nTENER HIJASTROS COMO HIJOS");
  }
</script>
</html>