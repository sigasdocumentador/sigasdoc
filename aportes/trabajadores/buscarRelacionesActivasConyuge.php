<?php
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $_SESSION['RAIZ'] . DIRECTORY_SEPARATOR . 'clases/grupo.familiar.class.php';

$campo0=$_REQUEST['v0'];		//idbeneficiario

$conr=0;
$data = array();
$objClase=new GrupoFamiliar;
$consulta = $objClase->buscarRelacionesActivasConyuge($campo0);

while($row=mssql_fetch_array($consulta)){
	$data[]=array_map("utf8_encode",$row);
	$conr++;
}

if($conr>0){
	echo json_encode($data);
}
else {
	echo 0;
}
?>