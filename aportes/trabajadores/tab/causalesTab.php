<?php
/* autor:       orlando puentes
 * fecha:       30/08/2010
 * objetivo:     
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;


$idpersona= $_REQUEST['v0'];
include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'p.subsidios.class.php';
$objClase=new Subsidios();
$consulta = $objClase->buscar_causales($idpersona);
$cont=0;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Documento sin título</title>
<script type="text/javascript" src="../../js/script.js"></script>
<script type="text/javascript">
  var sorter = new TINY.table.sorter("sorter");
	sorter.head = "head";
	sorter.asc = "asc";
	sorter.desc = "desc";
	sorter.even = "evenrow";
	sorter.odd = "oddrow";
	sorter.evensel = "evenselected";
	sorter.oddsel = "oddselected";
	sorter.paginate = true;
	sorter.pagesize = (5);
	sorter.currentid = "currentpage5";
	sorter.limitid = "pagelimit5";
	sorter.init("table5",1);
  </script>
</head>

<body>
<h4>Causales de no giro</h4>
<table width="100%" border="0" cellspacing="0"  class="tablero"  id="table5">
<thead>
<tr>
<th>Periodo</th>
<th >Beneficiario</th>
<th >Causal</th>
</tr>
</thead>
<tbody>
<?php 
while($row=mssql_fetch_array($consulta)){
$nombres=$row['pnombre']." ".$row['snombre']." ".$row['papellido']." ".$row['sapellido'];
?>
<tr>
	<td><?php echo $row['periodo']; ?></td>
    <td><?php echo $nombres; ?></td>
    <td><?php echo $row['caual']; ?></td>
  </tr>
  <?php }?>
  </tbody>
</table>  
<div id="controls">
		<div id="perpage">
			<select onChange="sorter.size(this.value)">
			<option value="5" selected="selected">5</option>
				<option value="10" >10</option>
				<option value="20">20</option>
				<option value="50">50</option>
				<option value="100">100</option>
			</select>
			<label>Registros Por P&aacute;gina</label>
		</div>
		<div id="navigation">
			<img src="../../imagenes/imagesSorter/first.gif" width="16" height="16" alt="first Page" onClick="sorter.move(-1,true)" />
			<img src="../../imagenes/imagesSorter/previous.gif" width="16" height="16" alt="first Page" onClick="sorter.move(-1)" />
			<img src="../../imagenes/imagesSorter/next.gif" width="16" height="16" alt="first Page" onClick="sorter.move(1)" />
			<img src="../../imagenes/imagesSorter/last.gif" width="16" height="16" alt="Last Page" onClick="sorter.move(1,true)" />
		</div>
		<div id="text">P&aacute;gina <label id="currentpage5"></label> de <label id="pagelimit5"></label></div>
	</div>  
</body>
</html>