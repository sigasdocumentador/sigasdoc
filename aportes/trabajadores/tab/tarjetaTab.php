<?php
/* autor:       orlando puentes
 * fecha:       30/08/2010
 * objetivo:     
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;


$idpersona= $_REQUEST['v0'];
include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTOR_SEPARATOR . 'p.tarjeta.class.php';
$objClase=new Tarjeta();
$consulta = $objClase->buscar_tarjeta($idpersona);
$row=mssql_fetch_array($consulta);
$bono=$row['bono'];
$cont=0;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Tarjeta</title>
<link href="../../css/Estilos.css" rel="stylesheet" type="text/css" />
<link href="../../css/estilo_tablas.css" rel="stylesheet" type="text/css" />
<link type="text/css" href="../../css/formularios/base/ui.all.css" rel="stylesheet" />

<script type="text/javascript" src="../../js/formularios/ui/ui.core.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.dialog.js"></script>
<script type="text/javascript" src="../../js/effects.Jquery.js"></script>
<script type="text/javascript" src="../../js/jquery.tablesorter.js"></script>
<script type="text/javascript" src="../../js/jquery.tablesorter.pager.js"></script>
<script type="text/javascript" src="../../js/comunes.js"></script>


<style type="text/css">
.sortable {border:1px solid #eaeaea;margin:10px auto}
.sortable th {background:#f3f3f3; text-align:left; color:#666; border:1px solid #eaeaea; border-right:none; padding:4px}
.sortable th h3 {padding:0 18px; margin:0; font:11px Verdana, Geneva, sans-serif}
.sortable td {padding:6px 6px 6px 10px;border:1px solid #eaeaea; font-size:10px; text-align:left}
.sortable .head h3 {background:url(../../imagenes/imagesSorter/sort.gif) 7px center no-repeat; cursor:pointer; padding:0 18x}
.sortable .desc h3 {background:url(../../imagenes/imagesSorter/desc.gif) 7px center no-repeat; cursor:pointer; padding:0 18px}
.sortable .asc h3 {background:url(../../imagenes/imagesSorter/asc.gif) 7px  center no-repeat; cursor:pointer; padding:0 18px}
.sortable .head:hover, .sortable .desc:hover, .sortable .asc:hover {color:#000}
.sortable .evenrow td {background:#ecf2f6}
.sortable .oddrow td {background:#fff}
.sortable td.evenselected {background:#F8F8F8}
.sortable td.oddselected {background:#ffffff}

#controls { margin:0 auto; height:20px; /*width:800px;*/ overflow:hidden;}
#perpage {float:left; width:200px; margin-left:40px;}
#perpage select {float:left; font-size:11px}
#perpage label {float:left; margin:2px 0 0 5px; font-size:10px}
#navigation {float:left;/* width:600px;*/ text-align:center}
#navigation img {cursor:pointer}
#text {float:left; width:200px; text-align:left; margin-first:2px; font-size:10px}
.pager{ margin:10px;}
</style>
<script>
$(document).ready(function() {
	$("#cargues #historico").tableSorter({
		sortColumn: 'periodo',
		sortClassAsc: 'headerSortUp',
		sortClassDesc: 'headerSortDown',
		headerClass: 'header',	
		dateFormat: 'mm/dd/yyyy'
			
	});
});
</script>
  
</head>

<body>

 <h4>Tarjeta - Saldo - Cargues - Movimientos</h4>

<center>
<table width="60%" border="0" style="border:1px dashed #CCC" >
<tr>
<td width="65%"><label style="font-size:16px; color:#333; font-weight:bold">Valor disponible para retiro</label></td>
<td width="32%" style="text-align:right"><label style="font-size:16px; color:#F00; font-weight:bold;">
<?php echo number_format( $row['saldo'] ); ?></label></td>
</tr>
<tr>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
</tr>
<tr>
  <td><label style="font-size:16px; color:#333; font-weight:bold">N�mero de TARJETA</label></td>
  <td style="text-align:right"><label style="font-size:16px; color:#F00; font-weight:bold">
  <?php echo $row['bono']; ?>&nbsp;</label></td>
</tr>
</table>
<label class="Rojo">Cargues a la Tarjeta</label>
</center>
<br />
<table width="100%" border="0" cellspacing="0" class="sortable hover" id="cargues">
<caption style="text-align:left; padding:5px; font-size:11px;"><span></span></caption>
  <thead>
  <tr>
<th class="head"><h3>Periodo</h3></th>
<th class="head"><h3>Tipo</h3></th>
<th class="head"><h3>Autorizado</h3></th>
<th class="head"><h3>Conyuge</h3></th>
<th class="head"><h3>Valor</h3></th>
<th class="head"><h3>Proc</h3></th>
<th class="head"><h3>Fecha</h3></th>
</tr>
</thead>
<tbody>
<?php 
$consulta = $objClase->buscar_cargues($idpersona);
while($row=mssql_fetch_array($consulta)){
?>
<tr>
    <td><?php echo $row['periodogiro']; ?></td>
    <td><?php echo $row['tipopago']; ?></td>
    <td><?php echo $row['idpersona']; ?></td>
    <td><?php echo $row['idconyuge']; ?></td>
    <td style="text-align:right"><?php echo number_format($row['valor']); ?></td>
    <td><?php echo $row['procesado']; ?></td>
    <td><?php echo $row['fechasistema']; ?></td>
  </tr>
  <?php }?>
  </tbody>
</table>
<div id="controls">
<div id="perpage">
<select onChange="sorter.size(this.value)">
	<option value="5" selected="selected">5</option>
	<option value="10" >10</option>
	<option value="20">20</option>
	<option value="50">50</option>
	<option value="100">100</option>
</select>
<label>Registros Por P&aacute;gina</label>
</div>
<div id="navigation">
<img src="../../imagenes/imagesSorter/first.gif" width="16" height="16" alt="first Page" onClick="sorter.move(-1,true)" />
<img src="../../imagenes/imagesSorter/previous.gif" width="16" height="16" alt="first Page" onClick="sorter.move(-1)" />
<img src="../../imagenes/imagesSorter/next.gif" width="16" height="16" alt="first Page" onClick="sorter.move(1)" />
<img src="../../imagenes/imagesSorter/last.gif" width="16" height="16" alt="Last Page" onClick="sorter.move(1,true)" />
</div>
<div id="text">P&aacute;gina <label id="currentpage4"></label> de <label id="pagelimit4"></label></div>
</div>  
<center>
<label class="Rojo">Movimientos de la Tarjeta</label>
</center>
<table width="100%" border="0" cellspacing="0" class="tablero" id="table5">
  <thead>
  <tr>

<th>Item</th>
<th>Fecha</th>
<th>Tipo</th>
<th>Valor</th>
<th>Fuente</th>
</tr>
</thead>
<tbody>
<?php 
$consulta = $objClase->buscar_movimientos($bono);
while($row=mssql_fetch_array($consulta)){
?>
<tr>
    <td><?php echo $row['item']; ?></td>
    <td><?php echo $row['fechamovimiento']; ?></td>
    <td><?php echo $row['tipomovimiento']; ?></td>
    <td style="text-align:right"><?php echo number_format($row['valormovimiento']); ?></td>
    <td><?php echo $row['comercio']; ?></td>
  </tr>
  <?php }?>
  </tbody>
</table>


<div id="pager" class="pager" style="margin-first:0px; padding:7px;">
<div id="perpage">
	<select class="pagesize">
			<option selected="selected"  value="10">10</option>
			<option value="20">20</option>
			<option value="30">30</option>
			<option  value="40">40</option>
		</select>
        <label>Registros Por P&aacute;gina</label>
        </div>
        <div id="navigation">
		<img src="../../imagenes/imagesSorter/first.gif" class="first"/>
		<img src="../../imagenes/imagesSorter/previous.gif" class="prev"/>
        <img src="../../imagenes/imagesSorter/next.gif" class="next"/>
		<img src="../../imagenes/imagesSorter/last.gif" class="last"/>&nbsp; 
        <label>P&aacute;gina</label>
		<input type="text" class="pagedisplay" readonly="readonly" style="border:none; font-size:10px; margin-first:0; padding:0"/>

        </div>
        
	</div>
   
</body>
</html>