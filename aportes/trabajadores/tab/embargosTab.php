<?php
/* autor:       orlando puentes
 * fecha:       30/08/2010
 * objetivo:     
 */
 
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;


$idtrabajador= $_REQUEST['v0'];
include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'p.embargos.class.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'p.pignoracion.class.php';
$objClase=new Embargos;
$consulta = $objClase->mostrar_registro($idtrabajador);
$objPignoracion=new Pignoracion;
$pignoracion=$objPignoracion->buscar_pignoracion($idtrabajador);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Documento sin título</title>
 <script type="text/javascript" src="../../js/script.js"></script>
<script type="text/javascript">
  var sorter = new TINY.table.sorter("sorter");
	sorter.head = "head";
	sorter.asc = "asc";
	sorter.desc = "desc";
	sorter.even = "evenrow";
	sorter.odd = "oddrow";
	sorter.evensel = "evenselected";
	sorter.oddsel = "oddselected";
	sorter.paginate = true;
	sorter.pagesize = (5);
	sorter.currentid = "currentpage";
	sorter.limitid = "pagelimit";
	sorter.init("table",1);
  </script>
</head>

<body>
<label class="Rojo">Embargos y Pignoraciones</label><br /><br />
EMBARGOS
<br />
<?php 
$cont=0;
while($row=mssql_fetch_array($consulta)){
$conyuge=$row['pnombre']." ".$row['snombre']." ".$row['papellido']." ".$row['sapellido'];
$embarga=$row['pne']." ".$row['sne']." ".$row['pae']." ".$row['sae'];
$estado=$row['estado']=='A'?'Activo':'Inactivo';
$cont++;
?>
<table width="100%" border="0" cellspacing="0" class="tablero">
  <tr>
    <td width="20%">Embargante</td>
    <td colspan="3"><?php echo $row['idene']; ?></td>
  </tr>
  <tr>
    <td width="20%">Nombre Embarga</td>
    <td colspan="3"><?php echo $embarga; ?></td>
  </tr>
  <tr>
    <td>Motivo</td>
    <td colspan="3"><?php echo $row['notas']; ?></td>
  </tr>
  <tr>
    <td>C&oacute;nyuge</td>
    <td colspan="3"><?php echo $conyuge ?></td>
  </tr>
  <tr>
    <td width="20%">Fecha Embargo</td>
    <td width="30%"><?php echo $row['fechaembargo']; ?></td>
    <td width="20%">Tipo Pago</td>
    <td width="30%"><?php echo $row['tipopago']; ?></td>
  </tr>
  <tr>
    <td width="20%">Estado</td>
    <td width="30%"><?php echo $estado; ?></td>
    <td width="20%">Fecha Estado</td>
    <td width="30%"><?php echo $row['fechaestado']; ?></td>
  </tr>
  <tr>
    <td width="20%">Motivo Estado</td>
    <td colspan="3"><?php echo $row['motivoestado']; ?></td>
  </tr>
  <tr>
    <td>Cuenta</td>
    <td><?php echo $row['cuenta']; ?></td>
    <td>Banco</td>
    <td><?php echo $row['idbanco']; ?></td>
  </tr>
</table>
<br />
<label class="Rojo">Subsidio Familiar Recibido</label><br />
<table width="90%" border="0" cellspacing="0" class="tablero"  id="table"  align="left">
<thead>
  <tr>
    <th>Periodo</th>
    <th>PerGiro</th>
    <th>TipoPago</th>
    <th>Fecha</th>
    <th>ValorLor</th>
  </tr>
  </thead>
  <tbody>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  </tbody>
</table>

<div id="controls" style="clear:left">
		<div id="perpage" style="float:left">
			<select onChange="sorter.size(this.value)">
			<option value="5" selected="selected">5</option>
				<option value="10" >10</option>
				<option value="20">20</option>
				<option value="50">50</option>
				<option value="100">100</option>
			</select>
			<label>Registros Por P&aacute;gina</label>
		</div>
		<div id="navigation">
			<img src="../../imagenes/imagesSorter/first.gif" width="16" height="16" alt="first Page" onClick="sorter.move(-1,true)" />
			<img src="../../imagenes/imagesSorter/previous.gif" width="16" height="16" alt="first Page" onClick="sorter.move(-1)" />
			<img src="../../imagenes/imagesSorter/next.gif" width="16" height="16" alt="first Page" onClick="sorter.move(1)" />
			<img src="../../imagenes/imagesSorter/last.gif" width="16" height="16" alt="Last Page" onClick="sorter.move(1,true)" />
		</div>
		<div id="text">P&aacute;gina <label id="currentpage"></label> de <label id="pagelimit"></label></div>
</div>


<?php 
}
if($cont==0){
	echo "<p class=Rojo>El trabajor no tiene Embargos!</p>";
}
?>
<br />
PIGNORACIONES
<?php 
$cont=0;
while($row=mssql_fetch_array($pignoracion)){
	$cont++;
	$idpig=$row['idpignoracion'];
?>
<table width="100%" border="0" cellspacing="0" class="tablero">
<tr>
<td width="25%">N�mero</td>
    <td width="25%"><?php echo $row['idpignoracion']; ?></td>
    <td width="25%">Tipo pago</td>
    <td width="25%"><?php echo $row['tipopago']; ?></td>
  </tr>
<tr>
  <td>Valor</td>
  <td><?php echo number_format($row['valorpignorado']); ?></td>
  <td>Saldo</td>
  <td><?php echo number_format($row['saldo']); ?></td>
</tr>
<tr>
  <td>Fecha pignoraci&oacute;n</td>
  <td><?php echo $row['fechapignoracion']; ?></td>
  <td>Estado</td>
  <td><?php echo $row['estado']; ?></td>
</tr>
<tr>
  <td>Factura</td>
  <td><?php echo $row['facturanumero']; ?></td>
  <td>Pagare</td>
  <td><?php echo $row['pagare']; ?></td>
</tr>
<tr>
  <td>Convenio</td>
  <td><?php echo $row['detalledefinicion']; ?></td>
  <td>A&ntilde;o</td>
  <td><?php echo $row['anno']; ?></td>
</tr>
<tr>
  <td>Observaciones</td>
  <td colspan="3"><?php echo $row['observaciones']; ?></td>
  </tr>
<tr>
  <td>Anulado</td>
  <td><?php echo $row['anulado']; ?></td>
  <td>Fecha anulaci&oacute;n</td>
  <td><?php echo $row['fechaanula']; ?></td>
</tr>
<tr>
  <td>Motivo</td>
  <td colspan="3"><?php echo $row['motivo']; ?></td>
  </tr>
</table>
<center>
<label class="Rojo">Abonos</label>
</center>
<?php 
$objAbonos = new Pignoracion;
$abonos=$objAbonos->buscar_detalle($idpig);
?> 
<table  width="100%" border="0" id="table" cellspacing="0" class="tablero">
<thead>
  <tr>
    <th>Periodo</th>
    <th>Proceso</th>
    <th>Beneficiario</th>
    <th>Fecha</th>
    <th>Valor</th>
  </tr>
  </thead>
  <tbody>
<?php 
while($r_abonos = mssql_fetch_array($abonos)){ 
$nom=$r_abonos['pnombre']." ".$r_abonos['snombre']." ".$r_abonos['papellido']." ".$r_abonos['sapellido'];
?>  
  <tr>
    <td><?php echo $r_abonos['periodo']; ?></td>
    <td><?php echo $r_abonos['periodoproceso']; ?></td>
    <td><?php echo $nom; ?></td>
    <td><?php echo $r_abonos['fechasistema']; ?></td>
    <td style="text-align:right"><?php echo number_format($r_abonos['valor']); ?></td>
  </tr>
<?php }?>
  
  </tbody>
</table><br />
<div id="controls">
		<div id="perpage">
			<select onChange="sorter.size(this.value)">
			<option value="5" selected="selected">5</option>
				<option value="10" >10</option>
				<option value="20">20</option>
				<option value="50">50</option>
				<option value="100">100</option>
			</select>
			<label>Registros Por P&aacute;gina</label>
		</div>
		<div id="navigation">
			<img src="../../imagenes/imagesSorter/first.gif" width="16" height="16" alt="first Page" onClick="sorter.move(-1,true)" />
			<img src="../../imagenes/imagesSorter/previous.gif" width="16" height="16" alt="first Page" onClick="sorter.move(-1)" />
			<img src="../../imagenes/imagesSorter/next.gif" width="16" height="16" alt="first Page" onClick="sorter.move(1)" />
			<img src="../../imagenes/imagesSorter/last.gif" width="16" height="16" alt="Last Page" onClick="sorter.move(1,true)" />
		</div>
		<div id="text">P&aacute;gina <label id="currentpage"></label> de <label id="pagelimit"></label></div>
</div>

<?php }
if($cont==0){
	echo "<p class=Rojo>El trabajor no tiene Pignoraciones!</p>";
}
?>
  
</body>
</html>