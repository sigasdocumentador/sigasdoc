<?php
/* autor:       orlando puentes
 * fecha:       12/07/2010
 * objetivo:     
 */
 
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;


$idpersona= $_REQUEST['v0'];
include_once $raiz . DIRECTORY_SEPARATOR . 'clases' .  DIRECTORY_SEPARATOR . 'p.afiliacion.class.php';
$objClase=new Afiliacion;
$consulta = $objClase->buscar_afiliacion($idpersona);
$row=mssql_fetch_array($consulta);
$nombre=$row['pnombre']." ".$row['snombre']." ".$row['papellido']." ".$row['sapellido'];
$estado=$row['estado']=='A'?'Activo':'Inactivo';
$primaria=$row['primaria']=='S'?'SI':'N0';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Afiliacion</title>
</head>

<body>
<h3>Ultima Afiliaci&oacute;n</h3>
<br>
<table width="90%" border="0" cellspacing="0" class="tablero">
  <tr>
    <td width="20%">Trabajador</td>
    <td colspan="3"><?php echo $nombre; ?>&nbsp;</td>
  </tr>
  <tr>
    <td width="20%">Empresa</td>
    <td colspan="3"><?php echo $row['razonsocial']; ?>&nbsp;</td>
  </tr>
  <tr>
    <td width="20%">Fecha Ingreso</td>
    <td width="30%"><?php echo $row['fechaingreso']; ?>&nbsp;</td>
    <td width="20%">Estado</td>
    <td width="30%"><?php echo $estado; ?>&nbsp;</td>
  </tr>
  <tr>
    <td width="20%">Tipo Afiliaci&oacute;n</td>
    <td width="30%"><?php echo $row['tipof']; ?>&nbsp;</td>
    <td width="20%">Clase  Afiliaci&oacute;n</td>
    <td width="30%"><?php echo $row['clasef']; ?>&nbsp;</td>
  </tr>
  <tr>
    <td width="20%">Horas mes</td>
    <td width="30%"><?php echo $row['horasmes']; ?>&nbsp;</td>
    <td width="20%">Salario</td>
    <td width="30%"><?php echo number_format($row['salario']); ?>&nbsp;</td>
  </tr>
  <tr>
    <td>Primaria</td>
    <td><?php echo $primaria; ?>&nbsp;</td>
    <td>Sector Agro</td>
    <td><?php echo $row['agricola']; ?>&nbsp;</td>
  </tr>
  <tr>
    <td>Fecha Retiro</td>
    <td><?php echo $row['fecharetiro']; ?>&nbsp;</td>
    <td>Motivo Retiro</td>
    <td><?php echo $row['motivoretiro']; ?>&nbsp;</td>
  </tr>
</table>
</body>
</html>