<?php
/* autor:       orlando puentes
 * fecha:       22/06/2010
 * objetivo:     
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;



?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Documento sin título</title>
<link type="text/css" href="../../../css/formularios/base/ui.all.css" rel="stylesheet" />
<link type="text/css" href="../../../css/Estilos.css" rel="stylesheet"/>
<link type="text/css" href="../../../css/estilo_tablas.css" rel="stylesheet"/>
</head>

<body>
<h4 align="left"> DATOS DEL CONYUGE ...</h4>
<table width="100%"  border="0" cellspacing="0" class="tablero">
<tr>
<td>Tipo de relaci&oacute;n</td>
<td><select name="tipRel" class="box1" id="tipRel">
<option selected="selected">Seleccione..</option>
<option value="0">CONYUGE</option>
<option value="1">COMPA&Ntilde;ERO(A)</option>
</select><img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
<td>Conviven</td>
<td><select name="conviven" class="box1" id="conviven">
<option>Seleccione..</option>
<option value="S">SI</option>
<option value="N">NO</option>
</select><img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
</tr>
<tr>
<td width="163">Tipo Documento</td>
<td width="225">
<select name="tipoDoc2" class="box1" id="tipoDoc2">
<option>Seleccione..</option>
<?php
	$consulta=$objClase->mostrar_datos(1, 2);
	while($row=mssql_fetch_array($consulta)){
		echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
	}
?>
</select><img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
<td width="106">Identificaci&oacute;n</td>
<td width="448"><input name="identificacion2" type="text" class="box1" id="identificacion2" maxlength="11" />
<img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
</tr>         
<tr>
<td>Primer Nombre</td>
<td class="box1">
<input name="pNombre2" type="text" class="box1" id="pNombre2" />
<img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
<td>Segundo Nombre</td>
<td><input name="sNombre2" type="text" class="box1" id="sNombre2" /></td>
</tr>
<tr>
<td>Primer Apellido</td>
<td><input name="pApellido2" type="text" class="box1" id="pApellido2" />
<img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
<td>Segundo Apellido</td>
<td><input name="sApellido2" type="text" class="box1" id="sApellido2" /></td>
</tr>
<tr>
<td>Sexo</td>
<td colspan="3"><select name="sexo2" class="box1" id="sexo2">
<option selected="selected">Seleccione..</option>
<option value="M">Masculino</option>
<option value="F">Femenino</option>
</select><img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
</tr>
<tr>
  <td>Departamento Residencia</td>
  <td><select name="cboDeptoC" class="box1" id="cboDeptoC">
    <option>Seleccione..</option>
    <?php
	$consulta=$objCiudad->departamentos();
	while($row=mssql_fetch_array($consulta)){
	?>
    <option value="<?php echo $row["coddepartamento"]; ?>"><?php echo $row["departmento"]?></option>
    <?php
	}
?>
  </select>
    <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
  <td>Ciudad Residencia</td>
  <td><select name="cboCiudadC" class="box1" id="cboCiudadC">
    <option>Seleccione..</option>
    <option>ejemplo</option>
  </select>
    <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
</tr>
<tr>
  <td>Zona</td>
  <td><select name="cboZonaC" class="box1" id="cboZonaC">
    <option>Seleccione..</option>
    <option>ejemplo</option>
  </select>
    <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
  <td>Direcci&oacute;n</td>
  <td><input name="direccion2" type="text" class="box1" id="direccion2" onfocus="direccion(this);" /></td>
</tr>
<tr>
<td>Barrio</td>
<td><select name="cboBarrioC" class="box1" id="cboBarrioC">
  <option>Seleccione..</option>
  <?php
	$consulta=$objClase->mostrar_datos(19, 4);
	while($row=mssql_fetch_array($consulta)){
		echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
	}
?>
</select>  <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>Tel&eacute;fono</td>
<td><input name="telefono2" type="text" class="box1" id="telefono2" maxlength="7" /></td>
<td>Celular</td>
<td><input name="celular2" type="text" class="box1" id="celular2" maxlength="10" /></td>
</tr>
<tr>
<td>E-mail</td>
<td colspan="3"><input name="email2" type="text" class="boxlargo" id="email2" /></td>
</tr>
<tr>
<td>Tipo Vivienda</td>
<td><select name="tipoVivienda2" class="box1" id="tipoVivienda2">
<option>Seleccione..</option>
<option value="U">Urbana</option>
<option value="R">Rural</option>
</select></td>
<td>&nbsp;</td>
<td><img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
</tr>
<tr>
<td>&nbsp;</td>
<td><img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
<td>&nbsp;</td>
<td><img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
    </tr>
          <tr>
            <td>Estado Civil</td>
            <td><select name="estadoCivil2" class="box1" id="estadoCivil2">
              <option>Seleccione..</option>
              <option>ejemplo</option>
            </select></td>
            <td>Fecha De Nacimiento</td>
            <td><input name="fecNac2" type="text" class="box1" id="fecNac2" value="mmddaaaa" maxlength="8" />
              <img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
            <input type="hidden" name="fecNacHide2" id="fecNacHide2" /></td>
    </tr>
          <tr>
            <td>Departamento Nacimiento</td>
            <td><select name="cboDeptoC2" class="box1" id="cboDeptoC2">
              <option>Seleccione..</option>
              <option>ejemplo</option>
            </select></td>
            <td>Ciudad Nacimiento</td>
            <td><select name="cboCiudadC2" class="box1" id="cboCiudadC2">
              <option>Seleccione..</option>
              <option>ejemplo</option>
            </select></td>
    </tr>
          <tr>
            <td>Capacidad Trabajo</td>
            <td><select name="capTrabajo2" class="box1" id="capTrabajo2">
              <option selected="selected">Seleccione..</option>
              <option>S&Iacute;</option>
              <option>NO</option>
            </select></td>
            <td>Profesi&oacute;n</td>
            <td><select name="profesion2" class="boxmediano" id="profesion2">
              <option>Seleccione..</option>
              <option>Otro..</option>
            </select></td>
    </tr>
          <tr>
            <td height="25" colspan="4">&nbsp;</td>
    </tr>
          <tr>
            <td>Nit</td>
            <td><input name="nitConyuge" type="text" class="box1" id="nitConyuge" readonly="readonly" /></td>
            <td>Empresa donde trabaja</td>
            <td><input name="empCony" type="text" class="box1" id="empCony" readonly="readonly" /></td>
    </tr>
          <tr>
            <td>Salario </td>
            <td><input name="salCony" type="text" class="box1" id="salCony" readonly="readonly" /></td>
            <td>�Subsidio?</td>
            <td><input name="subCony"  type="text" class="box1" id="rutaDoc" readonly="readonly" /></td>
    </tr>
  </table>

</body>
<script>
$("#tipRel").focus();
</script>
</html>
