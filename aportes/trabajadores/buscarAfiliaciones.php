<?php
/**
 * @author Oswaldo Gonzalez
 * @date 23/05/2012
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'p.afiliacion.class.php';
$objClase = new Afiliacion;
$idPersona = $_REQUEST['idpersona'];
$consulta = $objClase->obtener_afiliaciones_activaseinactivas($idPersona);
$conr=0;
$data = array();
while($afiliacion = mssql_fetch_array($consulta)){
	$data[] = array_map("utf8_encode",$afiliacion);
	$conr++;
}

if($conr>0)
	echo json_encode($data);
else
	echo 0;
?>