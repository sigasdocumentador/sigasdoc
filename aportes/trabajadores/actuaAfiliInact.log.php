<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
include_once $raiz.DIRECTORY_SEPARATOR.'aportes'.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'val.empresa.reactivacion.class.php';

include_once $raiz.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'observacion.class.php';

$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}




$usuario = $_SESSION["USUARIO"];

$arrDatos = $_REQUEST["datos"];

$objObservacion = new Observacion();

$data = array("error"=>0,"descripcion"=>"");

$db->inicioTransaccion();
foreach($arrDatos["datos_afiliacion"] as $arrRow){
	
	//Actualizar informacion
	if($arrRow["observacion_modifica"]!=""){
		$query = "UPDATE aportes017 SET 
						tipoformulario={$arrRow["tipo_formulario"]}
						, tipoafiliacion={$arrRow["tipo_afiliacion"]}
						, fechaingreso= '{$arrRow["fecha_ingreso"]}'
						, fecharetiro= '{$arrRow["fecha_retiro"]}'
						, cargo={$arrRow["cargo"]}
						, usuario='$usuario'
						, fechasistema=getdate()
					WHERE idformulario={$arrRow["id_formulario"]}";
		
		$rs = $db->queryActualiza($query);
		if(intval($rs)==0){
			$data['error']=1;
			$data['descripcion']=$db->error;
			break;
		}
		
		//Guardar observacion		
		$rsObservacion = $objObservacion->crearObservacion($arrDatos["id_persona"], 1	, $arrRow["observacion_modifica"], $usuario);
		
		if($rsObservacion==0){
			$data['error']=2;
			$data['descripcion']=$db->error;
			break;
		}
	}
	
	if($arrRow["activar"]=="SI"){
		//Activar afiliacion
		
		
		/*
		 *  inicio verificacion de estado activa de la empresa que pertenece el afiliado
		 *  2016-06-01
		 */ 
		 
		
		$objre= new valEmpresasReactivacionAfiliado($arrRow["id_formulario"],$arrDatos["id_persona"]);
		
		/* se consulta el id de la empresa asociada con el afiliado en la tabla aportes017 */
		$objre->consultarIdEmpresa();
		
		if($objre->idEmpresa==''){
			$data['error']= 6;
			$data['descripcion']="No se encontro registro en la tabla aportes017 con numero de formulario $c0";
			break;
		}else{
			// extraemos el estado de la empresa para verificar si esta activa o inactiva
			$rsest=$objre->cosultarEstadoEmpresa($objre->idEmpresa);
			// a traves del idempresa asociado al afiliado tabla aportes017 se busca la empresa y se valida si existe
			if($rsest==1){
				// se valida si el estado de la empresa esta != I (Inactiva) y si se trata de un afiliado dependiente o independiente para continuar
				if(($objre->estado=='I' and $objre->tipoafiliacion==18)){  //si el estado s Inactivo se detiene el proceso de activacion y se notifica.
					$data['error']=9;
					$data['descripcion']= $objre->tablaRes();
					break;
				}
			}
			else{
				$data['error']=7;
				$data['descripcion']= "No se encontro la empresa con id:".$objre->idEmpresa.", reportar al administrador";
				break;
			}
		
		}
		
		
		// fin verificacion activa empresa afiliado
		
		
		$query = "INSERT INTO dbo.aportes016 (tipoformulario, tipoafiliacion, idempresa, idpersona, fechaingreso, horasdia, horasmes, salario, agricola, cargo, primaria, estado, fecharetiro, motivoretiro, fechanovedad, semanas, fechafidelidad, estadofidelidad, traslado, codigocaja, flag, tempo1, tempo2, fechasistema, usuario,    tipopago, categoria, auditado, idagencia, idradicacion, vendedor, codigosalario, flag2, porplanilla, madrecomunitaria, claseafiliacion)
									  SELECT  tipoformulario, tipoafiliacion, idempresa, idpersona, fechaingreso, horasdia, horasmes, salario, agricola, cargo, primaria, 'A'   , NULL       , NULL        , fechanovedad, semanas, NULL          , NULL           , traslado, codigocaja, flag, tempo1, tempo2, getdate()   , '$usuario', tipopago, categoria, auditado, idagencia, idradicacion, NULL    , NULL         , NULL , NULL       , NULL            , NULL
				 					  FROM aportes017
									  WHERE idformulario={$arrRow["id_formulario"]}";
		
		$rs = $db->queryInsert($query,"aportes016");
		if($rs===null){
			$data['error']=3;
			$data['descripcion']=$db->error;
			break;
		}
		
		//Eliminar afiliacion
		$query = "DELETE FROM aportes017 WHERE idformulario={$arrRow["id_formulario"]}";
		
		$rs = $db->queryActualiza($query);
		if($rs===null){
			$data['error']=4;
			$data['descripcion']=$db->error;
			break;
		}
		
		//Actualizar el estado de la empresa
		if(intval($arrRow["tipo_afiliacion"])!=18){ //[18][DEPENDIENTE]
			$query = "UPDATE aportes048 SET 
						estado='A', usuario='$usuario', fechaestado=getdate() 
					  WHERE idempresa=".$arrRow["id_empresa"];
			
			$rs = $db->queryActualiza($query);
			if($rs===null){
				$data['error']=5;
				$data['descripcion']=$db->error;
				break;
			}
		}
		
		//Guardar observacion
		$rsObservacion = $objObservacion->crearObservacion($arrDatos["id_persona"], 1	, $arrRow["observacion_activa"], $usuario);
		
		if($rsObservacion==0){
			$data['error']=6;
			$data['descripcion']=$db->error;
			break;
		}
	}
	
} 

if($data['error']==0){
	$db->confirmarTransaccion();
	echo json_encode($data);
}else{
	$db->cancelarTransaccion();
	echo json_encode($data);
}

?>