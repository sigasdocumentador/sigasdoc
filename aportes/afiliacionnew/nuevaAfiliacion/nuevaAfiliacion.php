<?php
/* autor:       Orlando Puentes
 * fecha:       Marzo 21 de 2013
 * objetivo:    Almacenar en la base de datos Aportes la información de los trabajadores que se van a afiliar a Comfamiliar Huila.
 */
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");

$root= $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'newrsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'SQLDbManejador.php';
$db = SQLDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo $cadena;
	exit();
}
$fecver = date('Ymd h:i:s A',filectime('nuevaAfiliacion.php'));

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="es">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>::Administración Afiliaciones::</title>
<link type="text/css" href="../../../newcss/Estilos.css" rel="stylesheet"/>
<link type="text/css" href="../../../newcss/marco.css" rel="stylesheet"  />
<link type="text/css" href="../../../newcss/estilo_tablas.css" rel="stylesheet"/>
<link type="text/css" href="../../../newcss/css.1.8/jquery-ui-1.8.17.custom.css" rel="stylesheet"/>
<script language="javascript" src="../../../newjs/1.6/jquery-1.6.2.min.js"></script>
<script language="javascript" src="../../../newjs/1.6/jquery-ui-1.8.16.custom.min.js"></script>
<script language="javascript" src="../../../newjs/jquery.combos.js"></script>
<script type="text/javascript" src="../../../newjs/comunes.js"></script>
<script type="text/javascript" src="../../../newjs/jquery.utilitarios.js"></script>
<script type="text/javascript" src="../../../newjs/clases/persona.class.js"></script>
<script type="text/javascript" src="../../../js/direccion.js"></script>
<script type="text/javascript" src="../../../newjs/clases/grupoFamiliar.class.js"></script>

<script type="text/javascript" src="js/nuevaAfiliacion.js"></script>
<script type="text/javascript">
	shortcut.add("Shift+F",function() {
	var URL=src();
	var url=URL+"aportes/trabajadores/consultaTrabajador.php";
    window.open(url,"_blank");
    },{
	'propagate' : true,
	'target' : document 
        });        
</script>

</head>
<style>
#galeria img{ margin:5px; border:1px dashed #CCC; padding:4px; cursor:pointer;}
.box-table-a td{ padding:7px;}
.box-table-a caption{ margin:10px; text-align:left; font:12px Verdana, Geneva, sans-serif bold;}
#tablaC,#tablaB td img{cursor:pointer;}
</style>

<body>
<form name="forma">
<center>
<!-- TABLA VISIBLE CON BOTONES -->
<table width="97%" border="0" cellspacing="0" cellpadding="0" align="center">
<tr>
<td width="13" height="29" class="arriba_iz">&nbsp;</td>
<td class="arriba_ce"><span class="letrablanca">::&nbsp;Administraci&oacute;n de Afiliaciones&nbsp;::</span>
<div style="text-align:right; height:20px; top:4px; width:221px; position:absolute; right: 41px;">
<?php echo 'Versi&oacute;n: ' . $fecver; ?>
</div></td>
<td width="13" class="arriba_de" align="right">&nbsp;</td>
</tr>      
<tr>
 <td class="cuerpo_iz">&nbsp;</td>
 <td class="cuerpo_ce">
 <td class="cuerpo_de">&nbsp;</td>
</tr> 
<tr>     
<td class="cuerpo_iz">&nbsp;</td>
<td class="cuerpo_ce"><div id="error" style="color:#FF0000"></div></td>
<td class="cuerpo_de">&nbsp;</td>
</tr>
<tr>
<td class="cuerpo_iz">&nbsp;</td>
<!-- TABLAS DE FORMULARIO -->
<td class="cuerpo_ce">
<div>
<!-- MENU TABS -->
<div id="tabs">
<ul id="ul">
	<li><a href="#tabs-1" id='t1'>Datos Personales</a></li>	
    <li><a href="#tabs-2" id='t2'>Afiliaci&oacute;n</a></li>
    <li><a href="#tabs-3" id='t3'>Grupo Familiar</a></li>
    <li><a href="#tabs-4" id='t4'>Documentos</a></li>
</ul>

<!-- DATOS PERSONALES -->
<div id="tabs-1">
<center>
<table width="85%" border="0" cellspacing="10" cellpadding="0" class="tablero" >       
<tr>
<td colspan="4" align="center" background="../../../imagenes/tabla/centro.gif"><br />       
<center>
<table width="95%" border="0" cellspacing="0" class="tablero">
<tr>
<td width="17%">Radicaci&oacute;n N&uacute;mero</td>
<td width="35%" valign="baseline"><input name="txtRadicacion" class="box1" id="txtRadicacion" style="font-size:14px;color:#FF0000" />
  <img src="../../../imagenes/menu/iconoBuscar.png" width="66" height="24" onclick="buscarRadicacion();" style="cursor:pointer" /></td>
<td width="14%">Fecha</td>
<td width="34%"><input name="txtFecha" type="text" class="box1" id="txtFecha"  readonly="readonly" /></td>
</tr>
</table>
</center>
<h4 align="left">&nbsp;&nbsp;&nbsp; ACTUALIZACI&Oacute;N DE LA PERSONA</h4>
<center>
<table width="95%" border="0" cellspacing="0" class="tablero" id="tablaAfiTab">
<tr>
<td width="17%" >Tipo Documento</td>
<td width="8" ><select name="tipDocAfi" id="tipDocAfi" class="box1">
<?php 
$id=1;
$orden=3;
$rs=$db->Definiciones($id, $orden);
while ($row=$rs->fetch()){
	echo "<option value=".$row['iddetalledef'].">". $row['detalledefinicion'] ."</option>";
}
?>
</select></td>
<td width="7" >N&uacute;mero Documento</td>
<td width="14"><input name="txtNumero" class="box1" id="txtNumero"  /></td>
</tr>
<tr>
<td>P Apellido</td>
<td><input name="txtPApellido" type="text" class="box1" id="txtPApellido" />
  <img src="../../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
<td>S Apellido</td>
<td ><input name="txtSApellido" type="text" class="box1" id="txtSApellido" /></td>
</tr>
<tr>
  <td>P Nombre</td>
  <td><input name="txtPNombre" type="text" class="box1" id="txtPNombre" />
    <img src="../../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
  <td>S Nombre</td>
  <td ><input name="txtSNombre" type="text" class="box1" id="txtSNombre" /></td>
</tr>

<tr>
  <td>Estado Civil</td>
  <td><select name="cboCivil" id="cboCivil" class="box1">
<option value="0" selected="selected">Seleccione...</option>
  <?php 
$id=10;
$orden=3;
$rs=$db->Definiciones($id, $orden);
while ($row=$rs->fetch()){
	echo "<option value=".$row['iddetalledef'].">". $row['detalledefinicion'] ."</option>";
}
?>  
  </select>
    <img src="../../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
  <td>Fecha Nace</td>
  <td ><input name="txtFechaNace" type="text" class="box1" id="txtFechaNace" />
    <img src="../../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
</tr>
<tr>
  <td>Pais Nace</td>
  <td><select name="estados" class="box1" id="estados">
  </select></td>
  <td>Departamento Nace</td>
  <td ><select name="deptos" class="box1" id="deptos">
  </select>
    <img src="../../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
</tr>
<tr>
  <td>Ciudad Nace</td>
  <td><select name="ciudad" class="box1" id="ciudad">
  </select>
    <img src="../../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
  <td>Genero</td>
  <td ><select name="cboGenero" id="cboGenero" class="box1">
  <option value='0' selected>Seleccione...</option>
  <option value='M'>Masculino</option>
  <option value='F'>Femenino</option>
  </select>
    <img src="../../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
</tr>
<tr>
  <td>Direcci&oacute;n</td>
  <td><input name="txtDireccion" type="text" class="box1" id="txtDireccion" onfocus="direccion(this,document.getElementById('cboDepto'));"/>
    <img src="../../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
  <td>Depto Recide</td>
  <td ><select name="cboDepto" id="cboDepto" class="box1">
  </select>
    <img src="../../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
</tr>
<tr>
  <td>Ciudad Recide</td>
  <td><select name="cboCiudad" id="cboCiudad" class="box1">
  </select>
    <img src="../../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
  <td>Zona</td>
  <td ><select name="cboZona" id="cboZona" class="box1">
  </select>
    <img src="../../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
</tr>
<tr>
  <td>Barrio</td>
  <td><select name="cboBarrio" id="cboBarrio" class="box1">
  </select>
    <img src="../../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
  <td>Tipo Propiedad</td>
  <td ><select name="cboPropiedad" id="cboPropiedad" class="box1">
  <option value='0' selected="selected">Seleccione</option>
  <?php 
$id=42;
$orden=4;
$rs=$db->Definiciones($id, $orden);
while ($row=$rs->fetch()){
	echo "<option value=".$row['iddetalledef'].">". $row['detalledefinicion'] ."</option>";
}
?>     
  </select>
    <img src="../../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
</tr>
<tr>
  <td>Tel&eacute;fono</td>
  <td><input name="txtTelefono" type="text" class="box1" id="txtTelefono" /></td>
  <td>Celular</td>
  <td ><input name="txtCelular" type="text" class="box1" id="txtCelular" /></td>
</tr>
<tr>
  <td>Correo Electr&oacute;nico</td>
  <td colspan="3"><input name="txtCorreo" type="text" class="boxlargo" id="txtCorreo" /></td>
  </tr>
<tr>
  <td>Profesi&oacute;n</td>
  <td><select id="cboProfesion" class="box1">
  <option value='1' selected>Sin Profesion</option>
 <?php 
$id=20;
$orden=4;
$rs=$db->Definiciones($id, $orden);
while ($row=$rs->fetch()){
	echo "<option value=".$row['iddetalledef'].">". $row['detalledefinicion'] ."</option>";
}
?>       
  </select></td>
  <td>&nbsp;</td>
  <td >&nbsp;</td>
</tr>
<tr>
  <td>Nombre Corto</td>
  <td colspan="3"><input name="txtNombreCorto" type="text" class="boxlargo" id="txtNombreCorto" /></td>
  </tr>
</table>
</center>
<div id="div-boton" style="display:none">
<p align="center"><label onClick="actualizarPersona();" class="ui-state-default" style="padding:4px; cursor:pointer;">Actualizar Registro</label></p>
</div>
<p>&nbsp;</p></td>
</tr>
</table>
</center>
</div>

<!-- AFILIACION -->
<div id="tabs-2" align="center">

<table width="85%" border="0" cellspacing="10" cellpadding="0" class="tablero" >       
<tr>
<td colspan="4" align="center" background="../../../imagenes/tabla/centro.gif"><br />       
<h4 align="left">&nbsp;&nbsp;&nbsp;RADICACION NUMERO: <input name="copiaIdR" id="copiaIdR" class="box1" readonly="readonly" /> </h4>           
<h4 align="left">&nbsp;&nbsp;&nbsp;INFORMACION DE LA EMPRESA</h4>
<center>
<table width="95%" border="0" cellspacing="0" class="tablero">
<tr>
<td width="52">Nit</td>
<td width="171"><input name="txtNit" class="box1" id="txtNit"  /></td>
<td width="101">Raz&oacute;n Social</td>
<td width="354"><input name="txtRazon" type="text" class="boxmediano" id="txtRazon"  readonly="readonly" /></td>
<td width="87"></td>
<td width="289"></td>
</tr>
</table>
</center>
<h4 align="left">&nbsp;&nbsp;&nbsp; ACTUALIZACI&Oacute;N DE LA AFILIACI&Oacute;N</h4>
<center>
<table width="95%" border="0" cellspacing="0" class="tablero" id="tablaAfiTab">
<tr>
<td >Afiliado</td>
<td colspan="3" id="tdNombre">&nbsp;</td>
</tr>
<tr>
<td width="17%">Tipo  Formulario</td>
<td width="8">
<select name="txtFormulario" class="box1" id="txtFormulario" >
<option selected="selected" value=0>Seleccione..</option>
<?php 
$id=9;
$orden=3;
$rs=$db->Definiciones($id, $orden);
while ($row=$rs->fetch()){
	echo "<option value=".$row['iddetalledef'].">". $row['detalledefinicion'] ."</option>";
}
?>         
</select>
<img src="../../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
<td width="7">Tipo Afiliaci&oacute;n</td>
<td width="14">
<select name="txtAfiliacion" class="box1" id="txtAfiliacion" onblur="validarSubsidio();" >
<option value=18 selected="selected">DEPENDIENTE</option>
</select><img src="../../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
</tr>
<tr>
<td>Fecha Ingreso</td>
<td><input name="txtIngreso" type="text" class="boxfecha" id="txtIngreso" readonly="readonly"/><img src="../../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
<td>Horas D&iacute;a</td>
<td><input name="txtHorasDia" type="text" class="box1" id="txtHorasDia" value="8"  maxlength="1" onkeydown="solonumeros(this);" onkeyup="solonumeros(this);"/>
<img src="../../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
</tr>
<tr>
<td>Horas Mes</td>
<td><input name="txtHorasMes" type="text" class="box1" id="txtHorasMes" maxlength="3" value="240" onkeydown="solonumeros(this);" onkeyup="solonumeros(this);"/>
<img src="../../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
<td>Tipo Pago</td>
<td><select name="txtTipoPago" class="box1" id="txtTipoPago">
<option value="T" selected="selected" >Tarjeta</option>
</select><img src="../../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
</td>
</tr>
<tr>
<td>Salario</td>
<td><input name="txtSalario" type="text" class="box1" id="txtSalario" onkeydown="solonumeros(this);" onkeyup="solonumeros(this);"/>
<img src="../../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
<td>Categor&iacute;a</td>
<td><input name="txtCategoria" type="text" class="boxfecha" id="txtCategoria" readonly="readonly" /></td>
</tr>
<tr>
<td>Agr&iacute;cola</td>
<td><select name="txtAgricola" class="box1" id="txtAgricola" >
<option value="S">SI</option>
<option value="N" selected="selected">NO</option>
</select><img src="../../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
<td>Cargo</td>
<td><select name="txtCargo" class="box1" id="txtCargo" >
<option value="0" selected="selected">Seleccione..</option>
<?php 
$id=21;
$orden=4;
$rs=$db->Definiciones($id, $orden);
while ($row=$rs->fetch()){
	echo "<option value=".$row['iddetalledef'].">". $row['detalledefinicion'] ."</option>";
}
?>            
<option value="0">Otro</option>
    </select>
      <img src="../../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
      <input type="hidden" name="primaria" id="primaria" /></td>
    </tr>
    <tr>
    <td>Estado</td>
    <td><select name="txtEstado" class="box1" id="txtEstado" >
      <option value="A" selected="selected" >ACTIVO</option>
      <option value="I">INACTIVO</option>
      <option value="P" >PENDIENTE</option>
    </select>  </td>
    <td>Tipo</td>
    <td><select name="txtTipo" class="box1" id="txtTipo" disabled="disabled" >
      <option value="0">Seleccione..</option>
      <option value="S" selected="selected">Primaria</option>
      <option value="N">Adicional</option>
    </select></td>
    </tr>
    <tr>
      <td>Agencia</td>
      <td><select name="txtAgencia" class="box1" id="txtAgencia" >
        <option value="01">Neiva</option>
        <option value="02">Garzon</option>
        <option value="03">Pitalito</option>
        <option value="04">La Plata</option>
      </select></td>
      <td>Madre Comunitaria</td>
      <td><select name="txtMComunitaria" id="txtMComunitaria" class="box1">
      <option value="N" selected="selected">NO</option>
      <option value="S">SI</option>
      </select>
      </td>
    </tr>
</table>
</center>
<div id="div-boton2" style="display:none">
<p align="center"><label onClick="actualizarAfiliacion();" class="ui-state-default" style="padding:4px; cursor:pointer;">Actualizar Registro</label></p>
</div>
<p>&nbsp;</p></td>
</tr>
</table>
</div>

<!----------- GRUPO FAMILIAR ------------------------>
<div id="tabs-3">

<p style="font-weight:bold;cursor:pointer" onclick="nuevaRelacion();">Nueva Relación de Convivencia.</p>
<small><strong>Relaciones de convivencia registradas</strong></small><br />
<table width="95%" border="0" cellspacing="0" class="tablero" id="tabTableRelaciones">
<thead>
<tr>
<th width="10%" align="center">Id</th>
<th width="10%" align="center">N&uacute;mero</th>
<th width="5%" align="center">Conviven</th>
<th width="75%" align="center">Conyugue</th>
</tr>
</thead>
<tbody>

</tbody>     
</table>
<br />
<p style="font-weight:bold;cursor:pointer" onclick="nuevoBeneficiario();">Nuevo Beneficiario</p>
<small><strong>Grupo familiar registrado</strong></small><br />
<table width="95%" border="0" cellspacing="0" class="tablero" id="tabTableGrupo">
	<thead>
		<tr>
			<th>IDB</th>
			<th>T.D</th>
			<th>Numero</th>
			<th>Nombres</th>
			<th>Parentesco</th>
			<th>Relaci&oacute;n</th>
			<th>Fec Nac.</th>
			<th>Edad</th>
			<th>Embargo</th>
			<th>Disc.</th>
			<th>Giro</th>
			<th>F Asigna</th>
			<th>Estado</th>
			<th>F Afilia</th>
			<th>F Certificado</th>
			<th>Modificar</th>
		</tr>
	</thead>
	<tbody>
	
	</tbody>
</table>  
</div>
<div id="tabs-4"></div>

</div>
<div id="errores" align="left"></div>
<td class="cuerpo_de">&nbsp;</td>
</tr>
<tr>
<td class="abajo_iz" >&nbsp;</td>
<td class="abajo_ce" ></td>
<td class="abajo_de" >&nbsp;</td>
</tr>
</table>  
       
<!-- FIN TABLA VISIBLE -->
</center>

<!-- formulario direcciones  --> 
<div id="dialog-form" title="Formulario de direcciones"></div>

<!-------------- crelaciones de convivencia ------>
<div id="div-conyuge" title=":: Nueva Relaci&oacute;n ::" >
 <h4 align="left">Datos de la Persona</h4>
<table width="100%"  border="0" cellspacing="0" class="tablero">
<tr>
<td width="25%">Tipo Documento</td>
<td width="25%">
<select name="tipoDoc2" class="box1" id="tipoDoc2">
<option value="0">Seleccione..</option>
<?php
$orden=3;
$rs=$db->Definiciones(1, $orden);
while ($row=$rs->fetch()){
	if ($row['iddetalledef'] == 1)
		echo "<option value=".$row['iddetalledef']." selected=selected>". $row['detalledefinicion'] ."</option>";
	else
		echo "<option value=".$row['iddetalledef'].">". $row['detalledefinicion'] ."</option>";
}

?>
</select>
<img src="../../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
<td width="25%">N&uacute;mero Documento</td>
<td width="25%"><input name="identificacion2" type="text" class="box1" id="identificacion2" onblur="validarLongNumIdent(document.getElementById('tipoDoc2').value,this);buscarConyuge(this.value);" onkeyup="validarCaracteresPermitidos(document.getElementById('tipoDoc2').value,this);"/>
  <img src="../../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
</tr>         
<tr>
<td>Primer Nombre</td>
<td class="box1">
<input name="pNombre2" type="text" class="box1" id="pNombre2" onkeypress="return vpnombre(event)"/>
<img src="../../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
<td>Segundo Nombre</td>
<td><input name="sNombre2" type="text" class="box1" id="sNombre2" onkeypress="return vsnombre(event)" /></td>
</tr>
<tr>
<td>Primer Apellido</td>
<td><input name="pApellido2" type="text" class="box1" id="pApellido2" onkeypress="return vpnombre(event)"/>
  <img src="../../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
<td>Segundo Apellido</td>
<td><input name="sApellido2" type="text" class="box1" id="sApellido2" onkeypress="return vsnombre(event)" /></td>
</tr>
<tr>
<td>Genero</td>
<td><select name="txtGeneroCony" class="box1" id="txtGeneroCony">
<option selected="selected" value="0">Seleccione..</option>
<option value="M">Masculino</option>
<option value="F">Femenino</option>
</select>
  <img src="../../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
  <td>Tipo de relaci&oacute;n</td>
  <td><select name="txtRelacionCony" class="box1" id="txtRelacionCony">
    <option selected="selected" value="0">Seleccione..</option>
    <option value="2147">CONYUGE</option>
    <option value="2148">COMPA&Ntilde;ERO(A)</option>
  </select>
    <img src="../../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
  <td>Conviven</td>
  <td><select name="txtConviven" class="box1" id="txtConviven">
    <option value="0">Seleccione..</option>
    <option value="S">SI</option>
    <option value="N">NO</option>
  </select>
    <img src="../../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
</tr>
<tr>
<td>Fecha  Nacimiento</td>
<td><input type="text" class="box1" id="txtFechaNaceConyuge" readonly="readonly"/>
  <img src="../../../imagenes/menu/obligado.png" alt="" width="12" height="12" /><td>Pais Nace
<td><select name="estadosCony" class="box1" id="estadosCony"></select>
</tr>
<tr>
<td>Departamento Nacimiento</td>
<td><select name="txtDptoCony" class="box1" id="txtDptoCony">
</select></td>
<td>Ciudad Nacimiento</td>
<td><select name="txtCiudadCony" class="box1" id="txtCiudadCony">
</select></td>
</tr>
</table>
</div>
<br />
<!-- modificar beneficiarios -->
<div id="dialog-actualizar-bene" title="Actualizar Benefciario">
 <h4 align="left">Datos del Beneficiario</h4>
<table width="100%"  border="0" cellspacing="0" class="tablero">
<tr>
<td width="25%">Tipo Documento</td>
<td width="25%">
<select name="tipoDocBen" class="box1" id="tipoDocBen">
<?php
$orden=1;
$rs=$db->Definiciones(1, $orden);
while ($row=$rs->fetch()){
		echo "<option value=".$row['iddetalledef'].">". $row['detalledefinicion'] ."</option>";
}
?>
</select>
<img src="../../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
<td width="25%">N&uacute;mero Documento</td>
<td width="25%"><input name="numeroBen" type="text" class="box1" id="numeroBen" onblur="validarLongNumIdent( document.getElementById( 'tipoDocBen' ).value,this );" onkeyup="validarCaracteresPermitidos( document.getElementById('tipoDocBen' ).value,this);"/>
  <img src="../../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
</tr>         
<tr>
<td>Primer Nombre</td>
<td class="box1">
<input name="pNombreBen" type="text" class="box1" id="pNombreBen" onkeypress="return vpnombre(event)"/>
<img src="../../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
<td>Segundo Nombre</td>
<td><input name="sNombreBen" type="text" class="box1" id="sNombreBen" onkeypress="return vsnombre(event)" /></td>
</tr>
<tr>
<td>Primer Apellido</td>
<td><input name="pApellidoBen" type="text" class="box1" id="pApellidoBen" onkeypress="return vpnombre(event)"/>
  <img src="../../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
<td>Segundo Apellido</td>
<td><input name="sApellidoBen" type="text" class="box1" id="sApellidoBen" onkeypress="return vsnombre(event)" /></td>
</tr>
<tr>
<td>Genero</td>
<td><select name="txtGeneroBen" class="box1" id="txtGeneroBen">
<option selected="selected" value="0">Seleccione..</option>
<option value="M">Masculino</option>
<option value="F">Femenino</option>
</select>
  <img src="../../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
  <td>Fecha  Nacimiento</td>
  <td><input type="text" class="box1" id="txtFechaNaceBen" name="txtFechaNaceBen" readonly="readonly"/>
    <img src="../../../imagenes/menu/obligado.png" alt="" width="12" height="12" /><td>Pais Nace
  <td><select name="estadosBen" class="box1" id="estadosBen"></select>
</tr>
<tr>
<td>Departamento Nacimiento</td>
<td><select name="txtDptoBen" class="box1" id="txtDptoBen">
</select></td>
<td>Ciudad Nacimiento</td>
<td><select name="txtCiudadBen" class="box1" id="txtCiudadBen">
</select></td>
</tr>
</table>
</div>
<!-- fin modifiar beneficairios -->
<input type="hidden" id="formularioAfi" value="<?php echo $formulario; ?>" />
<input type="hidden" id="txtSmlv" name="txtSmlv" value="<?php echo $_SESSION['SMLV']; ?>" />
<input type="hidden" id="idPersona" name="idPersona" />
</form>

<!-- FORMULARIO OBSERVACIONES-->
<div name="div-observaciones-tab" style="display:none" title="Observaciones de modificaci&oacute;n">
<table class="tablero">
 <tr>
   <td>Usuario</td>
   <td colspan="3" >
   <input name="usuarioObs" id="usuarioObs" class="box1" disabled="disabled" value="<?php echo $_SESSION['USUARIO']?>" /></td>
   </tr>
 <tr>
   <td>Observaciones</td>
   <td colspan="3" >
   <textarea name="observacionGrupo" id="observacionGrupo" cols="45" rows="5" class="boxlargo"></textarea></td>
   </tr>
</table>
<div class="ui-state-highlight ui-corner-all" style="margin: 9px auto; display:none;padding: 6px; text-align: center; border: 1px solid rgb(255, 255, 0); width:300px" id="rtaObservacion"><span class="ui-icon ui-icon-info" style="float: left; margin-right: 0.3em;"></span>Observaci&oacute;n guardada con &eacute;xito.</div>
</div>

<!-- FORMULARIO MOTIVOS INACTIVACION -->
<div id="divMotivos" title="Motivos de Inactivacion" style="display:none; padding-first:40px" >
  <p>
    <label>Motivo</label>
    <select name="motivo" id="motivo" class="boxlargo">
          <option value="0"> Seleccione...</option>
      <?php        
        $rs=$db->Definiciones(47, 2);
        while ($row=$rs->fetch()){
          echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
        }
      ?>
      </select>
  </p>
  <button id="btnAcepInactivar" class="ui-state-default" onclick="updateEstadoBeneficiario($('#hideIdRel').val(),'I',$('#motivo').val());">Aceptar</button>
  <input type="hidden" name="hideIdRel" id="hideIdRel" />
</div>

<!-- modificar relacion con el beneficiario -->
<div id="dialog-actualizar-relacion" title="Actualizar Relaciones">
 <h4 align="left">Datos de la Relaci&oacute;n</h4>
<table width="100%"  border="0" cellspacing="0" class="tablero">
<tr>
<td width="25%" colspan="3">Beneficiario</td>
<td id="tdBeneficiario">&nbsp;</td>
<td >Fcha Nace</td>
<td id="tdFechaNace">&nbsp;</td>
</tr>         
<tr>
<td colspan="3">Tipo de beneficiario</td>
<td width="25%" class="box1">
  <select name="txtTipoBen" class="box1" id="txtTipoBen">
  <option value="35">HIJO</option>
  <option value="36">PADRE/MADRE</option>
  <option value="37">HERMANO</option>
  <option value="38">HIJASTRO</option>
</select> <img src="../../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
<td width="25%">C&eacute;dula Padre/Madre</td>
<td width="25%"><select name="txtMadrePadre" class="box1" id="txtMadrePadre">
<option value="0" selected="selected">Seleccione</option>
</select></td>
</tr>
<tr>
<td colspan="3">Tipo de formulario</td>
<td><select name="txtTipoForBene" class="box1" id="txtTipoForBene">
  <option value="S">Subsidio</option>
  <option value="N">Servicios</option>
</select>  <img src="../../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
<td>Fecha Asignaci&oacute;n</td>
<td><input name="txtFecAsig" type="text" class="box1" id="txtFecAsig" /></td>
</tr>
</table>
<div id="pbiologico" style="display:none">
<table width="100%"  border="0" cellspacing="0" class="tablero" id="tablaPB">
<td>Tipo Doc Padre Biol&oacute;gico</td>
<td><select name="tipoDocPB" class="box1" id="tipoDocPB">
<option value="0" selected="selected">Seleccione</option>
  <?php
$orden=1;
$rs=$db->Definiciones(1, $orden);
while ($row=$rs->fetch()){
		echo "<option value=".$row['iddetalledef'].">". $row['detalledefinicion'] ."</option>";
}
?>
</select></td>
<td>N&uacute;mero Documento </td>
<td><input name="numeroPB" type="text" class="box1" id="numeroPB" onblur="validarLongNumIdent( document.getElementById( 'tipoDocPB' ).value,this );" onkeyup="validarCaracteresPermitidos( document.getElementById('tipoDocPB' ).value,this);"/></td>
</tr><tr>
  <td width="25%">P. Nombre</td>
  <td width="25%"><input type="text" id="txtpnombrepb" name="txtpnombrepb" class="box1" /></td>
  <td width="25%">S. Nombre</td>
  <td width="25%"><input type="text" id="txtsnombrepb" name="txtsnombrepb" class="box1" /></td>
</tr>
<tr>
  <td>P. Apellido</td>
  <td><input type="text" id="txtpapellidopb" name="txtpapellidopb" class="box1" /></td>
  <td>S. Apellido</td>
  <td><input type="text" id="txtsapellidopb" name="txtsapellidopb" class="box1" /></td>
</tr>
</table>
</div>
</div>
<!----- beneficiario nuevo --------------->
<div id="dialog-nuevo-beneficiario" title="Nuevo Beneficiario">
 <h4 align="left">Informacion del Beneficiario</h4>
<table width="100%"  border="0" cellspacing="0" class="tablero">
<tr>
  <td colspan="3">Tipo Documento</td>
  <td>
  <select name="tipoDocBen2" class="box1" id="tipoDocBen2">
    <?php
$orden=1;
$rs=$db->Definiciones(1, $orden);
while ($row=$rs->fetch()){
		echo "<option value=".$row['iddetalledef'].">". $row['detalledefinicion'] ."</option>";
}
?>
  </select>
  <img src="../../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
  <td >N&uacute;mero Documento</td>
  <td ><input name="numeroBen2" type="text" class="box1" id="numeroBen2" onblur="validarLongNumIdent( document.getElementById( 'tipoDocBen2' ).value,this );" onkeyup="validarCaracteresPermitidos( document.getElementById('tipoDocBen2' ).value,this);"/>
    <img src="../../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
  
</tr>
<tr>
  <td colspan="3">P Nombre</td>
  <td><span class="box1">
    <input name="pNombreBen2" type="text" class="box1" id="pNombreBen2" onkeypress="return vpnombre(event)"/>
    <img src="../../../imagenes/menu/obligado.png" alt="" width="12" height="12" />  </span></td>
  <td >S Nombre</td>
  <td ><input name="sNombreBen2" type="text" class="box1" id="sNombreBen2" onkeypress="return vsnombre(event)" /></td>
</tr>
<tr>
  <td colspan="3">P Apellido</td>
  <td><input name="pApellidoBen2" type="text" class="box1" id="pApellidoBen2" onkeypress="return vpnombre(event)"/>
    <img src="../../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
  <td >S Apellido</td>
  <td ><input name="sApellidoBen2" type="text" class="box1" id="sApellidoBen2" onkeypress="return vsnombre(event)" /></td>
</tr>
<tr>
  <td colspan="3">Fecha Nace</td>
  <td><input type="text" class="box1" id="txtFechaNaceBen2" name="txtFechaNaceBen2" readonly="readonly"/>
    <img src="../../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
  <td >Pais</td>
  <td ><select name="estadosBen2" class="box1" id="estadosBen2">
  </select>
    <img src="../../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
</tr>
<tr>
  <td colspan="3">Departamento</td>
  <td><select name="txtDptoBen2" class="box1" id="txtDptoBen2">
  </select>
    <img src="../../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
  <td >Ciudad</td>
  <td ><select name="txtCiudadBen2" class="box1" id="txtCiudadBen2">
  </select>
    <img src="../../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
</tr>
<tr>
  <td colspan="3">Genero</td>
  <td><select name="txtGeneroBen2" class="box1" id="txtGeneroBen2">
    <option selected="selected" value="0">Seleccione..</option>
    <option value="M">Masculino</option>
    <option value="F">Femenino</option>
  </select>
    <img src="../../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
  <td >Edad</td>
  <td ><input name="txtEdadBen" type="text" class="box1" id="txtEdadBen" readonly="readonly" /></td>
</tr>
<tr>
  <td colspan="3">Tipo Beneficiario</td>
  <td><span class="box1">
    <select name="txtTipoBen2" class="box1" id="txtTipoBen2">
    <option value="0" selected="selected">Seleccione</option>
      <option value="35">HIJO</option>
      <option value="36">PADRE/MADRE</option>
      <option value="37">HERMANO</option>
      <option value="38">HIJASTRO</option>
    </select>
    <img src="../../../imagenes/menu/obligado.png" alt="" width="12" height="12" />  </span></td>
  <td >C&eacute;dula Padre/Madre</td>
  <td ><select name="txtMadrePadre2" class="box1" id="txtMadrePadre2">
    <option value="0" selected="selected">Seleccione</option>
  </select>
    <img src="../../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
</tr>
<tr>
  <td colspan="3">Tipo Formulario</td>
  <td><select name="txtTipoForBene2" class="box1" id="txtTipoForBene2">
  <option value="0" selected="selected">Seleccione</option>
    <option value="S">Subsidio</option>
    <option value="N">Servicios</option>
  </select>
    <img src="../../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
  <td >Fecha Asignaci&oacute;n</td>
  <td ><input name="txtFecAsig2" type="text" class="box1" id="txtFecAsig2" />
    <img src="../../../imagenes/menu/obligado.png" alt="" width="12" height="12" /></td>
</tr>
<tr>
  <td colspan="3">Discapacidad</td>
  <td><label><input type="radio" name="rDiscapacidad" value="1" id="rDiscapacidad_1" />SI</label>
      <label><input type="radio" name="rDiscapacidad" value="0" id="rDiscapacidad_0" checked="checked" />NO</label>
  </td>
  <td >Certificado de Discapacidad</td>
  <td ><label><input type="radio" name="rCertificado" value="1" id="rCertificado_1" />SI</label>
       <label><input type="radio" name="rCertificado" value="0" id="rCertificado_0" checked="checked" />NO</label></td>
</tr>
<tr>
  <td colspan="3">Certificado Escolaridad</td>
  <td><label><input type="radio" name="rEscolaridad" value="1" id="rEscolaridad_1" />SI</label>
       <label><input type="radio" name="rEscolaridad" value="0" id="rEscolaridad_0" checked="checked" />NO</label></td>
  <td >Certificado Universidad</td>
  <td ><label><input type="radio" name="rEscolaridad" value="1" id="rUniversidad_1" />SI</label>
       <label><input type="radio" name="rEscolaridad" value="0" id="rUniversidad_0" checked="checked" />NO</label></td>
</tr> 
</table>
<div id="pbiologico2" style="display:none">
<table width="100%"  border="0" cellspacing="0" class="tablero" id="tablaPB">
<td width="25%">Tipo Doc Padre Biol&oacute;gico</td>
<td width="25%"><select name="tipoDocPB2" class="box1" id="tipoDocPB2">
<option value="0" selected="selected">Seleccione</option>
  <?php
$orden=1;
$rs=$db->Definiciones(1, $orden);
while ($row=$rs->fetch()){
		echo "<option value=".$row['iddetalledef'].">". $row['detalledefinicion'] ."</option>";
}
?>
</select></td>
<td width="25%">N&uacute;mero Documento </td>
<td width="25%"><input name="numeroPB2" type="text" class="box1" id="numeroPB2" onblur="validarLongNumIdent( document.getElementById( 'tipoDocPB' ).value,this );" onkeyup="validarCaracteresPermitidos( document.getElementById('tipoDocPB' ).value,this);"/></td>
</tr><tr>
  <td width="25%">P. Nombre</td>
  <td width="25%"><input type="text" id="txtpnombrepb2" name="txtpnombrepb2" class="box1" /></td>
  <td width="25%">S. Nombre</td>
  <td width="25%"><input type="text" id="txtsnombrepb2" name="txtsnombrepb2" class="box1" /></td>
</tr>
<tr>
  <td>P. Apellido</td>
  <td><input type="text" id="txtpapellidopb2" name="txtpapellidopb2" class="box1" /></td>
  <td>S. Apellido</td>
  <td><input type="text" id="txtsapellidopb2" name="txtsapellidopb2" class="box1" /></td>
</tr>
</table>
</div>
</div>

	<!-- DIALOGO PERSONA SIMPLE -->
	<div id="dialog-persona" title="Formulario Datos Basicos" style="display:none">
	<center>
		<table width="578" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" class="tablero">
			<tbody>
				<tr bgcolor="#EBEBEB">
				  <td colspan="4" style="text-align:center" >Datos B&aacute;sicos de la persona</td>
			  </tr>
				<tr>
				  <td >Tipo Documento</td>
				  <td ><select name="tDocumento" id="tDocumento" class="box1" disabled>
		          	<option value="1">CEDULA DE CIUDADANIA</option>
					<option value="2">TARJETA DE IDENTIDAD</option>
					<option value="3">PASAPORTE</option>
					<option value="4">CEDULA DE EXTRANJERIA</option>
					<option value="5">NIT</option>
					<option value="6">REGISTRO CIVIL</option>
					<option value="7">MENOR SIN IDENTIFICACION</option>
					<option value="8">ADULTO SIN IDENTIFICACION</option>
		          </select>
		          </td>
				  <td >N&uacute;mero</td>
				  <td ><input name="txtNumeroP" id="txtNumeroP" type="text" class="box1" readonly></td>
			  </tr>
				<tr>
				  <td >Primer Nombre</td>
				  <td ><input name="spNombre" id="spNombre" type="text" class="box1" onkeydown='sololetras(this);' onkeyup='sololetras(this);' onkeypress="return validarEspacio(event)"></td>
				  <td >Segundo Nombre</td>
				  <td ><input name="ssNombre" id="ssNombre" type="text" class="box1" onkeydown='sololetras(this);' onkeyup='sololetras(this);'></td>
			  </tr>
				<tr>
				  <td >Primer Apellido</td>
				  <td ><input name="spApellido" id="spApellido" type="text" class="box1" onkeydown='sololetras(this);' onkeyup='sololetras(this);' onkeypress="return validarEspacio(event)"></td>
				  <td >Segundo Apellido</td>
				  <td ><input name="ssApellido" id="ssApellido" type="text" class="box1" onkeydown='sololetras(this);' onkeyup='sololetras(this);'></td>
			  </tr>
			  <tr>
			  	<td >Sexo</td>
				<td ><select name="tSexo" id="tSexo" class="box1" >
					<option value="0">Seleccione</option>
		        	<option value="M">MASCULINO</option>
					<option value="F">FEMENINO</option>
		          </select>
		        </td>
		    	<td><label for="fechaNaceDialogPersona">Fecha de nacimiento</label></td>
		    	<td><input type="text" name="fechaNaceDialogPersona" id="fechaNaceDialogPersona" /></td>		    	
			  </tr>
				<tr>
					<td >&nbsp;</td>
					<td >&nbsp;</td>
					<td >&nbsp;</td>
					<td >&nbsp;</td>
				</tr>
		</tbody>
		</table>
	</center>
	</div>

<!-- fin -->
</body>
<script>
	$("#txtRadicacion").focus();
</script>

</html>
