<?php
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once '../../../pdo/grupoFamiliar.class.php';
$idc = $_REQUEST['v0'];

$objeto = new GrupoFamiliar();
$rs= $objeto->relacionesPorConyuge($idc);
$filas = array();
while($row = $rs->fetch() ){
	$filas[] = $row;
}
if( count($filas)>0 ) 
	echo json_encode($filas);
else 
	echo 0;


?>