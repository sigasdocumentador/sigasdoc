/** VARIABLES **/
URL=src();
nueva=0;
tiempoFuera=30000;

/** DOCUMENT READY **/
$(document).ready(function(){	
	nuevo();
	
	shortcut.add("Shift+F",function() {		
		var url=URL+"aportes/trabajadores/consultaTrabajador.php";
	    window.open(url,"_blank");
	    },{
		'propagate' : true,
		'target' : document 
	});
	
	$("#tablaerror").dialog({
		autoOpen:false,
		width:640,
		hide: "clip",	
		open:function(event,ui){
		},//fin funcion open
		
		close:function(event,ui){
			$("#tablaerror tr td").html("");
		}
	});//fin dialog
});

/**
 * Funcion que permite iniciar
 */
function nuevo(){
	nueva=1;	
	limpiarCampos(0);		
	$("#tblAfiliaciones").hide();
	$("#cmbIdTipoDocumento").val('1');
	$("#tbAfiliacionesInactivas").html('');
	$("table.tablero :input.ui-state-error,select.ui-state-error").removeClass("ui-state-error");
}

/**
 * Funcion que busca si la persona existe en nuestra base de datos
 * 
 * @param cmbtd 	Select que contiene el tipo de documento a buscar
 * @param txtd 		Text que contiene el numero de documento a buscar
 * @param tdnom 	Td donde es visible el nombre de la persona encontrada
 * @param nuevo 	Opcion que determina si se crea nueva persona si no existe 
 * @returns {persona}
 */
function buscarPersona(cmbtd, txtd, tdnom, nuevo, id){
	$("#tblAfiliaciones").hide();
	$("#tbAfiliacionesInactivas").html('');
	
	var persona=null;
	tdnom.html('');
	id.val('');
	txtd.removeClass("ui-state-error");
	
	if(validarTexto(txtd,0)>0){return null;}
	if(validarSelect(cmbtd,0)>0){return null;}
	
	var tipodoc=cmbtd.val();
	var doc =txtd.val();
	
	$.ajax({
		url: 'buscarPersona.php',
		async: false,
		data: {v0:tipodoc,v1:doc},		
		beforeSend: function(objeto){
        	dialogLoading('show');
        },        
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            } else if(id.val()>0){
            	buscarAfiliacionesInactivas();
            }            
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("En buscarPersona Paso lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,		
		success: function(datoPersona){
			if(datoPersona){
				$.each(datoPersona,function(i,fila){
					persona=fila;
					id.val(fila.idpersona);
					var nom = "&nbsp;&nbsp;&nbsp;&nbsp;" + $.trim(fila.pnombre) + " " + $.trim(fila.snombre) + " " + $.trim(fila.papellido) + " " + $.trim(fila.sapellido);					
					tdnom.html(nom);								
					return;
				});
			} else {				
				alert("No se existe la persona en nuestra base de datos!");
				txtd.val('');
				txtd.addClass("ui-state-error");				
			}
		},
		timeout: tiempoFuera,
        type: "GET"
	});	
	
	return persona;
}

/**
 * Funcion que permite buscar las cuotas monetarias
 */
function buscarAfiliacionesInactivas(){
	$("#tblAfiliaciones").hide();
	$("#tbAfiliacionesInactivas").html('');
	var idPersona = $("#txtIdPersona").val();	
	var error = 0;
	
	if(esNumeroRespuesta(idPersona)==false){
		error++;
		$("#txtIdPersona").val('');
		$("#txtIdentificacion").val('');	
		$("#tdNombreCompleto").html("");
	}
	
	error = validarTexto($("#txtIdentificacion"),error);
	
	if(error>0){
		alert("Introduzca todos los datos para continuar con la consulta");
		return false;
	} else {
		$.ajax({
			url: 'buscarAfiliacionesInactivas.php',
			async: false,
			data: {v0:idPersona},		
			beforeSend: function(objeto){
	        	dialogLoading('show');
	        },        
	        complete: function(objeto, exito){
	        	dialogLoading('close');
	            if(exito != "success"){
	                alert("No se completo el proceso!");
	            }            
	        },
	        contentType: "application/x-www-form-urlencoded",
	        dataType: "json",
	        error: function(objeto, quepaso, otroobj){
	            alert("En buscarAfiliacionesInactivas Paso lo siguiente: "+quepaso);
	        },
	        global: true,
	        ifModified: false,
	        processData:true,		
			success: function(datoAfiliaciones){
				if(datoAfiliaciones==0 || datoAfiliaciones==""){					
	    			alert("La persona no tiene afiliaciones Inactivas!");
	    			$("#txtIdPersona").val('');
					$("#txtIdentificacion").val('');
					$("#tdNombreCompleto").html("");
	    			return false;
	    		}	
				$("#tblAfiliaciones").show();
	    		$.each(datoAfiliaciones,function(i,f){
	    			$("#tbAfiliacionesInactivas").append("<tr>" +							
							"<td style='text-align: center' ><input id='chkIdFormulario' type='checkbox' onclick='reactivar(" + f.idformulario +",$(this));' ></td>" +
							"<td style='text-align: center' >" + f.idformulario + "</td>" +
							"<td style='text-align: center' >" + f.nit + "</td>" +
							"<td>&nbsp;&nbsp;" + f.razonsocial + "</td>" +
							"<td style='text-align: center' >" + f.fechaingreso + "</td>" +
							"<td style='text-align: center' >" + f.fecharetiro + "</td>" +
							"<td style='text-align: center' >" + f.tipoformulario + "</td>" +
							"<td style='text-align: center' >" + f.tipoafiliacion + "</td>" +
						"</tr>")
	    			return;
	    		})
				
			},
			timeout: tiempoFuera,
	        type: "GET"
		});	
	}
	
}

/**
 * Funcion que permite anular la cuota monetaria
 */
function reactivar(idFormulario,chk){	
	if(esNumeroRespuesta(idFormulario)==false){ alert("Ocurrio un error, no se recibio el idFormulario, no se puede continuar"); chk.attr('checked', false); return false; }
	
	if(confirm("Esta seguro de REACTIVAR esta afiliacion?")==true){
		var tr = chk.parent().parent();
		var idPersona = $("#txtIdPersona").val();
		var observacion = "REACTIVACION AFILIACION - (" + tr.children(":eq(2)").text() + ")" + $.trim(tr.children(":eq(3)").text()) + " - Fecha Ingreso: " + tr.children(":eq(4)").text() + " - Retiro Erroneo: " + tr.children(":eq(5)").text();		
		$.ajax({
			url: 'procesoActivarAfiliacion.php',
			async: false,
			data: {v0:idFormulario, v1:idPersona, v2:observacion},		
			beforeSend: function(objeto){
	        	dialogLoading('show');
	        },        
	        complete: function(objeto, exito){
	        	dialogLoading('close');
	            if(exito != "success"){
	                alert("No se completo el proceso!");
	            }            
	        },
	        contentType: "application/x-www-form-urlencoded",
	        dataType: "json",
	        error: function(objeto, quepaso, otroobj){
	            alert("En reactivar Paso lo siguiente: "+quepaso);
	        },
	        global: true,
	        ifModified: false,
	        processData:true,		
			success: function(datoAfiliaciones){
				if(esNumeroRespuesta(datoAfiliaciones.error)){
					alert("La reactivacion se realizo con exito");	
					buscarAfiliacionesInactivas();
					return;
				} else {
					var	str = String(datoAfiliaciones.descripcion);
					var res = str.replace(/'/g, "|");  
					$('#tablaerror tr td').append(res);
					$('#tablaerror').dialog('open');
					chk.attr('checked', false);	
					return;
				}
			},
			timeout: tiempoFuera,
	        type: "GET"
		});			
	} else {
		chk.attr('checked', false);		
	}	
}