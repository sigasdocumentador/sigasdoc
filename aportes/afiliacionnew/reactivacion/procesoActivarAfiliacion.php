<?php
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $_SESSION['RAIZ'] . DIRECTORY_SEPARATOR . 'rsc/pdo/IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'aportes'.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'val.empresa.reactivacion.class.php';


$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo $cadena;
	exit();
}

$fecha = date("m/d/Y");
$usuario=$_SESSION['USUARIO'];

$c0 = $_REQUEST['v0'];		//idFormulario
$c1 = $_REQUEST['v1'];		//idpersona
$c2 = $_REQUEST['v2'];
$c2 = $fecha . " - " . $usuario . " - " . $c2;

$data = array("error"=>0,"descripcion"=>'');

/*
 * lineas creadas el 01-06-2016
 * se adicionan las siguientes lineas para validar que la empresa a la que pertenece el afiliado dependiente a reactivar
 * se encuentre activa de lo contrario no permitar realizar la reactivacion.
 */

$objre= new valEmpresasReactivacionAfiliado($c0,$c1);

/* se consulta el id de la empresa asociada con el afiliado en la tabla aportes017 */
$objre->consultarIdEmpresa();

if($objre->idEmpresa!=''){
           // extaemos el estado de la empresa para verificar si esta activa o inactiva
	       $rsest=$objre->cosultarEstadoEmpresa($objre->idEmpresa);
	       // a traves del idempresa asociado al afiliado tabla aportes017 se busca la empresa y se valida si existe 
           if($rsest==1){
           	 // se valida si el estado de la empresa esta != I (Inactiva) y si se trata de un afiliado dependiente o independiente para continuar
           	 if(($objre->estado!='I' and $objre->tipoafiliacion==18) or ($objre->estado=='I' and $objre->tipoafiliacion<>18)){  //si el estado s Inactivo se detiene el proceso de activacion y se notifica.
           	 	//inicio proceso inactivacion
           	 	
           	 	$sql="SELECT *
	                  FROM aportes016 a16
	                  INNER JOIN aportes017 a17 ON a17.idpersona=a16.idpersona AND a17.idempresa=a16.idempresa
                      WHERE a17.idformulario=:idFormulario";
           	 	$statement = $db->conexionID->prepare($sql);
           	 	$statement->bindValue(":idFormulario", $c0,  PDO::PARAM_STR);
           	 	$statement->execute();
           	 	
           	 	if($statement->rowCount() == 0){
           	 		$db->conexionID->beginTransaction();
           	 	
           	 		$sql="INSERT INTO aportes016(tipoformulario, tipoafiliacion, idempresa, idpersona, fechaingreso, horasdia, horasmes, salario, agricola, cargo, primaria, estado, fecharetiro, motivoretiro, fechanovedad, semanas, fechafidelidad, estadofidelidad, traslado, codigocaja, flag, tempo1, tempo2, fechasistema, usuario, tipopago, categoria, auditado, idagencia, idradicacion)
	                            SELECT tipoformulario, tipoafiliacion, idempresa, idpersona, fechaingreso, horasdia, horasmes, salario, agricola, cargo, primaria, 'A', null, null, fechanovedad, semanas, null, null, traslado, codigocaja, flag, tempo1, tempo2, CAST(getDate() AS DATE), '".$usuario."', tipopago, categoria, auditado, idagencia, idradicacion
	                      FROM aportes017 WHERE idformulario=:idFormulario";
           	 		$statement = $db->conexionID->prepare($sql);
           	 		$statement->bindValue(":idFormulario", $c0,  PDO::PARAM_STR);
           	 		$guardada = $statement->execute();
           	 	
           	 		if($guardada){
           	 	
           	 			$sqlTipo="SELECT tipoafiliacion,idempresa FROM aportes017 WHERE idformulario=$c0";
           	 			$rs=$db->querySimple($sqlTipo);
           	 			$w=$rs->fetch();
           	 	
           	 			$tipoafiliacionBase=$w['tipoafiliacion'];
           	 			$idempresaBase=$w['idempresa'];
           	 	
           	 	
           	 			if($tipoafiliacionBase !=18){
           	 					
           	 				$sqlActualizar="UPDATE aportes048 SET estado='A' WHERE idempresa=$idempresaBase";
           	 				$rs=$db->queryActualiza($sqlActualizar,'aportes048');
           	 			}
           	 	
           	 	
           	 			$sql="DELETE FROM aportes017 WHERE idformulario=:idFormulario";
           	 			$statement = $db->conexionID->prepare($sql);
           	 			$statement->bindValue(":idFormulario", $c0,  PDO::PARAM_STR);
           	 			$guardada = $statement->execute();
           	 	
           	 			$guardadaActEmp = $statement->execute();
           	 	
           	 			if($guardada){
           	 				$sql="SELECT * FROM aportes088 WHERE idregistro=:c1 AND identidad=1";
           	 				$statement = $db->conexionID->prepare($sql);
           	 				$statement->bindValue(":c1", $c1,  PDO::PARAM_STR);
           	 				$statement->execute();
           	 					
           	 				if($statement->rowCount() == 0){
           	 					$sql="	INSERT INTO aportes088 (idregistro,identidad,observaciones) VALUES (:idregistro,1,:observaciones)";
           	 					$statement = $db->conexionID->prepare($sql);
           	 					$statement->bindValue(":idregistro", $c1,  PDO::PARAM_STR);
           	 					$statement->bindValue(":observaciones", $c2,  PDO::PARAM_STR);
           	 					$guardada = $statement->execute();
           	 	
           	 					if($guardada){
           	 						$db->conexionID->commit();
           	 						$data['error']= 1;
           	 					} else {
           	 						$db->conexionID->rollBack();
           	 						$data['error']=-7;
           	 						$data['descripcion']="Error al Activar, no se registro a observacion en la tabla aportes088, reportelo al administrador";
           	 					}
           	 				} else {
           	 					$fila=$statement->fetch();
           	 					$observaciones=$fila['observaciones'];
           	 					$c2 = $c2 . "<br>" . $observaciones;
           	 	
           	 					$sql="	UPDATE aportes088 SET observaciones=:observaciones WHERE idregistro=:idregistro AND identidad=1";
           	 					$statement = $db->conexionID->prepare($sql);
           	 					$statement->bindValue(":idregistro", $c1,  PDO::PARAM_STR);
           	 					$statement->bindValue(":observaciones", $c2,  PDO::PARAM_STR);
           	 					$guardada = $statement->execute();
           	 	
           	 					if($guardada){
           	 						$db->conexionID->commit();
           	 						$data['error']= 1;
           	 					} else {
           	 						$db->conexionID->rollBack();
           	 						$data['error']=-8;
           	 						$data['descripcion']="Error al activar, no fue posible actualizar la tabla aportes088, reportelo al administrador";
           	 					}
           	 				}
           	 			} else {
           	 				$db->conexionID->rollBack();
           	 				$data['error']=-6;
           	 				$data['descripcion']="Error al activar, no se pudo elminar el registro de la tabla aportes017 ";
           	 			}
           	 		} else {
           	 			$db->conexionID->rollBack();
           	 			$data['error']=-5;
           	 			$data['descripcion']="Ocurrio un error al realizar la activacion\nActualizando Afiliacion";
           	 		}
           	 	} else {
           	 		$data['error']= -4;
           	 		$data['descripcion']="Ocurrio un error al realizar la activacion\nExisten Afiliaciones Activas entre la empresa y el afiliado";
           	 	}
           	 	
           	 	//fin proceso de inactivacion
           	 }else{
           	 	$data['error']=-3;
           	  	$data['descripcion']= $objre->tablaRes();
           	 }
           }else{
           	 $data['error']=-2;
           	 $data['descripcion']= "No se encontro la empresa con id:".$objre->idEmpresa.", reportar al administrador";
           }
                      
}else{
	$data['error']=-1;
	$data['descripcion']="No se encontro registro en la tabla aportes017 con numero de formulario $c0";
	
}

echo json_encode($data);


?>