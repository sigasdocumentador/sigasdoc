<?php
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");

$root= $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $_SESSION['RAIZ'] . DIRECTORY_SEPARATOR . 'rsc/pdo/IFXDbManejador.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo $cadena;
	exit();
}

$campo0=$_REQUEST['v0'];	//idpersona

$sql="SELECT 
			a17.*, a48.nit, a48.razonsocial, a91.detalledefinicion tipoformulario, b91.detalledefinicion tipoafiliacion 
	  FROM aportes017 a17 
	  INNER JOIN aportes048 a48 ON a48.idempresa=a17.idempresa 
	  INNER JOIN aportes091 a91 ON a91.iddetalledef=a17.tipoformulario 
	  INNER JOIN aportes091 b91 ON b91.iddetalledef=a17.tipoafiliacion 
	  WHERE a17.idpersona=:campo0";

$statement = $db->conexionID->prepare($sql);
$statement->bindValue(":campo0", $campo0,  PDO::PARAM_STR);
$statement->execute();

if($statement->rowCount() != 0){
	$result =$statement->fetchAll();
	$filas = array();
		
	foreach($result as $row){
		$filas[] = array_map("utf8_encode",$row);
	}
		
	echo json_encode($filas);
} else {
	echo 0;
	exit();
}

?>