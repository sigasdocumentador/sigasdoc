<?php

set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");

$root= $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';

include_once $raiz . DIRECTORY_SEPARATOR . 'rsc/pdo/IFXDbManejador.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="es"><head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Reactivar Afiliaci&oacute;n</title>
<link type="text/css" href="<?php echo URL_PORTAL; ?>newcss/Estilos.css" rel="stylesheet">
<link type="text/css" href="<?php echo URL_PORTAL; ?>newcss/css.1.8/jquery-ui-1.8.17.custom.css" rel="stylesheet">
<link type="text/css" href="<?php echo URL_PORTAL; ?>newcss/marco.css" rel="stylesheet">

<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/1.6/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/1.6/jquery-ui-1.8.16.custom.min.js"></script>

<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/comunes.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/jquery.utilitarios.js"></script>
<script type="text/javascript" src="js/reactivacion.js"></script>
</head>

<body>
	<form name="forma">
		<br />		
			<div>
				<table width="75%" border="0" cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td width="13" height="29" class="arriba_iz">&nbsp;</td>
						<td class="arriba_ce"><span class="letrablanca">:: Datos Afiliado ::</span></td>
						<td width="13" class="arriba_de" align="right">&nbsp;</td>
					</tr>
					<tr height="35" >
						<td class="cuerpo_iz">&nbsp;</td>
						<td class="cuerpo_ce">
							<table width="90%" cellspacing="0" cellpadding="0"
								align="center">
								<tr>
									<td>
										<img src="<?php echo URL_PORTAL; ?>imagenes/menu/nuevo.png" title="Nuevo" style="cursor: pointer" onclick="nuevo();">										
									</td>
								</tr>
							</table></td>
						<td class="cuerpo_de">&nbsp;</td>
					</tr>
					<tr>
						<td class="cuerpo_iz">&nbsp;</td>
						<td class="cuerpo_ce">
							<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center" class="tablero">
								<tr>
									<td width="10%" style="text-align: center;" >Tipo Documento</td>
									<td width="20%" >
										<label> &nbsp;&nbsp;&nbsp;
											<select name="cmbIdTipoDocumento" id="cmbIdTipoDocumento" class="box1" style="width: 210px" onchange="$('#txtIdentificacion').val('').trigger('blur');" >
												<option value="0" selected="selected">Seleccione...</option>
								           		<?php
													$rs = $db->Definiciones ( 1, 1 );
													while ( $row = $rs->fetch() ) {
														echo "<option value=" . $row ['iddetalledef'] . ">" . $row ['detalledefinicion'] . "</option>";
													} 
												?>
											</select> 
											<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
										</label>
									</td >
									<td width="10%" style="text-align: center;" >N&uacute;mero</td>
									<td width="20%" > &nbsp;&nbsp;&nbsp;
										<input id="txtIdPersona" type="hidden" value="" />
										<input name="txtIdentificacion" type="text" class="box1" id="txtIdentificacion" onkeyup="validarIdentificacion($('#cmbIdTipoDocumento'),this);" onkeydown="validarIdentificacion($('#cmbIdTipoDocumento'),this);" 
										onkeypress="validarIdentificacion($('#cmbIdTipoDocumento'),this);" onblur="buscarPersona($('#cmbIdTipoDocumento'), $('#txtIdentificacion'), $('#tdNombreCompleto'), false, $('#txtIdPersona'));" maxlength="12">											
										<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12"></td>
								</tr>
								<tr>
									<td style="text-align: center;" >Nombre Completo</td>
									<td id="tdNombreCompleto" colspan="3">&nbsp;</td>
								</tr>																									
							</table>
						</td>
						<td class="cuerpo_de">&nbsp;</td>
					</tr>
					<tr>
						<td class="abajo_iz">&nbsp;</td>
						<td class="abajo_ce"></td>
						<td class="abajo_de">&nbsp;</td>
					</tr>
				</table>
				<br />
				<table width="75%" border="0" cellspacing="0" cellpadding="0" align="center" id="tblAfiliaciones">
					<tr>
						<td width="13" height="29" class="arriba_iz">&nbsp;</td>
						<td class="arriba_ce"><span class="letrablanca">:: Afiliaciones Inactivas ::</span></td>
						<td width="13" class="arriba_de" align="right">&nbsp;</td>
					</tr>
					<tr>
						<td class="cuerpo_iz">&nbsp;</td>
						<td class="cuerpo_ce">&nbsp;</td>
						<td class="cuerpo_de">&nbsp;</td>
					</tr>					
					<tr>
						<td class="cuerpo_iz">&nbsp;</td>
						<td class="cuerpo_ce">
							<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center" class="tablero" id="tblAfiliacionesInactivas">
								<tr>
									<th width="%">Anulado</th>
									<th width="%">IdFormulario</th>
									<th width="%">Nit</th>
									<th width="%">Empresa</th>
									<th width="%">F Ingre</th>
									<th width="%">F Retiro</th>
									<th width="%">Tipo Form</th>
									<th width="%">Tipo Afi.</th>
								</tr>
								<tbody id="tbAfiliacionesInactivas"></tbody>
							</table>
						</td>
						<td class="cuerpo_de">&nbsp;</td>
					</tr>									
					<tr>
						<td class="abajo_iz">&nbsp;</td>
						<td class="abajo_ce"></td>
						<td class="abajo_de">&nbsp;</td>
					</tr>					
				</table>
			</div>				
	</form>
	<!-- DIV PARA MOSTRAR LOS ERRORES -->
        <div id="tablaerror" title="::ERROR AL ACTIVAR EL AFILIADO::" style="display: none;">
			<table width="100%" border="0" cellpadding="5" cellspacing="0" class="tablero" id="tblresultado">
		 	   <tr><td></td></tr>
     		</table>
         </div>
	
</body>
</html>