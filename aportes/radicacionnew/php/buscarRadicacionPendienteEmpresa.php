<?php
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $_SESSION['RAIZ'] . DIRECTORY_SEPARATOR . 'pdo/radicacion.class.php';

$campo0=$_REQUEST['v0'];		//nit
$campo1=$_REQUEST['v1'];		//tiposRadicacion

$objeto = new Radicacion();
echo json_encode($objeto->buscarRadicacionPendienteEmpresa($campo0,$campo1));
?>