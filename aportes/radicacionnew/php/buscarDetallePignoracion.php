<?php
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'pdo' . DIRECTORY_SEPARATOR . 'pignoracion.class.php';


$idp=$_REQUEST['v0'];
$pagare=$_REQUEST['v1'];
$saldo=$_REQUEST['v2'];

$objPignoracion = new Pignoracion();
$rsPignoracion = $objPignoracion->buscarDetallePignoracion($idp);
?>
<html>

<center>
	<table width="578" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" class="tablero">
		<tbody>
			<tr bgcolor="#EBEBEB">
			  <td colspan="5" style="text-align:center" >Detalle de la Pignoraci&oacute;n No. <?php echo $idp; ?> (<strong>Pagare <?php echo $pagare; ?></strong>) - <strong style="font-size:12pt">Saldo: $ <?php echo number_format($saldo); ?></strong></span></td>
			</tr>
			<tr>
				<td >Beneficiario</td>
				<td >Periodo</td>
				<td >Fecha</td>
				<td >Valor</td>
				<td >Anulado</td>
			</tr>
		<?php	
			if(!is_numeric($rsPignoracion)){
				foreach ($rsPignoracion as $row){ ?>        
			<tr>
			  <td ><?php echo $row['nombre']; ?></td>
			  <td style="text-align:center" ><?php echo $row['periodo']; ?>&nbsp;</td>
			  <td style="text-align:center" ><?php echo $row['fechasistema']; ?>&nbsp;</td>
			  <td style="text-align:center" ><?php echo number_format($row['valor']); ?>&nbsp;</td>
			  <td style="text-align:center" ><?php echo $row['anulado']; ?></td>
		  </tr>
				<?php } 
			} ?>
	    </tbody>
	</table>
</center>
</html>