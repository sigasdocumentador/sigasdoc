<?php
 set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $_SESSION['RAIZ'] . DIRECTORY_SEPARATOR . 'pdo/radicacion.class.php';
$campos= json_decode($_REQUEST['v0']);

$fechaActual = new DateTime();
$hora = $fechaActual->format("H");
$minuto = $fechaActual->format("i");
$validahora=$hora.":".$minuto;

if($validahora>='17:30'){	
	$fechaActual->modify('+1 day');
}

$campos->fecharadicacion=$fechaActual->format('Ymd');

$objeto = new Radicacion();

/*
 * Adicionar habeas data
 * Se actualiza en el caso en que el estado habeas data que 
 * radica en la tabla aportes015 sea diferente al que llega en la radicacion
 * 0: no se selecciono habeas data
 * 1: Si aprueba el manejo de los datos personales
 * 2: No aprueba el manejo de los datos personales
 * 
 */
$campos->habeas_data = empty($campos->habeas_data)?0:$campos->habeas_data;


 if ($campos->habeas_data!=0)
    if($objeto->radicarhabeasdata($campos->idtipodocumento,$campos->identificacion,$campos->habeas_data)!=0){
 	    echo 0;
 	    echo "hay un error en el metodo radicarhabeasdata de la clase radicacion.class.php";
 	    exit();
    }
 /*  Fin habeas data */
 
echo $objeto->guardarRadicacion($campos);

?>