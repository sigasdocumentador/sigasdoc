/**
 * Funcion que busca si la persona existe en nuestra base de datos
 * 
 * @param cmbtd 	Select que contiene el tipo de documento a buscar
 * @param txtd 		Text que contiene el numero de documento a buscar
 * @param tdnom 	Td donde es visible el nombre de la persona encontrada
 * @param nuevo 	Opcion que determina si se crea nueva persona si no existe 
 * @returns {persona}
 */
function buscarPersona(cmbtd, txtd, tdnom, nuevo, flag){
	var persona=null;
	
	
	if(txtd.get(0).id=="txtIdentificacion"){ idPersonaGlobal=0; idEmpresaGlobal=0; }	
	copyTipoDoc=1;	
	copyDoc='';
	if(flag==undefined){ flag=7; }	
	tdnom.html('');
	txtd.removeClass("ui-state-error");
	
	if(validarTexto(txtd,0)>0){return null;}
	if(error=validarSelect(cmbtd,0)>0){return null;}
	
	var tipodoc=cmbtd.val();
	var doc =txtd.val();
	
	$.ajax({
		url: rutaPHP + 'buscarPersona.php',
		async: false,
		data: {v0:tipodoc,v1:doc,v2:flag},		
		beforeSend: function(objeto){
        	dialogLoading('show');
        },        
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }            
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("En buscarPersona paso lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,		
		success: function(datoPersona){
			if ( datoPersona ) {
				if(flag!=7){ persona=datoPersona; return; }
				$.each(datoPersona,function(i,fila){
					$("#idPersona").val(fila.idpersona);
					
					
					if(txtd.attr("name")=='txtIdentificacion'){
						$("#cmbHabeasData").removeAttr('disabled');
						$("#cmbIdTipoRadicacion").val('0');
						$("#cmbHabeasData").val(fila.habeas_data);
						if(fila.habeas_data==1){
							$("#cmbHabeasData").attr('disabled','disabled');
						}
					}
					
					persona=fila;
					if(txtd.get(0).id=="txtIdentificacion"){ idPersonaGlobal=fila.idpersona; }
					copyTipoDoc=tipodoc;
					copyDoc=doc;
					var nom = "&nbsp;" + $.trim(fila.pnombre) + " " + $.trim(fila.snombre) + " " + $.trim(fila.papellido) + " " + $.trim(fila.sapellido);
					
					var fechanacec=fila.fechanacimiento;	
	
					// calcular edad
					var birthday = new Date(fechanacec);
					var today = new Date();
					var years = today.getFullYear() - birthday.getFullYear();
				
					// Reset birthday to the current year.
					birthday.setFullYear(today.getFullYear());
				
					// If the user's birthday has not occurred yet this year, subtract 1.
					if (today < birthday)
					{
						years--;
					}
							
					if(fila.fechanacimiento=='' || fila.sexo=='' || years<16 || years>100)
					{
						ActualizarDatosBasico(fila.idpersona);
					}							
					tdnom.html(nom);
					
					return;
				});
			} else {
				if(flag!=9) { alert("No existe en nuestra base de datos, registre los datos completos!"); }
				else { alert("El BONO no existe en nuestra base de datos!"); }
				if(nuevo){
					validarIdentificacionInsert(cmbtd,txtd);					
					if(validarTexto(txtd,0)>0){ alert("Documento no valido!"); return null;}
					enaDivDocDoble();
					newPersonaSimple(cmbtd,txtd);
				} else {
					txtd.val('');
					txtd.addClass("ui-state-error");
				}
			}
		},
		timeout: tiempoFuera,
        type: "GET"
	});	
	
	return persona;
}



function ActualizarDatosBasico(idpersonaActualiza){		
	idpersonaActualiza=$.trim(idpersonaActualiza);
	
	$("#dialog-persona2").dialog("destroy");	
	$("#dialog-persona2").dialog({
		show: "drop",
		hide: "clip",
		width:750,		
		modal: true,
		closeOnEscape: false,
		open:function(event,ui){
			$(this).parent().children().children("a.ui-dialog-titlebar-close").remove();
			$.ajax({
				url:URL+'phpComunes/pdo.buscar.persona.id.php',
				async: false,
				data:{v0:idpersonaActualiza},
				dataType:"json",
				beforeSend: function(objeto){
		        	dialogLoading('show');
		        },        
		        complete: function(objeto, exito){
		        	dialogLoading('close');
					if(exito != "success"){
		                alert("No se completo el proceso!");
		            }            
		        },
		        contentType: "application/x-www-form-urlencoded",
		        dataType: "json",
		        error: function(objeto, quepaso, otroobj){
		            alert("En actualizaBasico2 Paso lo siguiente: "+quepaso);
		        },
		        global: true,
		        ifModified: false,
		        processData:true,
		        success:function(data){
				    if(data==0){
				    	return false;
				    }else {
						$.each(data,function(i,fila){
							$('#txtHiddenIdPersona').val(fila.idpersona);
							$('#cmbDBTipoDoc').val(fila.idtipodocumento);
							$('#txtDBIdentificacion').val(fila.identificacion);
							$('#txtDBpNombre').val(fila.pnombre);
							$('#txtDBsNombre').val(fila.snombre);
							$('#txtDBpApellido').val(fila.papellido);
							$('#txtDBsApellido').val(fila.sapellido);
							$('#txtDBfecNace').val(fila.fechanacimiento);
							$('#cmbDBSexo').val(fila.sexo);					
						 }); //fin each
				    }
				},
				type:"POST"
			 }); //ajax buscarpersona.php
		},
		
		buttons: {
			'Actualizar': function() {
				$("table.tablero :input.ui-state-error,select.ui-state-error").removeClass("ui-state-error");//Limpiar la clase de error
				var salir=false;
				var idPersonaActualizar=$('#txtHiddenIdPersona').val();
				var tipoDocActualizar=$('#cmbDBTipoDoc').val();
				var identificacionActualizar=$.trim($('#txtDBIdentificacion').val());
				var fNaceActualizar=$.trim($('#txtDBfecNace').val());							
				var sexoActualizar=$('#cmbDBSexo').val();
				
				var years=0;
				var fechanacec=fNaceActualizar;	
	
					// calcular edad
					var birthday = new Date(fechanacec);
					var today = new Date();
					var years = today.getFullYear() - birthday.getFullYear();
				
					// Reset birthday to the current year.
					birthday.setFullYear(today.getFullYear());
				
					// If the user's birthday has not occurred yet this year, subtract 1.
					if (today < birthday)
					{
						years--;
					}
					
				if($("#cmbDBSexo").val()=='0')	{
						$("#cmbDBSexo").addClass("ui-state-error");						
						}// fin if
						
				if($("#txtDBfecNace").val()=='' || years<16 || years>100)	{
						$("#txtDBfecNace").addClass("ui-state-error");	
						$("#txtDBfecNace").val();			
						}// fin if
				
				if($("#cmbDBSexo").val()!='0' && $("#txtDBfecNace").val()!='') 
				{
							var datos={v0:idPersonaActualizar,v1:tipoDocActualizar,v2:identificacionActualizar,v3:fNaceActualizar,v4:sexoActualizar};
							
							$.ajax({
								url: URL+'phpComunes/pdo.modificar.persona.basico.php',
								type: "POST",
								data: datos,
								async: false,
								//dataType: "json",
								success: function(data){
							 		if(data==0){
							 			alert("No se pudo actualizar los datos!");
							 			return false;
									} else {
								 		alert("Se actualizo fecha nacimiento y genero correctamente.");
								 		$("#dialog-persona2").dialog("close");							 		
									}
								}
							});
					}	
					else
					{
						alert("Falta Completar Datos");
					}			
			}
		}
	});
}



/**
 * Funcion para verificar que se realiza el tipo de afiliación correcto
 * 
 * @param idp
 * @param opt 			opt = 1 retorna resultado de la consulta 
 * @returns {Number} 	( 0 => error, 1 => renovacion(Con Pendientes por PU), 2 => renovacion(SIN pendientes por PU), 3 => afiliacion(Nuevo), 4 => afiliacion(PU))
 */
function comprobarTipoAfiliacion(idp,opt){	
	var retorno=0;
	
	if(opt==undefined){ opt=0; }
	
	$.ajax({
		url: rutaPHP + 'buscarTotalAfiliaciones.php',
		async: false,
		data: {v0:idp},		
		beforeSend: function(objeto){
        	dialogLoading('show');
        },        
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }            
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("En comprobarTipoAfiliacion paso lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
		success: function(afiliaciones){
			if(opt==0){
				var cantAfiliaciones=parseInt(afiliaciones['0'])+parseInt(afiliaciones['1'])-parseInt(afiliaciones['2']);
				if (cantAfiliaciones>0){
					if (parseInt(afiliaciones['2'])>0)
						retorno=1;
					else
						retorno=2;
				} else if(cantAfiliaciones<=0) {
					if(parseInt(afiliaciones['0'])==0 && parseInt(afiliaciones['1'])==0 && parseInt(afiliaciones['2'])==0)
						retorno=3;
					else if (parseInt(afiliaciones['2'])>0)
						retorno=4;
					else 
						retorno=0;
				}
			} else {
				retorno=afiliaciones;
			}
		},
        timeout: tiempoFuera,
        type: "GET"
	});
	
	return retorno;
}

function buscarTiempoVinculacion(idp){
	if(esNumeroRespuesta(idp)==false){return -1;}
	
	var dias=0;
	
	$.ajax({
		url: rutaPHP + 'buscarTiempoVinculacion.php',
		async: false,
		data:{v0:idp},
		beforeSend: function(objeto){
        	dialogLoading('show');
        },        
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }            
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("En buscarTiempoVinculacion paso lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
		success:function(diasVinculado){
			if(esNumeroRespuesta(diasVinculado)){ dias=diasVinculado; }
		},
		timeout: tiempoFuera,
        type: "GET"
	});	
	
	return dias;
}

/**
 * Funcion que trae todas las afiliaciones activas que tiene el afiliado
 *
 * @param idp 		idpersona *  
 *    
 * @returns {afiliacion}
 */
function buscarAfiliacionesActivasAfiliado(idp){
	if(esNumeroRespuesta(idp)==false){return null;}
	
	var afiliacion=null;
	
	$.ajax({
		url: rutaPHP + 'buscarAfiliacionesActivasAfiliado.php',
		async: false,
		data:{v0:idp},
		beforeSend: function(objeto){
        	dialogLoading('show');
        },        
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }            
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("En buscarAfiliacionesActivasAfiliado paso lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
		success:function(datoAfiliacion){
			if(datoAfiliacion){ afiliacion=datoAfiliacion }
		},
		timeout: tiempoFuera,
        type: "GET"
	});	
	
	return afiliacion;
}

/**
 * Funcion que comprueba si tiene Afiliaciones con la empresa
 * 
 * @param txtNit 		Nit de la Empresa a comprobar
 * @param idp 			Persona a comprobar
 * @returns {Number} 	( 0 => Error, 1 => Tiene Afiliacion Activa, 2 => La afiliacion es Pendiente, 3 => No tiene afiliacion activa )
 */
function comprobarAfiliacion(txtNit, idp){
	if(esNumeroRespuesta(idp)==false){ return 0; }
	if(validarNumero(txtNit,0)>0){ return 0; }
	
	var retorno=1;
	
	$.ajax({
		url: rutaPHP + 'buscarAfiliacionesActivas.php',
		async: false,
		data:{v0:idp,v1:txtNit.val()},
		beforeSend: function(objeto){
        	dialogLoading('show');
        },        
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }            
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("En comprobarAfiliacion paso lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
		success:function(cantAfilPU){
			if(cantAfilPU[1]>0 || cantAfilPU[0]>0){
				if(cantAfilPU[1]>0 || cantAfilPU[0]==0)
					retorno=2;
			}
			else
				retorno=3;
		},
        timeout: tiempoFuera,
        type: "GET"
	});
		
	return retorno;
}

/**
 * Funcion para verificar si es una afiliacion por Planilla Unica
 * 
 * @param txtNit
 * @param idp
 * @returns {Boolean}
 */
function comprobarEsPU(txtNit, idp){
	var nit=0;
	if(esNumeroRespuesta(idp)==false){ return false; }
	if(esNumeroRespuesta(txtNit)){ nit=txtNit; } 
	else {
	    if(validarNumero(txtNit,0)==0){ nit=txtNit.val(); } 
	    else { return false; }
	}	
	
	var retorno=0;
	
	$.ajax({
		url: rutaPHP + 'buscarAfiliacionPporPlanilla.php',
		async: false,
		data:{v0:idp,v1:nit},
		beforeSend: function(objeto){
        	dialogLoading('show');
        },        
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }            
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("En comprobarEsPU paso lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
        success:function(cantAfilPU){
			if(esNumeroRespuesta(cantAfilPU))
				retorno=cantAfilPU;
		},
		timeout: tiempoFuera,
        type: "GET"
	});
		
	return retorno;
}

/**
 * Funcion para verificar si es una afiliacion por Planilla Unica
 * 
 * @param txtNit
 * @param idp
 * @returns {Boolean}
 */
function comprobarIndependienteEsPU(idp){
	if(esNumeroRespuesta(idp)==false){ return false; }	
	
	var retorno=0;
	
	$.ajax({
		url: rutaPHP + 'buscarAfiliacionIndependientePporPlanilla.php',
		async: false,
		data:{v0:idp},
		beforeSend: function(objeto){
        	dialogLoading('show');
        },        
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }            
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("En comprobarIndependienteEsPU paso lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
		success:function(cantAfilPU){
			if(esNumeroRespuesta(cantAfilPU))
				retorno=cantAfilPU;
		},
		timeout: tiempoFuera,
        type: "GET"
	});
		
	return retorno;
}

/**
 * Funcion que busca si la empresa existe en nuestra base de datos
 *
 * @param txtNit	Text que contiene el Nit a buscar
 * @param option 	Segun la opcion introducida retorna la empresa si es encontrada,
 * 					( 0 => todas, 1 => Empleador, 2 => independiente ) 
 *    
 * @returns {empresa}
 */
function buscarNit(txtNit, option){
	if(validarNumero(txtNit,0)>0){return null;}
	
	var empresa=null;
	
	$.ajax({
		url: rutaPHP + 'buscarEmpresaNit.php',
		async: false,
		data:{v0:txtNit.val()},
		beforeSend: function(objeto){
        	dialogLoading('show');
        },        
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }            
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("En buscarNit paso lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
		success:function(datoEmpresa){
			if(datoEmpresa){				
				if(option==0){ empresa=datoEmpresa; return; }
				
				$.each(datoEmpresa,function(i,fila){
					empresa=fila; return false; 
					if(fila.idtipoafiliacion==3316){ if(option==1){ empresa=fila; return; }						
					} else { if(option==2){ empresa=fila; return; } }
				});
			}
		},
		timeout: tiempoFuera,
        type: "GET"
	});	
	
	return empresa;
}

/**
 * Funcion que busca si la empresa existe en nuestra base de datos
 *
 * @param idempresa 	Text que contiene el Nit a buscar * 
 *    
 * @returns {empresa}
 */
function buscarEmpresaID(ide){
	if(esNumeroRespuesta(ide)==false){ return false; }	
	
	var empresa=null;
	
	$.ajax({
		url: rutaPHP + 'buscarEmpresaID.php',
		async: false,
		data:{v0:ide},
		beforeSend: function(objeto){
        	dialogLoading('show');
        },        
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }            
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("En buscarNit paso lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
		success:function(datoEmpresa){
			if(datoEmpresa){ empresa=datoEmpresa; return; }		
		},
		timeout: tiempoFuera,
        type: "GET"
	});	
	
	return empresa;
}

/**
 * Comprueba si existen radicaciones pendientes de la empresa
 */
function radicacionPendienteEmpresa(txtNit){
	if(validarNumero(txtNit,0)>0){ return false; }
	
	var retorno=true;
	
	$.ajax({
		url: rutaPHP + 'buscarRadicacionPendienteEmpresa.php',
		async: false,
		data: {v0:txtNit.val(), v1:'30,31'},
		beforeSend: function(objeto){
        	dialogLoading('show');
        },        
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }            
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("En radicacionPendienteEmpresa paso lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
		success: function(resRadEmp) {
			if(resRadEmp==0){ retorno=false; }
		},
		timeout: tiempoFuera,
        type: "GET"
	});
	
	return retorno;
}

function buscarRadicacionPendienteAfiliado(tipoRadicacion, tipoDocumento, numero){
	if(esNumeroRespuesta(tipoRadicacion,0)==false || esNumeroRespuesta(tipoDocumento,0)==false || esNumeroRespuesta(numero,0)==false){ return -1; }
	
	var retorno=1;
	
	$.ajax({
		url: rutaPHP + 'buscarRadicacionPendienteAfiliado.php',
		async: false,
		data: {v0:tipoRadicacion, v1:tipoDocumento, v2:numero},
		beforeSend: function(objeto){
        	dialogLoading('show');
        },        
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }            
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("En buscarRadicacionPendienteAfiliado paso lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
		success: function(resRadPendiente) {
			if(resRadPendiente==0){ retorno=resRadPendiente; }
		},
		timeout: tiempoFuera,
        type: "GET"
	});
	
	return retorno;
}

/**
 * Busca si la persona es o era afiliada a Comfamiliar
 * 
 * @param idp 		Id de la persona a buscar
 * 
 * @returns 		Retorna boolean segun si la encuentra
 */
function esAfiliado(idp){
	if(!parseInt(idp)>0){ return false; }
	
	var retorno=false;
	
	$.ajax({
		url: rutaPHP + 'buscarEsAfiliado.php',
		type: 'GET',
		data: {v0:idp},
		async: false,
		dataType: 'json',
		success: function(datos){
			if(datos && parseInt(datos)!=0){
				idEmpresaGlobal=datos.idempresa;
				retorno=true;
			}
		}
	});
	
	return retorno;
}

/**
 * Calcula categoria segun S.M.L.V. traerlo de la session 
 * 
 * @params salario 		Salario del cual se calculara la categoria
 * @returns {Strings} categoria
 */
function calcularCategoria(salario){
	salario=parseInt(salario);	
	smlv=esNumeroRespuesta($('#txtSMLV').val())?$('#txtSMLV').val():616000;
	var sm=parseFloat(salario/smlv);
	
	if(sm<=2){
		return 'A';
	} else if(sm>2 && sm<=4){
		return 'B';
	} else if(sm>4){
		return 'C';
	} else {
		return 'D';
	}
}

function buscarBeneficiarios(idp,opt){
	if(esNumeroRespuesta(idp)==false){return null;}	
	var beneficiario=null;
	
	var php='buscarBeneficiarios.php';
	if(opt==0){ php='buscarBeneficiariosActivos.php'; }
	//Consulta todos los beneficiarios Activos, Pendientes...
	if(opt==1){ php='buscarBeneficiariosTodos.php'; }
	
	$.ajax({
		url: rutaPHP + php,
		async: false,
		data:{v0:idp},
		beforeSend: function(objeto){
        	dialogLoading('show');
        },        
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }            
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("En buscarBeneficiarios paso lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
		success:function(datoBeneficiario){
			if(datoBeneficiario){ beneficiario=datoBeneficiario }
		},
		timeout: tiempoFuera,
        type: "GET"
	});	
	
	return beneficiario;
}

function buscarConyuges(idp,opt){
	if(esNumeroRespuesta(idp)==false){return null;}	
	var beneficiario=null;
	
	var php='buscarConyuges.php';
	if(opt==0){ php='buscarConyugeConvive.php'; }
	
	$.ajax({
		url: rutaPHP + php,
		async: false,
		data:{v0:idp},
		beforeSend: function(objeto){
        	dialogLoading('show');
        },        
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }            
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("En buscarConyugeConvive paso lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
		success:function(datoBeneficiario){
			if(datoBeneficiario){ beneficiario=datoBeneficiario }
		},
		timeout: tiempoFuera,
        type: "GET"
	});	
	
	return beneficiario;
}

function comparaFecha(fechaMinima, fecha, fechaFin){
	var anno = parseInt(fecha.slice(0,4));
	var mes = mesNumero(fecha.slice(4,6));
	var annoFin = parseInt(fechaFin.slice(0,4));
	var mesFin = mesNumero(fechaFin.slice(4,6));
	var annoMinima = parseInt(fechaMinima.slice(0,4));
	var mesMinimo = fechaMinima.slice(5,7);
	var mesMinima = mesNumero(mesMinimo);
		
	if( anno < annoMinima || ( anno == annoMinima && mes < mesMinima ) ){
		if ( annoMinima > annoFin || ( annoMinima == annoFin && mesMinima > mesFin ) ) {
			return -1;
		} else {
			return annoMinima + mesMinimo;
		}
	} else {			
		return 0;
	}
}

function mesNumero(mes){
	if(mes.slice(0,1)=="0")
		return parseInt(mes.slice(1,2));
	else
		return parseInt(mes);
}