/** VARIABLES GLOBALES **/
URL=src();
nuevaRad=0;
smlv=0;
nRadicacion=null;
idPersonaGlobal=0;
idEmpresaGlobal=0;
copyTipoDoc=0;
copyDoc='';
copyNit='';
tiempoFuera=30000;
rutaPHP= '../radicacionnew/php/';
formularioTab='';

/** VARIABLES LOCALES **/


/** DOCUMENT READY **/
$(document).ready(function(){
	smlv=salarioMinimo();
	smlv=esNumeroRespuesta(smlv)?smlv:0;
	nuevoR();
	
	initTabs();
	enaAcordeon();
	enaDivDocDoble();
	
	shortcut.add("Shift+F",function() {		
		var url=URL+"aportes/trabajadores/consultaTrabajador.php";
	    window.open(url,"_blank");
	    },{
		'propagate' : true,
		'target' : document 
	});
});

/*** Funciones ***/

/**
 * Validar que haya hecho click en nuevo * 
 */
function iniciarconN(){
	if(nuevaRad==0){
		$("#cmbIdTipoRadicacion").val('0').trigger("change");
		alert("Haga click primero en Nuevo!");		
		return false;
	} else
		return true;
}

/**
 * Funcion que permite iniciar la radicacion
 */
function nuevoR(){	
	//desAcordeon();
	$("#cmbHabeasData").removeAttr('disabled');
	limpiarCampos(0);
	nuevaRad=1;	
	nRadicacion=null;
	idPersonaGlobal=0;
	idEmpresaGlobal=0;
	copyTipoDoc=0;
	copyDoc='';
	var hra=hora().split(":");
	if(hra[0]>=17&&hra[1]>=30){ $("#horaDia").html("<b>Est&aacute;s en horario adicional</b>"); }
	
	$("#txtIdentificacion").focus();
	$("#txtFecha").val(fechaHoy());
	$("#txtHoraIni").val( hra[0] + ":" + hra[1] );
	$("#txtSMLV").val(smlv);
	$("#cmbIdTipoDocumento").val('1');
	$("#cmbIdTipoPresentacion").val('71');
	$("#cmbIdTipoRadicacion").val('0');
	$("#cmbHabeasData").val('0');
	$("table.tablero :input.ui-state-error,select.ui-state-error").removeClass("ui-state-error");
}

/** 
 * Funcion que se ejecuta si se desea acceder al accordion que contiene el detalle de la radicacion * 
 */
function abrirAcordeon(active){
	if(active==0){
		if(iniciarconN()){
			
			var error=validarCampos();
			if(error>0){
				$("#cmbIdTipoRadicacion").val('0').trigger("change");
				alert("Llene los campos obligatorios!");
				nRadicacion=null;
				return false;
			} else {
				abrirTabs();
				return true;
			}
		}
	} else if(active==2){		
		return true;
	} else 
		return false;
}

/** 
 * Funcion que se ejecuta si se desea acceder al accordion que contiene los documentos requeridos de la radicacion seleccionada * 
 */
function abrirAcordeonDocs(active){
	if(active==0){
		if(iniciarconN()){
			var error=validarCampos();
			if(error>0){
				$("#cmbIdTipoRadicacion").val('0').trigger("change");
				alert("Llene los campos obligatorios!");
				nRadicacion=null;
				return false;
			} else {
				if($.trim($('#docRequerido').html()).length) { return true; }
				else { return false; }				
			}
		}
	} else if(active==1){
		if($.trim($('#docRequerido').html()).length) { return true; }
		else { return false; }
	} else
		return false;
}

/**
 * Valida los campos comunes antes de guardar
 */ 
function validarCampos(){
	var error=0;
	nRadicacion=new Radicacion();
	nRadicacion=camposRadicacion(nRadicacion);
	
	nRadicacion.fecharadicacion=$("#txtFecha").val();
	nRadicacion.horainicio=$("#txtHoraIni").val();
	nRadicacion.horafinal=hora();
	nRadicacion.idtipodocumento=$("#cmbIdTipoDocumento").val();
	nRadicacion.identificacion=$("#txtIdentificacion").val();
	nRadicacion.idtipodocumentoafiliado=$("#cmbIdTipoDocumento").val();
	nRadicacion.numero=$("#txtIdentificacion").val();	
	nRadicacion.idtipopresentacion=$("#cmbIdTipoPresentacion").val();
	nRadicacion.idtiporadicacion=$("#cmbIdTipoRadicacion").val();
	nRadicacion.asignado="N";
	nRadicacion.digitalizada="N";
	nRadicacion.procesado="N";
	nRadicacion.anulada="N";
	nRadicacion.devuelto="N";
	nRadicacion.cierre="N";
	nRadicacion.habeas_data=$("#cmbHabeasData").val();
	
	
	$("table.tablero :input.ui-state-error,select.ui-state-error").removeClass("ui-state-error");
	
	error=validarSelect($("#cmbIdTipoDocumento"),error);
	error=validarTexto($("#txtIdentificacion"),error);
	error=validarSelect($("#cmbIdTipoPresentacion"),error);
	error=validarSelect($("#cmbIdTipoRadicacion"),error);
	/*
	 *  validar campo habeas data dependindo el tipo de radicacion.
	 */
	var array = [28,2926,29,30,4258,31];
	
	
	
	if(array.indexOf(parseInt($("#cmbIdTipoRadicacion").val()))>=0){
		error=validarSelect($("#cmbHabeasData"),error);
		if($("#cmbHabeasData").val()==0){
			 alert('Revise que el habeas data este chequeado; Verifique que fue presentado.');
		}
	}
	
	/*
	 * fin validacion habeas data
	 */
		
	if(error>0){		
		$("#titRadicacion").trigger('click');
		return error;
	} else 
		return 0;
}

/**
 * Guarda la radicacion dependiendo de los campos que contenga el objeto pasado por parametro
 */
function guardarRadicacion(objRadicacion){
	if (objRadicacion==null) { return 0; };
	str = JSON.stringify(objRadicacion);
	
	var retorno=0;
	
	$.ajax({
		url: rutaPHP+'guardarRadicacion.php',
		async: false,
		data: {v0:str},		
		beforeSend: function(objeto){
        	dialogLoading('show');
        },        
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }            
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("En guardarRadicacion Paso lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
		success: function(idRadicacionG){
			if(esNumeroRespuesta(idRadicacionG)){
				retorno=idRadicacionG;				
			}
		},
		timeout: tiempoFuera,
        type: "GET"
	});
	
	return retorno;
}

/**
 * Guarda la radicacion dependiendo de los campos que contenga el objeto pasado por parametro
 */
function guardarDevolucion(objRadicacion){
	if (objRadicacion==null) { return 0; };
	str = JSON.stringify(objRadicacion);
	
	var retorno=0;
	
	$.ajax({
		url: rutaPHP+'guardarDevolucion.php',
		async: false,
		data: {v0:str},		
		beforeSend: function(objeto){
        	dialogLoading('show');
        },        
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }            
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("En guardarRadicacion Paso lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
		success: function(idRadicacionG){
			if(esNumeroRespuesta(idRadicacionG)){
				retorno=idRadicacionG;				
			}
		},
		timeout: tiempoFuera,
        type: "GET"
	});
	
	return retorno;
}

/**
 * Actualiza la radicacion a procesada
 */
function cerrarRadicacion(idradicacionG){
	if( esNumeroRespuesta(idradicacionG)!=true ) { return false; }
	
	var retorno=false;
	
	$.ajax({
		url: rutaPHP + 'updateCerrarRadicacion.php',	
		async: false,
		data: {v0:idradicacionG},		
		beforeSend: function(objeto){
        	dialogLoading('show');
        },        
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }            
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("En cerrarRadicacion Paso lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,		
		success: function(rActualizada){
			if(esNumeroRespuesta(rActualizada)){ retorno=true; }
		},
		timeout: tiempoFuera,
        type: "GET"
	});
	
	return retorno;
}

/**
 * Anula la radicacion que es pasada por parametro
 */
function anularRadicacion(idradicacionG){
if( esNumeroRespuesta(idradicacionG)!=true ) { return false; }
	
	var retorno=false;
	
	$.ajax({
		url: rutaPHP + 'updateAnularRadicacion.php',	
		async: false,
		data: {v0:idradicacionG},		
		beforeSend: function(objeto){
        	dialogLoading('show');
        },        
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }            
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("En anularRadicacion Paso lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
		success: function(rActualizada){
			if(esNumeroRespuesta(rActualizada)){ retorno=true; }
		},
		timeout: tiempoFuera,
        type: "GET"
	});
	
	return retorno;
}

/**
 * Guarda la empresa segun los datos contenidos en el objeto pasado por parametro
 */
function guardarEmpresa(nEmpresa){
	if (nEmpresa==null) { return 0; }
	str = JSON.stringify(nEmpresa);
	
	var retorno=0;
	
	$.ajax({
		url: rutaPHP + 'guardarEmpresa.php',
		async: false,
		data: {v0:str},		
		beforeSend: function(objeto){
        	dialogLoading('show');
        },        
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }            
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("En guardarEmpresa Paso lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
		success: function(idEmpresaG){
			if(esNumeroRespuesta(idEmpresaG)){ retorno=idEmpresaG; }
		},
		timeout: tiempoFuera,
        type: "GET"
	});
	
	return retorno;
}

/**
 * Actualiza la empresa completamente segun los datos contenidos en el objeto pasado por parametro
 */
function actualizarEmpresa(nEmpresa){
	if (nEmpresa==null) { return 0; }
	str = JSON.stringify(nEmpresa);
	
	var retorno=0;
	
	$.ajax({
		url: rutaPHP + 'updateEmpresa.php',
		async: false,
		data: {v0:str},		
		beforeSend: function(objeto){
        	dialogLoading('show');
        },        
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }            
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("En guardarEmpresa Paso lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
		success: function(idEmpresaG){
			if(esNumeroRespuesta(idEmpresaG)){ retorno=idEmpresaG; }
		},
		timeout: tiempoFuera,
        type: "GET"
	});
	
	return retorno;
}

/**
 * Actualiza la empresa a independiente, con sus campos correspondientes
 */
function actualizarEmpresaIndependiente(nEmpresa){
	if (nEmpresa==null) { return 0; }
	if(esNumeroRespuesta(nEmpresa.idempresa)==false) { return 0; }
	
	var retorno=0;
	
	$.ajax({
		url: rutaPHP + 'updateEmpresaIndependiente.php',
		async: false,
		data: {v0:nEmpresa.idempresa, v1:nEmpresa.estado, v2:nEmpresa.contratista, 
			v3:nEmpresa.claseaportante, v4:nEmpresa.tipoaportante, v5:nEmpresa.idtipoafiliacion},		
		beforeSend: function(objeto){
        	dialogLoading('show');
        },        
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }            
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("En actualizarEmpresaIndependiente Paso lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
		success: function(idEmpresaG){
			if(esNumeroRespuesta(idEmpresaG)){ retorno=idEmpresaG; }
		},
		timeout: tiempoFuera,
        type: "GET"
	});
	
	return retorno;
}

/**
 * Actualiza la empresa a contratista, con sus campos correspondientes
 */
function actualizarEmpresaContratista(nEmpresa){
	if (nEmpresa==null) { return 0; }
	if(esNumeroRespuesta(nEmpresa.idempresa)==false) { return 0; }
	
	var retorno=0;
	
	$.ajax({
		url: rutaPHP + 'updateEmpresaContratista.php',
		async: false,
		data: {v0:nEmpresa.idempresa, v1:nEmpresa.estado, v2:nEmpresa.contratista},		
		beforeSend: function(objeto){
        	dialogLoading('show');
        },        
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }            
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("En actualizarEmpresaContratista Paso lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
		success: function(idEmpresaG){
			if(esNumeroRespuesta(idEmpresaG)){ retorno=idEmpresaG; }
		},
		timeout: tiempoFuera,
        type: "GET"
	});
}

/**
 * Guarda la afiliacion dependiendo de los campos que contenga el objeto pasado por parametro
 */
function guardarAfiliacion(nAfiliacion){
	if (nAfiliacion==null) { return 0; }
	str = JSON.stringify(nAfiliacion);
	
	var retorno=0;		
	
	$.ajax({
		url: rutaPHP + 'guardarAfiliacion.php',
		async: false,
		data: {v0:str},		
		beforeSend: function(objeto){
        	dialogLoading('show');
        },        
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }            
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("En guardarAfiliacion Paso lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
		success: function(idAfiliacionG){
	  		if(esNumeroRespuesta(idAfiliacionG)){ retorno=idAfiliacionG; }		 				 	
		},
		timeout: tiempoFuera,
        type: "GET"
	});
	
	return retorno;
}

/**
 * Actualiza la afiliacion dependiendo de los campos que contenga el objeto pasado por parametro
 */
function actualizarAfiliacion(nAfiliacion){
	if (nAfiliacion==null) { return 0; }
	str = JSON.stringify(nAfiliacion);
	
	var retorno=0;		
	
	$.ajax({
		url: rutaPHP + 'updateAfiliacion.php',
		async: false,
		data: {v0:str},		
		beforeSend: function(objeto){
        	dialogLoading('show');
        },        
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }            
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("En actualizarAfiliacion Paso lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
		success: function(idAfiliacionG){
	  		if(esNumeroRespuesta(idAfiliacionG)){ retorno=idAfiliacionG; }		 				 	
		},
		timeout: tiempoFuera,
        type: "GET"
	});
	
	return retorno;
}

/** Inactiva la afiliacion del formulario recibido por parametro
 *  
 *  @return {int} ( 0 => No se pudo inactivar la afiliacion, 1 => No se pudo insertar la afiliacion inactiva,
 *  			    2 => Se inactivo el afiliado, 3 => se inactivo la empresa, 4 => error inactivando la empresa )
 **/
function inactivarAfiliaciones(aAfiliacion){
	if (aAfiliacion==null) { return 0; }
		
		var retorno=0;		
		
		$.ajax({
			url: rutaPHP + 'spInactivarAfiliacion.php',
			async: false,
			data: {v0:JSON.stringify(aAfiliacion), v1:3716},		
			beforeSend: function(objeto){
	        	dialogLoading('show');
	        },        
	        complete: function(objeto, exito){
	        	dialogLoading('close');
	            if(exito != "success"){
	                alert("No se completo el proceso!");
	            }            
	        },
	        contentType: "application/x-www-form-urlencoded",
	        dataType: "json",
	        error: function(objeto, quepaso, otroobj){
	            alert("En inactivarAfiliaciones Paso lo siguiente: "+quepaso);
	        },
	        global: true,
	        ifModified: false,
	        processData:true,
			success: function(datoInactivacion){
		  		if(esNumeroRespuesta(datoInactivacion)){ retorno=datoInactivacion; }		 				 	
			},
			timeout: tiempoFuera,
	        type: "GET"
		});
		
		return retorno;
}

/**
 * Cuenta si ya tiene primaria
 * 
 * @param idp
 * @returns {Number} (0 => error, 1 => Tiene primaria, 2 => no tiene primaria)
 */
function contarAfiliacionesPrimaria(idp){
	if(esNumeroRespuesta(idp)==false) { return 0; }
	
	var retorno=1;
	
	$.ajax({
		url: rutaPHP + 'buscarSiTienePrimaria.php',
		async: false,
		data:{v0:idp},		
		beforeSend: function(objeto){
        	dialogLoading('show');
        },        
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }            
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("En contarAfiliacionesPrimaria Paso lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
		success:function(contAfilia){
			if(parseInt(contAfilia)==0){ retorno=2; }
		},
		timeout: tiempoFuera,
        type: "GET"
	});
	
	return retorno;
}

/**
 * Cuenta si ya tiene primaria con la empresa
 * 
 * @param idp
 * @param ide
 * @returns {Number} (0 => error, 1 => Tiene primaria, 2 => La primaria es con la empresa)
 */
function contarSiAfiliacionEsPrimaria(idp,ide){
	if(esNumeroRespuesta(idp)==false) { return 0; }
	if(esNumeroRespuesta(ide)==false) { return 0; }
	
	var retorno=1;
	
	$.ajax({
		url: rutaPHP + 'buscarOtraAfiliacionPrimaria.php',
		async: false,
		data: {v0:idp, v1:ide},		
		beforeSend: function(objeto){
        	dialogLoading('show');
        },        
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }            
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("En contarSiAfiliacionEsPrimaria Paso lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
		success: function(contAfilia){
			if(parseInt(contAfilia)==0){ retorno=2; }
		},
		timeout: tiempoFuera,
        type: "GET"							
	});
	
	return retorno;
}

/**
 * Guarda el certificado dependiendo de los campos que contenga el objeto pasado por parametro
 */
function guardarCertificado(nCertificado){
	if (nCertificado==null) { return 0; }
	str = JSON.stringify(nCertificado);
	
	var retorno=0;		
	
	$.ajax({
		url: rutaPHP + 'guardarCertificado.php',
		async: false,
		data: {v0:str},		
		beforeSend: function(objeto){
        	dialogLoading('show');
        },        
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }            
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("En guardarCertificado Paso lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
		success: function(idCertificado){
	  		if(esNumeroRespuesta(idCertificado)){ retorno=idCertificado; }		 				 	
		},
		timeout: tiempoFuera,
        type: "GET"
	});
	
	return retorno;
}

/**
 * Guarda el grupoFamiliar dependiendo de los campos que contenga el objeto pasado por parametro
 */
function guardarGrupoFamiliar(nGrupoFamiliar){
	if (nGrupoFamiliar==null) { return 0; }
	str = JSON.stringify(nGrupoFamiliar);
	
	var retorno=0;		
	
	$.ajax({
		url: rutaPHP + 'guardarGrupoFamiliar.php',
		async: false,
		data: {v0:str},		
		beforeSend: function(objeto){
        	dialogLoading('show');
        },        
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }            
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("En guardarCertificado Paso lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
		success: function(idRelacion){
	  		if(esNumeroRespuesta(idRelacion)){ retorno=idRelacion; }		 				 	
		},
		timeout: tiempoFuera,
        type: "GET"
	});
	
	return retorno;
}

function updateCrearConvivencia(nGrupoFamiliar){
	if (nGrupoFamiliar==null) { return 0; }
	str = JSON.stringify(nGrupoFamiliar);
	
	var retorno=0;		
	
	$.ajax({
		url: rutaPHP + 'updateCrearConvivencia.php',
		async: false,
		data: {v0:str},		
		beforeSend: function(objeto){
        	dialogLoading('show');
        },        
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }            
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("En guardarCertificado Paso lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
		success: function(idRelacion){
	  		if(esNumeroRespuesta(idRelacion)){ retorno=idRelacion; }		 				 	
		},
		timeout: tiempoFuera,
        type: "GET"
	});
	
	return retorno;
}

/**
 * Disuelve las convivencias dependiendo de los campos que contenga el objeto pasado por parametro
 */
function updateDisolverConvivencia(nGrupoFamiliar){
	if (nGrupoFamiliar==null) { return 0; }
	str = JSON.stringify(nGrupoFamiliar);
	
	var retorno=0;		
	
	$.ajax({
		url: rutaPHP + 'updateDisolverConvivencia.php',
		async: false,
		data: {v0:str},		
		beforeSend: function(objeto){
        	dialogLoading('show');
        },        
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }            
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("En guardarCertificado Paso lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
		success: function(idRelacion){
	  		if(esNumeroRespuesta(idRelacion)){ retorno=idRelacion; }		 				 	
		},
		timeout: tiempoFuera,
        type: "GET"
	});
	
	return retorno;
}

/**
 * Guarda el ReclamoTrabajador dependiendo de los campos que contenga el objeto pasado por parametro
 */
function guardarReclamoTrabajador(nReclamoTrabajador){
	if (nReclamoTrabajador==null) { return 0; }
	str = JSON.stringify(nReclamoTrabajador);
	
	var retorno=0;		
	
	$.ajax({
		url: rutaPHP + 'guardarReclamoTrabajador.php',
		async: false,
		data: {v0:str},		
		beforeSend: function(objeto){
        	dialogLoading('show');
        },        
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }            
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("En guardarReclamoTrabajador Paso lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
		success: function(idReclamo){
	  		if(esNumeroRespuesta(idReclamo)){ retorno=idReclamo; }		 				 	
		},
		timeout: tiempoFuera,
        type: "GET"
	});
	
	return retorno;
}

/**
 * Guarda el ReclamoEmpresa dependiendo de los campos que contenga el objeto pasado por parametro
 */
function guardarReclamoEmpresa(nReclamoEmpresa){
	if (nReclamoEmpresa==null) { return 0; }
	str = JSON.stringify(nReclamoEmpresa);
	
	var retorno=0;		
	
	$.ajax({
		url: rutaPHP + 'guardarReclamoEmpresa.php',
		async: false,
		data: {v0:str},		
		beforeSend: function(objeto){
        	dialogLoading('show');
        },        
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }            
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("En guardarReclamoEmpresa Paso lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
		success: function(idReclamo){
	  		if(esNumeroRespuesta(idReclamo)){ retorno=idReclamo; }		 				 	
		},
		timeout: tiempoFuera,
        type: "GET"
	});
	
	return retorno;
}

/**
 * Guarda el ReclamoEmpresa dependiendo de los campos que contenga el objeto pasado por parametro
 */
function guardarBloqueo(nTarjeta){
	if (nTarjeta==null) { return 0; }
	var retorno=0;		
	
	$.ajax({
		url: URL + 'asopagos/novedades/bloquear.php',
		async: false,
		data: {v0:nTarjeta.idtarjeta,v1:nTarjeta.idetapa},		
		beforeSend: function(objeto){
        	dialogLoading('show');
        },        
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }            
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("En guardarBloqueo Paso lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
		success: function(idTarjeta){
	  		if(esNumeroRespuesta(idTarjeta)){ retorno=idTarjeta; }		 				 	
		},
		timeout: tiempoFuera,
        type: "GET"
	});
	
	return retorno;
}

/**
 * Guarda el ReclamoEmpresa dependiendo de los campos que contenga el objeto pasado por parametro
 */
function guardarNota(idtarjeta, nota){
	var retorno=false;		
	
	$.ajax({
		url: URL + 'asopagos/guardarNota.php',
		async: false,
		data: {v0:idtarjeta,v1:nota},		
		beforeSend: function(objeto){
        	dialogLoading('show');
        },        
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }            
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("En guardarNota Paso lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
		success: function(idNota){
	  		if(esNumeroRespuesta(idNota)){ retorno=true; }		 				 	
		},
		timeout: tiempoFuera,
        type: "GET"
	});
	
	return retorno;
}

/**
 * Guardar documentos requeridos en la radicacion de los campos que contenga el objeto pasado por parametro
 */
function guardarDocumentos(objDocumento, idRadicacionDocumentos, idPersonaDocumentos, programaDocumentos){
	if (objDocumento==null) { return 0; };
	str = JSON.stringify(objDocumento);
	
	var retorno=0;
	
	$.ajax({
		url: rutaPHP+'guardarDocumentos.php',
		async: false,
		data: {v0:str,v1:idRadicacionDocumentos,v2:idPersonaDocumentos,v3:programaDocumentos},		
		beforeSend: function(objeto){
        	dialogLoading('show');
        },        
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }            
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("En guardarRadicacion Paso lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
		success: function(cantDocumentos){
			if(esNumeroRespuesta(cantDocumentos)){
				retorno=cantDocumentos;				
			}
		},
		timeout: tiempoFuera,
        type: "GET"
	});
	
	return retorno;
}