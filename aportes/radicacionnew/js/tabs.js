/*** FUNCIONES ACORDEON ***/

/**
 * Deshabilitar el acordeon, forma inicial
 */
function desAcordeon(){
	$( "#accordion" ).accordion('destroy');
	$("#titRadicacion").css('display','none');
	$("#titDetRadicacion").css('display','none');
	$("#detRadicacion").css('display','none');
}

/**
 * Habilitar el acordeon, forma final
 */ 
function enaAcordeon(){
	$("#titRadicacion").css('display','block');
	$("#titDetRadicacion").css('display','block');
	$("#detRadicacion").css('display','block');
	
	$( "#accordion" ).accordion({
		autoHeight: false,
		//animated: "bounceslide",
		//navigation: true,
		event: false
	});
	
	$("#accordion h3").click(function(event, object){		
		if(event.currentTarget.id=="titDetRadicacion"){
			if(abrirAcordeon($('#accordion').accordion('option', 'active'))){
				$("#accordion").accordion("activate", 1);
			}
		} else if(event.currentTarget.id=="titRadicacion") {
			$("#accordion").accordion("activate", 0);
		} else {			
			if(abrirAcordeonDocs($('#accordion').accordion('option', 'active'))){
				$("#accordion").accordion("activate", 2);
			}
		}
	});
}


/*** FUNCIONES TABS ***/
var iniTab;
var ruta=URL+"aportes/radicacionnew/tabs/";
var rutaConsulta=URL+"aportes/radicacionnew/tabsConsulta/";


function continuarprocesohd(opt){
	if(opt.value==1){
		initRad($("#cmbIdTipoRadicacion").val());
	}
}



function initRad_aux(opt){
	
	if($("#cmbHabeasData").val()==1){
		initRad(opt.value);	
	}else{
		var arraydata = [28,2926,29,30,4258,31];
		if($(opt).attr('name')=="cmbIdTipoRadicacion" && (arraydata.indexOf(parseInt($(opt).val()))>=0)){
			$("#cmbHabeasData").focus();
			return false;
		}
		initRad(opt.value);
	}
	
	
	
    
	
	
	
}



/**
 * Inicializa la tab inicial segun el tipo de Radicacion
 * @param opt  //Tipo de radicacion
 */
function initRad(opt){
	
	
	if(opt=='0'){
		//desAcordeon();
	} else {
		if(iniciarconN()){
			var error=validarCampos();		
			if(error>0){		
				//$("#cmbIdTipoRadicacion").val('0');
				alert("Llene los campos obligatorios!");
				nRadicacion=null;			
				return false;
			} else {
				//enaAcordeon();
				
				removeTabs(0);
				eliminarDivsBasura();
				createTabsRad(opt);
				crearFormularioDocRequerido();
				
				$("#titDetRadicacion").trigger('click');		
			}
		}
	}
	
	if (opt != 28 && opt != 29 && opt != 30 && opt != 31 && opt != 192 && opt != 2926 && opt != 4258){
	//BUSCAR PERSONA 
	var tipodoc = $("#cmbIdTipoDocumento").val();
	var doc = $("#txtIdentificacion").val();	
	
	$.ajax({
		url: rutaPHP + 'buscarPersonaAct.php',
		cache: false,
		type: "POST",
		data: {v0:tipodoc,v1:doc},	
		dataType:"json",
		success: function(data){
			
			$("#txtDireccion").val(data.direccion);	
			$("#txtTelefono").val(data.telefono);
			$("#txtCelular").val(data.celular);	
			$("#txtEmail").val(data.email);	
			$("#cboDepto").val($.trim(data.iddepresidencia)).trigger('change');
			$("#cboCiudad").val($.trim(data.idciuresidencia)).trigger("change");
			$("#cboZona").val($.trim(data.idzona)).trigger("change");
			$("#cboBarrio").val($.trim(data.idbarrio));
			
		}
	});
	newActualizaPersona($("#tDocumento").val(),$("#txtNumeroP").val());
	}
}

/**
 * Inicializa la funcionalidad de los TABS
 */
function initTabs(){
	$("#tabs").tabs({
		cache: true,
		select: function(evt,ui){
			
		}
	});
}

/**
 * Remueve todas las TABS existentes 
 */
function removeTabs(f) {
	if(f==1){
		$("#tabs").tabs({ selected:0 });
	}
	
	var i=$("#tabs").tabs("length");
		  
	for(i; i>=f; i--){
		$("#tabs").tabs("remove",i);   
	}
}

/**
 * Tab minimo
 */
function tabMinimo(){
	var divUi=jQuery('div[id^=ui-tabs-]');
	
	if(divUi.length>0){
		var n = divUi[0].id.substring(8);
	
		if(!isNaN(n)){		    		        
			iniTab=n;
		}
	} else { iniTab=0; }
}

/**
 * Eliminar divs Basura 
 */
function eliminarDivsBasura(){
	/** Control de Basuras **/
	jQuery('div[aria-labelledby^=ui-dialog-title]').remove();
	jQuery('div[id^=div-datos]').remove();
	jQuery('div[id^=info]').remove();
	/** **/
	
	tabMinimo();
	var divUi=jQuery('div[id^=ui-tabs-]');
	$.each(divUi,function(i,fila){
	    var n=fila.id.substring(8);
	    if(!isNaN(n)){
	        if (n!=iniTab)
	            $("#"+fila.id).remove();
	    }	
	});
}

/**
 * Habilitar div Documentos Dobles
 */
function enaDivDocDoble(){
	$("#divDocumentoDoble").dialog({
		autoOpen:false,
		modal:true,
		width:500,
		height:150,
		resizable:true
	});
}

function enaDivMotivosDevolucion(){
	$("#divMotivosDevolucion").dialog({
		autoOpen:false,
		modal:true,
		width:500,
		height:150,
		resizable:true,
		open: function(){
			$("#cmbMotivosDevolucion").val(0);
		}
	});
	
	$("#divMotivosDevolucion").dialog('open');
} 

/**
 * Oculta divs
 */
function ocultarDivs(){
	var divUi=jQuery('div[id^=ui-tabs-]');
	$.each(divUi,function(i,fila){
	    var n=fila.id.substring(8);
	    if(!isNaN(n)){
	        if(parseInt(n)%2!=0)
	            $("#"+fila.id).remove();
	        else if (n!=iniTab)
	            $("#"+fila.id).addClass("ui-tabs-hide");
	    }	
	});
}

/**
 * Crea el TAB inicial seg�n el tipo de radicaci�n
 * 
 * @param opt  //Tipo de radicaci�n
 */
function createTabsRad(opt) {
	switch(opt){
		case '27':
			crearTab(ruta,'informacionTab','','Informaci&oacute;n');		   		   
		break;		
		case '28':
			crearTab(ruta,'dependienteTab','','Afiliaci&oacute;n');		   		   
		break;
		case '29':
			crearTab(ruta,'renovacionTab','','Renovaci&oacute;n');		   		   
		break;
		case '30':
			crearTab(ruta,'afiliacionEmpresaTab','','Afiliaci&oacute;n');		   		   
		break;
		case '31':
			crearTab(ruta,'renovacionEmpresaTab','','Renovaci&oacute;n');
		break;
		case '32':
			crearTab(ruta,'certificadoTab','','Certificado');
		break;
		case '33':
			crearTab(ruta,'novedadEmpresaTab','','Novedad');
		break;
		case '69':
			crearTab(ruta,'postulacionFonedeTab','','FONEDE');
		break;
		case '70':
			crearTab(ruta,'grupoFamiliarTab','','Grupo Familiar');
		break;
		case '105':
			crearTab(ruta,'sucursalEmpresaTab','','Sucursal');
		break;
		case '169':
			crearTab(ruta,'reclamoTrabajadorTab','?idRadicacion=169','Reclamo');
		break;
		case '4230':
			crearTab(ruta,'reclamoTrabajadorTab','?idRadicacion=4230','Reclamo');
		break;
		case '170':
			crearTab(ruta,'reclamoEmpresaTab','?idRadicacion=170','Reclamo');
		break;
		case '4231':
			crearTab(ruta,'reclamoEmpresaTab','?idRadicacion=4231','Reclamo');
		break;		
		case '183':
			crearTab(ruta,'defuncionTab','','Defunci&oacute;n');
		break;
		case '191':
			crearTab(ruta,'disolverConvivenciaTab','','Convivencia');
		break;
		case '192':
			crearTab(ruta,'afiliacionMultipleTab','','Multiple');
		break;
		case '195':
			crearTab(ruta,'bloqueoTarjetaTab','','Tarjeta');
		break;
		case '211':
			crearTab(ruta,'crearConvivenciaTab','','Convivencia');
		break;
		case '2919':
			crearTab(ruta,'embargoTab','','Embargo');		   		   
		break;
		case '2926':
			crearTab(ruta,'independienteTab','','Independiente');		   		   
		break;		
		case '4066':
			crearTab(ruta,'subsidioViviendaTab','','Pago Subsidio de Vivienda');		   		   
		break;
		case '4067':
			crearTab(ruta,'visitaHabitabilidadTab','','Visita Habitabilidad');		   		   
		break;
		case '4068':
			crearTab(ruta,'movilizacionTab','','Movilizaci&oacute;n');		   		   
		break;
		case '4157':
			crearTab(ruta,'postulacionSFVTab','','Postulaci&oacute;n');		   		   
		break;
		case '4158':
			crearTab(ruta,'actualizacionSFVTab','','Actualizaci&oacute;n');		   		   
		break;
		case '4258': //[4258][RE AFILIACION EMPRESA]
			crearTab(ruta,'reAfiliEmpreExpulTab','','Re Afiliaci&oacute;n Empresa Expulsada');
		break;
	}
}

/**
 * Crea las tabs Dependiento el tipo de radicacion seleccionada. * 
 */
function abrirTabs(){	
	var opt=$("#cmbIdTipoRadicacion").val();		
	tabMinimo();		
		
	switch(opt){
		case '27':
			eliminarDivsBasura();
			removeTabs(1);
			if(esAfiliado(idPersonaGlobal)){ crearTabsInformacion(idPersonaGlobal,idEmpresaGlobal); }
		break;		
	}
	
	$("#tabs").tabs({ selected:0 });
	ocultarDivs();
}

/** CREACION DE TABS SEGUN TIPO DE RADICACION **/

/**
 * Crea Tabs INFORMACION
 */
function crearTabsInformacion(idp, ide){
	if(!parseInt(idp)>0){ return false; }
	
	var param1='?v0='+idp;	
	crearTab(rutaConsulta,'fichaTab',param1,'Afiliado');
	crearTab(rutaConsulta,'reclamosTab',param1,'Reclamos');
	crearTab(rutaConsulta,'embargosTab',param1,'Descuentos');
	crearTab(rutaConsulta,'defuncionesTab',param1,'Defunciones');
	
	if(parseInt(ide)>0){
		var param2='?v0='+ide;
		crearTab(rutaConsulta,'aportesTab',param2,'Aportes');
	}
	
	crearTab(rutaConsulta,'planillaTab',param1,'Planilla');
	crearTab(rutaConsulta,'girosTab',param1,'Subsidio');
	crearTab(rutaConsulta,'tarjetaTab',param1,'Cargues');
	crearTab(rutaConsulta,'causalesTab',param1,'Causales');
	crearTab(rutaConsulta,'documentosTab',param1,'Documentos');
	crearTab(rutaConsulta,'documentosTab',param1,'Documentos');
	crearTab(rutaConsulta,'contenedorTab',param1,'Conyuge');
}

/**
 * Crea Tabs INFORMACION
 */
function crearTabsInformacionEmpresa(ide){
	if(!parseInt(ide)>0){ return false; }
	
	var param2='?v0='+ide;
	crearTab(rutaConsulta,'aportesTab',param2,'Aportes');
	crearTab(rutaConsulta,'TablaReclamosEmpTab',param2,'Reclamos');
}

/**
 * Crear Tabs
 * 
 * @param patch 	Ruta
 * @param php 		Nombre del archivo
 * @param title 	Titulo del Tab
 */
function crearTab(patch, php, params, title){
	$("#tabs").tabs("add",patch+php+".php"+params, title);
}

/**
 * Crear Formulario Documentos Requeridos
 */
function crearFormularioDocRequerido(){
	$('#docRequerido').html('');
	var opt=$("#cmbIdTipoRadicacion").val();		
	tabDocumento='';		
		
	switch(opt){
		case '28':
			formularioTab='dependienteTab';
			tabDocumento='docsDependienteTab.php';
		break;
		case '29':
			formularioTab='renovacionTab';
			tabDocumento='docsRenovacionTab.php';
		break;
		case '30':
			formularioTab='afiliacionEmpresaTab';
			tabDocumento='docsAfiliacionEmpresaTab.php';
		break;
		case '31':
			formularioTab='renovacionEmpresaTab';
			tabDocumento='docsRenovacionEmpresaTab.php';
		break;
		case '32':
			formularioTab='certificadoTab';
			tabDocumento='docsCertificadoTab.php';
		break;
		case '69':
			formularioTab='postulacionFonedeTab';
			tabDocumento='docsPostulacionFonedeTab.php';
		break;
		case '183':
			formularioTab='defuncionTab';
			tabDocumento='docsDefuncionTab.php';
		break;
		case '191':
			formularioTab='disolverConvivenciaTab';
			tabDocumento='docsDisolverConvivenciaTab.php';
		break;
		case '195':
			formularioTab='bloqueoTarjetaTab';
			tabDocumento='docsBloqueoTarjetaTab.php';
		break;
		case '211':
			formularioTab='crearConvivenciaTab';
			tabDocumento='docsCrearConvivenciaTab.php';
		break;
		case '2919':
			formularioTab='embargoTab';
			tabDocumento='docsEmbargoTab.php';
		break;
		case '2926':
			formularioTab='independienteTab';
			tabDocumento='docsIndependienteTab.php';
		break;
		case '4066':
			formularioTab='subsidioViviendaTab';
			tabDocumento='docsSubsidioViviendaTab.php';
		break;
		case '4067':
			formularioTab='visitaHabitabilidadTab';
			tabDocumento='docsVisitaHabitabilidadTab.php';
		break;
		case '4068':
			formularioTab='movilizacionTab';
			tabDocumento='docsMovilizacionTab.php';
		break;
		case '4157':
			formularioTab='postulacionSFVTab';
			tabDocumento='docsPostulacionSFVTab.php';
		break;
		case '4158':
			formularioTab='actualizacionSFVTab';
			tabDocumento='docsActualizacionSFVTab.php';
		break;
		case '4258': //[4258][RE AFILIACION EMPRESA]
			formularioTab='reAfiliEmpreExpulTab';
			tabDocumento='docsReAfiliEmpreExpulTab.php';
		break;
	}
	if(tabDocumento!=''){
		var retorno=false;
		$.ajax({
			url: ruta + tabDocumento,
			async: false,				
			beforeSend: function(objeto){
	        	dialogLoading('show');
	        },        
	        complete: function(objeto, exito){
	        	dialogLoading('close');
	            if(exito != "success"){
	                alert("No se completo el proceso!");
	            }            
	        },
	        contentType: "application/x-www-form-urlencoded",	        
	        error: function(objeto, quepaso, otroobj){
	            alert("Pas� lo siguiente: "+quepaso);
	        },	        		
			success: function(datoFormulario){
				if($.trim(datoFormulario).length>0){
					$('#docRequerido').html(datoFormulario);
					retorno=true;
				}
			},
			timeout: tiempoFuera
		});
		
		return retorno;
	} else {
		return false;
	}
}