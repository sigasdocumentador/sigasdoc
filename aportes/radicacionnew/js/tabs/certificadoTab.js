/** VARIABLES LOCALES **/
var idPersonaLocal=0;
var idBeneficiario=0;  
var folios=0;
var idCertiDepenEcono = 4551; //certificado dependencia economica de HIJASTROS

$(document).ready(function(){
	$("#cmbDiscapacidad").attr("disabled",true);
	$("#filaCertificados,#filaVigencias,#filaFechas,#imgCertificado").hide();
	
	$("#cmbIdTipoDocumentoAfiliado").val(copyTipoDoc);
	$("#txtNumero").val(copyDoc);
	
	$("#div-datosCertificados").dialog({
		autoOpen:false,
		width:640,
		show: "drop",
		hide: "clip",	
		open:function(event,ui){
		},
		close: function(event,ui){
			$("#div-datosCertificados table tbody").empty();
		}
	});
	
	setTimeout("$('#tabs').trigger('click');$('#cmbIdTipoDocumentoAfiliado').focus();",700);
});

/**
 * Busca la persona a afiliar
 * 
 * @param cmbtd 		combo que contiene el tipo de documento
 * @param txtd 			text que contiene el documento
 * @param tdnom 		td que contiene el nombre del afiliado
 * @returns {Boolean}
 */
function busquedaPersona(cmbtd, txtd, tdnom){
	idPersonaLocal=0;
	idBeneficiario=0;
	tdnom.html('');
	$("#cmbDiscapacidad").val("N");
	$("#cmbDiscapacidad").attr("disabled",true);
	$("#cmbIdBeneficiario option:not(':first')").remove();
	$("#filaCertificados,#filaVigencias,#filaFechas,#imgCertificado").hide();
	$("#txtFechaNacimientoAfiliado").val("");
	$("#cmbIdBeneficiario").val(0).trigger("change");
	
	var persona=buscarPersona(cmbtd, txtd, tdnom, false);
	if(persona==null || persona==false){ 
		txtd.val('');
		txtd.addClass("ui-state-error"); 
		return false; 
	}
	
	var opcion=comprobarTipoAfiliacion(persona.idpersona,1);
	
	if((opcion['0']-opcion['2'])>0){
		var afiliaciones = buscarAfiliacionesActivasAfiliado(persona.idpersona);
		var contA=0;
		
		if(afiliaciones!=null){
			$.each(afiliaciones,function(i,fila){					
			    if(fila.estado=="A"){ contA++; } 
			});
		}
		
		//if(contA>0){
			var beneficiarios=buscarBeneficiarios(persona.idpersona,0);
			
			if(beneficiarios==null){
				alert("La persona que esta radicando NO TIENE BENEFICIARIOS!!");
				txtd.val(''); tdnom.html('');
				txtd.addClass("ui-state-error");
			} else {
				if(persona.fechanacimiento==null || persona.fechanacimiento==""){
					alert("Es necesario que ACTUALICE LA FECHA DE NACIMIENTO de la PERSONA que esta radicando!!");
					txtd.val(''); tdnom.html('');
					txtd.addClass("ui-state-error");
				} else {
					idPersonaLocal=persona.idpersona;
					$("#txtFechaNacimientoAfiliado").val(persona.fechanacimiento);
					$.each(beneficiarios,function(i,fila){	
						var nom = fila.identificacion + " " + fila.pnombre + " " + fila.snombre + " " + fila.papellido + " " + fila.sapellido;
						var value= fila.idbeneficiario + "," + fila.idparentesco + "," + fila.fechanacimiento + "," + fila.fechaasignacion + "," + fila.idcertificado + "," + fila.capacidadtrabajo;
						$('#cmbIdBeneficiario').append("<option id=\"" + fila.idparentesco + "\" value=\"" + value + "\">" + nom + " (" + fila.parentesco + ")</option>");						
					});
				}
			}
		/*} else {
			alert("La persona que esta radicando solo tiene AFILIACION(ES) PENDIENTE(S)!!");
			txtd.val(''); tdnom.html('');
			txtd.addClass("ui-state-error");
			}*/
	} else {
		if (opcion['2']>0){
			alert("La persona que esta radicando tiene AFILIACION(ES) PENDIENTE(S) por PU!");			
		} else if (opcion['1']>0){
			alert("La persona que esta radicando esta INACTIVA!");
		} else {
			alert("La persona que esta radicando no tiene AFILIACIONES!");
		}
		
		txtd.val(''); tdnom.html('');
		txtd.addClass("ui-state-error");
	}	
}

/**
 * Mostrar certificados del beneficiario 
 */
function mostrarDatosCertificados(){
	if(!(parseInt(idBeneficiario)>0)){
		alert("Seleccione un beneficiario!!");
		return false;
	}
	
	$("#div-datosCertificados table tbody").empty();
	$.getJSON(URL+"phpComunes/buscarCertificados.php", {idBeneficiario: idBeneficiario}, function(data){
		if(data.length > 0){
			$("#div-datosCertificados").dialog('open');
			$.each(data,function(i,fila){
				var strTr = "<tr><td>"+ fila.idcertificado +"</td><td>"+ fila.tipo_certificado +"</td><td>"+ fila.periodoinicio +"</td><td>"+ fila.periodofinal +"</td><td>"+ fila.fechapresentacion +"</td></tr>";
				$("#div-datosCertificados table tbody").append(strTr);
			});
		}
	});
}

/**
 * Evento al seleccionar un beneficiario
 */
function onChangeBeneficiario(){
	idBeneficiario=0;
	
	$("#cmbDiscapacidad").attr("disabled",true);
	$("#filaCertificados,#filaVigencias,#filaFechas,#imgCertificado").hide();
	$("#filaDiscapacidad,#filaEscolaridad,#filaUniversidad,#filaSupervivencia,#cerVigencia, #filaDependenciaEconomica").hide();
	$("#fDiscapacidad,#fEscolaridad,#fUniversidad,#fSupervivencia,#fDependenciaEconomica,#pInicialV3,#pFinalV3").val('');
	$("#cDiscapacidad,#cEscolaridad,#cUniversidad,#cSupervivencia,#cDependenciaEconomica,#vigencia1,#vigencia2").attr("checked",false);
	$("#txtFechaNacimiento,#txtFechaAsignacion").val('');	
	
	var datoCmb = $("#cmbIdBeneficiario").val().split(',');
	
	if(datoCmb[0]>0){
		if(datoCmb[2]==null || datoCmb[2]==""){
			alert("Es necesario que ACTUALICE LA FECHA DE NACIMIENTO del BENEFICIARIO que esta radicando!!");
			$("#cmbIdBeneficiario").val(0);
		} else {
			idBeneficiario=datoCmb[0];			
			$("#txtIdParentesco").val(datoCmb[1]);
			$("#txtFechaNacimiento").val(datoCmb[2]);
			$("#txtFechaAsignacion").val(datoCmb[3]);
			$("#cmbDiscapacidad").val(datoCmb[5]).trigger("change");
			validarEdad(datoCmb[4]);
		}
	} 
}

/**
 * Evento al cambiar capacidad de trabajo
 */
function onChangeDiscapacidad(obj){
	var opt=obj.value;
	var datoCmb = $("#cmbIdBeneficiario").val().split(',');
	
	$('#btnGuardarRadicacion, #btnGuardarDevolucion, #trFolios').show();
	$(".trCERCertificadoDiscapacidad").hide();
	$(".trCERCertificadoDiscapacidad input[type=checkbox]").removeAttr('checked').trigger("change");
	
	if(datoCmb[5]=="I"){ 
		if(opt=="I"){			
			$("#filaSupervivencia,#cerVigencia").hide();
			$("#filaCertificados,#filaVigencias").hide();
		} else {
			$("#filaSupervivencia,#cerVigencia").show();
			$("#filaCertificados,#filaVigencias").show();			
		}
	} else {	
		if(opt=="I"){
			$("#filaCertificados,#filaVigencias").hide();
			$("#filaDiscapacidad,#cerVigencia,#filaFechas").hide();
			
			$(".trCERCertificadoDiscapacidad").show();
			$("#btnGuardarRadicacion, #btnGuardarDevolucion, #trFolios").show();
		} else {
			$("#filaDiscapacidad").hide();
			//Mostra campos certificados
			$("#filaCertificados,#filaVigencias").show();
			$("#cerVigencia,#filaFechas").show();
			if($("#filaEscolaridad").css('display')=='none' && $("#filaUniversidad").css('display')=='none' && $("#filaSupervivencia").css('display')=='none'){
				$("#filaCertificados,#filaVigencias,#cerVigencia").hide();
			}
		}
	}
	$("#fDiscapacidad,#fEscolaridad,#fUniversidad,#fSupervivencia,#pInicialV3,#pFinalV3").val('');
	$("#cDiscapacidad,#cEscolaridad,#cUniversidad,#cSupervivencia,#vigencia1,#vigencia2").attr("checked",false);
}

/**
 * Validacion de certificados que corresponde a su edad y parentesco
 */
function validarEdad(idcertificado){
	var parentesco = $("#txtIdParentesco").val();
	var fechaNaceBeneficiario = $("#txtFechaNacimiento").val();
	var fechaNaceAfiliado = $("#txtFechaNacimientoAfiliado").val();		
	
	salir=!validarFechaNaceBeneficiario(parentesco, fechaNaceBeneficiario, fechaNaceAfiliado);
	if(salir){ $("#cmbIdBeneficiario").val(0); return false; }
	
	var fechaNacimientoValEdad = new Date(fechaNaceBeneficiario);
	var tiempoFechaNace = fechaNacimientoValEdad.getTime();
	var fechaActualValEdad = new Date();
	var tiempoFechaActual = fechaActualValEdad.getTime();
	$("#filaFechas").show();
	
	if($("#cmbDiscapacidad").val()=="N"){
		if ( parentesco == 36 ) {
			var tiempoMS60Anios = 60 * 60 * 24 * (365) * 1000 * 60;
			var tiempoMS60AnioConVisiestos = tiempoMS60Anios + ( 60 * 60 * 24 * 1000 * (60 / 4));
			if(tiempoFechaNace  >= (tiempoFechaActual - tiempoMS60AnioConVisiestos)){
				alert("La Edad Minima de un Afiliado Padre es 60 A\u00F1os");						
			} else {
				$("#filaCertificados,#filaVigencias").show();
				$("#filaSupervivencia,#cerVigencia").show();
			}
		} else if ( parentesco == '35' || parentesco == '38' || parentesco == '37' ) {
			var tiempoMS19Anios = 60 * 60 * 24 * 365 * 1000 * 19;
			var tiempoMS19AnioConVisiestos = tiempoMS19Anios + (60 * 60 * 24 * 1000 * (19 / 4));					
			if(tiempoFechaNace  <= (tiempoFechaActual - tiempoMS19AnioConVisiestos)){					
				alert("La Edad Maxima de un Afiliado Hijo/Hijastro/Hermano es Hasta los 19 A\u00F1os");				
			} else {
				
				
				var tiempoMS12Anios = 60*60*24*365*1000*11;
				var tiempoMS12AnioConVisiestos = tiempoMS12Anios + ( 60 * 60 * 24 * 1000 * (11 / 4) );
				if(tiempoFechaNace  <= (tiempoFechaActual - tiempoMS12AnioConVisiestos)){
					$("#filaCertificados,#filaVigencias").show();
					$("#filaEscolaridad,#filaUniversidad,#cerVigencia").show();
				} else {
					alert("La Edad Minima de un Afiliado Hijo/Hijastro/Hermano para presentar un certificado es de 12 A\u00F1os");
				}
			}
		}
		
	} else {
		$("#filaCertificados,#filaVigencias").show();
		$("#filaSupervivencia,#cerVigencia").show();
	}
	
	// Habilitar el formulario para los certificados de dependencia economica para hijastros
	if(parentesco == '38' ){
		$("#filaCertificados,#filaVigencias").show();
		$("#filaDependenciaEconomica,#cerVigencia").show();
	}
	
	if(parseInt(idcertificado)>0){ $("#imgCertificado").show(); }
	$("#cmbDiscapacidad").attr("disabled",false);
}

/**
 * Evento al seleccionar un tipo de beneficiario
 */
function setFechaAsignaCert(chk, obj){
	$("#pInicialV3,#pFinalV3").val("");
	$("#vigencia1,#vigencia2").attr("checked",false);
	$("#fDiscapacidad,#fEscolaridad,#fUniversidad,#fSupervivencia,#fDependenciaEconomica,#pInicialV3,#pFinalV3").val('');
	
	if(chk.is(":checked")){
		var today=new Date();
		var dd = today.getDate();
		var mm = today.getMonth() + 1 ;
		mm = mm < 10 ? "0" + mm : mm;
		var yy = today.getFullYear();
		obj.val(yy + "-" + mm + "-" + dd);
	} else {
		obj.val("");
	}
}

/**
 * Validacion edad del beneficiario respecto al parentesco con el afiliado
 */
function validarFechaNaceBeneficiario(parentesco,fechaNaceBene,fechaNaceAfi){
	var fx=fechaNaceAfi.split('-');
	fnace = fx[2] + "/" + fx[1] + "/" + fx[0];
	fx = fechaNaceBene.split('-');
	fbenef = fx[2] + "/" + fx[1] + "/" + fx[0];				
	var dd=diferenciaFechas(fnace,fbenef);
	
	if(parentesco==35 && (dd >= 0)){
		alert("El beneficiario es Mayor que el afiliado?, por favor revise las fechas de nacimiento!");
		return false;										
	} else if(parentesco==36 && (dd <= 0)){					
		alert("El beneficiario es Menor que el afiliado?, por favor revise las fechas de nacimiento!");
		return false;										
	} else {
		return true;
	}
}

/**
 * Validacion fecha maxima del certificado, no sea superior al maximo de edad permitida
 */
function validarFechaLimiteEdad(fechaIni, fechaFin, annos, formu){
	var fechaNaceBeneficiario = $("#txtFechaNacimiento").val();
	var annoCumple = parseInt(fechaNaceBeneficiario.slice(0,4)) + annos;
	var mesCumple = fechaNaceBeneficiario.slice(5,7);
	var mesCumple2=mesNumero(mesCumple);
	var annoIni = parseInt(fechaIni.slice(0,4));
	var mesIni = mesNumero(fechaIni.slice(4,6));
	var annoFin = parseInt(fechaFin.slice(0,4));
	var mesFin = mesNumero(fechaFin.slice(4,6));
		
	if( annoCumple < annoIni || ( annoCumple == annoIni && mesCumple2 < mesIni ) ){				//Si cumplio antes de la fecha Inicial		
		alert("Ha salido del rango de edad permitido para presentacion del certificado" );		
		return "0";
	} else if( annoCumple > annoFin || ( annoCumple == annoFin && mesCumple2 >= mesFin ) ){		//Si cumplio despues de la fecha Final		
		return "1";
	} else {																					//Si cumplio dentro del periodo que cubre el certificado		
		return annoCumple + mesCumple;
	}
}

/**
 * Validaciones correspondientes a los periodos que aplicara el certificado
 */
function vVigencia(op){	
	$("#errorVig").empty();
	$("#pInicialV3,#pFinalV3").val("");
	var fechaActual = new Date();
	var annoActual = parseInt( fechaActual.getFullYear() );
	var mesActual = fechaActual.getMonth() + 1;
	var ciclo,tcert,idcert;
	var pi,pf,mesInicia,mesFin,mesPresenta;
	
	if ( $("#txtFechaAsignacion").val() == 0 ) {
		alert ("Es necesario que ACTUALICE LA FECHA DE ASIGNACION del BENEFICIARIO que esta radicando!!");
		$("#vigencia1,#vigencia2").attr("checked",false);
		return false;
	}
	
	if($("#cEscolaridad,#cSupervivencia,#cDiscapacidad").is(":checked")){
		ciclo=1;
		tcert = ( $("#cEscolaridad").is(":checked") ? 55 : ( $("#cSupervivencia").is(":checked") ? 57 : 58 ) );
		idcert = ( $("#cEscolaridad").is(":checked") ? 'tc0' : ( $("#cSupervivencia").is(":checked") ? 'tc3' : 'tc4' ) );
	} else if($("#cUniversidad").is(":checked")){
		tcert=56;
		if ( op == 0 ){
			if( mesActual >= 7 && mesActual <= 12 ){ ciclo=3; idcert= 'tc2'; }
			else { ciclo=2; idcert= 'tc1'; }
		} else {
			if(mesActual >= 7 && mesActual <= 12){ ciclo=2; idcert= 'tc1'; }
			else { ciclo=3; idcert= 'tc2'; }
		}
	
	} else if($("#cDependenciaEconomica").is(":checked")){ // Certificado dependencia economica para hijastros
		
		//ciclo=0;
		tcert = idCertiDepenEcono;
		//idcert = 'tc00';
		idcert= 'tc5'; 
		ciclo=1;
		
		/*if ( op == 0 ){
			if( mesActual >= 7 && mesActual <= 12 ){ ciclo=3; idcert= 'tc5'; }
			else { ciclo=2; idcert= 'tc6'; }
		} else {
			if(mesActual >= 7 && mesActual <= 12){ ciclo=2; idcert= 'tc6'; }
			else { ciclo=3; idcert= 'tc5'; }
		}*/
		
	} else {
		alert ("No Hay seleccionado un certificado");
		$("#vigencia1,#vigencia2").attr("checked",false);
		return false;
	}
	
	$.ajax({
		url: URL+"phpComunes/buscarVigencia.php",
		cache: false,
		type: "GET",
		dataType:"json",
		data: {v0:tcert,v1:ciclo},
		async: false,
		success: function(datos){
			if ( op == 0 ) {
				if ( parseInt(idBeneficiario) > 0 ) {
					$.ajax({
						url: URL+"phpComunes/buscarCertPeriodoAnterior.php",
						cache: false,
						type: "GET",
						dataType:"json",
						data: {v0:idBeneficiario,v1:ciclo},
						async: false,
						success: function(cont){
							if ( cont == 0 ) {
								if ( ciclo == 1 || ciclo == 2 ) { datos.mesinicia="01"; }
								else if ( ciclo == 3 ) { datos.mesinicia="07"; }							
							}
						}
					});
				} else {
					if ( ciclo == 1 || ciclo == 2 ) { datos.mesinicia="01"; }
					else if ( ciclo == 3 ) { datos.mesinicia="07"; }
				}
			}			
			
			pi=annoActual+datos.mesinicia;
			pf=annoActual+datos.mesfinal;
			
			mesInicia = (datos.mesinicia.slice(0,1) == "0") ? parseInt(datos.mesinicia.slice(-1)) : parseInt(datos.mesinicia);
			mesFin = (datos.mesfinal.slice(0,1) == "0") ? parseInt(datos.mesfinal.slice(-1)) : parseInt(datos.mesfinal);
			mesPresenta = "02";		//Quemado para validar que se puede presentar hasta mayo (mes datepicker) para hacerle retroactivo de marzo
			if (idcert == 'tc1') { mesPresenta = "08"; }
			mesPresenta = (mesPresenta.slice(0,1) == "0") ? parseInt(mesPresenta.slice(-1)) : parseInt(mesPresenta);			
		}
	});
	
	if( op == 0 ) {
		var tiempoActual = fechaActual.getTime();			
		var fechaFinal = fechaActual;
		//if ( idcert != 'tc1' ) { fechaFinal.setYear( annoActual + 1 ); }		
		fechaFinal.setMonth( mesFin - 1 );
		var tiempoMS2Meses = 60 * 60 * 24 * 30 * 1000 * 2;
		var tiempoFinal = fechaFinal.setTime(fechaFinal.getTime() + tiempoMS2Meses);
		fechaFinal.setTime( fechaFinal.getTime( )- tiempoMS2Meses );
		
		if ( tiempoActual > tiempoFinal ) {
			alert("Ha superado la fecha limite de presentacion del certificado ("+ pf +")");
			$("#vigencia1,#vigencia2").attr("checked",false);
			return false;
		} 
		
		if( idcert == 'tc0' || idcert == 'tc1' || idcert == 'tc2' || idcert == 'tc3' || idcert == 'tc4'
			|| idcert == 'tc5') {
			
			if( mesActual <= mesInicia || mesActual == mesInicia + 1) {
				mesInicia = (mesInicia < 10) ? "0" + mesInicia.toString() : mesInicia.toString();
				pi = annoActual + mesInicia;
			} else {
				var fechaHace2Meses = new Date(annoActual,mesActual-3,1);
				mesPeriodoI = fechaHace2Meses.getMonth() + 1;
				mesPeriodoI = (mesPeriodoI <10) ? "0" + mesPeriodoI.toString() : mesPeriodoI.toString();
				pi = annoActual + mesPeriodoI;					
			}
			mesFin = (mesFin == 1) ? 12 : mesFin;
			mesFin = (mesFin < 10) ? "0" + mesFin.toString() : mesFin.toString();
			pf = fechaFinal.getFullYear() + mesFin;								
		} else { pi = ""; pf = ""; }
		
		if ( idcert == 'tc0' || idcert == 'tc1' || idcert == 'tc2' || idcert == 'tc5') {
			var pfV=validarFechaLimiteEdad(pi, pf, 19, 0);			
			if ( pfV == "0" ) { return false; } 
			else if ( parseInt(pfV) > 1 ) { pf = pfV; }
		}
				
		$("#pInicialV3").val(pi);
		$("#pFinalV3").val(pf);
	} else {
		var tiempoActual = fechaActual.getTime();			
		var tiempoFinal = fechaActual;		
		tiempoFinal.setMonth( mesPresenta - 1 );
		
		if ( tiempoActual > tiempoFinal.getTime() ) {
			mf = annoActual + "-" + ((mesPresenta < 10) ? "0" + mesPresenta.toString() : mesPresenta.toString());
			alert("Ha superado la fecha limite de presentacion del certificado ("+ mf +")");
			$("#vigencia1,#vigencia2").attr("checked",false);
			return false;
		}
		
		if( idcert == 'tc0' || idcert == 'tc1' || idcert == 'tc2' || idcert == 'tc3' || idcert == 'tc4' || idcert == 'tc5') {
			
			if ( ( mesActual <= 2 && idcert != 'tc1' ) ) { annoActual--; }			
			var fechaHace2Meses = new Date(annoActual,mesActual-3,1);
			mesPeriodoI = fechaHace2Meses.getMonth() + 1;
			mesPeriodoI = (mesPeriodoI <10) ? "0" + mesPeriodoI.toString() : mesPeriodoI.toString();
			pi = annoActual + mesPeriodoI;
			
			//if ( ( mesActual <= 2 && idcert != 'tc1' ) ) { annoActual++; }			
			mesFin = (mesFin <10)?"0"+mesFin.toString():mesFin.toString();
			pf = annoActual + mesFin;
		} else { pi = ""; pf = ""; }
		
		if ( idcert == 'tc0' || idcert == 'tc1' || idcert == 'tc2' || idcert == 'tc5') {
			var pfV=validarFechaLimiteEdad(pi, pf, 19, 0);			
			if ( pfV == "0" ) { return false; } 
			else if ( parseInt(pfV) > 1 ) { pf = pfV; }
		}
				
		$("#pInicialV3").val(pi);
		$("#pFinalV3").val(pf);
	}
	
	var pfV=comparaFecha($("#txtFechaAsignacion").val(),pi,pf);			
	if ( pfV > 0 ) { $("#pInicialV3").val(pfV); }
	else if (pfV == -1 ) { 
		alert("Fecha de Asignacion Mayor que la fecha final del periodo");
		$("#pInicialV3,#pFinalV3").val(""); $("#vigencia1,#vigencia2").attr("checked",false); 
	}
}

/**
 * Guardar la Radicacion
 */
function saveRadicacion(){
	if(iniciarconN()){
		if(validarTodosLosCampos()){		
			var nCertificado=new Certificado();
			nCertificado=llenarLasVariables(nCertificado);
			
			if(nCertificado==null){
				nRadicacion=null;
				alert("Se encontr\u00F3 errores en el procesamiento de su solicitud.");				
				return false;
			} else {	
				nRadicacion.idradicacion=guardarRadicacion(nRadicacion);
				if(esNumeroRespuesta(nRadicacion.idradicacion)){
					var resFolios=1;
					if(listDocumentos instanceof Array && listDocumentos.length>0){
						var resFolios=guardarDocumentos(listDocumentos, nRadicacion.idradicacion, idPersonaLocal, "S");
					}					
					
					if(esNumeroRespuesta(resFolios)){					
						$("#txtHoraFin").val(nRadicacion.horafinal);
						nCertificado.idcertificado=guardarCertificado(nCertificado);
						
						if($("#cmbDiscapacidad").val()=="N"){cerrarRadicacion(nRadicacion.idradicacion)}
						/*if(esNumeroRespuesta(nCertificado.idcertificado)){*/						
							/*if(cerrarRadicacion(nRadicacion.idradicacion)){*/
								alert("Radicaci\u00F3n guardada y cerrada correctamente!! \nRADICACI\u00D3N # "+nRadicacion.idradicacion);
								$("#txtIdRadicacion").css({ color: "RED"});
								$("#txtIdRadicacion").val(nRadicacion.idradicacion);
								
								if(confirm("Desea grabar otro certificado")==false){
									nuevaRad=0;
									$("#titRadicacion").trigger('click');						
									return true;
								} else {
									var datoCmb = $("#cmbIdBeneficiario").val().split(',');								
									var newvalue= datoCmb[0] + "," + datoCmb[1] + "," + datoCmb[2] + "," + datoCmb[3] + "," + datoCmb[4] + "," + $("#cmbDiscapacidad").val();
									$("#cmbIdBeneficiario :selected").val(newvalue);
									$("#cmbIdBeneficiario").val(0).trigger("change");
								}
							/*} else {
								alert("Ocurrio un error al cerrar la radicacion!!" +
										"\nPOR FAVOR reporte este numero a soporte:" +
										"\# "+nRadicacion.idradicacion);
							}*/
						/*} else {
							if(anularRadicacion(nRadicacion.idradicacion)){
								alert("No se ha guardado el certificado!!" +
										"\nPOR FAVOR vuelva a intentarlo");
							} else {
								alert("Ocurrio un error al anular la radicacion!!" +
										"\nPOR FAVOR reporte este numero a soporte:" +
										"\# "+nRadicacion.idradicacion);
							}						
							return false;
						}*/
					} else {
						if(anularRadicacion(nRadicacion.idradicacion)){
							alert("Ocurrio un error al guardar los Documentos!!" +
									"\nPOR FAVOR vuelva a intentarlo");
						} else {
							alert("Ocurrio un error al guardar los Documentos y al tratar de anular la radicacion!!" +
									"\nPOR FAVOR reporte este numero a soporte:" +
									"\# "+nRadicacion.idradicacion);
						}											
						return false;
					}
				} else {
					alert("Radicaci\u00F3n NO FUE GUARDADA!!");
					return false;
				}
			}
		}
	}
}

function saveDevolucion(opt){
	if(iniciarconN()){
		if(validarTodosLosCampos(opt)){
			if(opt==1) { enaDivMotivosDevolucion(); return false; }			
			
			var idMotivoDevolucion=$("#cmbMotivosDevolucion").val();
			llenarLasVariablesRadicacion(1);
			nRadicacion.motivodevolucion=idMotivoDevolucion;
			$("#divMotivosDevolucion").dialog('close');
			
			nRadicacion.idradicacion=guardarDevolucion(nRadicacion);
			if(esNumeroRespuesta(nRadicacion.idradicacion)){
				nuevaRad=0;
				$("#txtHoraFin").val(nRadicacion.horafinal);
				alert("Devoluci\u00F3n guardada correctamente!! \nRADICACI\u00D3N # "+nRadicacion.idradicacion);												
						
				$("#txtIdRadicacion").css({ color: "RED"});
				$("#txtIdRadicacion").val(nRadicacion.idradicacion);
				$("#titRadicacion").trigger('click');
				return true;
			} else {
				alert("Devoluci\u00F3n NO FUE GUARDADA!!"); 
				return false;
			}			
		}
	}
}

/**
 * Valida todos los campos necesarios para la radicacion 
 */
function validarTodosLosCampos(opt){
	var error=validarCampos();
	
	error=validarSelect($("#cmbIdTipoDocumentoAfiliado"),error);
	if(esNumeroRespuesta(idPersonaLocal)==false){ $("#txtNumero").val(''); }
	error=validarTexto($("#txtNumero"),error);
	if(esNumeroRespuesta(idBeneficiario)==false){ $("#cmbIdBeneficiario").val(0).trigger("change"); }
	error=validarSelect($("#cmbIdBeneficiario"),error);
	if(($('#accordion').accordion('option', 'active')==1 && $("#txtFolios").is(":visible")==true) || ($('#accordion').accordion('option', 'active')==2 && $(".certificadoTab .requerido:visible").length==0)){
		error=validarNumero($("#txtFolios"),error);
	}
	
	/*if($("input[name=certificado]:checked").attr("value")==undefined){	error++;
		$("input[name=certificado]").each(function(){ $(this).parent().addClass("ui-state-error"); });
	} else { $("input[name=certificado]").each(function(){ $(this).parent().removeClass("ui-state-error"); }); }
	
	error=validarTexto($("#pInicialV3"),error);
	error=validarTexto($("#pFinalV3"),error);*/
	
	if(error>0){
		if($('#accordion').accordion('option', 'active')==2){
			$("#titDetRadicacion").trigger('click');
		}		
		alert("Llene los campos obligatorios!");
		nRadicacion=null;
		return false;
	} else {
		if(opt==1){ return true; }
		else if(opt==2){
			error=validarSelect($("#cmbMotivosDevolucion"),error);
			if(error>0){
				nRadicacion=null;
				return false;
			} else {
				return true;
			}
		}
		
		if( validarExisteCertificado(idBeneficiario, $("#pInicialV3").val(), $("#pFinalV3").val()) ){
			alert("YA EXISTE un certificado para el Beneficiario en ese Periodo!");
			return false;
		} else {
			if(($('#accordion').accordion('option', 'active')==1 && $("#txtFolios").is(":visible")==false) || ($('#accordion').accordion('option', 'active')==2 && $(".certificadoTab .requerido:visible").length>0)){
				folios=validarRequeridos();
				if(esNumeroRespuesta(folios)){ return true; }
				else {
					nRadicacion=null;
					return false;
				}
			} else {
				return true;
			}
		}
	}	
}

/***
 * Valida si existe ya un certificado para el periodo seleccionado
 */
function validarExisteCertificado(v0, v2, v3){
	var retorno = true;	
	v1 = ( $("#cEscolaridad").is(":checked") ? 55 : ( $("#cUniversidad").is(":checked") ? 56 : ( $("#cSupervivencia").is(":checked") ? 57 : 58 ) ) );
	
	$.ajax({
		url:URL+"phpComunes/validarExisteCertificado.php",
		cache: false,
		type: "GET",
		dataType:"json",
		data:{ v0:v0, v1:v1, v2:v2, v3:v3 },
		async: false,
		success:function(datosC){
			if( datosC == 0 ) {
				retorno = false;
			}			
		}
	});
	
	return retorno;
}

function llenarLasVariablesRadicacion(opt){
	nRadicacion.idtipodocumentoafiliado=$("#cmbIdTipoDocumentoAfiliado").val();
	nRadicacion.numero=$("#txtNumero").val();
	
	if(($('#accordion').accordion('option', 'active')==1 && $("#txtFolios").is(":visible")==true) || ($('#accordion').accordion('option', 'active')==2 && $(".certificadoTab .requerido:visible").length==0)){
		nRadicacion.folios=$("#txtFolios").val();
	} else {
		nRadicacion.folios=folios;
	}
	
	nRadicacion.notas=$.trim($("#txtNotas").val());
	nRadicacion.idtipocertificado=( $("#cEscolaridad").is(":checked") ? 55 : ( $("#cUniversidad").is(":checked") ? 56 : ( $("#cSupervivencia").is(":checked") ? 57 : ( $("#cDependenciaEconomica").is(":checked") ? idCertiDepenEcono : 58) ) ) );
	nRadicacion.idbeneficiario=$("#cmbIdBeneficiario").val().split(',')[0];
	
	if(opt==1){
		nRadicacion.folios=0;
		nRadicacion.asignado="S";
		nRadicacion.procesado="S";
		nRadicacion.anulada="S";
		nRadicacion.devuelto="S";
		nRadicacion.cierre="S";
	}
}

/**
 * LLena las variables necesarias para la radicacion
 */
function llenarLasVariables(nCertificado){
	llenarLasVariablesRadicacion(0);
	
	nCertificado=camposCertificado(nCertificado);
	nCertificado.idbeneficiario=nRadicacion.idbeneficiario;
	nCertificado.idparentesco=$("#cmbIdBeneficiario").val().split(',')[1];
	nCertificado.idtipocertificado=nRadicacion.idtipocertificado;
	nCertificado.periodoinicio=$("#pInicialV3").val();
	nCertificado.periodofinal=$("#pFinalV3").val();
	nCertificado.formapresentacion='P';
	nCertificado.estado='A';
	
	return nCertificado;
}