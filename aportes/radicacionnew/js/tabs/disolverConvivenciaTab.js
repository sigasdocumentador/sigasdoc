/** VARIABLES LOCALES **/
var folios=0;
var idPersonaLocal=0;
var idBeneficiario=0;
var conyuges=null;

$(document).ready(function(){
	$("#cmbIdTipoDocumentoAfiliado,#cmbIdTipoDocumentoConyuge").val(copyTipoDoc);
	$("#txtNumero").val(copyDoc);
	
	$("#tbRelacionesActivas").empty();
	setTimeout("$('#tabs').trigger('click');$('#cmbIdTipoDocumentoAfiliado').focus();",700);
});

/**
 * Busca la persona a afiliar
 * 
 * @param cmbtd 		combo que contiene el tipo de documento
 * @param txtd 			text que contiene el documento
 * @param tdnom 		td que contiene el nombre del afiliado
 * @returns {Boolean}
 */
function busquedaPersona(cmbtd, txtd, tdnom){
	conyuges=null;
	idPersonaLocal=0;
	idBeneficiario=0;
	tdnom.html('');
	$("#tbRelacionesActivas").empty();
	
	var persona=buscarPersona(cmbtd, txtd, tdnom, false);
	if(persona==null || persona==false){ 
		txtd.val('');
		txtd.addClass("ui-state-error"); 
		return false; 
	}
	
	var beneficiarios=buscarConyuges(persona.idpersona,0);
	
	if(beneficiarios==null){
		alert("La persona que esta radicando NO TIENE CONVIVENCIA ACTIVA!!");				
		txtd.val(''); tdnom.html('');
		txtd.addClass("ui-state-error");								
	} else {
		var cadena="";
		$.each(beneficiarios,function(i,fila){
			idBeneficiario=fila.idconyuge;
			nom = fila.pnombre + " " +fila.snombre +" "+ fila.papellido + " " + fila.sapellido;
			cadena+="<tr>" +
						"<td>"+fila.codigo+"</td>" +
						"<td>"+fila.identificacion+"</td>" +
						"<td>"+nom+"</td>" +
						"<td><center>"+fila.detalledefinicion+"</center></td>" +
						"<td><center>"+fila.conviven+"</center></td>" +
					"</tr>";			
		});
		
		$("#tbRelacionesActivas").append(cadena);
		conyuges=beneficiarios;
		idPersonaLocal=persona.idpersona;						
	}	
}

/**
 * Guardar la Radicacion
 */
function saveRadicacion(){
	if(iniciarconN()){
		if(validarTodosLosCampos()){		
			var nGrupoFamiliar=new GrupoFamiliar();
			nGrupoFamiliar=llenarLasVariables(nGrupoFamiliar);
			
			if(nGrupoFamiliar==null){
				nRadicacion=null;
				alert("Se encontr\u00F3 errores en el procesamiento de su solicitud.");				
				return false;
			} else {	
				nRadicacion.idradicacion=guardarRadicacion(nRadicacion);
				if(esNumeroRespuesta(nRadicacion.idradicacion)){
					var resFolios=guardarDocumentos(listDocumentos, nRadicacion.idradicacion, nGrupoFamiliar.idtrabajador, "S");
					
					if(esNumeroRespuesta(resFolios)){
						nuevaRad=0;
						$("#txtHoraFin").val(nRadicacion.horafinal);
						nGrupoFamiliar.idrelacion=updateDisolverConvivencia(nGrupoFamiliar);
						
						if(esNumeroRespuesta(nGrupoFamiliar.idrelacion)){
							if(cerrarRadicacion(nRadicacion.idradicacion)){
								alert("Radicaci\u00F3n guardada y cerrada correctamente!! \nRADICACI\u00D3N # "+nRadicacion.idradicacion);
								$("#titRadicacion").trigger('click');
								$("#txtIdRadicacion").css({ color: "RED"});
								$("#txtIdRadicacion").val(nRadicacion.idradicacion);
							} else {
								alert("Ocurrio un error al cerrar la radicacion!!" +
										"\nPOR FAVOR reporte este numero a soporte:" +
										"\# "+nRadicacion.idradicacion);
							} 
						} else {
							if(anularRadicacion(nRadicacion.idradicacion)){
								alert("Ocurrio un error al disolver la relacion!!" +
										"\nPOR FAVOR vuelva a intentarlo");
							} else {
								alert("Ocurrio un error al anular la radicacion!!" +
										"\nPOR FAVOR reporte este numero a soporte:" +
										"\# "+nRadicacion.idradicacion);
							}						
							return false;
						}
					} else {
						if(anularRadicacion(nRadicacion.idradicacion)){
							alert("Ocurrio un error al guardar los Documentos!!" +
									"\nPOR FAVOR vuelva a intentarlo");
						} else {
							alert("Ocurrio un error al anular la radicacion!!" +
									"\nPOR FAVOR reporte este numero a soporte:" +
									"\# "+nRadicacion.idradicacion);
						}						
						return false;
					}
				} else {
					alert("Radicaci\u00F3n NO FUE GUARDADA!!");
					return false;
				}
			}
		}
	}
}

function saveDevolucion(opt){
	if(iniciarconN()){
		if(validarTodosLosCampos(opt)){
			if(opt==1) { enaDivMotivosDevolucion(); return false; }			
			
			var idMotivoDevolucion=$("#cmbMotivosDevolucion").val();
			llenarLasVariablesRadicacion(1);
			nRadicacion.motivodevolucion=idMotivoDevolucion;
			$("#divMotivosDevolucion").dialog('close');
			
			nRadicacion.idradicacion=guardarDevolucion(nRadicacion);
			if(esNumeroRespuesta(nRadicacion.idradicacion)){
				nuevaRad=0;
				$("#txtHoraFin").val(nRadicacion.horafinal);
				alert("Devoluci\u00F3n guardada correctamente!! \nRADICACI\u00D3N # "+nRadicacion.idradicacion);												
						
				$("#txtIdRadicacion").css({ color: "ORANGE"});
				$("#txtIdRadicacion").val(nRadicacion.idradicacion);
				$("#titRadicacion").trigger('click');
				return true;
			} else {
				alert("Devoluci\u00F3n NO FUE GUARDADA!!"); 
				return false;
			}			
		}
	}
}

/**
 * Valida todos los campos necesarios para la radicacion 
 */
function validarTodosLosCampos(opt){
	var error=validarCampos();
	
	if($("#tbRelacionesActivas tr").length==0){ $("#txtNumero").val(''); }	
	error=validarSelect($("#cmbIdTipoDocumentoAfiliado"),error);
	if(esNumeroRespuesta(idPersonaLocal)==false){ $("#txtNumero").val(''); }
	error=validarTexto($("#txtNumero"),error);		
	
	if(error>0){
		$("#titDetRadicacion").trigger('click');
		alert("Llene los campos obligatorios!");
		nRadicacion=null;
		return false;
	} else {
		if(opt==1){ return true; }
		else if(opt==2){
			error=validarSelect($("#cmbMotivosDevolucion"),error);
			if(error>0){
				nRadicacion=null;
				return false;
			} else {
				return true;
			}
		}
		
		if(confirm("Esta seguro de disolver la relaci\u00F3n de convivencia?")==true){
			folios=validarRequeridos();
			if(esNumeroRespuesta(folios)){ return true; }
			else {
				nRadicacion=null;
				return false;
			}
		} else {
			return false;
		}
	}
}

function llenarLasVariablesRadicacion(opt){
	nRadicacion.idtipodocumentoafiliado=$("#cmbIdTipoDocumentoAfiliado").val();
	nRadicacion.numero=$("#txtNumero").val();
	nRadicacion.notas=$.trim($("#txtNotas").val());	
	nRadicacion.idbeneficiario=idBeneficiario;
	
	if(opt==1){
		nRadicacion.folios=0;
		nRadicacion.asignado="S";
		nRadicacion.procesado="S";
		nRadicacion.anulada="S";
		nRadicacion.devuelto="S";
		nRadicacion.cierre="S";
	}
}

/**
 * LLena las variables necesarias para la radicacion
 */
function llenarLasVariables(nGrupoFamiliar){
	llenarLasVariablesRadicacion(0);	
	
	nGrupoFamiliar=camposGrupoFamiliar(nGrupoFamiliar);
	nGrupoFamiliar.idtrabajador=idPersonaLocal;
	nGrupoFamiliar.idbeneficiario=idBeneficiario;
	nGrupoFamiliar.idparentesco="38";
	nGrupoFamiliar.idconyuge=idBeneficiario;
	nGrupoFamiliar.conviven="N";
	nGrupoFamiliar.estado="I";	
	nGrupoFamiliar.idmotivo="3718";
		
	return nGrupoFamiliar;
}