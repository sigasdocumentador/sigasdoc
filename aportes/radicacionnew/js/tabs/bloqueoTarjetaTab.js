/** VARIABLES LOCALES **/
var folios=0;
var idTarjeta=0;
var idPersonaLocal=0;
var identidadVerificada=false;

//$("input[name^=respuesta].correcta:checked").length

$(document).ready(function(){
	$("#cmbIdTipoDocumentoAfiliado").val(copyTipoDoc);
	$("#txtNumero").val(copyDoc);
	
	$("input[name=radTipoNovedad]").bind("change",function(){
		if(esNumeroRespuesta(idPersonaLocal)==false){ 
			alert("Ingrese los datos del afiliado");
			$("input[name=radTipoNovedad]").attr("checked",false);
			return false;
		}
		$("[name=radTipoNovedad]").parent().removeClass("ui-state-error");
		var tipoNovedad = $(this).attr("id").replace("radTipoNovedad","");
		$("select[id^=cmbNovedad]").val(0).attr("disabled",true);	
		$("#cmbNovedad" + tipoNovedad).attr("disabled",false);
		
		switch(tipoNovedad){
			case 'Bloqueo':			
				$(".trBuscarPorVerificar").hide();
				break;
			case 'Suspencion': case 'Habilitacion':
				if(identidadVerificada==false){	$(".trBuscarPorVerificar").show(); }				
				break;
		}
	});
	
	$("ol[id^=olPreguntaSobre]").html("");
	$("input[name=radTipoNovedad]").attr("checked",false);
	$("select[id^=cmbNovedad]").val(0).attr("disabled",true);
	$(".trBuscarPor,#trBuscarPorPersona,#trBuscarPorBono,.trBuscarPorVerificar").hide();
	setTimeout("$('#tabs').trigger('click');$('#cmbBuscarPor').focus();",700);
	if($('#cmbIdTipoRadicacion').val()=='195'){ $('#pSuspencion,#pHabilitacion').css('display','none'); }
	
	setTimeout("$('#btnGuardarRadicacion').css('display','block');" +
			"$('.trBTTDenuncia, .trBTTTarjetaDeteriorada').hide();" +
			"$('.trBTTDenuncia input[type=checkbox]').removeAttr('checked').trigger('change');" +
			"$('.trBTTTarjetaDeteriorada input[type=checkbox]').removeAttr('checked').trigger('change');", 500);
});

function onChangeBuscarPor(obj){
	var opt=obj.value;
	limpiarCamposTarjeta();
	$(".trBuscarPor,#trBuscarPorPersona,#trBuscarPorBono,.trBuscarPorVerificar").hide();
	
	if(opt==1){
		$(".trBuscarPor,#trBuscarPorPersona").show();
	} else if(opt==2){
		$(".trBuscarPor,#trBuscarPorBono").show();
	} 
}

function limpiarCamposTarjeta(){
	idTarjeta=0;
	idPersonaLocal=0;	
	identidadVerificada=false;
	$(".trBuscarPor td[id^=td]").html("");
	$("ol[id^=olPreguntaSobre]").html("");
	$("input[name=radTipoNovedad]").attr("checked",false);
	$("select[id^=cmbNovedad]").val(0).attr("disabled",true);
	
	if($('#cmbIdTipoRadicacion').val()=='195'){ 
		$("#radTipoNovedadBloqueo").attr("checked",true);
		$("#cmbNovedadBloqueo").attr("disabled",false);
	}
}

function busquedaPersona(cmbtd, txtd, tdnom){
	limpiarCamposTarjeta();	
	
	var persona=buscarPersona(cmbtd, txtd, tdnom, false, 8);
	if(persona==null || persona==false){ 
		txtd.val('');
		txtd.addClass("ui-state-error"); 
		return false; 
	} else {
		var contA=0; var contB=0;
		$.each(persona,function(i,fila){			
			var nom = "&nbsp;" + fila.identificacion + " - " + $.trim(fila.pnombre) + " " + $.trim(fila.snombre) + " " + $.trim(fila.papellido) + " " + $.trim(fila.sapellido);					
			idPersonaLocal=fila.idpersona;
			tdnom.html(nom);
			$("#tdBono").html("&nbsp;" + fila.bono);
			$("#tdEstado").html("&nbsp;" + fila.estadoTarjeta);
			$("#tdFechaSolicitud").html("&nbsp;" + fila.fechasolicitud);
			$("#tdSaldo").html("&nbsp;" + formatCurrency(fila.saldo));
			$("#tdUbicacion").html("&nbsp;" + fila.ubicacionAgencia);
			
			if(fila.estadoTarjeta=="A" || fila.estadoTarjeta=="S"){ 
				idTarjeta=fila.idtarjeta; contA++; return false; 
			} else if(fila.estadoTarjeta=="B"){ contB++; }
		});
		
		var salir=false;
		if(contA==0 && contB>0){ 
			alert("La tarjeta tiene estado BLOQUEADO, no puede continuar.");
			salir=true;
		} else if(contA==0){
			alert("No tiene tarjeta asociada, no puede continuar.");
			salir=true;
		}
			
		if(salir){
			txtd.val('');
			txtd.addClass("ui-state-error");
			$(".trBuscarPor td[id^=td]").html("");			 
			return false;
		} else {
			generarPreguntasVerificacion(idPersonaLocal);
		}
	}
}

function busquedaBono(cmbtd, txtd, tdnom){
	limpiarCamposTarjeta();
	
	var persona=buscarPersona(cmbtd, txtd, tdnom, false, 9);
	if(persona==null || persona==false){ 
		txtd.val('');
		txtd.addClass("ui-state-error"); 
		return false; 
	} else {
		var contA=0; var contB=0;
		$.each(persona,function(i,fila){			
			var nom = "&nbsp;" + fila.identificacion + " - " + $.trim(fila.pnombre) + " " + $.trim(fila.snombre) + " " + $.trim(fila.papellido) + " " + $.trim(fila.sapellido);					
			idPersonaLocal=fila.idpersona;
			tdnom.html(nom);
			$("#tdBono").html("&nbsp;" + fila.bono);
			$("#tdEstado").html("&nbsp;" + fila.estadoTarjeta);
			$("#tdFechaSolicitud").html("&nbsp;" + fila.fechasolicitud);
			$("#tdSaldo").html("&nbsp;" + formatCurrency(fila.saldo));
			$("#tdUbicacion").html("&nbsp;" + fila.ubicacionAgencia);
			
			if(fila.estadoTarjeta=="A" || fila.estadoTarjeta=="S"){ 
				idTarjeta=fila.idtarjeta; contA++; return false; 
			} else if(fila.estadoTarjeta=="B"){ contB++; }
		});
		
		var salir=false;
		if(contA==0 && contB>0){ 
			alert("La tarjeta tiene estado BLOQUEADO, no puede continuar.");
			salir=true;
		} else if(contA==0){
			alert("No tiene tarjeta asociada, no puede continuar.");
			salir=true;
		}
			
		if(salir){
			txtd.val('');
			txtd.addClass("ui-state-error");
			$(".trBuscarPor td[id^=td]").html("");	
			limpiarCamposTarjeta();
			return false;
		} else {
			generarPreguntasVerificacion(idPersonaLocal);
		}
	}	
}

function generarPreguntasVerificacion(idp){
	$.getJSON(rutaPHP + "generarPreguntasVerificacion.php",{ v0: idp }, function(response){
		$.each( response, function( c, pregunta ) {
			pregunta.desordenar = function() {
				for ( var i = this.length-1; i > 0; i-- ) {
					var j = Math.floor( i * Math.random() );
					var tmp = this[ j ];
					this[ j ] = this[ i ];
					this[ i ] = tmp;
				}
				return this;
			};			
			pregunta.desordenar();
			$("#olPreguntaSobre" + c).html("");
			$.each( pregunta, function( ind, respuesta ) {
				var clase = ( respuesta.correcta ) ? "correcta":"";
				if( c == "saldotarjeta" ) respuesta.respuesta = formatCurrency(respuesta.respuesta);
				var campoRespuesta = "<li>" +
				    					"<input type='radio' name='radRespuesta" + c + "' class='"+ clase +"' />" +
				    					"<label>" + respuesta.respuesta + "</label>" +
				    				"</li>";

				$("#olPreguntaSobre" + c).append( campoRespuesta );
			});
		});
	});
}

function onChangeNovedad(obj){
	var opt=obj.value;
	
	if (opt==2865) {
		$(".trBTTTarjetaDeteriorada").show();
		$(".trBTTDenuncia, #btnGuardarRadicacion").hide();
		$(".trBTTDenuncia input[type=checkbox]").removeAttr('checked').trigger("change");		
	} else if (opt==2866 || opt==2867) {
		$(".trBTTDenuncia").show();
		$(".trBTTTarjetaDeteriorada, #btnGuardarRadicacion").hide();
		$(".trBTTTarjetaDeteriorada input[type=checkbox]").removeAttr('checked').trigger("change");
	} else {
		$('#btnGuardarRadicacion').css('display','block');
		$(".trBTTDenuncia, .trBTTTarjetaDeteriorada").hide();
		$(".trBTTDenuncia input[type=checkbox]").removeAttr('checked').trigger("change");
		$(".trBTTTarjetaDeteriorada input[type=checkbox]").removeAttr('checked').trigger("change");
	}
}

function onClickVerificar(){
	var opcionesRespuesta = $(".correcta");
	var seleccionesOk = $("[class=correcta]:checked");
	if((opcionesRespuesta.length - seleccionesOk.length) >= seleccionesOk.length){
		alert("Hay respuestas incorrectas. La verificaci\xf3n de seguridad NO FUE APROBADA.");
		identidadVerificada = false;
	} else {
		alert(seleccionesOk.length + " de " + opcionesRespuesta.length + " respuestas correctas. La verificaci\xf3n de seguridad FUE APROBADA.");
		$(".trBuscarPorVerificar").hide();
		identidadVerificada = true;
	}
}

function saveRadicacion(){
	if(iniciarconN()){
		if(validarTodosLosCampos()){
			var nTarjeta=new Tarjeta();
			nTarjeta=llenarLasVariables(nTarjeta);
			
			if(nTarjeta==null || nTarjeta==false){				
				nRadicacion=null;
				alert("Se encontr\u00F3 errores en el procesamiento de su solicitud.");				
				return false;
			} else {
				nRadicacion.idradicacion=guardarRadicacion(nRadicacion);
				if(esNumeroRespuesta(nRadicacion.idradicacion)){
					var resFolios=1;
					if(listDocumentos instanceof Array && listDocumentos.length>0){
						var resFolios=guardarDocumentos(listDocumentos, nRadicacion.idradicacion, idPersonaLocal, "S");
					}
					
					if(esNumeroRespuesta(resFolios)){
						$("#txtHoraFin").val(nRadicacion.horafinal);
						nuevaRad=0;
						
						var resBloqueo=guardarBloqueo(nTarjeta);
						
						if(resBloqueo==2){
							alert("Lo lamento, no se pudo grabar el registro de bloqueo. Es posible que haya un bloqueo pendiente de procesar.\n Por favor comunicar al Administrador del sistema.");
							$("#txtIdRadicacion").css({ color: "ORANGE"});
							$("#txtIdRadicacion").val(nRadicacion.idradicacion);
							$("#titRadicacion").trigger('click');
						} else if(resBloqueo==1){
							if(guardarNota(nTarjeta.idtarjeta,nRadicacion.notas)){
								alert("Radicaci\u00F3n guardada correctamente!! \nLa tarjeta (id. "+ nTarjeta.idtarjeta +") fue BLOQUEADA, no olvide generar el plano.");
							} else {
								alert("Radicaci\u00F3n guardada correctamente!! \nLa tarjeta (id. "+ nTarjeta.idtarjeta +") fue BLOQUEADA, no olvide generar el plano. \nNO SE PUDO GUARDAR LAS NOTAS, por favor comunicar al Administrador del Sistema.");
							}
							
							$("#txtIdRadicacion").css({ color: "RED"});
							$("#txtIdRadicacion").val(nRadicacion.idradicacion);
							$("#titRadicacion").trigger('click');
						} else {
							if(anularRadicacion(nRadicacion.idradicacion)){
								alert("Ocurrio un error al Guardar el Bloqueo!!" +
										"\nPOR FAVOR vuelva a intentarlo");
							} else {
								alert("Ocurrio un error al Anular la Radicacion!!" +
										"\nPOR FAVOR reporte este numero a soporte:" +
										"\# "+nRadicacion.idradicacion);
							}						
							return false;
						} 
						
						return true;
					} else {
						alert("Ocurrio un error al guardar los Documentos!!" +
							"\nPOR FAVOR vuelva a intentarlo");											
						return false;
					}
				} else {
					alert("Radicaci\u00F3n NO FUE GUARDADA!!");
					return false;
				}
			}
		}
	}
}

function validarTodosLosCampos(){
	var error=validarCampos();
	var idNovedad=0;
	
	error=validarSelect($("#cmbBuscarPor"),error);
	if(esNumeroRespuesta(idTarjeta)==false){ $("#txtNumero,#txtBono").val(''); }
	if(esNumeroRespuesta(idPersonaLocal)==false){ $("#txtNumero,#txtBono").val(''); }
	
	if(error==0){
		if($("#cmbBuscarPor").val()==1){
			error=validarSelect($("#cmbIdTipoDocumentoAfiliado"),error);
			error=validarTexto($("#txtNumero"),error);
		} else if($("#cmbBuscarPor").val()==2){
			error=validarTexto($("#txtBono"),error);	
		}
		
		if(error<validarChecked($("[name=radTipoNovedad]:checked"),$("[name=radTipoNovedad]"),error)){
			error++;
		} else {
			var tipoNovedad = ($("[name=radTipoNovedad]:checked").get(0).id).substring(7);
			if(error<validarSelect($("#cmb" + tipoNovedad),error)){
				error++;
			} else if((tipoNovedad=="NovedadSuspencion" || tipoNovedad=="NovedadHabilitacion") && identidadVerificada==false){
				alert("Debe verificar la identidad del afiliado por medio de las preguntas del formulario.");
				error++;
			} else {
				idNovedad=$("#cmb" + tipoNovedad).val();
			}
		}
	}
	
	if(error<validarTextArea($("#txtNotas"),error)){
		error++;
	} else if($.trim($("#txtNotas").val()).length<10){
		alert("Las notas son muy cortas (min. 10 caracteres).!");
		$("#txtNotas").addClass("ui-state-error");
		error++;		
	}
	
	if(error>0){
		$("#titDetRadicacion").trigger('click');
		alert("Llene los campos obligatorios!");
		nRadicacion=null;
		return false;
	} else {
		folios=validarRequeridos();
		if(esNumeroRespuesta(folios)){ return true; }
		else {
			if (idNovedad!=2865 && idNovedad!=2866 && idNovedad!=2867) { 
				return true; 
			} else {
				nRadicacion=null;
				return false;
			}
		}
	}
}

function llenarLasVariables(nTarjeta){
	nRadicacion.notas=$.trim($("#txtNotas").val());
	nRadicacion.folios=folios;
	
	nTarjeta=camposTarjeta(nTarjeta);
	nTarjeta.idtarjeta=idTarjeta;
	
	var tipoNovedad = ($("[name=radTipoNovedad]:checked").get(0).id).substring(7);
	nTarjeta.idetapa = $("#cmb" + tipoNovedad).val();
	
	return nTarjeta;	
}