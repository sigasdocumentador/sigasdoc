/** VARIABLES LOCALES **/
var idPersonaLocal=0;
var idEmpresaLocal=0;
var folios=0;
var afiliacionesPendientesPU=false;
var esPU=false;
var idPU=0;

$(document).ready(function(){
	$("#cmbIdTipoDocumentoAfiliado").val(copyTipoDoc);
	$("#txtNumero").val(copyDoc);
	
	actDatepicker($("#txtFechaIngreso"));
	
	$("#txtFechaIngreso").datepicker('option', 'yearRange','1900:+1');
	$("#txtFechaIngreso").datepicker('option','maxDate', '+2M');
	setTimeout("$('#tabs').trigger('click');$('#cmbIdTipoDocumentoAfiliado').focus();",700);
});

/**
 * Busca la persona a afiliar
 * 
 * @param cmbtd 		combo que contiene el tipo de documento
 * @param txtd 			text que contiene el documento
 * @param tdnom 		td que contiene el nombre del afiliado
 * @returns {Boolean}
 */
function busquedaPersona(cmbtd, txtd, tdnom){
	idPersonaLocal=0;
	idEmpresaLocal=0;
	afiliacionesPendientesPU=false;
	tdnom.html('');
	$('#txtNit').val('');
	$("#lblRazonSocial").html('');
	$('#cmbTipoAfiliacion').val('18');
	removeTabs(1);
	eliminarDivsBasura();
	
	var persona=buscarPersona(cmbtd, txtd, tdnom, false);
	if(persona==null || persona==false){ 
		txtd.val('');
		txtd.addClass("ui-state-error"); 
		return false; 
	}
	
	var opcion=comprobarTipoAfiliacion(persona.idpersona);
	
	if(opcion==0){
		alert("Ocurri\u00F3 un error comprobando el tipo de Afiliaci\u00F3n");
		txtd.val(''); tdnom.html('');
		txtd.addClass("ui-state-error");
	} else if (opcion==3 || opcion==4){
		alert("La persona que esta radicando es NUEVA, la Radicaci\u00F3n se cambia a AFILIACI\u00D3N!");
		$("#cmbIdTipoRadicacion").val('28').trigger('change');		
	} else if(opcion==1 || opcion==2) {
		var afiliaciones = buscarAfiliacionesActivasAfiliado(persona.idpersona);
		var contI=0;
		
		if(afiliaciones!=null){
			$.each(afiliaciones,function(i,fila){					
			    if(fila.tipoafiliacion!=18){ contI++; } 
			    document.getElementById('idEmpresa').value=fila.idempresa;
			    
			    
			});
		}
		
		if(contI>=1){
			alert("Tiene afiliaci\u00F3n como Independiente!!");
			txtd.val(''); tdnom.html('');
			txtd.addClass("ui-state-error");
		} else {
			idPersonaLocal=persona.idpersona;				
			
			crearTabsInformacion(idPersonaLocal, 0);
			ocultarDivs();
			var benes = buscarBeneficiarios(idPersonaLocal,0);
			if(benes!=null){
				$("#cmbTipoFormulario").val(48);
			}else{
				$("#cmbTipoFormulario").val(49);
			}
			if (opcion==1)
				afiliacionesPendientesPU=true;		
		}
	}
	document.getElementById('idPersona').value=idPersonaLocal;
}

/**
 * Busca la empresa donde labora el afiliado
 * 
 * @param txtNit 			Nit de la empresa
 * @param lblRazonSocial 	Nombre de la empresa
 * @returns {Boolean}
 */
function busquedaEmpresa(txtNit, lblRazonSocial){
	var comprobar=false;
	idEmpresaLocal=0;
	esPU=false;
	idPU=0;
	lblRazonSocial.html('');
	txtNit.removeClass("ui-state-error");
	$('#cmbTipoAfiliacion').val('18');
	
	if(esNumeroRespuesta(idPersonaLocal)==false){
		txtNit.val('');
		txtNit.addClass("ui-state-error");
		$("#txtNumero").val('');
		$("#txtNumero").addClass("ui-state-error");
		$("#tdNombreCompletoAfiliado").html('');
		return false; 
	}
	if(validarNumero(txtNit,0)>0){return null;}
	
	var opcion=comprobarAfiliacion(txtNit, idPersonaLocal);
			
	if(opcion==2 && afiliacionesPendientesPU){
		idPU=comprobarEsPU(txtNit, idPersonaLocal);
		comprobar=esNumeroRespuesta(idPU);
		esPU=comprobar;
	}
	if(opcion==3) { comprobar=true; }
	
	if(comprobar){
		var empresa=buscarNit(txtNit, 1);
		
		if(empresa==null || empresa==false){
			alert("La Empresa de tipo Empleador no Existe!!");
			txtNit.val('');
			txtNit.addClass("ui-state-error");
			return false; 
		} else {
			var salir = false;		
			if ( empresa.contratista=='S' ) {
				alert("La empresa es CONTRATISTA!!"); salir = true;
			} else if ( empresa.estado=='I' ) {
				alert("La empresa esta INACTIVA!!"); salir = true;
			} else if ( empresa.estado=='P' ) {
				if ( ( empresa.legalizada=="N" && radicacionPendienteEmpresa(txtNit)==false ) ) { 
					alert("La empresa esta PENDIENTE por PU!!");	salir = true;
				} 
			}		
			if ( salir == true ) {
				txtNit.val('');	txtNit.addClass("ui-state-error");
				return false; 
			}
			/*
			if(empresa.claseaportante==2875)
			{ 
				$('#cmbTipoAfiliacion').val('3320'); 
			}
			*/

			idEmpresaLocal=empresa.idempresa;			
			lblRazonSocial.html(empresa.razonsocial);
		    document.getElementById('idEmpresaForm').value=idEmpresaLocal;
			
		}
	} else {
		alert("Existen afiliaciones entre la persona y La Empresa!!");
		txtNit.val('');
		txtNit.addClass("ui-state-error");
		return false;
	}
}

/**
 * Guardar la Radicacion
 * 
 * @returns {Boolean}
 */
function saveRadicacion(){
	if(iniciarconN()){
		if(validarTodosLosCampos()){		
			var nAfiliacion=new Afiliacion();
			nAfiliacion=llenarLasVariables(nAfiliacion);
			
			if(nAfiliacion==null){
				nRadicacion=null;
				alert("Se encontr\u00F3 errores en el procesamiento de su solicitud.");				
				return false;
			} else {	
				nRadicacion.idradicacion=guardarRadicacion(nRadicacion);
				if(esNumeroRespuesta(nRadicacion.idradicacion)){
					var resFolios=guardarDocumentos(listDocumentos, nRadicacion.idradicacion, nAfiliacion.idpersona, "S");
					
					if(esNumeroRespuesta(resFolios)){
						nuevaRad=0;
						$("#txtHoraFin").val(nRadicacion.horafinal);
						nAfiliacion.idradicacion=nRadicacion.idradicacion;			
						if(esPU==false) {						
							nAfiliacion.idformulario=guardarAfiliacion(nAfiliacion);
						} else {
							nAfiliacion.idformulario=idPU;
							nAfiliacion.porplanilla='S';
							nAfiliacion.idformulario=actualizarAfiliacion(nAfiliacion);
						}
						
						if(esNumeroRespuesta(nAfiliacion.idformulario)){						
							alert("Radicaci\u00F3n guardada correctamente!! \nRADICACI\u00D3N # "+nRadicacion.idradicacion);						
							
							$("#txtIdRadicacion").css({ color: "RED"});
							$("#txtIdRadicacion").val(nRadicacion.idradicacion);
							$("#titRadicacion").trigger('click');						
							return true;
						} else {
							if(anularRadicacion(nRadicacion.idradicacion)){
								alert("Ocurrio un error!!" +
										"\nPOR FAVOR vuelva a intentarlo");
							} else {
								alert("Ocurrio un error!!" +
										"\nPOR FAVOR reporte este numero a soporte:" +
										"\# "+nRadicacion.idradicacion);
							}						
							return false;
						}
					} else {
						if(anularRadicacion(nRadicacion.idradicacion)){
							alert("Ocurrio un error al guardar los Documentos!!" +
									"\nPOR FAVOR vuelva a intentarlo");
						} else {
							alert("Ocurrio un error al guardar los Documentos y al tratar de anular la radicacion!!" +
									"\nPOR FAVOR reporte este numero a soporte:" +
									"\# "+nRadicacion.idradicacion);
						}							
						return false;
					}
				} else {
					alert("Radicaci\u00F3n NO FUE GUARDADA!!");
					return false;
				}
			}
		}
	}
}

function saveDevolucion(opt){
	if(iniciarconN()){
		if(validarTodosLosCampos(opt)){
			if(opt==1) { enaDivMotivosDevolucion(); return false; }			
			
			var idMotivoDevolucion=$("#cmbMotivosDevolucion").val();
			llenarLasVariablesRadicacion(1);
			nRadicacion.motivodevolucion=idMotivoDevolucion;
			$("#divMotivosDevolucion").dialog('close');
			
			nRadicacion.idradicacion=guardarDevolucion(nRadicacion);
			if(esNumeroRespuesta(nRadicacion.idradicacion)){
				nuevaRad=0;
				$("#txtHoraFin").val(nRadicacion.horafinal);
				alert("Devoluci\u00F3n guardada correctamente!! \nRADICACI\u00D3N # "+nRadicacion.idradicacion);												
						
				$("#txtIdRadicacion").css({ color: "RED"});
				$("#txtIdRadicacion").val(nRadicacion.idradicacion);
				$("#titRadicacion").trigger('click');
				return true;
			} else {
				alert("Devoluci\u00F3n NO FUE GUARDADA!!"); 
				return false;
			}			
		}
	}
}

/**
 * Valida todos los campos necesarios para la radicacion 
 * 
 * @returns {Boolean}
 */
function validarTodosLosCampos(opt){
	var error=validarCampos();
	
	error=validarSelect($("#cmbIdTipoDocumentoAfiliado"),error);
	if(esNumeroRespuesta(idPersonaLocal)==false){ $("#txtNumero").val(''); }
	error=validarTexto($("#txtNumero"),error);
	if(esNumeroRespuesta(idEmpresaLocal)==false){ $("#txtNit").val(''); }
	error=validarTexto($("#txtNit"),error);		
	
	error=validarSelect($("#cmbTipoFormulario"),error);
	error=validarSelect($("#cmbTipoAfiliacion"),error);
	error=validarTexto($("#txtFechaIngreso"),error);	
	error=validarNumero($("#txtSalario"),error);
	
	if(error>0){
		$("#titDetRadicacion").trigger('click');
		alert("Llene los campos obligatorios!");
		nRadicacion=null;
		return false;
	} else {
		if(opt==1){ return true; }
		else if(opt==2){
			error=validarSelect($("#cmbMotivosDevolucion"),error);
			if(error>0){
				nRadicacion=null;
				return false;
			} else {
				return true;
			}
		}
		
		folios=validarRequeridos();
		if(esNumeroRespuesta(folios)){ return true; }
		else {
			nRadicacion=null;
			return false;
		}
	}
}

function llenarLasVariablesRadicacion(opt){
	nRadicacion.idtipodocumentoafiliado=$("#cmbIdTipoDocumentoAfiliado").val();
	nRadicacion.numero=$("#txtNumero").val();
	nRadicacion.nit=$("#txtNit").val();
	nRadicacion.folios=folios;
	nRadicacion.notas=$.trim($("#txtNotas").val());
	nRadicacion.procesado='N';
	nRadicacion.idtipoformulario=$("#cmbTipoFormulario").val();		
	nRadicacion.afiliacionmultiple='N';
	
	if(opt==1){
		nRadicacion.folios=0;
		nRadicacion.asignado="S";
		nRadicacion.procesado="S";
		nRadicacion.anulada="S";
		nRadicacion.devuelto="S";
		nRadicacion.cierre="S";
	}
}

/**
 * LLena las variables necesarias para la radicacion
 * 
 * @param nAfiliacion
 * @returns {Afiliacion}
 */
function llenarLasVariables(nAfiliacion){
	llenarLasVariablesRadicacion(0);	
	
	nAfiliacion=camposAfiliacion(nAfiliacion);
	nAfiliacion.tipoformulario=$("#cmbTipoFormulario").val();
	nAfiliacion.tipoafiliacion=$("#cmbTipoAfiliacion").val();
	nAfiliacion.idempresa=idEmpresaLocal;
	nAfiliacion.idpersona=idPersonaLocal;
	nAfiliacion.fechaingreso=$("#txtFechaIngreso").val();
	nAfiliacion.salario=$("#txtSalario").val();	
	nAfiliacion.primaria=comprobarSiEsPrimaria();
	nAfiliacion.estado='P';
	nAfiliacion.categoria=calcularCategoria(nAfiliacion.salario);
	nAfiliacion.auditado='N';
	nAfiliacion.vendedor='N';
	nAfiliacion.madrecomunitaria="N";
	
	var claseAfiliacion=$("#cmbClaseAfiliacion").val();
	if(claseAfiliacion>0){ nAfiliacion.claseafiliacion=claseAfiliacion; }
	
	if(nAfiliacion.primaria=='N') nRadicacion.afiliacionmultiple='S';
	
	if(nAfiliacion.categoria=='D' || nAfiliacion.primaria=='E'){ 
		if(nAfiliacion.categoria=='D'){ $("#txtSalario").val(''); $("#txtSalario").addClass("ui-state-error"); }
		if(nAfiliacion.primaria=='E'){ alert("Ocurri\u00F3 un error comprobando afiliaci\u00F3n Primaria del afiliado"); }
		nRadicacion=null;
		nAfiliacion=null;
	}
	
	return nAfiliacion;
}

/**
 * Comprueba si la afiliacion es primaria
 * 
 * @returns {String} 'S' - 'N' - 'E'(ERROR)
 */
function comprobarSiEsPrimaria(){
	var primaria='N';
	
	var opcion=contarAfiliacionesPrimaria(idPersonaLocal);
	
	if(opcion==2){
		primaria='S';
	} else if(opcion==1){
		opcion=contarSiAfiliacionEsPrimaria(idPersonaLocal,idEmpresaLocal);
		
		if(opcion==2)
			primaria='S';
	}
	
	if(opcion==0) primaria="E";
		
	return primaria;
}

//VALIDAR LA FECHA DE INGRESO SEA MAYOR O IGUAL A LA FECHA DE RETIRO
function validarFechaIngresoRadica(nombrefecha){
	    var idp=$("#idPersona").val();
	    var ide1=$("#idEmpresa").val();
	    var idEmpresaForm=$("#idEmpresaForm").val();
		   if(ide1==idEmpresaForm)
			   {
			   var ide=ide1;		   
			   }
		   else
			   {
			   var ide=idEmpresaForm;		   
			   }

	    $.ajax({
		url:URL+'aportes/radicacionnew/php/buscarFechaIngresoRadica.php', 
		type: "POST",
		data: {v0:idp,v1:ide},
		async: false,
		dataType: "json",
		success:function(data){
			$.each(data,function(i,fila){ 
				
				var nombre = new Date($("#"+nombrefecha).val());
				//alert(nombre);
				
				/*var fechaingr= new Date($("#fecIngreso2").val());*/
				fila.fecharetiro = new Date(fila.fecharetiro);
				
				if(nombre<fila.fecharetiro){
				document.getElementById(nombrefecha).value = '';
				alert("La fecha de ingreso debe ser mayor o igual a la fecha de retiro");
				}
       		});
		}
	})
}