/** VARIABLES LOCALES **/
var objEmpresa=null;
var esActualizacion=false;

$(document).ready(function(){	
	$("#txtNit").val(copyNit).focus();
	copyNit='';
	
	setTimeout("$('#tabs').trigger('click');$('#txtNit').focus();",700);
});

/**
 * Busca la empresa donde labora el afiliado
 * 
 * @param txtNit 			Nit de la empresa
 * @param lblRazonSocial 	Nombre de la empresa
 * @returns {Boolean}
 */
function busquedaEmpresa(txtNit, lblRazonSocial){
	objEmpresa=null;
	esActualizacion=false;		
	lblRazonSocial.html('');	
	txtNit.removeClass("ui-state-error");
	
	if(validarNumero(txtNit,0)>0){return null;}
	
	var empresa=buscarNit(txtNit,1);		
	if(empresa==null || empresa==false){			
			alert("La EMPRESA que esta radicando NO EXISTE!!");
			txtNit.val('').focus();
			txtNit.addClass("ui-state-error");
			return true; 
	} else {
		if(empresa.estado=='P'){
			if(empresa.legalizada=="N" && radicacionPendienteEmpresa(txtNit)==false){				
				alert("La EMPRESA que esta radicando esta PENDIENTE POR PU!!");
				txtNit.val('');
				txtNit.addClass("ui-state-error");
				return false;
			} else {
				alert("La EMPRESA que esta radicando esta PENDIENTE. \nTIENE RADICACIONES POR GRABAR!!");
				txtNit.val('');
				txtNit.addClass("ui-state-error");
				return false;
			} 
		} else if(empresa.estado=='I') {
			alert("La EMPRESA que esta radicando esta INACTIVA!!");
			txtNit.val('');
			txtNit.addClass("ui-state-error");
			return false;
		} else {
			if(empresa.contratista=="S"){
				alert("La EMPRESA que esta radicando es CONTRATISTA!!");
				txtNit.val('');
				txtNit.addClass("ui-state-error");
				return false;
			} else {
				objEmpresa=empresa;
				esActualizacion=true;
				lblRazonSocial.html(empresa.razonsocial);
			}
		}
	}
}

/**
 * Guardar la Radicacion
 * 
 * @returns {Boolean}
 */
function saveRadicacion(){
	if(iniciarconN()){
		if(validarTodosLosCampos()){
			var nEmpresa=new Empresa();
			nEmpresa=llenarLasVariables(nEmpresa);			
			
			if(nEmpresa==null || nEmpresa==false){				
				nRadicacion=null;
				alert("Se encontr\u00F3 errores en el procesamiento de su solicitud.");				
				return false;
			} else {				
				nRadicacion.idradicacion=guardarRadicacion(nRadicacion);
				if(esNumeroRespuesta(nRadicacion.idradicacion)){
					nuevaRad=0;
					$("#txtHoraFin").val(nRadicacion.horafinal);					
					
					alert("Radicaci\u00F3n guardada correctamente!! \nRADICACI\u00D3N # "+nRadicacion.idradicacion);												
						
					$("#txtIdRadicacion").css({ color: "RED"});
					$("#txtIdRadicacion").val(nRadicacion.idradicacion);
					$("#titRadicacion").trigger('click');
					return true;
				} else {
					alert("Radicaci\u00F3n NO FUE GUARDADA!!"); 
					return false;
				}
			}
		}
	}
}

/**
 * Valida todos los campos necesarios para la radicacion 
 * 
 * @returns {Boolean}
 */
function validarTodosLosCampos(){
	var error=validarCampos();
	
	if(esActualizacion==false || objEmpresa==null || esNumeroRespuesta(objEmpresa.idempresa)==false){ $("#txtNit").val(''); $("#lblRazonSocial").html(''); }
	error=validarNumero($("#txtNit"),error);		
	error=validarNumero($("#txtFolios"),error);	
	
	if(error>0){
		alert("Llene los campos obligatorios!");
		nRadicacion=null;
		return false;
	} else 
		return true;	
}

/**
 * LLena las variables necesarias para la radicacion
 * 
 * @param nAfiliacion
 * @returns {Afiliacion}
 */
function llenarLasVariables(nEmpresa){
	nRadicacion.nit=$("#txtNit").val();
	nRadicacion.folios=$("#txtFolios").val();
	nRadicacion.notas=$.trim($("#txtNotas").val());	
	
	nEmpresa=camposEmpresa(objEmpresa);
	nEmpresa.codigosucursal='000';
	nEmpresa.principal='S';	
	nEmpresa.contratista='N';	
	nEmpresa.estado='P';
	nEmpresa.legalizada='N';
	nEmpresa.renovacion='S';
	nEmpresa.idtipoafiliacion=3316;
	nEmpresa.idpais=48;
	
	return nEmpresa;	
}