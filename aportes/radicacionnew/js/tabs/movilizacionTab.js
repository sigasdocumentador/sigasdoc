/** VARIABLES LOCALES **/
var idPersonaLocal=0;
var folios=0;

$(document).ready(function(){
	$("#cmbIdTipoDocumentoAfiliado").val(copyTipoDoc);
	$("#txtNumero").val(copyDoc);
		
	setTimeout("$('#tabs').trigger('click');$('#cmbIdTipoDocumentoAfiliado').focus();",700);
});
 
/**
 * Busca la persona a afiliar
 * 
 * @param cmbtd 		combo que contiene el tipo de documento
 * @param txtd 			text que contiene el documento
 * @param tdnom 		td que contiene el nombre del afiliado
 * @returns {Boolean}
 */
function busquedaPersona(cmbtd, txtd, tdnom){
	idPersonaLocal=0;
	tdnom.html('');
	$('#lblEstadoAfiliado').html('');
	
	var persona=buscarPersona(cmbtd, txtd, tdnom, false);
	if(persona==null || persona==false){ 
		txtd.val('');
		txtd.addClass("ui-state-error"); 
		return false; 
	}
	
	idPersonaLocal=persona.idpersona;
	var opcion=comprobarTipoAfiliacion(persona.idpersona,1);
	
	if((opcion['0']-opcion['2'])>0){
		var afiliaciones = buscarAfiliacionesActivasAfiliado(persona.idpersona);
		var contA=0;
		
		if(afiliaciones!=null){
			$.each(afiliaciones,function(i,fila){					
			    if(fila.estado=="A"){ contA++; } 
			});
		}
		
		if(contA>0){
			$('#lblEstadoAfiliado').html('&nbsp;ACTIVO');			
		} else {
			$('#lblEstadoAfiliado').html('&nbsp;PENDIENTE');
		}
	} else if (opcion['1']>0){
		$('#lblEstadoAfiliado').html('&nbsp;INACTIVO');		
	} else if (opcion['2']>0){
		$('#lblEstadoAfiliado').html('&nbsp;PENDIENTE POR PU');					
	} else {
		$('#lblEstadoAfiliado').html('&nbsp;SIN AFILIACIONES');
	}
}

/**
 * Guardar la Radicacion
 * 
 * @returns {Boolean}
 */
function saveRadicacion(){
	if(iniciarconN()){
		if(validarTodosLosCampos()){		
			llenarLasVariables();
			
			nRadicacion.idradicacion=guardarRadicacion(nRadicacion);
			
			if(esNumeroRespuesta(nRadicacion.idradicacion)){
				nuevaRad=0;
				$("#txtHoraFin").val(nRadicacion.horafinal);
				var resFolios=guardarDocumentos(listDocumentos, nRadicacion.idradicacion, idPersonaLocal, "V");
					
				if(esNumeroRespuesta(resFolios)){										
					alert("Radicaci\u00F3n guardada correctamente!! \nRADICACI\u00D3N # " + nRadicacion.idradicacion);					
				} else {
					alert("Radicaci\u00F3n guardada con error!! \nRADICACI\u00D3N # " + nRadicacion.idradicacion +
							"\nOcurrio un error al guardar los Documentos!!");
				}
				
				$("#txtIdRadicacion").css({ color: "RED"});
				$("#txtIdRadicacion").val(nRadicacion.idradicacion);
				$("#titRadicacion").trigger('click');
				return true;
			} else {
				alert("Radicaci\u00F3n NO FUE GUARDADA!!"); 
				return false;
			}			
		}
	}
}

/**
 * Valida todos los campos necesarios para la radicacion 
 * 
 * @returns {Boolean}
 */
function validarTodosLosCampos(){
	var error=validarCampos();
	
	error=validarSelect($("#cmbIdTipoDocumentoAfiliado"),error);
	error=validarSelect($("#cmbIdTipoMovilizacion"),error);
	if(esNumeroRespuesta(idPersonaLocal)==false){ $("#txtNumero").val(''); }
	error=validarTexto($("#txtNumero"),error);
		
	if(error>0){
		$("#titDetRadicacion").trigger('click');
		alert("Llene los campos obligatorios!");
		nRadicacion=null;
		return false;
	} else {
		folios=validarRequeridos();
		if(esNumeroRespuesta(folios)){ return true; }
		else {
			nRadicacion=null;
			return false;
		}
	}
}

/**
 * LLena las variables necesarias para la radicacion
 * 
 * @param nAfiliacion
 * @returns {Afiliacion}
 */
function llenarLasVariables(){
	nRadicacion.idtipodocumentoafiliado=$("#cmbIdTipoDocumentoAfiliado").val();
	nRadicacion.idtipomovilizacion=$("#cmbIdTipoMovilizacion").val();
	nRadicacion.numero=$("#txtNumero").val();	
	nRadicacion.folios=folios;
	nRadicacion.notas=$.trim($("#txtNotas").val());
	nRadicacion.procesado='N';
}