/** VARIABLES LOCALES **/
var idPU=0;
var folios=0;
var idPersonaLocal=0;
var idEmpresaLocal=0;
var afiliaciones=null;
var empresaActualizar=null;
var afiliacionesActivas=false;
var esActualizacionEmpresa=false;
var afiliacionesPendientesPU=false;
var esActualizacionAfiliacion=false;

$(document).ready(function(){
	$("#cmbIdTipoDocumentoAfiliado").val(copyTipoDoc);
	$("#txtNumero").val(copyDoc);
	
	actDatepicker($("#txtFechaIngreso"));	
	
	$("#txtFechaIngreso").datepicker('option', 'yearRange','1900:+1');
	$("#txtFechaIngreso").datepicker('option','maxDate', '+2M');
	setTimeout("$('#tabs').trigger('click');$('#cmbIdTipoDocumentoAfiliado').focus();",700);
});

/**
 * Busca la persona a afiliar
 * 
 * @param cmbtd 		combo que contiene el tipo de documento
 * @param txtd 			text que contiene el documento
 * @param tdnom 		td que contiene el nombre del afiliado
 * @returns {Boolean}
 */
function busquedaPersona(cmbtd, txtd, tdnom){
	idPU=0;
	idPersonaLocal=0;
	idEmpresaLocal=0;
	afiliaciones=null;
	afiliacionesActivas=false;	
	esActualizacionEmpresa=false;
	afiliacionesPendientesPU=false;
	esActualizacionAfiliacion=false;
	tdnom.html('');
	$('#txtNit').val('');
	$("#lblRazonSocial").html('');		 
	$("#cmbTipoAfiliacion").find("option[value='3320']").remove();
	$("#cmbTipoAfiliacion").removeAttr("disabled");
	removeTabs(1);
	eliminarDivsBasura();
	$('#chkIPTCopiaDocumentoAfiliado').addClass("requerido");
	$('#imgIPTCopiaDocumentoAfiliado').get(0).src=src() + "/imagenes/menu/obligado.png";
	
	var persona=buscarPersona(cmbtd, txtd, tdnom, true);
	if(persona==null || persona==false){ return false; }
	
	var opcion=comprobarTipoAfiliacion(persona.idpersona);
	
	if(opcion==0){
		alert("Ocurri\u00F3 un error comprobando el tipo de Afiliaci\u00F3n");
		txtd.val('');
		txtd.addClass("ui-state-error");
		return false;
	} else if (opcion==1 || opcion==4){
		if(opcion==1) { 
			afiliacionesActivas=true;
			$('#chkIPTCopiaDocumentoAfiliado').removeClass("requerido");
			$('#imgIPTCopiaDocumentoAfiliado').get(0).src=src() + "/imagenes/spacer.gif";
		}		
		afiliacionesPendientesPU=true;
	} else if(opcion==2 ) {
		afiliacionesActivas=true;
		$('#chkIPTCopiaDocumentoAfiliado').removeClass("requerido");
		$('#imgIPTCopiaDocumentoAfiliado').get(0).src=src() + "/imagenes/spacer.gif";
	}
	
	idPersonaLocal=persona.idpersona;
	$("#txtNit").val(txtd.val()).trigger('blur');
}

/**
 * Busca la empresa donde labora el afiliado
 * 
 * @param txtNit 			Nit de la empresa
 * @param lblRazonSocial 	Nombre de la empresa
 * @returns {Boolean}
 */
function busquedaEmpresa(txtNit, lblRazonSocial){
	var salir=false;	
	idEmpresaLocal=0;
	afiliaciones=null;
	empresaActualizar=null;
	lblRazonSocial.html('');	
	txtNit.removeClass("ui-state-error");
	
	if(esNumeroRespuesta(idPersonaLocal)==false){
		txtNit.val('');		
		$("#txtNumero").val('');		
		$("#txtNumero").addClass("ui-state-error");
		$("#tdNombreCompletoAfiliado").html('');
		return false; 
	}
	if(validarNumero(txtNit,0)>0){return null;}
	
	if(afiliacionesActivas==true || afiliacionesPendientesPU==true){
		afiliaciones = buscarAfiliacionesActivasAfiliado(idPersonaLocal);
		var contD=0; var contP=0; var contI=0; var estI='A'; var tmp = new Array(); 
		
		if(afiliaciones!=null){
			$.each(afiliaciones,function(i,fila){					
			    if(fila.identificacion==fila.nit){ 
			    	contI++; estI=fila.estado;
			    	idEmpresaLocal=fila.idempresa;
			    } else { 
			    	tmp[contD]=fila; 
			    	contD++;
			    	if(fila.estado=='P'){
			    		if(esNumeroRespuesta(comprobarEsPU(fila.nit, fila.idpersona))){
			    			contP++;
			    		}
			    	}
			    }
			});
		}
		
		if(contI>1){
			alert("Tiene m\u00E1s de una afiliaci\u00F3n Independiente!!");
			salir=true;
		} else if(contI==1){
			if(estI=='P' && afiliacionesPendientesPU){				
				idPU=comprobarIndependienteEsPU(idPersonaLocal);
				esActualizacionAfiliacion=esNumeroRespuesta(idPU);
				salir=!esActualizacionAfiliacion;
				if(salir){ 
					alert("No es posible realizar la afiliacion, verifique que se trate de un independiente.\nSu empresa no debe tener trabajadores afiliados!!"); 
				}
			} else {
				alert("Ya Tiene una Afiliaci\u00F3n como Independiente!!");
				salir=true; idEmpresaLocal=0;
			}
		}
		
		if(salir==false){
			crearTabsInformacion(idPersonaLocal, 0);
			ocultarDivs();
			if(contD>0){
				afiliaciones=null;
				if(afiliacionesActivas==true && ((contD-contP)>0)){
					alert("Tiene " + contD + " afiliacion(es) como DEPENDIENTE.");
				} else {
					alert("Tiene " + contD + " afiliacion(es) PENDIENTES por PU.");
				}
			} else { afiliaciones=null; }
		}		
	}
	
	if(salir==false){
		var empresa=null;
		
		if(esNumeroRespuesta(idEmpresaLocal)){ empresa=buscarEmpresaID(idEmpresaLocal); }
		
		if((empresa==null || empresa==false)){ empresa=buscarNit(txtNit, 2); }
		
		if((empresa==null || empresa==false)){
			idEmpresaLocal=0;			
			lblRazonSocial.html($.trim($("#tdNombreCompletoAfiliado").html()));			
		} else {
			esActualizacionEmpresa=true;
			empresaActualizar=empresa;
			idEmpresaLocal=empresa.idempresa;			
			lblRazonSocial.html(empresa.razonsocial);
			
			if(empresa.claseaportante==2875){			
				$('#cmbTipoAfiliacion').append('<option value="3320" selected="selected" >EXCENTO</option>');
				$("#cmbTipoAfiliacion").attr("disabled","disabled");
			}
		}
	}
	
	if(salir){		
		idPersonaLocal=0;
		txtNit.val('');
		$("#txtNumero").val('');		
		$("#txtNumero").addClass("ui-state-error");
		$("#tdNombreCompletoAfiliado").html('');
		$("#cmbTipoAfiliacion").find("option[value='3320']").remove();
		$("#cmbTipoAfiliacion").removeAttr("disabled");
		return false;
	}
}

function changeTipoAfiliacion(){	
	var ta=$('#cmbTipoAfiliacion').val();	
	$('.independienteFacultativo, .independientePensionadoVoluntario').hide();
	$(".independienteFacultativo input[type=checkbox]").removeAttr('checked').trigger("change");
	$(".independientePensionadoVoluntario input[type=checkbox]").removeAttr('checked').trigger("change");
	
	if(ta=='19'){ $('.independienteFacultativo').show(); }
	else if(ta=='20') { $('.independientePensionadoVoluntario').show(); }
}

/**
 * Guardar la Radicacion
 * 
 * @returns {Boolean}
 */
function saveRadicacion(){
	if(iniciarconN()){
		if(validarTodosLosCampos()){		
			var nAfiliacion=new Afiliacion();
			nAfiliacion=llenarLasVariables(nAfiliacion);
			
			if(nAfiliacion==null){
				nRadicacion=null;
				alert("Se encontr\u00F3 errores en el procesamiento de su solicitud.");				
				return false;
			} else {									
				var nEmpresa=new Empresa();
				nEmpresa=llenarVariablesEmpresa(nEmpresa);
				
				if(nEmpresa==null){
					nRadicacion=null;
					alert("Se encontr\u00F3 errores en el procesamiento de su solicitud.\nError al grabar Empresa Independiente");				
					return false;
				}				
				nAfiliacion.idempresa=nEmpresa.idempresa;
				
				nRadicacion.idradicacion=guardarRadicacion(nRadicacion);
				if(esNumeroRespuesta(nRadicacion.idradicacion)){
					var resFolios=guardarDocumentos(listDocumentos, nRadicacion.idradicacion, nAfiliacion.idpersona, "S");
					
					if(esNumeroRespuesta(resFolios)){
						nuevaRad=0;
						$("#txtHoraFin").val(nRadicacion.horafinal);					
						var cant= 0; var res=0;
						if(afiliaciones instanceof Array){ cant=afiliaciones.length; }
						if(cant>0){ res = inactivarAfiliaciones(afiliaciones); }
						
						if(cant==res){
							nAfiliacion.idradicacion=nRadicacion.idradicacion;			
							if(esActualizacionAfiliacion==false){
								nAfiliacion.idformulario=guardarAfiliacion(nAfiliacion);
							} else {
								nAfiliacion.idformulario=idPU;
								nAfiliacion.porplanilla='S';
								nAfiliacion.idformulario=actualizarAfiliacion(nAfiliacion);
							}
							
							if(esNumeroRespuesta(nAfiliacion.idformulario)){						
								alert("Radicaci\u00F3n guardada correctamente!! \nRADICACI\u00D3N # "+nRadicacion.idradicacion);							
								$("#txtIdRadicacion").css({ color: "RED"});
								$("#txtIdRadicacion").val(nRadicacion.idradicacion);
								$("#titRadicacion").trigger('click');							
								if(cant>0){ alert("Inactivada " + res + " de  " + cant + " Afiliaciones"); }
								return true;
							} else {
								if(anularRadicacion(nRadicacion.idradicacion)){
									alert("Ocurri\u00F3 un error guardando la afiliacion!!" +
											"\nPOR FAVOR vuelva a intentarlo");
								} else {
									alert("Ocurri\u00F3 un error guardando la afiliacion!!" +
											"\nPOR FAVOR reporte este numero a soporte:" +
											"\# "+nRadicacion.idradicacion);
								}
								
								return false;
							}
						} else {
							if(anularRadicacion(nRadicacion.idradicacion)){
								alert("Ocurri\u00F3 un error!! \nSe Inactiv\u00F3 " + res + " de " + cant + "  Afiliaciones");
							} else {
								alert("Ocurri\u00F3 un error inactivando!!" +
										"\nPOR FAVOR reporte este numero a soporte:" +
										"\# "+nRadicacion.idradicacion);
							}
							
							return false;
						}
					} else {
						if(anularRadicacion(nRadicacion.idradicacion)){
							alert("Ocurrio un error al guardar los Documentos!!" +
									"\nPOR FAVOR vuelva a intentarlo");
						} else {
							alert("Ocurrio un error al guardar los Documentos y al tratar de anular la radicacion!!" +
									"\nPOR FAVOR reporte este numero a soporte:" +
									"\# "+nRadicacion.idradicacion);
						}											
						return false;
					}					
				} else {
					alert("Radicaci\u00F3n NO FUE GUARDADA!!"); 
				}
			}
		}
	}
}

function saveDevolucion(opt){
	if(iniciarconN()){
		if(validarTodosLosCampos(opt)){
			if(opt==1) { enaDivMotivosDevolucion(); return false; }			
			
			var idMotivoDevolucion=$("#cmbMotivosDevolucion").val();
			llenarLasVariablesRadicacion(1);
			nRadicacion.motivodevolucion=idMotivoDevolucion;
			$("#divMotivosDevolucion").dialog('close');
			
			nRadicacion.idradicacion=guardarDevolucion(nRadicacion);
			if(esNumeroRespuesta(nRadicacion.idradicacion)){
				nuevaRad=0;
				$("#txtHoraFin").val(nRadicacion.horafinal);
				alert("Devoluci\u00F3n guardada correctamente!! \nRADICACI\u00D3N # "+nRadicacion.idradicacion);												
						
				$("#txtIdRadicacion").css({ color: "RED"});
				$("#txtIdRadicacion").val(nRadicacion.idradicacion);
				$("#titRadicacion").trigger('click');
				return true;
			} else {
				alert("Devoluci\u00F3n NO FUE GUARDADA!!"); 
				return false;
			}			
		}
	}
}

/**
 * Valida todos los campos necesarios para la radicacion 
 * 
 * @returns {Boolean}
 */
function validarTodosLosCampos(opt){
	var error=validarCampos();
	
	error=validarSelect($("#cmbIdTipoDocumentoAfiliado"),error);
	if(esNumeroRespuesta(idPersonaLocal)==false){ $("#txtNumero").val(''); }
	error=validarTexto($("#txtNumero"),error);
	if(esActualizacionEmpresa && esNumeroRespuesta(idEmpresaLocal)==false){ $("#txtNit").val(''); }
	error=validarTexto($("#txtNit"),error);		
	
	error=validarSelect($("#cmbTipoFormulario"),error);
	error=validarSelect($("#cmbTipoAfiliacion"),error);
	error=validarTexto($("#txtFechaIngreso"),error);	
	error=validarNumero($("#txtSalario"),error);
	
	if(error>0){
		$("#titDetRadicacion").trigger('click');
		alert("Llene los campos obligatorios!");
		nRadicacion=null;
		return false;
	} else {
		if(opt==1){ return true; }
		else if(opt==2){
			error=validarSelect($("#cmbMotivosDevolucion"),error);
			if(error>0){
				nRadicacion=null;
				return false;
			} else {
				return true;
			}
		}
		
		folios=validarRequeridos();
		if(esNumeroRespuesta(folios)){ return true; }
		else {
			nRadicacion=null;
			return false;
		}
	}
}

function llenarLasVariablesRadicacion(opt){
	nRadicacion.idtipodocumentoafiliado=$("#cmbIdTipoDocumentoAfiliado").val();
	nRadicacion.numero=$("#txtNumero").val();
	nRadicacion.nit=$("#txtNit").val();
	nRadicacion.folios=folios;
	nRadicacion.notas=$.trim($("#txtNotas").val());
	nRadicacion.procesado='N';
	nRadicacion.idtipoformulario=$("#cmbTipoFormulario").val();
	nRadicacion.afiliacionmultiple='N';
	
	if(opt==1){
		nRadicacion.folios=0;
		nRadicacion.asignado="S";
		nRadicacion.procesado="S";
		nRadicacion.anulada="S";
		nRadicacion.devuelto="S";
		nRadicacion.cierre="S";
	}
}

/**
 * LLena las variables necesarias para la radicacion
 * 
 * @param nAfiliacion
 * @returns {Afiliacion}
 */
function llenarLasVariables(nAfiliacion){
	llenarLasVariablesRadicacion(0);
	
	nAfiliacion=camposAfiliacion(nAfiliacion);
	nAfiliacion.tipoformulario=$("#cmbTipoFormulario").val();
	nAfiliacion.tipoafiliacion=$("#cmbTipoAfiliacion").val();	
	nAfiliacion.idpersona=idPersonaLocal;
	nAfiliacion.fechaingreso=$("#txtFechaIngreso").val();
	nAfiliacion.salario=$("#txtSalario").val();
	nAfiliacion.primaria=comprobarSiEsPrimaria();
	nAfiliacion.estado='P';
	nAfiliacion.categoria=calcularCategoria(nAfiliacion.salario);
	nAfiliacion.auditado='N';
	nAfiliacion.vendedor='N';
	
	if(nAfiliacion.primaria=='N') nRadicacion.afiliacionmultiple='S';
	
	if(nAfiliacion.categoria=='D' || nAfiliacion.primaria=='E'){ 
		if(nAfiliacion.categoria=='D'){ $("#txtSalario").val(''); $("#txtSalario").addClass("ui-state-error"); }
		if(nAfiliacion.primaria=='E'){ alert("Ocurri\u00F3 un error comprobando afiliaci\u00F3n Primaria del afiliado"); }
		nRadicacion=null;
		nAfiliacion=null;
	}
	
	return nAfiliacion;
}

/**
 * Comprueba si la afiliacion es primaria
 * 
 * @returns {String} 'S' - 'N'
 */
function comprobarSiEsPrimaria(){
	var primaria='N';
	
	if(afiliacionesActivas){	
		var opcion=contarAfiliacionesPrimaria(idPersonaLocal);
		
		if(opcion==2){
			primaria='S';
		} else if(opcion==1){
			if(esActualizacionEmpresa){
				opcion=contarSiAfiliacionEsPrimaria(idPersonaLocal,idEmpresaLocal);
			
				if(opcion==2) primaria='S';
			}
		}
		
		if(opcion==0) primaria="E";
	} else {
		primaria='S';
	}
		
	return primaria;
}

/**
 * Llena todos los datos de la empresa, para lo cual la guarda o actualiza segun sea el caso
 * 
 * @param nEmpresa
 * @returns {Empresa}
 */
function llenarVariablesEmpresa(nEmpresa){
	var nRadicacionEmpresa=jQuery.extend({}, nRadicacion);
	if(esActualizacionEmpresa){
		if(esNumeroRespuesta(idEmpresaLocal) && empresaActualizar!=null){
			nEmpresa=camposEmpresa(empresaActualizar);
			if(esActualizacionAfiliacion==true){ nEmpresa.porplanilla='S'; }
			nRadicacionEmpresa.idtiporadicacion=31;
			nEmpresa.renovacion='S';			
		} else {
			return null;
		}
	} else {
		nEmpresa=camposEmpresa(nEmpresa);
		nRadicacionEmpresa.idtiporadicacion=30;
		nEmpresa.renovacion='N';
	}	
	
	nEmpresa.idtipodocumento=$("#cmbIdTipoDocumentoAfiliado").val();
	nEmpresa.nit=$("#txtNumero").val();
	nEmpresa.codigosucursal='000';
	nEmpresa.principal='S';
	nEmpresa.razonsocial=$("#lblRazonSocial").html().replace("&nbsp;", "");
	nEmpresa.sigla=$("#lblRazonSocial").html().replace("&nbsp;", "");
	nEmpresa.contratista='N';
	nEmpresa.claseaportante=2655;
	nEmpresa.tipoaportante=2657;
	nEmpresa.estado='A';
	nEmpresa.legalizada='S';	
	
	var idtipoafiliacion=3316;
	var tipoafiliacion=$("#cmbTipoAfiliacion").val();
	if(tipoafiliacion==19){ idtipoafiliacion=3317; }
	else if(tipoafiliacion==21){ idtipoafiliacion=3318; }
	else if(tipoafiliacion!=3320){ idtipoafiliacion=3319; }	
	nEmpresa.idtipoafiliacion=idtipoafiliacion;
	
	nRadicacionEmpresa.idradicacion=guardarRadicacion(nRadicacionEmpresa);
	if(esNumeroRespuesta(nRadicacionEmpresa.idradicacion)){		
		if(esActualizacionEmpresa){
			if(esNumeroRespuesta(actualizarEmpresa(nEmpresa))==false){ nEmpresa.idempresa=0; }
		} else {
			nEmpresa.idempresa=guardarEmpresa(nEmpresa);
		}
		
		if(esNumeroRespuesta(nEmpresa.idempresa)){
			if(!cerrarRadicacion(nRadicacionEmpresa.idradicacion)){
				alert("Ocurrio un error al CERRAR la Radicacion de la empresa Independiente!!" +
						"\nPOR FAVOR reporte este numero a soporte:" +
						"\# "+nRadicacionEmpresa.idradicacion);				
			}			
			
			return nEmpresa;
		} else {
			if(anularRadicacion(nRadicacionEmpresa.idradicacion)){
				alert("Ocurrio un error al GRABAR la empresa Independiente!!" +
						"\nPOR FAVOR vuelva a intentarlo");
			} else {
				alert("Ocurrio un error al ANULAR la Radicacion de la empresa Independiente!!" +
						"\nPOR FAVOR reporte este numero a soporte:" +
						"\# "+nRadicacionEmpresa.idradicacion);
			}
			
			return null;
		}
		
	} else {
		return null;
	}	
}