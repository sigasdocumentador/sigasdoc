/** VARIABLES LOCALES **/
var contFolios=0;
var listDocumentos=new Array();
//var formularioTab="crearConvivenciaTab";

$(document).ready(function(){
	$(".dependienteTab, .trDPTGrupoFamiliar," +
			".renovacionTab, .trRNTGrupoFamiliar," +
			".independienteTab, .independienteFacultativo, .independientePensionadoVoluntario," +
			".afiliacionEmpresaTab, .trAFEPersonaJuridica, .trAFEPersonaNatural," +
			".renovacionEmpresaTab, .trREEPersonaJuridica, .trREEPersonaNatural," +
			".crearConvivenciaTab, .disolverConvivenciaTab," +
			".certificadoTab, .trCERCertificadoDiscapacidad," +
			".defuncionTab, .trDFTCertificadoRetiro," +
			".trBTTTarjetaDeteriorada, .trBTTDenuncia," +
			".postulacionFonedeTab, .conPostulacion," +
			".movilizacionTab, .visitaHabitabilidadTab," +
			".postulacionSFVTab, .actualizacionSFVTab," +
			".subsidioViviendaTab, .subsidioViviendaDesplazado, .reAfiliEmpreExpulTab").hide();
	
	$("." + formularioTab + ":not('.boxcorto2')").show();
	
	$('#tblDocRequeridos input[id^=txt]')
		.bind('keypress keydown keyup', function(){ solonumeros($(this).get(0)); });
});

function onclickCheck(chk,txt,limpiar,opt){
	if(limpiar){ txt.val(''); }
	
	if(chk.is(':checked')){
		if(limpiar){ if(!opt){ txt.removeAttr("style"); } txt.val('1'); }
		else { txt.css('display','block'); }
	} else { txt.css('display','none'); }
}

function onclickCheckOpcional(chkOpt1,chkOpt2,one){
	if(chkOpt1.hasClass('requerido')==true || one==true){
		if(chkOpt1.is(':checked')){
			chkOpt2.removeClass("requerido");
			$('#img' + (chkOpt2.get(0).id).substring(3)).get(0).src=src() + "/imagenes/spacer.gif";
		} else {			
			chkOpt2.addClass("requerido");
			$('#img' + (chkOpt2.get(0).id).substring(3)).get(0).src=src() + "/imagenes/menu/obligado.png";
			if(one==true){ return; }
			if(chkOpt2.is(':checked')){
				chkOpt1.removeClass("requerido");
				$('#img' + (chkOpt1.get(0).id).substring(3)).get(0).src=src() + "/imagenes/spacer.gif";
			}
		}
	}
}

function onclickCheckContenedor(chk, opt){
	var nombre=(chk.get(0).id).substring(3);
	var tr=$('.tr' + nombre);	
	if(chk.is(':checked')){
		if(opt==0){
			$.each(tr,function(i,obj){
				var nom=$(obj).attr("class");
				if(nom==("tr" + nombre))
					$(obj).show();		   
			});
		} else {
			tr.show();
		}
	} else {
		if(tr.length){
			tr.hide(); 
			$(".tr" + nombre + " .boxcorto2").val('1');
			$(".tr" + nombre + " input[type=checkbox]").removeAttr('checked').trigger("change");
		}
	}
}

function onclickRadioContenedor(rad){	
	$.each(rad,function(i,obj){
	    var nombre=(obj.id).substring(3);
	    var tr=$('.tr' + nombre);
	    
	    if($(obj).is(':checked')){
	        $.each(tr,function(i,obj){
				var nom=$(obj).attr("class");
				if(nom==("tr" + nombre))
					$(obj).show();		   
			});
	    } else {
	        tr.hide(); 
			$(".tr" + nombre + " .boxcorto2").val('1');
			$(".tr" + nombre + " input[type=checkbox]").removeAttr('checked').trigger("change");
	    }
	});
}

function validarText(txt,chk){
	if(isNaN(txt.val()) || txt.val()==0){ chk.removeAttr('checked').trigger("change"); }	
}

function validarRequeridos(){
	listDocumentos=new Array();
	var chksVisibles = $('input[type=checkbox].requerido:visible').length;
	var chksCheckeds = $('input[type=checkbox].requerido:visible:checked').length;
	$("input[type=checkbox]").parent().removeClass("ui-state-error");
	
	if((chksVisibles-chksCheckeds)>0){
		alert("Verifique que haya traido todos los documentos requeridos!");		
		$("input[type=checkbox].requerido:visible:not(:checked)").parent().addClass("ui-state-error");
		return 0;
	} else {
		var contador=0;
		var nDocumento=new Documento();		
		var chks = $("input[type=checkbox]:visible:checked");
		$.each(chks,function(i,chk){
		    if($(chk).hasClass('contenedor')==false){
		        var txt = $('#txt' + (chk.id).substring(3));
		        nDocumento.iddocumento=chk.value;
		        if(txt.length && isNaN(parseInt(txt.val()))==false){
		        	nDocumento.cantidad=parseInt(txt.val());
		            contador += parseInt(txt.val());                        
		        } else {
		        	nDocumento.cantidad=1;
		        }
		        listDocumentos.push(camposDocumento(nDocumento));		        
		    }
		});
				
		contFolios=contador;
		alert("La afiliaci\u00F3n trae " + contFolios + " anexos.");		
		return contador;		
	}
}