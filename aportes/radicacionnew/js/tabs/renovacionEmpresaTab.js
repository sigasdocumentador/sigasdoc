/** VARIABLES LOCALES **/
var objEmpresa=null;
var folios=0;
var esActualizacion=false;

$(document).ready(function(){	
	$("#txtNit").val(copyNit);
	copyNit='';
	
	setTimeout("$('#chkREEPersonaJuridica').attr('checked',true).trigger('change');",500);
	setTimeout("$('#tabs').trigger('click');$('#txtNit').focus();",700);
});

/**
 * Busca la empresa donde labora el afiliado
 * 
 * @param txtNit 			Nit de la empresa
 * @param lblRazonSocial 	Nombre de la empresa
 * @returns {Boolean}
 */
function busquedaEmpresa(txtNit, lblRazonSocial){
	objEmpresa=null;
	esActualizacion=false;		
	lblRazonSocial.html('');	
	txtNit.removeClass("ui-state-error");
	
	if(validarNumero(txtNit,0)>0){return null;}
	
	var empresa=buscarNit(txtNit,1);		
	if(empresa==null || empresa==false){			
			alert("La EMPRESA que esta radicando NO EXISTE!!");
			txtNit.val('').focus();
			txtNit.addClass("ui-state-error");
			return true; 
	} else {
		if(empresa.estado=='P'){
			if(empresa.legalizada=="N" && radicacionPendienteEmpresa(txtNit)==false){
				copyNit=txtNit.val();
				alert("La EMPRESA que esta radicando esta PENDIENTE POR PU. \nLa Radicaci\u00F3n se cambia a AFILIACI\u00F3N!!");
				$("#cmbIdTipoRadicacion").val('30').trigger('change');
				return false;
			} else {
				alert("La EMPRESA que esta radicando esta PENDIENTE. \nTIENE RADICACIONES POR GRABAR!!");
				txtNit.val('');
				txtNit.addClass("ui-state-error");
				return false;
			} 
		} else if(empresa.estado=='I') {
			objEmpresa=empresa;
			esActualizacion=true;
			lblRazonSocial.html(empresa.razonsocial);
		} else {
			if(empresa.contratista=="S"){
				copyNit=txtNit.val();
				alert("La EMPRESA que esta radicando es CONTRATISTA. \n" +
						"Para continuar la Radicaci\u00F3n se cambia a AFILIACI\u00F3N!!");
				$("#cmbIdTipoRadicacion").val('30').trigger('change');
				return false;
			} else {
				if (empresa.idtipoafiliacion != 3316) {
					copyNit=txtNit.val();
					alert("La EMPRESA que esta radicando es INDEPENDIENTE. \n" +
							"Para continuar la Radicaci\u00F3n se cambia a AFILIACI\u00F3N!!");
					$("#cmbIdTipoRadicacion").val('30').trigger('change');
					return false;
				} else {
					alert("La EMPRESA que esta radicando esta ACTIVA!!");
					txtNit.val('');
					txtNit.addClass("ui-state-error");
					return false;
				}
			}
		}
	}
}

/**
 * Guardar la Radicacion
 * 
 * @returns {Boolean}
 */
function saveRadicacion(){
	if(iniciarconN()){
		if(validarTodosLosCampos()){
			var nEmpresa=new Empresa();
			nEmpresa=llenarLasVariables(nEmpresa);			
			
			if(nEmpresa==null || nEmpresa==false){				
				nRadicacion=null;
				alert("Se encontr\u00F3 errores en el procesamiento de su solicitud.");				
				return false;
			} else {				
				nRadicacion.idradicacion=guardarRadicacion(nRadicacion);
				if(esNumeroRespuesta(nRadicacion.idradicacion)){
					nuevaRad=0;
					$("#txtHoraFin").val(nRadicacion.horafinal);					
					
					nEmpresa.idempresa=actualizarEmpresa(nEmpresa);
					
					if(esNumeroRespuesta(nEmpresa.idempresa)){						
						var resFolios=guardarDocumentos(listDocumentos, nRadicacion.idradicacion, nEmpresa.idempresa, "S");
						
						if(esNumeroRespuesta(resFolios)){
							alert("Radicaci\u00F3n guardada correctamente!! \nRADICACI\u00D3N # "+nRadicacion.idradicacion);
						} else {
							alert("Radicaci\u00F3n guardada con error!! \nRADICACI\u00D3N # " + nRadicacion.idradicacion +
									"\nOcurrio un error al guardar los Documentos!!");
						}												
						
						$("#txtIdRadicacion").css({ color: "RED"});
						$("#txtIdRadicacion").val(nRadicacion.idradicacion);
						$("#titRadicacion").trigger('click');
						return true;
					} else {
						if(anularRadicacion(nRadicacion.idradicacion)){
							alert("Ocurrio un error!!" +
									"\nPOR FAVOR vuelva a intentarlo");
						} else {
							alert("Ocurrio un error!!" +
									"\nPOR FAVOR reporte este numero a soporte:" +
									"\# "+nRadicacion.idradicacion);
						}						
						return false;
					}					
				} else {
					alert("Radicaci\u00F3n NO FUE GUARDADA!!"); 
					return false;
				}
			}
		}
	}
}

/**
 * Valida todos los campos necesarios para la radicacion 
 * 
 * @returns {Boolean}
 */
function validarTodosLosCampos(){
	var error=validarCampos();
	
	if(esActualizacion==false || objEmpresa==null || esNumeroRespuesta(objEmpresa.idempresa)==false){ $("#txtNit").val(''); $("#lblRazonSocial").html(''); }
	error=validarNumero($("#txtNit"),error);
	
	if(error>0){
		$("#titDetRadicacion").trigger('click');
		alert("Llene los campos obligatorios!");
		nRadicacion=null;
		return false;
	} else {
		folios=validarRequeridos();
		if(esNumeroRespuesta(folios)){ return true; }
		else {
			nRadicacion=null;
			return false;
		}
	}
}

/**
 * LLena las variables necesarias para la radicacion
 * 
 * @param nAfiliacion
 * @returns {Afiliacion}
 */
function llenarLasVariables(nEmpresa){
	nRadicacion.nit=$("#txtNit").val();
	nRadicacion.folios=folios;
	nRadicacion.notas=$.trim($("#txtNotas").val());	
	
	nEmpresa=camposEmpresa(objEmpresa);
	nEmpresa.codigosucursal='000';
	nEmpresa.principal='S';	
	nEmpresa.contratista='N';	
	nEmpresa.estado='P';
	nEmpresa.codigoestado='';
	nEmpresa.fechaestado='';	
	nEmpresa.legalizada='N';
	nEmpresa.renovacion='S';
	nEmpresa.idtipoafiliacion=3316;
	nEmpresa.idpais=48;
	
	return nEmpresa;	
}