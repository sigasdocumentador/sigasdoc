/** VARIABLES LOCALES **/
var folios=0;
var idPersonaLocal=0;
var idBeneficiario=0;
var conyuges=null;
var actualizarConvivencia=false;

$(document).ready(function(){
	$("#cmbIdTipoDocumentoAfiliado,#cmbIdTipoDocumentoConyuge").val(copyTipoDoc);
	$("#txtNumero").val(copyDoc);
	
	$("#div-datosRelacionesActivas").dialog({
		autoOpen:false,
		modal:true,
		width:600,
		height:120,
		resizable:false
	});
	
	setTimeout("$('#tabs').trigger('click');$('#cmbIdTipoDocumentoAfiliado').focus();",700);
});

/**
 * Busca la persona a afiliar
 * 
 * @param cmbtd 		combo que contiene el tipo de documento
 * @param txtd 			text que contiene el documento
 * @param tdnom 		td que contiene el nombre del afiliado
 * @returns {Boolean}
 */
function busquedaPersona(cmbtd, txtd, tdnom){
	actualizarConvivencia=false;
	conyuges=null;
	idPersonaLocal=0;
	idBeneficiario=0;
	tdnom.html('');
	$("#txtNumeroConyuge").val("").trigger("blur");
	
	var persona=buscarPersona(cmbtd, txtd, tdnom, false);
	if(persona==null || persona==false){ 
		txtd.val('');
		txtd.addClass("ui-state-error"); 
		return false; 
	}
	
	var opcion=comprobarTipoAfiliacion(persona.idpersona,1);
	
	if((opcion['0']-opcion['2'])>0){
		var afiliaciones = buscarAfiliacionesActivasAfiliado(persona.idpersona);
		var contA=0;
		
		if(afiliaciones!=null){
			$.each(afiliaciones,function(i,fila){					
			    if(fila.estado=="A"){ contA++; } 
			});
		}
		
		if(contA>0){			
			var beneficiarios=buscarConyuges(persona.idpersona,1);
			
			if(beneficiarios==null){
				idPersonaLocal=persona.idpersona;								
			} else {
				var conConv=0;
				$("#tbRelacionesActivas").empty();
				$.each(beneficiarios,function(i,fila){
					if(fila.conviven=="S"){
						conConv++;
						nom = fila.pnombre + " " +fila.snombre +" "+ fila.papellido + " " + fila.sapellido;
						$("#tbRelacionesActivas").append(
							"<tr>" +
								"<td>"+fila.codigo+"</td>" +
								"<td>"+fila.identificacion+"</td>" +
								"<td>"+nom+"</td>" +
								"<td><center>"+fila.detalledefinicion+"</center></td>" +
								"<td><center>"+fila.conviven+"</center></td>" +
							"</tr>");
					}
				});
				
				if(conConv>0){
					$("#div-datosRelacionesActivas").dialog('open');
					alert("La persona que esta radicando TIENE CONVIVENCIA ACTIVA!!");				
					txtd.val(''); tdnom.html('');
					txtd.addClass("ui-state-error");
				} else {
					conyuges=beneficiarios;
					idPersonaLocal=persona.idpersona;
				}				
			}
		} else {
			alert("La persona que esta radicando solo tiene AFILIACION(ES) PENDIENTE(S)!!");
			txtd.val(''); tdnom.html('');
			txtd.addClass("ui-state-error");
		}
	} else {
		if (opcion['2']>0){
			alert("La persona que esta radicando tiene AFILIACION(ES) PENDIENTE(S) por PU!");			
		} else if (opcion['1']>0){
			alert("La persona que esta radicando esta INACTIVA!");
		} else {
			alert("La persona que esta radicando no tiene AFILIACIONES!");
		}
		
		txtd.val(''); tdnom.html('');
		txtd.addClass("ui-state-error");
	}	
}

/**
 * Busca la conyuge
 * 
 * @param cmbtd 		combo que contiene el tipo de documento
 * @param txtd 			text que contiene el documento
 * @param tdnom 		td que contiene el nombre del afiliado
 * @returns {Boolean}
 */
function busquedaConyuge(cmbtd, txtd, tdnom){
	actualizarConvivencia=false;
	idBeneficiario=0;
	tdnom.html('');	
	
	var persona=buscarPersona(cmbtd, txtd, tdnom, true);
	if(persona==null || persona==false){ 
		txtd.val('');
		txtd.addClass("ui-state-error"); 
		return false; 
	}
	
	if(persona.idpersona!=idPersonaLocal){		
		var contExiste=0;
		if(!(conyuges==null || conyuges==false)){
			$.each(conyuges,function(i,fila){ if(fila.idconyuge==persona.idpersona){ contExiste++; } });
		}
		
		if(contExiste>0){ actualizarConvivencia=true; }
		
		var beneficiarios=buscarConyuges(persona.idpersona,0);
		
		if(beneficiarios==null){
			idBeneficiario=persona.idpersona;						
		} else {
			var conConv=0;
			$("#tbRelacionesActivas").empty();
			$.each(beneficiarios,function(i,fila){
				if(fila.conviven=="S"){
					if(fila.idconyuge!=idPersonaLocal){
						conConv++; 						
						nom = fila.pnombre + " " +fila.snombre +" "+ fila.papellido + " " + fila.sapellido;
						$("#tbRelacionesActivas").append(
							"<tr>" +
								"<td>"+fila.codigo+"</td>" +
								"<td>"+fila.identificacion+"</td>" +
								"<td>"+nom+"</td>" +
								"<td><center>"+fila.detalledefinicion+"</center></td>" +
								"<td><center>"+fila.conviven+"</center></td>" +
							"</tr>");
					}
				}
			});
			
			if(conConv>0){
				$("#div-datosRelacionesActivas").dialog('open');
				alert("La persona que esta radicando TIENE CONVIVENCIA ACTIVA!!");
				actualizarConvivencia=false;
				txtd.val(''); tdnom.html('');
				txtd.addClass("ui-state-error");
			} else {
				idBeneficiario=persona.idpersona;
			}
		}
	} else {
		alert("El afiliado no puede tener una relacion con el mismo!!");				
		txtd.val(''); tdnom.html('');
		txtd.addClass("ui-state-error");
	}
}

/**
 * Guardar la Radicacion
 */
function saveRadicacion(){
	if(iniciarconN()){
		if(validarTodosLosCampos()){		
			var nGrupoFamiliar=new GrupoFamiliar();
			nGrupoFamiliar=llenarLasVariables(nGrupoFamiliar);
			
			if(nGrupoFamiliar==null){
				nRadicacion=null;
				alert("Se encontr\u00F3 errores en el procesamiento de su solicitud.");				
				return false;
			} else {	
				nRadicacion.idradicacion=guardarRadicacion(nRadicacion);
				if(esNumeroRespuesta(nRadicacion.idradicacion)){
					var resFolios=guardarDocumentos(listDocumentos, nRadicacion.idradicacion, nGrupoFamiliar.idtrabajador, "S");
					
					if(esNumeroRespuesta(resFolios)){
						nuevaRad=0;
						$("#txtHoraFin").val(nRadicacion.horafinal);
						if(actualizarConvivencia==false){
							nGrupoFamiliar.idrelacion=guardarGrupoFamiliar(nGrupoFamiliar);
						} else {
							nGrupoFamiliar.idrelacion=updateCrearConvivencia(nGrupoFamiliar);
						}
						
						if(esNumeroRespuesta(nGrupoFamiliar.idrelacion)){
							if(cerrarRadicacion(nRadicacion.idradicacion)){
								alert("Radicaci\u00F3n guardada y cerrada correctamente!! \nRADICACI\u00D3N # "+nRadicacion.idradicacion);
								$("#titRadicacion").trigger('click');
								$("#txtIdRadicacion").css({ color: "RED"});
								$("#txtIdRadicacion").val(nRadicacion.idradicacion);
							} else {
								alert("Ocurrio un error al cerrar la radicacion!!" +
										"\nPOR FAVOR reporte este numero a soporte:" +
										"\# "+nRadicacion.idradicacion);
							} 
						} else {
							if(anularRadicacion(nRadicacion.idradicacion)){
								alert("Ocurrio un error al guardar la relacion!!" +
										"\nPOR FAVOR vuelva a intentarlo");
							} else {
								alert("Ocurrio un error al anular la radicacion!!" +
										"\nPOR FAVOR reporte este numero a soporte:" +
										"\# "+nRadicacion.idradicacion);
							}						
							return false;
						}
					} else {
						if(anularRadicacion(nRadicacion.idradicacion)){
							alert("Ocurrio un error al guardar los Documentos!!" +
									"\nPOR FAVOR vuelva a intentarlo");
						} else {
							alert("Ocurrio un error al anular la radicacion!!" +
									"\nPOR FAVOR reporte este numero a soporte:" +
									"\# "+nRadicacion.idradicacion);
						}						
						return false;
					}					
				} else {
					alert("Radicaci\u00F3n NO FUE GUARDADA!!");
					return false;
				}
			}
		}
	}
}

function saveDevolucion(opt){
	if(iniciarconN()){
		if(validarTodosLosCampos(opt)){
			if(opt==1) { enaDivMotivosDevolucion(); return false; }			
			
			var idMotivoDevolucion=$("#cmbMotivosDevolucion").val();
			llenarLasVariablesRadicacion(1);
			nRadicacion.motivodevolucion=idMotivoDevolucion;
			$("#divMotivosDevolucion").dialog('close');
			
			nRadicacion.idradicacion=guardarDevolucion(nRadicacion);
			if(esNumeroRespuesta(nRadicacion.idradicacion)){
				nuevaRad=0;
				$("#txtHoraFin").val(nRadicacion.horafinal);
				alert("Devoluci\u00F3n guardada correctamente!! \nRADICACI\u00D3N # "+nRadicacion.idradicacion);												
						
				$("#txtIdRadicacion").css({ color: "RED"});
				$("#txtIdRadicacion").val(nRadicacion.idradicacion);
				$("#titRadicacion").trigger('click');
				return true;
			} else {
				alert("Devoluci\u00F3n NO FUE GUARDADA!!"); 
				return false;
			}			
		}
	}
}

/**
 * Valida todos los campos necesarios para la radicacion 
 */
function validarTodosLosCampos(opt){
	var error=validarCampos();
	
	error=validarSelect($("#cmbIdTipoDocumentoAfiliado"),error);
	if(esNumeroRespuesta(idPersonaLocal)==false){ $("#txtNumero").val(''); }
	error=validarTexto($("#txtNumero"),error);
	error=validarSelect($("#cmbIdTipoDocumentoConyuge"),error);
	if(esNumeroRespuesta(idBeneficiario)==false){ $("#txtNumeroConyuge").val(''); }
	error=validarTexto($("#txtNumeroConyuge"),error);
	
	error=validarSelect($("#cmbTipoRelacion"),error);	
	
	if(error>0){
		$("#titDetRadicacion").trigger('click');
		alert("Llene los campos obligatorios!");
		nRadicacion=null;
		return false;
	} else {
		if(opt==1){ return true; }
		else if(opt==2){
			error=validarSelect($("#cmbMotivosDevolucion"),error);
			if(error>0){
				nRadicacion=null;
				return false;
			} else {
				return true;
			}
		}
		
		folios=validarRequeridos();
		if(esNumeroRespuesta(folios)){ return true; }
		else {
			nRadicacion=null;
			return false;
		}
	}
}

function llenarLasVariablesRadicacion(opt){
	nRadicacion.idtipodocumentoafiliado=$("#cmbIdTipoDocumentoAfiliado").val();
	nRadicacion.numero=$("#txtNumero").val();
	nRadicacion.notas=$.trim($("#txtNotas").val());	
	nRadicacion.idbeneficiario=idBeneficiario;
	nRadicacion.folios=folios;
	
	if(opt==1){
		nRadicacion.folios=0;
		nRadicacion.asignado="S";
		nRadicacion.procesado="S";
		nRadicacion.anulada="S";
		nRadicacion.devuelto="S";
		nRadicacion.cierre="S";
	}
}

/**
 * LLena las variables necesarias para la radicacion
 */
function llenarLasVariables(nGrupoFamiliar){
	llenarLasVariablesRadicacion(0);	
	
	nGrupoFamiliar=camposGrupoFamiliar(nGrupoFamiliar);
	nGrupoFamiliar.idtrabajador=idPersonaLocal;
	nGrupoFamiliar.idbeneficiario=idBeneficiario;
	nGrupoFamiliar.idparentesco="34";
	nGrupoFamiliar.idconyuge=idBeneficiario;
	nGrupoFamiliar.conviven="S";
	nGrupoFamiliar.estado="A";
	nGrupoFamiliar.idtiporelacion=$("#cmbTipoRelacion").val();
		
	return nGrupoFamiliar;
}