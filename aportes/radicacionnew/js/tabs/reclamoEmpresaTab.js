/** VARIABLES LOCALES **/
var objEmpresa=null;
var esActualizacion=false;

$(document).ready(function(){
	$("#txtNit").val(copyNit);
	copyNit='';
	
	actDatepickerPeriodoImgs($("#txtPeriodoInicial"));
	actDatepickerPeriodoImgs($("#txtPeriodoFinal"));
	
	setTimeout("$('#tabs').trigger('click');$('#txtNit').focus();",700);
});

/**
 * Busca la empresa donde labora el afiliado
 * 
 * @param txtNit 			Nit de la empresa
 * @param lblRazonSocial 	Nombre de la empresa
 * @returns {Boolean}
 */
function busquedaEmpresa(txtNit, lblRazonSocial){
	objEmpresa=null;
	esActualizacion=false;		
	lblRazonSocial.html('');	
	txtNit.removeClass("ui-state-error");
	removeTabs(1);
	eliminarDivsBasura();
	
	if(validarNumero(txtNit,0)>0){return null;}
	
	var empresa=buscarNit(txtNit,1);		
	if(empresa==null || empresa==false){			
			alert("La EMPRESA que esta radicando NO EXISTE!!");
			txtNit.val('').focus();
			txtNit.addClass("ui-state-error");
			return true; 
	} else {
		if(empresa.estado=='P'){
			if(empresa.legalizada=="N" && radicacionPendienteEmpresa(txtNit)==false){				
				alert("La EMPRESA que esta radicando esta PENDIENTE POR PU!!");
				txtNit.val('');
				txtNit.addClass("ui-state-error");
				return false;
			} else {
				alert("La EMPRESA que esta radicando esta PENDIENTE. \nTIENE RADICACIONES POR GRABAR!!");
				txtNit.val('');
				txtNit.addClass("ui-state-error");
				return false;
			} 
		} else if(empresa.estado=='I') {
			alert("La EMPRESA que esta radicando esta INACTIVA!!");
			txtNit.val('');
			txtNit.addClass("ui-state-error");
			return false;
		} else {
			if(empresa.contratista=="S"){
				alert("La EMPRESA que esta radicando es CONTRATISTA!!");
				txtNit.val('');
				txtNit.addClass("ui-state-error");
				return false;
			} else {
				objEmpresa=empresa;
				esActualizacion=true;
				lblRazonSocial.html(empresa.razonsocial);
				crearTabsInformacionEmpresa(empresa.idempresa);
				ocultarDivs();
			}
		}
	}
}

/**
 * Guardar la Radicacion
 */
function saveRadicacion(){
	if(iniciarconN()){
		if(validarTodosLosCampos()){		
			var nReclamoEmpresa=new ReclamoEmpresa();
			nReclamoEmpresa=llenarLasVariables(nReclamoEmpresa);
			
			if(nReclamoEmpresa==null){
				nRadicacion=null;
				alert("Se encontr\u00F3 errores en el procesamiento de su solicitud.");				
				return false;
			} else {	
				nRadicacion.idradicacion=guardarRadicacion(nRadicacion);
				if(esNumeroRespuesta(nRadicacion.idradicacion)){
					nuevaRad=0;
					$("#txtHoraFin").val(nRadicacion.horafinal);
					nReclamoEmpresa.idreclamo=guardarReclamoEmpresa(nReclamoEmpresa);
					
					if(esNumeroRespuesta(nReclamoEmpresa.idreclamo)){
						alert("Radicaci\u00F3n y RECLAMO guardados correctamente!! \nRADICACI\u00D3N # "+nRadicacion.idradicacion);
						$("#titRadicacion").trigger('click');
						$("#txtIdRadicacion").css({ color: "RED"});
						$("#txtIdRadicacion").val(nRadicacion.idradicacion);
					} else {
						if(anularRadicacion(nRadicacion.idradicacion)){
							alert("Ocurrio un error al guardar el reclamo!!" +
									"\nPOR FAVOR vuelva a intentarlo");
						} else {
							alert("Ocurrio un error al anular la radicacion!!" +
									"\nPOR FAVOR reporte este numero a soporte:" +
									"\# "+nRadicacion.idradicacion);
						}						
						return false;
					}					
				} else {
					alert("Radicaci\u00F3n NO FUE GUARDADA!!");
					return false;
				}
			}
		}
	}
}

/**
 * Valida todos los campos necesarios para la radicacion 
 */
function validarTodosLosCampos(){
	var error=validarCampos();
	
	if(esActualizacion==false || objEmpresa==null || esNumeroRespuesta(objEmpresa.idempresa)==false){ $("#txtNit").val(''); $("#lblRazonSocial").html(''); }
	error=validarNumero($("#txtNit"),error);
	error=validarSelect($("#cmbIdCausal"),error);
	error=validarTexto($("#txtPeriodoInicial"),error);
	error=validarTexto($("#txtPeriodoFinal"),error);
	
	if(error>0){		
		alert("Llene los campos obligatorios!");
		nRadicacion=null;
		return false;
	} else {
		return true;
	}
}

/**
 * LLena las variables necesarias para la radicacion
 */
function llenarLasVariables(nReclamoEmpresa){
	var $reclamoEmpresa=$("#cmbIdTipoRadicacion").val();
	nRadicacion.nit=$("#txtNit").val();
	nRadicacion.notas=$.trim($("#txtNotas").val());
	
	nReclamoEmpresa=camposReclamoEmpresa(nReclamoEmpresa);
	nReclamoEmpresa.idempresa=objEmpresa.idempresa;
	nReclamoEmpresa.periodoinicial=$("#txtPeriodoInicial").val();
	nReclamoEmpresa.periodofinal=$("#txtPeriodoFinal").val();
	nReclamoEmpresa.estado="A";
	nReclamoEmpresa.idcausal=$("#cmbIdCausal").val();
	nReclamoEmpresa.notas=$.trim($("#txtNotas").val());
	if($reclamoEmpresa==4231){
	nReclamoEmpresa.pqr="S";
		}else{
	nReclamoEmpresa.pqr="N";
		}
		
	return nReclamoEmpresa;
}

/**
 * Activar Rango Periodos
 */ 
function actDatepickerPeriodoImgs(objeto){
	objeto.datepicker({
		dateFormat: 'yymm',
		changeMonth: true,
		changeYear: true,
		showButtonPanel: true,
		maxDate: "+0D",
		minDate: "-6M",
		onClose: function(dateText, inst) {
			var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
			var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
			$(this).datepicker('setDate', new Date(year, month, 1));
			validarDatepickerPeriodoImgs();
		} 
	});
	objeto.focus(function () {
		$(".ui-datepicker-calendar").hide();
		$("#ui-datepicker-div").position({
			my: "center top",
			at: "center bottom",
			of: $(this)
		});
	}); 
}

/**
 * Validar fecha inicial menor o igual a fecha final
 */
function validarDatepickerPeriodoImgs(){
	$("#txtPeriodoFinal").removeClass("ui-state-error");
	var fechaIni=$("#txtPeriodoInicial").val();
	var fechaFin=$("#txtPeriodoFinal").val();
	
	if(fechaIni != "" && fechaFin != ""){
		var anioIni=fechaIni.slice(0,4);
		var mesIni=eval(fechaIni.slice(-2));
		var anioFin=fechaFin.slice(0,4);
		var mesFin=eval(fechaFin.slice(-2));
		
		if((anioFin<anioIni) || (anioFin==anioIni && mesFin<mesIni)){
			$("#txtPeriodoFinal").val('');
			$("#txtPeriodoFinal").addClass("ui-state-error");
			$("#txtNotas").focus();
		}
	}
}