<?php
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");

$root = $_SERVER ['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';

include_once $_SESSION['RAIZ'] . DIRECTORY_SEPARATOR . 'rsc/pdo/IFXDbManejador.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

$FrmActual=basename($_SERVER['PHP_SELF']);
$fecver = date('Ymd h:i:s A',filectime($FrmActual));
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Informaci&oacute;n</title>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>aportes/radicacionnew/js/tabs/reclamoTrabajadorTab.js"></script>
</head>
<body>
	<table width="90%" border="0" cellspacing="0" cellpadding="0"
		align="center">
		<tr>
			<td width="13" height="29" class="arriba_iz">&nbsp;</td>
			<td class="arriba_ce">
				<span class="letrablanca">:: Informaci&oacute;n Reclamo Trabajador ::</span>
				<div style="text-align:right; height:20px; top:58px; width:221px; position:absolute; right: 100px;"><?php echo 'Versi&oacute;n: ' . $fecver; ?></div>
			</td>
			<td width="13" class="arriba_de" align="right">&nbsp;</td>
		</tr>
		<tr>
			<td class="cuerpo_iz">&nbsp;</td>
			<td class="cuerpo_ce"><br />
				<table width="90%" cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td>&nbsp;</td>
					</tr>
				</table>
			</td>
			<td class="cuerpo_de">&nbsp;</td>
		</tr>
		<tr>
			<td class="cuerpo_iz">&nbsp;</td>
			<td class="cuerpo_ce">
				<table width="90%" border="0" cellspacing="0" class="tablero" align="center">
					<tr>
						<td align="left" width="17%">Tipo Documento Afiliado</td>
						<td>
							<select name="cmbIdTipoDocumentoAfiliado" id="cmbIdTipoDocumentoAfiliado" class="box1" style="width: 250px" onchange="$('#txtNumero').val('').trigger('blur');" >
								<option value="0" selected="selected">Seleccione...</option>
		            			<?php
		            				$rs = $db->Definiciones ( 1, 1 );															
									while ( $row = $rs->fetch() ) {
										echo "<option value=" . $row ['iddetalledef'] . ">" . $row ['detalledefinicion'] . "</option>";
									}
								?>
							</select> <img src="../../imagenes/menu/obligado.png" width="12" height="12">
						</td>
						<td>N&uacute;mero Documento Afiliado</td>
						<td>
							<input type="text" class="box1" name="txtNumero" maxlength="12" id="txtNumero" onkeyup="validarIdentificacion($('#cmbIdTipoDocumentoAfiliado'),this);"
								onkeydown="validarIdentificacion($('#cmbIdTipoDocumentoAfiliado'),this);" onkeypress="validarIdentificacion($('#cmbIdTipoDocumentoAfiliado'),this);"
								onblur="busquedaPersona($('#cmbIdTipoDocumentoAfiliado'), $('#txtNumero'), $('#tdNombreCompletoAfiliado'));">
							<img src="../../imagenes/menu/obligado.png" width="12" height="12">
						</td>
					</tr>
					<tr>
						<td>Nombre Completo</td>
						<td id="tdNombreCompletoAfiliado" colspan="3" >&nbsp;</td>						
					</tr>
					<tr>
						<td align="left">Causal</td>
						<td colspan="3">
							<select name="cmbIdCausal" id="cmbIdCausal" class="box1" style="width: 250px" >
								<option value="0" selected="selected">Seleccione...</option>
		            			<?php
		            				$rs = $db->Definiciones ( 23, 3 );															
									while ( $row = $rs->fetch() ) {
										echo "<option value=" . $row ['iddetalledef'] . ">" . $row ['detalledefinicion'] . "</option>";
									}
								?>
							</select> <img src="../../imagenes/menu/obligado.png" width="12" height="12">
						</td>
					</tr>
					<tr>
						<td align="left">Periodo Inicial</td>
						<td><input name="txtPeriodoInicial" id="txtPeriodoInicial" type="text" class="boxfecha" readonly="readonly"> <img src="../../imagenes/menu/obligado.png" alt="Obligatorio" width="12" height="12"></td>
						<td align="left">Periodo Final</td>
						<td><input name="txtPeriodoFinal" id="txtPeriodoFinal" type="text" class="boxfecha" readonly="readonly"> <img src="../../imagenes/menu/obligado.png" alt="Obligatorio" width="12" height="12"></td>
					</tr>										
					<tr>
						<td align="left">Notas</td>
						<td colspan="3"><textarea name="txtNotas" id="txtNotas" maxlength="250" class="boxlargo" style="width: 92%; height: 40px;" ></textarea> 
						</td>
					</tr>
				</table>
			</td>
			<td class="cuerpo_de">&nbsp;</td>
		</tr>
		<tr>
			<input id="cmbIdTipoRadicacion" type="hidden" value="<?php echo $_REQUEST["idRadicacion"];?>">
			<td class="abajo_iz" >&nbsp;</td>
    		<td class="abajo_ce" align="center" ><img src="../../imagenes/guardar.png" title="Guardar" style="cursor:pointer" onclick="saveRadicacion();"></td>
    		<td class="abajo_de" >&nbsp;</td>
		</tr>		
	</table>
</body>
</html>