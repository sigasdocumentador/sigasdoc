<?php
set_time_limit ( 0 );
ini_set ( "display_errors", '1' );
date_default_timezone_set ( "America/Bogota" );

$root = $_SERVER ['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'sigas' .DIRECTORY_SEPARATOR . 'session.php'; 
include_once $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';

$FrmActual=basename($_SERVER['PHP_SELF']);
$fecver = date('Ymd h:i:s A',filectime($FrmActual));
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Documentos</title>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>aportes/radicacionnew/js/tabs/documentosTab.js"></script>
</head>
<body>
	<div>
		<div style="text-align:right; height:20px; top:0px; width:221px; position:absolute; left: 20px;"><?php echo 'Versi&oacute;n: ' . $fecver; ?></div>
		<center>
			<h4>LISTADO DOCUMENTOS A SOLICITAR</h4>
			
			<table width="580px" id="tblDocRequeridos">
			
	<!-- FORMULARIO DEPENDIENTE -->
				<tr align="left" width="580px" class="dependienteTab">
					<td> <table margin-left: 17px;" border="0" width="580px">
						<tr align="left">
							<td>
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkDPTFormularioAfiliacion" class="dependienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="281"> 
									Formulario afiliaci&oacute;n. 
							</td>
							<td> 
								<input type="text" id="txtDPTFormularioAfiliacion" class="dependienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkDPTCopiaDocumentoAfiliado" class="dependienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="282"> 
									Fotocopia documento identidad. 
							</td>
							<td> 
								<input type="text" id="txtDPTCopiaDocumentoAfiliado" class="dependienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkDPTGrupoFamiliar" class="dependienteTab contenedor" onChange="onclickCheckContenedor($(this),0);"> 
									Grupo Familiar. 
							</td>
							<td> 
								&nbsp;
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkDPTConvivencias" class="dependienteTab contenedor" onChange="onclickCheckContenedor($(this),1);"> 
									Convivencias. 
							</td>
							<td> 
								&nbsp;
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar trDPTConvivencias">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkDPTCopiaDocumentoConyuge" class="dependienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="283"> 
									Fotocopia Documento Conyuge. 
							</td>
							<td> 
								<input type="text" id="txtDPTCopiaDocumentoConyuge" class="dependienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar trDPTConvivencias">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img id="imgDPTCertificadoConvivencia" src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkDPTCertificadoConvivencia" class="dependienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false); onclickCheckOpcional($(this), $('#chkDPTRegistroConvivencia'));" value="284"> 
									Certificado Convivencia. 
							</td>
							<td> 
								<input type="text" id="txtDPTCertificadoConvivencia" class="dependienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar trDPTConvivencias">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img id="imgDPTRegistroConvivencia" src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkDPTRegistroConvivencia" class="dependienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false); onclickCheckOpcional($(this), $('#chkDPTCertificadoConvivencia'));" value="285"> 
									Registro Convivencia. 
							</td>
							<td> 
								<input type="text" id="txtDPTRegistroConvivencia" class="dependienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>						
						<tr align="left" class="trDPTGrupoFamiliar">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkDPTHijos" class="dependienteTab contenedor" onChange="onclickCheckContenedor($(this),1);"> 
									Hijos menores de 19 a&ntilde;os.
							</td>
							<td> 
								&nbsp;
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar trDPTHijos">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkDPTCopiaDocumentoHijo" class="dependienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="286"> 
									Fotocopia documento Hijo.
							</td>
							<td> 
								<input type="text" id="txtDPTCopiaDocumentoHijo" class="dependienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar trDPTHijos">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkDPTDependenciaEconomicaHijo" class="dependienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="287"> 
									Formato Dependencia Econ&oacute;mica.
							</td>
							<td> 
								<input type="text" id="txtDPTDependenciaEconomicaHijo" class="dependienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar trDPTHijos">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkDPTCertificadoHijo" class="dependienteTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="288"> 
									Certificados.
							</td>
							<td> 
								<input type="text" id="txtDPTCertificadoHijo" class="dependienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkDPTHijastros" class="dependienteTab contenedor" onChange="onclickCheckContenedor($(this),1);"> 
									Hijastros menores de 19 a&ntilde;os. 
							</td>
							<td> 
								&nbsp;
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar trDPTHijastros">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkDPTCopiaDocumentoHijastro" class="dependienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="289"> 
									Fotocopia documento Hijastro.
							</td>
							<td> 
								<input type="text" id="txtDPTCopiaDocumentoHijastro" class="dependienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar trDPTHijastros">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img id="imgDPTDependenciaEconomicaHijastro" src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkDPTDependenciaEconomicaHijastro" class="dependienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false); onclickCheckOpcional($(this), $('#chkDPTSentenciaJudicialHijastro'));" value="2810"> 
									Formato Dependencia Econ&oacute;mica.
							</td>
							<td> 
								<input type="text" id="txtDPTDependenciaEconomicaHijastro" class="dependienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar trDPTHijastros">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img id="imgDPTSentenciaJudicialHijastro" src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkDPTSentenciaJudicialHijastro" class="dependienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false); onclickCheckOpcional($(this), $('#chkDPTDependenciaEconomicaHijastro'));" value="2811"> 
									Sentencia Judicial.
							</td>
							<td> 
								<input type="text" id="txtDPTSentenciaJudicialHijastro" class="dependienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar trDPTHijastros">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkDPTCertificadoHijastro" class="dependienteTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="2812"> 
									Certificados.
							</td>
							<td> 
								<input type="text" id="txtDPTCertificadoHijastro" class="dependienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkDPTHermanos" class="dependienteTab contenedor" onChange="onclickCheckContenedor($(this),1);"> 
									Hermanos que dependen econ&oacute;micamente. 
							</td>
							<td> 
								&nbsp;
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar trDPTHermanos">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkDPTCopiaDocumentoHermano" class="dependienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="2813"> 
									Fotocopia documento Hermano.
							</td>
							<td> 
								<input type="text" id="txtDPTCopiaDocumentoHermano" class="dependienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar trDPTHermanos">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img id="imgDPTDependenciaEconomicaHermano" src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkDPTDependenciaEconomicaHermano" class="dependienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false); onclickCheckOpcional($(this), $('#chkDPTSentenciaJudicialHermano'));" value="2814"> 
									Formato Dependencia Econ&oacute;mica.
							</td>
							<td> 
								<input type="text" id="txtDPTDependenciaEconomicaHermano" class="dependienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar trDPTHermanos">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img id="imgDPTSentenciaJudicialHermano" src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkDPTSentenciaJudicialHermano" class="dependienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false); onclickCheckOpcional($(this), $('#chkDPTDependenciaEconomicaHermano'));" value="2815"> 
									Sentencia Judicial.
							</td>
							<td> 
								<input type="text" id="txtDPTSentenciaJudicialHermano" class="dependienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar trDPTHermanos">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkDPTCertificadoHermano" class="dependienteTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="2816"> 
									Certificados.
							</td>
							<td> 
								<input type="text" id="txtDPTCertificadoHermano" class="dependienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>											
						<tr align="left" class="trDPTGrupoFamiliar">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkDPTPadres" class="dependienteTab contenedor" onChange="onclickCheckContenedor($(this),1);"> 
									Padres que dependen econ&oacute;micamente. 
							</td>
							<td> 
								&nbsp;
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar trDPTPadres">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkDPTRegistroCivilAfiliado" class="dependienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="2817"> 
									Registro Civil Afiliado
							</td>
							<td> 
								<input type="text" id="txtDPTRegistroCivilAfiliado" class="dependienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar trDPTPadres">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkDPTCopiaDocumentoPadre" class="dependienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="2818"> 
									Fotocopia documento Padre.
							</td>
							<td> 
								<input type="text" id="txtDPTCopiaDocumentoPadre" class="dependienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar trDPTPadres">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkDPTDependenciaEconomicaPadre" class="dependienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="2819"> 
									Formato Dependencia Econ&oacute;mica.
							</td>
							<td> 
								<input type="text" id="txtDPTDependenciaEconomicaPadre" class="dependienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trDPTGrupoFamiliar trDPTPadres">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkDPTCertificadoPadre" class="dependienteTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="2820"> 
									Certificados.
							</td>
							<td> 
								<input type="text" id="txtDPTCertificadoPadre" class="dependienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
					</table></td>
				</tr>
				
	<!-- FORMULARIO INDEPENDIENTE -->
				<tr align="left" width="580px" class="independienteTab">
					<td> <table margin-left: 17px;" border="0" width="580px">
						<tr align="left">
							<td>
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkIPTFormularioAfiliacion" class="independienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="29261"> 
									Formulario afiliaci&oacute;n. 
							</td>
							<td> 
								<input type="text" id="txtIPTFormularioAfiliacion" class="independienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkIPTCopiaDocumentoAfiliado" class="independienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="29262"> 
									Fotocopia documento identidad. 
							</td>
							<td> 
								<input type="text" id="txtIPTCopiaDocumentoAfiliado" class="independienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="independienteFacultativo" >
							<td>
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkIPTConstanciaAportes" class="independienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="29263"> 
									Constancia de pago de aportes a EPS y AFP. 
							</td>
							<td> 
								<input type="text" id="txtIPTConstanciaAportes" class="independienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="independientePensionadoVoluntario" >
							<td>
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkIPTCertificadoPension" class="independienteTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="29264"> 
									Certificado, desprendible de pensi&oacute;n, &oacute; resoluci&oacute;n. 
							</td>
							<td> 
								<input type="text" id="txtIPTCertificadoPension" class="independienteTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>					
					</table></td>
				</tr>
				
	<!-- FORMULARIO RENOVACION -->
				<tr align="left" width="580px" class="renovacionTab">
					<td> <table margin-left: 17px;" border="0" width="580px">
						<tr align="left">
							<td>
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkRNTFormularioAfiliacion" class="renovacionTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="291"> 
									Formulario afiliaci&oacute;n. 
							</td>
							<td> 
								<input type="text" id="txtRNTFormularioAfiliacion" class="renovacionTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkRNTCopiaDocumentoAfiliado" class="renovacionTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="292"> 
									Fotocopia documento identidad. 
							</td>
							<td> 
								<input type="text" id="txtRNTCopiaDocumentoAfiliado" class="renovacionTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkRNTGrupoFamiliar" class="renovacionTab contenedor" onChange="onclickCheckContenedor($(this),0);"> 
									Grupo Familiar. 
							</td>
							<td> 
								&nbsp;
							</td>
						</tr>
						<tr align="left" class="trRNTGrupoFamiliar">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkRNTConvivencias" class="renovacionTab contenedor" onChange="onclickCheckContenedor($(this),1);"> 
									Convivencias. 
							</td>
							<td> 
								&nbsp;
							</td>
						</tr>
						<tr align="left" class="trRNTGrupoFamiliar trRNTConvivencias">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkRNTCopiaDocumentoConyuge" class="renovacionTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="293"> 
									Fotocopia Documento Conyuge. 
							</td>
							<td> 
								<input type="text" id="txtRNTCopiaDocumentoConyuge" class="renovacionTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trRNTGrupoFamiliar trRNTConvivencias">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img id="imgRNTCertificadoConvivencia" src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkRNTCertificadoConvivencia" class="renovacionTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false); onclickCheckOpcional($(this), $('#chkRNTRegistroConvivencia'));" value="294"> 
									Certificado Convivencia. 
							</td>
							<td> 
								<input type="text" id="txtRNTCertificadoConvivencia" class="renovacionTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trRNTGrupoFamiliar trRNTConvivencias">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img id="imgRNTRegistroConvivencia" src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkRNTRegistroConvivencia" class="renovacionTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false); onclickCheckOpcional($(this), $('#chkRNTCertificadoConvivencia'));" value="295"> 
									Registro Convivencia. 
							</td>
							<td> 
								<input type="text" id="txtRNTRegistroConvivencia" class="renovacionTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>						
						<tr align="left" class="trRNTGrupoFamiliar">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkRNTHijos" class="renovacionTab contenedor" onChange="onclickCheckContenedor($(this),1);"> 
									Hijos menores de 19 a&ntilde;os.
							</td>
							<td> 
								&nbsp;
							</td>
						</tr>
						<tr align="left" class="trRNTGrupoFamiliar trRNTHijos">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkRNTCopiaDocumentoHijo" class="renovacionTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="296"> 
									Fotocopia documento Hijo.
							</td>
							<td> 
								<input type="text" id="txtRNTCopiaDocumentoHijo" class="renovacionTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trRNTGrupoFamiliar trRNTHijos">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkRNTDependenciaEconomicaHijo" class="renovacionTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="297"> 
									Formato Dependencia Econ&oacute;mica.
							</td>
							<td> 
								<input type="text" id="txtRNTDependenciaEconomicaHijo" class="renovacionTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trRNTGrupoFamiliar trRNTHijos">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkRNTCertificadoHijo" class="renovacionTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="298"> 
									Certificados.
							</td>
							<td> 
								<input type="text" id="txtRNTCertificadoHijo" class="renovacionTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trRNTGrupoFamiliar">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkRNTHijastros" class="renovacionTab contenedor" onChange="onclickCheckContenedor($(this),1);"> 
									Hijastros menores de 19 a&ntilde;os. 
							</td>
							<td> 
								&nbsp;
							</td>
						</tr>
						<tr align="left" class="trRNTGrupoFamiliar trRNTHijastros">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkRNTCopiaDocumentoHijastro" class="renovacionTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="299"> 
									Fotocopia documento Hijastro.
							</td>
							<td> 
								<input type="text" id="txtRNTCopiaDocumentoHijastro" class="renovacionTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trRNTGrupoFamiliar trRNTHijastros">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img id="imgRNTDependenciaEconomicaHijastro" src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkRNTDependenciaEconomicaHijastro" class="renovacionTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false); onclickCheckOpcional($(this), $('#chkRNTSentenciaJudicialHijastro'));" value="2910"> 
									Formato Dependencia Econ&oacute;mica.
							</td>
							<td> 
								<input type="text" id="txtRNTDependenciaEconomicaHijastro" class="renovacionTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trRNTGrupoFamiliar trRNTHijastros">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img id="imgRNTSentenciaJudicialHijastro" src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkRNTSentenciaJudicialHijastro" class="renovacionTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false); onclickCheckOpcional($(this), $('#chkRNTDependenciaEconomicaHijastro'));" value="2911"> 
									Sentencia Judicial.
							</td>
							<td> 
								<input type="text" id="txtRNTSentenciaJudicialHijastro" class="renovacionTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trRNTGrupoFamiliar trRNTHijastros">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkRNTCertificadoHijastro" class="renovacionTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="2912"> 
									Certificados.
							</td>
							<td> 
								<input type="text" id="txtRNTCertificadoHijastro" class="renovacionTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trRNTGrupoFamiliar">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkRNTHermanos" class="renovacionTab contenedor" onChange="onclickCheckContenedor($(this),1);"> 
									Hermanos que dependen econ&oacute;micamente. 
							</td>
							<td> 
								&nbsp;
							</td>
						</tr>
						<tr align="left" class="trRNTGrupoFamiliar trRNTHermanos">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkRNTCopiaDocumentoHermano" class="renovacionTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="2913"> 
									Fotocopia documento Hermano.
							</td>
							<td> 
								<input type="text" id="txtRNTCopiaDocumentoHermano" class="renovacionTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trRNTGrupoFamiliar trRNTHermanos">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img id="imgRNTDependenciaEconomicaHermano" src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkRNTDependenciaEconomicaHermano" class="renovacionTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false); onclickCheckOpcional($(this), $('#chkRNTSentenciaJudicialHermano'));" value="2914"> 
									Formato Dependencia Econ&oacute;mica.
							</td>
							<td> 
								<input type="text" id="txtRNTDependenciaEconomicaHermano" class="renovacionTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trRNTGrupoFamiliar trRNTHermanos">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img id="imgRNTSentenciaJudicialHermano" src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkRNTSentenciaJudicialHermano" class="renovacionTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false); onclickCheckOpcional($(this), $('#chkRNTDependenciaEconomicaHermano'));" value="2915"> 
									Sentencia Judicial.
							</td>
							<td> 
								<input type="text" id="txtRNTSentenciaJudicialHermano" class="renovacionTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trRNTGrupoFamiliar trRNTHermanos">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkRNTCertificadoHermano" class="renovacionTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="2916"> 
									Certificados.
							</td>
							<td> 
								<input type="text" id="txtRNTCertificadoHermano" class="renovacionTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>											
						<tr align="left" class="trRNTGrupoFamiliar">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkRNTPadres" class="renovacionTab contenedor" onChange="onclickCheckContenedor($(this),1);"> 
									Padres que dependen econ&oacute;micamente. 
							</td>
							<td> 
								&nbsp;
							</td>
						</tr>
						<tr align="left" class="trRNTGrupoFamiliar trRNTPadres">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkRNTRegistroCivilAfiliado" class="renovacionTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="2917"> 
									Registro Civil Afiliado
							</td>
							<td> 
								<input type="text" id="txtRNTRegistroCivilAfiliado" class="renovacionTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trRNTGrupoFamiliar trRNTPadres">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkRNTCopiaDocumentoPadre" class="renovacionTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="2858"> 
									Fotocopia documento Padre.
							</td>
							<td> 
								<input type="text" id="txtRNTCopiaDocumentoPadre" class="renovacionTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trRNTGrupoFamiliar trRNTPadres">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkRNTDependenciaEconomicaPadre" class="renovacionTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="2919"> 
									Formato Dependencia Econ&oacute;mica.
							</td>
							<td> 
								<input type="text" id="txtRNTDependenciaEconomicaPadre" class="renovacionTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trRNTGrupoFamiliar trRNTPadres">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkRNTCertificadoPadre" class="renovacionTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="2920"> 
									Certificados.
							</td>
							<td> 
								<input type="text" id="txtRNTCertificadoPadre" class="renovacionTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
					</table></td>
				</tr>
				
	<!-- FORMULARIO AFILIACION EMPRESA -->
				<tr align="left" width="580px" class="afiliacionEmpresaTab">
					<td> <table margin-left: 17px;" border="0" width="580px">
						<tr align="left">
							<td>
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkAFEFormularioAfiliacion" class="afiliacionEmpresaTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="301"> 
									Formulario afiliaci&oacute;n. 
							</td>
							<td> 
								<input type="text" id="txtAFEFormularioAfiliacion" class="afiliacionEmpresaTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkAFERUT" class="afiliacionEmpresaTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="302"> 
									RUT. 
							</td>
							<td> 
								<input type="text" id="txtAFERUT" class="afiliacionEmpresaTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkAFECopiaDocumentoRepresentante" class="afiliacionEmpresaTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="303"> 
									Fotocopia Documento Identidad Representante. 
							</td>
							<td> 
								<input type="text" id="txtAFECopiaDocumentoRepresentante" class="afiliacionEmpresaTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								&nbsp;&nbsp;&nbsp;
								<input type="radio" name="radAFETipoPersona" id="chkAFEPersonaJuridica" class="afiliacionEmpresaTab contenedor" onChange="onclickRadioContenedor($('input[name=radAFETipoPersona]'));"> 
									Persona Jur&iacute;dica. 
							</td>
							<td> 
								&nbsp;
							</td>
						</tr>
						<tr align="left">
							<td>
								&nbsp;&nbsp;&nbsp;
								<input type="radio" name="radAFETipoPersona" id="chkAFEPersonaNatural" class="afiliacionEmpresaTab contenedor" onChange="onclickRadioContenedor($('input[name=radAFETipoPersona]'));"> 
									Persona Natural 
							</td>
							<td> 
								&nbsp;
							</td>
						</tr>
						<tr align="left" class="trAFEPersonaJuridica">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkAFECamaraComercio" class="afiliacionEmpresaTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="304"> 
									C&aacute;mara de Comercio.
							</td>
							<td> 
								<input type="text" id="txtAFECamaraComercio" class="afiliacionEmpresaTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trAFEPersonaJuridica">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;								
								<input type="checkbox" id="chkAFENomina" class="afiliacionEmpresaTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="305"> 
									N&oacute;mina.
							</td>
							<td> 
								<input type="text" id="txtAFENomina" class="afiliacionEmpresaTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trAFEPersonaJuridica">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkAFEAfiliadoOtraCaja" class="afiliacionEmpresaTab contenedor" onChange="onclickCheckContenedor($(this),1);"> 
									Estuvo afiliado a otra caja. 
							</td>
							<td> 
								&nbsp;
							</td>
						</tr>
						<tr align="left" class="trAFEPersonaJuridica trAFEAfiliadoOtraCaja">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkAFEPazySalvo" class="afiliacionEmpresaTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="306"> 
									Paz y salvo.
							</td>
							<td> 
								<input type="text" id="txtAFEPazySalvo" class="afiliacionEmpresaTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trAFEPersonaJuridica">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkAFESinAnimoLucro" class="afiliacionEmpresaTab contenedor" onChange="onclickCheckContenedor($(this),1);"> 
									Es Sin &Aacute;nimo de Lucro. 
							</td>
							<td> 
								&nbsp;
							</td>
						</tr>
						<tr align="left" class="trAFEPersonaJuridica trAFESinAnimoLucro">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkAFEPersoneriaJuridica" class="afiliacionEmpresaTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="307"> 
									Personer&iacute;a Jur&iacute;dica.
							</td>
							<td> 
								<input type="text" id="txtAFEPersoneriaJuridica" class="afiliacionEmpresaTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trAFEPersonaJuridica">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkAFECooperativa" class="afiliacionEmpresaTab contenedor" onChange="onclickCheckContenedor($(this),1);"> 
									Es Cooperativa. 
							</td>
							<td> 
								&nbsp;
							</td>
						</tr>
						<tr align="left" class="trAFEPersonaJuridica trAFECooperativa">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkAFECertificadoSuper" class="afiliacionEmpresaTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="308"> 
									Certificado Superintendencia de econom&iacute;a solidaria.
							</td>
							<td> 
								<input type="text" id="txtAFECertificadoSuper" class="afiliacionEmpresaTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trAFEPersonaJuridica">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkAFEPropiedadHorizontal" class="afiliacionEmpresaTab contenedor" onChange="onclickCheckContenedor($(this),1);"> 
									Es Propiedad horizontal. 
							</td>
							<td> 
								&nbsp;
							</td>
						</tr>
						<tr align="left" class="trAFEPersonaJuridica trAFEPropiedadHorizontal">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkAFEActaNombramiento" class="afiliacionEmpresaTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="309"> 
									Acta de nombramiento de administraci&oacute;n Actual.
							</td>
							<td> 
								<input type="text" id="txtAFEActaNombramiento" class="afiliacionEmpresaTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trAFEPersonaJuridica">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkAFEConsorcioUnion" class="afiliacionEmpresaTab contenedor" onChange="onclickCheckContenedor($(this),1);"> 
									Es Consorcio o Uni&oacute;n Temporal. 
							</td>
							<td> 
								&nbsp;
							</td>
						</tr>
						<tr align="left" class="trAFEPersonaJuridica trAFEConsorcioUnion">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkAFEActaConformacion" class="afiliacionEmpresaTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="3010"> 
									Acta de conformaci&oacute;n.
							</td>
							<td> 
								<input type="text" id="txtAFEActaConformacion" class="afiliacionEmpresaTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trAFEPersonaNatural">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkAFENAfiliadoOtraCaja" class="afiliacionEmpresaTab contenedor" onChange="onclickCheckContenedor($(this),1);"> 
									Estuvo afiliado a otra caja. 
							</td>
							<td> 
								&nbsp;
							</td>
						</tr>
						<tr align="left" class="trAFEPersonaNatural trAFENAfiliadoOtraCaja">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkAFENPazySalvo" class="afiliacionEmpresaTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="306"> 
									Paz y salvo.
							</td>
							<td> 
								<input type="text" id="txtAFENPazySalvo" class="afiliacionEmpresaTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>											
					</table></td>
				</tr>
				
	<!-- FORMULARIO CREAR CONVIVENCIA -->
				<tr align="left" width="580px" class="crearConvivenciaTab">
					<td> <table margin-left: 17px;" border="0" width="580px">
						<tr align="left">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkCCTCopiaDocumentoConyuge" class="crearConvivenciaTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="283"> 
									Fotocopia Documento Conyuge. 
							</td>
							<td> 
								<input type="text" id="txtCCTCopiaDocumentoConyuge" class="crearConvivenciaTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img id="imgCCTCertificadoConvivencia" src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkCCTCertificadoConvivencia" class="crearConvivenciaTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false); onclickCheckOpcional($(this), $('#chkCCTRegistroConvivencia'));" value="284"> 
									Certificado Convivencia. 
							</td>
							<td> 
								<input type="text" id="txtCCTCertificadoConvivencia" class="crearConvivenciaTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img id="imgCCTRegistroConvivencia" src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkCCTRegistroConvivencia" class="crearConvivenciaTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false); onclickCheckOpcional($(this), $('#chkCCTCertificadoConvivencia'));" value="285"> 
									Registro Convivencia. 
							</td>
							<td> 
								<input type="text" id="txtCCTRegistroConvivencia" class="crearConvivenciaTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>					
					</table></td>
				</tr>
				
	<!-- FORMULARIO DEFUNCION -->
				<tr align="left" width="580px" class="defuncionTab">
					<td> <table margin-left: 17px;" border="0" width="580px">
						<tr align="left">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkDFTRegistroDefuncion" class="defuncionTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="283"> 
									Registro de Defunci&oacute;n. 
							</td>
							<td> 
								<input type="text" id="txtDFTRegistroDefuncion" class="defuncionTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkDFTCartaSolicitud" class="defuncionTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="283"> 
									Carta de Solicitud.
							</td>
							<td> 
								<input type="text" id="txtDFTCartaSolicitud" class="defuncionTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkDFTCopiaDocumentoSolicitante" class="defuncionTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="283"> 
									Fotocopia del documento de identidad del solicitante.  
							</td>
							<td> 
								<input type="text" id="txtDFTCopiaDocumentoSolicitante" class="defuncionTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trDFTCertificadoRetiro">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkDFTCertificadoRetiro" class="defuncionTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="283"> 
									Certificado de retiro de la empresa. 
							</td>
							<td> 
								<input type="text" id="txtDFTCertificadoRetiro" class="defuncionTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>											
					</table></td>
				</tr>
				
	<!-- FORMULARIO BLOQUEO TARJETA -->
				<tr align="left" width="580px" class="bloqueoTarjetaTab">
					<td> <table margin-left: 17px;" border="0" width="580px">
						<tr align="left" class="trBTTTarjetaDeteriorada">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkBTTTarjetaDeteriorada" class="bloqueoTarjetaTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="1951"> 
									Tarjeta deteriorada. 
							</td>
							<td> 
								<input type="text" id="txtBTTTarjetaDeteriorada" class="bloqueoTarjetaTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>																	
						<tr align="left" class="trBTTDenuncia">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkBTTDenuncia" class="bloqueoTarjetaTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="1952"> 
									Denuncia. 
							</td>
							<td> 
								<input type="text" id="txtBTTDenuncia" class="bloqueoTarjetaTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
					</table></td>
				</tr>
				
	<!-- FORMULARIO POSTULACION FONEDE -->
				<tr align="left" width="580px" class="postulacionFonedeTab">
					<td> <table margin-left: 17px;" border="0" width="580px">
						<tr align="left">
							<td>
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkPFTCopiaDocumentoPostulante" class="postulacionFonedeTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="691"> 
									Copia documento identidad Postulante. 
							</td>
							<td> 
								<input type="text" id="txtPFTCopiaDocumentoPostulante" class="postulacionFonedeTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>						
						<tr align="left">
							<td>
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkPFTCopiaDocumentoMenores" class="postulacionFonedeTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="692"> 
									Registro civil de nacimiento menores de 18 a&ntilde;os. 
							</td>
							<td> 
								<input type="text" id="txtPFTCopiaDocumentoMenores" class="postulacionFonedeTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkPFTCopiaDocumentoMayores" class="postulacionFonedeTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="693"> 
									Copia documento identidad mayores de 18 a&ntilde;os. 
							</td>
							<td> 
								<input type="text" id="txtPFTCopiaDocumentoMayores" class="postulacionFonedeTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="conPostulacion">
							<td>
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkPFTCertificadoEPS" class="postulacionFonedeTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="694"> 
									Certificado expedido por la &uacute;ltima E.P.S donde estuvo afiliado. 
							</td>
							<td> 
								<input type="text" id="txtPFTCertificadoEPS" class="postulacionFonedeTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="conPostulacion">
							<td>
								&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkPFTCertificadoLaboral" class="postulacionFonedeTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="695"> 
									Certificado laboral. (CON VINCULACION ANTERIOR A UNA CAJA)
							</td>
							<td> 
								<input type="text" id="txtPFTCertificadoLaboral" class="postulacionFonedeTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="conPostulacion">
							<td>
								&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkPFTCartaTerminacionContrato" class="postulacionFonedeTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="696"> 
									Carta de terminaci&oacute;n de contrato. (CON VINCULACION ANTERIOR A UNA CAJA)
							</td>
							<td> 
								<input type="text" id="txtPFTCartaTerminacionContrato" class="postulacionFonedeTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>												
					</table></td>
				</tr>
				<tr height="20px" >
					<td></td>
				</tr>
				<tr>					
					<td align="center" ><img src="<?php echo URL_PORTAL; ?>imagenes/guardar.png" title="Guardar" style="cursor:pointer" onClick="saveRadicacion();"></td>					
				</tr>
			</table>
		</center>
	</div>
</body>
</html>