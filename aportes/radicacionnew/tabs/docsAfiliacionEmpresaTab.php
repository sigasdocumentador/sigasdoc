<?php
set_time_limit ( 0 );
ini_set ( "display_errors", '1' );
date_default_timezone_set ( "America/Bogota" );

$root = $_SERVER ['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'sigas' .DIRECTORY_SEPARATOR . 'session.php'; 
include_once $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';

$FrmActual=basename($_SERVER['PHP_SELF']);
$fecver = date('Ymd h:i:s A',filectime($FrmActual));
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Documentos</title>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>aportes/radicacionnew/js/tabs/documentosTab.js"></script>
</head>
<body>
	<div>
		<div style="text-align:right; height:20px; top:0px; width:221px; position:absolute; left: 20px;"><?php echo 'Versi&oacute;n: ' . $fecver; ?></div>
		<center>
			<h4>LISTADO DOCUMENTOS A SOLICITAR</h4>
			
			<table width="580px" id="tblDocRequeridos">
			
	<!-- FORMULARIO AFILIACION EMPRESA -->
				<tr align="left" width="580px" class="afiliacionEmpresaTab">
					<td> <table margin-left: 17px;" border="0" width="580px">
						<tr align="left">
							<td>
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkAFEFormularioAfiliacion" class="afiliacionEmpresaTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4039"> 
									Formulario afiliaci&oacute;n. 
							</td>
							<td> 
								<input type="text" id="txtAFEFormularioAfiliacion" class="afiliacionEmpresaTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkAFERUT" class="afiliacionEmpresaTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4040"> 
									RUT. 
							</td>
							<td> 
								<input type="text" id="txtAFERUT" class="afiliacionEmpresaTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkAFECopiaDocumentoRepresentante" class="afiliacionEmpresaTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4041"> 
									Fotocopia Documento Identidad Representante. 
							</td>
							<td> 
								<input type="text" id="txtAFECopiaDocumentoRepresentante" class="afiliacionEmpresaTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								&nbsp;&nbsp;&nbsp;
								<input type="radio" name="radAFETipoPersona" id="chkAFEPersonaJuridica" class="afiliacionEmpresaTab contenedor" onChange="onclickRadioContenedor($('input[name=radAFETipoPersona]'));"> 
									Persona Jur&iacute;dica. 
							</td>
							<td> 
								&nbsp;
							</td>
						</tr>
						<tr align="left">
							<td>
								&nbsp;&nbsp;&nbsp;
								<input type="radio" name="radAFETipoPersona" id="chkAFEPersonaNatural" class="afiliacionEmpresaTab contenedor" onChange="onclickRadioContenedor($('input[name=radAFETipoPersona]'));"> 
									Persona Natural 
							</td>
							<td> 
								&nbsp;
							</td>
						</tr>
						<tr align="left" class="trAFEPersonaJuridica">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkAFECamaraComercio" class="afiliacionEmpresaTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4042"> 
									C&aacute;mara de Comercio.
							</td>
							<td> 
								<input type="text" id="txtAFECamaraComercio" class="afiliacionEmpresaTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trAFEPersonaJuridica">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;								
								<input type="checkbox" id="chkAFESoportePago" class="afiliacionEmpresaTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4043"> 
									Soporte de Pago.
							</td>
							<td> 
								<input type="text" id="txtAFESoportePago" class="afiliacionEmpresaTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trAFEPersonaJuridica">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;								
								<input type="checkbox" id="chkAFENomina" class="afiliacionEmpresaTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4044"> 
									N&oacute;mina.
							</td>
							<td> 
								<input type="text" id="txtAFENomina" class="afiliacionEmpresaTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>						
						<tr align="left" class="trAFEPersonaJuridica">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkAFEAfiliadoOtraCaja" class="afiliacionEmpresaTab contenedor" onChange="onclickCheckContenedor($(this),1);"> 
									Estuvo afiliado a otra caja. 
							</td>
							<td> 
								&nbsp;
							</td>
						</tr>
						<tr align="left" class="trAFEPersonaJuridica trAFEAfiliadoOtraCaja">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkAFEPazySalvo" class="afiliacionEmpresaTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="4045"> 
									Paz y salvo.
							</td>
							<td> 
								<input type="text" id="txtAFEPazySalvo" class="afiliacionEmpresaTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trAFEPersonaJuridica">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkAFESinAnimoLucro" class="afiliacionEmpresaTab contenedor" onChange="onclickCheckContenedor($(this),1);"> 
									Es Sin &Aacute;nimo de Lucro. 
							</td>
							<td> 
								&nbsp;
							</td>
						</tr>
						<tr align="left" class="trAFEPersonaJuridica trAFESinAnimoLucro">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkAFEPersoneriaJuridica" class="afiliacionEmpresaTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="4046"> 
									Personer&iacute;a Jur&iacute;dica.
							</td>
							<td> 
								<input type="text" id="txtAFEPersoneriaJuridica" class="afiliacionEmpresaTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trAFEPersonaJuridica">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkAFECooperativa" class="afiliacionEmpresaTab contenedor" onChange="onclickCheckContenedor($(this),1);"> 
									Es Cooperativa. 
							</td>
							<td> 
								&nbsp;
							</td>
						</tr>
						<tr align="left" class="trAFEPersonaJuridica trAFECooperativa">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkAFECertificadoSuper" class="afiliacionEmpresaTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="4047"> 
									Certificado Superintendencia de econom&iacute;a solidaria.
							</td>
							<td> 
								<input type="text" id="txtAFECertificadoSuper" class="afiliacionEmpresaTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trAFEPersonaJuridica">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkAFEPropiedadHorizontal" class="afiliacionEmpresaTab contenedor" onChange="onclickCheckContenedor($(this),1);"> 
									Es Propiedad horizontal. 
							</td>
							<td> 
								&nbsp;
							</td>
						</tr>
						<tr align="left" class="trAFEPersonaJuridica trAFEPropiedadHorizontal">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkAFEActaNombramiento" class="afiliacionEmpresaTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="4048"> 
									Acta de nombramiento de administraci&oacute;n Actual.
							</td>
							<td> 
								<input type="text" id="txtAFEActaNombramiento" class="afiliacionEmpresaTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trAFEPersonaJuridica">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkAFEConsorcioUnion" class="afiliacionEmpresaTab contenedor" onChange="onclickCheckContenedor($(this),1);"> 
									Es Consorcio o Uni&oacute;n Temporal. 
							</td>
							<td> 
								&nbsp;
							</td>
						</tr>
						<tr align="left" class="trAFEPersonaJuridica trAFEConsorcioUnion">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkAFEActaConformacion" class="afiliacionEmpresaTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="4049"> 
									Acta de conformaci&oacute;n.
							</td>
							<td> 
								<input type="text" id="txtAFEActaConformacion" class="afiliacionEmpresaTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						
						<tr align="left" class="trAFEPersonaNatural">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkAFENACamaraComercio" class="afiliacionEmpresaTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4042"> 
									C&aacute;mara de Comercio.
							</td>
							<td> 
								<input type="text" id="txtAFENACamaraComercio" class="afiliacionEmpresaTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>						
						<tr align="left" class="trAFEPersonaNatural">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;								
								<input type="checkbox" id="chkAFENANomina" class="afiliacionEmpresaTab" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4044"> 
									N&oacute;mina.
							</td>
							<td> 
								<input type="text" id="txtAFENANomina" class="afiliacionEmpresaTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trAFEPersonaNatural">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" id="chkAFENAfiliadoOtraCaja" class="afiliacionEmpresaTab contenedor" onChange="onclickCheckContenedor($(this),1);"> 
									Estuvo afiliado a otra caja. 
							</td>
							<td> 
								&nbsp;
							</td>
						</tr>
						<tr align="left" class="trAFEPersonaNatural trAFENAfiliadoOtraCaja">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkAFENPazySalvo" class="afiliacionEmpresaTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,false);" value="4045"> 
									Paz y salvo.
							</td>
							<td> 
								<input type="text" id="txtAFENPazySalvo" class="afiliacionEmpresaTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>											
					</table></td>
				</tr>				
	
				<tr height="20px" >
					<td></td>
				</tr>
				<tr>					
					<td align="center" ><img src="<?php echo URL_PORTAL; ?>imagenes/guardar.png" title="Guardar" style="cursor:pointer" onClick="saveRadicacion();"></td>					
				</tr>
			</table>
		</center>
	</div>
</body>
</html>