<?php
set_time_limit ( 0 );
ini_set ( "display_errors", '1' );
date_default_timezone_set ( "America/Bogota" );

$root = $_SERVER ['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'sigas' .DIRECTORY_SEPARATOR . 'session.php'; 
include_once $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';

$FrmActual=basename($_SERVER['PHP_SELF']);
$fecver = date('Ymd h:i:s A',filectime($FrmActual));
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Documentos</title>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>aportes/radicacionnew/js/tabs/documentosTab.js"></script>
</head>
<body>
	<div>
		<div style="text-align:right; height:20px; top:0px; width:221px; position:absolute; left: 20px;"><?php echo 'Versi&oacute;n: ' . $fecver; ?></div>
		<center>
			<h4>LISTADO DOCUMENTOS A SOLICITAR</h4>
			
			<table width="580px" id="tblDocRequeridos">
			
	<!-- FORMULARIO MOVILIZACION -->
				<tr align="left" width="580px" class="movilizacionTab">
					<td> <table margin-left: 17px;" border="0" width="580px">
						<tr align="left">
							<td>
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkMVTFormatoDiligenciado" class="movilizacionTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4069"> 
									Formato Completamente Diligenciado. 
							</td>
							<td> 
								<input type="text" id="txtMVTFormatoDiligenciado" class="movilizacionTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkMVTReciboCaja" class="movilizacionTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4072"> 
									Recibo de Caja por valor de $2.500. 
							</td>
							<td> 
								<input type="text" id="txtMVTReciboCaja" class="movilizacionTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								<img id="imgMVTCertificadoBancario" src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkMVTCertificadoBancario" class="movilizacionTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true); onclickCheckOpcional($(this), $('#chkMVTCertificadoCesantias'));" value="4070"> 
									Certificado Bancario Original. 
							</td>
							<td> 
								<input type="text" id="txtMVTCertificadoBancario" class="movilizacionTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								<img id="imgMVTCertificadoCesantias" src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkMVTCertificadoCesantias" class="movilizacionTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true); onclickCheckOpcional($(this), $('#chkMVTCertificadoBancario'));" value="4071"> 
									Certificado Cesantias (No mayor a 30 d&iacute;as). 
							</td>
							<td> 
								<input type="text" id="txtMVTCertificadoCesantias" class="movilizacionTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>										
					</table></td>
				</tr>								
	
				<tr height="20px" >
					<td></td>
				</tr>
				<tr>					
					<td align="center" ><img src="<?php echo URL_PORTAL; ?>imagenes/guardar.png" title="Guardar" style="cursor:pointer" onClick="saveRadicacion();"></td>					
				</tr>
			</table>
		</center>
	</div>
</body>
</html>