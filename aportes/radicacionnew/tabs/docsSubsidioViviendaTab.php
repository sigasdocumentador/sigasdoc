<?php
set_time_limit ( 0 );
ini_set ( "display_errors", '1' );
date_default_timezone_set ( "America/Bogota" );

$root = $_SERVER ['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'sigas' .DIRECTORY_SEPARATOR . 'session.php'; 
include_once $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';

$FrmActual=basename($_SERVER['PHP_SELF']);
$fecver = date('Ymd h:i:s A',filectime($FrmActual));
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Documentos</title>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>aportes/radicacionnew/js/tabs/documentosTab.js"></script>
</head>
<body>
	<div>
		<div style="text-align:right; height:20px; top:0px; width:221px; position:absolute; left: 20px;"><?php echo 'Versi&oacute;n: ' . $fecver; ?></div>
		<center>
			<h4>LISTADO DOCUMENTOS A SOLICITAR</h4>
			
			<table width="580px" id="tblDocRequeridos">
			
	<!-- FORMULARIO MOVILIZACION -->
				<tr align="left" width="580px" class="subsidioViviendaTab">
					<td> <table margin-left: 17px;" border="0" width="580px">
						<tr align="left">
							<td>
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkSVTCopiaCartaAsignacion" class="subsidioViviendaTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4077"> 
									Copia de la carta de asignacion del SFV. 
							</td>
							<td> 
								<input type="text" id="txtSVTCopiaCartaAsignacion" class="subsidioViviendaTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkSVTCopiaEscrituraPublica" class="subsidioViviendaTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4078"> 
									Copia escritura p&uacute;blica o declaraci&oacute;n de contrucci&oacute;n o mejoras. 
							</td>
							<td> 
								<input type="text" id="txtSVTCopiaEscrituraPublica" class="subsidioViviendaTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkSVTCertificadoTradicionLibertad" class="subsidioViviendaTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4074"> 
									Original del certificado de tradicci&oacute;n y libertad no mayor a 30 d&iacute;as.
							</td>
							<td> 
								<input type="text" id="txtSVTCertificadoTradicionLibertad" class="subsidioViviendaTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkSVTActaEntregaVivienda" class="subsidioViviendaTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4079"> 
									Acta de entrega de la vivienda debidamente firmada por las partes.
							</td>
							<td> 
								<input type="text" id="txtSVTActaEntregaVivienda" class="subsidioViviendaTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkSVTOriginalCertificacionBancaria" class="subsidioViviendaTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4080"> 
									Original de la certificaci&oacute;n bancaria del vendedor y oferente. 
							</td>
							<td> 
								<input type="text" id="txtSVTOriginalCertificacionBancaria" class="subsidioViviendaTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="subsidioViviendaAfiliado">
							<td>
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkSVTCuentaCobro" class="subsidioViviendaTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4096"> 
									Cuenta de Cobro. 
							</td>
							<td> 
								<input type="text" id="txtSVTCuentaCobro" class="subsidioViviendaTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left">
							<td>
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkSVTAutorizacionMovilizacion" class="subsidioViviendaTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4081"> 
									Autorizaci&oacute;n movilizaci&oacute;n recurso a cuenta del oferente. 
							</td>
							<td> 
								<input type="text" id="txtSVTAutorizacionMovilizacion" class="subsidioViviendaTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="subsidioViviendaDesplazado">
							<td>
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkSVTDesprendibleAperturaCAP" class="subsidioViviendaTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4082"> 
									Desprendible apertura de la CAP en el Banco Agrario. 
							</td>
							<td> 
								<input type="text" id="txtSVTDesprendibleAperturaCAP" class="subsidioViviendaTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="subsidioViviendaAfiliado">
							<td>
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkSVTCertificadoNormasTecnicas" class="subsidioViviendaTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4097"> 
									Certificado de normas t&eacute;cnicas (Anexo 32).
							</td>
							<td> 
								<input type="text" id="txtSVTCertificadoNormasTecnicas" class="subsidioViviendaTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>										
					</table></td>
				</tr>								
	
				<tr height="20px" >
					<td></td>
				</tr>
				<tr>					
					<td align="center" ><img src="<?php echo URL_PORTAL; ?>imagenes/guardar.png" title="Guardar" style="cursor:pointer" onClick="saveRadicacion();"></td>					
				</tr>
			</table>
		</center>
	</div>
</body>
</html>