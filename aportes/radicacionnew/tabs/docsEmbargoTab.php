<?php
set_time_limit ( 0 );
ini_set ( "display_errors", '1' );
date_default_timezone_set ( "America/Bogota" );

$root = $_SERVER ['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'sigas' .DIRECTORY_SEPARATOR . 'session.php'; 
include_once $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';

$FrmActual=basename($_SERVER['PHP_SELF']);
$fecver = date('Ymd h:i:s A',filectime($FrmActual));
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Documentos</title>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>aportes/radicacionnew/js/tabs/documentosTab.js"></script>
</head>
<body>
	<div>
		<div style="text-align:right; height:20px; top:0px; width:221px; position:absolute; left: 20px;"><?php echo 'Versi&oacute;n: ' . $fecver; ?></div>
		<center>
			<h4>LISTADO DOCUMENTOS A SOLICITAR</h4>
			
			<table width="580px" id="tblDocRequeridos">
			
	<!-- FORMULARIO EMBARGO -->
				<tr align="left" width="580px" class="embargoTab">
					<td> <table margin-left: 17px;" border="0" width="580px">
						<tr align="left">
							<td>
								&nbsp;&nbsp;&nbsp;
								<input type="radio" name="radEMBTipoEmbargo" id="chkEMBEmbargoJuzgado" class="embargoTab contenedor" onChange="onclickRadioContenedor($('input[name=radEMBTipoEmbargo]'));"> 
									JUZGADO. 
							</td>
							<td> 
								&nbsp;
							</td>
						</tr>
						<tr align="left">
							<td>
								&nbsp;&nbsp;&nbsp;
								<input type="radio" name="radEMBTipoEmbargo" id="chkEMBEmbargoAcuerdo" class="embargoTab contenedor" onChange="onclickRadioContenedor($('input[name=radEMBTipoEmbargo]'));"> 
									ACUERDO MUTUO. 
							</td>
							<td> 
								&nbsp;
							</td>
						</tr>
						<tr align="left" class="trEMBEmbargoJuzgado">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkEMBOrdenJuzgado" class="embargoTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4151"> 
									Orden del Juzgado.
							</td>
							<td> 
								<input type="text" id="txtEMBOrdenJuzgado" class="embargoTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>						
						<tr align="left" class="trEMBEmbargoAcuerdo">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkEMBPersonaAutoriza" class="embargoTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4152"> 
									Fotocopia de la identificaci&oacute;n de Persona a la que Autorizan. 
							</td>
							<td> 
								<input type="text" id="txtEMBPersonaAutoriza" class="embargoTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>
						<tr align="left" class="trEMBEmbargoAcuerdo">
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img src="<?php echo URL_PORTAL; ?>imagenes/menu/obligado.png" width="12" height="12">
								<input type="checkbox" id="chkEMBAutorizacionAutentificada" class="embargoTab requerido" onChange="onclickCheck($(this),$('#txt' + (this.id).substring(3)),true,true);" value="4153"> 
									Autorizaci&oacute;n Autentificada. 
							</td>
							<td> 
								<input type="text" id="txtEMBAutorizacionAutentificada" class="embargoTab boxcorto2" onblur="validarText($(this),$('#chk' + (this.id).substring(3)));" style="display: none" maxlength="2">
							</td>
						</tr>											
					</table></td>
				</tr>				
	
				<tr height="20px" >
					<td></td>
				</tr>
				<tr>					
					<td align="center" >
						<img src="<?php echo URL_PORTAL; ?>imagenes/guardar.png" title="Guardar" style="cursor:pointer" onClick="saveRadicacion();"> &nbsp;&nbsp;&nbsp;
						<img src="<?php echo URL_PORTAL; ?>imagenes/devolver2.png" title="Devolver" style="cursor:pointer" onClick="saveDevolucion(1);">
					</td>					
				</tr>
			</table>
		</center>
	</div>
</body>
</html>