<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'grupo.familiar.class.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'p.afiliacion.class.php';

$idpersona=$_REQUEST['v0'];
$objConyuge=new GrupoFamiliar();
$objAfiliacion= new Afiliacion();
$consulta=$objConyuge->buscarIdConyugeConvive($idpersona);
$con=0;
$idpc=0;
while($row=mssql_fetch_array($consulta)){
	$con++;
	$idpc=$row['idconyuge'];
}
if($con==0){
	echo "<script> alert('El trabajor no tiene relacion de convivencia!') </script>";
	exit();
}

$emp=$objAfiliacion->buscar_afiliacion_primaria($idpc);
$con=0;
$idec=0;
while ($row=mssql_fetch_array($emp)){
	$con++;
	$idec=$row['idempresa'];
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Consulta</title>
<script type="text/javascript">
	var URL=src();
	$(function(){
		$("#tabs2").tabs({
		    select:function(event, ui){
		    },
		    show:function(event){
		        $("#tabs2 > iframe").height($(window).height());
		        $("#tabs2 > iframe").width($(window).width());    
		    },
		    fx: {
		        opacity:'toggle',
		        duration:100
		    }
		});
	
		$("#tabs2").show("slow",function(){
			$("#tabs2 ul li:first a").trigger("click");
		});
	
	
		$("#tabs2 ul li a").bind("click",function(){
			indexTabC=$(this).attr("alt");
			href=$(this).attr('href');
			var idec=$("#txtIdeC").val();
			var idpc=$("#txtIdpC").val();
		
			switch(indexTabC){
				case 'a' : $(href).load(URL+"aportes/trabajadores/tabConyuge/fichaTabCon.php?v0="+idpc+"&flag=2"); break;
				case 'b' : $(href).load(URL+"aportes/trabajadores/tabConyuge/reclamosTabCon.php?v0="+idpc+"&flag=2"); break;
				case 'c' : $(href).load(URL+"aportes/trabajadores/tabConyuge/embargosTabCon.php?v0="+idpc+"&flag=2"); break;
				case 'd' : $(href).load(URL+"aportes/trabajadores/tabConyuge/aportesTabCon.php?v0="+idec+"&flag=2"); break;
				case 'e' : $(href).load(URL+"aportes/trabajadores/tabConyuge/planillaTabCon.php?v0="+idpc+"&flag=2"); break;
				case 'f' : $(href).load(URL+"aportes/trabajadores/tabConyuge/girosTabCon.php?v0="+idpc+"&flag=2"); break;
				case 'g' : $(href).load(URL+"aportes/trabajadores/tabConyuge/tarjetaTabCon.php?v0="+idpc+"&flag=2"); break;
				case 'h' : $(href).load(URL+"aportes/trabajadores/tabConyuge/movimientosTabCon.php?v0="+idpc+"&flag=2"); break;
				case 'i' : $(href).load(URL+"aportes/trabajadores/tabConyuge/causalesTabCon.php?v0="+idpc+"&flag=2"); break;
				case 'j' : $(href).load(URL+"aportes/trabajadores/tabConyuge/documentosTabCon.php?v0="+idpc+"&flag=2"); break;
			}
		});
	});
</script>
</head>
<body>
<input type="hidden" id="txtIdpC" value="<?php echo $idpc; ?>" />
<input type="hidden" id="txtIdeC" value="<?php echo $idec; ?>" />

<div id="tabs2" style="display:none;">
	<ul>
		<li><a href='#tabs2-1' alt='a'>Conyuge</a></li>
		<li><a href='#tabs2-2' alt='b'>Reclamos</a></li>
		<li><a href='#tabs2-3' alt='c'>Descuentos</a></li>
		<li><a href='#tabs2-4' alt='d'>Aportes</a></li>
		<li><a href='#tabs2-5' alt='e'>Planilla</a></li>
		<li><a href='#tabs2-6' alt='f'>Subsidio</a></li>
		<li><a href='#tabs2-7' alt='g'>Cargues</a></li>
		<li><a href='#tabs2-8' alt='h'>Movimientos</a></li>
		<li><a href='#tabs2-9' alt='i'>Causales</a></li>
		<li><a href='#tabs2-10' alt='j'>Documentos</a></li>
	</ul>

	<div id='tabs2-1'></div>
	<div id='tabs2-2'></div>
	<div id='tabs2-3'></div>
	<div id='tabs2-4'></div>
	<div id='tabs2-5'></div>
	<div id='tabs2-6'></div>
	<div id='tabs2-7'></div>
	<div id='tabs2-8'></div>
	<div id='tabs2-9'></div>
	<div id='tabs2-10'></div>
</div>
</body>
</html>
