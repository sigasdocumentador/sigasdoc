<?php
/* autor:       orlando puentes
 * fecha:       30/08/2010
 * objetivo:     
 */
 
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$idpersona= $_REQUEST['v0'];
include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR .'p.tarjeta.class.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';
$objClase=new Tarjeta();
$consulta = $objClase->buscar_tarjeta($idpersona);
$row=mssql_fetch_array($consulta);
$bono=$row['bono'];
$cont=0;
 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Movimientos</title>
<link href="<?php echo URL_PORTAL; ?>css/sorterStyle.css" rel="stylesheet" type="text/css" media="screen" />
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/comunes.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/jquery.tablesorter.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/jquery.tablesorter.pager.js"></script>
<script type="text/javascript">
$(document).ready(function(){	
	$("#tMovimientos").tablesorter();
	refrescar();
});
  
function refrescar(){
	$("#tMovimientos tbody tr").remove();
	var num=$("#nRegistrosMovimientos").val();
	var idp=$("#txtIdb").val();
	
	$.getJSON(URL+'phpComunes/pdo.buscar.movimientos.php',{v0:idp,v1:num},function(data){
		if(data==0){
			alert("Lo lamento, no hay movimientos asociados a este bono!");
			return false;
		}
		$.each(data,function(i,n){
	
			var no=data.length;
			$("#tMovimientos caption span").html(no+" registros encontrados."); 
			$("#tMovimientos tbody").append("<tr><td>"+n.idconsumo+"</td><td>"+n.fechatransaccion+' '+n.horatransaccion+"</td><td>"+n.descripcion+"</td><td style=text-align:right>"+formatCurrency(n.valor)+"</td><td>"+n.numdispositivo+"</td></tr>");  
		});
		 
		$("#tMovimientos").trigger("update");
		$("#tMovimientos tbody tr:odd").addClass("evenrow");
		return;
	});
}
</script>
</head>
<body>
 <h4>Tarjeta - Saldo - Movimientos</h4>
<center>
<table width="60%" border="0" style="border:1px dashed #CCC" >
<tr>
<td width="65%"><label style="font-size:16px; color:#333; font-weight:bold">Valor disponible para retiro</label></td>
<td width="32%" style="text-align:right"><label style="font-size:16px; color:#F00; font-weight:bold;">
<?php echo number_format( $row['saldo'] ); ?></label></td>
</tr>
<tr>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
</tr>
<tr>
  <td><label style="font-size:16px; color:#333; font-weight:bold">N&uacute;mero de TARJETA</label></td>
  <td style="text-align:right"><label style="font-size:16px; color:#F00; font-weight:bold">
  <?php echo $row['bono']; ?>&nbsp;</label></td>
</tr>
<tr>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
</tr>
<tr>
  <td><label style="font-size:16px; color:#333; font-weight:bold">Estado de TARJETA</label></td>
  <td style="text-align:right"><label style="font-size:16px; color:#F00; font-weight:bold">
  <?php echo $row['estado']; ?>&nbsp;(<?php echo $row["detalledefinicion"]; ?>)</label></td>
</tr>
</table>
<label class="Rojo">Movimientos de la nueva Tarjeta </label>
</center>
<br />
<table width="100%" border="0" cellspacing="0" class="sortable hover" id="tMovimientos">
<caption style="text-align:left; padding:5px; font-size:11px;"><span></span></caption>
  <thead>
  <tr>
<th class="head"><h3><strong>Item</strong></h3></th>
<th class="head"><h3><strong>Fecha</strong></h3></th>
<th class="head"><h3><strong>Tipo</strong></h3></th>
<th class="head"><h3><strong>Valor</strong></h3></th>
<th class="head"><h3><strong>Fuente</strong></h3></th>
</tr>
</thead>
<tbody>

</tbody>
</table>
<div id="div-registros">N&uacute;mero de Registros: 
<select name="nRegistrosMovimientos" id="nRegistrosMovimientos" onchange="refrescar();">
	<option selected="selected"  value="10" >10</option>
	<option value="20">20</option>
	<option value="30">30</option>
	<option  value="40">40</option>
    <option  value="50">50</option>
</select>
</div> 
<br />
<label class="Rojo">Movimientos de la ANTERIOR Tarjeta </label>
<table width="100%" border="0" cellspacing="0" class="sortable hover" id="tAnterior">
	<caption style="text-align:left; padding:5px; font-size:11px;"><span></span></caption>
	  <thead>
	  	<tr>
			<th class="head"><h3><strong>Item</strong></h3></th>
			<th class="head"><h3><strong>Fecha</strong></h3></th>
			<th class="head"><h3><strong>Tipo</strong></h3></th>
			<th class="head"><h3><strong>Valor</strong></h3></th>
			<th class="head"><h3><strong>Fuente</strong></h3></th>
		</tr>
	</thead>
	<tbody>
	
	</tbody>
</table>
<div id="div-registros2">N&uacute;mero de Registros: 
<select name="nRegistrosMovimientos2" id="nRegistrosMovimientos2" onchange="refrescar();">
	<option selected="selected"  value="10" >10</option>
	<option value="20">20</option>
	<option value="30">30</option>
	<option  value="40">40</option>
    <option  value="50">50</option>
</select>
</div> 
<input type="hidden" id="txtIdb" value="<?php echo $bono; ?>" />
<input type="hidden" id="txtIdb2" value="<?php echo $bono2; ?>" />
</body>
</html>
