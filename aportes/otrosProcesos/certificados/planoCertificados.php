<?php
/* autor:       orlando puentes
 * fecha:       05/10/2010
 * objetivo:    
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$url=$_SERVER['PHP_SELF'];
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'auditoria.php';
auditar($url);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Plano Certificados</title>
<link href="../../../css/Estilos.css" rel="stylesheet" type="text/css" />
<link type="text/css" href="../../../css/formularios/base/ui.all.css" rel="stylesheet" />
<script language="javascript" src="../../../js/jquery-1.4.2.js"></script>
<script type="text/javascript" src="../../../js/formularios/ui/ui.core.js"></script>
<script type="text/javascript" src="../../../js/formularios/ui/ui.tabs.js"></script>
<script type="text/javascript" src="../../../js/formularios/ui/ui.resizable.js"></script>
<script type="text/javascript" src="../../../js/formularios/ui/ui.draggable.js"></script>
<script type="text/javascript" src="../../../js/formularios/ui/ui.dialog.js"></script>
<script language="javascript" src="../../../js/jquery.MultiFile.js"></script>
<script type="text/jscript" src="js/certificados.js"></script>
<script>
$(document).ready(function(){
	$("#ayuda").dialog({
		autoOpen: false,
		height: 450,
		width: 700,
		draggable:true,
		modal:false,
		open: function(evt, ui){
				$('#ayuda').html('');
				$.get('../../../help/aportes/manualAyudaplanoCertificados.html',function(data){
						$('#ayuda').html(data);
				})
		 }
	});
	
	$("#dialog-form2").dialog({
		autoOpen: false,
		height: 400,
		width: 500,
		modal: true,
		buttons: {
			'Enviar': function() {
			var bValid = true;
			var campo=$('#notas').val();
			var campo0=$.trim(campo);
			if (campo0==""){
				$(this).dialog('close');
					return false;
				}
			var campo1=$('#usuario').val();
			var campo2="planoCertificados.php";
			$.post(URL+'phpComunes/colaboracion.php',{v0:campo0,v1:campo1,v2:campo2},
			function(datos){
				if(datos=='1'){
					alert("Su comentario fue enviado correctamente!. Gracias por participar en el mejoramiento de SIGAS");
				}else{
					alert(datos);
				}
		});
		$(this).dialog('close');
		},
		Cancelar: function() {
			$(this).dialog('close');
			$("#dialog-form2").dialog("destroy");
		}
	}
	});
	
});
</script>
</head>
<body>
<br /><br />
<center>
<form enctype="multipart/form-data" action="subirCertificados.php" method="post" onsubmit="mensaje();" >
<table width="90%" border="0" cellspacing="0" cellpadding="0">
	<tr> 
		<td width="13" height="29" background="../../../imagenes/tabla/arriba_izq.gif"></td>
		<td background="../../../imagenes/tabla/arriba_central2.gif"><span class="letrablanca">::..Plano Certificados..::</span></td>
		<td width="13" background="../../../imagenes/tabla/arriba_der.gif" align="right"></td>
	</tr>
	<tr> 
		<td background="../../../imagenes/tabla/izquierda2.jpg">&nbsp;</td>
		<td background="../../../imagenes/centro.gif">
			<img height="1" width="1" src="../../../imagenes/spacer.gif"> 
			<img height="1" width="1" src="../../../imagenes/spacer.gif"/> 
			<img src="../../../imagenes/menu/informacion.png" width="16" style="border:none; cursor:pointer" title="Manual" onClick="mostrarAyuda();" /> 
			<img src="../../../imagenes/menu/notas.png" width="16" height="16" style="cursor: pointer" title="Colaborac&oacute;on en l&oacute;nea" onClick="notas();" /> 
		</td>
		<td background="../../../imagenes/tabla/derecha.gif">&nbsp;</td>
	</tr>
	<tr> 
		<td background="../../../imagenes/tabla/izquierda2.jpg"></td>
		<td> 
			<table>
				<tr> 
					<td>&nbsp;</td>
					<td colspan="2">&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				
				<tr> 
					<td>&nbsp;</td>
					<td colspan="2" align="center" class="big">ESTE PROCESO COPIA AL SERVIDOR EL ARCHIVO PLANO CON LA INFORMACI&Oacute;N DE LOS CERTIFICADOS DE ESTUDIO</td>
					<td>&nbsp;</td>
				</tr>
				<tr> 
					<td>&nbsp;</td>
					<td colspan="2" align="center" class="normal"><ul>
					  <li>
					    <div align="left">Requerimientos:</div>
					  </li>
					  <li>
					    <div align="left">Extensi&oacute;n del Archivo: .txt (xxxxx.txt)</div>
					  </li>
					  <li>
					    <div align="left">Separador de Campos (,) </div>
					  </li>
					  <li>
					    <div align="left">Estructura:</div>
					  </li>
					  </ul>
			    <p align="left">&nbsp;</p></td>
					<td>&nbsp;</td>
				</tr>
				<tr>
				  <td>&nbsp;</td>
				  <td colspan="2">&nbsp;</td>
                  <td>&nbsp;</td>
			  </tr>
				<tr>
				  <td>&nbsp;</td>
				  <td width="50%" style="text-align:right">Archivos a procesar</td>
				  <td><input name="archivo[]" class="multi" accept="txt|TXT|csv" type="file" size="20" /></td>
                  <td>&nbsp;</td>
			  </tr>
				<tr>
				  <td>&nbsp;</td>
				  <td colspan="2" style="text-align:center"><div id="boton"><input type="submit" value="Subir Archivos" /></div></td>
				  <td>&nbsp;</td>
			  </tr>
              
			</table>
</td>
		<td background="../../../imagenes/tabla/derecha.gif">&nbsp;</td>
	</tr>
	<tr>
		<td height="38" background="../../../imagenes/abajo_izq2.gif">&nbsp;</td>
		<td background="../../../imagenes/abajo_central.gif"><label></label></td>
		<td background="../../../imagenes/abajo_der.gif">&nbsp;</td>
	</tr>
</table>
</form>
</center>

<!--colaboracion en linea-->
<!-- colaboracion en linea -->
<div id="div-observaciones-tab"></div>
<div id="dialog-form2" title="Colaboraci&oacute;n en l&iacute;nea">
<p>Por favor diligencie este formulario para enviar comentarios, errores o falencias encontradas en el proceso. M&aacute;ximo 250 caracteres </p>
<label>Tus comentarios:</label><br />
<textarea name="notas" id="notas" cols="60", rows="10"></textarea>
</div>

<!--cierre colaboracion en linea-->

<!-- Manual Ayuda -->
	<div id="ayuda" title="Manual Ayuda Plano Certificado" style="background-image:url(../../../imagenes/FondoGeneral0.png)"> </div>
	  
</body>
<script language="javascript">
function notas(){
	$("#dialog-form2").dialog('open');
	}	
	
function mostrarAyuda(){
	$("#ayuda").dialog('open' );
	//$("#div-conyuge2").dialog('open');
	}
</script>
</html>