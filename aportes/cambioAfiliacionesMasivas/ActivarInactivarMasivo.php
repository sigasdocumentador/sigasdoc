<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'p.afiliacion.class.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
include_once $raiz.DIRECTORY_SEPARATOR.'aportes'.DIRECTORY_SEPARATOR.'empresas'.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'empresas.class.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
$objAfiliacion = new Afiliacion();
$objAfiEmpresa = new Empresa();
$idsPersonas = $_REQUEST["idspersonas"];
$arrIdsPersonas = explode(",",$idsPersonas);
$idEmpresa = $_REQUEST["idempresa"];
$fechaRetiro = $_REQUEST["fechacambio"];
$estadoCambio = $_REQUEST["estadoCambio"];
$idsformularios = $_REQUEST["idsformularios"];
$arrIdsForm = explode(",",$idsformularios);
$idCausalInactivacion = $_REQUEST["idcausalinactivacion"];
$fechaFidelidad = $_REQUEST["fechafidelidad"];
$estadofidelidad = $_REQUEST["estadofidelidad"];
$nitEmpresa = $_REQUEST["nitempresa"];
$efectuados = 0;
$noEfectuados = 0;
$tipoAfiliacion = 0;
// tipos de afiliación para independientes
$arrIdsTiposAfiliacionIndependiente = array(19,20,21,23,3320,2938);
$arrResultado = array("noefectuados" => array(), "numefectuados" => 0, "numnoefectuados" => 0, "operacion" => '');
if($estadoCambio == 'INACTIVAR'){
	foreach($arrIdsPersonas as $idPersona){
		
			$sql = "select
				aportes016.idpersona,
				aportes016.idformulario,
				aportes016.tipoformulario,
				tipoafiliacion,
				horasdia,
				horasmes,
				agricola,
				cargo,
				categoria,
				aportes016.fechaingreso,
				aportes016.estado,
				aportes016.salario,
				aportes016.traslado,
				aportes016.codigocaja,
				aportes016.tipopago,
				aportes016.fechanovedad,
				aportes016.semanas,
				aportes016.fechafidelidad,
                aportes016.estadofidelidad,
                aportes016.flag,
                aportes016.tempo1,
                aportes016.tempo2,
                aportes016.auditado,
                aportes016.idagencia,
                aportes016.idradicacion,
				aportes015.idtipodocumento,
				aportes015.identificacion,
				aportes015.papellido,
				aportes015.sapellido,
				aportes015.pnombre,
				aportes015.snombre,
				aportes091.codigo
				from
				aportes016
				inner join aportes015 on aportes016.idpersona = aportes015.idpersona
				INNER JOIN aportes091 ON aportes015.idtipodocumento=aportes091.iddetalledef
				where
				aportes016.idempresa=$idEmpresa and
				aportes016.estado='A' and
				aportes016.idpersona=$idPersona";
			$result = $db->querySimple($sql);
			$afiliacionInicial = $result->fetch();
		
			// ejecutar procedimiento almacenado para inactivar la afiliación
			//$sql = "execute proc_inactivar_afiliacion 'I', {$afiliacionInicial["idformulario"]}, {$afiliacionInicial["idpersona"]}, $idCausalInactivacion, '$fechaRetiro', '{$_SESSION["USUARIO"]}', 0";
			$sql = "INSERT INTO aportes017( 
                        tipoformulario, 
                        tipoafiliacion, 
                        idempresa, 
                        idpersona, 
                        fechaingreso, 
                        horasdia, 
                        horasmes, 
                        salario, 
                        agricola, 
                        cargo, 
                        primaria, 
                        estado, 
                        fecharetiro, 
                        motivoretiro, 
                        fechanovedad, 
                        semanas, 
                        fechafidelidad, 
                        estadofidelidad, 
                        traslado, 
                        codigocaja, 
                        flag, 
                        tempo1, 
                        tempo2, 
                        fechasistema, 
                        usuario, 
                        tipopago, 
                        categoria, 
                        auditado, 
                        idagencia, 
                        idradicacion) 
	VALUES('{$afiliacionInicial["tipoformulario"]}',
			 '{$afiliacionInicial["tipoafiliacion"]}', 
			 '$idEmpresa', 
			 '{$afiliacionInicial["idpersona"]}', 
			 '{$afiliacionInicial["fechaingreso"]}', 
			 '{$afiliacionInicial["horasdia"]}', 
			 '{$afiliacionInicial["horasmes"]}', 
			 '{$afiliacionInicial["salario"]}', 
			 '{$afiliacionInicial["agricola"]}', 
			 '{$afiliacionInicial["cargo"]}', 
			 'N', 
			 'I', 
			 '$fechaRetiro', 
			 '$idCausalInactivacion', 
			 '{$afiliacionInicial["fechanovedad"]}', 
			 '{$afiliacionInicial["semanas"]}', 
			 '$fechaFidelidad', 
			 '$estadofidelidad', 
			 '{$afiliacionInicial["traslado"]}', 
			 '{$afiliacionInicial["codigocaja"]}', 
			 '{$afiliacionInicial["flag"]}', 
			 '{$afiliacionInicial["tempo1"]}',
			 '{$afiliacionInicial["tempo2"]}',
			 cast(getdate() as date), 
			 '{$_SESSION["USUARIO"]}', 
			 '{$afiliacionInicial["tipopago"]}', 
			 '{$afiliacionInicial["categoria"]}', 
			 '{$afiliacionInicial["auditado"]}', 
			 '{$afiliacionInicial["idagencia"]}', 
			 '{$afiliacionInicial["idradicacion"]}')";
			$resultInactivar = $db->queryActualiza($sql);
			if($resultInactivar == 1){
				// el insert se ejecutó sin problemas
				
				
				//GUARDAR OBSERVACION POR TRABAJADOR
				$idr=$idPersona;  //id persona o id empresa
				$obs="SE INACTIVA LA AFILIACION DE FORMA MASIVA CON EL NIT ".$nitEmpresa;	//observaciones
				$flag=1;	//1 trabajador  2 empresa
				$tabla="aportes088";
				$not=$db->b_obserbacion($idr, $flag);
				$ob=$fecha." - ".$_SESSION['USUARIO']." - ".$obs."<br>".$not;
				$sql4="Select count(*) as cuenta from aportes088 where idregistro=$idr and identidad=$flag";
				$rs4=$db->querySimple($sql4);
				$w4=$rs4->fetch();
				if($w4['cuenta']>0){
					$sql5="delete from aportes088 where idregistro=$idr and identidad=$flag";
					$rs15=$db->queryActualiza($sql5);
				}
				$rs15=$db->i_obserbacion($idr, $ob, $flag, $tabla);
				
				
				$efectuados++;
				$arrResultado["numefectuados"] = $efectuados;
				$arrResultado["operacion"] = 'INACTIVO';
				
				$sql = "DELETE FROM aportes016 WHERE idformulario = {$afiliacionInicial["idformulario"]}";
				$resultBorrar = $db->queryActualiza($sql);
				// Si es trabajador independiente se inactiva la empresa
				if(in_array($afiliacionInicial["tipoafiliacion"],$arrIdsTiposAfiliacionIndependiente)){
					$empresaInactivada = $objAfiEmpresa->inactivar_empresa_por_NIT($nitEmpresa);
					/*if($empresaInactivada){
						alert("Se inactivo Empresa");
					}*/
				}
				
			}else{
				if($resultInactivar == 0){
					// hubo problemas insertando
					$noEfectuados++;
					$arrResultado["noefectuados"][] = $idPersona;
					$arrResultado["numnoefectuados"] = $noEfectuados;
					$arrResultado["operacion"] = 'INACTIVO';
				}
			}
		
		
	}
}else{
	if($estadoCambio == 'ACTIVAR'){
		foreach($arrIdsForm as $idformulario){
			$sql = "select tipoformulario,tipoafiliacion,idempresa,idpersona,fechaingreso,horasdia,horasmes,salario,agricola,cargo,primaria,estado,fecharetiro,motivoretiro,fechanovedad,semanas,fechafidelidad,estadofidelidad,traslado,codigocaja,flag,tempo1,tempo2,fechasistema,usuario,tipopago,categoria,auditado,idagencia,idradicacion,null as vendedor,null as codigosalario from aportes017 where idformulario = '$idformulario'";
			$rs=$db->querySimple($sql);
			$w=$rs->fetch();
			$idPersonaBuscar = $w['idpersona'];
			$sql1 = "select count(*) as conteo from aportes016 where idpersona = '$idPersonaBuscar' and primaria = 'S'";
			$rs1=$db->querySimple($sql1);
			$w1=$rs1->fetch();
			if($w1['conteo']==0){
				$resp = $objAfiliacion->insert_reactivar_afiliacion($campos=array($w['tipoformulario'],$w['tipoafiliacion'],$w['idempresa'],$w['idpersona'],$fechaRetiro,$w['horasdia'],$w['horasmes'],$w['salario'],$w['agricola'],$w['cargo'],'S','A',NULL,'',NULL,'',NULL,'',$w['traslado'],$w['codigocaja'],$w['flag'],$w['tempo1'],$w['tempo2'],$w['fechasistema'],$_SESSION["USUARIO"],$w['tipopago'],$w['categoria'],$w['auditado'],$w['idagencia'],$w['idradicacion'],'',''));
			}else{
				$resp = $objAfiliacion->insert_reactivar_afiliacion($campos=array($w['tipoformulario'],$w['tipoafiliacion'],$w['idempresa'],$w['idpersona'],$fechaRetiro,$w['horasdia'],$w['horasmes'],$w['salario'],$w['agricola'],$w['cargo'],'N','A',NULL,'',NULL,'',NULL,'',$w['traslado'],$w['codigocaja'],$w['flag'],$w['tempo1'],$w['tempo2'],$w['fechasistema'],$_SESSION["USUARIO"],$w['tipopago'],$w['categoria'],$w['auditado'],$w['idagencia'],$w['idradicacion'],'',''));
			}	
			if($resp){
				
				//GUARDAR OBSERVACION POR TRABAJADOR
				$idr=$idPersonaBuscar;  //id persona o id empresa
				$obs="SE ACTIVA LA AFILIACION DE FORMA MASIVA CON EL NIT ".$nitEmpresa." ESTE PROCESO UTILIZA EL MISMO RADICADO DE LA AFILIAICON INACTIVA";	//observaciones
				$flag=1;	//1 trabajador  2 empresa
				$tabla="aportes088";
				$not=$db->b_obserbacion($idr, $flag);
				$ob=$fecha." - ".$_SESSION['USUARIO']." - ".$obs."<br>".$not;
				$sql4="Select count(*) as cuenta from aportes088 where idregistro=$idr and identidad=$flag";
				$rs4=$db->querySimple($sql4);
				$w4=$rs4->fetch();
				if($w4['cuenta']>0){
					$sql5="delete from aportes088 where idregistro=$idr and identidad=$flag";
					$rs15=$db->queryActualiza($sql5);
				}
				$rs15=$db->i_obserbacion($idr, $ob, $flag, $tabla);
				
				
				//$resp2 = $objAfiliacion->borrar_afiliacion_inactiva($idformulario);
				if(in_array($w['tipoafiliacion'],$arrIdsTiposAfiliacionIndependiente)){
					$empresaActivada = $objAfiEmpresa->activar_empresa_por_NIT($nitEmpresa);
				}
				$efectuados++;
				$arrResultado["efectuados"][] = $idformulario;
				$arrResultado["numefectuados"] = $efectuados;
				$arrResultado["operacion"] = 'ACTIVO';
			}else{
				$noEfectuados++;
				$arrResultado["noefectuados"][] = $idformulario;
				$arrResultado["numnoefectuados"] = $noEfectuados;
				$arrResultado["operacion"] = 'ACTIVO';
			}
		
		}
	}
}	

print_r(json_encode($arrResultado));
die();
?>