<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$url=$_SERVER['PHP_SELF'];
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'auditoria.php';
//include_once 'phpComunes/auditoria.php';
auditar($url);
include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';
//include_once 'config.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'p.definiciones.class.php';
$objClase=new Definiciones();

/* autor:       Luis Fernando Rojas Andrade*/
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Comfamiliar - S.I.G.A.S.</title>
<link href="<?php echo URL_PORTAL; ?>css/Estilos.css" rel="stylesheet" type="text/css" />
<link type="text/css" href="<?php echo URL_PORTAL; ?>css/estilo_tablas.css" rel="stylesheet"/>
<link type="text/css" href="<?php echo URL_PORTAL; ?>css/formularios/base/ui.all.css" rel="stylesheet" />
<link href="<?php echo URL_PORTAL; ?>css/marco.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/jquery-1.4.2.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/formularios/ui/ui.core.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/formularios/ui/ui.tabs.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/formularios/ui/ui.draggable.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/formularios/ui/ui.resizable.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/formularios/ui/ui.dialog.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/formularios/ui/ui.progressbar.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/formularios/ui/ui.datepicker.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/effects.Jquery.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/comunes.js"></script>

<script type="text/javascript">
shortcut.add("Shift+F",function() {
		                                var URL=src();
		var url=URL+"aportes/trabajadores/consultaTrabajador.php";
    	window.open(url,"_blank");
    },{
	'propagate' : true,
	'target' : document 
        });        
</script>


<script type="text/javascript" src="js/cambioAfiliacionesMasivas.js"></script>
<style type="text/css">
div.item{
	width:45%;
	float:left;
	margin-left:10px;
	padding: 10px;
}.tablaR td,.tablaR th{text-align:left}
</style>
</head>

<body>
 <!-- TABLA OCULTA -->
     <table border="0" cellpadding="0" cellspacing="0" class="tablaR" id="tableOrigenHidden" style="position:fixed; z-index:1; left:8.2%; display:none">
          <tr>
            <th width="7%"><strong>Seleccionar</strong></th>
            <th width="12%"><strong>Identificaci&oacute;n</strong></th>
            <th width="8%"><strong>Nombres</strong></th>
            <td width="2%"></td>
            <th width="8%"><strong>Deshacer</strong></th>
            <th width="14%"><strong>Identificacion</strong></th>
            <th width="6%"><strong>Nombres</strong></th>
          </tr>
      </table>
      
<!-- fin tablas ocultas -->   
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="13" height="29" class="arriba_iz">&nbsp;</td>
    <td class="arriba_ce"><span class="letrablanca">::&nbsp;Activar/Inactivar Trabajadores&nbsp;::</span></td>
    <td width="13" class="arriba_de" align="right">&nbsp;</td>
  </tr>
  
 <tr>
	<td class="cuerpo_iz">&nbsp;</td>
	<td class="cuerpo_ce">
	<img src="<?php echo URL_PORTAL; ?>imagenes/spacer.gif" width="1" height="1"/>
	<img src="<?php echo URL_PORTAL; ?>imagenes/menu/nuevo.png" title="Nuevo" width="16" height="16" style="cursor:pointer" onClick="nuevaCambioMasivo();" />
	<img src="<?php echo URL_PORTAL; ?>imagenes/spacer.gif" width="1" height="1"/>
	<img src="<?php echo URL_PORTAL; ?>imagenes/menu/grabar.png" width="16"  height=16 style="cursor:pointer" title="Guardar" onClick="guardar();" /> 
	<img src="<?php echo URL_PORTAL; ?>imagenes/spacer.gif" width="1" height="1"/><img src="<?php echo URL_PORTAL; ?>imagenes/menu/informacion.png" width="16" height="16" style="border:none; cursor:pointer" title="Manual" onClick="mostrarAyuda();" />
	<img src="<?php echo URL_PORTAL; ?>imagenes/menu/notas.png" width="16" height="16" style="cursor: pointer" title="Colaborac&oacute;on en l&oacute;nea" onClick="notas();" />
	</td>
   <td class="cuerpo_ce">&nbsp;</td>
 </tr>
  <tr>
    <td class="cuerpo_iz">&nbsp;</td>
    <td class="cuerpo_ce">
    <div id="resultado" class="Rojo" style="margin:10px; text-align:center"></div></td>
    <td class="cuerpo_de">&nbsp;</td>
  </tr>
 	 <tr>
   	<td class="cuerpo_iz">&nbsp;</td>
    	<td class="cuerpo_ce">
		<table width="95%" border="0" cellspacing="0" class="tablero">
	 </tr>	      
      <tr>
	        <td>Nit</td>
	        <td><input name="nitOrigen" class="box1" id="nitOrigen" value="" onblur="validarLongNumIdent(5,this);estaEnNuevo()" onkeyup='solonumeros(this);'/>
			  <!--<td>Nit </td>
			  	  <td><input name="nitDestino" id="nitDestino" class="box1" onblur="validarLongNumIdent(5,this)" onkeyup='solonumeros(this);'/>
			  <input name="buscarDestino" type="button" class="ui-state-default " id="buscarDestino" value="Buscar"  onclick="buscarEmpresa(2)" />
			  </td>-->
      </tr>
      <tr>
      		<td>Estado</td>
		       		<td><strong><select id="estado" class="box1" name="estado" onchange="cambioEstado();">
		       						<option selected="selected" value="0">Seleccione..</option>
									<option value="A">ACTIVO</option>
									<option value="I">INACTIVO</option>
								</select>
								<input name="buscarEmpresa" type="button" class="ui-state-default" id="buscarEmpresa" value="Buscar" onclick="buscarEmpresa(1)"/>
		       		</td>
		       		
		    </td>
		<tr id="codigoEstado">
			<td>C&oacute;digo Estado</td>
		    <td><select name="codigo" class="box1" id="codigo" >
		          <option value="0" selected="selected">Seleccione..</option>
		          <?php
				        $consulta=$objClase->mostrar_codigo(47, 'A');
				        while($row=mssql_fetch_array($consulta)){
				            echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
				        }
		           ?> 
				</select>
				<img src="../../imagenes/menu/obligado.png" alt="" width="12" height="12" />
			</td>
		</tr>    
      </tr>
		      <tr>
		        <td width="10%">Fecha Cambio Estado</td>
		        <td width="40%"><input name="fecRetiro" id="fecRetiro" class="boxfecha"/>
		        <img width="12" height="12" alt="" src="../../imagenes/menu/obligado.png">
		        </td>
		        <!--<td width="10%">Fecha Retiro</td>
		        <td width="40%"><input name="fecIngreso" class=boxfecha id="fecIngreso" readonly /></td>-->
		      </tr>
		      <tr>
		        <td>Empresa</td>
		        <td><strong>
		          <div  id="empresaOrigen" align="center"></div></strong></td>
		       <!--  <td></td><td><strong>
		          <div  id="empresaDestino" align="center"></div></strong></td>-->
		       </tr>
             
      </table>
      <div id="progreso"></div>
      <div  id="datost" style="width:94%; border:1px dashed #CCC; margin:10px; padding:0px; overflow:hidden; position:relative" class="ui-corner-all" >
        <img src="<?php echo URL_PORTAL; ?>imagenes/pass.png" width="32" height="32" title="Migrar Trabajadores" style="position:absolute; left: 44%; first: 8px; cursor:pointer; display:none" id="pasar"  onclick="migrarTrabajadores()"/> 
        <div class="item"> 
          <center><h5>TRABAJADORES <span id="EstadoCambio"></span> (<span id="trabajadoresOrigen" class="Rojo"></span>)</h5></center>
          <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tablaR hover">
          	<tr>
            	<th width="5%">
                <input type="checkbox"  id="seleccionCantidad"/></th>
                <th width="15%"><strong>Seleccionar:</strong></th>
                <th width="15%"><strong><select name="cantidad" class="box1" id="cantidad" >
		          							<option value="0" selected="selected">Seleccione..</option>
		          							<option value="20">20</option>
		          							<option value="50">50</option>
		          							<option value="100">100</option>
		          							<option value="200">200</option>
		          							<option value="400">400</option>
		          						</select></strong></th>
            </tr>
           </table> 
          <!-- FIN TABLA OCULTA PARA SCROLL -->
          <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tablaR hover" id="tOrigen">
            <tr> 
              <th width="5%"> 
                <!--<img src="../../imagenes/chk0.png" alt="Check" /> -->
                <input type="checkbox"  id="todosOrigen"/></th>
              <th width="15%"><strong>IdFormu.</strong></th>
              <th width="15%"><strong>Identif.</strong></th>
              <th width="40%"><strong>Nombre</strong></th>
              <th width="15%"><strong>Fecha Ingreso</strong></th>
              <th width="10%"><strong>Id</strong></th>
            </tr>
          </table>
        </div>
        <div class="item"> 
          <center><h5>TRABAJADORES A <span id="EstadoCambio2"></span> (<span id="trabajadoresDestino" class="Rojo"></span>)</h5></center>
          <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tablaR hover" id="tDestino">
            <tr> 
              <th width="5%"><strong></strong><!--<img style="margin-left:40%; cursor:pointer" src="<?php echo URL_PORTAL; ?>/imagenes/undo.png"  title='Devolver Todos' onClick="devolverTodos()" />--></th>
              <th width="15%"><strong>IdFormu.</strong></th>
              <th width="15%"><strong>Identif.</strong></th>
              <th width="40%"><strong>Nombre</strong></th>
              <th width="15%"><strong>Fecha Ingreso</strong></th>
              <th width="10%"><strong>Id</strong></th>
            </tr>
          </table>
        </div>
      </div>
      
    </td>
     <td class="cuerpo_de">&nbsp;</td>
  </tr>
  <tr>
    <td class="abajo_iz" >&nbsp;</td>
    <td class="abajo_ce" ></td>
    <td class="abajo_de" >&nbsp;</td>
  </tr>
</table>

<!-- DIV PARA MOSTRAR LOS ERRORES -->
        <div id="tablaerror" title="::ERROR AL ACTIVAR EL AFILIADO::" style="display: none;">
			<table width="100%" border="0" cellpadding="5" cellspacing="0" class="tablero" id="tblresultado">
		 	   <tr><td></td></tr>
     		</table>
         </div>


<!-- colaboracion en linea -->
<!-- FORMULARIO OBSERVACIONES-->
<div name="div-observaciones-tab" style="display:none" title="Observaciones de modificaci&oacute;n">
<table class="tablero">
 <tr>
   <td>Usuario</td>
   <td colspan="3" >
   <input name="usuarioObs" id="usuarioObs" class="box1" value="<?php echo $_SESSION['USUARIO']?>" /></td>
   </tr>
		<tr>
		<td>Observaciones</td>
		<td colspan="3" >
		<textarea name="observacionGrupo" id="observacionGrupo" cols="45" rows="5" class="boxlargo"></textarea></td>
		</tr>
</table>
<div class="ui-state-highlight ui-corner-all" style="margin: 9px auto; display:none;padding: 6px; text-align: center; border: 1px solid rgb(255, 255, 0); width:300px" id="rtaObservacion"><span class="ui-icon ui-icon-info" style="float: left; margin-right: 0.3em;"></span>Observaci&oacute;n guardada con &eacute;xito.</div>
</div>

<!-- Manual Ayuda -->
<div id="ayuda" title="Manual Migraci&oacute;n" style="background-image:url('<?php echo URL_PORTAL; ?>imagenes/FondoGeneral0.png')">
</div>

</body>
</html>
