// @autor: Luis Fernando Rojas Andrade 
var URL=src();
var entro = false;
//Dialog Ayuda
function mostrarAyuda(){
	$("#ayuda").dialog('open' );
	}
function cambioEstado(){
	$("#codigo").val(0);
	if(nuevo == true){
		if($("#estado").val() == 'A'){
			$("#codigoEstado").show();
		}else{
			$("#codigoEstado").hide();
		}
	}else{
		alert("Debe seleccionar primero el boton NUEVO.");
		$("#nitOrigen").val("");
		$("#codigo").val(0);
		return false;
	}
	
}
function estaEnNuevo(){
	if(nuevo == false){
		alert("Debe seleccionar primero el boton NUEVO.");
		$("#nitOrigen").val("");
		$("#codigo").val(0);
		return false;
	}
}
function notas(){
	$("#dialog-form2").dialog('open');
	}	

function nuevaCambioMasivo(){
	nuevo = true;
	entro = false;
	$("#cantidad").val(0);
	$("#progreso").fadeIn(1000).html("<br/>");
	limpiarCampos();
}

$(document).ready(function(){
	nuevo = false;
	idEmpr = null;
	
	$("#IdFormularioOculto").hide();
	$("#IdFormularioOculto2").hide();
	$("#codigoEstado").hide();
	
	//control de tablas en listado de trabajadores
	h=$("body").innerHeight();
	var alto=h/2;
	
	
	$(window).scroll(function(){
	
		var pos=$(this).scrollfirst();
		
		if(pos>alto){
			$("#tableOrigenHidden").fadeIn();
			if($("#pasar").is(":visible")){
				$("#pasar").animate({first:pos+"px"},300)
			}
			
		}else{
			$("#tableOrigenHidden").fadeOut();
			$("#pasar").css("first","8px");
		}
	
	});
		
	//Dialog ayuda
	$("#ayuda").dialog({
	 	autoOpen: false,
		height: 450,
		width: 700,
		draggable:true,
		modal:false,
		open: function(evt, ui){
				$('#ayuda').html('');
				$.get('../../help/aportes/ayudaMasivos.html',function(data){
						$('#ayuda').html(data);
				});
		 }
	});
	
	//Dialog Colaboracion en linea
	$("#dialog-form2").dialog({
		autoOpen: false,
		height: 400,
		width: 500,
		modal: true,
		buttons: {
			'Enviar': function() {
			var bValid = true;
			var campo=$('#notas').val();
			var campo0=$.trim(campo);
			if (campo0==""){
				$(this).dialog('close');
					return false;
				}
			var campo1=$('#usuario').val();
			var campo2="radicacion.php";
			$.post(URL+'phpComunes/colaboracion.php',{v0:campo0,v1:campo1,v2:campo2},
			function(datos){
				if(datos=='1'){
					alert("Su comentario fue enviado correctamente!. Gracias por participar en el mejoramiento de SIGAS");
				}else{
					alert(datos);
				}
			});
			$(this).dialog('close');
			},
			Cancelar: function() {
				$(this).dialog('close');
				$("#dialog-form2").dialog("destroy");
			}
		}
	});
		
	//DATEPICKER
	$("#fecIngreso,#fecRetiro").datepicker();
	$("#fecIngreso").datepicker("option", "minDate", "-6M");
	
	//seleccionar todos los check
	$("#todosOrigen").live("click",function(){
		if(!entro || $("#tDestino tr:not(':first')").length <400){
			$("#tOrigen input:checkbox").attr("checked",$(this).is(":checked"));
		}else{
			$("#tOrigen input:checkbox").attr("checked",false);
		}
	});
	
	//seleccionar x cantidad de checks
	$("#seleccionCantidad").live("click",function(){
		if(!entro || $("#tDestino tr:not(':first')").length <400){
			if($("#seleccionCantidad").is(":checked")){
				if($("#cantidad").val()!=0){
					var cantidad = $("#cantidad").val();
					for(var i=0;i<cantidad;i++){
						$("#chkO"+i).attr("checked",true);
					}
					$("#pasar").fadeIn();
				}else{
					alert("Debe Seleccionar Un # De Registros");
					$("#seleccionCantidad").attr("checked",false);
					$("#pasar").hide();
				}	
			}else{
				$("#tOrigen input:checkbox").attr("checked",false);
				$("#pasar").hide();
			}
		}
	});
	
	//Validar si esta seleccionado almenos un check apra mostrar el boton enviar	
	$("#tOrigen input:checkbox").live("click",function(){
		//Mostrar el boton de pasar si al menos un check es seleccionado	
		if(!entro || $("#tDestino tr:not(':first')").length <400){
			if($("#tOrigen input:checkbox:not('#todosOrigen')").is(":checked")){
				$("#pasar").fadeIn();
			}else{
				$("#pasar").hide();
			}
		}
	});	
	
	// muestra tabla dialog para mostrar error de empresas inactivas
	$("#tablaerror").dialog({
		autoOpen:false,
		width:640,
		draggable:true,
		modal:false,
		open:function(event,ui){
		},//fin funcion open
		close:function(event,ui){
			$("#tablaerror tr td").html("");
		}
	});//fin dialog
	
});//fin ready

//Buscar Empresa
var ide=0;
function buscarEmpresa(op){
	//op=1 Trabajdor ORIGEN
	//op=2 trabajador DESTINO
	//Variables a utilizar de empresas
	$("#cantidad").val(0);
	entro = false;
	if(nuevo == true){
		$("#tOrigen tr:not(':first')").remove();
		$("#tDestino tr:not(':first')").remove();
		$("#trabajadoresOrigen,#trabajadoresDestino").empty();
		$("#codigo").val(0);
		$("#fecRetiro").val("");
		if( $("#estado").val()=='A'){
			$("#EstadoCambio").html("ACTIVOS");
			$("#EstadoCambio2").html("INACTIVAR");
		}else{
			if($("#estado").val()=='I'){
				$("#EstadoCambio").html("INACTIVOS");
				$("#EstadoCambio2").html("ACTIVAR");
			}else{
				alert("Debe Seleccionar Estado");
				return false;
			}
		}
		
		
		$("#nitOrigen,#nitDestino").removeClass("ui-state-error");
		$("#resultado").html('');
		//ocultar boton pasar
		$("#pasar").hide();
		nit = $("#nitOrigen").val();
		$("#progreso").html("<br/><div align='center'><img src='../../imagenes/ajaxload.gif'/></div>");	
		//------------------------Traer de la BD dato de la empresa
	    $.getJSON(URL+'phpComunes/buscarEmpresaIniRea.php',{v0:nit,v1:$("#estado").val()},function(data){
	       if(data.error==2){
	    	 $("#progreso").html("<br/>");
	         $("#resultado").html("la empresa NO existe.").hide().fadeIn();
			 return false;
	       }if (data.error==1){
	    	   $("#progreso").html("<br/>");
	    	   alert('No es posible activar empleados de esta empresa, debido a que la empresa se encuentra Inactiva');
	    	   var	str = String(data.descripcion);
			   var res = str.replace(/'/g, "|");  
			   $('#tablaerror tr td').append(res);
			   $('#tablaerror').dialog('open');
			   // se listan los trabjadores sin opcion del ckecked
			   $.each(data.datos, function(i,fila){
	    		   rs=fila.razonsocial;
	    		   ide=fila.idempresa;
	    		   if(op == 1 ){
	    			   $("#empresaOrigen").html(rs);
	    			   idEmpr = ide;    			   
	    			   listarTrabajadores(ide,op,'2');    			    			   
	    		   }
	    	   });
	    	   return false;
	       }else{
	    	   $.each(data.datos, function(i,fila){
	    		   rs=fila.razonsocial;
	    		   ide=fila.idempresa;
	    		   if(op == 1 ){
	    			   $("#empresaOrigen").html(rs);
	    			   idEmpr = ide;    			   
	    			   listarTrabajadores(ide,op,'1');    			    			   
	    		   }
	    	   });
	       }
	    });//---------------------Fin buscar empresa json--------------------
	}else{
		alert("Debe seleccionar primero el boton NUEVO.");
		$("#nitOrigen").val("");
		$("#codigo").val(0);
		return false;
	}   
  }//END FN BUSCAR t

function listarTrabajadores(ide,op,tipocargue){
	if($("#estado").val()=='A'){
		//Buscar Trabajadores en BD.
		$.getJSON(URL+'phpComunes/buscarAfiliaciones.php',{v0:ide},function(datos){
			if(datos==0){
				$("#resultado").html("La empresa NO tiene empleados afiliados").hide().fadeIn();
				$("#progreso").fadeIn(1000).html("<br>");
				return false;
			}else{
				if(op==1){		  				 
					$("#tOrigen tr:not(':first'),#tDestino tr:not(':first')").remove();
					cargarTrabajadores(datos,tipocargue);
				}
				contarTrabajadores();
			}//end else
			 $("#progreso").fadeIn(1000).html(datos);
		});//json trbaajdores
	}else{
		//Buscar Trabajadores en BD.
		$.getJSON(URL+'phpComunes/buscarAfiliacionesInactivas.php',{v0:ide},function(datos){
			if(datos==0){
				$("#resultado").html("La empresa NO tiene empleados inactivos").hide().fadeIn();
				$("#progreso").fadeIn(1000).html("<br>");
				return false;
			}else{
				if(op==1){		  				 
					$("#tOrigen tr:not(':first'),#tDestino tr:not(':first')").remove();
					cargarTrabajadores(datos,tipocargue);
				}
				contarTrabajadores();
			}//end else
			 $("#progreso").fadeIn(1000).html(datos);
		});//json trbaajdores
	}

}


function cargarTrabajadores(datos,tipocargue){
	var cuentaTrabajadores=datos.length;
   	if(cuentaTrabajadores>0){	
		for(i=0;i<cuentaTrabajadores;i=i+1){
			var tr=$("<tr id='filaO_"+datos[i].idformulario+"'></tr>");
			if(tipocargue==1){
				var td0=$("<td><input type='checkbox' id='chkO"+i+"' ></td>");
			}else{
				var td0=$("<td></td>");			
			}
			var td1= $("<td>"+datos[i].idformulario+"</td>");
			var td2=$("<td style='text-align:left'>"+datos[i].identificacion+"</td>");
			var td3=$("<td>"+datos[i].papellido+" "+datos[i].sapellido+" "+datos[i].pnombre+" "+datos[i].snombre+"</td>");
			var td4=$("<td>"+datos[i].fechaingreso+"</td>");
			var td5=$("<td>"+datos[i].idpersona+"</td>");
			var fila=tr.append(td0.add(td1).add(td2).add(td3).add(td4).add(td5));	
			$("#tOrigen").append(fila);		
		}
	}
}

//Pasar los trabajadores de origen a destino, esta funcion está en el boton [->] el cual esta oculto(display:hidden)
function migrarTrabajadores(){
	var cont = 0;
	if(!entro || $("#tDestino tr:not(':first')").length <400){
		$("#progreso").html("<br/><div align='center'><img src='../../imagenes/ajaxload.gif'/></div>");
		//if(confirm("Esta seguro de migrar los trabajadores seleccionados?")){
		$("#tOrigen td input:checked").each(function(i){
		    cont++;
			if(cont<=400 && $("#tDestino tr:not(':first')").length <400){
				var row=$(this).parents("tr").attr("id");//extraigo el valor del id de la fila ej: fila1
				var aux=$("#"+row).html(); // extraigo el html de la fila ej: <td> ...</td>		       
				var id = row.replace("filaO_","");
			    $("#"+row).fadeOut(400,function(){
			    	$(this).remove();
			    	contarTrabajadores(); 
			    });
				$("#tDestino").append("<tr id='filaD_"+id+"'>"+aux+"</tr>").hide().fadeIn(900);
				$("#filaD_"+id+" td").find(":checkbox").replaceWith("<img style='margin-left:40%' src='"+URL+"/imagenes/undo.png' id='undo"+id+"' title='Devolver' onClick='devolver("+id+")'/>");
				contarTrabajadores();
			}else{
				alert("El Limite para procesar son 400, si son mas por favor Repita el Proceso con el Resto");
				$("#progreso").fadeIn(1000).html("<br/>");
				$("#pasar").hide();
				return false;
			}
		
		});
		//a la ultima fila le doy la clase hightlight, y a los hermanos se las quito si la tienen...
		$("#tDestino tr:last").addClass("ui-state-highlight").siblings().removeClass("ui-state-highlight");
		$("#progreso").fadeIn(1000).html("<br/>");
	}else{
		alert("La Lista Esta en el Limite para Procesar, \n Favor Realice Esas Primero y Haga Click En Buscar Para Realizar Mas");
	}
	entro = true;
}//fn pasarT

//function devolver
function devolver(id){
	var row=$("#filaD_"+id).html();
	$("#filaD_"+id).remove();
	 contarTrabajadores();  
	$("#tOrigen").append("<tr id='filaO_"+id+"'>"+row+"</tr>").hide().fadeIn(900);
	$("#filaO_"+id+" td").find("img").replaceWith("<input type='checkbox' id='chkO"+id+"'>");
	contarTrabajadores();  
}

function devolverTodos(){
	if($("#tDestino tr:not(':first')").lengt==0){
		return false;
	}
	$("#progreso").html("<br/><div align='center'><img src='../../imagenes/ajaxload.gif'/></div>");
	$("#tDestino tr:not(':first')").each(function(i,n){
		var row=n.id;
		$("#"+row).remove();
		contarTrabajadores();  
		$("#buscarEmpresa").trigger("click");
		$("#tOrigen input:checked").attr("checked",false);
	});
	$("#progreso").fadeIn(1000).html("<br/>");
}
function guardar(){
	
	if(nuevo == true){
		error=0;	
		$("table.tablero input:text").removeClass("ui-state-error");
		$("#tDestino").removeClass("ui-state-error");
		
		if($("#nitOrigen").val()==''){
			$("#nitOrigen").addClass("ui-state-error");
			error++;
		}
		
		if($("#fecRetiro").val()==''){
			$("#fecRetiro").addClass("ui-state-error");
			error++;
		}
		
		if( $("#estado").val()=='A'){
			estadoC = "INACTIVAR";
			if($("#codigo").val()==0){
				$("#codigo").addClass("ui-state-error");
				error++;
			}
		}else{
			if($("#estado").val()=='I'){
				estadoC = "ACTIVAR";
			}else{
				alert("Debe Seleccionar Estado");
				return false;
			}
		}
		if(error>0){
			alert("Faltan Datos por Ingresar");
			return false;
		} 
		var totalFilas=$("#tDestino tr").length;
		if(totalFilas==1){
			$("#resultado").html("NO ha seleccionado ningun trabajador.");
			$("#bGuardar").show();
			return false;
		}
		
		if(totalFilas>1){
			trabajadores=parseInt(totalFilas)-1;
			if(confirm("Esta seguro de "+estadoC+"  los trabajadores("+trabajadores+") seleccionados?")){
				$("#progreso").html("<br/><div align='center'><img src='../../imagenes/ajaxload.gif'/></div>");
				efectuados = 0;
				noEfectuados = 0;
				arregloIds = [];
				arregloIdForm = [];
				$("#tDestino tr:not(':first')").each(function(index){
					var idPersona=$(this).children("td:eq(5)").text();
					if(estadoC == "ACTIVAR"){
					var idFormulario=$(this).children("td:eq(1)").text();
					}else{
						idFormulario = 'NULL';
					}
					
					arregloIds[index] = idPersona;
					arregloIdForm[index] = idFormulario;
				});
				var objFecha = new Date();//creo un objeto tipo fecha
				var anioActual = objFecha.getFullYear();//Obtengo el a�o actual
				objFecha.setYear(anioActual+1);
				var mesActual = objFecha.getMonth()+1;
				mesActual = (mesActual <10)?"0"+mesActual.toString():mesActual.toString();
				var diaActual = objFecha.getDate();
				diaActual = (diaActual <10)?"0"+diaActual.toString():diaActual.toString();
				var fechaEnUnAnio = objFecha.getFullYear()+"-"+mesActual+"-"+diaActual;
				var Fechfidelidad = "";//Fechafidelidad
				var Estfidelidad = "";//Estadofidelidad
				//Afiliacion Anulada
				if($("#codigo").val()==2864){
					Fechfidelidad = $("#fecRetiro").val();
					Estfidelidad = 'I';
				}else{
					Fechfidelidad = fechaEnUnAnio;
					Estfidelidad = 'A';	
					}
					
				$.getJSON(URL+'aportes/cambioAfiliacionesMasivas/ActivarInactivarMasivo.php',{
					idspersonas:arregloIds.join(),
					idempresa: idEmpr, 
					fechacambio: $("#fecRetiro").val(),
					estadoCambio: estadoC,
					idsformularios: arregloIdForm.join(),
					idcausalinactivacion: $("#codigo").val(),
					fechafidelidad: Fechfidelidad,
					estadofidelidad: Estfidelidad,
					nitempresa: $("#nitOrigen").val(),
					},
					function(respuesta){
						if(respuesta.numefectuados == 0){
							alert("NO se "+respuesta.operacion+" ningun trabajador.");
						}else{
							if(respuesta.numefectuados > 0){
								alert("Se  "+respuesta.operacion+" "+ respuesta.numefectuados +" trabajadores. \nTrabajadores NO "+respuesta.operacion+" : "+ respuesta.numnoefectuados);
								limpiarCampos();
								observacionesTab(idEmpr,2);
								$("#trabajadoresDestino").empty();
							}
						}
							
						
						if(respuesta.numnoefectuados > 0){
							respuesta.noefectuados.forEach(function(idPersona, indNoMigrados, arreglo){
								$("#filaD_"+idPersona).css("border","red solid 1px");
								$("#filaD_"+idPersona).css("color","red");
							});
						}
						$("#progreso").fadeIn(1000).html(respuesta);
						nuevo = false;
					}
				);
			}else{
				$("#resultado").html("No hay trabajadores para "+estadoC+".").hide().fadeIn();
			}
		}//confirm			
	}else{
		alert("Debe seleccionar primero el boton NUEVO.");
		$("#nitOrigen").val("");
		$("#codigo").val(0);
		return false;
	}
	
	
}

//limpiar campos
function limpiarCampos(){
	$("#progreso").html("<br/><div align='center'><img src='../../imagenes/ajaxload.gif'/></div>");
	$("#todosOrigen").attr("disabled",false);	 
	$("#tOrigen tr:not(':first')").remove();	
	$("#tDestino tr:not(':first')").remove();
	$("table.tablero input:text").val('');
	$("input:checkbox").attr("checked",false);
	$("#empresaOrigen,#empresaDestino,#resultado").html("");
	$("table.tablero td input:text.ui-state-error").removeClass("ui-state-error");
	$("#trabajadoresOrigen,#trabajadoresDestino").empty();
	$("#EstadoCambio").empty();
	$("#EstadoCambio2").empty();
	$("#nitOrigen").focus();
	$("#estado").val("0");
	cambioEstado();
	$("#progreso").fadeIn(1000).html("<br>");
}//limpiar campos
function contarTrabajadores(){
	trabajadoresDestino= $("#tDestino tr:not(':first')").length;			
	trabajadoresOrigen = $("#tOrigen tr:not(':first')").length;
	$("#trabajadoresDestino").html(trabajadoresDestino);
	$("#trabajadoresOrigen").html(trabajadoresOrigen);
}