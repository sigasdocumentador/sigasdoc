<?php 
/* autor:       orlando puentes
* fecha:       24/09/2010
* objetivo:    
*/

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';

$_SESSION["REGISTRO"]=0;

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	echo "<script>alert('Hubo un error');</script>";
	exit();
}
$sql="SELECT distinct coddepartamento, departmento FROM aportes089 order by departmento";
$departamento=$db->querySimple($sql);
$seleccion = "";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<title>Barrios</title>
		<!-- <link href="../css/Estilos.css" rel="stylesheet" type="text/css" />
        <link href="../css/marco.css" rel="stylesheet" type="text/css" />
		<script language="javascript" src="../js/jquery-1.4.2.js"></script>
		<script language="javascript" src="../js/comunes.js"></script>-->
        
        <link type="text/css" href="<?php echo URL_PORTAL; ?>newcss/Estilos.css" rel="stylesheet">
		<link type="text/css" href="<?php echo URL_PORTAL; ?>newcss/css.1.8/jquery-ui-1.8.17.custom.css" rel="stylesheet">
		<link type="text/css" href="<?php echo URL_PORTAL; ?>newcss/marco.css" rel="stylesheet">
		
		<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/1.6/jquery-1.6.2.min.js"></script>
		<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/1.6/jquery-ui-1.8.16.custom.min.js"></script>
		<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/comunes.js"></script>
		<script language="javascript" src="../js/jquery.combos.js"></script>
		<script language="javascript" src="js/barrios.js"></script>

        
        <script type="text/javascript">
shortcut.add("Shift+F",function() {
		                                var URL=src();
		var url=URL+"aportes/trabajadores/consultaTrabajador.php";
    	window.open(url,"_blank");
    },{
	'propagate' : true,
	'target' : document 
        });

</script>
    </head>
	<body>
		<form name="forma" method="post" action="">
			<br/>
			<center>
				<table width="70%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="13" height="29" class="arriba_iz">&nbsp;</td>
						<td class="arriba_ce"><span class="letrablanca">::Administracion - Barrios ::</span></td>
						<td width="13" class="arriba_de" align="right">&nbsp;</td>
					</tr>
					<tr>
						<td class="cuerpo_iz">&nbsp;</td>
	                    <td class="cuerpo_ce">
							<img src="../imagenes/tabla/spacer.gif" width="1" height="1">
							<img src="../imagenes/spacer.gif" width="1" height="1">
							<img src="../imagenes/menu/nuevo.png" title="Nuevo" width="16" height="16" style="cursor:pointer" onClick="nuevoB();">
							<img src="../imagenes/spacer.gif" width="1" height="1">
							<img src="../imagenes/menu/grabar.png" title="Guardar" width="16" height="16" onClick="validarCampos(1)" style="cursor:pointer" id="bGuardar">
							<img src="../imagenes/spacer.gif" width="1" height="1">
							<img src="../imagenes/menu/modificar.png" title="Actualizar" width="16" height="16" onClick="validarCampos(2)" id="btnActualizar" style="display:none;cursor:pointer">
							<img src="../imagenes/spacer.gif" width="1" height="1">
							<img src="../imagenes/menu/imprimir.png" width="16" height="16" style="cursor:pointer" title="Imprimir" onClick = "procesar01()" >
							<img src="../imagenes/spacer.gif" width="1" height="1">
							<img src="../imagenes/menu/refrescar.png" width="16" height="16" style="cursor:pointer" title="Limpiar campos" onClick="limpiarCampos(); document.forms[0].elements[0].focus();">
							<img src="../imagenes/spacer.gif" width="1" height="1">
							<img src="../imagenes/menu/buscar.png" title="Buscar" width="16" height="16" style="cursor:pointer" onClick="consultaDatos();">
							<img src="../imagenes/spacer.gif" width="1" height="1">
							<a href="definicionesH.html" target="_blank" onClick="window.open(this.href, this.target, 'width=800,height=550,titlebar=0, resizable=no'); return false;" >
							    <img src="../imagenes/menu/informacion.png" width="16" height="16" style="border:none" title="Manual" />
							</a>
						</td>
						 <td class="cuerpo_de">&nbsp;</td>
					</tr>
				
						<tr>
                        <td class="cuerpo_iz">&nbsp;</td>
                        <td class="cuerpo_ce"><div id="resultado" style="color:#FF0000"></div></td>
                        <td class="cuerpo_de">&nbsp;</td>
                        </tr>  
						
					<tr>
						<td class="cuerpo_iz">&nbsp;</td>
						<td class="cuerpo_ce">
							<table width="95%" border="0" cellspacing="0" class="tablero">
								<tr style="display:none">
									<td width="25%" align="left">Id Barrio</td>
									<td align="left">
										<input name="txtId" class=boxfecha id="txtId" disabled="disabled" />
									</td>
								</tr>	
                                 <tr>
									<td align="left">Barrio</td>
									<td  align="left"><label>
										<input name="txtBarrio" type="text" class="boxlargo" id="txtBarrio" >
										</label>
                                    </td>
								</tr>
								<tr>
									<td>Departamento</td>
									<td>
                                    	<select name="cboDepto" class="boxlargo" id="cboDepto" >
											<option value="0">[Seleccione]</option>
											<?php 
											while ($row=$departamento->fetch()){
												echo "<option value='".$row['coddepartamento']."' ".$seleccion.">".$row['departmento']."</option>";
											}
											?>
										</select>
                                   		<img src="../imagenes/menu/obligado.png" width="12" height="12">
                                    </td>
								</tr>
                                <tr>
									<td>Municipio</td>
									<td>
                                    	<select name="cboCiudad" class="boxlargo" id="cboCiudad">
										</select>
                                   		<img src="../imagenes/menu/obligado.png" width="12" height="12">
                                    </td>
								</tr>
                                <tr>
									<td>Zona</td>
									<td>
                                    	<select name="cboZona" class="boxlargo" id="cboZona">
										</select>
                                   		<img src="../imagenes/menu/obligado.png" width="12" height="12">
                                    </td>
								</tr>
                               <tr>
									<td align="left">Fecha Creaci�n</td>
									<td align="left"><input name="tFecha" class="box1" id="tFecha" readonly="readonly" /></td>
								</tr>	
								<tr>
    	                            <td align="left">Usuario</td>
	    	                        <td align="left"><input name="tusuario" class="boxfecha" id="tusuario" readonly="readonly" /></td>
                                </tr>
							</table>
                        </td>
						 <td class="cuerpo_de">&nbsp;</td>
                          </tr>
                          <tr>
                            <td class="abajo_iz" >&nbsp;</td>
                            <td class="abajo_ce" ></td>
                            <td class="abajo_de" >&nbsp;</td>
                          </tr>
				</table>
			</center>
			<input type="hidden" value="<?php echo $_SESSION["USUARIO"];?>" name="usuario" id="usuario" />
			<input type="hidden" name="idr" id="idr" />
		</form>
	</body>
</html>