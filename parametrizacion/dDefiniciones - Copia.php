<?php
/* autor:       orlando puentes
 * fecha:       24/09/2010
 * objetivo:    
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once 'clases' . DIRECTORY_SEPARATOR . 'dDefinicion.class.php';
//require('clases/dDefinicion.class.php');
$objClase=new dDefinicion;
$consulta = $objClase->mostrar_definiciones();
//$iddefinicion=21;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Definiciones</title>
<link href="../css/Estilos.css" rel="stylesheet" type="text/css" />
<link href="../css/marco.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../js/jquery-1.4.2.js"></script>
<script language="javascript" src="../js/comunes.js"></script>
<script language="javascript" src="js/dDefiniciones.js"></script>
<script type="text/javascript">
shortcut.add("Shift+F",function() {
		                                var URL=src();
		var url=URL+"aportes/trabajadores/consultaTrabajador.php";
    	window.open(url,"_blank");
    },{
	'propagate' : true,
	'target' : document 
        });        
</script>
<script type="text/javascript">
nuevoDef();/*
$(function(){

$("#lDefinicion option[value='<?=$iddefinicion?>']").attr("selected",true);*/
});
</script>
</head>
<body >
<form name="forma">
<br/>
<center>
<table width="70%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="13" height="29" class="arriba_iz">&nbsp;</td>
    <td class="arriba_ce">
    <span class="letrablanca">::&nbsp;Administracion - Detalle Definiciones &nbsp;::</span></td>
    <td width="13" class="arriba_de" align="right">&nbsp;</td>
  </tr>
  <tr>
    <td class="cuerpo_iz">&nbsp;</td>
    <td class="cuerpo_ce"><img src="../imagenes/tabla/spacer.gif" width="1" height="1">
	  <img src="../imagenes/spacer.gif" width="1" height="1">
	  <img src="../imagenes/menu/nuevo.png" title="Nuevo" width="16" height="16" style="cursor:pointer" onClick="nuevoDef();">
	  <img src="../imagenes/spacer.gif" width="1" height="1">
	  <img src="../imagenes/menu/grabar.png" title="Guardar" width="16" height="16" onClick="validarCampos(1)" style="cursor:pointer" id= "bGuardar">
	  <img src="../imagenes/spacer.gif" width="1" height="1">
	  <img src="../imagenes/menu/modificar.png" title="Actualizar" width="16" height="16" onClick="validarCampos(2)" style="cursor:pointer">
	  <img src="../imagenes/spacer.gif" width="1" height="1">
	  <img src="../imagenes/menu/ico_error.png" width="16" height="16" border="0" title="Eliminar registro" style="cursor:pointer" onClick="eliminarDato()" >
	  <img src="../imagenes/spacer.gif" width="1" height="1">
	  <img src="../imagenes/menu/imprimir.png" width="16" height="16" style="cursor:pointer" title="Imprimir" onClick="window.open('dDefinicionesR.php','mainFrame')" >
	  <img src="../imagenes/spacer.gif" width="1" height="1">
	  <img src="../imagenes/menu/refrescar.png" width="16" height="16" style="cursor:pointer" title="Limpiar campos" onClick="limpiarCampos(); document.forms[0].elements[0].focus();">
	  <img src="../imagenes/spacer.gif" width="1" height="1">
      <img src="../imagenes/menu/buscar.png" title="Buscar" width="16" height="16" style="cursor:pointer" onClick="consultaDatos()">
	  <img src="../imagenes/spacer.gif" width="1" height="1">
	  <a href="definicionesH.html" target="_blank" onClick="window.open(this.href, this.target, 'width=800,height=550,titlebar=0, resizable=no'); return false;" >
      <img src="../imagenes/menu/informacion.png" width="16" height="16" style="border:none" title="Manual" /></a>

      </td>
     <td class="cuerpo_de">&nbsp;</td>
  </tr>
  <tr>
    <td class="cuerpo_iz">&nbsp;</td>
    <td class="cuerpo_ce">
    <div id="resultado" style="color:#FF0000"></div></td>
    <td class="cuerpo_de">&nbsp;</td>
  </tr>
  <tr>
    <td class="cuerpo_iz">&nbsp;</td>
    <td class="cuerpo_ce">
	<table width="95%" border="0" cellspacing="0" class="tablero">
      <tr>
        <td width="25%" align="left">Id Detalle Definici&oacute;n</td>
        <td align="left"><input name="tid" class=boxfecha id="tid" /></td>
        </tr>
        <tr>
      <td align="left">Definici&oacute;n</td>
        <td  align="left">
        <select name="lDefinicion" class="boxlargo" id="lDefinicion">
        <option value="0" selected="selected">Seleccione</option>
        <?php
		while($row=mssql_fetch_array($consulta)){
			echo "<option value=".$row['iddefinicion'].">".$row['definicion']."</option>";
			}
		?>	
        </select></td>
        </tr>
      <tr>
      <td align="left">C&oacute;digo Detalle Definici&oacute;n</td>
        <td  align="left"><label>
          <input name="tdefinicion2" type="text" class="boxfecha" id="tdefinicion2" />
        </label></td>
        </tr>
        <tr>
      <td align="left">Detalle Definici&oacute;n</td>
        <td  align="left"><label>
        <input name="tdefinicion" type="text" class="boxlargo" id="tdefinicion" >
        </label></td>
        </tr>
      <tr>
      <td align="left">Concepto</td>
        <td align="left"><input name="tconcepto" type="text" class="boxlargo" id="tconcepto" ></td>
        </tr>
	  <tr>
	    <td align="left">Fecha creaci&oacute;n</td>
	    <td align="left"><input name="tfecha" class="box1" id="tfecha" readonly="readonly" /></td>
	    </tr>
	  <tr>
	    <td align="left">Usuario</td>
	    <td align="left"><input name="tusuario" class="boxfecha" id="tusuario" readonly="readonly" /></td>
	  </tr>
    </table></td>
   <td class="cuerpo_de">&nbsp;</td>
  </tr>
 <tr>
    <td class="abajo_iz" >&nbsp;</td>
    <td class="abajo_ce" ></td>
    <td class="abajo_de" >&nbsp;</td>
  </tr>
</table>
</center>
<input type="hidden" value="<?php echo $_SESSION["USUARIO"];?>" name="usuario" id="usuario" />
<input type="hidden" name="idr" id="idr" />
</form>
</body>
</html>