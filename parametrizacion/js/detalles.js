var app=angular.module('DetallesApp',['ui.bootstrap']);
app.factory('Services',function($http,$q,$httpParamSerializerJQLike){
	var api="ServicioDetalles.php";
	function listar_detalles()
	{
		var request = $http({
			method:"get",
			url:api,
			params:{
				metodo:'listar_detalles'
			}
		});
		return request;
	}

	function listar_grupos()
	{
		var request = $http({
			method:"get",
			url:api,
			params:{
				metodo:'listar_grupos'
			}
		});
		return request;
	}

	function update(data,funcion)
	{
		 var temp_api=api+"?metodo="+funcion
		 var headers= {'Content-Type': 'application/x-www-form-urlencoded'}
	   var request=$http({
	       method:'POST',
	       url:temp_api,
	       data:$httpParamSerializerJQLike(data),
	       headers:headers,
	       cache:false
	   });
		 return request;
	}

	return{
		listar_detalles:listar_detalles,
		listar_grupos:listar_grupos,
		update:update
	};

});
app.controller('ModalInstanceCtrl1', function ($scope, $modalInstance,element,titulo,Services) {

		 $scope.titulo=titulo;
     //console.log(meal_alert_text);
     $scope.element = element;
		 var options=null;
		 Services.listar_grupos().success(function(response){
	 			//console.log(response);
	 			options=response;
	 	}).then(function(){
			$scope.data = {
	 	    availableOptions:options,
	 	    selectedOption: {ID: element.Grupos_ID, Descripcion: element.Grupo} //This sets the default value of the select in the ui
	 	  };
			console.log($scope.data);
		});

    $scope.ok = function () {
			   console.log("opcion seleccionada");
			   console.log($scope.data.selectedOption);
			 	 $scope.element.CodigoTabla1=$scope.data.selectedOption.ID;
				 $scope.element.Grupo=$scope.data.selectedOption.Descripcion;
         $modalInstance.close($scope.element);
     };

     $scope.cancel = function () {
         $modalInstance.dismiss(false);
     };
 });

 app.controller('ModalInstanceCtrl2', function ($scope, $modalInstance,element,titulo) {

 		 $scope.titulo=titulo;
      //console.log(meal_alert_text);
     $scope.element = element;

		 $scope.ok = function () {
 			  $modalInstance.close($scope.element);
      };

      $scope.cancel = function () {
          $modalInstance.dismiss(false);
      };
  });

app.controller('mainController',function($scope,Services,$modal){

  cargar_grupos();

	cargar_detalles();

	$scope.agregar_grupo=function()
	{
		console.log();
		var grupo={
			Descripcion:"",
		}

		modal_2('Agregar grupo',grupo).result.then(function (element) {
			Services.update(element,'agregar_grupo').success(function(response){
				response=angular.fromJson(response);
				toastr.success('Grupo agregado con exito!', {timeOut: 5000});
				element.ID=response;
				$scope.grupos.push(element);
				},function(error){

					toastr.error('Error agregando grupo contacte soporte', {timeOut: 5000});
				});
		}, function (value) {
				//Selection was false
				console.log('cancel');
				console.log(copy_detalle);
		});
	}

	$scope.remover_grupo=function(grupo)
	{
		Services.update(grupo,'remover_grupo').success(function(response){
			toastr.success('Grupo removido con exito!', {timeOut: 5000});
			var index = $scope.grupos.indexOf(grupo);
      $scope.grupos.splice(index, 1);
			//console.log(response);
		},function(error){
			//console.log(erro);
		});
	}

	$scope.agregar_detalle=function()
	{
		console.log();
		var detalle={
			CodigoTabla2_detalles: "",
			Descripcion1_detalles: "",
			Descripcion2_detalles: "",
			Grupo: "",
			CodigoTabla1:""
		}

		modal_1('Agregar Detalles',detalle).result.then(function (element) {
           //console.log(element);
					 Services.update(element,'agregar_detalles').success(function(response){
						 response=angular.fromJson(response);
						 //console.log(response);
						 element.ID_detalles=response;
						 //console.log("agregando nuevo detalle");
						 //console.log(element);
						 $scope.detalles.push(element);
						 toastr.success('Detalle agregado con exito!', {timeOut: 5000});
					 },function(error){

						 toastr.error('Error agregando detalle contacte soporte', {timeOut: 5000});
					 });
     }, function (value) {
           //Selection was false
           //console.log('cancel');
					 //console.log(copy_detalle);
     });

	}
  $scope.editar_detalle=function(detalle)
	{
		copy_detalle=angular.copy(detalle);
		console.log(detalle);
		modal_1('Editar Detalles',detalle).result.then(function (element) {
				console.log(element);
				Services.update(element,'actualizar_detalles').success(function(response){
					console.log(response);
					toastr.success('Detalle actualizado con exito!', {timeOut: 5000});
				},function(error){
					console.log(erro);
				});
     }, function (value) {
			 console.log('cancel');
			 console.log(copy_detalle);
			 angular.copy(copy_detalle, detalle);
			 //detalle=angular.copy(copy_detalle);
     });

	}

	$scope.editar_grupo=function(grupo)
	{
		copy_grupo=angular.copy(grupo);
		modal_2('Editar Grupo',grupo).result.then(function (element) {
				console.log(element);
				Services.update(element,'actualizar_grupo').success(function(response){
					console.log(response);
					toastr.success('Detalle actualizado con exito!', {timeOut: 5000});
				},function(error){
					console.log(erro);
				});
     }, function (value) {
			 angular.copy(copy_grupo, grupo);
		 });

	}

	$scope.remover_detalle=function(detalle)
	{
		console.log(detalle);
		Services.update(detalle,'remover_detalles').success(function(response){
			toastr.success('Detalle removido con exito!', {timeOut: 5000});
			var index = $scope.detalles.indexOf(detalle);
      $scope.detalles.splice(index, 1);
			console.log(response);
		},function(error){
			console.log(erro);
		});
	}

	$scope.setItemsPerPage = function(num) {
	  $scope.itemsPerPage = num;
	  $scope.currentPage = 1; //reset to first paghe
	}

	$scope.viewby = '10';
  $scope.currentPage = 1;
  $scope.itemsPerPage = $scope.viewby;
  $scope.maxSize = 5;

	function cargar_grupos()
	{
		Services.listar_grupos().success(function(response){
			//console.log(response);
			$scope.grupos=response;
		});
	}
	function cargar_detalles()
	{
		Services.listar_detalles().success(function(response){
			//console.log(response);
			$scope.detalles=response;
			$scope.totalItems = $scope.detalles.length;
		});

	}

	function modal_1(titulo,detalle)
	{
		return $modal.open({
         animation: true,
         //template: this_html,
         templateUrl: 'myModalContent1.html',
         controller: 'ModalInstanceCtrl1',
         resolve: {
             titulo: function () {
                 return titulo;
             },
             element: function () {
                 return detalle;
             }
         }
     });

	}

	function modal_2(titulo,grupo)
	{
		return $modal.open({
         animation: true,
         //template: this_html,
         templateUrl: 'myModalContent2.html',
         controller: 'ModalInstanceCtrl2',
         resolve: {
             titulo: function () {
                 return titulo;
             },
             element: function () {
                 return grupo;
             }
         }
     });

	}

});
