// JavaScript Document
// autor Orlando Puentes A
// fecha marzo 14 de 2010
// objeto administración de los eventos del formulario de barrios.php
// funcion para obtener cookie en javascript
	function getCookie(c_name)
	{
		var i,x,y,ARRcookies=document.cookie.split(";");
		for (i=0;i<ARRcookies.length;i++)
		{
  			x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
  			y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
  			x=x.replace(/^\s+|\s+$/g,"");
  			if (x==c_name)
    			{
    				return unescape(y);
    			}// fin if
		}// fin for
  	}// fin getCookie
//funcion para obtener cookie en javascript
	
nuevo=0;
modificar=0;
$(function(){
	$( "#txtBarrio" ).autocomplete({
	    source: function( request, response ) {
	    	var nombarrio=$("#txtBarrio").val();
	        $.ajax({
	            url: "barriosAut.php",
	            data: {v0:nombarrio},
	            dataType: "json",
	            success: function( data ) {
	                response( data );
	            }
	        });
	     },
	    select: function( event, ui ) {
	    	$("#txtId").val(ui.item.id);
	    }
	});
});
function validarCampos(op){
	
	if(nuevo==0){
	alert("Click en NUEVO !");
	return false;
	}
	
	if($("#txtBarrio").val()==''){
		alert("Digite el Barrio!");
		$("#txtBarrio").focus();
		return;
	}
		
	if($("#cboDepto").val()==0){
		alert("Seleccione Departamento!");
		$("#cboDepto").focus();
		return;
	}
	if($("#cboCiudad").val()==0){
		alert("Seleccione Municipio!");
		$("#cboCiudad").focus();
		return;
	}
	if($("#cboZona").val()==0){
		alert("Seleccione Zona!");
		$("#cboZona").focus();
		return;
	}
	
	if(op==1){
		guardarDatos();
	}	
	else{
		actualizarDatos();
	}
}
	
function nuevoB(){
	nuevo=1;
	$("#bGuardar").show();
	$("#btnActualizar").hide();
	$("#bGuardar").show();
	limpiarCampos();
	document.forms[0].elements[1].focus();
	document.forms[0].elements[5].value = fechaHoy();
	document.forms[0].elements[6].value= document.forms[0].elements['usuario'].value;
	}

function guardarDatos(){
	$("#bGuardar").hide();
	var campo0=$("#txtId").val();
	var campo1=$("#cboDepto").val();
	var campo2=$("#cboCiudad").val();
	var campo3=$("#cboZona").val();
	var campo4=$("#txtBarrio").val();
	var campo5=$("#tFecha").val();
	var campo6=$("#tusuario").val();
	$.ajax({
		url: 'barriosG.php',
		type: "POST",
		data: "submit=&v0="+campo0+"&v1="+campo1+"&v2="+campo2+"&v3="+campo3+"&v4="+campo4+"&v5="+campo5+"&v6="+campo6,
		success: function(datos){
			$("#bGuardar").show();
			alert(datos);
			limpiarCampos();
		}
	});
	nuevo=0;
}
	
/*function consultaDatos(){
	
		var nombarrio=$("#txtBarrio").val();		
		$.ajax({
			url: 'barriosC.php',
			type: "POST",
			data: {v0:nombarrio},
			async: false,
			dataType: 'json',
			success: function(data){
				dialogLoading("hide");
				
				if(data==0){
					alert("Lo lamento, el barrio no existe!");
					return false;
				}
				
				$.each(data,function(i,fila){				
					idb=fila.idbarrio;
					nom=fila.barrio;
					$("#txtId").val(fila.idbarrio);					
					$("#barrios").append('<p align="left" onclick="consultaBarrios('+idb+')">'+idb+" "+nom+'</p>');			
				});//fin each		
			}
		});
}*/

function consultaDatos(){
	if($("#txtBarrio").val()==''){
		alert("Ingrese el barrio a buscar.");
		$("#txtBarrio").focus();
		return false;
	}
	var idBarrio = $("#txtId").val(); 
	//var nombarrio=$("#txtBarrio").val();
		$.ajax({
			url: 'barriosC.php',
			cache: false,
			type: "POST",
			data: {v0:idBarrio},
			dataType:"json",
			success: function(data){
				if(data==0){
					alert("Lo lamento, el barrio no existe!");
					return false;
				}
				idzona=$.trim(data.codigozona);
				iddepto=idzona.slice(0,2);
				idciudad=idzona.slice(0,5);
			
				$("#txtId").val(data.idbarrio);
				//Cargar Departementos-ciudades y zonas
				$("#cboDepto").val(iddepto).trigger("change");
				setTimeout((function(){
			    $("#cboCiudad").val(idciudad);
			    $("#cboCiudad").trigger("change");
			    }),1000);
				setTimeout((function(){
			    $("#cboZona").val(idzona);
			    $("#cboZona").trigger("change");
			    }),2000);
				
				$("#txtBarrio").val($.trim(data.barrio));
				$("#tFecha").val($.trim(data.fechasistema));
				$("#tusuario").val($.trim(data.usuario));
				$("#idr").val(data.idbarrio);//campo oculto en barrios.php
				$("#btnActualizar").show();
				$("#bGuardar").hide();
			}
		});
	
}	

function actualizarDatos(){
		var campo0=parseInt($("#idr").val());
		var campo1=$("#cboDepto").val();
		var campo2=$("#cboCiudad").val();
		var campo3=$("#cboZona").val();
		var campo4=$("#txtBarrio").val();
		var campo5=$("#tFecha").val();
		var campo6=$("#tusuario").val();
		
		$.ajax({
			url: 'barriosA.php',
			type: "POST",
			data: {v0:campo0,v1:campo1,v2:campo2,v3:campo3,v4:campo4,v5:campo5,v6:campo6},
			success: function(datos){
				alert(datos);
				limpiarCampos();
			}
		});
		nuevo=0;
	}
	
/*function eliminarDato(){
		var msg = confirm("Desea eliminar este barrio?");
		if ( msg ) {
			var id_registro=parseInt($("#idr").val());
			$.ajax({
				url: 'barriosE.php',
				type: "GET",
				data: "v0="+id_registro,
				success: function(datos){
					limpiarCampos();
					alert(datos);	
				}
			});
		}
		return false;
	}*/
/**MANDA EL REPORTE A EXEL**/
		function procesar01(){
			var usuario=$("#usuario").val();		
			url="http://"+getCookie("URL_Reportes")+"/aportes/parametrizacion/rptExcel001.jsp?v0="+usuario;
			window.open(url,"_NEW");
		}
function ayuda(){
	
	}	