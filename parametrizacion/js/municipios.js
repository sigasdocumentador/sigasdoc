// funcion para obtener cookie en javascript
	function getCookie(c_name)
	{
		var i,x,y,ARRcookies=document.cookie.split(";");
		for (i=0;i<ARRcookies.length;i++)
		{
  			x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
  			y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
  			x=x.replace(/^\s+|\s+$/g,"");
  			if (x==c_name)
    			{
    				return unescape(y);
    			}// fin if
		}// fin for
  	}// fin getCookie
//funcion para obtener cookie en javascript

nuevo=0;
modificar=0;
$(function(){
	$( "#txtZona" ).autocomplete({
	    source: function( request, response ) {
	    	var nomzona=$("#txtZona").val();
	        $.ajax({
	            url: "zonaAut.php",
	            data: {v0:nomzona},
	            dataType: "json",
	            success: function( data ) {
	                response( data );
	            }
	        });
	     },
	    select: function( event, ui ) {
	    	$("#txtId").val(ui.item.id);
	    }
	});
});


$(function(){
	$( "#txtMunicipio" ).autocomplete({
	    source: function( request, response ) {
	    	var nomun=$("#txtMunicipio").val();
	        $.ajax({
	            url: "municipioAut.php",
	            data: {v0:nomun},
	            dataType: "json",
	            success: function( data ) {
	                response( data );
	            }
	        });
	     },
	    select: function( event, ui ) {
	    	$("#txtId").val(ui.item.id);
	    }
	});
});

$(function(){
	$( "#txtDep" ).autocomplete({
	    source: function( request, response ) {
	    	var nomdep=$("#txtDep").val();
	        $.ajax({
	            url: "departamentoAut.php",
	            data: {v0:nomdep},
	            dataType: "json",
	            success: function( data ) {
	                response( data );
	            }
	        });
	     },
	    select: function( event, ui ) {
	    	$("#txtId").val(ui.item.id);
	    }
	});
});

function mostrar(){
	var opt=$("#crearPor").val();
	//limpiarCampos();
	if(opt==1){
		$("#departamento").hide();
		$("#txtdepartamento").show();
		$("#codigodept").show();
		$("#cboPais").focus();
		}
	if(opt==2){
		$("#txtdepartamento").hide();
		$("#codigodept").hide();
		$("#departamento").show();
		$("#cboPais").focus();
		}
	
	}

$(function(){
//Carga Departamentos
$("#cboPais").change(function () {
	$("#cboPais option:selected").each(function() {
		elegido=$(this).val();
		if(elegido==0){
			$("#cboDepto").html('<option value="0">Seleccione</option>');
		} else {
			$.post(URL+"phpComunes/departamentos.php", { elegido: elegido }, function(data){
				$("#cboDepto").html(data);
				//$("#cboCiudad2").html('<option value="0">Seleccione</option>');
			});
		}					
	});
});
})
function validarCampos(op){
	var opt=$("#crearPor").val();
	
	if(nuevo==0){
	alert("Click en NUEVO !");
	return false;
		}
	
	if($("#crearPor").val()==0){
	alert("Seleccione la opcion a crear");
	$("#crearPor").focus();
	return;
	}

if(opt==1){
	if($("#cboPais").val()==0){
		alert("Seleccione el pais!");
		$("#cboPais").focus();
		return;
	}
	if($("#txtDep").val()==''){
		alert("Digite el Departamento!");
		$("#txtDep").focus();
		return;
	}
	if($("#txtCodDep").val()==''){
		alert("Digite el codigo del Departamento!");
		$("#txtCodDep").focus();
		return;
	}	
	if($("#txtMunicipio").val()==''){
		alert("Digite el Municipio!");
		$("#txtMunicipio").focus();
		return;
	}
	if($("#txtCodMun").val()==''){
		alert("Digite el codigo del municipio!");
		$("#txtCodMun").focus();
		return;
	}
	if($("#txtZona").val()==''){
		alert("Digite la zona!");
		$("#txtZona").focus();
		return;
	}
	if($("#txtCodZona").val()==''){
		alert("Digite la zona!");
		$("#txtCodZona").focus();
		return;
	}
}else{
	
	if($("#cboPais").val()==0){
		alert("Seleccione el pais!");
		$("#cboPais").focus();
		return;
	}
	if($("#cboDepto").val()==0){
		alert("Seleccione el Departamento!");
		$("#cboDepto").focus();
		return;
	}	
	if($("#txtMunicipio").val()==''){
		alert("Digite el Municipio!");
		$("#txtMunicipio").focus();
		return;
	}
	if($("#txtCodMun").val()==''){
		alert("Digite el codigo del municipio!");
		$("#txtCodMun").focus();
		return;
	}
	if($("#txtZona").val()==''){
		alert("Digite la zona!");
		$("#txtZona").focus();
		return;
	}
	if($("#txtCodZona").val()==''){
		alert("Digite la zona!");
		$("#txtCodZona").focus();
		return;
	}
}
	
	if(op==1){
		guardarDatos();
	}	
	else{
		actualizarDatos();
	}
}
	
function nuevoB(){
	nuevo=1;
	$("#bGuardar").show();
	$("#btnActualizar").hide();
	$("#bGuardar").show();
	limpiarCampos();
	document.forms[0].elements[1].focus();
	document.forms[0].elements[10].value = fechaHoy();
	document.forms[0].elements[11].value= document.forms[0].elements['usuario'].value;
	}

function guardarDatos(){
	$("#bGuardar").hide();
	
	var campo0=$("#cboPais").val();
	var campo1=$("#cboDepto").val();
	var campo2=$("#txtDep").val();
	var campo3=$("#txtCodDep").val();
	var campo4=$("#txtMunicipio").val();
	var campo5=$("#txtCodMun").val();
	var campo6=$("#txtZona").val();
	var campo7=$("#txtCodZona").val();
	var campo8=$("#tFecha").val();
	var campo9=$("#tusuario").val();
	var campo10=$("#crearPor").val();
	$.ajax({
		url: 'municipiosG.php',
		type: "POST",
		data: "submit=&v0="+campo0+"&v1="+campo1+"&v2="+campo2+"&v3="+campo3+"&v4="+campo4+"&v5="+campo5+"&v6="+campo6+"&v7="+campo7+"&v8="+campo8+"&v9="+campo9+"&v10="+campo10,
		success: function(datos){
			$("#bGuardar").show();
			alert(datos);
			limpiarCampos();
		}
	});
	nuevo=0;
}

function consultaDatos(){
	if($("#txtCodZona").val()==''){
		alert("Ingrese la zona a buscar.");
		$("#txtCodZona").focus();
		return false;
	}
	var idCodZona = $("#txtCodZona").val(); 
		$.ajax({
			url: 'municipioC.php',
			cache: false,
			type: "POST",
			data: {v0:idCodZona},
			dataType:"json",
			success: function(data){
				if(data==0){
					alert("Lo lamento, la zona no existe!");
					return false;
				}
				
				idpais=$.trim(data.idpais);
				idpais=idpais.slice(0,2);
								
				$("#cboPais").val(idpais).trigger("change");
			    $("#coddepartamento").val(idpais);
			    $("#coddepartamento").trigger("change");
			    
				$("#txtDep").val($.trim(data.departmento));
				$("#txtCodDep").val($.trim(data.coddepartamento));
				$("#txtMunicipio").val($.trim(data.municipio));
				$("#txtCodMun").val($.trim(data.codmunicipio));
				$("#txtZona").val($.trim(data.zona));
				$("#txtCodZona").val($.trim(data.codzona));
				$("#tFecha").val($.trim(data.fechasistema));
				$("#tusuario").val($.trim(data.usuario));
				$("#idr").val(data.codigozona);//campo oculto
				$("#btnActualizar").show();
				$("#bGuardar").hide();
			}
		});
	
}	

function actualizarDatos(){	
	
		var campo0=$("#cboPais").val();
		var campo1=$("#cboDepto").val();
		var campo2=$("#txtDep").val();
		var campo3=$("#txtCodDep").val();
		var campo4=$("#txtMunicipio").val();
		var campo5=$("#txtCodMun").val();
		var campo6=$("#txtZona").val();
		var campo7=$("#txtCodZona").val();
		var campo8=$("#tusuario").val();
		var campo9=$("#crearPor").val();
		
		$.ajax({
			url: 'municipioA.php',
			type: "POST",
			data: {v0:campo0,v1:campo1,v2:campo2,v3:campo3,v4:campo4,v5:campo5,v6:campo6,v7:campo7,v8:campo8,v9:campo9},
			success: function(datos){
				alert(datos);
				limpiarCampos();
			}
		});
		nuevo=0;
	}
/*	
function eliminarDato(){
		var msg = confirm("Desea eliminar este barrio?");
		if ( msg ) {
			var id_registro=parseInt($("#idr").val());
			$.ajax({
				url: 'barriosE.php',
				type: "GET",
				data: "v0="+id_registro,
				success: function(datos){
					limpiarCampos();
					alert(datos);	
				}
			});
		}
		return false;
	}*/
/**MANDA EL REPORTE A EXEL**/
		function procesar05(){
			var usuario=$("#usuario").val();		
			url="http://"+getCookie("URL_Reportes")+"/parametrizacion/rptExcel005.jsp?v0="+usuario;
			window.open(url,"_NEW");
		}
function ayuda(){
	
	}	