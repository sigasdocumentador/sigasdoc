var app=angular.module('HomologacionesApp',['ui.bootstrap']);
app.factory('Services',function($http,$q,$httpParamSerializerJQLike){
	var api="ServicioHomologacion.php";
	function listar_homologaciones()
	{
		var request = $http({
			method:"get",
			url:api,
			params:{
				metodo:'listar_homologaciones'
			}
		});
		return request;
	}

	function listar_detalles()
	{
		var request = $http({
			method:"get",
			url:api,
			params:{
				metodo:'listar_detalles'
			}
		});
		return request;
	}

	function update(data,funcion)
	{
		 var temp_api=api+"?metodo="+funcion
		 var headers= {'Content-Type': 'application/x-www-form-urlencoded'}
	   var request=$http({
	       method:'POST',
	       url:temp_api,
	       data:$httpParamSerializerJQLike(data),
	       headers:headers,
	       cache:false
	   });
		 return request;
	}

	return{
		listar_homologaciones:listar_homologaciones,
		listar_detalles:listar_detalles,
		update:update
	};

});
app.controller('ModalInstanceCtrl1', function ($scope, $modalInstance,element,titulo,Services) {

		 $scope.titulo=titulo;
     //console.log(meal_alert_text);
     $scope.element = element;
		 var options=null;
		 Services.listar_detalles().success(function(response){
	 			//console.log(response);
	 			options=response;
	 	}).then(function(){
			$scope.data = {
	 	    availableOptions:options,
	 	    selectedOption: {ID: element.Dato_Homologacion, Descripcion: element.Dato_Homologacion_Detalles} //This sets the default value of the select in the ui
	 	  };
			console.log($scope.data);
		});

    $scope.ok = function () {
			   console.log("opcion seleccionada");
			   console.log($scope.data.selectedOption);
			 	 $scope.element.Dato_Homologacion=$scope.data.selectedOption.ID;
				 $scope.element.Dato_Homologacion_Detalles=$scope.data.selectedOption.Descripcion;
         $modalInstance.close($scope.element);
     };

     $scope.cancel = function () {
         $modalInstance.dismiss(false);
     };
 });

app.controller('mainController',function($scope,Services,$modal){

	cargar_homologaciones();

	$scope.agregar_homologacion=function()
	{
		console.log("agregando");
		var homologacion={
			Descripcion: "",
			Dato_Sigas: "",
			Dato_Homologacion: "",
		}

		modal_1('Agregar Homologaciones',homologacion).result.then(function (element) {
           //console.log(element);
					 Services.update(element,'agregar_homologaciones').success(function(response){
						 response=angular.fromJson(response);
						 //console.log(response);
						 element.ID_homologaciones=response;
						 //console.log("agregando nuevo homologacion");
						 //console.log(element);
						 $scope.homologaciones.push(element);
						 toastr.success('homologacion agregada con exito!', {timeOut: 5000});
					 },function(error){

						 toastr.error('Error agregando homologacion contacte soporte', {timeOut: 5000});
					 });
     }, function (value) {
           //Selection was false
           //console.log('cancel');
					 //console.log(copy_homologacion);
     });

	}
  $scope.editar_homologacion=function(homologacion)
	{
		copy_homologacion=angular.copy(homologacion);
		console.log(homologacion);
		modal_1('Editar homologaciones',homologacion).result.then(function (element) {
				console.log(element);
				Services.update(element,'actualizar_homologaciones').success(function(response){
					console.log(response);
					toastr.success('homologacion actualizada con exito!', {timeOut: 5000});
				},function(error){
					console.log(erro);
				});
     }, function (value) {
			 console.log('cancel');
			 console.log(copy_homologacion);
			 angular.copy(copy_homologacion, homologacion);
			 //homologacion=angular.copy(copy_homologacion);
     });

	}

	$scope.remover_homologacion=function(homologacion)
	{
		console.log(homologacion);
		Services.update(homologacion,'remover_homologaciones').success(function(response){
			toastr.success('homologacion removida con exito!', {timeOut: 5000});
			var index = $scope.homologaciones.indexOf(homologacion);
      $scope.homologaciones.splice(index, 1);
			console.log(response);
		},function(error){
			console.log(erro);
		});
	}

	$scope.setItemsPerPage = function(num) {
	  $scope.itemsPerPage = num;
	  $scope.currentPage = 1; //reset to first paghe
	}

	$scope.viewby = '10';
  $scope.currentPage = 1;
  $scope.itemsPerPage = $scope.viewby;
  $scope.maxSize = 5;

	function cargar_homologaciones()
	{
		Services.listar_homologaciones().success(function(response){
			//console.log(response);
			$scope.homologaciones=response;
			$scope.totalItems = $scope.homologaciones.length;
		});

	}

	function modal_1(titulo,homologacion)
	{
		return $modal.open({
         animation: true,
         //template: this_html,
         templateUrl: 'myModalContent1.html',
         controller: 'ModalInstanceCtrl1',
         resolve: {
             titulo: function () {
                 return titulo;
             },
             element: function () {
                 return homologacion;
             }
         }
     });

	}

});
