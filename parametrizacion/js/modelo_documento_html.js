var form = null,
	objAccion = {SELECT:"S", INSERT:"I", UPDATE:"U"},
	urlModeloDocumento = "";
	

$(function(){
	form = document.form1;
	urlModeloDocumento = src()+"parametrizacion/logica/modelo_documento_html.log.php";

    $("#btnNuevo").click(nuevo);
    $("#btnGuardar").click(guardar);
    $("#btnListar").click(listar);
    
    //Preparar el editor de html
    CKEDITOR.replace ("txaContenidoHtml");
    
    $("#divListar").dialog({
    	autoOpen: false,
    	modal: true,
    	width: 800,
    	buttons: {
    		"Cerrar": function(){
    			$(this).dialog("close");
    		}
    	}
    });
});

function nuevo(){
	limpiarCampos2("#txtNombrModelDocum,#hidIdModeloDocumento,#txtCodigModelDocum,#tblModeloDocumentoHtml tbody");
	CKEDITOR.instances['txaContenidoHtml'].setData("");
}

function fetchModeloDocumentoHtml(accion,objFiltro){
	var resultado = null;
	
	$.ajax({
		url:urlModeloDocumento,
		type:"POST",
		data:{accion:accion,objDatosFiltro:objFiltro},
		dataType: "json",
		async:false,
		success:function(data){
			resultado = data;
		}
	});
	
	return resultado;
}

function ctrModeloDocumentoHtml(idModeloDocumento){
	
	var arrDatos = fetchModeloDocumentoHtml(objAccion.SELECT, {id_modelo_documento:idModeloDocumento});
	
	if(arrDatos.data && typeof arrDatos.data == "object" && arrDatos.data.length>0){
		arrDatos = arrDatos.data[0];
		
		$("#hidIdModeloDocumento").val(arrDatos.id_modelo_documento);
		$("#txtNombrModelDocum").val(arrDatos.nombr_model_docum);
		$("#txtCodigModelDocum").val(arrDatos.modelo_documento);
		CKEDITOR.instances['txaContenidoHtml'].setData(arrDatos.contenido_html);
		
	}else{
		alert("No se encontraron resultado!!");
	}
	
	$("#divListar").dialog("close");
}

function listar(){
	nuevo();
	var arrDatos = fetchModeloDocumentoHtml(objAccion.SELECT, {});
	
	if(arrDatos.data && typeof arrDatos.data == "object" && arrDatos.data.length>0){
		
		var contenidoHtml = "";
		
		$.each(arrDatos.data,function(i,row){
			contenidoHtml += "<tr>" +
					"<td>"+row.id_modelo_documento+"</td>" +
					"<td><a onclick='ctrModeloDocumentoHtml("+row.id_modelo_documento+");' style='cursor:pointer;'>"+row.nombr_model_docum+"</a></td>" +
					"<td>"+row.modelo_documento+"</td></tr>";
		});
		
		
		$("#tblModeloDocumentoHtml tbody").html(contenidoHtml);
		$("#divListar").dialog("open");
	}else{
		alert("No se encontraron resultado!!");
	}
}

function guardar(){
	
	//Verificar campos obligatorios
	if(validateElementsRequired()) return false;
	
	 var contenido = CKEDITOR.instances['txaContenidoHtml'].getData();	
	var objDatos = {
			id_modelo_documento: $("#hidIdModeloDocumento").val().trim()
			, nombr_model_docum: $("#txtNombrModelDocum").val().trim()
			, modelo_documento: $("#txtCodigModelDocum").val().trim()
			, contenido_html: (contenido)
			, estado: "A"
	};
	
	var accion = objDatos.id_modelo_documento?objAccion.UPDATE:objAccion.INSERT;
	
	$.ajax({
		url:urlModeloDocumento,
		type:"POST",
		data:{accion:accion,datos:objDatos},
		dataType: "json",
		async:false,
		success:function(data){
			if(data.error==0){
				alert("Los datos se guardaron correctamente");
				nuevo();
				return true;
				
			}
			
			alert("Error al guardar los datos");
		}
	});
}