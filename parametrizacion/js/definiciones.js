// funcion para obtener cookie en javascript
	function getCookie(c_name)
	{
		var i,x,y,ARRcookies=document.cookie.split(";");
		for (i=0;i<ARRcookies.length;i++)
		{
  			x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
  			y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
  			x=x.replace(/^\s+|\s+$/g,"");
  			if (x==c_name)
    			{
    				return unescape(y);
    			}// fin if
		}// fin for
  	}// fin getCookie
//funcion para obtener cookie en javascript

// JavaScript Document
// autor Orlando Puentes A
// fecha marzo 14 de 2010
// objeto administración de los eventos del formulario de definiciones.php
var nuevo=0;
var modificar=0;

$(function(){
	$( "#tdefinicion" ).autocomplete({
	    source: function( request, response ) {
	    	var nombarrio=$("#tdefinicion").val();
	        $.ajax({
	            url: "definicionAut.php",
	            data: {v0:nombarrio},
	            dataType: "json",
	            success: function( data ) {
	                response( data );
	            }
	        });
	     },
	    select: function( event, ui ) {
	    	$("#tid").val(ui.item.id);
	    }
	});
});

function validarCampos(op){
	if(document.forms[0].elements[1].value.length==0){
		alert("Digite Definicion!");
		document.forms[0].elements[1].focus();
		return;
		}
	if(document.forms[0].elements[2].value.length==0){
		alert("Digite el concepto!");
		document.forms[0].elements[2].focus();
		return;
		}
	if(op==1){
		guardarDatos();
		}	
	else{
		actualizarDatos();
		}
	}
	
function nuevoR(){
	nuevo=1;
	$("#bGuardar").show();
	limpiarCampos();
	document.forms[0].elements[1].focus();
	document.forms[0].elements[3].value = fechaHoy();
	document.forms[0].elements[4].value= document.forms[0].elements['usuario'].value;
	}

function guardarDatos(){
		$("#bGuardar").hide();
		var campo0=document.forms[0].elements[0].value;
		var campo1=document.forms[0].elements[1].value;
		var campo2=document.forms[0].elements[2].value;
		var campo3=document.forms[0].elements[3].value;
		var campo4=document.forms[0].elements[4].value;

		$.ajax({
			url: 'definicionesG.php',
			type: "POST",
			data: "submit=&v0="+campo0+"&v1="+campo1+"&v2="+campo2+"&v3="+campo3+"&v4="+campo4,
			success: function(datos){
				$("#bGuardar").show();
				nuevoR();
				alert(datos);
				limpiarCampos();
			}
		});
		return false;
	}
	
function consultaDatos(){
		if($("#tdefinicion").val()==''){
			alert("Ingrese la definicion a buscar.");
			$("#tdefinicion").focus();
			return false;
		}
		var idDefinicion = $("#tid").val(); 
		
		$.ajax({
			url: 'definicionesC.php',
			cache: false,
			type: "POST",
			data: {v0:idDefinicion},
			dataType:"json",
			success: function(data){
				if(data==0){
					alert("Lo lamento, el registro no existe!");
					return false;
				}
				//var result=datos.split(",");
				$("#tid").val(data.iddefinicion);				
				$("#tdefinicion").val($.trim(data.definicion));
				$("#tconcepto").val($.trim(data.concepto));
				$("#tfecha").val($.trim(data.fechacreacion));
				$("#tusuario").val($.trim(data.usuario));
				$("#idr").val(data.iddefinicion);//campo oculto en definiciones.php
				$("#btnActualizar").show();
				$("#bGuardar").hide();
				/*
				document.forms[0].elements[0].value=result[0];
				document.forms[0].elements[1].value=result[1];
				document.forms[0].elements[2].value=result[2];
				document.forms[0].elements[3].value=result[3];
				document.forms[0].elements[4].value=result[4];
				document.forms[0].idr.value=result[0];*/
			}
		});
	}	

function actualizarDatos(){
		var campo0=document.forms[0].idr.value;
		var campo1=document.forms[0].elements[1].value;
		var campo2=document.forms[0].elements[2].value;
		var campo3=document.forms[0].elements[3].value;
		var campo4=document.forms[0].elements[4].value;
		
		$.ajax({
			url: 'definicionesA.php',
			cache: false,
			type: "POST",
			data: "submit=&v0="+campo0+"&v1="+campo1+"&v2="+campo2+"&v3="+campo3+"&v4="+campo4,
			success: function(datos){
				alert(datos);
				limpiarCampos();
				
			}
		});
		return false;
	}
	
/*function eliminarDato(){
		var msg = confirm("Desea eliminar este dato?")
		if ( msg ) {
			var id_registro=document.forms[0].idr.value;
			$.ajax({
				url: 'definicionesE.php',
				type: "GET",
				data: "v0="+id_registro,
				success: function(datos){
					limpiarCampos();
					alert(datos);	
				}
			});
		}
		return false;
	}*/

/**MANDA EL REPORTE A EXEL**/
function procesar02(){
	var usuario=$("#usuario").val();		
	url="http://"+getCookie("URL_Reportes")+"/parametrizacion/rptExcel002.jsp?v0="+usuario;
	window.open(url,"_NEW");
}
function ayuda(){
	
	}	