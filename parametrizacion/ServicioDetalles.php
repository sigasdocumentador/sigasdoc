<?php
		/* autor:
		 * fecha:
		 * objetivo:   Servicios para detalles
		 */
		ini_set("display_errors",'1');
		$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
		include_once  $root;

		$conR=$_SESSION['REGISTRO'];

		include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
		include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';

		$metodo=$_REQUEST['metodo'];
		switch($metodo){
			case 'listar_detalles':
			 	$result=listar_detalles();
				break;
			case 'agregar_detalles':
				$result=agregar_detalles();
				break;
			case 'actualizar_detalles':
				$result=actualizar_detalles();
				break;
			case 'remover_detalles':
				$result=remover_detalles();
				break;
			case 'listar_grupos':
			 	$result=listar_grupos();
				break;
			case 'agregar_grupo':
				$result=agregar_grupos();
				break;
			case 'actualizar_grupo':
				$result=actualizar_grupos();
				break;
			case 'remover_grupo':
				$result=remover_grupos();
				break;
		}

		echo json_encode($result);

		function listar_detalles(){
			$db=connectar();
			$sql="select ID_detalles,CodigoTabla2_detalles,D.Grupos_ID,Descripcion1_detalles,Descripcion2_detalles,Descripcion as Grupo "
			."from Detalles as D "
			."join Grupos as G on G.ID=D.Grupos_ID "
			."WHERE G.Estado='A' and D.Estado='A'";
			$query=$db->querySimple($sql);
			$result=$query->fetchAll();
			return $result;
		}

		function listar_grupos(){
			$db=connectar();
			$sql="Select * from Grupos where Estado='A'";
			$query=$db->querySimple($sql);
			$result=$query->fetchAll();
			return $result;
		}
		function agregar_detalles(){
			  $CodigoTabla1=isset($_REQUEST['CodigoTabla1'])?$_REQUEST['CodigoTabla1']:'';
				$CodigoTabla2_detalles=isset($_REQUEST['CodigoTabla2_detalles'])?$_REQUEST['CodigoTabla2_detalles']:'';
				$Descripcion1_detalles=isset($_REQUEST['Descripcion1_detalles'])?$_REQUEST['Descripcion1_detalles']:'';
				$Descripcion2_detalles=isset($_REQUEST['Descripcion2_detalles'])?$_REQUEST['Descripcion2_detalles']:'';
				$ID_detalles=isset($_REQUEST['ID_detalles'])?$_REQUEST['ID_detalles']:'';
				$sql="INSERT INTO Detalles VALUES ('%s',%s,'%s','%s','%s')";
				$sql=sprintf($sql,$CodigoTabla2_detalles,$CodigoTabla1,$Descripcion1_detalles,$Descripcion2_detalles,'A');
				$db=connectar();
				$rs=$db->queryInsert($sql,'Detalles');

				if($rs !=0 ){
					$result=$rs;
				}
				else{
					$result=msg_error($db->error);
				}
				return $result;
		}
		function actualizar_detalles(){
			  $CodigoTabla1=isset($_REQUEST['CodigoTabla1'])?$_REQUEST['CodigoTabla1']:'';
				$CodigoTabla2_detalles=isset($_REQUEST['CodigoTabla2_detalles'])?$_REQUEST['CodigoTabla2_detalles']:'';
				$Descripcion1_detalles=isset($_REQUEST['Descripcion1_detalles'])?$_REQUEST['Descripcion1_detalles']:'';
				$Descripcion2_detalles=isset($_REQUEST['Descripcion2_detalles'])?$_REQUEST['Descripcion2_detalles']:'';
				$ID_detalles=isset($_REQUEST['ID_detalles'])?$_REQUEST['ID_detalles']:'';
				$sql="UPDATE Detalles SET Grupos_ID='%s',CodigoTabla2_detalles='%s',Descripcion1_detalles='%s',Descripcion2_detalles='%s' WHERE ID_detalles=%s";
				$sql=sprintf($sql,$CodigoTabla1,$CodigoTabla2_detalles,$Descripcion1_detalles,$Descripcion2_detalles,$ID_detalles);
				echo $sql;
				$db=connectar();
				$rs=$db->queryActualiza($sql);
				if($rs !=0 ){
					$result=$rs;
				}
				else{
					$result=$db->error;
				}
				return $result;
		}
		function agregar_grupos(){
			  $CodigoTabla1=isset($_REQUEST['CodigoTabla1'])?$_REQUEST['CodigoTabla1']:'';
				$ID=isset($_REQUEST['ID'])?$_REQUEST['ID']:'';
				$Descripcion=isset($_REQUEST['Descripcion'])?$_REQUEST['Descripcion']:'';
				$sql="INSERT INTO Grupos VALUES ('%s','%s')";
				$sql=sprintf($sql,$Descripcion,'A');
				$db=connectar();
				$rs=$db->queryInsert($sql,'Grupos');
				if($rs !=0 ){
					$result=$rs;
				}
				else{
					$result=msg_error($db->error);
				}
				return $result;
		}
		function actualizar_grupos(){
			$ID=isset($_REQUEST['ID'])?$_REQUEST['ID']:'';
			$Descripcion=isset($_REQUEST['Descripcion'])?$_REQUEST['Descripcion']:'';
			$sql="UPDATE Grupos SET Descripcion='%s' WHERE ID=%s";
			$sql=sprintf($sql,$Descripcion,$ID);
			$db=connectar();
			$rs=$db->queryActualiza($sql);
			if($rs !=0 ){
				$result=$rs;
			}
			else{
				$result=msg_error($db->error);
			}
			return $result;
		}
		function remover_detalles()
		{
			$ID=isset($_REQUEST['ID_detalles'])?$_REQUEST['ID_detalles']:'';
			$sql="UPDATE Detalles SET Estado_detalles='D' WHERE ID_detalles=%s";
			$sql=sprintf($sql,$ID);
			$db=connectar();
			$rs=$db->queryActualiza($sql);
			if($rs !=0 ){
				$result=$rs;
			}
			else{
				$result=msg_error($db->error);
			}
			return $result;
		}

		function remover_grupos()
		{
			$ID=isset($_REQUEST['ID'])?$_REQUEST['ID']:'';
			$sql="UPDATE Grupos SET Estado='D' WHERE ID=%s";
			$sql=sprintf($sql,$ID);
			$db=connectar();
			$rs=$db->queryActualiza($sql);
			if($rs !=0 ){
				$result=$rs;
			}
			else{
				$result=msg_error($db->error);
			}
			return $result;
		}

		function connectar()
		{
			$db = IFXDbManejador::conectarDB();
			if($db->conexionID==null){
				$cadena = $db->error;
				echo msg_error($cadena);
				exit();
			}
			return $db;
		}
?>
