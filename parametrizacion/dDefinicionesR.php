<?php
/* autor:       orlando puentes
 * fecha:       24/03/2010
 * objetivo:    
 */
header("Cache-Control: no-store, no-cache, must-revalidate");
date_default_timezone_set("America/Bogota");
setlocale(LC_ALL,"es_ES");

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once 'clases' . DIRECTORY_SEPARATOR . 'dDefinicion.class.php';

$objClase=new dDefinicion;

//Parametros de dDefinicones.php
$iddef=intval($_REQUEST['iddef']);
if(empty($iddef)){
$consulta=$objClase->mostrar_datos();
}else{
$consulta=$objClase->mostrar_datos_idd($iddef);
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Definiciones</title>
<script src="../js/jquery-1.3.2.min.js" type="text/javascript"></script>
<link href="../css/estiloReporte.css" rel="stylesheet" type="text/css" />
</head>
<body>
<table width="100%" border="0" cellspacing="0">
  <tr>
    <td width="2%">&nbsp;</td>
    <td width="5%"><img src="../imagenes/LogoSimple.gif" width="53" height="74" /></td>
    <td width="93%" align="center" valign="bottom"><img src="../imagenes/razonSocial.png" width="615" height="55" /></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="center"><label style="font-family:Tahoma, Geneva, sans-serif; color:#666; font-size:24px">Listado de Detalle Definiciones</label></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2">Fecha de impresi&oacute;n: <?php echo strftime("%B %d de %Y, hora: %r %p") ?></td>
  </tr>
</table>
<center>
<table width="80%" border="0" cellspacing="2" class="tablero">
  <tr>
    <th width="8%">ID DEFINICIION</th>
    <th width="25%">DEFINICION</th>
    <th width="9%">ID DETALLE</th>
    <th width="6%">C&oacute;digo</th>
    <th width="25%">DETALLLE DEF</th>
    <th width="27%">CONCEPTO</th>
    
  </tr>
  <?php
  while( $row = mssql_fetch_array($consulta) ){
?>	  
  <tr>
    <td align="left"><?php echo $row['iddefinicion']; ?> </td>
    <td align="left"><?php echo $row['definicion']; ?></td>
    <td align="left"><?php echo $row['iddetalledef']; ?>&nbsp;</td>
    <td align="left"><?php echo $row['codigo']; ?>&nbsp;</td>
    <td align="left"><?php echo $row['detalledefinicion']; ?></td>
    <td align="left"><?php echo $row['concepto']; ?></td>
    
  </tr>
  <?php
  }
  ?>
</table>
</center>
</body>
</html>
