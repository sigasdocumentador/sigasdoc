<?php
		/* autor:
		 * fecha:
		 * objetivo:   Servicios para homologacion
		 */
		ini_set("display_errors",'1');
		$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
		include_once  $root;

		$conR=$_SESSION['REGISTRO'];

		include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
		include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';

		$metodo=$_REQUEST['metodo'];
		switch($metodo){
			case 'listar_homologaciones':
			 	$result=listar_homologaciones();
				break;
			case 'listar_detalles':
			 	$result=listar_detalles();
				break;
			case 'agregar_homologaciones':
				$result=agregar_homologaciones();
				break;
			case 'actualizar_homologaciones':
				$result=actualizar_homologaciones();
				break;
			case 'remover_homologaciones':
				$result=remover_homologaciones();
				break;
		}

		echo json_encode($result);

		function listar_homologaciones(){
			$db=connectar();
			$sql="select H.Id,H.Descripcion,H.Dato_Sigas,D.CodigoTabla2_detalles as Dato_Homologacion_Detalles,H.Dato_Homologacion from Homologacion as H join Detalles as D on D.ID_detalles=H.Dato_Homologacion where H.Estado='A'";
			$query=$db->querySimple($sql);
			$result=$query->fetchAll();
			return $result;
		}

		function listar_detalles(){
			$db=connectar();
			$sql="select ID_detalles as ID, CodigoTabla2_detalles as Descripcion from Detalles where Estado='A'";
			$query=$db->querySimple($sql);
			$result=$query->fetchAll();
			return $result;
		}


		function agregar_homologaciones(){
			  $Descripcion=isset($_REQUEST['Descripcion'])?$_REQUEST['Descripcion']:'';
				$Dato_Sigas=isset($_REQUEST['Dato_Sigas'])?$_REQUEST['Dato_Sigas']:'';
				$Dato_Homologacion=isset($_REQUEST['Dato_Homologacion'])?$_REQUEST['Dato_Homologacion']:'';
				$sql="INSERT INTO homologacion VALUES ('%s','%s',%s,'%s')";
				$sql=sprintf($sql,$Descripcion,$Dato_Sigas,$Dato_Homologacion,'A');
				$db=connectar();
				$rs=$db->queryInsert($sql,'homologacion');

				if($rs !=0 ){
					$result=$rs;
				}
				else{
					$result=msg_error($db->error);
				}
				return $result;
		}
		function actualizar_homologaciones(){
				$Descripcion=isset($_REQUEST['Descripcion'])?$_REQUEST['Descripcion']:'';
				$Dato_Sigas=isset($_REQUEST['Dato_Sigas'])?$_REQUEST['Dato_Sigas']:'';
				$Dato_Homologacion=isset($_REQUEST['Dato_Homologacion'])?$_REQUEST['Dato_Homologacion']:'';
				$Id_homologacion=isset($_REQUEST['Id'])?$_REQUEST['Id']:'';
				$sql="UPDATE homologacion SET Descripcion='%s',Dato_Sigas='%s',Dato_Homologacion=%s WHERE Id=%s";
				$sql=sprintf($sql,$Descripcion,$Dato_Sigas,$Dato_Homologacion,$Id_homologacion);
				$db=connectar();
				$rs=$db->queryActualiza($sql);
				if($rs !=0 ){
					$result=$rs;
				}
				else{
					$result=msg_error($db->error);
				}
				return $result;
		}
		function remover_homologaciones()
		{
			$Id=isset($_REQUEST['Id'])?$_REQUEST['Id']:'';
			$sql="UPDATE homologacion SET Estado='D' WHERE Id=%s";
			$sql=sprintf($sql,$Id);
			$db=connectar();
			$rs=$db->queryActualiza($sql);
			if($rs !=0 ){
				$result=$rs;
			}
			else{
				$result=msg_error($db->error);
			}
			return $result;
		}

		function connectar()
		{
			$db = IFXDbManejador::conectarDB();
			if($db->conexionID==null){
				$cadena = $db->error;
				echo msg_error($cadena);
				exit();
			}
			return $db;
		}
?>
