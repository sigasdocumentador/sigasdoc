<?php
/* autor:       orlando puentes
* fecha:       24/09/2010
* objetivo:
*/
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';

$url=$_SERVER['PHP_SELF'];
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'auditoria.php';
//include_once 'phpComunes/auditoria.php';
auditar($url);

$_SESSION['REGISTRO']=0;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<title>Agencias</title>
		<!--<link href="../../css/Estilos.css" rel="stylesheet" type="text/css" />
        <link href="../../css/marco.css" rel="stylesheet" type="text/css" />
		<script language="javascript" src="../../js/ui/jquery-1.4.2.js"></script>
		<script language="javascript" src="../../js/comunes.js"></script>-->

		<link type="text/css" href="<?php echo URL_PORTAL; ?>newcss/Estilos.css" rel="stylesheet">
		<link type="text/css" href="<?php echo URL_PORTAL; ?>newcss/css.1.8/jquery-ui-1.8.17.custom.css" rel="stylesheet">
		<link type="text/css" href="<?php echo URL_PORTAL; ?>newcss/marco.css" rel="stylesheet">

		<?php /*<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/1.6/jquery-1.6.2.min.js"></script>*/
		?>
		<script src="<?php echo URL_PORTAL; ?>newjs/jquery-1.9.1.js"></script>
    <script src="<?php echo URL_PORTAL; ?>newjs/jquery-migrate-1.1.0.js"></script>
		<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/angular.min.js"></script>
		<script src="<?php echo URL_PORTAL; ?>newjs//ui-bootstrap-tpls-0.11.0.js"></script>

		<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/1.6/jquery-ui-1.8.16.custom.min.js"></script>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" integrity="sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous">
		<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js" integrity="sha512-K1qjQ+NcF2TYO/eI3M6v8EiNYZfA95pQumfvcVrTHtwQVDG+aHRqLi/ETn2uB+1JqwYqVG3LIvdm9lj6imS/pQ==" crossorigin="anonymous"></script>
		<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/comunes.js"></script>
		<script language="javascript" src="js/detalles.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
		<link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet"/>
	</head>
	<body ng-app='DetallesApp'>
			<div ng-controller='mainController'>
					<div class="panel panel-default">
						<div class="panel-heading">
							Grupos
						</div>
						<div class="panel-body">
							<button ng-click="agregar_grupo()">
								<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
								Agregar nuevo Grupo
							</button>
							<br/><br/>
							<ul class="list-group">
								<li class="list-group-item" ng-repeat='grupo in grupos'>
									{{grupo.CodigoTabla1}} | {{grupo.Descripcion}}
									<span class="glyphicon glyphicon-pencil" ng-click="editar_grupo(grupo)" aria-hidden="true"></span>
									<span class="glyphicon glyphicon-remove" ng-click="remover_grupo(grupo)" aria-hidden="true"></span>
								</li>
							</ul>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							Detalles
						</div>
						<div class="panel-body">
							<button ng-click="agregar_detalle()">
								<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
								Agregar nuevo Detalle
							</button>
							<table class="table">
								<thead>
									<tr>
										<th>Id</th><th>Grupo</th>
										<th>Descripcion 1</th>
										<th>Descripcion 2</th>
										<th>Codigo Detalles</th>
										<th>Opciones</th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat='detalle in detalles.slice(((currentPage-1)*itemsPerPage), ((currentPage)*itemsPerPage))'>
										<td>{{detalle.ID_detalles}}</td>
										<td>{{detalle.Grupo}}</td>
										<td>{{detalle.Descripcion1_detalles}}</td>
										<td>{{detalle.Descripcion2_detalles}}</td>
										<td>{{detalle.CodigoTabla2_detalles}}</td>
										<td>
											  <span class="glyphicon glyphicon-pencil" ng-click="editar_detalle(detalle)" aria-hidden="true"></span>
											  <span class="glyphicon glyphicon-remove" ng-click="remover_detalle(detalle)" aria-hidden="true"></span>
										</td>
									</tr>
								</tbody>
							</table>
							Ver <select ng-model="viewby" ng-change="setItemsPerPage(viewby)"><option>10</option><option>20</option><option>30</option><option>40</option><option>50</option></select> registros
							<pagination total-items="totalItems" ng-model="currentPage" max-size="maxSize" class="pagination-sm" boundary-links="true" items-per-page="itemsPerPage"></pagination>

						</div>
					</div>
			</div>
			<script type="text/ng-template" id="myModalContent1.html">
              <div class="modal-header">
                  <h3 class="modal-title">{{titulo}}</h3>
              </div>
              <div class="modal-body">
									Descripcion 1: <input type="text" ng-model="element.Descripcion1_detalles"><br/>
									Descripcion 2: <input type="text" ng-model="element.Descripcion2_detalles"><br/>
									Codigo Detalles:<input type="text" ng-model="element.CodigoTabla2_detalles"><br/>
									Grupo: <select name="mySelect" id="mySelect"
										      ng-options="option.Descripcion for option in data.availableOptions track by option.ID"
										      ng-model="data.selectedOption">
													</select>
              </div>
              <div class="modal-footer">
                  <button class="btn btn-primary" type="button" ng-click="ok()">Guardar</button>
                  <button class="btn btn-warning" type="button" ng-click="cancel()">Cancelar</button>
              </div>
      </script>
			<script type="text/ng-template" id="myModalContent2.html">
              <div class="modal-header">
                  <h3 class="modal-title">{{titulo}}</h3>
              </div>
              <div class="modal-body">
									Descripcion: <input type="text" ng-model="element.Descripcion"><br/>
							</div>
              <div class="modal-footer">
                  <button class="btn btn-primary" type="button" ng-click="ok()">Guardar</button>
                  <button class="btn btn-warning" type="button" ng-click="cancel()">Cancelar</button>
              </div>
      </script>
    </body>
</html>
