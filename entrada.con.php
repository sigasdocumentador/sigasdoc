<?php

/* @autor:		Orlando Puentes A. 
 * 
 * 
 */
//$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
//include_once  $root;
include_once $raiz.DIRECTORY_SEPARATOR ."config.php";

date_default_timezone_set('America/Bogota'); 
if ( !isset( $_SESSION ['USUARIO'] ) ) {
	session_start();
	if ( !isset( $_SESSION ['USUARIO'] ) ) {
		$ip=$_SERVER['SERVER_NAME'];
		$url="http://".$ip."/sigas/error.php";
		header("Location: $url");
	}
	$raiz=$_SESSION['RAIZ'];
	include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'session.php';
	}

include_once("config.php");
session();

$_SESSION['RAIZ']=$raiz;
$_SESSION['IMAGENES']=$imagenes;
$_SESSION['RUTAMOVI']=$ruta_movimientos;
$_SESSION['url']=$url;
ini_set("display_errors",'1');
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
$idrol=$_SESSION['IDROL'];
$sql="select rol from aportes516 where idrol=$idrol";
$rs=$db->querySimple($sql);
$row=$rs->fetch();
$rol=$row['rol'];
if($idrol==1){
	$sql0="SELECT idformulario, idmodulo, orden, formulario, url, submenu, destino, idmenu FROM aportes508 WHERE idformulario NOT IN (4,2,13,14,15,171,172,12,9,6,7,8,10,11,5,1) ORDER BY orden";
	$sql1="SELECT idmenu, orden,menu FROM aportes510";
	
	}
else{
	  $sql0="SELECT aportes508.idformulario, aportes508.idmodulo, orden, formulario, url, aportes508.submenu, destino, aportes508.idmenu FROM aportes508 INNER JOIN aportes517 ON aportes508.idformulario=aportes517.idformulario WHERE idrol=$idrol ORDER BY orden";
	 $sql1="SELECT DISTINCT aportes510.idmenu,orden,menu FROM aportes517 INNER JOIN aportes510 ON aportes517.idmenu=aportes510.idmenu AND idrol=$idrol";
} 
$rsFrm = $db->querySimple($sql0);
$rsMnu=$db->querySimple($sql1);

//versi�n del aplicativo
$sqlVersion = "SELECT TOP 1 idversion,version,notas,usuario,fechasistema FROM aportes520 where version!='BETA' ORDER BY version DESC";
$rsVersion = $db->querySimple($sqlVersion);
$rowVersion = $rsVersion->fetch();
$version = $rowVersion["version"];
$anno=$mes=$dia="";
list($anno,$mes,$dia) = explode("-",$rowVersion["fechasistema"]);
$fechaVersion = mes($mes)." ".$dia." del ".$anno;
// mes en letras
function mes($num){
	/**
	 * Creamos un array con los meses disponibles.
	 * Agregamos un valor cualquiera al comienzo del array para que los n�meros coincidan
	 * con el valor tradicional del mes. El valor "Error" resultar� �til
	 **/
	$meses = array('Error', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
			'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');

	/**
	 * Si el n�mero ingresado est� entre 1 y 12 asignar la parte entera.
	 * De lo contrario asignar "0"
	 **/
	$num_limpio = $num >= 1 && $num <= 12 ? intval($num) : 0;
	return $meses[$num_limpio];
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Comfamiliar - S.I.G.A.S.</title>


<link type="text/css" href="css/css.1.8/jquery-ui-1.8.17.custom.css" rel="stylesheet"/>
<link href="css/jMenu.jquery.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery-1.4.2.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/jMenu.jquery.js"></script>
<script type="text/javascript" src="js/shortcut.js"></script>
<script type="text/javascript" src="js/comunes.js"></script>
<script type="text/javascript" src="config.js"></script>

<!-- SCRIPT PARA EL PLUGIN DE LAS NOTIFICACIONES -->
<link type="text/css" href="notificacion/noti-okr/css/noti-okr.1.css" rel="stylesheet">
<!-- <script type="text/javascript" src="newjs/1.6/jquery-1.6.2.min.js"></script>-->
<script type="text/javascript" src="notificacion/noti-okr/js/noti-okr.1.js"></script>
<script type="text/javascript" src="notificacion/js/viewNotificationHome.js"></script>

<script>
	var alto = screen.height -300;
	var ancho = screen.width  ;
	document.write("<style>.divScreen{height: "+alto+"px; width: "+ancho+"px; scrolling=\"auto\";}</style>");
</script>
<script type="text/javascript">
	shortcut.add("Shift+F",function() {
		var URL=src();
		var url=URL+"aportes/trabajadores/consultaTrabajador.php";
    	window.open(url,"_blank");
    },{
	'propagate' : true,
	'target' : document 
        });
    
$(document).ready(function(){
	$("#jMenu").jMenu();
	
});
function despedida(){
	$.ajax({
		url: 'despedida.php',
		type: "POST",
		success: function (datos){
			alert("La Session fue cerrada!");	
			$("#sesionInfo").css("display", "none");
			$("#jMenu").remove(); 
		}
		});
}
ejecutarCron();
/**
 * Metodo para ejecutar el cron
 */
function ejecutarCron(){
	$.ajax({
		url: 'cron/cron.php',
		type: "POST",
		async:true,
		/*beforeSend: function(objeto){
			dialogLoading('show');
		},
		complete: function(objeto, exito){
			dialogLoading('close');
		},*/ 
		success: function (datos){
			if(datos!=1){
				alert("Ha ocurrido un error interno 'Cron', favor reportar a T.I.");
			}	 
		}
	});
}
</script>
<script type="text/javascript">
			$(document).ready(function(){
				/*
				$(window).unload(function(){
					alert('siii2222');
					});
				*/	
				if($.browser.mozilla){
				}
				else {
					alert("SE�OR USUARIO:\n\nEstas usando un navegador distinto a 'MOZILLA FIREFOX',por favor\nrecomendamos usar dicho navegador por comodidad y mejoras del\naplicativo Comunquese lo mas pronto posible con el Jefe de\nmantenimiento para instalarlo.\n\nAgradecemos su colaboracin.");
				}
			});

			//dialog acerca de sigas 
			$(function() {
				$("#acercaDeSigas").dialog({
					modal: true,
					 autoOpen: false,
					 height: 310, 
					 minHeight: 300,
			   		 maxHeight: 300,
					 width: 500, 
					 minWidth: 500,
		   			 maxWidth: 500,
	   				 show: "clip",
				     hide: "explode"
					 
					 });
			});  
			
			
</script>
<style  type="text/css">
			
			#sesionInfo{
				width:200px;
				font:10px Verdana, Geneva, sans-serif;
				color:#FFF;
				position:absolute;
				top:10px;
				left:80%;
			}
			#sesionInfo a{
				color:#eaeaea;
				text-decoration:none;
				font-size:11px
			}	
			#sesionInfo a:hover{
				text-decoration:underline}	
			
			/*styles del dialog acerca de sigas --begin*/
			#titulo{
				color: #fff;
				font-size: 32px;
				font-family:"Arial Black", Gadget, sans-serif;
				font-style: italic;
				text-shadow: 1px 2px 3px #03F;
				font-weight: bold;
			}
			#version{
				font-size:14px;
				font-style: italic;
				color: #333;
				text-align: center;
				font-weight: bold;
			}
			#piepagina{
				width:100%;
				height:50px;
				font-size:12px;
				color: #999;
				text-align: center;
				font-family: Arial, Helvetica, sans-serif;
				background:#EAEAEA
			}
			#informacion{
				font-size:12px;
				font-weight: bold;
				font-style: italic;
				text-align: center;
			}
			/*--end*/
</style>
</head>
    
<body  style="margin:0" onunload="cierre();"  >

<!-- ELEMENTOS OCULTOS -->
<input type="hidden" name="hidIdUsuario" id="hidIdUsuario" value="<?php echo $_SESSION["ID_USUARIO"];?>"/>


<div style="background:url(imagenes/pag_02.png); position:relative" >
<div id="sesionInfo">
	Hoy es: <?php echo strftime("%B %d de %Y") ?> <br /><br />
	<img src="imagenes/worker.png" width="16" height="16" alt="Usuario" align="absmiddle" />:<?php echo $_SESSION['USSER']." ".$_SESSION['CEDULA'].
	"<br /><img src='imagenes/agencia.png' width='16' height='16' alt='Usuario' align='absmiddle' />: ".$_SESSION['AGENCIA']." - ".$rol; ?>
	<div id="div-button-perfil">
		<div id="noti"></div>
	</div>
	<p align="center">
		[<a href="despedida.php" target="_self"> Salir </a>]
	</p>
	
</div>
<div> <img src="imagenes/logo.png" style="margin-left:20px" />
	<!-- end #header --> 
</div>
<!-- menu -->
<ul id="jMenu" title="Modulos SIGAS" alt="Menu"> 
<!-- INICIO menu -->
<!-- PRINCIPAL -->
<li rel="tile_002" class="menu">
	<a target="myFrame" class="fNiv" href="empty.php" style="text-decoration:none; padding:0; margin:0 5px;">
	<img src="imagenes/home.png" width="16" height="16" alt="P�gina Inicio" style="border:none; padding:0; margin:0; vertical-align:middle" />
	</a>
</li> 
<!-- MENU PARA LAS NOTIFICACIONES -->
<!--<li id="noti">
	 <span id="notification_count" class="parpadeo">5</span>
	<a href="#" id="notificationLink">
		<img src="imagenes/root.gif" width="16" height="16" style="border:none; padding:0; margin:0; vertical-align:middle" />
	</a>
	<div id="notificationContainer">
		<div id="notificationTitle">Notificaciones</div>
		<div id="notificationsBody" class="notifications">
			1. hola<br/>
			2. bien<br/>
		</div>
		<div id="notificationFooter">
			<a href="#">Ver Todas</a>
		</div>
	</div>
</li>-->


<strong></strong>
<?php 
$rsMnu=$db->querySimple($sql1);
while ($row=$rsMnu->fetch()){
	$orden=$row['orden'];
	$menu=$row['menu'];
//aportes y subsidio 100
	if($orden==100){
?>
	<li rel="tile_002" class="menu"><a target="myFrame" class="fNiv">Aportes y Subsidio</a>
		<ul id="tile_002"  title="Aportes y Subsidio" alt="Menu">
			<li rel="tile_020" ><a target="myFrame">Radicaci&oacute;n</a>
				<ul id="tile_020"  title="Radicación" alt="SIGAS" sigas>
                <?php 
				//1 radicacion
				$rsFrm = $db->querySimple($sql0);
				while ($form=$rsFrm->fetch()){
					if ($form['idmenu']==1){
						echo "<li><a href=". $form['url']." target=". $form['destino'] .">". $form['submenu'] ."</a></li>";
					}
				}
				?>		
				</ul>
			</li>
			<li rel="tile_022" sigas><a target="myFrame">Empresa</a>
				<ul id="tile_022"  title="Empresa" alt="SIGAS">
				 <?php 
				//2 empresa
				$rsFrm = $db->querySimple($sql0);
				while ($form=$rsFrm->fetch()){
					if ($form['idmenu']==2){
						echo "<li><a href=". $form['url']." target=". $form['destino'] .">". $form['submenu'] ."</a></li>";
					}
				}
				?>	
				</ul>
			</li>
			<li rel="tile_024" sigas><a target="myFrame">Liquidaci�n Contrato</a>
				<ul id="tile_024"  title="Empresas Contrato" alt="SIGAS">
				 <?php 
				//4 empresas contratistas
				$rsFrm = $db->querySimple($sql0);
				while ($form=$rsFrm->fetch()){
					if ($form['idmenu']==4){
						echo "<li><a href=". $form['url']." target=". $form['destino'] .">". $form['submenu'] ."</a></li>";
					}
				}
				?>	
								
				</ul>
			</li>
			
			<li rel="tile_026" sigas><a target="myFrame">Afiliados - Personas</a>
				<ul id="tile_026"  title="Trabajador" alt="SIGAS">
				 <?php 
				//5 afiliaciones 
				$rsFrm = $db->querySimple($sql0);
				while ($form=$rsFrm->fetch()){
					if ($form['idmenu']==5){
						echo "<li><a href=". $form['url']." target=". $form['destino'] .">". $form['submenu'] ."</a></li>";
					}
				}
				?>	
				</ul>
			</li>
			<li rel="tile_028" sigas><a target="myFrame">Aportes</a>
				<ul id="tile_028"  title="Aportes" alt="SIGAS">
				<?php 
				//6 aportes 
				$rsFrm = $db->querySimple($sql0);
				while ($form=$rsFrm->fetch()){
					if ($form['idmenu']==6){
						echo "<li><a href=". $form['url']." target=". $form['destino'] .">". $form['submenu'] ."</a></li>";
					}
				}
				?>	
                <li><a href="../planos_cargados/index.php" target="myFrame">Explorador de Archivos</a></li>			
				</ul>
			</li>
			<li rel="tile_030" sigas><a target="myFrame">Planilla Unica</a>
				<ul id="tile_030"  title="Planilla Unica" alt="SIGAS">
				<?php 
				//3 planilla unica
				$rsFrm = $db->querySimple($sql0);
				while ($form=$rsFrm->fetch()){
					if ($form['idmenu']==3){
						echo "<li><a href=". $form['url']." target=". $form['destino'] .">". $form['submenu'] ."</a></li>";
					}
				}
				?>		
                 <li><a href="../planos_cargados/index.php" target="myFrame">Explorador de Archivos</a></li>
                 <li><a href="<?php echo $ruta_reportes; ?>aportes/planilla/rptExcel006.jsp?v0=<?php echo $_SESSION ['USUARIO']; ?>" target="myFrame">Informe Trabajadores Sin Relacion PU</a></li>						
				</ul>
			</li>
			<li rel="tile_032" sigas><a target="myFrame">N&oacute;mina Manual</a>
				<ul id="tile_032"  title="N&oacute;mina Manual" alt="SIGAS">
				<?php 
				//7 nomina manual
				$rsFrm = $db->querySimple($sql0);
				while ($form=$rsFrm->fetch()){
					if ($form['idmenu']==7){
						echo "<li><a href=". $form['url']." target=". $form['destino'] .">". $form['submenu'] ."</a></li>";
					}
				}
				?>	
				<!--				<li><a href="aportes/nominaManual/nominaManual.php" target="myFrame">Generar</a></li>
								<li><a href="aportes/nominaManual/consultaNomina.php" target="myFrame">Consultar</a></li>
								<li><a href="aportes/nominaManual/menuReportes.html" target="myFrame">Informes</a></li> -->
				</ul>
			</li>
			<li rel="tile_034" sigas><a target="myFrame">Subsidio Cuota Monetaria</a>
				<ul id="tile_034"  title="Subsidio Cuota Monetaria" alt="SIGAS">
				<?php 
				//8 cuota monetaria
				$rsFrm = $db->querySimple($sql0);
				while ($form=$rsFrm->fetch()){
					if ($form['idmenu']==8){
						echo "<li><a href=". $form['url']." target=". $form['destino'] .">". $form['submenu'] ."</a></li>";
					}
				}
				?>	
				
				</ul>
			</li>
			<!-- aqui subsidio en especie  -->
			<li rel="tile_050" sigas><a target="myFrame">Subsidio en Especie</a>
				<ul id="tile_050"  title="Otros Procesos" alt="SIGAS">
					<li><a href="centroReportes/paquete/rptMenuPaquete.php" target="myFrame">Informes</a></li>
				</ul>	
			</li>
			
			<li rel="tile_038" sigas><a target="myFrame">Procesos Pignoraci&oacute;n</a>
				<ul id="tile_038"  title="Procesos Pignoraci�n" alt="SIGAS">
				<?php 
				//10 proceso pignoracion
				$rsFrm = $db->querySimple($sql0);
				while ($form=$rsFrm->fetch()){
					if ($form['idmenu']==10){
						echo "<li><a href=". $form['url']." target=". $form['destino'] .">". $form['submenu'] ."</a></li>";
					}
				}
				?>	
				</ul>
			</li>
			<li rel="tile_040" sigas><a target="myFrame">Secretar&iacute;a</a>
				<ul id="tile_040"  title="Secretaria" alt="SIGAS">
				<?php 
				//11 secretaria
				$rsFrm = $db->querySimple($sql0);
				while ($form=$rsFrm->fetch()){
					if ($form['idmenu']==11){
						echo "<li><a href=". $form['url']." target=". $form['destino'] .">". $form['submenu'] ."</a></li>";
					}
				}
				?>	
				</ul>
			</li>
			<li rel="tile_043" sigas><a target="myFrame">Contabilidad</a>
				<ul id="tile_043"  title="Contabilidad" alt="SIGAS">
				<?php 
				//12 contabilidad
				$rsFrm = $db->querySimple($sql0);
				while ($form=$rsFrm->fetch()){
					if ($form['idmenu']==12){
						echo "<li><a href=". $form['url']." target=". $form['destino'] .">". $form['submenu'] ."</a></li>";
					}
				}
				?>									
				</ul>
			</li>
			<li rel="tile_044" sigas><a target="myFrame">Revisi&oacute;n Doctos Grabados</a>
				<ul id="tile_044"  title="Revisión Doctos Grabados" alt="SIGAS">
				<?php 
				//13 revision documentos
				$rsFrm = $db->querySimple($sql0);
				while ($form=$rsFrm->fetch()){
					if ($form['idmenu']==13){
						echo "<li><a href=". $form['url']." target=". $form['destino'] .">". $form['submenu'] ."</a></li>";
					}
				}
				?>		
<!--					<li><a href="aportes/revision/revision.php" target="myFrame">Trabajadores</a></li>
								<li><a href="aportes/revision/revisionEmpresa.php" target="myFrame">Empresa</a></li>
								<li><a href="aportes/revision/menuReportes.php" target="myFrame">Informes</a></li>  -->
				</ul>
			</li>			
			<li rel="tile_046" sigas><a target="myFrame">Digitalizaci&oacute;n</a>
				<ul id="tile_046"  title="Digitalizaci&oacute;n" alt="SIGAS">
                <?php 
				//14 digitalizacion
				$rsFrm = $db->querySimple($sql0);
				while ($form=$rsFrm->fetch()){
					if ($form['idmenu']==14){
						echo "<li><a href=". $form['url']." target=". $form['destino'] .">". $form['submenu'] ."</a></li>";
					}
				}
				?>		
				</ul>
			</li>
			<li rel="tile_047" sigas><a target="myFrame">Subsidio Escolar</a>
				<ul id="tile_047"  title="Subsidio Escolar" alt="SIGAS">
                <?php 
				//64 Subsidio Escolar
				$rsFrm = $db->querySimple($sql0);
				while ($form=$rsFrm->fetch()){
					if ($form['idmenu']==64){
						echo "<li><a href=". $form['url']." target=". $form['destino'] .">". $form['submenu'] ."</a></li>";
					}
				}
				
	                
				?>	
					<!-- <li><a href="aportes/bonoEducativo/generarSabana.php" target="myFrame">Generar Sabana</a></li>
					<li><a href="aportes/bonoEducativo/consulta/consultarBonos.php" target="myFrame"> <span>Consulta Susidio Educativo</span></a></li>
			        <li><a href="aportes/bonoEducativo/reclamos/afiliados/reclamosAfiliado.php" target="myFrame"> <span>Reclamo Afiliado</span></a></li>
	                <li><a href="aportes/bonoEducativo/reclamos/empresa/reclamoEmpresa.php" target="myFrame"> <span>Reclamo Empresa</span></a></li>
	                <li><a href="aportes/bonoEducativo/reclamos/suspender/suspenderPaquete.php" target="myFrame"> <span>Suspender Subsidio</span></a></li>
	                <li><a href="centroReportes/bono/rptMenuBono.php" target="myFrame"><span>Informes</span></a></li>-->
			           
				</ul>
			</li>
			<li rel="tile_047" sigas><a target="myFrame">Resolucion 1056</a>
				<ul id="tile_047"  title="Resolucion 1056" alt="SIGAS">
                <?php 
				//65 Resolucion 1056
				$rsFrm = $db->querySimple($sql0);
				while ($form=$rsFrm->fetch()){
					if ($form['idmenu']==65){
						echo "<li><a href=". $form['url']." target=". $form['destino'] .">". $form['submenu'] ."</a></li>";
					}
				}
				
	                
				?>	
							           
				</ul>
			</li>
			<li rel="tile_050" sigas><a target="myFrame">Otros Procesos</a>
				<ul id="tile_050"  title="Otros Procesos" alt="SIGAS">
					<li><a href="phpComunes/observaciones.php" target="myFrame">Observaciones</a></li>
				</ul>	
			</li>

<!-- fin  -->	
		</ul>
	</li>
				
<?php 		
	}//fin aportes
//tarjeta 
	if($orden==300){
?>
<!--asopagos -->                  
	<li rel="tile_003" class="menu"><a target="myFrame" class="fNiv">ASOPAGOS</a>
		<ul id="tile_003"  title="Tarjetas" alt="Menu">
			<li rel="tile_300" sigas><a target="myFrame">Tarjetas</a>
				<ul id="tile_300"  title="Cargar Plano" alt="SIGAS">
                <?php 
				//16 ASOPAGOS - TARJETAS
				$rsFrm = $db->querySimple($sql0);
				while ($form=$rsFrm->fetch()){
					if ($form['idmenu']==16){
						echo "<li><a href=". $form['url']." target=". $form['destino'] .">". $form['submenu'] ."</a></li>";
					}
				}
				?>					
				</ul>
			</li>
            <li rel="tile_003" sigas><a target="myFrame">Novedades TARJETA</a>
				<ul id="tile_305"  title="Novedades de la tarjeta" alt="SIGAS">
                <?php 
				//17 NOVEDADES TARJETAS
				$rsFrm = $db->querySimple($sql0);
				while ($form=$rsFrm->fetch()){
					if ($form['idmenu']==17){
						echo "<li><a href=". $form['url']." target=". $form['destino'] .">". $form['submenu'] ."</a></li>";
					}
				}
				?>			
				</ul>
			</li>	
			<li rel="tile_003" sigas><a target="myFrame">Novedades NO Monetarias</a>
				<ul id="tile_310"  title="Novedades no monetarias" alt="SIGAS">
                <?php 
				//18 NOVEDADES NO MONETARIAS
				$rsFrm = $db->querySimple($sql0);
				while ($form=$rsFrm->fetch()){
					if ($form['idmenu']==18){
						echo "<li><a href=". $form['url']." target=". $form['destino'] .">". $form['submenu'] ."</a></li>";
					}
				}
				?>			                
				</ul>
			</li>	
            <li rel="tile_003" sigas><a target="myFrame">Novedades Monetarias</a>
				<ul id="tile_320"  title="Novedades monetarias" alt="SIGAS">
                <?php 
				//19 NOVEDADES MONETARIAS
				$rsFrm = $db->querySimple($sql0);
				while ($form=$rsFrm->fetch()){
					if ($form['idmenu']==19){
						echo "<li><a href=". $form['url']." target=". $form['destino'] .">". $form['submenu'] ."</a></li>";
					}
				}
				?>			
				</ul>
			</li>	
            <li rel="tile_003" sigas><a target="myFrame">Procesos</a>
				<ul id="tile_330"  title="Procesos" alt="SIGAS">
                <?php 
				//20 PROCESOS
				$rsFrm = $db->querySimple($sql0);
				while ($form=$rsFrm->fetch()){
					if ($form['idmenu']==20){
						echo "<li><a href=". $form['url']." target=". $form['destino'] .">". $form['submenu'] ."</a></li>";
					}
				}
				?>			
				</ul>
			</li>	
			<li rel="tile_003" sigas><a target="myFrame">Conciliacion Comercios</a>
				<ul id="tile_340"  title="Conciliacion Comercios" alt="SIGAS">
                <?php 
				//63 Conciliacion Comercios
				$rsFrm = $db->querySimple($sql0);
				while ($form=$rsFrm->fetch()){
					if ($form['idmenu']==63){
						echo "<li><a href=". $form['url']." target=". $form['destino'] .">". $form['submenu'] ."</a></li>";
					}
				}
				?>		
				
				</ul>
			</li>
             <li rel="tile_003" sigas><a target="myFrame">Inventario</a>
				<ul id="tile_340"  title="Inventario" alt="SIGAS">
                <?php 
				//21 INVENTARIO
				$rsFrm = $db->querySimple($sql0);
				while ($form=$rsFrm->fetch()){
					if ($form['idmenu']==21){
						echo "<li><a href=". $form['url']." target=". $form['destino'] .">". $form['submenu'] ."</a></li>";
					}
				}
				?>		
				<li><a href="asopagos/inventario/despachoAgencias.php" target="myFrame">Envios Agencias</a>
				<li><a href="asopagos/inventario/recibidoAgencias.php" target="myFrame">Recibido Agencias</a>
                <li><a href="asopagos/inventario/inventarioAgencias.php" target="myFrame">Inventario Agencias</a>
				</ul>
			</li>	
			<li rel="tile_003" sigas><a target="myFrame">Cruce</a>
				<ul id="tile_340"  title="Inventario" alt="SIGAS">
                <?php 
				//23 cruce
				$rsFrm = $db->querySimple($sql0);
				while ($form=$rsFrm->fetch()){
					if ($form['idmenu']==23){
						echo "<li><a href=". $form['url']." target=". $form['destino'] .">". $form['submenu'] ."</a></li>";
					}
				}
				?>		
				
				</ul>
			</li>	
            <li rel="tile_004" sigas><a target="myFrame">Informes</a>
				<ul id="tile_340"  title="Inventario" alt="SIGAS">
            <?php 
				//24 INFORMES
				$rsFrm = $db->querySimple($sql0);
				while ($form=$rsFrm->fetch()){
					if ($form['idmenu']==24){
						echo "<li><a href=". $form['url']." target=". $form['destino'] .">". $form['submenu'] ."</a></li>";
					}
				}
				?>			
             <!-- <li rel="tile_003" sigas><a href="centroReportes/asopagos/menuRptTesoreria.php" target="myFrame">Informes Tesoreria</a>  -->
			</ul>
		</li> 
        </ul>
        </li>                    
<?php	
	}//fin tarjeta	
//fonede
	if ($orden==500){
?>
		<!--
		<li rel="tile_004" class="menu"><a target="myFrame" class="fNiv">FONEDE</a>
		 
					<ul id="tile_004"  title="FONEDE" alt="Menu">
						<li rel="tile_400" sigas><a target="myFrame">Postulaciones</a>
							<ul id="tile_400"  title="Afiliaci&oacute;n" alt="SIGAS">
							<li><a href="fonede/grabacion/MenuGrabacion.php" target="myFrame">Nueva</a></li>
                            <li><a href="fonede/postulacion/procesopostulacion.php" target="myFrame">Proceso de Postulaci&oacute;n</a></li>
                            <li><a href="fonede/consulta/ConsultaPostulado.php" target="myFrame">Consultar Postulado</a></li>
                            								
							</ul>
						</li> 
						<li rel="tile_410" sigas><a target="myFrame">Asignaci&oacute;n</a>
							<ul id="tile_410"  title="Asignaci&oacute;n" alt="SIGAS">
                                <li><a href="fonede/asignacion/ListarAsignacion.php" target="myFrame">Realizar Asignaci&oacute;n</a></li>                                
							</ul>
						</li>
						<li rel="tile_420" sigas><a target="myFrame">Giro</a>
							<ul id="tile_420"  title="Giro" alt="SIGAS">
								<li><a href="fonede/giro/Giro.php" target="myFrame">Realizar Giro</a></li>
								<li><a href="fonede/giro/RevertirGiro.php" target="myFrame">Reversar Giro</a></li>	
								<li><a href="fonede/giro/procesopasarhistorico.php" target="myFrame">Pasar Historico</a></li>								
							</ul>
						</li>
                       	<li rel="tile_420" sigas><a target="myFrame">Administraci&oacute;n</a>
							<ul id="tile_420"  title="Giro" alt="SIGAS">
							  <li><a href="fonede/administracion/Apropiaciones.php" target="myFrame">Realizar Apropiaciones</a></li>	
                              <li><a href="fonede/asignacion/GenerarAsignacion.php" target="myFrame">Generar Asignaci&oacute;n</a></li>
							  <li><a href="fonede/administracion/SubirArchivo/subirarchivo.php" target="myFrame">Subir Archivo</a></li>
							  <li><a href="fonede/administracion/fosyga.php" target="myFrame">Registrar Fosyga</a></li>
							  <li><a href="fonede/administracion/Convenio.php" target="myFrame">Registrar Convenio</a></li>
							  <li><a href="fonede/administracion/CursosAdmin.php" target="myFrame">Seleccionar Cursos</a></li>
							</ul>
						</li>
                        <li rel="tile_420" sigas><a target="myFrame">Reportes</a>
							<ul id="tile_420"  title="Giro" alt="SIGAS">
							   <li><a href="fonede/postulacion/Postulacion.php" target="myFrame">Listar Postulados </a></li>
							   <!--<li><a href="fonede/asignacion/ListarAsignacion.php" target="myFrame">Listar Asignados</a></li>
                               <li><a href="fonede/reportes/cartadeasignacion.php" target="myFrame">Generar Carta de Asignaci&oacute;n</a></li>							   
							   <li><a href="fonede/reportes/cartadeinvitacion.php" target="myFrame">Generar Carta de Invitaci&oacute;n</a></li>
							   <li><a href="fonede/reportes/EjeFonede.php" target="myFrame">Generar Sociodemografico</a></li>
							   <li><a href="fonede/reportes/Convenios.php" target="myFrame">Listar Postulados por Convenio</a></li>
							   <li><a href="fonede/reportes/Estados.php" target="myFrame">Listar Postulados por Estados</a></li>
							   <li><a href="fonede/reportes/Cursos.php" target="myFrame">Listar Postulados por Cursos</a></li>
                               <li><a href="fonede/reportes/Eps.php" target="myFrame">Listar Postulados por Eps</a></li>
                               <li><a href="fonede/reportes/EntidadEducativa.php" target="myFrame">Listar Postulados por Entidad Educativa</a></li>
							</ul>
						</li>
					</ul> 
				
                </li>
                 -->
<?php 		
	}//fin fonede	
//promotoria
	if ($orden==700){
?>
		<li rel="tile_005" class="menu"><a target="myFrame" class="fNiv">Promotor&iacute;a</a>
					<ul id="tile_005"  title="Promotor&iacute;a Empresarial" alt="Menu">
						<li rel="tile_500" sigas><a target="myFrame">Asesores</a>
							<ul id="tile_500"  title="Asesores" alt="SIGAS">
								<?php 
									//16 ASOPAGOS - TARJETAS
									$rsFrm = $db->querySimple($sql0);
									while ($form=$rsFrm->fetch()){
										if ($form['idmenu']==29){
											echo "<li><a href=". $form['url']." target=". $form['destino'] .">". $form['submenu'] ."</a></li>";
										}
									}
								?>
							<!-- <ul id="tile_500"  title="Asesores" alt="SIGAS">
								<li><a href="promotoria/nuevoAsesor.php" target="myFrame">Administraci&oacute;n</a></li>-->
							</ul>
						</li>
						<li rel="tile_510" sigas><a target="myFrame">Promotores</a>
							<ul id="tile_510"  title="Promotores" alt="SIGAS">
								<?php 
									//16 ASOPAGOS - TARJETAS
									$rsFrm = $db->querySimple($sql0);
									while ($form=$rsFrm->fetch()){
										if ($form['idmenu']==30){
											echo "<li><a href=". $form['url']." target=". $form['destino'] .">". $form['submenu'] ."</a></li>";
										}
									}
								?>
								<!-- <li><a href="promotoria/nuevoPromotor.php" target="myFrame">Administraci&oacute;n</a></li>-->
							</ul>
						</li>
						<li rel="tile_520" sigas><a target="myFrame">Actas de visita</a>
							<ul id="tile_520"  title="Actas de visita" alt="SIGAS">
								<?php 
									//16 ASOPAGOS - TARJETAS
									$rsFrm = $db->querySimple($sql0);
									while ($form=$rsFrm->fetch()){
										if ($form['idmenu']==31){
											echo "<li><a href=". $form['url']." target=". $form['destino'] .">". $form['submenu'] ."</a></li>";
										}
									}
								?>
								<!-- <li><a href="promotoria/actaVisita.php" target="myFrame">Acta visita</a></li>-->
							</ul>
						</li>
						<li rel="tile_530" sigas><a target="myFrame">Liquidaciones</a>
							<ul id="tile_530"  title="Liquidaciones" alt="SIGAS">
								<?php 
									//16 ASOPAGOS - TARJETAS
									$rsFrm = $db->querySimple($sql0);
									while ($form=$rsFrm->fetch()){
										if ($form['idmenu']==32){
											echo "<li><a href=". $form['url']." target=". $form['destino'] .">". $form['submenu'] ."</a></li>";
										}
									}
								?>
								<!-- <li><a href="promotoria/liquidaciones.php" target="myFrame">Liquidaci&oacute;n - Visita</a></li>
								<li><a href="promotoria/liquidacionAportes.php" target="myFrame">Liquidaci&oacute;n - Aportes</a></li>-->
							</ul>
						</li>
						<li rel="tile_540" sigas><a target="myFrame">Convenios</a>
							<ul id="tile_540"  title="Convenios" alt="SIGAS">
								<?php 
									//16 ASOPAGOS - TARJETAS
									$rsFrm = $db->querySimple($sql0);
									while ($form=$rsFrm->fetch()){
										if ($form['idmenu']==33){
											echo "<li><a href=". $form['url']." target=". $form['destino'] .">". $form['submenu'] ."</a></li>";
										}
									}
								?>
								<!-- <li><a href="promotoria/consultaEmpresaConvenio.php" target="myFrame">Consulta</a></li>-->
							</ul>	
						</li>
						<!-- <li rel="tile_550" sigas><a target="myFrame" href="promotoria/pazysalvo.php">Paz y salvo</a>-->
						<li rel="tile_550" sigas><a target="myFrame">Reportes</a>
							<ul id="tile_550"  title="Informes" alt="SIGAS">
								<li><a href="centroReportes/promotoria/menuPromotoria.php" target="myFrame">Informes</a></li>
							</ul>
						</li>
					</ul>
				</li>
<?php 		
	}//fin promotoria
//informes
	if ($orden==900){
?>
		<li rel="tile_006" class="menu"><a target="myFrame" class="fNiv">Reportes</a>
			<ul id="tile_600"  title="Centro de Reportes" alt="SIGAS">
				<li><a href="centroReportes/index.php" target="myFrame">Centro de Reportes</a></li>
			</ul>
		</li>
<?php 		
	}//fin informes
//estadistica
	if ($orden==1000){
?>
		<li rel="tile_007" class="menu"><a target="myFrame" class="fNiv">Estad&iacute;stica</a>
					<ul id="tile_007"  title="Estadistica" alt="Menu">
					<!-- 
						<li rel="tile_700" sigas><a target="myFrame">Generar Estadistica</a>
							<ul id="tile_700"  title="Generar Estadistica" alt="SIGAS">
								<li><a href="estadisticas/estadistica.php" target="myFrame">Generar Estad&iacute;stica</a></li>
							</ul>
						</li>
					 -->
					<li rel="tile_710" sigas><a target="myFrame">Informes</a>
						<ul id="tile_710"  title="Reportes" alt="SIGAS">
							<li><a href="centroReportes/estadisticas/menuRptEstadisticas.php" target="myFrame">Informes</a></li>
							
						</ul>
					</li>
				</ul>
			</li>
<?php 		
	}//fin estadistica	
//ruaf
	if ($orden==1200){
?>
		
		<li rel="tile_008" class="menu"><a target="myFrame" class="fNiv">RUAF</a>
				<ul id="tile_008"  title="RUAF" alt="SIGAS">		
				
					<li rel="tile_810" sigas><a target="myFrame">Modificaciones</a>
						<ul id="tile_810"  title="Modificaciones" alt="SIGAS">
							<li rel="tile_8010"><a target="myFrame">Limpieza</a>
								<ul id="tile_8010"  title="Limpieza" alt="SIGAS">
									<?php 
										//54 RUAF
										$rsFrm = $db->querySimple($sql0);
										while ($form=$rsFrm->fetch()){
										if ($form['idmenu']==60){
										echo "<li><a href=". $form['url']." target=". $form['destino'] .">". $form['submenu'] ."</a></li>";
										}
									}
									?>
								</ul>
							</li>
							<li rel="tile_8011"><a target="myFrame">Personas</a>
								<ul id="tile_8011"  title="Personas" alt="SIGAS">
									<?php 
									//54 RUAF
										$rsFrm = $db->querySimple($sql0);
										while ($form=$rsFrm->fetch()){
										if ($form['idmenu']==61){
										echo "<li><a href=". $form['url']." target=". $form['destino'] .">". $form['submenu'] ."</a></li>";
										}
									}
									?>
								</ul>
							</li> 
						</ul>
					</li>
					<li rel="tile_910" sigas><a target="myFrame">Subsidios</a>
						<ul id="tile_910"  title="Subsidios" alt="SIGAS">
						<?php 
						//54 RUAF
						$rsFrm = $db->querySimple($sql0);
						while ($form=$rsFrm->fetch()){
							if ($form['idmenu']==57){
								echo "<li><a href=". $form['url']." target=". $form['destino'] .">". $form['submenu'] ."</a></li>";
							}
						}
						?>
						</ul>
					</li>
					<li rel="tile_1010" sigas><a target="myFrame">Afiliados</a>
						<ul id="tile_1010"  title="Afiliados" alt="SIGAS">
							<?php 
							//54 RUAF
							$rsFrm = $db->querySimple($sql0);
							while ($form=$rsFrm->fetch()){
								if ($form['idmenu']==58){
									echo "<li><a href=". $form['url']." target=". $form['destino'] .">". $form['submenu'] ."</a></li>";
								}
							}
							?>
						</ul>
					</li>
					 
					<li rel="tile_1020" sigas><a target="myFrame">Supervivencia</a>
						<ul id="tile_1020"  title="Subsidio Educaci&oacute;n" alt="SIGAS">
							<?php 
							//54 RUAF
							$rsFrm = $db->querySimple($sql0);
							while ($form=$rsFrm->fetch()){
								if ($form['idmenu']==59){
									echo "<li><a href=". $form['url']." target=". $form['destino'] .">". $form['submenu'] ."</a></li>";
								}
							}
							?>
							
						</ul>
					</li>
					
				</ul>
			</li>

<?php 		
	}//fin ruaf		
//eventoas especiales
	if ($orden==1400){
?>
		<li rel="tile_009" class="menu"><a target="myFrame" class="fNiv">Eventos Especiales</a>
				<ul id="tile_009"  title="Eventos Especiales" alt="Menu">
					<li rel="tile_900" sigas><a target="myFrame">Pignoraci&oacute;n</a>
						<ul id="tile_900"  title="Pignoración" alt="SIGAS">
                        <?php 
						//36,37 PIGNORACION
						$rsFrm = $db->querySimple($sql0);
						while ($form=$rsFrm->fetch()){
							if ($form['idmenu']==37 || $form['idmenu']==36 ){
								echo "<li><a href=". $form['url']." target=". $form['destino'] .">". $form['submenu'] ."</a></li>";
							}
						}
						?>		
							<!--<li><a href="pignoracion/pignoracion.php" target="myFrame">Pignorar</a></li>
							<li><a href="pignoracion/autorizarPignoracion.php" target="myFrame">Autorizar</a></li>
							<li><a href="pignoracion/anularPignoracion.php" target="myFrame">Anular</a></li>
							<li><a href="centroReportes/pignoracion/menuPignoracion.php" target="myFrame">Reportes</a></li>-->
						</ul>
					</li>
					<li rel="tile_902" sigas><a target="myFrame">Bonos Computador</a>
						<ul id="tile_902"  title="Bono Computador" alt="SIGAS">
                        <?php 
						//44 BONOS COMPUTADOR
						$rsFrm = $db->querySimple($sql0);
						while ($form=$rsFrm->fetch()){
							if ($form['idmenu']== 45 ){
								echo "<li><a href=". $form['url']." target=". $form['destino'] .">". $form['submenu'] ."</a></li>";
							}
						}
						?>	
							<!--<li><a href="eventos/bonoComputador/bonoCp.php" target="myFrame">Bono Computador</a></li></li>
							<li><a href="eventos/bonoComputador/autorizarBono.php" target="myFrame">Autorizar Bono </a></li></li>
							<li><a href="eventos/bonoComputador/anularBono.php" target="myFrame">Anular Bono</a></li></li>-->
                            <!--<li><a href="centroReportes/evento/bonosCP/menuReportesEvento.php" target="_blank">Informes</a></li></li> -->
						</ul>
					</li>
                   
					<li><a href="aportes/trabajadores/consultaTrabajador.php" target="_blank">Consulta Afiliado</a></li>
					<li><a href="eventos/regalos/regalos_navidad.php" target="myFrame">Regalos Navide�os</a></li>
					<?php 
						//62 PAQUETE ESCOLAR
						$rsFrm = $db->querySimple($sql0);
						while ($form=$rsFrm->fetch()){
							if ($form['idmenu']== 62 ){
								echo "<li><a href="."http://".$ip."/".$form['url']." target=". $form['destino'] .">". $form['submenu'] ."</a></li>";
							}
						}
						?> 
				</ul>
			</li>
<?php 		
	}//fin eventos
//vivienda  
	if ($orden==1600){
?>
		<!-- 
		<li rel="tile_010" class="menu"><a target="myFrame" class="fNiv">VIVIENDA</a>
			
			<ul id="tile_010"  title="Tarjetas" alt="Menu">
				<li rel="tile_900" sigas><a target="myFrame">VIVIENDA</a>
						<ul id="tile_900"  title="Cargar Plano" alt="SIGAS">
							<li><a href="construccion.html" target="myFrame">VIVIENDA</a></li>
						</ul>
					</li> 
			</ul>
			 
		</li>
		-->
<?php 		
	}//fin parametrizacion
	
//seguridad
	if($orden==2000){
?>
	<li rel="tile_014" class="menu"><a target="myFrame" class="fNiv">Seguridad</a>
		<ul id="tile_014"  title="Seguridad" alt="Menu" sigas>
			<li rel="tile_0140"><a target="myFrame">Usuarios</a>
				<ul id="tile_0140"  title="Usuarios" alt="SIGAS">
				<?php 
				//25 usuarios
				$rsFrm = $db->querySimple($sql0);
				while ($form=$rsFrm->fetch()){
					if ($form['idmenu']==25){
						echo "<li><a href=". $form['url']." target=". $form['destino'] .">". $form['submenu'] ."</a></li>";
					}
				}
				?>				
				</ul>
			</li>
			<li rel="tile_0141"><a target="myFrame">B�sicas</a>
				<ul id="tile_0141"  title="Basicas" alt="SIGAS">
					<?php 
				//26 basicas
				$rsFrm = $db->querySimple($sql0);
				while ($form=$rsFrm->fetch()){
					if ($form['idmenu']==26){
						echo "<li><a href=". $form['url']." target=". $form['destino'] .">". $form['submenu'] ."</a></li>";
					}
				}
				?>		
				</ul>
			</li>
			<li rel="tile_0142"><a target="myFrame">Roles</a>
				<ul id="tile_0142"  title="Usuarios" alt="SIGAS">
					<?php 
				//27 roles
				$rsFrm = $db->querySimple($sql0);
				while ($form=$rsFrm->fetch()){
					if ($form['idmenu']==27){
						echo "<li><a href=". $form['url']." target=". $form['destino'] .">". $form['submenu'] ."</a></li>";
					}
				}
				?>				
				</ul>
			</li> 
			<li rel="tile_0143"><a target="myFrame">Infomes</a>
				<ul id="tile_0143"  title="Informes" alt="SIGAS">
                <?php 
				//28 informes
				$rsFrm = $db->querySimple($sql0);
				while ($form=$rsFrm->fetch()){
					if ($form['idmenu']==28){
						echo "<li><a href=". $form['url']." target=". $form['destino'] .">". $form['submenu'] ."</a></li>";
					}
				}
				?>	
				</ul>
			</li> 
			<li rel="tile_01410"><a target="myFrame">Parametrizacion</a>
				<ul id="tile_01410"  title="Informes" alt="SIGAS">
                 <?php 
				//53 parametrizacion
				$rsFrm = $db->querySimple($sql0);
				while ($form=$rsFrm->fetch()){
					if ($form['idmenu']==53){
						echo "<li><a href=". $form['url']." target=". $form['destino'] .">". $form['submenu'] ."</a></li>";
					}
				}
				?>	
				<!--
                <li rel="tile_1000"><a target="myFrame">Definiciones</a>
						<ul id="tile_1000"  title="Definiciones" alt="SIGAS">
							<li><a href="parametrizacion/definiciones.php" target="myFrame">Definiciones</a></li>
							<li><a href="parametrizacion/dDefiniciones.php" target="myFrame">Detalle de las Definiciones</a></li>
							<li><a href="parametrizacion/construccion.html" target="myFrame">Informes</a></li>
						</ul>
					</li>
                    <li>
						<a href="parametrizacion/barrios.php" target="myFrame">Barrios</a>
					</li>
					<li>	
						<a href="parametrizacion/dDefiniciones.php?iddef=21&nombre=Cargo" target="myFrame">Cargos</a>
					</li>
                    <li>	
						<a href="parametrizacion/dDefiniciones.php?iddef=54&nombre=Causal devoluci�n" target="myFrame">Causales de devoluci&oacute;n</a>
					</li>
                     <li>	
						<a href="parametrizacion/dDefiniciones.php?iddef=35&nombre=Banco" target="myFrame">Bancos</a>
					</li>
                    <li>	
						<a href="parametrizacion/dDefiniciones.php?iddef=36&nombre=Juzgado" target="myFrame">Juzgados</a>
					</li>
                    <li>	
						<a href="parametrizacion/dDefiniciones.php?iddef=45&nombre=Causal NO giro" target="myFrame">Causales de NO giro</a>
					</li>
                 -->
				<li rel="tile_01415"><a target="myFrame">Manuales</a>
				<ul id="tile_01415"  title="Manuales" alt="SIGAS">
					<li><a href="help/MANUAL DE USUARIO DE SIGAS.pdf" target="myFrame">Manual del Usuario</a></li>
						<li><a href="help/MANUAL TECNICO DE SIGAS.pdf" target="myFrame">Manual del T&eacute;cnico</a></li>
						<li><a href="help/Manual de Funciones SIGAS.pdf" target="myFrame">Manual de Funciones</a></li>
						<li><a href="help/MANUAL  DE  PROCEDIMIENTO DE SIGAS.pdf" target="myFrame">Manual de Prodedimiento </a></li>
						<li><a href="help/MANUAL DE SEGURIDAD.pdf" target="myFrame">Manual de Seguridad</a></li>
				</ul>

			</li> 
                
				</ul>
				<li rel="tile_0143"><a onclick="acercaDeSigas();" target="myFrame">Acerca de SIGAS</a></li>
				
				<li rel="tile_01433"><a onclick="acercaDeSigas();" target="myFrame">Versionamiento</a></li>
				
				<li rel="tile_0144"><a target="myFrame">Requerimientos</a>
				<ul id="tile_0144" title="Requerimientos de Software" alt='SIAS'>
					<li rel="tile_0144"><a href="requerimientos/requerimientos.php" target="myFrame">Nuevo Requerimiento</a></li>
					<li rel="tile_0144"><a href="requerimientos/seguimientoRequerimiento.php" target="myFrame">Seguimiento Requerimiento</a></li>
					<li rel="tile_0144"><a href="requerimientos/consultaRequerimiento.php" target="myFrame">Consulta Requerimiento</a></li>
				</ul>
				</li>
			</li>
		</ul>
	</li>
<?php 		
	}//fin seguridad
if ($orden==2200){
?>
		<li rel="tile_220" class="menu"><a target="myFrame" class="fNiv">CONSULTAS</a>
			<ul id="tile_220"  title="Consultas" alt="Menu">
            <?php 
				//49 Consulta Basica
				$rsFrm = $db->querySimple($sql0);
				while ($form=$rsFrm->fetch()){
					if ($form['idmenu']==49){
						echo "<li><a href=". $form['url']." target=". $form['destino'] .">". $form['submenu'] ."</a></li>";
					}
				}
				?>	
                
              <?php 
				//50 Consulta General
				$rsFrm = $db->querySimple($sql0);
				while ($form=$rsFrm->fetch()){
					if ($form['idmenu']==50){
						echo "<li><a href=". $form['url']." target=". $form['destino'] .">". $form['submenu'] ."</a></li>";
					}
				}
				?>
                
                <?php 
				//52 Consulta 
				$rsFrm = $db->querySimple($sql0);
				while ($form=$rsFrm->fetch()){
					if ($form['idmenu']==52){
						echo "<li><a href=". $form['url']." target=". $form['destino'] .">". $form['submenu'] ."</a></li>";
					}
				}
				?>
				<li rel="tile_221" ><a href="credito/subirPlano.html" target="myFrame">Cruce Informaci&oacute;n</a></li>
					  	  
				<!--
                <li rel="tile_221" ><a href="aportes/trabajadores/consultaTSimple.php" target="myFrame">Consulta Afiliado</a></li>
				<li rel="tile_221" ><a href="aportes/trabajadores/consultaGeneral.php" target="myFrame">Ficha Afiliado</a></li>
				<li rel="tile_221" ><a href="consultas/empresa/consultaEmpresa.php" target="myFrame">Empresa...</a></li> 
                -->
                
			</ul>
		</li>   
<?php
	
}// fin consultas

}// fin while menu


?>

 	      		
</ul><!-- Menu FIN --> 			
</div>
<div id="acercaDeSigas" title="Acerca de SIGAS" style="background-image:url('imagenes/FondoGeneral0.png')">

<table border="0" align="center">
	<tr>
		<td ><img src="imagenes/logo_comfamiliar.png" width="280" height="84" heigth="350"/></td>
	</tr>
	<tr>
		<td align="center"><label id="titulo" >&nbsp; SIGAS</label></td>
	</tr>
	<tr>
		<td align="center"><label id="version" ><?php echo $version;?></label></td>
	</tr>
	<tr>
		<td align="center"><label id="informacion" ><?php echo $fechaVersion;?></label></td>
	</tr>
    <tr>
    	<td colspan="2">&nbsp;<br/>
        
        </td>
    </tr>
	
</table>
<div id="piepagina"><br/>
S&iacute;stemas de informaci&oacute;n y gesti&oacute;n de aportes y subsidio<br/>
Desarrollado Por: Ing. Orlando Puentes Andrade.
</div>

</div>
<iframe id="myFrame" name="myFrame" frameborder="0" class="divScreen"  src="empty.php"> </iframe> 

</body>
<script>  
$("#acercaDeSigas").css("display","none");
function cierre(){
}

function acercaDeSigas(){
	$("#acercaDeSigas").css("display","block");
	    $("#acercaDeSigas").dialog('open' );
}
</script>
</html>