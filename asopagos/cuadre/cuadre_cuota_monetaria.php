<?php 
	date_default_timezone_set('America/Bogota');
	set_time_limit(0);
	$raiz="";
	$ruta_generados = "";
	$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
	include_once  $root;
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
	
	$db = IFXDbManejador::conectarDB();
	if($db->conexionID==null){
		$cadena = $db->error;
		echo msg_error($cadena);
		exit();
	}
	
	$sql = "SELECT isnull(sum(cast(valor as bigint)),0) as valor,'A' as definicion from aportes014
			union
			select isnull(sum(cast(descuento as bigint)),0) as valor,'B' as definicion from aportes014 where embargo='S' and tipopago in ('T','E')
			union
			select isnull(sum(cast(descuento as bigint)),0) as valor,'C' as definicion from aportes014 where embargo='N' and descuento>0
			union
			select sum(isnull(cast(valor as bigint),0)-isnull(cast(descuento as bigint),0)) as valor,'D' as definicion from aportes014 where tipopago='T'
			union
			select isnull(sum(cast(valor as bigint)),0) as valor,'E' as definicion from aportes014 where tipopago='C'
			union
			select isnull(sum(cast(valor as bigint)),0) as valor,'F' as definicion from aportes104 where tipopago in ('E','T') and flag is null and periodogiro=(select min(periodo) from aportes012 where procesado='N')
			union
			select isnull(sum(cast(valor as bigint)),0) as valor,'G' as definicion from aportes014 where tipopago='E' and codigopago='03=MUERTOS'	
			";
	$rs = $db->querySimple($sql);
?>
<html>
	<head>
		<title>Cuota Monetaria</title>
		<style>
			.tabla{
				margin:0 auto;
				border:1px solid #ccc;
				width:250px;
			}
			.tabla td{
				border:1px solid #ccc;
			}
			.tabla th{background:#ccc;}
			.numero{text-align:right;}
		</style>
	</head>
	<body>
		<table  class='tabla'>
			<tr><th colspan="2">Cuadre Cuota Monetaria</th></tr>
			<?php 
				$tarjeta=0;
				$embargo=0;
				$cheque=0;
				$pignoracion=0;
				$pasoValores=0;
				$total=0;
				$muerto=0;
				while(($row=$rs->fetch())==true){
					if(($row["definicion"]=="D")){
						$tarjeta = $row["valor"];
					}else if ($row["definicion"]=="B"){
						$embargo = $row["valor"];
					}else if($row["definicion"]=="E"){
						$cheque = $row["valor"];

					}else if($row["definicion"]=="C"){
						$pignoracion = $row["valor"];

					}else if($row["definicion"]=="F"){
						$pasoValores = $row["valor"];
					}else if($row["definicion"]=="A"){
						$total = $row["valor"];
					}
					else if($row["definicion"]=="G"){
						$muerto = $row["valor"];
					}					
				}
				//$diferencia=(intval($tarjeta)+intval($embargo+$muerto))-intval($pasoValores);
				$diferencia=($tarjeta+$embargo+$muerto)-$pasoValores;
				$difeTotal = ($tarjeta+$embargo+$cheque+$pignoracion+$muerto)-$total;
			?>
			<tr>
				<td>Valor Cuota</td>
				<td class="numero"><?php echo number_format($total);?></td>
			</tr>
			<tr>
				<td>Cheques</td>
				<td class="numero"><?php echo number_format($cheque);?></td>
			</tr>
			<tr>
				<td>Embargos</td>
				<td class="numero"><?php echo number_format($embargo);?></td>
			</tr>
			<tr>
				<td>Tarjetas</td>
				<td class="numero"><?php echo number_format($tarjeta);?></td>
			</tr>
			<tr>
				<td>Pignoracion</td>
				<td class="numero"><?php echo number_format($pignoracion);?></td>
			</tr>
			<tr>
				<td>Muertos</td>
				<td class="numero"><?php echo number_format($muerto);?></td>
			</tr>
			<tr>
				<th colspan="2">Paso Valores</th>
			</tr>
			<tr>
				<td>Total</td>
				<td class="numero"><?php echo number_format($pasoValores);?></td>
			</tr>	
			<tr>
				<th colspan="2">Diferencias</th>
			</tr>		
			<tr>
				<td><h3>Aportes014</h3></td><td class="numero"><h1 ><?php echo number_format($difeTotal);?></h1></td>
			</tr>	
			<tr>
				<td><h3>TARJETAS</h3></td><td class="numero"><h1 ><?php echo number_format($diferencia);?></h1></td>
			</tr>
		</table>
	</body>
</html>