<?php

/*# remover vacios
$cadena = 'hola oscar  como   esta';
$patron = '/ /';
echo 'Vacios: ' . preg_replace($patron, '', $cadena).'<br/>';

# SOLO NUMEROS
$cadena = '12-3-o*+sAcVa�r4';
$conservar = '0-9'; // juego de caracteres a conservar
$regex = sprintf('~[^%s]++~i', $conservar); // case insensitive
$cadena = preg_replace($regex, '', $cadena);
echo 'solo numeros: ' . preg_replace($patron, '', $cadena).'<br/>';

//$cadena = 'ESPAÑ^^A';
$cadena = 'PEÃÂ�al ';
//$cadena = 'MUÑOZ';
$conservar = '0-9a-z '; // juego de caracteres a conservar
$regex = sprintf('~[^%s]++~i', $conservar); // case insensitive
$cadena = preg_replace($regex, 'N', $cadena);

echo 'Caracteres raroz por (N): ' . preg_replace($patron, '', $cadena).'<br/>';

exit();*/


$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

//$url=$_SERVER['PHP_SELF'];
//include_once $raiz.DIRECTORY_SEPARATOR.'phpComunes'.DIRECTORY_SEPARATOR.'auditoria.php';
//auditar($url);

include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';

?>
<!DOCTYPE html">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>::Generar Plano::</title>
		
		<link href="<?php echo URL_PORTAL; ?>css/Estilos.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo URL_PORTAL; ?>css/marco.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo URL_PORTAL; ?>newcss/css.1.8/jquery-ui-1.8.17.custom.css" rel="stylesheet" type="text/css" />
		
		<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/1.6/jquery-1.6.2.min.js"></script>
		<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/1.6/jquery-ui-1.8.16.custom.min.js"></script>
		<script type="text/javascript" src="<?php echo URL_PORTAL; ?>newjs/comunes.js"></script>
		<script type="text/javascript" src="js/generar_plano_novedad.js"></script>
</head>
<body>
	<div id="wrapTable">
		<form name="form1" enctype="multipart/form-data" action="" method="post">
			<table width="70%" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr>
					<td width="13" height="29" class="arriba_iz">&nbsp;</td>
					<td class="arriba_ce"><span class="letrablanca">::&nbsp;Generar Plano Novedad&nbsp;::</span></td>
					<td width="13" class="arriba_de">&nbsp;</td>
				</tr>
				<tr>
					<td class="cuerpo_iz">&nbsp;</td>
					<td class="cuerpo_ce">
						<br/>
						<img src="<?php echo URL_PORTAL; ?>imagenes/menu/visitas.png" name="btnListar" width="16" height="16" id="btnListar" style="cursor: pointer" title="Listar Archivos"/>
						<br/>
					</td>
					<td class="cuerpo_de">&nbsp;</td>
				</tr>
				<tr>
					<td class="cuerpo_iz">&nbsp;</td>
					<!-- TABLAS DE FORMULARIO -->
					<td class="cuerpo_ce" >
						<table width="95%" border="0" cellspacing="0" class="tablero" style="margin:0 auto 0 auto;" id="tblProceso">
							<thead>								
								<tr >
									<td style="text-align:center;">
										Tipo Plano
									</td>
								</tr>
								<tr>
									<td style="text-align:center;">
										<select name="cmbTipoPlano" id="cmbTipoPlano" class="boxlargo element-required">
											<option value="">--Selecciones--</option>
											<optgroup label="NO MONETARIAS">
												<option value="nov_bloqueo_tarjeta">BLOQUEO DE TARJETAS (NOV 4)</option>
												<option value="nov_modif_datos_basic">MODIFICACION DATOS BASICOS (NOV 6)</option>
											</optgroup>
											<optgroup label="MONETARIAS">
												<option value="nov_abono_subsidio">ABONO SUBSIDIO CUOTA MONETARIA</option>
												<option value="nov_abono_por_reverso">ABONO POR REVERSO</option>
												<option value="nov_reversos">REVERSOS</option>
											</optgroup>
										</select>
										<span class="clsObligatorio">&nbsp;&nbsp;&nbsp;</span> 
									</td>
								</tr>	
								<tr>
									<td style="text-align:center;">
										Encriptar Plano 
										<input type="checkbox" name="chkEncriptar" id="chkEncriptar" />
									</td>
								</tr>
								<tr>
									<td style="text-align:center;">
										<input type='button' name='btnGenerarPlano' id='btnGenerarPlano' value='Generar Plano'/>
									</td>
								</tr>
								<tr>
									<td id="tdDetalle" style="text-align: center;"></td>
								</tr>
								<tr>
									<td id="tdError" style="text-align:center; color: red; font-weight: bold;">&nbsp;</td>
								</tr>
							</thead>
							<tbody>
								<tr><td>&nbsp;</td></tr>
							</tbody>
						</table>
					<td class="cuerpo_de"></td>
				<tr>
					<td height="41" class="abajo_iz">&nbsp;</td>
					<td class="abajo_ce"></td>
					<td class="abajo_de">&nbsp;</td>
				</tr>
			</table>
		</form>
	</div>	
	
	<!-- DIV LISTAR ARCHIVOS -->
	<div id="divArchivos" title="REGISTRO DE ARCHIVOS GENERADOS" style="display: none;">
		<table width="95%" class="tablero" style="margin:0 auto 0 auto;" id="tblListaArchivo">
			<thead>
				<tr>
					<th>&nbsp;</th>
					<th>Id Control</th>
					<th>Nombre Archivo</th>
					<th>Tipo Archivo</th>
					<th>Fecha Sistema</th>
				</tr>
			</thead>
			<tbody></tbody>
		</table>
	</div>
</body>

</html>