<?php
set_time_limit(0);
date_default_timezone_set("America/Bogota");

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';

include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'utilitario.class.php';
include_once $raiz.DIRECTORY_SEPARATOR.'asopagos'.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'encri_plano_asopa.class.php';

include_once $raiz.DIRECTORY_SEPARATOR.'asopagos'.DIRECTORY_SEPARATOR.'generar_plano_novedad'.DIRECTORY_SEPARATOR.'helper'.DIRECTORY_SEPARATOR.'helper_validacion.class.php';

class PlanoMonetaria {
	/************************************
	 *  ATRIBUTOS ENTIDADES			    *
	***********************************/
	private static $con = null;
	private $rutaGenerados = null;
	private $nombrePlano = '';
	private $pathPlano = null;
	private $handlePlano = null;
	
	private $tipoArchivo = 0;
	
	private $consecutivoDia = 0;
	private $contDetalles = 0;
	private $acumValor = 0;
	private $arrTypeData = null;
	private $arrData = null;
	private $arrLogProceso = array('uri_plano'=>null,'nombre_plano'=>'','error'=>0,'descripcion'=>'','logs'=>array(),'id_update'=>array());
	
	private $arrLogRequired = array();
	private $arrLogValidar = array();
	private $arrLogLong = array();
	private $arrLogTotal = array();
	
	private $usuario = '';
	
	private static $CERO = '0';
	private static $ESPACIO = ' ';
	
	private $objHandlerArchivo = null;
	
	function __construct(){
		try{
			
			$this->objHandlerArchivo = new HandlerArchivo();
			
			self::$con = IFXDbManejador::conectarDB();
			if( self::$con->conexionID==null ){
				throw new Exception(self::$con->error);
			}
		}catch(Exception $e){
			echo $e->getMessage();
			exit();
		}
	}
	
	public function get_file($tipoArchivo, $arrData, $rutaGenerados, $encriptar, $usuario){
		
		# Verificar si existen datos
		if(count($arrData)==0){
			$this->arrLogProceso["error"] = 1;
			$this->arrLogProceso["descripcion"] = "No hay datos para generar el archivo";
			return $this->arrLogProceso;
		}
		
		$this->usuario = $usuario;
		$this->rutaGenerados = $rutaGenerados;
		$this->tipoArchivo = $tipoArchivo;
		$this->arrData = $arrData;
				
		$this->set_nombre();
		
		$this->set_detalle();
		
		if(count($this->arrLogRequired)>0){
			$this->arrLogProceso["error"] = 1;
			$this->arrLogProceso["descripcion"] = "Existen datos vacios y la estructura del plano los requiere";
		}
		
		if($this->arrLogProceso["error"] == 0)
			$this->set_trailer();
		
		if($this->arrLogProceso["error"] == 0 && $encriptar=='S'){
			
			# Encriptar
			$objEncriPlanoAsopa = new EncriPlanoAsopa();
			
			# Guardar en la DB el registro del archivo
			
			$hora=date("H:i:s");
			$idArchivo = $this->objHandlerArchivo->guardar_archivo($this->nombrePlano, $this->tipoArchivo,$this->contDetalles, $hora, $usuario);
			
			if($idArchivo==0){
				$this->arrLogProceso["error"] = 1;
				$this->arrLogProceso["descripcion"] = "No fue posible guardar (DB) el registro del archivo";
				return;
			}
		}
		
		# Logs
		$this->arrLogProceso['logs']['required'] = $this->arrLogRequired;
		$this->arrLogProceso['logs']['validar'] = $this->arrLogValidar;
		$this->arrLogProceso['logs']['long'] = $this->arrLogLong;
		$this->arrLogProceso['logs']['total'] = $this->arrLogTotal;
		
		$this->arrLogProceso["uri_plano"] = $this->pathPlano;
		$this->arrLogProceso["nombre_plano"] = $this->nombrePlano;
		
		return $this->arrLogProceso;
	}
	
	private function set_nombre(){
		# Obtener el consecutivo de archivos de este tipo enviados en el dia actual
		$this->consecutivoDia = $this->objHandlerArchivo->fetch_consecutivo_dia($this->tipoArchivo);
		
		$caracterFijo = 'C';	// Caracter Fijo
		$codigoCaja = '032';	// Codigo de la caja segun Supersubsidio (Ej: 032 Huila).
		$fechaEnvio = date('ymd');	// fecha envio.
		
		//$consecutivoDia = $this->fetch_consecutivo_dia();// Consecutivo de archivos de este tipo enviados en el d�a
		$consecutivoDia = str_pad($this->consecutivoDia,3,"0",STR_PAD_LEFT); 
		
		$this->nombrePlano = $caracterFijo.$codigoCaja.$fechaEnvio.$consecutivoDia.'.txt';
		
		# Crear el archivo plano
		$this->pathPlano = $this->rutaGenerados."asopagos".DIRECTORY_SEPARATOR."novedades_monetarias".DIRECTORY_SEPARATOR.$this->nombrePlano;
		
		$this->handlePlano = fopen($this->pathPlano, "w");
	}
	
	private function set_detalle(){
		
		# Obtener los tipos de datos, validaciones y logitud de cada atributo del plano
		$this->set_arr_type_data();
		
		foreach($this->arrData as $row){
			
			$lineaArchivo = '';
			foreach($row as $key => $value){
				
				if($key=='id_update')continue;
				
				
				$arrTypeDataBande = $this->arrTypeData[$key];
				
				# Adicionar el consecutivo
				if($value==='CONSECUTIVO'){
					$value = $this->consecutivoDia;
				}				
				
				# Validar dato 
				if(isset($arrTypeDataBande['validar'])){
					$value = $this->fn_validar($row['col5'], $value, $arrTypeDataBande);
				}		
				
				# Validar la longitud
				if(isset($arrTypeDataBande['long'])){
					$value = $this->fn_long($row['col5'],$value, $arrTypeDataBande);
				}
				
				# Adicionar los str_pad
				if(isset($arrTypeDataBande['STR_PAD_RIGHT']) || isset($arrTypeDataBande['STR_PAD_LEFT'])){
					//$type = isset($arrTypeDataBande['STR_PAD_RIGHT'])?'STR_PAD_RIGHT':'STR_PAD_LEFT';
					//$data = isset($arrTypeDataBande['STR_PAD_RIGHT'])?$arrTypeDataBande['STR_PAD_RIGHT']:$arrTypeDataBande['STR_PAD_LEFT'];
					
					//$value = $this->fn_str_pad($value, $arrTypeDataBande['long'], $type, $data);
					$value = $this->fn_str_pad($value, $arrTypeDataBande);
				}
								
				$lineaArchivo .= $value;
				
				
			}
			
			$lineaArchivo .= "\r\n";
			
			# Adicionar al archivo plano
			fwrite($this->handlePlano, $lineaArchivo);
			
			# Contador para la cantidad de detalles que tiene el archivo
			$this->contDetalles++;
			$this->acumValor += $row['col9'];
			
			$this->arrLogProceso['id_update'][] = $row['id_update'];
		}
		
		$this->arrLogTotal['numero_registros'] = $this->contDetalles;
		$this->arrLogTotal['valor'] = $this->acumValor/100;
		
	}
	
	private function set_trailer(){
		
		$invertido='';
		for($i=strlen($this->acumValor);$i>0;$i--){
			$invertido .= substr($this->acumValor,$i-1,1);
		}
		
		$c1= "000000900319291";  # NIT Empresa
		$c2= date('Ymd');	# Fecha env�o
		$c3= str_pad($this->consecutivoDia,5, self::$CERO,STR_PAD_LEFT);	# Consecutivo
		$c4=str_pad(($this->contDetalles+1), 6, self::$CERO,STR_PAD_LEFT);			# Numero registros reportados
		$c5=str_pad(self::$ESPACIO, 34);	# filler
		$c6=str_pad($this->acumValor,19, self::$CERO,STR_PAD_LEFT); # Monto
		$c7=str_pad($invertido,19,self::$CERO,STR_PAD_RIGHT);		# Monto Invertido
		$c8=str_pad(self::$ESPACIO, 13);	 # filler
		$c9='636480';	# prefijo convenio
		$c10='001'; # subtipo Default 001
		$c11="T";	//trailer
		$c12=str_pad(self::$ESPACIO, 3); //codigo error
		
		$trailer = $c1.$c2.$c3.$c4.$c5.$c6.$c7.$c8.$c9.$c10.$c11.$c12;
		
		# Adicionar al archivo plano
		fwrite($this->handlePlano, $trailer);
		fclose($this->handlePlano);
	}
	
	private function fn_validar($numeroIdentificacion, $dato, $arrTypeDataBande){
		$datoFinal = $dato;
		$long = 0;
		$label = $arrTypeDataBande['label'];
		$arrValidacion = explode(',',$arrTypeDataBande['validar']);
		
		# Validar numeros
		if(array_search('numeric',$arrValidacion)!==false){
			$datoFinal = HelperValicacion::get_numerico($dato);
			if(trim($datoFinal)!=trim($dato)){
				$this->arrLogValidar[] = array('numero_identificacion'=>$numeroIdentificacion,'dato_inicial'=>$dato,'dato_final'=>$datoFinal,'label'=>$label);
				$dato = $datoFinal;
			}
		}
		
		# Reemplazar espacios por vacio
		if(array_search('reemplace_empty',$arrValidacion)!==false){
				
			$datoFinal = HelperValicacion::remove_empty($dato);
				
			if(trim($datoFinal)!=trim($dato)){
				$this->arrLogValidar[] = array('numero_identificacion'=>$numeroIdentificacion,'dato_inicial'=>$dato,'dato_final'=>$datoFinal,'label'=>$label);
				$dato = $datoFinal;
			}
		}
		
		# Reemplazar � o caracteres raros por N
		if(array_search('reemplace_enne',$arrValidacion)!==false){
			
			$datoFinal = HelperValicacion::remove_carater_enne($dato);
			
			if(trim($datoFinal)!=trim($dato)){
				$this->arrLogValidar[] = array('numero_identificacion'=>$numeroIdentificacion,'dato_inicial'=>$dato,'dato_final'=>$datoFinal,'label'=>$label);
				$dato = $datoFinal;
			}
		}
		

		# Validar dato requerido
		if(array_search('required',$arrValidacion)!==false){
			if(HelperValicacion::is_empty($dato)){
				$this->arrLogRequired[] = array('numero_identificacion'=>$numeroIdentificacion,'dato_inicial'=>'','dato_final'=>'','label'=>$label);
			}
		}
		
		return $datoFinal;
	}
	
	private function fn_long($numeroIdentificacion, $dato, $arrTypeDataBande){
		$long = $arrTypeDataBande['long'];
		$label = $arrTypeDataBande['label'];
		
		$datoFinal = HelperValicacion::str_long($dato, $long);
		
		# Adicionar Log
		if(strlen(trim($dato))!=strlen(trim($datoFinal))){
			$this->arrLogLong[] = array('numero_identificacion'=>$numeroIdentificacion,'dato_inicial'=>$dato,'dato_final'=>$datoFinal,'label'=>$label);
		}
		
		return $datoFinal;
	}
	
	private function fn_str_pad($dato, $arrTypeDataBande){
		$long = $arrTypeDataBande['long'];		
		$align = isset($arrTypeDataBande['STR_PAD_RIGHT'])?'STR_PAD_RIGHT':'STR_PAD_LEFT';
		$data_str_pad = isset($arrTypeDataBande['STR_PAD_RIGHT'])?$arrTypeDataBande['STR_PAD_RIGHT']:$arrTypeDataBande['STR_PAD_LEFT'];	
		
		$dato = HelperValicacion::str_pad($dato, $long, $align, $data_str_pad);
		
		return $dato;
	}
	
	private function set_arr_type_data(){
		$this->arrTypeData = array(
				"col1"=>array("type"=>"N","long"=>"15", 'label'=>'NIT Empresa',"STR_PAD_LEFT"=>self::$CERO, 'validar'=>'required,numeric')    # NIT Empresa
				, "col2"=>array("type"=>"N","long"=>"8",'label'=>'Fecha envio', 'validar'=>'required,numeric')    # Fecha envio
				, "col3"=>array("type"=>"N","long"=>"5",'label'=>'Consecutivo',"STR_PAD_LEFT"=>self::$CERO, 'validar'=>'required,numeric')    # Consecutivo
				, "col4"=>array("type"=>"C","long"=>"2",'label'=>'Tipo Identificacion de Cliente',"STR_PAD_LEFT"=>self::$ESPACIO, 'validar'=>'required')    # Tipo Identificacion de Cliente
				, "col5"=>array("type"=>"N","long"=>"15",'label'=>'Numero Identificacion de Cliente',"STR_PAD_LEFT"=>self::$CERO, 'validar'=>'required,numeric')    # Numero Identificacion de Cliente
				, "col6"=>array("type"=>"C","long"=>"19",'label'=>'Numero de Tarjeta',"STR_PAD_RIGHT"=>self::$ESPACIO, 'validar'=>'required')    # Numero de Tarjeta
				, "col7"=>array("type"=>"C","long"=>"19",'label'=>'No. De cuenta',"STR_PAD_RIGHT"=>self::$ESPACIO, 'validar'=>'required')    # No. De cuenta
				, "col8"=>array("type"=>"C","long"=>"1",'label'=>'Nov', 'validar'=>'required')    # Nov.0=Ab/1=Desc
				, "col9"=>array("type"=>"N","long"=>"17",'label'=>'Valor Novedad',"STR_PAD_LEFT"=>self::$CERO, 'validar'=>'required,numeric')    # Valor Novedad
				, "col10"=>array("type"=>"N","long"=>"2",'label'=>'Codigo de Bolsillo',"STR_PAD_LEFT"=>self::$CERO, 'validar'=>'required,numeric')    # C�digo de Bolsillo
				, "col11"=>array("type"=>"C","long"=>"26",'label'=>'Filler',"STR_PAD_RIGHT"=>self::$ESPACIO)    # Filler
				, "col12"=>array("type"=>"C","long"=>"3",'label'=>'Blancos',"STR_PAD_RIGHT"=>self::$ESPACIO)    # Blancos
		);
	}
}
?>