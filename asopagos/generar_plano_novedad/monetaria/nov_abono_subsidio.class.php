<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';

include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'utilitario.class.php';
include_once 'helper_nov_monetaria.class.php';
include_once 'plano_monetaria.class.php';

class NovAbonoSubsidio {
	/************************************
	 *  ATRIBUTOS ENTIDADES			    *
	***********************************/
	private static $con = null;
	private $tipoArchivo = 2877;
	private $arrData = array();
	private $usuario = '';
	
	function __construct(){
		try{
			self::$con = IFXDbManejador::conectarDB();
			if( self::$con->conexionID==null ){
				throw new Exception(self::$con->error);
			}
		}catch(Exception $e){
			echo $e->getMessage();
			exit();
		}
	}
	
	public function get_plano($rutaGenerados, $encriptar, $usuario){
		$this->usuario = $usuario;
		
		self::$con->inicioTransaccion();
		
		try {
			
		
			# Obtener informacion
			$arrData = $this->fetch_data();
			
			# Preparar informacion
			$this->preparar_datos();
			
			# Obtener el archivo plano
			$objPlanoMonetaria = new PlanoMonetaria();
			$arrLog = $objPlanoMonetaria->get_file($this->tipoArchivo, $this->arrData, $rutaGenerados, $encriptar, $usuario);
			
			# Actuaiizar informacion DB si el plano se encripta
			if($encriptar=='S' && $arrLog["error"]==0){
				$this->update_data($arrLog['id_update']);
			}
			
		}catch(Exception $e){
			$arrLog['error'] = 1;
			$arrLog['descripcion'] = 'Error desconocido al generar el plano';
		}
		
		if($arrLog['error']==0){	
			self::$con->confirmarTransaccion();
		}else{
			self::$con->cancelarTransaccion();
		}
		
		return $arrLog;
	}
	
	private function preparar_datos(){
		
		$arrDataPreparada = array();
		$fechaSistema = date('Ymd');
		
		foreach ($this->arrData as $data) {
			
			$arrDataPreparada [] = array(
					"id_update"=>$data['idtraslado']    # ID UPDATE PARA ACTUALIZAR LOS DATOS ENVIADOS
					, "col1"=>'000000900319291'    # NIT Empresa
					, "col2"=>$fechaSistema    # Fecha env�o
					, "col3"=>'CONSECUTIVO'    # Consecutivo
					, "col4"=>HNovMonetaria::get_tipo_identificacion($data['idtipodocumento'])    # Tipo Identificaci�n de Cliente
					, "col5"=>trim($data['identificacion'])   # N�mero Identificaci�n de Cliente
					, "col6"=>trim($data['bono'])    # N�mero de Tarjeta. Alineado a la izquierda. Blancos a la derecha
					, "col7"=>trim($data['bono'])    # No. De cuenta. Alineado a la izquierda. Blancos a la derecha.
					, "col8"=>'0'    # Nov.0=Ab/1=Desc
					, "col9"=>intVal($data['valor'])*100    # Valor Novedad
					, "col10"=>'01'   # C�digo de Bolsillo. Fijo 01
					, "col11"=>''   # Filler
					, "col12"=>''    # se env�a en blancos
			);
		}
		
		$this->arrData = $arrDataPreparada;
		
	}
	
	public function fetch_data(){
		$sql="SELECT isnull((aportes104.valor - aportes115.valor),aportes104.valor) AS valor
				,aportes015.idtipodocumento,aportes015.identificacion,aportes101.bono,aportes104.idtraslado 
			FROM aportes104 
				INNER JOIN aportes015 ON aportes104.idtrabajador=aportes015.idpersona 
				INNER JOIN aportes101 ON aportes104.idtrabajador=aportes101.idpersona AND aportes101.estado='A'
				LEFT  JOIN aportes115 on aportes115.idtraslado  =aportes104.idtraslado AND aportes104.reverso='S'
			WHERE aportes104.procesado='N'";
	
		$this->arrData = Utilitario::fetchConsulta($sql,self::$con);
	}
	
	private function update_data($arrIdUpdate){
		
		$arrId = array_chunk($arrIdUpdate,500);

		
		$hora=date("H:i:s a");
		$banderaError = 0;
		
		/*var_dump(self::$con->conexionID);
		self::$con->conexionID->beginTransaction();		
		var_dump('hola');exit();*/
		
		foreach($arrId as $row){
			$id = implode(',',$row);
			
			$query = "UPDATE aportes104 SET procesado='S', fechaproceso=CAST(GETDATE() AS DATE),horaproceso='$hora' where idtraslado IN ($id)";
			$statement = self::$con->conexionID->prepare($query);
			if($statement->execute() == false) $banderaError++;
		}
		
		if($banderaError==0){
			//self::$con->confirmarTransaccion();
			return 1;
		}else{
			//self::$con->cancelarTransaccion();
			return 0;
		}
	}
}
?>