<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';

include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'utilitario.class.php';
include_once 'helper_nov_monetaria.class.php';
include_once 'plano_monetaria.class.php';

class NovReverso {
	/************************************
	 *  ATRIBUTOS ENTIDADES			    *
	***********************************/
	private static $con = null;
	private $tipoArchivo = 2877;
	private $arrData = array();
	private $usuario = '';
	
	function __construct(){
		try{
			self::$con = IFXDbManejador::conectarDB();
			if( self::$con->conexionID==null ){
				throw new Exception(self::$con->error);
			}
		}catch(Exception $e){
			echo $e->getMessage();
			exit();
		}
	}
	
	public function get_plano($rutaGenerados, $encriptar, $usuario){
		$this->usuario = $usuario;
		
		self::$con->inicioTransaccion();
		
		try {
			
		
			# Obtener informacion
			$arrData = $this->fetch_data();
			
			# Preparar informacion
			$this->preparar_datos();
			
			# Obtener el archivo plano
			$objPlanoMonetaria = new PlanoMonetaria();
			$arrLog = $objPlanoMonetaria->get_file($this->tipoArchivo, $this->arrData, $rutaGenerados, $encriptar, $usuario);
			
			# Actuaiizar informacion DB si el plano se encripta
			if($encriptar=='S' && $arrLog["error"]==0){
				$this->update_data($arrLog['id_update']);
			}
			
		}catch(Exception $e){
			$arrLog['error'] = 1;
			$arrLog['descripcion'] = 'Error desconocido al generar el plano';
		}
		
		if($arrLog['error']==0){	
			self::$con->confirmarTransaccion();
		}else{
			self::$con->cancelarTransaccion();
		}
		
		return $arrLog;
	}
	
	private function preparar_datos(){
		
		$arrDataPreparada = array();
		$fechaSistema = date('Ymd');
		
		foreach ($this->arrData as $data) {
			
			$arrDataPreparada [] = array(
					"id_update"=>$data['idreverso']    # ID UPDATE PARA ACTUALIZAR LOS DATOS ENVIADOS
					, "col1"=>'000000900319291'    # NIT Empresa
					, "col2"=>$fechaSistema    # Fecha env�o
					, "col3"=>'CONSECUTIVO'    # Consecutivo
					, "col4"=>HNovMonetaria::get_tipo_identificacion($data['idtipodocumento'])    # Tipo Identificaci�n de Cliente
					, "col5"=>trim($data['identificacion'])   # N�mero Identificaci�n de Cliente
					, "col6"=>trim($data['bono'])    # N�mero de Tarjeta. Alineado a la izquierda. Blancos a la derecha
					, "col7"=>trim($data['bono'])    # No. De cuenta. Alineado a la izquierda. Blancos a la derecha.
					, "col8"=>1   # Nov.0=Ab/1=Desc
					, "col9"=>(($data['valor'])*100)    # Valor Novedad
					, "col10"=>'01'   # C�digo de Bolsillo. Fijo 01
					, "col11"=>''   # Filler
					, "col12"=>''    # se env�a en blancos
			);
		}
		
		$this->arrData = $arrDataPreparada;
		
	}
	
	public function fetch_data(){
		$sql="SELECT idreverso, valor,naturaleza,aportes101.idtarjeta, aportes101.bono, aportes015.idtipodocumento, aportes015.identificacion 
				FROM aportes112 
					INNER JOIN aportes101 on aportes101.idtarjeta=aportes112.idtarjeta 
					INNER JOIN aportes015 on aportes015.idpersona=aportes101.idpersona 
				WHERE procesado='N'
				ORDER BY idreverso";
	
		$this->arrData = Utilitario::fetchConsulta($sql,self::$con);
	}
	
	private function update_data($arrIdUpdate){
		$id = implode(',',$arrIdUpdate);
		$hora=date("H:i:s a");
		$query = "UPDATE aportes112 SET procesado='S', fechaproceso=CAST(GETDATE() AS DATE) WHERE idreverso IN ($id)";
		
		$statement = self::$con->conexionID->prepare($query);
		$guardada = $statement->execute();
		return $guardada == false ? 0 : 1;
	}
}
?>