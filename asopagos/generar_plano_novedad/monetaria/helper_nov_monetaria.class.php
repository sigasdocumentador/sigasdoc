<?php

class HNovMonetaria{
	
	/**
	 * Tipo de identificación
	 * 
	 * @param Int $codigo
	 * @return Int
	 */
	public static function get_tipo_identificacion($codigo){
		switch (intval($codigo)){
			case 1 : $codigo=2; break;
			case 2 : $codigo=4; break;
			case 4 : $codigo=3; break;
			default:$codigo=9;
		}
		
		return $codigo;
	}
}
