<?php
date_default_timezone_set("America/Bogota");

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';

include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'utilitario.class.php';

class HandlerArchivo{
	/************************************
	 *  ATRIBUTOS ENTIDADES			    *
	***********************************/
	private static $con = null;
	
	function __construct(){
		try{
	
			self::$con = IFXDbManejador::conectarDB();
			if( self::$con->conexionID==null ){
				throw new Exception(self::$con->error);
			}
			
		}catch(Exception $e){
			echo $e->getMessage();
			exit();
		}
	}
	
	/**
	 * Metodo encargado de guardar los datos del archivo
	 * 
	 * @param unknown_type $nombreArchivo
	 * @param unknown_type $tipoArchivo
	 * @param unknown_type $cantidadRegistros
	 * @param unknown_type $hora
	 * @param unknown_type $usuario
	 * @return number Id Archivo
	 */
	public function guardar_archivo($nombreArchivo, $tipoArchivo,$cantidadRegistros, $hora, $usuario){
	
		$query = "INSERT INTO aportes149
					(archivo,tipoarchivo,fechasistema,usuario,registros,hora) 	
				VALUES('$nombreArchivo','$tipoArchivo', cast(getdate() as date), '$usuario',$cantidadRegistros, '$hora')";
	
		$resultado = self::$con->queryInsert($query,"aportes149");
	
		$idArchivo = 0;
		if($resultado!==null){
			$idArchivo = self::$con->conexionID->lastInsertId("aportes149");
		}
		return $idArchivo;
	}
	
	public function fetch_consecutivo_dia($tipoArchivo){
		$sql="SELECT COUNT(*) as contador
				FROM aportes149
				WHERE fechasistema=cast(getdate() as date) AND tipoarchivo=$tipoArchivo";
	
		$data = Utilitario::fetchConsulta($sql,self::$con);
	
		$consecutivo = ($data[0]['contador'])+1;
		return $consecutivo;
	}
	
	public function fetch_archivo($tipoArchivo){
		$sql="SELECT TOP 20 idcontrol, archivo, tipoarchivo AS idtipoarchivo, fechasistema, a149.usuario, registros, totalregistros, registros1, totalregistros1, registros2, totalregistros2, registros3, totalregistros3, registros4, registros5, fechamovimiento, hora, consecutivo
					, a91.detalledefinicion AS tipoarchivo
				FROM aportes149 a149
					LEFT JOIN aportes091 a91 ON a91.iddetalledef=a149.tipoarchivo
				WHERE tipoarchivo=$tipoArchivo
				ORDER BY fechasistema DESC, hora DESC";
		
		return Utilitario::fetchConsulta($sql,self::$con);
	}
	
	public function eliminar_archivo($idArchivos){
		
		# Eliminar los registros de la DB
		$sql="DELETE FROM aportes149 WHERE idcontrol IN ({$idArchivos})";
		
		$statement = self::$con->conexionID->prepare($sql);
		$guardada = $statement->execute();
		return $guardada == false ? 0 : 1;
	}
}