var url = "",
	urlGenerarPlano = "",
	urlArchivoLog = "";


$(function(){
	
	url = src();
	urlGenerarPlano = url+"asopagos/generar_plano_novedad/generar_plano_novedad.log.php";
	urlArchivoLog = url+"asopagos/generar_plano_novedad/archivo.log.php";
	
	$("#btnGenerarPlano")
			.button()
			.click(generarPlano);
	$("#btnListar").click(listarArchivo);
	
	
	$("#divArchivos").dialog({
		autoOpen:false,
		modal:true,
		width: 700, 
		buttons: {
			"Eliminar": function(){
				if(confirm("Desea eliminar los archivos?")){
					eliminar_archivo();
				}
			}
			, "Cerrar": function(){
				$("#divArchivos").dialog("close");
			}
		}
	});
});


function nuevo(){
	limpiarCampos2("#cmbTipoPlano,#chkEncriptar,#tblProceso tbody,#tdBandera,#tdDetalle");
}

function fetchArchivo(tipoArchivo){
	var resultado = null;
	$.ajax({
		url:urlArchivoLog,
		type:"POST",
		data:{accion:'buscar_archivo',tipo_archivo:tipoArchivo},
		dataType: "json",
		async:false,
		success:function(data){
			resultado = data.datos;
		}
	});
	
	return resultado;
}

function generarPlano(){
	
	$("#tblProceso tbody, #tdError, #tdDetalle").html("");
	
	//Verificar campos obligatorios
	if(validateElementsRequired()) return false;
	
	var encriptar = "N";
	
	if($("#chkEncriptar") && $("#chkEncriptar").is(":checked")){
		if(confirm("Desea Encriptar el archivo?")){
			encriptar = "S";
		}else{
			limpiarCampos2("#chkEncriptar");
		}	
	}
	
	var objData = {
			tipo_plano: $("#cmbTipoPlano").val()
			, encriptar: encriptar
	};
	
	$.ajax({
		url:urlGenerarPlano,
		type:"POST",
		data:objData,
		dataType: "json",
		async:false,
        //contentType:false,
        //processData:false,
        //cache:false,		
        beforeSend: function(objeto){
        	dialogLoading('show');
        },
        complete: function(objeto, exito){
        	dialogLoading('close');
        },
		success:function(data){
			
			var htmlTotal = "";
			
			if(data.error==1){
				$("#tdError").html("Error: "+data.descripcion);
				ctrLog(data.logs);
			}else if(data.error==0){	
				ctrDatosDetalle(data.logs);
				ctrLog(data.logs);
				descargar();
			}else{
				$("#tdError").html("Error Desconocido");
			}
		}
	});
	
	dialogLoading('close');
}

function ctrDatosDetalle(objLog){
	
	var htmlDetalle = '';
	if(objLog.total && typeof objLog.total=='object'){
		
		var total = 0;
		
		if(objLog.total.valor){
			htmlDetalle = 'TOTAL VALOR: (<b>'+formatCurrency(objLog.total.valor)+'</b>)'
		}
		
		htmlDetalle += ' - TOTAL REG: (<b>'+objLog.total.numero_registros+'</b>)';
	}
	
	$("#tdDetalle").html(htmlDetalle);
}

function ctrLog(objLog/*,tipoLog*/){
	var htmlLogError = "<tr><td><table class='tablero' width='100%'>";
	
	
	//if(tipoLog=="NO_MONETARIA"){
		
		htmlLogError += "<tr><th>Numero Identificacion</th>"
			+ "<th>Dato DB</th>"
			+ "<th>Dato Plano</th>"
			+ "<th>Atributo</th>"
			+ "<th>Tipo</th></tr>";
		
		// Log datos requeridos
		$.each(objLog.required,function(i,row){
			htmlLogError += "<tr><td>"+row.numero_identificacion+"</td>"
				+ "<td>"+row.dato_inicial+"</td>"
				+ "<td>"+row.dato_final+"</td>"
				+ "<td>"+row.label+"</td>"
				+ "<td>Dato requerido</td></tr>";
			
		});
		
		// Log al momento de validar los datos
		$.each(objLog.validar,function(i,row){
			htmlLogError += "<tr><td>"+row.numero_identificacion+"</td>"
				+ "<td>"+row.dato_inicial+"</td>"
				+ "<td>"+row.dato_final+"</td>"
				+ "<td>"+row.label+"</td>"
				+ "<td>Validacion del dato</td></tr>";
			
		});
		
		// Log al momento de validar la longitud
		$.each(objLog.long,function(i,row){
			htmlLogError += "<tr><td>"+row.numero_identificacion+"</td>"
				+ "<td>"+row.dato_inicial+"</td>"
				+ "<td>"+row.dato_final+"</td>"
				+ "<td>"+row.label+"</td>"
				+ "<td>Longitud del dato</td></tr>";
		});
		
	/*}else if(tipoLog=="MONETARIA"){
		
		alert("hola")
	}*/
	
	htmlLogError += "</table></td></tr>";
	
	if((objLog.required.length+objLog.validar.length+objLog.long.length)>0)
		$("#tblProceso tbody").html(htmlLogError);
}

function listarArchivo(){
	var tipoArchivo = $("#cmbTipoPlano").val();
	var idTipoArchivo = 0;
	
	if(tipoArchivo==0){
		alert("Debe indicar el tipo de plano");
		return false;
	}
	
	switch (tipoArchivo) {
		case 'nov_bloqueo_tarjeta': case 'nov_modif_datos_basic':
			idTipoArchivo = 2876;
			break;
		case 'nov_abono_subsidio': case 'nov_abono_por_reverso': case 'nov_reversos':
			idTipoArchivo = 2877;
			break;
	}
	
	var arrData = fetchArchivo(idTipoArchivo);
	if(arrData && typeof arrData == "object" && arrData.length>0){
		var htmlLista = "";
		$.each(arrData,function(i,row){
			htmlLista += "<tr><td><input type='checkbox' name='chkIdControl' id='chkIdControl' value='"+row.idcontrol+"'/></td>"
					+ "<td>"+row.idcontrol+"</td>"
					+ "<td>"+row.archivo+"</td>"
					+ "<td>"+row.tipoarchivo+"</td>"
					+ "<td>"+row.fechasistema+":"+row.hora+"</td></tr>";
		});
		
		$("#tblListaArchivo tbody").html(htmlLista);
		$("#divArchivos").dialog("open");
	}else{
		alert("No se encontraron archivos");
	}
}

function eliminar_archivo(){
	var arrIdControl = [];
	
	if($("#chkIdControl:checked").length==0){
		alert("Debe indicar los archivos a eliminar");
		return false;
	}
	
	$("#chkIdControl:checked").each(function(i,row){
		arrIdControl[arrIdControl.length] = row.value;
	});
	
	$.ajax({
		url:urlArchivoLog,
		type:"POST",
		data:{accion:'eliminar_archivo',arr_id_archivo:arrIdControl},
		dataType: "json",
		async:false,
		success:function(data){

			if(data.error==0){
				$("#divArchivos").dialog("close");
				alert("Los archivos se eliminaron correctamente");
			}else{
				alert("Error al eliminar los archivos");
			}
		}
	});
}

function descargar(){
	var url=URL+'phpComunes/descargaPlanos.php';
	window.open(url);
}