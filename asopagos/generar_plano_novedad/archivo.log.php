<?php
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
include_once $raiz.DIRECTORY_SEPARATOR. 'config.php';

include_once $raiz.DIRECTORY_SEPARATOR.'asopagos'.DIRECTORY_SEPARATOR.'generar_plano_novedad'.DIRECTORY_SEPARATOR.'handler'.DIRECTORY_SEPARATOR.'handler_archivo.class.php';

$accion = $_POST['accion'];
$usuario = $_SESSION['USUARIO'];
$arrLogProceso = array('error'=>0,'descripcion'=>'','datos'=>null);

$objHandlerArchivo = new HandlerArchivo();

switch ($accion) {
	
	case 'buscar_archivo': # Buscar los registros en la DB
		
		$tipoArchivo = $_POST["tipo_archivo"];
		
		$arrLogProceso['datos'] = $objHandlerArchivo->fetch_archivo($tipoArchivo);
		break;
		
	case 'eliminar_archivo': # Eliminar los registros de la DB y los archivo del directorio
		
		$arrIdArchivo = ($_POST["arr_id_archivo"]);
		
		$idArchivos = implode(',',$arrIdArchivo);
		
		$resultado = $objHandlerArchivo->eliminar_archivo($idArchivos);
		
		if($resultado==0){
			$arrLogProceso['error'] = 1;
			$arrLogProceso['descripcion'] = 'Error al realizar la accion';
		}
		
		break;
}

echo json_encode($arrLogProceso);
?>