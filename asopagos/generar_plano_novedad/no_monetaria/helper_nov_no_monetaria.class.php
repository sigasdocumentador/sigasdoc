<?php

class HNovNoMonetaria{
	
	/**
	 * Numero producto referencia cruzada
	 *
	 * @return string
	 */
	public static function get_numer_produ_refer_cruza(){
		return '0';
	}
	
	/**
	 * Tipo de persona
	 * 
	 * @return string
	 */
	public static function get_tipo_persona(){
		return 'N';
	}
	
	/**
	 * Tipo de identificaci�n
	 * 
	 * @param Int $codigo
	 * @return Int
	 */
	public static function get_tipo_identificacion($codigo){
		switch (intval($codigo)){
			case 1 : $codigo=2; break;
			case 2 : $codigo=4; break;
			case 4 : $codigo=3; break;
			default:$codigo=9;
		}
		
		return $codigo;
	}
	
	/**
	 * Nombre corto
	 * 
	 * @param String $primerNombre
	 * @param String $primerApellido
	 * @return String
	 */
	public static function get_nombre_corto($primerNombre, $primerApellido){
		return trim($primerNombre)." ".trim($primerApellido);
	}
	
	/**
	 * Fecha de nacimiento
	 * 
	 * @param String $fecha
	 * @return Int
	 */
	public static function get_fecha_nacimiento($fecha){
		$intFecha = '';
		if(strlen(trim($fecha))!=0){
			$arrFecha = explode("-",$fecha);
			$intFecha = $arrFecha[0].$arrFecha[1].$arrFecha[2];
		}
		return $intFecha;
	}
	
	/**
	 * Sexo
	 *
	 * @param String $codigo
	 * @return Int
	 */
	public static function get_sexo($codigo){
		$intCodigo = 1;
		switch($codigo){
			case 'M' : $intCodigo=1; break;
			case 'F' : $intCodigo=2; break;
		}
		return $intCodigo;
	}
	
	/**
	 * Estado civil
	 * 
	 * @param Int $codigo
	 * @return Int
	 */
	public static function get_estado_civil($codigo){
		switch (intval($codigo)){
			case 50 : $codigo = 1; break;
			case 51 : $codigo = 5; break;
			case 52 : $codigo = 4; break;
			case 53 : $codigo = 2; break;
			case 54 : $codigo = 3; break;
			default:$codigo = 9;
		}
		return $codigo;
	}
	
	
	
	/**
	 * Direcci�n residencial L�nea 1
	 * @return string
	 */
	public static function get_direccion_residencia_1($direccion){
		if(strlen(trim($direccion))==0){
			$direccion="CALLE 11 5 13";
		}
		return $direccion;
	}
	
	/**
	 * Direcci�n residencial L�nea 2
	 * @return string
	 */
	public static function get_direccion_residencia_2($direccion){
		return self::get_direccion_residencia_1($direccion);
	}
	
	/**
	 * C�digo departamento de la direcci�n residencial
	 * 
	 * @param String $codigo
	 * @return String
	 */
	public static function get_cod_depar_direc_resid($codigo){
		if(strlen(trim($codigo))==0){
			$codigo='41';
		}
		return $codigo;
	}
	
	/**
	 * C�digo ciudad de la direcci�n residencial
	 *
	 * @param String $codigo
	 * @return String
	 */
	public static function get_cod_ciuda_direc_resid($codigo){
		if(strlen(trim($codigo))==0){
			$codigo='41001';
		}
		return $codigo;
	}
	
	/**
	 * Zona postal direcci�n residencial
	 *
	 * @param String $codigo
	 * @return String
	 */
	public static function get_zona_posta_direc_resid(){
		return '999999';
	}
	
	/**
	 * Direcci�n correspondencia L�nea 1
	 * @return string
	 */
	public static function get_direccion_correspondencia_1($direccion){
		return self::get_direccion_residencia_1($direccion);
	}
	
	/**
	 * Direcci�n correspondencia L�nea 2
	 * @return string
	 */
	public static function get_direccion_correspondencia_2($direccion){
		return self::get_direccion_residencia_1($direccion);
	}
	
	
	/**
	 * C�digo departamento de la direcci�n correspondencia
	 *
	 * @param String $codigo
	 * @return String
	 */
	public static function get_cod_depar_direc_corre($codigo){
		return self::get_cod_depar_direc_resid($codigo);
	}
	
	/**
	 * C�digo ciudad de la direcci�n correspondencia
	 *
	 * @param String $codigo
	 * @return String
	 */
	public static function get_cod_ciuda_direc_corre($codigo){
		return self::get_cod_ciuda_direc_resid($codigo);
	}
	
	
	/**
	 * Zona postal direcci�n de correspondencia
	 *
	 * @return String
	 */
	public static function get_zona_posta_direc_corre(){
		return self::get_zona_posta_direc_resid();
	}
	
	/**
	 * Oficina de radicaci�n de la solicitud
	 *
	 * @return String
	 */
	public static function get_oficina_radicacion(){
		return '0';
	}
	
	/**
	 * Tel�fono residencia
	 * 
	 * @param unknown_type $telefono
	 * @return String
	 */
	public static function get_telefono_recidencia($telefono){
		if(strlen(trim($telefono))==0){
			$telefono='8759544';
		}
		return $telefono;
	}
	
	/**
	 * Tel�fono oficina
	 * 
	 * @param unknown_type $telefono
	 * @return String
	 */
	public static function get_telefono_oficina($telefono){
		return self::get_telefono_recidencia($telefono);
	}
	
	/**
	 * Afinidad de la tarjeta
	 *
	 * @return String
	 */
	public static function get_afinidad_tarjeta(){
		return '0';
	}
	
	/**
	 * Cupo asignado
	 *
	 * @return String
	 */
	public static function get_cupo_asignado(){
		return '0';
	}
	
	/**
	 * Fecha de la solicitud
	 *
	 * @return String
	 */
	public static function get_fecha_solicitud(){
		return '00000000';
	}
	
	/**
	 * Tipo de solicitud
	 *
	 * @return String
	 */
	public static function get_tipo_solicitud(){
		return '000';
	}
	
	/**
	 * Grupo de manejo (cuotas de manejo)
	 *
	 * @return String
	 */
	public static function get_grupo_manejo(){
		return '002';
	}
	
	/**
	 * Tipo de Tarjeta
	 *
	 * @return String
	 */
	public static function get_tipo_tarjeta(){
		return '001';
	}
	
	/**
	 * C�digo de Vendedor
	 *
	 * @return String
	 */
	public static function get_codigo_vendedor(){
		return '000000';
	}
	
	/**
	 * Estado registro recibido
	 *
	 * @return String
	 */
	public static function get_estad_regis_recib(){
		return '';
	}
	
	/**
	 * C�digo de error
	 *
	 * @return String
	 */
	public static function get_codigo_error(){
		return '';
	}
	
	/**
	 * Numero de solicitud
	 *
	 * @return String
	 */
	public static function get_numero_solicitud(){
		return '0';
	}
	
	/**
	 * Tarjeta anterior
	 *
	 * @return String
	 */
	public static function get_tarjeta_anterior(){
		return '';
	}
	
	/**
	 * C�digo punto de distribuci�n
	 *
	 * @return String
	 */
	public static function get_codig_punto_distr(){
		return '0';
	}
	
	/**
	 * C�digo barras � Convenio Mercadeo
	 *
	 * @return Int
	 */
	public static function get_codig_barra_conve_merca(){
		return 209;
	}
	
	/**
	 * C�digo Bloqueo tarjeta
	 *
	 * @param String $codigo
	 * @return String
	 */
	public static function get_codig_bloqu_tarje($codigo){
		if(strlen(trim($codigo))==0)
			$codigo = '';
		 
		return $codigo;
	}
	
	/**
	 * Motivo del Bloqueo
	 *
	 * @param String $codigo
	 * @return String
	 */
	public static function get_motivo_bloqueo($codigo){
		if(strlen(trim($codigo))==0)
			$codigo = '';
			
		return $codigo;
	}
	
	/**
	 * Tipo de Cuenta
	 *
	 * @return String
	 */
	public static function get_tipo_cuenta(){
		return '01';
	}
	
	/**
	 * Tipo de Filler
	 *
	 * @return String
	 */
	public static function get_filter(){
		return '';
	}
}
