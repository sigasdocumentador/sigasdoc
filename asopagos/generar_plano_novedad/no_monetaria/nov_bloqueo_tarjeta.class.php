<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';

include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'utilitario.class.php';
include_once 'helper_nov_no_monetaria.class.php';
include_once 'plano_no_monetaria.class.php';

class NovBloqueoTarjeta {
	/************************************
	 *  ATRIBUTOS ENTIDADES			    *
	***********************************/
	private static $con = null;
	private $tipoArchivo = 2876;
	private $arrData = array();
	private $usuario = '';
	
	function __construct(){
		try{
			self::$con = IFXDbManejador::conectarDB();
			if( self::$con->conexionID==null ){
				throw new Exception(self::$con->error);
			}
		}catch(Exception $e){
			echo $e->getMessage();
			exit();
		}
	}
	
	public function get_plano($rutaGenerados, $encriptar, $usuario){
		$this->usuario = $usuario;
		
		self::$con->inicioTransaccion();
		
		try {
			
		
			# Obtener informacion
			$arrData = $this->fetch_data();
			
			# Preparar informacion
			$this->preparar_datos();
			
			# Obtener el archivo plano
			$objPlanoNoMonetaria = new PlanoNoMonetaria();
			$arrLog = $objPlanoNoMonetaria->get_file($this->tipoArchivo, $this->arrData, $rutaGenerados, $encriptar, $usuario);
			
			# Actuaiizar informacion DB si el plano se encripta
			if($encriptar=='S' && $arrLog["error"]==0){
				$this->update_data($arrLog['id_update']);
			}
			
		}catch(Exception $e){
			$arrLog['error'] = 1;
			$arrLog['descripcion'] = 'Error desconocido al generar el plano';
		}
		
		if($arrLog['error']==0){	
			self::$con->confirmarTransaccion();
		}else{
			self::$con->cancelarTransaccion();
		}
		
		return $arrLog;
	}
	
	private function preparar_datos(){
		
		$arrDataPreparada = array();
		
		foreach ($this->arrData as $data) {
			$nombreCorto = HNovNoMonetaria::get_nombre_corto($data['pnombre'], $data['papellido']);
			
			$arrDataPreparada [] = array(
					"id_update"=>$data['idbloqueo']    # ID UPDATE PARA ACTUALIZAR LOS DATOS ENVIADOS
					, "col1"=>'04'    # Tipo de Novedad
					, "col2"=>HNovNoMonetaria::get_numer_produ_refer_cruza()    # Numero producto referencia cruzada
					, "col3"=>trim($data['bono'])    # N�mero de tarjeta asignado
					, "col4"=>HNovNoMonetaria::get_tipo_persona()    # Tipo de persona
					, "col5"=>HNovNoMonetaria::get_tipo_identificacion($data['idtipodocumento'])    # Tipo de identificaci�n
					, "col6"=>trim($data['identificacion'])    # N�mero de identificaci�n
					, "col7"=>trim($data['papellido'])    # Primer apellido
					, "col8"=>trim($data['sapellido'])    # Segundo apellido
					, "col9"=>trim($data['pnombre'])    # Primer nombre
					, "col10"=>trim($data['snombre'])    # Segundo nombre
					, "col11"=>$nombreCorto    # Nombre corto
					, "col12"=>(empty($data['nombrecorto'])?$nombreCorto : trim($data['nombrecorto']))    # Nombre de realce
					, "col13"=>HNovNoMonetaria::get_fecha_nacimiento($data['fechanacimiento'])    # Fecha de nacimiento
					, "col14"=>HNovNoMonetaria::get_sexo($data['sexo'])    # Sexo
					, "col15"=>HNovNoMonetaria::get_estado_civil($data['idestadocivil'])    # Estado civil
					, "col16"=>HNovNoMonetaria::get_direccion_residencia_1($data['direccion'])    # Direcci�n residencial L�nea 1
					, "col17"=>HNovNoMonetaria::get_direccion_residencia_2($data['direccion'])    # Direcci�n residencial L�nea 2
					, "col18"=>HNovNoMonetaria::get_cod_depar_direc_resid($data['iddepresidencia'])    # C�digo departamento de la direcci�n residencial
					, "col19"=>HNovNoMonetaria::get_cod_ciuda_direc_resid($data['idciuresidencia'])    # C�digo ciudad de la direcci�n residencial
					, "col20"=>HNovNoMonetaria::get_zona_posta_direc_resid()    # Zona postal direcci�n residencial
					, "col21"=>HNovNoMonetaria::get_direccion_correspondencia_1($data['direccion'])    # Direcci�n correspondencia L�nea 1
					, "col22"=>HNovNoMonetaria::get_direccion_correspondencia_2($data['direccion'])    # Direcci�n de correspondencia L�nea 2
					, "col23"=>HNovNoMonetaria::get_cod_depar_direc_corre($data['iddepresidencia'])    # C�digo departamento de la direcci�n residencial
					, "col24"=>HNovNoMonetaria::get_cod_ciuda_direc_corre($data['idciuresidencia'])    # C�digo ciudad de la direcci�n de correspondencia
					, "col25"=>HNovNoMonetaria::get_zona_posta_direc_corre()    # Zona postal direcci�n de correspondencia
					, "col26"=>HNovNoMonetaria::get_oficina_radicacion()    # Oficina de radicaci�n de la solicitud
					, "col27"=>HNovNoMonetaria::get_telefono_recidencia($data['telefono'])    # Tel�fono residencia
					, "col28"=>HNovNoMonetaria::get_telefono_oficina($data['telefono'])    # Tel�fono oficina
					, "col29"=>HNovNoMonetaria::get_afinidad_tarjeta()    # Afinidad de la tarjeta
					, "col30"=>HNovNoMonetaria::get_cupo_asignado()    # Cupo asignado
					, "col31"=>HNovNoMonetaria::get_fecha_solicitud()    # Fecha de la solicitud
					, "col32"=>HNovNoMonetaria::get_tipo_solicitud()    # Tipo de solicitud
					, "col33"=>HNovNoMonetaria::get_grupo_manejo()    # Grupo de manejo (cuotas de manejo)
					, "col34"=>HNovNoMonetaria::get_tipo_tarjeta()    # Tipo de Tarjeta
					, "col35"=>HNovNoMonetaria::get_codigo_vendedor()    # C�digo de Vendedor
					, "col36"=>HNovNoMonetaria::get_estad_regis_recib()    # Estado registro recibido
					, "col37"=>HNovNoMonetaria::get_codigo_error()    # C�digo de error
					, "col38"=>HNovNoMonetaria::get_numero_solicitud()    # Numero de solicitud
					, "col39"=>HNovNoMonetaria::get_tarjeta_anterior()    # Tarjeta anterior
					, "col40"=>HNovNoMonetaria::get_codig_punto_distr()    # C�digo punto de distribuci�n
					, "col41"=>HNovNoMonetaria::get_codig_barra_conve_merca()    # C�digo barras � Convenio Mercadeo
					, "col42"=>HNovNoMonetaria::get_codig_bloqu_tarje('01')    # C�digo Bloqueo tarjeta
					, "col43"=>HNovNoMonetaria::get_motivo_bloqueo($data['codigo'])    # Motivo del Bloqueo
					, "col44"=>HNovNoMonetaria::get_tipo_cuenta()    # Tipo de Cuenta
					, "col45"=>HNovNoMonetaria::get_filter()    # Filler
			);
		}
		
		$this->arrData = $arrDataPreparada;
		
	}
	
	public function fetch_data(){
		$sql="SELECT bono, idbloqueo,aportes015.idtipodocumento,identificacion,codigobarra,aportes091.codigo, papellido, sapellido, pnombre, snombre, nombrecorto, fechanacimiento, sexo, idestadocivil, direccion, iddepresidencia, idciuresidencia,telefono 
				FROM aportes113 
					INNER JOIN aportes091 ON aportes113.codigobloqueo = aportes091.iddetalledef 
					INNER JOIN aportes101 ON aportes113.idtarjeta=aportes101.idtarjeta 
					INNER JOIN aportes015 ON aportes101.idpersona = aportes015.idpersona 
				WHERE aportes113.procesado='N'
				UNION
				SELECT bono, idbloqueo,aportes015.idtipodocumento,identificacion,codigobarra,aportes091.codigo, papellido, sapellido, pnombre, snombre, nombrecorto, fechanacimiento, sexo, idestadocivil, direccion, iddepresidencia, idciuresidencia,telefono 
				FROM aportes113 
					INNER JOIN aportes091 ON aportes113.codigobloqueo = aportes091.iddetalledef 
					INNER JOIN aportes102 ON aportes113.idtarjeta=aportes102.idtarjeta 
					INNER JOIN aportes015 ON aportes102.idpersona = aportes015.idpersona 
				WHERE aportes113.procesado='N'";
	
		$this->arrData = Utilitario::fetchConsulta($sql,self::$con);
	}
	
	private function update_data($arrIdUpdate){
		$id = implode(',',$arrIdUpdate);
		$query = "Update aportes113 set procesado='S', fechaproceso=cast(getdate() as date),usuarioprocesa='{$this->usuario}' where idbloqueo in ($id)";
		
		$statement = self::$con->conexionID->prepare($query);
		$guardada = $statement->execute();
		return $guardada == false ? 0 : 1;
	}
}
?>