<?php
set_time_limit(0);
date_default_timezone_set("America/Bogota");

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';

include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'utilitario.class.php';
include_once $raiz.DIRECTORY_SEPARATOR.'asopagos'.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'encri_plano_asopa.class.php';

include_once $raiz.DIRECTORY_SEPARATOR.'asopagos'.DIRECTORY_SEPARATOR.'generar_plano_novedad'.DIRECTORY_SEPARATOR.'helper'.DIRECTORY_SEPARATOR.'helper_validacion.class.php';

class PlanoNoMonetaria {
	/************************************
	 *  ATRIBUTOS ENTIDADES			    *
	***********************************/
	private static $con = null;
	private $rutaGenerados = null;
	private $nombrePlano = '';
	private $pathPlano = null;
	private $handlePlano = null;
	
	private $tipoArchivo = 0;
	
	private $consecutivoDia = 0;
	private $contDetalles = 0;
	private $arrTypeData = null;
	private $arrData = null;
	private $arrLogProceso = array('uri_plano'=>null,'nombre_plano'=>'','error'=>0,'descripcion'=>'','logs'=>array(),'id_update'=>array());
	
	private $arrLogRequired = array();
	private $arrLogValidar = array();
	private $arrLogLong = array();
	private $arrLogTotal = array();
	
	private $usuario = '';
	
	private static $CERO = '0';
	private static $ESPACIO = ' ';
	
	private $objHandlerArchivo = null;
	
	function __construct(){
		try{
			
			$this->objHandlerArchivo = new HandlerArchivo();
			
			self::$con = IFXDbManejador::conectarDB();
			if( self::$con->conexionID==null ){
				throw new Exception(self::$con->error);
			}
		}catch(Exception $e){
			echo $e->getMessage();
			exit();
		}
	}
	
	public function get_file($tipoArchivo, $arrData, $rutaGenerados, $encriptar, $usuario){
		
		# Verificar si existen datos
		if(count($arrData)==0){
			$this->arrLogProceso["error"] = 1;
			$this->arrLogProceso["descripcion"] = "No hay datos para generar el archivo";
			return $this->arrLogProceso;
		}
		
		$this->usuario = $usuario;
		$this->rutaGenerados = $rutaGenerados;
		$this->tipoArchivo = $tipoArchivo;
		$this->arrData = $arrData;
				
		$this->set_nombre();
		
		$this->set_detalle();
		
		if(count($this->arrLogRequired)>0){
			$this->arrLogProceso["error"] = 1;
			$this->arrLogProceso["descripcion"] = "Existen datos vacios y la estructura del plano los requiere";
		}
		
		if($this->arrLogProceso["error"] == 0)
			$this->set_trailer();
		
		if($this->arrLogProceso["error"] == 0 && $encriptar=='S'){
			
			# Encriptar
			$objEncriPlanoAsopa = new EncriPlanoAsopa();
			
			# Guardar en la DB el registro del archivo
			
			$hora=date("H:i:s");
			$idArchivo = $this->objHandlerArchivo->guardar_archivo($this->nombrePlano, $this->tipoArchivo,$this->contDetalles, $hora, $usuario);
			
			if($idArchivo==0){
				$this->arrLogProceso["error"] = 1;
				$this->arrLogProceso["descripcion"] = "No fue posible guardar (DB) el registro del archivo";
				return;
			}
		}
		
		# Logs
		$this->arrLogProceso['logs']['required'] = $this->arrLogRequired;
		$this->arrLogProceso['logs']['validar'] = $this->arrLogValidar;
		$this->arrLogProceso['logs']['long'] = $this->arrLogLong;
		$this->arrLogProceso['logs']['total'] = $this->arrLogTotal;
		
		$this->arrLogProceso["uri_plano"] = $this->pathPlano;
		$this->arrLogProceso["nombre_plano"] = $this->nombrePlano;
		
		return $this->arrLogProceso;
	}
	
	private function set_nombre(){
		# Obtener el consecutivo de archivos de este tipo enviados en el dia actual
		$this->consecutivoDia = $this->objHandlerArchivo->fetch_consecutivo_dia($this->tipoArchivo);
		
		$caracterFijo = 'A';	// Caracter Fijo
		$codigoBolsillo = '01';	// Codigo del bolsillo (debito).
		$codigoCaja = '032';	// Codigo de la caja segun Supersubsidio (Ej: 032 Huila).
		$fechaEnvio = date('ymd');	// fecha envio.
		
		//$consecutivoDia = $this->fetch_consecutivo_dia();// Consecutivo de archivos de este tipo enviados en el d�a
		$consecutivoDia = str_pad($this->consecutivoDia,3,"0",STR_PAD_LEFT); 
		
		$this->nombrePlano = $caracterFijo.$codigoBolsillo.$codigoCaja.$fechaEnvio.$consecutivoDia.'.txt';
		
		# Crear el archivo plano
		$this->pathPlano = $this->rutaGenerados."asopagos".DIRECTORY_SEPARATOR."novedades_no".DIRECTORY_SEPARATOR.$this->nombrePlano;
		
		$this->handlePlano = fopen($this->pathPlano, "w");
	}
	
	private function set_detalle(){
		
		# Obtener los tipos de datos, validaciones y logitud de cada atributo del plano
		$this->set_arr_type_data();
		
		foreach($this->arrData as $row){
			
			$lineaArchivo = '';
			
			foreach($row as $key => $value){
				
				if($key=='id_update')continue;
				
				$arrTypeDataBande = $this->arrTypeData[$key];
				
				# Validar dato
				if(isset($arrTypeDataBande['validar'])){
					$value = $this->fn_validar($row['col6'], $value, $arrTypeDataBande);
				}
				
				# Validar la longitud
				if(isset($arrTypeDataBande['long'])){
					$value = $this->fn_long($row['col6'],$value, $arrTypeDataBande);
				}
				
				# Adicionar los str_pad
				if(isset($arrTypeDataBande['STR_PAD_RIGHT']) || isset($arrTypeDataBande['STR_PAD_LEFT'])){
					$value = $this->fn_str_pad($value, $arrTypeDataBande);
				}
								
				$lineaArchivo .= $value;
			}
			
			$lineaArchivo .= "\r\n";
			
			# Adicionar al archivo plano
			fwrite($this->handlePlano, $lineaArchivo);
			
			# Contador para la cantidad de detalles que tiene el archivo
			$this->contDetalles++;
			$this->arrLogProceso['id_update'][] = $row['id_update'];
		}
		
		$this->arrLogTotal['numero_registros'] = $this->contDetalles;
		
	}
	
	private function set_trailer(){
		$c1= "000000900319291";  # NIT Empresa
		$c2= date('Ymd');	# Fecha env�o
		$c3= str_pad($this->consecutivoDia,5,"0",STR_PAD_LEFT);	# Consecutivo
		$c4=str_pad($this->contDetalles, 6, "0",STR_PAD_LEFT);			# Numero registros reportados
		$c5="00"; # Codigo de area
		$c6=str_pad(self::$ESPACIO, 427);		# filler
		$c7="636480";					# Prefijo del Convenio (BIN)
		$c8="001";						# Subtipo
		$c9='T';	# Indicador trailer
		$c10=str_pad(self::$ESPACIO, 3); # C�digo error
		
		$trailer = $c1.$c2.$c3.$c4.$c5.$c6.$c7.$c8.$c9.$c10;
		
		# Adicionar al archivo plano
		fwrite($this->handlePlano, $trailer);
		fclose($this->handlePlano);
	}

	private function fn_validar($numeroIdentificacion, $dato, $arrTypeDataBande){
		$datoFinal = $dato;
		$long = 0;
		$label = $arrTypeDataBande['label'];
		$arrValidacion = explode(',',$arrTypeDataBande['validar']);
		
		# Validar numeros
		if(array_search('numeric',$arrValidacion)!==false){
			$datoFinal = HelperValicacion::get_numerico($dato);
			if(trim($datoFinal)!=trim($dato)){
				$this->arrLogValidar[] = array('numero_identificacion'=>$numeroIdentificacion,'dato_inicial'=>$dato,'dato_final'=>$datoFinal,'label'=>$label);
				$dato = $datoFinal;
			}
		}
	
		# Reemplazar espacios por vacio
		if(array_search('reemplace_empty',$arrValidacion)!==false){
	
			$datoFinal = HelperValicacion::remove_empty($dato);
	
			if(trim($datoFinal)!=trim($dato)){
				//$this->arrLogValidar[] = array('numero_identificacion'=>$numeroIdentificacion,'dato_inicial'=>$dato,'dato_final'=>$datoFinal,'label'=>$label);
				$dato = $datoFinal;
			}
		}
	
		# Reemplazar � o caracteres raros por N
		if(array_search('reemplace_enne',$arrValidacion)!==false){
			
			$datoFinal = HelperValicacion::remove_carater_enne($dato);			
			if(trim($datoFinal)!=trim($dato)){
				$this->arrLogValidar[] = array('numero_identificacion'=>$numeroIdentificacion,'dato_inicial'=>$dato,'dato_final'=>$datoFinal,'label'=>$label);
				$dato = $datoFinal;
			}
		}
		
		if(array_search('direccion',$arrValidacion)!==false){
				
			$datoFinal = HelperValicacion::get_direccion($dato);
			if(trim($datoFinal)!=trim($dato)){
				$this->arrLogValidar[] = array('numero_identificacion'=>$numeroIdentificacion,'dato_inicial'=>$dato,'dato_final'=>$datoFinal,'label'=>$label);
				$dato = $datoFinal;
			}
		}
	
		# Validar dato requerido
		if(array_search('required',$arrValidacion)!==false){
			if(HelperValicacion::is_empty($dato)){
				$this->arrLogRequired[] = array('numero_identificacion'=>$numeroIdentificacion,'dato_inicial'=>'','dato_final'=>'','label'=>$label);
			}
		}
	
		return $datoFinal;
	}
	
	private function fn_long($numeroIdentificacion, $dato, $arrTypeDataBande){
		$long = $arrTypeDataBande['long'];
		$label = $arrTypeDataBande['label'];
	
		$datoFinal = HelperValicacion::str_long($dato, $long);
	
		# Adicionar Log
		if(strlen(trim($dato))!=strlen(trim($datoFinal))){
			$this->arrLogLong[] = array('numero_identificacion'=>$numeroIdentificacion,'dato_inicial'=>$dato,'dato_final'=>$datoFinal,'label'=>$label);
		}
		return $datoFinal;
	}
	
	private function fn_str_pad($dato, $arrTypeDataBande){
		$long = $arrTypeDataBande['long'];
		$align = isset($arrTypeDataBande['STR_PAD_RIGHT'])?'STR_PAD_RIGHT':'STR_PAD_LEFT';
		$data_str_pad = isset($arrTypeDataBande['STR_PAD_RIGHT'])?$arrTypeDataBande['STR_PAD_RIGHT']:$arrTypeDataBande['STR_PAD_LEFT'];
	
		$dato = HelperValicacion::str_pad($dato, $long, $align, $data_str_pad);
	
		return $dato;
	}
	
	private function set_arr_type_data(){
		$this->arrTypeData = array(
				"col1"=>array("type"=>"C","long"=>"2", 'label'=>'Tipo de Novedad')    # Tipo de Novedad
				, "col2"=>array("type"=>"N","long"=>"14",'label'=>'Numero producto referencia cruzada',"STR_PAD_LEFT"=>self::$CERO)    # Numero producto referencia cruzada
				, "col3"=>array("type"=>"C","long"=>"19",'label'=>'Numero de tarjeta asignado',"STR_PAD_RIGHT"=>self::$ESPACIO, 'validar'=>'required')    # N�mero de tarjeta asignado
				, "col4"=>array("type"=>"C","long"=>"1",'label'=>'Tipo de persona', 'validar'=>'required')    # Tipo de persona
				, "col5"=>array("type"=>"N","long"=>"1",'label'=>'Tipo de identificacion', 'validar'=>'required')    # Tipo de identificacion
				, "col6"=>array("type"=>"N","long"=>"15",'label'=>'Numero de identificacion',"STR_PAD_LEFT"=>self::$CERO, 'validar'=>'required,numeric')    # Numero de identificacion
				, "col7"=>array("type"=>"C","long"=>"15",'label'=>'Primer apellido',"STR_PAD_RIGHT"=>self::$ESPACIO,'validar'=>'required,reemplace_enne,reemplace_empty')    # Primer apellido
				, "col8"=>array("type"=>"C","long"=>"15",'label'=>'Segundo apellido',"STR_PAD_RIGHT"=>self::$ESPACIO,'validar'=>'reemplace_enne,reemplace_empty')    # Segundo apellido
				, "col9"=>array("type"=>"C","long"=>"15",'label'=>'Primer nombre',"STR_PAD_RIGHT"=>self::$ESPACIO,'validar'=>'required,reemplace_enne,reemplace_empty')    # Primer nombre
				, "col10"=>array("type"=>"C","long"=>"15",'label'=>'Segundo nombre',"STR_PAD_RIGHT"=>self::$ESPACIO,'validar'=>'reemplace_enne,reemplace_empty')    # Segundo nombre
				, "col11"=>array("type"=>"C","long"=>"20",'label'=>'Nombre corto',"STR_PAD_RIGHT"=>self::$ESPACIO,'validar'=>'required,reemplace_enne')    # Nombre corto
				, "col12"=>array("type"=>"C","long"=>"26",'label'=>'Nombre de realce',"STR_PAD_RIGHT"=>self::$ESPACIO,'validar'=>'required,reemplace_enne')    # Nombre de realce
				, "col13"=>array("type"=>"N","long"=>"8",'label'=>'Fecha de nacimiento','validar'=>'required,numeric')    # Fecha de nacimiento
				, "col14"=>array("type"=>"N","long"=>"1",'label'=>'Sexo')    # Sexo
				, "col15"=>array("type"=>"N","long"=>"1",'label'=>'Estado civil')    # Estado civil
				, "col16"=>array("type"=>"C","long"=>"40",'label'=>'Direccion residencial Linea 1',"STR_PAD_RIGHT"=>self::$ESPACIO,'validar'=>'required,direccion')    # Direccion residencial L�nea 1
				, "col17"=>array("type"=>"C","long"=>"40",'label'=>'Direccion residencial Linea 2',"STR_PAD_RIGHT"=>self::$ESPACIO,'validar'=>'direccion')    # Direccion residencial L�nea 2
				, "col18"=>array("type"=>"N","long"=>"2",'label'=>'Codigo departamento de la direccion residencial',"STR_PAD_RIGHT"=>self::$ESPACIO)    # Codigo departamento de la direccion residencial
				, "col19"=>array("type"=>"N","long"=>"5",'label'=>'Codigo ciudad de la direccion residencial',"STR_PAD_RIGHT"=>self::$ESPACIO)    # Codigo ciudad de la direccion residencial
				, "col20"=>array("type"=>"N","long"=>"6",'label'=>'Zona postal direccion residencial',"STR_PAD_RIGHT"=>self::$ESPACIO)    # Zona postal direccion residencial
				, "col21"=>array("type"=>"C","long"=>"40",'label'=>'Direccion correspondencia Linea 1',"STR_PAD_RIGHT"=>self::$ESPACIO,'validar'=>'required,direccion')    # Direccion correspondencia L�nea 1
				, "col22"=>array("type"=>"C","long"=>"40",'label'=>'Direccion de correspondencia Linea 2',"STR_PAD_RIGHT"=>self::$ESPACIO,'validar'=>'direccion')    # Direccion de correspondencia L�nea 2
				, "col23"=>array("type"=>"N","long"=>"2",'label'=>'Codigo departamento de la direccion residencial',"STR_PAD_RIGHT"=>self::$ESPACIO)    # Codigo departamento de la direccion residencial
				, "col24"=>array("type"=>"N","long"=>"5",'label'=>'Codigo ciudad de la direccion de correspondencia',"STR_PAD_RIGHT"=>self::$ESPACIO)    # Codigo ciudad de la direccion de correspondencia
				, "col25"=>array("type"=>"N","long"=>"6",'label'=>'Zona postal direccion de correspondencia',"STR_PAD_RIGHT"=>self::$ESPACIO)    # Zona postal direccion de correspondencia
				, "col26"=>array("type"=>"N","long"=>"5",'label'=>'Oficina de radicacion de la solicitud',"STR_PAD_RIGHT"=>self::$CERO)    # Oficina de radicacion de la solicitud
				, "col27"=>array("type"=>"C","long"=>"14",'label'=>'Telefono residencia',"STR_PAD_RIGHT"=>self::$ESPACIO, 'validar'=>'numeric')    # Telefono residencia
				, "col28"=>array("type"=>"C","long"=>"14",'label'=>'Telefono oficina',"STR_PAD_RIGHT"=>self::$ESPACIO, 'validar'=>'numeric')    # Telefono oficina
				, "col29"=>array("type"=>"C","long"=>"5",'label'=>'Afinidad de la tarjeta',"STR_PAD_RIGHT"=>self::$CERO)    # Afinidad de la tarjeta
				, "col30"=>array("type"=>"N","long"=>"15",'label'=>'Cupo asignado',"STR_PAD_RIGHT"=>self::$CERO)    # Cupo asignado
				, "col31"=>array("type"=>"C","long"=>"8",'label'=>'Fecha de la solicitud')    # Fecha de la solicitud
				, "col32"=>array("type"=>"N","long"=>"3",'label'=>'Tipo de solicitud',"STR_PAD_LEFT"=>self::$CERO)    # Tipo de solicitud
				, "col33"=>array("type"=>"N","long"=>"3",'label'=>'Grupo de manejo (cuotas de manejo)',"STR_PAD_LEFT"=>self::$CERO)    # Grupo de manejo (cuotas de manejo)
				, "col34"=>array("type"=>"N","long"=>"3",'label'=>'Tipo de Tarjeta',"STR_PAD_LEFT"=>self::$CERO)    # Tipo de Tarjeta
				, "col35"=>array("type"=>"C","long"=>"6",'label'=>'Codigo de Vendedor',"STR_PAD_LEFT"=>self::$CERO)    # Codigo de Vendedor
				, "col36"=>array("type"=>"C","long"=>"1",'label'=>'Estado registro recibido',"STR_PAD_LEFT"=>self::$ESPACIO)    # Estado registro recibido
				, "col37"=>array("type"=>"C","long"=>"4",'label'=>'Codigo de error',"STR_PAD_LEFT"=>self::$ESPACIO)    # Codigo de error
				, "col38"=>array("type"=>"N","long"=>"10",'label'=>'Numero de solicitud',"STR_PAD_RIGHT"=>self::$CERO)    # Numero de solicitud
				, "col39"=>array("type"=>"N","long"=>"19",'label'=>'Tarjeta anterior',"STR_PAD_LEFT"=>self::$ESPACIO)    # Tarjeta anterior
				, "col40"=>array("type"=>"C","long"=>"3",'label'=>'Codigo punto de distribucion',"STR_PAD_LEFT"=>self::$CERO)    # Codigo punto de distribuci�n
				, "col41"=>array("type"=>"C","long"=>"3",'label'=>'Codigo barras � Convenio Mercadeo')    # Codigo barras � Convenio Mercadeo
				, "col42"=>array("type"=>"C","long"=>"2",'label'=>'Codigo Bloqueo tarjeta',"STR_PAD_LEFT"=>self::$ESPACIO)    # Codigo Bloqueo tarjeta
				, "col43"=>array("type"=>"C","long"=>"2",'label'=>'Motivo del Bloqueo',"STR_PAD_LEFT"=>self::$ESPACIO)    # Motivo del Bloqueo
				, "col44"=>array("type"=>"N","long"=>"2",'label'=>'Tipo de Cuenta')    # Tipo de Cuenta
				, "col45"=>array("type"=>"C","long"=>"36",'label'=>'Filler',"STR_PAD_RIGHT"=>self::$ESPACIO)    # Filler
		);
	}
}
?>