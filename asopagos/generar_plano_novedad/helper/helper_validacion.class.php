<?php

class HelperValicacion{
	
	public static function is_empty($dato){
		$resultado = false;
		$dato = (string)$dato;
		if(strlen(trim($dato))==0){
			$resultado = true;
		}
		return $resultado;
	}
	
	public static function str_long($dato, $long){
		$dato = substr((string) $dato, 0,$long);
		return $dato;
	}
	
	public static function str_pad($dato, $long, $align, $data_str_pad){
		
		if($align=='STR_PAD_RIGHT')
			$dato = str_pad($dato,$long,$data_str_pad,STR_PAD_RIGHT);
		else
			$dato = str_pad($dato,$long,$data_str_pad,STR_PAD_LEFT);
		
		return $dato;
	}

	public static function get_numerico($dato){
		
		$dato = (string) $dato;
		# SOLO NUMEROS
		$conservar = '0-9'; // juego de caracteres a conservar
		$regex = sprintf('~[^%s]++~i', $conservar); // case insensitive
		$dato = preg_replace($regex, '', $dato);
		
		return $dato;
	}
	
	public static function get_direccion($dato){
		$dato = (string) $dato;
	
		# REEMPLAZAR CARATERES RAROS POR vacio
		$conservar = '0-9a-z \-#:/'; // juego de caracteres a conservar
		$regex = sprintf('~[^%s]++~i', $conservar); // case insensitive
		$dato = preg_replace($regex, '', $dato);
		
		return $dato;
	}
	
	public static function remove_carater_enne($dato){
		$dato = (string) $dato;
		
		# REEMPLAZAR CARATERES RAROS POR N
		$conservar = '0-9a-z '; // juego de caracteres a conservar
		$regex = sprintf('~[^%s]++~i', $conservar); // case insensitive
		$dato = preg_replace($regex, 'N', $dato);	
		
		# REEMPLAZAR CARATER � POR N
		$patron = '/�/';
		$dato = preg_replace($patron, '', utf8_decode($dato));
		
		return $dato;
	}
	
	
	
	public static function remove_empty($dato){
		# remover vacios
		$dato = (string) $dato;
		$patron = '/ /';
		$dato = preg_replace($patron, '', $dato);
		return $dato;
	}
}