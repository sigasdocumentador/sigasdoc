<?php
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
include_once $raiz.DIRECTORY_SEPARATOR. 'config.php';

include_once $raiz.DIRECTORY_SEPARATOR.'asopagos'.DIRECTORY_SEPARATOR.'generar_plano_novedad'.DIRECTORY_SEPARATOR.'handler'.DIRECTORY_SEPARATOR.'handler_archivo.class.php';
include_once 'no_monetaria' . DIRECTORY_SEPARATOR . 'nov_bloqueo_tarjeta.class.php';
include_once 'no_monetaria' . DIRECTORY_SEPARATOR . 'nov_modif_datos_basic.class.php';
include_once 'monetaria' . DIRECTORY_SEPARATOR . 'nov_abono_subsidio.class.php';
include_once 'monetaria' . DIRECTORY_SEPARATOR . 'nov_abono_por_rever.class.php';
include_once 'monetaria' . DIRECTORY_SEPARATOR . 'nov_reverso.class.php';

$tipoPlano = $_POST['tipo_plano'];
$encriptar = $_POST['encriptar'];
$usuario = $_SESSION['USUARIO'];
$arrLogProceso = array('uri_plano'=>null,'nombre_plano'=>'','error'=>0,'descripcion'=>'','tipo_logs'=>'','logs'=>array(),'datos'=>null);

switch ($tipoPlano) {
	case "nov_bloqueo_tarjeta": # BLOQUEO DE TARJETAS (NOV 4)
		
		try {
			
			$objNovBloqueoTarjeta = new NovBloqueoTarjeta();
			
			$arrResultado = $objNovBloqueoTarjeta->get_plano($ruta_generados, $encriptar, $usuario);
			
			$arrLogProceso['uri_plano'] = $arrResultado['uri_plano'];
			$arrLogProceso['nombre_plano'] = $arrResultado['nombre_plano'];
			$arrLogProceso['error'] = $arrResultado['error'];
			$arrLogProceso['descripcion'] = $arrResultado['descripcion'];
			$arrLogProceso['tipo_logs'] = 'NO_MONETARIA';
			$arrLogProceso['logs'] = $arrResultado['logs'];

		}catch(Exception $e){
			$arrLogProceso['error'] = 1;
			$arrLogProceso['descripcion'] = 'Error desconocido';
		}
		
	break;
	case "nov_modif_datos_basic": # MODIFICACION DATOS BASICOS (NOV 6)
		try {
			
			$objNovModifDatosBasic = new NovModifDatosBasic();
			
			$arrResultado = $objNovModifDatosBasic->get_plano($ruta_generados, $encriptar, $usuario);
			
			$arrLogProceso['uri_plano'] = $arrResultado['uri_plano'];
			$arrLogProceso['nombre_plano'] = $arrResultado['nombre_plano'];
			$arrLogProceso['error'] = $arrResultado['error'];
			$arrLogProceso['descripcion'] = $arrResultado['descripcion'];
			//$arrLogProceso['tipo_logs'] = 'NO_MONETARIA';
			$arrLogProceso['logs'] = $arrResultado['logs'];

		}catch(Exception $e){
			$arrLogProceso['error'] = 1;
			$arrLogProceso['descripcion'] = 'Error desconocido';
		}
		break;
	case "nov_abono_subsidio": # ABONO SUBSIDIO CUOTA MONETARIA
		try {
				
			$objNovAbonoSubsidio = new NovAbonoSubsidio();
				
			$arrResultado = $objNovAbonoSubsidio->get_plano($ruta_generados, $encriptar, $usuario);
				
			$arrLogProceso['uri_plano'] = $arrResultado['uri_plano'];
			$arrLogProceso['nombre_plano'] = $arrResultado['nombre_plano'];
			$arrLogProceso['error'] = $arrResultado['error'];
			$arrLogProceso['descripcion'] = $arrResultado['descripcion'];
			//$arrLogProceso['tipo_logs'] = 'MONETARIA';
			$arrLogProceso['logs'] = $arrResultado['logs'];
	
		}catch(Exception $e){
			$arrLogProceso['error'] = 1;
			$arrLogProceso['descripcion'] = 'Error desconocido';
		}
		break;
	case "nov_abono_por_reverso": # ABONO POR REVERSO
		try {
				
			$objNovAbonoPorRever = new NovAbonoPorRever();
				
			$arrResultado = $objNovAbonoPorRever->get_plano($ruta_generados, $encriptar, $usuario);
				
			$arrLogProceso['uri_plano'] = $arrResultado['uri_plano'];
			$arrLogProceso['nombre_plano'] = $arrResultado['nombre_plano'];
			$arrLogProceso['error'] = $arrResultado['error'];
			$arrLogProceso['descripcion'] = $arrResultado['descripcion'];
			//$arrLogProceso['tipo_logs'] = 'MONETARIA';
			$arrLogProceso['logs'] = $arrResultado['logs'];
	
		}catch(Exception $e){
			$arrLogProceso['error'] = 1;
			$arrLogProceso['descripcion'] = 'Error desconocido';
		}
		break;
	case "nov_reversos": # REVERSOS
		try {
				
			$objNovReverso = new NovReverso();
				
			$arrResultado = $objNovReverso->get_plano($ruta_generados, $encriptar, $usuario);
				
			$arrLogProceso['uri_plano'] = $arrResultado['uri_plano'];
			$arrLogProceso['nombre_plano'] = $arrResultado['nombre_plano'];
			$arrLogProceso['error'] = $arrResultado['error'];
			$arrLogProceso['descripcion'] = $arrResultado['descripcion'];
			//$arrLogProceso['tipo_logs'] = 'MONETARIA';
			$arrLogProceso['logs'] = $arrResultado['logs'];
	
		}catch(Exception $e){
			$arrLogProceso['error'] = 1;
			$arrLogProceso['descripcion'] = 'Error desconocido';
		}
		break;
}

# Preparar el archivo para ser descargado
if($arrLogProceso['error']==0){
	$_SESSION['ENLACE']=$arrLogProceso['uri_plano'];
	$_SESSION['ARCHIVO']=$arrLogProceso['nombre_plano'];
}

echo json_encode($arrLogProceso);
?>