<?php
/* autor:       Orlando Puentes
 * fecha:       Julio 23 de 2010
 * objetivo:    
 */
date_default_timezone_set('America/Bogota');
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'p.definiciones.class.php';
$objClase=new Definiciones();
$fecha=date("m/d/Y");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="es">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>::Consulta Consumos::</title>
<link rel="stylesheet" type="text/css" href="../../css/themes/blue/style.css" />
<link type="text/css" href="../../css/Estilos.css" rel="stylesheet"/>
<link href="../../css/marco.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="../../css/css.1.8/jquery-ui-1.8.17.custom.css" />


<script type="text/javascript" src="../../js/1.6/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="../../js/tablesorter/jquery-latest.js"></script>
<script type="text/javascript" src="../../js/tablesorter/jquery.tablesorter.js"></script>
<script type="text/javascript" src="../../js/tablesorter/jquery.tablesorter.pager.js"></script>


<script type="text/javascript" src="../../js/comunes.js"></script>
<script type="text/javascript" src="js/consultaConsumos.js"></script>
<script type="text/javascript">
$(function() {
		$("#tConsumos")
			.tablesorter({widthFixed: true, widgets: ['zebra']})
			.tablesorterPager({container: $("#pager")});
	});
</script>
</head>
<body>

<center>
<table width="95%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td class="arriba_iz" >&nbsp;</td>
<td class="arriba_ce" ><span class="letrablanca">::Consulta Consumos&nbsp;::</span></td>
<td class="arriba_de" >&nbsp;</td>
</tr>
<tr>
<td class="cuerpo_iz">&nbsp;</td>
<td class="cuerpo_ce">&nbsp;</td>
<td class="cuerpo_de">&nbsp;</td>
<tr>
<tr>
<td class="cuerpo_iz" >&nbsp;</td>
<td class="cuerpo_ce" ><table width="100%" border="0" class="tablero">
  <tr>
    <td width="25%">Bono a consultar</td>
    <td width="25%"><input type="text" class="box1" name="bono" id="bono" onblur="buscarBono();" /></td>
    <td width="25%">&nbsp;</td>
    <td width="25%">&nbsp;</td>
  </tr>
  <tr>
    <td>Nombres</td>
    <td colspan="3" id="tdNombre">&nbsp;</td>
    </tr>
  <tr>
    <td>Estado</td>
    <td id="tdEstado">&nbsp;</td>
    <td>Saldo</td>
    <td id="tdSaldo">&nbsp;</td>
  </tr>
</table></td>
<td class="cuerpo_de" >&nbsp;</td>
<tr>
<tr>
<td class="cuerpo_iz" >&nbsp;</td>
<td class="cuerpo_ce" >&nbsp;</td>
<td class="cuerpo_de" >&nbsp;</td>
<tr>
<td class="abajo_iz" >&nbsp;</td>
<td class="abajo_ce" ></td>
<td class="abajo_de" >&nbsp;</td>
</tr>
</table>
<br />
<table id="tConsumos" class="tablesorter" >
<thead>
<tr>
<th >Fecha Trans</th>
<th >Hora Trans</th>
<th >Valor</th>
<th >Dispositivo</th>
<th >Descripcion</th>
</tr>
</thead>
<tbody>

</tbody>
</table>
<div id="pager" class="pager">
	<form>
		<img src="../../imagenes/icons/first.png" class="first"/>
		<img src="../../imagenes/icons/prev.png" class="prev"/>
		<input type="text" class="pagedisplay"/>
		<img src="../../imagenes/icons/next.png" class="next"/>
		<img src="../../imagenes/icons/last.png" class="last"/>
		<select class="pagesize">
			<option selected="selected"  value="10">10</option>
			<option value="20">20</option>
			<option value="30">30</option>
			<option  value="40">40</option>
		</select>
	</form>
</div>
</center>
</body>

</html>
