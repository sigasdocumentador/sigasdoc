URL=src();
nuevo=0;
var idp=0;

$(document).ready(function(){
$("#codigoBarras").focus();
//Dialog Colaboracion en linea
  $("#dialog-form2").dialog({
		autoOpen: false,
		height: 400,
		width: 500,
		draggable:false,
		modal: true,
		buttons: {
		    'Enviar': function() {
			var bValid = true;
			var campo=$('#notas').val();
			var campo0=$.trim(campo);
			if (campo0==""){
				$(this).dialog('close');
					return false;
				}
			var campo1=$('#usuario').val();
			var campo2="asignarTarjeta.php";
			$.post(URL+'phpComunes/colaboracion.php',{v0:campo0,v1:campo1,v2:campo2},
			function(datos){
				if(datos=='1'){
					alert("Su comentario fue enviado correctamente!. Gracias por participar en el mejoramiento de SIGAS");
				}else{
					alert(datos);
				}
		});
		$(this).dialog('close');
		},
		Cancelar: function() {
			$(this).dialog('close');
			$("#dialog-form2").dialog("close");
		}
	}
  });
   //Dialog ayuda
  $("#ayuda").dialog({
		 	      autoOpen: false,
			      height: 450,
			      width: 700,
			      modal:true,
			      open: function(evt, ui){
					$('#ayuda').html('');
					$.get('../../help/tarjetas/imprimirTarjeta.html',function(data){
					$('#ayuda').html(data);
					});
			      }
});
});//ready
//----------------------------------------VENTANA DE DIALOGO DE AYUDA
function mostrarAyuda(){
	$("#ayuda").dialog('open');
}
//----------------------------------------VENTANA DE DIALOGO DE MANUAL
function notas(){
	$("#dialog-form2").dialog('open');
}
//---------------------------------------BUSCAR DATOS-PERSONA
function buscarPersona(numero){
	var idtd=$("#tipoD").val();
	var num=$("#numero").val();
	  $.ajax({
		  url:URL+'phpComunes/pdo.buscar.persona.php',
		  type:"POST",
		  dataType:"json",
		  data:{v0:idtd,v1:num,v2:6},
		  async:false,
		  success:function(datos){
			if(datos==0){
				MENSAJE("No existe la persona!");
				return false;
			}
			$("#nombreCompleto").val(datos[0].nombrecorto);
			idp=datos[0].idpersona;
		  },
		  complete: function(){
		  	 $.ajax({
		  		url:URL+'phpComunes/pdo.buscar.tarjeta.act.php',
		  		type:"POST",
		  		dataType:"json",
		  		data:{v0:idp},
		  		async:false,
		  		success:function(data){
				if(data==0){
					MENSAJE("La persona no tiene tarjeta asignada!");
					return false;
				}
				$("#tarjeta").val(data[0].bono);
				$("#barra").val(data[0].codigobarra);
	//solo activas
				}
				});
		  }
	  });//ajax
} 

function generar(){
	var nombre=$("#nombreCompleto").val();
	var tarjeta=$("#tarjeta").val();
	
	$("#titulo1").html("<img src='../../imagenes/ajax-loader.gif'>");
	$.ajax({
		url: 'generarPlano.php',
		type: "POST",
		async: false,
		data: {v0:nombre,v1:tarjeta},
		success: function(datos){
			if(datos==1){
				$("#titulo1").html("<br><label style='cursor:pointer' onClick='descargar();'><img src=../../imagenes/descargarArchivo.png width=143 height=30 /></label>");
			}
			if(datos==2){
				$("#titulo1").html("");
				alert("Hay registros sin fecha de nacimiento, no se puede procesar el archivo!")
			}
			if(datos==3){
				$("#titulo1").html("");
				alert("No hay registros, no se puede procesar el archivo!")
			}
		}
	})
	}

function descargar(){
	var url=URL+'phpComunes/descargaPlanos.php';
	window.open(url);
	}