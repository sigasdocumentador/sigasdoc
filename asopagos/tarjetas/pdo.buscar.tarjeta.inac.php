<?php 
/*
* @autor:      Ing. Orlando Puentes
* @fecha:      04/09/2010
* objetivo:
*/

ini_set("display_errors",'1');
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
$c0=$_REQUEST['v0'];
$sql="SELECT a102.idtarjeta,a102.bono,a102.idpersona,a102.estado,a102.idetapa,a102.saldo,a102.fechaetapa
		,a91.detalledefinicion,a113.codigobloqueo,c91.detalledefinicion AS bloqueo,a120.nota,a113.usuario
		FROM aportes102 a102 
			LEFT JOIN aportes091 a91 ON a102.idetapa=a91.iddetalledef
			LEFT JOIN aportes113 a113 ON a113.idtarjeta=a102.idtarjeta
			LEFT JOIN aportes091 c91 ON a113.codigobloqueo=c91.iddetalledef
			LEFT JOIN aportes120 a120 ON a120.idtarjeta=a102.idtarjeta 
	  	WHERE idpersona=$c0";
$result=$db->querySimple($sql);
$filas=array();
$con=0;
while ($row=$result->fetch()){
	$filas[]=$row;
	$con++;
}
if ($con==0){
	echo 0;
}
else {
	echo json_encode($filas);
}
?>
