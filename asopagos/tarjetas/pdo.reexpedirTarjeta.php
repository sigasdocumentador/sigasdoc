<?php
// @autor:		Orlando Puentes A
// @fecha:		mayo 31/2010
// @formulario:	guardar datos persona - modulo empresa
ini_set("display_errors",'1');
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'ean13.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
$campo1 = $_REQUEST['v1'];		//idpersona
$campo2 = $_REQUEST['v2'];		//negocio
$campo3 = $_REQUEST['v3'];		//tipo
$campo4 = $_REQUEST['v4'];		//cedtra

$campo6 = $_SESSION['USUARIO'];
$agencia=$_SESSION['AGENCIA'];
$sec =$agencia;

/*
if ( $sec=='04'){
	$sec='01';
}
*/
//bono, idpersona, precodigo, codigobarra, programa, tipo, usuario

$sql="SELECT COUNT(*) AS cuenta FROM aportes101 WHERE idpersona=$campo1 AND estado='A'";
$rs=$db->querySimple($sql);
$row=$rs->fetch();
$cuenta=$row['cuenta'];
if($cuenta > 0){
	echo 1;
    exit (1);		// ya existe una tarjeta
}

$sql="Select count(*) as cuenta from aportes100 where asignada='N' AND plastico='S' and agencia='$sec' and estado='A'";
$rs=$db->querySimple($sql);
$row=$rs->fetch();
$cuenta=$row['cuenta'];

if($cuenta==0){
	echo 2;
    exit (2);  //No hay plasticos disponibles
}
$sql="SELECT top 1 idbono,bono,codigobarra FROM aportes100 WHERE asignada='N' AND plastico='S' and agencia='$sec' and estado='A'";
$rs=$db->querySimple($sql);
$row=$rs->fetch();
$bono=$row['bono'];
$idbono=$row['idbono'];
$barra=$row['codigobarra'];
$db->inicioTransaccion();
$sql="Update aportes100 set asignada='S',idpersona=$campo1 where idbono=$idbono";
$rs=$db->queryActualiza($sql);
if($rs<1){
	$db->cancelarTransaccion();
	echo 3;
	exit(3);	//No se pudo actualizar el maestro de tarjetas
}

$sql="SELECT TOP 1 periodo FROM aportes012 WHERE procesado='S' ORDER BY periodo desc";
$rs=$db->querySimple($sql);
$row=$rs->fetch();
$periodo=$row['periodo'];

$sql="Select idtarjeta,saldo from aportes101 where estado <>'A' and idpersona=$campo1";
$rs=$db->querySimple($sql);
$row=$rs->fetch();
$idbono2=intval($row['idtarjeta']);	
$saldo=$row['saldo'];
if($idbono2<1){
	//revarsar bono  ojooooooo
	echo 6;
	exit(6);	//No hay bono bobleado
}
$cad='';
$sql="INSERT INTO aportes101(bono, idpersona, precodigo, codigobarra, programa, tipo, fechaexpedicion, entregada, estado, ubicacion, idetapa, saldo, usuario, fechasistema,fechavence,fechaetapa,fechasolicitud) VALUES('$bono',$campo1,'0','$barra','$campo2','$campo3',cast(getdate() as date),'N','A','$agencia',61, 0,'$campo6',cast(getdate() as date),'2999/12/31',cast(getdate() as date),cast(getdate() as date))";
$cad .= $sql."<br>";
$rs=$db->queryInsert($sql,'aportes101');
$idtarjeta=intval($rs);
if($idtarjeta < 1){
	$db->cancelarTransaccion();
	echo 4;
	exit(4);	//No se pudo asignar la tarjeta
	}
// actualizar maestro novedad 06
$sql="Insert into aportes108 (idtarjeta,procesado,usuario,fechasistema,codigonovedad) values($idtarjeta,'N','$campo6',cast(getdate() as date),'06')";
$cad .= $sql."<br>";
$rs=$db->queryInsert($sql, 'aportes108');
$x=intval($rs);
if($x < 1){
	$db->cancelarTransaccion();
	echo 5;
	exit(5);	// No se pudo actualizar el maestro ASOPAGOS
}
//novedad02    REVERSO
$saldo=intval($saldo);
$sql="INSERT INTO aportes112 (idtarjeta,valor,idnota,procesado,fechasistema,usuario,idmotivo,naturaleza) VALUES($idbono2, $saldo, 0, 'N', cast(getdate() as date),'$campo6', 2881, '0')";
$cad .= $sql."<br>";
$rs=$db->queryInsert($sql,'aportes112');
$x=intval($rs);
if($x < 1){
	$db->cancelarTransaccion();
	echo 5;
	exit(5);	// No se pudo actualizar el maestro ASOPAGOS
}
//traslado valores
$sql="INSERT INTO aportes110(iddesde,idpara,valor,procesado,usuario,fechasistema) VALUES($idbono2,$idtarjeta,$saldo,'N','$campo6',cast(getdate() as date))";
$cad .= $sql."<br>";
$rs=$db->queryInsert($sql,'aportes110');
$x=intval($rs);
if($x < 1){
	$db->cancelarTransaccion();
	echo 7;
	exit(7);	// No se pudo realizar el traslado del saldo 
}
//historico
$sql="Insert into aportes102 select * from aportes101 where idtarjeta=$idbono2";
$cad .= $sql."<br>";
$rs=$db->queryInsert($sql,'aportes110');
$x=intval($rs);
if($x < 1){
	$db->cancelarTransaccion();
	echo 8;
	exit(8);	// No se pudo pasar a historico! 
}
$sql="Delete from aportes101 where idtarjeta=$idbono2";
$cad .= $sql."<br>";
$rs=$db->queryActualiza($sql);
if($rs<1){
	$db->cancelarTransaccion();
	echo 9;
	exit(9);	//No se pudo borrar la tarjeta boqueada!
}
$db->confirmarTransaccion();	
echo 0;	
?>