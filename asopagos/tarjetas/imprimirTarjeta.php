<?php

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

if ( !isset( $_SESSION ['USUARIO'] ) ) {session_start();}
if(empty($_SESSION['LOGUIADO'])){
	echo "no hay session!!";
	exit();
}
$raiz=$_SESSION['RAIZ'];
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'session.php';
session();
$url=$_SERVER['PHP_SELF'];
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'auditoria.php';
auditar($url);
include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'p.definiciones.class.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';
$objClase=new Definiciones();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Imprimir Tarjeta::</title>
<link href="../../css/Estilos.css" rel="stylesheet" type="text/css" />
<link type="text/css" href="../../css/estilo_tablas.css" rel="stylesheet"/>
<link type="text/css" href="../../css/formularios/base/ui.all.css" rel="stylesheet" />
<link href="../../css/marco.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../../js/jquery-1.4.2.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.core.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.dialog.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.datepicker.js"></script>
<script type="text/javascript" src="../../js/comunes.js"></script>
<script type="text/javascript" src="js/imprimirTarjeta.js"></script>
<script type="text/javascript">
shortcut.add("Shift+F",function() {
		var URL=src();
		var url=URL+"aportes/trabajadores/consultaTrabajador.php";
    	window.open(url,"_blank");
    },{
	'propagate' : true,
	'target' : document 
        });        
</script>
</head>

<body>
<br /><br />
<table border="0" align="center" cellpadding="0" cellspacing="0" width="80%">
  <tbody>
 
  <!-- ESTILOS SUPERIOR TABLA-->
  <tr>
    <td width="13" height="29" class="arriba_iz">&nbsp;</td>
    <td class="arriba_ce"><span class="letrablanca">::&nbsp;Imprimir Tarjeta&nbsp;::</span></td>
    <td width="13" class="arriba_de" align="right">&nbsp;</td>
  </tr>
  
  <!-- ESTILOS ICONOS TABLA-->
  <tr>
   <td class="cuerpo_iz">&nbsp;</td>
   
	<td class="cuerpo_ce">
    <img src="<?php echo URL_PORTAL; ?>imagenes/menu/informacion.png" alt="Ayuda" width="16" height="16" style="border:none; cursor:pointer" title="Manual" onclick="mostrarAyuda();" />
   <img src="<?php echo URL_PORTAL; ?>imagenes/menu/notas.png" alt="Soporte" width="16" height="16" style="cursor: pointer" title="Colaboraci&oacute;n en l&iacute;nea" onclick="notas();" />
    <td class="cuerpo_de">&nbsp;</td>
  </tr>
  
  <!-- ESTILOS MEDIO TABLA-->
  <tr>
    <td class="cuerpo_iz">&nbsp;</td>
    <td class="cuerpo_ce">
   <table border="0" cellpadding="5" align="center" class="tablero" width="90%">
<tr>
<td width="17%">Tipo Documento</td>
<td width="33%"><select name="tipoD" id="tipoD" class="box1">
<option value="1" selected="selected">Cedula ciudadania</option>
<option value="2">Tarjeta Identidad</option>
<option value="3">Pasaporte</option>
<option value="4">Cedula de Extranjeria</option>
</select>
</td>
<td>Número</td>
<td><input id="numero" name="numero" type="text" class="box1" onblur="buscarPersona(this.value)" /></td>
</tr>
<tr>
  <td>Nombre Realce</td>
  <td colspan="3"  >
    <input type="text" name="nombreCompleto" id="nombreCompleto"  class="boxlargo"/></td>
  </tr>
  <tr>
  <td>Número Tarjeta</td>
  <td><input id="tarjeta" type="text" class="boxmediano" style="font-size: 14px" /></td>
  <td>Código de Barras</td>
  <td><input id="barra" type="text" class="box1" /></td>
</tr>
</table>
	<div></div>
    <div id="boton" align="center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>

    </td>
    <td class="cuerpo_de">&nbsp;</td>
  </tr>
  <!-- CONTENIDO TABLA-->
  <tr>
    <td class="cuerpo_iz">&nbsp;</td>
    <td class="cuerpo_ce">
    <center>
      <div id="titulo1"><label style="cursor:pointer" class="RojoGrande" onClick="generar();">Generar Archivo</label></div>
    </center>
    </td>
    <td class="cuerpo_de">&nbsp;</td>
  </tr>
  
  <!-- ESTILOS PIE TABLA-->
  <tr>
    <td height="34" class="abajo_iz" >&nbsp;</td>
    <td class="abajo_ce" ></td>
    <td class="abajo_de" >&nbsp;</td>
  </tr>
  
</tbody>
</table>

<!-- colaboracion en linea -->
<div id="dialog-form2" title="Colaboraci&oacute;n en l&iacute;nea">
<p>Por favor diligencie este formulario para enviar comentarios, errores o falencias encontradas en el proceso. M&aacute;ximo 250 caracteres </p>
<label>Tus comentarios:</label><br />
<textarea name="notas" id="notas" cols="60" rows="10"></textarea>
</div>

<!-- Manual Ayuda -->
<div id="ayuda" title="::Manual Imprimir Tarjeta::"></div>
<input id="idPersona" type="hidden"  />
</body>
</html>