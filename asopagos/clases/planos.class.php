<?php
/* autor:       orlando puentes
 * fecha:       14/10/2010
 * objetivo:    
 */
date_default_timezone_set('America/Bogota');
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;


include_once $raiz . DIRECTORY_SEPARATOR. 'rsc' . DIRECTORY_SEPARATOR . 'conexion.class.php';

class Planos{
	//constructor
	/*var $con;
	function Planos(){
		$this->con=new DBManager;
	}*/
	
	
	/************************************
	 *  ATRIBUTOS ENTIDADES			    *
	***********************************/
	private static $conPDO = null;
	
	private $con;
	private $con2;
	private $fechaSistema;
	
	function __construct(){
		$this->con=new DBManager;
		$this->con2=new DBManager;
	
		try{
			self::$conPDO = IFXDbManejador::conectarDB();
			if( self::$conPDO->conexionID==null ){
				throw new Exception(self::$conPDO->error);
			}
		}catch(Exception $e){
			echo $e->getMessage();
			exit();
		}
	}
	
	function inicioTransaccion(){
		self::$conPDO->inicioTransaccion();
	}
	function cancelarTransaccion(){
		self::$conPDO->cancelarTransaccion();
	}
	function confirmarTransaccion(){
		self::$conPDO->confirmarTransaccion();
	}
	
function plano_bloqueo(){
	if($this->con->conectar()==true){
		$sql="SELECT aportes113.idbloqueo,aportes113.idtarjeta,aportes113.fechasistema,aportes015.identificacion, aportes015.pnombre,aportes015.snombre, aportes015.papellido, aportes015.sapellido, aportes101.bono,aportes101.saldo FROM aportes113 INNER JOIN aportes101 ON aportes113.idtarjeta=aportes101.idtarjeta INNER JOIN aportes015 ON aportes101.idpersona=aportes015.idpersona WHERE procesado='N'";
		return mssql_query($sql,$this->con->conect);
		}
}	

function plano_reverso(){
	if($this->con->conectar()==true){
		$sql="SELECT aportes112.idreverso, aportes112.idtarjeta, aportes112.fechasistema, aportes015.identificacion, aportes015.pnombre, aportes015.snombre,aportes015.papellido,aportes015.sapellido, aportes101.bono,aportes112.valor FROM aportes112 INNER JOIN aportes101 ON aportes112.idtarjeta=aportes101.idtarjeta INNER JOIN aportes015 ON aportes101.idpersona=aportes015.idpersona WHERE procesado='N'";
		return mssql_query($sql,$this->con->conect);
		}
}	

function plano_suspencion(){
	if($this->con->conectar()==true){
		$sql="SELECT aportes111.idsuspencion, aportes111.idtarjeta, aportes111.fechasistema, aportes015.identificacion, aportes015.pnombre, aportes015.snombre,aportes015.papellido,aportes015.sapellido, aportes101.bono,aportes101.saldo FROM aportes111 INNER JOIN aportes101 ON aportes111.idtarjeta=aportes101.idtarjeta INNER JOIN aportes015 ON aportes101.idpersona=aportes015.idpersona WHERE procesado='N'";
		return mssql_query($sql,$this->con->conect);
		}
}	

function plano_transferencia(){
	if($this->con->conectar()==true){
		$sql="SELECT aportes110.idtraslado, aportes110.idpara, aportes110.fechasistema, aportes015.identificacion, aportes015.pnombre, aportes015.snombre,aportes015.papellido,aportes015.sapellido, aportes101.bono,aportes110.valor  FROM aportes110 INNER JOIN aportes101 ON aportes110.idpara=aportes101.idtarjeta INNER JOIN aportes015 ON aportes101.idpersona=aportes015.idpersona WHERE procesada='N'";
		return mssql_query($sql,$this->con->conect);
		}
}	

function plano_habilitacion(){
	if($this->con->conectar()==true){
		$sql="SELECT aportes109.idhabilitacion, aportes109.idtarjeta, aportes109.fechasistema, aportes015.identificacion, aportes015.pnombre, aportes015.snombre,aportes015.papellido,aportes015.sapellido, aportes101.bono,aportes101.saldo FROM aportes109 INNER JOIN aportes101 ON aportes109.idtarjeta=aportes101.idtarjeta INNER JOIN aportes015 ON aportes101.idpersona=aportes015.idpersona WHERE procesado='N'";
		return mssql_query($sql,$this->con->conect);
		}
}
function marcar_bloqueos($idb){
if($this->con->conectar()==true){
		$sql="Update aportes113 set procesado='S',fechaproceso=cast(getdate() as date)  where idbloqueo=$idb";
		return mssql_query($sql,$this->con->conect);
		}
}	

function marcar_reverso($idb){
if($this->con->conectar()==true){
		$sql="Update aportes112 set procesado='S',fechaproceso=cast(getdate() as date)  where idreverso=$idb";
		return mssql_query($sql,$this->con->conect);
		}
}	

function marcar_suspencion($idb){
if($this->con->conectar()==true){
		$sql="Update aportes111 set procesado='S',fechaproceso=cast(getdate() as date)  where idsuspencion=$idb";
		return mssql_query($sql,$this->con->conect);
		}
}	

function marcar_habilitacion($idb){
if($this->con->conectar()==true){
		$sql="Update aportes109 set procesado='S',fechaproceso=cast(getdate() as date)  where idhabilitacion=$idb";
		return mssql_query($sql,$this->con->conect);
		}
}	

function marcar_transferencia($idb){
if($this->con->conectar()==true){
		$sql="Update aportes110 set procesada='S',fechaproceso=cast(getdate() as date)  where idtraslado=$idb";
		return mssql_query($sql,$this->con->conect);
		}
}

/*function comerciosProcesarCruces($arrids53){
	$sql="UPDATE aportes153 SET conciliado='S', estado='N'
	,fechasistema=CAST(GETDATE() AS DATE) WHERE conciliado='N' AND idcomercio IN ($arrids53)";
	$statement = self::$conPDO->conexionID->prepare($sql);
	$guardada = $statement->execute();
	return $guardada == false ? 0 : 1;
}

function comerciosActualizarCruces($archivo){
	$sql="UPDATE aportes153 SET conciliado='S', estado='S'
	,fechasistema=CAST(GETDATE() AS DATE) WHERE conciliado='N' AND archivo='$archivo'";
	$statement = self::$conPDO->conexionID->prepare($sql);
	$guardada = $statement->execute();
	return $guardada == false ? 1 : 0;
}*/

function consecutivoProcesarCruces($nomArchivo,$nuevo_consecutivo){
	$sql="UPDATE aportes149 SET consecutivo=$nuevo_consecutivo, fechasistema=CAST(GETDATE() AS DATE) WHERE archivo='$nomArchivo'";
	$statement = self::$conPDO->conexionID->prepare($sql);
	$guardada = $statement->execute();
	return $guardada == false ? 0 : 1;
}

}