<?php
/* autor:       orlando puentes
 * fecha:       14/10/2010
 * objetivo:    
 */
date_default_timezone_set('America/Bogota');
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'rsc' . DIRECTORY_SEPARATOR . 'conexion.class.php';

class Tarjeta{
	//constructor
	var $con;
	var $con2;
	var $fechaSistema;
	function Tarjeta(){
		$this->con=new DBManager;
		$this->con2=new DBManager;//conexion informa web
	}

	function tarjetas_activas(){
		if($this->con->conectar()==true){
			$sql="select idtarjeta,bono,codigobarra from aportes101 where estado='A'";
			return mssql_query($sql,$this->con->conect);
		}
	}

	function insertar_movimiento($campos){
		if($this->con->conectar()==true){
			$sql="INSERT INTO aportes114 (item,convenio,bono,valorbono,fechamovimiento, comercio, terminal, tipomovimiento, valormovimiento,control,fechamov,fechasistema,usuario) VALUES (".$campos[0].",'".$campos[1]."','".$campos[2]."','".$campos[3]."','".$campos[4]."','".$campos[5]."','".$campos[6]."','".$campos[7]."','".$campos[8]."','".$campos[9]."','".$campos[10]."',cast(getdate() as date),'".$campos[11]."')";
			//echo $sql;
			return mssql_query($sql,$this->con->conect);
		}
	}

	function insertar_control($campos){
		date_default_timezone_set('America/Bogota');
		$hora=date("h:i:s A");
		if($this->con->conectar()==true){
			$sql="INSERT INTO aportes149 (archivo, tipoarchivo, fechasistema, usuario, registros, totalregistros, registros1, totalregistros1, registros2, totalregistros2, registros3, totalregistros3, registros4, registros5, fechamovimiento,hora) VALUES ('".$campos[0]."','".$campos[1]."',cast(getdate() as date),'".$campos[3]."','".$campos[4]."','".$campos[5]."','".$campos[6]."','".$campos[7]."','".$campos[8]."','".$campos[9]."','".$campos[10]."','".$campos[11]."','".$campos[12]."','".$campos[13]."','".$campos[14]."','$hora')";
			//echo $sql;
			return mssql_query($sql,$this->con->conect);
		}
	}

	function buscar_archivo($id){
		if($this->con->conectar()==true){
			$sql="SELECT * FROM aportes149 WHERE archivo='".$id."'";
			return mssql_query($sql,$this->con->conect);
		}
	}

	function grabar_inventario($id,$us,$ag){
		$fechaSistema=date("m/d/Y");
		if($this->con->conectar()==true){
			$sql="Update aportes101 set entregada='N',ubicacion='$ag',idetapa=63,fecharecepcion='$fechaSistema' where bono='$id'";
			//$result=mssql_query($sql,$this->con->conect);
			$sql="insert into aportes150 (codigobarras,cedtra,usuario,fecha,agencia) values('$id',1,'$us',current,'$ag')";
			echo $sql;
			return mssql_query($sql,$this->con->conect);
		}
	}

	function actualizar_saldo($bono,$valor,$tipo){
		if($this->con->conectar()==true){
			switch($tipo){
				case 1 : $sql="Update aportes101 set saldo=saldo - $valor where bono='$bono'"; break;
				case 2 : $sql="Update aportes101 set saldo=saldo + $valor where bono='$bono'"; break;
				case 3 : $sql="Update aportes101 set saldo=saldo - $valor where bono='$bono'"; break;
			}
			return mssql_query($sql,$this->con->conect);
		}
	}

	function obtener_movimiento($bono){
		if($this->con->conectar()==true){
			$sql="SELECT sum(valormovimiento) AS valor, substring(tipomovimiento FROM 1 FOR 4) as tipom FROM aportes114 WHERE bono='$bono' GROUP BY tipomovimiento";
			return mssql_query($sql,$this->con->conect);
		}
	}

	function saldo_inicial($bono,$saldo){
		if($this->con->conectar()==true){
			$sql="Update aportes101 set saldo=$saldo WHERE bono='$bono'";
			return mssql_query($sql,$this->con->conect);
		}
	}

	function datos_tarjetaXcodigo($codigo){
		if($this->con->conectar()==true){
			$sql = "SET ISOLATION TO DIRTY READ";
			$sucia=mssql_query($sql,$this->con->conect);
			$sql="SELECT identificacion,pnombre,snombre,papellido,sapellido FROM aportes015 LEFT JOIN aportes101 ON aportes101.idpersona=aportes015.idpersona
WHERE codigobarra='$codigo'";
			return mssql_query($sql,$this->con->conect);
		}
	}

	function bonos_nuevos($bono){
		if($this->con->conectar()==TRUE){
			$sql="INSERT INTO aportes100(bono, asignada, estado,agencia) values('$bono','N','A','01')";
			return mssql_query($sql,$this->con->conect);
		}
	}

	function borrar_104($periodo){
		if($this->con->conectar()==TRUE){
			$sql="DELETE FROM aportes104 where periodogiro='$periodo' and tipopago in ('T','E') and procesado='N'";
			return mssql_query($sql,$this->con->conect);
		}
	}

	function agrupar_014($periodo){
		if($this->con->conectar()==TRUE){
			$sql="select top 100 periodoproceso,idtrabajador,sum(valor - descuento) AS valor from aportes014 where periodoproceso='$periodo' and tipopago='T' group by periodoproceso,idtrabajador";
			return mssql_query($sql,$this->con->conect);
		}
	}

	function buscar_cuenta_bono($id){
		if($this->con->conectar()==TRUE){
			$sql="SELECT COUNT(*) as cuenta FROM aportes101 WHERE estado='A' and idpersona=$id";
			return mssql_query($sql,$this->con->conect);
		}
	}

	function buscar_cargo_periodo($id,$p){
		if($this->con->conectar()==TRUE){
			$sql="SELECT sum(valor) AS valor, count(*) AS cuenta FROM aportes104 WHERE idtrabajador=$id AND periodogiro='$p'";
			return mssql_query($sql,$this->con->conect);
		}
	}

	function insert_104($campos){
		if($this->con->conectar()==TRUE){
			$sql="INSERT INTO aportes104(periodogiro, tipopago, idtrabajador, idpersona, idconyuge, valor, procesado, fechasistema, usuario) VALUES('".$campos[0]."', '".$campos[1]."', '".$campos[2]."', '".$campos[3]."', '".$campos[4]."', '".$campos[5]."', '".$campos[6]."', cast(getdate() as date), '".$campos[7]."')";
			return mssql_query($sql,$this->con->conect);
		}
	}

	function insert_115($campos){
		if($this->con->conectar()==TRUE){
			$sql="INSERT INTO aportes115(periodoproceso, tipopago, idtrabajador, valor, procesado)
    VALUES('".$campos[0]."', '".$campos[1]."', '".$campos[2]."', '".$campos[3]."', '".$campos[4]."')";
			return mssql_query($sql,$this->con->conect);
		}
	}

	function update_014($idt){
		if($this->con->conectar()==TRUE){
			$sql="UPDATE aportes014  SET fechatarjeta=cast(getdate() as date), procesado='S'  WHERE idtrabajador=$idt";
			return mssql_query($sql,$this->con->conect);
		}
	}
	function agrupar_014_embargo($periodo){
		if($this->con->conectar()==TRUE){
			$sql="select periodoproceso,idtrabajador,idconyuge,idbeneficiario,sum(valor - descuento) AS valor from aportes014 where periodoproceso='$periodo' and tipopago='T' and embargo='S' group by periodoproceso,idtrabajador,idconyuge,idbeneficiario";
			return mssql_query($sql,$this->con->conect);
		}
	}

	function buscar_cargo_periodo_embargo($idt,$idter,$p){
		if($this->con->conectar()==TRUE){
			$sql="SELECT sum(valor) AS valor, count(*) AS cuenta FROM aportes104 WHERE idtrabajador=$idter AND idpersona=$idt AND periodogiro='$p'";
			return mssql_query($sql,$this->con->conect);
		}
	}

function buscar_tercero($idt,$idb){
		if($this->con->conectar()==TRUE){
			$sql="select idtercero from aportes018 INNER JOIN aportes022 ON aportes018.idembargo=aportes022.idembargo where tipopago='T' and idtrabajador=$idt and idbeneficiario=$idb";
			return mssql_query($sql,$this->con->conect);
		}
	}

function procesar_archivo_1(){
//subsidio cuota monetaria
	if($this->con->conectar()==TRUE){
        $sql="SELECT aportes101.bono, (aportes104.valor * 100) AS valor,idtraslado FROM aportes104 INNER JOIN aportes101 ON aportes104.idtrabajador = aportes101.idpersona WHERE aportes104.procesado='N' AND aportes104.tipopago IN ('T','E')";
        return mssql_query($sql,$this->con->conect);
        }
	}

function procesar_archivo_2(){
//fonede
	if($this->con->conectar()==TRUE){
        $sql="SELECT aportes101.bono, (aportes104.valor * 100) AS valor FROM aportes104 INNER JOIN aportes101 ON aportes104.idtrabajador = aportes101.idpersona WHERE aportes104.procesado='N' AND aportes104.tipopago ='F'";
        return mssql_query($sql,$this->con->conect);
        }
	}

function contar_archivos(){
    if($this->con->conectar()==true){
        $sql="SELECT count(*) AS cuenta FROM aportes149 WHERE tipoarchivo=10 AND fechasistema=CAST(GETDATE() AS DATE)";
        return mssql_query($sql,$this->con->conect);
    }
}

function comprobar_existen($agencia,$tipo,$programa){
    if($this->con->conectar()==true){
        $sql="SELECT count(*) AS cuenta FROM aportes101 INNER JOIN aportes016 ON aportes101.idpersona=aportes016.idpersona WHERE idetapa=61 AND idagencia='$agencia' AND aportes016.tipoformulario=$tipo AND programa='$programa' AND aportes016.auditado='S'";
        //echo $sql;
        return mssql_query($sql,$this->con->conect);
    }
}

function buscar_bonos($agencia,$tipo,$programa){
    if($this->con->conectar()==TRUE){
        $sql="SELECT idtarjeta,bono,aportes101.idpersona,codigobarra,nombrecorto,identificacion,categoria FROM aportes101
INNER JOIN aportes016 ON aportes101.idpersona=aportes016.idpersona
INNER JOIN aportes015 ON aportes101.idpersona=aportes015.idpersona
WHERE idetapa=61 AND idagencia='$agencia' AND aportes016.tipoformulario=$tipo AND programa='$programa' AND auditado='S'";
        return mssql_query($sql,$this->con->conect);
    }
}

function comprobar_archivo($archivo){
    if($this->con->conectar()==true){
        $sql="SELECT count(*) AS cuenta FROM aportes149 WHERE archivo='$archivo'";
        //echo $sql;
        return mssql_query($sql,$this->con->conect);
    }
}

function insert_116($campos){
    if($this->con->conectar()==true){
        $sql="INSERT INTO aportes116(idagencia, idtipo, programa, notas, archivo, registros, estado, usuario, fechasistema)
    VALUES('$campos[0]', $campos[1], '$campos[2]', '$campos[3]', '$campos[4]', 0, 'A', '$campos[5]', cast(getdate() as date))";
        //echo $sql;
        return mssql_query($sql,$this->con->conect);
    }
}

function insert_detalle_116($ids,$bono){
    if($this->con->conectar()==true){
        $sql="INSERT INTO aportes117(idsolicitud, bono)  VALUES( $ids, '$bono')";
        //echo $sql;
        return mssql_query($sql,$this->con->conect);
    }
}

function update_116($ids,$bono){
    if($this->con->conectar()==true){
        $sql="Update aportes116 set registros=$bono where idsolicitud=$ids";
        //echo $sql;
        return mssql_query($sql,$this->con->conect);
    }
}

function update_102($bono){
	if($this->con->conectar()==true){
		$sql="UPDATE aportes101 SET idetapa=62 WHERE bono='$bono'";
		return mssql_query($sql,$this->con->conect);
	}
}

function contar_bono($idpersona){
    if($this->con->conectar()==TRUE){
        $sql="Select count(*) as cuenta from aportes101 where idpersona=$idpersona";
        return mssql_query($sql,$this->con->conect);
    }
}

function buscar_tarjeta($idpersona){
    if($this->con->conectar()==TRUE){
        $sql="SELECT idtarjeta, bono, codigobarra, fechaexpedicion, fechasolicitud, fecharecepcion, fechaentrega, fechavence, entregada,
idtipoentrega, estado, idetapa, saldo, fechaetapa,aportes091.detalledefinicion, e.detalledefinicion AS etapa,
CASE WHEN programa='S' THEN 'SUBSIDIO' WHEN programa='F' THEN 'FONEDE' WHEN programa='H' THEN 'HIPER' END AS programa,
CASE WHEN ubicacion='01' THEN 'NEIVA' WHEN ubicacion='02' THEN 'GARZON' WHEN ubicacion='03' THEN 'PITALITO'
 WHEN ubicacion='04' THEN 'LA PLATA' END AS ubicacion FROM aportes101 INNER JOIN aportes091 ON aportes101.idtipoentrega=aportes091.iddetalledef
INNER JOIN aportes091 e ON aportes101.idetapa=e.iddetalledef WHERE idpersona=$idpersona ORDER BY estado";
        //echo $sql;
        return mssql_query($sql,$this->con->conect);
    }
}

function buscar_reversos($idpersona){
    if($this->con->conectar()==true){
        $sql="SELECT * FROM aportes106 WHERE idtrabajador=$idpersona ORDER BY idreverso ASC";
        return mssql_query($sql,$this->con->conect);
    }
}

function buscar_movimiento($bono,$fecha,$tipo,$valor){
	if($this->con->conectar()==true){
		$sql="SELECT COUNT(*) as cuenta FROM aportes114 where bono='$bono' and fechamovimiento= '$fecha' and tipomovimiento='$tipo' and valormovimiento=$valor";
		return mssql_query($sql,$this->con->conect);
		}
	}

function insert_148($bono,$fecha,$tipo,$valor,$flag){
	if($this->con->conectar()==true){
		$sql="insert into aportes148 (bono,fecha,tipo,valor,estado) values('$bono','$fecha','$tipo',$valor,'$flag')";
		return mssql_query($sql,$this->con->conect);
		}
	}

function borrar_148(){
	if($this->con->conectar()==true){
		$sql="delete from aportes148";
		return mssql_query($sql,$this->con->conect);
		}
	}

function marcar_traslado($idtraslado){
	if($this->con->conectar()==true){
		$sql="update aportes104 set procesado='X' where idtraslado=$idtraslado ";
		return mssql_query($sql,$this->con->conect);
		}
}
	
function buscar_bloqueadas(){
	if($this->con->conectar()==true){
		$sql="SELECT aportes101.bono,aportes015.identificacion,aportes015.pnombre,aportes015.snombre,aportes015.papellido,aportes015.sapellido 
FROM aportes104 INNER JOIN aportes101 ON aportes104.idtrabajador = aportes101.idpersona 
INNER JOIN aportes015 ON aportes101.idpersona=aportes015.idpersona WHERE aportes104.procesado='N' AND aportes104.tipopago IN ('T','E') AND aportes101.estado IN('B')";
		return mssql_query($sql,$this->con->conect);	
	}
}

function buscar_sin_bono(){
	if($this->con->conectar()==true){
		$sql="SELECT aportes015.identificacion,aportes015.pnombre,aportes015.snombre,aportes015.papellido,aportes015.sapellido
FROM aportes104 INNER JOIN aportes015 ON aportes104.idtrabajador=aportes015.idpersona WHERE aportes104.procesado='N' AND idtrabajador NOT IN(SELECT idpersona FROM aportes101)";
		return mssql_query($sql,$this->con->conect);
	}
}
function saldo_cero(){
	if($this->con->conectar()==true){
		$sql="Update aportes101 set saldo=0";
		return mssql_query($sql,$this->con->conect);
		}
}	


function cerrar2(){
	$this->con->cerrarConexion2();
}	
	
function cerrar(){
	$this->con->cerrarConexion();
}	

//Agosto 2011 lara
function buscar_tarjeta_activa($idpersona){
	if($this->con->conectar()==true){
	$sql="select idtarjeta,idpersona,bono,estado from aportes101 where estado in ('A','B') and idpersona=".$idpersona;
	return mssql_query($sql,$this->con->conect);
	}
}

function buscar_tarjeta_activa2($idpersona){
	if($this->con->conectar()==true){
		$sql="select idtarjeta,idpersona,bono,estado from aportes101 where estado in ('A') and idpersona=".$idpersona;
		return mssql_query($sql,$this->con->conect);
	}
}


//Tarjetas Bloqueadas
function buscar_tarjeta_bloqueada($idpersona){
	if($this->con->conectar()==true){
	$sql="select idpersona,bono,estado,saldo,fechaetapa,datediff(dd,fechaetapa,getdate()) diaspasados,datediff(dd,fechaetapa,fechasaldo) diassaldo from aportes101 where estado='B' and idpersona=".$idpersona;
	return mssql_query($sql,$this->con->conect);
	}
}

function guardar_terceros_informa($tipoDoc,$numero,$nombre,$sexo,$direccion,$telefono,$celular,$departamento,$ciudad,$pais,$estado,$tipoCliente,$tipoImpuesto,$retieneRenta,$responsableIva,$usuario){
	if($this->con2->conectarInforma()==true){
	$sql="INSERT INTO nits (nit,clase,nombre,direccion,telefono,ciudad,autoret,tipoclie,depto,pais,estado,resp_iva,t_contrib,sexo,celular,grabador) values (".$numero.",'".$tipoDoc."','".$nombre."','".$direccion."','".$telefono."','".$ciudad."','".$retieneRenta."','".$tipoCliente."','".$departamento."','".$pais."','".$estado."','".$responsableIva."','".$tipoImpuesto."','".$sexo."','".$celular."','".$usuario."')";
	//echo $sql;
	return mssql_query($sql,$this->con2->conect);
	}
}


}
?>
