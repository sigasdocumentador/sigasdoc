<?php
/**
 * autor	Orlando Puentes Andrade
 * fecha	septiembre 2 de 2010
 * objetivo
 */

set_time_limit(0);
date_default_timezone_set("America/Bogota");
ini_set("display_errors",'1');

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$periodo='201209';

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

 
/******************* CARGUE EMBARGOS *******************************/

$sql= "SELECT idtrabajador,idtercero,sum(valor) AS cuenta FROM aportes014 WHERE embargo='S' AND anulado IS NULL AND procesado <> 'N'  GROUP BY idtrabajador,idtercero";
$rs = $db->querySimple($sql);

while($row=$rs->fetch()){
	$valor=$row["cuenta"];
    if($valor==0){
		continue;
	}
	$idtrabajador=$row["idtrabajador"];
	$idtercero=$row["idtercero"];
	$tp='E';
	
	$sql ="INSERT INTO dbo.aportes104 (periodogiro, tipopago, idtrabajador, idpersona, idconyuge, valor, procesado, fechasistema, usuario)
	VALUES ('$periodo', '$tp', $idtercero,$idtrabajador, $idtercero, $valor, 'N', cast(getdate() as date), 'tiopa3')";
	$rs3=$db->queryInsert($sql, 'aportes014');
	echo $rs3.'<br>';
	$sql="update aportes014 set procesado='S' where idtrabajador=$idtrabajador and idtercero=$idtercero and embargo='S'";
	$rs2=$db->queryActualiza($sql);
    }
//fin embarbo

 /**************** SIN EMBARGO ******************************/
$sql="SELECT periodo,idtrabajador,tipopago,sum(valor-descuento) AS valor FROM aportes014
where tipopago='T' and procesado='N' GROUP BY periodo,idtrabajador,tipopago";
    $rs=$db->querySimple($sql);
    while($row=$rs->fetch()){
    	$idp=$row['idtrabajador'];
    	$valor=$row['valor'];
    	$tp=$row['tipopago'];
    	$periodo=$row['periodo'];
    	$sql ="INSERT INTO dbo.aportes104 (periodogiro, tipopago, idtrabajador, idpersona, idconyuge, valor, procesado, fechasistema, usuario)
    	VALUES ('$periodo', '$tp', $idp, 0, 0, $valor, 'N', cast(getdate() as date), 'tiopa2')";
    	$rs=$db->queryInsert($sql, 'aportes104');
    	
    }
  
echo "<br>Proceso terminado";

?>