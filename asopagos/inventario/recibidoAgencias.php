<?php
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$agencia=$_SESSION['AGENCIA'];
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
include_once $raiz.DIRECTORY_SEPARATOR. 'funcionesComunes'. DIRECTORY_SEPARATOR. 'funcionesComunes.php';
include_once $raiz.DIRECTORY_SEPARATOR.'config.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
$sql="Select count(*) as cuenta from aportes100 where estado='T' and agencia='$agencia'";
$rs=$db->querySimple($sql);
$row=$rs->fetch();
$cuenta=$row['cuenta'];
?>
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <title>Procesar archivo</title>
 <link href="../../css/Estilos.css" rel="stylesheet" type="text/css">
 <script language="javascript" src="../../js/jquery-1.4.2.js"></script>
 </head>
 <body>
 
 <br />
 <p align="center" class="RojoGrande" style="margin-first:30px">Hay <?php echo $cuenta; ?> tajetas para ser marcadas como recibidas en la agencia: <?php echo $agencia ?></p>
 <br />
 <br />
 <br />
 <p align="center" style="cursor:pointer" class="RojoGrande" onclick="procesar();">Procesar</p>
</body>
 <script language="javascript">
 function procesar(){
	 $.ajax({
			url: 'marcarAgencia.php',
			type: "POST",
			async: false,
			data: {v0:'03'},
			success: function(datos){
				alert("Se actualizaron "+ datos + "tarjetas");
			}
		})
	 }
</script>
</html>