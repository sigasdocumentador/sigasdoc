<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$url=$_SERVER['PHP_SELF'];
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'auditoria.php';
auditar($url);

include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'p.definiciones.class.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';
$objClase=new Definiciones();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Verificar Tarjeta::</title>
<link href="<?php echo URL_PORTAL; ?>css/Estilos.css" rel="stylesheet" type="text/css" />
<link type="text/css" href="<?php echo URL_PORTAL; ?>css/estilo_tablas.css" rel="stylesheet"/>
<link type="text/css" href="<?php echo URL_PORTAL; ?>css/formularios/base/ui.all.css" rel="stylesheet" />
<link href="<?php echo URL_PORTAL; ?>css/marco.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/formularios/ui/ui.core.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/formularios/ui/ui.tabs.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/formularios/ui/ui.dialog.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/formularios/ui/ui.draggable.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/formularios/ui/ui.resizable.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/effects.Jquery.js"></script>
<script type="text/javascript" src="<?php echo URL_PORTAL; ?>js/comunes.js"></script>
<script type="text/javascript" src="js/validar.js"></script>

<script type="text/javascript">
shortcut.add("Shift+F",function() {
		                                var URL=src();
		var url=URL+"aportes/trabajadores/consultaTrabajador.php";
    	window.open(url,"_blank");
    },{
	'propagate' : true,
	'target' : document 
        });        
</script>

<script type="text/javascript">
$(document).ready(function(){
	$("#ayuda").dialog({
		 	autoOpen: false,
			height: 550, 
			width: 800, 
			open: function(evt, ui){
					$('#ayuda').html('');
					$.get(URL +'help/tarjetas/manualayudaverificar.html',function(data){
							$('#ayuda').html(data);
					})
			 }
 		});
 	});

</script>

<!-- colaboracion en linea  -->

<script type="text/javascript">
	$(function() {
		$("#dialog-form2").dialog("destroy");
		/*var notas = $("#notas"),
			allFields = $([]).add(notas);*/
		
		$("#dialog-form2").dialog({
			autoOpen: false,
			height: 400,
			width: 500,
			modal: true,
			buttons: {
				'Enviar': function() {
					var bValid = true;
					var campo=$('#notas').val();
					var campo0=$.trim(campo);
					if (campo0==""){
						$(this).dialog('close');
						return false;
						}
					var campo1=$('#usuario').val();
					var campo2="validar.php";
					$.post('../../phpComunes/colaboracion.php',{v0:campo0,v1:campo1,v2:campo2}, function(datos){
						if(datos=='1'){
							alert("Su comentario fue enviado correctamente!. Gracias por participar en el mejoramiento de SIGAS");
						}else{
							alert(datos);
						}
				});
				$(this).dialog('close');
				},
				Cancelar: function() {
					$(this).dialog('close');
					$("#dialog-form2").dialog("destroy");
				}
			}
		});
});	
</script>
</head>

<body>
<br /><br />
<table border="0" align="center" cellpadding="0" cellspacing="0" width="80%">
  <tbody>
 
  <!-- ESTILOS SUPERIOR TABLA-->
  <tr>
    <td width="13" height="29" class="arriba_iz">&nbsp;</td>
    <td class="arriba_ce"><span class="letrablanca"><strong>::&nbsp;Verificación&nbsp;::</strong></span></td>
    <td width="13" class="arriba_de" align="right">&nbsp;</td>
  </tr>
  
  <!-- ESTILOS ICONOS TABLA-->
  <tr>
   <td class="cuerpo_iz">&nbsp;</td>
	<td class="cuerpo_ce">  
    <img src="../../imagenes/tabla/spacer.gif" width="1" height="1"> <img src="../../imagenes/menu/informacion.png" width="16" height="16" style="border: none; cursor: pointer" title="Manual" onClick="mostrarAyuda();" />
    <img src="../../imagenes/menu/notas.png" width="16" height="16" style="cursor: pointer" title="Colaboración en línea" onclick="notas();" /> 
    </td>
    <td class="cuerpo_de">&nbsp;</td>
  
  </tr>
  
  <!-- ESTILOS MEDIO TABLA-->
  <tr>
    <td class="cuerpo_iz">&nbsp;</td>
    <td class="cuerpo_ce">
   <table border="0" cellpadding="5" align="center" class="tablero" width="90%">
<tr>
<td width="17%">Código de Barras</td>
<td width="33%"><input id="codigoBarras" type="text" class="box1" onblur="buscarPersona(this.value)" /></td>
<td colspan="2"></td>
</tr>
<tr>
  <td>Nombres</td>
  <td ><label for="nombreCompleto"></label>
    <input type="text" name="nombreCompleto" id="nombreCompleto"  class="box1"/></td>
  <td width="17%">Apellidos</td>
  <td width="33%"><input type="text" name="apellidoCompleto" id="apellidoCompleto"  class="box1"/></td>
  </tr>
  <tr>
  <td>Identificación</td>
  <td><input id="identificacion" type="text" class="box1" /></td>
  <td>Número Tarjeta</td>
  <td><input id="numeroTarjeta" type="text" class="box1" /></td>
</tr>
</table>
	<div></div>
    <td class="cuerpo_de">&nbsp;</td>
  </tr>
  <!-- CONTENIDO TABLA-->
  <tr>
    <td class="cuerpo_iz">&nbsp;</td>
    <td class="cuerpo_ce" align="center">
	<br/>
   	<br/>
    <input type="button" class="ui-state-default" id="siguiente" value="Siguiente" onclick="seguir()" />
    </td>
    <td class="cuerpo_de">&nbsp;</td>
  </tr>
  
  <!-- ESTILOS PIE TABLA-->
  <tr>
    <td height="34" class="abajo_iz" >&nbsp;</td>
    <td class="abajo_ce" ></td>
    <td class="abajo_de" >&nbsp;</td>
  </tr>
  
</tbody>
</table>
<!-- ayuda en linea -->
<div id="ayuda" title="Manual .:. Ayuda Verificar" style="background-image:url(<?php echo URL_PORTAL; ?>imagenes/FondoGeneral0.png)"></div>
<!-- fin ayuda en linea -->

<!-- colaboracion en linea -->

<div id="dialogo-archivo" title="Archivo banco">
<div id="progreso" style="display: none; font-size: 15pt; font-weight: bold;">Procesado(s) <span id="pg">0</span> de <span id="tt"></span> archivo(s)</div>
<div id="log"></div>
</div>
<div id="dialog-form2" title="Colaboraci&oacute;n en l&iacute;nea" style="display:none">
<p>Por favor diligencie este formulario para enviar comentarios, errores o falencias encontradas en el proceso. M&aacute;ximo 250 caracteres </p>
<label>Tus comentarios:</label><br />
<textarea name="notas" id="notas" cols="60", rows="10"></textarea>
</div>
<!-- fin colaboracion -->

<script language="javascript">
function mostrarAyuda(){
	$("#ayuda").dialog('open' );
	}
function notas(){
	$("#dialog-form2").dialog('open');
	}
</script>
</body>
</html>