URL=src();
nuevo=0;
$(document).ready(function(){
$("#codigoBarras").focus();
});//ready

//---------------------------------------BUSCAR DATOS-PERSONA
function buscarPersona(numero){
	if(numero.length>0)
	  $.ajax({
		  url:URL+'asopagos/inventario/consultardatos.php',
		  type:"POST",
		  dataType:"json",
		  data:{v0:numero},
		  success:function(datos){
			if(datos==0){
				MENSAJE("No existe el c&oacute;digo de barras!. Debe marcar la tarjeta como RECHAZADA, para volverla a imprimir.");
				$("#nombreCompleto").val("");
				$("#apellidoCompleto").val("");
				$("#identificacion").val("");
				$("#numeroTarjeta").val("");
				$("#codigoBarras").val("");
				$("#codigoBarras").focus();
				return false;
				
			}
			nom=$.trim(datos.pnombre)+" "+$.trim(datos.snombre);
			apll=$.trim(datos.papellido)+" "+$.trim(datos.sapellido);
			ident=datos.identificacion;
			nt=datos.bono;
			$("#nombreCompleto").val(nom);
     		$("#apellidoCompleto").val(apll);
			$("#identificacion").val(ident);
			$("#numeroTarjeta").val(nt);
			$("#codigoBarras").val();
			$("#siguiente").focus();
		  },
	  });//ajax
}
//----------------------------------SIGUIENTE CODIGO BARRA
function seguir(){
	$("#nombreCompleto").val("");
	$("#apellidoCompleto").val("");
	$("#identificacion").val("");
	$("#numeroTarjeta").val("");
	$("#codigoBarras").val("");
	$("#codigoBarras").focus();
}