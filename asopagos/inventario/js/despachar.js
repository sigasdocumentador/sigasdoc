URL=src();
nuevo=0;
var barraOK;
var idTarjeta;
var limInicial = 0;
var limFinal = 0;
var agencia = 0;

$(document).ready(function(){
	$('#btnProcesar').bind('click',function(){
		$("#boton").html("<img src='../../imagenes/ajax-loader.gif'>");
		limInicial = $("#serieInicio").val();
		limFinal = $("#serieFin").val();
		if(limInicial==limFinal) return false
		agencia = $("#agencia").val();
		$("#tabla_result_despacho tbody").empty();
		
		if(limInicial == '' || limFinal == '' || isNaN(limInicial) || isNaN(limFinal)){
			MENSAJE("Verifique las series.");
		}else{
			$.getJSON(URL+'asopagos/inventario/despachar.php',{serieInicio: limInicial, serieFin: limFinal, agencia: agencia},function(respuesta){
				if(respuesta==0 || respuesta==9){
					alert('No se marcaron como a enviadas a ' + agencia);
					$("#boton2").html("<label class='Rojo'>Error al procesar</label>");
					return false
				}
				
				alert('Se marcaron como despachadas a ' + agencia + ' ' + respuesta + ' tarjetas en blanco!');
				$("#boton2").html("<label style='cursor:pointer' class='RojoGrande' onClick='generarPlano();'>Generar Archivo...</label>");
			});
		}
	});
	
   //Dialog ayuda
  $("#ayuda").dialog({
      autoOpen: false,
      height: 450,
      width: 700,
      modal:true,
      open: function(evt, ui){
		$('#ayuda').html('');
      }
  });
});//ready
//----------------------------------------VENTANA DE DIALOGO DE AYUDA
function mostrarAyuda(){
	$("#ayuda").dialog('open');
}
//----------------------------------------VENTANA DE DIALOGO DE MANUAL
function notas(){
	$("#dialog-form2").dialog('open');
}

function generarPlano(){
	$("#boton2").html("<img src='../../imagenes/ajax-loader.gif'>");
	$.ajax({
		url: 'generarPlano.php',
		type: "POST",
		async: false,
		data: {v0:limInicial,v1:limFinal,v2:agencia},
		success: function(datos){
			if(datos==1){
				$("#boton2").html("<br><label style='cursor:pointer' onClick='descargar();'><img src=../../imagenes/descargarArchivo.png width=143 height=30 /></label>");
			}
			if(datos==9){
				$("#titulo1").html("");
				alert("No se pudo continuar con el proceso!")
			}
		}
	})
	}

function descargar(){
	var url=URL+'phpComunes/descargaPlanos.php';
	window.open(url);
	}