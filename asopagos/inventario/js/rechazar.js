URL=src();
var nuevo=0;
var idtarjeta=0;
var idpersona=0;
var encontrado=0;
var continuar=0;
$(document).ready(function(){

  $("#dialog-form2").dialog({
		autoOpen: false,
		height: 400,
		width: 500,
		draggable:false,
		modal: true,
		buttons: {
		    'Enviar': function() {
			var bValid = true;
			var campo=$('#notas').val();
			var campo0=$.trim(campo);
			if (campo0==""){
				$(this).dialog('close');
					return false;
				}
			var campo1=$('#usuario').val();
			var campo2="empresaSinConv.php";
			$.post(URL+'phpComunes/colaboracion.php',{v0:campo0,v1:campo1,v2:campo2},
			function(datos){
				if(datos=='1'){
					alert("Su comentario fue enviado correctamente!. Gracias por participar en el mejoramiento de SIGAS");
				}else{
					alert(datos);
				}
		});
		$(this).dialog('close');
		},
		Cancelar: function() {
			$(this).dialog('close');
			$("#dialog-form2").dialog("close");
		}
	}
  });
   //Dialog ayuda
  $("#ayuda").dialog({
		 	      autoOpen: false,
			      height: 450,
			      width: 700,
			      modal:true,
			      open: function(evt, ui){
					$('#ayuda').html('');
					/*$.get(URL +'help/promotoria/xxx.html',function(data){
							$('#ayuda').html(data);
					});*/
			      }
});
});//ready
//----------------------------------------VENTANA DE DIALOGO DE AYUDA
function mostrarAyuda(){
	$("#ayuda").dialog('open');
}
//----------------------------------------VENTANA DE DIALOGO DE MANUAL
function notas(){
	$("#dialog-form2").dialog('open');
}

function buscarTarjeta(op){
	var tarjeta=$.trim($("#tarjeta").val());
	var barra=$.trim($("#barra").val());
	if(op==1){		
		if(tarjeta.length < 16){
			$("#tarjeta").val('');
			return false}
		if(barra.length > 0){
			MENSAJE("Va a buscar por Numero de la Tarjeta o por Codigo de Barras!!");
			$("#tarjeta").val('');
			$("#barra").val('');
			}
		$.ajax({
			url: '../buscarTarjeta.php',
			type: "GET",
			data: {v0:tarjeta,v1:1},
			dataType: "json",
			success: function(datos){
				if(datos==0){
					MENSAJE("No existe la TARJETA!.");
					$("#tarjeta").val('');
					continuar=1;
					return false;
				}
				idtarjeta=datos[0].idtarjeta;
				idpersona=datos[0].idpersona;
				$("#codigoBarra1").val(datos[0].codigobarra);
				$("#numeroTarjeta1").val(datos[0].bono);
				if(datos[0].idetapa==61){
					MENSAJE("La tarjeta tiene codigo de rechazada");
					}
				continuar=0;
			},
			complete: function(){
				if(continuar==1){return false}
				$.getJSON(URL+'phpComunes/pdo.buscar.persona.id.php',{v0:idpersona},function(data){
					if(data==0){
						MENSAJE("No existe la persona!.");
						$("#tarjeta").val('');
						return false;
					}
					nom=$.trim(data[0].pnombre)+" "+$.trim(data[0].snombre);
					apll=$.trim(data[0].papellido)+" "+$.trim(data[0].sapellido);		
					$("#nombreCompleto1").val(nom);
					$("#apellidoCompleto1").val(apll);
					$("#corto").val(data[0].nombrecorto);
					})
				}
		});
		}
	else{
		if(barra.length<13){return false}
		$.ajax({
			url: '../buscarTarjeta.php',
			type: "GET",
			data: {v0:barra,v1:2},
			dataType: "json",
			success: function(datos){
				if(datos==0){
					MENSAJE("No existe la TARJETA!.");
					$("#barra").val('');
					continuar=1;
					return false;
				}
				idtarjeta=datos[0].idtarjeta;
				idpersona=datos[0].idpersona;
				$("#codigoBarra1").val(datos[0].codigobarra);
				$("#numeroTarjeta1").val(datos[0].bono);
				if(datos[0].idetapa==61){
					MENSAJE("La tarjeta tiene codigo de rechazada");
					}
				continuar=0;
			},
			complete: function(){
				if(continuar==1){return false}
				$.getJSON(URL+'phpComunes/pdo.buscar.persona.id.php',{v0:idpersona},function(data){
					if(data==0){
						MENSAJE("No existe la persona!.");
						$("#tarjeta").val('');
						return false;
					}
					nom=$.trim(data[0].pnombre)+" "+$.trim(data[0].snombre);
					apll=$.trim(data[0].papellido)+" "+$.trim(data[0].sapellido);		
					$("#nombreCompleto1").val(nom);
					$("#apellidoCompleto1").val(apll);
					$("#corto").val(data[0].nombrecorto);
					})
				}
		});
		}
	}

function rechazar(){
	$.getJSON('rechazar.php',{v0:idtarjeta},function(datos){
		if(datos==0){
			MENSAJE("La tarjeta no se pudo actualizar!");
			return false;
			}
		camposLimpiar();
		//MENSAJE("La tarjeta fue actualizada!");
		$("#barra").focus();
		})
	}
	
function camposLimpiar(){
	$("table.tablero input:text").val('');
	}