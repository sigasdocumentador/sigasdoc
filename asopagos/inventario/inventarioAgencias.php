<?php

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
$url=$_SERVER['PHP_SELF'];
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'auditoria.php';
auditar($url);

include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';
$usuario=$_SESSION['USUARIO'];
$agencia=$_SESSION['AGENCIA'];

if(isset($_REQUEST["numero"])){
	$numero=$_REQUEST['numero'];
	if ($numero!=""){
		$sql="update aportes101 set ubicacion='$agencia',idetapa=63,inventario='S',fechainventario=cast(getdate() as date),usuarioinventario='$usuario' where codigobarra='$numero'";
		$rs=$db->queryActualiza($sql);
		if($rs != 1){
			echo "<script>alert ('La Tarjeta NO fue inventariada');</script>";
		}
	}else{
		echo "<script>alert ('Ingrese el codigo de barras');</script>";
	}
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Inventario Tarjetas</title>

<link href="../../css/Estilos.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../../js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="inventario.js"></script>
</head>
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td class="cuerpo_iz">&nbsp;</td>
		<td class="cuerpo_ce"><img height="1" width="1" src="../../imagenes/spacer.gif"> 
      <img height="1" width="1" src="../../imagenes/spacer.gif"/>  
       
    </td>
		<td class="cuerpo_de">&nbsp;</td>
	</tr>
<tr>
	<td class="cuerpo_iz">&nbsp;</td>
	<td>
		<form name="form1" method="post" action="">
			<blockquote>&nbsp;</blockquote>
			<table width="100%" border="0" cellspacing="0">
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td><div align="center" class="big">PROCESO DE INVENTARIO DE TARJETA MULTISERVICIO</div></td>
				</tr>
				<tr>
					<td class="Rojo"><div align="center">Por favor escanear una a una las Tarjetas Multiservicio teniendo en cuenta de no repetir la tarjeta escaneada</div></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td align="center"><input type="text" style="font-size:18px"  name="numero" id="numero" onKeyPress="if(event.keyCode==13){submit();}"/>  </td>
				</tr>
				<tr>
					<td align="center"><div id="ultimo" style="font-size:18px; color:#F00; font-weight:bold" ></div></td>
				</tr>
				<tr>
					<td align="center">&nbsp;</td>
				</tr>
				<tr>
					<td align="center">Usuario: <?php echo $usuario ?>  Agencia: <?php echo $agencia; ?></td>
				</tr>
			</table>
			<input type="hidden" name="tAgencia" id="tAgencia" value="<?php echo $agencia; ?>" />
		</form>
		</td>
		<td class="cuerpo_de">&nbsp;</td>
	</tr>
	<tr>
		<td class="abajo_iz" >&nbsp;</td>
    <td class="abajo_ce" ></td>
    <td class="abajo_de" >&nbsp;</td>
	</tr>
</table>
<!-- Manual Ayuda -->
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
</body>
<script>
document.getElementById('numero').focus();
</script>
</html>