<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$url=$_SERVER['PHP_SELF'];
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'auditoria.php';
auditar($url);

include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'p.definiciones.class.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';
$objClase=new Definiciones();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>::Rechazar Tarjeta::</title>
<link href="../../css/Estilos.css" rel="stylesheet" type="text/css" />
<link type="text/css" href="../../css/estilo_tablas.css" rel="stylesheet"/>
<link type="text/css" href="../../css/formularios/base/ui.all.css" rel="stylesheet" />
<link href="../../css/marco.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../../js/jquery-1.4.2.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.core.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.dialog.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.datepicker.js"></script>
<script type="text/javascript" src="../../js/comunes.js"></script>
<script type="text/javascript" src="js/rechazar.js"></script>

<script type="text/javascript">
shortcut.add("Shift+F",function() {
		                                var URL=src();
		var url=URL+"aportes/trabajadores/consultaTrabajador.php";
    	window.open(url,"_blank");
    },{
	'propagate' : true,
	'target' : document 
        });        
</script>

</head>

<body>
<br /><br />
<table border="0" align="center" cellpadding="0" cellspacing="0" width="80%">
  <tbody>
 
  <!-- ESTILOS SUPERIOR TABLA-->
  <tr>
    <td width="13" height="29" class="arriba_iz">&nbsp;</td>
    <td class="arriba_ce"><span class="letrablanca">::&nbsp;Rechazar Tarjeta&nbsp;::</span></td>
    <td width="13" class="arriba_de" align="right">&nbsp;</td>
  </tr>
  
  <!-- ESTILOS ICONOS TABLA-->
  <tr>
   <td class="cuerpo_iz">&nbsp;</td>
	<td class="cuerpo_ce">&nbsp;</td>
    <td class="cuerpo_de">&nbsp;</td>
  </tr>
  
  <!-- ESTILOS MEDIO TABLA-->
  <tr>
    <td class="cuerpo_iz">&nbsp;</td>
    <td class="cuerpo_ce">
   <table border="0" cellpadding="5" align="center" class="tablero" width="90%" id="tablaDatos">
<tr>
  <td>Código Barra</td>
    <td><label>
      <input name="barra" type="text" class="box1" id="barra" onblur="buscarTarjeta(2);" />
      <img src="../../imagenes/menu/obligado.png" width="12" height="12"></label></td>
    <td>Tarjeta Número</td>
    <td><input name="tarjeta" type="text" class="box1" id="tarjeta" onblur="buscarTarjeta(1);" />      <img src="../../imagenes/menu/obligado.png" width="12" height="12"></td>
    </tr>
<tr>
  <td>Nombres</td>
  <td ><label for="nombreCompleto"></label>
    <input type="text" name="nombreCompleto1" id="nombreCompleto1"  class="box1" readonly="readonly"/></td>
  <td width="17%">Apellidos</td>
  <td width="33%"><input type="text" name="apellidoCompleto1" id="apellidoCompleto1"  class="box1" readonly="readonly"/></td>
  </tr>
  <tr>
    <td>Nombre Corto</td>
    <td colspan="3"><input type="text" name="corto" id="corto" class="boxlargo" /></td>
    </tr>
  <tr>
  <td>Código de Barras</td>
  <td><input id="codigoBarra1" type="text" class="box1" readonly="readonly"/></td>
  <td>Número Tarjeta</td>
  <td><input id="numeroTarjeta1" type="text" class="box1" readonly="readonly"/></td>
</tr>
</table>
	<div></div>
    <div id="boton1" align="center"><input type="submit" value="Rechazar" class="ui-state-default" style="margin-first:20px" onclick="rechazar();"/>
    </div>

    </td>
    <td class="cuerpo_de">&nbsp;</td>
  </tr>
  <!-- CONTENIDO TABLA-->
  <tr>
    <td class="cuerpo_iz">&nbsp;</td>
    <td class="cuerpo_ce"><span class="RojoGrande">Nota: primero verifique que todas las tarjetas se hayan escaniado (Reporte 001</span>) </td>
    <td class="cuerpo_de">&nbsp;</td>
  </tr>
  
  <!-- ESTILOS PIE TABLA-->
  <tr>
    <td height="34" class="abajo_iz" >&nbsp;</td>
    <td class="abajo_ce" ></td>
    <td class="abajo_de" >&nbsp;</td>
  </tr>
  
</tbody>
</table>

<!-- colaboracion en linea -->

<div id="dialogo-archivo" title="Archivo banco">
<div id="progreso" style="display: none; font-size: 15pt; font-weight: bold;">Procesado(s) <span id="pg">0</span> de <span id="tt"></span> archivo(s)</div>
<div id="log"></div>
</div>
<div id="dialog-form2" title="Colaboraci&oacute;n en l&iacute;nea" style="display:none">
<p>Por favor diligencie este formulario para enviar comentarios, errores o falencias encontradas en el proceso. M&aacute;ximo 250 caracteres </p>
<label>Tus comentarios:</label><br />
<textarea name="notas" id="notas" cols="60", rows="10"></textarea>
</div>
<!--fin colaboracion en linea-->

<!-- Manual Ayuda -->
<div id="ayuda" title="::Manual Discapacitados::"></div>
<input id="idPersona" type="hidden"  />
</body>
</html>