<?php
set_time_limit(0);
ini_set("display_errors",'0');
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$usuario= $_SESSION["USUARIO"];

include_once $raiz.DIRECTORY_SEPARATOR.'funcionesComunes'.DIRECTORY_SEPARATOR.'funcionesComunes.php';

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

$fecha = (isset($_REQUEST["fechaCorte"]) && $_REQUEST["fechaCorte"] != "")?$_REQUEST["fechaCorte"]:null;
$pagina = $_REQUEST["pagina"];
$numRegistros = $_REQUEST["numRegistros"];
$bono = (isset($_REQUEST["bono"]) && $_REQUEST["bono"] != "")?$_REQUEST["bono"]:null;
$documento = (isset($_REQUEST["documento"]) && $_REQUEST["documento"] != "")?$_REQUEST["documento"]:null;
$strCondicionBusqueda = "";

if($bono != null)
	$strCondicionBusqueda = " AND a101.bono like '%{$bono}%' ";
else if($documento != null)
	$strCondicionBusqueda = " AND a015.identificacion like '%{$documento}%' ";

$sql="DECLARE @fecha_corte DATE = '$fecha'
SELECT * FROM (SELECT row_number() OVER(ORDER BY even.idpersona ASC) AS contador,even.idpersona,(a015.pnombre + ' ' + a015.papellido) AS nombre,a015.identificacion,a015.direccion,a015.telefono,
(SELECT count(DISTINCT even3.tarjeta) FROM aportes152 even3 WHERE even3.idpersona=even.idpersona) AS can_total_tarjetas,
(SELECT max(e152.tarjeta) FROM aportes152 e152 WHERE e152.idpersona=even.idpersona) AS tarjeta,
(SELECT e101.idtarjeta FROM aportes101 e101 WHERE e101.bono=(SELECT max(e152.tarjeta) FROM aportes152 e152 WHERE e152.idpersona=even.idpersona)) AS idtarjeta,
(SELECT sum(e152.valor) FROM aportes152 e152 WHERE e152.descripcion IN ('50 SALDO INICILAL','50 ABONO') AND e152.idpersona=even.idpersona AND e152.fechatransaccion<=@fecha_corte)-isnull((SELECT sum(e152.valor) FROM aportes152 e152 WHERE e152.descripcion IN ('0  MN COMPRA CUOTA MONETARIA','30 CARGOS') AND e152.idpersona=even.idpersona),0) AS SaldoCorte,
(SELECT sum(e152.valor) FROM aportes152 e152 WHERE e152.descripcion IN ('50 SALDO INICILAL','50 ABONO') AND e152.idpersona=even.idpersona) - isnull((SELECT sum(e152.valor) FROM aportes152 e152 WHERE e152.descripcion IN ('0  MN COMPRA CUOTA MONETARIA','30 CARGOS') AND e152.idpersona=even.idpersona),0) AS saldoActual,
isnull((SELECT sum(e152.valor) FROM aportes152 e152 WHERE e152.descripcion IN ('30 CARGOS') AND e152.idpersona=even.idpersona),0) AS Reverso
FROM aportes152 even
INNER JOIN aportes015 a015 ON even.idpersona=a015.idpersona
WHERE
even.fechatransaccion<@fecha_corte
AND even.idpersona IS not NULL
AND isnull(isnull((SELECT sum(valor) from aportes152 WHERE idpersona=even.idpersona AND (descripcion='50 SALDO INICILAL' OR descripcion='50 ABONO') AND fechatransaccion<=@fecha_corte) - isnull((SELECT sum(valor) from aportes152 WHERE idpersona=even.idpersona AND (descripcion='0  MN COMPRA CUOTA MONETARIA'OR descripcion='30 CARGOS')),0),0),0) > 0
AND isnull(isnull((SELECT sum(valor) from aportes152 WHERE idpersona=even.idpersona AND (descripcion='50 SALDO INICILAL' OR descripcion='50 ABONO')),0) - isnull((SELECT sum(valor) from aportes152 WHERE idpersona=even.idpersona AND (descripcion='0  MN COMPRA CUOTA MONETARIA' OR descripcion='30 CARGOS')),0),0) > 0
AND DATEDIFF(day,isnull((SELECT TOP 1 CAST(fechasistema AS DATETIME) FROM aportes121 e121 WHERE e121.idpersona=even.idpersona ORDER BY CAST(fechasistema AS DATETIME) DESC),0),getdate())  >= 20
{$strCondicionBusqueda}
GROUP BY even.idpersona,a015.pnombre,a015.papellido,a015.identificacion,a015.direccion,a015.telefono
) AS c WHERE c.contador BETWEEN ($pagina-1) * $numRegistros + 1 AND $pagina * $numRegistros ";

$rs = $db->querySimple($sql);
$filas=array();
while($w = $rs->fetch())
	$filas[] = $w;

if(count($filas) == 0)
	echo 0;
else
	echo json_encode($filas);
?>