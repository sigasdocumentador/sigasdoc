<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';

class prescripciones{
	Public $idpersona;
	public $identificacion;
	public $tarjeta;
	public $saldocorte;
	Public $reverso;
	Public $saldoactual;
	Private static $con = NULL;
	public $arrError;
    
	// definimos el contructor
	public function __construct($idpersona){
		//Conexion
		try{
			self::$con = IFXDbManejador::conectarDB();
			$this->idpersona=$idpersona;
			if( self::$con->conexionID==null ){
				throw new Exception(self::$con->error);
			}
		}catch(Exception $e){
			echo $e->getMessage();
			exit();
		}
	}
	
	//  metodo para preparar las tablas para prescripciones
	public function PrePrescripciones(){
		echo "Se esta hacindo la preparación del proceso prescripción".$this->idpersona;		
	}
	
	// clase que permite consultar los eventos en apotes152 de determina persona
	public function consuEvenBonosPer(){
		$sql="SELECT a152.tarjeta,a152.descripcion,a152.valor,a152.fechatransaccion,a015.identificacion,a015.pnombre + ' ' + a015.papellido AS nombre,
              a015.direccion,a015.telefono,(SELECT count(DISTINCT tarjeta) FROM aportes152 WHERE idpersona='{$this->idpersona}') AS can_tarjetas,
              a152.idconsumo,cp
              from aportes152 a152
              INNER JOIN  
              aportes015 a015 ON a152.idpersona=a015.idpersona
              where a152.idpersona='{$this->idpersona}' ORDER BY a152.tarjeta,a152.fechatransaccion";
		$resdata = array();
		$res = self::$con->querySimple($sql);
		while($row=$res->fetch()){
			$resdata[]=$row;
		}
		return 	$resdata;	
	}
	
	//metodo para consultar el saldo de la actual tarjeta
	public function consuSaldo($tarjeta){
	  $sql="SELECT tarjeta,
           isnull((SELECT sum(valor) FROM aportes152 WHERE tarjeta='{$tarjeta}' AND (descripcion='50 ABONO' OR descripcion='50 SALDO INICILAL')),0) - 
           isnull((SELECT sum(valor) FROM aportes152 WHERE tarjeta='{$tarjeta}' AND (descripcion='30 CARGOS' OR descripcion='0  MN COMPRA CUOTA MONETARIA')),0) AS saldo 
           FROM aportes152 WHERE tarjeta='{$tarjeta}'
           GROUP BY tarjeta";
	  $res=self::$con->querysimple($sql);
	  return $res->fetchAll();
	}
	
	//metodo para actualizar los idconsumos habilitados 
	public function actualiceIdConsumoTrue($chktrue,$chkfalse,$fechacorte){
		//iniciar transaccion
		self::$con->inicioTransaccion();
		if($chktrue[0]>0){
		    foreach($chktrue as $valor){
			    // sentenccia de update tabla aportes152 campo cp(control prescripcion)
		    	$datos= explode('*',$valor);
			    $sql="update aportes152 set cp='2',cpfecha=GETDATE(),cpusuario='{$_SESSION['USUARIO']}' where idconsumo='{$datos[0]}' ";
			    if(!self::$con->queryActualiza($sql)>0){
				    $this->arrError.='update idconsumo:'.$datos[0].self::$con->error;
				    self::$con->cancelarTransaccion();
				    return 0;
			    }
     	    }
		}
		if($chkfalse[0]>0){
     	    foreach($chkfalse as $valor){
     	    	$datos= explode('*',$valor);
     		    // sentenccia de update tabla aportes152 campo cp(control prescripcion)
     	    	if($datos[1]=='30CARGOS' || $datos[1]=='0MNCOMPRACUOTAMONETARIA'){
     		        $sql="update aportes152 set cp='3',cpfecha=GETDATE(),cpusuario='{$_SESSION['USUARIO']}' where idconsumo='{$datos[0]}' ";
     		        if(!self::$con->queryActualiza($sql)>0){
     			       $this->arrError.='update idconsumo:'.$datos[0].self::$con->error;
     			       self::$con->cancelarTransaccion();
     			       return 0;
     		        }
     	    	}
     	    	else if(str_replace('-','',$datos[2]) <= $fechacorte && ($datos[1]=='50SALDOINICILAL' || $datos[1]=='50ABONO')){
     	    		$sql="update aportes152 set cp='3',cpfecha=GETDATE(),cpusuario='{$_SESSION['USUARIO']}' where idconsumo='{$datos[0]}' ";
     	    		if(!self::$con->queryActualiza($sql)>0){
     	    			$this->arrError.='update idconsumo:'.$datos[0].self::$con->error;
     	    			self::$con->cancelarTransaccion();
     	    			return 0;
     	    		}
     	    	}
     	    }
		}
     	self::$con->confirmarTransaccion();
		return 1;
	}
	
	
}


?>