<?php
/* autor:       orlando puentes
 * fecha:       11/10/2010
 * objetivo:    
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'rsc' . DIRECTORY_SEPARATOR . 'conexion.class.php';

class Tarjetas{
	//constructor
	var $con;
function Tarjetas(){
		$this->con=new DBManager;
	}

function buscar_solicitud(){
		if($this->con->conectar()==true){
			$sql="SELECT * FROM aportes116 WHERE estado='A'";
			return mssql_query($sql,$this->con->conect);
		}
	}

function buscar_solicitud_agencias(){
    if($this->con->conectar()==true){
    $sql="SELECT * FROM aportes116 WHERE estado='I' and envioagencia='N'";
    return mssql_query($sql,$this->con->conect);
}
}

function buscar_tarjetas_sol($id){
    if($this->con->conectar()==true){
        $sql="SELECT aportes116.idagencia,aportes117.bono, identificacion,pnombre,snombre,papellido, sapellido FROM aportes116
INNER JOIN aportes117 ON aportes116.idsolicitud=aportes117.idsolicitud INNER JOIN aportes102 ON aportes117.bono = aportes102.bono
INNER JOIN aportes015 ON aportes102.idpersona=aportes015.idpersona WHERE aportes116.idsolicitud=$id";
	return mssql_query($sql,$this->con->conect);
	}
}

function recibir_tarjetas($ida){
    if($this->con->conectar()==true){
        $sql="UPDATE aportes102 SET idetapa=63, fechaetapa=CAST(GETDATE() AS DATE) WHERE bono IN(SELECT bono FROM aportes117 WHERE idsolicitud=$ida)";
        return mssql_query($sql,$this->con->conect);
    }

}

function marcar_archivo($ida){
    if($this->con->conectar()==true){
        $sql="UPDATE aportes116 SET estado='I' WHERE idsolicitud=$ida";
        return mssql_query($sql,$this->con->conect);
    }
}

function buscar_archivos($age){
    if($this->con->conectar()==true){
    $sql="SELECT idsolicitud, archivo FROM aportes116 WHERE idagencia='$age' AND estado='I' AND envioagencia='N'";
    return mssql_query($sql,$this->con->conect);
}
}

function enviar_agencia($age,$ids){
    if($this->con->conectar()==true){
    $sql="UPDATE aportes102 SET ubicacion='$age',idetapa=2694, fechaetapa=CAST(GETDATE() AS DATE) WHERE bono IN (SELECT bono FROM aportes117 WHERE idsolicitud=$ids)";
    //echo $sql;
    return mssql_query($sql,$this->con->conect);
}
}

function marcar_archivo_envio($ids,$usuario,$age){
    if($this->con->conectar()==true){
        $sql="UPDATE aportes116 set envioagencia='S',fechaenvio=CAST(GETDATE() AS DATE),usuarioenvia='$usuario' WHERE idsolicitud=$ids";
        //echo $sql;
        return mssql_query($sql,$this->con->conect);
    }
}

function marcar_archivo_agencia($ida,$u){
    if($this->con->conectar()==true){
        $sql="UPDATE aportes116 SET fecharecibo=CAST(GETDATE() AS DATE),usuariorecibe='$u',recibeagencia='S' WHERE idsolicitud=$ida";
        return mssql_query($sql,$this->con->conect);
    }
}

function tarjetas_enviar_servicios($ide) {
    if($this->con->conectar()==true){
        $sql="SELECT aportes015.idpersona,identificacion,pnombre,snombre,papellido,sapellido,bono,idtarjeta FROM aportes015 INNER JOIN aportes102 ON aportes102.idpersona=aportes015.idpersona
WHERE aportes015.idpersona IN (SELECT idpersona FROM aportes016 WHERE idempresa =$ide AND estado='A' AND tipoformulario =49)
AND aportes102.idetapa=63 ORDER BY identificacion";
        return mssql_query($sql,$this->con->conect);
    }
}

function insert_130($campo0,$campo1,$campo2,$campo3,$campo4){
    if($this->con->conectar()==true){
        $sql="INSERT INTO aportes130(idpromotor, idempresa, total, tipoenvio, fechasistema, usuario) VALUES( $campo0, $campo1, $campo2, '$campo3', cast(getdate() as date), '$campo4')";
        return mssql_query($sql,$this->con->conect);
    }
}
function insert_detalle130($campo0,$campo1){
    if($this->con->conectar()==true){
        $sql="INSERT INTO aportes131(idenvio, idpersona) VALUES($campo0, $campo1)";
        return mssql_query($sql,$this->con->conect);
    }
}

function marcar_asesor($campo0,$campo1,$campo2){
    if($this->con->conectar()==true){
        $sql="Update aportes102 set idetapa=$campo0, fechaetapa=CAST(GETDATE() AS DATE), asesor=$campo1,fechaasesor=CAST(GETDATE() AS DATE) where bono='$campo2'";
        return mssql_query($sql,$this->con->conect);
    }
}

function datos_bono($bono){
	if($this->con->conectar()==true){
		$sql="SELECT aportes102.*,pnombre,snombre,papellido,sapellido FROM aportes102 INNER JOIN aportes015 ON aportes102.idpersona=aportes015.idpersona WHERE bono='$bono'";
		return mssql_query($sql,$this->con->conect);
		}
	}	

function guardar_notas($idb,$nota,$usuario){
	if($this->con->conectar()==true){
		$sql="INSERT INTO aportes120 (idtarjeta,nota,fechasistema,usuario) VALUES($idb,'$nota',CAST(GETDATE() AS DATE),'$usuario')";
		return mssql_query($sql,$this->con->conect);
		}
	}

function actualizar_estado($idb,$estado,$usuario){
	if($this->con->conectar()==true){
		if($estado=65){
		$sql="UPDATE aportes102 SET entregada='S', fechaentrega= CAST(GETDATE() AS DATE), estado='A',usuarioentrega='$usuario', 
		fechaetapa=CAST(GETDATE() AS DATE), idetapa=$estado WHERE idtarjeta=$idb";
		}
		else {
			$sql="UPDATE aportes102 SET fechaetapa=CAST(GETDATE() AS DATE), idetapa=$estado WHERE idtarjeta=$idb";
		}
		return mssql_query($sql,$this->con->conect);
		}
}	

function buscar_barra($barra){
	if($this->con->conectar()==true){
		$sql="Select count(*) as cuenta from aportes102 where codigobarra='$barra'";
		return mssql_query($sql,$this->con->conect);
	}
}

function bloquear_tarjeta($idt){
	if($this->con->conectar()==true){
		$sql="Update aportes102 set estado='B' where idtarjeta=$idt";
		return mssql_query($sql,$this->con->conect);
	}
}

function update_estado_tarjeta($idt){
	if($this->con->conectar()==true){
		$sql="Update aportes101 set (estado,idetapa,fechaetapa)=('I',2722,CAST(GETDATE() AS DATE)) where idtarjeta=$idt";
		return mssql_query($sql,$this->con->conect);
	}
}

function suspender_tarjeta($idt){
	if($this->con->conectar()==true){
		$sql="Update aportes101 set estado='S' where idtarjeta=$idt";
		return mssql_query($sql,$this->con->conect);
	}
}

function habilitar_tarjeta($idt){
	if($this->con->conectar()==true){
		$sql="Update aportes102 set estado='A' where idtarjeta=$idt";
		return mssql_query($sql,$this->con->conect);
	}
}

function insert_113($idt,$usuario){
	if($this->con->conectar()==true){
		$sql="INSERT INTO aportes113 (idtarjeta,procesado,usuario,fechasistema) VALUES($idt,'N','$usuario',cast(getdate() as date))";
		return mssql_query($sql,$this->con->conect);
	}
}

function insert_111($idt,$usuario){
	if($this->con->conectar()==true){
		$sql="INSERT INTO aportes111 (idtarjeta,procesado,usuario,fechasistema) VALUES($idt,'N','$usuario',cast(getdate() as date))";
		return mssql_query($sql,$this->con->conect);
	}
}

function insert_112($idt,$reverso,$idn,$usuario){
	if($this->con->conectar()==true){
		$sql="INSERT INTO aportes112(idtarjeta,valor,idnota,procesado,fechasistema,usuario) VALUES($idt,$reverso,$idn,'N',cast(getdate() as date),'$usuario')";
		return mssql_query($sql,$this->con->conect);
	}
}

function insert_110($ido,$idd,$saldo,$usuario){
	if($this->con->conectar()==true){
		$sql="INSERT INTO aportes110 (iddesde, idpara, valor, procesada, usuario, fechasistema) VALUES($ido,$idd,$saldo,'N','$usuario',cast(getdate() as date))";
		return mssql_query($sql,$this->con->conect);
	}
}

function insert_109($idt,$usuario){
	if($this->con->conectar()==true){
		$sql="INSERT INTO aportes109 (idtarjeta,procesado,usuario,fechasistema) VALUES($idt,'N','$usuario',cast(getdate() as date))";
		return mssql_query($sql,$this->con->conect);
	}
}

}
?>
