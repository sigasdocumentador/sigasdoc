<?php
set_time_limit(0);
ini_set("display_errors",'0');
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$usuario= $_SESSION["USUARIO"];

include_once $raiz.DIRECTORY_SEPARATOR.'funcionesComunes'.DIRECTORY_SEPARATOR.'funcionesComunes.php';

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

// se evalua si el proceso de prescripcion ya ha sido ejecutado una vez en el dia para y en caso en que no se hace un update a la tabla aportes152 para actualizar el compo idpersona
if(!isset($_COOKIE['preparet'])){
	$sql="UPDATE aportes152  SET idpersona=(SELECT idpersona FROM aportes102 e102 WHERE  e102.bono=aportes152.tarjeta) WHERE idpersona IS NULL
          UPDATE aportes152  SET idpersona=(SELECT idpersona FROM aportes101 e101 WHERE  e101.bono=aportes152.tarjeta) WHERE idpersona IS NULL";
	$dbupdate= $db->queryActualiza($sql);
	setcookie("preparet",'1',time()+3600*24);
}

$fecha = (isset($_REQUEST["fechaCorte"]) && $_REQUEST["fechaCorte"] != "")?$_REQUEST["fechaCorte"]:null;
$bono = (isset($_REQUEST["bono"]) && $_REQUEST["bono"] != "")?$_REQUEST["bono"]:null;
$documento = (isset($_REQUEST["documento"]) && $_REQUEST["documento"] != "")?$_REQUEST["documento"]:null;
$strCondicionBusqueda = "";

if($bono != null)
	$strCondicionBusqueda = " AND a101.bono like '%{$bono}%' ";
else if($documento != null)
	$strCondicionBusqueda = " AND a015.identificacion like '%{$documento}%' ";
					
	$sql="DECLARE @fecha_corte DATE = '$fecha'
		  SELECT count(*) AS total FROM (SELECT  row_number() OVER(ORDER BY even.idpersona ASC) AS contador,even.idpersona
          FROM aportes152 even 
          INNER JOIN aportes015 a015 ON even.idpersona=a015.idpersona
          WHERE 
          even.fechatransaccion<@fecha_corte 
          AND even.idpersona IS not NULL
          AND isnull(isnull((SELECT sum(valor) from aportes152 WHERE idpersona=even.idpersona AND (descripcion='50 SALDO INICILAL' OR descripcion='50 ABONO') AND fechatransaccion<=@fecha_corte) - isnull((SELECT sum(valor) from aportes152 WHERE idpersona=even.idpersona AND (descripcion='0  MN COMPRA CUOTA MONETARIA'OR descripcion='30 CARGOS')),0),0),0) > 0
          AND isnull(isnull((SELECT sum(valor) from aportes152 WHERE idpersona=even.idpersona AND (descripcion='50 SALDO INICILAL' OR descripcion='50 ABONO')),0) - isnull((SELECT sum(valor) from aportes152 WHERE idpersona=even.idpersona AND (descripcion='0  MN COMPRA CUOTA MONETARIA' OR descripcion='30 CARGOS')),0),0) > 0
          AND DATEDIFF(day,isnull((SELECT TOP 1 CAST(fechasistema AS DATETIME) FROM aportes121 e121 WHERE e121.idpersona=even.idpersona ORDER BY CAST(fechasistema AS DATETIME) DESC),0),getdate())  >= 20
          {$strCondicionBusqueda}
          GROUP BY even.idpersona
          )  AS total";				
$result = $db->querySimple($sql);
$registro = $result->fetch();
if(is_array($registro))
	echo intval($registro["total"]);
else
	echo 0;
?>