<?php
/* autor:       Orlando Puentes
 * fecha:       Julio 23 de 2010
 * objetivo:    
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$url=$_SERVER['PHP_SELF'];
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'auditoria.php';
auditar($url);

include_once $raiz . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'p.definiciones.class.php';
$objClase=new Definiciones();
$fecha=date("m/d/Y");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="es">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>::Bloqueo Tarjeta::</title>
<link type="text/css" href="../../css/formularios/base/ui.all.css" rel="stylesheet" />
<link type="text/css" href="../../css/Estilos.css" rel="stylesheet"/>
<link type="text/css" href="../../css/estilo_tablas.css" rel="stylesheet"/>
<link href="../../css/marco.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../../js/jquery-1.4.2.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.core.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.tabs.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.dialog.js"></script>
<script type="text/javascript" src="../../js/formularios/ui/ui.datepicker.js"></script>
<script type="text/javascript" src="../../js/jquery.alphanumeric.js"></script>
<script type="text/javascript" src="../../js/comunes.js"></script>
<script type="text/javascript" src="js/bloqueoTarjeta.js"></script>

<script type="text/javascript">
shortcut.add("Shift+F",function() {
		                                var URL=src();
		var url=URL+"aportes/trabajadores/consultaTrabajador.php";
    	window.open(url,"_blank");
    },{
	'propagate' : true,
	'target' : document 
        });        
</script>

</head>

<body>
<form name="forma">
<center>
<!-- TABLA VISIBLE CON BOTONES -->
<br />
<table width="80%" border="0" cellspacing="0" cellpadding="0" align="center">
<tr>
<td width="13" height="29" class="arriba_iz">&nbsp;</td>
<td class="arriba_ce"><span class="letrablanca">::Bloqueo Tarjeta&nbsp;::</span></td>
<td width="13" class="arriba_de" align="right">&nbsp;</td>
</tr>      
<tr>
 <td class="cuerpo_iz">&nbsp;</td>
<td class="cuerpo_ce">
<img src="../../imagenes/tabla/spacer.gif" width="1" height="1">
<img src="../../imagenes/spacer.gif" width="1" height="1"><img src="../../imagenes/spacer.gif" width="1" height="1"><img src="../../imagenes/spacer.gif" width="1" height="1">
<img src="../../imagenes/menu/modificar.png" title="Bloquear Tarjeta" width="16" height="16" onClick="guardar();" style="cursor:pointer">
<img src="../../imagenes/spacer.gif" width="1" height="1">
<img src="../../imagenes/menu/refrescar.png" width="16" height="16" style="cursor:pointer" title="Limpiar campos" onClick="limpiarCampos();">
<img src="../../imagenes/spacer.gif" width="1" height="1"><img src="../../imagenes/spacer.gif" width="1" height="1"><img src="../../imagenes/spacer.gif" width="1" height="1"><img src="../../imagenes/menu/informacion.png" width="16" height="16" style="border:none; cursor:pointer" title="Manual" onClick="mostrarAyuda();" />
<img src="../../imagenes/menu/notas.png" width="16" height="16" style="cursor: pointer" title="Colaboración en línea" onclick="notas();" />
 <td class="cuerpo_de">&nbsp;</td>
</tr> 
<tr>     
<td class="cuerpo_iz">&nbsp;</td>
<td class="cuerpo_ce"><div id="error" style="color:#FF0000"></div></td>
<td class="cuerpo_de">&nbsp;</td>
</tr>
<tr>
<td class="cuerpo_iz">&nbsp;</td>

<td class="cuerpo_ce">
<table width="90%" border="0" cellspacing="0" class="tablero">
<tr>
<td width="25%">Buscar Por</td>
<td width="25%">
<select name="buscarPor" id="buscarPor" class="box1" onchange="mostrar();">
<option value="0" selected="selected">Seleccione</option>
<option value="1">Identificación</option>
<option value="2">Bono</option>
</select>
</td>
<td width="25%">Fecha</td>
<td width="25%"><?php echo $fecha;?>&nbsp;</td>
</tr>
</table>

<div id="tNumero" style="display:none">
<table width="90%" border="0" cellspacing="0" class="tablero">
<tr>
    <td width="25%">Tipo Documento</td>
    <td width="25%">
    <select name="tipoI" id="tipoI" class="box1" >
    <?php
	$consulta=$objClase->mostrar_datos(1, 2);
	while($row=mssql_fetch_array($consulta)){
		echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
	}
	?>
    </select>
    </td>
    <td width="25%">N&uacute;mero</td>
    <td width="25%">
    <input name="numero" type="text" class="box1" id="numero" onblur="buscarPersona();" onkeypress="tabular(this,event);" /></td>
</tr>
</table>
</div>

<div id="tBono" style="display:none">
<table width="90%" border="0" cellspacing="0" class="tablero">
<tr>
  <td width="25%">Bono N&uacute;mero</td>
  <td width="25%"><input name="txtBono" type="text" class="box1" id="txtBono" onblur="rellenarTarjeta(this,this.value);buscarPersona2();" onkeypress="tabular(this,event);" /></td>
  <td width="25%">&nbsp;</td>
  <td width="25%">&nbsp;</td>
</tr>
</table>
</div>
<table width="90%" border="0" cellspacing="0" class="tablero">
<tr>
  <td width="25%">Nombres</td>
  <td colspan="3" id="lnombre" width="75%">&nbsp;</td>
  </tr>
  <tr>
  <td width="25%">Bono</td>
  <td id="lbono" width="25%">&nbsp;</td>
  <td width="25%">Estado</td>
  <td id="lestado" width="25%">&nbsp;</td>
  </tr>

<tr>
  <td width="25%">Fecha Solicitud</td>
  <td width="25%" id="lfechasol"></td>
  <td width="25%">Saldo</td>
  <td width="25%" id="lsaldo"></td>
</tr>
<tr>
  <td width="25%">Motivo</td>
  <td colspan="3" width="75%">
  <select name="codBloqueo" class="boxlargo" id="codBloqueo">
  <option value="0" selected="selected">Seleccione</option>
  <?php
  $consulta=$objClase->mostrar_datos(48, 2);
	while($row=mssql_fetch_array($consulta)){
		echo "<option value=".$row['iddetalledef'].">".$row['detalledefinicion']."</option>";
	}
  ?>
  </select>
  </td>
  </tr>
<tr>
  <td width="25%">Notas</td>
  <td colspan="3" width="75%"><input type="text" class="boxlargo" name="notas" id="notas"/></td>
  </tr>
</table>
<div id="errores" align="left"></div>
<td class="cuerpo_de">&nbsp;</td><!-- FONDO DERECHA -->
<tr>
<td class="abajo_iz" >&nbsp;</td>
    <td class="abajo_ce" ></td>
    <td class="abajo_de" >&nbsp;</td>
</tr>
</table>  
       
<!--colaboracion en linea-->
<div id="dialogo-archivo" title="Archivo banco">
<div id="progreso" style="display: none; font-size: 15pt; font-weight: bold;">Procesado(s) <span id="pg">0</span> de <span id="tt"></span> archivo(s)</div>
<div id="log"></div>
</div>
<div id="dialog-form2" title="Colaboraci&oacute;n en l&iacute;nea" style="display:none">
<p>Por favor diligencie este formulario para enviar comentarios, errores o falencias encontradas en el proceso. M&aacute;ximo 250 caracteres </p>
<label>Tus comentarios:</label><br />
<textarea name="notas" id="notas" cols="60" rows="10"></textarea>
</div>

<!-- fin colaboracion -->

<!-- ayuda en linea -->
<div id="ayuda" title="Manual .:. Bloqueo Tarjeta" style="background-image:url(../../imagenes/FondoGeneral0.png)"></div>
</center>
</form>

</body>
<script language="javascript">
$("#buscarPor").focus();

function mostrarAyuda(){
	$("#ayuda").dialog('open' );
	}

function notas(){
	$("#dialog-form2").dialog('open');
	}	
</script>
</html>
