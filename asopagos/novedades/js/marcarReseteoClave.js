var paginaActual=null;
$(document).ready(function(){
	var URL = src();	
	
	$("#buscarTarjeta").click(function(){
		$("#tDatos tbody,tfoot").empty();
		//VALIDAR QUE EL NIT SEA NUMEROS
		if( $("#buscarPor").val()=="1"&& isNaN($("#idTarjeta").val()) ){
			$(this).next("span.Rojo").html("La Identificacion debe ser num\u00E9rico.").hide().fadeIn("slow");
				return false;
		}
		
		if($("#buscarPor").val()=='Seleccione..'){
			$(this).next("span.Rojo").html("Seleccione el criterio de b\u00FAsqueda.").hide().fadeIn("slow");
			return false;
		}

		if($("#idTarjeta").val()==''){
			$(this).next("span.Rojo").html("Ingrese la Identificacion \u00F3 Bono").hide().fadeIn("slow");
			$("#idEmpresa").focus();
			return false;
		}

		tarjetasPendientesReseteo(1,[$("#idTarjeta").val().toUpperCase(),$("#buscarPor").val()]);
	});//fin click
	
	
	tarjetasPendientesReseteo(1,[$("#idTarjeta").val().toUpperCase(),$("#buscarPor").val()]);
	
}); // dom ready

function tarjetasPendientesReseteo(pagina,parametros){
	paginaActual = pagina;
	$("#tDatos tbody").empty();
	var documento = "";
	var bono = "";
	if(parametros.length == 2){
		// buscar por
		if(parametros[1] == "documento")
			documento = parametros[0];
		else if(parametros[1] == "bono"){
			bono = parametros[0];
		}
	}
	$.getJSON('contarReseteoClave.php?documento='+ documento +"&bono="+ bono,function(totalRegistros){
		var regsPorPagina = 20;
		var numPaginas = Math.ceil(totalRegistros/regsPorPagina);
		var strEnlacesPag = "";
		$("#span_num_empresas").html(totalRegistros);
		c=0;
		for(var i = pagina; i<=numPaginas; i++){
			if(c <= 10){
				strEnlacesPag += "<a href='#' id='pagina_"+ i +"' class='link_paginacion' >"+ i +"</a> ";
			}
			c++;	
		}
		
		var strEnlaceAnterior = "";
		if(pagina > 1)
			strEnlaceAnterior = "<a href='#' id='pagina_anterior'> << </a>";
		
		var strEnlaceSiguiente = "";
		if(pagina < numPaginas)
			strEnlaceSiguiente = "<a href='#' id='pagina_siguiente'> >> </a>";
		
		strEnlacesPag = strEnlaceAnterior + strEnlacesPag + strEnlaceSiguiente; 
		//carga las empresas de la BD
		$.getJSON('tarjetasPendientesReseteo.php?pagina='+ pagina +'&numRegistros='+regsPorPagina +'&bono='+ bono +"&documento="+ documento,function(data){
		  	if(data == 0){
		  		alert("no hay datos");
		  		return false;
		  		//$("#idEmpresa").val("");
		  		//$("#cmbIdCausal").val("0");
		  		//tarjetasPendientesReseteo(paginaActual,[$("#idEmpresa").val().toUpperCase(),$("#buscarPor").val()]);
		  	}
		  	$.each(data,function(i,fila){
				$("#tDatos").append("<tr><td style=text-align:center>"+fila.identificacion+"</td><td style=text-align:left>"+fila.bono+"</td><td style=text-align:left>"+fila.nombres+"</td><td style=text-align:center>"+fila.fechasistema+"</td><td style=text-align:center>"+fila.usuario+"</td><td><img src='"+ URL+"imagenes/chk0.png' /><input type='checkbox' value='"+fila.idtarjeta+"' style='display:none' /></td></tr>");
		  	});
		  	
		  	$("table.tablaR tr:even").addClass("zebra");
			//Dar un ID dinamico a los check box de la tabla de empresas
			$("table.tablaR td input:checkbox").each(function(index){
				//$(this).attr({"id":"chk"+index,"value": $(this).parents("tr").children("td:first").find("a").html()}).hide();//id de los check
			});
			
			$("table.tablaR td img").each(function(index,imagen){
				$(imagen).attr({"id":"img"+index,"value": $(imagen).parents("tr").children("td:first").find("a").html()});//id de los check
				$(imagen).bind('click',function(){
					if($(imagen).next("input:checkbox").is(":checked")){
						$(imagen).attr("src",URL+"imagenes/chk0.png");
						$(imagen).next("input:checkbox").attr("checked",false);
					}else{
						$(imagen).attr("src",URL+"imagenes/chk1.png");
						$(imagen).next("input:checkbox").attr("checked",true);
					}
				});
			});
		});//end post
		
		$("#tDatos tfoot").html("<tr><td colspan='6'>"+ strEnlacesPag +"</td></tr>");
		$(".link_paginacion").each(function(ind,enlace){
			$(enlace).bind('click',function(){
				var pag = $(this).attr("id").replace("pagina_","");
				tarjetasPendientesReseteo(pag,[]);
			});
		});
		$("#pagina_anterior").bind("click",function(){
			if(pagina > 1)
				tarjetasPendientesReseteo(parseInt(pagina)-1,[$("#idTarjeta").val().toUpperCase(),$("#buscarPor").val()]);
		});
		
		$("#pagina_siguiente").bind("click",function(){
			if(pagina < numPaginas)
				tarjetasPendientesReseteo(parseInt(pagina)+1,[$("#idTarjeta").val().toUpperCase(),$("#buscarPor").val()]);
		});
		
		$("#pagina_"+pagina).addClass("pagina_actual");
	});	
}

function guardarMarcadas(){
	if(confirm('Realmente desea Marcar?'))
	{   
	var seleccionadas = [];
	var marcadas = [];
	var contador = 0;
	$("table.tablaR td input:checked").each(function(i){
		var ide = $(this).val();//extraigo el valor del value del check cargado dinamicamente
		seleccionadas[i] = ide;
		if(ide > 0){
			marcadas[contador] = ide;
			contador++;
		}
	});
	 
 
 	if(marcadas.length > 0){
 		$.getJSON('marcarSolicitudReseteo.php',{v0:marcadas.join()},function(data){
			if(data == 0)
				alert("Hubo problemas al actualizar los registros");
			else{
					if(data == marcadas.length && marcadas.length == seleccionadas.length)
						alert("Registro actualizado.");
						$("#idTarjeta").val("");
						//obsReseteoClaveTarjeta(marcadas);
						
					/*else
						alert("Se Marcaron las empresas\n"+ marcadas.join() +". \n"+ (seleccionadas.length-marcadas.length) +" Registros no pudieron ser modificados.");
						*/
				}
			tarjetasPendientesReseteo(paginaActual,[$("#idTarjeta").val().toUpperCase(),$("#buscarPor").val()]);
 		});
 	}else{
 		alert("Debe marcar al menos una Tarjeta.");
 		}
	} // Fin confirm
}