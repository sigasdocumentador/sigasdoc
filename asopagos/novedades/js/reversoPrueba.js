/*
* @autor:      Ing. Orlando Puentes
* @fecha:      septiembre 6 de 2010
* objetivo:
*/
var URL=src();
var estado=0;
var idp=0;
var idb=0;
var saldo=0;
var idnt=0;

//Dialog Ayuda
function mostrarAyuda(){
	$("#ayuda").dialog('open' );
}

function notas(){
	$("#dialog-form2").dialog('open');
}

$(document).ready(function(){
	
	
	$("#tNumero").hide();
	$("#tBono").show();
	$("#txtBono").focus();
	
	//Dialog ayuda
	$("#ayuda").dialog({
		autoOpen: false,
		height: 500,
		width: 750,
		draggable:true,
		modal:false,
		open: function(evt, ui){
			$('#ayuda').html('');
			$.get(URL +'help/tarjetas/manualayudaReversovalores.html',function(data){
				$('#ayuda').html(data);
			});
		}
	});
	//Dialog Colaboracion en linea
	$("#dialog-form2").dialog({
		autoOpen: false,
		height: 400,
		width: 500,
		modal: true,
		buttons: {
		'Enviar': function() {
			var bValid = true;
			var campo=$('#notas').val();
			var campo0=$.trim(campo);
			if (campo0==""){
				$(this).dialog('close');
				return false;
			}
			var campo1=$('#usuario').val();
			var campo2="radicacion.php";
			$.post(URL+'phpComunes/colaboracion.php',{v0:campo0,v1:campo1,v2:campo2},
			function(datos){
				if(datos=='1'){
					alert("Su comentario fue enviado correctamente!. Gracias por participar en el mejoramiento de SIGAS");
				}else{
					alert(datos);
				}
			});
			$(this).dialog('close');
		},
		Cancelar: function() {
			$(this).dialog('close');
			$("#dialog-form2").dialog("destroy");
		}
		}
	});
	
	$("#txtPeriodoGiro").datepicker({
        dateFormat: 'yymm',
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        onClose: function(dateText, inst) {
        	var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            $(this).datepicker('setDate', new Date(year, month, 1));
       	}
    });
    	
	$("#txtPeriodoGiro").focus(function () {
		$(".ui-datepicker-calendar").hide();
		$("#ui-datepicker-div").position({
			my: "center top",
			at: "center bottom",
			of: $(this)
		});
	}); 
	
});//fin ready

function mostrar(){
	var opt=$("#buscarPor").val();
	limpiarCampos();
	if(opt==1){
		$("#tNumero").show();
		$("#tBono").hide();
		}
	if(opt==2){ 
		$("#tNumero").hide();
		$("#tBono").show();
		$("#txtBono").focus();
		}
	
	}
	
function buscarPersona(){
	var tipoI=$("#tipoI").val();
	var numero=$("#numero").val();
	var idp=0;
	if(numero.length==0){
		$("#numero").focus();
		return false;
		}
			
	$.ajax({
		url: URL+'phpComunes/pdo.buscar.persona.php',
		type: "POST",
		async: false,
		data: {v0:tipoI,v1:numero,v2:2},
		dataType: "json",
		success: function(data){
			if(data==0){
				alert("Lo lamento, el n\u00FAmero no existe!");
				$("#numero").val("");
				$("#numero").focus();
				return false;
			}
			var nom=data[0].pnombre+" "+data[0].snombre+" "+data[0].papellido+" "+data[0].sapellido;
			idp=data[0].idpersona;
			$("#lnombre").html(nom);
		},
		complete: function(){
			$.getJSON(URL+'phpComunes/pdo.buscar.tarjeta.act.php',{v0:idp},function(datos){
				if(datos==0){
					alert("Lo lamento, no hay bono asociado a ese n�mero!");
					return false;
				}
			
			$("#lbono").html(datos[0].bono);
			$("#lfechasol").html(datos[0].fechasolicitud);
			$("#lsaldo").html(formatCurrency(datos[0].saldo));
			$("#lestado").html(datos[0].estado);
			estado=datos[0].estado;
			saldo=datos[0].saldo;	
			idb=datos[0].idtarjeta;
			$.ajax({
				url: 'buscarRegistro.php',
				type: "POST",
				async: false,
				data: {v0:idb},
				dataType: "json",
				success: function(data){
				if(data>0){
					alert("Ya existe un reverso pendiente de procesar!");
					limpiarCampos();
				}
				}
		});
	});	//fin buscar persona
			}
	})
	}	

function buscarPersona2(){
	var bono=$("#txtBono").val();
	if(bono.length==0){
		$("#txtBono").focus();
		return false;
		}
	
	$.getJSON('buscarPorBonoPrueba.php',{v0:bono},function(data){
		if(data==0){
			alert("Lo lamento, el n\u00FAmero de bono no existe!");
			$("#txtBono").val("");
			$("#txtBono").focus();
			return false;
		}
		if(data==1){
			alert("Hay un reverso pendiente de procesar!");
			$("#txtBono").val("");
			$("#txtBono").focus();
			return false;
		}
		$.each(data,function(i,fila){
			var nom=fila.pnombre+" "+fila.snombre+" "+fila.papellido+" "+fila.sapellido;
			idp=fila.idpersona;
			idb=fila.idtarjeta;
			$("#lnombre").html(nom);
			$("#lbono").html(fila.bono);
			$("#lestado").html(fila.estado);
			$("#lfechasol").html(fechaHoy());
			$("#lsaldo").html(formatCurrency(fila.saldo));
			estado=fila.estado;
			saldo=fila.saldo;
			return;
		});
	})
	}	

function limpiarCampos(){
	//$("table.tablero input:text").val('');
	$("table.tablero select").val(0);
	$("#lnombre,#lbono,#lfechasol,#lsaldo").html(" ");
	$("#lestado").html("");
	$("#txtBono").focus();
	//$("#notas").val("");
	$("#txtBono").val("");
	$("#txtReverso").val("");
}

function guardar(){
	reverso=$("#txtReverso").val();
	
	if(reverso.length==0){
		alert("Digite el valor a reversar!");
		return false;
		}
	cont=saldo-reverso;
	if(cont < 0){
		alert("No puede reversar un valor mayor al saldo!");
		return false;
	}
	var notas=$("#notas").val();
	if(notas.length==0){
		alert("Digite las notas!");
		return false;
	}
	if(notas.length<10){
		alert("Las notas son muy cortas...!");
		return false;
	}
	var periodoGiro = $("#txtPeriodoGiro").val();
	if(periodoGiro.length==0){
		alert("Seleccion el periodo giro!");
		return false;
	}
	if($("#mReverso").val()==0){
		alert("Falta el motivo del reverso!");
		return false;
		}
	if($("#mReverso").val()=='2881'){
		var b=$.trim($("#txtBonoNuevo").val());
		if(b.length != 16){
			alert("Falta numero Tajeta nueva!");
			return false;
		}
	}
	var reverso=parseInt($("#txtReverso").val());
	var idmotivo=$("#mReverso").val();
	var continuar=1;
	
	var idnota=0;
	var notas=$("#notas").val();
	$.ajax({
		url: URL+'asopagos/guardarNota.php',
		type: "POST",
		async: false,
		data: {v0:idb,v1:notas},
		dataType: "json",
		success: function(data){
			if(data>0){
				idnota=data;
				$.ajax({
					url:'reversarPrueba.php',
					type:'POST',
					async:false,
					data:{v0:idb,v1:reverso,v2:idnota,v3:idmotivo,v4:'1',v5:periodoGiro},
					dataType:'json',
					success:function(datos){
						if(datos>0){
							alert("Valor reversado!");
							limpiarCampos();
						}
						else{
							alert("No se pudo realizar el Reverso, comuniquese con el administrador del sistema, error: "+datos)
							continuar=0;
							return false
						}
					},
					complete:function(){
						if((idmotivo==2881) && (continuar==1)){
							$.ajax({
								url:'reversar.php',
								type:'POST',
								async:false,
								data:{v0:idnt,v1:reverso,v2:idnota,v3:idmotivo,v4:'0',v5:periodoGiro},
								dataType:'json',
								success:function(datos){
									if(datos>0){
										alert("Valor abonado a la nueva tarjeta!");
										limpiarCampos();
									}
									else{
										alert("No se pudo realizar el Abono a la nueva tarjeta, comuniquese con el administrador del sistema, error: "+datos)
										continuar=0;
										return false
									}
								}
							})
						}
					}
				})
				
			}
			else{
				alert("No se pudo realizar el Reverso, comuniquese con el administrador del sistema, error: "+data)
				continuar=0;
				return false;
			}
			
		}
	});
	
}
	
function mostrarTr(){
	var idtr=$("#mReverso").val();
	if( idtr=='2881'){
		$("#nuevaTarjeta").show();
		$("#txtReverso").val(saldo);
		}
	else{
		$("#nuevaTarjeta").hide();
		}
	}	

function buscarNuevaT(){
	//activa
	var bono=$("#txtBonoNuevo").val();
	if(bono.length==0){
		$("#txtBonoNuevo").focus();
		return false;
		}
	
	$.getJSON('buscarPorBonoPrueba.php',{v0:bono},function(data){
		if(data==0){
			alert("Lo lamento, el n\u00FAmero de bono no existe!");
			$("#txtBonoNuevo").val("");
			return false;
		}
		if(data==1){
			alert("Hay un reverso pendiente de procesar!");
			$("#txtBonoNuevo").val("");
			return false;
		}
		$.each(data,function(i,fila){
			var nom=fila.pnombre+" "+fila.snombre+" "+fila.papellido+" "+fila.sapellido;
			$("#trNombre").html(nom);
			idnt=fila.idtarjeta;
		});
	})
	
	
}
