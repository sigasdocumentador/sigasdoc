/*
* @autor:      Ing. Orlando Puentes
* @fecha:      septiembre 6 de 2010
* objetivo:
*/
var URL=src();
var estado=0;
var idp=0;
var idb=0;

//Dialog Ayuda
function mostrarAyuda(){
	$("#ayuda").dialog('open' );
}

function notas(){
	$("#dialog-form2").dialog('open');
}

$(document).ready(function(){
	//Dialog ayuda
	$("#ayuda").dialog({
		autoOpen: false,
		height: 500,
		width: 750,
		draggable:true,
		modal:false,
		open: function(evt, ui){
			$('#ayuda').html('');
			$.get(URL +'help/tarjetas/manualayudaBloqueotarjeta.html',function(data){
				$('#ayuda').html(data);
			});
		}
	});
	//Dialog Colaboracion en linea
	$("#dialog-form2").dialog({
		autoOpen: false,
		height: 400,
		width: 500,
		modal: true,
		buttons: {
		'Enviar': function() {
			var bValid = true;
			var campo=$('#notas').val();
			var campo0=$.trim(campo);
			if (campo0==""){
				$(this).dialog('close');
				return false;
			}
			var campo1=$('#usuario').val();
			var campo2="radicacion.php";
			$.post(URL+'phpComunes/colaboracion.php',{v0:campo0,v1:campo1,v2:campo2},
			function(datos){
				if(datos=='1'){
					alert("Su comentario fue enviado correctamente!. Gracias por participar en el mejoramiento de SIGAS");
				}else{
					alert(datos);
				}
			});
			$(this).dialog('close');
		},
		Cancelar: function() {
			$(this).dialog('close');
			$("#dialog-form2").dialog("destroy");
		}
		}
	});
	$("#buscarPor").bind('change',function(){
 		$("#lnombre,#lbono,#lfechasol,#lsaldo").html(" ");
		$("#lestado").html("");
//		$("#buscarPor").focus();
		});
	
		
	
	
});//fin ready

function mostrar(){
	var opt=$("#buscarPor").val();
	if(opt==0){
		$("#tNumero").css("display", "none"); 
		$("#tBono").css("display", "none"); 
		$("#numero").val("");
		}
	if(opt==1){
		$("#tNumero").css("display", "block"); 
		$("#tBono").css("display", "none"); 
		$("#tipoI").focus();
		}
	if(opt==2){
		$("#tBono").css("display", "block"); 
		$("#tNumero").css("display", "none"); 
		$("#txtBono").focus();
		}
	}
	
function buscarPersona(){
	var tipoI=$("#tipoI").val();
	var numero=$("#numero").val();
	if(numero.length==0){
		return false;
		}
	$.getJSON(URL+'phpComunes/buscarPersona2.php',{v0:tipoI,v1:numero},function(data){
		if(data==0){
			alert("Lo lamento, el n\u00FAmero no existe!");
			$("#numero").val("");
			$("#numero").focus();
			return false;
		}
		$.each(data,function(i,fila){
		var nom=fila.pnombre+" "+fila.snombre+" "+fila.papellido+" "+fila.sapellido;
		idp=fila.idpersona;
		$("#lnombre").html(nom);
		//return;
		});
	$.getJSON(URL+'phpComunes/pdo.buscar.tarjeta.act.php',{v0:idp},function(datos){
			if(datos==0){
				alert("Lo lamento, no hay bono asociado a ese n�mero!");
				return false;
			}
		$.each(datos,function(i,fila){
			if(fila.SALDO==null){
				fila.SALDO='0';	
			}
			idb=fila.idtarjeta;
			$("#lbono").html(fila.bono);
			$("#lfechasol").html(fila.fechasolicitud);
			$("#lestado").html(fila.estado);
			$("#lsaldo").html(formatCurrency(fila.saldo));
			estado=fila.estado;
			return;
		});
		});
	})
	}	

function buscarPersona2(){
	var bono=$.trim($("#txtBono").val());
	var idp=0;
	if(bono.length==0){
		alert("Digite el n\u00FAmero de Bono!");
		$("#txtBono").focus();
		return false;
		}
	$.ajax({
		url: URL+'phpComunes/pdo.buscar.tarjeta.act2.php',
		type: "POST",
		async: false,
		data: {v0:bono},
		dataType: "json",
		success: function(datos){
		if(datos==0){
			alert("Lo lamento, el bono no existe!");
             limpiarCamposBloqueo();
			return false;
		}
		idb=datos[0].idtarjeta;
		idp=datos[0].idpersona;
		$("#lbono").html(datos[0].bono);
		$("#lfechasol").html(datos[0].fechasolicitud);
		$("#lsaldo").html(formatCurrency(datos[0].saldo));
		$("#lestado").html(datos[0].estado);
		estado=datos[0].estado;
		
		$.getJSON(URL+'phpComunes/pdo.buscar.persona.id.php',{v0:idp},function(data){
		if(data==0){
			alert("Lo lamento, el n\u00FAmero no existe!");
			$("#numero").val("");
			$("#numero").focus();
			return false;
		}
		$.each(data,function(i,fila){
			var nom=fila.pnombre+" "+fila.snombre+" "+fila.papellido+" "+fila.sapellido;
			$("#lnombre").html(nom);
			return;
			});
		});
		}
		});
	
	}	

function limpiarCampos(){
	$("table.tablero input:text").val('');
	$("table.tablero select").val(0);
	$("#lnombre,#lbono,#lfechasol,#lsaldo").html(" ");
	$("#lestado").html("");
	$("#tNumero").css("display", "none"); 
	$("#tBono").css("display", "none"); 
	$("#numero").val("");
	$("#buscarPor").focus();
}


function limpiarCamposBloqueo(){
	$("table.tablero input:text").val('');
	$("#lnombre,#lbono,#lfechasol,#lsaldo").html(" ");
	$("#lestado").html("");
	$("#tNumero").css("display", "none"); 
	$("#tBono").css("display", "none"); 
	$("#numero").val("");
	$("#txtBono").focus();
	mostrar();
}
function guardar(){
	if(estado=='B'){
		alert("La tarjeta YA esta BLOQUEADA!");
		return false;
	}
	var opt=$("#buscarPor").val();
	var notas=$("#notas").val();
	if(opt==0){
		alert("No hay informacion para guardar!");
		return false;
	}
	if(notas.length==0){
		alert("Escriba las notas!");
		return false;
	}
	if(notas.length<10){
		alert("Las notas son muy cortas...!");
		return false;
	}
	var codigo=parseInt($("#codBloqueo").val());
	$.ajax({
	url: 'bloquear.php',
	type: "POST",
	async: false,
	data: {v0:idb,v1:codigo},
	success: function(datos){
		if(datos==0){
			alert("Lo lamento, no se pudo BLOQUEAR la tarjeta!")
			return false;
		}
		if(datos==2){
			alert("Lo lamento, no se pudo Insertar el registro en la 113, por favor comunicar al Administrador del sistema!");
		}
		if(datos==1){
		if(notas.length>0){
			$.getJSON(URL+'asopagos/guardarNota.php',{v0:idb,v1:notas},function(data){
				if(data==0){
					alert("No se pudo guardar las notas, por favor comunicar al Administrador del Sistema!");
					}
				if(data>0){
					alert("La tarjeta fue BLOQUEADA, genere el plano!");
					limpiarCampos();
				}
				else{
					alert("No se pudo guardar las notas, por favor comunicar al Administrador del Sistema!"+data);
					}
			});
		}
		}
	}
	});
}
