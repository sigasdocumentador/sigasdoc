// funcion para obtener cookie en javascript
	function getCookie(c_name)
	{
		var i,x,y,ARRcookies=document.cookie.split(";");
		for (i=0;i<ARRcookies.length;i++)
		{
  			x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
  			y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
  			x=x.replace(/^\s+|\s+$/g,"");
  			if (x==c_name)
    			{
    				return unescape(y);
    			}// fin if
		}// fin for
  	}// fin getCookie
//funcion para obtener cookie en javascript

var paginaActual=null;
$(document).ready(function(){
	var URL = src();	
	
	$('#buscarListado,#buscarTarjeta').button();
	
	$("#buscarTarjeta").click(function(){
		$("#tDatos tbody,tfoot").empty();
		//VALIDAR QUE EL NIT SEA NUMEROS
		if( $("#buscarPor").val()=="1"&& isNaN($("#idTarjeta").val()) ){
			$(this).next("span.Rojo").html("La Identificacion debe ser num\u00E9rico.").hide().fadeIn("slow");
				return false;
		}
		
		if($("#buscarPor").val()=='Seleccione..'){
			$(this).next("span.Rojo").html("Seleccione el criterio de b\u00FAsqueda.").hide().fadeIn("slow");
			return false;
		}

		if($("#idTarjeta").val()==''){
			$(this).next("span.Rojo").html("Ingrese la Identificacion \u00F3 Bono").hide().fadeIn("slow");
			$("#idEmpresa").focus();
			return false;
		}

		tarjetasPrescripciones(1,[$("#idTarjeta").val().toUpperCase(),$("#buscarPor").val()]);
	});//fin click
	
	/* Genera el listado automaticamente */
	//tarjetasPrescripciones(1,[$("#idTarjeta").val().toUpperCase(),$("#buscarPor").val()]);
	
	$("#buscarListado").click(function(){
		$("#tDatos tbody,tfoot").empty();
		var fecha=$("#fechaCorte").val();
		if(fecha.length==0){
			alert("Debe colocar la Fecha de Corte!");
			return false;
		}
		
		tarjetasPrescripciones(1,[$("#idTarjeta").val().toUpperCase(),$("#buscarPor").val()]);

	});//fin click
	
	
	
	
}); // dom ready


//Generar Lista de las prescripciones
function procesar01(){
//Genera reporte en excel
var fecha=$("#txtFecha").val(); //Fecha del sistema	
var fechcorte = $("#fechaCorte").val();
var usuario=$("#txtusuario").val();

url="http://"+getCookie("URL_Reportes")+"/asopagos/rptExcel038.jsp?v0="+ usuario + "&v1=" + fecha + "&v2=" + fechcorte;


window.open(url,"_NEW");
}

function tarjetasPrescripciones(pagina,parametros){
	var fechaCorte=$("#fechaCorte").val();
	paginaActual = pagina;
	$("#tDatos tbody").empty();
	var documento = "";
	var bono = "";
	if(parametros.length == 2){
		// buscar por
		if(parametros[1] == "documento")
			documento = parametros[0];
		else if(parametros[1] == "bono"){
			bono = parametros[0];
		}
	}
	dialogLoading('show');
	$.getJSON('contarPrescripciones.php?fechaCorte='+ fechaCorte +"&documento="+ documento +"&bono="+ bono,function(totalRegistros){
		var regsPorPagina = 30;
		var numPaginas = Math.ceil(totalRegistros/regsPorPagina);
		var strEnlacesPag = "";
		$("#span_num_empresas").html(totalRegistros);
		c=0;
		for(var i = pagina; i<=numPaginas; i++){
			if(c <= 10){
				strEnlacesPag += "<a href='#' id='pagina_"+ i +"' class='link_paginacion' >"+ i +"</a> ";
			}
			c++;	
		}
		
		var strEnlaceAnterior = "";
		if(pagina > 1)
			strEnlaceAnterior = "<a href='#' id='pagina_anterior'> << </a>";
		
		var strEnlaceSiguiente = "";
		if(pagina < numPaginas)
			strEnlaceSiguiente = "<a href='#' id='pagina_siguiente'> >> </a>";
		
		strEnlacesPag = strEnlaceAnterior + strEnlacesPag + strEnlaceSiguiente;
		//carga las empresas de la BD
		$.getJSON('tarjetasPrescripciones.php?fechaCorte='+ fechaCorte + "&pagina="+ pagina +'&numRegistros='+regsPorPagina +'&bono='+ bono +"&documento="+ documento,function(data){
		  	if(data == 0){
		  		alert("no hay datos");
		  		dialogLoading("hide");
		  		return false;
		  		//$("#idEmpresa").val("");
		  		//$("#cmbIdCausal").val("0");
		  		//tarjetasPrescripciones(paginaActual,[$("#idEmpresa").val().toUpperCase(),$("#buscarPor").val()]);
		  	}
		  	dialogLoading("hide");
		  	var idval=0;
		  	$.each(data,function(i,fila){
		  		var componen = fila.can_total_tarjetas<=1 ? "<input type='checkbox' value='" + fila.idtarjeta +"*"+ fila.SaldoCorte +"*"+ fila.saldoActual +"*"+ fila.identificacion +"*"+ fila.idpersona +"'/>" : "<input type='button' id='"+ idval+ "*" + fila.idtarjeta +"*"+ fila.SaldoCorte +"*"+ fila.saldoActual +"*"+ fila.identificacion +"*"+ fila.idpersona +"' class='validar_prescripcion' value='validar'/>";
		  		$("#tDatos").append("<tr><td style=text-align:center><a class='consueventosper' id='"+ idval+ "*" + fila.idtarjeta +"*"+ fila.SaldoCorte +"*"+ fila.saldoActual +"*"+ fila.identificacion +"*"+ fila.idpersona +"'>"+fila.nombre+"</a></td><td style=text-align:center>"+fila.identificacion+"</td><td style=text-align:center>"+fila.tarjeta+"</td><td style=text-align:left>"+fila.SaldoCorte+"</td><td style=text-align:center>"+fila.Reverso+"</td><td style=text-align:center>"+fila.saldoActual+"</td><td style=text-align:center>"+fila.direccion+"</td><td style=text-align:center>"+fila.telefono+"</td><td>"+componen+"</td></tr>");
				idval++;
		  	});
		  	
		  	$("table.tablaR tr:even").addClass("zebra");
			//Dar un ID dinamico a los check box de la tabla de empresas
			$("table.tablaR td input:checkbox").each(function(index){
				//$(this).attr({"id":"chk"+index,"value": $(this).parents("tr").children("td:first").find("a").html()}).hide();//id de los check
			});
			
						
			$(":button").button();
			
			// ejecuta el evento sobre el boton validar y pasa los parametros al dialog
			$(":button.validar_prescripcion").bind('click',function(){
				var $input= $(this);
				$('#tablavalidarprescripcion').data('parametro',$input.attr('id')).dialog('open');
				
			});
			
			$(".consueventosper").each(function (ind,enlace){
				$(enlace).bind('click',function(){
					var $input= $(this);
					$('#tablavalidarprescripcion').data('parametro',$input.attr('id')).dialog('open');
				});
			});
			
			/*$("table.tablaR td img").each(function(index,imagen){
				$(imagen).attr({"id":"img"+index,"value": $(imagen).parents("tr").children("td:first").find("a").html()});//id de los check
				$(imagen).bind('click',function(){
					if($(imagen).next("input:checkbox").is(":checked")){
						$(imagen).attr("src",URL+"imagenes/chk0.png");
						$(imagen).next("input:checkbox").attr("checked",false);
					}else{
						$(imagen).attr("src",URL+"imagenes/chk1.png");
						$(imagen).next("input:checkbox").attr("checked",true);
					}
				});
			});*/
		});//end post
		
		$("#tDatos tfoot").html("<tr><td colspan='8'>"+ strEnlacesPag +"</td></tr>");
		$(".link_paginacion").each(function(ind,enlace){
			$(enlace).bind('click',function(){
				var pag = $(this).attr("id").replace("pagina_","");
				tarjetasPrescripciones(pag,[]);
			});
		});
		$("#pagina_anterior").bind("click",function(){
			if(pagina > 1)
				tarjetasPrescripciones(parseInt(pagina)-1,[$("#idTarjeta").val().toUpperCase(),$("#buscarPor").val()]);
		});
		
		$("#pagina_siguiente").bind("click",function(){
			if(pagina < numPaginas)
				tarjetasPrescripciones(parseInt(pagina)+1,[$("#idTarjeta").val().toUpperCase(),$("#buscarPor").val()]);
		});
		
		$("#pagina_"+pagina).addClass("pagina_actual");
		

		
		
	});	
}

$(document).ready(function(){
	//Checkbox
	$("input[name=checktodos]").change(function(){
		$('input[type=checkbox]').each( function() {			
			if($("input[name=checktodos]:checked").length == 1){
				this.checked = true;
			} else {
				this.checked = false;
			}
		});
	});

	$("#periodoGiro").datepicker({
	    dateFormat: 'yymm',
	    changeMonth: true,
	    changeYear: true,
	    showButtonPanel: true,
	    onClose: function(dateText, inst) {
	    	var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
	        var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
	        $(this).datepicker('setDate', new Date(year, month, 1));
	   	}
	});
		
	$("#periodoGiro").focus(function () {
		$(".ui-datepicker-calendar").hide();
		$("#ui-datepicker-div").position({
			my: "center top",
			at: "center bottom",
			of: $(this)
		});
	});
	
	$("#fechaCorte").datepicker({
		maxDate:"+0D",
		changeMonth: true,
		changeYear: true
	});
	
});

$(function(){
	
	
	//dialog abrir tabla de validación prescripciones
	$("#tablavalidarprescripcion").dialog({
	    autoOpen:false,
		modal:true,
		width: 1300,
		open:function(){
			// recibimos los parametros enviados al dialog para pasarlos a la consulta ajax por getJSON
			var parm=$('#tablavalidarprescripcion').data('parametro');
			var fechaCorte=new Date($('#fechaCorte').val());   //variable importante para determinar los bonos que ingresan a la prescripcion
			$.getJSON('validarPrescripciones.php?fechaCorte='+fechaCorte+'&dataPer='+parm,function (data){
			   $('#bonoActual tbody').empty(); //vaciamos los tbody de ambas tablas del dialog
			   $('#bonoGeneral tbody').empty();
			  if(data==0){
				  alert('Error reporte al administrador');
			  }else{
				  $('#txtPresIdentificacion').val(data.identificacion);
				  $('#txtPresNombre').val(data.nombre);
				  $('#txPresDireccion').val(data.direccion);
				  $('#txtPresTelefono').val(data.telefono);
				  $('#txtSaldoActual').val(data.saldotar);
				  $('#txtPresIdPersona').val(data.idpersona);
				  $('#numtarjeta').html(data.tarjeta);
				  $('#numtarjeta').html(data.tarjeta);
				  $('#txtPresidfila').val(data.filatable);
				  $('#txtIdTarjeta').val(data.idtarjeta);
				  $('input[name=rvalidar]').attr('checked',false);
				  //$('#rvalidar2').attr('checked',false);
				  var tarjetanum='0';
				  var tarjeta='';
				  var credito = 0;
				  var debito = 0;
				  // recorremos los datos recibido de ajax
				  var disabled='';
				  var componengen ="";
				  var componenact ="";
				  $.each(data.contenido,function(index,value){
				      var fechareg = new Date(value.fechatransaccion);				  
				      if(tarjeta!=value.tarjeta){
						 if(tarjeta!=''){
						    saldo = credito - debito;
							$('#bonoGeneral tbody').append('<tr bgcolor=#F5F6CE><td>Saldo Tarjeta:</td><td>'+ tarjeta +'</td><td>'+saldo+'</td><td></td></tr>');
							credito=0;
							debito=0;
						 }	  
						 tarjeta=value.tarjeta;
						 tarjetanum++;
						 color= tarjetanum % 2 == 0 ? '#fff' : ''; // se utiliza para cambiar los colores segun la tarjeta
				      }
					  //seccion para contabilidar los valores positivos y negativos de la tarjeta
					  if(value.descripcion.replace(/\s/g,'')=="50ABONO" || value.descripcion.replace(/\s/g,'')=="50SALDOINICILAL"){
					    credito += parseInt(value.valor);
					  }else{
					   	debito += parseInt(value.valor);
					  }	
					  //fin 
					  if(value.cp==2){
						  disabled="disabled checked";
					  }
					  if(value.cp==3 && (value.descripcion.replace(/\s/g,'')=="50ABONO" || value.descripcion.replace(/\s/g,'')=="50SALDOINICILAL")){
						  componengen ="";
						  componenact ="<input class='positivoact' id='"+ value.idconsumo + "*" + value.descripcion.replace(/\s/g,'') + "*" + value.fechatransaccion + "' value='"+value.valor+"' type='checkbox' checked onclick='calculotaractual()'/>";
					  }else{
						  if(value.cp==3 && (value.descripcion.replace(/\s/g,'')=="0MNCOMPRACUOTAMONETARIA" || value.descripcion.replace(/\s/g,'')=="30CARGOS")){
							  componengen ="";
							  componenact ="<input class='negativoact' id='"+ value.idconsumo + "*" + value.descripcion.replace(/\s/g,'') + "*" + value.fechatransaccion + "' value='"+value.valor+"' type='checkbox' checked onclick='calculotaractual()'/>";
						  }else{
						     if(fechareg <= fechaCorte && (value.descripcion.replace(/\s/g,'')=="50ABONO" || value.descripcion.replace(/\s/g,'')=="50SALDOINICILAL")){
						    	 componengen ="<input class='positivogen' id='"+ value.idconsumo + "*" + value.descripcion.replace(/\s/g,'') + "*" +  value.fechatransaccion + "' value='"+value.valor+"' type='checkbox' checked onclick='calculotargeneral()' "+disabled+"/>"; 
						    	 componenact ="<input class='positivoact' id='"+ value.idconsumo + "*" + value.descripcion.replace(/\s/g,'') + "*" +  value.fechatransaccion + "' value='"+value.valor+"' type='checkbox' checked onclick='calculotaractual()'/>";
						     }else{
						    	 if(fechareg > fechaCorte && (value.descripcion.replace(/\s/g,'')=="50ABONO" || value.descripcion.replace(/\s/g,'')=="50SALDOINICILAL")){
						    		 componengen ="<input class='positivogen' id='"+ value.idconsumo + "*" + value.descripcion.replace(/\s/g,'') + "*" + value.fechatransaccion + "' value='"+value.valor+"' type='checkbox'  onclick='calculotargeneral()' "+disabled+"/>"; 
							    	 componenact ="<input class='positivoact' id='"+ value.idconsumo + "*" + value.descripcion.replace(/\s/g,'') + "*" + value.fechatransaccion + "' value='"+value.valor+"' type='checkbox'  onclick='calculotaractual()'/>";
						    	 }else{
						    		 if(value.descripcion.replace(/\s/g,'')=="0MNCOMPRACUOTAMONETARIA" ||  value.descripcion.replace(/\s/g,'')=="30CARGOS"){
							    		 componengen ="<input name='"+ value.descripcion.replace(/\s/g,'') + "' class='negativogen' id='"+ value.idconsumo + "*" + value.descripcion.replace(/\s/g,'') + "*" + value.fechatransaccion + "' value='"+value.valor+"' type='checkbox' checked onclick='calculotargeneral()' "+disabled+"/>";
							    		 componenact ="<input name='"+ value.descripcion.replace(/\s/g,'') + "' class='negativoact' id='"+ value.idconsumo + "*" + value.descripcion.replace(/\s/g,'') + "*" + value.fechatransaccion + "' value='"+value.valor+"' type='checkbox' checked onclick='calculotaractual()'/>";
							    	 } 
						    	 }
						    	 
						     }
						  }
					  }
					  
					  if(tarjetanum==value.can_tarjetas)
						  $('#bonoActual tbody').append('<tr ><td>'+value.descripcion+'</td><td>'+value.valor+'</td><td>'+value.fechatransaccion+'</td><td>'+componenact+'</td></tr>');	  
					  $('#bonoGeneral tbody').append('<tr bgcolor='+color+'><td>'+value.tarjeta+'</td><td>'+value.descripcion+'</td><td>'+value.valor+'</td><td>'+value.fechatransaccion+'</td><td>'+componengen+'</td></tr>');
					  componen='';
					  disabled='';
				  })
				  saldo = credito - debito;
				  $('#bonoGeneral tbody').append('<tr bgcolor=#F5F6CE><td>Saldo Tarjeta:</td><td>'+ tarjeta +'</td><td>'+saldo+'</td><td></td></tr>');
				  calculotargeneral();
				  calculotaractual();
				  
				  //click en seleccion
				  $("#rvalidar[value='general']").bind("click", function(){
					  calculotargeneral();
				  });
				  $("#rvalidar[value='actual']").bind("click", function(){
					  calculotaractual();  
				  });
				  
			  }
			});
			
		},
		buttons:{
		   "VALIDAR":function(){
			//alert ('Esta seguro de guardar la información.');
			   var validador=0;
			   $('input[name=rvalidar]').each(function(index,data){
				   if(data.checked){
					  validador=1;
					  conten=data.value;
				   }
			   });
			   if(validador==1){
				  if(confirm('Esta seguro de validar la prescripción')){
					  var x = 0;
					  var y = 0;
					  var chktrue=[];
					  var chkfalse=[];
					  var fil = $('#txtPresidfila').val();
					  if(conten=='general'){
						 if(parseInt($('#txtSaldoActual').val())<parseInt($('#txtValorPrescripcionGeneral').val()) || parseInt($('#txtValorPrescripcionGeneral').val())<=0){
							 alert('Error el valor a prescribir no puede ser mayor al saldo actual o menor e igual a cero. Saldo Actual:'+$('#txtSaldoActual').val()+'  Valor a prescribir: '+ $('#txtValorPrescripcionGeneral').val());
							 return false;
						 }else{
						    $('#bonoGeneral input:checkbox').each(function(index,data){
						       if(!data.disabled){
							       if(data.checked){
								      chktrue[x]=data.id;
								      x++;
							       }else{
							          chkfalse[y]=data.id;
							          y++;
							       }
						       } 
						    }); 
						 // actualizamo los item de eventos mediante su campo idconsumo para evitar la validacion nuevamente
						      dialogLoading('show');
						      $.getJSON("registroevenprescripcion.php?chktrue=" + chktrue + "&chkfalse=" + chkfalse + "&fechacorte=" + $('#fechaCorte').val(),function(data){
								 if(data==0){
									 alert('Hubo problemas con el update de los consumos de la tabla aportes152, codigo 101010 reportelo al administrador');
									 return false;
								 }else{
									 dialogLoading('hide');
									 if(data.contenido>0){
									   alert('validación exitosa');
									   var chkboxpres= "<input type='checkbox' value='" + $('#txtIdTarjeta').val() +"*"+ $('#txtValorPrescripcionGeneral').val() +"*"+ $('#txtSaldoActual').val() +"*"+ $('#txtPresIdentificacion').val() +"*"+ $('#txtPresIdPersona').val() +"'/>"
									   $('#tDatos tbody tr:eq('+fil+') td:eq(3)').html($('#txtValorPrescripcionGeneral').val());
									   $('#tDatos tbody tr:eq('+fil+') td:eq(4)').html($('#txtPresRever').val());
									   $('#tDatos tbody tr:eq('+fil+') td:eq(5)').html($('#txtSaldoActual').val());
									   $('#tDatos tbody tr:eq('+fil+') td:eq(8)').html(chkboxpres);
									   $('#bonoActual tbody').empty(); //vaciamos los tbody de ambas tablas del dialog
									   $('#bonoGeneral tbody').empty();
									   $('#tablavalidarprescripcion').dialog('close');
									 }else{
									   alert('Se presento el siguiente error:' + data.error); 
									   return false;
									 }
								 }
							  });
							  //fin actualizacion de consumos tabla aportes152	    
						 }
					  }else{
						  if(parseInt($('#txtSaldoActual').val()) < parseInt($('#txtValorPrescripcionActual').val()) || parseInt($('#txtValorPrescripcionActual').val())<=0){
								 alert('Error el valor a prescribir no puede ser mayor al saldo actual o menor e igual a cero.  Saldo Actual:'+$('#txtSaldoActual').val()+'  Valor a prescribir: '+ $('#txtValorPrescripcionActual').val());
								 return false;
							 }else{
								 alert('Validacion exitosa');
								   var chkboxpres= "<input type='checkbox' value='" + $('#txtIdTarjeta').val() +"*"+ $('#txtValorPrescripcionActual').val() +"*"+ $('#txtSaldoActual').val() +"*"+ $('#txtPresIdentificacion').val() +"*"+ $('#txtPresIdPersona').val() +"'/>"
								   $('#tDatos tbody tr:eq('+fil+') td:eq(3)').html($('#txtValorPrescripcionActual').val());
								   $('#tDatos tbody tr:eq('+fil+') td:eq(4)').html($('#txtPresRever').val());
								   $('#tDatos tbody tr:eq('+fil+') td:eq(5)').html($('#txtSaldoActual').val());
								   $('#tDatos tbody tr:eq('+fil+') td:eq(8)').html(chkboxpres);
								   $('#bonoActual tbody').empty(); //vaciamos los tbody de ambas tablas del dialog
								   $('#bonoGeneral tbody').empty();
								   $('#tablavalidarprescripcion').dialog('close'); 
						     }
					  }
					 
				  }
     		   }else{
				   alert("Debe seleccionar si la prescripcion se va a realizar con los eventos de la tarjeta actual o con los de todas las tarjetas");
			   }
		    }
		}
	});
    //fin dialog validacion prescripciones

	$("#dialog-reverso-mayor").dialog({
		autoOpen:false,
		width:640,
		hide: "clip",	
		open:function(event,ui){
		},//fin funcion open
		
		close:function(event,ui){
			$("#lidentificacion").html("");
			$("#lbono").html("");	
			$("#lsaldo").html("");	
		}
	});//fin dialog
});

//funcion para contabilizar datos debitos y creditos de las prescripciones
//crdito Genera
function  calculotargeneral(){
	var valpositivogen=0;
	var valnegativogen=0;
	var valpresgen=0;
	var revergen=0;
	var epositivo=document.getElementsByClassName('positivogen');
	for(var i=0;i <epositivo.length;i++){
		if(epositivo[i].checked){
			valpositivogen += parseInt(epositivo[i].value);
			//alert("index: "+ i + " valor:" + epositivo[i].value);	
		}
	       		
	}
	var enegativo=document.getElementsByClassName('negativogen');
	for(var i=0;i <enegativo.length;i++){
		if(enegativo[i].checked){
			valnegativogen += parseInt(enegativo[i].value);
			if(enegativo[i].name=="30CARGOS"){
			    revergen += parseInt(enegativo[i].value);
			}
		}
	       		
	}
	$('#txtValorBonoGeneral').val(valpositivogen);
	$('#txtValorDebitoGeneral').val(valnegativogen);
	$('#txtValorPrescripcionGeneral').val(valpositivogen - valnegativogen);
	$('#txtPresRever').val(revergen);
}

//Debito Actual
function  calculotaractual(){
	var valpositivoact=0;
	var valnegativoact=0;
	var valpresact=0;
	var reveract=0;
	var epositivo=document.getElementsByClassName('positivoact');
	for(var i=0;i <epositivo.length;i++){
		if(epositivo[i].checked){
			valpositivoact += parseInt(epositivo[i].value);
		}
	       		
	}
	var enegativo=document.getElementsByClassName('negativoact');
	for(var i=0;i <enegativo.length;i++){
		if(enegativo[i].checked){
			valnegativoact += parseInt(enegativo[i].value);
			if(enegativo[i].name=="30CARGOS"){
			    reveract += parseInt(enegativo[i].value);
			}
		}
	       		
	}
	$('#txtValorBonoActual').val(valpositivoact);
	$('#txtValorDebitoActual').val(valnegativoact);
	$('#txtValorPrescripcionActual').val(valpositivoact - valnegativoact);
	$('#txtPresRever').val(reveract);
}

function validareventos152(){
	//alert ('Esta seguro de guardar la información.');
	   var validador=0;
	   $('input[name=rvalidar]').each(function(index,data){
		   if(data.checked){
			  validador=1;
			  conten=data.value;
		   }
	   });
	   if(validador==1){
		  if(confirm('Esta seguro de validar la prescripción')){
			  var x = 0;
			  var y = 0;
			  var chktrue=[];
			  var chkfalse=[];
			  var fil = $('#txtPresidfila').val();
			  if(conten=='general'){
				 if(parseInt($('#txtSaldoActual').val())<parseInt($('#txtValorPrescripcionGeneral').val()) || parseInt($('#txtValorPrescripcionGeneral').val())<=0){
					 alert('Error el valor a prescribir no puede ser mayor al saldo actual o menor e igual a cero. Saldo Actual:'+$('#txtSaldoActual').val()+'  Valor a prescribir: '+ $('#txtValorPrescripcionGeneral').val());
					 return false;
				 }else{
				    $('#bonoGeneral input:checkbox').each(function(index,data){
				       if(!data.disabled){
					       if(data.checked){
						      chktrue[x]=data.id;
						      x++;
					       }else{
					          chkfalse[y]=data.id;
					          y++;
					       }
				       } 
				    }); 
				 // actualizamo los item de eventos mediante su campo idconsumo para evitar la validacion nuevamente
				      dialogLoading('show');
					  $.getJSON("registroevenprescripcion.php?chktrue=" + chktrue + "&chkfalse=" + chkfalse + "&fechacorte=" + $('#fechaCorte').val(),function(data){
						 if(data==0){
							 alert('Hubo problemas con el update de los consumos de la tabla aportes152, codigo 101010 reportelo al administrador');
							 return false;
						 }else{
							 dialogLoading('hide');
							 if(data.contenido>0){
							   alert('validación exitosa');
							   var chkboxpres= "<input type='checkbox' value='" + $('#txtIdTarjeta').val() +"*"+ $('#txtValorPrescripcionGeneral').val() +"*"+ $('#txtSaldoActual').val() +"*"+ $('#txtPresIdentificacion').val() +"*"+ $('#txtPresIdPersona').val() +"'/>"
							   $('#tDatos tbody tr:eq('+fil+') td:eq(3)').html($('#txtValorPrescripcionGeneral').val());
							   $('#tDatos tbody tr:eq('+fil+') td:eq(4)').html($('#txtPresRever').val());
							   $('#tDatos tbody tr:eq('+fil+') td:eq(5)').html($('#txtSaldoActual').val());
							   $('#tDatos tbody tr:eq('+fil+') td:eq(8)').html(chkboxpres);
							   $('#bonoActual tbody').empty(); //vaciamos los tbody de ambas tablas del dialog
							   $('#bonoGeneral tbody').empty();
							   $('#tablavalidarprescripcion').dialog('close');
							 }else{
							   alert('Se presento el siguiente error:' + data.error); 
							   return false;
							 }
						 }
					  });
					  //fin actualizacion de consumos tabla aportes152	    
				 }
			  }else{
				  if(parseInt($('#txtSaldoActual').val()) < parseInt($('#txtValorPrescripcionActual').val()) || parseInt($('#txtValorPrescripcionActual').val())<=0){
						 alert('Error el valor a prescribir no puede ser mayor al saldo actual o menor e igual a cero.  Saldo Actual:'+$('#txtSaldoActual').val()+'  Valor a prescribir: '+ $('#txtValorPrescripcionActual').val());
						 return false;
					 }else{
						 alert('Validacion exitosa');
						   var chkboxpres= "<input type='checkbox' value='" + $('#txtIdTarjeta').val() +"*"+ $('#txtValorPrescripcionActual').val() +"*"+ $('#txtSaldoActual').val() +"*"+ $('#txtPresIdentificacion').val() +"*"+ $('#txtPresIdPersona').val() +"'/>"
						   $('#tDatos tbody tr:eq('+fil+') td:eq(3)').html($('#txtValorPrescripcionActual').val());
						   $('#tDatos tbody tr:eq('+fil+') td:eq(4)').html($('#txtPresRever').val());
						   $('#tDatos tbody tr:eq('+fil+') td:eq(5)').html($('#txtSaldoActual').val());
						   $('#tDatos tbody tr:eq('+fil+') td:eq(8)').html(chkboxpres);
						   $('#bonoActual tbody').empty(); //vaciamos los tbody de ambas tablas del dialog
						   $('#bonoGeneral tbody').empty();
						   $('#tablavalidarprescripcion').dialog('close'); 
				     }
			  }
			 
		  }
	   }else{
		   alert("Debe seleccionar si la prescripcion se va a realizar con los eventos de la tarjeta actual o con los de todas las tarjetas");
	   }
}

function guardarMarcadas(){
	if(confirm('Realmente desea continuar con el proceso?'))
	{   
	var seleccionadas = [];
	var marcadastarjetas = [];
	var contador = 0;
	var arrayjson = [];
	
	$("table.tablaR td input:checked").each(function(i){
		var ide = $(this).val();//extraigo el valor del value del check cargado dinamicamente
		seleccionadas[i] = ide;

		arreglo = ide.split('*');
		
		var idt=arreglo[0];
		var saldocorte=arreglo[1];
		var saldoAct=arreglo[2];
		var documento=arreglo[3];
		var idpersona=arreglo[4];
		
		cont=saldoAct-saldocorte;
		if(cont < 0){
			//alert("No puede reversar un valor mayor al saldo!");
			marcadastarjetas[contador] = idt;
			contador++;
		}else{
			
			arrayjson[arrayjson.length] = {
					id_tarjeta : idt,
					id_persona : idpersona,
					reverso : saldocorte,
					documento : documento
				};
		}
	});//Fin valores del check

	if($("#tDatos tbody :checkbox:checked").length > 0){
		
 		var periodo=$("#periodoGiro").val();
 		var nota=$("#notas").val();
 		
 		if(nota.length==0){
 			alert("Digite las notas!");
 			return false;
 		}
 		if(nota.length<10){
 			alert("Las notas son muy cortas...!");
 			return false;
 		}
 		if(periodo.length==0){
 			alert("Seleccion el periodo giro!");
 			return false;
 		}

 		$.getJSON('guardarNotaPrescripcion.php',{v0:arrayjson,v1:nota},function(data){
			if(data == 0)
				alert("Hubo problemas al guardar la nota");
			else{
					idnota=data;
					/*alert("Registros guardados.");*/
				}
			//Reversos
	 		$.getJSON('marcarGuardarReversos.php',{v0:arrayjson, v1:idnota,v2:periodo},function(data){
				if(data == 0)
					alert("Hubo problemas al momento de reversar los valores");
				else{
						alert("Registros reversados.");
					}
	 		}); //Fin getJSON 2
 		});//Fin getJSON 1
 		
 		$.getJSON('marcarGuardarPrescripciones.php',{v0:arrayjson},function(data){
			if(data == 0){
				alert("Prescripciones guardadas.");
				$("#periodoGiro").val("");
				$("#notas").val("");
			}else{
					alert("Hubo problemas al momento de ingresar la prescripcion.");
				}
			
			tarjetasPrescripciones(paginaActual,[$("#idTarjeta").val().toUpperCase(),$("#buscarPor").val()]);
 		}); //Fin getJSON

 		//Buscar tarjetas con reverso mayor al saldo actual
		$.getJSON('buscarTarjetasPrescripcion.php',{v0:marcadastarjetas.join()},function(datos){
			if(datos!=0){
				$.each(datos,function(i,fila){
				$("#DivTable").append("<tr><td style=text-align:center>"+fila.identificacion+"</td><td style=text-align:center>"+fila.bono+"</td><td style=text-align:left>"+fila.saldo+"</td></tr>");
				});
				$("#dialog-reverso-mayor").dialog('open');
			}
		});	//fin buscar tarjeta
 		
 	}else{
 		alert("Debe marcar al menos una Tarjeta.");
 		}
	} // Fin confirm
}