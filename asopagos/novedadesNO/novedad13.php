<?php
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR .'config.php'; 
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR .'ean13.php';
session();

$url=$_SERVER['PHP_SELF'];
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'auditoria.php';
auditar($url);

$fecha=date('Ymd');
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

$ruta= $ruta_generados."asopagos".DIRECTORY_SEPARATOR."novedades_no".DIRECTORY_SEPARATOR;
$c1='CBSP';	// Carácter Fijo 
$c2='636480';	// Código del bolsillo (débito).
$c3='032';	// Código de la caja según Supersubsidio (Ej: 032 Huila).
$c4=date('ymd');	// fecha envio.
$cant="SELECT COUNT(*) as contador FROM aportes149 WHERE fechasistema=cast(getDate() as Date) AND tipoarchivo=2887";
$resultado = $db->querySimple($cant);// consecutivo diario
$temp=$resultado->fetch();
$c5=str_pad($temp['contador']+1,2,"0",STR_PAD_LEFT);
$temp=$c5;
$resultado->closeCursor();
$archivo=$c1.$c2.$c3.$c4.$c5.".txt";
$path =$ruta.$archivo;
$handle=fopen($path, "w");
fclose($handle);

$sql1 = "SELECT bono, codigobarra from aportes100 where codigo='N' and plastico='S'";
$rs = $db->querySimple($sql1);
$con=0;
$espacio=" ";
$dir="CALLE 11 5 13";
$tel='8759544';
while ($row = $rs->fetch()){
	$con++;
	$bono=trim($row['bono']);
	$codbarras=trim($row['codigobarra']);	
	$c1=str_pad($con,6,"0",STR_PAD_LEFT);//secuencia registro
	$c2=str_pad($bono,16,STR_PAD_LEFT);//num de tarjeta
	$c3=str_pad(substr ($codbarras, 0, strlen($codbarras) - 1),50," ",STR_PAD_RIGHT);//info codigo de barras
	$c4=str_pad("ASOPAGOS",40);//nombre empresa afiliado
	$c5=str_pad("ASOPAGOS",40);//nombre centro de distribucion
	$c6=str_pad("ASOPAGOS",40);//contacto centro de distribucion
	$c7=str_pad("ASOPAGOS",40);//direccion centro de distribucion
	$c8=str_pad("ASOPAGOS",40);//ciudad centro de distribucion
	$c9=str_pad("ASOPAGOS",20);//departamento centro de distribucion
	$c10=str_pad($tel,10);//telefono centro de distribucion
	$c11="001";//subtipo
	$cadena=$c1.$c2.$c3.$c4.$c5.$c6.$c7.$c8.$c9.$c10.$c11."\r\n";
	$handle=fopen($path, "a");
	fwrite($handle, $cadena);
	fclose($handle);
	$sql="Update aportes100 set codigo='S',fechasistema=CAST(GETDATE() AS DATE) where bono='$bono'";
	$rs2=$db->queryActualiza($sql);
	if($rs2 != 1){
		//error update log salir?
	}
}
$rs->closeCursor();
$handle=fopen($path, "a"); 
if($handle)
{
	if($con==0){
		echo 2; // no hay registros
		exit();
	}
	else{
	fclose($handle);
	$hora=date("H:i:s");
	$sent="INSERT INTO aportes149(archivo,tipoarchivo,fechasistema,usuario,registros,hora)VALUES('$archivo',2887, cast(getdate() as date),'".$_SESSION['USUARIO']."','$con','$hora')";
	$db->queryActualiza($sent);
	echo 1;
	}
}
$enlace=$ruta_generados."asopagos".DIRECTORY_SEPARATOR."novedades_no".DIRECTORY_SEPARATOR.$archivo;
$_SESSION['ENLACE']=$enlace;
$_SESSION['ARCHIVO']=$archivo;
?>