<?php
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';

include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'utilitario.class.php';

include_once $raiz . DIRECTORY_SEPARATOR . 'asopagos' . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'maestro.proceso.class.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'asopagos' . DIRECTORY_SEPARATOR . 'clases' . DIRECTORY_SEPARATOR . 'log.proceso.class.php';

include_once 'planoSaldo.class.php';

class MaestroSaldo{

	private $idTrazaProceso = 0;
	private $idProceso = 0;
	private $fechaPago = "";
	private $tipoCarge = null;
	private $arrFiles = array();
	private $tipoArchivo = 2927; //[2927][ARCHIVO DIARIO DE SALDOS ASOPAGOS]
	
	/*************************************
	 * ATTR para el carge de los archivos
	 *************************************/
	
	/**
	 * Puede ser: ARCHIVO, EMPRESA, AFILIADO 
	 * @var unknown_type
	 */
	private $codigoError = "";
	
	/**
	 * Los indices de array son: nombre del error, [nombre del archivo plano], [informacion en formato json] 
	 * @var unknown_type
	 */
	private $arrError = array("error"=>0,"descripcion"=>"");
	
	private $objMaestroProceso = null;
	private $tipoProceso = "SALDOS";
	private $usuario = null;
	private $rutaCargados;
	
	private $bandeEtapaProceTermi = null;
	
	private $objLogProceso = null;
	
	private static $con = null;
	
	//private $objService;
	
	function __construct(){
		
		$this->objMaestroProceso = new MaestroProceso();
		$this->objMaestroProceso->setTipo($this->tipoProceso);
		
		$this->objLogProceso = new LogProceso();
	
		//$this->objService = new ClientWSInfWeb(USUARIO_WEB_SIGAS, CONTRASENA_WEB_SIGAS);		
		
		//Conexion
		try{
			self::$con = IFXDbManejador::conectarDB();
			if( self::$con->conexionID==null ){
				throw new Exception(self::$con->error);
			}
		}catch(Exception $e){
			echo $e->getMessage();
			exit();
		}
	}
	
	public function ejecutar($idProceso = 0, $usuario, $fechaPago, $reprocesar=false, $rutaCargados, $tipoCarge,$arrFiles){
		$this->idProceso = $idProceso;
		$this->fechaPago = $fechaPago;
		$this->usuario = $usuario;
		$this->rutaCargados = $rutaCargados;
		$this->tipoCarge = $tipoCarge;
		$this->arrFiles = $arrFiles;
	
		$this->objMaestroProceso->setIdProceso($this->idProceso);
		$this->objMaestroProceso->setUsuario($this->usuario);
		$this->objMaestroProceso->setRutaCargados($this->rutaCargados);
	
		$this->objLogProceso->setUsuario($this->usuario);
	
		if($reprocesar==false){
			$this->procesar();
		}else{
			$this->reprocesar();
		}
	
		return $this->arrError;
	}
	
	private function procesar(){
		
		//Verificar si es un nuevo proceso
		if($this->idProceso==0){
			//iniciar nuevo proceso
			$arrResultado = $this->objMaestroProceso->iniciar_proceso();
			
			if($arrResultado["error"]==1){
				//Error
				$this->arrError["error"] = 1;
				$this->arrError["descripcion"] = $arrResultado["descripcion"];
			}else{
				
				//Resetear procesos antiguos
				$resultado = $this->ejecutar_sp_resetear_proceso();
				if($resultado===false){
					$descripcion = 'Error al resetear los procesos anteriores';
					$this->arrError["error"] = 1;
					$this->arrError["descripcion"] = $descripcion;
					
					//Error al ejecutar el proceso
					$this->guardar_log_db($this->idTrazaProceso,'RESETEAR_DATOS_ANTIGUOS','A',$descripcion, null,'ERROR');
				}else{
					$this->arrError["error"] = 0;
					$this->arrError["descripcion"] = $arrResultado["descripcion"];
					$this->idProceso = $arrResultado["data"]["id_proceso"];
				}
			}
		}
		
		if($this->idProceso>0){
			
			$this->procesar_etapa_proceso();
			
		}else if($this->arrError["error"] == 0){
			$this->arrError["error"] = 1;
			$this->arrError["descripcion"] = "Error: Inesperado metodo: MaestroSaldo->procesar()";
		}
	}
	
	private function reprocesar(){
		//iniciar nuevo proceso
		$arrResultado = $this->objMaestroProceso->iniciar_reproceso();
			
		if($arrResultado["error"]==1){
			//Error
			$this->arrError["error"] = 1;
			$this->arrError["descripcion"] = $arrResultado["descripcion"];
		}else{
			$this->arrError["error"] = 0;
			$this->arrError["descripcion"] = $arrResultado["descripcion"];
			$this->idProceso = $arrResultado["data"]["id_proceso"];
		}
	}
	
	private function procesar_etapa_proceso(){
		$arrDataTrazaProce = $this->objMaestroProceso->iniciar_etapa_proceso();
		
		if($arrDataTrazaProce["error"]==0){
			
			$idEtapaProceso = $arrDataTrazaProce["data"]["id_etapa_proceso"];
			$etapaProceso = $arrDataTrazaProce["data"]["etapa_proceso"];
			$idTrazaProceso = $arrDataTrazaProce["data"]["id_traza_proceso"];
			
			$this->idTrazaProceso = $idTrazaProceso;
			
			$this->arrError["error"] = 0;
			$this->arrError["descripcion"] = "";
			
			$banderaProceTermi = false;
			
			//Procesar las etapas--------------------------------------------------------
			switch ($etapaProceso){
				case "inicio": //Se inicio el proceso
					$banderaProceTermi = true;
					break;
				case "subir_archivo": //Cargar los archivos planos al servidor desde el FTP
					
					$this->subir_archivo();
					//Verificar si el proceso se completo
					if($this->bandeEtapaProceTermi === true){
						$banderaProceTermi = true;
					}
					
					break;
				case "preparar_archivo": //Preparar los archivos planos
					
					$this->preparar_archivo($idEtapaProceso);
					//Verificar si el proceso se completo
					if($this->bandeEtapaProceTermi === true){
						$banderaProceTermi = true;
					}
					
					break;
				case "procesar_dato_sigas": //Procesar la informacion en la Base de datos de SIGAS
					
					$this->procesar_dato_sigas();
					//Verificar si el proceso se completo
					if($this->bandeEtapaProceTermi === true){
						$banderaProceTermi = true;
					}
					
					break;
				/*case "procesar_dato_service": //Procesar la informacion en la Base de datos externa
					
					$this->procesar_dato_service();
					//Verificar si el proceso se completo
					if($this->bandeEtapaProceTermi === true){
						$banderaProceTermi = true;
					}
					
					break;*/
				case "log_proceso": //Logs del proceso
					
					$this->log_proceso();
					//Verificar si el proceso se completo
					if($this->bandeEtapaProceTermi === true){
						$banderaProceTermi = true;
					}
					
					break;
				case "fin": //Se termino el proceso
					
					$banderaProceTermi = true;
					
					break;
			}
			
			//Finalizar etapa proceso exitoso
			if($banderaProceTermi === true){
				$procesado = $this->arrError["error"]==0 ? "S" : "E"; //[S]Fin exitoso - [E]Fin error  
				
				//Terminar la etapa proceso
				$arrResultado = $this->objMaestroProceso->terminar_etapa_proceso($idTrazaProceso,$procesado,$etapaProceso);
				
				if($arrResultado["error"]==0){
					
					$descripcion = 'La etapa proceso se termino correctamente';
					$this->arrError["error"] = 0;
					$this->arrError["descripcion"] = $descripcion;
					$this->arrError["data"]["etapa_proceso"] = $etapaProceso;
					
					$this->guardar_log_db($this->idTrazaProceso,'PROCESAR_ETAPA','A',$descripcion, null,'EXITO');
				}else{
					$this->arrError["error"] = 1;
					$this->arrError["descripcion"] = $arrDataTrazaProce["descripcion"];
					
					$this->guardar_log_db($this->idTrazaProceso,'PROCESAR_ETAPA','A',$arrDataTrazaProce["descripcion"],null,'ERROR');
				}
				
			}else if($this->arrError["error"]==0){
				$this->arrError["error"] = 0;
				$this->arrError["descripcion"] = "La etapa esta en proceso";
				$this->arrError["data"]["etapa_proceso"] = $etapaProceso;
			}
			
		}else{
			$this->arrError["error"] = 1;
			$this->arrError["descripcion"] = $arrDataTrazaProce["descripcion"];
			
			$this->guardar_log_db($this->idTrazaProceso,'PROCESAR_ETAPA','A',$arrDataTrazaProce["descripcion"],null,'ERROR');
		}
	}
	
	

	/**
	 * Carge Archivo Dir PHP (php) 
	 */
	private function subir_archivo(){
		
		$this->bandeEtapaProceTermi = false;
		
		$uriPlanoCargado = $this->rutaCargados . 'asopagos' . DIRECTORY_SEPARATOR .'saldos'. DIRECTORY_SEPARATOR .'cargados' . DIRECTORY_SEPARATOR;
		
		//Cargar archivos manualmente
		if($this->tipoCarge=="CARGE"){
			
			$resultadoCarge = $this->objMaestroProceso->carga_archivo_manual($uriPlanoCargado,"txt", array("txt","text","text/plain"),$this->arrFiles);
			
			//Verificar si los archivos se cargaron correctamente
			if($resultadoCarge==false){
				$descripcion = 'Error al cargar los archivos al servidor';
				$this->arrError["error"] = 0;
				$this->arrError["descripcion"] = $descripcion;
				$this->bandeEtapaProceTermi = true;
					
				$this->guardar_log_db($this->idTrazaProceso,'CARGAR_ARCHIVO','A',$descripcion, null,'ERROR');
			
			}else if(count($this->arrFiles)==0){
				//El proceso termino correctamente
				$descripcion = 'Los Archivos se cargaron correctamente al servidor';
				$this->arrError["error"] = 0;
				$this->arrError["descripcion"] = $descripcion;
				$this->bandeEtapaProceTermi = true;
					
				$this->guardar_log_db($this->idTrazaProceso,'CARGAR_ARCHIVO','A',$descripcion, null,'EXITO');
			}
			
		}		
		//Falta configurar el FTP		
			
	}
	
	private function preparar_archivo($idEtapaProceso){		
		$this->bandeEtapaProceTermi = false;
		
		$uriPlanoCargado = $this->rutaCargados .  'asopagos' . DIRECTORY_SEPARATOR .'saldos'. DIRECTORY_SEPARATOR . 'cargados' . DIRECTORY_SEPARATOR;
		$uriPlanoProcesado = $this->rutaCargados .  'asopagos' . DIRECTORY_SEPARATOR .'saldos'. DIRECTORY_SEPARATOR . 'procesados' . DIRECTORY_SEPARATOR;
		
		$arrArchivos = $this->objMaestroProceso->fetch_archivo_cargado($uriPlanoCargado);
		
		if(count($arrArchivos)==0){
			$descripcion = 'Error no se encontraron archivos para preparar';
			$this->arrError["error"] = 1;
			$this->arrError["descripcion"] = $descripcion;
				
			$this->guardar_log_db($this->idTrazaProceso,'ARCHIVO','A',$descripcion, null,'ERROR');
			return;
		}
		
		//$banderaSizeFile = 0;
		//$contArchivo = 0;
		$lengthArchivo = 0;
		$idArchivo = 0;
		$idArchivoProceso = 0;
		$lineaActual = 0;
		
		foreach ($arrArchivos as $archivo){
			
			$lengthArchivo = 0;
			$idArchivo = 0;
			$idArchivoProceso = 0;
			$lineaActual = 0;
			
			//Datos generales del archivo
			$nombreArchivo = $archivo;
			
			//Directorios
			$uriPlanoCargaActua = $uriPlanoCargado . $nombreArchivo;
			
			//Obtener las lineas del archivo
			$arrLineaArchivo = file($uriPlanoCargaActua);
			$lengthArchivo = count($arrLineaArchivo);
			
			$fechaArchivo = substr ( $nombreArchivo, 7, 8 );
			$mesArchivo = substr ( $fechaArchivo, 0, 2 );
			$diaArchivo = substr ( $fechaArchivo, 2, 2 );
			$annoArchivo = substr ( $fechaArchivo, 4, 4 );
			$fechaArchivo = $annoArchivo. $mesArchivo.$diaArchivo;
			
			$uriPlanoProceActua = $uriPlanoProcesado;
			
			//Verificar directorio
			$bandeVerifDirec = $this->objMaestroProceso->verifica_directorio( $uriPlanoProceActua );
			
			if ($bandeVerifDirec !== true) {
				//El directorio no fue posible crearlo
				$descripcion = 'Error el directorio para procesar los archivos no fue posible crearlo';
				$this->arrError["error"] = 1;
				$this->arrError["descripcion"] = $descripcion;
					
				$data = '{"nombre_archivo":"'.$nombreArchivo.'"}';
				$this->guardar_log_db($this->idTrazaProceso,'DIRECTORIO','C',$descripcion, $data,'ERROR');
				break;
			}
			
			//Verificar si el archivo ya esta guardado para el proceso
			$arrFiltro = array("id_proceso"=>$this->idProceso,"archivo"=>$nombreArchivo);
			$arrDataArchivoProceso = $this->objMaestroProceso->fetch_archivo_proceso($arrFiltro);
			if(count($arrDataArchivoProceso)>0){
				$idArchivo = $arrDataArchivoProceso[0]["id_archivo"];
				$idArchivoProceso = $arrDataArchivoProceso[0]["id_archivo_proceso"]; 
				$lineaActual = $arrDataArchivoProceso[0]["linea_actual"];
			}else{
				//Guardar el archivo y la relacion del archivo con el proceso
				
				//Guardar los datos del archivo
				$idArchivo = $this->objMaestroProceso->guardar_archivo($nombreArchivo, $this->tipoArchivo, $fechaArchivo, $lengthArchivo, date('h:i:s'));
				if($idArchivo==0){
					//El registro del archivo no se pudo guardar
					$descripcion = 'Error el registro del archivo no se pudo guardar';
					$this->arrError["error"] = 1;
					$this->arrError["descripcion"] = $descripcion;
				
					$data = '{"nombre_archivo":"'.$nombreArchivo.'"}';
					$this->guardar_log_db($this->idTrazaProceso,'ARCHIVO','C',$descripcion, $data,'ERROR');
					break;
				}
					
				//Guardar la relacion del archivo con el proceso
				$idArchivoProceso = $this->objMaestroProceso->guardar_archivo_proceso($idArchivo, $lengthArchivo);
				
				if($idArchivoProceso==0){
					//El registro del archivo no se pudo guardar
					$descripcion = 'Error La relacion del archivo con el proceso no se pudo guardar';
					$this->arrError["error"] = 1;
					$this->arrError["descripcion"] = $descripcion;
					
					$data = '{"nombre_archivo":"'.$nombreArchivo.'"}';
					$this->guardar_log_db($this->idTrazaProceso,'ARCHIVO','C',$descripcion, $data,'ERROR');
					break;
				}
			}
			
			//Recorrer las lineas del archivo
			for($i=$lineaActual, $bandeLineaActua = 1; $i<$lengthArchivo; $i++, $bandeLineaActua++){
				$lineaArchivo = $arrLineaArchivo[$i];
				$lineaActual = $i;
				
				$objPlanoSaldo = new PlanoSaldo();

				$objPlanoSaldo->setIdProceso( $this->idProceso );
				$objPlanoSaldo->setIdEtapaProceso( $idEtapaProceso );
				$objPlanoSaldo->setIdArchivo( $idArchivo );
				$objPlanoSaldo->setError( 'N' );
				$objPlanoSaldo->setEstado( 'PROCESADO' );
				$objPlanoSaldo->setUsuario( $this->usuario );
				
				$objPlanoSaldo->setBin( trim(substr( $lineaArchivo, 0, 9 )));
				$objPlanoSaldo->setNit( trim(substr( $lineaArchivo, 9, 15 )));
				$objPlanoSaldo->setNumeroTarjeta( trim(substr( $lineaArchivo, 24, 19 )));
				$objPlanoSaldo->setMiembro( trim(substr( $lineaArchivo, 43, 3 )));
				$objPlanoSaldo->setNombreTitular( utf8_encode(trim(substr( $lineaArchivo, 46, 27 ))));
				$objPlanoSaldo->setSaldoDisponible( trim(substr( $lineaArchivo, 73, 17 )));
				$objPlanoSaldo->setEstadoSaldo( trim(substr( $lineaArchivo, 90, 3 )));
				$objPlanoSaldo->setDescripcionEstado( utf8_encode(trim(substr( $lineaArchivo, 93, 30 ))));
				$objPlanoSaldo->setSubtipo( trim(substr( $lineaArchivo, 123, 3 )));
				$objPlanoSaldo->setCodigoCuenta( trim(substr( $lineaArchivo, 126, 2 )));
				$objPlanoSaldo->setFechaArchivo( trim($fechaArchivo));
				
				$objPlanoSaldo->setSaldoDisponibleDB( trim(substr( $lineaArchivo, 73, 15 )));
				
				$resultado = $objPlanoSaldo->guardar();
				
				if($resultado==0){
					
					//Guardar el log de error
					//No se fue posible preparar la linea del archivo
					$descripcion = 'Error no fue posible preparar la linea del archivo';
					$data = '{"nombre_archivo":"'.$nombreArchivo.'","numero_tarjeta":"'.$objPlanoSaldo->getNumeroTarjeta().'","fecha_archivo":"'.$objPlanoSaldo->getFechaArchivo().'","nit":"'.$objPlanoSaldo->getNit().'","nombre_titular":"'.$objPlanoSaldo->getNombreTitular().'","codigo_cuenta":"'.$objPlanoSaldo->getCodigoCuenta().'"}';
					$this->guardar_log_db($this->idTrazaProceso,'SALDO','B',$descripcion, $data,'ERROR');
				}
				
				//Actualizar la linea actual
				if($bandeLineaActua==1000 || ($lineaActual+1)==$lengthArchivo){
					$resultado = $this->objMaestroProceso->update_linea_actual_archivo_proceso($idArchivoProceso, ($lineaActual+1));
					
					if($resultado==0){
						//Error
						//El registro del archivo no se pudo guardar
						$descripcion = 'Error La linea actual de archivo proceso no se pudo actualizar';
						$this->arrError["error"] = 1;
						$this->arrError["descripcion"] = $descripcion;
						
						$data = '{"nombre_archivo":"'.$nombreArchivo.'"}';
						$this->guardar_log_db($this->idTrazaProceso,'ARCHIVO','C',$descripcion, $data,'ERROR');
					}
					break;		
				}
			}
			
			//Mover el archivo del directorio Cargados a Procesados
			if($lineaActual==$lengthArchivo){
				$this->objMaestroProceso->trasladar_archivo($uriPlanoCargaActua, $uriPlanoProceActua . $nombreArchivo);
			}
		}
		
		//Comprobar si existen archivos en el directorio cargado
		$banderaArchivo = count($this->objMaestroProceso->fetch_archivo_cargado($uriPlanoCargado));
		if($this->arrError["error"]==0 && $banderaArchivo==0){
			
			$descripcion = 'Los archivos se prepararon correctamente';
			$this->arrError["error"] = 0;
					
			$this->arrError["descripcion"] = $descripcion;
			$this->bandeEtapaProceTermi = true;
			
			$this->guardar_log_db($this->idTrazaProceso,'ARCHIVO','A',$descripcion, null,'EXITO');
		}
	}
	
	private function procesar_dato_sigas(){		
		$this->bandeEtapaProceTermi = false;
		
		//$this->bandeEtapaProceTermi = true;
		//return;
		
		$objPlanoSaldo = new PlanoSaldo();
		$objPlanoSaldo->setIdProceso($this->idProceso);
		$objPlanoSaldo->setUsuario($this->usuario);
		
		$estadoProceso = $objPlanoSaldo->procesar_datos();
		if($estadoProceso=='FIN_EXITOSO'){
			
			$descripcion = 'Los datos se procesaron correctamente en la DB';
			$this->arrError["error"] = 0;
			$this->arrError["descripcion"] = $descripcion;
			$this->bandeEtapaProceTermi = true;
			
			$this->guardar_log_db($this->idTrazaProceso,'DATOS_SIGAS','A',$descripcion, null,'EXITO');
			
		}else if($estadoProceso=='FIN_ERROR'){
			
			$descripcion = 'Existen algunas inconsistencias. Pero las etapa proceso termino correctamente';
			$this->arrError["error"] = 1;
			$this->arrError["descripcion"] = $descripcion;
			$this->bandeEtapaProceTermi = true;
				
			$this->guardar_log_db($this->idTrazaProceso,'DATOS_SIGAS','A',$descripcion, null,'EXITO');
			
		}else if($estadoProceso!='PROCESO'){
			//Error al ejecutar el proceso
			
			$descripcion = 'Error al procesar los datos en sigas';
			$this->arrError["error"] = 1;
			$this->arrError["descripcion"] = $descripcion;
			$this->guardar_log_db($this->idTrazaProceso,'PROCESAR_DATOS_SIGAS','A',$descripcion, null,'ERROR');
		}
	}
	
	private function log_proceso(){
		$this->bandeEtapaProceTermi = true;
		return;
	}
	
	private function guardar_log_db($idTrazaProceso, $codigo,$tipoLog,$descripcion, $data = null,$estado){
		$this->objLogProceso->guardar_log_db($idTrazaProceso,$codigo,$tipoLog,$descripcion,$data,$estado);
	}
	
	private function ejecutar_sp_resetear_proceso(){
		//Remover estado de error para las etapas con error

		$resultado = 0;
		$sentencia = self::$con->conexionID->prepare ( "EXEC [asopagos].[sp_Resetear_Proceso]
				@id_proceso = $this->idProceso
				, @tipo_proceso = '$this->tipoProceso'
				, @usuario = '$this->usuario'
				, @resultado = :resultado" );
		$sentencia->bindParam ( ":resultado", $resultado, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000 );
		$sentencia->execute ();
			
		if($resultado==0){			
			return false;
		}
		return true;
	}
}