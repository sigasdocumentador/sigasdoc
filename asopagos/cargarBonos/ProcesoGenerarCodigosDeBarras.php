<?php
ini_set('display_errors','0');
date_default_timezone_set('America/Bogota');
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

$url=$_SERVER['PHP_SELF'];
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'auditoria.php';
auditar($url);

include_once '../../rsc/pdo/IFXDbManejador.php';
include_once '../../rsc/pdo/IFXerror.php';

$db = IFXDbManejador::conectarDB(); 
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();	
}
    $valida=0;
    $sqlap110="select * from aportes100 where codigobarra is null";
    $rsap100=$db->querySimple($sqlap110);	
	$valida= $rsap100->fetchColumn ();
	
?>
<html>
<meta http-equiv="Pragma"content="no-cache">
<meta http-equiv="expires"content="0">
<head> 
<title>Proceso Generar Codigo de Barras</title>
<link type="text/css" href="../../css/css.1.8/jquery-ui-1.8.17.custom.css" rel="stylesheet" />
 <script language="javascript" src="../../js/ui/jquery-1.4.2.js"></script>
 <script language="javascript" src="js/bonosNuevos.js"></script>

</head>
<body>
<fieldset>
 <br />
 <p align="center" class="Rojo"><strong>Resultado de la Generaci&oacute;n de C&oacute;digo de Barras</strong></p>
 <br />
 
<?php
if($valida>0)
{
?>
<center>
    <div id="boton1">
    <p style="text-decoration:none; font-size:12px; color:#F00; font-weight:bold; cursor:pointer" onClick="procesarcodigobarras();">Procesar C&oacute;digo de Barras</p>
    </div>
</center>
<center>
    <div id="procesar" style="display:none">
    <p>Procesando...</p>
    </div>
</center>
<?php
}
else
{
	echo "<script language='JavaScript'>alert('No Existen Bonos Disponibles Para Asignar Codigos de Barras');</script>";
}
?>

<div id="ajax" align="center"> </div>
<div id="mensajes" align="center">..:..</div>
</fieldset>
</body>
</html>


