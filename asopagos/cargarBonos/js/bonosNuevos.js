/*
* @autor:      Ing. Orlando Puentes
* @fecha:      21/09/2010
* objetivo:
*/

function procesar(){
$("#ajax").html('<img src=../../imagenes/ajax-loader.gif />');
$("#boton1").html(' ');
document.getElementById("procesar").style.display="block";
$.ajax({
	url: 'procesarBonosNuevos.php',
	type: "POST",
	async:false,
	success: function(datos){
	$("#ajax").html('&nbsp;');
    $("#ajax").html(datos);	
	},
	complete: function(objeto, exito){	
	$("#ajax").html('');
	document.getElementById('boton1').innerHTML="";
	document.getElementById("procesar").style.display="none";
	$("#ajax").html('Proceso Finalizado');	
						
	}
});
		
}


function procesarcodigobarras(){
$("#ajax").html('<img src=../../imagenes/ajax-loader.gif />');
$("#boton1").html(' ');
document.getElementById("procesar").style.display="block";
$.ajax({
	url: 'GenerarCodigoDeBarras.php',
	type: "POST",
	async:false,
	success: function(datos){
	$("#ajax").html('&nbsp;');
    $("#ajax").html(datos);	
	},
	complete: function(objeto, exito){	
	$("#ajax").html('');	
	document.getElementById('boton1').innerHTML="";
	document.getElementById("procesar").style.display="none";
	$("#ajax").html('Proceso Finalizado');	
						
	}
});
		
}