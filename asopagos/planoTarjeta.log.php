<?php
	date_default_timezone_set('America/Bogota');
	set_time_limit(0);
	$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
	include_once  $root;
	
	$usuario=$_SESSION['USUARIO'];
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
	include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
	include_once $raiz.'/config.php';
	
	$db = IFXDbManejador::conectarDB();
	if($db->conexionID==null){
		$cadena = $db->error;
		echo msg_error($cadena);
		exit();
	}
	
	$caso = $_REQUEST["caso"]; 	
	$resultado = null;
	switch( $caso ){
		case "archivo_plano_saldo":
			$idProceso = $_REQUEST["id_proceso"]; 
			
			$sql = "SELECT 
					a164.id_saldo,
					a164.id_proceso,
					a164.id_etapa_proceso,
					a164.id_archivo,
					a164.estado,
					a164.error,
					a164.bin,
					a164.nit,
					a164.numero_tarjeta,
					a164.miembro,
					a164.nombre_titular,
					a164.saldo_disponible,
					a164.estado_saldo,
					a164.descripcion_estado,
					a164.subtipo,
					a164.codigo_cuenta,
					a164.fecha_archivo,
					a164.saldo_disponible_db,
					a149.archivo,
					a161.descripcion AS etapa_proceso,
					a164.usuario,
					a164.fecha_creacion,
					a164.fecha_modificacion
				FROM dbo.aportes164 a164
					INNER JOIN aportes161 a161 ON a161.id_etapa_proceso=a164.id_etapa_proceso
					INNER JOIN aportes149 a149 ON a149.idcontrol=a164.id_archivo
				WHERE a164.id_proceso=".$idProceso;
			$resultado = $db->querySimple($sql);			
		break;			
	}
	
	$path_plano =$ruta_generados.'asopagos'.DIRECTORY_SEPARATOR.'reporte'.DIRECTORY_SEPARATOR;
	if( !file_exists($path_plano) ){
		mkdir($path_plano, 0777, true);
	}
	$fecha=date('Ymd');
	$archivo='reporte_'.$caso."_".$fecha.'.csv';
	$path_plano.=DIRECTORY_SEPARATOR.$archivo;
	$cont = 0;
	$indices = 0;
	$cadena = null;
	$bandera = 'w';
	$fp=fopen($path_plano,'w');
	while ( ( $row = $resultado->fetch() ) == true ){	
		if($cont == 0){
			$indices = array_keys($row);
			$cadena = join(";", $indices)."\r\n";
			fwrite($fp, $cadena);
		}
		$row = array_map("utf8_decode", $row);
		$cadena = join(";", $row)."\r\n";
		fwrite($fp, $cadena);
		$cont++;
	}
	fclose($fp);
	
	$_SESSION['ENLACE']=$path_plano;
	$_SESSION['ARCHIVO']=$archivo;
	echo 1;
?>