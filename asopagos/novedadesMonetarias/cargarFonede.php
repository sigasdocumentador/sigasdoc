<?php
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR . 'config.php';
$fecha=date('Ymd');
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
$sql="select top 1 periodo from aportes012 where procesadofonede='N' order by periodo";
$rs = $db->querySimple($sql);
$row=$rs->fetch();
$periodo=$row['periodo'];
if(strlen($periodo)!=6){
	echo "El periodo parece estar errado!";
	exit();
}  
$sql="Select count(*) as cuenta from aportes104 where procesado = 'N' and periodogiro='$periodo' and tipopago='F'";
$rs=$db->querySimple($sql);
$w=$rs->fetch();
if($w['cuenta']==0){
echo 2;
exit();
}
$sql="SELECT COUNT(*) as contador FROM aportes149 WHERE fechasistema=CAST(GETDATE() AS DATE) AND tipoarchivo=2877";
$resultado = $db->querySimple($sql);// consecutivo diario
$temp=$resultado->fetch();

$consecutivo=$temp['contador']+1;

$ruta= $ruta_generados. DIRECTORY_SEPARATOR.'asopagos'.DIRECTORY_SEPARATOR.'novedades_monetarias'.DIRECTORY_SEPARATOR;
$c1='C';	// Carácter Fijo 
$c3='032';	// Código de la caja según Supersubsidio (Ej: 032 Huila).
$c4=date('ymd');	// fecha envio.
$c5=str_pad($temp['contador']+1,3,"0",STR_PAD_LEFT);
$temp=$c5;
$resultado->closeCursor();

$archivo=$c1.$c3.$c4.$c5.".txt";
$path =$ruta.$archivo;
$handle=fopen($path, "w");
fclose($handle);
$espacio=" ";
$sql = "SELECT valor,aportes015.idtipodocumento,aportes015.identificacion,aportes101.bono FROM aportes104 INNER JOIN aportes015 ON aportes104.idtrabajador=aportes015.idpersona INNER JOIN aportes101 ON aportes104.idtrabajador=aportes101.idpersona AND aportes101.estado='A'
WHERE periodogiro='$periodo' AND aportes104.procesado='N' AND aportes104.tipopago='F'";
$rs = $db->querySimple($sql);
$con=1;
$acumula=0;
while ($row = $rs->fetch()){
	$con++;
	$idtd=$row['idtipodocumento'];
	$numero=trim($row['identificacion']);
	$bono=trim($row['bono']);
	$valor=$row['valor']*100;
	$idRegistro=$row['idtraslado'];
	$acumula+=$valor;
	switch ($idtd){
		case 1 : $td=' 2'; break;
		case 2 : $td=' 4'; break;
		case 4 : $td=' 3'; break;
		default:$td=' 9';
	}
$c1="000000900319291";  //NIT Empresa
$c2=$fecha;				//Fecha envío
$c3=str_pad($consecutivo,5,'0',STR_PAD_LEFT);	//Consecutivo
$c4=$td;				//Tipo Identificación de Cliente
$c5=str_pad($numero,15,"0",STR_PAD_LEFT);	//Número Identificación de Cliente
$c6=str_pad($bono,19);	//Número de Tarjeta. Alineado a la izquierda. Blancos a la derecha
//$c7=str_pad($espacio,19);	//No. De cuenta. Alineado a la izquierda. Blancos a la derecha.
$c7=str_pad($bono,19);	//No. De cuenta. Alineado a la izquierda. Blancos a la derecha.
$c8='0';					//Nov.0=Ab/1=Desc
$c9=str_pad($valor,17,"0",STR_PAD_LEFT);		//Valor Novedad
$c10='01';					//Código de Bolsillo. Fijo 01
$c11=str_pad($espacio,26);	//Filler
$c12=str_pad($espacio,3);	//Se valida con la tabla de Códigos de Error del sistema de Medios de Pago,   
//$c12=str_pad(0,3,0);	//Se valida con la tabla de Códigos de Error del sistema de Medios de Pago,   

$cadena=$c1.$c2.$c3.$c4.$c5.$c6.$c7.$c8.$c9.$c10.$c11.$c12."\r\n";
	$handle=fopen($path, "a");
	fwrite($handle, $cadena);
	fclose($handle);
	
	$hora=date("H:i:s a");
	$sql="Update aportes104 set procesado='S', fechaproceso=CAST(GETDATE() AS DATE),horaproceso='$hora' where idtraslado=$idRegistro";
	$db->queryActualiza($sql);
}
$rs->closeCursor();
$invertido='';
for($i=strlen($acumula);$i>0;$i--){
	$invertido .= substr($acumula,$i-1,1);
}

$c1="000000900319291";  //NIT Empresa
$c2=$fecha;		//Fecha envío
$c3=str_pad($temp,5,"0",STR_PAD_LEFT);	//consecutivo
$c4=str_pad($con, 6, "0",STR_PAD_LEFT);			//num de registros
$c5=str_pad($espacio, 34);		//filler
$c6=str_pad($acumula,19,"0",STR_PAD_LEFT);			//Monto
$c7=str_pad($invertido,19,"0",STR_PAD_RIGHT);		//Monto Invertido
$c8=str_pad($espacio, 13);		//filler
$c9="636480";					//prefijo convenio
$c10="001";						//subtipo Default 001
$c11="T";	//str_pad('T',3,$espacio);	//trailer
$c12=str_pad($espacio, 3);		//codigo error

$cadena=$c1.$c2.$c3.$c4.$c5.$c6.$c7.$c8.$c9.$c10.$c11.$c12;
$handle=fopen($path, "a");
fwrite($handle, $cadena);
fclose($handle);
$hora1=date("H:i:s");
$sql="INSERT INTO aportes149(archivo,tipoarchivo,fechasistema,usuario,registros,hora) VALUES('$archivo',2877,CAST(GETDATE() AS DATE),'.$_SESSION[USUARIO].','$con','$hora1')";
$rs=$db->queryActualiza($sql);
if($rs==0){
	// no se hizo el inser
	echo 0;
	return;
}

$enlace=$ruta_generados. DIRECTORY_SEPARATOR.'asopagos'.DIRECTORY_SEPARATOR.'novedades_monetarias'.DIRECTORY_SEPARATOR.$archivo;
$_SESSION['ENLACE']=$enlace;
$_SESSION['ARCHIVO']=$archivo;
echo 1;
?>