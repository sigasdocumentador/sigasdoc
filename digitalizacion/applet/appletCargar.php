<?php
if ( !isset( $_SESSION ['USUARIO'] ) ) {
	session_start();
	$raiz=$_SESSION['RAIZ'];
	include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'session.php';
	include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'wssdk' . DIRECTORY_SEPARATOR . 'ClientWSSigas.php';
	include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';
	session();
	}


include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}

$ced=$_REQUEST['cedula'];
$tipodocumento=$_REQUEST['tipodocumento'];
$agencia=$_REQUEST['agencia'];
$carpetadocumento=$_REQUEST['carpetadocumento'];
if($carpetadocumento=="subsidio\\")
		{
			$carpetadocumento="";//si seleccionamos subcarpeta subsidio mandamos los archivos a la carpeta encima q es el num de cedula
		}
$rutadocs="";
$idgeneral="";
$nombre="";
$fechaNacimOafilia="";
$numero="";
$empresaInactiva=0;
if((int)$tipodocumento!=5)//diferente de NIT busca en aportes015
{
	$sql="select idpersona,rutadocumentos,pnombre,snombre,papellido,sapellido,fechanacimiento,identificacion from aportes015 where identificacion='$ced' and idtipodocumento='$tipodocumento'";
	$rs=$db->querySimple($sql);
	$ruta=$rs->fetch();
	if($ruta==false)
	{
		?>
		<script type="text/javascript">alert("No existe afiliado con ese numero o tipo de documento!!")</script>
		<?php
		exit();	
	}
	$nombre=$ruta['pnombre'].' '.$ruta['snombre'].' '.$ruta['papellido'].' '.$ruta['sapellido'];
	$rutadocs=$ruta['rutadocumentos'];
	$idgeneral=$ruta['idpersona'];
	$fechaNacimOafilia=$ruta['fechanacimiento'];
	$numero = trim($ruta ['identificacion']);
}
else
{
	$sql="select idempresa,rutadocumentos,razonsocial,fechaafiliacion,nit from aportes048 where nit='$ced'";
	$rs=$db->querySimple($sql);
	$ruta=$rs->fetch();
	if($ruta==false)
	{
		unset($ruta);
		$sql="select idempresa,rutadocumentos,razonsocial,fechaafiliacion,nit from aportes049 where nit='$ced'";
		$rs2=$db->querySimple($sql);
		$ruta=$rs2->fetch();
		if($ruta==false)
		{
			?>
			<script type="text/javascript">alert("No existe empresa con ese numero de NIT o documento!!")</script>
			<?php
			exit();	
		}
		$empresaInactiva=1;
		
	}
	$rutadocs=$ruta['rutadocumentos'];
	$idgeneral=$ruta['idempresa'];
	$nombre=$ruta['razonsocial'];
	$fechaNacimOafilia=$ruta['fechaafiliacion'];
	$numero = trim($ruta ['nit']);
}
?>
<HTML>
<HEAD>
</HEAD>
<BODY>
<P>
<?php
$sWebSigas= new ClientWSSigas(USUARIO_WEB_SIGAS,CONTRASENA_WEB_SIGAS);
$cadena="";
if($rutadocs=='')
{
	if((int)$tipodocumento!=5&&($fechaNacimOafilia!=""&&$fechaNacimOafilia!=null))
	{
		if(strlen($fechaNacimOafilia)<10){
			$fechaNacimOafilia='00-00-0000';
		}
		
		$f = explode ( "-", $fechaNacimOafilia );
		$a = $f [0] . "\\";
		$m = $f [1] . "\\";
		$d = $f [2] . "\\";
		$cadena="digitalizacion\afiliados\\".$a.$m.$d.$numero."\\";
		if($sWebSigas->rutaDigitalizacionExiste($cadena))
		{
			$sql="update aportes015 set rutadocumentos='$cadena' where idpersona='$idgeneral'";
			$db->queryActualiza($sql);
		}else{
			if(!$sWebSigas->rutaDigitalizacionCrear($cadena))
			{?>
			<script type="text/javascript">alert("Fallo al crear la ruta de documentos de la persona, intente digitalizar de nuevo.")</script>
			<?php 	
			}else{
				$sql="update aportes015 set rutadocumentos='$cadena' where idpersona='$idgeneral'";
				$db->queryActualiza($sql);
			}
		}
		
	}else {
	
		if((int)$tipodocumento==5&&($fechaNacimOafilia!=""&&$fechaNacimOafilia!=null))
		{
			if(strlen($fechaNacimOafilia)<10){
				$fechaNacimOafilia='00-00-0000';
			}
			
			$f = explode ( "-", $fechaNacimOafilia );
			$a = $f [0] . "\\";
			$m = $f [1] . "\\";
			$d = $f [2] . "\\";
			$cadena="digitalizacion\empresa\\".$a.$m.$d.$numero."\\";
			if($sWebSigas->rutaDigitalizacionExiste($cadena))
			{
				if($empresaInactiva==0)
				{
				$sql="update aportes048 set rutadocumentos='$cadena' where idempresa='$idgeneral'";
				$db->queryActualiza($sql);
				}
				else{
					$sql="update aportes049 set rutadocumentos='$cadena' where idempresa='$idgeneral'";
					$db->queryActualiza($sql);
				}
			}else{
				if(!$sWebSigas->rutaDigitalizacionCrear($cadena))
						{?>
						<script type="text/javascript">alert("Fallo al crear la ruta de documentos de la empresa, intente digitalizar de nuevo.")</script>
						<?php 	
						}else{
							if($empresaInactiva==0)
							{
							$sql="update aportes048 set rutadocumentos='$cadena' where idempresa='$idgeneral'";
							$db->queryActualiza($sql);
							}
							else{
								$sql="update aportes049 set rutadocumentos='$cadena' where idempresa='$idgeneral'";
								$db->queryActualiza($sql);
							}
						}
					}
		}else {?>
			<script type="text/javascript">alert("No es posible crear carpeta para documentos. No hay suficiente informacion 22.");</script>	
		<?php
		die();
		}
		$rutadocs = $cadena;
	}
}
else
{
	if((int)$tipodocumento!=5&&($fechaNacimOafilia!=""&&$fechaNacimOafilia!=null))
	{
		if(strlen($fechaNacimOafilia)<10){
			$fechaNacimOafilia='00-00-0000';
		}
	
		$f = explode ( "-", $fechaNacimOafilia );
		$a = $f [0] . "\\";
		$m = $f [1] . "\\";
		$d = $f [2] . "\\";
		$cadena="digitalizacion\afiliados\\".$a.$m.$d.$numero."\\";
		if($sWebSigas->rutaDigitalizacionExiste($cadena))
		{
			$sql="update aportes015 set rutadocumentos='$cadena' where idpersona='$idgeneral'";
			$db->queryActualiza($sql);
		}else{
			if(!$sWebSigas->rutaDigitalizacionCrear($cadena))
				{?>
				<script type="text/javascript">alert("Fallo al crear la ruta de documentos de la persona, intente digitalizar de nuevo.")</script>
				<?php 	
				}else{
					$sql="update aportes015 set rutadocumentos='$cadena' where idpersona='$idgeneral'";
					$db->queryActualiza($sql);
				}
			}
			
		}else {
		
			if((int)$tipodocumento==5&&($fechaNacimOafilia!=""&&$fechaNacimOafilia!=null))
			{
				if(strlen($fechaNacimOafilia)<10){
					$fechaNacimOafilia='00-00-0000';
				}
				
				$f = explode ( "-", $fechaNacimOafilia );
				$a = $f [0] . "\\";
				$m = $f [1] . "\\";
				$d = $f [2] . "\\";
				$cadena="digitalizacion\empresa\\".$a.$m.$d.$numero."\\";
				if($sWebSigas->rutaDigitalizacionExiste($cadena))
				{
					if($empresaInactiva==0)
					{
					$sql="update aportes048 set rutadocumentos='$cadena' where idempresa='$idgeneral'";
					$db->queryActualiza($sql);
					}
					else{
						$sql="update aportes049 set rutadocumentos='$cadena' where idempresa='$idgeneral'";
						$db->queryActualiza($sql);
					}
				}else{
					if(!$sWebSigas->rutaDigitalizacionCrear($cadena))
						{?>
						<script type="text/javascript">alert("Fallo al crear la ruta de documentos de la empresa, intente digitalizar de nuevo.")</script>
						<?php 	
						}else{
							if($empresaInactiva==0)
							{
							$sql="update aportes048 set rutadocumentos='$cadena' where idempresa='$idgeneral'";
							$db->queryActualiza($sql);
							}
							else{
								$sql="update aportes049 set rutadocumentos='$cadena' where idempresa='$idgeneral'";
								$db->queryActualiza($sql);
							}
						}
					}
			}else {?>
				<script type="text/javascript">alert("No es posible crear carpeta para documentos. No hay suficiente informacion 33.");</script>	
			<?php
			die();
			}
			$rutadocs = $cadena;
		}
}
	$rutadocs = $cadena;
	$servImagenes='\\\\10.10.1.55\\htdocs';
	$servRegistro='http://10.10.1.57';
	$tamanoPaquete='8000';
	$usuarioD=base64_encode(USUARIO_WEB_SIGAS);
	$contrasenaD=base64_encode(CONTRASENA_WEB_SIGAS);
	$_SESSION["ID_AGENCIA_DIGITALIZACION"] = $agencia;
?>
<iframe name="frameoculto" align="right" height="1" width="1" src=""></iframe>
<APPLET id="idapplet" code="AppletDigitalizacion.class" codebase="applet/classes" archive="AppletDigitalizacion.jar, JTwain.jar,JTwainLicense.class" width=1050 height=1200>
<PARAM NAME="ruta1" VALUE="<?php echo $rutadocs.$carpetadocumento?>">
<PARAM NAME="idpersona1" VALUE="<?php echo $idgeneral?>">
<PARAM NAME="identificacion1" VALUE="<?php echo $ced?>">
<PARAM NAME="tipoDocumento" VALUE="<?php echo $tipodocumento?>">
<PARAM NAME="agencia" VALUE="<?php echo $agencia?>">
<PARAM NAME="nombre" VALUE="<?php echo $nombre?>">
<PARAM NAME="servImagenes" VALUE="<?php echo $servImagenes; ?>">
<PARAM NAME="servRegistro" VALUE="<?php echo $servRegistro; ?>">
<PARAM NAME="tamanoPaquete" VALUE="<?php echo $tamanoPaquete; ?>">
<PARAM NAME="usuarioD" VALUE="<?php echo $usuarioD; ?>">
<PARAM NAME="contrasenaD" VALUE="<?php echo $contrasenaD; ?>">
</APPLET>
</P>
</BODY>
</HTML>