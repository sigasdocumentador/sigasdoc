
<?php
ini_set('display_errors','1');
include_once '../rsc/pdo/IFXDbManejador.php';
include_once '../rsc/pdo/IFXerror.php';

$db = IFXDbManejador::conectarDB(); 
if($db->conexionID==null){
	$cadena = $db->error;
	echo msg_error($cadena);
	exit();
}
?>

<?php
$rs_tipodocumento=$db->Definiciones(1, 3);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>TRASLADAR RUTA DE DOCUMENTOS</title>

<link type="text/css" href="../css/css.1.8/jquery-ui-1.8.17.custom.css" rel="stylesheet" />
<link type="text/css" href="../css/Estilos.css" rel="stylesheet"/>
<link type="text/css" rel="stylesheet" href="../css/marco.css" />


<script language="javascript" src="../newjs/1.6/jquery-1.6.2.min.js"></script>
<script language="javascript" src="../newjs/1.6/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="../js/comunes.js"></script>
<script languaje="javascript" src="js/MigrarRutaDocumentos.js"></script>


<script>
$(function() {
	$('#txtFechaN').datepicker({
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		maxDate: "+0D",
	});
	var anoActual = new Date();
	var strYearRange = anoActual.getFullYear()-120 +":"+ anoActual.getFullYear();
	$("#txtFechaN").datepicker( "option", "yearRange", strYearRange );
});	

$(function(){
	$("#div-datosAuditoria").dialog({
		autoOpen:false,
		width:250,
		show: "drop",
		hide: "clip",	
		open:function(event,ui){
		},//fin funcion open
		close: function(event,ui){
			$("#div-datosCertificados table tbody").empty();
		}
	});
	});
 </script>

</head>

<body>


<form id="form1" name="form1" method="post" action="">
<table border="0" cellspacing="0" cellpadding="0" align="center"> 
<tr>
<td width="13" height="29" class="arriba_iz">&nbsp;</td>
<td class="arriba_ce"><span class="letrablanca">::&nbsp;TRASLADAR RUTA DE DOCUMENTOS::</span></td>
<td width="13" class="arriba_de" >&nbsp;</td>
</tr>
<tr>     
<td class="cuerpo_iz">&nbsp;</td>
<td class="cuerpo_ce"><table width="285" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="23" style="border:0"><img src="../imagenes/menu/refrescar.png" width="16" height="16" style="cursor: pointer" title="Refrescar" onclick="refrescar();" /></td>
    <td width="262" height="0" style="border:0"><img id="guardar" src="../imagenes/menu/trasladar.png" width="16" height="16" style=" display:block;cursor: pointer" title="Trasladar" onclick="TrasladaRrutaDocumentos();" /></td>
    </tr>
</table>  <div id="error" style="color:#FF0000"></div>
</td>
<td class="cuerpo_de">&nbsp;</td>
</tr>
<tr>
<td class="cuerpo_iz">&nbsp;</td>
<!-- TABLAS DE FORMULARIO -->
<td class="cuerpo_ce">
<center>

     <table border="0" cellspacing="0" class="tablero">
            <tr bgcolor="#EBEBEB">
              <td colspan="4" style="text-align:center" ></td>
            </tr>
                       <tr>                   
              <td width="133">Tipo de identificaci&oacute;n:</td>
              <td width="279"><select name="tipodocumento" id="tipodocumento" class="box" value="<?php echo $idtipodocumento;?>">
                <option value="">::Seleccione::</option>
				   <?php
					   while($row=$rs_tipodocumento->fetch()){
						   if($row['iddetalledef']==1)
						   {
								echo "<option selected=selected value=".$row['iddetalledef']." >".$row['detalledefinicion']."</option>";
						   }
						   else
						   if($row['iddetalledef'] !=6 && $row['iddetalledef'] !=7 && $row['iddetalledef'] !=8 && $row['iddetalledef'] !=4246 && $row['iddetalledef'] !=4277)
						   {
								echo "<option value=".$row['iddetalledef']." >".$row['detalledefinicion']."</option>";
						   }
										  
						   }// fin while
         			 ?>
              </select>
                <span class="abajo_ce"><img src="../imagenes/menu/obligado.png" align="middle"/></span></td>
              <td width="154">N&uacute;mero:</td>
              <td width="294"><input name="numeroidentificacion" type="text" id="numeroidentificacion" onkeydown='solonumeros(this);' onkeyup='solonumeros(this);' onchange="buscarpersona();""/>
                <span class="abajo_ce"><img src="../imagenes/menu/obligado.png" align="middle"/></span></td>
            </tr>
            <tr>
                    
              <td width="133">Fecha Destino:</td>
              <td width="279"><input name="fechaactual" type="text" id="fechaactual" readonly="readonly" "/></td>
              <td width="154">Ruta Destino:</td>
              <td width="294"><input name="rutaactual" type="text" id="rutaactual" size="55" readonly="readonly" "/></td>
            </tr>
            <tr>
              <td>Fecha Origen:</td>
              <td><input name="txtFechaN" type="text" id="txtFechaN" validarutadocumentos" onchange="validarutaambos('txtFechaN','numeroidentificacion','rutaorigen');"/>
              <span class="abajo_ce"><img src="../imagenes/menu/obligado.png" align="middle"/><a href="#"><img src="../imagenes/menu/visitas.png" align="baseline" title="Auditoria" onclick="BuscarDatosAuditoria();"/></a></span></td>
              <td>Ruta Origen:</td>
              <td><input name="rutaorigen" type="text" id="rutaorigen" size="55" readonly="readonly""/></td>
            </tr>
          </table>
</center>

<div id="div-datosAuditoria" title="::DATOS AUDITORIA::" style="display:none">
	<table width="100%" class="tablero" >
		<thead>
			<tr>
				<th>Fecha Nacimiento</th>				
			</tr>
		</thead>
		<tbody></tbody>
	</table>
</div>

<td class="cuerpo_de"></td><!-- FONDO DERECHA -->
<tr>
<td height="41" class="abajo_iz">&nbsp;</td>
<td class="abajo_ce">&nbsp;<font color="#FF0000">Campos Obligatorios</font><img src="../imagenes/menu/obligado.png" align="middle"/></td>
<td class="abajo_de">&nbsp;</td>
</tr>
</table>
</form>
</body>
</html>