var URL=src();

function nombreCorto(){
	var pn="";
	var sn="";
	var pa="";
	var sa="";
	var nu=0;
	var i;
	identificacionTemp="";
	fechaNacimientoTemp="";
	var patron=new RegExp('[\u00F1|\u00D1]');
	$("#txtPNombre").blur(function(){
		pn=$("#txtPNombre").val();
		sn=$("#txtSNombre").val();
		pa=$("#txtPApellido").val();
		sa=$("#txtSApellido").val();
		/*
		//---Reemplazar �
		for(i=1;i<=pn.length;i++){
			pn=pn.replace(patron,"�");
		}
		//-------------
		*/
		txt=pn+" "+sn+" "+pa+" "+sa;
		txt=txt.toUpperCase();
		$("#txtNombreC").val(txt);
		num=parseInt(txt.length);
		$("#txtNombreC").next("span").html(num);
		$("#txtNombreC").trigger("change");
	});
	
	$("#txtSNombre").blur(function(){
		pn=$("#txtPNombre").val();
		sn=$("#txtSNombre").val();
		pa=$("#txtPApellido").val();
		sa=$("#txtSApellido").val();
		/*		
		//---Reemplazar �
		for(i=1;i<=sn.length;i++){
		sn=sn.replace(patron,"�");
		}
		//-------------
		*/
		txt=pn+" "+sn+" "+pa+" "+sa;
		txt=txt.toUpperCase();
		$("#txtNombreC").val(txt);
		num=parseInt(txt.length);
		$("#txtNombreC").next("span").html(num);
		$("#txtNombreC").trigger("change");
	});
	$("#txtPApellido").blur(function(){
		pn=$("#txtPNombre").val();
		sn=$("#txtSNombre").val();
		pa=$("#txtPApellido").val();
		sa=$("#txtSApellido").val();
		/*
		//---Reemplazar �
		for(i=1;i<=pa.length;i++){
		pa=pa.replace(patron,"�");
		}
		//-------------
		*/
		txt=pn+" "+sn+" "+pa+" "+sa;
		txt=txt.toUpperCase();
		$("#txtNombreC").val(txt);
		num=parseInt(txt.length);
		$("#txtNombreC").next("span").html(num);
		$("#txtNombreC").trigger("change");
	});
	$("#txtSApellido").blur(function(){
		pn=$("#txtPNombre").val();
		sn=$("#txtSNombre").val();
		pa=$("#txtPApellido").val();
		sa=$("#txtSApellido").val();
		/*
		//---Reemplazar �
		for(i=1;i<=sa.length;i++){
		sa=sa.replace(patron,"�");
		}
		//-------------
		*/
		txt=pn+" "+sn+" "+pa+" "+sa;
		txt=txt.toUpperCase();
		$("#txtNombreC").val(txt);
		num=parseInt(txt.length);
		$("#txtNombreC").next("span").html(num);
		$("#txtNombreC").trigger("change");
	});
	
	$("#txtNombreC").change(function(){
		if(num>30){
			if(sn.length>0){
				sn=sn.slice(0,1);
				txt=pn+" "+sn+" "+pa+" "+sa;
				txt=txt.toUpperCase();
				$("#txtNombreC").val(txt);
				num=parseInt(txt.length);
				$("#txtNombreC").next("span").html(num);
				}
			if(num>30)
			if(sa.length>0){
				sa=sa.slice(0,1);
				txt=pn+" "+sn+" "+pa+" "+sa;
				txt=txt.toUpperCase();
				$("#txtNombreC").val(txt);
				num=parseInt(txt.length);
				$("#txtNombreC").next("span").html(num);
				}
			}
		});
}

function llenarCampos(idp){
	$.ajax({
		url: URL+"phpComunes/llenarCampos.php",
		async: false,
		data: {v0:idp},		
		beforeSend: function(objeto){
        	dialogLoading('show');
        },        
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }            
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("En llenarCampos Paso lo siguiente: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
		success: function(data){
			$.each(data,function(i,fila){
				$("#cboDepto").val($.trim(fila.iddepresidencia)).trigger('change');
				$("#cboCiudad").val($.trim(fila.idciuresidencia)).trigger("change");
				$("#cboZona").val($.trim(fila.idzona)).trigger("change");
				$("#cboBarrio").val($.trim(fila.idbarrio));
				$("#cboPais2").val($.trim(fila.idpais)).trigger('change');												
				$("#cboDepto2").val($.trim(fila.iddepnace)).trigger('change');						    
				$("#cboCiudad2").val($.trim(fila.idciunace));					
				
				$("#txtId").val(idp);
				$("#txtTipoD").val(fila.idtipodocumento);
				$("#txtNumero").val(fila.identificacion);
				$("#lblIdPersona").html(idp);
				identificacionTemp=$.trim(fila.identificacion);
				$("#txtPNombre").val($.trim(fila.pnombre));
				$("#txtSNombre").val($.trim(fila.snombre));
				$("#txtPApellido").val($.trim(fila.papellido)); 
				$("#txtSApellido").val($.trim(fila.sapellido));
				$("#txtSexo,#txtHiddenSexo,#txtHiddenSexoR").val(fila.sexo);			
				$("#txtDireccion").val($.trim(fila.direccion));
				$("#txtTelefono").val($.trim(fila.telefono));
				$("#txtCelular").val($.trim(fila.celular));
				$("#txtEmail").val($.trim(fila.email));
				$("#txtCasa").val(fila.idpropiedadvivienda);
				$("#txtTipoV").val(fila.idtipovivienda);
				$("#txtEstado2").val(fila.idestadocivil);
				$("#txtFechaN").val(fila.fechanacimiento);
				fechaNacimientoTemp=$.trim(fila.fechanacimiento);
				$("#txtCapacidad").val(fila.capacidadtrabajo);
				$("#txtNombreC").val($.trim(fila.nombrecorto));
				$("#profesion").val($.trim(fila.idprofesion));
				$("#rutaDocP").val($.trim(fila.rutadocumentos));				
			});
		},		
        type: "GET"
	});
}

function limpiarCampos_otro(){
	$('#txtId').val('');
	$("#txtId2").val('');
	$("#txtTipoD").val('');
	$("#txtNumero").val('');//no obligado
	$("#txtPNombre").val('');//no obligado
	$("#txtSNombre").val('');
	$("#txtPApellido").val('');
	$("#txtSApellido").val('');
	$("#txtSexo").val('');
	$("#txtEstado2").val('');
	$("#txtTelefono").val('');
	$("#txtCelular").val('');//$("#deptRes").val();
	$("#txtEmail").val('');//$("#ciudadRes").val();
	$("#cboDepto").val(0);//$("#zonaRes").val();
	$("#cboCiudad").val(0);
	$("#cboZona").val(0);;
	$("#cboBarrio").val(0);
	$("#txtDireccion").val('');
	$("#txtTipoV").val('');
    $('#txtCasa').val('');
    $('#txtFechaN').val('');
	$("#cboDepto2").val(0);	//rescato elv alor de la fecha del cmapo oculto
	$("#cboCiudad2").val(0);
	$("#txtCapacidad").val('');
	$("#profesion").val('');
	$("#txtNombreC").val('');
	$("#rutaDocP").val('');
}

function actualizar(idp){
	var v0=idp;
	var v1= $("#txtTipoD").val();			//idtipodocumento
	var v2= $("#txtNumero").val();			//identificacion
	var v3=	$("#txtPApellido").val(); 		//papellido
	var v4=	$("#txtSApellido").val();		//sapellido
	var v5=	$("#txtPNombre").val();			//pnombre
	var v6=	$("#txtSNombre").val();			//snombre
	var v7=	$("#txtSexo").val();			//sexo
	var v8=	$("#txtDireccion").val();		//direccion
	var v9=	$("#cboBarrio").val();			//idbarrio
	var v10= $("#txtTelefono").val();		//telefono
	var v11= $("#txtCelular").val();		//celular
	var v12= $("#txtEmail").val();			//email
	var v13= $("#txtCasa").val();			//idpropiedadvivienda
	var v14= $("#txtTipoV").val();			//idtipovivienda
	var v15= $("#cboDepto").val();			//iddepresidencia
	var v16= $("#cboCiudad").val();		    //idciuresidencia
	var v17= $("#cboZona").val();			//idzona
	var v18= $("#txtEstado2").val();		//idestadocivil
	
	/*var elem = ($("#txtFechaN").val()).split('/');
	mes = elem[0];
	dia = elem[1];
	anio = elem[2];
	var v19= anio+"-"+mes+"-"+dia;			//fechanacimiento*/
	
	var v19= $("#txtFechaN").val();			//fechanacimiento
	var v26= $("#cboPais2").val();			//idpais Nace
	var v20= $("#cboDepto2").val();			//iddepnace
	var v21= $("#cboCiudad2").val();		//idciunace
	var v22= $("#txtCapacidad").val();		//capacidadtrabajo
	var v23= $("#txtNombreC").val();		//nombrecorto
	var v24= $("#profesion").val();
	var v25= $("#rutaDocP").val();
	
	var error=0;
	$("table.tablero :input.ui-state-error,select.ui-state-error").removeClass("ui-state-error");//Limpiar la clase de error
	
	if(v1==0){
		$("#txtTipoD").addClass("ui-state-error");
		error++;
		
	}
	if(v2.length==0){
		$("#txtNumero").addClass("ui-state-error");
		error++;
	}
	if(v3.length==0){
		$("#txtPApellido").addClass("ui-state-error");
		error++;
	}
	if(v5.length==0){
		$("#txtPNombre").addClass("ui-state-error");
		error++;
	}
	if(v7==0){
		$("#txtSexo").addClass("ui-state-error");
		error++;
	}
	if(v8==0){
		$("#txtDireccion").addClass("ui-state-error");
		error++;
	}
	if(v10==0){
		$("#txtTelefono").addClass("ui-state-error");
		error++;
	}
	/*if(v11==0){
		$("#txtCelular").addClass("ui-state-error");
		error++;
	}*/
	if(v13==0){
		$("#txtCasa").addClass("ui-state-error");
		error++;
	}
	if(v14==0){
		$("#txtTipoV").addClass("ui-state-error");
		error++;
	}
	if(v15==0){
		$("#cboDepto").addClass("ui-state-error");
		error++;
	}
	if(v16==0){
		$("#cboCiudad").addClass("ui-state-error");
		error++;
	}
	if(v17==0){
		$("#cboZona").addClass("ui-state-error");
		error++;
	}
	if(v18==0){
		$("#txtEstado2").addClass("ui-state-error");
		error++;
	}
	if(v19==0){
		$("#txtFechaN").addClass("ui-state-error");
		error++;
	}
	if(v20==0){
		$("#cboDepto2").addClass("ui-state-error");
		error++;
	}
	if(v21==0){
		$("#cboCiudad2").addClass("ui-state-error");
		error++;
	}
	if(v24==0){
		$("#profesion").addClass("ui-state-error");
		error++;
	}
	if(v26==0){
		$("#cboPais2").addClass("ui-state-error");
		error++;
	}


	
	/*
	if(v8.length==0){
		$("#txtDireccion").addClass("ui-state-error");
		error++;
		}
	totalCombo=$("#cboBarrio option").length;
	if(totalCombo>1){
		// Para las veredas, fincas y kil�metros no se requiere barrio
		patronBarrio = new RegExp(/(vereda|finca|km)/i);
		if(!patronBarrio.test($("#txtDireccion").val())){
			if(v9==0){
				$("#cboBarrio").addClass("ui-state-error");
				error++;
			}
		}
		
	}
	if(v14==0){
		$("#txtTipoV").addClass("ui-state-error");
		error++;
		}
	if(v15==0){
		$("#cboDepto").addClass("ui-state-error");
		error++;
		}
	if(v16==0){
		$("#cboCiudad").addClass("ui-state-error");
		error++;
		}
	if(v17==0){
		$("#cboZona").addClass("ui-state-error");
		error++;
		}
	if(v19.length==0){
		$("#txtFechaN").addClass("ui-state-error");
		error++;
		}
	if(v20==0){
		$("#cboDepto2").addClass("ui-state-error");
		error++;
		}
	if(v21==0){
		$("#cboCiudad2").addClass("ui-state-error");
		error++;
		}
	if(v10 == 0){
		$("#txtTelefono").addClass("ui-state-error");
		error++;
	}
	*/
	if(error>0){
		MENSAJE("Llene los campos obligados!");
		return false; 
	}else{
		$.post(URL +"phpComunes/verifIdentificacionDisponible.php",{idTipo: v1, numero: v2, idPersona: v0},function(respuesta){
			if(respuesta == "EXISTE"){
				alert("Verifique la informaci\xf3n, ya existe una persona con ese tipo y numero de documento ("+ $("#txtTipoD :selected").text() +" - "+ v2 +").");
				return false;
			}// si no existe otra persona con el mismo tipo y n�mero de documento, entonces se puede continuar
			/*			
			if(identificacionTemp!=v2||fechaNacimientoTemp!=v19){
				$.post(URL+'phpComunes/crearRutaDocumentos.php',{identificacion:v2,tipodocumento:0,fecha:v19},function(data){
					if(data!=""&&v25!=""){
						$.post(URL+'phpComunes/migrarDocumentosWS.php',{rutaOrigen:v25,rutaDestino:data},function(data){
							if(data==0){
								alert("No se pudo generar la ruta de Documentos.");
								return false; 
							}
						});
					}
					if(data!="")
						v25=data;
				});
			}
			*/
			/** ----------------------------------------------- **/
			/** AUDITORIA PARA VERIFICAR LOS CAMPOS ACTUALIZADOS **/
			/** ----------------------------------------------- **/
			//AUDITORIA				
			var campos = "";				
			$.ajax({
				url: URL+"phpComunes/llenarCampos.php",
				async: false,
				data: {v0:idp},		
				beforeSend: function(objeto){
		        	dialogLoading('show');
		        },        
		        complete: function(objeto, exito){
		        	dialogLoading('close');
		            if(exito != "success"){
		                alert("No se completo el proceso!");
		            }            
		        },
		        contentType: "application/x-www-form-urlencoded",
		        dataType: "json",
		        error: function(objeto, quepaso, otroobj){
		            alert("En llenarCampos Paso lo siguiente: "+quepaso);
		        },
		        global: true,
		        ifModified: false,
		        processData:true,
				success: function(data){
					$.each(data,function(i,fila){
					campos +='Campos(';
						//alert($("#txtEmail").val());
						//alert("Base de datos"+" "+fila.email);
						if($.trim($("#txtTipoD").val())!=$.trim(fila.idtipodocumento))
						{
						  campos += " TipoDocumento ";
						}
						if($.trim($("#txtNumero").val())!=$.trim(fila.identificacion))
						{							 
						  campos += " identificacion ";
						}
						if($.trim($("#txtPNombre").val())!=$.trim(fila.pnombre)) 
						{
						  campos += " pnombre ";
						}
						if($.trim($("#txtSNombre").val())!=$.trim(fila.snombre))
						{
						  campos += " snombre ";
						}
						if($.trim($("#txtPApellido").val())!=$.trim(fila.papellido)) 
						{
						  campos += " papellido ";
						}
						if($.trim($("#txtSApellido").val())!=$.trim(fila.sapellido)) 
						{
						  campos += " sapellido ";
						}
						if($.trim($("#txtSexo").val())!=$.trim(fila.sexo)) 
						{
						  campos += " sexo ";
						}
						if($.trim($("#txtEstado2").val())!=$.trim(fila.idestadocivil))
						{							
						 	campos += " estadocivil ";
						}
						if($.trim($("#txtTelefono").val())!=$.trim(fila.telefono)) 
						{							
						 	campos += " telefono ";
						}
						if($.trim($("#txtCelular").val())!=$.trim(fila.celular)) 
						{							
						 	campos += " celular ";
						}
						if($.trim($("#txtEmail").val())!=$.trim(fila.email)) 
						{							
						 	campos += " email ";
						}
						if($("#cboDepto").val()!=$.trim(fila.iddepresidencia)) 
						{
							campos += " depresidencia ";
						}
						if($("#cboCiudad").val()!=$.trim(fila.idciuresidencia)) 
						{
							campos += " ciuresidencia ";
						}
						if($("#cboZona").val()!=$.trim(fila.idzona)) 
						{
							campos += " Zona ";
						}
						if($("#cboBarrio").val()!=$.trim(fila.idbarrio)) 
						{
							campos += " Barrio ";
						}
						if($.trim($("#txtDireccion").val())!=$.trim(fila.direccion))
						{							
						 	campos += " direccion ";
						}
						if($.trim($("#txtTipoV").val())!=$.trim(fila.idtipovivienda)) 
						{							
						 	campos += " tipovivienda ";
						}
						if($.trim($("#txtCasa").val())!=$.trim(fila.idpropiedadvivienda)) 
						{							
						 	campos += " propiedadvivienda ";
						}
						if($.trim($("#txtFechaN").val())!=$.trim(fila.fechanacimiento)) 
						{							
						 	campos += " fechanacimiento ";
						}
						if($("#cboPais2").val()!=$.trim(fila.idpais)) 
						{
							campos += " pais ";
						}
						if($("#cboDepto2").val()!=$.trim(fila.iddepnace)) 
						{
							campos += " depnace ";
						}
						if($("#cboCiudad2").val()!=$.trim(fila.idciunace)) 
						{
							campos += " ciunace ";
						}
						if($.trim($("#txtCapacidad").val())!=$.trim(fila.capacidadtrabajo))
						{							
						 	campos += " capacidadtrabajo ";
						}
						if($.trim($("#profesion").val())!=$.trim(fila.idprofesion)) 
						{							
						 	campos += " profesion ";
						}
						if($.trim($("#txtNombreC").val())!=$.trim(fila.nombrecorto)) 
						{							
						 	campos += " nombrecorto ";
						}
						campos +=')';
					var idpersona=idp;
					var observacion=campos;
					var URLs=src();
					 var campo0=$("#pendientes").val();
					 $.ajax({
						   url: URLs + '/aportes/radicacionnew/php/guardarAuditaPersona.php',
						   type:"POST",
						   data:{v0:idpersona,v1:observacion, v2:2},
						   success:function(data){
							   var e = parseInt(data);
							   if(e>0){
								   //alert("Auditado");						   	
								   //ActualizarInformacionPersona();
								   
									$.post(URL+'phpComunes/actualizarPersona.php',{v0:v0,v1:v1,v2:v2,v3:v3,v4:v4,v5:v5,v6:v6,v7:v7,v8:v8,v9:v9,v10:v10,v11:v11,v12:v12,v13:v13,v14:v14,v15:v15,v16:v16,v17:v17,v18:v18,v19:v19,v20:v20,v21:v21,v22:v22,v23:v23,v24:v24,v25:v25,v26:v26},function(data){
										if(data==0){
											MENSAJE("El registro no se pudo actualizar!");
											}
										else{
											$.ajax({
												url:URL+'phpComunes/actualizarEmpresaIndependiente.php',
												type:"POST",
												data:{v0:v0,v2:v2,v8:v8,v10:v10,v15:v15,v16:v16},
												async:false,
												success: function(datos) {
													
												}
											});
												if(campo0==undefined || campo0==0){
														
														alert("El registro fue actualizado!");//MENSAJE
													}else{
														msg="Registro actualizado!";
														cerrarRadicacion(msg);
													}
											$("#actualizar").hide();
											$("#txtHiddenSexo,#txtHiddenSexoR").val($("#txtSexo").val());
											$("#tablaPersonaTab input,#tablaPersonaTab select").attr("disabled","disabled");
											//limpiarCampos_otro();
											//$("#div-msje").remove();
											//LLamo el cuadro observaciones de comunes.js parametros (idpersona,tipo) tipo=>1:trabajadores, 2:empresa 
											observacionesTab(idp,1);
										}
									});
							   }else{
								   alert("Error Auditoria");
							   }
						   }
				     });//ajax
					});
				}
			});	
			// FIN AUDITORIA
			/** FIN AUDITORIA PARA VERIFICAR LOS CAMPOS ACTUALIZADOS**/
		});
	}
}
	
	
	function direccion2(obj,obj2){
	/*$("#dialog-form").dialog("destroy"); //Destruir dialog para liberar recursos
	
	$("#dialog-form").dialog({
		autoopen: true,
		height: 450,
		width: 650,
		modal: true,
		open: function(){
			$('#dialog-form').html('');
			$.get(URL+'phpComunes/direcciones.php',function(data){
			$('#dialog-form').html(data);
			});
				$('#tDirCompleta').val('');
				$('#dialog-form select,#dialog-form input[type=text]').val('');
			    $('#uno').focus();
			},
		buttons: {
			'Crear direcci\u00F3n': function() {
			$("#dialog-form").dialog("destroy");
		    $("#dialog-form").dialog('close');
			obj.value=$('#tDirCompleta').val();
			obj2.focus();
			$('#tDirCompleta').val('');
			$('#dialog-form select,#dialog-form input[type=text]').val('');
			}
			},
			close: function() {
				$("#dialog-form").dialog("destroy");
		        $("#dialog-form").dialog('close');
				$('#tDirCompleta').val('');
				$('#dialog-form select,#dialog-form input[type=text]').val('');
				obj2.focus();
				//focus al siguiente cmapo de texto; temporal //10280436
			}
	});
*/
}//end function


function validarutadocumentos(nombre,iden,rutacampo){
	var fechanace=$("#"+nombre).val();
	var iden=$("#"+iden).val();
	if(iden!='')
	arreglo = fechanace.split('-');
	        var ano=arreglo[0];
			var mes=arreglo[1];
			var dia=arreglo[2];
			
	var ruta="digitalizacion\\afiliados\\"+ano+"\\"+mes+"\\"+dia+"\\"+iden+"\\";	
	$("#"+rutacampo).val(ruta);
	//alert(ruta);
	
}

function cerrarRadicacion(msg){
    var campo0=$("#pendientes").val();
	$.ajax({
		url: URL+'phpComunes/cerrarRadicacion.php',
		type: "POST",
		data: "submit=&v0="+campo0,
		success: function(datos){
			if(campo0 != undefined || campo0 != 0){
			msg+="<br>"+datos;
			MENSAJE(msg);
			$("#pendientes").find("option[value='"+campo0+"']").remove();
			}
		}
	});
}
