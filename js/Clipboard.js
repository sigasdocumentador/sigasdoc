/**
 * Clase para manejo de Clipboard para FF e IE (no se agrego la funcionalidad para Chrome ni Opera)
 */
var Clipboard = {
 /**
 * Verifica si el browser es alguna version de Internet Explorer
 * return boolean
 */
 isIE:function(){
 return (/MSIE/.test(navigator.userAgent))
 },
 /**
 * Recupera los datos del clipboard
 * return mixed
 */
 get:function(){
 if(Clipboard.isIE()){
 return clipboardData.getData("Text");
 }else{
 try{
 //verifico que esten habilitados los privilegios de UniversalXPConnect, los cuales son necesarios para acceder a la data del cliente
 if (netscape.security.PrivilegeManager.enablePrivilege){
 netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect");
 }else{
 return "";
 }
 }catch (ex){
 alert('No posee permisos para acceder al ClipBoard desde Firefox. Para habilitarlo deber cambiar el valor de signed.applets.codebase_principal_support a true desde about:config');
 return "";
 }
 //se deben utilizar estos componentes para acceder al clipboard
 //para mas info ver => https://developer.mozilla.org/en/Using_the_Clipboard
 var clip = Components.classes["@mozilla.org/widget/clipboard;1"].getService(Components.interfaces.nsIClipboard);
 if (!clip) return "";
 var trans = Components.classes["@mozilla.org/widget/transferable;1"].createInstance(Components.interfaces.nsITransferable);
 if (!trans) return "";
 trans.addDataFlavor("text/unicode");
 clip.getData(trans, clip.kGlobalClipboard);
 var str = new Object();
 var strLength = new Object();
 trans.getTransferData("text/unicode", str, strLength);
 if (str) str = str.value.QueryInterface(Components.interfaces.nsISupportsString);
 if (str) text = str.data.substring(0, strLength.value / 2);
 return text;
 }
 },
 /**
 * Guarda un dato en el clipboard
 * param mixed text texto a guardar
 * return bool
 */
 set:function(text){
 if(Clipboard.isIE()){
 clipboardData.setData("Text",text);
 }else{
 try{
 //verifico que esten habilitados los privilegios de UniversalXPConnect, los cuales son necesarios para acceder a la data del cliente
 if (netscape.security.PrivilegeManager.enablePrivilege){
 netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect");
 }else{
 return "";
 }
 }catch (ex){
 alert('No posee permisos para acceder al ClipBoard desde Firefox. Para habilitarlo deber cambiar el valor de signed.applets.codebase_principal_support a true desde about:config');
 return "";
 }
 //se deben utilizar estos componentes para acceder al clipboard
 //para mas info ver => https://developer.mozilla.org/en/Using_the_Clipboard
 gClipboardHelper = Components.classes["@mozilla.org/widget/clipboardhelper;1"].getService(Components.interfaces.nsIClipboardHelper);
 gClipboardHelper.copyString(text);
 }
 },
 /**
 * Limpia el contenido de una variable
 * param mixed text texto a limpiar
 * return string
 */
 clear:function(text){
 return text.replace(
 function(){
 var regstr = "<\/?([^>]+)?>";
 reg = new RegExp(regstr, "gi");
 return reg;
 },'');
 },

/**
 * Verifica si hay acceso al clipboard
 * return string
 */
 enabled:function(){
if(Clipboard.isIE()){
 return true;
 }else{
 try{
 //verifico que esten habilitados los privilegios de UniversalXPConnect, los cuales son necesarios para acceder a la data del cliente
 if (netscape.security.PrivilegeManager.enablePrivilege){
 netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect");
 }else{
 return true;
 }
 }catch (ex){
 //alert('No posee permisos para acceder al ClipBoard desde Firefox. Para habilitarlo deber cambiar el valor de signed.applets.codebase_principal_support a true desde about:config');
 return false;
 }
 return true;
 }
 }
}