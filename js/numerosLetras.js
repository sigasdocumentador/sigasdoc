
function mod(dividendo , divisor)
{
  resDiv = dividendo / divisor ;  
  parteEnt = Math.floor(resDiv);            // Obtiene la parte Entera de resDiv
  parteFrac = resDiv - parteEnt ;      // Obtiene la parte Fraccionaria de la divisi�n
  modulo = Math.round(parteFrac * divisor);  // Regresa la parte fraccionaria * la divisi�n (modulo)
  return modulo;
} // Fin de funci�n mod

function ObtenerParteEntDiv(dividendo , divisor)
{
  resDiv = dividendo / divisor ;  
  parteEntDiv = Math.floor(resDiv);
  return parteEntDiv;
} // Fin de funci�n ObtenerParteEntDiv

function fraction_part(dividendo , divisor)
{
  resDiv = dividendo / divisor ;  
  f_part = Math.floor(resDiv);
  return f_part;
} // Fin de funci�n fraction_part


function string_literal_conversion(number)
{  

   centenas = ObtenerParteEntDiv(number, 100);
   
   number = mod(number, 100);

   decenas = ObtenerParteEntDiv(number, 10);
   number = mod(number, 10);

   unidades = ObtenerParteEntDiv(number, 1);
   number = mod(number, 1);  
   string_hundreds="";
   string_tens="";
   string_units="";
 
   if(centenas == 1){
      string_hundreds = "ciento ";
   }
   
   
   if(centenas == 2){
      string_hundreds = "doscientos ";
   }
   
   if(centenas == 3){
      string_hundreds = "trescientos ";
   }
   
   if(centenas == 4){
      string_hundreds = "cuatrocientos ";
   }
   
   if(centenas == 5){
      string_hundreds = "quinientos ";
   }
   
   if(centenas == 6){
      string_hundreds = "seiscientos ";
   }
   
   if(centenas == 7){
      string_hundreds = "setecientos ";
   }
   
   if(centenas == 8){
      string_hundreds = "ochocientos ";
   }
   
   if(centenas == 9){
      string_hundreds = "novecientos ";
   }
   
   if(decenas == 1){
      if(unidades == 1){
         string_tens = "once";
      }
     
      if(unidades == 2){
         string_tens = "doce";
      }
     
      if(unidades == 3){
         string_tens = "trece";
      }
     
      if(unidades == 4){
         string_tens = "catorce";
      }
     
      if(unidades == 5){
         string_tens = "quince";
      }
     
      if(unidades == 6){
         string_tens = "dieciseis";
      }
     
      if(unidades == 7){
         string_tens = "diecisiete";
      }
     
      if(unidades == 8){
         string_tens = "dieciocho";
      }
     
      if(unidades == 9){
         string_tens = "diecinueve";
      }
   }
   
   if(decenas == 2){
      string_tens = "veinti";

   }
   if(decenas == 3){
      string_tens = "treinta";
   }
   if(decenas == 4){
      string_tens = "cuarenta";
   }
   if(decenas == 5){
      string_tens = "cincuenta";
   }
   if(decenas == 6){
      string_tens = "sesenta";
   }
   if(decenas == 7){
      string_tens = "setenta";
   }
   if(decenas == 8){
      string_tens = "ochenta";
   }
   if(decenas == 9){
      string_tens = "noventa";
   }

   if (decenas == 1)
   {
      string_units="";  // empties the units check, since it has alredy been handled on the tens switch
   }
   else
   {
      if(unidades == 1){
         string_units = "un";
      }
      if(unidades == 2){
         string_units = "dos";
      }
      if(unidades == 3){
         string_units = "tres";
      }
      if(unidades == 4){
         string_units = "cuatro";
      }
      if(unidades == 5){
         string_units = "cinco";
      }
      if(unidades == 6){
         string_units = "seis";
      }
      if(unidades == 7){
         string_units = "siete";
      }
      if(unidades == 8){
         string_units = "ocho";
      }
      if(unidades == 9){
         string_units = "nueve";
      }
       // end switch units
   } // end if-then-else
   

if (centenas == 1 && decenas == 0 && unidades == 0)
{
   string_hundreds = "cien " ;
}  

if (decenas == 1 && unidades ==0)
{
   string_tens = "diez " ;
}

if (decenas == 2 && unidades ==0)
{
  string_tens = "veinte " ;
}

if (decenas >=3 && unidades >=1)
{
   string_tens = string_tens+" y ";
}

final_string = string_hundreds+string_tens+string_units;


return final_string ;

}

function covertirNumLetras(number)
{
   number1=number;
   //settype (number, "integer");
   //cent = number1.split(".");  
   centavos = 0; //cent[1];
   
   
   if (centavos == 0 || centavos == undefined){
   centavos = "00";}

   if (number == 0 || number == "")
   { // if amount = 0, then forget all about conversions,
      centenas_final_string=" cero "; // amount is zero (cero). handle it externally, to
      // function breakdown
  }
   else
   {
   
     millions  = ObtenerParteEntDiv(number, 1000000); // first, send the millions to the string
      number = mod(number, 1000000);           // conversion function
     
     if (millions != 0)
      {                      
      // This condition handles the plural case
         if (millions == 1)
         {              // if only 1, use 'millon' (million). if
            descriptor= " millon ";  // > than 1, use 'millones' (millions) as
            }
         else
         {                           // a descriptor for this triad.
              descriptor = " millones ";
            }
      }
      else
      {    
         descriptor = " ";                 // if 0 million then use no descriptor.
      }
      millions_final_string = string_literal_conversion(millions)+descriptor;
         
     
      thousands = ObtenerParteEntDiv(number, 1000);  // now, send the thousands to the string
        number = mod(number, 1000);            // conversion function.
      //print "Th:".thousands;
     if (thousands != 1)
      {                   // This condition eliminates the descriptor
         thousands_final_string =string_literal_conversion(thousands) + " mil ";
       //  descriptor = " mil ";          // if there are no thousands on the amount
      }
      if (thousands == 1)
      {
         thousands_final_string = " mil ";
     }
      if (thousands < 1)
      {
         thousands_final_string = " ";
      }
 
     centenas  = number;                    
      centenas_final_string = string_literal_conversion(centenas) ;
     
   } //end if (number ==0)

   cad = millions_final_string+thousands_final_string+centenas_final_string;
   
   /* Convierte la cadena a May�sculas*/
   cad = cad.toUpperCase();      

   if (centavos.length>2)
   {  
      if(centavos.substring(2,3)>= 5){
         centavos = centavos.substring(0,1)+(parseInt(centavos.substring(1,2))+1).toString();
      }   else{
        centavos = centavos.substring(0,2);
       }
   }

   /* Concatena a los centavos la cadena "/100" */
   if (centavos.length==1)
   {
      centavos = centavos+"0";
   }
   centavos = centavos+ "/100";


   /* Asigna el tipo de moneda, para 1 = PESO, para distinto de 1 = PESOS*/
   if (number == 1)
   {
      moneda = " PESO ";  
   }
   else
   {
      moneda = " PESOS ";  
   }
   /* Regresa el n�mero en cadena entre par�ntesis y con tipo de moneda y la fase M.N.*/
   return cad+moneda;
}
