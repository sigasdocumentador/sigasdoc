// JavaScript Document
// creado por orlando puentes
// fecha marzo 15 de 2010
// objeto funciones comunes
 
function src(){
	var ruta="http://"+location.hostname+"/sigas/";
   return ruta;	
}

var URL=src();

function limpiarCampos() {
  for (var i=0;i<document.forms[0].elements.length;i++){
  	if (document.forms[0].elements[i].type=="text")
		 document.forms[0].elements[i].value="";
	if (document.forms[0].elements[i].type=="select-one")
		 document.forms[0].elements[i].value="0";
	if (document.forms[0].elements[i].type=="textarea")
		document.forms[0].elements[i].value="";
  }
  }

function limpiarCampos2( classCampo ){
	var tipoCampo = "";
	$( classCampo ).each(function(i,row){
		
		if( $( this ).get(0).name == "base" ){
			$( this ).val(0);
			return;
		}
		tipoCampo = $( this ).get(0).type;
		
		if( typeof tipoCampo === "undefined" )
			tipoCampo = $( this ).get(0).localName;
		
		if( tipoCampo == "select-one" )
			$( this ).val(0);
		else if( tipoCampo == "text" || tipoCampo == "textarea" || tipoCampo == "hidden" )
			$( this ).val("");
		else if( tipoCampo == "div" || tipoCampo == "td" || tipoCampo == "span" 
				|| tipoCampo == "tbody" || tipoCampo == "label")
			$( this ).text("");
		else if( tipoCampo == "checkbox" )
			$( this ).attr("checked",false);
	});	
}

/*
 * Metodo: Validar los elementos requeridos del formulario
 */
function validateElementsRequired() {
    var banderaError = 0;
    removeClassError( ".ui-state-error" );
    //$( ".input-error" ).removeClass( "input-error" );
    $(".element-required").each(function(i,row){
            if( $( this ).val() == 0 ){
            	addClassError( this );
                    //$( this ).addClassError( "input-error" );
                    banderaError++;
            }
    });
    return banderaError;
}

function addClassError(selector){
    $( selector ).addClass( "ui-state-error" );
}

function removeClassError(selector){
    $( selector ).removeClass( "ui-state-error" );
}

/**
 * Asignar datapicker de PERIODO | FECHA | FECHA_BETWEEN
 * @param caso [PERIODO | FECHA]
 * @param id [#id,#id]
 */
function datepickerC(caso,id){
	switch(caso){
		case "PERIODO":
			$(id).datepicker({
		        dateFormat: 'yymm',
		        changeMonth: true,
		        changeYear: true,
		        showButtonPanel: true,
		        onClose: function(dateText, inst) {
		        	var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
		            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
		            $(this).datepicker('setDate', new Date(year, month, 1));
		        }
		    });
		    $(id).focus(function () {
				$(".ui-datepicker-calendar").hide();
				$("#ui-datepicker-div").position({
					my: "center top",
					at: "center bottom",
					of: $(this)
				});
			});
			break;
		case "FECHA":
			$(id).datepicker({
				changeMonth: true,
				changeYear: true,
				maxDate: "+0D"
			});
			break;
		case "ANIO":
			$(id).datepicker({
		        dateFormat: 'yy',
		        changeMonth: true,
		        changeYear: true,
		        showButtonPanel: true,
		        onClose: function(dateText, inst) {
		        	var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
		            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
		            $(this).datepicker('setDate', new Date(year, month, 1));
		        }
		    });
		    $(id).focus(function () {
				$(".ui-datepicker-calendar").hide();
				$("#ui-datepicker-div").position({
					my: "center top",
					at: "center bottom",
					of: $(this)
				});
			});
			break;
		case "FECHA_BETWEEN":
			var arrIdFechas = id.split(",");
			$(id).datepicker({
				 maxDate:"+0D",
				 changeMonth: true,
				 changeYear: true,
				 onSelect: function(dateText, inst) {
					 var lockDate = new Date($(arrIdFechas[0]).datepicker('getDate'));
				 	$('input'+arrIdFechas[1]).datepicker('option', 'minDate', lockDate);
				 }
			 });
			 $(arrIdFechas[1]).datepicker({
				 maxDate:"+0D",
				 changeMonth: true,
				 changeYear: true
			 });
			break;
	}
}

/**
 * Funci�n que permite validar que el campo reciba s�lo n�meros y letras. No recibe caracteres especiales ni espacios.
 * @param box Elemento en el que se digitan los datos
 */
function solotexto(box){
	regexp = /\W/g;
	 if(box.value.search(regexp) >= 0){
	 box.value = box.value.replace(regexp, '');
	 }
}

function solonumeros(box){
	regexp =/[A-Za-z��s]/;
	regexp1 = /\W/g;
	if((box.value.search(regexp) >= 0)||(box.value.search(regexp1) >= 0)){
	 box.value = box.value.replace(regexp, '');
	 box.value = box.value.replace(regexp1, '');
	 }
}

function sololetras(box){
	regexp =/[^A-Z�a-z�\s]/;
	regexp1 = /\W/g;
	box.value = box.value.toUpperCase();
	if(box.value.match(regexp)){
		box.value = box.value.replace(regexp, '');
	}
}

 function observacionesTab(id,tipo){
	//Andres lara Sep 28 de 2011
	var URL=src();
	$("div[name='div-observaciones-tab']").dialog("destroy");
	$("textarea[name='observacionGrupo']").val("");
	$("div[name='div-observaciones-tab']").dialog({
		  modal:true,
		  width:520,
		  resizable:false,
		  draggable:false,
		  closeOnEscape:false,
		  open:function(){
			  $(this).prev().children().hide();//Oculto la barra de titulo
			  $(this).prev().html("OBSERVACIONES...");//Pongo un nuevo titulo
			  $("textarea[name='observacionGrupo']").focus();
			  },
		  buttons:{
		    'Guardar':function(){
		    	
		    	var observacion=$("textarea[name='observacionGrupo']").val();
				var observacionSinEspacios = observacion.replace(/^\s+|\s+$/g,"");
				
		    	//Concatenar la observacion del formulario actualizar datos del afiliado
		    	if($("#hidObservacionAutomatica") && typeof $("#hidObservacionAutomatica").val() != "undefined" && $("#hidObservacionAutomatica").val().trim()!=""){
		    		observacion = $("#hidObservacionAutomatica").val() + " - " + observacion;
		    		$("#hidObservacionAutomatica").val("");
		    	}
		    	
				//Si observacion tiene algun valor, se guarda de lo contrario no pasa nada
			    if(observacionSinEspacios!=""){
				 $.ajax({
					   url:URL+"phpComunes/pdo.insert.notas.php",
					   type:"POST",
					   data:{v0:id,v1:observacion,v2:tipo},
					   success:function(data){
					    $("[name='rtaObservacion']").fadeIn();//el mensaje esta oculto en la capa div-observacion
						$("div[name='div-observaciones-tab']").dialog("close");
					   }
			     });//ajax
				}//if observacion
				else{
					alert("No se pudo guardar la observaci\xf3n");
				}
			 }//guardar
		  }//buttons
		  });   
} 
  
  
 function refrescarCombo(obj){
 var elmHermano=jQuery(obj).prev();
 var idHermano=elmHermano.attr("id");
// console.log(idHermano);
 $("tablero:visible #"+idHermano).val("0").trigger("change");

 }
  
function fechaHoy(){
	var mydate=new Date();
	var year=mydate.getYear();
	if (year < 1000)
		year+=1900;
	var day=mydate.getDay();
	var month=mydate.getMonth();
	var daym=mydate.getDate();
	month=parseInt(month);
	switch (month){
		case 0:mes="Enero";break;
		case 1:mes="Febrero";break;
		case 2:mes="Marzo";break;
		case 3:mes="Abril";break;
		case 4:mes="Mayo";break;
		case 5:mes="Junio";break;
		case 6:mes="Julio";break;
		case 7:mes="Agosto";break;
		case 8:mes="Septiembre";break;
		case 9:mes="Octubre";break;
		case 10:mes="Noviembre";break;
		case 11:mes="Diciembre";break;
		}
	
	return mes+", " + daym + " de " + year;
	}  
	
function fechaHoyCorta(){
	var mydate=new Date()
	var year=mydate.getYear()
	if (year < 1000)
		year+=1900
	var day=mydate.getDay()
	var month=mydate.getMonth()
	var daym=mydate.getDate()
	month=parseInt(month)+1;
	return month+"/" + daym + "/" + year;
	}  	
	
function sumar90dias(){
	var mydate=new Date()
	var year=mydate.getYear()
	if (year < 1000)
		year+=1900
	var day=mydate.getDay()
	var month=mydate.getMonth()
	var daym=mydate.getDate()
	month=parseInt(month)+4;
	if(month>12){
		month=1;
		year++;
		}
	return month+"/" + daym + "/" + year;
	}
	
function hora(){
	var mydate=new Date();
	var myhora=mydate.getHours();
	var myminutos=mydate.getMinutes();
	if(myminutos<10)
		m='0'+myminutos;
	else
		m=myminutos;
	return myhora+":"+m;
	}	

 function tabular (field, event) {//funcion para tabular con tecla enter
        var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
        if (keyCode == 13) {
            var i;
            for (i = 0; i < field.form.elements.length; i++)
                if (field == field.form.elements[i])
                    break;
            document.forms[0].elements[i+1].focus();
            return false;
        }
        else
        return true;
    }    

function buscarSucursalesCombo(obj){
	if(obj.value.length==0)
		return false;
	
	$(function () {
   		//var nit =$('#txtNit').val();
		var nit =obj.value;
		$.post(URL+"phpComunes/buscarSucursalesCombo.php", {nit: nit}, function(data){
			var cmbSucursal = $("#comboSucursal");
			$(cmbSucursal).html(data);
			if($(cmbSucursal).children().length==1){
				alert('Lo lamento el NIT no existe o tiene estado pendiente o inactivo.');
				return false;
			}
		});			
   })
		
}	
	
function buscarSucursalesListar(obj){
	if(obj.value.length==0)
		return false;
	
	var campo0=obj.value;
	$.getJSON(URL+'phpComunes/buscarSucursalesListar.php',{v0:campo0},function(datos){
		var cadena="";
		$.each(datos, function(i, fila){
		cadena+="<label style=cursor:pointer onclick=buscarEmpresa("+fila.idempresa+")>"+ fila.idempresa+" "+fila.codigosucursal+" "+fila.razonsocial+"</label><br>";	
		})
		document.getElementById('div-sucursales').innerHTML=cadena;
	});	
}


function formatCurrency(num) {
num = num.toString().replace(/\$|\,/g,'');
if(isNaN(num))
num = "0";
sign = (num == (num = Math.abs(num)));
num = Math.floor(num*100+0.50000000001);
cents = num%100;
num = Math.floor(num/100).toString();
if(cents<10)
cents = "0" + cents;
for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
num = num.substring(0,num.length-(4*i+3))+','+
num.substring(num.length-(4*i+3));
//return (((sign)?'':'-') + '$' + num + '.' + cents);
return (((sign)?'':'-') + '$' + num);

}	

function formatNumber(num) {
	num = num.toString().replace(/\$|\,/g,'');
	if(isNaN(num))
	num = "0";
	sign = (num == (num = Math.abs(num)));
	num = Math.floor(num*100+0.50000000001);
	cents = num%100;
	num = Math.floor(num/100).toString();
	if(cents<10)
	cents = "0" + cents;
	for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
	num = num.substring(0,num.length-(4*i+3))+','+
	num.substring(num.length-(4*i+3));
	//return (((sign)?'':'-') + '$' + num + '.' + cents);
	return (((sign)?'':'-') + num);
}	

function digito(num) {
var arut = new Array(13);
var i, j, dv;

for (i=1; i<12;i++) {
arut[i]=0;}
i=0
for (j = (12-(num.length)); j<12;j++) {
    if (( num.substr(i,1) >= 0) & ( num.substr(i,1) <= 9)) {
    arut[j] = num.substr(i,1);i++;
    }
    else {
        window.alert("Debe ingresar sólo números!!! " + num);
        i=0;
        break;
    }
}
if (i>0) {
dv = 11 - (( (arut[1]*3) + (arut[2]*2) + (arut[3]*7) + (arut[4]*6) + (arut[5]*5) + (arut[6]*4) + (arut[7]*3) + (arut[8]*2) )%11)
if (dv === 10) {
dv = "9";}
else if (dv === 11) {
dv = "0";}
//window.alert("El Rut sería : " + num + "-" + dv)
var xx=parseInt(dv);
if(isNaN(xx))
    dv="9";
return dv}

}

function mesDiaAnnoSin(){
	var mydate=new Date()
	var year=mydate.getYear()
	if (year < 1000)
		year+=1900
	var day=mydate.getDay()
	var month=mydate.getMonth()
	var daym=mydate.getDate()
	month=parseInt(month);
	switch (month){
		case 0:mes="01";break;
		case 1:mes="02";break;
		case 2:mes="03";break;
		case 3:mes="04";break;
		case 4:mes="05";break;
		case 5:mes="06";break;
		case 6:mes="07";break;
		case 7:mes="08";break;
		case 8:mes="09";break;
		case 9:mes="10";break;
		case 10:mes="11";break;
		case 11:mes="12";break;
		}
        daym=parseInt(daym);
        var d=(daym<0)?"0"+daym:daym;
	return mes+d+year;
	}

function click(e){
    if(e.button==0){
        alert("El boton izquierdo esta desabilitado! Por favor lea los mensajes");
        return false;
    }
}   

function espacios(cadena){
	str = cadena.replace(/^\s*|\s*$/g,"");
	return(str);
	}
	
function MENSAJE(str){
	var dialog="<div title='MENSAJE SIGAS' id='info'></div>";
	$("#info").dialog("destroy").remove();
	$(dialog).appendTo("body");
    $("#info").append("<p>"+str+"</p>");
    $("#info").dialog({
	  modal:true,
	  resizable:false,
	  width: 460,
	  closeOnEscape:true
	});      
}

function dialogLoading(display){
 var URL=src();	
 $("#dlgLoading").remove();
 
 if(display=="show"){
 var dialog="<div title='Cargando Datos' id='dlgLoading' align='center'><img src='"+URL+"imagenes/ajaxload.gif' /><p>Procesando...</p></div>";
 $(dialog).appendTo("body");
 $("#dlgLoading").dialog({
		  modal:true,
		  resizable:false,
		  closeOnEscape:false,
		  width: 180,
		  open:function(){
		  $(this).prev().children().children().hide(); 
		  //alert($(this).prev().children().children().attr("class"));
		  }
		  });    
 }
 
 /*else{
   $("#dlgLoading").dialog("close");
 }*/

}

function informar(pagina){
	//var URL=src();	
	//$.getJSON(URL+'phpComunes/paginaActual.php',{v0:pagina},function(data){
	//})
	}

/*
 * Calcula la edad a partir de la fecha.
 * Formato: MM/DD/AAAA
 */
function calcular_edad(fecha){
	
	var hoy = new Date();
	var fechaNac = new Date();
	
	var array_fecha = fecha.split("/");
    //si el array no tiene tres partes, la fecha es incorrecta
    if (array_fecha.length!=3)
       return false;
	
    var regexpFechas = /^(|(0[1-9])|(1[0-2]))\/((0[1-9])|(1\d)|(2\d)|(3[0-1]))\/((\d{4}))$/;
	if(!fecha.match(regexpFechas)){
		//la fecha no cumple con el formato para fechas
		return false;
	}
    
	mes = array_fecha[0];
	if(mes.length == 2){
		//console.log(mes);
		if(parseInt(mes[0]) == 0){
			mes = parseInt(mes[1]);
		}else{
			mes = parseInt(mes[1]);
		}
		
		if (isNaN(mes) || mes == 0)
		       return false;
	}else{
		mes = parseInt(mes);
		if (isNaN(mes) || mes == 0)
			return false;
	}

	var dia;
	dia = parseInt(array_fecha[1]);
    if (isNaN(dia))
    	return false;
	
    var ano;
    ano = parseInt(array_fecha[2]);
    if (isNaN(ano))
    	return false;

	fechaNac.setFullYear(ano,(mes-1),dia);
	var edad = parseInt((hoy - fechaNac)/365/24/60/60/1000);
	
	return edad;
} 
//                         fec ingresa   fecha actual  
function diferenciaFechas (CadenaFecha1,CadenaFecha2) {  
   //Obtiene dia, mes y año  
   var fecha1 = new fecha( CadenaFecha1 ); 
   var fecha2 = new fecha( CadenaFecha2 );
     
   //Obtiene objetos Date  
   var miFecha1 = new Date( fecha1.anio, fecha1.mes, fecha1.dia );  
   var miFecha2 = new Date( fecha2.anio, fecha2.mes, fecha2.dia );
  
   //Resta fechas y redondea  
   var diferencia = miFecha1.getTime() - miFecha2.getTime();
   var dias = Math.floor(diferencia / (1000 * 60 * 60 * 24));
   var segundos = Math.floor(diferencia / 1000);
   return dias;
} 
 
function fecha( fecha ) {  
   //Separador para la introduccion de las fechas  
   var separador = "/" ; 
   var cadFecha=fecha;
   //Separa por dia, mes y año  
   if (cadFecha.indexOf(separador)!= -1 ) {  
        var posi1 = 0;
        var posi2 = cadFecha.indexOf( separador, posi1 + 1 );
        var posi3 = cadFecha.indexOf( separador, posi2 + 1 );
        this.dia = cadFecha.substring( posi1, posi2 );
        this.mes = cadFecha.substring( posi2 + 1, posi3 );
        this.anio = cadFecha.substring( posi3 + 1, cadFecha.length );
   } else {  
        this.dia = 0;  
        this.mes = 0;  
        this.anio = 0;     
   }  
}  

shortcut = {
	'all_shortcuts':{},//All the shortcuts are stored in this array
	'add': function(shortcut_combination,callback,opt) {
		//Provide a set of default options
		var default_options = {
			'type':'keydown',
			'propagate':false,
			'disable_in_input':false,
			'target':document,
			'keycode':false
		}
		if(!opt) opt = default_options;
		else {
			for(var dfo in default_options) {
				if(typeof opt[dfo] == 'undefined') opt[dfo] = default_options[dfo];
			}
		}

		var ele = opt.target;
		if(typeof opt.target == 'string') ele = document.getElementById(opt.target);
		var ths = this;
		shortcut_combination = shortcut_combination.toLowerCase();

		//The function to be called at keypress
		var func = function(e) {
			e = e || window.event;
			
			if(opt['disable_in_input']) { //Don't enable shortcut keys in Input, Textarea fields
				var element;
				if(e.target) element=e.target;
				else if(e.srcElement) element=e.srcElement;
				if(element.nodeType==3) element=element.parentNode;

				if(element.tagName == 'INPUT' || element.tagName == 'TEXTAREA') return;
			}
	
			//Find Which key is pressed
			if (e.keyCode) code = e.keyCode;
			else if (e.which) code = e.which;
			var character = String.fromCharCode(code).toLowerCase();
			
			if(code == 188) character=","; //If the user presses , when the type is onkeydown
			if(code == 190) character="."; //If the user presses , when the type is onkeydown

			var keys = shortcut_combination.split("+");
			//Key Pressed - counts the number of valid keypresses - if it is same as the number of keys, the shortcut function is invoked
			var kp = 0;
			
			//Work around for stupid Shift key bug created by using lowercase - as a result the shift+num combination was broken
			var shift_nums = {
				"`":"~",
				"1":"!",
				"2":"@",
				"3":"#",
				"4":"$",
				"5":"%",
				"6":"^",
				"7":"&",
				"8":"*",
				"9":"(",
				"0":")",
				"-":"_",
				"=":"+",
				";":":",
				"'":"\"",
				",":"<",
				".":">",
				"/":"?",
				"\\":"|"
			}
			//Special Keys - and their codes
			var special_keys = {
				'esc':27,
				'escape':27,
				'tab':9,
				'space':32,
				'return':13,
				'enter':13,
				'backspace':8,
	
				'scrolllock':145,
				'scroll_lock':145,
				'scroll':145,
				'capslock':20,
				'caps_lock':20,
				'caps':20,
				'numlock':144,
				'num_lock':144,
				'num':144,
				
				'pause':19,
				'break':19,
				
				'insert':45,
				'home':36,
				'delete':46,
				'end':35,
				
				'pageup':33,
				'page_up':33,
				'pu':33,
	
				'pagedown':34,
				'page_down':34,
				'pd':34,
	
				'left':37,
				'up':38,
				'right':39,
				'down':40,
	
				'f1':112,
				'f2':113,
				'f3':114,
				'f4':115,
				'f5':116,
				'f6':117,
				'f7':118,
				'f8':119,
				'f9':120,
				'f10':121,
				'f11':122,
				'f12':123
			}
	
			var modifiers = { 
				shift: { wanted:false, pressed:false},
				ctrl : { wanted:false, pressed:false},
				alt  : { wanted:false, pressed:false},
				meta : { wanted:false, pressed:false}	//Meta is Mac specific
			};
                        
			if(e.ctrlKey)	modifiers.ctrl.pressed = true;
			if(e.shiftKey)	modifiers.shift.pressed = true;
			if(e.altKey)	modifiers.alt.pressed = true;
			if(e.metaKey)   modifiers.meta.pressed = true;
                        
			for(var i=0; k=keys[i],i<keys.length; i++) {
				//Modifiers
				if(k == 'ctrl' || k == 'control') {
					kp++;
					modifiers.ctrl.wanted = true;

				} else if(k == 'shift') {
					kp++;
					modifiers.shift.wanted = true;

				} else if(k == 'alt') {
					kp++;
					modifiers.alt.wanted = true;
				} else if(k == 'meta') {
					kp++;
					modifiers.meta.wanted = true;
				} else if(k.length > 1) { //If it is a special key
					if(special_keys[k] == code) kp++;
					
				} else if(opt['keycode']) {
					if(opt['keycode'] == code) kp++;

				} else { //The special keys did not match
					if(character == k) kp++;
					else {
						if(shift_nums[character] && e.shiftKey) { //Stupid Shift key bug created by using lowercase
							character = shift_nums[character]; 
							if(character == k) kp++;
						}
					}
				}
			}
			
			if(kp == keys.length && 
						modifiers.ctrl.pressed == modifiers.ctrl.wanted &&
						modifiers.shift.pressed == modifiers.shift.wanted &&
						modifiers.alt.pressed == modifiers.alt.wanted &&
						modifiers.meta.pressed == modifiers.meta.wanted) {
				callback(e);
	
				if(!opt['propagate']) { //Stop the event
					//e.cancelBubble is supported by IE - this will kill the bubbling process.
					e.cancelBubble = true;
					e.returnValue = false;
	
					//e.stopPropagation works in Firefox.
					if (e.stopPropagation) {
						e.stopPropagation();
						e.preventDefault();
					}
					return false;
				}
			}
		}
		this.all_shortcuts[shortcut_combination] = {
			'callback':func, 
			'target':ele, 
			'event': opt['type']
		};
		//Attach the function with the event
		if(ele.addEventListener) ele.addEventListener(opt['type'], func, false);
		else if(ele.attachEvent) ele.attachEvent('on'+opt['type'], func);
		else ele['on'+opt['type']] = func;
	},

	//Remove the shortcut - just specify the shortcut and I will remove the binding
	'remove':function(shortcut_combination) {
		shortcut_combination = shortcut_combination.toLowerCase();
		var binding = this.all_shortcuts[shortcut_combination];
		delete(this.all_shortcuts[shortcut_combination])
		if(!binding) return;
		var type = binding['event'];
		var ele = binding['target'];
		var callback = binding['callback'];

		if(ele.detachEvent) ele.detachEvent('on'+type, callback);
		else if(ele.removeEventListener) ele.removeEventListener(type, callback, false);
		else ele['on'+type] = false;
	}
}

//Tooltips de razon social--------------------------
function tooltip(elm,nit){
	var div="<div class='tooltip'><p>"+nit+"</p></div>";
	
	var cssValores={
 	    "position":"absolute",
		"z-index":"999",
		"left":"-9999px",
		"backgroundColor":"#dedede",
		"padding":"5px",
		"border":"1px solid #ccc",
		"width":"250px"
		}
	
	//Añado los estilos al tooltip
	$(".tooltip").css(cssValores);
		
	//iteracion de elementos	
	$(elm).each(function(i){
	  
	   //Gestor de eventos
	   $(this).live({
	    mouseover:function(){
			       //Agrego el div a la pagina
	               $("body").append(div);
				   $(".tooltip").hide().fadeIn();
				  },
		mousemove:function(event){
			       $(".tooltip").css({left:event.pageX+15,top:event.pageY+15})
				  },
		mouseout:function(){
			     $(".tooltip").fadeOut();
				 $(".tooltip").remove();
				  }
		});//bind
	});//each
		
}

function salarioMinimo(){
	var smlv=0;
	$.ajax({
		url: URL+'phpComunes/pdo.salario.minimo.php',
		type: "POST",
		dateType: "json",
		async: false,
		success: function(datos){
			smlv = datos;
		}
	});
	return smlv;
}

function dialogLoading(display){
	 var URL=src();	
	 $("#dlgLoading").remove();
	 
	 if(display=="show"){
	 var dialog="<div title='Cargando Datos' id='dlgLoading' align='center'><img src='"+URL+"imagenes/ajaxload.gif' /><p>Procesando...</p></div>";
	 $(dialog).appendTo("body");
	 $("#dlgLoading").dialog({
			  modal:true,
			  resizable:false,
			  closeOnEscape:false,
			  width: 180,
			  open:function(){
			  $(this).prev().children().children().hide(); 
			  //alert($(this).prev().children().children().attr("class"));
			  }
			  });    
	 }
}

function validarEstado016(td,num,form){
	var cedula=num;
	var tdoc=td;
	var formularioDispara = form;
	if(tdoc==0){
		alert("Falta tipo documento del padre Biológico!!");
		$("#nomBiologico").html(" ");
		$("#IdPadreBiol").val("");
		return false;
	}
	if(cedula==''){
		//alert("Escriba el documento del padre Biológico!!");
		return false;
	}
	var op=2;
	var idp=0;
	if(num!="")
		{
			$.ajax({
				url: URL+'phpComunes/pdo.buscar.persona.php',
				type: "POST",
				data: { v0:tdoc,v1:cedula,v2:op},
				async: false,
				dataType: "json",
				success: function(datos){
					if(datos==0){
						alert("La persona NO Existe en nuestra Base, guarde los datos Completos!");
						if(formularioDispara == 'n'){
							persona_simple(document.getElementById('IdPadreBiol'), document.getElementById('hideIdbiol'),tdoc);
							return false;
						}else if(formularioDispara == 'm'){
							persona_simple(document.getElementById('IdPadreBiolUpd'), document.getElementById('hideIdbiolUpd'),tdoc);
						}
						
					}
					else{
						idp=datos[0].idpersona;
						var nom=datos[0].pnombre+" "+datos[0].snombre+" "+datos[0].papellido+" "+datos[0].sapellido;
						$("#nomBiologico").html(nom);
					}
				},
				complete: function(){
					if(idp!=0)
						{
							$.ajax({
								url: URL+'phpComunes/pdo.b.afiliacion.php',
								type:"POST",
								data:{v0:idp,v1:1},
								async:false,
								dataType:"json",
								success:function(dato){
									if(dato==0){
										alert("El padre biologico NO tiene afiliacion ACTIVA");
										return true;
										}
									else{
										alert("El padre biologico tiene afiliacion ACTIVA");
										//$("#tipoIdPadreBiol").val('0');
										$("#IdPadreBiol").val("");
										$("#nomBiologico").html(" ");
										return false;
										}
								}
							});
						}
					}
			});
		}
}

function vpnombre(e){
	tecla = (document.all) ? e.keyCode : e.which; 
    if ((tecla==8) ||(tecla==0)) return true; 
    patron =/[A-Za-z\u00F1\u00D1]/; 
    te = String.fromCharCode(tecla); 
    if(patron.test(te)) return te; else return false;
}

function vsnombre(e){
	tecla = (document.all) ? e.keyCode : e.which; 
    if ((tecla==8) ||(tecla==0)) return true; 
    patron =/[A-Za-z\u00F1\u00D1\s?]/; 
    te = String.fromCharCode(tecla); 
    if(patron.test(te)) return te; else return false;
}

function soloNumeros(e,f){
	//cc 3,4,5,6,8,10
	//rg alfa 
	tecla = (document.all) ? e.keyCode : e.which; 
    if ((tecla==8) ||(tecla==0)) return true; 
    patron =/[0-9]/; 
    te = String.fromCharCode(tecla); 
    if(patron.test(te)) {
		return te;} 
	else {
		return false;}
	}
	
function voltiarFecha(f){
	var dia=f.slice(-2);
	var mes=f.slice(5,7);
	var ano=f.slice(0,4);
	fecha=mes+"/"+dia+"/"+ano;
	return fecha;
	}
	
function rellenarTarjeta(campo,bono){
	if(bono.length==15 || bono.length==0 ) return false;
	if(bono.length==16){
		tarjeta=bono;
	}
	else{
		var ceros = ""; 
		lenBono = 11-bono.length;
			 
		for(var i=0;i<lenBono;i++){			 
			ceros +="0" ;
		}
		
		/**
		* 63648 es el c�digo inicial de las tarjetas para comfamiliar.
		* Todas las tarjetas de la base de datos tienen ese c�digo inicial
		* @todo se debe utilizar valor de parametrizaci�n
		*/
		tarjeta="63648"+ceros+bono;
	}
	campo.value=tarjeta;
}
	
function nombre_corto(pn,sn,pa,sa){
	var patron=new RegExp('[\u00F1|\u00D1]');
	for(i=1;i<=pn.length;i++){
		pn=pn.replace(patron,"&");
	}
	for(i=1;i<=sn.length;i++){
		sn=sn.replace(patron,"&");
	}
	for(i=1;i<=pa.length;i++){
		pa=pa.replace(patron,"&");
	}
	for(i=1;i<=sa.length;i++){
		sa=sa.replace(patron,"&");
	}
	txt=pn+" "+sn+" "+pa+" "+sa;
	txt=txt.toUpperCase();
	num=parseInt(txt.length);
	
	if(num>30){
		if(sn.length>0){
			sn=sn.slice(0,1);
			txt=pn+" "+pa+" "+sa;
			txt=txt.toUpperCase();
			num=parseInt(txt.length);
			}
		if(num>30)
			if(sa.length>0){
				sa=sa.slice(0,1);
				txt=pn+" "+pa;
				txt=txt.toUpperCase();
				}
	}
	return txt;
}

function soloemail(box){
	if(box.value != '' && !validateEmail(box.value)){
		alert("Verifique el E-mail digitado.");
		box.focus();
	}
}

function validateEmail(email){
	var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	if( !emailReg.test( email ) ) {
		return false;
	}else{
		return true;
	}
}

/**
 * No permite que se digiten espacios en el campo objetivo
 * @param e event
 * @returns {Boolean}
 */
function validarEspacio(e){
	tecla=(document.all) ? e.keyCode : e.which;
	if(tecla==32){
		return false;
	}
}


/**
 * Valida la longitud del numero de documento
 * @param tipoDoc string
 * @param objetDoc object
 */
function validarLongNumIdent(tipoDoc,objetDoc){
	objetDoc.value = $.trim(objetDoc.value);
	
	switch(parseInt(tipoDoc))
	{
	case 1:
		if(objetDoc.value.length!=4 && objetDoc.value.length!=5 && objetDoc.value.length!=6 && objetDoc.value.length != 7 && objetDoc.value.length != 8 && objetDoc.value.length != 10)
			{
			alert("El numero de documento debe tener 5,6,7,8 o 10 digitos!");
			objetDoc.focus();
			objetDoc.value='';
			}
		break;
	case 2:
		if(objetDoc.value.length!=10&&objetDoc.value.length!=11)
		{
			alert("El numero de documento debe tener 10 o 11 digitos!");
			objetDoc.focus();
			objetDoc.value='';
		}
		break;
	case 3:
		/*if(objetDoc.value.length!=9)
		{
			alert("El numero de documento debe tener 9 digitos!");
			objetDoc.focus();
			objetDoc.value='';
		}*/
		break;
	case 4:
		/**
		 * @todo Se debe definir cu�l es la validaci�n correcta para poder grabar c�dulas de extranjer�a
		 */
		/*if(objetDoc.value.length!=7)
		{
			alert("El numero de documento debe tener 7 digitos!");
			objetDoc.focus();
			objetDoc.value='';
		}*/
		
		break;
	case 5:
		if(objetDoc.value.length == '')
			return false;
		
		if(objetDoc.value.length<3||objetDoc.value.length>11)
		{
			alert("El numero de documento debe tener de 3 a 11 digitos!");
			objetDoc.focus();
			objetDoc.value='';
		}
		break;
	case 6:
		if(objetDoc.value.length!=8&&objetDoc.value.length!=10&&objetDoc.value.length!=11)
		{
			alert("El numero de documento debe tener 8,10 o 11 digitos!");
			objetDoc.focus();
			objetDoc.value='';
		}
		break;
	default:
		break;
	}
}


function validarLongitudIdentificacionRetorno(idTipo, numero){
	var retorno = {'valido': true, 'mensaje': ''};
	switch(parseInt(idTipo)){
		case 1:
			if(numero.length!=5 && numero.length!=6 && numero.length!=7 && numero.length!=8 && numero.length!=10){
				retorno.mensaje = "El numero de documento debe tener 5,6,7,8 o 10 digitos!";
				retorno.valido = false;
			}
			break;
		case 2:
			if(numero.length!=10 && numero.length!=11){
				retorno.mensaje = "El numero de documento debe tener 10 o 11 digitos!";
				retorno.valido = false;
			}
			break;
		case 3:
			/*if(numero.length!=9){
				retorno.mensaje = "El numero de documento debe tener 9 digitos!";
				retorno.valido = false;
			}*/
			break;
		case 4:
			/**
			 * @todo Se debe definir cu�l es la validaci�n correcta para poder grabar c�dulas de extranjer�a
			 */
			/*if(numero.length!=7)
			{
				alert("El numero de documento debe tener 7 digitos!");
				objetDoc.focus();
				numero='';
			}*/
			
			break;
		case 5:
			if(numero.length<3 || numero.length>11){
				retorno.mensaje = "El numero de documento debe tener de 3 a 11 digitos!";
				retorno.valido = false;
			}
			break;
		case 6:
			if(numero.length!=8 && numero.length!=10 && numero.length!=11){
				retorno.mensaje = "El numero de documento debe tener 8,10 o 11 digitos!";
				retorno.valido = false;
			}
			break;
		default:
			break;
	}
	return retorno;
}

function validarCaracteresPermitidos(tipoDoc,campoAsignar)
{
	campoAsignar.value = $.trim(campoAsignar.value);
	regexp =/[A-Za-z��s]/;
	regexp1 = /\W/g;
	regexpsolotexto = /\W/g;
	switch(parseInt(tipoDoc))
	{
	case 1:
		if((campoAsignar.value.search(regexp) >= 0)||(campoAsignar.value.search(regexp1) >= 0)){
			campoAsignar.value = campoAsignar.value.replace(regexp, '');
			campoAsignar.value = campoAsignar.value.replace(regexp1, '');}
		break;
	case 2:
		if((campoAsignar.value.search(regexp) >= 0)||(campoAsignar.value.search(regexp1) >= 0)){
			campoAsignar.value = campoAsignar.value.replace(regexp, '');
			campoAsignar.value = campoAsignar.value.replace(regexp1, '');}
		break;
	case 3:
		if(campoAsignar.value.search(regexpsolotexto) >= 0){
			campoAsignar.value = campoAsignar.value.replace(regexpsolotexto, '');}
		break;
	case 4:
		if(campoAsignar.value.search(regexpsolotexto) >= 0){
			campoAsignar.value = campoAsignar.value.replace(regexpsolotexto, '');}
		break;
	case 5:
		if((campoAsignar.value.search(regexp) >= 0)||(campoAsignar.value.search(regexp1) >= 0)){
			campoAsignar.value = campoAsignar.value.replace(regexp, '');
			campoAsignar.value = campoAsignar.value.replace(regexp1, '');}
		break;
	case 6:
		if(campoAsignar.value.search(regexpsolotexto) >= 0){
			campoAsignar.value = campoAsignar.value.replace(regexpsolotexto, '');}
		break;
	default:
		break;
	}
}

function CambiarComboActEconm(ciuDosPrim)
{
	if(ciuDosPrim=='01'||ciuDosPrim=='02')
		return 1;
	if(ciuDosPrim=='05')
		return 32;
	if(ciuDosPrim>='10'&&ciuDosPrim<='14')
		return 41;
	if(ciuDosPrim>='15'&&ciuDosPrim<='37')
		return 80;
	if(ciuDosPrim=='40'||ciuDosPrim=='41')
		return 329;
	if(ciuDosPrim=='45')
		return 343;
	if(ciuDosPrim>='50'&&ciuDosPrim<='52')
		return 368;
	if(ciuDosPrim=='55')
		return 466;
	if(ciuDosPrim>='60'&&ciuDosPrim<='64')
		return 486;
	if(ciuDosPrim>='65'&&ciuDosPrim<='67')
		return 543;
	if(ciuDosPrim>='70'&&ciuDosPrim<='74')
		return 581;
	if(ciuDosPrim=='75')
		return 635;
	if(ciuDosPrim=='80')
		return 655;
	if(ciuDosPrim=='85')
		return 672;
	if(ciuDosPrim>='90'&&ciuDosPrim<='93')
		return 690;
	if(ciuDosPrim>='95'&&ciuDosPrim<='97')
		return 728;
	if(ciuDosPrim=='99')
		return 739;
}
//VALIDAR LA FECHA DE INGRESO SEA MAYOR O IGUAL A LA FECHA DE RETIRO
function validarFechaIngresoGrabacion(nombrefecha){
	    var idp=$("#idPersona").val();
	    var ide=$("#idEmpresa").val();
	    //alert(ide);
	    $.ajax({
		url:URL+'aportes/radicacionnew/php/buscarFechaIngresoRadica.php', 
		type: "POST",
		data: {v0:idp,v1:ide},
		async: false,
		dataType: "json",
		success:function(data){
			$.each(data,function(i,fila){ 
				
				var nombre = new Date($("#"+nombrefecha).val());
				//alert(nombre);
				
				/*var fechaingr= new Date($("#fecIngreso2").val());*/
				fila.fecharetiro = new Date(fila.fecharetiro);
				
				if(nombre<fila.fecharetiro){
				document.getElementById(nombrefecha).value = '';
				alert("La fecha de ingreso debe ser mayor o igual a la fecha de retiro");
				}
       		});
		}
	})
}
function alfanumerico(campo){ 
	var charpos = campo.value.search("[^A-Za-z0-9& ]"); 
	if(campo.value.length > 0 &&  charpos >= 0) { 
		strError = "El campo solo acepta letras de A a la Z el & y digitos"; 
		alert(strError + "\n [Posici�n del caracter err�neo: " + eval(charpos+1)+"]"); 
		campo.value = campo.value.substr(0, charpos);
		return false; 
	} else {//if 
		return true;
	}
}

//objeto creado para dar formato a pesos $
//de 256700.23 retorna "$256.700,23"
// creada el  31 de marzo de 2016 Oscar Parra
var formatNumber = {
        separador: ".", // separador para los miles
        sepDecimal: ',', // separador para los decimales
        formatear:function (num){
                  num +='';
                  var splitStr = num.split('.');
                  var splitLeft = splitStr[0];
                  var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
                  var regx = /(\d+)(\d{3})/;
                  while (regx.test(splitLeft)) {
                        splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
                  }
                  return this.simbol + splitLeft  +splitRight;
        },
        new:function(num, simbol){
         this.simbol = simbol || '';
         return this.formatear(num);
        }
}
//fin formateo

//funcion llamada ajax general

function call_Ajax(rutaajax,objdata,typesend="GET"){
	
    var retorno;
	$.ajax({
		url: rutaajax,
		async: false,
		data: objdata,		
		beforeSend: function(objeto){
        	dialogLoading('show');
        },        
        complete: function(objeto, exito){
        	dialogLoading('close');
            if(exito != "success"){
                alert("No se completo el proceso!");
            }            
        },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        error: function(objeto, quepaso, otroobj){
            alert("Ocurrio el siguiente error: "+quepaso);
        },
        global: true,
        ifModified: false,
        processData:true,
		success: function(data){
			retorno=data;
		},
		timeout: 30000,
        type: typesend
	});
	
	return retorno;
	
}








