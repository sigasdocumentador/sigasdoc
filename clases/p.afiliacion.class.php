<?php
/* autor:       orlando puentes
 * fecha:       15/09/2010
 * objetivo:     
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'phpComunes'.DIRECTORY_SEPARATOR.'session.php';
session();

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'conexion.class.php';
include_once $_SESSION['RAIZ'] . DIRECTORY_SEPARATOR . '/rsc/pdo/IFXDbManejador.php';

include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'utilitario.class.php';

class Afiliacion{
	 //constructor	
	var $con;
	private $db;
function Afiliacion(){
 	$this->con=new DBManager;
 	
 	$this->db = IFXDbManejador::conectarDB();
 	if($this->db->conexionID==null){
 		$cadena = $this->db->error;
 		echo $cadena;
 		exit();
 	}
}

function actualizar_afiliacion($campos){
	if($this->con->conectar()==true){
		$sql="update aportes016 set tipoformulario=".$campos[1].",tipoafiliacion=".$campos[2].",idempresa=".$campos[3].",fechaingreso='".$campos[4]."',horasdia=".$campos[5].",horasmes=".$campos[6].",salario=".$campos[7].",agricola='".$campos[8]."',cargo=".$campos[9].",primaria='".$campos[10]."',estado='".$campos[11]."',tipopago='".$campos[12]."',categoria='".$campos[13]."', usuario='".$_SESSION["USUARIO"]."' where idformulario = ".$campos[0];
		return mssql_query($sql,$this->con->conect);
	}
}

function actualizar_afiliacion2($campos){
	if($this->con->conectar()==true){
		$sql="update aportes016 set tipoformulario=".$campos[1].",tipoafiliacion=".$campos[2].",idempresa=".$campos[3].",fechaingreso='".$campos[4]."',horasdia=".$campos[5].",horasmes=".$campos[6].",salario=".$campos[7].",agricola='".$campos[8]."',cargo=".$campos[9].",primaria='".$campos[10]."',estado='".$campos[11]."',tipopago='".$campos[12]."',categoria='".$campos[13]."', usuario='".$_SESSION["USUARIO"]."', claseafiliacion='".$campos[14]."' where idformulario = ".$campos[0];
		return mssql_query($sql,$this->con->conect);
	}
}

function actualizar_afiliacion_inactiva($campos){
	if($this->con->conectar()==true){
		$sql="update aportes017 set 
				tipoformulario= {$campos["tipoformulario"]},
				tipoafiliacion= {$campos["tipoafiliacion"]},
				idempresa= {$campos["idempresa"]},
				fechaingreso='{$campos["fechaingreso"]}',
				fecharetiro='{$campos["fecharetiro"]}',
				motivoretiro= {$campos["motivoretiro"]},
				horasdia= {$campos["horasdia"]},
				horasmes= {$campos["horasmes"]},
				salario= {$campos["salario"]},
				agricola= '{$campos["agricola"]}',
				cargo= {$campos["cargo"]},
				primaria= '{$campos["primaria"]}',
				estado= '{$campos["estado"]}',
				tipopago= '{$campos["tipopago"]}',
				categoria= '{$campos["categoria"]}' 
			where 
				idformulario = {$campos["idformulario"]}";
		return mssql_query($sql,$this->con->conect);
	}
}

function actualizar_afiliacion_inactiva2($campos){
	if($this->con->conectar()==true){
				$sql="update aportes017 set tipoformulario= {$campos["tipoformulario"]}, tipoafiliacion= {$campos["tipoafiliacion"]}, idempresa= {$campos["idempresa"]},
				fechaingreso='{$campos["fechaingreso"]}', fecharetiro='{$campos["fecharetiro"]}', motivoretiro= {$campos["motivoretiro"]}, horasdia= {$campos["horasdia"]},
				horasmes= {$campos["horasmes"]}, salario= {$campos["salario"]}, agricola= '{$campos["agricola"]}', cargo= {$campos["cargo"]}, primaria= '{$campos["primaria"]}',
				estado= '{$campos["estado"]}', tipopago= '{$campos["tipopago"]}', categoria= '{$campos["categoria"]}', claseafiliacion= {$campos["claseafiliacion"]} 
			  where idformulario = {$campos["idformulario"]}";
		return mssql_query($sql,$this->con->conect);
	}
}

		
function ficha($idp,$ide=0){
	if($this->con->conectar()==true){
		$sql="SELECT idformulario,fechaingreso,horasdia,horasmes,salario,c.concepto as cargo, c.detalledefinicion as nomcargo,agricola,primaria,aportes016.estado,categoria, fecharetiro, motivoretiro,fechanovedad, aportes091.detalledefinicion as tipof, tipoformulario, afi.detalledefinicion as clasef, idpersona, vendedor,  razonsocial, aportes048.nit,aportes048.idempresa,aportes048.razonsocial, datediff (day, fechaingreso,getdate()) as dias_tray, idradicacion, isnull(aportes016.flag,0) flag FROM aportes016 INNER JOIN aportes048 ON aportes016.idempresa =  aportes048.idempresa LEFT JOIN aportes091 c on aportes016.cargo=c.iddetalledef LEFT JOIN aportes091 ON aportes016.tipoformulario = aportes091.iddetalledef LEFT JOIN aportes091 AS afi ON aportes016.tipoafiliacion = afi.iddetalledef WHERE aportes016.idpersona=$idp ORDER BY primaria desc";
		return mssql_query($sql,$this->con->conect);
	}
}

/** Trae completa todas las Afiliaciones Activas o Pendientes de un afiliado **/
function buscarFichaAfiliacionesActivasAfiliado($idp){
	if($this->con->conectar()==true){
		$sql = "SELECT isnull(a16.idradicacion,0) idradicacion,isnull(CAST(a4.fecharadicacion AS VARCHAR),'N/A') fecharadicacion,isnull(a4.usuario,'') usuarioRadica,isnull(CAST(a4.fechaproceso AS VARCHAR),'N/A') fechaproceso,isnull(a4.recibegrabacion,'') usuarioGraba,a48.idempresa,a48.nit,a48.razonsocial, a16.fechaingreso,
					CASE WHEN a16.tipoformulario=48 THEN 'Subsidio' ELSE 'Servicios' END tipoformulario, c91.detalledefinicion as clasef, a16.horasmes,a16.salario,a91.concepto as cargo,a91.detalledefinicion as nomcargo,a16.categoria,a16.primaria,a16.agricola,datediff(day,a16.fechaingreso, getdate()) diasLab,
						
					CASE WHEN a16.estado='P' THEN (
						isnull((SELECT TOP 1 'PU' FROM aportes010 b10 WHERE b10.idtrabajador=a16.idpersona AND b10.idempresa=a16.idempresa AND b10.fechasistema=a16.fechasistema
							AND (a16.idradicacion IS NULL OR 0=isnull((  SELECT count(*) cuenta FROM aportes004 b4 WHERE b4.idradicacion=a16.idradicacion AND b4.numero=b10.identificacion AND b4.nit=b10.nit AND (b4.procesado='N' OR b4.procesado IS NULL) AND (b4.idtiporadicacion=28 or b4.idtiporadicacion=2926 or b4.idtiporadicacion=29)), 0))), 'P')
					) ELSE a16.estado END estado,
					isnull(a16.flag,0) flag
				FROM aportes016 a16 LEFT JOIN aportes004 a4 ON a16.idradicacion=a4.idradicacion INNER JOIN aportes048 a48 ON a16.idempresa=a48.idempresa LEFT JOIN aportes091 a91 ON a16.cargo=a91.iddetalledef LEFT JOIN aportes091 b91 ON a16.tipoformulario=b91.iddetalledef
				LEFT JOIN aportes091 c91 ON a16.tipoafiliacion=c91.iddetalledef WHERE a16.idpersona=$idp ORDER BY a16.primaria DESC";
		return mssql_query($sql,$this->con->conect);
	}
}

function buscarObservacionesRadicacion($idp){
	if($this->con->conectar()==true){
		$sql = "SELECT a4.idradicacion, a4.fecharadicacion, a91.detalledefinicion, a4.usuario, isnull(a4.notas, '') observaciones
				FROM aportes004 a4
				INNER JOIN aportes091 a91 ON a91.iddetalledef=a4.idtiporadicacion
				INNER JOIN aportes015 a15
					ON a15.idpersona=$idp 
						AND ( 
								( a4.idtiporadicacion IN (27, 30, 211) AND ( rtrim(ltrim(a4.identificacion)) = a15.identificacion AND a4.idtipodocumento=a15.idtipodocumento ) )
								OR 
								( a4.idtiporadicacion NOT IN (27, 30, 211) AND ( rtrim(ltrim(a4.numero)) = a15.identificacion AND a4.idtipodocumentoafiliado=a15.idtipodocumento ) )
							)
				ORDER BY a4.idradicacion DESC";
		return mssql_query($sql,$this->con->conect);
	}
}

function obtener_afiliaciones_activaseinactivas($idPersona){
	if($this->con->conectar()==true){
		$sql = "SELECT idformulario,fechaingreso,horasdia,horasmes,salario,c.concepto as cargo,agricola,primaria,aportes016.estado,categoria, fecharetiro, motivoretiro,fechanovedad, aportes091.detalledefinicion as tipof, tipoformulario, afi.detalledefinicion as clasef, razonsocial, aportes048.nit,aportes048.razonsocial, datediff (day, fechaingreso,getdate()) as dias_tray FROM aportes016 INNER JOIN aportes048 ON aportes016.idempresa =  aportes048.idempresa LEFT JOIN aportes091 c on aportes016.cargo=c.iddetalledef LEFT JOIN aportes091 ON aportes016.tipoformulario = aportes091.iddetalledef LEFT JOIN aportes091 AS afi ON aportes016.tipoafiliacion = afi.iddetalledef WHERE aportes016.idpersona=$idPersona
		UNION
		SELECT idformulario,fechaingreso,horasdia,horasmes,salario,c.concepto as cargo,agricola,primaria,aportes017.estado,categoria, fecharetiro, motivoretiro,fechanovedad, aportes091.detalledefinicion as tipof, tipoformulario, afi.detalledefinicion as clasef, razonsocial, aportes048.nit,aportes048.razonsocial, datediff (day, fechaingreso,getdate()) as dias_tray FROM aportes017 INNER JOIN aportes048 ON aportes017.idempresa =  aportes048.idempresa LEFT JOIN aportes091 c on aportes017.cargo=c.iddetalledef LEFT JOIN aportes091 ON aportes017.tipoformulario = aportes091.iddetalledef LEFT JOIN aportes091 AS afi ON aportes017.tipoafiliacion = afi.iddetalledef WHERE aportes017.idpersona=$idPersona
		order by idformulario desc";
		return mssql_query($sql,$this->con->conect);
	}
}

function afiliacion_idafiliacion($ida){
	if($this->con->conectar()==true){
			$sql="Select * from aportes016 where idformulario=$ida";
		//echo $sql;
			return mssql_query($sql,$this->con->conect);
	}
}

function afiliacion_idformulario_idpersona($ida,$idp){
	if($this->con->conectar()==true){
		$sql="Select * from aportes016 where idformulario=$ida and idpersona=$idp";
		//echo $sql;
		return mssql_query($sql,$this->con->conect);
	}
}

function afiliacion_idformulario_idpersona_2($ida,$idp){
	if($this->con->conectar()==true){
		$sql="Select * from aportes017 where idformulario=$ida and idpersona=$idp";
		//echo $sql;
		return mssql_query($sql,$this->con->conect);
	}
}


function afiliacion_idafiliacion2($ida){
	if($this->con->conectar()==true){
		$sql="Select * from aportes017 where idformulario=$ida";
		//echo $sql;
		return mssql_query($sql,$this->con->conect);
	}
}
	
function estado_afiliacion($c1,$c2,$c3){
	if($this->con->conectar()==true){
			$sql="EXECUTE PROCEDURE inactivar_afiliacion('$c1','$c2',$c3)";
		//echo $sql;
			return mssql_query($sql,$this->con->conect);
		}
	}	
	
function afiliacion_simple_a($idp){
	if($this->con->conectar()==true){
			$sql="SELECT fechaingreso,horasmes,salario,primaria,aportes091.detalledefinicion as tipof, afi.detalledefinicion as clasef FROM aportes016 
LEFT JOIN aportes091 ON aportes016.tipoformulario = aportes091.iddetalledef LEFT JOIN aportes091 AS afi ON aportes016.tipoafiliacion = afi.iddetalledef WHERE aportes016.idpersona=$idp and estado='A'";
		//echo $sql;
			return mssql_query($sql,$this->con->conect);
		}
	}	
	
function buscar_historico($idp){
	if($this->con->conectar()==true){
			$sql="SELECT aportes048.nit, razonsocial, aportes048.telefono, fechaingreso,fecharetiro, categoria, dateDiff(day, aportes017.fechaingreso, aportes017.fecharetiro) as dias, salario,horasmes,semanas,motivoretiro,defmotivo.detalledefinicion as definicionmotivo, aportes091.detalledefinicion as tipof, afi.detalledefinicion as clasef,aportes017.idradicacion, aportes017.estado, aportes017.fechafidelidad, aportes017.porplanilla FROM aportes017 INNER JOIN aportes048 ON  aportes017.idempresa=aportes048.idempresa LEFT JOIN aportes091 ON aportes017.tipoformulario = aportes091.iddetalledef LEFT JOIN aportes091 AS defmotivo ON aportes017.motivoretiro = defmotivo.iddetalledef LEFT JOIN aportes091 AS afi ON aportes017.tipoafiliacion = afi.iddetalledef WHERE idpersona='$idp' ORDER BY fechaingreso desc";
		//echo $sql;
			return mssql_query($sql,$this->con->conect);
		}
	}
	
	/** Trae completa todas las Afiliaciones Activas o Pendientes de un afiliado **/
	function buscarFichaAfiliacionesInactivasAfiliado($idp){
		if($this->con->conectar()==true){		
			$sql = "SELECT isnull(CAST(a4.fecharadicacion AS VARCHAR),'N/A') fecharadicacion,isnull(a4.usuario,'') usuarioRadica,isnull(CAST(a4.fechaproceso AS VARCHAR),'N/A') fechaproceso,isnull(a4.recibegrabacion,'') usuarioGraba,a48.nit,a48.razonsocial, a17.fechaingreso,a17.fecharetiro,
			CASE WHEN a17.tipoformulario=48 THEN 'Subsidio' ELSE 'Servicios' END tipoformulario,c91.detalledefinicion as clasef, a17.horasmes,a17.salario,c.detalledefinicion as nomcargo,a17.categoria,a17.estado,a17.semanas,dateDiff(day, a17.fechaingreso, a17.fecharetiro) as dias,a17.motivoretiro,isnull(a17.idradicacion,0) idradicacion,
			b91.detalledefinicion as definicionmotivo,a17.fechafidelidad
			FROM aportes017 a17 
            LEFT JOIN aportes004 a4 ON a17.idradicacion=a4.idradicacion 
            INNER JOIN aportes048 a48 ON  a17.idempresa=a48.idempresa 
            LEFT JOIN aportes091 a91 ON a17.tipoformulario=a91.iddetalledef 
            LEFT JOIN aportes091 b91 ON a17.motivoretiro=b91.iddetalledef
			LEFT JOIN aportes091 c91 ON a17.tipoafiliacion = c91.iddetalledef 
            LEFT JOIN aportes091 c on a17.cargo=c.iddetalledef 
            WHERE a17.idpersona=$idp ORDER BY a17.fecharetiro DESC";
			return mssql_query($sql,$this->con->conect);
		}
	}

	/**
	 * @author Oswaldo 06-08-2011
	 * Obtiene las trayectorias de un afiliado teniendo en cuenta idpersona y el n�mero de a�os en los cuales
	 * se cuenta el tiempo de afiliaci�n seg�n FONEDE
	 * @param int $idPersona
	 * @param int $numAnos default 3
	 */
	function buscar_historico_fonede($idPersona, $numAnos = 3, $restrictivo = true){
		$fechaHoyHace3Anos = date("m") ."/". date("d") ."/". (intval(date("Y"))-$numAnos);

		if($this->con->conectar()==true){
			//segmento de la condicion para obtener s�lo las trayectorias que cumplen con el rango
			$strCondicionSegmento = "AND
									  	(
									  	 (fechaingreso <= '$fechaHoyHace3Anos' and fecharetiro >= '$fechaHoyHace3Anos') OR 
									  	 (fechaingreso <= '$fechaHoyHace3Anos' and fecharetiro is null) OR
									  	 (fechaingreso >= '$fechaHoyHace3Anos')
									  	)";
			$sql="SELECT 
					(fechaingreso <= '$fechaHoyHace3Anos' and fecharetiro >= '$fechaHoyHace3Anos') as cumple_rango_1, 
					(fechaingreso <= '$fechaHoyHace3Anos' and fecharetiro is null) as cumple_rango_2,
					(fechaingreso >= '$fechaHoyHace3Anos') as cumple_rango_3,
					razonsocial, 
					aportes048.telefono, 
					fechaingreso,
					fecharetiro, 
					(aportes017.fecharetiro-aportes017.fechaingreso) as dias, 
					salario,
					horasmes,
					semanas,
					motivoretiro, 
					aportes091.detalledefinicion as tipof, 
					afi.detalledefinicion as clasef 
				  FROM aportes017 
					INNER JOIN aportes048 ON aportes017.idempresa=aportes048.idempresa 
					LEFT JOIN aportes091 ON aportes017.tipoformulario = aportes091.iddetalledef 
					LEFT JOIN aportes091 AS afi ON aportes017.tipoafiliacion = afi.iddetalledef 
				  WHERE 
				  	idpersona=$idPersona
				  	$strCondicionSegmento
				  	ORDER BY fechaingreso asc";
			$resultado = mssql_query($sql,$this->con->conect);
			return $resultado;
		}
	}
	
	/**
	 * 
	 * Obtiene la lista de postulaciones a FONEDE seg�n el id de la persona recibido
	 * @param int $idPersona
	 * @todo La consulta debe ser ajustada cuando se haga la migraci�n de fonede generando 
	 * n�meros de radicaci�n
	 */
	function buscar_postulaciones_fonede($idPersona){
		if($this->con->conectar()==true){
			$sql="SELECT rad.idradicacion, post.idpostulacion, post.idpersona, post.fechasolicita as fecharadicacion, post.antiguedad as tiempo, post.idpersona, post.afiliado as aficaja, CASE rad.idagencia WHEN '01' THEN 'NEIVA' WHEN '02' THEN 'GARZON' WHEN '03' THEN 'PITALITO' WHEN '04' THEN 'LA PLATA' END as seccional, post.idradicacion as num_rad_rsn, post.documentos as documen, post.apto, epost.concepto as estado_postulacion, epost.codigo as codigo_estado_postulacion FROM aportes201 post LEFT JOIN aportes004 rad ON rad.idradicacion = post.idradicacion LEFT JOIN aportes091 epost ON epost.iddetalledef = post.estadopostulacion WHERE post.idpersona=$idPersona ORDER BY rad.fecharadicacion DESC";
			$resultado = mssql_query($sql,$this->con->conect);
			return $resultado;
		}
	}
	
	/**
	 * 
	 * Obtiene la lista de postulaciones a FONEDE que tienen bandera T, S, X o C
	 * @param int $idPersona
	 * @todo La consulta debe ser ajustada cuando se haga la migraci�n de fonede generando 
	 * n�meros de radicaci�n
	 */
	function buscar_postulaciones_fonede_culminadas($idPersona){
		if($this->con->conectar()==true){
			$sql="SELECT rad.idradicacion, post.idpostulacion, post.idpersona, post.fechasolicita as fecharadicacion, post.antiguedad as tiempo, post.idpersona, post.afiliado as aficaja, post.seccional, '' as num_rad_rsn, post.documentos as documen, post.apto, epost.concepto as estado_postulacion, epost.codigo as codigo_estado_postulacion FROM aportes201 post LEFT JOIN aportes004 rad ON rad.idradicacion = post.idradicacion LEFT JOIN aportes091 epost ON epost.iddetalledef = post.estadopostulacion WHERE post.idpersona=$idPersona AND post.estadopostulacion IN (SELECT iddetalledef FROM aportes091 WHERE iddefinicion=37 AND codigo IN ('X','C','T','S')) ORDER BY  rad.fecharadicacion DESC";
			$resultado = mssql_query($sql,$this->con->conect);
			return $resultado;
		}
	}
	
	/**
	 * 
	 * Obtiene la lista de postulaciones a FONEDE que tienen bandera A, G o P
	 * @param int $idPersona
	 * @todo La consulta debe ser ajustada cuando se haga la migraci�n de fonede generando 
	 * n�meros de radicaci�n v�lidos en sigas
	 */
	function buscar_postulaciones_fonede_activas($idPersona){
		if($this->con->conectar()==true){
			$sql="SELECT rad.idradicacion, post.idpostulacion, post.idpersona, post.fechasolicita as fecharadicacion, post.antiguedad as tiempo, post.idpersona, post.afiliado as aficaja, post.seccional, '' as num_rad_rsn, post.documentos as documen, post.apto, epost.concepto as estado_postulacion, epost.codigo as codigo_estado_postulacion FROM aportes201 post LEFT JOIN aportes004 rad ON rad.idradicacion = post.idradicacion LEFT JOIN aportes091 epost ON epost.iddetalledef = post.estadopostulacion WHERE post.idpersona=$idPersona AND post.estadopostulacion IN (SELECT iddetalledef FROM aportes091 WHERE iddefinicion=37 AND codigo IN ('A','G','P')) ORDER BY rad.fecharadicacion DESC";
			$resultado = mssql_query($sql,$this->con->conect);
			return $resultado;
		}
	}
	
		/**
		 * 
		 * Obtiene las afiliaciones activa de la persona especificada
		 * @param int $iden id de la persona
		 */
	function buscar_afiliacion_a($iden){
		if($this->con->conectar()==true){
			$sql="select aportes016.salario,aportes016.fechaingreso, aportes048.nit, aportes048.razonsocial, aportes016.tipoformulario, aportes091.detalledefinicion FROM aportes016  INNER JOIN aportes048 ON aportes016.idempresa=aportes048.idempresa LEFT JOIN aportes091 ON aportes091.iddetalledef=aportes016.tipoformulario WHERE idpersona=$iden and aportes016.estado='A'";
		//echo $sql;
			return mssql_query($sql,$this->con->conect);
		}
	}
	
	/**
	 *
	 * Obtiene las afiliaciones activa de la persona especificada
	 * @param int $iden id de la persona
	 */
	function buscar_afiliacion_a_general($iden){
		if($this->db->conexionID==true){
			$sql="select * from aportes016 where idpersona=$iden";
			return $this->db->querySimple($sql);;
		}
	}
	
		
	function buscar_afiliacion_p_a($idp){
		if($this->con->conectar()==true){
			$sql="select aportes016.estado,aportes016.salario,aportes016.fechaingreso, aportes048.nit, aportes048.razonsocial, aportes016.tipoformulario, aportes091.detalledefinicion, isnull(datediff(dd,aportes016.fechaingreso,CAST(dateadd(mm,-2,getDate()) AS DATE)),0) diferencia FROM aportes016  INNER JOIN aportes048 ON aportes016.idempresa=aportes048.idempresa LEFT JOIN aportes091 ON aportes091.iddetalledef=aportes016.tipoformulario WHERE idpersona=$idp and aportes016.estado='A' and primaria='S'";
			return mssql_query($sql,$this->con->conect);
		}
	}

	function buscar_tipo_formulario($idp){
		if($this->con->conectar()==true){
			$sql="select idpersona,tipoformulario,estado
			from aportes016 where estado='A' and idpersona=$idp";
			return mssql_query($sql,$this->con->conect);
		}
	}
	function buscar_fechaingreso_radica($idp,$ide){
		if($this->con->conectar()==true){
			 		$sql="SELECT DISTINCT a17.fecharetiro,a48.nit,a17.idpersona
					FROM aportes017 a17
					INNER JOIN aportes048 a48 ON a48.idempresa=a17.idempresa
					WHERE a17.fecharetiro=(SELECT max(ap17.fecharetiro) 
					FROM aportes017 ap17 WHERE ap17.idpersona=a17.idpersona) 
					AND a17.idpersona=$idp AND a17.idempresa=$ide";
			return mssql_query($sql,$this->con->conect);
		}
	}
	
	function buscar_primera_afiliacion_activa($idp,$estado){
		if($this->con->conectar()==true){
			$sql="SELECT TOP 1 a.maxIngreso, a.diferencia, a.estado
					FROM (
						SELECT 
							isnull(max(a16.fechaingreso),'') maxIngreso, case when min(a16.estado) = 'P' then 'E' else 'A' end estado,
							isnull(datediff(dd,max(a16.fechaingreso),CAST(dateadd(mm,-2,getDate()) AS DATE)),0) diferencia 
						FROM aportes016 a16 WHERE a16.idpersona=$idp and a16.estado in ($estado)
						UNION
						SELECT 
							isnull(max(a16.fechaingreso),'') maxIngreso, 'I' estado,
							isnull(datediff(dd,max(a16.fechaingreso),CAST(dateadd(mm,-2,getDate()) AS DATE)),0) diferencia 
						FROM aportes017 a16 WHERE a16.idpersona=$idp
					) a
					ORDER BY a.estado";
			//echo $sql;
			return mssql_query($sql,$this->con->conect);
		}
	}
		
function contar_afiliacion_a($idp){
		if($this->con->conectar()==true){
			$sql="select count(*) as cuenta from aportes016 WHERE idpersona=$idp and estado in('A','P')";
		//echo $sql;
			return mssql_query($sql,$this->con->conect);
		}
	}
	
function contar_afiliacion_a_fechaingreso($idp,$idempresa,$fechaingreso){
		if($this->con->conectar()==true){
			$sql="select count(*) as cuenta from aportes016 WHERE idpersona='$idp' and estado in('A','P') and fechaingreso='$fechaingreso' and idempresa='$idempresa'";
			//echo $sql;
			return mssql_query($sql,$this->con->conect);
		}
	}
	
function contar_afiliacion_a_indepm($idp){
		if($this->con->conectar()==true){
			$sql="select count(*) as cuenta from aportes016 WHERE idpersona=$idp and estado in('A','P') and idempresa in (SELECT idempresa FROM aportes048 inner join aportes015 on aportes015.identificacion=aportes048.nit WHERE idpersona=$idp)";
			//echo $sql;
			return mssql_query($sql,$this->con->conect);
		}
	}

/* 
 * Busca la afiliacion pendiente con aportes, donde el nit sea igual a la identificacion de la persona, 
 * que no tenga radicacion, y solo lo tenga a este como trabajador
*/
function buscar_afiliacion_p_indepm_planilla($idp){
		if($this->con->conectar()==true){
			$sql="select top 1 a16.idempresa
				from aportes016 a16
				  inner join aportes010 a10
				    on a16.idpersona=a10.idtrabajador and a16.idempresa=a10.idempresa and a10.identificacion=a10.nit
				where a16.idpersona=$idp and a16.estado='P' and a10.idplanilla=(select max(b10.idplanilla) from aportes010 b10 where b10.idtrabajador=a10.idtrabajador and b10.idempresa=a10.idempresa)
				    and 0=isnull((  SELECT count(*) cuenta FROM aportes004 
				                    WHERE procesado='N' AND numero=a10.identificacion 
				                    AND nit=a10.nit AND (idtiporadicacion=28 or idtiporadicacion=2926 or idtiporadicacion=29)), 0)    
				    and 0=isnull((  SELECT count(*) cuenta FROM aportes016 b16
				                    WHERE b16.idempresa=a16.idempresa and b16.idpersona<>a16.idpersona) , 0)";
			//echo $sql;
			return mssql_query($sql,$this->con->conect);
		}
	}
		
function contar_afiliacion_p($idp){
		if($this->con->conectar()==true){
			$sql="select count(*) as cuenta from aportes016 WHERE idpersona=$idp and primaria='S'";
		//echo $sql;
			return mssql_query($sql,$this->con->conect);
		}
	}
		
function contar_afiliacion_i($idp){
		if($this->con->conectar()==true){
			$sql="select count(*) as cuenta from aportes017 WHERE idpersona=$idp";
		//echo $sql;
			return mssql_query($sql,$this->con->conect);
		}
	}
		
function afiliacion_activa_2p($idtd,$numero){
		if($this->con->conectar()==true){
			$sql="select idformulario,papellido,sapellido,pnombre,snombre from aportes015 inner join aportes016 on aportes015.idpersona=aportes016.idpersona
where idtipodocumento=$idtd and aportes015.identificacion='$numero' and aportes016.estado='A'";
		//echo $sql;
			return mssql_query($sql,$this->con->conect);
		}
}	

function buscar_afiliacion($idp){
	if($this->con->conectar()==true){
		$sql="SELECT fechaingreso,horasdia,horasmes,salario,agricola,primaria,aportes016.estado,aportes016.idempresa, fecharetiro, motivoretiro,fechanovedad,pnombre,snombre,papellido,sapellido,razonsocial, aportes091.detalledefinicion as tipof, afi.detalledefinicion as clasef FROM aportes016 INNER JOIN aportes015 ON aportes016.idpersona = aportes015.idpersona INNER JOIN aportes048 ON aportes016.idempresa = aportes048.idempresa LEFT JOIN aportes091 ON aportes016.tipoformulario = aportes091.iddetalledef LEFT JOIN aportes091 AS afi ON aportes016.tipoafiliacion = afi.iddetalledef WHERE aportes016.idpersona=$idp";
		return mssql_query($sql,$this->con->conect);
		}
	}

function buscar_afiliacion_CC($id){
		if($this->con->conectar()==true){
			$sql="SELECT idformulario,aportes016.idpersona,identificacion,papellido,sapellido,pnombre,snombre FROM aportes016 INNER JOIN aportes015 ON aportes016.idpersona=aportes015.idpersona WHERE identificacion='$id'";
			//echo $sql;
         return mssql_query($sql,$this->con->conect);
		}
	}
	
function borrar_afiliacion($idf){
		if($this->con->conectar()==true){
			$sql="delete from aportes016 where idformulario=$idf";
			//echo $sql;
         return mssql_query($sql,$this->con->conect);
		}
	}
		
function buscar_afiliacion_CCT($idtipo,$numero){
		if($this->con->conectar()==true){
			$sql="SELECT idformulario,aportes016.idpersona,identificacion,papellido,sapellido,pnombre,snombre FROM aportes016 
INNER JOIN aportes015 ON aportes016.idpersona=aportes015.idpersona 
WHERE identificacion='$numero' AND aportes015.idtipodocumento='$idtipo'";
			//echo $sql;
            return mssql_query($sql,$this->con->conect);
		}
	}	
        
function  insert_afiliacion($campos){
 if($this->con->conectar()==true){
     $sql="insert into aportes016 (tipoformulario,tipoafiliacion,idempresa, idpersona,fechaingreso,horasdia,horasmes,salario,agricola, cargo,categoria,traslado,codigocaja,tipopago,usuario,primaria,estado,fechasistema,auditado,idagencia,idradicacion)
         values (".$campos[0].",'".$campos[1]."','".$campos[2]."','".$campos[3]."','".$campos[4]."','".$campos[5]."','".$campos[6]."','".$campos[7]."','".$campos[8]."','".$campos[9]."','".$campos[10]."','".$campos[11]."','".$campos[12]."','".$campos[13]."','".$campos[14]."','".$campos[18]."','".$campos[17]."',cast(getdate() as date),'N','".$campos[15]."',".$campos[16].")";
     return mssql_query($sql,$this->con->conect);
 }   
}

function buscar_afiliacion_cc_a($campo0,$campo1){
	if($this->con->conectar()==true){
		$sql="select aportes015.idpersona, aportes015.idtipodocumento, aportes015.identificacion, aportes015.papellido, aportes015.sapellido, aportes015.pnombre, aportes015.snombre, aportes015.nombrecorto,aportes016.categoria, aportes016.fechaingreso, aportes016.salario, aportes048.nit, aportes048.razonsocial  from aportes016 inner join aportes015 on aportes016.idpersona = aportes015.idpersona inner join aportes048 on aportes016.idempresa = aportes048.idempresa where identificacion='$campo0' and aportes015.idtipodocumento='$campo1' and aportes016.estado='A'";
		//echo $sql;
		return mssql_query($sql,$this->con->conect);
		}
	}
	
	function buscar_afiliacion_cc_a2($campo0,$campo1){
		if($this->con->conectar()==true){
			$sql="SELECT a15.idpersona, a15.idtipodocumento, a15.identificacion, a15.papellido, a15.sapellido, a15.pnombre, a15.snombre, a15.nombrecorto, isnull(a16.idformulario,0) idformulario, a16.categoria, a16.fechaingreso, a16.salario, isnull(a48.idempresa,0) idempresa, a48.nit, a48.razonsocial 
				  FROM aportes015 a15 LEFT JOIN aportes016 a16  ON a16.idpersona=a15.idpersona AND a16.estado='A' LEFT JOIN aportes048 a48 ON a48.idempresa=a16.idempresa WHERE a15.identificacion='$campo0' and a15.idtipodocumento='$campo1'";
			//echo $sql;
			return mssql_query($sql,$this->con->conect);
		}
	}

function buscar_afiliacion_primaria($idp){
	if($this->con->conectar()==true){
		$sql="select * from aportes016 where primaria='S' and idpersona=$idp";
		//echo $sql;
		return mssql_query($sql,$this->con->conect);
		}
	}
	
function buscar_afiliaciones($ide){
    if($this->con->conectar()==true){
        $sql="select aportes016.idformulario, aportes016.idpersona, aportes016.fechaingreso, aportes016.salario, aportes015.idtipodocumento, aportes015.identificacion,
	aportes015.papellido, aportes015.sapellido, aportes015.pnombre, aportes015.snombre,aportes091.codigo from aportes016 inner join aportes015 on aportes016.idpersona = aportes015.idpersona
	INNER JOIN aportes091 ON aportes015.idtipodocumento=aportes091.iddetalledef where aportes016.idempresa=$ide and aportes016.estado='A'";  
    return mssql_query($sql,$this->con->conect);
    }
}

function buscar_afiliaciones_inactivasxempresa($ide){
	if($this->con->conectar()==true){
		$sql="SELECT aportes017.idformulario,
				     aportes017.idpersona,
		             aportes017.fechaingreso,
		             aportes017.salario,
		             aportes015.idtipodocumento,
		             aportes015.identificacion,
		             aportes015.papellido, 
		             aportes015.sapellido, 
		             aportes015.pnombre, 
		             aportes015.snombre,
		             aportes091.codigo
		        FROM aportes017
		            INNER JOIN aportes015 ON aportes017.idpersona = aportes015.idpersona
		            INNER JOIN aportes091 ON aportes015.idtipodocumento=aportes091.iddetalledef
		        WHERE aportes017.idempresa = '$ide'";
		return mssql_query($sql,$this->con->conect);
	}
}

	/**
	 * Obtiene todos los datos de una afiliaci�n seg�n el id del afiliado
	 * especificado
	 *
	 * @param int $idPersona
	 * @return ifx query result � false
	 */
	function buscar_datos_afiliacion_por_idpersona($idPersona){
		if($this->con->conectar()==true){
			$query = "select * from aportes016 where idpersona = ".$idPersona." AND primaria='S'";
			return mssql_query($query,$this->con->conect);
		}
		return false;
	}
	function buscar_idpersona_afiliacion_por_idempresa($idempresa){
		if($this->con->conectar()==true){
			$query = "select idpersona from aportes016 where idempresa = '$idempresa' and estado = 'A'";
			return mssql_query($query,$this->con->conect);
		}
		return false;
	}
	
	function buscar_afiliacion_inactiva_idp($idPersona){
		if($this->con->conectar()==true){
			$query = "select * from aportes017 where idpersona = ".$idPersona;
			return mssql_query($query,$this->con->conect);
		}
		return false;
	}
	
	
	function buscar_afiliacion_inactiva_por_idformulario($idFormulario){
		if($this->con->conectar()==true){
			$query = "select * from aportes017 where idformulario = ". $idFormulario;
			return mssql_query($query,$this->con->conect);
		}
		return false;
	}
	
	/**
	 * Obtiene los datos de la �ltima trayectoria de un afiliado seg�n su id
	 *
	 * @param int $idPersona
	 * @return ifx query result or false
	 */
	function obtener_ultima_trayectoria_por_idpersona($idPersona){
		if($this->con->conectar()==true){
			$query = "select top 1 
				b.nit, 
				b.razonsocial, 
				b.direccion, 
				b.telefono 
			  from 
			  	aportes017 a, 
			  	aportes048 b 
			  where 
				a.idempresa = b.idempresa and 
				idpersona = $idPersona 
			  order by fechaingreso desc";
			return mssql_query($query,$this->con->conect);
		}
		return false;
	}
	
	//AGOSTO 2011
	function buscar_afi_primaria($idp){
	  		if($this->con->conectar()==true){
	 			$sql="select count(*) as total from aportes016 where idpersona='".$idp."' and primaria='S' group by primaria";
				return mssql_query($sql,$this->con->conect);			
			}
	}
	
	function buscar_afi_cedula($tpdoc,$ced){
		if($this->con->conectar()==true){
		$sql="SELECT * FROM aportes016 where idpersona in(SELECT idpersona FROM aportes015 where idtipodocumento='$tpdoc' and identificacion='$ced')";
		return mssql_query($sql,$this->con->conect);			
		}
	}

	function buscar_afi_idr($idr){
		if($this->con->conectar()==true){
			$sql="select * from aportes016 where idradicacion=$idr";
			return mssql_query($sql,$this->con->conect);			
		}
	}
	
	function buscar_afi_idr2($idr){
		if($this->con->conectar()==true){
			$sql="select * from aportes017 where idradicacion=$idr";
			return mssql_query($sql,$this->con->conect);
		}
	}
	
	function insert_reactivar_afiliacion($campos){
		if($this->con->conectar()==true){
			$sql="INSERT INTO aportes016 (tipoformulario,tipoafiliacion,idempresa,idpersona,fechaingreso,horasdia,horasmes,salario,agricola,cargo,primaria,estado,fecharetiro,motivoretiro,fechanovedad,semanas,fechafidelidad,estadofidelidad,traslado,codigocaja,flag,tempo1,tempo2,fechasistema,usuario,tipopago,categoria,auditado,idagencia,idradicacion,vendedor,codigosalario) values ('".$campos[0]."','".$campos[1]."','".$campos[2]."','".$campos[3]."','".$campos[4]."','".$campos[5]."','".$campos[6]."','".$campos[7]."','".$campos[8]."','".$campos[9]."','".$campos[10]."','".$campos[11]."','".$campos[12]."','".$campos[13]."','".$campos[14]."','".$campos[15]."','".$campos[16]."','".$campos[17]."','".$campos[18]."','".$campos[19]."','".$campos[20]."','".$campos[21]."','".$campos[22]."','".$campos[23]."','".$campos[24]."','".$campos[25]."','".$campos[26]."','".$campos[27]."','".$campos[28]."','".$campos[29]."','".$campos[30]."','".$campos[31]."')";
			//echo($sql);
			return mssql_query($sql,$this->con->conect);
		}
	}
	function borrar_afiliacion_inactiva($idformulario){
		if($this->con->conectar()==true){
			$sql="DELETE FROM aportes017 WHERE idformulario = '$idformulario'";
			return mssql_query($sql,$this->con->conect);
	}
}

	
	/*
	 * Buscar afiliaciones activas o pendientes de un afiliado con una empresa
	 */
	function buscar_afi_AxNit($idp,$nit){
		if($this->con->conectar()==true){
			$sql="select a16.estado, count(a16.idformulario) cant
				  from aportes016 a16
  					inner join aportes048 a48
    				  on a16.idempresa=a48.idempresa
				  where a16.idpersona=$idp and a48.nit='".$nit."' and a16.estado in ('A','P')
				  group by a16.estado";
			return mssql_query($sql,$this->con->conect);
		}		
	}
	
	/*
	 * Buscar cantidad de todas las afiliaciones encontradas por persona
	*/
	function buscar_total_afiliaciones($idp){
		if($this->con->conectar()==true){
			$sql="SELECT 'A' estado, COUNT(a16.idformulario) cant
			FROM aportes016 a16
			WHERE a16.idpersona=$idp
			UNION
			SELECT 'I' estado, COUNT(a17.idformulario) cant
			FROM aportes017 a17
			WHERE a17.idpersona=$idp
			UNION
			SELECT 'P' estado, COUNT(a16.idformulario) cant
			FROM aportes016 a16
			INNER JOIN aportes010 a10
			ON a10.idempresa=a16.idempresa AND a10.idtrabajador=a16.idpersona
			WHERE a16.idpersona=$idp AND a16.estado='P' AND a10.idplanilla=(SELECT MAX(b10.idplanilla) FROM aportes010 b10 WHERE b10.idtrabajador=a10.idtrabajador AND b10.idempresa=a10.idempresa)
			AND 0=isnull((  SELECT count(*) cuenta FROM aportes004
			WHERE procesado='N' AND numero=a10.identificacion
			AND nit=a10.nit AND (idtiporadicacion=28 or idtiporadicacion=2926 or idtiporadicacion=29)), 0)";
			return mssql_query($sql,$this->con->conect);
		}
	}	
	
	/*
	 * Busca afiliaciones pendientes por planilla unica por idpersona y nit de la empresa, y tiene mas afiliaciones
	*/
	function buscar_afiliacion_p_planillaUnica($idp,$nit){
		if($this->con->conectar()==true){
			$sql="select count(a16.idformulario) cant
			from aportes016 a16
			inner join aportes010 a10
			on a16.idpersona=a10.idtrabajador and a16.idempresa=a10.idempresa and a10.nit='".$nit."'
			where a16.idpersona=$idp and a16.estado='P' and a10.idplanilla=(select max(b10.idplanilla) from aportes010 b10 where b10.idtrabajador=a10.idtrabajador and b10.idempresa=a10.idempresa)
			and 0=isnull((  SELECT count(*) cuenta FROM aportes004
			WHERE procesado='N' AND numero=a10.identificacion
			AND nit=a10.nit AND (idtiporadicacion=28 or idtiporadicacion=2926 or idtiporadicacion=29)), 0)
			and 0=isnull((  SELECT count(*) cuenta FROM aportes016 b16
			WHERE b16.idempresa=a16.idempresa and b16.idpersona<>a16.idpersona) , 0)";
			//echo $sql;
			return mssql_query($sql,$this->con->conect);
		}
	}
	
	/*
	 * Busca afiliaciones pendientes por planilla unica por idpersona y nit de la empresa
	*/
	function buscar_afiliacion_p_planillaUnica2($idp,$nit){
		if($this->con->conectar()==true){
			$sql="select count(a16.idformulario) cant
			from aportes016 a16
			inner join aportes010 a10
			on a16.idpersona=a10.idtrabajador and a16.idempresa=a10.idempresa and a10.nit='".$nit."'
			where a16.idpersona=$idp and a16.estado='P' and a10.idplanilla=(select max(b10.idplanilla) from aportes010 b10 where b10.idtrabajador=a10.idtrabajador and b10.idempresa=a10.idempresa)
			and 0=isnull((  SELECT count(*) cuenta FROM aportes004
			WHERE procesado='N' AND numero=a10.identificacion
			AND nit=a10.nit AND (idtiporadicacion=28 or idtiporadicacion=2926 or idtiporadicacion=29)), 0)";
			//echo $sql;
			return mssql_query($sql,$this->con->conect);
		}
	}
	
	
	/*
	 * Verifica retornando un numero superior a 0 si de la afiliacion activa con la empresa es primaria
	*/
	function contar_si_afiliacion_primaria($idp,$ide){
		if($this->con->conectar()==true){
			$sql="select count(a16.idformulario) cant
			from aportes016 a16
			WHERE a16.idpersona=$idp AND a16.primaria='S' AND a16.idempresa<>$ide --AND a16.estado='P' AND a16.idradicacion IS NULL";
			//echo $sql;
			return mssql_query($sql,$this->con->conect);
		}
	}
	
	/*
	 * Verifica retornando un numero superior a 0 si de la afiliacion activa con la empresa es primaria
	*/
	function buscar_Empresa_Inactiva_sinTrab($idp){
		if($this->con->conectar()==true){
			$sql="SELECT a48.idempresa
				  FROM aportes048 a48
  					INNER JOIN aportes015 a15
    				  ON a48.nit=a15.identificacion
				  WHERE a48.estado='I' AND a15.idpersona=".$idp."
    				and 0=isnull((  SELECT count(*) cuenta FROM aportes016 b16
                    				WHERE b16.idempresa=a48.idempresa) , 0)";			
			return mssql_query($sql,$this->con->conect);
		}
	}
	
	/*
	 * Verificar si existe afiliacion realizada por radicacion 
	*/
	function buscar_Afiliacion_Radicacion($idr){
		if($this->con->conectar()==true){
			$sql="select count(a16.idformulario) cant
			from aportes016 a16
			WHERE a16.idradicacion=$idr";
			return mssql_query($sql,$this->con->conect);
		}
	}
	
	/*
	 * Contar formularios por auditar
	*/
	function Count_Afil_Auditar($str){
		if($this->con->conectar()==true){
			$sql="SELECT count(*) cant FROM aportes015 INNER JOIN aportes016 ON aportes015.idpersona=aportes016.idpersona INNER JOIN aportes091 td ON aportes015.idtipodocumento=td.iddetalledef INNER JOIN aportes048 ON aportes016.idempresa=aportes048.idempresa WHERE aportes016.auditado='N' AND aportes016.estado='A' AND idagencia IS NOT NULL $str";
			return mssql_query($sql,$this->con->conect);			
		}
	}
	
	/*
	 * Traer Afilicaciones por auditar
	*/
	function Select_Afil_Auditar($str, $campo_de_inicio, $campo_fin){
		if($this->con->conectar()==true){
			$sql="SELECT * FROM ( SELECT idradicacion,aportes015.idpersona, aportes015.idtipodocumento,td.codigo, identificacion,pnombre, snombre, papellido, sapellido, aportes015.direccion,aportes016.idagencia,aportes016.idempresa,nit,idformulario, ROW_NUMBER() OVER (ORDER BY idradicacion desc) as row FROM aportes015 INNER JOIN aportes016 ON aportes015.idpersona=aportes016.idpersona INNER JOIN aportes091 td ON aportes015.idtipodocumento=td.iddetalledef INNER JOIN aportes048 ON aportes016.idempresa=aportes048.idempresa WHERE aportes016.auditado='N' AND aportes016.estado='A' AND idagencia IS NOT NULL $str) a WHERE row > $campo_de_inicio and row <= $campo_fin";
			return mssql_query($sql,$this->con->conect);
		}
	}
	
	/*
	 * Contar documentos por auditar
	*/
	function Count_Docs_Auditar($str){
		if($this->con->conectar()==true){
			$sql="SELECT count(*) cant FROM aportes004 a4 INNER JOIN aportes015 a15 ON a15.idtipodocumento=a4.idtipodocumentoafiliado AND a15.identificacion=a4.numero INNER JOIN aportes091 a91 ON a91.iddetalledef=a15.idtipodocumento INNER JOIN aportes091 b91 ON b91.iddetalledef=a4.idtiporadicacion
				  WHERE a4.fechasistema>='2013-08-01' AND a4.idtiporadicacion IN (32, 70, 191, 211 ) AND a4.usuarioaudita IS NULL $str";
			//echo $sql;
			return mssql_query($sql,$this->con->conect);
		}
	}
	
	/*
	 * Traer Documentos por auditar
	*/
	function Select_Docs_Auditar( $str, $campo_de_inicio, $campo_fin ){
		if($this->con->conectar()==true){
			$sql="SELECT * FROM ( 
					SELECT a4.idradicacion, a15.idpersona, a15.idtipodocumento, a91.codigo, a15.identificacion, a15.pnombre, a15.snombre, a15.papellido, a15.sapellido, a4.idtiporadicacion, b91.detalledefinicion AS tiporadicacion, a4.idagencia, isnull(a4.idbeneficiario,0 ) idbeneficiario, ROW_NUMBER() OVER (ORDER BY a4.idradicacion desc) as row 
					FROM aportes004 a4 INNER JOIN aportes015 a15 ON a15.idtipodocumento=a4.idtipodocumentoafiliado AND a15.identificacion=a4.numero INNER JOIN aportes091 a91 ON a91.iddetalledef=a15.idtipodocumento INNER JOIN aportes091 b91 ON b91.iddetalledef=a4.idtiporadicacion
					WHERE a4.fechasistema>='2013-08-01' AND a4.idtiporadicacion IN (32, 70, 191, 211 ) AND a4.usuarioaudita IS NULL $str
				  ) a WHERE row > $campo_de_inicio and row <= $campo_fin";
			//echo $sql;
			return mssql_query($sql,$this->con->conect);
		}
	}
	
	/**
	 * Metodo: Inactiva los beneficiarios mayores o iguales a 19 a�os
	 * 			de acuerdo a la fecha del sistema
	 *
	 * @return {int} ( 	0 => No se pudo realizar el procedimiento,
	 * 					1 => El procedimiento se ejecuto correctamente)
	 */
	function inactivar_beneficiarios(){
		$sql = "SELECT DISTINCT a21.idbeneficiario
				FROM aportes015 a15
					INNER JOIN aportes021 a21 ON a21.idbeneficiario=a15.idpersona
				WHERE
					(
						DATEDIFF(year,fechanacimiento,getdate())
						+
						CASE
							WHEN
								(Month(getdate()) < Month(fechanacimiento)
								OR (Month(getdate()) = Month(fechanacimiento)
								AND	Day(getdate()) < day(fechanacimiento)))
							THEN -1
						ELSE 0
						END
					)>=19
				AND a21.idparentesco in(35,38,37)
				AND a21.estado='A'
				AND a15.capacidadtrabajo <>'I'
UNION
SELECT DISTINCT a21.idbeneficiario
				FROM aportes015 a15
					INNER JOIN aportes021 a21 ON a21.idbeneficiario=a15.idpersona
				WHERE
					(
						DATEDIFF(year,fechanacimiento,getdate())
						+
						CASE
							WHEN
								(Month(getdate()) < Month(fechanacimiento)
								OR (Month(getdate()) = Month(fechanacimiento)
								AND	Day(getdate()) < day(fechanacimiento)))
							THEN -1
						ELSE 0
						END
					)<60
				AND a21.idparentesco in(36)
				AND a21.estado='A'
				AND a15.capacidadtrabajo <>'I'";
		
		$rs = $this->db->querySimple ( $sql );
		$banIdBeneficiarios = "";
		while ( $row = $rs->fetch () ) {
			$banIdBeneficiarios .= $row ["idbeneficiario"] . ",";
		}
		$banIdBeneficiarios = trim ( $banIdBeneficiarios, ',' );
		
		// Verificar si existe id beneficiarios
		if (strlen ( $banIdBeneficiarios ) > 0) {
			$queryUpdate = "UPDATE aportes021 SET giro='N', estado='I', idmotivo=3707, fechaestado=cast(GetDate() as date) WHERE idbeneficiario IN ($banIdBeneficiarios)";
			if ($this->db->queryActualiza ( $queryUpdate ) > 0) {
				return 1;
			} else {
				return 0;
			}
		} else {
			return 0;
		}
	
	}
	
	/***********************************
	 *  SELECT
	* *********************************/
	/**
	 * METODO Encargado de obtener la mayor informacion de la afiliacion inactiva
	 * 		
	 * El parametro debe ser un array indice valor array("atributo"=>valor,...)
	 * @param unknown_type $arrAtributoValor
	 * @return multitype:NULL | array
	 */
	function busca_compl_afili_inact($arrAtributoValor){
		/*
		 * NOTA: Ha este metodo se le puede agregar mas filtro y atributos a la consulta. 
		 * 		Pero no se pueden alterar los que ya existen
		 * */
		
		$attrEntidad = array(
				array("nombre"=>"idformulario","tipo"=>"NUMBER","entidad"=>"a17")
				,array("nombre"=>"idpersona","tipo"=>"NUMBER","entidad"=>"a17")
				,array("nombre"=>"identificacion","tipo"=>"NUMBER","entidad"=>"a15")
				,array("nombre"=>"idempresa","tipo"=>"NUMBER","entidad"=>"a17"));
	
		$filtroSql = Utilitario::obtenerFiltro($attrEntidad,$arrAtributoValor);
		$sql = "SELECT 
					a17.idformulario, isnull(a17.tipoformulario,0) AS tipoformulario, isnull(a17.tipoafiliacion,0) AS tipoafiliacion, a17.idempresa, a17.idpersona, a17.fechaingreso, a17.horasdia, a17.horasmes, a17.salario, a17.agricola, isnull(a17.cargo,0) AS cargo, a17.primaria, a17.estado, a17.fecharetiro, a17.motivoretiro, a17.fechanovedad, a17.semanas, a17.fechafidelidad, a17.estadofidelidad, a17.traslado, a17.codigocaja, a17.flag, a17.tempo1, a17.tempo2, a17.fechasistema, a17.usuario, a17.tipopago, a17.categoria, a17.auditado, a17.idagencia, a17.idradicacion, a17.porplanilla, a17.madrecomunitaria, a17.claseafiliacion
					, a48.nit, a48.razonsocial
					, a15.pnombre + ' ' + isnull(a15.snombre,'') + ' ' + a15.papellido + ' ' + isnull(a15.sapellido,'') AS trabajador
					, a91.detalledefinicion AS deftipoformulario
					, b91.detalledefinicion AS deftipoafiliacion
				FROM aportes017 a17
					INNER JOIN aportes015 a15 ON a15.idpersona=a17.idpersona
					INNER JOIN aportes048 a48 ON a48.idempresa=a17.idempresa
					LEFT JOIN aportes091 a91 ON a91.iddetalledef=a17.tipoformulario
					LEFT JOIN aportes091 b91 ON b91.iddetalledef=a17.tipoafiliacion
			$filtroSql
			ORDER BY a17.fecharetiro DESC";
		
		return $this->fetchConsulta($sql);
	}
	
	/**
	 * METODO Encargado de obtener la mayor informacion de la afiliacion activa
	 *
	 * El parametro debe ser un array indice valor array("atributo"=>valor,...)
	 * @param unknown_type $arrAtributoValor
	 * @return multitype:NULL | array
	 */
	function busca_compl_afili_activ($arrAtributoValor){
		/*
		 * NOTA: Ha este metodo se le puede agregar mas filtro y atributos a la consulta.
		* 		Pero no se pueden alterar los que ya existen
		* */
	
		$attrEntidad = array(
				array("nombre"=>"idformulario","tipo"=>"NUMBER","entidad"=>"a16")
				,array("nombre"=>"idpersona","tipo"=>"NUMBER","entidad"=>"a16")
				,array("nombre"=>"idtipodocumento","tipo"=>"NUMBER","entidad"=>"a15")
				,array("nombre"=>"identificacion","tipo"=>"TEXT","entidad"=>"a15")
				,array("nombre"=>"idempresa","tipo"=>"NUMBER","entidad"=>"a16"));
	
		$filtroSql = Utilitario::obtenerFiltro($attrEntidad,$arrAtributoValor);
		$sql = "SELECT
					a16.idformulario, isnull(a16.tipoformulario,0) AS tipoformulario, isnull(a16.tipoafiliacion,0) AS tipoafiliacion, a16.idempresa, a16.idpersona, a16.fechaingreso, a16.horasdia, a16.horasmes, a16.salario, a16.agricola, isnull(a16.cargo,0) AS cargo, a16.primaria, a16.estado, a16.fecharetiro, a16.motivoretiro, a16.fechanovedad, a16.semanas, a16.fechafidelidad, a16.estadofidelidad, a16.traslado, a16.codigocaja, a16.flag, a16.tempo1, a16.tempo2, a16.fechasistema, a16.usuario, a16.tipopago, a16.categoria, a16.auditado, a16.idagencia, a16.idradicacion, a16.porplanilla, a16.madrecomunitaria, a16.claseafiliacion
					, a48.nit, a48.razonsocial
					, a15.identificacion, a15.idtipodocumento, a15.pnombre + ' ' + isnull(a15.snombre,'') + ' ' + a15.papellido + ' ' + isnull(a15.sapellido,'') AS trabajador
					, a91.detalledefinicion AS deftipoformulario
					, b91.detalledefinicion AS deftipoafiliacion
				FROM aportes016 a16
					INNER JOIN aportes015 a15 ON a15.idpersona=a16.idpersona
					INNER JOIN aportes048 a48 ON a48.idempresa=a16.idempresa
					LEFT JOIN aportes091 a91 ON a91.iddetalledef=a16.tipoformulario
					LEFT JOIN aportes091 b91 ON b91.iddetalledef=a16.tipoafiliacion
				$filtroSql
				ORDER BY a16.fechaingreso";
	
		return $this->fetchConsulta($sql);
	}
	
	/**
	* METODO Encargado de obtener la mayor informacion de la afiliacion activa e inactiva
	*
	* El parametro debe ser un array indice valor array("atributo"=>valor,...)
	* @param unknown_type $arrAtributoValor
	* @return multitype:NULL | array
	*/
	function busca_compl_afili_activ_inact($arrAtributoValor){
		/*
		 * NOTA: Ha este metodo se le puede agregar mas filtro y atributos a la consulta.
		* 		Pero no se pueden alterar los que ya existen
		* */
	
		$attrEntidad = array(
				array("nombre"=>"idformulario","tipo"=>"NUMBER","entidad"=>"a16")
				,array("nombre"=>"idpersona","tipo"=>"NUMBER","entidad"=>"a16")
				,array("nombre"=>"idtipodocumento","tipo"=>"NUMBER","entidad"=>"a15")
				,array("nombre"=>"identificacion","tipo"=>"TEXT","entidad"=>"a15")
				,array("nombre"=>"idempresa","tipo"=>"NUMBER","entidad"=>"a16"));
	
		$filtroSql = Utilitario::obtenerFiltro($attrEntidad,$arrAtributoValor);
		$sql = "SELECT
					a16.idformulario, isnull(a16.tipoformulario,0) AS tipoformulario, isnull(a16.tipoafiliacion,0) AS tipoafiliacion, a16.idempresa, a16.idpersona, a16.fechaingreso, a16.horasdia, a16.horasmes, a16.salario, a16.agricola, isnull(a16.cargo,0) AS cargo, a16.primaria, a16.estado, a16.fecharetiro, a16.motivoretiro, a16.fechanovedad, a16.semanas, a16.fechafidelidad, a16.estadofidelidad, a16.traslado, a16.codigocaja, a16.flag, a16.tempo1, a16.tempo2, a16.fechasistema, a16.usuario, a16.tipopago, a16.categoria, a16.auditado, a16.idagencia, a16.idradicacion, a16.porplanilla, a16.madrecomunitaria, a16.claseafiliacion
					, a48.nit, a48.razonsocial
					, a15.identificacion, a15.idtipodocumento, a15.pnombre + ' ' + isnull(a15.snombre,'') + ' ' + a15.papellido + ' ' + isnull(a15.sapellido,'') AS trabajador
					, a91.detalledefinicion AS deftipoformulario
					, b91.detalledefinicion AS deftipoafiliacion, 1 AS bandera_orden
				FROM aportes016 a16
					INNER JOIN aportes015 a15 ON a15.idpersona=a16.idpersona
					INNER JOIN aportes048 a48 ON a48.idempresa=a16.idempresa
					LEFT JOIN aportes091 a91 ON a91.iddetalledef=a16.tipoformulario
					LEFT JOIN aportes091 b91 ON b91.iddetalledef=a16.tipoafiliacion
				$filtroSql 
				UNION 
				SELECT
					a16.idformulario, isnull(a16.tipoformulario,0) AS tipoformulario, isnull(a16.tipoafiliacion,0) AS tipoafiliacion, a16.idempresa, a16.idpersona, a16.fechaingreso, a16.horasdia, a16.horasmes, a16.salario, a16.agricola, isnull(a16.cargo,0) AS cargo, a16.primaria, a16.estado, a16.fecharetiro, a16.motivoretiro, a16.fechanovedad, a16.semanas, a16.fechafidelidad, a16.estadofidelidad, a16.traslado, a16.codigocaja, a16.flag, a16.tempo1, a16.tempo2, a16.fechasistema, a16.usuario, a16.tipopago, a16.categoria, a16.auditado, a16.idagencia, a16.idradicacion, a16.porplanilla, a16.madrecomunitaria, a16.claseafiliacion
					, a48.nit, a48.razonsocial
					, a15.identificacion, a15.idtipodocumento, a15.pnombre + ' ' + isnull(a15.snombre,'') + ' ' + a15.papellido + ' ' + isnull(a15.sapellido,'') AS trabajador
					, a91.detalledefinicion AS deftipoformulario
					, b91.detalledefinicion AS deftipoafiliacion, 2 AS bandera_orden
				FROM aportes017 a16
					INNER JOIN aportes015 a15 ON a15.idpersona=a16.idpersona
					INNER JOIN aportes048 a48 ON a48.idempresa=a16.idempresa
					LEFT JOIN aportes091 a91 ON a91.iddetalledef=a16.tipoformulario
					LEFT JOIN aportes091 b91 ON b91.iddetalledef=a16.tipoafiliacion
				$filtroSql
				ORDER BY a16.fecharetiro DESC, bandera_orden";
	
		return $this->fetchConsulta($sql);
	}
	
	/**
	 * Retorna los valores obtenidos en la consulta
	 * @return multitype:array
	 */
	private function fetchConsulta($querySql){
		$resultado = array();
		$rs = $this->db->querySimple($querySql);
		while($row = $rs->fetch())
			$resultado[] = array_map("trim",$row);
			//$resultado[] = array_map("utf8_encode",$row);
		return $resultado;
	}
	
/** PROCEDURES **/
	/**
	 * Ejecuta el procedimiento almacenado que realiza la actualizacion de la categoria de la afiliacion
	 * @param unknown_type $idPersona
	 * @return {int} ( 	0 => No se pudo realizar el procedimiento,
	 * 					1 => El procedimiento se ejecuto correctamente)
	 */
	function spProcesoIndepPensCategorias($idPersona){
		/*echo "IDPERSONA=".$idPersona;
		exit();*/
		$resultado = 0;
		$sentencia = $this->db->conexionID->prepare ( "EXEC [dbo].[sp_Proceso_Indep_Pens_Categorias]
				@idpersona = $idPersona
				, @resultado = :resultado" );
		$sentencia->bindParam ( ":resultado", $resultado, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000 );
		$sentencia->execute ();
		if ($resultado > 0)
			return 1;
		else
			return 0;
	}
	
	/**
	 * Ejecuta el procedimiento almacenado para inactivar las afiliacion por motivo
	 * de la desafiliacion de la empresa
	 * @param unknown_type $idEmpresa
	 * @return {int} ( 	0 => No se pudo realizar el procedimiento,
	 * 					1 => El procedimiento se ejecuto correctamente)
	 */
	function spProcesoInactAfiliExpul($idEmpresa,$usuario){
		$resultado = 0;
		$sentencia = $this->db->conexionID->prepare ( "EXEC [kardex].[sp_000_Inactivar_Afiliacion_Expul]
			@id_empresa = $idEmpresa
			,@usuario = '$usuario'
			, @resultado = :resultado" );
		
		$sentencia->bindParam ( ":resultado", $resultado, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000 );
		$sentencia->execute ();
		if ($resultado > 0)
			return 1;
		else
			return 0;
	}
	
	/**
	 * Ejecuta el procedimiento almacenado para activar las afiliacion por motivo
	 * de la re afiliacion de la empresa expulsada
	 * @param unknown_type $idEmpresa
	 * @param unknown_type $idDesafiliacion
	 * @return {int} ( 	0 => No se pudo realizar el procedimiento,
	 * 					# => El numero de trabajadores de la empresa)
	 */
	function spProcesoActivAfiliExpul($idEmpresa,$idDesafiliacion,$usuario){
		$resultado = 0;
		$sentencia = $this->db->conexionID->prepare ( "EXEC [kardex].[sp_000_Activar_Afiliacion_Expul]
				@id_empresa = $idEmpresa
				,@id_desafiliacion=$idDesafiliacion
				,@usuario = '$usuario'
				, @resultado = :resultado" );
	
		$sentencia->bindParam ( ":resultado", $resultado, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000 );
		$sentencia->execute ();
		if ($resultado > 0)
			return 1;
		else
			return 0;
	}
	
	/** Retornar el valor del IDformulario de aportes 017 **/
	function spProcesoAfiliacionAnula($idPersona){
		$resultado = 0;
		$sentencia = $this->db->conexionID->prepare ( "EXEC [dbo].[sp_inactivar]
				@idpersona = $idPersona
				,@idUltimaTrayectoria = :formulario" );
				$sentencia->bindParam ( ":formulario", $formulario, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000 );
				$sentencia->execute ();
	}
	
	/*
	 * metodo cambia el estado a M de la persona en aportes015 y desafilia todo lo que haya activo poniendo causal la 2860 muerte y la Fecha de Inactivaci�n del dia
	*/
	function sp_inactivar_afiliacion($idformulario,$motivoretiro,$fecharetiro){
		$formulario = 0;
		$usuarioinactiva=$_SESSION['USUARIO'];
		$sentencia = $this->db->conexionID->prepare ( "EXEC [dbo].[sp_inactivar]
				 @idFormulario = $idformulario
				,@idMotivoRetiro =$motivoretiro
				,@fecharetiro = '$fecharetiro'
				,@usuarioInactiva=$usuarioinactiva
				,@idUltimaTrayectoria = :formulario" );
				$sentencia->bindParam ( ":formulario", $formulario, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000 );
				$sentencia->execute ();
		return 	$formulario;
	}
	
	function CambioEstadoM($idPersona,$motivoretiro=null,$fecharetiro=null){
	
		if($motivoretiro=='2860'){
			
			$estTrabjador=$this->consultar_estado_trabajador($idPersona);
			
			// inicia la transaccion
			$this->db->inicioTransaccion();
			
			if($estTrabjador['estado']=='N'){
				// se actualiza el estado de la persona a M (Muerto)
				$sqlupdate="update aportes015 set estado='M',fechadefuncion='$fecharetiro' where idpersona=$idPersona";
				$rowupdate = $this->db->queryActualiza($sqlupdate);
				
				// se evalua que el update de estado se alla hecho de forma correcta
				if($rowupdate==0){
					$this->db->cancelarTransaccion();
					return 0;
				}
			}
			
			 // se verifica si esta persona tiene afiliaciones activas para inactivarlas poniento el motivo 2680 muerte
			 $rs=$this->buscar_afiliacion_a_general($idPersona);
			 $ctrerror=0;
			 while($row=$rs->fetch()){
			  	    // invocamos el procedimiento almacenado inactivar.
			   	    $respro=$this->sp_inactivar_afiliacion($row['idformulario'],$motivoretiro,$fecharetiro);
 		      	    if($respro==0){
			      	   	$ctrerror=1;
			      	   	break;
			      	}
         
			 } 
			 
			 if($ctrerror==0){
			  	 $this->db->confirmarTransaccion();
			   	 return 1;
			 }else{
			   	 $this->db->cancelarTransaccion();
			   	 return 0;
			 }
	
		
	   }
	}	
	function consultar_estado_trabajador($idpersona){
		$sql="SELECT estado FROM aportes015 WHERE idpersona=$idpersona";
		$rs=$this->db->querySimple($sql);
		return $rs->fetch();
	}
	
}
?>