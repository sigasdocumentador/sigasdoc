<?php
/* autor:       orlando puentes
 * fecha:       14/09/2010
 * objetivo:     
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'rsc' . DIRECTORY_SEPARATOR . 'conexion.class.php';

class Tarjeta{
 //constructor	
var $con;
function Tarjeta(){
 		$this->con=new DBManager;
 	}

 	function buscar_cargue_reverso($idTraslado){
 		if($this->con->conectar()==true){
 			$sql="SELECT aportes115.*, aportes091.detalledefinicion, aportes120.nota 
					FROM aportes115
					INNER JOIN aportes120 ON aportes120.idnotas=aportes115.idnota 
					INNER JOIN aportes091 ON aportes091.iddetalledef=aportes115.idmotivo 
					WHERE idtraslado=$idTraslado";
 			return mssql_query($sql,$this->con->conect);
 		}
 	} 	
 	
function buscar_tarjeta($idper){
	if($this->con->conectar()==true){
	$sql="SELECT aportes101.*, aportes091.detalledefinicion FROM aportes101 INNER JOIN aportes091 ON aportes101.idetapa=aportes091.iddetalledef where idpersona=$idper and estado='A'";
	return mssql_query($sql,$this->con->conect);
	}
	}
	
function buscar_tarjeta_idt($idt){
	if($this->con->conectar()==true){
	$sql="select * from aportes101 where idpersona=$idt and estado='A'";
	return mssql_query($sql,$this->con->conect);
	}
	}	

function buscar_cargues($idt,$lim=100){
	if($this->con->conectar()==true){
		//$sql="Select top $lim * from aportes104 where idtrabajador=$idt order by periodogiro desc";
		$sql="SELECT TOP $lim a104.*, ISNULL(a15.identificacion,0) embargado, ISNULL(b15.identificacion,0) tercero, isnull((a104.valor - a115.valor),a104.valor) AS valorcargue from aportes104 a104 LEFT JOIN aportes015 a15 ON a15.idpersona=a104.idpersona LEFT JOIN aportes015 b15 ON b15.idpersona=a104.idconyuge LEFT JOIN aportes115 a115 ON a115.idtraslado=a104.idtraslado where a104.idtrabajador=$idt order by a104.periodogiro desc";
		return mssql_query($sql,$this->con->conect);
		}
	}

function buscar_movimientos($bono,$lim=100){
	$bono=trim($bono);
	if($this->con->conectar()==true){
		$sql="select top $lim item,valormovimiento,fechamovimiento,tipomovimiento,fechamov,comercio from aportes114 where bono='$bono' and tipomovimiento <> 'Consulta' ORDER BY fechamov DESC";
		//echo $sql;
		return mssql_query($sql,$this->con->conect);
		}
	}

function buscar_tarjeta_datos($id){
	if($this->con->conectar()==true){
	$sql="SELECT aportes015.papellido,aportes015.sapellido,aportes015.pnombre,aportes015.snombre, aportes015.identificacion, idtarjeta, bono, aportes102.idpersona, precodigo, codigobarra, programa, tipo, fechaexpedicion, fechasolicitud, fecharecepcion, fechaentrega, fechavence, entregada, idtipoentrega, estado, ubicacion,saldo,idetapa, aportes091.detalledefinicion FROM aportes102 INNER JOIN aportes015 ON aportes102.idpersona=aportes015.idpersona INNER JOIN aportes091 ON aportes102.idetapa=aportes091.iddetalledef WHERE aportes015.identificacion='$id' and aportes102.estado='A'";
	return mssql_query($sql,$this->con->conect);
	}
	}

function entregar_tarjeta($campos){
	if($this->con->conectar()==true){
	$sql="Update aportes102 set dateDiff(day, fechaentrega=
CAST(GETDATE() AS DATE)),entregada='S',dateDiff (day, fechaetapa=
CAST(GETDATE() AS DATE)),idetapa=65,usuarioentrega='".$campos[1]."' WHERE idtarjeta='".$campos[0]."'";
	return mssql_query($sql,$this->con->conect);
	}
	}

function insert_reposicion($campos){
    if($this->con->conectar()==true){
        $sql="insert into aportes103(idradicacion,idtrabajador,codigoreposicion,codigobarra,bono,comprobante,numero,procesado,fechasistema,usuario) 
            values(".$campos[0].",".$campos[1].",'".$campos[2]."','".$campos[3]."','".$campos[4]."','".$campos[5]."','".$campos[6]."','N',cast(getdate() as date),'".$campos[7]."')";
		//echo $sql;	
        return mssql_query($sql,$this->con->conect);
    }
}

function insert_bono($campos){
    if($this->con->conectar()==true){
        $sql="INSERT INTO aportes101(bono, idpersona, precodigo, codigobarra, programa, tipo, fechaexpedicion, entregada, estado, ubicacion, idetapa, saldo, usuario, fechasistema,fechavence) VALUES('".$campos[0]."',".$campos[1].",'".$campos[2]."','".$campos[3]."','".$campos[4]."','".$campos[5]."',cast(getdate() as date),'N','A','01',61, 0,'".$campos[9]."',cast(getdate() as date),'12/31/2999')";
	//echo $sql;
    return mssql_query($sql,$this->con->conect);
    }
}

function contar_bonos_disponibles(){
    if($this->con->conectar()==true){
        $sql="Select count(*) as cuenta from aportes100 where asignada='N'";
        return mssql_query($sql,$this->con->conect);
    }
}

function buscar_primer_bono(){
    if($this->con->conectar()==TRUE){
        $sql="SELECT top 1 * FROM aportes100 WHERE asignada='N'";
        return  mssql_query($sql,$this->con->conect);
    }
}
function marcar_bono($bono,$idp=0){
    if($this->con->conectar()==TRUE){
        $sql="Update aportes100 set asignada='S',idpersona=$idp where bono='$bono'";
        return mssql_query($sql,$this->con->conect);
    }
}

function buscar_tarjetas_servicio($ide){
    if($this->con->conectar()==TRUE){
        $sql="SELECT identificacion,pnombre,snombre,papellido,sapellido FROM aportes015 INNER JOIN aportes102 ON aportes102.idpersona = aportes015.idpersona WHERE aportes015.idpersona IN (SELECT idpersona FROM aportes016 WHERE idempresa=$ide AND estado='A' AND tipoformulario =49) AND aportes102.idetapa=63";
        return mssql_query($sql,$this->con->conect);
    }
}

}