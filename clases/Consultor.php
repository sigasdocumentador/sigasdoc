<?php
/* autor:       orlando puentes
 * fecha:       10/09/2010
 * objetivo:
 */
/**
 * NO SE DEBE INCLUIR MANEJO DE SESION POR QUE NO PERMITE CONEXIONES MULTIPLES X BLOQUEO DE ARCHIVO DE SESION
 */

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'conexion.class.php';

class Consultor{

	private $conManager;
	private $rscManager;

	public function __construct(){
		$this->conManager = new DBManager();
		if($this->conManager->conectar()){
			$this->rscManager = $this->conManager->conect;
		}
	}

	public function noRetorno($sql){
		$r = mssql_query($sql, $this->rscManager);
		if($r == true){
			@mssql_free_result($r);
			return true;
		}else{
			exit($sql);
			return false;
		}


	}

	/**
	 * Ejecuta la sentencia de insercion de datos y devuelve el id del ultimo registro insertado
	 *
	 * @param string $sql Sentencia INSERT
	 */
	public function insertId($sql){
		$sql .= '; SELECT SCOPE_IDENTITY()';
		$r = mssql_query($sql, $this->rscManager);
		if($r == true){
			$row = mssql_fetch_array($r);
			$rowid = $row['0'];
			@mssql_free_result($r);
			return $rowid;
		}else{
			exit($sql);
			return false;
		}

	}

	public function insertBytes($sql, $objArray){
		$r = mssql_query($sql, $this->rscManager, $objArray);
		return $r;
	}

	public function enArray($sql){
		$array = array();
		$r = mssql_query($sql, $this->rscManager);
		if(! $r){ //la consulta no trae ning�n registro
			//@todo Borrar
			exit($sql);
			@mssql_free_result($r);
			//echo $sql . '<br/>';
			return false;
		}else{
			while($row = mssql_fetch_array($r)){
				$row = array_map('trim', $row);
				$array[] = $row;
			}
			mssql_free_result($r);
			return $array;
		}
	}

	public function enJSON($sql){
		$array = array();
		$r = mssql_query($sql, $this->rscManager);
		if(! $r){
			exit($sql);
			@mssql_free_result($r);
			return false;
		}else{
			while($row = mssql_fetch_array($r)){
				$row = array_map('trim', $row);
				$row = array_map('utf8_encode', $row);
				$array[] = $row;
			}
			mssql_free_result($r);
			if(count($array) > 0){
				return json_encode($array);
			}else{
				return false;
			}
		}
	}

}

?>
