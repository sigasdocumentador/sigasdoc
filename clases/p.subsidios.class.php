<?php
/* autor:       orlando puentes
 * fecha:       20/07/2010
 * objetivo:     
 */
 
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'rsc' . DIRECTORY_SEPARATOR . 'conexion.class.php';

class Subsidios{
 //constructor	
var $con;
function Subsidios(){
 		$this->con=new DBManager;
 	}
	
function buscar_actual($idt){
		if($this->con->conectar()==true){
			//$sql="select periodo,periodoproceso, idbeneficiario, idconyuge, tipopago, valor, tipogiro, embargo, descuento,fechatarjeta,idpignoracion,procesado,idtipobeneficiario,papellido,sapellido,pnombre,snombre, detalledefinicion, aportes048.nit from aportes014 INNER JOIN aportes015 ON aportes014.idbeneficiario = aportes015.idpersona INNER JOIN aportes091 ON aportes014.idtipobeneficiario = aportes091.iddetalledef INNER JOIN aportes048 ON aportes014.idempresa=aportes048.idempresa WHERE idtrabajador=$idt AND aportes048.principal='S' order by periodo desc";
			$sql="select periodo,periodoproceso, idbeneficiario, idconyuge, tipopago, valor, tipogiro, embargo, descuento,fechatarjeta,idpignoracion,procesado,idtipobeneficiario,papellido,sapellido,pnombre,snombre,detalledefinicion,aportes048.nit,isnull(numche,'')as numche  from aportes014 INNER JOIN aportes015 ON aportes014.idbeneficiario = aportes015.idpersona left JOIN aportes091 ON aportes014.idtipobeneficiario = aportes091.iddetalledef INNER JOIN aportes048 ON aportes014.idempresa=aportes048.idempresa WHERE idtrabajador=$idt order by periodo desc";
			return mssql_query($sql,$this->con->conect);
		}
	}

function buscar_pre($idt){
		if($this->con->conectar()==true){
			//$sql="SELECT aportes099.idregistro, nit,periodo,embarga,defuncion,idparentesco,idbeneficiario,valorcuota,valordescuento,causal,p.detalledefinicion AS parentesco,c.detalledefinicion AS causal, aportes099.flag1, aportes099.flag2 FROM aportes099 INNER JOIN aportes091 p ON aportes099.idparentesco=p.iddetalledef left JOIN aportes091 c ON aportes099.causal=c.iddetalledef WHERE idpersona=$idt and procesado='N' ORDER BY aportes099.valorcuota DESC, aportes099.periodo DESC";
			$sql="SELECT aportes099.idregistro, nit,periodo,embarga,idparentesco,idbeneficiario,valorcuota,
					causal,p.detalledefinicion AS parentesco,c.detalledefinicion AS causal, aportes099.flag1, aportes099.flag2  
					FROM aportes099 
						INNER JOIN aportes091 p ON aportes099.idparentesco=p.iddetalledef left JOIN aportes091 c ON aportes099.causal=c.iddetalledef WHERE idpersona=$idt and procesado='N' ORDER BY aportes099.valorcuota DESC, aportes099.periodo DESC";
			return mssql_query($sql,$this->con->conect);
		}
	}
	
function buscar_historico($idt,$lim=50){
		if($this->con->conectar()==true){
			$sql="select TOP $lim periodo,periodoproceso, idbeneficiario, idconyuge, tipopago, valor, tipogiro, embargo, descuento,fechatarjeta,idpignoracion,procesado,idtipobeneficiario,papellido,sapellido,pnombre,snombre,aportes091.detalledefinicion,nit,anulado,fechaanula,b91.detalledefinicion motivoAnulacion from aportes009 INNER JOIN aportes015 ON aportes009.idbeneficiario = aportes015.idpersona left JOIN aportes091 ON aportes009.idtipobeneficiario = aportes091.iddetalledef INNER JOIN aportes048 ON aportes009.idempresa=aportes048.idempresa left JOIN aportes091 b91 ON aportes009.idcausal = b91.iddetalledef WHERE isnumeric(periodo)=1 AND idtrabajador=$idt order by periodo desc";
			return mssql_query($sql,$this->con->conect);
		}
	}
	
function buscar_causales($idt){
	if($this->con->conectar()==true){
		$sql="SELECT periodo,idbeneficiario,idcodigocausal, pnombre,snombre,papellido,sapellido,detalledefinicion FROM aportes065 left JOIN aportes015 ON aportes065.idbeneficiario=aportes015.idpersona left JOIN aportes091 ON aportes065.idcodigocausal=aportes091.iddetalledef
WHERE aportes065.idpersona=$idt";
		return mssql_query($sql,$this->con->conect);
		}
	}
	
	/**
	 * Busca un giro en la tabla de giros hist�ricos
	 * @param int $idt
	 * @param string $periodo aaaamm
	 * @return identificador de apuntador
	 */
	function buscarEnHistorico($idt,$periodo){
		if($this->con->conectar()==true){
			$sql="select periodo,periodoproceso, idbeneficiario, idconyuge, tipopago, valor, tipogiro, embargo, descuento,fechatarjeta,idpignoracion,procesado,idtipobeneficiario,papellido,sapellido,pnombre,snombre,detalledefinicion,nit from aportes009 INNER JOIN aportes015 ON aportes009.idbeneficiario = aportes015.idpersona INNER JOIN aportes091 ON aportes009.idtipobeneficiario = aportes091.iddetalledef INNER JOIN aportes048 ON aportes009.idempresa=aportes048.idempresa WHERE idtrabajador=$idt and periodo='$periodo'";
			return mssql_query($sql,$this->con->conect);
		}
	}
	
	
	/**
	 * Busca un giro en la tabla de giros actuales
	 * @param int $idt
	 * @param string $periodo aaaamm
	 * @return identificador de apuntador
	 */
	function buscarEnActual($idt,$periodo){
		if($this->con->conectar()==true){
			$sql="select periodo,periodoproceso, idbeneficiario, idconyuge, tipopago, valor, tipogiro, embargo, descuento,fechatarjeta,idpignoracion,procesado,idtipobeneficiario,papellido,sapellido,pnombre,snombre, detalledefinicion, aportes014.nit from aportes014 INNER JOIN aportes015 ON aportes014.idbeneficiario = aportes015.idpersona INNER JOIN aportes091 ON aportes014.idtipobeneficiario = aportes091.iddetalledef INNER JOIN aportes048 ON aportes014.idempresa=aportes048.idempresa WHERE idtrabajador=$idt AND aportes048.principal='S' AND periodo='$periodo'";
			return mssql_query($sql,$this->con->conect);
		}
	}

}