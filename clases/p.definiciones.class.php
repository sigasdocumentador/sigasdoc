<?php
/* autor:       orlando puentes
 * fecha:       23/07/2010
 * objetivo:
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'conexion.class.php';

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';

include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'utilitario.class.php';

class Definiciones{
 //constructor
var $con;
var $fechaSistema;

	private static $conPDO = null;

	function Definiciones(){
 		$this->con=new DBManager;
 		
 		try{
 			self::$conPDO = IFXDbManejador::conectarDB();
 			if( self::$conPDO->conexionID==null ){
 				throw new Exception(self::$conPDO->error);
 			}
 		}catch(Exception $e){
 			echo $e->getMessage();
 			exit();
 		}
 	}

function mostrar_registro($id){
		if($this->con->conectar()==true){
			$sql="SELECT * FROM aportes091 WHERE iddetalledef='$id'";
			return mssql_query($sql,$this->con->conect);
		}
	}

function mostrar_datos($id,$col=4){
	if($this->con->conectar()==true){
		$sql="SELECT * FROM aportes091 where iddefinicion=$id order by $col";
		return mssql_query($sql,$this->con->conect);
	}
}
	
function mostrar_datos_varios($id,$col=4,$codigos){
	if($this->con->conectar()==true){
		$sql="SELECT * FROM aportes091 where iddefinicion=$id and codigo in($codigos) order by $col";
		return mssql_query($sql,$this->con->conect);
	}
}

function mostrar_detalle($id){
	if(strlen(trim($id))==0)
		return false;
	if($this->con->conectar()==true){
		$sql="SELECT detalledefinicion FROM aportes091 where iddetalledef = $id";
		return mssql_query($sql,$this->con->conect);
		}
	}

function mostrar_codigo($col=4,$flag){
		if($this->con->conectar()==true){
			$sql="SELECT * FROM aportes091 WHERE iddefinicion=47 AND codigo LIKE '$flag%' order by detalledefinicion";
			return mssql_query($sql,$this->con->conect);
		}
	}

function obtener_tipos_aportante(){
		if($this->con->conectar()==true){
			$sql="SELECT * FROM aportes091 WHERE iddefinicion=34 ORDER BY detalledefinicion";
			return mssql_query($sql,$this->con->conect);
		}
	}
function obtener_clases_aportante(){
		if($this->con->conectar()==true){
			$sql="SELECT * FROM aportes091 WHERE iddefinicion=33 ORDER BY detalledefinicion";
			return mssql_query($sql,$this->con->conect);
		}
	}
function obtener_actividad_economica(){
		if($this->con->conectar()==true){
			$sql="SELECT * FROM aportes091 WHERE iddefinicion=15 ORDER BY detalledefinicion";
			return mssql_query($sql,$this->con->conect);
		}
	}
function mostrar_indicador(){
		if($this->con->conectar()==true){
			$sql="SELECT * FROM aportes091 WHERE iddefinicion=18 ORDER BY detalledefinicion";
			return mssql_query($sql,$this->con->conect);
		}
	}
function obtener_sector_economico(){
		if($this->con->conectar()==true){
			$sql="SELECT * FROM aportes091 WHERE iddefinicion=16 ORDER BY detalledefinicion";
			return mssql_query($sql,$this->con->conect);
		}
	}
	
	/**
	 * El parametro debe ser un array indice valor array("atributo"=>valor,...)
	 * @param unknown_type $arrAtributoValor
	 * @return multitype:NULL | array
	 */
	public function buscar_definicion($arrAtributoValor){
	
		/*
		 * NOTA: Ha este metodo se le puede agregar mas filtro y atributos a la consulta.
		* 		Pero no se pueden alterar los que ya existen
		* */
	
		$attrEntidad = array(
				array("nombre"=>"iddefinicion","tipo"=>"NUMBER","entidad"=>"a90")
				,array("nombre"=>"codigo","tipo"=>"TEXT","entidad"=>"a90"));
	
		$filtroSql = Utilitario::obtenerFiltro($attrEntidad,$arrAtributoValor);
		$sql = "SELECT iddefinicion, definicion, concepto, fechacreacion, usuario, codigo
				FROM aportes090 a90
				$filtroSql";
		return $this->fetchConsulta($sql);
	}
	
	/**
	 * El parametro debe ser un array indice valor array("atributo"=>valor,...)
	 * @param unknown_type $arrAtributoValor
	 * @return multitype:NULL | array
	 */
	public function buscar_detalle_definicion($arrAtributoValor){
		
		/*
		 * NOTA: Ha este metodo se le puede agregar mas filtro y atributos a la consulta.
		* 		Pero no se pueden alterar los que ya existen
		* */
		
		$attrEntidad = array(
				array("nombre"=>"iddetalledef","tipo"=>"NUMBER","entidad"=>"a91")
				,array("nombre"=>"iddefinicion","tipo"=>"NUMBER","entidad"=>"a91")
				,array("nombre"=>"codigo","tipo"=>"TEXT","entidad"=>"a91"));
	
		$filtroSql = Utilitario::obtenerFiltro($attrEntidad,$arrAtributoValor);
		$sql = "SELECT iddetalledef, iddefinicion, codigo, detalledefinicion, concepto, fechacreacion, usuario
				FROM aportes091 a91
				$filtroSql";
		return $this->fetchConsulta($sql);
	}
	
	/**
	 * El parametro debe ser un array indice valor array("atributo"=>valor,...)
	 * @param unknown_type $arrAtributoValor
	 * @return multitype:NULL | array
	 */
	public function buscar_definicion_detalle_definicion($arrAtributoValor){
	
		/*
		 * NOTA: Ha este metodo se le puede agregar mas filtro y atributos a la consulta.
		* 		Pero no se pueden alterar los que ya existen
		* */
		
		$resultado = array();
		//Obtener los datos de la definicion
		$arrValor = array("codigo"=>$arrAtributoValor["codigo_definicion"]);
		$arrDataDefinicion = $this->buscar_definicion($arrValor);
		
		
	
		if(count($arrDataDefinicion)>0){
			$arrValor = array(
					"iddefinicion"=>$arrDataDefinicion[0]["iddefinicion"]
					, "codigo"=>$arrAtributoValor["codigo_detalle_definicion"]);
			
			$resultado = $this->buscar_detalle_definicion($arrValor);
		}
		return $resultado;
	}
	
	/**
	 * Retorna los valores obtenidos en la consulta
	 * @return multitype:array
	 */
	private function fetchConsulta($querySql){
		$resultado = array();
		$rs = self::$conPDO->querySimple($querySql);
		while($row = $rs->fetch())
			$resultado[] = array_map("trim",$row);
		//$resultado[] = array_map("utf8_encode",$row);
		return $resultado;
	}

 }
 

?>