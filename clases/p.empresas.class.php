<?php
/* autor:       orlando puentes
 * fecha:       30/08/2010     
 */
 
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'rsc' . DIRECTORY_SEPARATOR . 'conexion.class.php';

class Empresa{
	//constructor	
	var $con;
	function Empresa(){
 		$this->con=new DBManager;
 	}
	
 	function actualizar_registroempresa($campos){
 		if($this->con->conectar()==true){
 			$sql="UPDATE aportes048 SET idrepresentante='".$campos[0]."', idjefepersonal='".$campos[0]."', direccion='".$campos[2]."', telefono='".$campos[3]."', iddepartamento='".$campos[4]."', idciudad='".$campos[5]."', idclasesociedad='101', contratista='N', colegio='N', exento='N', usuario='".$campos[6]."' WHERE nit='".$campos[1]."'";
 			return mssql_query($sql,$this->con->conect);
 		}
 	}
 	
 	function empresa_p_id($id) {
		if ($this->con->conectar () == true) {
			$sql = "SELECT * FROM aportes048 WHERE idempresa=$id and principal='S'";
			return mssql_query ( $sql, $this->con->conect );
		}
}
		
	function buscar_sucursales($nit){
		if($this->con->conectar()==true){
			$sql="select idempresa,nit,digito,principal,razonsocial,codigosucursal,razonsocial,direccion,telefono,seccional,estado from aportes048 where nit='$nit' and estado='A'";
			return mssql_query($sql,$this->con->conect);
		}
	}
	
	function contar_sucursales($nit){
		if($this->con->conectar()==true){
			$sql="select count(idempresa) as cantidad from aportes048 where nit='$nit'";
			$resultado = mssql_query($sql,$this->con->conect);
			if($resultado){
				$arrResultado = mssql_fetch_row($resultado);
				return intval($arrResultado["cantidad"]);
			}
			return false;
		}
		return false;
	}
	
	
	function obtener_empresas_para_giro_actual_sin_procesar($fechaLimite1, $fechaLimite2, $periodoInicial, $periodoActual){
		if($this->con->conectar()==true){
			$sql = "select
						e.idempresa,
						e.nit,
						e.codigosucursal,
						e.razonsocial,
						pu.periodo,
						count(pu.idtrabajador) as cant_pagos_pu,
						z.zona
					  from
						aportes048 e
						inner join aportes010 pu on e.idempresa=pu.idempresa
						inner join aportes089 z on z.codzona=e.idzona
					  where
						e.estado='A' and
						e.contratista = 'N' and
						e.idsector in (93,94) and
						pu.fechasistema between '". $fechaLimite1 ."' and '". $fechaLimite2 ."' and
						e.idempresa not in (SELECT idempresa FROM aportes014) and
						pu.periodo >= '". $periodoInicial ."' and
						pu.periodo <= '". $periodoActual ."'
					  group by
						e.idempresa,e.nit,e.codigosucursal,e.razonsocial,pu.periodo,z.zona
					  order by e.nit";
			return mssql_query($sql,$this->con->conect);
		}
		return false;
	}
	
	
	function contar_trabajadores_activos_subsidio_empresa($idEmpresa,$fechaAfiliacion = ''){
		if($this->con->conectar()==true){
			$strCondicFecha = ($fechaAfiliacion != '')?" and fechaingreso<='$fechaAfiliacion'":'';
			$sql = "SELECT
					count(*) as cantidad_trab
				  FROM 
				  	aportes016 
				  WHERE 
				  	idempresa=". $idEmpresa ." and 
				  	estado='A' and 
				  	tipoformulario=48 $strCondicFecha";
			if($resultado = mssql_query($sql,$this->con->conect)){
				$arrayResult = mssql_fetch_row($resultado);
				return intval($arrayResult["cantidad_trab"]);
			}
			return false;
		}
		return false;
	}
	
	function contar_trabajadores_inactivos_subsidio_empresa($idEmpresa,$fechaAfiliacion = ''){
		if($this->con->conectar()==true){
			$strCondicFecha = ($fechaAfiliacion != '')?" and fechaingreso<='$fechaAfiliacion'":'';
			$sql = "select 
						count(distinct idpersona) as cantidad_trab 
					from 
						aportes017 
					where 
						idempresa=". $idEmpresa ." and 
						estado='I' $strCondicFecha";
			if($resultado = mssql_query($sql,$this->con->conect)){
				$arrayResult = mssql_fetch_row($resultado);
				return intval($arrayResult["cantidad_trab"]);
			}
			return false;
		}
		return false;
	}
	
	/**
	 * Calcula el n�mero de giros hist�ricos de una empresa a partir de un per�odo girado inicial
	 *
	 * @param int $idEmpresa
	 * @param string $periodoInicial opcional
	 * @return int
	 */
	function calcular_giros_historicos_empresa($idEmpresa, $periodoInicial = ''){
		if($this->con->conectar()==true){
			$strCondicPeriodo = ($periodoInicial != '')?" and periodo >= '$periodoInicial'":'';
			$sql = "select count(*) as cuenta_giros from aportes009 where idempresa=$idEmpresa $strCondicPeriodo";
			if($resultado = mssql_query($sql,$this->con->conect)){
				$arrayResult = mssql_fetch_row($resultado);
				return intval($arrayResult["cuenta_giros"]);
			}
			return false;
		}
		return false;
	}
	
	function contar_beneficiarios_trabajadores_empresa($idEmpresa, $estadoBenef = 'A', $estadoTrab = 'A'){
		if($this->con->conectar()==true){
			$sql = "SELECT 
					count(b.idpersona) as cuenta_beneficiarios 
				FROM 
					aportes015 t
					inner join aportes021 rb on rb.idtrabajador=t.idpersona
					inner join aportes015 b on b.idpersona=rb.idbeneficiario
					inner join aportes016 af on af.idpersona=t.idpersona
					inner join aportes048 e on e.idempresa=af.idempresa
				WHERE
					t.estado='$estadoTrab' and
					b.estado='$estadoBenef' and
					af.tipoformulario = 48 and
					rb.giro = 'S' and
					e.idempresa = ". $idEmpresa;
			if($resultado = mssql_query($sql,$this->con->conect)){
				$arrayResult = mssql_fetch_row($resultado);
				return intval($arrayResult["cuenta_beneficiarios"]);
			}
			return false;
		}
		return false;
	}
	
	
	function obtener_empresas_contratistas($estado = 'A', $seccional = ''){
		if($this->con->conectar()==true){
			$strCondSeccional = ($seccional != '')?" and e.seccional = '$seccional'":'';
			$sql = "SELECT
					e.idempresa,
					e.nit,
					e.codigosucursal,
					e.razonsocial,
					e.contratista,
					e.direccion,
					e.telefono,
					e.seccional,
					CASE WHEN e.seccional='01' then 'NEIVA' WHEN e.seccional='02' then 'GARZON' WHEN e.seccional='03' then 'PITALITO' WHEN e.seccional='04' then 'LA PLATA' end as nom_seccional,
					e.idzona,
					e.idrepresentante,
					r.identificacion,
					r.papellido,
					r.sapellido,
					r.pnombre,
					r.snombre
				  FROM 
					aportes048 e
					inner join aportes015 r on r.idpersona=e.idrepresentante
				  WHERE 
					e.contratista='S' and
					e.estado='$estado' ".
					$strCondSeccional;
			return $resultado = mssql_query($sql,$this->con->conect);
		}
		return false;
	}
	
	function obtener_giro_actual_empresa_contratista(){
		if($this->con->conectar()==true){
			$sql = "SELECT distinct
						e.idempresa,
						e.nit,
						e.codigosucursal,
						e.razonsocial,
						e.contratista,
						e.direccion,
						e.telefono,
						e.seccional,
						CASE WHEN e.seccional='01' then 'NEIVA' WHEN e.seccional='02' then 'GARZON' WHEN e.seccional='03' then 'PITALITO' WHEN e.seccional='04' then 'LA PLATA' end as nom_seccional,
						e.idzona,
						z.zona,
						e.estado,
						gh.idbeneficiario,
						gh.idtrabajador,
						t.identificacion
						FROM 
						aportes048 e
						inner join aportes014 gh on gh.idempresa = e.idempresa
						inner join aportes015 t on t.idpersona=gh.idtrabajador
						inner join aportes089 z on z.codzona=e.idzona
						WHERE 
						e.contratista='S' and
						e.idsector in (96,98,99)";
			return $resultado = mssql_query($sql,$this->con->conect);
		}
		return false;
	}
	
	
	function contar_giros_pagados_a_terceros_por_empresas($idEmpresa = null){
		if($this->con->conectar()==true){
			$strCondEmpresa = (isset($idEmpresa) && is_numeric($idEmpresa))?" and g.idempresa=$idEmpresa":"";
			$query = "select
					e.idempresa,
					e.nit,
					e.codigosucursal,
					e.razonsocial,
					z.zona,
					count(*) as cuenta
				  from 
					aportes014 g
					inner join aportes048 e on e.idempresa=g.idempresa
					inner join aportes015 t on t.idpersona=g.idtrabajador
					inner join aportes089 z on z.codzona=e.idzona
				  where 
					g.codigopago in ('02=JUZGADO','04=EMBARGOS','03=MUERTOS') $strCondEmpresa
				  group by 
					e.idempresa,
					e.nit,
					e.codigosucursal,
					e.razonsocial,
					z.zona
				  order by 
					e.razonsocial
					asc";
			return $resultado = mssql_query($query,$this->con->conect);
		}
		return false;
	}
	
	function contar_giros_pagados_a_terceros_por_empresa($idEmpresa){
		if($this->con->conectar()==true){
			$query = "select 
						g.codigopago,
						g.idtrabajador,
						t.identificacion,
						t.papellido,
						t.sapellido,
						t.pnombre,
						t.snombre,
						g.idconyuge,
						sum(g.valor) as val_girado,
						sum(g.numerocuotas) as cant_cuotas,
						g.periodo
						from 
						aportes014 g
						inner join aportes015 t on t.idpersona=g.idtrabajador
						where 
						g.codigopago in ('02=JUZGADO','04=EMBARGOS','03=MUERTOS') and 
						g.idempresa = ". $idEmpresa ."
						group by 
						g.codigopago,
						g.idtrabajador,
						t.identificacion,
						t.papellido,
						t.sapellido,
						t.pnombre,
						t.snombre,
						g.idconyuge,
						g.periodo";
			return $resultado = mssql_query($query,$this->con->conect);
		}
		return false;
	}
	
	
	/**
	 * Ejecuta una consulta de insert
	 *
	 * @param unknown_type $query
	 * @return unknown
	 */
	function insert($query){
		if($this->con->conectar()==true){
			return $resultado = mssql_query($query,$this->con->conect);
		}
		return false;
	}
	
	function vaciar_tabla($nombreTabla = ''){
		if($this->con->conectar()==true){
			$query="delete from $nombreTabla";
			return $resultado = mssql_query($query,$this->con->conect);
		}
	}
	
	/*
	 * Busca si la empresa esta como pendiente, sin legalizar, y tiene radicaciones como afiliacion o renovacion
	*/
	function buscar_empresa_p_planillaUnica($filtro){
		if($this->con->conectar()==true){
			$sql="SELECT count(a48.idempresa) cant 
				  FROM aportes048 a48
				  WHERE a48.legalizada='N' AND a48.estado='P' $filtro
    				AND 0=ISNULL((SELECT COUNT(a4.idradicacion) FROM aportes004 a4 WHERE a4.nit=a48.nit AND a4.procesado='N' AND a4.idtiporadicacion IN (30,31)),0)";			
			return mssql_query($sql,$this->con->conect);
		}
		}
	
	function buscar_datos_empresa($nit){
		if($this->con->conectar()==true){
			$sql="SELECT DISTINCT a48.idempresa, a48.idtipodocumento, nit, digito, codigosucursal, principal, razonsocial, 
						sigla, a48.direccion, iddepartamento, idciudad, a48.idzona, a48.telefono, fax, a48.url, a48.email, idrepresentante, 
						idjefepersonal, contratista, idcodigoactividad, actieconomicadane, idasesor, fechamatricula,
						CASE WHEN a48.estado='I' THEN a48.fechaestado ELSE null END AS fechaestado, 
						idsector, seccional, tipopersona, claseaportante, tipoaportante, a48.estado, codigoestado, fechaaportes,
					 	a48.fechaafiliacion, trabajadores, aportantes, a48.usuario, a48.fechasistema, idclasesociedad, a48.rutadocumentos, 
					 	legalizada,d.departmento,m.municipio, r.papellido AS par,r.sapellido AS sar,r.pnombre AS pnr,r.snombre snr, c.papellido AS pac,
					 	c.sapellido AS sac,c.pnombre AS pnc,c.snombre snc, ca.descripcion AS actividad, da.clase AS codact, da.descripcion AS dane, 
					 	isnull(i.detalledefinicion,indicador) AS indicador, s.detalledefinicion AS sector, p.detalledefinicion tipopersona,
					 	cs.detalledefinicion AS clasesoci, capo.detalledefinicion AS clase_apo, ta.detalledefinicion AS tipo_apo, td.detalledefinicion AS tipo_doc
					FROM aportes048 a48 
					LEFT JOIN aportes089 d ON a48.iddepartamento=d.coddepartamento OR (isnumeric(a48.iddepartamento)=1 AND isnumeric(d.coddepartamento)=1 AND CAST(a48.iddepartamento AS INT)=CAST(d.coddepartamento AS INT))
					LEFT JOIN aportes089 m ON a48.idciudad=m.codmunicipio OR (isnumeric(a48.idciudad)=1 AND isnumeric(m.codmunicipio)=1 AND CAST(a48.idciudad AS INT)=CAST(m.codmunicipio AS INT))
					LEFT JOIN aportes015 r ON a48.idrepresentante=r.idpersona
					LEFT JOIN aportes015 c ON a48.idjefepersonal=c.idpersona
					LEFT JOIN aportes079 ca ON a48.idcodigoactividad=ca.idciiu
					LEFT JOIN aportes079 da ON a48.actieconomicadane=da.clase
					LEFT JOIN aportes091 i ON a48.indicador=i.iddetalledef
					LEFT JOIN aportes091 s ON a48.idsector=s.iddetalledef
					LEFT JOIN aportes091 p ON a48.tipopersona=p.iddetalledef
					LEFT JOIN aportes091 cs ON a48.idclasesociedad=cs.iddetalledef 
					LEFT JOIN aportes091 capo ON a48.claseaportante=capo.iddetalledef
					LEFT JOIN aportes091 ta ON a48.tipoaportante=ta.iddetalledef
					LEFT JOIN aportes091 td ON a48.idtipodocumento=td.iddetalledef
					WHERE nit='{$nit}'";
			//echo $sql;
			return mssql_query($sql,$this->con->conect);
		}
	}
	
	function buscar_datos_empresa_ID($ide){
		if($this->con->conectar()==true){
			$sql="SELECT DISTINCT a48.idempresa, a48.idtipodocumento, nit, digito, codigosucursal, principal, razonsocial, 
						sigla, a48.direccion, iddepartamento, idciudad, a48.idzona, a48.telefono, fax, a48.url, a48.email, idrepresentante, 
						idjefepersonal, contratista, idcodigoactividad, actieconomicadane, idasesor, fechamatricula,
						CASE WHEN a48.estado='I' THEN a48.fechaestado ELSE null END AS fechaestado, 
						idsector, seccional, tipopersona, claseaportante, tipoaportante, a48.estado, codigoestado, fechaaportes,
					 	a48.fechaafiliacion, trabajadores, aportantes, a48.usuario, a48.fechasistema, idclasesociedad, a48.rutadocumentos, 
					 	legalizada,d.departmento,m.municipio, r.papellido AS par,r.sapellido AS sar,r.pnombre AS pnr,r.snombre snr, c.papellido AS pac,
					 	c.sapellido AS sac,c.pnombre AS pnc,c.snombre snc, ca.descripcion AS actividad, da.clase AS codact, da.descripcion AS dane, 
					 	isnull(i.detalledefinicion,indicador) AS indicador, s.detalledefinicion AS sector, p.detalledefinicion tipopersona,
					 	cs.detalledefinicion AS clasesoci, capo.detalledefinicion AS clase_apo, ta.detalledefinicion AS tipo_apo, td.detalledefinicion AS tipo_doc
					FROM aportes048 a48 
					LEFT JOIN aportes089 d ON a48.iddepartamento=d.coddepartamento OR (isnumeric(a48.iddepartamento)=1 AND isnumeric(d.coddepartamento)=1 AND CAST(a48.iddepartamento AS INT)=CAST(d.coddepartamento AS INT))
					LEFT JOIN aportes089 m ON a48.idciudad=m.codmunicipio OR (isnumeric(a48.idciudad)=1 AND isnumeric(m.codmunicipio)=1 AND CAST(a48.idciudad AS INT)=CAST(m.codmunicipio AS INT))
					LEFT JOIN aportes015 r ON a48.idrepresentante=r.idpersona
					LEFT JOIN aportes015 c ON a48.idjefepersonal=c.idpersona
					LEFT JOIN aportes079 ca ON a48.idcodigoactividad=ca.idciiu
					LEFT JOIN aportes079 da ON a48.actieconomicadane=da.clase
					LEFT JOIN aportes091 i ON a48.indicador=i.iddetalledef
					LEFT JOIN aportes091 s ON a48.idsector=s.iddetalledef
					LEFT JOIN aportes091 p ON a48.tipopersona=p.iddetalledef
					LEFT JOIN aportes091 cs ON a48.idclasesociedad=cs.iddetalledef 
					LEFT JOIN aportes091 capo ON a48.claseaportante=capo.iddetalledef
					LEFT JOIN aportes091 ta ON a48.tipoaportante=ta.iddetalledef
					LEFT JOIN aportes091 td ON a48.idtipodocumento=td.iddetalledef
					WHERE idempresa='{$ide}'";
			//echo $sql;
			return mssql_query($sql,$this->con->conect);
		}
		}
		
}
?>