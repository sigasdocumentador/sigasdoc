<?php
/* autor:       orlando puentes
 * fecha:      22/07/2010
 * objetivo:     
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'rsc' . DIRECTORY_SEPARATOR . 'conexion.class.php';

class Certificados{
 //constructor	
var $con;
function Certificados(){
 		$this->con=new DBManager;
 	}
	
function insert_certificado($campos){
	if($this->con->conectar()==true){
		$sql="Insert into aportes026 (idbeneficiario, idparentesco, idtipocertificado, periodoinicio, periodofinal, fechapresentacion, formapresentacion, estado, usuario, fechasistema) values(".$campos[0].",".$campos[1].",".$campos[2].",'".$campos[3]."','".$campos[4]."',cast(getdate() as date),'".$campos[5]."','A','".$campos[6]."',cast(getdate() as date))";
		return mssql_query($sql,$this->con->conect);
	}
}

function insert_certificado_discapacitado($campos){
	if($this->con->conectar()==true){
		$sql="Insert into aportes026 (idbeneficiario, idparentesco, idtipocertificado, periodoinicio, periodofinal, fechapresentacion, formapresentacion, estado, usuario, fechasistema) values(".$campos[0].",".$campos[1].",58,'".$campos[3]."','".$campos[4]."',cast(getdate() as date),'".$campos[5]."','I','".$campos[6]."',cast(getdate() as date))";
		return mssql_query($sql,$this->con->conect);
	}
}

function buscar_vigencia($idc){
	if($this->con->conectar()==true){
		$sql="Select * from aportes024 where idcertificado=$idc";
		return mssql_query($sql,$this->con->conect);
		}
	}
	
function obtener_tipos_certificados_por_codigos($tipos = array(), $idDefinicion=11){
		if($this->con->conectar() == true){
			$sql = "SELECT * FROM aportes091 WHERE iddefinicion=$idDefinicion AND iddetalledef IN (". implode(",",$tipos) .")";
			return mssql_query($sql,$this->con->conect);
		}else
			return false;
	}
	
	
function obtener_certificados_por_tipos_y_ano($tipos = array(),$ano){
		if($this->con->conectar() == true){
			$sql="SELECT * FROM aportes026 where idtipocertificado in (". implode(",",$tipos) .") and year(fechapresentacion) = $ano";
			return mssql_query($sql,$this->con->conect);
		}else 
			return false;
	}

function inactivar_certificados($idben,$idtcer){
		if($this->con->conectar() == true){
			$sql="update aportes026 set estado = 'I'  where idbeneficiario = $idben";
			//and idtipocertificado=$idtcer
			return mssql_query($sql,$this->con->conect);
		}else 
			return false;
	}
	
function certificados_idp($idp){
	if($this->con->conectar()==true){
		$sql="SELECT pnombre,snombre,papellido,sapellido,aportes091.detalledefinicion, dd.detalledefinicion AS certificado,periodoinicio,periodofinal,
fechapresentacion,aportes026.estado FROM aportes026 
INNER JOIN aportes015 ON aportes026.idbeneficiario=aportes015.idpersona
INNER JOIN aportes091 ON aportes026.idparentesco=aportes091.iddetalledef
INNER JOIN aportes091 dd ON aportes026.idtipocertificado=dd.iddetalledef
WHERE idbeneficiario=$idp ORDER BY fechapresentacion DESC";
		//echo $sql;
		return mssql_query($sql,$this->con->conect);
	}
}	

}
?>