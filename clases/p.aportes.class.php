<?php
/* autor:       orlando puentes
 * fecha:       16/07/2010
 * objetivo:     
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';

include_once $raiz . DIRECTORY_SEPARATOR . 'rsc' . DIRECTORY_SEPARATOR .'conexion.class.php';
include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'utilitario.class.php';

class Aportes{
 	//constructor	
	var $con;
	private static $conPDO = null;
	
	function Aportes(){
		//Conexion nativa
 		$this->con=new DBManager;
 		//Conexion PDO
 		try{
 			self::$conPDO = IFXDbManejador::conectarDB();
 			if( self::$conPDO->conexionID==null ){
 				throw new Exception(self::$conPDO->error);
 			}
 		}catch(Exception $e){
 			echo $e->getMessage();
 			exit();
 		}
 	}
	
 	function inicioTransaccion(){
 		self::$conPDO->inicioTransaccion();
 	}
 	function cancelarTransaccion(){
 		self::$conPDO->cancelarTransaccion();
 	}
 	function confirmarTransaccion(){
 		self::$conPDO->confirmarTransaccion();
 	}
 	
	function buscar_aportes($ide,$lim=50){
	    if($this->con->conectar()==true){
	    	$sql="select nit,aportes011.idaporte,periodo,valornomina,valoraporte,aportes011.trabajadores,ajuste,fechapago,comprobante,documento,numerorecibo,aportes011.fechasistema,aportes011.usuario,aportes011.caja,aportes011.sena,aportes011.icbf,aportes011.indicador,aportes011.planilla,aportes011.usuariotraslado, aportes011.fechatraslado from aportes011 INNER JOIN aportes048 ON aportes011.idempresa=aportes048.idempresa where aportes011.idempresa in ($ide) ORDER BY periodo DESC";
			return mssql_query($sql,$this->con->conect);
		}
	}

	function buscar_aporte_ult_per($idt,$per='201110'){
	    if($this->con->conectar()==true){
	        $sql="select top 1 periodo,salariobasico,horascotizadas,periodo from aportes010 where idtrabajador=$idt and periodo='$per' order by periodo desc";
			//echo $sql;
		return mssql_query($sql,$this->con->conect);
		}
	}

	function buscar_aporte_ult_per2($idt,$per='201110'){
		if($this->con->conectar()==true){
			//$sql="select top 1 a10.periodo,(sum(a10.salariobasico)/a12.smlv) salariobasico, sum(a10.horascotizadas) horascotizadas from aportes010 a10 inner join aportes012 a12 on a10.periodo=a12.periodo where a10.idtrabajador=$idt and a10.periodo >= (cast(year(getDate()) as varchar(4)) + '09') group by a10.idtrabajador, a10.periodo, a12.smlv order by a10.periodo desc";
			
			$sql="select top 1 a10.periodo,
				(SELECT categoria_persona FROM aportes015 WHERE idpersona=$idt) as cate
					from aportes010 a10 
					where a10.idtrabajador=$idt and a10.periodo >= (cast(year(getDate()) as varchar(4)) + '09') 
					and a10.periodo <= (cast(year(getDate()) as varchar(4)) + '09')
					group by a10.idtrabajador, a10.periodo order by a10.periodo desc";
			
			return mssql_query($sql,$this->con->conect);
		}
	}
	
	
	function buscar_catego_ult($idt,$per='201110'){
		if($this->con->conectar()==true){
			//$sql="select top 1 a10.periodo,(sum(a10.salariobasico)/a12.smlv) salariobasico, sum(a10.horascotizadas) horascotizadas from aportes010 a10 inner join aportes012 a12 on a10.periodo=a12.periodo where a10.idtrabajador=$idt and a10.periodo >= (cast(year(getDate()) as varchar(4)) + '09') group by a10.idtrabajador, a10.periodo, a12.smlv order by a10.periodo desc";
				
			$sql="SELECT categoria_persona FROM aportes015 WHERE idpersona=$idt)";
				
			return mssql_query($sql,$this->con->conect);
		}
		}
	
	

function buscar_aporte_primer_per($ide){
	if($this->con->conectar()==true){
		$sql="select min(periodo) as mperiodo from aportes011 where idempresa='$ide'";
		//echo $sql;
		return mssql_query($sql,$this->con->conect);
	}
}

	/**
	 * Obtiene los datos del �ltimo per�odo procesado � del siguiente per�odo por 
	 * procesar, seg�n el par�metro recibido: procesado: S � N
	 *
	 * @param string $procesado default S
	 * @return array $arrayResult arreglo con los datos del per�odo � false
	 */
	function obtener_limite_pago_aportes($procesado = 'S'){
		if($this->con->conectar()==true){
			if($procesado == 'S' || $procesado == 'N' ){
				$query = "SELECT top 1 periodo, fechalimite FROM aportes012 WHERE procesado='$procesado' ORDER BY periodo DESC";
				$resultado = mssql_query($query, $this->con->conect);
				$arrayResult = mssql_fetch_row($resultado);
				if(is_array($arrayResult) && count($arrayResult)>0)
					return $arrayResult;
			}
			return false;
		}
	}
	
	/**
	 * Obtiene las empresas que han hecho aportes durante un rango de fechas
	 *
	 * @param string $fechaInicial
	 * @param string $fechaFinal
	 * @return informix resulset $resultado o false
	 */
	function rep_empresas_pago_aportes_por_fechas($fechaInicial,$fechaFinal){
		if($this->con->conectar()==true){
			$query = "SELECT e.idempresa,a.comprobante,a.documento,a.numerorecibo,e.nit,e.codigosucursal,e.razonsocial,a.periodo,a.fechapago, sum(a.valornomina) AS val_nom, sum(a.valorpagado) AS val_pag, sum(a.trabajadores) AS num_trab, e.contratista, te.iddetalledef, te.detalledefinicion
				  FROM aportes011 a, aportes048 e, aportes091 te
				  WHERE a.idempresa=e.idempresa AND e.idsector = te.iddetalledef AND iddefinicion=16 AND te.codigo in (1,2) AND fechapago > '$fechaInicial' AND fechapago <= '$fechaFinal' AND e.estado='A' 
				  GROUP BY e.idempresa,a.comprobante,a.documento,a.numerorecibo,e.nit,e.codigosucursal,e.razonsocial,a.periodo,a.fechapago, e.contratista, te.iddetalledef,te.detalledefinicion	
				  HAVING sum(a.trabajadores) > 0 
				  ORDER BY a.comprobante, e.nit, a.periodo";
			$resultado = mssql_query($query, $this->con->conect);
			if($resultado)
				return $resultado;
			return false;
		}else{
			// problemas de conexi�n
		}
	}
	
	
	function obtener_cuenta_aportes_pagados_empresa($idEmpresa,$periodo){
		if(is_numeric($idEmpresa) && is_numeric(intval($periodo))){
			if($this->con->conectar()==true){
				$query = "SELECT COUNT(*) AS cuenta FROM aportes010 WHERE idempresa=$idEmpresa AND periodo='$periodo'";
				$resultado = mssql_query($query, $this->con->conect);
				if($resultado){
					$arrCuenta = mssql_fetch_row($resultado);
					return intval($arrCuenta["cuenta"]);
				}
				return false;
			}
		}
	}
	
	/**
	 * Obtiene los datos del per�odo de giro actual
	 *
	 * @return array $arregloResultado or false
	 */
	function obtener_periodo_giro_actual(){
		if($this->con->conectar()==true){
			$query = "SELECT top 1 * FROM aportes012 where procesado='N' order by fechainicio asc";
			$resultado = mssql_query($query, $this->con->conect);
			if($resultado){
				$arregloResultado = mssql_fetch_row($resultado);
				return $arregloResultado;
			}
			return false;
		}
		return false;		
	}
	
	/**
	 * Ejecuta una consulta para obtener las empresas que han pagado los aportes dentro de cierto
	 * per�odo de fechas
	 *
	 * @param string $fechaInicial 
	 * @param string $fechaFinal
	 */
	function rep_pago_aportes_por_fechas($fechaInicial,$fechaFinal){
		if($this->con->conectar()==true){
			$query = "SELECT
						a.comprobante,
						a.documento,
						a.numerorecibo,
						e.nit,
						e.razonsocial,
						a.periodo,
						e.contratista,
						sum(a.valornomina) AS nom,
						sum(a.valorpagado) AS cont,
						sum(a.trabajadores) AS num_tra
					  FROM 
						aportes011 a, 
						aportes048 e
					  WHERE 
						a.idempresa=e.idempresa and 
						fechapago BETWEEN '$fechaInicial' AND '$fechaFinal'
					  GROUP BY
						a.comprobante,
						a.documento,
						a.numerorecibo,
						e.nit,
						e.razonsocial,
						a.periodo,
						e.contratista";
			$resultado = mssql_query($query, $this->con->conect);
			if($resultado){
				return $resultado;
			}
			return false;
		}
		return false;
	}
	
	function rep_empresas_pago_aportes_periodo_actual(){
		$datosUltPeriodoProcesado  = $this->obtener_limite_pago_aportes('S');
		$datosProxPeriodoAProcesar = $this->obtener_limite_pago_aportes('N');
		$reporte = $this->rep_pago_aportes_por_fechas($datosUltPeriodoProcesado["fechalimite"],$datosProxPeriodoAProcesar["fechalimite"]);
		if($reporte)
			return $reporte;
		return false;
	}
	
	/**
	 * Busca los trabajadores que laboraron m�s de 240 horas � menos de 96 seg�n la empresa
	 * y per�odo recibidos
	 *
	 * @param int $idEmpresa
	 * @param string $periodo
	 * @param int $horas
	 */
	function buscar_trabajadores_240horas_96horas($idEmpresa,$periodo,$horas,$execQuery = true){
		$arregloResultado = array("result","query");
 		if($this->con->conectar()==true){
 			
 			switch($horas){
				case "96":
					$strCondHoras = " < $horas";
					$strHoras = "Menos de $horas";
					break;
				case "240":
					$strCondHoras = " > $horas";
					$strHoras = "Mas de $horas";
					break;
				default:
					$strCondHoras = " < $horas";
					$strHoras = "Menos de $horas";
					break;
			}
 			$query = "SELECT
						COUNT(a.idtrabajador) AS cant,
						t.identificacion,
						ti.codigo as tipo_doc,
						a.pnombre,
						a.snombre,
						a.papellido,
						a.sapellido,
						SUM(a.horascotizadas) AS hortras,
						SUM(a.salariobasico) AS suebas
					  FROM
						aportes010  a
					  LEFT JOIN aportes015 t ON a.idtrabajador=t.idpersona
					  LEFT JOIN aportes091 ti ON ti.iddetalledef = t.idtipodocumento
					  WHERE
						a.idempresa=$idEmpresa and
						a.periodo=$periodo
					  GROUP BY
						t.identificacion,
						ti.codigo,
						a.pnombre,
						a.snombre,
						a.papellido,
						a.sapellido
					  HAVING SUM(a.horascotizadas) $strCondHoras ORDER BY hortras";
 			$arregloResultado["query"] = $query;
 			if($execQuery){
 				$resultado = mssql_query($query, $this->con->conect);
				if($resultado)
					$arregloResultado["result"] = $resultado;
				else 
					$arregloResultado["result"] = false;
 			}else{
 				$arregloResultado["result"] = null;
 			}
			
			return $arregloResultado;
		}
		return false;
 	}// end buscar_trabajadores_240horas_96horas

 	
 	/**
 	 * Busca las novedades reportadas en planilla �nica por la empresa
 	 * especificada durante el per�odo especificado
 	 *
 	 * @param int $idEmpresa
 	 * @param string $periodo
 	 * @return ifx result o false
 	 */
 	function buscar_novedades_empresa($idEmpresa,$periodo){
 		$arregloResultado = array("result","query");
 		if($this->con->conectar()==true){
 			$query = "SELECT
			            '' as contador,
						aportes010.nit, 
						aportes010.identificacion, 
						aportes010.pnombre, 
						aportes010.snombre, 
						aportes010.papellido, 
						aportes010.sapellido, 
						aportes010.horascotizadas, 
						aportes010.diascotizados,
						aportes010.ingreso,
						aportes010.retiro,
						aportes010.var_tra_salario,
						aportes010.var_per_salario,
						aportes010.sus_tem_contrato,
						aportes010.inc_tem_emfermedad, 
						aportes010.lic_maternidad,
						aportes010.vacaciones, 
						aportes010.inc_tem_acc_trabajo,
						aportes010.fechasistema,
						aportes010.periodo
					  FROM aportes010
					  WHERE 
						aportes010.idempresa=$idEmpresa and
						(ingreso = 'X' OR 
						retiro = 'X' OR 
						var_tra_salario = 'X' OR 
						var_per_salario = 'X' OR 
						sus_tem_contrato = 'X' OR 
						inc_tem_emfermedad = 'X' OR 
						lic_maternidad = 'X' OR 
						vacaciones ='X' OR 
						(inc_tem_acc_trabajo <> '' AND inc_tem_acc_trabajo <> '00' AND inc_tem_acc_trabajo <> '0')) AND 
						aportes010.periodo='$periodo'
					  ORDER BY aportes010.nit,aportes010.identificacion";
			$resultado = mssql_query($query, $this->con->conect);
			if($resultado)
				return $resultado;
			return false;
		}
		return false;
 	}
 	
 	
 	/**
 	 * Busca todos los registros de la tabla de afiliados(aportes015)
 	 *
 	 * @param int $primeros default null utilizado para sacar los primeros n registros
 	 * @return ifx query result $resultado or false
 	 */
 	function buscar_todos_afiados($primeros = null){
 		if($this->con->conectar()==true){
 			$strPrimeros = ($primeros && is_numeric($primeros))?" top $primeros ":"";
 			$query = "select $strPrimeros
						idpersona,
						identificacion,
						pnombre,
						snombre,
						papellido,
						sapellido,
						direccion,
						idciuresidencia,
						z.municipio,
						idzona,
						z.zona,
						telefono,
						email,
						estado,
						sexo
					  from 
						aportes015 p
					  left join aportes089 z on z.codzona=p.idzona";
 			$resultado = mssql_query($query, $this->con->conect);
			if($resultado)
				return $resultado;
			return false;
		}
		return false;
 	}
 	
 	
 	/**
 	 * Obtiene la informaci�n del �ltimo retiro reportado por un afiliado en planilla �nica
 	 *
 	 * @param int $idAfiliado
 	 * @return ifx result or false
 	 */
 	function buscar_ultimo_retiro_afiliado($idAfiliado){
 		if($this->con->conectar()==true){
 			$query = "SELECT top 1 
					idtrabajador,
					idempresa,
					nit,
					periodo,
					fechasistema,
					identificacion,
					papellido,
					sapellido,
					pnombre,
					snombre
				  FROM 
					aportes010 
				  WHERE
				  	idtrabajador = $idAfiliado and 
					retiro='X' 
				  ORDER BY
					fechasistema DESC";
 			$resultado = mssql_query($query, $this->con->conect);
			if($resultado)
				return $resultado;
			return false;
 		}
 		return false;
 	}
 	
 	
 	/**
	 * Obtiene los datos del per�odo de giro anterior al actual
	 *
	 * @return array $arregloResultado or false
	 */
	function obtener_periodo_giro_anterior(){
		if($this->con->conectar()==true){
			$query = "SELECT top 1 * FROM aportes012 where procesado='S' order by fechainicio desc";
			$resultado = mssql_query($query, $this->con->conect);
			if($resultado){
				$arregloResultado = mssql_fetch_row($resultado);
				return $arregloResultado;
			}
			return false;
		}
		return false;		
	}
	
	
	function obtener_periodos_antiguos($ultimos = 12){
		if($this->con->conectar()==true){
			$query = "SELECT top $ultimos * FROM aportes012 where procesado='S' order by fechainicio desc";
			$resultado = mssql_query($query, $this->con->conect);
			if($resultado)
				return $resultado;
			return false;
		}
		return false;
	}


	function obtener_aportes_empresa_por_periodo($idEmpresa, $periodo){
		if($this->con->conectar()==true){
			$query = "SELECT * FROM aportes011 WHERE idempresa = $idEmpresa AND periodo='$periodo' ORDER BY fechasistema DESC";
			$resultado = mssql_query($query, $this->con->conect);
			if($resultado)
				return $resultado;
			return false;
		}
		return false;
	}
	
	function actualizar_devolucion_aporte($idAporte,$valoraporte,$valornomina,$indicador,$caja,$sena,$icbf){
		if($this->con->conectar()==true){	
			$sql="UPDATE aportes011 SET 
					valoraporte=$valoraporte, valorpagado=$valoraporte, valornomina=$valornomina,
					indicador=$indicador, tipo='S', caja=$caja, sena=$sena, icbf=$icbf 
				  WHERE idaporte=$idAporte";			
			
			return mssql_query($sql,$this->con->conect);
		}
	}
	
	function devolucion_indice_aporte($idAporte,$indiceEmpresa){
		if($this->con->conectar()==true){
			$sql="UPDATE aportes011 SET indicador=$indiceEmpresa WHERE idaporte=$idAporte";
			return mssql_query($sql,$this->con->conect);
		}
		}
	
	function actualizar_devolucion_planilla($idmpresa,$idtrabajador,$perido,$valor){
		if($this->con->conectar()==true){
			$sql="UPDATE aportes010 SET valoraporte=$valor WHERE idempresa=$idmpresa AND idtrabajador=$idtrabajador AND periodo='".$perido."'";
				
			return mssql_query($sql,$this->con->conect);
		}
	}
	
	/**
	 * Almacena el calendario del pago de aportes:
	 * 	
	 * @param array $arrDatos
	 */
	function guardar_calendario($arrDatos) {
		$sql="INSERT INTO dbo.aportes047
					(periodo_aporte,clase_aportante,fecha_inicio,fecha_final,usuario)
			VALUES ('{$arrDatos["periodo_aporte"]}',{$arrDatos["clase_aportante"]},'{$arrDatos["fecha_inicio"]}','{$arrDatos["fecha_final"]}','{$_SESSION["USUARIO"]}')";
		return self::$conPDO->queryInsert($sql,"aportes042") === null ? 0 : 1;
	}
	
	/**
	 * Busacar el calendario del pago de aportes:
	 *
	 * @param array $arrDatos
	 */
	function consultar_calendario($arrAtributoValor) {
		$attrEntidad = array(
				array("nombre"=>"id_fecha_vencimiento","tipo"=>"NUMBER","entidad"=>"a47")
				,array("nombre"=>"periodo_aporte","tipo"=>"TEXT","entidad"=>"a47")
				,array("nombre"=>"clase_aportante","tipo"=>"NUMBER","entidad"=>"a47")
				,array("nombre"=>"fecha_inicio","tipo"=>"DATE","entidad"=>"a47")
				,array("nombre"=>"fecha_final","tipo"=>"DATE","entidad"=>"a47")
				,array("nombre"=>"procesado","tipo"=>"TEXT","entidad"=>"a47")
				,array("nombre"=>"fecha_proceso","tipo"=>"DATE","entidad"=>"a47")
				,array("nombre"=>"fecha_sistema","tipo"=>"DATE","entidad"=>"a47"));
	
		$filtroSql = Utilitario::obtenerFiltro($attrEntidad,$arrAtributoValor);
	
		$sql = "SELECT * FROM aportes047 a47 $filtroSql";
		return $this->fetchConsulta($sql);
	}
	
	/**
	 * Metodo para actualizar el calendario de pago de los aportes
	 *
	 * @param String $idAportes [ejemplo(id1,id2,)]
	 * @param int $idEmpresaTraslado
	 * @param String $periodoTraslado
	 */
	function actualizar_calendario($datos){
			$sql="UPDATE aportes047 
				SET periodo_aporte = '{$datos["periodo_aporte"]}'
					,clase_aportante = {$datos["clase_aportante"]}
					,fecha_inicio = '{$datos["fecha_inicio"]}'
					,fecha_final = '{$datos["fecha_final"]}'
				WHERE id_fecha_vencimiento = {$datos["id_fecha_vencimiento"]}";
		return self::$conPDO->queryActualiza($sql);
	}
	
	/**
	 * Almacena la nota de los aportes con un tipo que puede ser:
	 * 		- G: Guardar aporte
	 * 		- M: Mododificar aporte
	 * 		- T: Traslado de aporte 
	 * @param int $idAporte
	 * @param String $nota
	 * @param String $tipoNota
	 */
	function guardarNota($idAporte, $nota, $tipoNota) {
		$sql="INSERT INTO aportes042 (idaporte, notas, tipo, fechasistema) 
				VALUES ($idAporte,'$nota','$tipoNota',cast(getdate() as date))";
		return self::$conPDO->queryInsert($sql,"aportes042") === null ? 0 : 1;
	}
	/**
	 * Metodo para realizar el traslado de aportes
	 * 
	 * @param String $idAportes [ejemplo(id1,id2,)]
	 * @param int $idEmpresaTraslado 
	 * @param String $periodoTraslado 
	 */
	function actualizar_traslado_aporte($idAportes,$idEmpresaTraslado=null,$periodoTraslado=null,$usuario){
			$camposSql = "";
			if($idEmpresaTraslado!=null)
				$camposSql .= "idempresa=".$idEmpresaTraslado.",";
			if($periodoTraslado!=null)
				$camposSql .= "periodo='".$periodoTraslado."',";
			
			$sql="UPDATE aportes011 SET $camposSql 
					usuariotraslado='$usuario', fechatraslado=cast(getdate() as date) 
				WHERE idaporte in ($idAportes)";
			return self::$conPDO->queryActualiza($sql);
	}
	
	/**
	 * Metodo para buscar los aportes trasladados
	 *
	 * @param String $idAportes [ejemplo(id1,id2,)]
	 */
	function buscar_aporte_traslado($idAportes){
		$sql = "SELECT a11.periodo,a11.idempresa,a48.nit,a48.razonsocial,a11.fechapago,a11.planilla
				FROM aportes011 a11 
					INNER JOIN aportes048 a48 ON a48.idempresa=a11.idempresa
				WHERE a11.idaporte IN ($idAportes)";
		return $this->fetchConsulta($sql);
	}
	
	/**
	 * Metodo para buscar los aportes trasladados
	 *
	 * @param array $arrDatos
	 */
	function buscar_encabezado_aporte($arrAtributoValor){
		$attrEntidad = array(
				array("nombre"=>"periodo","tipo"=>"TEXT","entidad"=>"a31")
				,array("nombre"=>"fechapago","tipo"=>"DATE","entidad"=>"a31")
				,array("nombre"=>"planilla","tipo"=>"TEXT","entidad"=>"a31")
				,array("nombre"=>"idempresa","tipo"=>"NUMBER","entidad"=>"a31"));
	
		$filtroSql = Utilitario::obtenerFiltro($attrEntidad,$arrAtributoValor);
	
		$sql = "SELECT * FROM aportes031 a31 $filtroSql";
		return $this->fetchConsulta($sql);
	}
	
	/**
	 * Metodo para realizar el traslado del encabezado aporte
	 *
	 * @param array $datosUpdate
	 * @param array $datosFiltro
	 */
	function actualizar_encabezado_aporte($datosUpdate, $datosFiltro){
		
		$attrEntidad = array(
				array("nombre"=>"periodo","tipo"=>"TEXT","entidad"=>"aportes031")
				,array("nombre"=>"fechapago","tipo"=>"DATE","entidad"=>"aportes031")
				,array("nombre"=>"planilla","tipo"=>"TEXT","entidad"=>"aportes031")
				,array("nombre"=>"idempresa","tipo"=>"NUMBER","entidad"=>"aportes031"));
		
		$filtroSql = Utilitario::obtenerFiltro($attrEntidad,$datosFiltro);
		
		$sql="UPDATE aportes031 SET periodo = '{$datosUpdate["periodo"]}' ,idempresa = {$datosUpdate["idempresa"]}
					, nit = '{$datosUpdate["nit"]}', razonsocial = '{$datosUpdate["razonsocial"]}' 
			$filtroSql";
		
		/*$sql="UPDATE aportes031 SET periodo = '{$datosUpdate["periodo"]}' ,idempresa = {$datosUpdate["idempresa"]}
						, nit = '{$datosUpdate["nit"]}', razonsocial = '{$datosUpdate["razonsocial"]}'
				WHERE periodo = '{$datosFiltro["periodo"]}' AND fechapago = '{$datosFiltro["fechapago"]}' 
						AND planilla = '{$datosFiltro["planilla"]}' AND idempresa = {$datosFiltro["idempresa"]}";*/
		return self::$conPDO->queryActualiza($sql);
	}
	
	/**
	 * Consultar las notas de los aportes
	 * @param int $idAporte
	 * @param String $tipoNota
	 * @return unknown|boolean
	 */
	function consultar_notas($idAporte,$tipoNota=null){
			$filtroSql = "idaporte=".$idAporte;
			if($tipoNota!=null)
				$filtroSql .= " AND tipo='$tipoNota'";
			$sql="SELECT notas, tipo FROM aportes042 WHERE ".$filtroSql;
			return self::$conPDO->querySimple($sql);
	}
	
	function consultar_tiene_devolucion($idAporte,$idPersona){
		if($this->con->conectar()==true){
			$sql="SELECT a59.valor, a58.fechasistema
				  FROM aportes058 a58  
  					INNER JOIN aportes059 a59
    				  ON a59.iddevolucion=a58.iddevolcion  					
				  WHERE a58.idaporte=$idAporte AND a59.idpersona=$idPersona";
			
			$resultado = mssql_query($sql,$this->con->conect);
			
			if($resultado)
				return $resultado;
			return false;
		}
	}
	
	/**
	 * Retorna los valores obtenidos en la consulta
	 * @return multitype:array
	 */
	private function fetchConsulta($querySql){
		$resultado = array();
		$rs = self::$conPDO->querySimple($querySql);
		while($row = $rs->fetch())
			$resultado[] = array_map("trim",$row);
		//$resultado[] = array_map("utf8_encode",$row);
		return $resultado;
	}
	
	/**
	 * Ejecuta el procedimiento almacenado que procesa las 
	 * empresas morosas, suspendidas, expulsadas,...
	 *
	 * @param unknown_type $usuario
	 * @return {int} ( 	0 => No se pudo realizar el procedimiento,
	 * 					1 => El procedimiento se ejecuto correctamente)
	 */
	/*function ejecutar_sp_procesar_morosas($usuario){		
		$resultado = 0;
		$sentencia = self::$conPDO->conexionID->prepare ( "EXEC [kardex].[sp_Maestro_Kardex_Moras]
				@usuario = $usuario
				, @resultado = :resultado" );
		$sentencia->bindParam ( ":resultado", $resultado, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000 );
		$sentencia->execute();
		if ($resultado > 0)
			return 1;
		else
			return 0;
	}*/
}
?>