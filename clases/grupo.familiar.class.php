﻿<?php
/* autor:       orlando puentes
 * fecha:       29/09/2010
 * objetivo:     
 */ 
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
include_once $raiz . DIRECTORY_SEPARATOR . 'rsc' . DIRECTORY_SEPARATOR . 'conexion.class.php';

include_once $_SESSION['RAIZ'] . DIRECTORY_SEPARATOR . '/rsc/pdo/IFXDbManejador.php';

include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'utilitario.class.php';

class GrupoFamiliar{
	//constructor
	var $con;
	private $db;
	function GrupoFamiliar(){
 		$this->con=new DBManager;
 		
 		$this->db = IFXDbManejador::conectarDB();
 		if($this->db->conexionID==null){
 			$cadena = $this->db->error;
 			echo $cadena;
 			exit();
 		}
 	}
 	
	function buscar_grupo($cc){
		if($this->con->conectar()==true){
			$sql="SELECT aportes021.idrelacion,aportes021.idtrabajador,aportes021.idbeneficiario, aportes021.idparentesco, aportes091.detalledefinicion 
			AS parentesco, aportes015.papellido,aportes015.sapellido,aportes015.pnombre,aportes015.snombre,aportes015.fechanacimiento,year(getdate()-aportes015.fechanacimiento -1)-1900
			AS edad,t.identificacion
			FROM aportes021 
			INNER JOIN aportes091 
			ON aportes021.idparentesco=aportes091.iddetalledef
			INNER JOIN aportes015 
			ON aportes021.idbeneficiario=aportes015.idpersona 
			INNER JOIN aportes015 t 
			ON aportes021.idtrabajador=t.idpersona 
			WHERE t.identificacion='$cc' 
			AND aportes021.idparentesco
			IN(35,36,37,38) 
			ORDER BY aportes021.idparentesco";
			//echo $sql;
			return mssql_query($sql,$this->con->conect);
		}
	}
	
	function buscar_conyuge($id){
		if($this->con->conectar()==true){
			$sql="SELECT aportes021.idrelacion,aportes021.idtrabajador, aportes021.idbeneficiario, aportes021.idparentesco, aportes091.detalledefinicion 
			AS parentesco, aportes015.papellido,aportes015.sapellido,aportes015.pnombre,aportes015.snombre,aportes015.fechanacimiento,year(getdate()-aportes015.fechanacimiento -1)-1900
			AS edad,t.identificacion 
			FROM aportes021 
			INNER JOIN aportes091 
			ON aportes021.idparentesco=aportes091.iddetalledef 
			INNER JOIN aportes015 
			ON aportes021.idbeneficiario=aportes015.idpersona 
			INNER JOIN aportes015 t 
			ON aportes021.idtrabajador=t.idpersona 
			WHERE t.identificacion='$id' 
			AND aportes021.idparentesco =34
			ORDER BY aportes021.idparentesco";
			return mssql_query($sql,$this->con->conect);
		}
	}
function buscarConyuges2p($idtd,$num){
		if($this->con->conectar()==true){
			$sql="";
			return mssql_query($sql,$this->con->conect);
		}
	}
	
function buscar_beneficiario_todo021($idben){
	if($this->con->conectar()==true){
		$sql="SELECT * FROM aportes021 WHERE idbeneficiario='$idben'";
		return mssql_query($sql,$this->con->conect);
	}
}

function buscar_beneficiario_rel_021($idBenef, $idTrab){
	if($this->con->conectar()==true){
		$sql="SELECT * FROM aportes021 WHERE idbeneficiario=$idBenef AND idtrabajador = $idTrab";
		return mssql_query($sql,$this->con->conect);
	}
} 


function buscarIdConyugeConvive($idp){
		if($this->con->conectar()==true){
			$sql="SELECT idconyuge FROM aportes021 WHERE idtrabajador=$idp AND  conviven = 'S' and idparentesco = 34 and estado = 'A'";
			return mssql_query($sql,$this->con->conect);
		}
	}
	function buscar_beneficiario($id,$idp){
		if($this->con->conectar()==true){
			$sql="SELECT idpersona, idtipodocumento, identificacion, papellido, sapellido, pnombre, snombre, sexo,fechanacimiento,aportes021.idparentesco 
			FROM aportes015 
			INNER JOIN aportes021 
			ON aportes015.idpersona = aportes021.idbeneficiario 
			WHERE identificacion='$id' 
			AND aportes021.idtrabajador=$idp";
			return mssql_query($sql,$this->con->conect);
		}
	}
	function buscar_parentesco($idt,$idb){
		if($this->con->conectar()==true){
			$sql="SELECT idparentesco 
			from aportes021 
			where idtrabajador=$idt 
			AND idbeneficiario=$idb";
			return mssql_query($sql,$this->con->conect);
		}
	}
	function buscar_grupo_id($idt){
		if($this->con->conectar()==true){
			$sql="SELECT DISTINCT aportes021.idrelacion,a.idpersona, aportes021.idbeneficiario, aportes021.idparentesco, aportes021.conviven, aportes021.giro, aportes021.embarga, b.capacidadtrabajo,aportes022.idembargo as embargo,aportes021.estado, aportes021.fechaafiliacion, aportes021.fechaasignacion, aportes021.fechasistema, c.identificacion AS idencony, aportes091.detalledefinicion AS parentesco,b.identificacion, b.papellido,b.sapellido, b.pnombre,b.snombre, b.sexo, b.fechanacimiento, dateDiff(day, b.fechanacimiento,getDate())/360 AS edad, b.estado as estado_ben, b.fechasistema as fechasistema_ben, aportes026.fechapresentacion FROM aportes021 INNER JOIN aportes015 a ON aportes021.idtrabajador=a.idpersona INNER JOIN aportes091 ON aportes021.idparentesco=aportes091.iddetalledef INNER JOIN aportes015 b ON aportes021.idbeneficiario=b.idpersona LEFT JOIN aportes015 c ON aportes021.idconyuge=c.idpersona LEFT JOIN aportes026 ON aportes021.idbeneficiario=aportes026.idbeneficiario AND aportes026.estado='A' LEFT JOIN aportes022 on aportes021.idbeneficiario=aportes022.idbeneficiario WHERE a.idpersona=$idt AND aportes021.idparentesco IN(35,36,37,38) ORDER BY aportes021.idparentesco";
			//echo $sql;
			return mssql_query($sql,$this->con->conect);
		}
	}
	function buscar_grupo_id2($idt){	//Se crea este porque en la 22 un beneficiario puede aparecer mas de una vez y por lo tanto aparece muchas veces en la consulta
		if($this->con->conectar()==true){
			$sql="SELECT DISTINCT aportes021.idrelacion,a.idpersona, aportes021.idbeneficiario, aportes021.idparentesco, aportes021.conviven, aportes021.giro, aportes021.embarga, b.capacidadtrabajo,aportes021.estado, aportes021.fechaafiliacion, aportes021.fechaasignacion, aportes021.fechasistema, c.identificacion AS idencony, aportes091.detalledefinicion AS parentesco,b.identificacion, b.papellido,b.sapellido, b.pnombre,b.snombre, b.sexo, b.fechanacimiento, dateDiff(day, b.fechanacimiento,getDate())/360 AS edad, b.estado as estado_ben, b.fechasistema as fechasistema_ben, aportes026.fechapresentacion, aportes021.fechaestado, b.fechaactualizacion, d.detalledefinicion, b.fechadefuncion as fechadefuncion_ben, b.tipoestado as tipoestado FROM aportes021 INNER JOIN aportes015 a ON aportes021.idtrabajador=a.idpersona INNER JOIN aportes091 ON aportes021.idparentesco=aportes091.iddetalledef INNER JOIN aportes015 b ON aportes021.idbeneficiario=b.idpersona LEFT JOIN aportes015 c ON aportes021.idconyuge=c.idpersona LEFT JOIN aportes026 ON aportes021.idbeneficiario=aportes026.idbeneficiario AND aportes026.estado='A' LEFT JOIN aportes091 d on aportes021.idmotivo = d.iddetalledef WHERE a.idpersona='$idt' AND aportes021.idparentesco IN(35,36,37,38) ORDER BY aportes021.idparentesco";
			//echo $sql;
			return mssql_query($sql,$this->con->conect);
		}
	}

	/** Trae informacion basica de todos los(as) compañeros(as) que ha notificado un afiliado **/
	function buscarBasicoGrupoFamiliar($idp){
		if($this->con->conectar()==true){
			$sql="SELECT a21.idrelacion, a21.idbeneficiario,td.codigo,b15.identificacion identificacionBeneficiario,b15.pnombre,b15.snombre,b15.papellido,b15.sapellido,a21.idparentesco,a91.detalledefinicion parentesco,
						b15.estado estadoBeneficiario,CASE WHEN b15.estado='N' THEN b15.fechasistema ELSE b15.fechadefuncion END fechaEstadoBeneficiario,b15.fechanacimiento,
						CAST(dateDiff(day, b15.fechanacimiento,getDate())/365.25 AS DECIMAL(5,2)) as edad,a21.fechaafiliacion,a26.fechapresentacion,a21.fechaasignacion,c15.identificacion identificacionConyuge,
						isnull(a21.idconyuge,0) idconyuge,c15.pnombre pnombreC,c15.snombre snombreC,c15.papellido papellidoC,c15.sapellido sapellidoC,
						a21.giro,CASE WHEN a21.fechagiro IS NULL OR a21.fechagiro='19000101' THEN a21.fechasistema ELSE a21.fechagiro END fechaGiroAfiliacion,
						CASE WHEN b15.capacidadtrabajo='I' THEN 'S' ELSE 'N' END discapacitado,CASE WHEN a21.embarga='S' THEN 'S' ELSE 'N' END embarga,a21.estado estadoAfiliacion,
						CASE WHEN a21.fechaestado IS NULL OR a21.fechaestado='19000101' THEN a21.fechasistema when a21.idmotivo='4242' then b15.fechadefuncion  ELSE a21.fechaestado END fechaEstadoAfiliacion, b91.detalledefinicion motivoInactivo,b15.tipoestado						
				FROM aportes021 a21 INNER JOIN aportes015 b15 ON a21.idbeneficiario=b15.idpersona INNER JOIN aportes091 a91 ON a21.idparentesco=a91.iddetalledef LEFT JOIN aportes026 a26
				    ON a26.idcertificado=(SELECT TOP 1 b26.idcertificado FROM aportes026 b26 WHERE a21.idbeneficiario=b26.idbeneficiario ORDER BY b26.estado, b26.fechapresentacion DESC)
				  LEFT JOIN aportes015 c15 ON a21.idconyuge=c15.idpersona LEFT JOIN aportes091 b91 ON a21.estado='I' AND a21.idmotivo=b91.iddetalledef LEFT JOIN aportes091 td ON b15.idtipodocumento = td.iddetalledef 
				WHERE a21.idtrabajador=$idp AND a21.idparentesco IN(35,36,37,38) ORDER BY a21.idparentesco";
			return mssql_query($sql,$this->con->conect);
		}
	}
	
	/**
	* Recibe el id de la persona, devuelve el grupo familiar completo
	* @param $idt Id de la persona a buscar
	*/
	function buscar_grupoFamXidT($idt){
		if($this->con->conectar()==true){
			$sql="SELECT idbeneficiario,p.idtipodocumento, d.codigo AS tipdoc, p.identificacion, p.papellido, p.sapellido, p.pnombre, p.snombre, 
			p.fechanacimiento, p.sexo, r.idparentesco, pr.detalledefinicion AS parentesco, r.conviven, p.desplazado, p.capacidadtrabajo, p.reinsertado, p.idescolaridad, es.detalledefinicion AS escolaridad, r.postulacionfonede
			FROM aportes021 r
			INNER JOIN aportes015 p ON p.idpersona=r.idbeneficiario
			LEFT JOIN aportes091 d ON p.idtipodocumento=d.iddetalledef
			LEFT JOIN aportes091 pr ON r.idparentesco=pr.iddetalledef
			LEFT JOIN aportes091 es ON p.idescolaridad=es.iddetalledef
			WHERE r.idtrabajador=$idt
			ORDER BY r.idparentesco";
			return mssql_query($sql,$this->con->conect);
		}
	}
	function grupo_simple_id($idt){
		if($this->con->conectar()==true){
			$sql="SELECT aportes021.idbeneficiario, aportes015.identificacion, aportes015.papellido, aportes015.sapellido, aportes015.pnombre,aportes015.snombre 
			FROM aportes021 
			INNER JOIN aportes015 
			ON aportes021.idbeneficiario=aportes015.idpersona 
			WHERE aportes021.idtrabajador=$idt";
			//echo $sql;
			return mssql_query($sql,$this->con->conect);
		}
	}
	function buscar_padres($idt){
		if($this->con->conectar()==true){
			$sql="SELECT idbeneficiario, aportes015.pnombre, aportes015.snombre, aportes015.papellido, aportes015.sapellido 
			FROM aportes021
			INNER JOIN aportes015 ON aportes021.idbeneficiario=aportes015.idpersona
			WHERE idparentesco=36 AND idtrabajador=$idt";
			return mssql_query($sql,$this->con->conect);
		}
	}
	function buscar_hijos($idt){
		if($this->con->conectar()==true){
			$sql="SELECT idbeneficiario, aportes015.pnombre, aportes015.snombre, aportes015.papellido, aportes015.sapellido 
			FROM aportes021
			INNER JOIN aportes015 ON aportes021.idbeneficiario=aportes015.idpersona
			WHERE idparentesco in (35,38) and aportes021.estado='A' AND idtrabajador=$idt";
			return mssql_query($sql,$this->con->conect);
		}
	}
	function contar_conyuge_s($idp){
		if($this->con->conectar()==true){
			$sql="SELECT COUNT(*) AS cuenta FROM aportes021 WHERE idconyuge=$idp AND idparentesco=34 AND conviven='S'";
			return mssql_query($sql,$this->con->conect);
		}
	}
	function buscar_convivencia($idp){
		if($this->con->conectar()==true){
			$sql="SELECT idbeneficiario,idconyuge, aportes015.identificacion,aportes015.pnombre, aportes015.snombre, aportes015.papellido, aportes015.sapellido, aportes015.fechanacimiento, aportes015.sexo, aportes021.conviven, aportes021.fechasistema, aportes091.detalledefinicion FROM aportes021 
			INNER JOIN aportes015 ON aportes021.idbeneficiario = aportes015.idpersona 
			INNER JOIN aportes091 ON aportes021.idparentesco=aportes091.iddetalledef 
			WHERE idparentesco=34 AND conviven='S' and idtrabajador=$idp";
			return mssql_query($sql,$this->con->conect);
		}
	}
//buscar convivencias ago 2011
   function buscarConvivencias($idp){
		if($this->con->conectar()==true){
			$sql="SELECT idbeneficiario,idconyuge, idtrabajador, aportes015.identificacion,aportes015.pnombre, aportes015.snombre, aportes015.papellido, aportes015.sapellido, aportes015.fechanacimiento, aportes015.sexo, aportes021.conviven, aportes021.estado as estado_relacion, aportes091.detalledefinicion, aportes021.idparentesco FROM aportes021 
			INNER JOIN aportes015 ON aportes021.idbeneficiario = aportes015.idpersona 
			INNER JOIN aportes091 ON aportes021.idparentesco=aportes091.iddetalledef 
			WHERE idparentesco=34 AND idtrabajador=$idp";
			return mssql_query($sql,$this->con->conect);
		}
	}
	
	/** Trae informacion basica de todos los(as) compañeros(as) que ha notificado un afiliado **/
	function buscarBasicoConyuges($idp){
		if($this->con->conectar()==true){
			$sql = "SELECT a21.idbeneficiario,b91.codigo,a15.identificacion,a15.pnombre,a15.snombre,a15.papellido,a15.sapellido,a21.conviven,a15.fechanacimiento,a21.fechasistema,
			a15.sexo,a21.estado estadoRelacion,a91.detalledefinicion,CASE WHEN a16.estado IS NULL THEN 'N' ELSE 'S' END labora, a16.salario,a16.fechaingreso,a21.idrelacion,a21.idconyuge
			FROM aportes021 a21 INNER JOIN aportes015 a15 ON a21.idbeneficiario=a15.idpersona LEFT JOIN aportes016 a16
			ON a16.idformulario=(SELECT TOP 1 b16.idformulario FROM aportes016 b16 WHERE a21.idbeneficiario=b16.idpersona AND b16.estado='A' ORDER BY b16.fechaingreso)
			INNER JOIN aportes091 a91 ON a21.idparentesco=a91.iddetalledef INNER JOIN aportes091 b91 ON b91.iddetalledef=a15.idtipodocumento WHERE a21.idparentesco=34 AND a21.idtrabajador=$idp";
			return mssql_query($sql,$this->con->conect);
		}
	}
	
	function buscar_hijos_conviven($idt,$idc){
		if($this->con->conectar()==true){
		$sql="SELECT idbeneficiario,idconyuge, aportes015.identificacion,aportes015.pnombre, aportes015.snombre, aportes015.papellido, aportes015.sapellido, 
		aportes021.conviven, aportes091.detalledefinicion 
		FROM aportes021 
		INNER JOIN aportes015 ON aportes021.idbeneficiario = aportes015.idpersona 
		INNER JOIN aportes091 ON aportes021.idparentesco=aportes091.iddetalledef 
		WHERE idparentesco in (35,38) AND idtrabajador=$idt and idconyuge=$idc";
		//echo $sql;
		return mssql_query($sql,$this->con->conect);
		}
	}
	function disolver_convivencia($idt,$idc,$usuario){
		if($this->con->conectar()==true){
			$sql="UPDATE aportes021 SET conviven='N',estado='I',usuario='$usuario' WHERE idtrabajador=$idt AND idconyuge=$idc AND idparentesco=34";
			mssql_query($sql,$this->con->conect);
			$sql="UPDATE aportes021 SET conviven='N',estado='I',usuario='$usuario' WHERE idtrabajador=$idc AND idconyuge=$idt AND idparentesco=34";
			return mssql_query($sql,$this->con->conect);
		}
	}
	function buscar_relacion($idp,$idt){
		if($this->con->conectar()==true){
			$sql="select idrelacion,idtrabajador,idbeneficiario,idparentesco, idconyuge, conviven, aportes021.fechaafiliacion, fechaasignacion, giro, estado, 
			aportes091.detalledefinicion, aportes015.pnombre AS pnomt,aportes015.snombre AS snomt, aportes015.papellido AS papet, aportes015.sapellido 
			AS sapet,c.pnombre AS pnomc,c.snombre AS snomc, c.papellido AS papec, c.sapellido AS sapec 
			FROM aportes021 
			INNER JOIN aportes091 ON aportes021.idparentesco=aportes091.iddetalledef LEFT JOIN aportes015 ON aportes021.idtrabajador=aportes015.idpersona
 
 			LEFT JOIN aportes015 c ON aportes021.idconyuge=c.idpersona WHERE idbeneficiario=$idp and idtrabajador=$idt";
			return mssql_query($sql,$this->con->conect);
		}
	}
	function buscarDatosRelacionID($idr){
		if($this->con->conectar()==true){
			$sql="SELECT TOP 1 a15.idpersona,a15.idtipodocumento,a15.identificacion,a15.pnombre,a15.snombre,a15.papellido,a15.sapellido,a15.fechanacimiento,
						 a15.sexo,a15.capacidadtrabajo,a21.idparentesco,a21.estado,a21.fechaasignacion,a21.idconyuge,a21.fechaafiliacion,
					     a21.idmotivo,a21.fechaestado,a21.giro,a26.idcertificado,a26.idtipocertificado,a26.fechapresentacion,biol.idtipodocumento idtipodocumentoBiol,biol.identificacion identificacionBiol,a21.idbiologico
				  FROM aportes021 a21 INNER JOIN aportes015 a15 ON a15.idpersona=a21.idbeneficiario LEFT JOIN aportes026 a26 ON a26.idbeneficiario=a21.idbeneficiario AND a26.estado='A' LEFT JOIN aportes015 biol ON biol.idpersona=a21.idbiologico
				  WHERE a21.idrelacion=$idr";
			return mssql_query($sql,$this->con->conect);
		}
	}
	function buscar_relaciones($idp){
		if($this->con->conectar()==true){
			$sql="select idrelacion,idtrabajador,idbeneficiario,idparentesco, idconyuge, conviven, aportes021.fechaafiliacion, fechaasignacion, giro, aportes021.estado, 
			aportes091.detalledefinicion, aportes015.pnombre AS pnomt,aportes015.snombre AS snomt, aportes015.papellido AS papet, aportes015.sapellido 
			AS sapet,c.pnombre AS pnomc,c.snombre AS snomc, c.papellido AS papec, c.sapellido AS sapec, aportes015.identificacion as cedula FROM aportes021 INNER JOIN aportes091 
			ON aportes021.idparentesco=aportes091.iddetalledef 
			LEFT JOIN aportes015 ON aportes021.idtrabajador=aportes015.idpersona
 			LEFT JOIN aportes015 c ON aportes021.idconyuge=c.idpersona WHERE aportes021.estado='A' and idbeneficiario=$idp and idparentesco not in(35,38) and idtrabajador not in(select idpersona from aportes017)";
			return mssql_query($sql,$this->con->conect);
		}
	}
	function buscar_relacion_padre($idp){
    	if($this->con->conectar()==true){
        	$sql="SELECT * FROM aportes021 WHERE idbeneficiario=$idp AND idparentesco=36 AND estado='A'";    //AND giro='48'
			return mssql_query($sql,$this->con->conect);
		}
	}	
	function buscar_relacion_hijo($idp,$idb){
    	if($this->con->conectar()==true){
        	$sql="SELECT * FROM aportes021 where idbeneficiario=$idb and idparentesco in (35,38) and estado='A' and idtrabajador=$idp";
        	//echo $sql;
        	return mssql_query($sql,$this->con->conect);
    	}
	}
	function buscar_relacion_hermano($idp,$idb){
    	if($this->con->conectar()==true){
        	$sql="Select * from aportes021 where idbeneficiario=$idb and idparentesco=37 and estado='A' and idtrabajador=$idp";
        	//echo $sql;
        	return  mssql_query($sql,$this->con->conect);
    	}
	}
	function guardar_beneficiario($campos){
    	if($this->con->conectar()==TRUE){
        	if($campos[4]=='48'){
        		$sql="Insert into aportes021 (idtrabajador, idbeneficiario, idparentesco, idconyuge, giro,fechaafiliacion,fechaasignacion,usuario, fechasistema,
				estado) values (".$campos[0].",".$campos[1].",".$campos[2].",".$campos[3].",'".$campos[4]."',cast(getdate() as date),'".$campos[6]."','".$campos[5]."',cast(getdate() as date),'A')";
        	}
        	else{
        		$sql="Insert into aportes021 (idtrabajador, idbeneficiario, idparentesco, idconyuge, giro,fechaafiliacion,fechaasignacion,usuario,fechasistema,estado) values(".$campos[0].",".$campos[1].",".$campos[2].",".$campos[3].",'".$campos[4]."',cast(getdate() as date),'".$campos[6]."','".$campos[5]."',cast(getdate() as date),'A')";
        	}
        	//echo $sql;
        	return mssql_query($sql,$this->con->conect);
    	}
	}
	function buscar_convivencia_conyuge($idp){
    	if($this->con->conectar()==true){
        	$sql="select distinct aportes021.fechaafiliacion,aportes015.identificacion, aportes015.papellido,aportes015.sapellido,aportes015.pnombre, aportes015.snombre, aportes021.conviven,
			aportes021.idparentesco, aportes021.estado, aportes021.idconyuge, aportes015.idpersona, aportes021.idtiporelacion from aportes021 inner join aportes015 on aportes021.idtrabajador = 
			aportes015.idpersona
			where aportes021.conviven = 'S' and aportes021.idparentesco = 34 and aportes021.estado = 'A' and aportes021.idconyuge =$idp";
        	//echo $sql;
        	return mssql_query($sql,$this->con->conect);
    	}
	}
	function buscar_conyuge_activo($idp){
		if($this->con->conectar()==true){
			$sql="select distinct aportes021.fechaafiliacion,aportes015.identificacion, aportes015.papellido,aportes015.sapellido,aportes015.pnombre, aportes015.snombre, aportes021.conviven,
			aportes021.idparentesco, aportes021.estado, aportes021.idconyuge, aportes015.idpersona, aportes021.idtiporelacion from aportes021 inner join aportes015 on aportes021.idtrabajador =
			aportes015.idpersona
			where aportes021.idparentesco = 34 and aportes021.estado = 'A' and aportes021.idconyuge =$idp";
			//echo $sql;
			return mssql_query($sql,$this->con->conect);
		}
	}
	function guardar_conyuge($campos){
    	if($this->con->conectar()==TRUE){
        	$sql="Insert into aportes021 (idtrabajador, idbeneficiario, idparentesco,idconyuge,conviven,idtiporelacion,
			usuario,fechaafiliacion,estado,fechasistema) 
			values(".$campos[0].",".$campos[1].",".$campos[2].",".$campos[3].",'".$campos[4]."',".$campos[5].",'".$campos[6]."',cast(getdate() as date),'A',cast(getdate() as date)";
        	//echo $sql;
        	return mssql_query($sql,$this->con->conect);
    	}
	}
	
	function buscar_grupo_sinC($idt){
    	if($this->con->conectar()==TRUE){
        	$sql="select aportes021.idbeneficiario, aportes021.idparentesco, aportes021.giro,aportes021.embarga, aportes091.detalledefinicion, aportes015.idpersona, aportes015.identificacion, aportes015.papellido, aportes015.sapellido, aportes015.pnombre,aportes015.snombre, c.idpersona as idc, c.identificacion as idenc, c.papellido as pac, c.sapellido as sac, c.pnombre as pnc, c.snombre as snc FROM aportes021 INNER JOIN aportes091 ON aportes021.idparentesco = aportes091.iddetalledef INNER JOIN aportes015 ON aportes021.idbeneficiario = aportes015.idpersona LEFT JOIN aportes015 c ON aportes021.idconyuge = c.idpersona WHERE aportes021.idparentesco IN(35,36,37,38) AND aportes021.estado='A' AND idtrabajador=$idt";
        	return mssql_query($sql,$this->con->conect);
    	}
	}
	//Se filtran que no aparezcan los beneficiarios que ya se encuentran enbargados
	function buscar_grupo_sinC2($idt){
		if($this->con->conectar()==TRUE){
			$sql="select aportes021.idbeneficiario, aportes021.idparentesco, aportes021.giro,aportes021.embarga, aportes091.detalledefinicion, aportes015.idpersona, aportes015.identificacion, aportes015.papellido, aportes015.sapellido, aportes015.pnombre,aportes015.snombre, c.idpersona as idc, c.identificacion as idenc, c.papellido as pac, c.sapellido as sac, c.pnombre as pnc, c.snombre as snc FROM aportes021 INNER JOIN aportes091 ON aportes021.idparentesco = aportes091.iddetalledef INNER JOIN aportes015 ON aportes021.idbeneficiario = aportes015.idpersona LEFT JOIN aportes015 c ON aportes021.idconyuge = c.idpersona WHERE aportes021.idparentesco IN(35,36,37,38) AND aportes021.estado='A' AND idtrabajador=$idt and (aportes021.embarga is null or aportes021.embarga<>'S')";
			return mssql_query($sql,$this->con->conect);
		}
	}
	
	function buscar_grupo_sinC3($idt){
		if($this->con->conectar()==TRUE){
			$sql="SELECT a21.idbeneficiario, a21.idparentesco, a21.giro, a21.embarga, a91.detalledefinicion, a15.idpersona, a15.identificacion, a15.papellido, a15.sapellido, a15.pnombre, a15.snombre, c.idpersona as idc, c.identificacion as idenc, c.papellido as pac, c.sapellido as sac, c.pnombre as pnc, c.snombre as snc 
				  FROM aportes021 a21 INNER JOIN aportes091 a91 ON a21.idparentesco = a91.iddetalledef INNER JOIN aportes015 a15 ON a21.idbeneficiario = a15.idpersona LEFT JOIN aportes015 c ON a21.idconyuge = c.idpersona WHERE a21.idparentesco IN (35,36,37,38) AND a21.idtrabajador=$idt AND (a21.embarga IS NULL OR a21.embarga<>'S')";
			return mssql_query($sql,$this->con->conect);
		}
	}
	
	//para rellenar el combo de cedula padre o madre en formulario de beneficiarios
	function buscar_padres_de_beneficiarios($idt){
if($this->con->conectar()==true){
$sql="select distinct apo21.idbeneficiario as idc,apo15.identificacion,apo15.pnombre,apo15.papellido, apo21.conviven from aportes015 apo15 inner join aportes021 apo21 on apo15.idpersona=apo21.idbeneficiario where apo21.idtrabajador=".$idt." and idparentesco=34";
//echo $sql;
return mssql_query($sql,$this->con->conect);
}
}

//Buscar beneficiario especifico con idbeneficiario , parentesco e idtrabajador
	function buscar_beneficiario_p($idb,$idparentesco,$idt){
if($this->con->conectar()==true){
$sql="select aportes015.identificacion, aportes015.papellido, aportes015.sapellido, aportes015.pnombre,aportes015.snombre,giro,aportes021.idrelacion,aportes021.estado,embarga,idparentesco,idmotivo,fechaestado,fechaasignacion,aportes021.fechaafiliacion from aportes021 inner join aportes015 on idbeneficiario=idpersona where idbeneficiario=".$idb." and idparentesco=".$idparentesco." and idtrabajador=".$idt;
//echo $sql;
return mssql_query($sql,$this->con->conect);
}
}

//En pestaña grupo familiar para actualizar giro y estado en la relacion.
function cambiar_estado_y_giro($idb,$giro,$estado,$idmotivo,$idcon=0){
      if($this->con->conectar()==true){
      	if($idcon==0){
	   		$sql="UPDATE aportes021 SET giro='$giro',estado='$estado',idmotivo=$idmotivo,fechaestado=cast(getdate() as date)  WHERE idbeneficiario=$idb";
      	}
      	else{
      		$sql="UPDATE aportes021 SET giro='$giro',estado='$estado',idmotivo=$idmotivo,fechaestado=cast(getdate() as date),idconyuge='$idcon'  WHERE idbeneficiario=$idb";
      	}
	   return mssql_query($sql,$this->con->conect);
	  }
}

function buscar_si_ben_padre_activo($CedulaBeneficiario){
	if($this->con->conectar()==true){
		$sql="select idtrabajador,identificacion from aportes015 
                inner join aportes021 on aportes015.idpersona = aportes021.idbeneficiario 
        	  where identificacion='".$CedulaBeneficiario."' and aportes021.estado = 'A' and idparentesco = 36";
		//echo $sql;
		return mssql_query($sql,$this->con->conect);
	}
}

function buscar_relaciones_activas_beneficiario_hijo_hijastro($idben){
	if($this->con->conectar()==true){
		$sql="SELECT count(idrelacion) cuenta FROM aportes021 WHERE estado='A' AND idbeneficiario=".$idben;
		//echo $sql;
		return mssql_query($sql,$this->con->conect);
	}
}

	/** Trae todas las relaciones activas del beneficiario **/
	function buscarRelacionesActivas($idb){
		if($this->con->conectar()==true){
			$con=0;
			$sql = "SELECT a21.idtrabajador,a21.estado,a21.idparentesco,a21.conviven,a91.detalledefinicion,td.codigo,a15.identificacion,a15.pnombre,a15.snombre,a15.papellido,a15.sapellido,a15.sexo 
					FROM aportes021 a21 INNER JOIN aportes015 a15 ON a15.idpersona=a21.idtrabajador INNER JOIN aportes091 a91 ON a91.iddetalledef=a21.idparentesco LEFT JOIN aportes091 td ON td.iddetalledef=a15.idtipodocumento
					WHERE a21.idbeneficiario=$idb";
			//echo $sql;
			return mssql_query($sql,$this->con->conect);
		}
	}
	
	/** Trae todas las relaciones activas del conyuge **/
	function buscarRelacionesActivasConyuge($idb){
		if($this->con->conectar()==true){
			$con=0;
			$sql = "SELECT a21.idtrabajador,a21.idbeneficiario,a21.estado,a21.idparentesco,a21.conviven,a91.detalledefinicion,td.codigo,a15.identificacion,a15.pnombre,a15.snombre,a15.papellido,a15.sapellido,a15.sexo
					FROM aportes021 a21 INNER JOIN aportes015 a15 ON a15.idpersona=a21.idtrabajador INNER JOIN aportes091 a91 ON a91.iddetalledef=a21.idparentesco LEFT JOIN aportes091 td ON td.iddetalledef=a15.idtipodocumento
					WHERE a21.idbeneficiario=$idb
					UNION
					SELECT a21.idtrabajador,a21.idbeneficiario,a21.estado,a21.idparentesco,a21.conviven,a91.detalledefinicion,td.codigo,a15.identificacion,a15.pnombre,a15.snombre,a15.papellido,a15.sapellido,a15.sexo 
					FROM aportes021 a21 INNER JOIN aportes015 a15 ON a15.idpersona=a21.idbeneficiario INNER JOIN aportes091 a91 ON a91.iddetalledef=a21.idparentesco LEFT JOIN aportes091 td ON td.iddetalledef=a15.idtipodocumento 
					WHERE a21.idtrabajador=$idb AND a21.idparentesco=34 AND 0=isnull((SELECT count(b21.idrelacion) FROM aportes021 b21 WHERE b21.idbeneficiario=a21.idtrabajador AND b21.idtrabajador=a21.idbeneficiario AND b21.idparentesco=34),0)";
			//echo $sql;
			return mssql_query($sql,$this->con->conect);
		}
	}
	
	/***********************************
	 *  SELECT
	* *********************************/
	/**
	 * METODO Encargado de obtener el grupo familiar del trabajador
	 *
	 * El parametro debe ser un array indice valor array("atributo"=>valor,...)
	 * @param unknown_type $arrAtributoValor
	 * @return multitype:NULL | array
	 */
	function busca_compl_grupo_famil($arrAtributoValor){
		/*
		 * NOTA: Ha este metodo se le puede agregar mas filtro y atributos a la consulta.
		* 		Pero no se pueden alterar los que ya existen
		* */
	
		$attrEntidad = array(
				array("nombre"=>"idtrabajador","tipo"=>"NUMBER","entidad"=>"a21")
				,array("nombre"=>"idbeneficiario","tipo"=>"NUMBER","entidad"=>"a21")
				,array("nombre"=>"identificacion","tipo"=>"NUMBER","entidad"=>"a15"));
	
		$filtroSql = Utilitario::obtenerFiltro($attrEntidad,$arrAtributoValor);
		$sql = "SELECT 
					a21.idrelacion,a21.idtrabajador,a21.idbeneficiario, a21.idparentesco, a91.detalledefinicion AS parentesco, a21.estado AS estado_relacion
					, a15.pnombre + ' ' + isnull(a15.snombre,'') + ' ' + a15.papellido + ' ' + isnull(a15.sapellido,'') AS beneficiario
					, a15.fechanacimiento,datediff(dd,a15.fechanacimiento,getdate())/365.25 AS edad_beneficiario
					, a15.estado AS estad_perso_benef
				FROM aportes021 a21
					INNER JOIN aportes091 a91 ON a21.idparentesco=a91.iddetalledef
					INNER JOIN aportes015 a15 ON a21.idbeneficiario=a15.idpersona 
					INNER JOIN aportes015 b15 ON a21.idtrabajador=b15.idpersona 
				$filtroSql
				ORDER BY a21.idparentesco";
	
		return $this->fetchConsulta($sql);
	}
	
	/**
	* Retorna los valores obtenidos en la consulta
	* @return multitype:array
	*/
	private function fetchConsulta($querySql){
		$resultado = array();
		$rs = $this->db->querySimple($querySql);
		while($row = $rs->fetch())
			$resultado[] = array_map("trim",$row);
		//$resultado[] = array_map("utf8_encode",$row);
		return $resultado;
	}
	
}
?>