<?php
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");

$root= $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $_SESSION['RAIZ'] . DIRECTORY_SEPARATOR . 'rsc/pdo/IFXDbManejador.php';

class Defuncion{
	private $db;
	
/** CONSTRUCTOR **/
	function Defuncion(){
		$this->db = IFXDbManejador::conectarDB();
		if($this->db->conexionID==null){
			$cadena = $this->db->error;
			echo msg_error($cadena);
			exit();
		}
 	}
	
/** CREATE **/
 	 	 	
/** READ **/
 	 	
 	function buscarDefunciones($idp){
 		$data = array(); $con = 0;
 		$sql = "SELECT m15.idpersona Fidpersona, m15.identificacion Fidentificacion,m15.pnombre Fpnombre,m15.snombre Fsnombre,m15.papellido Fpapellido,m15.sapellido Fsapellido,max(a19.fechadefuncion) fechadefuncion
				FROM aportes019 a19 INNER JOIN aportes015 m15 ON a19.idfallecido=m15.idpersona 
				WHERE a19.idtrabajador=$idp GROUP BY m15.identificacion,m15.idpersona,m15.pnombre,m15.snombre,m15.papellido,m15.sapellido";
 		$rs = $this->db->querySimple($sql);
 		while($row = $rs->fetch()){
 		$con++;
 		$data[]=array_map("utf8_encode",$row);
	 	}
	 	if($con>0) {
	 		return $data;
	 	} else
	 			return 0;
 	}
 	
 	function buscarBeneficiarioDefuncion($idt,$idf){
 		$data = array(); $con = 0;
 		$sql = "SELECT isnull(a19.idradicacion,'') idradicacion,a19.fechasistema,t15.idpersona Tidpersona, t15.identificacion Tidentificacion,t15.pnombre Tpnombre,t15.snombre Tsnombre,t15.papellido Tpapellido,t15.sapellido Tsapellido,
				b15.idpersona Bidpersona, b15.identificacion Bidentificacion,b15.pnombre Bpnombre,b15.snombre Bsnombre,b15.papellido Bpapellido,b15.sapellido Bsapellido,
				a19.idtrabajador,substring(convert(VARCHAR,a19.fechasistema,112),0,7) periodo
				FROM aportes020 a20 INNER JOIN aportes019 a19 ON a19.iddefuncion=a20.iddefuncion AND a19.idtrabajador=$idt AND a19.idfallecido=$idf
				INNER JOIN aportes015 t15 ON t15.idpersona=a20.idtercero INNER JOIN aportes015 b15 ON b15.idpersona=a20.idbeneficiario";
 		$rs = $this->db->querySimple($sql);
 		while($row = $rs->fetch()){
 			$con++;
 			$data[]=array_map("utf8_encode",$row);
 		}
 		if($con>0) {
 			return $data;
 		} else
 			return 0;
 	}
 	
 	function buscarDetalleDefuncion($idtrabajador,$idbeneficiario){
 		$data = array(); $con = 0;
 		$sql="SELECT a9.idcuota,a9.periodo,a9.fechagiro,a9.valor,a9.anulado
			  FROM aportes009 a9
			  WHERE isnumeric(a9.periodo)=1 AND a9.idtrabajador=$idtrabajador AND a9.idbeneficiario=$idbeneficiario AND a9.codigopago='03=MUERTOS' 
			UNION
			  SELECT a14.idcuota,a14.periodo,a14.fechagiro,a14.valor,a14.anulado 
			  FROM aportes014 a14
			  WHERE a14.idtrabajador=$idtrabajador AND a14.idbeneficiario=$idbeneficiario AND a14.codigopago='03=MUERTOS'"; 		
 		$rs = $this->db->querySimple($sql);
 		while($row = $rs->fetch()){
 		$con++;
 		$data[]=array_map("utf8_encode",$row);
 		}
 			if($con>0) {
 			return $data;
 		} else
 		return 0;
 		}
 	
/** UPDATE **/
 	
/** DELETE **/
}	
?>