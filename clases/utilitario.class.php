<?php
class Utilitario{
	
	
	/**
	 * Formar el filtro para realizar las consultas
	 * El parametro $arrDatosAtributos debe ser de este modelo:
	 * 	array("nombre"=>atributo,"tipo"=>,"entidad"=>)
	 * 	El tipo puede ser NUMBER|TEXT|DATE
	 *
	 * El parametro $arrAtributosValor debe ser un array indice valor:
	 * 	array("atributo"=>valor,...)
	 * 
	 * Retorna el filtro
	 * @param unknown_type $arrNombrAtributos
	 * @param unknown_type $valorAtributos
	 * return string filtro 
	 */
	public static function obtenerFiltro($arrDatosAtributos,$arrAtributosValor){
		$filtroSql = "";
		$atributo;
		foreach($arrDatosAtributos as $arrDatoAtributo){
			if(isset($arrAtributosValor[$arrDatoAtributo["nombre"]])
					&& (($arrDatoAtributo["tipo"]!="DATE" && (!empty($arrAtributosValor[$arrDatoAtributo["nombre"]])))
		 				|| ($arrDatoAtributo["tipo"]=="DATE" && !empty($arrAtributosValor[$arrDatoAtributo["nombre"]]["I"])
		 						&& !empty($arrAtributosValor[$arrDatoAtributo["nombre"]]["F"])))){
				$atributo = "";
				if($arrDatoAtributo["tipo"]=="NUMBER"){
					$atributo = $arrDatoAtributo["entidad"].".".$arrDatoAtributo["nombre"]."=".$arrAtributosValor[$arrDatoAtributo["nombre"]];
				}else if($arrDatoAtributo["tipo"]=="TEXT"){
					$atributo = $arrDatoAtributo["entidad"].".".$arrDatoAtributo["nombre"]."='".$arrAtributosValor[$arrDatoAtributo["nombre"]]."'";
				}else if($arrDatoAtributo["tipo"]=="DATE"){
					$atributo = $arrDatoAtributo["entidad"].".".$arrDatoAtributo["nombre"]
					." BETWEEN '".$arrAtributosValor[$arrDatoAtributo["nombre"]]["I"]
					."' AND '".$arrAtributosValor[$arrDatoAtributo["nombre"]]["F"]."'";
				}
				$filtroSql .= $atributo.",";
			}
		}
		$filtroSql = str_replace(',',' AND ',trim($filtroSql,','));
		$filtroSql = empty($filtroSql)?"":"WHERE ".$filtroSql;
		return $filtroSql;
	}
	
	/**
	 * Retorna los valores obtenidos en la consulta
	 * @return multitype:array
	 */
	public static function fetchConsulta($querySql,$con){
		$resultado = array();
		$rs = $con->querySimple($querySql);
		while($row = $rs->fetch())
			$resultado[] = array_map("trim",$row);
		//$resultado[] = array_map("utf8_encode",$row);
		return $resultado;
	}
	
	static function format_nit($nit) {
	  // nit debe ser 900316300-4
	  $nit = preg_replace("/[^0-9]/", "", $nit);
	  $length = strlen($nit);
	  //debug($length);
	  switch($length) {
	  case 6:
	   return preg_replace("/([0-9]{3})([0-9]{3})/", "$1.$2", $nit);
	  break;
	  case 7:
	   return preg_replace("/([0-9]{1})([0-9]{3})([0-9]{3})/", "$1.$2.$3", $nit);
	  break;
	  case 8:
	   return preg_replace("/([0-9]{2})([0-9]{3})([0-9]{3})/", "$1.$2.$3", $nit);
	  break;
	  case 9:
	   return preg_replace("/([0-9]{3})([0-9]{3})([0-9]{3})/", "$1.$2.$3", $nit);
	  break;
	  case 10:
	   return preg_replace("/([0-9]{1})([0-9]{3})([0-9]{3})([0-9]{3})/", "$1.$2.$3.$4", $nit);
	  break;
	  case 11:
		return preg_replace("/([0-9]{2})([0-9]{3})([0-9]{3})([0-9]{3})/", "$1.$2.$3.$4", $nit);
	  case 12:
		return preg_replace("/([0-9]{3})([0-9]{3})([0-9]{3})([0-9]{3})/", "$1.$2.$3.$4", $nit);
	  default:
		return $nit;
	  break;
	  }
	}
} 
