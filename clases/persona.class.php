<?php
/* autor:       orlando puentes
 * fecha:       12/10/2010
 * objetivo:     
 */
 
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'conexion.class.php';

class Persona{
 //constructor	
var $con;
var $fechaSistema;
function Persona(){
 		$this->con=new DBManager;
 	}
	
function insertSimple($campos){
		if($this->con->conectar()==true){
			$sql="INSERT INTO aportes015 (identificacion,idtipodocumento,papellido,sapellido,pnombre,snombre,usuario,fechasistema) VALUES ('".$campos[0]."','".$campos[1]."','".$campos[2]."','".$campos[3]."','".$campos[4]."','".$campos[5]."','".$campos[6]."',cast (getdate() as date)";
			//echo $sql;
			return mssql_query($sql,$this->con->conect);
		}
	}	

function insertar($campos){
		if($this->con->conectar()==true){
			$fechaSistema=date("m/d/Y");
			//print_r($campos);
			$sql="INSERT INTO aportes090 (definicion,concepto,fechacreacion,usuario) VALUES ('".$campos[1]."','".$campos[2]."','".$fechaSistema."','".$campos[4]."')";
			return mssql_query($sql,$this->con->conect);
		}
	}
 
function mostrar_registro($id){
		if($this->con->conectar()==true){
			$sql="SELECT aportes015.*,aportes091.detalledefinicion AS ti,ap2.detalledefinicion AS ec FROM aportes015 
INNER JOIN aportes091 ON aportes015.idtipodocumento=aportes091.iddetalledef
LEFT JOIN aportes091 ap2 ON aportes015.idestadocivil=ap2.iddetalledef WHERE identificacion='$id'";
			return mssql_query($sql,$this->con->conect);
		}
	}

function mostrar_registro_por_documento($idTipoDocumento, $numDocumento){
		if($this->con->conectar()==true){
			$sql="SELECT aportes015.*,aportes091.detalledefinicion AS ti,ap2.detalledefinicion AS ec FROM aportes015 
INNER JOIN aportes091 ON aportes015.idtipodocumento=aportes091.iddetalledef
LEFT JOIN aportes091 ap2 ON aportes015.idestadocivil=ap2.iddetalledef WHERE identificacion='$numDocumento' AND idtipodocumento=$idTipoDocumento";
			return mssql_query($sql,$this->con->conect);
		}
	}

function actualizar($campos){
		if($this->con->conectar()==true){
			//print_r($campos);
			$sql="UPDATE aportes090 SET definicion = '".$campos[1]."', concepto = '".$campos[2]."' WHERE iddefinicion = ". $campos[0];	
			return mssql_query($sql,$this->con->conect);
		}
	}	
 
function eliminar($id){
	if($this->con->conectar()==true){
		$sql="DELETE FROM aportes090 WHERE iddefinicion=".$id;
		return mssql_query($sql,$this->con->conect);
		}
	} 
	
function borrar_persona($idp){
	if($this->con->conectar()==true){
		$sql="DELETE FROM aportes015 WHERE idpersona=".$idp;
		return mssql_query($sql,$this->con->conect);
		}
	} 
		
function mostrar_datos(){
		if($this->con->conectar()==true){
			return mssql_query("SELECT * FROM aportes090 order by definicion",$this->con->conect);
		}
	}	
	
function mostrar_registro2($idtd,$numero){
		if($this->con->conectar()==true){
			$sql="SELECT aportes015.*, ap2.detalledefinicion AS ec FROM aportes015 LEFT JOIN aportes091 ap2 ON aportes015.idestadocivil=ap2.iddetalledef WHERE  idtipodocumento='$idtd' and identificacion='$numero'";
			return mssql_query($sql,$this->con->conect);
		}
	}
	
function obtener_trabajadores($criteriosBusqueda = array()){
		if($this->con->conectar()==true){
			
			$arrayCondiciones = array();
			$arrayCondiciones[] = "t.estado = 'A'";
			
			/**
			 * Condiciones para agregar en la consulta, se puede agregar las que se necesiten
			 */
			if(isset($criteriosBusqueda["idagencia"]))
				$arrayCondiciones[] = "af.idagencia = '". $criteriosBusqueda["idagencia"] ."'";
			
			if(count($arrayCondiciones)>0)
				$strCondiciones = " WHERE ". implode(" AND ",$arrayCondiciones);
			
			$sql = "SELECT
						t.idpersona,
						td.codigo as tipo_documento,
						t.identificacion,
						t.papellido,
						t.sapellido,
						t.pnombre,
						t.snombre,
						t.direccion as direccion_afiliado,
						t.telefono as telefono_afiliado,
						t.idzona,
						z.zona,
						t.fechanacimiento,
						t.estado as estado_afiliado,
						af.estado as estado_afiliacion,
						af.fechaingreso,
						af.salario,
						af.tipoformulario,
						taf.detalledefinicion as tipo_formulario,
						e.nit,
						e.razonsocial,
						e.codigosucursal,
						e.direccion as direccion_empresa,
						e.telefono as telefono_empresa
					  FROM 
						aportes015 t 
						inner join aportes016 af on af.idpersona=t.idpersona
						inner join aportes091 taf on taf.iddetalledef=af.tipoformulario
						inner join aportes091 td on td.iddetalledef=t.idtipodocumento
						inner join aportes089 z on z.codzona = t.idzona
						inner join aportes048 e on e.idempresa = af.idempresa " . $strCondiciones;
			return mssql_query($sql,$this->con->conect);
		}
	}
		
 }	
?>