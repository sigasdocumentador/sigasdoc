<?php
/* autor:       orlando puentes
 * fecha:       12/09/2010
 * objetivo:     
 */
 
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

ini_set('memory_limit','64M');
include_once $raiz . DIRECTORY_SEPARATOR . 'rsc' . DIRECTORY_SEPARATOR . 'conexion.class.php';
class GrupoEmbargo{
 //constructor
var $con;
function GrupoEmbargo(){
 		$this->con=new DBManager;
 	}

function buscar_grupo_embargo1($idt,$idc){
    if($this->con->conectar()==TRUE){
        $sql="select aportes021.idbeneficiario, aportes021.idparentesco, aportes021.giro,aportes021.embarga, aportes091.detalledefinicion,
	aportes015.idpersona, aportes015.identificacion, aportes015.papellido, aportes015.sapellido, aportes015.pnombre,
	aportes015.snombre, c.idpersona as idc, c.identificacion as idenc, c.papellido as pac, c.sapellido as sac, c.pnombre as pnc, c.snombre as snc FROM aportes021
INNER JOIN aportes091 ON aportes021.idparentesco = aportes091.iddetalledef INNER JOIN aportes015 ON aportes021.idbeneficiario = aportes015.idpersona
LEFT JOIN aportes015 c ON aportes021.idconyuge = c.idpersona WHERE aportes021.idparentesco in (35,38) AND idtrabajador=$idt and idconyuge=$idc AND (embarga='N' OR embarga is null)";
        return mssql_query($sql,$this->con->conect);
    }
}

function buscar_grupo_embargo2($idt,$idc){
    if($this->con->conectar()==TRUE){
        $sql="select aportes021.idbeneficiario, aportes021.idparentesco, aportes021.giro,aportes021.embarga, aportes091.detalledefinicion,
	aportes015.idpersona, aportes015.identificacion, aportes015.papellido, aportes015.sapellido, aportes015.pnombre,
	aportes015.snombre, c.idpersona as idc, c.identificacion as idenc, c.papellido as pac, c.sapellido as sac, c.pnombre as pnc, c.snombre as snc FROM aportes021
INNER JOIN aportes091 ON aportes021.idparentesco = aportes091.iddetalledef INNER JOIN aportes015 ON aportes021.idbeneficiario = aportes015.idpersona
LEFT JOIN aportes015 c ON aportes021.idconyuge = c.idpersona WHERE aportes021.idparentesco =36 AND idtrabajador=$idt AND (embarga='N' OR embarga is null)";
        //echo $sql;
        return mssql_query($sql,$this->con->conect);
    }
}

function buscar_grupo_embargo3($idt,$idc){
    if($this->con->conectar()==TRUE){
        $sql="select aportes021.idbeneficiario, aportes021.idparentesco, aportes021.giro,aportes021.embarga, aportes091.detalledefinicion,
	aportes015.idpersona, aportes015.identificacion, aportes015.papellido, aportes015.sapellido, aportes015.pnombre,
	aportes015.snombre, c.idpersona as idc, c.identificacion as idenc, c.papellido as pac, c.sapellido as sac, c.pnombre as pnc, c.snombre as snc FROM aportes021
INNER JOIN aportes091 ON aportes021.idparentesco = aportes091.iddetalledef INNER JOIN aportes015 ON aportes021.idbeneficiario = aportes015.idpersona
LEFT JOIN aportes015 c ON aportes021.idconyuge = c.idpersona WHERE aportes021.idparentesco in(35,36) AND idtrabajador=$idt AND (embarga='N' OR embarga is null)";
        //echo $sql;
        return mssql_query($sql,$this->con->conect);
    }
}

function buscar_conyuge($idt,$idc){
	if($this->con->conectar()==true){
	$sql="SELECT count(*) as cuenta FROM aportes021 WHERE idtrabajador=$idt AND idconyuge=$idc AND idparentesco=34";
	//echo $sql;
        return mssql_query($sql,$this->con->conect);
	}
	}

        function buscar_padre($idt,$idc){
	if($this->con->conectar()==true){
	$sql="SELECT count(*) as cuenta FROM aportes021 WHERE idtrabajador=$idt AND idbeneficiario=$idc AND idparentesco=36";
	//echo $sql;
        return mssql_query($sql,$this->con->conect);
	}
	}

}
?>       