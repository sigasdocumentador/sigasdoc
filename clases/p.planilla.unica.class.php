<?php
/* autor:       orlando puentes
 * fecha:       17/07/2010
 * objetivo:     
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz . DIRECTORY_SEPARATOR . 'rsc' . DIRECTORY_SEPARATOR . 'conexion.class.php';

class Planilla{	
	var $con;
	
	function Planilla(){
 		$this->con=new DBManager;
 	}
	
 	/**
 	 * Metodo para realizar el traslado de planillas
 	 *
 	 * @param String $idPlanillas [ejemplo(id1,id2,)]
 	 * @param int $idEmpresaTraslado 
 	 * @param int $nitTraslado 
 	 * @param String $periodoTraslado 
 	 */
 	function actualizar_traslado_planilla($usuario,$idPlanillas,$idEmpresaTraslado=null,$nitTraslado=null,$periodoTraslado=null){
 		if($this->con->conectar()==true){
 			$camposSql = "";
 			if($idEmpresaTraslado!=null && $nitTraslado!=null)
 				$camposSql .= "idempresa=".$idEmpresaTraslado.",nit='$nitTraslado',";
 			if($periodoTraslado!=null)
 				$camposSql .= "periodo=".$periodoTraslado.",";
 				
 			$sql="UPDATE aportes010 SET $camposSql usuariomodifica='{$_SESSION["USUARIO"]}'
 					, fechamodifica=cast(getdate() as date)
 					, fechatraslado=cast(getdate() as date)
 					, usuariotraslado='$usuario'
 					WHERE idplanilla in ($idPlanillas)";
 	
 			return mssql_query($sql,$this->con->conect);
 		}
 	}
 	
	function buscar_planillas($ide,$per=0){	
		if($this->con->conectar()==true){
			$sql="	SELECT 
					    isnull( a59.valor, 0 ) AS devolucion,
					    a10.idplanilla, a10.tipodocumento, a10.identificacion, a10.planilla, a10.valoraporte, 
					    a10.papellido,a10.pnombre,a10.salariobasico, a10.ingresobase, a10.diascotizados,
					    a10.ingreso,a10.retiro,a10.var_tra_salario,a10.var_per_salario,a10.sus_tem_contrato,
					    a10.inc_tem_emfermedad,a10.lic_maternidad,a10.vacaciones,a10.inc_tem_acc_trabajo,
					    a10.tipo_cotizante,a10.horascotizadas,a10.procesado, a10.fechapago, a10.periodo, a10.correccion,
					    a10.usuario, a10.fechasistema, a10.usuariomodifica, a10.fechamodifica
					FROM aportes010 a10
						INNER JOIN aportes011 a11 ON a11.planilla=a10.planilla
						LEFT JOIN aportes058 a58 ON a58.idaporte=a11.idaporte
						LEFT JOIN aportes059 a59 ON a59.iddevolucion=a58.iddevolcion AND a59.idpersona=a10.idtrabajador
			 		WHERE a10.idempresa=$ide AND a10.periodo='$per' --ORDER BY papellido";
			//echo $sql;
			return mssql_query($sql,$this->con->conect);
		}
	}
	
//Buscar planilla por nit, periodo, tipo de documento e identificacion del afiliado
function buscar_planillas_persona($nit,$per,$td,$id){
		if($this->con->conectar()==true){
			$sql="select a10.idplanilla,a10.nit,a48.razonsocial,isnull(a15.pnombre,'') pnombre,isnull(a15.papellido,'') papellido,
					isnull(a15.snombre,'') snombre,isnull(a15.sapellido,'') sapellido, a10.idtrabajador idpersona2,
				    a10.planilla,a10.valoraporte,a10.salariobasico,a10.ingresobase,a10.diascotizados,a10.periodo, a15.idpersona
				  from aportes010 a10 
  	  			    left join aportes048 a48 
    				  on a48.nit=a10.nit 
  					left join aportes015 a15
    				  on a15.identificacion=a10.identificacion and a15.idtipodocumento=$td 
			      where a10.nit='".$nit."' and a10.periodo='".$per."' and a10.identificacion='".$id."'";	//aportes010 tiene diferentes tipos de idtipodocumento no contemplados
			//echo $sql;
			return mssql_query($sql,$this->con->conect);
		}
}

//Buscar planilla por nit, periodo, planilla, tipo de documento e identificacion del afiliado
function buscar_planilla_persona($nit,$per,$td,$id,$planilla){
	if($this->con->conectar()==true){
		$sql="select a10.idplanilla,a10.nit,a48.razonsocial,isnull(a15.pnombre,'') pnombre,isnull(a15.papellido,'') papellido,
		isnull(a15.snombre,'') snombre,isnull(a15.sapellido,'') sapellido, a10.idtrabajador idpersona2,
		a10.planilla,a10.valoraporte,a10.salariobasico,a10.ingresobase,a10.diascotizados,a10.periodo, a15.idpersona
		from aportes010 a10
		left join aportes048 a48
		on a48.nit=a10.nit
		left join aportes015 a15
		on a15.identificacion=a10.identificacion and a15.idtipodocumento=$td
		where a10.nit='".$nit."' and a10.periodo='".$per."' and a10.identificacion='".$id."' and a10.planilla='$planilla'";
		//echo $sql;
		return mssql_query($sql,$this->con->conect);
	}
}

//Buscar planilla por idplanilla
	function buscar_planillas_idplanilla($idp){
		if($this->con->conectar()==true){
			$sql="select a10.idplanilla,a10.nit,a48.razonsocial,a15.idtipodocumento,a15.idpersona, a10.identificacion,isnull(a15.pnombre,'') pnombre,isnull(a15.snombre,'') snombre,isnull(a15.papellido,'') papellido,isnull(a15.sapellido,'') sapellido,
			a10.planilla,a10.valoraporte,a10.salariobasico,a10.ingresobase,a10.diascotizados,a10.horascotizadas,a10.periodo
			from aportes010 a10
			inner join aportes048 a48
			on a48.nit=a10.nit
			inner join aportes015 a15
			on a15.identificacion=a10.identificacion
			where a10.idplanilla=$idp";
			//echo $sql;
			return mssql_query($sql,$this->con->conect);
		}
	}
	
	//Buscar planillas Consulta trab
	function buscar_planillas_consulta($idp,$lim=500){
		if($this->con->conectar()==true){
			//$sql="select TOP $lim idplanilla, planilla, aportes010.nit, aportes048.razonsocial as rsocial, salariobasico, ingresobase, diascotizados, isnull(ingreso,'')ingreso, isnull(retiro,'')retiro, isnull(var_tra_salario,'')var_tra_salario, isnull(var_per_salario,'')var_per_salario, isnull(sus_tem_contrato,'')sus_tem_contrato, isnull(inc_tem_emfermedad,'')inc_tem_emfermedad, isnull(lic_maternidad,'')lic_maternidad, isnull(vacaciones,'')vacaciones, isnull(inc_tem_acc_trabajo,'')inc_tem_acc_trabajo, isnull(tipo_cotizante,'')tipo_cotizante, isnull(periodo,'')periodo, correccion, aportes010.idempresa, idtrabajador, horascotizadas, procesado, control,  fechapago, aportes010.fechasistema, usuariomodifica, fechamodifica, valoraporte from aportes010 inner join aportes048 on aportes048.nit=aportes010.nit where idtrabajador=$idp ORDER BY periodo desc";
			$sql="SELECT TOP $lim a10.idplanilla, a10.planilla, a10.nit, a48.razonsocial AS rsocial, 
					    a10.salariobasico, a10.ingresobase, a10.diascotizados, isnull(a10.ingreso,'')ingreso, isnull(a10.retiro,'')retiro, 
					    isnull(a10.var_tra_salario,'')var_tra_salario, isnull(a10.var_per_salario,'')var_per_salario, 
					    isnull(a10.sus_tem_contrato,'')sus_tem_contrato, isnull(a10.inc_tem_emfermedad,'')inc_tem_emfermedad, 
					    isnull(a10.lic_maternidad,'')lic_maternidad, isnull(a10.vacaciones,'')vacaciones, isnull(a10.inc_tem_acc_trabajo,'')inc_tem_acc_trabajo,
					    isnull(a10.tipo_cotizante,'')tipo_cotizante, isnull(a10.periodo,'')periodo, a10.correccion, a10.idempresa, 
					    a10.idtrabajador, a10.horascotizadas, a10.procesado, a10.control,  a10.fechapago, a10.fechasistema, a10.usuariomodifica, 
					    a10.fechamodifica, a10.valoraporte	,a10.tarifaaporte, a10.novedad_sln					
						,isnull((select a59.valor from aportes059 a59 inner join aportes058 a58 on a58.idaporte=a11.idaporte and a58.iddevolcion=a59.iddevolucion where a59.idpersona=a10.idtrabajador),'0') as devolucion
					FROM aportes010 a10
					    LEFT JOIN aportes048 a48 ON a48.nit=a10.nit 
					    LEFT JOIN aportes011 a11 ON a11.planilla=a10.planilla and a11.periodo=a10.periodo                        
					WHERE a10.idtrabajador=$idp";					
			return mssql_query($sql,$this->con->conect);
		}
	}
	
	//Buscar planillas Correccion trab
	function buscar_planillas_correccion($idp,$idPlanilla){
		if($this->con->conectar()==true){
			$sql="SELECT a10.idplanilla, a10.planilla, a10.nit, a48.razonsocial AS rsocial,
					a10.salariobasico, a10.ingresobase, a10.diascotizados, isnull(a10.ingreso,'')ingreso, isnull(a10.retiro,'')retiro,
					isnull(a10.var_tra_salario,'')var_tra_salario, isnull(a10.var_per_salario,'')var_per_salario,
					isnull(a10.sus_tem_contrato,'')sus_tem_contrato, isnull(a10.inc_tem_emfermedad,'')inc_tem_emfermedad,
					isnull(a10.lic_maternidad,'')lic_maternidad, isnull(a10.vacaciones,'')vacaciones, isnull(a10.inc_tem_acc_trabajo,'')inc_tem_acc_trabajo,
					isnull(a10.tipo_cotizante,'')tipo_cotizante, isnull(a10.periodo,'')periodo, a10.correccion, a10.idempresa,
					a10.idtrabajador, a10.horascotizadas, a10.procesado, a10.control,  a10.fechapago, a10.fechasistema, a10.usuariomodifica,
					a10.fechamodifica, a10.valoraporte,
					isnull(a59.valor,0)devolucion
				FROM auditoria010 a10
					LEFT JOIN aportes048 a48 ON a48.nit=a10.nit 
					LEFT JOIN aportes011 a11 ON a11.planilla=a10.planilla
                    LEFT JOIN aportes059 a59 on a59.idpersona=a10.idtrabajador
					LEFT JOIN aportes058 a58 ON a58.idaporte=a11.idaporte and a58.iddevolcion=a59.iddevolucion
                    LEFT JOIN aportes059 b59 ON b59.iddevolucion=a58.iddevolcion
				WHERE a10.idtrabajador=$idp AND a10.idplanilla=$idPlanilla
				ORDER BY a10.fechasistema DESC";
			return mssql_query($sql,$this->con->conect);
			}
		}	
}
?>