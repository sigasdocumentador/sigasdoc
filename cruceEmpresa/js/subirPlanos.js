/*
* @autor:      Ing.  Orlando Puentes
* @fecha:      24/09/2010
* objetivo:
*/
/* var URL=src();*/

function mostrarAyuda(){
	$("#ayuda").dialog('open' );
}

function notas(){
	$("#dialog-form2").dialog('open');
}	

function procesar2(op){
	$("#ajax").html('<img src=../imagenes/ajax-loader.gif />');
	$("#boton").html(' ');
	$.ajax({
		url: 'procesarPlanoT.php',
		data: {v0:op},
		type: "POST",
		async:false,
		success: function(datos){
			if(isNaN(datos)){
				$("#ajax").html("Ocurrio un error con el procesamiento de su solicitud.");
			} else if(parseInt(datos)==-2){
				$("#ajax").html("Ocurrio un error con la conexion a la base de datos.");
			} else if(parseInt(datos)==-1){
				$("#ajax").html("No se encontraron cedulas que procesar.");
			} else if(parseInt(datos)==0) {
				$("#ajax").html("Las cedulas procesadas no arrojaron ningun resultado que mostrar.");
			}else {
				$("#ajax").html("<small>Informacion procesada satisfactoriamente.</b></small><br><br><label style='cursor:pointer' onClick='descargar();'><img src=../imagenes/descargarArchivo.png width=143 height=30 /></label>");
			} 
		}
	});	
	
}

function procesar(archivo){
	$("#ajax").html('<img src=../imagenes/ajax-loader.gif />');
	$("#boton").html(' ');
	$.ajax({
		url: 'procesarPlano.php',
		data: {v0:archivo},
		type: "POST",
		async:false,
		success: function(datos){
			if(isNaN(datos)){
				$("#ajax").html("Ocurrio un error con el procesamiento de su solicitud.");
			} else if(parseInt(datos)==-2){
				$("#ajax").html("Ocurrio un error con la conexion a la base de datos.");
			} else if(parseInt(datos)==-1){
				$("#ajax").html("No se encontraron cedulas que procesar.");
			} else if(parseInt(datos)==0) {
				$("#ajax").html("Las cedulas procesadas no arrojaron ningun resultado que mostrar.");
			}else {
				$("#ajax").html("<small>Informacion procesada satisfactoriamente.</b></small><br><br><label style='cursor:pointer' onClick='descargar();'><img src=../imagenes/descargarArchivo.png width=143 height=30 /></label>");
			} 
		}
	});	
}


function procesar3(archivo){
	$("#ajax").html('<img src=../imagenes/ajax-loader.gif />');
	$("#boton").html(' ');
	$.ajax({
		url: 'procesarPlanoEstadoP.php',
		data: {v0:archivo},
		type: "POST",
		async:false,
		success: function(datos){
			if(isNaN(datos)){
				$("#ajax").html("Ocurrio un error con el procesamiento de su solicitud.");
			} else if(parseInt(datos)==-2){
				$("#ajax").html("Ocurrio un error con la conexion a la base de datos.");
			} else if(parseInt(datos)==-1){
				$("#ajax").html("No se encontraron cedulas que procesar.");
			} else if(parseInt(datos)==0) {
				$("#ajax").html("Las cedulas procesadas no arrojaron ningun resultado que mostrar.");
			}else {
				$("#ajax").html("<small>Informacion procesada satisfactoriamente.</b></small><br><br><label style='cursor:pointer' onClick='descargar();'><img src=../imagenes/descargarArchivo.png width=143 height=30 /></label>");
			} 
		}
	});	
}

function descargar(){
	var url='../phpComunes/descargaPlanos.php';
	window.open(url);
}
