<?php
set_time_limit ( 0 );
ini_set ( "display_errors", '1' );
date_default_timezone_set ( "America/Bogota" );

$c0=$_REQUEST["v0"];

$root = $_SERVER ['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once $root;
include_once $raiz . DIRECTORY_SEPARATOR . 'config.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'funcionesComunes' . DIRECTORY_SEPARATOR . 'funcionesComunes.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'rsc' . DIRECTORY_SEPARATOR . 'pdo' . DIRECTORY_SEPARATOR . 'IFXDbManejador.php';
include_once $raiz . DIRECTORY_SEPARATOR . 'rsc' . DIRECTORY_SEPARATOR . 'pdo' . DIRECTORY_SEPARATOR . 'IFXerror.php';
$db = IFXDbManejador::conectarDB ();
if ($db->conexionID == null) {
	$cadena = $db->error;
	echo -2;
	exit ();
}

$directorio = $ruta_cargados . "credito".DIRECTORY_SEPARATOR."cargados".DIRECTORY_SEPARATOR;
$dir = opendir ( $directorio );
while ( $elemento = readdir ( $dir ) ) {
	if (strlen ( $elemento ) > 2)
		$archivos [] = $elemento;
}
closedir ( $dir );
$directorioProcesado = $ruta_cargados . "credito/procesados/";
if (!file_exists($directorioProcesado)) {
	mkdir($directorioProcesado, 0777, true);
}
$archivo='reporteEstadoP.csv';
$rutadelplano=$directorioProcesado.DIRECTORY_SEPARATOR.$archivo;
$cadena = "nit;razonsocial;telefono;email;direccion;departmento;municipio;estado;fechaafiliacion;fechaaportes;primerPeriodo;ultPeriodo;totalValorAporte;periodosPagados\r\n";
$fp=fopen($rutadelplano,'w');
fwrite($fp, $cadena);
fclose($fp);

$i=0;

	for(; $i<count($archivos); $i++){
		if($archivos[$i]==$c0){
			$directorio .= $archivos[$i];
			$archivoSubido = file ( $directorio );
			$totalLineas = count ( $archivoSubido );
			$cont=0;	
			for($c = 0; $c < $totalLineas; $c ++) {
				$cont++;
				$linea = $archivoSubido [$c];
				$documento = explode ( ' ', $linea );
				$num=trim($documento[0]);
				buscar($num,$db,$rutadelplano);
			}		
			break;
		}
	}


	if($cont==0){
		echo -1;
		exit();
	}
	
	unlink($directorio);


function buscar($num,$db,$rutadelplano){
	$sql="SELECT DISTINCT b48.nit,b48.razonsocial,b48.telefono,b48.email,b48.direccion,a89.departmento,a89.municipio,b48.estado,
b48.fechaafiliacion,b48.fechaaportes,
(SELECT min(a11.periodo) FROM aportes011 a11 WHERE a11.idempresa=b48.idempresa) AS primerPeriodo,
(SELECT max(a11.periodo) FROM aportes011 a11 WHERE a11.idempresa=b48.idempresa) AS ultPeriodo,
(SELECT sum(a11.valoraporte) FROM aportes011 a11 WHERE a11.idempresa=b48.idempresa) AS totalValorAporte,
(SELECT ';'+periodo
					FROM aportes011 b11
					WHERE b11.idempresa=b48.idempresa
					FOR xml path('')
				) AS periodosPagados
FROM aportes048 b48
LEFT JOIN aportes011 b11 ON b11.idempresa=b48.idempresa
LEFT JOIN aportes089 a89 ON b48.iddepartamento=a89.coddepartamento AND b48.idciudad=a89.codmunicipio
WHERE  b48.estado='A' AND b48.nit IN ('$num')";
	
	$cont=0;
	
	$rs=$db->querySimple($sql);
	while($row=$rs->fetch()){
		$cont++;
		escribirPlano($row,$cont,$rutadelplano);
	}
	
		if( $cont==0 ){
			$cade=$num."; no existe en nuestra base de datos;\r\n";
			$fp=fopen($rutadelplano,'a');
			fwrite($fp, $cade);
			fclose($fp);
		}
		
 }

function escribirPlano($row,$contidad,$rutadelplano){
	$cadena = "$row[nit];$row[razonsocial];$row[telefono];$row[email];$row[direccion];$row[departmento];$row[municipio];$row[estado];$row[fechaafiliacion];$row[fechaaportes];$row[primerPeriodo];$row[ultPeriodo];$row[totalValorAporte];$row[periodosPagados]"."\r\n";
	$fp=fopen($rutadelplano,'a');
	fwrite($fp, $cadena);
	fclose($fp);
}

$_SESSION['ENLACE']=$rutadelplano;
$_SESSION['ARCHIVO']=$archivo;
echo 1;
?>