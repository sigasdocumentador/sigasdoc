<?php
date_default_timezone_set('America/Bogota');
set_time_limit(0);
ini_set('memory_limit','2000M');

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';

include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'utilitario.class.php';
include_once 'maestro_control.class.php';

class MaestroNovedadAyS {
	/** #### ATRIBUTOS #### **/

	/**
	 * Contiene el objeto Conexion
	 * @var Conexion
	 */
	public static $con = null;

	/** #### METODOS MAGICOS #### **/

	function __construct(){
		try{
			self::$con = IFXDbManejador::conectarDB();
			if( self::$con->conexionID==null ){
				throw new Exception(self::$con->error);
			}
		}catch(Exception $e){
			echo $e->getMessage();
			exit();
		}
	}
	
	/**
	 * Eliminar las novedades de aportes442 y aportes443
	 *
	 * @param unknown_type $datos
	 * @param unknown_type $usuario
	 * @return number id_registro_control
	 */
	public function delete_novedad($datos){
		$resultado = 0;
		
		self::$con->inicioTransaccion();
		
		$query = "DELETE
				FROM aportes443
				WHERE id_novedad IN(
					SELECT a442.id_novedad 
					FROM aportes442 a442
						INNER JOIN aportes440 a440 ON a440.id_definicion_novedad=a442.id_definicion_novedad 
					WHERE a440.definicion_novedad IN ('C01','C02','C03','C04','C07','C08','C09','C10')
						AND CONVERT(DATE,a442.fecha_creacion) BETWEEN '{$datos["fecha_inicio"]}' AND '{$datos["fecha_fin"]}'
				)";
		
		$resultado = self::$con->queryActualiza( $query );
		
		if($resultado>0){
			
			$query = "DELETE a442 FROM aportes442 a442
					WHERE CONVERT(DATE,fecha_creacion) BETWEEN '{$datos["fecha_inicio"]}' AND '{$datos["fecha_fin"]}'
						AND 0<(
							SELECT count(*) 
							FROM aportes440 a440 
							WHERE a440.id_definicion_novedad=a442.id_definicion_novedad 
								AND a440.definicion_novedad IN ('C01','C02','C03','C04','C07','C08','C09','C10')
						)";
			$resultado = self::$con->queryActualiza( $query );
		}
		
		if($resultado>0){
			//commit
			self::$con->confirmarTransaccion();
		}else{
			//rollback
			self::$con->cancelarTransaccion();
		}		

		return $resultado;
	}
	
	public function select_maestro_novedad_ays($arrAtributoValor){
		$attrEntidad = array(
				array("nombre"=>"id_registro_control","tipo"=>"NUMBER","entidad"=>"a448")
			);

		$filtroSql = Utilitario::obtenerFiltro($attrEntidad,$arrAtributoValor);
		$sql = "SELECT tipo_registro, codigo_CCF, tipo_ident_afili, numer_ident_afili, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, codigo_novedad, nuevo_valor_1, nuevo_valor_2, nuevo_valor_3, nuevo_valor_4, nuevo_valor_5, nuevo_valor_6, nuevo_valor_7, nuevo_valor_8, nuevo_valor_9, nuevo_valor_10, nuevo_valor_11, nuevo_valor_12, nuevo_valor_13, nuevo_valor_14, nuevo_valor_15
				FROM aportes448 a448
				$filtroSql
				ORDER BY id_maestro_novedad_a_s";
		return Utilitario::fetchConsulta($sql,self::$con);
	}
	
	public function ejecutar($arrDatos,$usuario){
		$idRegistroControl = 0;
		$bandePrepaDatos = false;
		
		//Verificar si existen registros enviados
		$objMaestroControl = new MaestroControl();
		$arrFiltro = array("fecha_inici_perio_infor"=>$arrDatos["fecha_inici_perio_infor"]
				,"fecha_final_perio_infor"=>$arrDatos["fecha_final_perio_infor"]
				,"id_tipo_archivo"=>$arrDatos["id_tipo_archivo"]
				,"enviado"=>'S'
			);
		
		$arrDatosMaestContr = $objMaestroControl->select_registro_control($arrFiltro);
		if(count($arrDatosMaestContr)==0){
			
			$arrFiltro = array("fecha_inicio"=>$arrDatos["fecha_inici_perio_infor"]
					, "fecha_fin"=>$arrDatos["fecha_final_perio_infor"]
					, "enviado"=>$arrDatos["enviado"]);
			
			//preparar los datos si no existen registro
			$idRegistroControl = $this->preparar_datos($arrFiltro,$usuario);
			$bandePrepaDatos = true;
		}else{
			$idRegistroControl = $arrDatosMaestContr[0]["id_registro_control"];
		}
		
		//var_dump($idRegistroControl);exit();
		
		if($idRegistroControl==0) return false;
		
		//obtener el archivo a descargar
		$resultado = $this->fetch_archivo($idRegistroControl,$arrDatos["ruta_generados"]);
		
		if(!$resultado) return false;
		
		$resultado = true;
		
		if($arrDatos["enviado"]=="S"){
			//Actualizar como enviado el registro de control
			$arrDatosUpdat = array("fecha_envio"=>date('Ymd'),"id_registro_control"=>$idRegistroControl);
			$resultado = $objMaestroControl->update_enviado_registro_control($arrDatosUpdat,$usuario);
			
			if($resultado==true){
				$arrFiltro = array("fecha_inicio"=>$arrDatos["fecha_inici_perio_infor"]
						, "fecha_fin"=>$arrDatos["fecha_final_perio_infor"]
						, "enviado"=>$arrDatos["enviado"]);
				
				if($bandePrepaDatos==true){
					//Eliminar las novedades en aportes442 y aportes443
					$resultado = $this->delete_novedad($arrFiltro);
				}
			}
		}
		
		return $resultado;
	}
	
	/**
	 * Preparar los datos para el archivo maestro
	 * 
	 * @param unknown_type $datos
	 * @param unknown_type $usuario
	 * @return number id_registro_control
	 */
	public function preparar_datos($datos, $usuario){
		$resultado = 0;
		$sentencia = self::$con->conexionID->prepare ( "EXEC resolucion1056.sp_Maestro_Novedad_AyS
				@fecha_inicio = '{$datos["fecha_inicio"]}'
				, @fecha_fin = '{$datos["fecha_fin"]}'
				, @enviada = '{$datos["enviado"]}'
				, @usuario = '$usuario'
				, @resultado = :resultado" );
		
		$sentencia->bindParam ( ":resultado", $resultado, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000 );
		$sentencia->execute();
	
		return $resultado;
	}
	
	/**
	 * Preparar los datos para el archivo maestro
	 *
	 * @param unknown_type $datos
	 * @param unknown_type $usuario
	 * @return number id_registro_control
	 */
	public function fetch_archivo($idRegistroControl, $rutaGenerados){
		
		$objMaestroControl = new MaestroControl();
		$arrDatosRegisContr = $objMaestroControl->select_registro_control(array("id_registro_control"=>$idRegistroControl));
		
		$arrDatosMaestNovedad = $this->select_maestro_novedad_ays(array("id_registro_control"=>$idRegistroControl));
				
		if(count($arrDatosRegisContr)==0)
			return false;
		
	
		$nombreArchivo = $arrDatosRegisContr[0]["nombre_archivo"];
		
		//Crear el archivo
		$objMaestroControl = new MaestroControl();
		$path_plano = $objMaestroControl->crear_archivo($nombreArchivo, $rutaGenerados);
		$objMaestroControl->abrir_archivo();
		
		//Adicionar registro Control
		foreach($arrDatosRegisContr as $row){
			$contenidoFila = $row["tipo_registro"]."|".$row["codigo_CCF"]."|".$row["fecha_inici_perio_infor"]."|".$row["fecha_final_perio_infor"]."|".$row["total_registros"]."|".$row["nombre_archivo"]."\r\n";
			$objMaestroControl->escribir_archivo($contenidoFila);
		}
		
		//Adicionar los registros detalles
		foreach ($arrDatosMaestNovedad as $row){
			$contenidoFila = join("|", $row)."\r\n";
			$objMaestroControl->escribir_archivo($contenidoFila);
		}
		
		$objMaestroControl->cerrar_archivo();
		
		$_SESSION['ENLACE']=$path_plano;
		$_SESSION['ARCHIVO']=$nombreArchivo;
	
		return true;
	}
	
}
	?>