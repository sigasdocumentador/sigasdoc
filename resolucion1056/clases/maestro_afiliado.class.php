<?php
date_default_timezone_set('America/Bogota');
set_time_limit(0);
ini_set('memory_limit','2000M');

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';

include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'utilitario.class.php';
include_once 'maestro_control.class.php';

class MaestroAfiliado {
	/** #### ATRIBUTOS #### **/

	/**
	 * Contiene el objeto Conexion
	 * @var Conexion
	 */
	public static $con = null;

	/** #### METODOS MAGICOS #### **/

	function __construct(){
		try{
			self::$con = IFXDbManejador::conectarDB();
			if( self::$con->conexionID==null ){
				throw new Exception(self::$con->error);
			}
		}catch(Exception $e){
			echo $e->getMessage();
			exit();
		}
	}

	public function select_maestro_afiliado($arrAtributoValor){
		$attrEntidad = array(
				array("nombre"=>"id_registro_control","tipo"=>"NUMBER","entidad"=>"a446")
			);

		$filtroSql = Utilitario::obtenerFiltro($attrEntidad,$arrAtributoValor);
		$sql = "SELECT tipo_registro, tipo_ident_afili, numer_ident_afili, tipo_ident_miemb_pobla_cubie, numer_ident_miemb_pobla_cubie, codigo_genero, fecha_nacimiento, primer_apellido, segundo_apellido, primer_nombre, segundo_nombre, departamento_recidencia, municipio_recidencia, fecha_afiliacion, codigo_CCF, codig_tipo_afili, tipo_ident_aport, numer_ident_aport, digit_verif_aport, razon_socia_aport, fecha_vincu_aport, depar_ubica_labor, munic_ubica_labor, al_dia, codig_tipo_miemb_pobla_cubie, codig_condi_benef, codig_tipo_relac_afili
				FROM aportes446 a446
				$filtroSql
				ORDER BY id_maestro_afiliado";
		return Utilitario::fetchConsulta($sql,self::$con);
	}
	
	public function ejecutar($arrDatos,$usuario){
		$idRegistroControl = 0;
		
		//Verificar si existen registros enviados
		$objMaestroControl = new MaestroControl();
		$arrFiltro = array("fecha_inici_perio_infor"=>$arrDatos["fecha_inici_perio_infor"]
				,"fecha_final_perio_infor"=>$arrDatos["fecha_final_perio_infor"]
				,"id_tipo_archivo"=>$arrDatos["id_tipo_archivo"]
				,"enviado"=>'S'
			);
		
		$arrDatosMaestContr = $objMaestroControl->select_registro_control($arrFiltro);
		if(count($arrDatosMaestContr)==0){
			
			$arrFiltro = array("fecha_inicio"=>$arrDatos["fecha_inici_perio_infor"],"fecha_fin"=>$arrDatos["fecha_final_perio_infor"]);
			
			//preparar los datos si no existen registro
			$idRegistroControl = $this->preparar_datos($arrFiltro,$usuario);
		}else{
			$idRegistroControl = $arrDatosMaestContr[0]["id_registro_control"];
		}
		
		//var_dump($idRegistroControl);exit();
		
		if($idRegistroControl==0) return false;
		
		//obtener el archivo a descargar
		$resultado = $this->fetch_archivo($idRegistroControl,$arrDatos["ruta_generados"]);
		
		
		if(!$resultado) return false;
		
		$resultado = true;
		
		if($arrDatos["enviado"]=="S"){
			//Actualizar como enviado el registro de control
			$arrDatosUpdat = array("fecha_envio"=>date('Ymd'),"id_registro_control"=>$idRegistroControl);
			$resultado = $objMaestroControl->update_enviado_registro_control($arrDatosUpdat,$usuario);
		}
		
		return $resultado;
	}
	
	/**
	 * Preparar los datos para el archivo maestro
	 * 
	 * @param unknown_type $datos
	 * @param unknown_type $usuario
	 * @return number id_registro_control
	 */
	public function preparar_datos($datos, $usuario){
		$resultado = 0;
		$sentencia = self::$con->conexionID->prepare ( "EXEC resolucion1056.sp_Maestro_Afiliado
				@fecha_inicio = '{$datos["fecha_inicio"]}'
				, @fecha_fin = '{$datos["fecha_fin"]}'
				, @usuario = '$usuario'
				, @resultado = :resultado" );
	
		$sentencia->bindParam ( ":resultado", $resultado, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000 );
		$sentencia->execute ();
	
		return $resultado;
	}
	
	/**
	 * Preparar los datos para el archivo maestro
	 *
	 * @param unknown_type $datos
	 * @param unknown_type $usuario
	 * @return number id_registro_control
	 */
	public function fetch_archivo($idRegistroControl, $rutaGenerados){
		
		$objMaestroControl = new MaestroControl();
		$arrDatosRegisContr = $objMaestroControl->select_registro_control(array("id_registro_control"=>$idRegistroControl));
		
		$arrDatosMaestAfili = $this->select_maestro_afiliado(array("id_registro_control"=>$idRegistroControl));
		
		if(count($arrDatosRegisContr)==0)
			return false;
		
	
		$nombreArchivo = $arrDatosRegisContr[0]["nombre_archivo"];
		
		//Crear el archivo
		$objMaestroControl = new MaestroControl();
		$path_plano = $objMaestroControl->crear_archivo($nombreArchivo, $rutaGenerados);
		$objMaestroControl->abrir_archivo();
		
		//Adicionar registro Control
		foreach($arrDatosRegisContr as $row){
			$contenidoFila = $row["tipo_registro"]."|".$row["codigo_CCF"]."|".$row["fecha_inici_perio_infor"]."|".$row["fecha_final_perio_infor"]."|".$row["total_registros"]."|".$row["nombre_archivo"]."\r\n";
			$objMaestroControl->escribir_archivo($contenidoFila);
		}
		
		//Adicionar los registros detalles
		foreach ($arrDatosMaestAfili as $row){
			$contenidoFila = join("|", $row)."\r\n";
			$objMaestroControl->escribir_archivo($contenidoFila);
		}
		
		$objMaestroControl->cerrar_archivo();
		
		$_SESSION['ENLACE']=$path_plano;
		$_SESSION['ARCHIVO']=$nombreArchivo;
	
		return true;
	}
	
}
	?>