<?php

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';

include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'utilitario.class.php';


class MaestroControl {
	/** #### ATRIBUTOS #### **/

	
	private $ruta_generados;
	private $path_plano;
	private $fp_archivo;
	
	/**
	 * Contiene el objeto Conexion
	 * @var Conexion
	 */
	public static $con = null;

	/** #### METODOS MAGICOS #### **/

	function __construct(){
		try{
			self::$con = IFXDbManejador::conectarDB();
			if( self::$con->conexionID==null ){
				throw new Exception(self::$con->error);
			}
		}catch(Exception $e){
			echo $e->getMessage();
			exit();
		}
	}

	public function update_enviado_registro_control($datos, $usuario){
		$queryDML = " UPDATE aportes445 
					SET enviado='S'
						, fecha_envio=GETDATE()
						, usuario_envio='$usuario'
						, usuario_modificacion='$usuario'
						, fecha_modificacion=GETDATE() 
					WHERE id_registro_control= {$datos["id_registro_control"]}";

		$statement = self::$con->conexionID->prepare($queryDML);
		$guardada = $statement->execute();
		return $guardada == false ? false : true;
	}

	public function select_registro_control($arrAtributoValor){
		$attrEntidad = array(
			array("nombre"=>"fecha_inici_perio_infor","tipo"=>"TEXT","entidad"=>"a445")
			,array("nombre"=>"fecha_final_perio_infor","tipo"=>"TEXT","entidad"=>"a445")
			,array("nombre"=>"id_tipo_archivo","tipo"=>"NUMBER","entidad"=>"a445")
			,array("nombre"=>"enviado","tipo"=>"TEXT","entidad"=>"a445")
			,array("nombre"=>"id_registro_control","tipo"=>"NUMBER","entidad"=>"a445")
		);

		$filtroSql = Utilitario::obtenerFiltro($attrEntidad,$arrAtributoValor);
		$sql = " SELECT * 
				FROM aportes445 a445
				$filtroSql";
		return Utilitario::fetchConsulta($sql,self::$con);
	}
	
	public function crear_archivo($nombreArchivo,$ruta_generados){
		$this->ruta_generados = $ruta_generados;
		$path_plano = $this->ruta_generados.'resolucion1056'.DIRECTORY_SEPARATOR.'archivos'.DIRECTORY_SEPARATOR;
		
		if( !file_exists($path_plano) ){
			mkdir($path_plano, 0777, true);
		}
		
		
		$this->path_plano .=DIRECTORY_SEPARATOR.$nombreArchivo;
		
		return $this->path_plano; 
	}
	
	public function abrir_archivo(){
		$this->fp_archivo=fopen($this->path_plano,'w');
		return $this->fp_archivo; 
	}
	
	public function escribir_archivo($contenidoFila){
		fwrite($this->fp_archivo, $contenidoFila);
	}
	
	public function cerrar_archivo(){
		fclose($this->fp_archivo);
	}
}
	
?>