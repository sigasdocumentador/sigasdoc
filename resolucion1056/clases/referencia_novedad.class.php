<?php 

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';

include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'utilitario.class.php';


class ReferenciaNovedad {
    /** #### ATRIBUTOS #### **/
	    /**
     * Contiene el objeto Conexion
     * @var Conexion 
     */
    public static $con = null;
    /** #### METODOS MAGICOS #### **/
	function __construct(){
		try{
			self::$con = IFXDbManejador::conectarDB();
			if( self::$con->conexionID==null ){
				throw new Exception(self::$con->error);
			}
		}catch(Exception $e){
			echo $e->getMessage();
			exit();
		}
	}
	    public function insert_referencia_novedad($datos, $usuario){
        $queryDML = " INSERT INTO aportes441 (referencia_novedad, descripcion, estado, usuario_creacion)
			VALUES('{$datos["referencia_novedad"]}',:descripcion,'{$datos["estado"]}','$usuario')";
        
        $statement = self::$con->conexionID->prepare($queryDML);
        $statement->bindValue(":descripcion", $datos["descripcion"],  PDO::PARAM_STR);
        $guardada = $statement->execute();
        return $guardada == false ? 0 : 1;
    }
    public function update_referencia_novedad($datos, $usuario){
        $queryDML = " UPDATE aportes441
					 SET referencia_novedad = '{$datos["referencia_novedad"]}'
					     , descripcion = :descripcion
					     , estado = '{$datos["estado"]}'
					     , usuario_modificacion = '$usuario'
					     , fecha_modificacion = GETDATE()
				      WHERE id_referencia_novedad = '{$datos["id_referencia_novedad"]}'";
        
        $statement = self::$con->conexionID->prepare($queryDML);
        $statement->bindValue(":descripcion", $datos["descripcion"],  PDO::PARAM_STR);
        $guardada = $statement->execute();
        return $guardada == false ? 0 : 1;
    }

    public function select_referencia_novedad($arrAtributoValor){
    	$attrEntidad = array(
    			array("nombre"=>"referencia_novedad","tipo"=>"TEXT","entidad"=>"a441")
    			,array("nombre"=>"estado","tipo"=>"TEXT","entidad"=>"a441"));
    
    	$filtroSql = Utilitario::obtenerFiltro($attrEntidad,$arrAtributoValor);
    	$sql = " SELECT
				    a441.id_referencia_novedad
				    , a441.referencia_novedad
				    , a441.descripcion
				    , a441.estado
				    , a441.usuario_creacion
				    , a441.usuario_modificacion
				    , a441.fecha_creacion
				    , a441.fecha_modificacion
				FROM aportes441 a441
    		$filtroSql";
    	return Utilitario::fetchConsulta($sql,self::$con);
    }
}
?>