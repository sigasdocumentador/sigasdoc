<?php

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXerror.php';

include_once $raiz.DIRECTORY_SEPARATOR.'clases'.DIRECTORY_SEPARATOR.'utilitario.class.php';

class PaqueteEscolar {
	/** #### ATRIBUTOS #### **/

	/**
	 * Contiene el objeto Conexion
	 * @var Conexion
	 */
	public static $con = null;

	/** #### METODOS MAGICOS #### **/

	function __construct(){
		try{
			self::$con = IFXDbManejador::conectarDB();
			if( self::$con->conexionID==null ){
				throw new Exception(self::$con->error);
			}
		}catch(Exception $e){
			echo $e->getMessage();
			exit();
		}
	}
	
	public function update_valor($datos, $usuario){
		$queryDML = "UPDATE aportes057 
						SET valor={$datos["valor"]}, usuario_modificacion='$usuario' , fecha_modificacion = GETDATE()  
					WHERE anno='{$datos["anio"]}'";

		$statement = self::$con->conexionID->prepare($queryDML);
		$guardada = $statement->execute();
		return $guardada == false ? 0 : 1;
	}
}
?>