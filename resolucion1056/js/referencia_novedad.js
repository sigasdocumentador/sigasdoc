var uri = "",
	uriLogReferenciaNovedad = "",
	objAccion = {SELECT:"S", INSERT:"I", UPDATE:"U"};

$(function(){
	uri = src();
	uriLogReferenciaNovedad = uri + "resolucion1056/logica/referencia_novedad.log.php";
	
	$("#txtReferenciaNovedad").blur(contrReferNoved);
});

function nuevo(){
	limpiarCampos2("#hidIdReferenciaNovedad,#txtReferenciaNovedad,#txaDescripcion,#cmbEstado");
}

function contrReferNoved(){
	var referenciaNovedad = $("#txtReferenciaNovedad").val().trim();
	
	if(!referenciaNovedad) return false;
	
	var objFiltro = {referencia_novedad:referenciaNovedad};
	var arrDatos = fetchReferNoved(objAccion.SELECT, objFiltro);
	
	if(arrDatos && typeof arrDatos == "object" && arrDatos.data.length>0){
		
		if(!confirm("La referencia ya existe, desea modificarla!!")){
			nuevo();
			return false;
		}
		
		arrDatos = arrDatos.data[0];
		
		$("#hidIdReferenciaNovedad").val(arrDatos.id_referencia_novedad);
		$("#txaDescripcion").val(arrDatos.descripcion);
		$("#cmbEstado").val(arrDatos.estado);
	}
}

function guardar(){
	
	//Verificar campos obligatorios
	if(validateElementsRequired()) return false;
	
	var objDatos = {
			id_referencia_novedad: $("#hidIdReferenciaNovedad").val().trim()
			, referencia_novedad: $("#txtReferenciaNovedad").val().trim()
			, estado: $("#cmbEstado").val()
			, descripcion: $("#txaDescripcion").val().trim()
	};
	
	var accion = objDatos.id_referencia_novedad?objAccion.UPDATE:objAccion.INSERT;
	
	$.ajax({
		url:uriLogReferenciaNovedad,
		type:"POST",
		data:{accion:accion,datos:objDatos},
		dataType: "json",
		async:false,
		success:function(data){
			if(data.error==0){
				alert("La referencia de la novedad se guardo correctamente!!");
				nuevo();
				return true;
				
			}
			
			alert("ERROR al guardar la referencia de la novedad!!");
		}
	});
}

function fetchReferNoved(accion,objFiltro){
	var resultado = null;
	
	$.ajax({
		url:uriLogReferenciaNovedad,
		type:"POST",
		data:{accion:accion,objDatosFiltro:objFiltro},
		dataType: "json",
		async:false,
		success:function(data){
			resultado = data;
		}
	});
	
	return resultado;
}
