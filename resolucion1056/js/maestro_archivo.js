var urlMaestroArchivo = ""
	, urlBuscaDetalDefin = ""
	, urlDescargarArchivo = ""
		
	, idDefinTipoArchi = 79 //[79][TIPOS ARCHIVOS RESOLUCION 1056]
	, objAccion = {
			CMCP: 0     //[4407][Maestro de Afiliados Cargue Peri�dico]
			, CNCA: 0   //[4408][Novedades de Actualizaci�n de afiliaciones y subsidios]
			, CNCE: 0   //[4409][Novedades Estado Afiliaci�n y al d�a del aportante]
			, CMSP: 0   //[4410][Maestro de Subsidios]
		};

$(function(){
	url = src();
	urlMaestroArchivo = url+"resolucion1056/logica/maestro_archivo.log.php";
	urlBuscaDetalDefin = url+"phpComunes/buscaDetalDefin.php";
	urlDescargarArchivo= url+'phpComunes/descargaPlanos.php';
	
	$("#cmbTipoArchivo").change(comboTipoArchivo);
	
	$("#btnGenerar").button()
		.click(generarArchivo);
	
	datepickerC("FECHA_BETWEEN","#txtFechaInicio,#txtFechaFin");
	
	controlTipoArchivo();
});

function controlTipoArchivo(){
	var objDatosTipoArchi = fetchTipoArchivo();
	var htmlOption = "<option value=''>--Selecccione--</option>";
	
	if(objDatosTipoArchi && typeof objDatosTipoArchi == "object"){
		
		$.each(objDatosTipoArchi,function(i,row){
			htmlOption += "<option value='"+row.iddetalledef+"'>"+ row.codigo +" - " +row.detalledefinicion+"</option>";
			
			if(row.codigo=='CMCP')
				objAccion.CMCP = row.iddetalledef;
			else if(row.codigo=='CNCA')
				objAccion.CNCA = row.iddetalledef;
			else if(row.codigo=='CNCE')
				objAccion.CNCE = row.iddetalledef;
			else if(row.codigo=='CMSP')
				objAccion.CMSP = row.iddetalledef;
		});
		
	}
	
	$("#cmbTipoArchivo").html(htmlOption);
}

function comboTipoArchivo(){
	var tipoArchivo = $("#cmbTipoArchivo").val();

	if(tipoArchivo==objAccion.CMSP){
		$("#trPaqueteEscolar").show();
	}else{
		$("#trPaqueteEscolar").hide();
	}
	
}

function generarArchivo(){
	if(validateElementsRequired()) return false;
	 
	var idTipoArchivo = $("#cmbTipoArchivo").val();
	var enviar = $("#chkEnviado").is(":checked")?$("#chkEnviado").val():"N";
	var paqueteEscolar = $("#chkPaqueteEscolar").is(":checked")?$("#chkPaqueteEscolar").val():"N";
	
	if(enviar=="S" && !confirm("Desea enviar la informacion?")) return false;
		
	
	var objDatos = {
			fecha_inici_perio_infor: $("#txtFechaInicio").val().trim()
			, fecha_final_perio_infor: $("#txtFechaFin").val().trim()
			, id_tipo_archivo: idTipoArchivo
			, enviado: enviar
			, paquete_escolar: paqueteEscolar 
	};
	
	var accion = "";
	
	if(idTipoArchivo == objAccion.CMCP)
		accion = "CMCP";
	else if(idTipoArchivo == objAccion.CNCA)
		accion = "CNCA";
	else if(idTipoArchivo == objAccion.CNCE)
		accion = "CNCE";
	else if(idTipoArchivo == objAccion.CMSP)
		accion = "CMSP";
			
	var resultado = false;
	$.ajax({
		url:urlMaestroArchivo,
		type:"POST",
		data:{accion:accion,datos:objDatos},
		dataType: "json",
		async:false,
		beforeSend: function(objeto){
        	dialogLoading('show');
        },
        complete: function(objeto, exito){
        	dialogLoading('close');
        },
		success:function(data){
			if(data.error==0){
				//Descargar el archivo
				resultado = true;
				return true;				
			}
			
			alert("ERROR al generar el archivo plano!!");
		}
	});
	
	if(resultado){
		//Descargar el archivo
		window.open(urlDescargarArchivo);
	}
}

function fetchTipoArchivo(){
	
	var resultado = null;
	objFiltro = {iddefinicion:idDefinTipoArchi};
	
	$.ajax({
		url:urlBuscaDetalDefin,
		type:"POST",
		data:{objDatosFiltro:objFiltro},
		dataType: "json",
		async:false,
		success:function(data){
			resultado = data;
		}
	});
	
	return resultado;
}