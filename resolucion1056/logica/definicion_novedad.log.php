<?php 
/**
 * Script de la logica del formulario definicion_novedad
 *
 * @author Oscar
 * @version 0
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once '../clases' . DIRECTORY_SEPARATOR . 'definicion_novedad.class.php';
/**
 * Contiene los datos de retorno al formulario
 * @var array
 */
$arrMensaje = array('error'=>0,'data'=>null);

/*Verificar si accion existe*/
$accion = isset($_POST['accion'])?$_POST['accion']:'';

$usuario = $_SESSION["USUARIO"];
switch($accion){
    case 'I':
    	
        $datos = $_POST["datos"];
        	        $objDefinicionNovedad = new DefinicionNovedad();
        $resultado = $objDefinicionNovedad->insert_definicion_novedad($datos,$usuario);

        $arrMensaje["error"] = 0;
            
        if($resultado == 0){
        	$arrMensaje["error"] = 1;
       	}

        break;
    case 'U':

    	$datos = $_POST["datos"];
    	 
    	$objDefinicionNovedad = new DefinicionNovedad();
    	$resultado = $objDefinicionNovedad->update_definicion_novedad($datos,$usuario);
    	
    	$arrMensaje["error"] = 0;
    	
    	if($resultado == 0){
    		$arrMensaje["error"] = 1;
    	}
    	
        break;
    case 'S':

    		$objDatosFiltro = $_POST["objDatosFiltro"];                        $objDefinicionNovedad = new DefinicionNovedad();
            $resultado = $objDefinicionNovedad->select_definicion_novedad($objDatosFiltro);
                        /*Verificar si existen datos*/
            if(count($resultado)>0){
                $arrMensaje["error"] = 0;
                $arrMensaje["data"] = $resultado;
            }
        break;
}

echo ($accion != '') ? json_encode($arrMensaje) : "";
?>