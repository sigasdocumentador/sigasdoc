<?php	
	$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
	include_once  $root;
	
	$url=$_SERVER['PHP_SELF'];
	include_once $raiz.DIRECTORY_SEPARATOR.'phpComunes'.DIRECTORY_SEPARATOR.'auditoria.php';
	auditar($url);
?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<title>::Referencia Novedad::</title>
		
		<link href="../css/Estilos.css" rel="stylesheet" type="text/css" />
		<link href="../css/marco.css" rel="stylesheet" type="text/css" />
		<link href="../newcss/css.1.8/jquery-ui-1.8.17.custom.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="../newjs/1.6/jquery-1.6.2.min.js"></script>
		<script type="text/javascript" src="../newjs/1.6/jquery-ui-1.8.16.custom.min.js"></script>
		<script type="text/javascript" src="../js/comunes.js"></script>
		<script type="text/javascript" src="js/referencia_novedad.js"></script>
	</head>
	<body>
		<table width="50%" border="0" align="center" cellpadding="0" cellspacing="0">
			<tbody>
		  	<!-- ESTILOS SUPERIOR TABLA-->
		  	<tr>
			    <td width="13" height="29" class="arriba_iz">&nbsp;</td>
			    <td class="arriba_ce"><span class="letrablanca">::&nbsp;Referencia Novedad::</span></td>
			    <td width="13" class="arriba_de" align="right">&nbsp;</td>
		  	</tr> 
		  	<!-- ESTILOS ICONOS TABLA-->
		  	<tr>
		    	<td class="cuerpo_iz">&nbsp;</td>
		    	<td class="cuerpo_ce">
					<img src="../imagenes/menu/nuevo.png" name="iconNuevo" width="16" height="16" id="iconNuevo" style="cursor:pointer" title="Nuevo" onClick="nuevo();" /> 
					<img width="3" height="1" src="../imagenes/spacer.gif" />
					<img src="../imagenes/menu/grabar.png" name="iconGuardar" width="16"  height=16 id="iconGuardar" style="cursor:pointer" title="Guardar" onClick="guardar();" />
					<img width="3" height="1" src="../imagenes/spacer.gif" />   
		    	</td>
		    	<td class="cuerpo_de">&nbsp;</td>
		  	</tr>
		  	<!-- ESTILOS MEDIO TABLA-->
		  	<tr>
		    	<td class="cuerpo_iz">&nbsp;</td>
		    	<td class="cuerpo_ce"></td>
		    	<td class="cuerpo_de">&nbsp;</td>
		  	</tr>  
		  	<!-- CONTENIDO TABLA-->
		  	<tr>
		   		<td class="cuerpo_iz">&nbsp;</td>
		   		<td class="cuerpo_ce">
					<table cellspacing="0" width="90%" border="0" class="tablero" align="center">
		      			<tbody>
		      				<tr>
			      				<td >Definicion Novedad:</td>
								<td >
									<input type="text" name="txtReferenciaNovedad" id="txtReferenciaNovedad" class="box1 element-required"/>
									<input type="hidden" name="hidIdReferenciaNovedad" id="hidIdReferenciaNovedad"/>
									<span class="clsObligatorio">&nbsp;&nbsp;&nbsp;</span>
								</td>
			        		</tr>
			    			<tr>
			      				<td >Estado:</td>
								<td >
									<select id="cmbEstado" class="box1 element-required">
										<option value="">--Estado--</option>
										<option value="A">Activo</option>
										<option value="I">Inactivo</option>
										<span class="clsObligatorio">&nbsp;&nbsp;&nbsp;</span>
									</select>
								</td>
			        		</tr>
			        		<tr>
			      				<td >Descripci&oacute;n:</td>
								<td >
									<textarea cols="30" rows="3" name="txaDescripcion" id="txaDescripcion" class="boxlargo element-required"></textarea>
									<span class="clsObligatorio">&nbsp;&nbsp;&nbsp;</span>
								</td>
			        		</tr>
		      			</tbody>
		      		</table>
					</td>
		    		<td class="cuerpo_de">&nbsp;</td>
		  		</tr>		  
		  		<!-- ESTILOS PIE TABLA-->
		  		<tr>
		    		<td class="abajo_iz" >&nbsp;</td>
		    		<td class="abajo_ce" ></td>
		    		<td class="abajo_de" >&nbsp;</td>
		  		</tr>		  
			</tbody>
		</table>
	</body>
</html>