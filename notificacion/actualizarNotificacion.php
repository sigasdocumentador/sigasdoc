<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once 'clases' . DIRECTORY_SEPARATOR . 'notificacion.class.php';

//Recibir datos del js
$objDatos= $_REQUEST['datos'];
$usuario=$_SESSION['USUARIO'];

$objNotificacion = new Notificacion;

$objNotificacion->inicioTransaccion();

$arrBanderaDatos = array();
$banderaError = 0;

//Actualizar datos notificacion
$arrBanderaDatos["id_notificacion"] = $objDatos["datos_generales"]["id_notificacion"];
$arrBanderaDatos["codigo"] = $objDatos["datos_generales"]["codigo"]; 
$arrBanderaDatos["enunciado"] = $objDatos["datos_generales"]["enunciado"]; 
$arrBanderaDatos["descripcion"] = $objDatos["datos_generales"]["descripcion"]; 
$arrBanderaDatos["query"] = $objDatos["datos_generales"]["query"]; 
$arrBanderaDatos["uri"] = $objDatos["datos_generales"]["uri"]; 
$arrBanderaDatos["dia_ejecucion"] = $objDatos["datos_generales"]["dia_ejecucion"]; 
$arrBanderaDatos["tiempo_expiracion"] = $objDatos["datos_generales"]["tiempo_expiracion"]; 
$arrBanderaDatos["estado"] = $objDatos["datos_generales"]["estado"];
 
$banderaNotificacion = $objNotificacion->update_notificacion($arrBanderaDatos,$usuario);

//Actualizar tiempo ejecucion
if($banderaNotificacion>0 && isset($objDatos["datos_tiempo_ejecu"])){
	$arrBanderaDatos = array();

	$arrBanderaDatos["id_notificacion"] = $objDatos["datos_generales"]["id_notificacion"];
	foreach($objDatos["datos_tiempo_ejecu"] as $rowDatos){
		$arrBanderaDatos["id_tiempo_ejecucion"] = $rowDatos["id_tiempo_ejecucion"];
		$arrBanderaDatos["hora_ejecucion"] = $rowDatos["hora_ejecucion"];
		$arrBanderaDatos["fecha_ulima_ejecu"] = null;
		$arrBanderaDatos["estado"] = $rowDatos["estado"];
		
		//Update
		if(intval($rowDatos["id_tiempo_ejecucion"])>0){
			$bandeTiempEjecu = $objNotificacion->update_hora_notif($arrBanderaDatos,$usuario);
			
		//Insert
		}else{
			$bandeTiempEjecu = $objNotificacion->insert_hora_notif($arrBanderaDatos,$usuario);
		}
		
		if($bandeTiempEjecu==0){
			$banderaError++;
			break;
		}
	}
}

//Actualizar medio difunsion
if($banderaError==0 && isset($objDatos["datos_medio_difus"])){
	$arrBanderaDatos = array();
	
	$arrBanderaDatos["id_notificacion"] = $objDatos["datos_generales"]["id_notificacion"];
	foreach($objDatos["datos_medio_difus"] as $rowDatos){
		
		$arrBanderaDatos["id_notif_medio_difus"] = $rowDatos["id_notif_medio_difus"];
		$arrBanderaDatos["id_medio_difusion"] = $rowDatos["id_medio_difusion"];
		
		$bandeMedioDifus = 0;
		
		if($rowDatos["accion"]=="INSERT"){
			$bandeMedioDifus = $objNotificacion->insert_notif_medio_difus($arrBanderaDatos,$usuario);
			
		}else if($rowDatos["accion"]=="DELETE"){
			$bandeMedioDifus = $objNotificacion->delete_notif_medio_difus($rowDatos["id_notif_medio_difus"]);
		}
		
		if($bandeMedioDifus==0){
			$banderaError++;
			break;
		}
	}
}

//Actualizar rol
if($banderaError==0 && isset($objDatos["datos_rol"])){
	$arrBanderaDatos = array();
	
	$arrBanderaDatos["id_notificacion"] = $objDatos["datos_generales"]["id_notificacion"];
	foreach($objDatos["datos_rol"] as $rowDatos){

		$arrBanderaDatos["id_rol"] = $rowDatos["id_rol"];
		$arrBanderaDatos["id_notificacion_rol"] = $rowDatos["id_notificacion_rol"];
		
		$banderaRol = 0;
		
		if($rowDatos["accion"]=="INSERT"){
			$banderaRol = $objNotificacion->insert_notif_rol($arrBanderaDatos,$usuario);
				
		}else if($rowDatos["accion"]=="DELETE"){
			$banderaRol = $objNotificacion->delete_notif_rol($rowDatos["id_notificacion_rol"]);
		}

		if($banderaRol==0){
			$banderaError++;
			break;
		}
	}
}

if($banderaError==0 && $banderaNotificacion>0){
	$objNotificacion->confirmarTransaccion();
	echo 1;
}else{
	$objNotificacion->cancelarTransaccion();
	echo 0;
}
?>