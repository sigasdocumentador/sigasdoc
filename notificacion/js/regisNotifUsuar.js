var url = "";
$(function(){
	url = URL;
	controlNoticicacion();
});

function controlNoticicacion(){
	var idUsuario = $("#hidIdUsuario").val().trim();
	
	var objNotificacionUsuario = fetchNotifUsuar({id_usuario:idUsuario,accion:"ALL"});
	
	var htmlNotificacion = "";
	if(typeof objNotificacionUsuario == "object"){
		var arrNotificacion = [];
		
		$.each(objNotificacionUsuario,function(i,row){
			var uri = url+""+encodeURI(row.uri_notificacion);
			var uriParamet = url+"notificacion/guardarNotifUsuar.php?uri="+uri
					+"&id_registro_notificacion="+row.id_registro_notificacion
					+"&id_usuario_notificacion="+idUsuario;
			
			var bandera = "SI";
			
			if(typeof row.id_regis_notif_usuar == "undefined" || row.id_regis_notif_usuar==0)
				bandera = "NO";
			
			htmlNotificacion += "<tr><td><a href='"+uriParamet+"' target='_blank'>"+row.enunciado_notificacion+"</a></td>" +
					"<td>"+row.descripcion_notificacion+"</td>" +
					"<td>"+row.fecha_creac_regis_notif+"</td>"+
					"<td>"+bandera+"</td></tr>";
		});
		
		
	}else if(objNotificacionUsuario!=0){
		htmlNotificacion = "<tr><td colspan='3'>NO HAY NOTIFICACIONES</td></tr>";
	}
	$("#tbNotificacion").html(htmlNotificacion);
}

/**
 * METODO: Obtener las notificaciones del usuario
 * 
 * @param objDatosFiltro
 * @returns Array
 * */
function fetchNotifUsuar(objDatosFiltro){
	var datosRetorno = "";
	$.ajax({
		url:url+"notificacion/buscarNotifUsuar.php",
		type:"POST",
		data:{objDatosFiltro:objDatosFiltro},
		dataType:"json",
		async:false,
		success:function(datos){
			//************************************
			datosRetorno = datos;
		}
	});
	return datosRetorno;
}