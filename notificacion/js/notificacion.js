var globalDatosNotificacion = []; 
$(function(){
	
	$("#btnAddTrHoraEjecu").button();
	
	addMedioDifusion();
	addRol();
	
	$("#divBuscarNotificacion").dialog({
		autoOpen: false,
		modal:true,
		width: 900,
		close: function(){
			limpiarCampo("#tblBuscarNotificacion tbody");
		}
	})
});

/**
 * METODO: Adiciona nueva fila "tr" para ingresar otra hora
 * 
 * @param objDatos Datos para cada element
 * */
function addTrHoraEjecu(objDatos){
	var numeroTr = 1;
	//Verificamos si existen filas "tr" 
	if($("#tblTiempoEjecucion tbody tr").length>0){
		var arrIdTrAnterior = $("#tblTiempoEjecucion tbody tr:last").attr("id");
		numeroTr = parseInt(arrIdTrAnterior.replace("trHoraEjecucion",""))+1
	}
	
	//Creamos el id para la nueva fila "tr"
	var idTrNuevo = "trHoraEjecucion"+numeroTr;
	
	var htmlTr = '<tr id="'+idTrNuevo+'">'
			+ '<td><img src="../imagenes/borrar.png" alt="Eliminar" style="cursor:pointer;" onclick="removeTrHoraEjecu(\''+idTrNuevo+'\');"/>' 
			+ '<input type="hidden" name="hidIdTiempoEjecucion" /></td>'
			+ '<td><input type="text" name="txtHoraEjecucion" size="8" class="box2 element-required"/>'
			+ '<td>'
				+ '<select name="cmbEstadHoraEjecu" class="box2 element-required">'
					+ '<option value="A">Activo</option><option value="I">Inactivo</option>'
				+'</select>'
			+ '</td></tr>';
	
	$("#tblTiempoEjecucion tbody").append(htmlTr);
	
	if(typeof objDatos == "object"){
		
		//Adicionar los valores a cada element
		$("#tblTiempoEjecucion #"+idTrNuevo+" input[name='hidIdTiempoEjecucion']").val(objDatos.id_tiempo_ejecucion);
		$("#tblTiempoEjecucion #"+idTrNuevo+" input[name='txtHoraEjecucion']").val(objDatos.hora_ejecucion);
		$("#tblTiempoEjecucion #"+idTrNuevo+" select[name='cmbEstadHoraEjecu']").val(objDatos.estado);
	}
}

/**
 * METODO: Remover fila "tr" para eliminar la hora <br/>
 * 
 * Solo se eliminan los tr nuevos
 * 
 * @param idTrPeriodo Id tr a remover
 * */
function removeTrHoraEjecu(idTr){
	var idTiempoEjecurion = $("#tblTiempoEjecucion #"+idTr+" input[name='hidIdTiempoEjecucion']").val();
	if (idTiempoEjecurion==0){
		 $("#tblTiempoEjecucion #"+idTr).remove();
	}else{
		alert("No es posible eliminar la hora. Pero puede cambiar el estado!!");
	}
}

/**
 * METODO: Add Medio de difusion
 * 
 * 
 * */
function addMedioDifusion(){
	var objMedioDifusion = fetchMedioDifusion();
	
	var htmlTr = "";
	
	if(typeof objMedioDifusion == "object" && objMedioDifusion.length>0){
		$.each(objMedioDifusion,function(i,row){
			htmlTr += "<tr id='trMedioDifusion"+row.id_medio_difusion+"'>"
						+"<td><input type='hidden' name='hidIdNotifMedioDifus'/>"
								+ "<input type='checkbox' name='chkIdMedioDifusion' value='"+row.id_medio_difusion+"'/></td>"
						+"<td>"+row.descripcion+"</td></tr>";
		});
	}else{
		htmlTr = "<tr><td colspan='2'><b>NO SE ENCONTRARON MEDIOS DE DIFUSION</b></td></tr>";
	}
	
	$("#tblMedioDifusion tbody").html(htmlTr);
}

/**
 * METODO: Add Rol
 * 
 * 
 * */
function addRol(){
	var objRol = fetchRol();
	
	var htmlTr = "";
	
	if(typeof objRol == "object" && objRol.length>0){
		$.each(objRol,function(i,row){
			htmlTr += "<tr id='trRol"+row.idrol+"'>"
						+"<td><input type='hidden' name='hidIdNotificacionRol'/>"
								+ "<input type='checkbox' name='chkIdRol' value='"+row.idrol+"'/></td>"
						+"<td>"+row.rol+"</td></tr>";
		});
	}else{
		htmlTr = "<tr><td colspan='2'><b>NO SE ENCONTRARON ROLES</b></td></tr>";
	}
	
	$("#tblRol tbody").html(htmlTr);
}

/******************************
 * METODOS PARA LAS PETICIONES
 * ****************************/


/**
 * METODO: Obtener los medios de difusion
 * 
 * @param objDatosFiltro
 * @returns Array
 * */

function fetchMedioDifusion(objDatosFiltro){
	
	if(typeof objDatosFiltro == "undefined" || objDatosFiltro==0){
		objDatosFiltro = {};
	}
	
	objDatosFiltro.estado = "A";
	
	var datosRetorno = "";
	$.ajax({
		url:"buscarMedioDifusion.php",
		type:"POST",
		data:{objDatosFiltro:objDatosFiltro},
		dataType:"json",
		async:false,
		success:function(datos){
			//************************************
			datosRetorno = datos;
		}
	});
	return datosRetorno;
}

/**
 * METODO: Obtener los medios de difusion
 * 
 * @param objDatosFiltro
 * @returns Array
 * */

function fetchRol(objDatosFiltro){
	if(typeof objDatosFiltro == "undefined" || objDatosFiltro==0){
		objDatosFiltro = {};
	}
	
	objDatosFiltro.estado = "A";
	
	var datosRetorno = "";
	$.ajax({
		url:"buscarRol.php",
		type:"POST",
		data:{objDatosFiltro:objDatosFiltro},
		dataType:"json",
		async:false,
		success:function(datos){
			//************************************
			datosRetorno = datos;
		}
	});
	return datosRetorno;
}

/**
 * METODO: Obtener las notificaciones
 * 
 * @param objDatosFiltro
 * @returns Array
 * */

function fetchNotificacion(objDatosFiltro){
	
	if(typeof objDatosFiltro == "undefined" || objDatosFiltro==0){
		objDatosFiltro = {};
	}
	
	var datosRetorno = "";
	$.ajax({
		url:"buscarNotificacion.php",
		type:"POST",
		data:{objDatosFiltro:objDatosFiltro},
		dataType:"json",
		async:false,
		success:function(datos){
			//************************************
			datosRetorno = datos;
		}
	});
	return datosRetorno;
}

/**
 * METODO: Guardar la notificacion
 * 
 * */
function guardar(){
	
	//Validar los datos del formulario
	if(isDataIncompleta())return false;

	//Obtener los datos del formulario
	var objDatosFormulario = obtenDatosFormu();
	
	if(objDatosFormulario.length==0)return false;
	
	//Enviar la peticion
	$.ajax({
		url:"guardarNotificacion.php",
		type:"POST",
		data:{datos:objDatosFormulario},
		dataType:"json",
		async:false,
		success:function(datos){
			//************************************
			if(datos==1){
				alert("Los datos se guardaron correctamente!!");
				nuevaNotificacion();
			}else{
				alert("Error al guardar los datos!!");
			}
		}
	});
}

/**
 * METODO: Actualizar la notificacion
 * 
 * */
function actualizar(){
	//Validar los datos del formulario
	if(isDataIncompleta())return false;

	//Obtener los datos del formulario
	var objDatosFormulario = obtenDatosFormu();
	
	if(objDatosFormulario.length==0)return false;
	
	//Enviar la peticion
	$.ajax({
		url:"actualizarNotificacion.php",
		type:"POST",
		data:{datos:objDatosFormulario},
		dataType:"json",
		async:false,
		success:function(datos){
			//************************************
			if(datos==1){
				alert("Los datos se actualizaron correctamente!!");
				nuevaNotificacion();
			}else{
				alert("Error al actualizar los datos!!");
			}
		}
	});
}

/**
 * METODO: Obtener los datos del formulario
 * 
 * @returns Object
 * */
function obtenDatosFormu(){
	var objResultado = {
			datos_generales:{},
			datos_tiempo_ejecu:[],
			datos_medio_difus:[],
			datos_rol:[]
		};
	
	var diasEjecucion = "";
	
	//Obtener los datos dias de ejecucion
	$(":checkbox[name='chkDiaEjecucion']:checked").each(function(i,row){
		diasEjecucion += $(this).val().trim()+",";
	});
	//Remover la ultima coma ","
	diasEjecucion = diasEjecucion.substring(0,diasEjecucion.length-1);
	
	//Obtener datos generales
	objResultado.datos_generales = {
			id_notificacion: $("#hidIdNotificacion").val().trim(),
			codigo: $("#txtCodigo").val().trim(),
			enunciado: $("#txtEnunciado").val().trim(),
			descripcion: $("#txaDescripcion").val().trim(),
			query: $("#txaQuery").val().trim(),
			uri: $("#txtUri").val().trim(),
			dia_ejecucion: diasEjecucion,
			tiempo_expiracion: $("#txtTiempoExpiracion").val().trim(),
			estado: $("#cmbEstadoNotificacion").val().trim()
	}
	
	
	
	//Obtener las tiempos de ejecucion
	$("#tblTiempoEjecucion tbody tr").each(function(i,row){
	    var idTr = "#"+$(this).attr("id");

	    var idTempoEjecucion = $(idTr + " input[name='hidIdTiempoEjecucion']").val().trim();
	    var horaEjecucion = $(idTr + " input[name='txtHoraEjecucion']").val().trim();
	    var estado = $(idTr + " select[name='cmbEstadHoraEjecu']").val().trim();
	    
	    objResultado.datos_tiempo_ejecu[objResultado.datos_tiempo_ejecu.length] = {
	    		id_tiempo_ejecucion: idTempoEjecucion,
				hora_ejecucion: horaEjecucion, 
				estado:estado
		}
	});
	
	//Obtener los medios de difusion
	$("#tblMedioDifusion tbody tr").each(function(i,row){
	    var idTr = "#"+$(this).attr("id");
	    var accion = "";
	    
	    var idNotifMedioDifus = $(idTr + " input[name='hidIdNotifMedioDifus']").val().trim();
	    var idMedioDifusion = 0;
	    var bandeMedioDifus = $(idTr + " :checkbox[name='chkIdMedioDifusion']:checked").length;
	    
	    
	    //Medios de difusion nuevos
	    if(idNotifMedioDifus==0 && bandeMedioDifus>0){
	    	idMedioDifusion = $(idTr + " input[name='chkIdMedioDifusion']").val().trim();
	    	accion = "INSERT";
	    	
	    //Medios difusion a eliminar
	    }else if(idNotifMedioDifus!=0 && bandeMedioDifus==0){
	    	accion = "DELETE";
	    }
	    
	    if(accion!=""){	
		    objResultado.datos_medio_difus[objResultado.datos_medio_difus.length] = {
		    		id_notif_medio_difus: idNotifMedioDifus,
		    		id_medio_difusion: idMedioDifusion, 
					accion: accion
			};
	    }
	});
	
	//Obtener los roles
	$("#tblRol tbody tr").each(function(i,row){
	    var idTr = "#"+$(this).attr("id");
	    var accion = "";
	    
	    var idNotificacionRol = $(idTr + " input[name='hidIdNotificacionRol']").val().trim();
	    var idRol = 0;
	    var banderaRol = $(idTr + " :checkbox[name='chkIdRol']:checked").length;
	    
	    //Medios de difusion nuevos
	    if(idNotificacionRol==0 && banderaRol>0){
	    	idRol = $(idTr + " input[name='chkIdRol']").val().trim();
	    	accion = "INSERT";
	    	
	    //Medios difusion a eliminar
	    }else if(idNotificacionRol!=0 && banderaRol==0){
	    	accion = "DELETE";
	    }
	    
	    if(accion!=""){	
		    objResultado.datos_rol[objResultado.datos_rol.length] = {
		    		id_notificacion_rol: idNotificacionRol,
		    		id_rol: idRol, 
					accion: accion
			};
	    }
	});
	
	/*var banderaMensaje = "";
	//Verificar si los datos estan completos
	if(objResultado.datos_generales.length==0){
		banderaMensaje += "Datos generales\n";
	}
	if(objResultado.datos_tiempo_ejecu.length==0){
		banderaMensaje += "Datos tiempo ejecuci\xF3n\n";
	}
	if(objResultado.datos_medio_difus.length==0){
		banderaMensaje += "Datos medio difusi\xF3n\n";
	}
	if(objResultado.datos_rol.length==0){
		banderaMensaje += "Datos rol\n";
	}
	if(banderaMensaje!=""){
		alert("Error al obtener los datos:\n "+banderaMensaje+" INFORMAR A T.I.");
		objResultado = {};
	}*/
	return objResultado;
}

/**
 * METODO: Buscar las notificaciones para despues modificarlas
 * 
 * */
function buscar(){
	nuevaNotificacion();
	globalDatosNotificacion = [];
	var objDatosNotificacion = fetchNotificacion();
	var htmlBuscar = "";
	
	if(typeof objDatosNotificacion.data == "object" && objDatosNotificacion.data!=0){
		globalDatosNotificacion = objDatosNotificacion.data;
		
		$.each(globalDatosNotificacion,function(i,row){
			htmlBuscar += "<tr><td onclick='addDataModificar("+row.datos_generales.id_notificacion+");' style='cursor:pointer;'>"+row.datos_generales.codigo+"</td>"
					+ "<td>"+row.datos_generales.enunciado+"</td></tr>";
		});
	}else{
		htmlBuscar = "<tr><td colspan='2'><b>NO EXISTEN NOTIFICACIONES</b></td></tr>";
	}
	
	$("#tblBuscarNotificacion tbody").html(htmlBuscar);
	$("#divBuscarNotificacion").dialog("open");
}

/**
 * METODO: Adiciona los datos de la notificacion al formulario HTML
 * 
 * */
function addDataModificar(idNotificacion){
	var bandeObjDatoNotif = globalDatosNotificacion[idNotificacion];
	var bandeObjDatoGener = bandeObjDatoNotif.datos_generales;
	
	//Add datos generales
	$("#hidIdNotificacion").val(bandeObjDatoGener.id_notificacion);
	$("#txtCodigo").val(bandeObjDatoGener.codigo);
	$("#txtEnunciado").val(bandeObjDatoGener.enunciado);
	$("#txaDescripcion").val(bandeObjDatoGener.descripcion);
	$("#txaQuery").val(bandeObjDatoGener.query);
	$("#txtUri").val(bandeObjDatoGener.uri);
	$("#txtTiempoExpiracion").val(bandeObjDatoGener.tiempo_expiracion);
	$("#cmbEstadoNotificacion").val(bandeObjDatoGener.estado);
	
	//Add Dias de ejecucion
	$.each(bandeObjDatoGener.dia_ejecucion.split(","),function(i,row){
		$(":checkbox[name='chkDiaEjecucion'][value='"+row+"']").attr("checked",true);
	});
	
	//Add tiempos ejecucion
	$.each(bandeObjDatoNotif.tiempo_ejecucion,function(i,row){
		var bandeObjHoraEjecu = {
				id_tiempo_ejecucion: row.id_tiempo_ejecucion,
				hora_ejecucion: row.hora_ejecucion,
				estado: row.estado
			};
		
		addTrHoraEjecu(bandeObjHoraEjecu);
	});
	
	//Add Medio difusion
	$.each(bandeObjDatoNotif.medio_difusion,function(i,row){
		var thisMedioDifus = $("#tblMedioDifusion :checkbox[name='chkIdMedioDifusion'][value='"+row.id_medio_difusion+"']");
		if(typeof thisMedioDifus != "undefined"){
			var idTr = "#"+thisMedioDifus.parent().parent().attr("id");
			
			$(idTr + " input[name='hidIdNotifMedioDifus']").val(row.id_notif_medio_difus);
			thisMedioDifus.attr("checked",true);
		}
	});
	
	//Add Medio difusion
	$.each(bandeObjDatoNotif.rol,function(i,row){
		var thisRol = $("#tblRol :checkbox[name='chkIdRol'][value='"+row.id_rol+"']");
		if(typeof thisRol != "undefined"){
			var idTr = "#"+thisRol.parent().parent().attr("id");
			
			$(idTr + " input[name='hidIdNotificacionRol']").val(row.id_notificacion_rol);
			thisRol.attr("checked",true);
		}
	});
	
	$("#btnGuardar").hide();
	$("#btnActualizar").show();
	$("#divBuscarNotificacion").dialog("close");
}


/********************************
 * METODOS PARA LAS VALIDACIONES
 * ******************************/

/**
 * METODO: Validar los datos del formulario
 * 
 * @returns Boolean
 * */
function isDataIncompleta(){
	var resultado = false;
	var banderaMensaje = "";
	//Validar datos generales
	if(validaCampoFormu()>0){ resultado = true;}
	
	//Verificar los dias de ejecucion
	if($(":checkbox[name='chkDiaEjecucion']:checked").length==0){
		banderaMensaje += ("Debe chequear los d\xEDas de ejecuci\xF3n! \n");
		resultado = true;
	}
	
	//Verificar los tiempos de ejecucion
	if($("#tblTiempoEjecucion tbody tr").length==0){
		banderaMensaje += ("Debe digitar los tiempos de ejecuci\xF3n! \n");
		resultado = true;
	}
	
	//Verificar los medios de difusion
	if($("#tblMedioDifusion tbody :checkbox:checked").length==0){
		banderaMensaje += ("Debe chequear los medios de difusi\xF3n! \n");
		resultado = true;
	}
	
	//Verificar los roles
	if($("#tblRol tbody :checkbox:checked").length==0){
		banderaMensaje += ("Debe chequear los medios de difusi\xF3n! \n");
		resultado = true;
	}
	
	//Mostrar el mensaje al usuario
	if(banderaMensaje!=""){
		alert(banderaMensaje);
	}
	
	return resultado;
}

/**
 * METODO: Valida los elementos requeridos del formulario
 * 
 * @returns int Numero de elementos vacios
 */
function validaCampoFormu(){
	//En este metodo no se validan los periodos a eliminar o actualizar
	
	var banderaError = 0;
	$( ".ui-state-error" ).removeClass( "ui-state-error" );
	$(".element-required").each(function(i,row){
		//Verifica que los campos esten visibles porque pueden ser periodos eliminados
		if($( this ).val() == 0 && $( this ).is(":visible")){
			$( this ).addClass( "ui-state-error" );
			banderaError++;
		}
	});
	return banderaError;
}

function nuevaNotificacion(){
	limpiarCampo(":checkbox,input,textarea,#tblTiempoEjecucion tbody, #tblBuscarNotificacion tbody");
	$("#btnGuardar").show();
	$("#btnActualizar").hide();
	globalDatosNotificacion = [];
}

function limpiarCampo( classCampo ){
	var usuario = $("#txtUsuario").val();
	var tipoCampo = "";
	$( classCampo ).each(function(i,row){
		
		if( $( this ).get(0).name == "base" ){
			$( this ).val(0);
			return;
		}
		tipoCampo = $( this ).get(0).type;
		
		if( typeof tipoCampo === "undefined" )
			tipoCampo = $( this ).get(0).localName;
		
		if( tipoCampo == "select-one" )
			$( this ).val(0);
		else if( tipoCampo == "text" || tipoCampo == "textarea" || tipoCampo == "hidden" )
			$( this ).val("");
		else if( tipoCampo == "div" || tipoCampo == "td" || tipoCampo == "span" )
			$( this ).text("");
		else if( tipoCampo ==  "tbody")
			$( this ).html("");
		else if( tipoCampo == "checkbox" )
			$( this ).attr("checked",false);
	});	
	$("#txtUsuario").val( usuario );
}