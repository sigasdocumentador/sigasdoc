<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once 'clases' . DIRECTORY_SEPARATOR . 'notificacion.class.php';

//Recibir datos del js
$objDatos= $_REQUEST['datos'];
$usuario=$_SESSION['USUARIO'];

$objNotificacion = new Notificacion;

$objNotificacion->inicioTransaccion();

$arrBanderaDatos = array();
$banderaError = 0;

//Guadar datos notificacion
$arrBanderaDatos["codigo"] = $objDatos["datos_generales"]["codigo"]; 
$arrBanderaDatos["enunciado"] = $objDatos["datos_generales"]["enunciado"]; 
$arrBanderaDatos["descripcion"] = $objDatos["datos_generales"]["descripcion"]; 
$arrBanderaDatos["query"] = $objDatos["datos_generales"]["query"]; 
$arrBanderaDatos["uri"] = $objDatos["datos_generales"]["uri"]; 
$arrBanderaDatos["dia_ejecucion"] = $objDatos["datos_generales"]["dia_ejecucion"]; 
$arrBanderaDatos["tiempo_expiracion"] = $objDatos["datos_generales"]["tiempo_expiracion"]; 
$arrBanderaDatos["estado"] = $objDatos["datos_generales"]["estado"];
 
$idNotificacion = $objNotificacion->insert_notificacion($arrBanderaDatos,$usuario);

//Guardar tiempo ejecucion
if($idNotificacion>0){
	$arrBanderaDatos = array();
	
	$arrBanderaDatos["fecha_ulima_ejecu"] = null;
	$arrBanderaDatos["id_notificacion"] = $idNotificacion;
	foreach($objDatos["datos_tiempo_ejecu"] as $rowDatos){
		$arrBanderaDatos["hora_ejecucion"] = $rowDatos["hora_ejecucion"];
		$arrBanderaDatos["estado"] = $rowDatos["estado"];
		
		$idTiempoEjecucion = $objNotificacion->insert_hora_notif($arrBanderaDatos,$usuario);
		if($idTiempoEjecucion==0){
			$banderaError++;
			break;
		}
	}
}

//Guardar medio difursion
if($banderaError==0){
	$arrBanderaDatos = array();
	
	$arrBanderaDatos["id_notificacion"] = $idNotificacion;
	foreach($objDatos["datos_medio_difus"] as $rowDatos){
		$arrBanderaDatos["id_medio_difusion"] = $rowDatos["id_medio_difusion"];
	
		$idMedioDifusion = $objNotificacion->insert_notif_medio_difus($arrBanderaDatos,$usuario);
		if($idMedioDifusion==0){
			$banderaError++;
			break;
		}
	}
}

//Guardar rol
if($banderaError==0){
	$arrBanderaDatos = array();

	$arrBanderaDatos["id_notificacion"] = $idNotificacion;
	foreach($objDatos["datos_rol"] as $rowDatos){
		$arrBanderaDatos["id_rol"] = $rowDatos["id_rol"];

		$idRol = $objNotificacion->insert_notif_rol($arrBanderaDatos,$usuario);
		if($idRol==0){
			$banderaError++;
			break;
		}
	}
}

if($banderaError==0 && $idNotificacion>0){
	$objNotificacion->confirmarTransaccion();
	echo 1;
}else{
	$objNotificacion->cancelarTransaccion();
	echo 0;
}
?>