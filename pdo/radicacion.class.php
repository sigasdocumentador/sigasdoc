<?php
set_time_limit(0);
ini_set("display_errors",'0');
date_default_timezone_set("America/Bogota");

$root= $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $_SESSION['RAIZ'] . DIRECTORY_SEPARATOR . '/rsc/pdo/IFXDbManejador.php';

class Radicacion {
	private $db;

/** CONSTRUCTOR **/
	function Radicacion () {
		$this->db = IFXDbManejador::conectarDB();
		if($this->db->conexionID==null){
			$cadena = $this->db->error;
			exit();
		}
 	}
	
/** CREATE **/
 	
 		//Insert aportes016_aux para eliminar aportes016 y actualizar aportes004
	 	function insertRadicacionErronea($campo0,$campo1,$campo2){
	 		
	 	$cont=0;
	 	$conte=0;
	 		 	
	 	$this->db->inicioTransaccion();
	 	//busca idradicado de aportes016
	 	 $sqlb="select * from aportes016 where idradicacion='".$campo0."'"; 	 
	 	 $rs = $this->db->querySimple($sqlb);
	 	 
	 	 //actualiza en aportes004 
	 	 $sqla="UPDATE aportes004 SET asignado='S', procesado='S', anulada='S', usuarioanula='$campo2', devuelto='S' WHERE idradicacion='$campo0'";
	 	 $rs1=$this->db->queryActualiza($sqla);
	 	 
	 	 if ($rs1==0){
	 	 ++$cont;
	 	 }

	 	 //inserta en aportes016_aux
	 	 while($row = $rs->fetch())
	 	 { 		

	 	 		$fechaRetiro = empty($row['fecharetiro'])? '(null)' : "'".$row['fecharetiro']."'";	 	 		
	 	 		$fechaNovedad = empty($row['fechanovedad'])? '(null)' : "'".$row['fechanovedad']."'";
	 	 		$fechaFidelidad = empty($row['fechafidelidad'])? '(null)' : "'".$row['fechafidelidad']."'";
	 	 		 		
				$sqli="INSERT INTO aportes016_aux ( idformulario,tipoformulario,tipoafiliacion,idempresa,idpersona
				,fechaingreso,horasdia,horasmes,salario,agricola,cargo,primaria,estado,fecharetiro
				,motivoretiro,fechanovedad,semanas,fechafidelidad,estadofidelidad
				,traslado,codigocaja,flag,tempo1,tempo2,fechasistema,usuario,tipopago,categoria,auditado
				,idagencia,idradicacion,vendedor,codigosalario,flag2,porplanilla,madrecomunitaria
				,claseafiliacion, fechaanula )
				VALUES ('".$row['idformulario']."','".$row['tipoformulario']."'
				,'".$row['tipoafiliacion']."','".$row['idempresa']."','".$row['idpersona']."'
				,'".$row['fechaingreso']."','".$row['horasdia']."','".$row['horasmes']."'
				,'".$row['salario']."','".$row['agricola']."','".$row['cargo']."'
				,'".$row['primaria']."','".$row['estado']."',$fechaRetiro
				,'".$row['motivoretiro']."',$fechaNovedad,'".$row['semanas']."'
				,$fechaFidelidad,'".$row['estadofidelidad']."','".$row['traslado']."'
				,'".$row['codigocaja']."','".$row['flag']."','".$row['tempo1']."'
				,'".$row['tempo2']."','".$row['fechasistema']."','".$row['usuario']."'
				,'".$row['tipopago']."','".$row['categoria']."','".$row['auditado']."'
				,'".$row['idagencia']."','".$row['idradicacion']."','".$row['vendedor']."'
				,'".$row['codigosalario']."','".$row['flag2']."','".$row['porplanilla']."'
				,'".$row['madrecomunitaria']."','".$row['claseafiliacion']."',cast(getDate() AS DATE))";
				
				$rs2=$this->db->queryInsert($sqli,'aportes016_aux');
				
				if ($rs2===null){
					++$cont;
				}
								
	 	 	}

	 	 	$sqlo="select  distinct a16.idpersona,a15.identificacion,a16.idradicacion,a16.idempresa
	 	 	,(select count(ap16.idpersona) from aportes016 ap16 where ap16.idempresa=a16.idempresa )+
             (select count(a17.idpersona) from aportes017 a17 where a17.idempresa=a48.idempresa)+
             (select count(a10.idtrabajador) from aportes010 a10 where a10.identificacion=a15.identificacion and a10.nit=a48.nit) as contar         
	 	 	from aportes016 a16
	 	 	inner join aportes015 a15 on a15.idpersona=a16.idpersona
	 	 	inner join aportes004 a04 on a04.numero=a15.identificacion
	 	 	inner join aportes048 a48 on a48.idempresa=a16.idempresa
	 	 	where 
             (select count(ap16.idpersona) from aportes016 ap16 where ap16.idempresa=a16.idempresa )+
             (select count(a17.idpersona) from aportes017 a17 where a17.idempresa=a48.idempresa)+ 
             (select count(a10.idtrabajador)  from aportes010 a10 where a10.identificacion=a15.identificacion and a10.nit=a48.nit)=1 
            and (select count(ap16.idpersona) from aportes016 ap16 where ap16.idempresa=a16.idempresa )=1
	 	 	and a04.idtiporadicacion in(2926)
	 	 	and a16.idpersona='".$campo1."' and a16.idradicacion='".$campo0."'";
	 	 	
	 	 	$rs5 = $this->db->querySimple($sqlo);
	 	 	 
	 	 	while($row5 = $rs5->fetch())
	 	 	{
	 	 		$idempresa=$row5['idempresa'];
	 	 		$contar=$row5['contar'];
	 	 		

	 	 		if($contar==1)
	 	 		{
	 	 			$sqlh="delete from aportes048 where idempresa='".$idempresa."'";
	 	 			$rs6=$this->db->querySimple($sqlh,'aportes048');

	 	 			if ($rs6==0){
	 	 				++$cont;	 	 				
	 	 			}	 	 			
	 	 			
	 	 		}
	 	 	
	 	 	}	
	 	//elimina la afiliacion
 	 	$sqlc="delete from aportes016 where idpersona='".$campo1."' and idradicacion='".$campo0."'";
 	 	$rsc = $this->db->querySimple($sqlc,'aportes016');
 	 	
 	 	if ($rsc==0){
 	 		++$cont;
 	 	} 	
 	 	
 	 	//cosulta en (aportes 017,021,010,043,18,19,101,104,009) para eliminar en la 15  	 	
 	 	$sqll="select
 	 	(select count(a21.idtrabajador) from aportes021 a21 where a21.idtrabajador=a15.idpersona)+
        (select count(ap21.idbeneficiario) from aportes021 ap21 where ap21.idbeneficiario=a15.idpersona)+
 	 	(select count(a17.idpersona) from aportes017 a17 where a17.idpersona=a15.idpersona)+
        (select count(a10.identificacion) from aportes010 a10 where a10.identificacion=a15.identificacion)+
        (select count(a43.idtrabajador) from aportes043 a43 where a43.idtrabajador=a15.idpersona)+
        (select count(a18.idtrabajador) from aportes018 a18 where a18.idtrabajador=a15.idpersona)+
        (select count(a19.idtrabajador) from aportes019 a19 where a19.idtrabajador=a15.idpersona)+
        (select count(a101.idpersona) from aportes101 a101 where a101.idpersona=a15.idpersona)+
        (select count(a104.idpersona) from aportes104 a104 where a104.idpersona=a15.idpersona)+
        (select count(a09.idtrabajador) from aportes009 a09 where a09.idtrabajador=a15.idpersona)+
        (select count(a14.idtrabajador) from aportes014 a14 where a14.idtrabajador=a15.idpersona )+
        (select count(ap14.idbeneficiario) from aportes014 ap14 where ap14.idbeneficiario=a15.idpersona) AS contador
 	 	from aportes015 a15 where a15.idpersona='".$campo1."'";
 	 	$rs3=$this->db->querySimple($sqll);
 	 	$contador=$rs3->fetchObject()->contador;
 	 	
 	 	if($contador==0)
 	 	{
 	 	//eliminar en aportes015
 	 	$sqle="delete from aportes015 where idpersona='".$campo1."'";
 	 	$rs4=$this->db->querySimple($sqle,'aportes015');
	 	 	if ($rs4==0){
	 	 		++$cont;
	 	 	}
 	 	}
 	 	
 	 	if($cont==0)
 	 	{
 	 		$this->db->confirmarTransaccion();
 	 		return 1;
 	 	}else{
 	 		$this->db->cancelarTransaccion();
 	 		return 0;
 	 	}
 	 	
 	 }// fin function
 		
 	function guardarRadicacion($campos){
 		
 		 		
 		$sql="INSERT INTO aportes004 ( fecharadicacion, identificacion, idtipodocumento, idtiporadicacion, idtipopresentacion, horainicio, horafinal, tiempo, notas, idtipoinformacion, idtipodocumentoafiliado, numero, nit, folios, idtipocertificado, idbeneficiario, observaciones, asignado, fechaasignacion, digitalizada, fechadigitalizacion, procesado, fechaproceso, flag, tempo1, fechasistema, 			 usuario, usuarioasignado, anulada, usuarioanula, idtipoformulario, devuelto, motivodevolucion, idagencia, idtipodocben, numerobeneficiario, afiliacionmultiple, cierre, recibeventanilla, recibegrabacion, fecharecibegraba, idtipomovilizacion,habeas_data ) 
 			  				  VALUES (:fecharadicacion,:identificacion,:idtipodocumento,:idtiporadicacion,:idtipopresentacion,:horainicio,:horafinal,:tiempo,:notas,:idtipoinformacion,:idtipodocumentoafiliado,:numero,:nit,:folios,:idtipocertificado,:idbeneficiario,:observaciones,:asignado,:fechaasignacion,:digitalizada,:fechadigitalizacion,:procesado,:fechaproceso,:flag,:tempo1,cast(getDate() AS DATE),:usuario,:usuarioasignado,:anulada,:usuarioanula,:idtipoformulario,:devuelto,:motivodevolucion,:idagencia,:idtipodocben,:numerobeneficiario,:afiliacionmultiple,:cierre,:recibeventanilla,:recibegrabacion,:fecharecibegraba,:idtipomovilizacion,:habeas_data )"; 		
 		
 		$statement = $this->db->conexionID->prepare($sql);
 		$campos->usuario=$_SESSION['USUARIO'];
 		$campos->idagencia=$_SESSION['AGENCIA'];
 		$guardada = false;
 		
 		$count=0;
 		$tmp = get_object_vars($campos); 		
 		foreach($tmp as $indice=>$valor){ 			
 			if($count>0 && $count!=26){ 
 				if(empty($valor)==true){ 
 					$statement->bindValue(":".$indice, null,  PDO::PARAM_STR);
 				} else { 
 					$statement->bindValue(":".$indice, $valor,  PDO::PARAM_STR);
 				}
 			}
 			$count++;
 		} 		
 		$guardada = $statement->execute();
 		if($guardada){
 			$rs = $this->db->conexionID->lastInsertId();
 			return trim($rs);
 		}else{
 			return 0;
 		} 		
 	}
 	
/** READ **/
 	
 /** buscar radicaciones para Eliminar dependiendo del tipo de radicacion**/
 	
 	function buscarRadicacionErronea($idp,$tipoDocumento){
 		 $sql="SELECT a15.idpersona,a15.identificacion,a15.pnombre+' '+isnull(a15.snombre,'')+' '+a15.papellido+' '+isnull(a15.sapellido,'') as trabajador
		,a04.idradicacion,a04.fecharadicacion,a04.procesado,a04.fechaproceso,a91.detalledefinicion,a48.idempresa,a48.nit,a48.razonsocial 
		from aportes015 a15 
		inner join aportes004 a04 on a04.numero=a15.identificacion 
		inner join aportes016 a16 on a16.idpersona=a15.idpersona and a16.idradicacion=a04.idradicacion
		inner join aportes091 a91 on a91.iddetalledef=a04.idtiporadicacion
		inner join aportes048 a48 on a48.idempresa=a16.idempresa
		where a04.idtiporadicacion in(28,29,2926) and a15.idtipodocumento='".$tipoDocumento."' and a15.identificacion='".$idp."' ";
 	 
 		return $this->db->conexionID->query($sql);
 	}

 	
 	function buscarRadicacionErronea1($radicado){
 		$sql="SELECT a15.idpersona,a15.identificacion,a15.pnombre+' '+isnull(a15.snombre,'')+' '+a15.papellido+' '+isnull(a15.sapellido,'') as trabajador
 		,a04.idradicacion,a04.fecharadicacion,a04.procesado,a04.fechaproceso,a91.detalledefinicion,a48.idempresa,a48.nit,a48.razonsocial
 		from aportes015 a15
 		inner join aportes004 a04 on a04.numero=a15.identificacion
 		inner join aportes016 a16 on a16.idpersona=a15.idpersona and a16.idradicacion=a04.idradicacion
 		inner join aportes091 a91 on a91.iddetalledef=a04.idtiporadicacion
 		inner join aportes048 a48 on a48.idempresa=a16.idempresa
 		where a04.idtiporadicacion in(28,29,2926) and a04.idradicacion='".$radicado."' ";
 			
 		return $this->db->conexionID->query($sql);
 	}

 /** buscar radicaciones por tipo pendientes**/
 	
 	function buscarRadicacionTipo($idr,$tipo){
 		$sql="SELECT idradicacion,fecharadicacion,horainicio,horafinal,tiempo,idtipodocumentoafiliado,numero,nit,folios,idtipocertificado,idbeneficiario,procesado,fechasistema,idtipoformulario,idagencia,idtipodocben,numerobeneficiario from aportes004 where idradicacion=$idr and idtiporadicacion in ($tipo)";
 		return $this->db->conexionID->query($sql);
 	}
 	
/** Busca si la empresa no tiene radicaciones de afiliacion o radicacion pendientes **/
 	function buscarRadicacionPendienteEmpresa($nit,$tr){
 		$con=0;
 		$sql="SELECT count(idradicacion) cant FROM aportes004  WHERE nit='$nit' AND procesado='N' AND idtiporadicacion in ($tr)";
 		$rs = $this->db->querySimple($sql);
		while($row = $rs->fetch()){ $con=$row['cant']; }
		return $con;
 	}
 	
 	/** Busca si la empresa no tiene radicaciones de afiliacion o radicacion pendientes **/
 	function buscarRadicacionPendienteAfiliado($tr,$td,$num){
 		$con=0;
 		$sql="SELECT count(idradicacion) cant FROM aportes004  WHERE numero='$num' AND idtipodocumentoafiliado='$td' AND procesado='N' AND idtiporadicacion in ($tr)";
 		$rs = $this->db->querySimple($sql);
 		while($row = $rs->fetch()){
 			$con=$row['cant'];
 		}
 		return $con;
 	}

/** buscar radicaciones por id **/
 	function buscarRadicacionId($idr){
 		$sql="SELECT idradicacion,fecharadicacion,horainicio,horafinal,tiempo,idtipodocumentoafiliado,numero,nit,folios,idtipocertificado,idbeneficiario,procesado,fechasistema,idtipoformulario,idagencia,idtipodocben,numerobeneficiario from aportes004 where idradicacion=$idr ";
 		return $this->db->conexionID->query($sql);
 	}
 	
/** UPDATE **/
 	
 	function updateCerrarRadicacion($idr,$usuario){
 		$sql="UPDATE aportes004 SET procesado='S', fechaproceso=cast(getdate() as date), recibegrabacion='$usuario' WHERE idradicacion=$idr";
 		return $this->db->queryActualiza($sql); 		
 	}
 	
 	function updateAnularRadicacion($idr,$usuario){
 		$sql="UPDATE aportes004 SET asignado='S', procesado='S', anulada='S', usuarioanula='$usuario', devuelto='S' WHERE idradicacion=$idr";
 		return $this->db->queryActualiza($sql);
 	}

 	function actualizaObservacionErronea($idr,$ob){
 		$sql="UPDATE aportes004 SET notas='$ob' WHERE idradicacion='".$idr."'";
 		return $this->db->queryActualiza($sql);
 	}
 	
/** DELETE **/
 	
/** RADICAR APROBACION DE HABEAS DATA TABLA APORTES015**/
    function radicarhabeasdata($tipoIden,$numIden,$habeas_data){
    	$sql="select * from aportes015 where idtipodocumento=$tipoIden and identificacion='$numIden'";
    	$res=$this->db->querySimple($sql);
    	$ctrerror=0;
    	while($row=$res->fetch()){
    		 // Si el campo habeas_data de la tabla aportes015 no es vacio y es diferente el nuevo dato se actualiza
    		 if($row['habeas_data']!=$habeas_data){
    		 	$sqlupd="update aportes015 set habeas_data=$habeas_data,fechahd=cast(getdate() as date),usuariohd='".$_SESSION['USUARIO']."' where idpersona=".$row['idpersona'];
    		 }
    		 
    		 $resupd=$this->db->queryActualiza($sqlupd);
    		 
    		 if($resupd==false and $sqlupd!=""){
    		 	$ctrerror=1;
    		 	break;
    		 }
    		 
    	}
    	
    	
    	return  $ctrerror;
    	
    }	
 	
 	
}	
?>