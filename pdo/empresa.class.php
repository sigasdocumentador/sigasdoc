<?php
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");

$root= $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $_SESSION['RAIZ'] . DIRECTORY_SEPARATOR . '/rsc/pdo/IFXDbManejador.php';

class Empresa{
	private $db;
	
/** CONSTRUCTOR **/
	function Empresa(){
		$this->db = IFXDbManejador::conectarDB();
		if($this->db->conexionID==null){
			$cadena = $this->db->error;
			echo msg_error($cadena);
			exit();
		}
 	}
	
/** CREATE **/
 	
 	function guardarEmpresa($campos){
 		$sql="INSERT INTO aportes048 ( idtipodocumento, nit, digito, nitrsn, codigosucursal, principal, razonsocial, sigla, direccion, iddepartamento, idciudad, idzona, telefono, fax, url, email, idrepresentante, idjefepersonal, contratista, colegio, exento, idcodigoactividad, actieconomicadane, indicador, idasesor, fechamatricula, idsector, seccional, tipopersona, claseaportante, tipoaportante, estado, codigoestado, fechaestado, fechaaportes, fechaafiliacion, trabajadores, aportantes, conyuges, hijos, hermanos, padres, tempo, flag, usuario, fechasistema, 			idclasesociedad, rutadocumentos, legalizada, renovacion, cedrep, representante, auditado, idtipoafiliacion, exenta, codigotama, clase, carta, porplanilla, idpais )
 							  VALUES (:idtipodocumento,:nit,:digito,:nitrsn,:codigosucursal,:principal,:razonsocial,:sigla,:direccion,:iddepartamento,:idciudad,:idzona,:telefono,:fax,:url,:email,:idrepresentante,:idjefepersonal,:contratista,:colegio,:exento,:idcodigoactividad,:actieconomicadane,:indicador,:idasesor,:fechamatricula,:idsector,:seccional,:tipopersona,:claseaportante,:tipoaportante,:estado,:codigoestado,:fechaestado,:fechaaportes,:fechaafiliacion,:trabajadores,:aportantes,:conyuges,:hijos,:hermanos,:padres,:tempo,:flag,:usuario,cast(getdate() as date),:idclasesociedad,:rutadocumentos,:legalizada,:renovacion,:cedrep,:representante,:auditado,:idtipoafiliacion,:exenta,:codigotama,:clase,:carta,:porplanilla,:idpais )";
 							   		
 		$statement = $this->db->conexionID->prepare($sql);
 		$campos->usuario=$_SESSION['USUARIO'];
 		$campos->seccional=$_SESSION['AGENCIA'];
 		$guardada = false;

 		$count=0;
 		$tmp = get_object_vars($campos);
 		foreach($tmp as $indice=>$valor){
 			if($count>0 && $count!=46){
 				if(empty($valor)==true){ $statement->bindValue(":".$indice, null,  PDO::PARAM_STR);
 				} else { $statement->bindValue(":".$indice, $valor,  PDO::PARAM_STR); }
 			}
 			$count++;
 		} 		
 		$guardada = $statement->execute(); 		
 		if($guardada){
 			$rs = $this->db->conexionID->lastInsertId('aportes048');
 			return trim($rs);
 		}else{
 			return 0;
 		} 		
 	}
 	
	function actualizarEmpresaCompleta($campos){
		$usuario = $_SESSION['USUARIO'];
	 	$campo00 = $campos->idempresa;
	 	$campo01 = empty( $campos->idtipodocumento )   ? NULL : $campos->idtipodocumento;
	 	$campo02 = empty( $campos->nit )               ? NULL : $campos->nit;
	 	$campo03 = empty( $campos->digito )            ? NULL : $campos->digito;
	 	$campo04 = empty( $campos->razonsocial )       ? NULL : $campos->razonsocial;
	 	$campo05 = empty( $campos->direccion )         ? NULL : $campos->direccion;
	 	$campo06 = empty( $campos->iddepartamento )    ? NULL : $campos->iddepartamento;
	 	$campo07 = empty( $campos->idciudad )          ? NULL : $campos->idciudad;
	 	$campo08 = empty( $campos->idzona )            ? NULL : $campos->idzona;
	 	$campo09 = empty( $campos->telefono )          ? NULL : $campos->telefono;
	 	$campo10 = empty( $campos->fax )               ? NULL : $campos->fax;
	 	$campo11 = empty( $campos->url )               ? NULL : $campos->url;
	 	$campo12 = empty( $campos->email )             ? NULL : $campos->email;
	 	$campo13 = empty( $campos->idrepresentante )   ? NULL : $campos->idrepresentante;
	 	$campo14 = empty( $campos->idjefepersonal )    ? NULL : $campos->idjefepersonal;
	 	$campo15 = empty( $campos->contratista )       ? NULL : $campos->contratista;
	 	$campo16 = empty( $campos->colegio )           ? NULL : $campos->colegio;
	 	$campo17 = empty( $campos->exento )            ? NULL : $campos->exento;
	 	$campo18 = empty( $campos->idcodigoactividad ) ? NULL : $campos->idcodigoactividad;
	 	$campo19 = empty( $campos->indicador )         ? NULL : $campos->indicador;
	 	$campo20 = empty( $campos->fechamatricula )    ? NULL : $campos->fechamatricula;
	 	$campo21 = empty( $campos->idsector )          ? NULL : $campos->idsector;
	 	$campo22 = empty( $campos->seccional )         ? NULL : $campos->seccional;
	 	$campo23 = empty( $campos->tipopersona )       ? NULL : $campos->tipopersona;
	 	$campo24 = empty( $campos->claseaportante )    ? NULL : $campos->claseaportante;
	 	$campo25 = empty( $campos->tipoaportante )     ? NULL : $campos->tipoaportante;
	 	$campo26 = empty( $campos->estado )            ? NULL : $campos->estado;
	 	$campo27 = empty( $campos->codigoestado )      ? NULL : $campos->codigoestado;
	 	$campo28 = empty( $campos->fechaaportes )      ? NULL : $campos->fechaaportes;
	 	$campo29 = empty( $campos->fechaafiliacion )   ? NULL : $campos->fechaafiliacion;
	 	$campo30 = empty( $campos->trabajadores )      ? NULL : $campos->trabajadores;
	 	$campo31 = empty( $campos->aportantes )        ? NULL : $campos->aportantes;
	 	$campo32 = empty( $campos->conyuges )          ? NULL : $campos->conyuges;
	 	$campo33 = empty( $campos->hijos )             ? NULL : $campos->hijos;
	 	$campo34 = empty( $campos->hermanos )          ? NULL : $campos->hermanos;
	 	$campo35 = empty( $campos->padres )            ? NULL : $campos->padres;
	 	$campo36 = empty( $campos->tempo )             ? NULL : $campos->tempo;
	 	$campo37 = empty( $campos->flag )              ? NULL : $campos->flag;
	 	$campo38 = empty( $campos->idclasesociedad )   ? NULL : $campos->idclasesociedad;
	 	$campo39 = empty( $campos->rutadocumentos )    ? NULL : $campos->rutadocumentos;
	 	$campo40 = empty( $campos->legalizada )        ? NULL : $campos->legalizada;
	 	$campo41 = empty( $campos->renovacion )        ? NULL : $campos->renovacion;
	 	$campo42 = empty( $campos->cedrep )            ? NULL : $campos->cedrep;
	 	$campo43 = empty( $campos->representante )     ? NULL : $campos->representante;
	 	$campo44 = empty( $campos->auditado )          ? NULL : $campos->auditado;
	 	$campo45 = empty( $campos->idtipoafiliacion )  ? NULL : $campos->idtipoafiliacion;
	 	$campo46 = empty( $campos->codigotama )        ? NULL : $campos->codigotama;
	 	$campo47 = empty( $campos->clase )             ? NULL : $campos->clase;
	 	$campo48 = empty( $campos->carta )             ? NULL : $campos->carta;
	 	$campo49 = empty( $campos->porplanilla )       ? NULL : $campos->porplanilla;
	 		
	 	$sql="UPDATE aportes048 
					SET idtipodocumento = :campo01, nit= :campo02, digito = :campo03,
	    				razonsocial = :campo04 , direccion = :campo05, iddepartamento = :campo06, 
	    				idciudad = :campo07 , idzona = :campo08, telefono = :campo09, fax = :campo10, url = :campo11,
	    				email = :campo12, idrepresentante = :campo13, idjefepersonal = :campo14, contratista = :campo15, colegio = :campo16, 
	    				exento = :campo17, idcodigoactividad = :campo18, indicador = :campo19, 
	    				fechamatricula = :campo20, idsector = :campo21, seccional = :campo22, tipopersona = :campo23, claseaportante = :campo24, 
	    				tipoaportante = :campo25, estado = :campo26, codigoestado = :campo27, fechaaportes = :campo28,
	    				fechaafiliacion = :campo29, trabajadores = :campo30, aportantes = :campo31, conyuges = :campo32, hijos = :campo33,
	    				hermanos = :campo34, padres = :campo35, tempo = :campo36, flag = :campo37, idclasesociedad = :campo38,
	    				rutadocumentos = :campo39, legalizada = :campo40, renovacion = :campo41, 
	    				cedrep = :campo42, representante = :campo43, auditado = :campo44, idtipoafiliacion = :campo45, codigotama = :campo46, 
	    				clase = :campo47, carta = :campo48, porplanilla = :campo49, usuario = :campo50
				WHERE idempresa = $campo00";
	 		
	 	$statement = $this->db->conexionID->prepare($sql);
	 	$guardada = false;
	 	
	 	$statement->bindParam(':campo01', $campo01, PDO::PARAM_INT);
	 	$statement->bindParam(':campo02', $campo02, PDO::PARAM_STR);
	 	$statement->bindParam(':campo03', $campo03, PDO::PARAM_STR);
	 	$statement->bindParam(':campo04', $campo04, PDO::PARAM_STR);
	 	$statement->bindParam(':campo05', $campo05, PDO::PARAM_STR);
	 	$statement->bindParam(':campo06', $campo06, PDO::PARAM_STR);
	 	$statement->bindParam(':campo07', $campo07, PDO::PARAM_STR);
	 	$statement->bindParam(':campo08', $campo08, PDO::PARAM_STR);
	 	$statement->bindParam(':campo09', $campo09, PDO::PARAM_STR);
	 	$statement->bindParam(':campo10', $campo10, PDO::PARAM_STR);
	 	$statement->bindParam(':campo11', $campo11, PDO::PARAM_STR);
	 	$statement->bindParam(':campo12', $campo12, PDO::PARAM_STR);
	 	$statement->bindParam(':campo13', $campo13, PDO::PARAM_INT);
	 	$statement->bindParam(':campo14', $campo14, PDO::PARAM_STR);
	 	$statement->bindParam(':campo15', $campo15, PDO::PARAM_STR);
	 	$statement->bindParam(':campo16', $campo16, PDO::PARAM_STR);
	 	$statement->bindParam(':campo17', $campo17, PDO::PARAM_STR);
	 	$statement->bindParam(':campo18', $campo18, PDO::PARAM_STR);
	 	$statement->bindParam(':campo19', $campo19, PDO::PARAM_STR);
	 	$statement->bindParam(':campo20', $campo20, PDO::PARAM_STR);
	 	$statement->bindParam(':campo21', $campo21, PDO::PARAM_STR);
	 	$statement->bindParam(':campo22', $campo22, PDO::PARAM_STR);
	 	$statement->bindParam(':campo23', $campo23, PDO::PARAM_STR);
	 	$statement->bindParam(':campo24', $campo24, PDO::PARAM_STR);
	 	$statement->bindParam(':campo25', $campo25, PDO::PARAM_STR);
	 	$statement->bindParam(':campo26', $campo26, PDO::PARAM_STR);
	 	$statement->bindParam(':campo27', $campo27, PDO::PARAM_STR);
	 	$statement->bindParam(':campo28', $campo28, PDO::PARAM_STR);
	 	$statement->bindParam(':campo29', $campo29, PDO::PARAM_STR);
	 	$statement->bindParam(':campo30', $campo30, PDO::PARAM_STR);
	 	$statement->bindParam(':campo31', $campo31, PDO::PARAM_STR);
	 	$statement->bindParam(':campo32', $campo32, PDO::PARAM_STR);
	 	$statement->bindParam(':campo33', $campo33, PDO::PARAM_STR);
	 	$statement->bindParam(':campo34', $campo34, PDO::PARAM_STR);
	 	$statement->bindParam(':campo35', $campo35, PDO::PARAM_STR);
	 	$statement->bindParam(':campo36', $campo36, PDO::PARAM_STR);
	 	$statement->bindParam(':campo37', $campo37, PDO::PARAM_STR);
	 	$statement->bindParam(':campo38', $campo38, PDO::PARAM_STR);
	 	$statement->bindParam(':campo39', $campo39, PDO::PARAM_STR);
	 	$statement->bindParam(':campo40', $campo40, PDO::PARAM_STR);
	 	$statement->bindParam(':campo41', $campo41, PDO::PARAM_STR);
	 	$statement->bindParam(':campo42', $campo42, PDO::PARAM_STR);
	 	$statement->bindParam(':campo43', $campo43, PDO::PARAM_STR);
	 	$statement->bindParam(':campo44', $campo44, PDO::PARAM_STR);
	 	$statement->bindParam(':campo45', $campo45, PDO::PARAM_STR);
	 	$statement->bindParam(':campo46', $campo46, PDO::PARAM_STR);
	 	$statement->bindParam(':campo47', $campo47, PDO::PARAM_STR);
	 	$statement->bindParam(':campo48', $campo48, PDO::PARAM_STR);
	 	$statement->bindParam(':campo49', $campo49, PDO::PARAM_STR);
	 	$statement->bindParam(':campo50', $usuario, PDO::PARAM_STR);
	 	
	 	$guardada = $statement->execute();
	 			
	 	return $guardada ? 1 : 0;
	 		
	}
/** READ **/
 	function buscarRazonSocial($nit='0',$ide=0,$op){
 		if($op==1){
 			//por nit
 			$sql="SELECT idempresa, razonsocial from aportes048 where nit='$nit'";
 		}
 		else{
 			//por ide
 			$sql="SELECT nit, razonsocial from aportes048 where idempresa=$ide";
 		}
 		$rs=$this->db->conexionID->query($sql)->fetch();
 		if(is_array($rs)){
 			return json_encode($rs);
 		}
 		else {
 			return 0;
 		}
 		
 	}
 	
 	function buscarEmpresaNit($nit){
 		$data = array();
 		$con = 0;
 		$sql = "SELECT a48.* FROM aportes048 a48 WHERE a48.nit='$nit' --and a48.principal='S'";
 		$rs = $this->db->querySimple($sql);
 		while($row = $rs->fetch()){
 			$con++;
 			$data[]=array_map("utf8_encode",$row);
 		}
 		if($con>0) {
 			return array($data[0]);
 		} else 
 			return 0; 		
 	}
 	
 	function buscarEmpresaID($ide){
 		$data = array();
 		$con = 0;
 		$sql = "SELECT a48.* FROM aportes048 a48 WHERE a48.idempresa=$ide";
 		$rs = $this->db->querySimple($sql);
 		while($row = $rs->fetch()){
 			$con++;
 			$data[]=array_map("utf8_encode",$row);
 		}
 		if($con>0) {
 			return $data[0];
 		} else
 			return 0;
 	}
 	
 	function buscarDatosEmpresa($nit){
 		$data = array();
 		$con = 0;
 		$sql = "SELECT DISTINCT idempresa, aportes048.idtipodocumento, nit, digito, codigosucursal, principal, razonsocial, sigla, aportes048.direccion, iddepartamento, idciudad, aportes048.idzona, aportes048.telefono, fax, aportes048.url, aportes048.email, idrepresentante, idjefepersonal, contratista, idcodigoactividad, actieconomicadane, indicador, idasesor, fechamatricula, aportes048.fechaestado, idsector, seccional, tipopersona, claseaportante, tipoaportante, aportes048.estado, codigoestado, fechaestado, fechaaportes, aportes048.fechaafiliacion, trabajadores, aportantes, aportes048.usuario, aportes048.fechasistema, idclasesociedad, aportes048.rutadocumentos, legalizada,d.departmento,m.municipio, r.papellido AS par,r.sapellido AS sar,r.pnombre AS pnr,r.snombre snr, c.papellido AS pac,c.sapellido AS sac,c.pnombre AS pnc,c.snombre snc, ca.descripcion AS actividad, da.clase AS codact, da.descripcion AS dane, i.detalledefinicion AS indicador, s.detalledefinicion AS sector, p.detalledefinicion tipopersona,cs.detalledefinicion AS clasesoci, capo.detalledefinicion AS clase_apo, ta.detalledefinicion AS tipo_apo, td.detalledefinicion AS tipo_doc
				FROM aportes048 
				LEFT JOIN aportes089 d ON aportes048.iddepartamento=d.coddepartamento
				LEFT JOIN aportes089 m ON aportes048.idciudad=m.codmunicipio
				LEFT JOIN aportes015 r ON aportes048.idrepresentante=r.idpersona
				LEFT JOIN aportes015 c ON aportes048.idjefepersonal=c.idpersona
				LEFT JOIN aportes079 ca ON aportes048.idcodigoactividad=ca.idciiu
				LEFT JOIN aportes079 da ON aportes048.actieconomicadane=da.clase
				LEFT JOIN aportes091 i ON aportes048.indicador=i.iddetalledef
				LEFT JOIN aportes091 s ON aportes048.idsector=s.iddetalledef
				LEFT JOIN aportes091 p ON aportes048.tipopersona=p.iddetalledef
				LEFT JOIN aportes091 cs ON aportes048.idclasesociedad=cs.iddetalledef 
				LEFT JOIN aportes091 capo ON aportes048.claseaportante=capo.iddetalledef
				LEFT JOIN aportes091 ta ON aportes048.tipoaportante=ta.iddetalledef
				LEFT JOIN aportes091 td ON aportes048.idtipodocumento=td.iddetalledef
				where nit='$nit'"; 		
 		$rs = $this->db->querySimple($sql); 		
 		while($row = $rs->fetch()){
 			$con++;
 			$data[]=array_map("utf8_encode",$row);
 		}
 		if($con>0) {
 			return $data[0];
 		} else
 			return 0;
 	}

 	function contNitDuplicado($idempresa, $nit){
 		$sql = "SELECT count(idempresa) AS contador FROM aportes048 WHERE idempresa != $idempresa AND nit = '$nit'";
 		$rs = $this->db->querySimple($sql);
 		if( ( $row = $rs->fetch() ) == true ){
 			return $row["contador"];
 		}else{
 			return 0;
 		}
 		
 	}
	 	
/** UPDATE **/
 	
 	function updateEmpresa($campos){
 		$sql="UPDATE aportes048
				SET idtipodocumento = :idtipodocumento, nit = :nit, digito = :digito, nitrsn = :nitrsn, codigosucursal = :codigosucursal, principal = :principal, razonsocial = :razonsocial, sigla = :sigla, direccion = :direccion, iddepartamento = :iddepartamento, idciudad = :idciudad, idzona = :idzona, telefono = :telefono, fax = :fax, url = :url, email = :email, idrepresentante = :idrepresentante, idjefepersonal = :idjefepersonal, contratista = :contratista, colegio = :colegio, exento = :exento, idcodigoactividad = :idcodigoactividad, actieconomicadane = :actieconomicadane, indicador = :indicador, idasesor = :idasesor, fechamatricula = :fechamatricula,
					idsector = :idsector, seccional = :seccional, tipopersona = :tipopersona, claseaportante = :claseaportante, tipoaportante = :tipoaportante, estado = :estado, codigoestado = :codigoestado, fechaestado = :fechaestado, fechaaportes = :fechaaportes, fechaafiliacion = :fechaafiliacion, trabajadores = :trabajadores, aportantes = :aportantes, conyuges = :conyuges, hijos = :hijos, hermanos = :hermanos, padres = :padres, tempo = :tempo, flag = :flag, usuario = :usuario, idclasesociedad = :idclasesociedad, rutadocumentos = :rutadocumentos, legalizada = :legalizada, renovacion = :renovacion,
					cedrep = :cedrep, representante = :representante, auditado = :auditado, idtipoafiliacion = :idtipoafiliacion, exenta = :exenta, codigotama = :codigotama, clase = :clase, carta = :carta, porplanilla = :porplanilla, idpais = :idpais 
				WHERE idempresa = :idempresa";
 		
 		$statement = $this->db->conexionID->prepare($sql);
 		$campos->usuario=$_SESSION['USUARIO'];
 		$guardada = false;
 	
 		$count=0;
 		$tmp = get_object_vars($campos);
 		foreach($tmp as $indice=>$valor){
 			if($count!=46){ 				
 				if(empty($valor)==true){ $statement->bindValue(":".$indice, null,  PDO::PARAM_STR); 
 				} else { $statement->bindValue(":".$indice, $valor,  PDO::PARAM_STR); }
 			}
 			$count++;
 		}
 		
 		$guardada = $statement->execute();
	 			
	 	return $guardada ? 1 : 0;
 	}
 	
 	function updateEmpresaIndependiente($campos){
 		$empresa=$this->buscarEmpresaID($campos[0]);
 		if($empresa!=0){
 			if($empresa['estado']!=$campos[1]){
 				$sql="UPDATE aportes048 SET estado = '$campos[1]', contratista = '$campos[2]', claseaportante=$campos[3], tipoaportante=$campos[4], idtipoafiliacion=$campos[5], usuario='$campos[6]', legalizada='S', fechaestado=CAST(getDate() AS DATE) WHERE idempresa=$campos[0]";
 			} else {
 				$sql="UPDATE aportes048 SET estado = '$campos[1]', contratista = '$campos[2]', claseaportante=$campos[3], tipoaportante=$campos[4], idtipoafiliacion=$campos[5], usuario='$campos[6]', legalizada='S' WHERE idempresa=$campos[0]";
 			}
	 		return $this->db->queryActualiza($sql);
 		} else {
 			return 0;
 		}
 	}
 	
 	function updateEmpresaContratista($campos){
 		$empresa=$this->buscarEmpresaID($campos[0]);
 		if($empresa!=0){
 			if($empresa['estado']!=$campos[1]){
 				$sql="UPDATE aportes048 SET estado = '$campos[1]', contratista = '$campos[2]', usuario='$campos[3]', legalizada='S', fechaestado=CAST(getDate() AS DATE) WHERE idempresa=$campos[0]";
 			} else {
 				$sql="UPDATE aportes048 SET estado = '$campos[1]', contratista = '$campos[2]', usuario='$campos[3]', legalizada='S' WHERE idempresa=$campos[0]";
 			}
 			return $this->db->queryActualiza($sql);
 		} else {
 			return 0;
 		}
 	}
 	
/** DELETE **/
}	
?>