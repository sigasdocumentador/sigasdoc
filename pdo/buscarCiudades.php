 <?php
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");
 
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
$raiz=$_SESSION['RAIZ'];
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
  	$cadena = $db->error;
  	echo $cadena;
  	exit();
}

$idp=$_REQUEST['id'];

switch($idp){
	case 1 : $sql = "SELECT DISTINCT idpais idparent,coddepartamento idchild,departmento child FROM aportes089 ORDER BY departmento"; break;
	case 2 : $sql = "SELECT DISTINCT coddepartamento idparent,codmunicipio idchild,municipio child FROM aportes089 order by municipio"; break;
	case 3 : $sql = "SELECT DISTINCT codmunicipio idparent,codzona idchild,zona child FROM aportes089 ORDER BY zona"; break;
	case 4 : $sql = "SELECT DISTINCT codigozona idparent,idbarrio idchild,barrio child from aportes087 ORDER BY barrio"; break;
}

$con=0;
$rs = $db->querySimple($sql);
$data = array();

while( $row = $rs->fetch() ){
	$data[]=array_map("utf8_encode",$row);
	$con++;
}

if($con>0){
	echo json_encode($data);
}else {
	echo 0;
}   
?> 
