<?php
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");

$root= $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $_SESSION['RAIZ'] . DIRECTORY_SEPARATOR . '/rsc/pdo/IFXDbManejador.php';

class PaqueteEscolar{
	private $db;
	
/** CONSTRUCTOR **/
	function PaqueteEscolar(){
		$this->db = IFXDbManejador::conectarDB();
		if($this->db->conexionID==null){
			$cadena = $this->db->error;
			echo msg_error($cadena);
			exit();
		}
 	}
	
/** CREATE **/
 	 	 	
/** READ **/
 	
 	/** Trae informacion basica de todos los certificados de una persona **/
 	function buscarBasicoPaquetes($idp){
 		$data = array(); $con = 0;
 		$sql = "SELECT a57.idpersona, a15.pnombre,a15.snombre,a15.papellido,a15.sapellido,a57.anno,a57.entregado,a57.fechaentrega,a57.horaentrega,a57.agencia,a57.mesa,a57.edadbeneficiario 
				FROM aportes057 a57 INNER JOIN aportes015 a15 ON a57.idbeneficiario=a15.idpersona WHERE a57.idpersona=$idp  ORDER BY a57.anno DESC"; 		
 		$rs = $this->db->querySimple($sql);
 		while($row = $rs->fetch()){
 			$con++;
 			$data[]=array_map("utf8_encode",$row);
 		}
 		if($con>0) {
 			return $data;
 		} else
 			return 0;
 	}
 	
/** UPDATE **/
 	
/** DELETE **/
}	
?>