<?php
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");

$root= $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $_SESSION['RAIZ'] . DIRECTORY_SEPARATOR . '/rsc/pdo/IFXDbManejador.php';

class Certificado{
	private $db;
	
/** CONSTRUCTOR **/
	function Certificado(){
		$this->db = IFXDbManejador::conectarDB();
		if($this->db->conexionID==null){
			$cadena = $this->db->error;
			echo msg_error($cadena);
			exit();
		}
 	}
 	
 	public function inicioTransaccion(){
 		$this->db->conexionID->beginTransaction();
 	}
 	
 	public function cancelarTransaccion(){
 		$this->db->conexionID->rollBack();
 	}
 	
 	public function confirmarTransaccion(){
 		$this->db->conexionID->commit();
 	}
	
/** CREATE **/
 	
 	function guardarCertificado($campos){
 		$sql="INSERT INTO aportes026 ( idbeneficiario, idparentesco, idtipocertificado, periodoinicio, periodofinal, fechapresentacion, 	 formapresentacion, estado, usuario, fechasistema 		   )
 			                  VALUES (:idbeneficiario,:idparentesco,:idtipocertificado,:periodoinicio,:periodofinal,cast(getDate() AS DATE),:formapresentacion,:estado,:usuario,cast(getDate() AS DATE) )";
 			
 		$statement = $this->db->conexionID->prepare($sql);
 		$campos->usuario=$_SESSION['USUARIO']; 		
 		$guardada = false;
 			
 		$count=0;
 		$tmp = get_object_vars($campos);
 		foreach($tmp as $indice=>$valor){
 			if($count>0 && $count!=6 && $count!=10){
 				if(empty($valor)==true){ $statement->bindValue(":".$indice, null,  PDO::PARAM_STR);
 				} else { $statement->bindValue(":".$indice, $valor,  PDO::PARAM_STR); }
 			}
 			$count++;
 		}
 		$guardada = $statement->execute();
 		if($guardada){
 			$rs = $this->db->conexionID->lastInsertId();
 			return trim($rs);
 		}else{
 			return 0;
 		}
 	}
 	 	 	
/** READ **/
 	
 	/** Trae informacion basica de todos los certificados de una persona **/
 	function buscarBasicoCertificados($idb){
 		$data = array(); $con = 0;
 		$sql = "SELECT a26.idcertificado,a91.detalledefinicion tipoCertificado,a26.periodoinicio,a26.periodofinal,a26.fechapresentacion
				FROM aportes026 a26 INNER JOIN aportes091 a91 ON a26.idtipocertificado=a91.iddetalledef WHERE a26.idbeneficiario=$idb ORDER BY fechapresentacion DESC"; 		
 		$rs = $this->db->querySimple($sql);
 		while($row = $rs->fetch()){
 			$con++;
 			$data[]=array_map("utf8_encode",$row);
 		}
 		if($con>0) {
 			return $data;
 		} else
 			return 0;
 	}
 	
/** UPDATE **/
 	
 	function inactivarCertificados($idb){
 		$sql="UPDATE aportes026 SET estado='I' WHERE idbeneficiario=:campo0";
 		$statement = $this->db->conexionID->prepare($sql);
 		$guardada = false;
 		$statement->bindParam(":campo0",  $idb,  PDO::PARAM_INT);
 		$guardada = $statement->execute();
 			
 		return $guardada ? 1 : 0;
 	}
 	
/** DELETE **/
}	
?>