<?php
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");

$root= $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $_SESSION['RAIZ'] . DIRECTORY_SEPARATOR . '/rsc/pdo/IFXDbManejador.php';

class Documento{
	private $db;
	
/** CONSTRUCTOR **/
	function Documento(){
		$this->db = IFXDbManejador::conectarDB();
		if($this->db->conexionID==null){
			$cadena = $this->db->error;
			echo msg_error($cadena);
			exit();
		}
 	}
 	
 	public function inicioTransaccion(){
 		$this->db->conexionID->beginTransaction();
 	}
 	
 	public function cancelarTransaccion(){
 		$this->db->conexionID->rollBack();
 	}
 	
 	public function confirmarTransaccion(){
 		$this->db->conexionID->commit();
 	}
	
/** CREATE **/
 	
 	function guardarDocumento($idradicacion, $idpersona, $usuario){
 		$sql="INSERT INTO aportes028 ( idradicacion, idpersona, usuario, fechasistema )
							  VALUES (:idradicacion,:idpersona,:usuario, CAST(getDate() AS DATE) )";
 		
 		$statement = $this->db->conexionID->prepare($sql);
 		$guardada = false;
 		
 		$statement->bindValue(":idradicacion", $idradicacion,  PDO::PARAM_STR);
 		$statement->bindValue(":idpersona", $idpersona,  PDO::PARAM_STR);
 		$statement->bindValue(":usuario", $usuario,  PDO::PARAM_STR);
 		 		
 		$guardada = $statement->execute();
 		if($guardada){
 			$rs = $this->db->conexionID->lastInsertId();
 			return trim($rs);
 		} else {
 			return 0;
 		}
 	}
 	
 	function guardarDetalleDocumento($campos){
 		$sql="INSERT INTO aportes029 ( idregistro, iddocumento, cantidad, nombrearchivo, programa, digitalizado, fechasistema           , usuario )
							  VALUES (:idregistro,:iddocumento,:cantidad,:nombrearchivo,:programa,:digitalizado, CAST(getDate() AS DATE),:usuario )";
 			
 		$statement = $this->db->conexionID->prepare($sql);
 		$campos->usuario=$_SESSION['USUARIO']; 		
 		$guardada = false;
 			
 		$count=0;
 		$tmp = get_object_vars($campos);
 		foreach($tmp as $indice=>$valor){
 			if($count>0 && $count!=7){
 				if(empty($valor)==true){ $statement->bindValue(":".$indice, null,  PDO::PARAM_STR);
 				} else { $statement->bindValue(":".$indice, $valor,  PDO::PARAM_STR); }
 			}
 			$count++;
 		}
 		$guardada = $statement->execute();
 		if($guardada){
 			$rs = $this->db->conexionID->lastInsertId();
 			return trim($rs);
 		} else {
 			return 0;
 		}
 	}
 	 	 	
/** READ **/

 	
 	
/** UPDATE **/
 	 	
 	
/** DELETE **/
}	
?>