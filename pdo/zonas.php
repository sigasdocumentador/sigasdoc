<?php
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
$raiz=$_SESSION['RAIZ'];
include_once $raiz.DIRECTORY_SEPARATOR.'rsc'.DIRECTORY_SEPARATOR.'pdo'.DIRECTORY_SEPARATOR.'IFXDbManejador.php';
$db = IFXDbManejador::conectarDB();
if($db->conexionID==null){
	$cadena = $db->error;
	echo $cadena;
	exit();
}

$idp=$_REQUEST['id'];

$sql = "SELECT codzona,zona from aportes089 where codmunicipio='$idp'";
$rs = $db->querySimple($sql);
$items = array();

while( $row = $rs->fetch() ) {
	$items[$row['codzona']] = utf8_encode(htmlentities($row['zona']));
}

$response = isset($_GET['callback'])?$_GET['callback']."(".json_encode($items).")":json_encode($items);
echo $response;
?>
