<?php
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");

$root= $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

include_once $_SESSION['RAIZ'] . DIRECTORY_SEPARATOR . '/rsc/pdo/IFXDbManejador.php';

class Pignoracion{
	private $db;
	
/** CONSTRUCTOR **/
	function Pignoracion(){
		$this->db = IFXDbManejador::conectarDB();
		if($this->db->conexionID==null){
			$cadena = $this->db->error;
			echo msg_error($cadena);
			exit();
		}
 	}
	
/** CREATE **/
 	 	 	
/** READ **/
 	 	
 	function buscarCantidadPignoraciones($idp){
 		$con=0;
 		$sql="select count(*) as cant from aportes043 WHERE idtrabajador = $idp";
 		$rs = $this->db->querySimple($sql);
 		while($row = $rs->fetch()){
 			$con=$row['cant'];
 		}
 		return $con;
 	}
 	 	
 	function buscarPignoraciones($idp){
 		$data = array(); $con = 0;
 		$sql = "SELECT idpignoracion,valorpignorado,idconvenio,fechapignoracion, estado, saldo, observaciones, pagare, facturanumero,anulado, fechaanula,motivo,aportes091.detalledefinicion,fechacancelacion,periodoinicia FROM aportes043 INNER JOIN aportes091 ON aportes043.idconvenio = aportes091.iddetalledef  WHERE idtrabajador = $idp ORDER BY fechapignoracion DESC";
 		$rs = $this->db->querySimple($sql);
 		while($row = $rs->fetch()){
 		$con++;
 		$data[]=array_map("utf8_encode",$row);
	 	}
	 	if($con>0) {
	 		return $data;
	 	} else
	 			return 0;
 	}
 	
 	function buscarDetallePignoracion($idp){
 		$data = array(); $con = 0;
 		$sql="SELECT idbeneficiario, periodo, periodoproceso, dbo.aportes044.fechasistema, valor, anulado,rtrim(pnombre) + ' ' + CASE 
			     WHEN snombre is NULL THEN '' 
			     ELSE rtrim(snombre) END +' '+ rtrim(papellido) +' '+
			     CASE 
			     WHEN sapellido is NULL THEN '' 
			     ELSE rtrim(sapellido) END AS nombre
			  FROM dbo.aportes044
			  INNER JOIN aportes015 ON aportes044.idbeneficiario=aportes015.idpersona where idpignoracion=$idp";
 		$rs = $this->db->querySimple($sql);
 		while($row = $rs->fetch()){
 			$con++;
 			$data[]=array_map("utf8_encode",$row);
 		}
 		if($con>0) {
 			return $data;
 		} else
 			return 0;
 	}
 	
/** UPDATE **/
 	
/** DELETE **/
}	
?>