<?php

class controDocumentos{
	
	private $db;
	private $idrelacion;
	private $datosbene;
	private $datosdocu = array();
	private $datosdocuvalor = array ();
	public  $errordocu=0;
	public  $descriperror="";

	/*
	 * constructor el cual instancia la conexion a la base, asigna el valor idrelacion y datos beneficiario
	 */
	function __construct($idrelacion){
		
		$this->db = IFXDbManejador::conectarDB();
		if($this->db->conexionID==null){
			$this->errordocu = 1;
			$this->descriperror = "Ocurrio el siguiente error conectadose a la base ".$this->db->error;
			exit();
		}
		//inicion transacciones 
				
		$this->idrelacion=$idrelacion;
		$this->consultarDatosBenefificiario();
	}
	
	
	/*
	 * Metodo utilizado para consultar los datos del beneficiario a controlar
	 */
	
	
	function consultarDatosBenefificiario(){
		
		$sql="select * from aportes021 where idrelacion=".$this->idrelacion;
	    $rs = $this->db->querySimple($sql);
		if(!$this->datosbene=$rs->fetch()){
			$this->errordocu = 2;
			$this->descriperror = "Error no se encontro registro del beneficiario";
			return 2;
		}
		
	}
	
	/*
	 * validar errores
	 */
	function validarerror(){
		if(strleng($this->errordocu)>0){
			return 1;
		}
	}
	
	
	/*
	 * Consultar los documentos asociados al tipo de beneficiario
	 * 
	 */
		
    function documentosBeneficiario(){
    	$sql="SELECT   a124.idparentesco,
                       a124.iddefinicion,
                       a124.iddetalledef,
                       a091.detalledefinicion,
                       a124.obligatorio 
             FROM aportes124 a124 
             LEFT JOIN Aportes091 a091 ON a091.iddefinicion=a124.iddefinicion AND a091.iddetalledef=a124.iddetalledef
             WHERE a124.controldocumento='1' and a124.idparentesco=".$this->datosbene['idparentesco']."
             ORDER BY a124.obligatorio DESC	";

    	$rs=$this->db->querySimple($sql);
    	while($row=$rs->fetch()){
    		$this->datosdocu[]=$row;
    	}
    }
    
    /*
     *  
     */
    
    function valorDocumentos(){

    	//llamamos el metodo documentos beneficiarios para obtener una a una la relacion de los documentos asociados al beneiciario y asi revisar el valor de estos
    	$this->documentosBeneficiario();
    	
    	        
    	
    	if(count($this->datosdocu)==0){
    		$this->errordocu = 3;
			$this->descriperror = "No hay documentos controlados, asociados al parentesco del beneficiario, reporte al administrador.";
			return 3;
    	}
    		
    		

    	 
    	 foreach ($this->datosdocu as $index=>$valor){
    	 	
    	 	  /* se revisa si el datos es 0 Normal, 1 obligatorio 2 discapacidad*/
    	 	  if($valor['obligatorio']==2){
    	 	  	   /* se verifica estado de discapacidad en la tabla aportes 015*/
    	 	  	   $sql="select capacidadtrabajo from aportes015 where idpersona=".$this->datosbene['idbeneficiario'];
    	 	  	   $rs=$this->db->querySimple($sql);
    	 	  	   if($row=$rs->fetch()){
    	 	  	   	   if($row['capacidadtrabajo']=='I'){
    	 	  	   	   	
    	 	  	   	   	$sql="select * from aportes125 where idrelacion=".$this->idrelacion." and iddefinicion=".$valor['iddefinicion']." and iddetalledef=".$valor['iddetalledef'];
    	 	  	   	   	
    	 	  	   	   	$rs=$this->db->querySimple($sql);
    	 	  	   	   	 
    	 	  	   	   	if(!$data=$rs->fetch()){
    	 	  	   	   		$data=$this->insertarNuevoDocumento($valor);
    	 	  	   	   	}
    	 	  	   	   	
    	 	  	   	   	$data['detalledefinicion']=$valor['detalledefinicion'];
    	 	  	   	   	$data['obligatorio']=$valor['obligatorio'];
    	 	  	   	   	$this->datosdocuvalor[]=$data;
    	 	  	   	   	  
    	 	  	   	   }
    	 	  	   }else{
    	 	  	   	$this->errordocu = 7;
    	 	  	   	$this->descriperror = "El beneficiario con id:".$this->datosbene['idbeneficiario']." no esta registrado en la tabla aportes015, reportar al administrador";
    	 	  	   }
    	 	  
    	 	  }else if($valor['obligatorio']==0 or $valor['obligatorio']==1){
    	           $sql="select * from aportes125 where idrelacion=".$this->idrelacion." and iddefinicion=".$valor['iddefinicion']." and iddetalledef=".$valor['iddetalledef'];
    	           $rs=$this->db->querySimple($sql);
    	      
    	           if(!$data=$rs->fetch()){
    	      	      $data=$this->insertarNuevoDocumento($valor);
    	           }
    	      	
    	           $data['detalledefinicion']=$valor['detalledefinicion'];
    	           $data['obligatorio']=$valor['obligatorio'];
    	           $this->datosdocuvalor[]=$data;
    	 	  }
    	     
    	     
    	      		
    	 }
    	 
    	 return $this->datosdocuvalor;
    	
    }
    
    
    function insertarNuevoDocumento($valor){
    	
    	$sql="insert into aportes125(idrelacion,idparentesco,iddefinicion,iddetalledef,controlexiste) 
    			              values(".$this->idrelacion.",".$valor['idparentesco'].",".$valor['iddefinicion'].",".$valor['iddetalledef'].",0)";
    	
    	$rs=$this->db->queryInsert($sql,'aportes125');
    	if($rs>0){
    		return $this->consuControladosId($rs);
    	}else{
    		return 0;
    	}
    	
    	
    }
    
    function consuControladosId($id){
    	$sql="select * from aportes125 where iddatos=$id";
    	$rs=$this->db->querySimple($sql);
    	return $rs->fetch();
    	
    }
    
    function actualizarControlDocument($campos){
    	
    	$error=0;
    	/* se hace una actualizacion masiva para controlar las actualizaciones chequeadas */
    	$sqlgen="update aportes125 set controlexiste=0 where idrelacion=".$this->idrelacion;
    	$rsgen= $this->db->queryActualiza($sqlgen);
    	if($rsgen){
    	      foreach ($campos->data as $index=>$value){
 	  
    	 	           $sql="update aportes125 set controlexiste=1 where iddatos=$value and idrelacion=".$this->idrelacion;
    	 	           $rows = $this->db->queryActualiza($sql);
    	 	           
    	 	           if(!$rows){
    	 	  	            echo "ocurrio un error al actualizar"; var_dump($this->db->conexionID->errorinfo());
    	 	  	            $error=1;
    	 	  	            break;
    	 	           }
    	 	  
    	       }
    	 
    	      if($error>0){
    	 	     return $error;
    	      }else{
    	 	     return 0;
    	      }
    	}else{
    		return 2;
    	}
    	
    }
    
    function revisardatosobligatorios(){
    	$oblimarcados=0;
    	$oblinomarcados=0;
    	$total=0;
    	$sql="SELECT count(*) AS cannomarcada
             FROM aportes125 a125
             INNER JOIN aportes124 a124 ON a124.idparentesco=a125.idparentesco AND a124.iddefinicion=a125.iddefinicion AND a124.iddetalledef=a125.iddetalledef 
             WHERE  a124.obligatorio>=1 AND a125.controlexiste=0 and a125.idrelacion=".$this->idrelacion;
    	$rs=$this->db->querySimple($sql);
    	$cantidad=$rs->fetch();
    	
    	$data['oblinomarcados']=$cantidad['cannomarcada'];
    	
    	return $data;
    	
    }
    
    
    function actualizarBanderaGiro($campos,$estado){
    	$sql="Update aportes021 set giro='$estado', fechaasignacion='".$campos->fechaAsignacion."', fechagiro=cast(getDate() as DATE), usuario='".$_SESSION["USUARIO"]."' where idrelacion=".$this->idrelacion;
    	$rs=$this->db->queryActualiza($sql);
    	if($rs==0){
    		$this->errordocu = 4;
    		$this->descriperror ="Error al actualizar la bandera de giro";
    	}
    	else{
    		
    		/* Registramos el valor actual de los documentos y  la observacion indicada por el funcionario al momento de la actualizacion para los caso en el que el giro sea N*/
    		$ctrcadena=$this->controlcadenadocumentos();
    		if($campos->giroAnt==$estado){
    			$this->registrarSeguimiento($campos,$ctrcadena,$estado,1);
    		}else{
    			$this->registrarSeguimiento($campos,$ctrcadena,$estado,2);
    		}
    		 
                 		
    	}
    }
    
    
    function inicioTransaccion(){
    	$this->db->inicioTransaccion();
    }
    
    function confirmarTransaccion(){
    	$this->db->confirmarTransaccion();
    }
    
    function cancelarTransaccion(){
    	$this->db->cancelarTransaccion();
    }
    
    /*
     * Metodo utilizado para verificar el estado actual de los documentos con relacion al control existencia y devolveer una cadena separada por |
     */
    function controlcadenadocumentos(){
    	  $cadena="{";
    	  $ctrdocument= $this->valorDocumentos();  
    	  foreach ($ctrdocument as $index=>$value){
    	  	 $cadena.="\"".$value['detalledefinicion']."\":\"".$value['controlexiste']."\",";
    	  }
    	  $cadena=substr($cadena, 0, -1);
    	  $cadena.="}";
    	  return $cadena;
    }
    
    function registrarSeguimiento($campos,$ctrcadena,$estado,$opt){
    	
    	if($opt==1){
    		$sql="insert into aportes021a(id_relacion,
    				                     usuario_creacion,
    				                     fecha_creacion,
    				                     bandera_giro,
    				                     ctrdocumento) 
    			               values(".$this->idrelacion.",
    			               		  '".$_SESSION['USUARIO']."',
    			               		   cast(getDate() as DATETIME),
    			               		   '$estado',
    			               		   '$ctrcadena'
    			               		  )";
    		$idradsegui= $this->db->queryInsert($sql,'aportes021a');
    		if($idradsegui<=0){
    			$this->errordocu=5;
    			$this->descriperror="Hubo problemas para insertar la trazabilidad del cambio";
    		}
    	}else if($opt==2){
    		$idaportes21a = $this->db->conexionID->lastInsertId('aportes021a');
    		$sql="update aportes021a set ctrdocumento='$ctrcadena' where id=$idaportes21a and id_relacion=".$this->idrelacion;
    		$rs=$this->db->queryActualiza($sql);
    		if($rs==0){
    			$this->errordocu=6;
    			$this->descriperror="Hubo problemas para actualizar la trazabilidad del cambio";
    		}

    	}
    	
    }
    
    function consultardatosaportes21(){
    	$sql="select * from aportes021 where idrelacion=".$this->idrelacion;
    	$rs=$this->db->querySimple($sql);
    	return $rs->fetch();
    }
    
	
	
}



?>