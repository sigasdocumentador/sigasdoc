<?php
set_time_limit(0);
ini_set("display_errors",'1');
date_default_timezone_set("America/Bogota");

$root= $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
$usuario = $_SESSION['USUARIO'];
include_once $_SESSION['RAIZ'] . DIRECTORY_SEPARATOR . '/rsc/pdo/IFXDbManejador.php';

class Afiliacion {
	private $db;
	
/** CONSTRUCTOR **/
	function Afiliacion () {
		$this->db = IFXDbManejador::conectarDB();
		if($this->db->conexionID==null){
			$cadena = $this->db->error;
			echo $cadena;
			exit();
		}
 	}
	
/** CREATE **/
 	
 	function guardarAfiliacion($campos){
 		$sql="INSERT INTO aportes016 (tipoformulario,tipoafiliacion,idempresa,idpersona,fechaingreso,horasdia,horasmes,salario,agricola,cargo,primaria,estado,fecharetiro,motivoretiro,fechanovedad,semanas,fechafidelidad,estadofidelidad,traslado,codigocaja,flag,tempo1,tempo2,fechasistema,usuario,tipopago,categoria,auditado,idagencia,idradicacion,vendedor,codigosalario,flag2,porplanilla,madrecomunitaria,claseafiliacion) 
			VALUES (:campo1,:campo2,:campo3,:campo4,:campo5,:campo6,:campo7,:campo8,:campo9,:campo10,:campo11,:campo12,:campo13,:campo14,:campo15,:campo16,:campo17,:campo18,:campo19,:campo20,:campo21,:campo22,:campo23,cast(getDate() as Date),:campo25,:campo26,:campo27,:campo28,:campo29,:campo30,:campo31,:campo32,:campo33,:campo34,:campo35,:campo36)"; 		
 		$statement = $this->db->conexionID->prepare($sql);
 		$campos->usuario=$_SESSION['USUARIO'];
 		$campos->idagencia=$_SESSION['AGENCIA'];
 		$guardada = false; 		
 		
 		$count=0;
 		$tmp = get_object_vars($campos);
 		foreach($tmp as $indice=>$valor){
 			if($count>0 && $count!=24){ 				
 				if(empty($valor)==true){ 					
 					$statement->bindValue(":campo".$count, null,  PDO::PARAM_STR);
 				} else { 					 
 					$statement->bindValue(":campo".$count, $valor,  PDO::PARAM_STR);
 				}
 			}
 			$count++;
 		} 		
 		$guardada = $statement->execute(); 	
 		if($guardada){
 			$rs = $this->db->conexionID->lastInsertId();
 			return trim($rs);
 		}else{
 			return 0;
 		} 		
 	}
 	
/** READ **/
 	
 	/** busar afiliacion pendiente por idradicacion **/
 	public  function buscarAfiliacionPendiente($idr){
 		$sql="select * from aportes016 where idradicacion=$idr";
 		return $this->db->conexionID->query($sql);
 	}
 	
 	/** Trae afiliación primaria u otra si la persona esta, o estuvo, afiliada. **/ 
	function buscarEsAfiliado($idp){
		$data = array();			
		$sql="SELECT a.tabla, a.idempresa, a.primaria FROM (
				    SELECT '16' tabla, a.idempresa, a.primaria FROM aportes016 a WHERE a.idpersona=$idp
				UNION
				    SELECT '17' tabla, a.idempresa, a.primaria FROM aportes017 a WHERE a.idpersona=$idp
			  ) a";
		$conA=0; $conI=0;
		$rs = $this->db->querySimple($sql);
		while($row = $rs->fetch()){
			$data[]=$row;
			if($row['tabla']=='16'){ $conA++;
			} else if($conA==0){ $conI++; }				
		}
		if($conA==0 && $conI==0){
			return 0;
		} else {
			if($conA>0){	
				foreach ($data as $valor){
					if($valor['primaria']=='S'){
						return $valor;						
					}
				}					
			} 
			return $data[0];
		}			
	}
	
	/** Busca Afiliaciones Activas o Pendientes de un afiliado con una empresa **/
	function buscarAfiliacionesActivas($idp,$nit){
		$data=array(0 => 0, 1 => 0);
		$sql="select a16.estado, count(a16.idformulario) cant from aportes016 a16 inner join aportes048 a48 on a16.idempresa=a48.idempresa
		where a16.idpersona=$idp and a48.nit='".$nit."' and a16.estado in ('A','P') group by a16.estado";
		$rs = $this->db->querySimple($sql);
		while($row = $rs->fetch()){
			if ($row['estado']=='A')
				$data[0]=$row['cant'];
			else
				$data[1]=$row['cant'];
		}
		return $data;
	}
	
	/** Trae resumida todas las Afiliaciones Activas o Pendientes de un afiliado **/
	function buscarAfiliacionesActivasAfiliado($idp){
		$data = array(); $con = 0;
 		$sql = "SELECT a16.idformulario,a16.tipoafiliacion,a16.idpersona,a16.idempresa,a16.estado,a15.identificacion,a48.nit,a48.razonsocial,a91.detalledefinicion,a16.cargo
 				FROM aportes016 a16 INNER JOIN aportes015 a15 ON a15.idpersona=a16.idpersona INNER JOIN aportes048 a48 ON a16.idempresa=a48.idempresa INNER JOIN aportes091 a91 ON a16.tipoformulario=a91.iddetalledef 
 				WHERE a16.idpersona=$idp";	
 		$rs = $this->db->querySimple($sql);
 		while($row = $rs->fetch()){
 			$con++;
 			$data[]=array_map("utf8_encode",$row);
 		}
 		if($con>0) {
 			return $data;
 		} else
 			return 0;
	}
	
	/** Trae completa todas las Afiliaciones Activas o Pendientes de un afiliado **/
	function buscarFichaAfiliacionesActivasAfiliado($idp){
		$data = array(); $con = 0;
		$sql = "SELECT isnull(a16.idradicacion,0) idradicacion,isnull(CAST(a4.fecharadicacion AS VARCHAR),'N/A') fecharadicacion,isnull(a4.usuario,'') usuarioRadica,isnull(CAST(a4.fechaproceso AS VARCHAR),'N/A') fechaproceso,isnull(a4.recibegrabacion,'') usuarioGraba,a48.idempresa,a48.nit,a48.razonsocial, a16.fechaingreso,
					CASE WHEN a16.tipoformulario=48 THEN 'Subsidio' ELSE 'Servicios' END tipoformulario, c91.detalledefinicion as clasef, a16.horasmes,a16.salario,a91.concepto as cargo,a91.detalledefinicion as nomcargo,a16.categoria,a16.primaria,a16.agricola,datediff(day,a16.fechaingreso, getdate()) diasLab,
						
					CASE WHEN a16.estado='P' THEN (
						isnull((SELECT TOP 1 'PU' FROM aportes010 b10 WHERE b10.idtrabajador=a16.idpersona AND b10.idempresa=a16.idempresa AND b10.fechasistema=a16.fechasistema
							AND (a16.idradicacion IS NULL OR 0=isnull((  SELECT count(*) cuenta FROM aportes004 b4 WHERE b4.idradicacion=a16.idradicacion AND b4.numero=b10.identificacion AND b4.nit=b10.nit AND (b4.procesado='N' OR b4.procesado IS NULL) AND (b4.idtiporadicacion=28 or b4.idtiporadicacion=2926 or b4.idtiporadicacion=29)), 0))), 'P')
					) ELSE a16.estado END estado,
					isnull(a16.flag,0) flag
				FROM aportes016 a16 LEFT JOIN aportes004 a4 ON a16.idradicacion=a4.idradicacion INNER JOIN aportes048 a48 ON a16.idempresa=a48.idempresa LEFT JOIN aportes091 a91 ON a16.cargo=a91.iddetalledef LEFT JOIN aportes091 b91 ON a16.tipoformulario=b91.iddetalledef
				LEFT JOIN aportes091 c91 ON a16.tipoafiliacion=c91.iddetalledef WHERE a16.idpersona=$idp ORDER BY a16.primaria DESC";
		$rs = $this->db->querySimple($sql);
		while($row = $rs->fetch()){
			$con++;
			$data[]=array_map("utf8_encode",$row);
		}
		if($con>0) {
			return $data;
		} else
			return 0;
	}
	
	/** Trae completa todas las Afiliaciones Activas o Pendientes de un afiliado **/
	function buscarFichaAfiliacionesInactivasAfiliado($idp){
		$data = array(); $con = 0;
		$sql = "SELECT isnull(CAST(a4.fecharadicacion AS VARCHAR),'N/A') fecharadicacion,isnull(a4.usuario,'') usuarioRadica,isnull(CAST(a4.fechaproceso AS VARCHAR),'N/A') fechaproceso,isnull(a4.recibegrabacion,'') usuarioGraba,a48.nit,a48.razonsocial, a17.fechaingreso,a17.fecharetiro,
					   CASE WHEN a17.tipoformulario=18 THEN 'Subsidio' ELSE 'Servicios' END tipoformulario,c91.detalledefinicion as clasef, a17.horasmes,a17.salario,a17.categoria,a17.estado,a17.semanas,dateDiff(day, a17.fechaingreso, a17.fecharetiro) as dias,a17.motivoretiro,
					   b91.detalledefinicion as definicionmotivo,a17.fechafidelidad
				FROM aportes017 a17 LEFT JOIN aportes004 a4 ON a17.idradicacion=a4.idradicacion INNER JOIN aportes048 a48 ON  a17.idempresa=a48.idempresa LEFT JOIN aportes091 a91 ON a17.tipoformulario=a91.iddetalledef LEFT JOIN aportes091 b91 ON a17.motivoretiro=b91.iddetalledef  
				LEFT JOIN aportes091 c91 ON a17.tipoafiliacion = c91.iddetalledef WHERE a17.idpersona=$idp ORDER BY a17.fechaingreso DESC";
		$rs = $this->db->querySimple($sql);
		while($row = $rs->fetch()){
			$con++;
			$data[]=array_map("utf8_encode",$row);
		}
		if($con>0) {
			return $data;
		} else
			return 0;
	}
	
	/** Trae el totalizado de Afiliaciones por estado que tiene una persona **/
	function buscarTotalAfiliaciones($idp){
		$data=array(0 => 0, 1 => 0, 2 => 0);
		$sql="SELECT 'A' estado, COUNT(a16.idformulario) cant FROM aportes016 a16 WHERE a16.idpersona=$idp
				UNION
			  SELECT 'I' estado, COUNT(a17.idformulario) cant FROM aportes017 a17 WHERE a17.idpersona=$idp
				UNION
			  SELECT TOP 1 'P' estado, COUNT(a16.idformulario) cant FROM aportes016 a16 INNER JOIN aportes010 a10 ON a10.idplanilla= isnull((SELECT TOP 1 b10.idplanilla FROM aportes010 b10 WHERE b10.idtrabajador=a16.idpersona AND b10.idempresa=a16.idempresa AND b10.fechasistema=a16.fechasistema), 0) WHERE a16.idpersona=$idp 
				AND a16.estado='P' AND ( a16.idradicacion IS NULL OR 0=isnull( ( SELECT count(*) cuenta FROM aportes004 a4 WHERE a4.idradicacion=a16.idradicacion AND a4.numero=a10.identificacion AND a4.nit=a10.nit AND ( a4.procesado='N' OR a4.procesado is null ) AND ( a4.idtiporadicacion=28 OR a4.idtiporadicacion=2926 OR a4.idtiporadicacion=29 ) ), 0 ) )";
		$rs = $this->db->querySimple($sql);
		while($row = $rs->fetch()){
			if ($row['estado']=='A')
				$data[0]=$row['cant'];
			else if ($row['estado']=='I')
				$data[1]=$row['cant'];
			else
				$data[2]=$row['cant'];
		}
		return $data;
	}
	
	/** Comprueba si la afiliacion que tiene con la empresa es pendiente por PU **/
	function buscarAfiliacionPporPlanilla($idp,$nit){
		$con=0;
		$sql="SELECT isnull(max(a16.idformulario),0) cant FROM aportes016 a16 INNER JOIN aportes010 a10 ON a10.idtrabajador=a16.idpersona AND a10.idempresa=a16.idempresa AND a10.fechasistema=a16.fechasistema AND a10.nit='".$nit."'
				WHERE a16.idpersona=$idp AND a16.estado='P' AND ( a16.idradicacion IS NULL OR 0=isnull( ( SELECT count(*) cuenta FROM aportes004 a4 WHERE a4.idradicacion=a16.idradicacion AND a4.numero=a10.identificacion AND a4.nit=a10.nit AND ( a4.procesado='N' OR procesado is null) AND ( idtiporadicacion=28 OR idtiporadicacion=2926 OR idtiporadicacion=29 ) ), 0 ) )";
		$rs = $this->db->querySimple($sql);
		while($row = $rs->fetch()){ $con=$row['cant']; }
		return $con;	
	}
	
	/** Comprueba si la afiliacion independiente que tiene es pendiente por PU **/
	function buscarAfiliacionIndependientePporPlanilla($idp){
		$con=0;
		$sql="select isnull(max(a16.idformulario),0) cant from aportes016 a16
				inner join aportes010 a10
				  on a16.idpersona=a10.idtrabajador and a16.idempresa=a10.idempresa and a10.identificacion=a10.nit
				where a16.idpersona=$idp and a16.estado='P' and a10.idplanilla=(select max(b10.idplanilla) from aportes010 b10 where b10.idtrabajador=a10.idtrabajador and b10.idempresa=a10.idempresa)
				  and 0=isnull((  SELECT count(*) cuenta FROM aportes004 WHERE (procesado='N' OR procesado is null) AND numero=a10.identificacion AND nit=a10.nit AND (idtiporadicacion=28 or idtiporadicacion=2926 or idtiporadicacion=29)), 0)    
				  and 0=isnull((  SELECT count(*) cuenta FROM aportes016 b16 WHERE b16.idempresa=a16.idempresa and b16.idpersona<>a16.idpersona) , 0)";
		$rs = $this->db->querySimple($sql);
		while($row = $rs->fetch()){ $con=$row['cant']; }
		return $con;
	}
	
	function buscarSiTienePrimaria($idp){
		$con=0;
		$sql="select count(*) as cant from aportes016 where idpersona='".$idp."' and primaria='S'";
		$rs = $this->db->conexionID->query($sql)->fetch(PDO::FETCH_OBJ)->cant;
		return $rs;
	}
	
	function buscarOtraAfiliacionPrimaria($idp,$ide){
		$con=0;
		$sql="select count(a16.idformulario) as cant from aportes016 a16 WHERE a16.idpersona=$idp AND a16.primaria='S' AND a16.idempresa<>$ide";
		$rs = $this->db->querySimple($sql);
		while($row = $rs->fetch()){ $con=$row['cant']; }
		return $con;
	}
	
	function buscarTotalInactivasPersona($idp){
		$con = 0;
		$sql = "SELECT count(idformulario) cant FROM aportes017 WHERE idpersona=$idp";
		$rs = $this->db->querySimple($sql);
		while($row = $rs->fetch()){ $con=$row['cant']; }
		return $con;
	}
	
	//buscar todas las afiliaciones activas por empresa 
	
	function buscarAfiliadosEmpresa($ide){
		$sql="SELECT aportes016.idpersona,fechaingreso,salario,identificacion,papellido,sapellido,pnombre,snombre,codigo FROM aportes016 INNER JOIN aportes015 ON aportes016.idpersona=aportes015.idpersona INNER JOIN aportes091 ON aportes015.idtipodocumento=aportes091.iddetalledef WHERE idempresa=$ide order by papellido";
		$rs = $this->db->conexionID->query($sql);
		$filas =array();
		while ($row=$rs->fetch())
			$filas[]=$row;
		return $filas;
	}
	
	function buscarTiempoVinculacion($idp){
		$sql="SELECT sum(a.dias) cant FROM (
					SELECT datediff(dd,
							(CASE WHEN datediff(dd,CAST(dateadd(yy,-3,getDate()) AS DATE),a17.fechaingreso)>=0 THEN a17.fechaingreso ELSE CAST(dateadd(yy,-3,getDate()) AS DATE) END),
							(CASE WHEN datediff(dd,getDate(),a17.fecharetiro)>0 THEN getDate() ELSE a17.fecharetiro END)
					) dias
					FROM aportes017 a17
					WHERE a17.idpersona=$idp AND a17.fecharetiro >= CAST(dateadd(yy,-3,getDate()) AS DATE)
			  ) a";
		$rs = $this->db->querySimple($sql);
		while($row = $rs->fetch()){
			$con=$row['cant'];
		}
		return $con;
	}
/** UPDATE **/
	
	function updateAfiliacion($campos){
		$sql="UPDATE aportes016 SET
		tipoformulario=$campos[1],
		tipoafiliacion=$campos[2],
		idempresa=$campos[3];
		idpersona=$campos[4];
		fechaingreso=$campos[5];
		horasdia=$campos[6],
		horasmes=$campos[7],
		salario=$campos[8],
		agricola=$campos[9],
		cargo=$campos[10],
		primaria=$campos[11],
		estado=$campos[12],
		tipopago=$campos[13],
		categoria=$campos[14],
		idagencia=$campos[15],
		madrecomunitaria = $campos[16],
		usuario = $campos[17],
		codigosalario=1  where idformulario = campos[0]";
		return $this->db->queryActualiza($sql);
	}
	
	function updateAfiliacionC($campos){
		$sql="UPDATE aportes016 SET	tipoformulario=:campo1,tipoafiliacion=:campo2,idempresa=:campo3,idpersona=:campo4,fechaingreso=:campo5,horasdia=:campo6,horasmes=:campo7,salario=:campo8,agricola=:campo9,cargo=:campo10,primaria=:campo11,estado=:campo12,fecharetiro=:campo13,motivoretiro=:campo14,fechanovedad=:campo15,semanas=:campo16,fechafidelidad=:campo17,estadofidelidad=:campo18,traslado=:campo19,codigocaja=:campo20,
			  flag=:campo21,tempo1=:campo22,tempo2=:campo23,usuario=:campo25,tipopago=:campo26,categoria=:campo27,auditado=:campo28,idagencia=:campo29,idradicacion=:campo30,vendedor=:campo31,codigosalario=:campo32,flag2=:campo33,porplanilla=:campo34,madrecomunitaria=:campo35,claseafiliacion=:campo36 WHERE idformulario=:campo0";
		$statement = $this->db->conexionID->prepare($sql);
		$campos->usuario=$_SESSION['USUARIO'];
		$campos->idagencia=$_SESSION['AGENCIA'];
		$guardada = false;
			
		$count=0;
		$tmp = get_object_vars($campos);
		foreach($tmp as $indice=>$valor){
			if($count!=24){
				if(empty($valor)==true){
					$statement->bindValue(":campo".$count, null,  PDO::PARAM_STR);
				} else {
					$statement->bindValue(":campo".$count, $valor,  PDO::PARAM_STR);
				}
			}
			$count++;
		}
		
		$guardada = $statement->execute();			
		return $guardada ? 1 : 0;
	}
 	
/** DELETE **/


/** PROCEDURES **/
	/** Ejecuta el procedimiento almacenado que inactiva la afiliacion del formulario recibido por parametro
	 *  
	 *  @return {int} ( 0 => No se pudo inactivar la afiliacion, 1 => No se pudo insertar la afiliacion inactiva,
	 *  			    2 => Se inactivo el afiliado, 3 => se inactivo la empresa, 4 => error inactivando la empresa )
	 **/
	function spInactivarAfiliacion($idf,$idMotivoRetiro,$fecha,$usuario,$idta, $ide, $idp){
		$preInactivar =	$this->buscarTotalInactivasPersona($idp);
						
		$proc = '{ CALL sp_inactivar (:idf, :idMotivoRetiro, :fecha, :usuario, 0) }';
		$stmt = $this->db->conexionID->prepare($proc);
		$stmt->bindParam(':idf', $idf, PDO::PARAM_STR);
		$stmt->bindParam(':idMotivoRetiro', $idMotivoRetiro, PDO::PARAM_STR);
		$stmt->bindParam(':fecha', $fecha, PDO::PARAM_STR);
		$stmt->bindParam(':usuario', $usuario, PDO::PARAM_STR);		
		try { $stmt->execute(); 
		} catch (PDOException $e){ return 0; die(); }
				
		$postInactivar = $this->buscarTotalInactivasPersona($idp);
		if(intval($postInactivar)>intval($preInactivar)){
			if($idta != 18){
				$sql="Update aportes048 set estado='I',fechaestado = cast(getdate() as date),flag='P' where idempresa=$ide";
				$rs = $this->db->queryActualiza($sql);
				if(intval($rs)>0) return 3;
				else return 4;
			} else { return 2; }
		} else {
			return 1;
		}
	}
	
	
	/**
	 * Ejecuta el procedimiento almacenado que realiza la actualizacion de la categoria de la afiliacion
	 * @param unknown_type $idPersona
	 * @return {int} ( 	0 => No se pudo realizar el procedimiento,
	 * 					1 => El procedimiento se ejecuto correctamente)
	 */
	function spProcesoIndepPensCategorias($idPersona){
		$resultado = 0;
		$sentencia = $this->db->conexionID->prepare ( "EXEC [dbo].[sp_Proceso_Indep_Pens_Categorias]
				@idpersona = $idPersona
				, @resultado = :resultado" );
				$sentencia->bindParam ( ":resultado", $resultado, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4000 );
				$sentencia->execute ();
				if ($resultado > 0)
					return 1;
				else
					return 0;
	}
}	
?>