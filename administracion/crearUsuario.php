<?php
/* autor:       Orlando Puentes
 * fecha:       Septiembre 24 de 2010
 * objetivo:    Controlar la manipulación del SIGAS por parte de los usuarios. 
 */
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Definiciones</title>
<link href="../css/Estilos.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="../js/jquery-1.4.2.js"></script>
<script language="javascript" src="../js/comunes.js"></script>
<script language="javascript" src="js/crearUsuario.js"></script>
<script type="text/javascript">
shortcut.add("Shift+F",function() {
		                                var URL=src();
		var url=URL+"aportes/trabajadores/consultaTrabajador.php";
    	window.open(url,"_blank");
    },{
	'propagate' : true,
	'target' : document 
        });        
</script>
<body>
<form name="forma">
<br/>
<center>
<table width="70%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="13" height="29" background="../imagenes/tabla/arriba_izq.gif">&nbsp;</td>
    <td background="../imagenes/tabla/arriba_central2.gif"><span class="letrablanca">::&nbsp;Administracion - Usuarios &nbsp;::</span></td>
    <td width="13" background="../imagenes/tabla/arriba_der.gif" align="right">&nbsp;</td>
  </tr>
  <tr>
    <td background="../imagenes/tabla/izquierda2.jpg">&nbsp;</td>
    <td background="../imagenes/tabla/centro.gif"><img src="../imagenes/tabla/spacer.gif" width="1" height="1">
	  <img src="../../imagenes/spacer.gif" width="1" height="1">
	  <img src="../imagenes/menu/nuevo.png" title="Nuevo" width="16" height="16" style="cursor:pointer" onClick="nuevoR();">
	  <img src="../imagenes/spacer.gif" width="1" height="1">
	  <img src="../imagenes/menu/grabar.png" title="Guardar" width="16" height="16" onClick="validarCampos(1)" style="cursor:pointer" id="bGuardar">
	  <img src="../imagenes/spacer.gif" width="1" height="1">
	  <img src="../imagenes/menu/modificar.png" title="Actualizar" width="16" height="16" onClick="validarCampos(2)" style="cursor:pointer">
	  <img src="../imagenes/spacer.gif" width="1" height="1">
	  <img src="../imagenes/menu/ico_error.png" width="16" height="16" border="0" title="Eliminar registro" style="cursor:pointer" onClick="eliminarDato()" >
	  <img src="../imagenes/spacer.gif" width="1" height="1">
	  <img src="../imagenes/menu/imprimir.png" width="16" height="16" style="cursor:pointer" title="Limpiar campos" onClick="window.open('definicionesR.php','mainFrame')" >
	  <img src="../imagenes/spacer.gif" width="1" height="1">
	  <img src="../imagenes/menu/refrescar.png" width="16" height="16" style="cursor:pointer" title="Limpiar campos" onClick="limpiarCampos(); document.forms[0].elements[0].focus();">
	  <img src="../imagenes/spacer.gif" width="1" height="1">
      <img src="../imagenes/menu/buscar.png" title="Buscar" width="16" height="16" style="cursor:pointer" onClick="consultaDatos()">
	  <img src="../imagenes/spacer.gif" width="1" height="1">
	  <a href="../help/aportes/AyudaAdministracionUsuario.html" target="_blank" onClick="window.open(this.href, this.target, 'width=800,height=850,titlebar=0, resizable=no'); return false;" >
      <img src="../imagenes/menu/informacion.png" width="16" height="16" style="border:none" title="Manual" /></a>

      </td>
    <td background="../imagenes/tabla/derecha.gif">&nbsp;</td>
  </tr>
  <tr>
    <td background="../imagenes/tabla/izquierda2.jpg">&nbsp;</td>
    <td align="center" background="../imagenes/tabla/centro.gif"><div id="resultado" style="color:#FF0000"></div></td>
    <td background="../imagenes/tabla/derecha.gif">&nbsp;</td>
  </tr>
  <tr>
    <td background="../imagenes/tabla/izquierda2.jpg">&nbsp;</td>
    <td align="center" background="../imagenes/tabla/centro.gif">
	<table width="95%" border="0" cellspacing="0" class="tablero">
      <tr>
        <td width="25%" align="left">Usuario</td>
        <td align="left"><input name="tid" class=boxfecha id="tid"  ></td>
        </tr>
      <tr>
      <td align="left">GRANT CONNECT</td>
        <td  align="left">
        <select name="select" class="boxfecha" id="select">
        <option value="NO" selected="selected">NO</option>
        <option value="SI">SI</option>
        </select></td>
        </tr>
      <tr>
      <td align="left">GRANT RESOURCE</td>
        <td align="left"><select name="select2" class="boxfecha" id="select2">
          <option value="NO" selected="selected">NO</option>
          <option value="SI">SI</option>
        </select></td>
        </tr>
	  <tr>
	    <td align="left">GRANT DBA </td>
	    <td align="left"><select name="select3" class="boxfecha" id="select3">
	      <option value="NO" selected="selected">NO</option>
	      <option value="SI">SI</option>
	      </select></td>
	    </tr>
	  <tr>
	    <td align="left">&nbsp;</td>
	    <td align="left">&nbsp;</td>
	  </tr>
    </table></td>
    <td background="../imagenes/tabla/derecha.gif">&nbsp;</td>
  </tr>
  <tr>
    <td height="38" background="../imagenes/tabla/abajo_izq2.gif">&nbsp;</td>
    <td background="../imagenes/tabla/abajo_central.gif"></td>
    <td background="../imagenes/tabla/abajo_der.gif">&nbsp;</td>
  </tr>
</table>
</center>
<input type="hidden" value="<?php echo $_SESSION["USUARIO"];?>" name="usuario" id="usuario" />
<input type="hidden" name="idr" id="idr" />
</form>

</body>
<script>
nuevoR();
</script>
</html>