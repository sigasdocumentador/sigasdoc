<?php
date_default_timezone_set('UTC');
class ControladorCdi{
	var $conexion;
	var $dsn = DSN_CDI;
	var $dbUser = USER_DB_CDI;
	var $dbPassword = PASS_USER_DB_CDI;
	var $nombreDbCdi = DATABASE_CDI;
	
	function ControladorCdi(){
		$this->conexion = mssql_connect($this->dsn,$this->dbUser,$this->dbPassword);
		$selectDb = mssql_select_db($this->nombreDbCdi,$this->conexion);
		if(!$selectDb)
			die("Error de Controlador de CDI --> No se puede conectar con la base de datos del CDI (". $this->nombreDbCdi .")<br/>");
	}
	
	/**
	 * Obtiene el c�digo del documento que ser� generado.
	 * Consecutivo Correspondencia Externa
	 *
	 * @return string $codigo
	 */
	 
	function obtenerUltimoConsecutivo(){
		$ano = date("Y");
		$trans = mssql_query("begin transaction",$this->conexion);
		if($trans != true)
			die("Error de Controlador de CDI --> No se puede iniciar una transacci�n con la base de datos del CDI (". $this->nombreDbCdi .")<br/>");
		$sql="select max(ceConsecutivo) as ceConsecutivo from cdi_CorrespExterna where ceAgencia=1 and ceAnoActual='". $ano ."'";
		$consulta = mssql_query($sql,$this->conexion);
		$consecutivomax = mssql_result($consulta,0,'ceConsecutivo');
		$consecutivo 	= $consecutivomax + 1;
		mssql_free_result($consulta);
		return $consecutivo;
	}

	 /**
	 * Obtiene el c�digo del documento que ser� generado.
	 * Consecutivo Correspondencia Interna
	 *
	 * @return string $codigo
	 */
	function obtenerUltimoConsecutivoInterno(){
		$ano = date("Y");
		$trans = mssql_query("begin transaction",$this->conexion);
		if($trans != true)
			die("Error de Controlador de CDI --> No se puede iniciar una transacci�n con la base de datos del CDI (". $this->nombreDbCdi .")<br/>");
		$sql="select max(ciConsecutivo) as ciConsecutivo from cdi_CorrespInterna where ciAgencia=1 and ciAnoActual='$ano'";
		$consulta = mssql_query($sql,$this->conexion);
		$consecutivomax = mssql_result($consulta,0,'ciConsecutivo');
		$consecutivo 	= $consecutivomax + 1;
		mssql_free_result($consulta);
		return $consecutivo;
	}

	
	/**
	 * Obtiene los datos del coordinador del �rea de aportes y subsidio
	 * relacionado en la base de datos del CDI
	 *
	 * @return array $datosCoordinador
	 */
	function obtenerDatosCoordinadorSubsidioCdi(){
		$resultQuery = mssql_query("select * from cdi_Coordinador where corCodiProceso='". PROCESO_DESARROLLO_FINANCIERO_CDI ."' and corCodiArea=". AREA_SUBSIDIO_CDI ." and corEstado='A' and corConSeccion is null",$this->conexion);
		$datosCoordinador = mssql_fetch_array($resultQuery);
		if(is_array($datosCoordinador))
			return $datosCoordinador;
		return false;
	}
	
	function obtenerDatosPersonaNaturalOJuridica($numDocumento){
		$resultQuery = mssql_query("SELECT * FROM cdi_PersNatuJuri WHERE njNoDoc='$numDocumento'",$this->conexion);
		$datos = mssql_fetch_array($resultQuery);
		if(is_array($datos))
			return $datos;
		return false;
	}
	
	/**
	 * Graba un documento, correspondiente a correspondencia Externa, 
	 * en la base de datos del CDI, con los datos recibidos, retorna un booleano
	 *
	 * @param int $consecutivo
	 * @param string $docFuncionarioCoord
	 * @param string $docEntidadDest
	 * @param string $asunto
	 * @param string $cuerpo
	 * @param string $observaciones
	 * @param int $numAnexos
	 * @param int $idPersNatuJuri
	 * @param string $codTd
	 * @param int $deptoDest
	 * @param string $ciuDest
	 * @param int $idAgencia
	 * @param int $tituloFuncDest
	 * @param string $estadoCdi
	 * @return boolean
	 */
	function generarDocumentoCdi($consecutivo, $docFuncionarioCoord, $docEntidadDest, $asunto, $cuerpo, $observaciones = '', $numAnexos = 0, $idPersNatuJuri = null, $codTd = 27, $deptoDest = 41, $ciuDest='001', $idAgencia = 1, $tituloFuncDest=1, $estadoCdi = 'T'){
		$ceAnoActual = date("Y");
		$ceAgencia = $idAgencia;
		$ceCedulaFunFirma = $docFuncionarioCoord;
		$ceCodigoTd = $codTd;
		$ceDeptoDest = $deptoDest;
		$ceCiudadDest = $ciuDest;
		$ceEntidadDest = $docEntidadDest;
		$ceTituloFuncDest = $tituloFuncDest;
		$ceAnexos = $numAnexos;
		$ceAsunto = $asunto;
		$ceCuerpo = htmlentities($cuerpo);
		$ceEstadoCdi = $estadoCdi;
		$sql="INSERT INTO cdi_CorrespExterna (
				ceAnoActual, 
				ceConsecutivo, 
				ceProcArea, 
				ceAgencia, 
				cefechaemision, 
				ceUsuario, 
				ceCedulaFunFirma,
				ceCodigoTd, 
				ceDeptoDest, 
				ceCiudadDest, 
				ceConsPersNatuJuri,
				ceEntidadDest, 
				ceTituloFuncDest, 
				ceObservaciones,
				ceAnexos,
				ceAsunto,
				ceCuerpo, 
				ceEstadoCdi)
			  VALUES (
			  	'".$ceAnoActual."', 
			  	'".$consecutivo."', 
			  	'".PROCESO_AREA_SUBSIDIO_CDI."', 
			  	'".$ceAgencia."', 
			  	'". date("m/d/Y G:i") ."', 
			  	'". strtolower($_SESSION["USUARIO"]) ."', 
			  	'".$ceCedulaFunFirma."', 
			  	'".$ceCodigoTd."', 
			  	'".$ceDeptoDest."',
				'".$ceCiudadDest."', 
				'".$idPersNatuJuri."',
				'".$ceEntidadDest."', 
				'".$ceTituloFuncDest."', 
				'". $observaciones ."',
				'".$ceAnexos."', 
				'".$ceAsunto."', 
				'".$ceCuerpo."', 
				'".$ceEstadoCdi."')";
		$insertar = mssql_query($sql,$this->conexion);
		if($insertar!=true){
			echo "Error de Controlador de CDI --> No se pudo grabar el documento en CDI<br/>";
			$variableError = mssql_query("rollback transaction",$this->conexion);
			return false;
		}
		$commit = mssql_query("commit transaction",$this->conexion);
		if($commit!=true){
			echo "Error de Controlador de CDI --> No se pudo grabar el documento en CDI<br/>";
			$variableError = mssql_query("rollback transaction",$this->conexion);
			return false;
		}else{
			mssql_close($this->conexion);
			return true;
		}
	}
	
	
	/**
	 * Graba un documento, correspondiente a correspondencia interna, 
	 * en la base de datos del CDI, con los datos recibidos, retorna un booleano
	 *
	 * @param int $idAgencia
	 * @param string $consecutivo
	 * @param int $cedulaFunFirma
	 * @param string $procesoFunFi
	 * @param string $areaFunFi	 
	 * @param int $cedulaFunDest
	 * @param string $codiProcDest
	 * @param int $codiAreaDest
	 * @param string $tipoUsuario
	 * @param int $anexos 
	 * @param int $codigoTd
	 * @param string $asunto 
	 * @param string $saludo 
	 * @param string $cuerpo
	 * @param string $despedida 
	 * @param int $prioridad
	 * @return boolean
	 */
	function generarDocumentoCdiInterno($idAgencia, $consecutivo, $cedulaFunFirma, $procesoFunFi, $AreaFunFi, $cedulaFunDest, $codiProcDest,  $codiAreaDest, $tipoUsuario, $anexos, $codigoTd, $asunto, $saludo, $cuerpo, $despedida, $prioridad){

		$ciAnoActual = date("Y");
		$ciConsecutivo = $consecutivo;
		$ciAgencia = $idAgencia;
		$ciCedulaFunFirma = $cedulaFunFirma;				
		$ciProcesoFunFi = $procesoFunFi;
		$ciAreaFunFi = $AreaFunFi;
		$ciFechaEmision = date("m/d/Y G:i");
		$ciFechaEnvio = date("m/d/Y G:i");
		$ciCedulaFunDest = $cedulaFunDest;
		$ciCodiProcDest = $codiProcDest;
		$ciCodiAreaDest = $codiAreaDest;
		$ciTipoUsuario = $tipoUsuario;
		$ciAnexos = $anexos;
		$ciCodigoTd = $codigoTd;
		$ciAsunto = $asunto;
		$ciSaludo = $saludo;
		$ciCuerpo = htmlentities($cuerpo);
		$ciDespedida = $despedida;
		$ciPrioridad = $prioridad;		
		$usuario = strtolower($_SESSION["USUARIO"]);		
		echo " hola ".$sql="INSERT INTO cdi_CorrespInterna (
				ciAnoActual,
				ciConsecutivo,
				ciAgencia,
				ciCedulaFunFirma,
				ciProcesoFunFi,
				ciAreaFunFi,
				ciFechaEmision,
				ciFechaEnvio,
				ciCedulaFunDest,
				ciCodiProcDest,
				ciCodiAreaDest,
				ciTipoUsuario,
				ciAnexos,
				ciCodigoTd,
				ciAsunto,
				ciSaludo,
				ciCuerpo,
				ciDespedida,
				ciPrioridad,
				usuario)
			  VALUES (
			  	'".$ciAnoActual."', 
			  	".$ciConsecutivo.", 
			  	".$ciAgencia.", 
			  	".$ciCedulaFunFirma.", 
			  	'".$ciProcesoFunFi."', 
			  	".$ciAreaFunFi.", 
			  	'".$ciFechaEmision."', 
			  	'".$ciFechaEnvio."', 
			  	".$ciCedulaFunDest.", 
			  	'".$ciCodiProcDest."',
				".$ciCodiAreaDest.", 
				'".$ciTipoUsuario."',
				".$ciAnexos.", 
				".$ciCodigoTd.", 
				'".$ciAsunto."',
				'".$ciSaludo."', 
				'".$ciCuerpo."', 
				'".$ciDespedida."', 
				'".$ciPrioridad."',
				'".$usuario."')";
		$insertar = mssql_query($sql,$this->conexion);
		if($insertar!=true){
			echo "Error de Controlador de CDI --> No se pudo grabar el documento en CDI<br/>";
			$variableError = mssql_query("rollback transaction",$this->conexion);
                        exit();
			return false;
		}
		$commit = mssql_query("commit transaction",$this->conexion);
		if($commit!=true){
			echo "Error de Controlador de CDI --> No se pudo grabar el documento en CDI<br/>";
			$variableError = mssql_query("rollback transaction",$this->conexion);
			return false;
		}else{
			mssql_close($this->conexion);
			return true;
		}		
	}	
	
}

?>