<?php
/*if ( !isset( $_SESSION ['USUARIO'] ) ) {session_start();}
if(empty($_SESSION['LOGUIADO'])){
	echo "no hay session!!";
	exit();
}
$raiz=$_SESSION['RAIZ'];
include_once $raiz . DIRECTORY_SEPARATOR . 'phpComunes' . DIRECTORY_SEPARATOR . 'session.php';
session();
include_once $raiz . DIRECTORY_SEPARATOR . 'IFXDbConfiguracion.php';*/

$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;
include_once 'configDb.php';
include_once 'IFXDbConfiguracion.php';

/**
 * Description of IFXDBManejador
 *
 */
class SQLDbManejador {
//put your code here

    public $conexionID;
    public $error;
    private $logger;

    static $_instancia;

    /**
     * Constructor privado de la clase, crea la conexion a informix
     */
    public function __construct() {
        try {
            $this->conexionID = new PDO("dblib:host=". HOST_DB_APORTES .";dbname=". DB_NOMBRE_APORTES, USR_DB_APORTES, PASSW_DB_APORTES);
            $this->conexionID->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
            //$this->conexionID->setAttribute(PDO::ATTR_CASE, PDO::CASE_LOWER);
        }catch(PDOException $e) {
            $this->error=$e->getMessage();
            echo $this->error;
            self::$_instancia = null;
        }
    }

    /**
     * Previene la clonacion de la conexion
     */
    private function __clone() {

    }

    /**
     *Conecta con la base de datos, si ya se tiene conexion a la base de datos
     * devuelve la conexion que ya se encuentra establecida mediante el manejador
     *
     * @return IFXDbManager
     */
    public static function conectarDB() {
        if( ! (self::$_instancia instanceof self) ) {
            self::$_instancia = new self();
        }
    	
        return self::$_instancia;
    }


    public function setNivelError($nivel = 0) {
        switch ($nivel) {
            case 1:
                $this->conexionID->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
                break;
            case 2:
                $this->conexionID->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                break;
            default:
                $this->conexionID->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
                break;
        }
    }

    private function getNivelAsociacion($nivelAsoc = 0) {
        switch ($nivelAsoc) {
            case 1:
                $asoc = PDO::FETCH_NUM;
                break;
            case 2:
                $asoc = PDO::FETCH_BOTH;
                break;
            default;
                $asoc = PDO::FETCH_ASSOC;
                break;
        }
        return $asoc;
    }


    /**
     * Devuelve el recurso para manejar de manera manual las
     * funciones PDO de PHP
     *
     * @return <resource>
     */
    public function controlManualDb() {
        return $this->conexionID;
    }



    /**
     * Ejecuta sentencias sencillas SQL SELECT
     *
     * @param String $sql
     * @param Integer $tipoAsoc
     * @return PDOStatement
     */
    public function querySimple($sql, $tipoAsoc = 0) {
        $asoc = $this->getNivelAsociacion($tipoAsoc);
        try {
            $row = $this->conexionID->query($sql, $asoc);
        }catch(PDOException $e) {
			$this->error=$e->getMessage();
        }
        return $row;
    }


    public function queryComplejo() {
	
    }

    /**
     * Ejecuta sentencias SQL INSERT, UPDATE, DELETE
     * y devuelve el numero de filas afectadas
     *
     * ej. Al insertar un registro devuelve el numero 1, indicando
     * que una fila fue insertada
     *
     * @param <String> $sql
     * @return <Int>
     */

public function queryActualiza($sql){
	$numRows = $this->conexionID->exec($sql);
	if(intval($numRows)>=0){
		return $numRows;
	}
	else 
	{
		$e=$this->conexionID->errorInfo();
		$this->error=$e;
	}
}
	
public function queryInsert($sql,$tabla) {
	$numRows = $this->conexionID->exec($sql);
	if($numRows==0){
		$e=$this->conexionID->errorInfo();
		$this->error=$e;
	}
	else{
		return $this->conexionID->lastInsertId($tabla);
	} 
		
}	
		
public function sucia(){
	$sql = "SET ISOLATION TO DIRTY READ";
	$this->querySimple($sql);
	$sql = "SET LOCK MODE TO WAIT 5";
	$this->querySimple($sql);
}		

public function inicioTransaccion(){
	$sql="BEGIN WORK";
	$this->querySimple($sql);
} 	

public function cancelarTransaccion(){
	$sql="ROLLBACK WORK";
	$this->querySimple($sql);
}

public function confirmarTransaccion(){
	$sql="COMMIT WORK";
	$this->querySimple($sql);
}

function Definiciones($id,$orden){
	$sql="select * from aportes091 where iddefinicion=$id order by $orden";
	$result = $this->querySimple($sql);
	return $result;
	}	

function Departamento(){
	$sql="SELECT distinct coddepartamento,departmento FROM aportes089";
	$result = $this->querySimple($sql);
	return $result;
	}	
	
function Persona($idtd=0,$num=0,$flag=0){
	switch($flag){
		case 1 : $sql="Select idpersona, idtipodocumento, identificacion, papellido, sapellido, pnombre, snombre, sexo, fechanacimiento,estado from aportes015 where identificacion='$num'"; break;
		case 2 : $sql="Select idpersona, idtipodocumento, identificacion, papellido, sapellido, pnombre, snombre, sexo, fechanacimiento,estado from aportes015 where idtipodocumento=$idtd and identificacion='$num'"; break;
		case 3 : $sql="Select idpersona, idtipodocumento, identificacion, papellido, sapellido, pnombre, snombre, sexo, fechanacimiento,estado from aportes015 where pnombre like '$num'"; break;
		case 4 : $sql="Select idpersona, idtipodocumento, identificacion, papellido, sapellido, pnombre, snombre, sexo, fechanacimiento,estado from aportes015 where papellido like '$num'"; break;
		case 5 : $sql="Select idpersona, idtipodocumento, identificacion, papellido, sapellido, pnombre, snombre, sexo, fechanacimiento,estado from aportes015 where idpersona=$idtd"; break;
		}
	$result = $this->querySimple($sql);
	return $result;
	}	
	
function Persona_todo_id($idp){
	$sql="SELECT DISTINCT idtipodocumento, identificacion, papellido, sapellido, pnombre, snombre, sexo, direccion, idbarrio, telefono, celular, email, idpropiedadvivienda, idtipovivienda, idciuresidencia, iddepresidencia, idzona, idestadocivil, fechanacimiento, idciunace, iddepnace, capacidadtrabajo, idprofesion, ruaf, biometria, rutadocumentos, nombrecorto, year(getdate()-fechanacimiento -1)-1900 AS dias, aportes089.municipio AS zona FROM aportes015 LEFT JOIN aportes089 ON aportes015.idzona=aportes089.codzona WHERE idpersona=$idp";
	$result = $this->querySimple($sql);
	return $result;
	}	
	
public function ultimo_periodo(){
	$sql="select top 1 periodo from aportes012 where procesado='N' order by periodo desc";
	$result = $this->querySimple($sql);
	return $result;
}	

public function solo_nombres_id($idp){
	$sql="SELECT identificacion, papellido, sapellido, pnombre, snombre FROM aportes015 WHERE idpersona=$idp";
	$result = $this->querySimple($sql);
	return $result;
	}
		
function insertar_control($campos){
	date_default_timezone_set('America/Bogota');
	$hora=date("h:i:s A");
	$sql="INSERT INTO aportes149 (archivo, tipoarchivo, fechasistema, usuario, registros, totalregistros, registros1, totalregistros1, registros2, totalregistros2, registros3, totalregistros3, registros4, registros5, fechamovimiento,hora) VALUES ('".$campos[0]."','".$campos[1]."',cast(getdate() as date),'".$campos[3]."','".$campos[4]."','".$campos[5]."','".$campos[6]."','".$campos[7]."','".$campos[8]."','".$campos[9]."','".$campos[10]."','".$campos[11]."','".$campos[12]."','".$campos[13]."','".$campos[14]."','$hora')";
	return  $this->queryInsert($sql,$tabla);
}	
	
function buscar_archivo($id){
	$sql="SELECT count(*) as cuenta FROM aportes149 WHERE archivo='".$id."'";
	$result = $this->querySimple($sql);
	return $result;
}

public function b_obserbacion($id,$flag){
	try {
		$observaciones = "";
		$sql="Select observaciones from aportes088 where idregistro=$id and identidad=$flag";
		$rs=$this->conexionID->prepare($sql);
		$rs->execute();
		$rs->bindColumn(1, $observaciones, PDO::PARAM_STR);
		$rs->fetch();
		return $observaciones;			
	} catch (PDOException $e) {
		$this->error=$e->errorInfo;
	}
}

public function i_obserbacion($id,$ob,$flag,$tabla){
	try {
		$sql="Insert into aportes088 (idregistro,identidad,observaciones) values(?,?,?)";
		$rs=$this->conexionID->prepare($sql);
		$rs->bindParam(1, $id);
		$rs->bindParam(2, $flag);
		$rs->bindParam(3, $ob,PDO::PARAM_LOB);
		$filas=$rs->execute();
		if($filas==0){
			$e=$this->conexionID->errorInfo();
			$this->error=$e;
		}
		else{
			return $this->conexionID->lastInsertId($tabla);
		}
	} catch (PDOException $e) {
	}
}
}//fin class
/*
if(strstr($e->getMessage(), 'SQLSTATE[')) {
preg_match('/SQLSTATE\[(\w+)\] \[(\w+)\] (.*)/', $e->getMessage(), $matches);
$this->code = ($matches[1] == 'HT000' ? $matches[2] : $matches[1]);
$this->message = $matches[3];
} 
*/ 
$db = new SQLDbManejador();
?>
