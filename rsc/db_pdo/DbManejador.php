<?php
$root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
include_once  $root;


include_once $raiz . DIRECTORY_SEPARATOR . 'PDO_DSN.php';
//require_once 'PDO_DSN.php';

/**
 * Description of DBManejador
 *
 */
class DbManejador{
	
	private $_idDb;
	public $error;
	private $logger;
	
	private static $_instancia = array();
	
	/**
	 * Constructor privado de la clase, crea la conexion a informix
	 */
	private function __construct($dsn){
		
		$conf = PDO_DSN::_getDSN($dsn);
		try{
			$this->_idDb = new PDO($conf['dsn'], $conf['user'], $conf['password']);
			$this->_idDb->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
			$this->_idDb->setAttribute(PDO::ATTR_CASE, PDO::CASE_LOWER);
		}catch(PDOException $e){
			$this->error = $e->getMessage();
			self::$_instancia = null;
		}
	
	}
	
	/**
	 *Conecta con la base de datos, si ya se tiene conexion a la base de datos
	 * devuelve la conexion que ya se encuentra establecida mediante el manejador
	 *
	 * @return IFXDbManager
	 */
	public static function connectDB($dsn){
		if(! (self::$_instancia[$dsn] instanceof self) && ! isset(self::$_instancia[$dsn])){
			self::$_instancia[$dsn] = new self($dsn);
			if(self::$_instancia[$dsn] != null){
				return self::$_instancia[$dsn];
			}else{
				return false;
			}
		}else{
			return self::$_instancia[$dsn];
		}
	
	}
	
	public function setErrorLevel($level = 0){
		switch($level){
			case 1:
				$this->_idDb->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
				break;
			case 2:
				$this->_idDb->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				break;
			default:
				$this->_idDb->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
				break;
		}
	}
	
	private function getAsociationLevel($levelAsoc = 0){
		switch($levelAsoc){
			case 1:
				$asoc = PDO::FETCH_NUM;
				break;
			case 2:
				$asoc = PDO::FETCH_BOTH;
				break;
			default:
				$asoc = PDO::FETCH_ASSOC;
				break;
		}
		return $asoc;
	}
	
	/**
	 * Devuelve el recurso para manejar de manera manual las
	 * funciones PDO de PHP
	 *
	 * @return <resource>
	 */
	public function controlManualDb(){
		return $this->_idDb;
	}
	
	/**
	 * Ejecuta sentencias SQL SELECT
	 * Para ejecutar INSERT, UPDATE, DELETE, etc, usar el metodo queryUpdate
	 *
	 * @param String $sql
	 * @param Integer $trest Tipo de resultado 0 -> array, 1 -> JSON, default array
	 * @param Integer $tipoAsoc Nivel de asociacion 1 -> asociacion numerica, 2 -> asociacion numerica y asociativa, default asoacion asociativa
	 * 
	 * 
	 * @return PDOStatement
	 */
	public function query($sql, $trest = 0, $asoc = 0){
		
		$asoc = $this->getAsociationLevel($asoc);
		$array = array();
		$arrayjson = array();
		try{
			$rset = $this->_idDb->query($sql, $asoc);
			if($rset != false){
				$p1 = '%\A(?:((19|20)[0-9]{2})[-](0[1-9]|1[012])[-](0[1-9]|[12][0-9]|3[01]))\Z%';
				$p2 = '${3}/${4}/${1}';
				foreach($rset as $row){
					$row = array_map('trim', $row);
					$row = preg_replace($p1, $p2, $row);
					$rowjson = array_map('utf8_encode', $row);
					$array[] = $row;
					$arrayjson[] = $rowjson;
				}
				if($trest == 0){
					return $array;
				}elseif($trest == 1){
					return json_encode($arrayjson);
				}else{
					return false;
				}
			}else{
				return false;
			}
		}catch(PDOException $e){
			$this->error = $e->getMessage();
			return false;
		}
	
		//return $row;
	}
	
	/**
	 * Ejecuta sentencias SQL INSERT, UPDATE, DELETE
	 * y devuelve el numero de filas afectadas
	 *
	 * ej. Al insertar un registro devuelve el numero 1, indicando
	 * que una fila fue insertada
	 *
	 * @param <String> $sql
	 * @return <Int>
	 */
	public function queryUpdate($sql){
		$numRows = $this->_idDb->exec($sql);
		if($numRows == 0){
			$e = $this->_idDb->errorInfo();
			$this->error = $e;
			return false;
		}else{
			return $numRows;
		}
	}
	
	public function queryInsert($sql, $tabla){
		$numRows = $this->_idDb->exec($sql);
		if($numRows == 0){
			$e = $this->_idDb->errorInfo();
			$this->error = $e;
			return false;
		}else{
			return $this->_idDb->lastInsertId($tabla);
		}
	
	}

} //fin class
/*
if(strstr($e->getMessage(), 'SQLSTATE[')) {
preg_match('/SQLSTATE\[(\w+)\] \[(\w+)\] (.*)/', $e->getMessage(), $matches);
$this->code = ($matches[1] == 'HT000' ? $matches[2] : $matches[1]);
$this->message = $matches[3];
} 
*/
/*$db = IFXDbManejador::connectDB('aportes');
$st = $db->query("SELECT * FROM aportes015 WHERE identificacion = '7720360'",1,1);
$db2 = IFXDbManejador::connectDB('aportes_dev');
$st1 = $db2->query("SELECT * FROM aportes015 WHERE identificacion = '26431739'",1,1);
$db = IFXDbManejador::connectDB('aportes');*/
?>
