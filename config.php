<?php
/* autor:       orlando puentes
 * fecha:       02/09/2010
 * objetivo:    
 */
//mod29/08/2012 $root= $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . 'sigas' . DIRECTORY_SEPARATOR . 'session.php';
//include_once  $root;
 
$raiz=dirname(__FILE__);
$raiz2=substr($raiz,0,-6);
include_once  $raiz.DIRECTORY_SEPARATOR.'vendor'.DIRECTORY_SEPARATOR.'autoload.php';
use Monolog\Logger;
use Monolog\Handler\StreamHandler;


$imagenes=$raiz2 . DIRECTORY_SEPARATOR . "imagenes" . DIRECTORY_SEPARATOR;
$ruta_generados= $raiz2 . DIRECTORY_SEPARATOR . 'planos_generados' . DIRECTORY_SEPARATOR;
$ruta_cargados= $raiz2 . DIRECTORY_SEPARATOR . "planos_cargados" . DIRECTORY_SEPARATOR;
define('ruta_cargados2', $raiz2 . DIRECTORY_SEPARATOR . "planos_cargados" . DIRECTORY_SEPARATOR);
$ruta_movimientos= $raiz2 . DIRECTORY_SEPARATOR . "planos_cargados" . "movimientos" . DIRECTORY_SEPARATOR;
$ip=$_SERVER['SERVER_NAME'];
$url="http://".$ip."/sigas/";
$url_img="http://".$ip."/imagenes/";
$url_dig="http://".$ip."/";
$url_rep="http://".$ip."/sigas/centroReportes/";
//$ruta_reportes="http://".$ip.":8080/sigasReportes/";
$ruta_reportes="http://10.10.1.57:8080/sigasReportes/";

$url2 ="http://".$ip."/";
$conexion_ifx ="jdbc:informix-sqli://10.10.1.190:9088/aportes:informixserver=sigas_dev;user=informix;password=informix";

define("DESARROLLO",1);
define("URL_PORTAL",$url);
define("URL_SERVIDOR_ARCHIVOS_PLANOS","http://". $ip ."/");
define("DIRECTORIO_RAIZ",$raiz);
define("DIRECTORIO_CENTRO_REPORTES","centroReportes/");
define("DIRECTORIO_LOGS","logs/");
define("DIRECTORIO_PHP_COMUNES","phpComunes/");

define("RUTA_FISICA_CENTRO_REPORTES",DIRECTORIO_RAIZ.DIRECTORIO_CENTRO_REPORTES);
define("DIRECTORIO_CLASES","clases/");
define("DIRECTORIO_CLASES_RSC","rsc/");
define("RUTA_FISICA_DIRECTORIO_CLASES",DIRECTORIO_RAIZ.DIRECTORIO_CLASES);
define("RUTA_FISICA_DIRECTORIO_CLASES_RSC",DIRECTORIO_RAIZ. DIRECTORY_SEPARATOR .DIRECTORIO_CLASES_RSC);

define("RUTA_DIR_ARCHIVOS_LOGS", "/var/www/html/logs/");
define("DIR_ARCHIVOS_PLANOS_CARGADOS", "/var/www/html/planos_cargados/");
define("DIR_ARCHIVOS_PLANOS_GENERADOS", "/var/www/html/planos_generados/");
define("CADENA_CONEXION_INFORMIX",$conexion_ifx);
define("URL_JAVABRIDGE","http://127.0.0.1:8080/JavaBridge/java/Java.inc");
define("URL_REPORTES_JSP","http://10.10.1.57:8080");
define("DIRECTORIO_REPORTES_JSP","sigasReportes");

define("DIRECTORIO_ESTILOS_CSS","css/");
define("DIRECTORIO_IMAGENES","imagenes/");
define("DIRECTORIO_ESTILOS_CSS_FORMULARIOS",DIRECTORIO_ESTILOS_CSS."formularios/");

define("DIRECTORIO_SCRIPTS_JS","js/");
define("DIRECTORIO_SCRIPTS_JS_FORMULARIOS",DIRECTORIO_SCRIPTS_JS."formularios/");

define("DSN_CDI","");
define("USER_DB_CDI","");
define("PASS_USER_DB_CDI","");
define("DATABASE_CDI","");
define("PROCESO_DESARROLLO_FINANCIERO_CDI","DF");
define("AREA_SUBSIDIO_CDI",7);
define("PROCESO_AREA_SUBSIDIO_CDI","DF-07");

// servidor y rutas digitalización de documentos
define("SERVIDOR_IMAGENES_DIGITALIZADAS","10.10.1.55:8080");

global $arregloAgencias;
$arregloAgencias = array(
						"01" => "Neiva",
						"02" => "Garzon",
						"03" => "Pitalito",
						"04" => "La plata");



define ('USUARIO_WEB_SIGAS','');
define ('CONTRASENA_WEB_SIGAS','');

define('COMPROBANTE_PLANILLA_UNICA','CPU');
define('CUENTA_CONTABLE_CREDITO','410505001');
define('CUENTA_CONTABLE_DEBITO','111005049'); //183001001
define('NUEVA_CUENTA_CONTABLE_CREDITO','2.111005.0703');
define('NUEVA_CUENTA_CONTABLE_DEBITO','02090401.410505.05');

define('DOCUMENT_TYPE_CODE','L4');//usado en serviceTesoreria
define('CENTRO_DE_COSTO','010401'); //090105
define('CODIGO_INSTIT_COMFAMILIAR',2695);

global $comprobantesAportes;
$comprobantesAportes = array("99", "RC1", "RC2", "RC3", "RC4", "RC5", "RC6", "R10", "CP1", "CP2", "CP3","PU2","CO5","NC2","CRC");

// Las planillas únicas que quedan marcadas con este usuario son las que se han cargado por medio del módulo de PU.
define("USUARIO_CARGA_PLANILLA_UNICA","PorPlanilla");

function log_info($message,$array=null)
{
	$raiz=dirname(__FILE__);
	// create a log channel
	$stream = new StreamHandler($raiz.'/logs/your.log', Logger::DEBUG);
	$log = new Logger('Log');
	$log->pushHandler($stream);

	if ($array!=null)
		$log->addInfo($message,$array);
	else
		$log->addInfo($message);
}

function debug() {
	$trace = debug_backtrace();
	$rootPath = dirname(dirname(__FILE__));
	$file = str_replace($rootPath, '', $trace[0]['file']);
	$line = $trace[0]['line'];
	$var = $trace[0]['args'][0];
	$lineInfo = sprintf('<div><strong>%s</strong> (line <strong>%s</strong>)</div>', $file, $line);
	$debugInfo = sprintf('<pre>%s</pre>', print_r($var, true));
	print_r($lineInfo.$debugInfo);
}
?>